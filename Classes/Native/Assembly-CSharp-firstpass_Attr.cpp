﻿#include "pch-cpp.hpp"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include <limits>
#include <stdint.h>



// System.Char[]
struct CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34;
// System.Type[]
struct TypeU5BU5D_t85B10489E46F06CEC7C4B1CCBD0E01FAB6649755;
// System.Reflection.Binder
struct Binder_t2BEE27FD84737D1E79BC47FD67F6D3DD2F2DDA30;
// System.Runtime.CompilerServices.CompilationRelaxationsAttribute
struct CompilationRelaxationsAttribute_t661FDDC06629BDA607A42BD660944F039FE03AFF;
// System.Runtime.CompilerServices.CompilerGeneratedAttribute
struct CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C;
// System.Diagnostics.DebuggableAttribute
struct DebuggableAttribute_tA8054EBD0FC7511695D494B690B5771658E3191B;
// System.Diagnostics.DebuggerHiddenAttribute
struct DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88;
// UnityEngine.DisallowMultipleComponent
struct DisallowMultipleComponent_tDB3D3DBC9AC523A0BD11DA0B7D88F960FDB89E3E;
// UnityEngine.ExecuteInEditMode
struct ExecuteInEditMode_tAA3B5DE8B7E207BC6CAAFDB1F2502350C0546173;
// System.Runtime.CompilerServices.ExtensionAttribute
struct ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC;
// UnityEngine.HeaderAttribute
struct HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB;
// UnityEngine.HelpURLAttribute
struct HelpURLAttribute_t0924A6D83FABA7B77780F7F9BBCBCB9E0EA15023;
// UnityEngine.HideInInspector
struct HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA;
// System.Runtime.CompilerServices.IteratorStateMachineAttribute
struct IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830;
// System.Reflection.MemberFilter
struct MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81;
// UnityEngine.MultilineAttribute
struct MultilineAttribute_tA4DCA9D8519E0C1B05636F2EFF74F81335A7F291;
// System.ParamArrayAttribute
struct ParamArrayAttribute_t9DCEB4CDDB8EDDB1124171D4C51FA6069EEA5C5F;
// UnityEngine.RangeAttribute
struct RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5;
// UnityEngine.RequireComponent
struct RequireComponent_tEDA546F9722B8874DA9658BDAB821BA49647FC91;
// System.Runtime.CompilerServices.RuntimeCompatibilityAttribute
struct RuntimeCompatibilityAttribute_tFF99AB2963098F9CBCD47A20D9FD3D51C17C1C80;
// System.String
struct String_t;
// UnityEngine.TooltipAttribute
struct TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B;
// System.Type
struct Type_t;
// System.Void
struct Void_t700C6383A2A510C2CF4DD86DABD5CA9FF70ADAC5;

IL2CPP_EXTERN_C const RuntimeType* AudioSource_tC4BF65AF8CDCAA63724BB3CA59A7A29249269E6B_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* Renderer_t58147AB5B00224FE1460FD47542DC0DA7EC9378C_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CCreateNameInputFieldU3Ed__13_tF99DAFC92BB23A5FF3D97BB4B86C12186720DDCE_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CCreateNameInputFieldU3Ed__18_tCF407195076F20F3CC29E9320B5836378BF87FA4_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CCreateTalkInputFieldU3Ed__14_t8E84DBC2AA498A934C2001267574FE7D360F832D_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CCreateTalkInputFieldU3Ed__19_t5C727E8D85F30A4CC308C816B210A1FD28056A29_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CDialogSequenceU3Ed__25_t44374F6C645D70CD0D9FC61B194C18AF79DBA159_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CDoHighlightU3Ed__5_tF820C5AFB9FD264363835FEA11DC3D2FAB920E17_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CGenerateU3Ed__27_t733346A9DBD137DA2DD7EA3252EFFE76500D3C5B_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CGenerateU3Ed__31_tFA662A35AA694F207DCA5B6C83CA1964EBA15A73_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CGenerateU3Ed__39_t03292738BAB852A4B9E700EEBCEA2F4808456175_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CGetResultU3Ed__13_t429D1A37625B8E217E88830CCFE7B4B9E5F3DCED_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CGetVoicesU3Ed__10_t89A30C17D49996122417B745EB21C1F275A12870_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CGetVoicesU3Ed__11_t766D855FBC27F4F8B65AF9AB254CFF24E3F5F600_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CGetVoicesU3Ed__21_tBCC92EF8427358114BDF9FE975C411924B4B8CF5_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CGetVoicesU3Ed__22_t63C88EA26724AA6C07C27964299A020FC5C9D217_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CGetVoicesU3Ed__23_t4C74B1E95210BDA509B6AB73C0CE5286B905B3A1_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CGetVoicesU3Ed__24_tC53D4CB8A21E838EE8088A9FDD86CA7324EB8050_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CGetVoicesU3Ed__32_tD206C9C44DE7C20EB7573ED4D8761B0FD13677FD_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CInitU3Ed__10_tDC9857230B9D766BA44C1A8CD9BC89DAF332560A_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CInitU3Ed__12_tD8DCA3016689D1AC98B5293D2E98515302FC46F6_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CProxyLanguagesU3Ed__19_t3B013A40DC9B77C22F68441BA9F709DD9D113FE9_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CProxyOnEndU3Ed__16_t496FEB12873920FC762FFCB8CFEC07F32B3F5DBC_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CProxyOnEndU3Ed__3_tB3A6640F746B233069CBE83B8CE0930F714BCAAA_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CProxyUtteranceU3Ed__14_t8193D4147E2CE182457EAEF45EDACC550B23BCCF_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CProxyVoicesU3Ed__15_tD895D1F2A7ED22311F8F67BAF0D4593B7AF528B9_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CRunPendingCommandsU3Ed__12_t3B51F7A544880108D20AA679685AAD3200FD6E8D_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CRunPendingCommandsU3Ed__13_t7581E10AF39B06A7884DCFFC6F826010995D4F7F_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CSetPitchU3Ed__25_t00215C2568848CF0F1B6EC7FD3965ED387698544_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CSetPitchU3Ed__27_tEFABEA1BBCBB4634FFC95C1C6C6BC2E1AB296B1B_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CSetPitchU3Ed__35_tF30264CE7FB8EFB8DD7CC3A54F6E538C7526D22E_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CSetRateU3Ed__26_tB66C71381FE02958DCCB4A1A50724DFFA9A78477_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CSetRateU3Ed__28_t47111ED4117F0FE7EB7944C25EE6DE198191DDC9_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CSetRateU3Ed__36_tE587DF9904D56F1F471E8AAB2C639678AE1C2983_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CSpeakNativeU3Ed__29_tAE002A72C2A3F020A17BF4A56FEBBC5EA2787303_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CSpeakNativeU3Ed__29_tAEFCE1D6AFE6BEB16F03F830C079DAA23C35B481_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CSpeakNativeU3Ed__37_tB56F739052E648271E910C501E0E5DE0A2826C12_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CSpeakU3Ed__28_t51C9498C945D57FD321E65C4CD6972BB1933A896_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CSpeakU3Ed__30_tC962B657041FD44AF5A88FD52604438A78BA1649_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CSpeakU3Ed__38_t0CAF7D4D1537BFF140CFFE747E35BD71F0C3FD33_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CSpeakerBU3Ed__8_t4FDA50AFC81C4F929F34CFAB06B4B0B6AF635A06_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CStartU3Ed__10_t25226C3095C4900C7D3EC61B87E5A46ECF2CF359_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CStartU3Ed__10_t4C950ADFDCBB18E14D5159D1D6BC6D49ABEC06D0_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CStartU3Ed__10_t5B930EBF98D13360F6F542B9931A660A8A3383D9_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CStartU3Ed__10_t84D96784539E4E6C4F0B0D80CF2BE76078D3C65B_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CStartU3Ed__19_t61919FD2ED3E01C9A816502E48D6BBE8CC4D515B_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CStartU3Ed__1_t24B0D6D6CCC34B861307115FF573C6287C8DEBF8_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CStartU3Ed__21_tCD0B0FE65242BC30DD807F964ABE50628F2C6FD8_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CStartU3Ed__21_tFD573D57D53E02687F393696FC999F7C0DDB846E_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CStartU3Ed__22_t3F06EB55E379386889BEE897F565664726CFB9B9_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CStartU3Ed__28_t15833A9AB280EADB404BBB5150CC5E571050157D_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CStartU3Ed__2_t69FDEAF9EA13E082CF88C7B60B90C5A05CC6179C_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CStartU3Ed__7_t99F1576B8ECF7C51829E8FD496D881EC6A07024C_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CStartU3Ed__9_t0AC6DFD8163B675E7D948040950A9AD90DE2F27F_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CStartU3Ed__9_t36FCBB780B64B702CF4ABD4EBB368E6B34586D7B_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CStartU3Ed__9_t9F2C26950487FA4AA558AF9FDBC70EFC86CA1C3A_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CgenerateU3Ed__19_t158A067925EC863CE90E4DCCF85D7536E97CDBA0_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CgetVoicesU3Ed__33_t42277CC46AC28C4C054B2A9350319771F5FBA45D_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CinitalizeVoicesU3Ed__10_t2A275B1AA68149AB13303E07E9688291E5EF2C20_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3ClerpAlphaDownU3Ed__8_tFA588D110070DFAE10E2AAC390DE45F2C6DCE063_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3ClerpAlphaUpU3Ed__9_tA4B9A2133A395822BDCF87133798169872E8B6B1_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CplayAudioFileU3Ed__79_t5D80B48C5103969538788365776FBFD2481F2989_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CplayAudioFileU3Ed__81_t16FD3617907DBC006BF7EDBFDD79142630A5DC55_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CplayMeU3Ed__19_tDCB0B566C9E09125455A47D5F21A01BB9E163B27_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CprocessStackU3Ed__42_tED87FEFE7D9355FCAD33EAAEFFDA223FAB4C41D2_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CspeakU3Ed__32_tDEC1697FFF0729D0B77A9482BE143E6DC4C893CD_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CspeakU3Ed__41_t6D7ABE0037F6C095D53C8134C299473D203F6A54_0_0_0_var;


IL2CPP_EXTERN_C_BEGIN
IL2CPP_EXTERN_C_END

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Object


// System.Attribute
struct Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71  : public RuntimeObject
{
public:

public:
};


// System.Reflection.MemberInfo
struct MemberInfo_t  : public RuntimeObject
{
public:

public:
};


// System.String
struct String_t  : public RuntimeObject
{
public:
	// System.Int32 System.String::m_stringLength
	int32_t ___m_stringLength_0;
	// System.Char System.String::m_firstChar
	Il2CppChar ___m_firstChar_1;

public:
	inline static int32_t get_offset_of_m_stringLength_0() { return static_cast<int32_t>(offsetof(String_t, ___m_stringLength_0)); }
	inline int32_t get_m_stringLength_0() const { return ___m_stringLength_0; }
	inline int32_t* get_address_of_m_stringLength_0() { return &___m_stringLength_0; }
	inline void set_m_stringLength_0(int32_t value)
	{
		___m_stringLength_0 = value;
	}

	inline static int32_t get_offset_of_m_firstChar_1() { return static_cast<int32_t>(offsetof(String_t, ___m_firstChar_1)); }
	inline Il2CppChar get_m_firstChar_1() const { return ___m_firstChar_1; }
	inline Il2CppChar* get_address_of_m_firstChar_1() { return &___m_firstChar_1; }
	inline void set_m_firstChar_1(Il2CppChar value)
	{
		___m_firstChar_1 = value;
	}
};

struct String_t_StaticFields
{
public:
	// System.String System.String::Empty
	String_t* ___Empty_5;

public:
	inline static int32_t get_offset_of_Empty_5() { return static_cast<int32_t>(offsetof(String_t_StaticFields, ___Empty_5)); }
	inline String_t* get_Empty_5() const { return ___Empty_5; }
	inline String_t** get_address_of_Empty_5() { return &___Empty_5; }
	inline void set_Empty_5(String_t* value)
	{
		___Empty_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___Empty_5), (void*)value);
	}
};


// System.ValueType
struct ValueType_tDBF999C1B75C48C68621878250DBF6CDBCF51E52  : public RuntimeObject
{
public:

public:
};

// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_tDBF999C1B75C48C68621878250DBF6CDBCF51E52_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_tDBF999C1B75C48C68621878250DBF6CDBCF51E52_marshaled_com
{
};

// System.Boolean
struct Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37 
{
public:
	// System.Boolean System.Boolean::m_value
	bool ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37, ___m_value_0)); }
	inline bool get_m_value_0() const { return ___m_value_0; }
	inline bool* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(bool value)
	{
		___m_value_0 = value;
	}
};

struct Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37_StaticFields
{
public:
	// System.String System.Boolean::TrueString
	String_t* ___TrueString_5;
	// System.String System.Boolean::FalseString
	String_t* ___FalseString_6;

public:
	inline static int32_t get_offset_of_TrueString_5() { return static_cast<int32_t>(offsetof(Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37_StaticFields, ___TrueString_5)); }
	inline String_t* get_TrueString_5() const { return ___TrueString_5; }
	inline String_t** get_address_of_TrueString_5() { return &___TrueString_5; }
	inline void set_TrueString_5(String_t* value)
	{
		___TrueString_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___TrueString_5), (void*)value);
	}

	inline static int32_t get_offset_of_FalseString_6() { return static_cast<int32_t>(offsetof(Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37_StaticFields, ___FalseString_6)); }
	inline String_t* get_FalseString_6() const { return ___FalseString_6; }
	inline String_t** get_address_of_FalseString_6() { return &___FalseString_6; }
	inline void set_FalseString_6(String_t* value)
	{
		___FalseString_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___FalseString_6), (void*)value);
	}
};


// System.Runtime.CompilerServices.CompilationRelaxationsAttribute
struct CompilationRelaxationsAttribute_t661FDDC06629BDA607A42BD660944F039FE03AFF  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.Int32 System.Runtime.CompilerServices.CompilationRelaxationsAttribute::m_relaxations
	int32_t ___m_relaxations_0;

public:
	inline static int32_t get_offset_of_m_relaxations_0() { return static_cast<int32_t>(offsetof(CompilationRelaxationsAttribute_t661FDDC06629BDA607A42BD660944F039FE03AFF, ___m_relaxations_0)); }
	inline int32_t get_m_relaxations_0() const { return ___m_relaxations_0; }
	inline int32_t* get_address_of_m_relaxations_0() { return &___m_relaxations_0; }
	inline void set_m_relaxations_0(int32_t value)
	{
		___m_relaxations_0 = value;
	}
};


// System.Runtime.CompilerServices.CompilerGeneratedAttribute
struct CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:

public:
};


// System.Diagnostics.DebuggerHiddenAttribute
struct DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:

public:
};


// UnityEngine.DisallowMultipleComponent
struct DisallowMultipleComponent_tDB3D3DBC9AC523A0BD11DA0B7D88F960FDB89E3E  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:

public:
};


// System.Enum
struct Enum_t23B90B40F60E677A8025267341651C94AE079CDA  : public ValueType_tDBF999C1B75C48C68621878250DBF6CDBCF51E52
{
public:

public:
};

struct Enum_t23B90B40F60E677A8025267341651C94AE079CDA_StaticFields
{
public:
	// System.Char[] System.Enum::enumSeperatorCharArray
	CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34* ___enumSeperatorCharArray_0;

public:
	inline static int32_t get_offset_of_enumSeperatorCharArray_0() { return static_cast<int32_t>(offsetof(Enum_t23B90B40F60E677A8025267341651C94AE079CDA_StaticFields, ___enumSeperatorCharArray_0)); }
	inline CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34* get_enumSeperatorCharArray_0() const { return ___enumSeperatorCharArray_0; }
	inline CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34** get_address_of_enumSeperatorCharArray_0() { return &___enumSeperatorCharArray_0; }
	inline void set_enumSeperatorCharArray_0(CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34* value)
	{
		___enumSeperatorCharArray_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___enumSeperatorCharArray_0), (void*)value);
	}
};

// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t23B90B40F60E677A8025267341651C94AE079CDA_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t23B90B40F60E677A8025267341651C94AE079CDA_marshaled_com
{
};

// UnityEngine.ExecuteInEditMode
struct ExecuteInEditMode_tAA3B5DE8B7E207BC6CAAFDB1F2502350C0546173  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:

public:
};


// System.Runtime.CompilerServices.ExtensionAttribute
struct ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:

public:
};


// UnityEngine.HelpURLAttribute
struct HelpURLAttribute_t0924A6D83FABA7B77780F7F9BBCBCB9E0EA15023  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.String UnityEngine.HelpURLAttribute::m_Url
	String_t* ___m_Url_0;
	// System.Boolean UnityEngine.HelpURLAttribute::m_Dispatcher
	bool ___m_Dispatcher_1;
	// System.String UnityEngine.HelpURLAttribute::m_DispatchingFieldName
	String_t* ___m_DispatchingFieldName_2;

public:
	inline static int32_t get_offset_of_m_Url_0() { return static_cast<int32_t>(offsetof(HelpURLAttribute_t0924A6D83FABA7B77780F7F9BBCBCB9E0EA15023, ___m_Url_0)); }
	inline String_t* get_m_Url_0() const { return ___m_Url_0; }
	inline String_t** get_address_of_m_Url_0() { return &___m_Url_0; }
	inline void set_m_Url_0(String_t* value)
	{
		___m_Url_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Url_0), (void*)value);
	}

	inline static int32_t get_offset_of_m_Dispatcher_1() { return static_cast<int32_t>(offsetof(HelpURLAttribute_t0924A6D83FABA7B77780F7F9BBCBCB9E0EA15023, ___m_Dispatcher_1)); }
	inline bool get_m_Dispatcher_1() const { return ___m_Dispatcher_1; }
	inline bool* get_address_of_m_Dispatcher_1() { return &___m_Dispatcher_1; }
	inline void set_m_Dispatcher_1(bool value)
	{
		___m_Dispatcher_1 = value;
	}

	inline static int32_t get_offset_of_m_DispatchingFieldName_2() { return static_cast<int32_t>(offsetof(HelpURLAttribute_t0924A6D83FABA7B77780F7F9BBCBCB9E0EA15023, ___m_DispatchingFieldName_2)); }
	inline String_t* get_m_DispatchingFieldName_2() const { return ___m_DispatchingFieldName_2; }
	inline String_t** get_address_of_m_DispatchingFieldName_2() { return &___m_DispatchingFieldName_2; }
	inline void set_m_DispatchingFieldName_2(String_t* value)
	{
		___m_DispatchingFieldName_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_DispatchingFieldName_2), (void*)value);
	}
};


// UnityEngine.HideInInspector
struct HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:

public:
};


// System.IntPtr
struct IntPtr_t 
{
public:
	// System.Void* System.IntPtr::m_value
	void* ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(IntPtr_t, ___m_value_0)); }
	inline void* get_m_value_0() const { return ___m_value_0; }
	inline void** get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(void* value)
	{
		___m_value_0 = value;
	}
};

struct IntPtr_t_StaticFields
{
public:
	// System.IntPtr System.IntPtr::Zero
	intptr_t ___Zero_1;

public:
	inline static int32_t get_offset_of_Zero_1() { return static_cast<int32_t>(offsetof(IntPtr_t_StaticFields, ___Zero_1)); }
	inline intptr_t get_Zero_1() const { return ___Zero_1; }
	inline intptr_t* get_address_of_Zero_1() { return &___Zero_1; }
	inline void set_Zero_1(intptr_t value)
	{
		___Zero_1 = value;
	}
};


// System.ParamArrayAttribute
struct ParamArrayAttribute_t9DCEB4CDDB8EDDB1124171D4C51FA6069EEA5C5F  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:

public:
};


// UnityEngine.PropertyAttribute
struct PropertyAttribute_t4A352471DF625C56C811E27AC86B7E1CE6444052  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:

public:
};


// UnityEngine.RequireComponent
struct RequireComponent_tEDA546F9722B8874DA9658BDAB821BA49647FC91  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.Type UnityEngine.RequireComponent::m_Type0
	Type_t * ___m_Type0_0;
	// System.Type UnityEngine.RequireComponent::m_Type1
	Type_t * ___m_Type1_1;
	// System.Type UnityEngine.RequireComponent::m_Type2
	Type_t * ___m_Type2_2;

public:
	inline static int32_t get_offset_of_m_Type0_0() { return static_cast<int32_t>(offsetof(RequireComponent_tEDA546F9722B8874DA9658BDAB821BA49647FC91, ___m_Type0_0)); }
	inline Type_t * get_m_Type0_0() const { return ___m_Type0_0; }
	inline Type_t ** get_address_of_m_Type0_0() { return &___m_Type0_0; }
	inline void set_m_Type0_0(Type_t * value)
	{
		___m_Type0_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Type0_0), (void*)value);
	}

	inline static int32_t get_offset_of_m_Type1_1() { return static_cast<int32_t>(offsetof(RequireComponent_tEDA546F9722B8874DA9658BDAB821BA49647FC91, ___m_Type1_1)); }
	inline Type_t * get_m_Type1_1() const { return ___m_Type1_1; }
	inline Type_t ** get_address_of_m_Type1_1() { return &___m_Type1_1; }
	inline void set_m_Type1_1(Type_t * value)
	{
		___m_Type1_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Type1_1), (void*)value);
	}

	inline static int32_t get_offset_of_m_Type2_2() { return static_cast<int32_t>(offsetof(RequireComponent_tEDA546F9722B8874DA9658BDAB821BA49647FC91, ___m_Type2_2)); }
	inline Type_t * get_m_Type2_2() const { return ___m_Type2_2; }
	inline Type_t ** get_address_of_m_Type2_2() { return &___m_Type2_2; }
	inline void set_m_Type2_2(Type_t * value)
	{
		___m_Type2_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Type2_2), (void*)value);
	}
};


// System.Runtime.CompilerServices.RuntimeCompatibilityAttribute
struct RuntimeCompatibilityAttribute_tFF99AB2963098F9CBCD47A20D9FD3D51C17C1C80  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.Boolean System.Runtime.CompilerServices.RuntimeCompatibilityAttribute::m_wrapNonExceptionThrows
	bool ___m_wrapNonExceptionThrows_0;

public:
	inline static int32_t get_offset_of_m_wrapNonExceptionThrows_0() { return static_cast<int32_t>(offsetof(RuntimeCompatibilityAttribute_tFF99AB2963098F9CBCD47A20D9FD3D51C17C1C80, ___m_wrapNonExceptionThrows_0)); }
	inline bool get_m_wrapNonExceptionThrows_0() const { return ___m_wrapNonExceptionThrows_0; }
	inline bool* get_address_of_m_wrapNonExceptionThrows_0() { return &___m_wrapNonExceptionThrows_0; }
	inline void set_m_wrapNonExceptionThrows_0(bool value)
	{
		___m_wrapNonExceptionThrows_0 = value;
	}
};


// System.Runtime.CompilerServices.StateMachineAttribute
struct StateMachineAttribute_tA6E77C77F821508E405473BA1C4C08A69FDA0AC3  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.Type System.Runtime.CompilerServices.StateMachineAttribute::<StateMachineType>k__BackingField
	Type_t * ___U3CStateMachineTypeU3Ek__BackingField_0;

public:
	inline static int32_t get_offset_of_U3CStateMachineTypeU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(StateMachineAttribute_tA6E77C77F821508E405473BA1C4C08A69FDA0AC3, ___U3CStateMachineTypeU3Ek__BackingField_0)); }
	inline Type_t * get_U3CStateMachineTypeU3Ek__BackingField_0() const { return ___U3CStateMachineTypeU3Ek__BackingField_0; }
	inline Type_t ** get_address_of_U3CStateMachineTypeU3Ek__BackingField_0() { return &___U3CStateMachineTypeU3Ek__BackingField_0; }
	inline void set_U3CStateMachineTypeU3Ek__BackingField_0(Type_t * value)
	{
		___U3CStateMachineTypeU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CStateMachineTypeU3Ek__BackingField_0), (void*)value);
	}
};


// System.Void
struct Void_t700C6383A2A510C2CF4DD86DABD5CA9FF70ADAC5 
{
public:
	union
	{
		struct
		{
		};
		uint8_t Void_t700C6383A2A510C2CF4DD86DABD5CA9FF70ADAC5__padding[1];
	};

public:
};


// System.Reflection.BindingFlags
struct BindingFlags_tAAAB07D9AC588F0D55D844E51D7035E96DF94733 
{
public:
	// System.Int32 System.Reflection.BindingFlags::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(BindingFlags_tAAAB07D9AC588F0D55D844E51D7035E96DF94733, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// UnityEngine.HeaderAttribute
struct HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB  : public PropertyAttribute_t4A352471DF625C56C811E27AC86B7E1CE6444052
{
public:
	// System.String UnityEngine.HeaderAttribute::header
	String_t* ___header_0;

public:
	inline static int32_t get_offset_of_header_0() { return static_cast<int32_t>(offsetof(HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB, ___header_0)); }
	inline String_t* get_header_0() const { return ___header_0; }
	inline String_t** get_address_of_header_0() { return &___header_0; }
	inline void set_header_0(String_t* value)
	{
		___header_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___header_0), (void*)value);
	}
};


// System.Runtime.CompilerServices.IteratorStateMachineAttribute
struct IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830  : public StateMachineAttribute_tA6E77C77F821508E405473BA1C4C08A69FDA0AC3
{
public:

public:
};


// UnityEngine.MultilineAttribute
struct MultilineAttribute_tA4DCA9D8519E0C1B05636F2EFF74F81335A7F291  : public PropertyAttribute_t4A352471DF625C56C811E27AC86B7E1CE6444052
{
public:
	// System.Int32 UnityEngine.MultilineAttribute::lines
	int32_t ___lines_0;

public:
	inline static int32_t get_offset_of_lines_0() { return static_cast<int32_t>(offsetof(MultilineAttribute_tA4DCA9D8519E0C1B05636F2EFF74F81335A7F291, ___lines_0)); }
	inline int32_t get_lines_0() const { return ___lines_0; }
	inline int32_t* get_address_of_lines_0() { return &___lines_0; }
	inline void set_lines_0(int32_t value)
	{
		___lines_0 = value;
	}
};


// UnityEngine.RangeAttribute
struct RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5  : public PropertyAttribute_t4A352471DF625C56C811E27AC86B7E1CE6444052
{
public:
	// System.Single UnityEngine.RangeAttribute::min
	float ___min_0;
	// System.Single UnityEngine.RangeAttribute::max
	float ___max_1;

public:
	inline static int32_t get_offset_of_min_0() { return static_cast<int32_t>(offsetof(RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5, ___min_0)); }
	inline float get_min_0() const { return ___min_0; }
	inline float* get_address_of_min_0() { return &___min_0; }
	inline void set_min_0(float value)
	{
		___min_0 = value;
	}

	inline static int32_t get_offset_of_max_1() { return static_cast<int32_t>(offsetof(RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5, ___max_1)); }
	inline float get_max_1() const { return ___max_1; }
	inline float* get_address_of_max_1() { return &___max_1; }
	inline void set_max_1(float value)
	{
		___max_1 = value;
	}
};


// System.RuntimeTypeHandle
struct RuntimeTypeHandle_tC33965ADA3E041E0C94AF05E5CB527B56482CEF9 
{
public:
	// System.IntPtr System.RuntimeTypeHandle::value
	intptr_t ___value_0;

public:
	inline static int32_t get_offset_of_value_0() { return static_cast<int32_t>(offsetof(RuntimeTypeHandle_tC33965ADA3E041E0C94AF05E5CB527B56482CEF9, ___value_0)); }
	inline intptr_t get_value_0() const { return ___value_0; }
	inline intptr_t* get_address_of_value_0() { return &___value_0; }
	inline void set_value_0(intptr_t value)
	{
		___value_0 = value;
	}
};


// UnityEngine.TooltipAttribute
struct TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B  : public PropertyAttribute_t4A352471DF625C56C811E27AC86B7E1CE6444052
{
public:
	// System.String UnityEngine.TooltipAttribute::tooltip
	String_t* ___tooltip_0;

public:
	inline static int32_t get_offset_of_tooltip_0() { return static_cast<int32_t>(offsetof(TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B, ___tooltip_0)); }
	inline String_t* get_tooltip_0() const { return ___tooltip_0; }
	inline String_t** get_address_of_tooltip_0() { return &___tooltip_0; }
	inline void set_tooltip_0(String_t* value)
	{
		___tooltip_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___tooltip_0), (void*)value);
	}
};


// System.Diagnostics.DebuggableAttribute/DebuggingModes
struct DebuggingModes_t279D5B9C012ABA935887CB73C5A63A1F46AF08A8 
{
public:
	// System.Int32 System.Diagnostics.DebuggableAttribute/DebuggingModes::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(DebuggingModes_t279D5B9C012ABA935887CB73C5A63A1F46AF08A8, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// System.Diagnostics.DebuggableAttribute
struct DebuggableAttribute_tA8054EBD0FC7511695D494B690B5771658E3191B  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.Diagnostics.DebuggableAttribute/DebuggingModes System.Diagnostics.DebuggableAttribute::m_debuggingModes
	int32_t ___m_debuggingModes_0;

public:
	inline static int32_t get_offset_of_m_debuggingModes_0() { return static_cast<int32_t>(offsetof(DebuggableAttribute_tA8054EBD0FC7511695D494B690B5771658E3191B, ___m_debuggingModes_0)); }
	inline int32_t get_m_debuggingModes_0() const { return ___m_debuggingModes_0; }
	inline int32_t* get_address_of_m_debuggingModes_0() { return &___m_debuggingModes_0; }
	inline void set_m_debuggingModes_0(int32_t value)
	{
		___m_debuggingModes_0 = value;
	}
};


// System.Type
struct Type_t  : public MemberInfo_t
{
public:
	// System.RuntimeTypeHandle System.Type::_impl
	RuntimeTypeHandle_tC33965ADA3E041E0C94AF05E5CB527B56482CEF9  ____impl_9;

public:
	inline static int32_t get_offset_of__impl_9() { return static_cast<int32_t>(offsetof(Type_t, ____impl_9)); }
	inline RuntimeTypeHandle_tC33965ADA3E041E0C94AF05E5CB527B56482CEF9  get__impl_9() const { return ____impl_9; }
	inline RuntimeTypeHandle_tC33965ADA3E041E0C94AF05E5CB527B56482CEF9 * get_address_of__impl_9() { return &____impl_9; }
	inline void set__impl_9(RuntimeTypeHandle_tC33965ADA3E041E0C94AF05E5CB527B56482CEF9  value)
	{
		____impl_9 = value;
	}
};

struct Type_t_StaticFields
{
public:
	// System.Reflection.MemberFilter System.Type::FilterAttribute
	MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * ___FilterAttribute_0;
	// System.Reflection.MemberFilter System.Type::FilterName
	MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * ___FilterName_1;
	// System.Reflection.MemberFilter System.Type::FilterNameIgnoreCase
	MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * ___FilterNameIgnoreCase_2;
	// System.Object System.Type::Missing
	RuntimeObject * ___Missing_3;
	// System.Char System.Type::Delimiter
	Il2CppChar ___Delimiter_4;
	// System.Type[] System.Type::EmptyTypes
	TypeU5BU5D_t85B10489E46F06CEC7C4B1CCBD0E01FAB6649755* ___EmptyTypes_5;
	// System.Reflection.Binder System.Type::defaultBinder
	Binder_t2BEE27FD84737D1E79BC47FD67F6D3DD2F2DDA30 * ___defaultBinder_6;

public:
	inline static int32_t get_offset_of_FilterAttribute_0() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___FilterAttribute_0)); }
	inline MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * get_FilterAttribute_0() const { return ___FilterAttribute_0; }
	inline MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 ** get_address_of_FilterAttribute_0() { return &___FilterAttribute_0; }
	inline void set_FilterAttribute_0(MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * value)
	{
		___FilterAttribute_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___FilterAttribute_0), (void*)value);
	}

	inline static int32_t get_offset_of_FilterName_1() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___FilterName_1)); }
	inline MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * get_FilterName_1() const { return ___FilterName_1; }
	inline MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 ** get_address_of_FilterName_1() { return &___FilterName_1; }
	inline void set_FilterName_1(MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * value)
	{
		___FilterName_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___FilterName_1), (void*)value);
	}

	inline static int32_t get_offset_of_FilterNameIgnoreCase_2() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___FilterNameIgnoreCase_2)); }
	inline MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * get_FilterNameIgnoreCase_2() const { return ___FilterNameIgnoreCase_2; }
	inline MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 ** get_address_of_FilterNameIgnoreCase_2() { return &___FilterNameIgnoreCase_2; }
	inline void set_FilterNameIgnoreCase_2(MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * value)
	{
		___FilterNameIgnoreCase_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___FilterNameIgnoreCase_2), (void*)value);
	}

	inline static int32_t get_offset_of_Missing_3() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___Missing_3)); }
	inline RuntimeObject * get_Missing_3() const { return ___Missing_3; }
	inline RuntimeObject ** get_address_of_Missing_3() { return &___Missing_3; }
	inline void set_Missing_3(RuntimeObject * value)
	{
		___Missing_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___Missing_3), (void*)value);
	}

	inline static int32_t get_offset_of_Delimiter_4() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___Delimiter_4)); }
	inline Il2CppChar get_Delimiter_4() const { return ___Delimiter_4; }
	inline Il2CppChar* get_address_of_Delimiter_4() { return &___Delimiter_4; }
	inline void set_Delimiter_4(Il2CppChar value)
	{
		___Delimiter_4 = value;
	}

	inline static int32_t get_offset_of_EmptyTypes_5() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___EmptyTypes_5)); }
	inline TypeU5BU5D_t85B10489E46F06CEC7C4B1CCBD0E01FAB6649755* get_EmptyTypes_5() const { return ___EmptyTypes_5; }
	inline TypeU5BU5D_t85B10489E46F06CEC7C4B1CCBD0E01FAB6649755** get_address_of_EmptyTypes_5() { return &___EmptyTypes_5; }
	inline void set_EmptyTypes_5(TypeU5BU5D_t85B10489E46F06CEC7C4B1CCBD0E01FAB6649755* value)
	{
		___EmptyTypes_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___EmptyTypes_5), (void*)value);
	}

	inline static int32_t get_offset_of_defaultBinder_6() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___defaultBinder_6)); }
	inline Binder_t2BEE27FD84737D1E79BC47FD67F6D3DD2F2DDA30 * get_defaultBinder_6() const { return ___defaultBinder_6; }
	inline Binder_t2BEE27FD84737D1E79BC47FD67F6D3DD2F2DDA30 ** get_address_of_defaultBinder_6() { return &___defaultBinder_6; }
	inline void set_defaultBinder_6(Binder_t2BEE27FD84737D1E79BC47FD67F6D3DD2F2DDA30 * value)
	{
		___defaultBinder_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___defaultBinder_6), (void*)value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif



// System.Void System.Diagnostics.DebuggableAttribute::.ctor(System.Diagnostics.DebuggableAttribute/DebuggingModes)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void DebuggableAttribute__ctor_m7FF445C8435494A4847123A668D889E692E55550 (DebuggableAttribute_tA8054EBD0FC7511695D494B690B5771658E3191B * __this, int32_t ___modes0, const RuntimeMethod* method);
// System.Void System.Runtime.CompilerServices.CompilationRelaxationsAttribute::.ctor(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void CompilationRelaxationsAttribute__ctor_mAC3079EBC4EEAB474EED8208EF95DB39C922333B (CompilationRelaxationsAttribute_t661FDDC06629BDA607A42BD660944F039FE03AFF * __this, int32_t ___relaxations0, const RuntimeMethod* method);
// System.Void System.Runtime.CompilerServices.ExtensionAttribute::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * __this, const RuntimeMethod* method);
// System.Void System.Runtime.CompilerServices.RuntimeCompatibilityAttribute::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void RuntimeCompatibilityAttribute__ctor_m551DDF1438CE97A984571949723F30F44CF7317C (RuntimeCompatibilityAttribute_tFF99AB2963098F9CBCD47A20D9FD3D51C17C1C80 * __this, const RuntimeMethod* method);
// System.Void System.Runtime.CompilerServices.RuntimeCompatibilityAttribute::set_WrapNonExceptionThrows(System.Boolean)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void RuntimeCompatibilityAttribute_set_WrapNonExceptionThrows_m8562196F90F3EBCEC23B5708EE0332842883C490_inline (RuntimeCompatibilityAttribute_tFF99AB2963098F9CBCD47A20D9FD3D51C17C1C80 * __this, bool ___value0, const RuntimeMethod* method);
// System.Void System.Runtime.CompilerServices.CompilerGeneratedAttribute::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35 (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * __this, const RuntimeMethod* method);
// System.Void UnityEngine.ExecuteInEditMode::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ExecuteInEditMode__ctor_m52849B67DB46F6CF090D45E523FAE97A10825C0A (ExecuteInEditMode_tAA3B5DE8B7E207BC6CAAFDB1F2502350C0546173 * __this, const RuntimeMethod* method);
// System.Void UnityEngine.DisallowMultipleComponent::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void DisallowMultipleComponent__ctor_mDCA4B0F84AB4B3E17D216DB29318032547AB7F0D (DisallowMultipleComponent_tDB3D3DBC9AC523A0BD11DA0B7D88F960FDB89E3E * __this, const RuntimeMethod* method);
// System.Void UnityEngine.HelpURLAttribute::.ctor(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void HelpURLAttribute__ctor_mCC837CE7900738F3152D585D454A34A15793C215 (HelpURLAttribute_t0924A6D83FABA7B77780F7F9BBCBCB9E0EA15023 * __this, String_t* ___url0, const RuntimeMethod* method);
// System.Void UnityEngine.HeaderAttribute::.ctor(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void HeaderAttribute__ctor_m601319E0BCE8C44A9E79B2C0ABAAD0FEF46A9F1E (HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB * __this, String_t* ___header0, const RuntimeMethod* method);
// System.Void UnityEngine.TooltipAttribute::.ctor(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042 (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * __this, String_t* ___tooltip0, const RuntimeMethod* method);
// System.Void UnityEngine.RangeAttribute::.ctor(System.Single,System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void RangeAttribute__ctor_mC74D39A9F20DD2A0D4174F05785ABE4F0DAEF000 (RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5 * __this, float ___min0, float ___max1, const RuntimeMethod* method);
// System.Void System.Runtime.CompilerServices.IteratorStateMachineAttribute::.ctor(System.Type)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481 (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * __this, Type_t * ___stateMachineType0, const RuntimeMethod* method);
// System.Void System.Diagnostics.DebuggerHiddenAttribute::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3 (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * __this, const RuntimeMethod* method);
// System.Void UnityEngine.RequireComponent::.ctor(System.Type)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void RequireComponent__ctor_m5EC89D3D22D7D880E1B88A5C9FADF1FBDC713EE4 (RequireComponent_tEDA546F9722B8874DA9658BDAB821BA49647FC91 * __this, Type_t * ___requiredComponent0, const RuntimeMethod* method);
// System.Void UnityEngine.MultilineAttribute::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void MultilineAttribute__ctor_m353DC377C95D4C341F5DDD0F9894878F64E5703E (MultilineAttribute_tA4DCA9D8519E0C1B05636F2EFF74F81335A7F291 * __this, const RuntimeMethod* method);
// System.Void UnityEngine.HideInInspector::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void HideInInspector__ctor_mE2B7FB1D206A74BA583C7812CDB4EBDD83EB66F9 (HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA * __this, const RuntimeMethod* method);
// System.Void System.ParamArrayAttribute::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ParamArrayAttribute__ctor_mCC72AFF718185BA7B87FD8D9471F1274400C5719 (ParamArrayAttribute_t9DCEB4CDDB8EDDB1124171D4C51FA6069EEA5C5F * __this, const RuntimeMethod* method);
static void AssemblyU2DCSharpU2Dfirstpass_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		DebuggableAttribute_tA8054EBD0FC7511695D494B690B5771658E3191B * tmp = (DebuggableAttribute_tA8054EBD0FC7511695D494B690B5771658E3191B *)cache->attributes[0];
		DebuggableAttribute__ctor_m7FF445C8435494A4847123A668D889E692E55550(tmp, 2LL, NULL);
	}
	{
		CompilationRelaxationsAttribute_t661FDDC06629BDA607A42BD660944F039FE03AFF * tmp = (CompilationRelaxationsAttribute_t661FDDC06629BDA607A42BD660944F039FE03AFF *)cache->attributes[1];
		CompilationRelaxationsAttribute__ctor_mAC3079EBC4EEAB474EED8208EF95DB39C922333B(tmp, 8LL, NULL);
	}
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[2];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
	{
		RuntimeCompatibilityAttribute_tFF99AB2963098F9CBCD47A20D9FD3D51C17C1C80 * tmp = (RuntimeCompatibilityAttribute_tFF99AB2963098F9CBCD47A20D9FD3D51C17C1C80 *)cache->attributes[3];
		RuntimeCompatibilityAttribute__ctor_m551DDF1438CE97A984571949723F30F44CF7317C(tmp, NULL);
		RuntimeCompatibilityAttribute_set_WrapNonExceptionThrows_m8562196F90F3EBCEC23B5708EE0332842883C490_inline(tmp, true, NULL);
	}
}
static void ExtensionMethods_t3A63E01D154FF156B5910983BBD9656044FCBAEF_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void ExtensionMethods_t3A63E01D154FF156B5910983BBD9656044FCBAEF_CustomAttributesCacheGenerator_ExtensionMethods_CTToTitleCase_m0FAF40693A916D1BA985195E30AA7B9753C11DE2(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void ExtensionMethods_t3A63E01D154FF156B5910983BBD9656044FCBAEF_CustomAttributesCacheGenerator_ExtensionMethods_CTReverse_m375B1B5F94367BC73BC054481C4528E1829758A0(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void ExtensionMethods_t3A63E01D154FF156B5910983BBD9656044FCBAEF_CustomAttributesCacheGenerator_ExtensionMethods_CTReplace_mE45837F5BF2ECDD64251F7DC85303D36308AFBB9(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void ExtensionMethods_t3A63E01D154FF156B5910983BBD9656044FCBAEF_CustomAttributesCacheGenerator_ExtensionMethods_CTEquals_m70FD226B78B4DD7ED0672F326B16568EA3D9B446(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void ExtensionMethods_t3A63E01D154FF156B5910983BBD9656044FCBAEF_CustomAttributesCacheGenerator_ExtensionMethods_CTContains_m52B3F3D1019BBD1746371264A2671656CDADB3C2(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void ExtensionMethods_t3A63E01D154FF156B5910983BBD9656044FCBAEF_CustomAttributesCacheGenerator_ExtensionMethods_CTContainsAny_m50A29F87C6A50663D9B599FEA1D1359EC07969A3(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void ExtensionMethods_t3A63E01D154FF156B5910983BBD9656044FCBAEF_CustomAttributesCacheGenerator_ExtensionMethods_CTContainsAll_mAB9F9024F8E76F4449FE666602718634956C2464(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void ExtensionMethods_t3A63E01D154FF156B5910983BBD9656044FCBAEF_CustomAttributesCacheGenerator_ExtensionMethods_CTShuffle_m69617262C775678CA0CFFF468C20E275ECABD211(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void ExtensionMethods_t3A63E01D154FF156B5910983BBD9656044FCBAEF_CustomAttributesCacheGenerator_ExtensionMethods_CTDump_mB5A7A1F35FC14838220750C2889DE6D41C68F086(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void ExtensionMethods_t3A63E01D154FF156B5910983BBD9656044FCBAEF_CustomAttributesCacheGenerator_ExtensionMethods_CTDump_m9372AF9431740E725E0A0EE720EEB4B51EF8D626(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void ExtensionMethods_t3A63E01D154FF156B5910983BBD9656044FCBAEF_CustomAttributesCacheGenerator_ExtensionMethods_CTDump_mDD2831261D66781D294ADBFFBD9A397D4A5B4681(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void ExtensionMethods_t3A63E01D154FF156B5910983BBD9656044FCBAEF_CustomAttributesCacheGenerator_ExtensionMethods_CTDump_m2A792DFFEECEFF18E99A289916B9165C127E7883(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void ExtensionMethods_t3A63E01D154FF156B5910983BBD9656044FCBAEF_CustomAttributesCacheGenerator_ExtensionMethods_CTDump_mEDCBC052999C7F88ECF5897E865B6467FF1F452E(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void ExtensionMethods_t3A63E01D154FF156B5910983BBD9656044FCBAEF_CustomAttributesCacheGenerator_ExtensionMethods_CTToString_m181115D9882BBB5E201C31443FCFA49DBADE6D30(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void ExtensionMethods_t3A63E01D154FF156B5910983BBD9656044FCBAEF_CustomAttributesCacheGenerator_ExtensionMethods_CTShuffle_mB67EA3011C43B440B75C4B90B8719323BEE020E4(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void ExtensionMethods_t3A63E01D154FF156B5910983BBD9656044FCBAEF_CustomAttributesCacheGenerator_ExtensionMethods_CTDump_m63A8F721F15E1CB0491451A09378CBB63710F902(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void ExtensionMethods_t3A63E01D154FF156B5910983BBD9656044FCBAEF_CustomAttributesCacheGenerator_ExtensionMethods_CTDump_m59F8DE0A578B3B891B4D8C1ED38688E86940F976(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void ExtensionMethods_t3A63E01D154FF156B5910983BBD9656044FCBAEF_CustomAttributesCacheGenerator_ExtensionMethods_CTDump_m6E14577C017276ACD5DA587C029A664694E859C9(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void ExtensionMethods_t3A63E01D154FF156B5910983BBD9656044FCBAEF_CustomAttributesCacheGenerator_ExtensionMethods_CTDump_mD3A9D8E4C2FAF2BE628602C89238233DBE170FDE(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void ExtensionMethods_t3A63E01D154FF156B5910983BBD9656044FCBAEF_CustomAttributesCacheGenerator_ExtensionMethods_CTDump_mB00AF342A1C001008DDEE5422E6CE9386ADF044C(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void ExtensionMethods_t3A63E01D154FF156B5910983BBD9656044FCBAEF_CustomAttributesCacheGenerator_ExtensionMethods_CTToString_m21B12643EEC617E394A1A8C5DB57D3DA63EE0A36(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void ExtensionMethods_t3A63E01D154FF156B5910983BBD9656044FCBAEF_CustomAttributesCacheGenerator_ExtensionMethods_CTDump_mED08E2C0BAC319C99DF07AA4752ACCB2645C2E72(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void ExtensionMethods_t3A63E01D154FF156B5910983BBD9656044FCBAEF_CustomAttributesCacheGenerator_ExtensionMethods_CTAddRange_mA8E3A743EA575918A5B29284CB5CEDD965D88261(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void ExtensionMethods_t3A63E01D154FF156B5910983BBD9656044FCBAEF_CustomAttributesCacheGenerator_ExtensionMethods_CTIsVisibleFrom_m5E3DED552FD8B132DD4E2B700E4BC17BCE83E587(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void U3CU3Ec__DisplayClass5_0_tE5690FA9ACA4586CA14252530514637C1651F6FC_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CU3Ec__DisplayClass6_0_tE339584696C7D04D095E648B21D42B2849672875_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void LiveSpeaker_t7B31D2171E070D53CE0B04874A700A898F2D72E7_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		ExecuteInEditMode_tAA3B5DE8B7E207BC6CAAFDB1F2502350C0546173 * tmp = (ExecuteInEditMode_tAA3B5DE8B7E207BC6CAAFDB1F2502350C0546173 *)cache->attributes[0];
		ExecuteInEditMode__ctor_m52849B67DB46F6CF090D45E523FAE97A10825C0A(tmp, NULL);
	}
	{
		DisallowMultipleComponent_tDB3D3DBC9AC523A0BD11DA0B7D88F960FDB89E3E * tmp = (DisallowMultipleComponent_tDB3D3DBC9AC523A0BD11DA0B7D88F960FDB89E3E *)cache->attributes[1];
		DisallowMultipleComponent__ctor_mDCA4B0F84AB4B3E17D216DB29318032547AB7F0D(tmp, NULL);
	}
	{
		HelpURLAttribute_t0924A6D83FABA7B77780F7F9BBCBCB9E0EA15023 * tmp = (HelpURLAttribute_t0924A6D83FABA7B77780F7F9BBCBCB9E0EA15023 *)cache->attributes[2];
		HelpURLAttribute__ctor_mCC837CE7900738F3152D585D454A34A15793C215(tmp, il2cpp_codegen_string_new_wrapper("\x68\x74\x74\x70\x73\x3A\x2F\x2F\x77\x77\x77\x2E\x63\x72\x6F\x73\x73\x74\x61\x6C\x65\x73\x2E\x63\x6F\x6D\x2F\x6D\x65\x64\x69\x61\x2F\x64\x61\x74\x61\x2F\x61\x73\x73\x65\x74\x73\x2F\x72\x74\x76\x6F\x69\x63\x65\x2F\x61\x70\x69\x2F\x63\x6C\x61\x73\x73\x5F\x63\x72\x6F\x73\x73\x74\x61\x6C\x65\x73\x5F\x31\x5F\x31\x5F\x72\x5F\x74\x5F\x76\x6F\x69\x63\x65\x5F\x31\x5F\x31\x5F\x6C\x69\x76\x65\x5F\x73\x70\x65\x61\x6B\x65\x72\x2E\x68\x74\x6D\x6C"), NULL);
	}
}
static void Speaker_t639C193C2402AD73ED45F6530A8FD0EB62D3ACA1_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		ExecuteInEditMode_tAA3B5DE8B7E207BC6CAAFDB1F2502350C0546173 * tmp = (ExecuteInEditMode_tAA3B5DE8B7E207BC6CAAFDB1F2502350C0546173 *)cache->attributes[0];
		ExecuteInEditMode__ctor_m52849B67DB46F6CF090D45E523FAE97A10825C0A(tmp, NULL);
	}
	{
		DisallowMultipleComponent_tDB3D3DBC9AC523A0BD11DA0B7D88F960FDB89E3E * tmp = (DisallowMultipleComponent_tDB3D3DBC9AC523A0BD11DA0B7D88F960FDB89E3E *)cache->attributes[1];
		DisallowMultipleComponent__ctor_mDCA4B0F84AB4B3E17D216DB29318032547AB7F0D(tmp, NULL);
	}
	{
		HelpURLAttribute_t0924A6D83FABA7B77780F7F9BBCBCB9E0EA15023 * tmp = (HelpURLAttribute_t0924A6D83FABA7B77780F7F9BBCBCB9E0EA15023 *)cache->attributes[2];
		HelpURLAttribute__ctor_mCC837CE7900738F3152D585D454A34A15793C215(tmp, il2cpp_codegen_string_new_wrapper("\x68\x74\x74\x70\x73\x3A\x2F\x2F\x77\x77\x77\x2E\x63\x72\x6F\x73\x73\x74\x61\x6C\x65\x73\x2E\x63\x6F\x6D\x2F\x6D\x65\x64\x69\x61\x2F\x64\x61\x74\x61\x2F\x61\x73\x73\x65\x74\x73\x2F\x72\x74\x76\x6F\x69\x63\x65\x2F\x61\x70\x69\x2F\x63\x6C\x61\x73\x73\x5F\x63\x72\x6F\x73\x73\x74\x61\x6C\x65\x73\x5F\x31\x5F\x31\x5F\x72\x5F\x74\x5F\x76\x6F\x69\x63\x65\x5F\x31\x5F\x31\x5F\x73\x70\x65\x61\x6B\x65\x72\x2E\x68\x74\x6D\x6C"), NULL);
	}
}
static void Speaker_t639C193C2402AD73ED45F6530A8FD0EB62D3ACA1_CustomAttributesCacheGenerator_CustomProvider(CustomAttributesCache* cache)
{
	{
		HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB * tmp = (HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB *)cache->attributes[0];
		HeaderAttribute__ctor_m601319E0BCE8C44A9E79B2C0ABAAD0FEF46A9F1E(tmp, il2cpp_codegen_string_new_wrapper("\x43\x75\x73\x74\x6F\x6D\x20\x50\x72\x6F\x76\x69\x64\x65\x72"), NULL);
	}
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[1];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x43\x75\x73\x74\x6F\x6D\x20\x70\x72\x6F\x76\x69\x64\x65\x72\x20\x66\x6F\x72\x20\x52\x54\x2D\x56\x6F\x69\x63\x65\x2E"), NULL);
	}
}
static void Speaker_t639C193C2402AD73ED45F6530A8FD0EB62D3ACA1_CustomAttributesCacheGenerator_CustomMode(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x45\x6E\x61\x62\x6C\x65\x20\x6F\x72\x20\x64\x69\x73\x61\x62\x6C\x65\x20\x74\x68\x65\x20\x63\x75\x73\x74\x6F\x6D\x20\x70\x72\x6F\x76\x69\x64\x65\x72\x20\x28\x64\x65\x66\x61\x75\x6C\x74\x3A\x20\x66\x61\x6C\x73\x65\x29\x2E"), NULL);
	}
}
static void Speaker_t639C193C2402AD73ED45F6530A8FD0EB62D3ACA1_CustomAttributesCacheGenerator_MaryTTSMode(CustomAttributesCache* cache)
{
	{
		HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB * tmp = (HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB *)cache->attributes[0];
		HeaderAttribute__ctor_m601319E0BCE8C44A9E79B2C0ABAAD0FEF46A9F1E(tmp, il2cpp_codegen_string_new_wrapper("\x4D\x61\x72\x79\x54\x54\x53"), NULL);
	}
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[1];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x45\x6E\x61\x62\x6C\x65\x20\x6F\x72\x20\x64\x69\x73\x61\x62\x6C\x65\x20\x4D\x61\x72\x79\x54\x54\x53\x20\x28\x64\x65\x66\x61\x75\x6C\x74\x3A\x20\x66\x61\x6C\x73\x65\x29\x2E"), NULL);
	}
}
static void Speaker_t639C193C2402AD73ED45F6530A8FD0EB62D3ACA1_CustomAttributesCacheGenerator_MaryTTSUrl(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x53\x65\x72\x76\x65\x72\x20\x55\x52\x4C\x20\x66\x6F\x72\x20\x4D\x61\x72\x79\x54\x54\x53\x2E"), NULL);
	}
}
static void Speaker_t639C193C2402AD73ED45F6530A8FD0EB62D3ACA1_CustomAttributesCacheGenerator_MaryTTSPort(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x53\x65\x72\x76\x65\x72\x20\x70\x6F\x72\x74\x20\x66\x6F\x72\x20\x4D\x61\x72\x79\x54\x54\x53\x20\x28\x64\x65\x66\x61\x75\x6C\x74\x3A\x20\x35\x39\x31\x32\x35\x29\x2E"), NULL);
	}
	{
		RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5 * tmp = (RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5 *)cache->attributes[1];
		RangeAttribute__ctor_mC74D39A9F20DD2A0D4174F05785ABE4F0DAEF000(tmp, 0.0f, 65535.0f, NULL);
	}
}
static void Speaker_t639C193C2402AD73ED45F6530A8FD0EB62D3ACA1_CustomAttributesCacheGenerator_MaryTTSUser(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x55\x73\x65\x72\x20\x6E\x61\x6D\x65\x20\x66\x6F\x72\x20\x4D\x61\x72\x79\x54\x54\x53\x20\x28\x64\x65\x66\x61\x75\x6C\x74\x3A\x20\x65\x6D\x70\x74\x79\x29\x2E"), NULL);
	}
}
static void Speaker_t639C193C2402AD73ED45F6530A8FD0EB62D3ACA1_CustomAttributesCacheGenerator_MaryTTSPassword(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x55\x73\x65\x72\x20\x70\x61\x73\x73\x77\x6F\x72\x64\x20\x66\x6F\x72\x20\x4D\x61\x72\x79\x54\x54\x53\x20\x28\x64\x65\x66\x61\x75\x6C\x74\x3A\x20\x65\x6D\x70\x74\x79\x29\x2E"), NULL);
	}
}
static void Speaker_t639C193C2402AD73ED45F6530A8FD0EB62D3ACA1_CustomAttributesCacheGenerator_MaryTTSType(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x49\x6E\x70\x75\x74\x20\x74\x79\x70\x65\x20\x66\x6F\x72\x20\x4D\x61\x72\x79\x54\x54\x53\x20\x28\x64\x65\x66\x61\x75\x6C\x74\x3A\x20\x4D\x61\x72\x79\x54\x54\x53\x54\x79\x70\x65\x2E\x52\x41\x57\x4D\x41\x52\x59\x58\x4D\x4C\x29\x2E"), NULL);
	}
}
static void Speaker_t639C193C2402AD73ED45F6530A8FD0EB62D3ACA1_CustomAttributesCacheGenerator_ESpeakMode(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x45\x6E\x61\x62\x6C\x65\x20\x6F\x72\x20\x64\x69\x73\x61\x62\x6C\x65\x20\x65\x53\x70\x65\x61\x6B\x20\x66\x6F\x72\x20\x73\x74\x61\x6E\x64\x61\x6C\x6F\x6E\x65\x20\x70\x6C\x61\x74\x66\x6F\x72\x6D\x73\x20\x28\x64\x65\x66\x61\x75\x6C\x74\x3A\x20\x66\x61\x6C\x73\x65\x29\x2E"), NULL);
	}
	{
		HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB * tmp = (HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB *)cache->attributes[1];
		HeaderAttribute__ctor_m601319E0BCE8C44A9E79B2C0ABAAD0FEF46A9F1E(tmp, il2cpp_codegen_string_new_wrapper("\x65\x53\x70\x65\x61\x6B\x20\x53\x65\x74\x74\x69\x6E\x67\x73"), NULL);
	}
}
static void Speaker_t639C193C2402AD73ED45F6530A8FD0EB62D3ACA1_CustomAttributesCacheGenerator_ESpeakModifier(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x41\x63\x74\x69\x76\x65\x20\x6D\x6F\x64\x69\x66\x69\x65\x72\x20\x66\x6F\x72\x20\x61\x6C\x6C\x20\x65\x53\x70\x65\x61\x6B\x20\x76\x6F\x69\x63\x65\x73\x20\x28\x64\x65\x66\x61\x75\x6C\x74\x3A\x20\x6E\x6F\x6E\x65\x2C\x20\x6D\x31\x2D\x6D\x36\x20\x3D\x20\x6D\x61\x6C\x65\x2C\x20\x66\x31\x2D\x66\x34\x20\x3D\x20\x66\x65\x6D\x61\x6C\x65\x29\x2E"), NULL);
	}
}
static void Speaker_t639C193C2402AD73ED45F6530A8FD0EB62D3ACA1_CustomAttributesCacheGenerator_AutoClearTags(CustomAttributesCache* cache)
{
	{
		HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB * tmp = (HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB *)cache->attributes[0];
		HeaderAttribute__ctor_m601319E0BCE8C44A9E79B2C0ABAAD0FEF46A9F1E(tmp, il2cpp_codegen_string_new_wrapper("\x41\x64\x76\x61\x6E\x63\x65\x64\x20\x53\x65\x74\x74\x69\x6E\x67\x73"), NULL);
	}
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[1];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x41\x75\x74\x6F\x6D\x61\x74\x69\x63\x61\x6C\x6C\x79\x20\x63\x6C\x65\x61\x72\x20\x74\x61\x67\x73\x20\x66\x72\x6F\x6D\x20\x73\x70\x65\x65\x63\x68\x65\x73\x20\x64\x65\x70\x65\x6E\x64\x69\x6E\x67\x20\x6F\x6E\x20\x74\x68\x65\x20\x63\x61\x70\x61\x62\x69\x6C\x69\x74\x69\x65\x73\x20\x6F\x66\x20\x74\x68\x65\x20\x63\x75\x72\x72\x65\x6E\x74\x20\x54\x54\x53\x2D\x73\x79\x73\x74\x65\x6D\x20\x28\x64\x65\x66\x61\x75\x6C\x74\x3A\x20\x66\x61\x6C\x73\x65\x29\x2E"), NULL);
	}
}
static void Speaker_t639C193C2402AD73ED45F6530A8FD0EB62D3ACA1_CustomAttributesCacheGenerator_WSANative(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x45\x6E\x61\x62\x6C\x65\x20\x6F\x72\x20\x64\x69\x73\x61\x62\x6C\x65\x20\x6E\x61\x74\x69\x76\x65\x20\x73\x70\x65\x61\x6B\x20\x75\x6E\x64\x65\x72\x20\x57\x53\x41\x2E\x20\x49\x66\x20\x65\x6E\x61\x62\x6C\x65\x64\x2C\x20\x74\x68\x65\x20\x62\x75\x69\x6C\x64\x20\x74\x79\x70\x65\x20\x6D\x75\x73\x74\x20\x62\x65\x20\x27\x58\x41\x4D\x4C\x27\x21\x20\x28\x64\x65\x66\x61\x75\x6C\x74\x3A\x20\x66\x61\x6C\x73\x65\x29"), NULL);
	}
}
static void Speaker_t639C193C2402AD73ED45F6530A8FD0EB62D3ACA1_CustomAttributesCacheGenerator_SilenceOnDisable(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x53\x69\x6C\x65\x6E\x63\x65\x20\x61\x6E\x79\x20\x73\x70\x65\x65\x63\x68\x65\x73\x20\x69\x66\x20\x74\x68\x69\x73\x20\x63\x6F\x6D\x70\x6F\x6E\x65\x6E\x74\x20\x67\x65\x74\x73\x20\x64\x69\x73\x61\x62\x6C\x65\x64\x20\x28\x64\x65\x66\x61\x75\x6C\x74\x3A\x20\x66\x61\x6C\x73\x65\x29\x2E"), NULL);
	}
	{
		HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB * tmp = (HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB *)cache->attributes[1];
		HeaderAttribute__ctor_m601319E0BCE8C44A9E79B2C0ABAAD0FEF46A9F1E(tmp, il2cpp_codegen_string_new_wrapper("\x42\x65\x68\x61\x76\x69\x6F\x75\x72\x20\x53\x65\x74\x74\x69\x6E\x67\x73"), NULL);
	}
}
static void Speaker_t639C193C2402AD73ED45F6530A8FD0EB62D3ACA1_CustomAttributesCacheGenerator_SilenceOnFocustLost(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x53\x69\x6C\x65\x6E\x63\x65\x20\x61\x6E\x79\x20\x73\x70\x65\x65\x63\x68\x65\x73\x20\x69\x66\x20\x74\x68\x65\x20\x61\x70\x70\x6C\x69\x63\x61\x74\x69\x6F\x6E\x20\x6C\x6F\x73\x65\x73\x20\x74\x68\x65\x20\x66\x6F\x63\x75\x73\x2E\x20\x4F\x74\x68\x65\x72\x77\x69\x73\x65\x20\x74\x68\x65\x20\x73\x70\x65\x65\x63\x68\x65\x73\x20\x61\x72\x65\x20\x70\x61\x75\x73\x65\x64\x20\x61\x6E\x64\x20\x75\x6E\x70\x61\x75\x73\x65\x64\x20\x28\x64\x65\x66\x61\x75\x6C\x74\x3A\x20\x66\x61\x6C\x73\x65\x29\x2E"), NULL);
	}
}
static void Speaker_t639C193C2402AD73ED45F6530A8FD0EB62D3ACA1_CustomAttributesCacheGenerator_DontDestroy(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x44\x6F\x6E\x27\x74\x20\x64\x65\x73\x74\x72\x6F\x79\x20\x67\x61\x6D\x65\x6F\x62\x6A\x65\x63\x74\x20\x64\x75\x72\x69\x6E\x67\x20\x73\x63\x65\x6E\x65\x20\x73\x77\x69\x74\x63\x68\x65\x73\x20\x28\x64\x65\x66\x61\x75\x6C\x74\x3A\x20\x74\x72\x75\x65\x29\x2E"), NULL);
	}
}
static void Speaker_t639C193C2402AD73ED45F6530A8FD0EB62D3ACA1_CustomAttributesCacheGenerator_U3CareVoicesReadyU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void Speaker_t639C193C2402AD73ED45F6530A8FD0EB62D3ACA1_CustomAttributesCacheGenerator_U3CenforcedStandaloneTTSU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void Speaker_t639C193C2402AD73ED45F6530A8FD0EB62D3ACA1_CustomAttributesCacheGenerator_Speaker_get_areVoicesReady_m55042C89D6ABB754FF15AC1E880F6FAC8CE6C318(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void Speaker_t639C193C2402AD73ED45F6530A8FD0EB62D3ACA1_CustomAttributesCacheGenerator_Speaker_set_areVoicesReady_m2F79E5855BD2C1E6BC59F3A29C7972FAB5C49DE7(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void Speaker_t639C193C2402AD73ED45F6530A8FD0EB62D3ACA1_CustomAttributesCacheGenerator_Speaker_get_enforcedStandaloneTTS_m2B69DCFD8736769BE2716D304570F88D3F915D6B(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void Speaker_t639C193C2402AD73ED45F6530A8FD0EB62D3ACA1_CustomAttributesCacheGenerator_Speaker_set_enforcedStandaloneTTS_m8E0627900FA976E4F96CE064B22F6A19A1952DC6(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CU3Ec__DisplayClass171_0_t1D4DEA10DFF9B2953F9E5C2D69FF13A70C813E7A_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CU3Ec_t286538CCBFD7D920B5EF3EBD00F0B6DA2C054F9A_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CU3Ec__DisplayClass190_0_t13E11E15D078B45788B97CABD4F8C24E1030AA18_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void VoiceProviderExample_tF1FB762ADFC1220BD86B88A758A6FA10381825EB_CustomAttributesCacheGenerator_VoiceProviderExample_Generate_mCEF1A347318B74AA723F6ABFA9C2EB48DC2DF9C4(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CGenerateU3Ed__27_t733346A9DBD137DA2DD7EA3252EFFE76500D3C5B_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CGenerateU3Ed__27_t733346A9DBD137DA2DD7EA3252EFFE76500D3C5B_0_0_0_var), NULL);
	}
}
static void VoiceProviderExample_tF1FB762ADFC1220BD86B88A758A6FA10381825EB_CustomAttributesCacheGenerator_VoiceProviderExample_Speak_mBDB765FB3CC330D5B5FC491DC795FD5297EDF0E7(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CSpeakU3Ed__28_t51C9498C945D57FD321E65C4CD6972BB1933A896_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CSpeakU3Ed__28_t51C9498C945D57FD321E65C4CD6972BB1933A896_0_0_0_var), NULL);
	}
}
static void VoiceProviderExample_tF1FB762ADFC1220BD86B88A758A6FA10381825EB_CustomAttributesCacheGenerator_VoiceProviderExample_SpeakNative_m9AE9125ACB3493351913B96173D7DBC83E2EAF13(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CSpeakNativeU3Ed__29_tAEFCE1D6AFE6BEB16F03F830C079DAA23C35B481_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CSpeakNativeU3Ed__29_tAEFCE1D6AFE6BEB16F03F830C079DAA23C35B481_0_0_0_var), NULL);
	}
}
static void U3CGenerateU3Ed__27_t733346A9DBD137DA2DD7EA3252EFFE76500D3C5B_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CGenerateU3Ed__27_t733346A9DBD137DA2DD7EA3252EFFE76500D3C5B_CustomAttributesCacheGenerator_U3CGenerateU3Ed__27__ctor_mBE7648CA9BC983832FB40122AE1F309C028FAE80(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CGenerateU3Ed__27_t733346A9DBD137DA2DD7EA3252EFFE76500D3C5B_CustomAttributesCacheGenerator_U3CGenerateU3Ed__27_System_IDisposable_Dispose_m31A13EDD00230227067357AEDF3081F4DFE30A20(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CGenerateU3Ed__27_t733346A9DBD137DA2DD7EA3252EFFE76500D3C5B_CustomAttributesCacheGenerator_U3CGenerateU3Ed__27_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m5D2AC763594283B97AF22C0ECE832F4019B1DB85(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CGenerateU3Ed__27_t733346A9DBD137DA2DD7EA3252EFFE76500D3C5B_CustomAttributesCacheGenerator_U3CGenerateU3Ed__27_System_Collections_IEnumerator_Reset_mBD3F2BBF7EB868747C7D15542F2E9089C61F50C1(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CGenerateU3Ed__27_t733346A9DBD137DA2DD7EA3252EFFE76500D3C5B_CustomAttributesCacheGenerator_U3CGenerateU3Ed__27_System_Collections_IEnumerator_get_Current_m1BEE20547347457E10EF46105AA485378FA8EEE3(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CSpeakU3Ed__28_t51C9498C945D57FD321E65C4CD6972BB1933A896_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CSpeakU3Ed__28_t51C9498C945D57FD321E65C4CD6972BB1933A896_CustomAttributesCacheGenerator_U3CSpeakU3Ed__28__ctor_m00F559E690F57E2B4D1C6180D7BCF76110BBC6E1(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CSpeakU3Ed__28_t51C9498C945D57FD321E65C4CD6972BB1933A896_CustomAttributesCacheGenerator_U3CSpeakU3Ed__28_System_IDisposable_Dispose_mC282884D02DBBFF1E195D8728EA2B8FF2D68351D(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CSpeakU3Ed__28_t51C9498C945D57FD321E65C4CD6972BB1933A896_CustomAttributesCacheGenerator_U3CSpeakU3Ed__28_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m155241265B291EBEE2FD2381BC48B220AA3BA054(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CSpeakU3Ed__28_t51C9498C945D57FD321E65C4CD6972BB1933A896_CustomAttributesCacheGenerator_U3CSpeakU3Ed__28_System_Collections_IEnumerator_Reset_mC4A4CC3360ACD12464DEE845E9CE608105356EF3(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CSpeakU3Ed__28_t51C9498C945D57FD321E65C4CD6972BB1933A896_CustomAttributesCacheGenerator_U3CSpeakU3Ed__28_System_Collections_IEnumerator_get_Current_mA9B5A00AE897B39C9DEA60B9E4DA3135421125EC(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CSpeakNativeU3Ed__29_tAEFCE1D6AFE6BEB16F03F830C079DAA23C35B481_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CSpeakNativeU3Ed__29_tAEFCE1D6AFE6BEB16F03F830C079DAA23C35B481_CustomAttributesCacheGenerator_U3CSpeakNativeU3Ed__29__ctor_mAD9F48CC410BA068A8CBF1E72BAD7EAFFE682B0A(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CSpeakNativeU3Ed__29_tAEFCE1D6AFE6BEB16F03F830C079DAA23C35B481_CustomAttributesCacheGenerator_U3CSpeakNativeU3Ed__29_System_IDisposable_Dispose_m309B55ACD321D7AC45AD632C1B4EACED83D47100(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CSpeakNativeU3Ed__29_tAEFCE1D6AFE6BEB16F03F830C079DAA23C35B481_CustomAttributesCacheGenerator_U3CSpeakNativeU3Ed__29_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m7C5EDA7375CBEE70E45B133C63BED52FBD448E07(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CSpeakNativeU3Ed__29_tAEFCE1D6AFE6BEB16F03F830C079DAA23C35B481_CustomAttributesCacheGenerator_U3CSpeakNativeU3Ed__29_System_Collections_IEnumerator_Reset_m5F62E754EA07230AF596C779B5484274CCBFB409(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CSpeakNativeU3Ed__29_tAEFCE1D6AFE6BEB16F03F830C079DAA23C35B481_CustomAttributesCacheGenerator_U3CSpeakNativeU3Ed__29_System_Collections_IEnumerator_get_Current_m64E0A5D15216AFA6D8650F2B8453143B89539613(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void AudioFileGenerator_t25C4CCBDEAE5E63CB0745BACFAEF787E31EEA298_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		HelpURLAttribute_t0924A6D83FABA7B77780F7F9BBCBCB9E0EA15023 * tmp = (HelpURLAttribute_t0924A6D83FABA7B77780F7F9BBCBCB9E0EA15023 *)cache->attributes[0];
		HelpURLAttribute__ctor_mCC837CE7900738F3152D585D454A34A15793C215(tmp, il2cpp_codegen_string_new_wrapper("\x68\x74\x74\x70\x73\x3A\x2F\x2F\x63\x72\x6F\x73\x73\x74\x61\x6C\x65\x73\x2E\x63\x6F\x6D\x2F\x6D\x65\x64\x69\x61\x2F\x64\x61\x74\x61\x2F\x61\x73\x73\x65\x74\x73\x2F\x72\x74\x76\x6F\x69\x63\x65\x2F\x61\x70\x69\x2F\x63\x6C\x61\x73\x73\x5F\x63\x72\x6F\x73\x73\x74\x61\x6C\x65\x73\x5F\x31\x5F\x31\x5F\x72\x5F\x74\x5F\x76\x6F\x69\x63\x65\x5F\x31\x5F\x31\x5F\x74\x6F\x6F\x6C\x5F\x31\x5F\x31\x5F\x61\x75\x64\x69\x6F\x5F\x66\x69\x6C\x65\x5F\x67\x65\x6E\x65\x72\x61\x74\x6F\x72\x2E\x68\x74\x6D\x6C"), NULL);
	}
	{
		ExecuteInEditMode_tAA3B5DE8B7E207BC6CAAFDB1F2502350C0546173 * tmp = (ExecuteInEditMode_tAA3B5DE8B7E207BC6CAAFDB1F2502350C0546173 *)cache->attributes[1];
		ExecuteInEditMode__ctor_m52849B67DB46F6CF090D45E523FAE97A10825C0A(tmp, NULL);
	}
}
static void AudioFileGenerator_t25C4CCBDEAE5E63CB0745BACFAEF787E31EEA298_CustomAttributesCacheGenerator_TextFiles(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x54\x65\x78\x74\x20\x66\x69\x6C\x65\x73\x20\x74\x6F\x20\x67\x65\x6E\x65\x72\x61\x74\x65\x2E"), NULL);
	}
}
static void AudioFileGenerator_t25C4CCBDEAE5E63CB0745BACFAEF787E31EEA298_CustomAttributesCacheGenerator_FileInsideAssets(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x41\x72\x65\x20\x74\x68\x65\x20\x73\x70\x65\x63\x69\x66\x69\x65\x64\x20\x66\x69\x6C\x65\x20\x70\x61\x74\x68\x73\x20\x69\x6E\x73\x69\x64\x65\x20\x74\x68\x65\x20\x41\x73\x73\x65\x74\x73\x2D\x66\x6F\x6C\x64\x65\x72\x20\x28\x63\x75\x72\x72\x65\x6E\x74\x20\x70\x72\x6F\x6A\x65\x63\x74\x29\x3F\x20\x49\x66\x20\x74\x68\x69\x73\x20\x6F\x70\x74\x69\x6F\x6E\x20\x69\x73\x20\x65\x6E\x61\x62\x6C\x65\x64\x2C\x20\x69\x74\x20\x70\x72\x65\x66\x69\x78\x65\x73\x20\x74\x68\x65\x20\x70\x61\x74\x68\x20\x77\x69\x74\x68\x20\x27\x41\x70\x70\x6C\x69\x63\x61\x74\x69\x6F\x6E\x2E\x64\x61\x74\x61\x50\x61\x74\x68\x27\x20\x28\x64\x65\x66\x61\x75\x6C\x74\x3A\x20\x74\x72\x75\x65\x29\x2E"), NULL);
	}
}
static void AudioFileGenerator_t25C4CCBDEAE5E63CB0745BACFAEF787E31EEA298_CustomAttributesCacheGenerator_isNormalize(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x4E\x6F\x72\x6D\x61\x6C\x69\x7A\x65\x20\x74\x68\x65\x20\x76\x6F\x6C\x75\x6D\x65\x20\x6F\x66\x20\x74\x68\x65\x20\x57\x41\x56\x20\x66\x69\x6C\x65\x73\x20\x28\x64\x65\x66\x61\x75\x6C\x74\x3A\x20\x66\x61\x6C\x73\x65\x29\x2E\x20\x4E\x6F\x74\x65\x3A\x20\x74\x68\x69\x73\x20\x77\x6F\x72\x6B\x73\x20\x6F\x6E\x6C\x79\x20\x75\x6E\x64\x65\x72\x20\x57\x69\x6E\x64\x6F\x77\x73\x20\x73\x74\x61\x6E\x64\x61\x6C\x6F\x6E\x65\x2E"), NULL);
	}
}
static void AudioFileGenerator_t25C4CCBDEAE5E63CB0745BACFAEF787E31EEA298_CustomAttributesCacheGenerator_GenerateOnStart(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x45\x6E\x61\x62\x6C\x65\x20\x67\x65\x6E\x65\x72\x61\x74\x69\x6E\x67\x20\x6F\x66\x20\x74\x68\x65\x20\x74\x65\x78\x74\x73\x20\x6F\x6E\x20\x73\x74\x61\x72\x74\x20\x28\x64\x65\x66\x61\x75\x6C\x74\x3A\x20\x66\x61\x6C\x73\x65\x29\x2E"), NULL);
	}
}
static void AudioFileGenerator_t25C4CCBDEAE5E63CB0745BACFAEF787E31EEA298_CustomAttributesCacheGenerator_AudioFileGenerator_generate_m4FEFFBF2EFCD1E9AE3710DA8C6DA480C781F70F9(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CgenerateU3Ed__19_t158A067925EC863CE90E4DCCF85D7536E97CDBA0_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CgenerateU3Ed__19_t158A067925EC863CE90E4DCCF85D7536E97CDBA0_0_0_0_var), NULL);
	}
}
static void U3CgenerateU3Ed__19_t158A067925EC863CE90E4DCCF85D7536E97CDBA0_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CgenerateU3Ed__19_t158A067925EC863CE90E4DCCF85D7536E97CDBA0_CustomAttributesCacheGenerator_U3CgenerateU3Ed__19__ctor_m304D082A586B1520066B6F1F5D70922AF6AF4358(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CgenerateU3Ed__19_t158A067925EC863CE90E4DCCF85D7536E97CDBA0_CustomAttributesCacheGenerator_U3CgenerateU3Ed__19_System_IDisposable_Dispose_m9637C84D3DE5BA34D03CD47F7254960ED4DFFCB6(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CgenerateU3Ed__19_t158A067925EC863CE90E4DCCF85D7536E97CDBA0_CustomAttributesCacheGenerator_U3CgenerateU3Ed__19_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m3C28CECD7AE53E425198C381D87112731DEA0FB7(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CgenerateU3Ed__19_t158A067925EC863CE90E4DCCF85D7536E97CDBA0_CustomAttributesCacheGenerator_U3CgenerateU3Ed__19_System_Collections_IEnumerator_Reset_m45465FCA07B5C00BE97AA285379D2880E212DD73(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CgenerateU3Ed__19_t158A067925EC863CE90E4DCCF85D7536E97CDBA0_CustomAttributesCacheGenerator_U3CgenerateU3Ed__19_System_Collections_IEnumerator_get_Current_m1990C2C28E0F67CD67DE84F9E86ABAEE4EDECC3F(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void ChangeGender_t7942C8B9C9685EF714B6674AC27C681AC3D91104_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		ExecuteInEditMode_tAA3B5DE8B7E207BC6CAAFDB1F2502350C0546173 * tmp = (ExecuteInEditMode_tAA3B5DE8B7E207BC6CAAFDB1F2502350C0546173 *)cache->attributes[0];
		ExecuteInEditMode__ctor_m52849B67DB46F6CF090D45E523FAE97A10825C0A(tmp, NULL);
	}
	{
		HelpURLAttribute_t0924A6D83FABA7B77780F7F9BBCBCB9E0EA15023 * tmp = (HelpURLAttribute_t0924A6D83FABA7B77780F7F9BBCBCB9E0EA15023 *)cache->attributes[1];
		HelpURLAttribute__ctor_mCC837CE7900738F3152D585D454A34A15793C215(tmp, il2cpp_codegen_string_new_wrapper("\x68\x74\x74\x70\x73\x3A\x2F\x2F\x77\x77\x77\x2E\x63\x72\x6F\x73\x73\x74\x61\x6C\x65\x73\x2E\x63\x6F\x6D\x2F\x6D\x65\x64\x69\x61\x2F\x64\x61\x74\x61\x2F\x61\x73\x73\x65\x74\x73\x2F\x72\x74\x76\x6F\x69\x63\x65\x2F\x61\x70\x69\x2F\x63\x6C\x61\x73\x73\x5F\x63\x72\x6F\x73\x73\x74\x61\x6C\x65\x73\x5F\x31\x5F\x31\x5F\x72\x5F\x74\x5F\x76\x6F\x69\x63\x65\x5F\x31\x5F\x31\x5F\x74\x6F\x6F\x6C\x5F\x31\x5F\x31\x5F\x63\x68\x61\x6E\x67\x65\x5F\x67\x65\x6E\x64\x65\x72\x2E\x68\x74\x6D\x6C"), NULL);
	}
}
static void ChangeGender_t7942C8B9C9685EF714B6674AC27C681AC3D91104_CustomAttributesCacheGenerator_NewGender(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x54\x68\x65\x20\x6E\x65\x77\x20\x67\x65\x6E\x64\x65\x72\x20\x66\x6F\x72\x20\x61\x6C\x6C\x20\x76\x6F\x69\x63\x65\x73\x2E"), NULL);
	}
}
static void ChangeGender_t7942C8B9C9685EF714B6674AC27C681AC3D91104_CustomAttributesCacheGenerator_RefreshOnVoicesReady(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x52\x65\x66\x72\x65\x73\x68\x20\x6F\x6E\x20\x76\x6F\x69\x63\x65\x73\x20\x72\x65\x61\x64\x79\x20\x28\x64\x65\x66\x61\x75\x6C\x74\x3A\x20\x74\x72\x75\x65\x29\x2E"), NULL);
	}
}
static void ChangeGender_t7942C8B9C9685EF714B6674AC27C681AC3D91104_CustomAttributesCacheGenerator_ESpeakOnly(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x43\x68\x61\x6E\x67\x65\x20\x76\x6F\x69\x63\x65\x73\x20\x6F\x6E\x6C\x79\x20\x77\x68\x65\x6E\x20\x65\x53\x70\x65\x61\x6B\x20\x69\x73\x20\x75\x73\x65\x64\x20\x28\x64\x65\x66\x61\x75\x6C\x74\x3A\x20\x74\x72\x75\x65\x29\x2E"), NULL);
	}
}
static void Loudspeaker_t78C64F45415540BE87422E3A54942AA35E24ED42_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&AudioSource_tC4BF65AF8CDCAA63724BB3CA59A7A29249269E6B_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		HelpURLAttribute_t0924A6D83FABA7B77780F7F9BBCBCB9E0EA15023 * tmp = (HelpURLAttribute_t0924A6D83FABA7B77780F7F9BBCBCB9E0EA15023 *)cache->attributes[0];
		HelpURLAttribute__ctor_mCC837CE7900738F3152D585D454A34A15793C215(tmp, il2cpp_codegen_string_new_wrapper("\x68\x74\x74\x70\x73\x3A\x2F\x2F\x77\x77\x77\x2E\x63\x72\x6F\x73\x73\x74\x61\x6C\x65\x73\x2E\x63\x6F\x6D\x2F\x6D\x65\x64\x69\x61\x2F\x64\x61\x74\x61\x2F\x61\x73\x73\x65\x74\x73\x2F\x72\x74\x76\x6F\x69\x63\x65\x2F\x61\x70\x69\x2F\x63\x6C\x61\x73\x73\x5F\x63\x72\x6F\x73\x73\x74\x61\x6C\x65\x73\x5F\x31\x5F\x31\x5F\x72\x5F\x74\x5F\x76\x6F\x69\x63\x65\x5F\x31\x5F\x31\x5F\x74\x6F\x6F\x6C\x5F\x31\x5F\x31\x5F\x6C\x6F\x75\x64\x73\x70\x65\x61\x6B\x65\x72\x2E\x68\x74\x6D\x6C"), NULL);
	}
	{
		RequireComponent_tEDA546F9722B8874DA9658BDAB821BA49647FC91 * tmp = (RequireComponent_tEDA546F9722B8874DA9658BDAB821BA49647FC91 *)cache->attributes[1];
		RequireComponent__ctor_m5EC89D3D22D7D880E1B88A5C9FADF1FBDC713EE4(tmp, il2cpp_codegen_type_get_object(AudioSource_tC4BF65AF8CDCAA63724BB3CA59A7A29249269E6B_0_0_0_var), NULL);
	}
}
static void Loudspeaker_t78C64F45415540BE87422E3A54942AA35E24ED42_CustomAttributesCacheGenerator_Source(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x4F\x72\x69\x67\x69\x6E\x20\x41\x75\x64\x69\x6F\x53\x6F\x75\x72\x63\x65\x2E"), NULL);
	}
}
static void Loudspeaker_t78C64F45415540BE87422E3A54942AA35E24ED42_CustomAttributesCacheGenerator_Synchronized(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x53\x79\x6E\x63\x68\x72\x6F\x6E\x69\x7A\x65\x20\x77\x69\x74\x68\x20\x74\x68\x65\x20\x6F\x72\x69\x67\x69\x6E\x20\x28\x64\x65\x66\x61\x75\x6C\x74\x3A\x20\x66\x61\x6C\x73\x65\x29\x2E"), NULL);
	}
}
static void Loudspeaker_t78C64F45415540BE87422E3A54942AA35E24ED42_CustomAttributesCacheGenerator_SilenceSource(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x53\x69\x6C\x65\x6E\x63\x65\x20\x74\x68\x65\x20\x6F\x72\x69\x67\x69\x6E\x20\x28\x64\x65\x66\x61\x75\x6C\x74\x3A\x20\x74\x72\x75\x65\x29\x2E"), NULL);
	}
}
static void Paralanguage_t257B51534DB18301E85DF9EABF6265E9C7FC4F25_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&AudioSource_tC4BF65AF8CDCAA63724BB3CA59A7A29249269E6B_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		HelpURLAttribute_t0924A6D83FABA7B77780F7F9BBCBCB9E0EA15023 * tmp = (HelpURLAttribute_t0924A6D83FABA7B77780F7F9BBCBCB9E0EA15023 *)cache->attributes[0];
		HelpURLAttribute__ctor_mCC837CE7900738F3152D585D454A34A15793C215(tmp, il2cpp_codegen_string_new_wrapper("\x68\x74\x74\x70\x73\x3A\x2F\x2F\x77\x77\x77\x2E\x63\x72\x6F\x73\x73\x74\x61\x6C\x65\x73\x2E\x63\x6F\x6D\x2F\x6D\x65\x64\x69\x61\x2F\x64\x61\x74\x61\x2F\x61\x73\x73\x65\x74\x73\x2F\x72\x74\x76\x6F\x69\x63\x65\x2F\x61\x70\x69\x2F\x63\x6C\x61\x73\x73\x5F\x63\x72\x6F\x73\x73\x74\x61\x6C\x65\x73\x5F\x31\x5F\x31\x5F\x72\x5F\x74\x5F\x76\x6F\x69\x63\x65\x5F\x31\x5F\x31\x5F\x74\x6F\x6F\x6C\x5F\x31\x5F\x31\x5F\x70\x61\x72\x61\x6C\x61\x6E\x67\x75\x61\x67\x65\x2E\x68\x74\x6D\x6C"), NULL);
	}
	{
		RequireComponent_tEDA546F9722B8874DA9658BDAB821BA49647FC91 * tmp = (RequireComponent_tEDA546F9722B8874DA9658BDAB821BA49647FC91 *)cache->attributes[1];
		RequireComponent__ctor_m5EC89D3D22D7D880E1B88A5C9FADF1FBDC713EE4(tmp, il2cpp_codegen_type_get_object(AudioSource_tC4BF65AF8CDCAA63724BB3CA59A7A29249269E6B_0_0_0_var), NULL);
	}
}
static void Paralanguage_t257B51534DB18301E85DF9EABF6265E9C7FC4F25_CustomAttributesCacheGenerator_Text(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x54\x65\x78\x74\x20\x74\x6F\x20\x73\x70\x65\x61\x6B\x2E"), NULL);
	}
	{
		MultilineAttribute_tA4DCA9D8519E0C1B05636F2EFF74F81335A7F291 * tmp = (MultilineAttribute_tA4DCA9D8519E0C1B05636F2EFF74F81335A7F291 *)cache->attributes[1];
		MultilineAttribute__ctor_m353DC377C95D4C341F5DDD0F9894878F64E5703E(tmp, NULL);
	}
}
static void Paralanguage_t257B51534DB18301E85DF9EABF6265E9C7FC4F25_CustomAttributesCacheGenerator_Voices(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x56\x6F\x69\x63\x65\x73\x20\x66\x6F\x72\x20\x74\x68\x65\x20\x73\x70\x65\x65\x63\x68\x2E"), NULL);
	}
}
static void Paralanguage_t257B51534DB18301E85DF9EABF6265E9C7FC4F25_CustomAttributesCacheGenerator_Mode(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x53\x70\x65\x61\x6B\x20\x6D\x6F\x64\x65\x20\x28\x64\x65\x66\x61\x75\x6C\x74\x3A\x20\x27\x53\x70\x65\x61\x6B\x27\x29\x2E"), NULL);
	}
}
static void Paralanguage_t257B51534DB18301E85DF9EABF6265E9C7FC4F25_CustomAttributesCacheGenerator_Clips(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x41\x75\x64\x69\x6F\x20\x63\x6C\x69\x70\x73\x20\x74\x6F\x20\x70\x6C\x61\x79\x2E"), NULL);
	}
}
static void Paralanguage_t257B51534DB18301E85DF9EABF6265E9C7FC4F25_CustomAttributesCacheGenerator_Rate(CustomAttributesCache* cache)
{
	{
		HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB * tmp = (HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB *)cache->attributes[0];
		HeaderAttribute__ctor_m601319E0BCE8C44A9E79B2C0ABAAD0FEF46A9F1E(tmp, il2cpp_codegen_string_new_wrapper("\x4F\x70\x74\x69\x6F\x6E\x61\x6C\x20\x53\x65\x74\x74\x69\x6E\x67\x73"), NULL);
	}
	{
		RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5 * tmp = (RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5 *)cache->attributes[1];
		RangeAttribute__ctor_mC74D39A9F20DD2A0D4174F05785ABE4F0DAEF000(tmp, 0.0f, 3.0f, NULL);
	}
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[2];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x53\x70\x65\x65\x63\x68\x20\x72\x61\x74\x65\x20\x6F\x66\x20\x74\x68\x65\x20\x73\x70\x65\x61\x6B\x65\x72\x20\x69\x6E\x20\x70\x65\x72\x63\x65\x6E\x74\x20\x28\x31\x20\x3D\x20\x31\x30\x30\x25\x2C\x20\x64\x65\x66\x61\x75\x6C\x74\x3A\x20\x31\x2C\x20\x6F\x70\x74\x69\x6F\x6E\x61\x6C\x29\x2E"), NULL);
	}
}
static void Paralanguage_t257B51534DB18301E85DF9EABF6265E9C7FC4F25_CustomAttributesCacheGenerator_Pitch(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x53\x70\x65\x65\x63\x68\x20\x70\x69\x74\x63\x68\x20\x6F\x66\x20\x74\x68\x65\x20\x73\x70\x65\x61\x6B\x65\x72\x20\x69\x6E\x20\x70\x65\x72\x63\x65\x6E\x74\x20\x28\x31\x20\x3D\x20\x31\x30\x30\x25\x2C\x20\x64\x65\x66\x61\x75\x6C\x74\x3A\x20\x31\x2C\x20\x6F\x70\x74\x69\x6F\x6E\x61\x6C\x2C\x20\x6D\x6F\x62\x69\x6C\x65\x20\x6F\x6E\x6C\x79\x29\x2E"), NULL);
	}
	{
		RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5 * tmp = (RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5 *)cache->attributes[1];
		RangeAttribute__ctor_mC74D39A9F20DD2A0D4174F05785ABE4F0DAEF000(tmp, 0.0f, 2.0f, NULL);
	}
}
static void Paralanguage_t257B51534DB18301E85DF9EABF6265E9C7FC4F25_CustomAttributesCacheGenerator_Volume(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x56\x6F\x6C\x75\x6D\x65\x20\x6F\x66\x20\x74\x68\x65\x20\x73\x70\x65\x61\x6B\x65\x72\x20\x69\x6E\x20\x70\x65\x72\x63\x65\x6E\x74\x20\x28\x31\x20\x3D\x20\x31\x30\x30\x25\x2C\x20\x64\x65\x66\x61\x75\x6C\x74\x3A\x20\x31\x2C\x20\x6F\x70\x74\x69\x6F\x6E\x61\x6C\x2C\x20\x57\x69\x6E\x64\x6F\x77\x73\x20\x6F\x6E\x6C\x79\x29\x2E"), NULL);
	}
	{
		RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5 * tmp = (RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5 *)cache->attributes[1];
		RangeAttribute__ctor_mC74D39A9F20DD2A0D4174F05785ABE4F0DAEF000(tmp, 0.0f, 1.0f, NULL);
	}
}
static void Paralanguage_t257B51534DB18301E85DF9EABF6265E9C7FC4F25_CustomAttributesCacheGenerator_PlayOnStart(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x45\x6E\x61\x62\x6C\x65\x20\x73\x70\x65\x61\x6B\x69\x6E\x67\x20\x6F\x66\x20\x74\x68\x65\x20\x74\x65\x78\x74\x20\x6F\x6E\x20\x73\x74\x61\x72\x74\x20\x28\x64\x65\x66\x61\x75\x6C\x74\x3A\x20\x66\x61\x6C\x73\x65\x29\x2E"), NULL);
	}
	{
		HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB * tmp = (HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB *)cache->attributes[1];
		HeaderAttribute__ctor_m601319E0BCE8C44A9E79B2C0ABAAD0FEF46A9F1E(tmp, il2cpp_codegen_string_new_wrapper("\x42\x65\x68\x61\x76\x69\x6F\x75\x72\x20\x53\x65\x74\x74\x69\x6E\x67\x73"), NULL);
	}
}
static void Paralanguage_t257B51534DB18301E85DF9EABF6265E9C7FC4F25_CustomAttributesCacheGenerator_Delay(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x44\x65\x6C\x61\x79\x20\x75\x6E\x74\x69\x6C\x20\x74\x68\x65\x20\x73\x70\x65\x65\x63\x68\x20\x66\x6F\x72\x20\x74\x68\x69\x73\x20\x74\x65\x78\x74\x20\x73\x74\x61\x72\x74\x73\x20\x28\x64\x65\x66\x61\x75\x6C\x74\x3A\x20\x30\x29\x2E"), NULL);
	}
}
static void Paralanguage_t257B51534DB18301E85DF9EABF6265E9C7FC4F25_CustomAttributesCacheGenerator_Paralanguage_processStack_mA8A64AFE252A5308590B2241692C7A106025887D(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CprocessStackU3Ed__42_tED87FEFE7D9355FCAD33EAAEFFDA223FAB4C41D2_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CprocessStackU3Ed__42_tED87FEFE7D9355FCAD33EAAEFFDA223FAB4C41D2_0_0_0_var), NULL);
	}
}
static void U3CU3Ec_t721A41E997083B9226FF2AEB7F9CDE142231523C_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CprocessStackU3Ed__42_tED87FEFE7D9355FCAD33EAAEFFDA223FAB4C41D2_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CprocessStackU3Ed__42_tED87FEFE7D9355FCAD33EAAEFFDA223FAB4C41D2_CustomAttributesCacheGenerator_U3CprocessStackU3Ed__42__ctor_m21BC01AD74ECA7C64B458339F0B9DCC19A45ADE0(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CprocessStackU3Ed__42_tED87FEFE7D9355FCAD33EAAEFFDA223FAB4C41D2_CustomAttributesCacheGenerator_U3CprocessStackU3Ed__42_System_IDisposable_Dispose_m7430826A64DB1BA83762995F321AA81FDD62220A(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CprocessStackU3Ed__42_tED87FEFE7D9355FCAD33EAAEFFDA223FAB4C41D2_CustomAttributesCacheGenerator_U3CprocessStackU3Ed__42_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m6F766B569CFE9E96C325BEEC079BE142E366415D(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CprocessStackU3Ed__42_tED87FEFE7D9355FCAD33EAAEFFDA223FAB4C41D2_CustomAttributesCacheGenerator_U3CprocessStackU3Ed__42_System_Collections_IEnumerator_Reset_m93CF8EC321B392A350E480F18AA6D728FEA88BE6(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CprocessStackU3Ed__42_tED87FEFE7D9355FCAD33EAAEFFDA223FAB4C41D2_CustomAttributesCacheGenerator_U3CprocessStackU3Ed__42_System_Collections_IEnumerator_get_Current_m8EC92988F024B6CA75A7A577E3BAC83ACC67253A(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void Sequencer_t55938B3A6AE7BE5ECE446AC08D43795759152190_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		HelpURLAttribute_t0924A6D83FABA7B77780F7F9BBCBCB9E0EA15023 * tmp = (HelpURLAttribute_t0924A6D83FABA7B77780F7F9BBCBCB9E0EA15023 *)cache->attributes[0];
		HelpURLAttribute__ctor_mCC837CE7900738F3152D585D454A34A15793C215(tmp, il2cpp_codegen_string_new_wrapper("\x68\x74\x74\x70\x73\x3A\x2F\x2F\x77\x77\x77\x2E\x63\x72\x6F\x73\x73\x74\x61\x6C\x65\x73\x2E\x63\x6F\x6D\x2F\x6D\x65\x64\x69\x61\x2F\x64\x61\x74\x61\x2F\x61\x73\x73\x65\x74\x73\x2F\x72\x74\x76\x6F\x69\x63\x65\x2F\x61\x70\x69\x2F\x63\x6C\x61\x73\x73\x5F\x63\x72\x6F\x73\x73\x74\x61\x6C\x65\x73\x5F\x31\x5F\x31\x5F\x72\x5F\x74\x5F\x76\x6F\x69\x63\x65\x5F\x31\x5F\x31\x5F\x74\x6F\x6F\x6C\x5F\x31\x5F\x31\x5F\x73\x65\x71\x75\x65\x6E\x63\x65\x72\x2E\x68\x74\x6D\x6C"), NULL);
	}
}
static void Sequencer_t55938B3A6AE7BE5ECE446AC08D43795759152190_CustomAttributesCacheGenerator_Sequences(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x41\x6C\x6C\x20\x61\x76\x61\x69\x6C\x61\x62\x6C\x65\x20\x73\x65\x71\x75\x65\x6E\x63\x65\x73\x2E"), NULL);
	}
}
static void Sequencer_t55938B3A6AE7BE5ECE446AC08D43795759152190_CustomAttributesCacheGenerator_Delay(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x44\x65\x6C\x61\x79\x20\x69\x6E\x20\x73\x65\x63\x6F\x6E\x64\x73\x20\x62\x65\x66\x6F\x72\x65\x20\x74\x68\x65\x20\x53\x65\x71\x75\x65\x6E\x63\x65\x72\x20\x73\x74\x61\x72\x74\x73\x20\x70\x72\x6F\x63\x65\x73\x73\x69\x6E\x67\x20\x28\x64\x65\x66\x61\x75\x6C\x74\x3A\x20\x30\x29\x2E"), NULL);
	}
}
static void Sequencer_t55938B3A6AE7BE5ECE446AC08D43795759152190_CustomAttributesCacheGenerator_PlayOnStart(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x45\x6E\x61\x62\x6C\x65\x20\x74\x68\x65\x20\x53\x65\x71\x75\x65\x6E\x63\x65\x72\x20\x6F\x6E\x20\x73\x74\x61\x72\x74\x20\x28\x64\x65\x66\x61\x75\x6C\x74\x3A\x20\x66\x61\x6C\x73\x65\x29\x2E"), NULL);
	}
	{
		HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB * tmp = (HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB *)cache->attributes[1];
		HeaderAttribute__ctor_m601319E0BCE8C44A9E79B2C0ABAAD0FEF46A9F1E(tmp, il2cpp_codegen_string_new_wrapper("\x42\x65\x68\x61\x76\x69\x6F\x75\x72\x20\x53\x65\x74\x74\x69\x6E\x67\x73"), NULL);
	}
}
static void Sequencer_t55938B3A6AE7BE5ECE446AC08D43795759152190_CustomAttributesCacheGenerator_Sequencer_playMe_m092FC62EA99ECB5C44C73C2B37A12683DF2227B2(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CplayMeU3Ed__19_tDCB0B566C9E09125455A47D5F21A01BB9E163B27_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CplayMeU3Ed__19_tDCB0B566C9E09125455A47D5F21A01BB9E163B27_0_0_0_var), NULL);
	}
}
static void U3CplayMeU3Ed__19_tDCB0B566C9E09125455A47D5F21A01BB9E163B27_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CplayMeU3Ed__19_tDCB0B566C9E09125455A47D5F21A01BB9E163B27_CustomAttributesCacheGenerator_U3CplayMeU3Ed__19__ctor_mA6F984F9A7E5AD2F6BA681C9E7893F88E1C3FBAC(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CplayMeU3Ed__19_tDCB0B566C9E09125455A47D5F21A01BB9E163B27_CustomAttributesCacheGenerator_U3CplayMeU3Ed__19_System_IDisposable_Dispose_mCC358E65250BA6032B90AA940898A050746C8DE5(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CplayMeU3Ed__19_tDCB0B566C9E09125455A47D5F21A01BB9E163B27_CustomAttributesCacheGenerator_U3CplayMeU3Ed__19_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m26711DB4168A39C095F497E8F84AAF6FB867E0D6(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CplayMeU3Ed__19_tDCB0B566C9E09125455A47D5F21A01BB9E163B27_CustomAttributesCacheGenerator_U3CplayMeU3Ed__19_System_Collections_IEnumerator_Reset_mBAA7A47CC030736934F37ED90C374CEC7A67E6FB(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CplayMeU3Ed__19_tDCB0B566C9E09125455A47D5F21A01BB9E163B27_CustomAttributesCacheGenerator_U3CplayMeU3Ed__19_System_Collections_IEnumerator_get_Current_m4F1613CD9A49140520F720F374C25D5614AABD7F(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void SpeechText_t89F39958695673475D1D42B203FA444F75E5ED39_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		HelpURLAttribute_t0924A6D83FABA7B77780F7F9BBCBCB9E0EA15023 * tmp = (HelpURLAttribute_t0924A6D83FABA7B77780F7F9BBCBCB9E0EA15023 *)cache->attributes[0];
		HelpURLAttribute__ctor_mCC837CE7900738F3152D585D454A34A15793C215(tmp, il2cpp_codegen_string_new_wrapper("\x68\x74\x74\x70\x73\x3A\x2F\x2F\x77\x77\x77\x2E\x63\x72\x6F\x73\x73\x74\x61\x6C\x65\x73\x2E\x63\x6F\x6D\x2F\x6D\x65\x64\x69\x61\x2F\x64\x61\x74\x61\x2F\x61\x73\x73\x65\x74\x73\x2F\x72\x74\x76\x6F\x69\x63\x65\x2F\x61\x70\x69\x2F\x63\x6C\x61\x73\x73\x5F\x63\x72\x6F\x73\x73\x74\x61\x6C\x65\x73\x5F\x31\x5F\x31\x5F\x72\x5F\x74\x5F\x76\x6F\x69\x63\x65\x5F\x31\x5F\x31\x5F\x74\x6F\x6F\x6C\x5F\x31\x5F\x31\x5F\x73\x70\x65\x65\x63\x68\x5F\x74\x65\x78\x74\x2E\x68\x74\x6D\x6C"), NULL);
	}
}
static void SpeechText_t89F39958695673475D1D42B203FA444F75E5ED39_CustomAttributesCacheGenerator_Text(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x54\x65\x78\x74\x20\x74\x6F\x20\x73\x70\x65\x61\x6B\x2E"), NULL);
	}
	{
		MultilineAttribute_tA4DCA9D8519E0C1B05636F2EFF74F81335A7F291 * tmp = (MultilineAttribute_tA4DCA9D8519E0C1B05636F2EFF74F81335A7F291 *)cache->attributes[1];
		MultilineAttribute__ctor_m353DC377C95D4C341F5DDD0F9894878F64E5703E(tmp, NULL);
	}
}
static void SpeechText_t89F39958695673475D1D42B203FA444F75E5ED39_CustomAttributesCacheGenerator_Voices(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x56\x6F\x69\x63\x65\x73\x20\x66\x6F\x72\x20\x74\x68\x65\x20\x73\x70\x65\x65\x63\x68\x2E"), NULL);
	}
}
static void SpeechText_t89F39958695673475D1D42B203FA444F75E5ED39_CustomAttributesCacheGenerator_Mode(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x53\x70\x65\x61\x6B\x20\x6D\x6F\x64\x65\x20\x28\x64\x65\x66\x61\x75\x6C\x74\x3A\x20\x27\x53\x70\x65\x61\x6B\x27\x29\x2E"), NULL);
	}
}
static void SpeechText_t89F39958695673475D1D42B203FA444F75E5ED39_CustomAttributesCacheGenerator_Source(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x41\x75\x64\x69\x6F\x53\x6F\x75\x72\x63\x65\x20\x66\x6F\x72\x20\x74\x68\x65\x20\x6F\x75\x74\x70\x75\x74\x20\x28\x6F\x70\x74\x69\x6F\x6E\x61\x6C\x29\x2E"), NULL);
	}
	{
		HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB * tmp = (HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB *)cache->attributes[1];
		HeaderAttribute__ctor_m601319E0BCE8C44A9E79B2C0ABAAD0FEF46A9F1E(tmp, il2cpp_codegen_string_new_wrapper("\x4F\x70\x74\x69\x6F\x6E\x61\x6C\x20\x53\x65\x74\x74\x69\x6E\x67\x73"), NULL);
	}
}
static void SpeechText_t89F39958695673475D1D42B203FA444F75E5ED39_CustomAttributesCacheGenerator_Rate(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x53\x70\x65\x65\x63\x68\x20\x72\x61\x74\x65\x20\x6F\x66\x20\x74\x68\x65\x20\x73\x70\x65\x61\x6B\x65\x72\x20\x69\x6E\x20\x70\x65\x72\x63\x65\x6E\x74\x20\x28\x31\x20\x3D\x20\x31\x30\x30\x25\x2C\x20\x64\x65\x66\x61\x75\x6C\x74\x3A\x20\x31\x2C\x20\x6F\x70\x74\x69\x6F\x6E\x61\x6C\x29\x2E"), NULL);
	}
	{
		RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5 * tmp = (RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5 *)cache->attributes[1];
		RangeAttribute__ctor_mC74D39A9F20DD2A0D4174F05785ABE4F0DAEF000(tmp, 0.0f, 3.0f, NULL);
	}
}
static void SpeechText_t89F39958695673475D1D42B203FA444F75E5ED39_CustomAttributesCacheGenerator_Pitch(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x53\x70\x65\x65\x63\x68\x20\x70\x69\x74\x63\x68\x20\x6F\x66\x20\x74\x68\x65\x20\x73\x70\x65\x61\x6B\x65\x72\x20\x69\x6E\x20\x70\x65\x72\x63\x65\x6E\x74\x20\x28\x31\x20\x3D\x20\x31\x30\x30\x25\x2C\x20\x64\x65\x66\x61\x75\x6C\x74\x3A\x20\x31\x2C\x20\x6F\x70\x74\x69\x6F\x6E\x61\x6C\x2C\x20\x6D\x6F\x62\x69\x6C\x65\x20\x6F\x6E\x6C\x79\x29\x2E"), NULL);
	}
	{
		RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5 * tmp = (RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5 *)cache->attributes[1];
		RangeAttribute__ctor_mC74D39A9F20DD2A0D4174F05785ABE4F0DAEF000(tmp, 0.0f, 2.0f, NULL);
	}
}
static void SpeechText_t89F39958695673475D1D42B203FA444F75E5ED39_CustomAttributesCacheGenerator_Volume(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x56\x6F\x6C\x75\x6D\x65\x20\x6F\x66\x20\x74\x68\x65\x20\x73\x70\x65\x61\x6B\x65\x72\x20\x69\x6E\x20\x70\x65\x72\x63\x65\x6E\x74\x20\x28\x31\x20\x3D\x20\x31\x30\x30\x25\x2C\x20\x64\x65\x66\x61\x75\x6C\x74\x3A\x20\x31\x2C\x20\x6F\x70\x74\x69\x6F\x6E\x61\x6C\x2C\x20\x57\x69\x6E\x64\x6F\x77\x73\x20\x6F\x6E\x6C\x79\x29\x2E"), NULL);
	}
	{
		RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5 * tmp = (RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5 *)cache->attributes[1];
		RangeAttribute__ctor_mC74D39A9F20DD2A0D4174F05785ABE4F0DAEF000(tmp, 0.0f, 1.0f, NULL);
	}
}
static void SpeechText_t89F39958695673475D1D42B203FA444F75E5ED39_CustomAttributesCacheGenerator_PlayOnStart(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x45\x6E\x61\x62\x6C\x65\x20\x73\x70\x65\x61\x6B\x69\x6E\x67\x20\x6F\x66\x20\x74\x68\x65\x20\x74\x65\x78\x74\x20\x6F\x6E\x20\x73\x74\x61\x72\x74\x20\x28\x64\x65\x66\x61\x75\x6C\x74\x3A\x20\x66\x61\x6C\x73\x65\x29\x2E"), NULL);
	}
	{
		HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB * tmp = (HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB *)cache->attributes[1];
		HeaderAttribute__ctor_m601319E0BCE8C44A9E79B2C0ABAAD0FEF46A9F1E(tmp, il2cpp_codegen_string_new_wrapper("\x42\x65\x68\x61\x76\x69\x6F\x75\x72\x20\x53\x65\x74\x74\x69\x6E\x67\x73"), NULL);
	}
}
static void SpeechText_t89F39958695673475D1D42B203FA444F75E5ED39_CustomAttributesCacheGenerator_Delay(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x44\x65\x6C\x61\x79\x20\x69\x6E\x20\x73\x65\x63\x6F\x6E\x64\x73\x20\x75\x6E\x74\x69\x6C\x20\x74\x68\x65\x20\x73\x70\x65\x65\x63\x68\x20\x66\x6F\x72\x20\x74\x68\x69\x73\x20\x74\x65\x78\x74\x20\x73\x74\x61\x72\x74\x73\x20\x28\x64\x65\x66\x61\x75\x6C\x74\x3A\x20\x30\x29\x2E"), NULL);
	}
}
static void SpeechText_t89F39958695673475D1D42B203FA444F75E5ED39_CustomAttributesCacheGenerator_GenerateAudioFile(CustomAttributesCache* cache)
{
	{
		HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB * tmp = (HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB *)cache->attributes[0];
		HeaderAttribute__ctor_m601319E0BCE8C44A9E79B2C0ABAAD0FEF46A9F1E(tmp, il2cpp_codegen_string_new_wrapper("\x4F\x75\x74\x70\x75\x74\x20\x46\x69\x6C\x65\x20\x53\x65\x74\x74\x69\x6E\x67\x73"), NULL);
	}
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[1];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x47\x65\x6E\x65\x72\x61\x74\x65\x20\x61\x75\x64\x69\x6F\x20\x66\x69\x6C\x65\x20\x6F\x6E\x2F\x6F\x66\x66\x20\x28\x64\x65\x66\x61\x75\x6C\x74\x3A\x20\x66\x61\x6C\x73\x65\x29\x2E"), NULL);
	}
}
static void SpeechText_t89F39958695673475D1D42B203FA444F75E5ED39_CustomAttributesCacheGenerator_FileName(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x46\x69\x6C\x65\x20\x6E\x61\x6D\x65\x20\x28\x69\x6E\x63\x6C\x2E\x20\x70\x61\x74\x68\x29\x20\x66\x6F\x72\x20\x74\x68\x65\x20\x67\x65\x6E\x65\x72\x61\x74\x65\x64\x20\x61\x75\x64\x69\x6F\x2E"), NULL);
	}
}
static void SpeechText_t89F39958695673475D1D42B203FA444F75E5ED39_CustomAttributesCacheGenerator_FileInsideAssets(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x49\x73\x20\x74\x68\x65\x20\x67\x65\x6E\x65\x72\x61\x74\x65\x64\x20\x66\x69\x6C\x65\x20\x70\x61\x74\x68\x20\x69\x6E\x73\x69\x64\x65\x20\x74\x68\x65\x20\x41\x73\x73\x65\x74\x73\x2D\x66\x6F\x6C\x64\x65\x72\x20\x28\x63\x75\x72\x72\x65\x6E\x74\x20\x70\x72\x6F\x6A\x65\x63\x74\x29\x3F\x20\x49\x66\x20\x74\x68\x69\x73\x20\x6F\x70\x74\x69\x6F\x6E\x20\x69\x73\x20\x65\x6E\x61\x62\x6C\x65\x64\x2C\x20\x69\x74\x20\x70\x72\x65\x66\x69\x78\x65\x73\x20\x74\x68\x65\x20\x70\x61\x74\x68\x20\x77\x69\x74\x68\x20\x27\x41\x70\x70\x6C\x69\x63\x61\x74\x69\x6F\x6E\x2E\x64\x61\x74\x61\x50\x61\x74\x68\x27\x2E"), NULL);
	}
}
static void TextFileSpeaker_t28C4E74F2AA327B88BD90BBE4A123A435747410C_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		HelpURLAttribute_t0924A6D83FABA7B77780F7F9BBCBCB9E0EA15023 * tmp = (HelpURLAttribute_t0924A6D83FABA7B77780F7F9BBCBCB9E0EA15023 *)cache->attributes[0];
		HelpURLAttribute__ctor_mCC837CE7900738F3152D585D454A34A15793C215(tmp, il2cpp_codegen_string_new_wrapper("\x68\x74\x74\x70\x73\x3A\x2F\x2F\x77\x77\x77\x2E\x63\x72\x6F\x73\x73\x74\x61\x6C\x65\x73\x2E\x63\x6F\x6D\x2F\x6D\x65\x64\x69\x61\x2F\x64\x61\x74\x61\x2F\x61\x73\x73\x65\x74\x73\x2F\x72\x74\x76\x6F\x69\x63\x65\x2F\x61\x70\x69\x2F\x63\x6C\x61\x73\x73\x5F\x63\x72\x6F\x73\x73\x74\x61\x6C\x65\x73\x5F\x31\x5F\x31\x5F\x72\x5F\x74\x5F\x76\x6F\x69\x63\x65\x5F\x31\x5F\x31\x5F\x74\x6F\x6F\x6C\x5F\x31\x5F\x31\x5F\x74\x65\x78\x74\x5F\x66\x69\x6C\x65\x5F\x73\x70\x65\x61\x6B\x65\x72\x2E\x68\x74\x6D\x6C"), NULL);
	}
	{
		ExecuteInEditMode_tAA3B5DE8B7E207BC6CAAFDB1F2502350C0546173 * tmp = (ExecuteInEditMode_tAA3B5DE8B7E207BC6CAAFDB1F2502350C0546173 *)cache->attributes[1];
		ExecuteInEditMode__ctor_m52849B67DB46F6CF090D45E523FAE97A10825C0A(tmp, NULL);
	}
}
static void TextFileSpeaker_t28C4E74F2AA327B88BD90BBE4A123A435747410C_CustomAttributesCacheGenerator_TextFiles(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x54\x65\x78\x74\x20\x66\x69\x6C\x65\x73\x20\x74\x6F\x20\x73\x70\x65\x61\x6B\x2E"), NULL);
	}
}
static void TextFileSpeaker_t28C4E74F2AA327B88BD90BBE4A123A435747410C_CustomAttributesCacheGenerator_Voices(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x56\x6F\x69\x63\x65\x73\x20\x66\x6F\x72\x20\x74\x68\x65\x20\x73\x70\x65\x65\x63\x68\x2E"), NULL);
	}
}
static void TextFileSpeaker_t28C4E74F2AA327B88BD90BBE4A123A435747410C_CustomAttributesCacheGenerator_Mode(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x53\x70\x65\x61\x6B\x20\x6D\x6F\x64\x65\x20\x28\x64\x65\x66\x61\x75\x6C\x74\x3A\x20\x27\x53\x70\x65\x61\x6B\x27\x29\x2E"), NULL);
	}
}
static void TextFileSpeaker_t28C4E74F2AA327B88BD90BBE4A123A435747410C_CustomAttributesCacheGenerator_PlayOnStart(CustomAttributesCache* cache)
{
	{
		HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB * tmp = (HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB *)cache->attributes[0];
		HeaderAttribute__ctor_m601319E0BCE8C44A9E79B2C0ABAAD0FEF46A9F1E(tmp, il2cpp_codegen_string_new_wrapper("\x42\x65\x68\x61\x76\x69\x6F\x75\x72\x20\x53\x65\x74\x74\x69\x6E\x67\x73"), NULL);
	}
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[1];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x45\x6E\x61\x62\x6C\x65\x20\x73\x70\x65\x61\x6B\x69\x6E\x67\x20\x6F\x66\x20\x61\x20\x72\x61\x6E\x64\x6F\x6D\x20\x74\x65\x78\x74\x20\x66\x69\x6C\x65\x20\x6F\x6E\x20\x73\x74\x61\x72\x74\x20\x28\x64\x65\x66\x61\x75\x6C\x74\x3A\x20\x66\x61\x6C\x73\x65\x29\x2E"), NULL);
	}
}
static void TextFileSpeaker_t28C4E74F2AA327B88BD90BBE4A123A435747410C_CustomAttributesCacheGenerator_PlayAllOnStart(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x45\x6E\x61\x62\x6C\x65\x20\x73\x70\x65\x61\x6B\x69\x6E\x67\x20\x6F\x66\x20\x61\x20\x72\x61\x6E\x64\x6F\x6D\x20\x74\x65\x78\x74\x20\x66\x69\x6C\x65\x20\x6F\x6E\x20\x73\x74\x61\x72\x74\x20\x28\x64\x65\x66\x61\x75\x6C\x74\x3A\x20\x66\x61\x6C\x73\x65\x29\x2E"), NULL);
	}
}
static void TextFileSpeaker_t28C4E74F2AA327B88BD90BBE4A123A435747410C_CustomAttributesCacheGenerator_SpeakRandom(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x53\x70\x65\x61\x6B\x73\x20\x74\x68\x65\x20\x74\x65\x78\x74\x20\x66\x69\x6C\x65\x73\x20\x69\x6E\x20\x72\x61\x6E\x64\x6F\x6D\x20\x6F\x72\x64\x65\x72\x20\x28\x64\x65\x66\x61\x75\x6C\x74\x3A\x20\x66\x61\x6C\x73\x65\x29\x2E"), NULL);
	}
}
static void TextFileSpeaker_t28C4E74F2AA327B88BD90BBE4A123A435747410C_CustomAttributesCacheGenerator_Delay(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x44\x65\x6C\x61\x79\x20\x75\x6E\x74\x69\x6C\x20\x74\x68\x65\x20\x73\x70\x65\x65\x63\x68\x20\x66\x6F\x72\x20\x74\x68\x69\x73\x20\x74\x65\x78\x74\x20\x73\x74\x61\x72\x74\x73\x20\x28\x64\x65\x66\x61\x75\x6C\x74\x3A\x20\x30\x29\x2E"), NULL);
	}
}
static void TextFileSpeaker_t28C4E74F2AA327B88BD90BBE4A123A435747410C_CustomAttributesCacheGenerator_Source(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x41\x75\x64\x69\x6F\x53\x6F\x75\x72\x63\x65\x20\x66\x6F\x72\x20\x74\x68\x65\x20\x6F\x75\x74\x70\x75\x74\x20\x28\x6F\x70\x74\x69\x6F\x6E\x61\x6C\x29\x2E"), NULL);
	}
	{
		HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB * tmp = (HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB *)cache->attributes[1];
		HeaderAttribute__ctor_m601319E0BCE8C44A9E79B2C0ABAAD0FEF46A9F1E(tmp, il2cpp_codegen_string_new_wrapper("\x4F\x70\x74\x69\x6F\x6E\x61\x6C\x20\x53\x65\x74\x74\x69\x6E\x67\x73"), NULL);
	}
}
static void TextFileSpeaker_t28C4E74F2AA327B88BD90BBE4A123A435747410C_CustomAttributesCacheGenerator_Rate(CustomAttributesCache* cache)
{
	{
		RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5 * tmp = (RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5 *)cache->attributes[0];
		RangeAttribute__ctor_mC74D39A9F20DD2A0D4174F05785ABE4F0DAEF000(tmp, 0.0f, 3.0f, NULL);
	}
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[1];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x53\x70\x65\x65\x63\x68\x20\x72\x61\x74\x65\x20\x6F\x66\x20\x74\x68\x65\x20\x73\x70\x65\x61\x6B\x65\x72\x20\x69\x6E\x20\x70\x65\x72\x63\x65\x6E\x74\x20\x28\x31\x20\x3D\x20\x31\x30\x30\x25\x2C\x20\x64\x65\x66\x61\x75\x6C\x74\x3A\x20\x31\x2C\x20\x6F\x70\x74\x69\x6F\x6E\x61\x6C\x29\x2E"), NULL);
	}
}
static void TextFileSpeaker_t28C4E74F2AA327B88BD90BBE4A123A435747410C_CustomAttributesCacheGenerator_Pitch(CustomAttributesCache* cache)
{
	{
		RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5 * tmp = (RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5 *)cache->attributes[0];
		RangeAttribute__ctor_mC74D39A9F20DD2A0D4174F05785ABE4F0DAEF000(tmp, 0.0f, 2.0f, NULL);
	}
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[1];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x53\x70\x65\x65\x63\x68\x20\x70\x69\x74\x63\x68\x20\x6F\x66\x20\x74\x68\x65\x20\x73\x70\x65\x61\x6B\x65\x72\x20\x69\x6E\x20\x70\x65\x72\x63\x65\x6E\x74\x20\x28\x31\x20\x3D\x20\x31\x30\x30\x25\x2C\x20\x64\x65\x66\x61\x75\x6C\x74\x3A\x20\x31\x2C\x20\x6F\x70\x74\x69\x6F\x6E\x61\x6C\x2C\x20\x6D\x6F\x62\x69\x6C\x65\x20\x6F\x6E\x6C\x79\x29\x2E"), NULL);
	}
}
static void TextFileSpeaker_t28C4E74F2AA327B88BD90BBE4A123A435747410C_CustomAttributesCacheGenerator_Volume(CustomAttributesCache* cache)
{
	{
		RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5 * tmp = (RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5 *)cache->attributes[0];
		RangeAttribute__ctor_mC74D39A9F20DD2A0D4174F05785ABE4F0DAEF000(tmp, 0.0f, 1.0f, NULL);
	}
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[1];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x56\x6F\x6C\x75\x6D\x65\x20\x6F\x66\x20\x74\x68\x65\x20\x73\x70\x65\x61\x6B\x65\x72\x20\x69\x6E\x20\x70\x65\x72\x63\x65\x6E\x74\x20\x28\x31\x20\x3D\x20\x31\x30\x30\x25\x2C\x20\x64\x65\x66\x61\x75\x6C\x74\x3A\x20\x31\x2C\x20\x6F\x70\x74\x69\x6F\x6E\x61\x6C\x2C\x20\x57\x69\x6E\x64\x6F\x77\x73\x20\x6F\x6E\x6C\x79\x29\x2E"), NULL);
	}
}
static void VoiceInitalizer_t628535D1C01FC59180A5EE351F3502A45342556C_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		HelpURLAttribute_t0924A6D83FABA7B77780F7F9BBCBCB9E0EA15023 * tmp = (HelpURLAttribute_t0924A6D83FABA7B77780F7F9BBCBCB9E0EA15023 *)cache->attributes[0];
		HelpURLAttribute__ctor_mCC837CE7900738F3152D585D454A34A15793C215(tmp, il2cpp_codegen_string_new_wrapper("\x68\x74\x74\x70\x73\x3A\x2F\x2F\x63\x72\x6F\x73\x73\x74\x61\x6C\x65\x73\x2E\x63\x6F\x6D\x2F\x6D\x65\x64\x69\x61\x2F\x64\x61\x74\x61\x2F\x61\x73\x73\x65\x74\x73\x2F\x72\x74\x76\x6F\x69\x63\x65\x2F\x61\x70\x69\x2F\x63\x6C\x61\x73\x73\x5F\x63\x72\x6F\x73\x73\x74\x61\x6C\x65\x73\x5F\x31\x5F\x31\x5F\x72\x5F\x74\x5F\x76\x6F\x69\x63\x65\x5F\x31\x5F\x31\x5F\x74\x6F\x6F\x6C\x5F\x31\x5F\x31\x5F\x76\x6F\x69\x63\x65\x5F\x69\x6E\x69\x74\x61\x6C\x69\x7A\x65\x72\x2E\x68\x74\x6D\x6C"), NULL);
	}
}
static void VoiceInitalizer_t628535D1C01FC59180A5EE351F3502A45342556C_CustomAttributesCacheGenerator_Provider(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x53\x65\x6C\x65\x63\x74\x65\x64\x20\x70\x72\x6F\x76\x69\x64\x65\x72\x20\x74\x6F\x20\x69\x6E\x69\x74\x61\x6C\x69\x7A\x65\x20\x74\x68\x65\x20\x76\x6F\x69\x63\x65\x73\x20\x28\x64\x65\x66\x61\x75\x6C\x74\x3A\x20\x41\x6E\x64\x72\x6F\x69\x64\x29\x2E"), NULL);
	}
}
static void VoiceInitalizer_t628535D1C01FC59180A5EE351F3502A45342556C_CustomAttributesCacheGenerator_VoiceNames(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x49\x6E\x69\x74\x69\x61\x6C\x69\x7A\x65\x20\x76\x6F\x69\x63\x65\x73\x20\x62\x79\x20\x6E\x61\x6D\x65\x2E"), NULL);
	}
}
static void VoiceInitalizer_t628535D1C01FC59180A5EE351F3502A45342556C_CustomAttributesCacheGenerator_AllVoices(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x49\x6E\x69\x74\x69\x61\x6C\x69\x7A\x65\x20\x61\x6C\x6C\x20\x76\x6F\x69\x63\x65\x73\x20\x28\x64\x65\x66\x61\x75\x6C\x74\x3A\x20\x66\x61\x6C\x73\x65\x29\x2E"), NULL);
	}
}
static void VoiceInitalizer_t628535D1C01FC59180A5EE351F3502A45342556C_CustomAttributesCacheGenerator_DestroyWhenFinished(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x44\x65\x73\x74\x72\x6F\x79\x20\x74\x68\x65\x20\x67\x61\x6D\x65\x6F\x62\x6A\x65\x63\x74\x20\x61\x66\x74\x65\x72\x20\x69\x6E\x69\x74\x69\x61\x6C\x69\x7A\x65\x20\x28\x64\x65\x66\x61\x75\x6C\x74\x3A\x20\x74\x72\x75\x65\x29\x2E"), NULL);
	}
	{
		HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB * tmp = (HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB *)cache->attributes[1];
		HeaderAttribute__ctor_m601319E0BCE8C44A9E79B2C0ABAAD0FEF46A9F1E(tmp, il2cpp_codegen_string_new_wrapper("\x42\x65\x68\x61\x76\x69\x6F\x75\x72\x20\x53\x65\x74\x74\x69\x6E\x67\x73"), NULL);
	}
}
static void VoiceInitalizer_t628535D1C01FC59180A5EE351F3502A45342556C_CustomAttributesCacheGenerator_VoiceInitalizer_initalizeVoices_m00AE8AD949E2048D303BF3DCB9AFC0BF91CC35AF(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CinitalizeVoicesU3Ed__10_t2A275B1AA68149AB13303E07E9688291E5EF2C20_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CinitalizeVoicesU3Ed__10_t2A275B1AA68149AB13303E07E9688291E5EF2C20_0_0_0_var), NULL);
	}
}
static void U3CinitalizeVoicesU3Ed__10_t2A275B1AA68149AB13303E07E9688291E5EF2C20_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CinitalizeVoicesU3Ed__10_t2A275B1AA68149AB13303E07E9688291E5EF2C20_CustomAttributesCacheGenerator_U3CinitalizeVoicesU3Ed__10__ctor_m8362902403B1FE161F594248B8AE6732B925DCBD(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CinitalizeVoicesU3Ed__10_t2A275B1AA68149AB13303E07E9688291E5EF2C20_CustomAttributesCacheGenerator_U3CinitalizeVoicesU3Ed__10_System_IDisposable_Dispose_mF897C063121C49E81290DF42E4FB044C856A921F(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CinitalizeVoicesU3Ed__10_t2A275B1AA68149AB13303E07E9688291E5EF2C20_CustomAttributesCacheGenerator_U3CinitalizeVoicesU3Ed__10_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m19AB22AD386CF1E797DEF859EBA73F5980575238(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CinitalizeVoicesU3Ed__10_t2A275B1AA68149AB13303E07E9688291E5EF2C20_CustomAttributesCacheGenerator_U3CinitalizeVoicesU3Ed__10_System_Collections_IEnumerator_Reset_mA2AC727EFF62D2404A9D9E0A44431EB15FA6E21D(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CinitalizeVoicesU3Ed__10_t2A275B1AA68149AB13303E07E9688291E5EF2C20_CustomAttributesCacheGenerator_U3CinitalizeVoicesU3Ed__10_System_Collections_IEnumerator_get_Current_m6EE41A71936F6DC8E38DE9780509A78D505FC0B2(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void BaseCustomVoiceProvider_tA11734995C990E2A5451C062F8CBBE1EA94E2C97_CustomAttributesCacheGenerator_BaseCustomVoiceProvider_playAudioFile_mFF2435C8CA85BA9B44A9967159EE60E7F1E38EF3(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CplayAudioFileU3Ed__81_t16FD3617907DBC006BF7EDBFDD79142630A5DC55_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CplayAudioFileU3Ed__81_t16FD3617907DBC006BF7EDBFDD79142630A5DC55_0_0_0_var), NULL);
	}
}
static void U3CU3Ec_tFA29053F24A1921702C048CAEE862DA27E2A9F58_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CplayAudioFileU3Ed__81_t16FD3617907DBC006BF7EDBFDD79142630A5DC55_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CplayAudioFileU3Ed__81_t16FD3617907DBC006BF7EDBFDD79142630A5DC55_CustomAttributesCacheGenerator_U3CplayAudioFileU3Ed__81__ctor_m12F9909D1F5B37CBBDF5768383EE59A10995FCA5(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CplayAudioFileU3Ed__81_t16FD3617907DBC006BF7EDBFDD79142630A5DC55_CustomAttributesCacheGenerator_U3CplayAudioFileU3Ed__81_System_IDisposable_Dispose_m8F70B0ED165D9F8F2C0F4ED496647D22AE55E010(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CplayAudioFileU3Ed__81_t16FD3617907DBC006BF7EDBFDD79142630A5DC55_CustomAttributesCacheGenerator_U3CplayAudioFileU3Ed__81_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mBD68E65ACF9A83E963B49E130B6E07ADB5D4FA19(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CplayAudioFileU3Ed__81_t16FD3617907DBC006BF7EDBFDD79142630A5DC55_CustomAttributesCacheGenerator_U3CplayAudioFileU3Ed__81_System_Collections_IEnumerator_Reset_mFA6ACB523E034EF1620981E29B012849F698D0C5(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CplayAudioFileU3Ed__81_t16FD3617907DBC006BF7EDBFDD79142630A5DC55_CustomAttributesCacheGenerator_U3CplayAudioFileU3Ed__81_System_Collections_IEnumerator_get_Current_m6007D9DD5B76EFE943D370BBEC3A868B0447FDF6(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void BaseVoiceProvider_t0427110C1F97E96455F9DE9BF0D4BB9ABDA63EBA_CustomAttributesCacheGenerator_BaseVoiceProvider_playAudioFile_m4CCFF1BB44ED2ACE5F10AB4AAEE81D2B7CD24DD7(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CplayAudioFileU3Ed__79_t5D80B48C5103969538788365776FBFD2481F2989_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CplayAudioFileU3Ed__79_t5D80B48C5103969538788365776FBFD2481F2989_0_0_0_var), NULL);
	}
}
static void U3CU3Ec_t870715C29A8E9D55FA8B0E48F5CA97C1D03FEC5C_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CplayAudioFileU3Ed__79_t5D80B48C5103969538788365776FBFD2481F2989_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CplayAudioFileU3Ed__79_t5D80B48C5103969538788365776FBFD2481F2989_CustomAttributesCacheGenerator_U3CplayAudioFileU3Ed__79__ctor_m1918F5820D88E3F6361AF83F51F6389CA0FE8495(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CplayAudioFileU3Ed__79_t5D80B48C5103969538788365776FBFD2481F2989_CustomAttributesCacheGenerator_U3CplayAudioFileU3Ed__79_System_IDisposable_Dispose_m016A01B81977C3EED37BE31F4601782D790E2577(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CplayAudioFileU3Ed__79_t5D80B48C5103969538788365776FBFD2481F2989_CustomAttributesCacheGenerator_U3CplayAudioFileU3Ed__79_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m2F7ED2CAB4FD2CE5ACDEE216AA960E8F48A1D4ED(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CplayAudioFileU3Ed__79_t5D80B48C5103969538788365776FBFD2481F2989_CustomAttributesCacheGenerator_U3CplayAudioFileU3Ed__79_System_Collections_IEnumerator_Reset_m7EAB980BAE0EC597EB0D8AF89DD2E220F7600552(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CplayAudioFileU3Ed__79_t5D80B48C5103969538788365776FBFD2481F2989_CustomAttributesCacheGenerator_U3CplayAudioFileU3Ed__79_System_Collections_IEnumerator_get_Current_m081CAEDEE10983E393B7386FBD4D29ACC25D70B3(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void VoiceProviderIOS_tF5BF7BFF6D3A9FB9D597BBC28387E6B27E524A97_CustomAttributesCacheGenerator_VoiceProviderIOS_SpeakNative_m5A460E4C43E3F6B3F5C5B50B2FBA960E906087A1(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CSpeakNativeU3Ed__37_tB56F739052E648271E910C501E0E5DE0A2826C12_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CSpeakNativeU3Ed__37_tB56F739052E648271E910C501E0E5DE0A2826C12_0_0_0_var), NULL);
	}
}
static void VoiceProviderIOS_tF5BF7BFF6D3A9FB9D597BBC28387E6B27E524A97_CustomAttributesCacheGenerator_VoiceProviderIOS_Speak_m5D3CE8F57DAE33BE09E8A2F7AF87281DC47181D0(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CSpeakU3Ed__38_t0CAF7D4D1537BFF140CFFE747E35BD71F0C3FD33_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CSpeakU3Ed__38_t0CAF7D4D1537BFF140CFFE747E35BD71F0C3FD33_0_0_0_var), NULL);
	}
}
static void VoiceProviderIOS_tF5BF7BFF6D3A9FB9D597BBC28387E6B27E524A97_CustomAttributesCacheGenerator_VoiceProviderIOS_Generate_m5C4D7AC723D419BA1EFBD9411242CBE29C6D09BB(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CGenerateU3Ed__39_t03292738BAB852A4B9E700EEBCEA2F4808456175_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CGenerateU3Ed__39_t03292738BAB852A4B9E700EEBCEA2F4808456175_0_0_0_var), NULL);
	}
}
static void VoiceProviderIOS_tF5BF7BFF6D3A9FB9D597BBC28387E6B27E524A97_CustomAttributesCacheGenerator_VoiceProviderIOS_speak_mFB7D7BA16B879B9C8E69B83CDA91D109189EB96E(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CspeakU3Ed__41_t6D7ABE0037F6C095D53C8134C299473D203F6A54_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CspeakU3Ed__41_t6D7ABE0037F6C095D53C8134C299473D203F6A54_0_0_0_var), NULL);
	}
}
static void U3CU3Ec_tE90D39DB69EB0C0E2F146BB1C619945A4E114B80_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CSpeakNativeU3Ed__37_tB56F739052E648271E910C501E0E5DE0A2826C12_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CSpeakNativeU3Ed__37_tB56F739052E648271E910C501E0E5DE0A2826C12_CustomAttributesCacheGenerator_U3CSpeakNativeU3Ed__37__ctor_m523A47BABBCEB6115AB8BC5602D2194F4C9DEF4F(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CSpeakNativeU3Ed__37_tB56F739052E648271E910C501E0E5DE0A2826C12_CustomAttributesCacheGenerator_U3CSpeakNativeU3Ed__37_System_IDisposable_Dispose_m8516463D2831A9F5409582D34F16DC2697018BD8(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CSpeakNativeU3Ed__37_tB56F739052E648271E910C501E0E5DE0A2826C12_CustomAttributesCacheGenerator_U3CSpeakNativeU3Ed__37_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mF2CE12F2B43A8A935DAA332E01EF3DF9D3659427(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CSpeakNativeU3Ed__37_tB56F739052E648271E910C501E0E5DE0A2826C12_CustomAttributesCacheGenerator_U3CSpeakNativeU3Ed__37_System_Collections_IEnumerator_Reset_mDE6E3EAB0C3383024A07F9518EC95E2615BE50A2(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CSpeakNativeU3Ed__37_tB56F739052E648271E910C501E0E5DE0A2826C12_CustomAttributesCacheGenerator_U3CSpeakNativeU3Ed__37_System_Collections_IEnumerator_get_Current_mF4BB2E19D59C10E2E19E1A071D8B963C2318D72B(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CSpeakU3Ed__38_t0CAF7D4D1537BFF140CFFE747E35BD71F0C3FD33_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CSpeakU3Ed__38_t0CAF7D4D1537BFF140CFFE747E35BD71F0C3FD33_CustomAttributesCacheGenerator_U3CSpeakU3Ed__38__ctor_m17AA83B8F4292BE9A434FAD451B6042F85B1CA7A(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CSpeakU3Ed__38_t0CAF7D4D1537BFF140CFFE747E35BD71F0C3FD33_CustomAttributesCacheGenerator_U3CSpeakU3Ed__38_System_IDisposable_Dispose_m1D62F9167AD94A284EB61F82C0A4F55B3CD87CFD(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CSpeakU3Ed__38_t0CAF7D4D1537BFF140CFFE747E35BD71F0C3FD33_CustomAttributesCacheGenerator_U3CSpeakU3Ed__38_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m55A294AB03578EC259B3F90FF30ED7D6E9B850BB(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CSpeakU3Ed__38_t0CAF7D4D1537BFF140CFFE747E35BD71F0C3FD33_CustomAttributesCacheGenerator_U3CSpeakU3Ed__38_System_Collections_IEnumerator_Reset_mB2BC205AFCE9DA70AAF9757596AB8EE0E2085E41(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CSpeakU3Ed__38_t0CAF7D4D1537BFF140CFFE747E35BD71F0C3FD33_CustomAttributesCacheGenerator_U3CSpeakU3Ed__38_System_Collections_IEnumerator_get_Current_m3D7E6B2FE72C3643435F91F8C65FDBD74A4E2C6B(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CGenerateU3Ed__39_t03292738BAB852A4B9E700EEBCEA2F4808456175_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CGenerateU3Ed__39_t03292738BAB852A4B9E700EEBCEA2F4808456175_CustomAttributesCacheGenerator_U3CGenerateU3Ed__39__ctor_mD9733152DA5C477BC3EBDE9374773C3614007179(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CGenerateU3Ed__39_t03292738BAB852A4B9E700EEBCEA2F4808456175_CustomAttributesCacheGenerator_U3CGenerateU3Ed__39_System_IDisposable_Dispose_m77EA5EA8743977C2BB472BD2FF79D131B2A25D38(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CGenerateU3Ed__39_t03292738BAB852A4B9E700EEBCEA2F4808456175_CustomAttributesCacheGenerator_U3CGenerateU3Ed__39_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m55C35151E2EBDA7C7287CC1DB8AC808094F27B2A(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CGenerateU3Ed__39_t03292738BAB852A4B9E700EEBCEA2F4808456175_CustomAttributesCacheGenerator_U3CGenerateU3Ed__39_System_Collections_IEnumerator_Reset_m4D4A7F1C40FB78E0D45F689E2638CF4203F0BB8B(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CGenerateU3Ed__39_t03292738BAB852A4B9E700EEBCEA2F4808456175_CustomAttributesCacheGenerator_U3CGenerateU3Ed__39_System_Collections_IEnumerator_get_Current_mECE12334B8337427611893D644D1735115B00175(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CspeakU3Ed__41_t6D7ABE0037F6C095D53C8134C299473D203F6A54_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CspeakU3Ed__41_t6D7ABE0037F6C095D53C8134C299473D203F6A54_CustomAttributesCacheGenerator_U3CspeakU3Ed__41__ctor_m276AE39E567257B32C7896EA8EDDA24EC9D824ED(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CspeakU3Ed__41_t6D7ABE0037F6C095D53C8134C299473D203F6A54_CustomAttributesCacheGenerator_U3CspeakU3Ed__41_System_IDisposable_Dispose_m0E4873CEED2CE27F21F5EB92F97879BFB53783C2(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CspeakU3Ed__41_t6D7ABE0037F6C095D53C8134C299473D203F6A54_CustomAttributesCacheGenerator_U3CspeakU3Ed__41_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mEEA1EC15804398BACB904C1707E5281F50328F18(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CspeakU3Ed__41_t6D7ABE0037F6C095D53C8134C299473D203F6A54_CustomAttributesCacheGenerator_U3CspeakU3Ed__41_System_Collections_IEnumerator_Reset_m93EA86B1BE247D3CE920516B739BB3E25B25C55F(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CspeakU3Ed__41_t6D7ABE0037F6C095D53C8134C299473D203F6A54_CustomAttributesCacheGenerator_U3CspeakU3Ed__41_System_Collections_IEnumerator_get_Current_m95840D9DA7B2AD2A2C84637718BD96658116F22F(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void VoiceProviderMary_t4D4BF6869E86B5E6E86F4C105F3369AF9D343573_CustomAttributesCacheGenerator_VoiceProviderMary_SpeakNative_m331610A2ED4ACEA5F1B1E8285E531D9A812A09C2(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CSpeakNativeU3Ed__29_tAE002A72C2A3F020A17BF4A56FEBBC5EA2787303_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CSpeakNativeU3Ed__29_tAE002A72C2A3F020A17BF4A56FEBBC5EA2787303_0_0_0_var), NULL);
	}
}
static void VoiceProviderMary_t4D4BF6869E86B5E6E86F4C105F3369AF9D343573_CustomAttributesCacheGenerator_VoiceProviderMary_Speak_m891800543E668A5334BD528A4144C8785D80DAE7(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CSpeakU3Ed__30_tC962B657041FD44AF5A88FD52604438A78BA1649_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CSpeakU3Ed__30_tC962B657041FD44AF5A88FD52604438A78BA1649_0_0_0_var), NULL);
	}
}
static void VoiceProviderMary_t4D4BF6869E86B5E6E86F4C105F3369AF9D343573_CustomAttributesCacheGenerator_VoiceProviderMary_Generate_m5E6DF2FF9DB49139BB98D02BB3B8703159E50EB3(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CGenerateU3Ed__31_tFA662A35AA694F207DCA5B6C83CA1964EBA15A73_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CGenerateU3Ed__31_tFA662A35AA694F207DCA5B6C83CA1964EBA15A73_0_0_0_var), NULL);
	}
}
static void VoiceProviderMary_t4D4BF6869E86B5E6E86F4C105F3369AF9D343573_CustomAttributesCacheGenerator_VoiceProviderMary_speak_mEDD8B66B63C43FD85CAD280D309C4276729D3FD6(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CspeakU3Ed__32_tDEC1697FFF0729D0B77A9482BE143E6DC4C893CD_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CspeakU3Ed__32_tDEC1697FFF0729D0B77A9482BE143E6DC4C893CD_0_0_0_var), NULL);
	}
}
static void VoiceProviderMary_t4D4BF6869E86B5E6E86F4C105F3369AF9D343573_CustomAttributesCacheGenerator_VoiceProviderMary_getVoices_mA44E670C9E75CC20259D5BFBC4884064E4BB1DCC(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CgetVoicesU3Ed__33_t42277CC46AC28C4C054B2A9350319771F5FBA45D_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CgetVoicesU3Ed__33_t42277CC46AC28C4C054B2A9350319771F5FBA45D_0_0_0_var), NULL);
	}
}
static void U3CSpeakNativeU3Ed__29_tAE002A72C2A3F020A17BF4A56FEBBC5EA2787303_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CSpeakNativeU3Ed__29_tAE002A72C2A3F020A17BF4A56FEBBC5EA2787303_CustomAttributesCacheGenerator_U3CSpeakNativeU3Ed__29__ctor_m7470A0504C3BABDC965367131EA97B330ECAA0B3(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CSpeakNativeU3Ed__29_tAE002A72C2A3F020A17BF4A56FEBBC5EA2787303_CustomAttributesCacheGenerator_U3CSpeakNativeU3Ed__29_System_IDisposable_Dispose_m02B9BF12673B6C720138AE5C87DCA36928F31A6B(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CSpeakNativeU3Ed__29_tAE002A72C2A3F020A17BF4A56FEBBC5EA2787303_CustomAttributesCacheGenerator_U3CSpeakNativeU3Ed__29_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mB9405A8A86C3770CC1F111D0C4000A7A9B7D7A67(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CSpeakNativeU3Ed__29_tAE002A72C2A3F020A17BF4A56FEBBC5EA2787303_CustomAttributesCacheGenerator_U3CSpeakNativeU3Ed__29_System_Collections_IEnumerator_Reset_mBC4615F0056EF0BC799A52C6A4149F468953E3B1(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CSpeakNativeU3Ed__29_tAE002A72C2A3F020A17BF4A56FEBBC5EA2787303_CustomAttributesCacheGenerator_U3CSpeakNativeU3Ed__29_System_Collections_IEnumerator_get_Current_mFED8E5538033280FD348A7C8FFB81D8D26DC06A2(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CSpeakU3Ed__30_tC962B657041FD44AF5A88FD52604438A78BA1649_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CSpeakU3Ed__30_tC962B657041FD44AF5A88FD52604438A78BA1649_CustomAttributesCacheGenerator_U3CSpeakU3Ed__30__ctor_m9587AD139AB417176D703120DF98958092A13743(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CSpeakU3Ed__30_tC962B657041FD44AF5A88FD52604438A78BA1649_CustomAttributesCacheGenerator_U3CSpeakU3Ed__30_System_IDisposable_Dispose_m23CE40AF61F6D8649073C36D77C590953E895803(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CSpeakU3Ed__30_tC962B657041FD44AF5A88FD52604438A78BA1649_CustomAttributesCacheGenerator_U3CSpeakU3Ed__30_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m4BE9CD55DFC56B895DDBBEDFFFF9D846C2F24096(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CSpeakU3Ed__30_tC962B657041FD44AF5A88FD52604438A78BA1649_CustomAttributesCacheGenerator_U3CSpeakU3Ed__30_System_Collections_IEnumerator_Reset_mDDC9BFD5B6F9F4992FF961A1916F6CFFF370032B(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CSpeakU3Ed__30_tC962B657041FD44AF5A88FD52604438A78BA1649_CustomAttributesCacheGenerator_U3CSpeakU3Ed__30_System_Collections_IEnumerator_get_Current_m80972AF0FFE79545A22C01A56202CEA16644B859(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CGenerateU3Ed__31_tFA662A35AA694F207DCA5B6C83CA1964EBA15A73_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CGenerateU3Ed__31_tFA662A35AA694F207DCA5B6C83CA1964EBA15A73_CustomAttributesCacheGenerator_U3CGenerateU3Ed__31__ctor_m267A2910841978F5CDB6CCB831A10313B0798DEC(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CGenerateU3Ed__31_tFA662A35AA694F207DCA5B6C83CA1964EBA15A73_CustomAttributesCacheGenerator_U3CGenerateU3Ed__31_System_IDisposable_Dispose_m22DC6783EC7327F0EFAFDF726143D978509C7C62(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CGenerateU3Ed__31_tFA662A35AA694F207DCA5B6C83CA1964EBA15A73_CustomAttributesCacheGenerator_U3CGenerateU3Ed__31_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m6BA121BFF88A61F810A85519EA5166243AA8F2F7(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CGenerateU3Ed__31_tFA662A35AA694F207DCA5B6C83CA1964EBA15A73_CustomAttributesCacheGenerator_U3CGenerateU3Ed__31_System_Collections_IEnumerator_Reset_mA8522467F71712CDE975F96D86B51B3F6C04B1CC(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CGenerateU3Ed__31_tFA662A35AA694F207DCA5B6C83CA1964EBA15A73_CustomAttributesCacheGenerator_U3CGenerateU3Ed__31_System_Collections_IEnumerator_get_Current_m9EC33BA38BF83D3CA8FF84D807491EEBDC56CBD2(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CspeakU3Ed__32_tDEC1697FFF0729D0B77A9482BE143E6DC4C893CD_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CspeakU3Ed__32_tDEC1697FFF0729D0B77A9482BE143E6DC4C893CD_CustomAttributesCacheGenerator_U3CspeakU3Ed__32__ctor_m39F1A6EF5BF1ECD0725D91A20A03439E3AD4E103(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CspeakU3Ed__32_tDEC1697FFF0729D0B77A9482BE143E6DC4C893CD_CustomAttributesCacheGenerator_U3CspeakU3Ed__32_System_IDisposable_Dispose_m47A016B1C9DA6E21E7501088414B7AE4026C210E(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CspeakU3Ed__32_tDEC1697FFF0729D0B77A9482BE143E6DC4C893CD_CustomAttributesCacheGenerator_U3CspeakU3Ed__32_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m8CFA25DFB6CD460D6C3AA1108C416152A15230F5(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CspeakU3Ed__32_tDEC1697FFF0729D0B77A9482BE143E6DC4C893CD_CustomAttributesCacheGenerator_U3CspeakU3Ed__32_System_Collections_IEnumerator_Reset_m73F40ECB8D160F5A0111DCDC777F3BFB1CEEC6F3(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CspeakU3Ed__32_tDEC1697FFF0729D0B77A9482BE143E6DC4C893CD_CustomAttributesCacheGenerator_U3CspeakU3Ed__32_System_Collections_IEnumerator_get_Current_m2D2650AC8FF888202F794982EB33EE9B57F98367(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CU3Ec_tB1BC9FCD16045B07F039E456A3F897DE7E8C1CDF_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CgetVoicesU3Ed__33_t42277CC46AC28C4C054B2A9350319771F5FBA45D_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CgetVoicesU3Ed__33_t42277CC46AC28C4C054B2A9350319771F5FBA45D_CustomAttributesCacheGenerator_U3CgetVoicesU3Ed__33__ctor_m95F6F409F82D4F4973F63E456C242F66A76B3205(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CgetVoicesU3Ed__33_t42277CC46AC28C4C054B2A9350319771F5FBA45D_CustomAttributesCacheGenerator_U3CgetVoicesU3Ed__33_System_IDisposable_Dispose_mF7D198C1948C01A6A4D9D46B84AB60DE30DF3C30(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CgetVoicesU3Ed__33_t42277CC46AC28C4C054B2A9350319771F5FBA45D_CustomAttributesCacheGenerator_U3CgetVoicesU3Ed__33_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m2C5797A9C9F2E33DE06235C398E7B36C6EA1197B(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CgetVoicesU3Ed__33_t42277CC46AC28C4C054B2A9350319771F5FBA45D_CustomAttributesCacheGenerator_U3CgetVoicesU3Ed__33_System_Collections_IEnumerator_Reset_m995AFECA0C5D06884A5DA8C78BF92AF75EAB19E1(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CgetVoicesU3Ed__33_t42277CC46AC28C4C054B2A9350319771F5FBA45D_CustomAttributesCacheGenerator_U3CgetVoicesU3Ed__33_System_Collections_IEnumerator_get_Current_mA53C128274D93B9FE40D699DD1192FC1A9C5B829(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void Sequence_t9F70DC5B0D7CF74B0FC88E0784E5857D0A475F79_CustomAttributesCacheGenerator_Text(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x54\x65\x78\x74\x20\x74\x6F\x20\x73\x70\x65\x61\x6B\x2E"), NULL);
	}
	{
		MultilineAttribute_tA4DCA9D8519E0C1B05636F2EFF74F81335A7F291 * tmp = (MultilineAttribute_tA4DCA9D8519E0C1B05636F2EFF74F81335A7F291 *)cache->attributes[1];
		MultilineAttribute__ctor_m353DC377C95D4C341F5DDD0F9894878F64E5703E(tmp, NULL);
	}
}
static void Sequence_t9F70DC5B0D7CF74B0FC88E0784E5857D0A475F79_CustomAttributesCacheGenerator_Voices(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x56\x6F\x69\x63\x65\x73\x20\x66\x6F\x72\x20\x74\x68\x65\x20\x73\x70\x65\x65\x63\x68\x2E"), NULL);
	}
}
static void Sequence_t9F70DC5B0D7CF74B0FC88E0784E5857D0A475F79_CustomAttributesCacheGenerator_Mode(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x53\x70\x65\x61\x6B\x20\x6D\x6F\x64\x65\x20\x28\x64\x65\x66\x61\x75\x6C\x74\x3A\x20\x27\x53\x70\x65\x61\x6B\x27\x29\x2E"), NULL);
	}
}
static void Sequence_t9F70DC5B0D7CF74B0FC88E0784E5857D0A475F79_CustomAttributesCacheGenerator_Source(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x41\x75\x64\x69\x6F\x53\x6F\x75\x72\x63\x65\x20\x66\x6F\x72\x20\x74\x68\x65\x20\x6F\x75\x74\x70\x75\x74\x20\x28\x6F\x70\x74\x69\x6F\x6E\x61\x6C\x29\x2E"), NULL);
	}
	{
		HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB * tmp = (HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB *)cache->attributes[1];
		HeaderAttribute__ctor_m601319E0BCE8C44A9E79B2C0ABAAD0FEF46A9F1E(tmp, il2cpp_codegen_string_new_wrapper("\x4F\x70\x74\x69\x6F\x6E\x61\x6C\x20\x53\x65\x74\x74\x69\x6E\x67\x73"), NULL);
	}
}
static void Sequence_t9F70DC5B0D7CF74B0FC88E0784E5857D0A475F79_CustomAttributesCacheGenerator_Rate(CustomAttributesCache* cache)
{
	{
		RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5 * tmp = (RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5 *)cache->attributes[0];
		RangeAttribute__ctor_mC74D39A9F20DD2A0D4174F05785ABE4F0DAEF000(tmp, 0.0f, 3.0f, NULL);
	}
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[1];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x53\x70\x65\x65\x63\x68\x20\x72\x61\x74\x65\x20\x6F\x66\x20\x74\x68\x65\x20\x73\x70\x65\x61\x6B\x65\x72\x20\x69\x6E\x20\x70\x65\x72\x63\x65\x6E\x74\x20\x28\x31\x20\x3D\x20\x31\x30\x30\x25\x2C\x20\x64\x65\x66\x61\x75\x6C\x74\x3A\x20\x31\x2C\x20\x6F\x70\x74\x69\x6F\x6E\x61\x6C\x29\x2E"), NULL);
	}
}
static void Sequence_t9F70DC5B0D7CF74B0FC88E0784E5857D0A475F79_CustomAttributesCacheGenerator_Pitch(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x53\x70\x65\x65\x63\x68\x20\x70\x69\x74\x63\x68\x20\x6F\x66\x20\x74\x68\x65\x20\x73\x70\x65\x61\x6B\x65\x72\x20\x69\x6E\x20\x70\x65\x72\x63\x65\x6E\x74\x20\x28\x31\x20\x3D\x20\x31\x30\x30\x25\x2C\x20\x64\x65\x66\x61\x75\x6C\x74\x3A\x20\x31\x2C\x20\x6F\x70\x74\x69\x6F\x6E\x61\x6C\x2C\x20\x6D\x6F\x62\x69\x6C\x65\x20\x6F\x6E\x6C\x79\x29\x2E"), NULL);
	}
	{
		RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5 * tmp = (RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5 *)cache->attributes[1];
		RangeAttribute__ctor_mC74D39A9F20DD2A0D4174F05785ABE4F0DAEF000(tmp, 0.0f, 2.0f, NULL);
	}
}
static void Sequence_t9F70DC5B0D7CF74B0FC88E0784E5857D0A475F79_CustomAttributesCacheGenerator_Volume(CustomAttributesCache* cache)
{
	{
		RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5 * tmp = (RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5 *)cache->attributes[0];
		RangeAttribute__ctor_mC74D39A9F20DD2A0D4174F05785ABE4F0DAEF000(tmp, 0.0f, 1.0f, NULL);
	}
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[1];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x56\x6F\x6C\x75\x6D\x65\x20\x6F\x66\x20\x74\x68\x65\x20\x73\x70\x65\x61\x6B\x65\x72\x20\x69\x6E\x20\x70\x65\x72\x63\x65\x6E\x74\x20\x28\x31\x20\x3D\x20\x31\x30\x30\x25\x2C\x20\x64\x65\x66\x61\x75\x6C\x74\x3A\x20\x31\x2C\x20\x6F\x70\x74\x69\x6F\x6E\x61\x6C\x2C\x20\x57\x69\x6E\x64\x6F\x77\x73\x20\x6F\x6E\x6C\x79\x29\x2E"), NULL);
	}
}
static void Sequence_t9F70DC5B0D7CF74B0FC88E0784E5857D0A475F79_CustomAttributesCacheGenerator_initalized(CustomAttributesCache* cache)
{
	{
		HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA * tmp = (HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA *)cache->attributes[0];
		HideInInspector__ctor_mE2B7FB1D206A74BA583C7812CDB4EBDD83EB66F9(tmp, NULL);
	}
}
static void Voice_t4C22EF6D29FB2A9A2E0AD7E539BDCD5F93C63D01_CustomAttributesCacheGenerator_Name(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x4E\x61\x6D\x65\x20\x6F\x66\x20\x74\x68\x65\x20\x76\x6F\x69\x63\x65\x2E"), NULL);
	}
}
static void Voice_t4C22EF6D29FB2A9A2E0AD7E539BDCD5F93C63D01_CustomAttributesCacheGenerator_Description(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x44\x65\x73\x63\x72\x69\x70\x74\x69\x6F\x6E\x20\x6F\x66\x20\x74\x68\x65\x20\x76\x6F\x69\x63\x65\x2E"), NULL);
	}
}
static void Voice_t4C22EF6D29FB2A9A2E0AD7E539BDCD5F93C63D01_CustomAttributesCacheGenerator_Gender(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x47\x65\x6E\x64\x65\x72\x20\x6F\x66\x20\x74\x68\x65\x20\x76\x6F\x69\x63\x65\x2E"), NULL);
	}
}
static void Voice_t4C22EF6D29FB2A9A2E0AD7E539BDCD5F93C63D01_CustomAttributesCacheGenerator_Age(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x41\x67\x65\x20\x6F\x66\x20\x74\x68\x65\x20\x76\x6F\x69\x63\x65\x2E"), NULL);
	}
}
static void Voice_t4C22EF6D29FB2A9A2E0AD7E539BDCD5F93C63D01_CustomAttributesCacheGenerator_Identifier(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x49\x64\x65\x6E\x74\x69\x66\x69\x65\x72\x20\x6F\x66\x20\x74\x68\x65\x20\x76\x6F\x69\x63\x65\x2E"), NULL);
	}
}
static void Voice_t4C22EF6D29FB2A9A2E0AD7E539BDCD5F93C63D01_CustomAttributesCacheGenerator_Vendor(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x56\x65\x6E\x64\x6F\x72\x20\x6F\x66\x20\x74\x68\x65\x20\x76\x6F\x69\x63\x65\x2E"), NULL);
	}
}
static void Voice_t4C22EF6D29FB2A9A2E0AD7E539BDCD5F93C63D01_CustomAttributesCacheGenerator_Version(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x56\x65\x72\x73\x69\x6F\x6E\x20\x6F\x66\x20\x74\x68\x65\x20\x76\x6F\x69\x63\x65\x2E"), NULL);
	}
}
static void VoiceAlias_t721FA1FC67A2E220FE5CDEE91532251CEED5B588_CustomAttributesCacheGenerator_VoiceNameWindows(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x4E\x61\x6D\x65\x20\x6F\x66\x20\x74\x68\x65\x20\x76\x6F\x69\x63\x65\x20\x75\x6E\x64\x65\x72\x20\x57\x69\x6E\x64\x6F\x77\x73\x2E"), NULL);
	}
}
static void VoiceAlias_t721FA1FC67A2E220FE5CDEE91532251CEED5B588_CustomAttributesCacheGenerator_VoiceNameMac(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x4E\x61\x6D\x65\x20\x6F\x66\x20\x74\x68\x65\x20\x76\x6F\x69\x63\x65\x20\x75\x6E\x64\x65\x72\x20\x6D\x61\x63\x4F\x53\x2E"), NULL);
	}
}
static void VoiceAlias_t721FA1FC67A2E220FE5CDEE91532251CEED5B588_CustomAttributesCacheGenerator_VoiceNameLinux(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x4E\x61\x6D\x65\x20\x6F\x66\x20\x74\x68\x65\x20\x76\x6F\x69\x63\x65\x20\x75\x6E\x64\x65\x72\x20\x4C\x69\x6E\x75\x78\x20\x61\x6E\x64\x20\x66\x6F\x72\x20\x65\x53\x70\x65\x61\x6B\x2E"), NULL);
	}
}
static void VoiceAlias_t721FA1FC67A2E220FE5CDEE91532251CEED5B588_CustomAttributesCacheGenerator_VoiceNameAndroid(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x4E\x61\x6D\x65\x20\x6F\x66\x20\x74\x68\x65\x20\x76\x6F\x69\x63\x65\x20\x75\x6E\x64\x65\x72\x20\x41\x6E\x64\x72\x6F\x69\x64\x2E"), NULL);
	}
}
static void VoiceAlias_t721FA1FC67A2E220FE5CDEE91532251CEED5B588_CustomAttributesCacheGenerator_VoiceNameIOS(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x4E\x61\x6D\x65\x20\x6F\x66\x20\x74\x68\x65\x20\x76\x6F\x69\x63\x65\x20\x75\x6E\x64\x65\x72\x20\x69\x4F\x53\x2E"), NULL);
	}
}
static void VoiceAlias_t721FA1FC67A2E220FE5CDEE91532251CEED5B588_CustomAttributesCacheGenerator_VoiceNameWSA(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x4E\x61\x6D\x65\x20\x6F\x66\x20\x74\x68\x65\x20\x76\x6F\x69\x63\x65\x20\x75\x6E\x64\x65\x72\x20\x57\x53\x41\x2E"), NULL);
	}
}
static void VoiceAlias_t721FA1FC67A2E220FE5CDEE91532251CEED5B588_CustomAttributesCacheGenerator_VoiceNameMaryTTS(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x4E\x61\x6D\x65\x20\x6F\x66\x20\x74\x68\x65\x20\x76\x6F\x69\x63\x65\x20\x75\x6E\x64\x65\x72\x20\x4D\x61\x72\x79\x54\x54\x53\x2E"), NULL);
	}
}
static void VoiceAlias_t721FA1FC67A2E220FE5CDEE91532251CEED5B588_CustomAttributesCacheGenerator_VoiceNameCustom(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x4E\x61\x6D\x65\x20\x6F\x66\x20\x74\x68\x65\x20\x76\x6F\x69\x63\x65\x20\x66\x6F\x72\x20\x63\x75\x73\x74\x6F\x6D\x20\x54\x54\x53\x2D\x73\x79\x73\x74\x65\x6D\x73\x2E"), NULL);
	}
}
static void VoiceAlias_t721FA1FC67A2E220FE5CDEE91532251CEED5B588_CustomAttributesCacheGenerator_Culture(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x46\x61\x6C\x6C\x62\x61\x63\x6B\x20\x63\x75\x6C\x74\x75\x72\x65\x20\x66\x6F\x72\x20\x74\x68\x65\x20\x74\x65\x78\x74\x20\x28\x65\x2E\x67\x2E\x20\x27\x65\x6E\x27\x2C\x20\x6F\x70\x74\x69\x6F\x6E\x61\x6C\x29\x2E"), NULL);
	}
}
static void VoiceAlias_t721FA1FC67A2E220FE5CDEE91532251CEED5B588_CustomAttributesCacheGenerator_Gender(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x46\x61\x6C\x6C\x62\x61\x63\x6B\x20\x67\x65\x6E\x64\x65\x72\x20\x66\x6F\x72\x20\x74\x68\x65\x20\x74\x65\x78\x74\x2E"), NULL);
	}
}
static void Dialog_t638F1505D57D2F294DDEA45174D9130410067CD7_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		HelpURLAttribute_t0924A6D83FABA7B77780F7F9BBCBCB9E0EA15023 * tmp = (HelpURLAttribute_t0924A6D83FABA7B77780F7F9BBCBCB9E0EA15023 *)cache->attributes[0];
		HelpURLAttribute__ctor_mCC837CE7900738F3152D585D454A34A15793C215(tmp, il2cpp_codegen_string_new_wrapper("\x68\x74\x74\x70\x73\x3A\x2F\x2F\x77\x77\x77\x2E\x63\x72\x6F\x73\x73\x74\x61\x6C\x65\x73\x2E\x63\x6F\x6D\x2F\x6D\x65\x64\x69\x61\x2F\x64\x61\x74\x61\x2F\x61\x73\x73\x65\x74\x73\x2F\x72\x74\x76\x6F\x69\x63\x65\x2F\x61\x70\x69\x2F\x63\x6C\x61\x73\x73\x5F\x63\x72\x6F\x73\x73\x74\x61\x6C\x65\x73\x5F\x31\x5F\x31\x5F\x72\x5F\x74\x5F\x76\x6F\x69\x63\x65\x5F\x31\x5F\x31\x5F\x64\x65\x6D\x6F\x5F\x31\x5F\x31\x5F\x64\x69\x61\x6C\x6F\x67\x2E\x68\x74\x6D\x6C"), NULL);
	}
}
static void Dialog_t638F1505D57D2F294DDEA45174D9130410067CD7_CustomAttributesCacheGenerator_CultureA(CustomAttributesCache* cache)
{
	{
		HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB * tmp = (HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB *)cache->attributes[0];
		HeaderAttribute__ctor_m601319E0BCE8C44A9E79B2C0ABAAD0FEF46A9F1E(tmp, il2cpp_codegen_string_new_wrapper("\x43\x6F\x6E\x66\x69\x67\x75\x72\x61\x74\x69\x6F\x6E"), NULL);
	}
}
static void Dialog_t638F1505D57D2F294DDEA45174D9130410067CD7_CustomAttributesCacheGenerator_RateA(CustomAttributesCache* cache)
{
	{
		RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5 * tmp = (RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5 *)cache->attributes[0];
		RangeAttribute__ctor_mC74D39A9F20DD2A0D4174F05785ABE4F0DAEF000(tmp, 0.0f, 3.0f, NULL);
	}
}
static void Dialog_t638F1505D57D2F294DDEA45174D9130410067CD7_CustomAttributesCacheGenerator_RateB(CustomAttributesCache* cache)
{
	{
		RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5 * tmp = (RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5 *)cache->attributes[0];
		RangeAttribute__ctor_mC74D39A9F20DD2A0D4174F05785ABE4F0DAEF000(tmp, 0.0f, 3.0f, NULL);
	}
}
static void Dialog_t638F1505D57D2F294DDEA45174D9130410067CD7_CustomAttributesCacheGenerator_PitchA(CustomAttributesCache* cache)
{
	{
		RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5 * tmp = (RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5 *)cache->attributes[0];
		RangeAttribute__ctor_mC74D39A9F20DD2A0D4174F05785ABE4F0DAEF000(tmp, 0.0f, 2.0f, NULL);
	}
}
static void Dialog_t638F1505D57D2F294DDEA45174D9130410067CD7_CustomAttributesCacheGenerator_PitchB(CustomAttributesCache* cache)
{
	{
		RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5 * tmp = (RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5 *)cache->attributes[0];
		RangeAttribute__ctor_mC74D39A9F20DD2A0D4174F05785ABE4F0DAEF000(tmp, 0.0f, 2.0f, NULL);
	}
}
static void Dialog_t638F1505D57D2F294DDEA45174D9130410067CD7_CustomAttributesCacheGenerator_VolumeA(CustomAttributesCache* cache)
{
	{
		RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5 * tmp = (RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5 *)cache->attributes[0];
		RangeAttribute__ctor_mC74D39A9F20DD2A0D4174F05785ABE4F0DAEF000(tmp, 0.0f, 1.0f, NULL);
	}
}
static void Dialog_t638F1505D57D2F294DDEA45174D9130410067CD7_CustomAttributesCacheGenerator_VolumeB(CustomAttributesCache* cache)
{
	{
		RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5 * tmp = (RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5 *)cache->attributes[0];
		RangeAttribute__ctor_mC74D39A9F20DD2A0D4174F05785ABE4F0DAEF000(tmp, 0.0f, 1.0f, NULL);
	}
}
static void Dialog_t638F1505D57D2F294DDEA45174D9130410067CD7_CustomAttributesCacheGenerator_DialogPersonA(CustomAttributesCache* cache)
{
	{
		HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB * tmp = (HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB *)cache->attributes[0];
		HeaderAttribute__ctor_m601319E0BCE8C44A9E79B2C0ABAAD0FEF46A9F1E(tmp, il2cpp_codegen_string_new_wrapper("\x44\x69\x61\x6C\x6F\x67\x75\x65\x73"), NULL);
	}
}
static void Dialog_t638F1505D57D2F294DDEA45174D9130410067CD7_CustomAttributesCacheGenerator_Dialog_DialogSequence_m7D77C870918DB2F259C90731C7EE7E6E4F788B9D(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CDialogSequenceU3Ed__25_t44374F6C645D70CD0D9FC61B194C18AF79DBA159_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CDialogSequenceU3Ed__25_t44374F6C645D70CD0D9FC61B194C18AF79DBA159_0_0_0_var), NULL);
	}
}
static void U3CDialogSequenceU3Ed__25_t44374F6C645D70CD0D9FC61B194C18AF79DBA159_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CDialogSequenceU3Ed__25_t44374F6C645D70CD0D9FC61B194C18AF79DBA159_CustomAttributesCacheGenerator_U3CDialogSequenceU3Ed__25__ctor_m0B8FC59C5611D1416FB254E739F74FAC0B7241BF(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CDialogSequenceU3Ed__25_t44374F6C645D70CD0D9FC61B194C18AF79DBA159_CustomAttributesCacheGenerator_U3CDialogSequenceU3Ed__25_System_IDisposable_Dispose_m43CAB6AFDA5A407257DCE2BB40AF5562F71833EA(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CDialogSequenceU3Ed__25_t44374F6C645D70CD0D9FC61B194C18AF79DBA159_CustomAttributesCacheGenerator_U3CDialogSequenceU3Ed__25_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mE00F000EBAFBDDFF5D2C8F42E742D77F3DD80A97(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CDialogSequenceU3Ed__25_t44374F6C645D70CD0D9FC61B194C18AF79DBA159_CustomAttributesCacheGenerator_U3CDialogSequenceU3Ed__25_System_Collections_IEnumerator_Reset_mD9737A7BD110755EF4E00DA6CB49C2B29F9D3B42(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CDialogSequenceU3Ed__25_t44374F6C645D70CD0D9FC61B194C18AF79DBA159_CustomAttributesCacheGenerator_U3CDialogSequenceU3Ed__25_System_Collections_IEnumerator_get_Current_m1EF267AEC7A80B67C2C484CABB5254A8CA16068A(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void GUIAudioFilter_tCC8DE531770AD69322920FE7BBCC9EC5B5EB2249_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		HelpURLAttribute_t0924A6D83FABA7B77780F7F9BBCBCB9E0EA15023 * tmp = (HelpURLAttribute_t0924A6D83FABA7B77780F7F9BBCBCB9E0EA15023 *)cache->attributes[0];
		HelpURLAttribute__ctor_mCC837CE7900738F3152D585D454A34A15793C215(tmp, il2cpp_codegen_string_new_wrapper("\x68\x74\x74\x70\x73\x3A\x2F\x2F\x77\x77\x77\x2E\x63\x72\x6F\x73\x73\x74\x61\x6C\x65\x73\x2E\x63\x6F\x6D\x2F\x6D\x65\x64\x69\x61\x2F\x64\x61\x74\x61\x2F\x61\x73\x73\x65\x74\x73\x2F\x72\x74\x76\x6F\x69\x63\x65\x2F\x61\x70\x69\x2F\x63\x6C\x61\x73\x73\x5F\x63\x72\x6F\x73\x73\x74\x61\x6C\x65\x73\x5F\x31\x5F\x31\x5F\x72\x5F\x74\x5F\x76\x6F\x69\x63\x65\x5F\x31\x5F\x31\x5F\x64\x65\x6D\x6F\x5F\x31\x5F\x31\x5F\x67\x5F\x75\x5F\x69\x5F\x61\x75\x64\x69\x6F\x5F\x66\x69\x6C\x74\x65\x72\x2E\x68\x74\x6D\x6C"), NULL);
	}
}
static void GUIAudioFilter_tCC8DE531770AD69322920FE7BBCC9EC5B5EB2249_CustomAttributesCacheGenerator_Source(CustomAttributesCache* cache)
{
	{
		HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB * tmp = (HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB *)cache->attributes[0];
		HeaderAttribute__ctor_m601319E0BCE8C44A9E79B2C0ABAAD0FEF46A9F1E(tmp, il2cpp_codegen_string_new_wrapper("\x41\x75\x64\x69\x6F\x20\x53\x6F\x75\x72\x63\x65"), NULL);
	}
}
static void GUIAudioFilter_tCC8DE531770AD69322920FE7BBCC9EC5B5EB2249_CustomAttributesCacheGenerator_ReverbFilter(CustomAttributesCache* cache)
{
	{
		HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB * tmp = (HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB *)cache->attributes[0];
		HeaderAttribute__ctor_m601319E0BCE8C44A9E79B2C0ABAAD0FEF46A9F1E(tmp, il2cpp_codegen_string_new_wrapper("\x46\x69\x6C\x74\x65\x72\x73"), NULL);
	}
}
static void GUIAudioFilter_tCC8DE531770AD69322920FE7BBCC9EC5B5EB2249_CustomAttributesCacheGenerator_Distortion(CustomAttributesCache* cache)
{
	{
		HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB * tmp = (HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB *)cache->attributes[0];
		HeaderAttribute__ctor_m601319E0BCE8C44A9E79B2C0ABAAD0FEF46A9F1E(tmp, il2cpp_codegen_string_new_wrapper("\x55\x49\x20\x4F\x62\x6A\x65\x63\x74\x73"), NULL);
	}
}
static void GUIDialog_tC739AB986043972A584C1DE7BE4EB16DD4E8464E_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		HelpURLAttribute_t0924A6D83FABA7B77780F7F9BBCBCB9E0EA15023 * tmp = (HelpURLAttribute_t0924A6D83FABA7B77780F7F9BBCBCB9E0EA15023 *)cache->attributes[0];
		HelpURLAttribute__ctor_mCC837CE7900738F3152D585D454A34A15793C215(tmp, il2cpp_codegen_string_new_wrapper("\x68\x74\x74\x70\x73\x3A\x2F\x2F\x77\x77\x77\x2E\x63\x72\x6F\x73\x73\x74\x61\x6C\x65\x73\x2E\x63\x6F\x6D\x2F\x6D\x65\x64\x69\x61\x2F\x64\x61\x74\x61\x2F\x61\x73\x73\x65\x74\x73\x2F\x72\x74\x76\x6F\x69\x63\x65\x2F\x61\x70\x69\x2F\x63\x6C\x61\x73\x73\x5F\x63\x72\x6F\x73\x73\x74\x61\x6C\x65\x73\x5F\x31\x5F\x31\x5F\x72\x5F\x74\x5F\x76\x6F\x69\x63\x65\x5F\x31\x5F\x31\x5F\x64\x65\x6D\x6F\x5F\x31\x5F\x31\x5F\x67\x5F\x75\x5F\x69\x5F\x64\x69\x61\x6C\x6F\x67\x2E\x68\x74\x6D\x6C"), NULL);
	}
}
static void GUIDialog_tC739AB986043972A584C1DE7BE4EB16DD4E8464E_CustomAttributesCacheGenerator_DialogScript(CustomAttributesCache* cache)
{
	{
		HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB * tmp = (HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB *)cache->attributes[0];
		HeaderAttribute__ctor_m601319E0BCE8C44A9E79B2C0ABAAD0FEF46A9F1E(tmp, il2cpp_codegen_string_new_wrapper("\x44\x69\x61\x6C\x6F\x67\x20\x53\x63\x72\x69\x70\x74"), NULL);
	}
}
static void GUIDialog_tC739AB986043972A584C1DE7BE4EB16DD4E8464E_CustomAttributesCacheGenerator_SpeakerColor(CustomAttributesCache* cache)
{
	{
		HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB * tmp = (HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB *)cache->attributes[0];
		HeaderAttribute__ctor_m601319E0BCE8C44A9E79B2C0ABAAD0FEF46A9F1E(tmp, il2cpp_codegen_string_new_wrapper("\x55\x49\x20\x4F\x62\x6A\x65\x63\x74\x73"), NULL);
	}
}
static void GUIMain_tD82B775FB46C7E2456748B5A944CBBFF713ECB61_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		HelpURLAttribute_t0924A6D83FABA7B77780F7F9BBCBCB9E0EA15023 * tmp = (HelpURLAttribute_t0924A6D83FABA7B77780F7F9BBCBCB9E0EA15023 *)cache->attributes[0];
		HelpURLAttribute__ctor_mCC837CE7900738F3152D585D454A34A15793C215(tmp, il2cpp_codegen_string_new_wrapper("\x68\x74\x74\x70\x73\x3A\x2F\x2F\x77\x77\x77\x2E\x63\x72\x6F\x73\x73\x74\x61\x6C\x65\x73\x2E\x63\x6F\x6D\x2F\x6D\x65\x64\x69\x61\x2F\x64\x61\x74\x61\x2F\x61\x73\x73\x65\x74\x73\x2F\x72\x74\x76\x6F\x69\x63\x65\x2F\x61\x70\x69\x2F\x63\x6C\x61\x73\x73\x5F\x63\x72\x6F\x73\x73\x74\x61\x6C\x65\x73\x5F\x31\x5F\x31\x5F\x72\x5F\x74\x5F\x76\x6F\x69\x63\x65\x5F\x31\x5F\x31\x5F\x64\x65\x6D\x6F\x5F\x31\x5F\x31\x5F\x67\x5F\x75\x5F\x69\x5F\x6D\x61\x69\x6E\x2E\x68\x74\x6D\x6C"), NULL);
	}
}
static void GUIMain_tD82B775FB46C7E2456748B5A944CBBFF713ECB61_CustomAttributesCacheGenerator_Name(CustomAttributesCache* cache)
{
	{
		HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB * tmp = (HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB *)cache->attributes[0];
		HeaderAttribute__ctor_m601319E0BCE8C44A9E79B2C0ABAAD0FEF46A9F1E(tmp, il2cpp_codegen_string_new_wrapper("\x55\x49\x20\x4F\x62\x6A\x65\x63\x74\x73"), NULL);
	}
}
static void GUIMultiAudioFilter_t9FDD491C7C801C47D7872FE2AD05D6CAC2A3557F_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		HelpURLAttribute_t0924A6D83FABA7B77780F7F9BBCBCB9E0EA15023 * tmp = (HelpURLAttribute_t0924A6D83FABA7B77780F7F9BBCBCB9E0EA15023 *)cache->attributes[0];
		HelpURLAttribute__ctor_mCC837CE7900738F3152D585D454A34A15793C215(tmp, il2cpp_codegen_string_new_wrapper("\x68\x74\x74\x70\x73\x3A\x2F\x2F\x77\x77\x77\x2E\x63\x72\x6F\x73\x73\x74\x61\x6C\x65\x73\x2E\x63\x6F\x6D\x2F\x6D\x65\x64\x69\x61\x2F\x64\x61\x74\x61\x2F\x61\x73\x73\x65\x74\x73\x2F\x72\x74\x76\x6F\x69\x63\x65\x2F\x61\x70\x69\x2F\x63\x6C\x61\x73\x73\x5F\x63\x72\x6F\x73\x73\x74\x61\x6C\x65\x73\x5F\x31\x5F\x31\x5F\x72\x5F\x74\x5F\x76\x6F\x69\x63\x65\x5F\x31\x5F\x31\x5F\x64\x65\x6D\x6F\x5F\x31\x5F\x31\x5F\x67\x5F\x75\x5F\x69\x5F\x6D\x75\x6C\x74\x69\x5F\x61\x75\x64\x69\x6F\x5F\x66\x69\x6C\x74\x65\x72\x2E\x68\x74\x6D\x6C"), NULL);
	}
}
static void GUIMultiAudioFilter_t9FDD491C7C801C47D7872FE2AD05D6CAC2A3557F_CustomAttributesCacheGenerator_Sources(CustomAttributesCache* cache)
{
	{
		HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB * tmp = (HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB *)cache->attributes[0];
		HeaderAttribute__ctor_m601319E0BCE8C44A9E79B2C0ABAAD0FEF46A9F1E(tmp, il2cpp_codegen_string_new_wrapper("\x41\x75\x64\x69\x6F\x20\x53\x6F\x75\x72\x63\x65\x73"), NULL);
	}
}
static void GUIMultiAudioFilter_t9FDD491C7C801C47D7872FE2AD05D6CAC2A3557F_CustomAttributesCacheGenerator_ReverbFilters(CustomAttributesCache* cache)
{
	{
		HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB * tmp = (HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB *)cache->attributes[0];
		HeaderAttribute__ctor_m601319E0BCE8C44A9E79B2C0ABAAD0FEF46A9F1E(tmp, il2cpp_codegen_string_new_wrapper("\x46\x69\x6C\x74\x65\x72\x73"), NULL);
	}
}
static void GUIMultiAudioFilter_t9FDD491C7C801C47D7872FE2AD05D6CAC2A3557F_CustomAttributesCacheGenerator_Distortion(CustomAttributesCache* cache)
{
	{
		HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB * tmp = (HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB *)cache->attributes[0];
		HeaderAttribute__ctor_m601319E0BCE8C44A9E79B2C0ABAAD0FEF46A9F1E(tmp, il2cpp_codegen_string_new_wrapper("\x55\x49\x20\x4F\x62\x6A\x65\x63\x74\x73"), NULL);
	}
}
static void GUIScenes_t6F72249B673EF03AF769EE6AC0EC241AA0F8E6CB_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		HelpURLAttribute_t0924A6D83FABA7B77780F7F9BBCBCB9E0EA15023 * tmp = (HelpURLAttribute_t0924A6D83FABA7B77780F7F9BBCBCB9E0EA15023 *)cache->attributes[0];
		HelpURLAttribute__ctor_mCC837CE7900738F3152D585D454A34A15793C215(tmp, il2cpp_codegen_string_new_wrapper("\x68\x74\x74\x70\x73\x3A\x2F\x2F\x77\x77\x77\x2E\x63\x72\x6F\x73\x73\x74\x61\x6C\x65\x73\x2E\x63\x6F\x6D\x2F\x6D\x65\x64\x69\x61\x2F\x64\x61\x74\x61\x2F\x61\x73\x73\x65\x74\x73\x2F\x72\x74\x76\x6F\x69\x63\x65\x2F\x61\x70\x69\x2F\x63\x6C\x61\x73\x73\x5F\x63\x72\x6F\x73\x73\x74\x61\x6C\x65\x73\x5F\x31\x5F\x31\x5F\x72\x5F\x74\x5F\x76\x6F\x69\x63\x65\x5F\x31\x5F\x31\x5F\x64\x65\x6D\x6F\x5F\x31\x5F\x31\x5F\x67\x5F\x75\x5F\x69\x5F\x73\x63\x65\x6E\x65\x73\x2E\x68\x74\x6D\x6C"), NULL);
	}
}
static void GUIScenes_t6F72249B673EF03AF769EE6AC0EC241AA0F8E6CB_CustomAttributesCacheGenerator_PreviousScene(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x4E\x61\x6D\x65\x20\x6F\x66\x20\x74\x68\x65\x20\x70\x72\x65\x76\x69\x6F\x75\x73\x20\x73\x63\x65\x6E\x65\x2E"), NULL);
	}
}
static void GUIScenes_t6F72249B673EF03AF769EE6AC0EC241AA0F8E6CB_CustomAttributesCacheGenerator_PreviousSceneWebGL(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x4E\x61\x6D\x65\x20\x6F\x66\x20\x74\x68\x65\x20\x70\x72\x65\x76\x69\x6F\x75\x73\x20\x73\x63\x65\x6E\x65\x20\x28\x57\x65\x62\x47\x4C\x20\x6F\x6E\x6C\x79\x29\x2E"), NULL);
	}
}
static void GUIScenes_t6F72249B673EF03AF769EE6AC0EC241AA0F8E6CB_CustomAttributesCacheGenerator_NextScene(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x4E\x61\x6D\x65\x20\x6F\x66\x20\x74\x68\x65\x20\x6E\x65\x78\x74\x20\x73\x63\x65\x6E\x65\x2E"), NULL);
	}
}
static void GUIScenes_t6F72249B673EF03AF769EE6AC0EC241AA0F8E6CB_CustomAttributesCacheGenerator_NextSceneWebGL(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x4E\x61\x6D\x65\x20\x6F\x66\x20\x74\x68\x65\x20\x6E\x65\x78\x74\x20\x73\x63\x65\x6E\x65\x20\x28\x57\x65\x62\x47\x4C\x20\x6F\x6E\x6C\x79\x29\x2E"), NULL);
	}
}
static void GUISpeech_t415B11E6596BA1EFF7191AD10A721FEF21BF97A0_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		HelpURLAttribute_t0924A6D83FABA7B77780F7F9BBCBCB9E0EA15023 * tmp = (HelpURLAttribute_t0924A6D83FABA7B77780F7F9BBCBCB9E0EA15023 *)cache->attributes[0];
		HelpURLAttribute__ctor_mCC837CE7900738F3152D585D454A34A15793C215(tmp, il2cpp_codegen_string_new_wrapper("\x68\x74\x74\x70\x73\x3A\x2F\x2F\x77\x77\x77\x2E\x63\x72\x6F\x73\x73\x74\x61\x6C\x65\x73\x2E\x63\x6F\x6D\x2F\x6D\x65\x64\x69\x61\x2F\x64\x61\x74\x61\x2F\x61\x73\x73\x65\x74\x73\x2F\x72\x74\x76\x6F\x69\x63\x65\x2F\x61\x70\x69\x2F\x63\x6C\x61\x73\x73\x5F\x63\x72\x6F\x73\x73\x74\x61\x6C\x65\x73\x5F\x31\x5F\x31\x5F\x72\x5F\x74\x5F\x76\x6F\x69\x63\x65\x5F\x31\x5F\x31\x5F\x64\x65\x6D\x6F\x5F\x31\x5F\x31\x5F\x67\x5F\x75\x5F\x69\x5F\x73\x70\x65\x65\x63\x68\x2E\x68\x74\x6D\x6C"), NULL);
	}
}
static void GUISpeech_t415B11E6596BA1EFF7191AD10A721FEF21BF97A0_CustomAttributesCacheGenerator_StartAsNative(CustomAttributesCache* cache)
{
	{
		HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB * tmp = (HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB *)cache->attributes[0];
		HeaderAttribute__ctor_m601319E0BCE8C44A9E79B2C0ABAAD0FEF46A9F1E(tmp, il2cpp_codegen_string_new_wrapper("\x53\x65\x74\x74\x69\x6E\x67\x73"), NULL);
	}
}
static void GUISpeech_t415B11E6596BA1EFF7191AD10A721FEF21BF97A0_CustomAttributesCacheGenerator_ItemPrefab(CustomAttributesCache* cache)
{
	{
		HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB * tmp = (HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB *)cache->attributes[0];
		HeaderAttribute__ctor_m601319E0BCE8C44A9E79B2C0ABAAD0FEF46A9F1E(tmp, il2cpp_codegen_string_new_wrapper("\x54\x61\x62\x6C\x65"), NULL);
	}
}
static void GUISpeech_t415B11E6596BA1EFF7191AD10A721FEF21BF97A0_CustomAttributesCacheGenerator_Input(CustomAttributesCache* cache)
{
	{
		HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB * tmp = (HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB *)cache->attributes[0];
		HeaderAttribute__ctor_m601319E0BCE8C44A9E79B2C0ABAAD0FEF46A9F1E(tmp, il2cpp_codegen_string_new_wrapper("\x55\x49\x20\x4F\x62\x6A\x65\x63\x74\x73"), NULL);
	}
}
static void NativeAudio_tC4F90593188637DF55A20E05E1764FFE6833715E_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		HelpURLAttribute_t0924A6D83FABA7B77780F7F9BBCBCB9E0EA15023 * tmp = (HelpURLAttribute_t0924A6D83FABA7B77780F7F9BBCBCB9E0EA15023 *)cache->attributes[0];
		HelpURLAttribute__ctor_mCC837CE7900738F3152D585D454A34A15793C215(tmp, il2cpp_codegen_string_new_wrapper("\x68\x74\x74\x70\x73\x3A\x2F\x2F\x77\x77\x77\x2E\x63\x72\x6F\x73\x73\x74\x61\x6C\x65\x73\x2E\x63\x6F\x6D\x2F\x6D\x65\x64\x69\x61\x2F\x64\x61\x74\x61\x2F\x61\x73\x73\x65\x74\x73\x2F\x72\x74\x76\x6F\x69\x63\x65\x2F\x61\x70\x69\x2F\x63\x6C\x61\x73\x73\x5F\x63\x72\x6F\x73\x73\x74\x61\x6C\x65\x73\x5F\x31\x5F\x31\x5F\x72\x5F\x74\x5F\x76\x6F\x69\x63\x65\x5F\x31\x5F\x31\x5F\x64\x65\x6D\x6F\x5F\x31\x5F\x31\x5F\x6E\x61\x74\x69\x76\x65\x5F\x61\x75\x64\x69\x6F\x2E\x68\x74\x6D\x6C"), NULL);
	}
}
static void PreGeneratedAudio_t042FA862F81AC8AC2E868CFF437A88BD6AFCD884_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		HelpURLAttribute_t0924A6D83FABA7B77780F7F9BBCBCB9E0EA15023 * tmp = (HelpURLAttribute_t0924A6D83FABA7B77780F7F9BBCBCB9E0EA15023 *)cache->attributes[0];
		HelpURLAttribute__ctor_mCC837CE7900738F3152D585D454A34A15793C215(tmp, il2cpp_codegen_string_new_wrapper("\x68\x74\x74\x70\x73\x3A\x2F\x2F\x77\x77\x77\x2E\x63\x72\x6F\x73\x73\x74\x61\x6C\x65\x73\x2E\x63\x6F\x6D\x2F\x6D\x65\x64\x69\x61\x2F\x64\x61\x74\x61\x2F\x61\x73\x73\x65\x74\x73\x2F\x72\x74\x76\x6F\x69\x63\x65\x2F\x61\x70\x69\x2F\x63\x6C\x61\x73\x73\x5F\x63\x72\x6F\x73\x73\x74\x61\x6C\x65\x73\x5F\x31\x5F\x31\x5F\x72\x5F\x74\x5F\x76\x6F\x69\x63\x65\x5F\x31\x5F\x31\x5F\x64\x65\x6D\x6F\x5F\x31\x5F\x31\x5F\x70\x72\x65\x5F\x67\x65\x6E\x65\x72\x61\x74\x65\x64\x5F\x61\x75\x64\x69\x6F\x2E\x68\x74\x6D\x6C"), NULL);
	}
}
static void SendMessage_t9D6A7D158B11451BF3D7DCC357330A2F4363BB94_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		HelpURLAttribute_t0924A6D83FABA7B77780F7F9BBCBCB9E0EA15023 * tmp = (HelpURLAttribute_t0924A6D83FABA7B77780F7F9BBCBCB9E0EA15023 *)cache->attributes[0];
		HelpURLAttribute__ctor_mCC837CE7900738F3152D585D454A34A15793C215(tmp, il2cpp_codegen_string_new_wrapper("\x68\x74\x74\x70\x73\x3A\x2F\x2F\x77\x77\x77\x2E\x63\x72\x6F\x73\x73\x74\x61\x6C\x65\x73\x2E\x63\x6F\x6D\x2F\x6D\x65\x64\x69\x61\x2F\x64\x61\x74\x61\x2F\x61\x73\x73\x65\x74\x73\x2F\x72\x74\x76\x6F\x69\x63\x65\x2F\x61\x70\x69\x2F\x63\x6C\x61\x73\x73\x5F\x63\x72\x6F\x73\x73\x74\x61\x6C\x65\x73\x5F\x31\x5F\x31\x5F\x72\x5F\x74\x5F\x76\x6F\x69\x63\x65\x5F\x31\x5F\x31\x5F\x64\x65\x6D\x6F\x5F\x31\x5F\x31\x5F\x73\x65\x6E\x64\x5F\x6D\x65\x73\x73\x61\x67\x65\x2E\x68\x74\x6D\x6C"), NULL);
	}
}
static void SendMessage_t9D6A7D158B11451BF3D7DCC357330A2F4363BB94_CustomAttributesCacheGenerator_TextA(CustomAttributesCache* cache)
{
	{
		MultilineAttribute_tA4DCA9D8519E0C1B05636F2EFF74F81335A7F291 * tmp = (MultilineAttribute_tA4DCA9D8519E0C1B05636F2EFF74F81335A7F291 *)cache->attributes[0];
		MultilineAttribute__ctor_m353DC377C95D4C341F5DDD0F9894878F64E5703E(tmp, NULL);
	}
}
static void SendMessage_t9D6A7D158B11451BF3D7DCC357330A2F4363BB94_CustomAttributesCacheGenerator_TextB(CustomAttributesCache* cache)
{
	{
		MultilineAttribute_tA4DCA9D8519E0C1B05636F2EFF74F81335A7F291 * tmp = (MultilineAttribute_tA4DCA9D8519E0C1B05636F2EFF74F81335A7F291 *)cache->attributes[0];
		MultilineAttribute__ctor_m353DC377C95D4C341F5DDD0F9894878F64E5703E(tmp, NULL);
	}
}
static void SendMessage_t9D6A7D158B11451BF3D7DCC357330A2F4363BB94_CustomAttributesCacheGenerator_SendMessage_SpeakerB_m6C538E9CD2CDABCC1F85DC7514AB9ABAC7523F90(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CSpeakerBU3Ed__8_t4FDA50AFC81C4F929F34CFAB06B4B0B6AF635A06_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CSpeakerBU3Ed__8_t4FDA50AFC81C4F929F34CFAB06B4B0B6AF635A06_0_0_0_var), NULL);
	}
}
static void U3CSpeakerBU3Ed__8_t4FDA50AFC81C4F929F34CFAB06B4B0B6AF635A06_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CSpeakerBU3Ed__8_t4FDA50AFC81C4F929F34CFAB06B4B0B6AF635A06_CustomAttributesCacheGenerator_U3CSpeakerBU3Ed__8__ctor_m4A8ED00A523A95298E776A4A523FF2A04F3D6724(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CSpeakerBU3Ed__8_t4FDA50AFC81C4F929F34CFAB06B4B0B6AF635A06_CustomAttributesCacheGenerator_U3CSpeakerBU3Ed__8_System_IDisposable_Dispose_mF7B8070E854813D83A83738E3A5AFF4A85A58C2F(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CSpeakerBU3Ed__8_t4FDA50AFC81C4F929F34CFAB06B4B0B6AF635A06_CustomAttributesCacheGenerator_U3CSpeakerBU3Ed__8_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m410D072C0C20CC5489B8A8A4782DF66577B670DD(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CSpeakerBU3Ed__8_t4FDA50AFC81C4F929F34CFAB06B4B0B6AF635A06_CustomAttributesCacheGenerator_U3CSpeakerBU3Ed__8_System_Collections_IEnumerator_Reset_m51FC493E4D2F9C532B5C80D086A39099F32302ED(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CSpeakerBU3Ed__8_t4FDA50AFC81C4F929F34CFAB06B4B0B6AF635A06_CustomAttributesCacheGenerator_U3CSpeakerBU3Ed__8_System_Collections_IEnumerator_get_Current_mE1A5CEC7B178D65FF55AC03932D40A96DA8697A6(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void SequenceCaller_t443E4818044C7EA60F108C026B31AF65E6CD0F3D_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		HelpURLAttribute_t0924A6D83FABA7B77780F7F9BBCBCB9E0EA15023 * tmp = (HelpURLAttribute_t0924A6D83FABA7B77780F7F9BBCBCB9E0EA15023 *)cache->attributes[0];
		HelpURLAttribute__ctor_mCC837CE7900738F3152D585D454A34A15793C215(tmp, il2cpp_codegen_string_new_wrapper("\x68\x74\x74\x70\x73\x3A\x2F\x2F\x77\x77\x77\x2E\x63\x72\x6F\x73\x73\x74\x61\x6C\x65\x73\x2E\x63\x6F\x6D\x2F\x6D\x65\x64\x69\x61\x2F\x64\x61\x74\x61\x2F\x61\x73\x73\x65\x74\x73\x2F\x72\x74\x76\x6F\x69\x63\x65\x2F\x61\x70\x69\x2F\x63\x6C\x61\x73\x73\x5F\x63\x72\x6F\x73\x73\x74\x61\x6C\x65\x73\x5F\x31\x5F\x31\x5F\x72\x5F\x74\x5F\x76\x6F\x69\x63\x65\x5F\x31\x5F\x31\x5F\x64\x65\x6D\x6F\x5F\x31\x5F\x31\x5F\x73\x65\x71\x75\x65\x6E\x63\x65\x5F\x63\x61\x6C\x6C\x65\x72\x2E\x68\x74\x6D\x6C"), NULL);
	}
}
static void Simple_t557AF9B6F3B5301F929702090D5291F7DBAA5490_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		HelpURLAttribute_t0924A6D83FABA7B77780F7F9BBCBCB9E0EA15023 * tmp = (HelpURLAttribute_t0924A6D83FABA7B77780F7F9BBCBCB9E0EA15023 *)cache->attributes[0];
		HelpURLAttribute__ctor_mCC837CE7900738F3152D585D454A34A15793C215(tmp, il2cpp_codegen_string_new_wrapper("\x68\x74\x74\x70\x73\x3A\x2F\x2F\x77\x77\x77\x2E\x63\x72\x6F\x73\x73\x74\x61\x6C\x65\x73\x2E\x63\x6F\x6D\x2F\x6D\x65\x64\x69\x61\x2F\x64\x61\x74\x61\x2F\x61\x73\x73\x65\x74\x73\x2F\x72\x74\x76\x6F\x69\x63\x65\x2F\x61\x70\x69\x2F\x63\x6C\x61\x73\x73\x5F\x63\x72\x6F\x73\x73\x74\x61\x6C\x65\x73\x5F\x31\x5F\x31\x5F\x72\x5F\x74\x5F\x76\x6F\x69\x63\x65\x5F\x31\x5F\x31\x5F\x64\x65\x6D\x6F\x5F\x31\x5F\x31\x5F\x73\x69\x6D\x70\x6C\x65\x2E\x68\x74\x6D\x6C"), NULL);
	}
}
static void Simple_t557AF9B6F3B5301F929702090D5291F7DBAA5490_CustomAttributesCacheGenerator_SourceA(CustomAttributesCache* cache)
{
	{
		HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB * tmp = (HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB *)cache->attributes[0];
		HeaderAttribute__ctor_m601319E0BCE8C44A9E79B2C0ABAAD0FEF46A9F1E(tmp, il2cpp_codegen_string_new_wrapper("\x43\x6F\x6E\x66\x69\x67\x75\x72\x61\x74\x69\x6F\x6E"), NULL);
	}
}
static void Simple_t557AF9B6F3B5301F929702090D5291F7DBAA5490_CustomAttributesCacheGenerator_RateSpeakerA(CustomAttributesCache* cache)
{
	{
		RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5 * tmp = (RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5 *)cache->attributes[0];
		RangeAttribute__ctor_mC74D39A9F20DD2A0D4174F05785ABE4F0DAEF000(tmp, 0.0f, 3.0f, NULL);
	}
}
static void Simple_t557AF9B6F3B5301F929702090D5291F7DBAA5490_CustomAttributesCacheGenerator_RateSpeakerB(CustomAttributesCache* cache)
{
	{
		RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5 * tmp = (RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5 *)cache->attributes[0];
		RangeAttribute__ctor_mC74D39A9F20DD2A0D4174F05785ABE4F0DAEF000(tmp, 0.0f, 3.0f, NULL);
	}
}
static void Simple_t557AF9B6F3B5301F929702090D5291F7DBAA5490_CustomAttributesCacheGenerator_TextSpeakerA(CustomAttributesCache* cache)
{
	{
		HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB * tmp = (HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB *)cache->attributes[0];
		HeaderAttribute__ctor_m601319E0BCE8C44A9E79B2C0ABAAD0FEF46A9F1E(tmp, il2cpp_codegen_string_new_wrapper("\x55\x49\x20\x4F\x62\x6A\x65\x63\x74\x73"), NULL);
	}
}
static void SimpleNative_t00B45673BD9201F12CD43BDD847E5C69489CD131_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		HelpURLAttribute_t0924A6D83FABA7B77780F7F9BBCBCB9E0EA15023 * tmp = (HelpURLAttribute_t0924A6D83FABA7B77780F7F9BBCBCB9E0EA15023 *)cache->attributes[0];
		HelpURLAttribute__ctor_mCC837CE7900738F3152D585D454A34A15793C215(tmp, il2cpp_codegen_string_new_wrapper("\x68\x74\x74\x70\x73\x3A\x2F\x2F\x77\x77\x77\x2E\x63\x72\x6F\x73\x73\x74\x61\x6C\x65\x73\x2E\x63\x6F\x6D\x2F\x6D\x65\x64\x69\x61\x2F\x64\x61\x74\x61\x2F\x61\x73\x73\x65\x74\x73\x2F\x72\x74\x76\x6F\x69\x63\x65\x2F\x61\x70\x69\x2F\x63\x6C\x61\x73\x73\x5F\x63\x72\x6F\x73\x73\x74\x61\x6C\x65\x73\x5F\x31\x5F\x31\x5F\x72\x5F\x74\x5F\x76\x6F\x69\x63\x65\x5F\x31\x5F\x31\x5F\x64\x65\x6D\x6F\x5F\x31\x5F\x31\x5F\x73\x69\x6D\x70\x6C\x65\x5F\x6E\x61\x74\x69\x76\x65\x2E\x68\x74\x6D\x6C"), NULL);
	}
}
static void SimpleNative_t00B45673BD9201F12CD43BDD847E5C69489CD131_CustomAttributesCacheGenerator_RateSpeakerA(CustomAttributesCache* cache)
{
	{
		HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB * tmp = (HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB *)cache->attributes[0];
		HeaderAttribute__ctor_m601319E0BCE8C44A9E79B2C0ABAAD0FEF46A9F1E(tmp, il2cpp_codegen_string_new_wrapper("\x43\x6F\x6E\x66\x69\x67\x75\x72\x61\x74\x69\x6F\x6E"), NULL);
	}
	{
		RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5 * tmp = (RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5 *)cache->attributes[1];
		RangeAttribute__ctor_mC74D39A9F20DD2A0D4174F05785ABE4F0DAEF000(tmp, 0.0f, 3.0f, NULL);
	}
}
static void SimpleNative_t00B45673BD9201F12CD43BDD847E5C69489CD131_CustomAttributesCacheGenerator_RateSpeakerB(CustomAttributesCache* cache)
{
	{
		RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5 * tmp = (RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5 *)cache->attributes[0];
		RangeAttribute__ctor_mC74D39A9F20DD2A0D4174F05785ABE4F0DAEF000(tmp, 0.0f, 3.0f, NULL);
	}
}
static void SimpleNative_t00B45673BD9201F12CD43BDD847E5C69489CD131_CustomAttributesCacheGenerator_RateSpeakerC(CustomAttributesCache* cache)
{
	{
		RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5 * tmp = (RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5 *)cache->attributes[0];
		RangeAttribute__ctor_mC74D39A9F20DD2A0D4174F05785ABE4F0DAEF000(tmp, 0.0f, 3.0f, NULL);
	}
}
static void SimpleNative_t00B45673BD9201F12CD43BDD847E5C69489CD131_CustomAttributesCacheGenerator_TextSpeakerA(CustomAttributesCache* cache)
{
	{
		HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB * tmp = (HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB *)cache->attributes[0];
		HeaderAttribute__ctor_m601319E0BCE8C44A9E79B2C0ABAAD0FEF46A9F1E(tmp, il2cpp_codegen_string_new_wrapper("\x55\x49\x20\x4F\x62\x6A\x65\x63\x74\x73"), NULL);
	}
}
static void SpeakWrapper_t46A78C1BDD6F731870403517E01C60528927C0D4_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&AudioSource_tC4BF65AF8CDCAA63724BB3CA59A7A29249269E6B_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		RequireComponent_tEDA546F9722B8874DA9658BDAB821BA49647FC91 * tmp = (RequireComponent_tEDA546F9722B8874DA9658BDAB821BA49647FC91 *)cache->attributes[0];
		RequireComponent__ctor_m5EC89D3D22D7D880E1B88A5C9FADF1FBDC713EE4(tmp, il2cpp_codegen_type_get_object(AudioSource_tC4BF65AF8CDCAA63724BB3CA59A7A29249269E6B_0_0_0_var), NULL);
	}
	{
		HelpURLAttribute_t0924A6D83FABA7B77780F7F9BBCBCB9E0EA15023 * tmp = (HelpURLAttribute_t0924A6D83FABA7B77780F7F9BBCBCB9E0EA15023 *)cache->attributes[1];
		HelpURLAttribute__ctor_mCC837CE7900738F3152D585D454A34A15793C215(tmp, il2cpp_codegen_string_new_wrapper("\x68\x74\x74\x70\x73\x3A\x2F\x2F\x77\x77\x77\x2E\x63\x72\x6F\x73\x73\x74\x61\x6C\x65\x73\x2E\x63\x6F\x6D\x2F\x6D\x65\x64\x69\x61\x2F\x64\x61\x74\x61\x2F\x61\x73\x73\x65\x74\x73\x2F\x72\x74\x76\x6F\x69\x63\x65\x2F\x61\x70\x69\x2F\x63\x6C\x61\x73\x73\x5F\x63\x72\x6F\x73\x73\x74\x61\x6C\x65\x73\x5F\x31\x5F\x31\x5F\x72\x5F\x74\x5F\x76\x6F\x69\x63\x65\x5F\x31\x5F\x31\x5F\x64\x65\x6D\x6F\x5F\x31\x5F\x31\x5F\x73\x70\x65\x61\x6B\x5F\x77\x72\x61\x70\x70\x65\x72\x2E\x68\x74\x6D\x6C"), NULL);
	}
}
static void MaterialChanger_tA6610F95A3FA18CC626F076A94014B8356AF88B4_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Renderer_t58147AB5B00224FE1460FD47542DC0DA7EC9378C_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		RequireComponent_tEDA546F9722B8874DA9658BDAB821BA49647FC91 * tmp = (RequireComponent_tEDA546F9722B8874DA9658BDAB821BA49647FC91 *)cache->attributes[0];
		RequireComponent__ctor_m5EC89D3D22D7D880E1B88A5C9FADF1FBDC713EE4(tmp, il2cpp_codegen_type_get_object(Renderer_t58147AB5B00224FE1460FD47542DC0DA7EC9378C_0_0_0_var), NULL);
	}
	{
		HelpURLAttribute_t0924A6D83FABA7B77780F7F9BBCBCB9E0EA15023 * tmp = (HelpURLAttribute_t0924A6D83FABA7B77780F7F9BBCBCB9E0EA15023 *)cache->attributes[1];
		HelpURLAttribute__ctor_mCC837CE7900738F3152D585D454A34A15793C215(tmp, il2cpp_codegen_string_new_wrapper("\x68\x74\x74\x70\x73\x3A\x2F\x2F\x77\x77\x77\x2E\x63\x72\x6F\x73\x73\x74\x61\x6C\x65\x73\x2E\x63\x6F\x6D\x2F\x6D\x65\x64\x69\x61\x2F\x64\x61\x74\x61\x2F\x61\x73\x73\x65\x74\x73\x2F\x72\x74\x76\x6F\x69\x63\x65\x2F\x61\x70\x69\x2F\x63\x6C\x61\x73\x73\x5F\x63\x72\x6F\x73\x73\x74\x61\x6C\x65\x73\x5F\x31\x5F\x31\x5F\x72\x5F\x74\x5F\x76\x6F\x69\x63\x65\x5F\x31\x5F\x31\x5F\x64\x65\x6D\x6F\x5F\x31\x5F\x31\x5F\x75\x74\x69\x6C\x5F\x31\x5F\x31\x5F\x6D\x61\x74\x65\x72\x69\x61\x6C\x5F\x63\x68\x61\x6E\x67\x65\x72\x2E\x68\x74\x6D\x6C"), NULL);
	}
}
static void NativeController_t75220F3EAB005E617337B7EBD5673FE5E94A4B26_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		HelpURLAttribute_t0924A6D83FABA7B77780F7F9BBCBCB9E0EA15023 * tmp = (HelpURLAttribute_t0924A6D83FABA7B77780F7F9BBCBCB9E0EA15023 *)cache->attributes[0];
		HelpURLAttribute__ctor_mCC837CE7900738F3152D585D454A34A15793C215(tmp, il2cpp_codegen_string_new_wrapper("\x68\x74\x74\x70\x73\x3A\x2F\x2F\x77\x77\x77\x2E\x63\x72\x6F\x73\x73\x74\x61\x6C\x65\x73\x2E\x63\x6F\x6D\x2F\x6D\x65\x64\x69\x61\x2F\x64\x61\x74\x61\x2F\x61\x73\x73\x65\x74\x73\x2F\x72\x74\x76\x6F\x69\x63\x65\x2F\x61\x70\x69\x2F\x63\x6C\x61\x73\x73\x5F\x63\x72\x6F\x73\x73\x74\x61\x6C\x65\x73\x5F\x31\x5F\x31\x5F\x72\x5F\x74\x5F\x76\x6F\x69\x63\x65\x5F\x31\x5F\x31\x5F\x64\x65\x6D\x6F\x5F\x31\x5F\x31\x5F\x75\x74\x69\x6C\x5F\x31\x5F\x31\x5F\x6E\x61\x74\x69\x76\x65\x5F\x63\x6F\x6E\x74\x72\x6F\x6C\x6C\x65\x72\x2E\x68\x74\x6D\x6C"), NULL);
	}
}
static void NativeController_t75220F3EAB005E617337B7EBD5673FE5E94A4B26_CustomAttributesCacheGenerator_Active(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x45\x6E\x61\x62\x6C\x65\x20\x6F\x72\x20\x64\x69\x73\x61\x62\x6C\x65\x20\x74\x68\x65\x20\x27\x4F\x62\x6A\x65\x63\x74\x73\x27\x20\x66\x6F\x72\x20\x6E\x61\x74\x69\x76\x65\x20\x6D\x6F\x64\x65\x20\x28\x64\x65\x66\x61\x75\x6C\x74\x3A\x20\x74\x72\x75\x65\x29\x2E"), NULL);
	}
	{
		HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB * tmp = (HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB *)cache->attributes[1];
		HeaderAttribute__ctor_m601319E0BCE8C44A9E79B2C0ABAAD0FEF46A9F1E(tmp, il2cpp_codegen_string_new_wrapper("\x43\x6F\x6E\x66\x69\x67\x75\x72\x61\x74\x69\x6F\x6E"), NULL);
	}
}
static void NativeController_t75220F3EAB005E617337B7EBD5673FE5E94A4B26_CustomAttributesCacheGenerator_Objects(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x53\x65\x6C\x65\x63\x74\x65\x64\x20\x6F\x62\x6A\x65\x63\x74\x73\x20\x66\x6F\x72\x20\x74\x68\x65\x20\x63\x6F\x6E\x74\x72\x6F\x6C\x6C\x65\x72\x2E"), NULL);
	}
	{
		HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB * tmp = (HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB *)cache->attributes[1];
		HeaderAttribute__ctor_m601319E0BCE8C44A9E79B2C0ABAAD0FEF46A9F1E(tmp, il2cpp_codegen_string_new_wrapper("\x4F\x62\x6A\x65\x63\x74\x73"), NULL);
	}
}
static void PlatformController_t92E180723A1895FB7F7B46B4279A987A82ECC318_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		HelpURLAttribute_t0924A6D83FABA7B77780F7F9BBCBCB9E0EA15023 * tmp = (HelpURLAttribute_t0924A6D83FABA7B77780F7F9BBCBCB9E0EA15023 *)cache->attributes[0];
		HelpURLAttribute__ctor_mCC837CE7900738F3152D585D454A34A15793C215(tmp, il2cpp_codegen_string_new_wrapper("\x68\x74\x74\x70\x73\x3A\x2F\x2F\x77\x77\x77\x2E\x63\x72\x6F\x73\x73\x74\x61\x6C\x65\x73\x2E\x63\x6F\x6D\x2F\x6D\x65\x64\x69\x61\x2F\x64\x61\x74\x61\x2F\x61\x73\x73\x65\x74\x73\x2F\x72\x74\x76\x6F\x69\x63\x65\x2F\x61\x70\x69\x2F\x63\x6C\x61\x73\x73\x5F\x63\x72\x6F\x73\x73\x74\x61\x6C\x65\x73\x5F\x31\x5F\x31\x5F\x72\x5F\x74\x5F\x76\x6F\x69\x63\x65\x5F\x31\x5F\x31\x5F\x64\x65\x6D\x6F\x5F\x31\x5F\x31\x5F\x75\x74\x69\x6C\x5F\x31\x5F\x31\x5F\x70\x6C\x61\x74\x66\x6F\x72\x6D\x5F\x63\x6F\x6E\x74\x72\x6F\x6C\x6C\x65\x72\x2E\x68\x74\x6D\x6C"), NULL);
	}
}
static void iOSController_t7A78C41A4782DEDF08E61AE01F470467347F6879_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		HelpURLAttribute_t0924A6D83FABA7B77780F7F9BBCBCB9E0EA15023 * tmp = (HelpURLAttribute_t0924A6D83FABA7B77780F7F9BBCBCB9E0EA15023 *)cache->attributes[0];
		HelpURLAttribute__ctor_mCC837CE7900738F3152D585D454A34A15793C215(tmp, il2cpp_codegen_string_new_wrapper("\x68\x74\x74\x70\x73\x3A\x2F\x2F\x77\x77\x77\x2E\x63\x72\x6F\x73\x73\x74\x61\x6C\x65\x73\x2E\x63\x6F\x6D\x2F\x6D\x65\x64\x69\x61\x2F\x64\x61\x74\x61\x2F\x61\x73\x73\x65\x74\x73\x2F\x72\x74\x76\x6F\x69\x63\x65\x2F\x61\x70\x69\x2F\x63\x6C\x61\x73\x73\x5F\x63\x72\x6F\x73\x73\x74\x61\x6C\x65\x73\x5F\x31\x5F\x31\x5F\x72\x5F\x74\x5F\x76\x6F\x69\x63\x65\x5F\x31\x5F\x31\x5F\x64\x65\x6D\x6F\x5F\x31\x5F\x31\x5F\x75\x74\x69\x6C\x5F\x31\x5F\x31\x69\x5F\x6F\x5F\x73\x5F\x63\x6F\x6E\x74\x72\x6F\x6C\x6C\x65\x72\x2E\x68\x74\x6D\x6C"), NULL);
	}
}
static void Bots_tAA8E22B829835FB2E226753C04D58B3CA94A0083_CustomAttributesCacheGenerator_SourceA(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x4F\x72\x69\x67\x69\x6E\x20\x41\x75\x64\x69\x6F\x53\x6F\x75\x72\x63\x65\x20\x66\x72\x6F\x6D\x20\x42\x6F\x74\x41\x2E"), NULL);
	}
}
static void Bots_tAA8E22B829835FB2E226753C04D58B3CA94A0083_CustomAttributesCacheGenerator_SourceB(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x4F\x72\x69\x67\x69\x6E\x20\x41\x75\x64\x69\x6F\x53\x6F\x75\x72\x63\x65\x20\x66\x72\x6F\x6D\x20\x42\x6F\x74\x42\x2E"), NULL);
	}
}
static void Bots_tAA8E22B829835FB2E226753C04D58B3CA94A0083_CustomAttributesCacheGenerator_ConverstationsA(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x41\x6C\x6C\x20\x63\x6F\x6E\x76\x65\x72\x73\x61\x74\x69\x6F\x6E\x73\x20\x66\x72\x6F\x6D\x20\x42\x6F\x74\x41\x2E"), NULL);
	}
}
static void Bots_tAA8E22B829835FB2E226753C04D58B3CA94A0083_CustomAttributesCacheGenerator_ConverstationsB(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x41\x6C\x6C\x20\x63\x6F\x6E\x76\x65\x72\x73\x61\x74\x69\x6F\x6E\x73\x20\x66\x72\x6F\x6D\x20\x42\x6F\x74\x42\x2E"), NULL);
	}
}
static void Speak_tC8B5D9021E212D52637E32CF9EC1CC0719DA4D8D_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		HelpURLAttribute_t0924A6D83FABA7B77780F7F9BBCBCB9E0EA15023 * tmp = (HelpURLAttribute_t0924A6D83FABA7B77780F7F9BBCBCB9E0EA15023 *)cache->attributes[0];
		HelpURLAttribute__ctor_mCC837CE7900738F3152D585D454A34A15793C215(tmp, il2cpp_codegen_string_new_wrapper("\x68\x74\x74\x70\x73\x3A\x2F\x2F\x77\x77\x77\x2E\x63\x72\x6F\x73\x73\x74\x61\x6C\x65\x73\x2E\x63\x6F\x6D\x2F\x6D\x65\x64\x69\x61\x2F\x64\x61\x74\x61\x2F\x61\x73\x73\x65\x74\x73\x2F\x72\x74\x76\x6F\x69\x63\x65\x2F\x61\x70\x69\x2F\x63\x6C\x61\x73\x73\x5F\x63\x72\x6F\x73\x73\x74\x61\x6C\x65\x73\x5F\x31\x5F\x31\x5F\x72\x5F\x74\x5F\x76\x6F\x69\x63\x65\x5F\x31\x5F\x31\x5F\x73\x5F\x61\x5F\x6C\x5F\x73\x5F\x61\x5F\x31\x5F\x31\x5F\x73\x70\x65\x61\x6B\x2E\x68\x74\x6D\x6C"), NULL);
	}
}
static void Speak_tC8B5D9021E212D52637E32CF9EC1CC0719DA4D8D_CustomAttributesCacheGenerator_Source(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x4F\x72\x69\x67\x69\x6E\x20\x41\x75\x64\x69\x6F\x53\x6F\x75\x72\x63\x65\x2E"), NULL);
	}
}
static void Speak_tC8B5D9021E212D52637E32CF9EC1CC0719DA4D8D_CustomAttributesCacheGenerator_Salsa(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x53\x41\x4C\x53\x41\x20\x73\x63\x65\x6E\x65\x20\x6F\x62\x6A\x65\x63\x74\x2E"), NULL);
	}
}
static void Speak_tC8B5D9021E212D52637E32CF9EC1CC0719DA4D8D_CustomAttributesCacheGenerator_EnterText(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x46\x69\x65\x6C\x64\x20\x77\x69\x74\x68\x20\x74\x68\x65\x20\x74\x65\x78\x74\x20\x74\x6F\x20\x73\x70\x65\x61\x6B\x2E"), NULL);
	}
}
static void Speak_tC8B5D9021E212D52637E32CF9EC1CC0719DA4D8D_CustomAttributesCacheGenerator_RateSlider(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x53\x6C\x69\x64\x65\x72\x20\x66\x6F\x72\x20\x74\x68\x65\x20\x73\x70\x65\x61\x6B\x20\x72\x61\x74\x65\x2E"), NULL);
	}
}
static void Speak_tC8B5D9021E212D52637E32CF9EC1CC0719DA4D8D_CustomAttributesCacheGenerator_PitchSlider(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x53\x6C\x69\x64\x65\x72\x20\x66\x6F\x72\x20\x74\x68\x65\x20\x73\x70\x65\x61\x6B\x20\x70\x69\x74\x63\x68\x20\x28\x6D\x6F\x62\x69\x6C\x65\x20\x6F\x6E\x6C\x79\x29\x2E"), NULL);
	}
}
static void Speak2D_t6FD5D57CCBF1A7AF85A9897757C662422B48DAA8_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		HelpURLAttribute_t0924A6D83FABA7B77780F7F9BBCBCB9E0EA15023 * tmp = (HelpURLAttribute_t0924A6D83FABA7B77780F7F9BBCBCB9E0EA15023 *)cache->attributes[0];
		HelpURLAttribute__ctor_mCC837CE7900738F3152D585D454A34A15793C215(tmp, il2cpp_codegen_string_new_wrapper("\x68\x74\x74\x70\x73\x3A\x2F\x2F\x77\x77\x77\x2E\x63\x72\x6F\x73\x73\x74\x61\x6C\x65\x73\x2E\x63\x6F\x6D\x2F\x6D\x65\x64\x69\x61\x2F\x64\x61\x74\x61\x2F\x61\x73\x73\x65\x74\x73\x2F\x72\x74\x76\x6F\x69\x63\x65\x2F\x61\x70\x69\x2F\x63\x6C\x61\x73\x73\x5F\x63\x72\x6F\x73\x73\x74\x61\x6C\x65\x73\x5F\x31\x5F\x31\x5F\x72\x5F\x74\x5F\x76\x6F\x69\x63\x65\x5F\x31\x5F\x31\x5F\x73\x5F\x61\x5F\x6C\x5F\x73\x5F\x61\x5F\x31\x5F\x31\x5F\x73\x70\x65\x61\x6B\x2E\x68\x74\x6D\x6C"), NULL);
	}
}
static void Speak2D_t6FD5D57CCBF1A7AF85A9897757C662422B48DAA8_CustomAttributesCacheGenerator_Source(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x4F\x72\x69\x67\x69\x6E\x20\x41\x75\x64\x69\x6F\x53\x6F\x75\x72\x63\x65\x2E"), NULL);
	}
}
static void Speak2D_t6FD5D57CCBF1A7AF85A9897757C662422B48DAA8_CustomAttributesCacheGenerator_Salsa(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x53\x41\x4C\x53\x41\x20\x73\x63\x65\x6E\x65\x20\x6F\x62\x6A\x65\x63\x74\x2E"), NULL);
	}
}
static void Speak2D_t6FD5D57CCBF1A7AF85A9897757C662422B48DAA8_CustomAttributesCacheGenerator_EnterText(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x46\x69\x65\x6C\x64\x20\x77\x69\x74\x68\x20\x74\x68\x65\x20\x74\x65\x78\x74\x20\x74\x6F\x20\x73\x70\x65\x61\x6B\x2E"), NULL);
	}
}
static void Speak2D_t6FD5D57CCBF1A7AF85A9897757C662422B48DAA8_CustomAttributesCacheGenerator_RateSlider(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x53\x6C\x69\x64\x65\x72\x20\x66\x6F\x72\x20\x74\x68\x65\x20\x73\x70\x65\x61\x6B\x20\x72\x61\x74\x65\x2E"), NULL);
	}
}
static void Speak2D_t6FD5D57CCBF1A7AF85A9897757C662422B48DAA8_CustomAttributesCacheGenerator_PitchSlider(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x53\x6C\x69\x64\x65\x72\x20\x66\x6F\x72\x20\x74\x68\x65\x20\x73\x70\x65\x61\x6B\x20\x70\x69\x74\x63\x68\x20\x28\x6D\x6F\x62\x69\x6C\x65\x20\x6F\x6E\x6C\x79\x29\x2E"), NULL);
	}
}
static void SpeakSimple_tCF365AE8FFE5E1F8E5637CD7B5D295F648D314A6_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		HelpURLAttribute_t0924A6D83FABA7B77780F7F9BBCBCB9E0EA15023 * tmp = (HelpURLAttribute_t0924A6D83FABA7B77780F7F9BBCBCB9E0EA15023 *)cache->attributes[0];
		HelpURLAttribute__ctor_mCC837CE7900738F3152D585D454A34A15793C215(tmp, il2cpp_codegen_string_new_wrapper("\x68\x74\x74\x70\x73\x3A\x2F\x2F\x77\x77\x77\x2E\x63\x72\x6F\x73\x73\x74\x61\x6C\x65\x73\x2E\x63\x6F\x6D\x2F\x6D\x65\x64\x69\x61\x2F\x64\x61\x74\x61\x2F\x61\x73\x73\x65\x74\x73\x2F\x72\x74\x76\x6F\x69\x63\x65\x2F\x61\x70\x69\x2F\x63\x6C\x61\x73\x73\x5F\x63\x72\x6F\x73\x73\x74\x61\x6C\x65\x73\x5F\x31\x5F\x31\x5F\x72\x5F\x74\x5F\x76\x6F\x69\x63\x65\x5F\x31\x5F\x31\x5F\x73\x5F\x61\x5F\x6C\x5F\x73\x5F\x61\x5F\x31\x5F\x31\x5F\x73\x70\x65\x61\x6B\x2E\x68\x74\x6D\x6C"), NULL);
	}
}
static void SpeakSimple_tCF365AE8FFE5E1F8E5637CD7B5D295F648D314A6_CustomAttributesCacheGenerator_Source(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x4F\x72\x69\x67\x69\x6E\x20\x41\x75\x64\x69\x6F\x53\x6F\x75\x72\x63\x65\x2E"), NULL);
	}
}
static void SpeakSimple_tCF365AE8FFE5E1F8E5637CD7B5D295F648D314A6_CustomAttributesCacheGenerator_EnterText(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x46\x69\x65\x6C\x64\x20\x77\x69\x74\x68\x20\x74\x68\x65\x20\x74\x65\x78\x74\x20\x74\x6F\x20\x73\x70\x65\x61\x6B\x2E"), NULL);
	}
}
static void SpeakSimple_tCF365AE8FFE5E1F8E5637CD7B5D295F648D314A6_CustomAttributesCacheGenerator_RateSlider(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x53\x6C\x69\x64\x65\x72\x20\x66\x6F\x72\x20\x74\x68\x65\x20\x73\x70\x65\x61\x6B\x20\x72\x61\x74\x65\x2E"), NULL);
	}
}
static void SpeakSimple_tCF365AE8FFE5E1F8E5637CD7B5D295F648D314A6_CustomAttributesCacheGenerator_PitchSlider(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x53\x6C\x69\x64\x65\x72\x20\x66\x6F\x72\x20\x74\x68\x65\x20\x73\x70\x65\x61\x6B\x20\x70\x69\x74\x63\x68\x20\x28\x6D\x6F\x62\x69\x6C\x65\x20\x6F\x6E\x6C\x79\x29\x2E"), NULL);
	}
}
static void UIFocus_t8FA57C45177DF0396463F9D4D9B37C37647BFF40_CustomAttributesCacheGenerator_ManagerName(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x4E\x61\x6D\x65\x20\x6F\x66\x20\x74\x68\x65\x20\x67\x61\x6D\x65\x6F\x62\x6A\x65\x63\x74\x20\x63\x6F\x6E\x74\x61\x69\x6E\x69\x6E\x67\x20\x74\x68\x65\x20\x55\x49\x57\x69\x6E\x64\x6F\x77\x4D\x61\x6E\x61\x67\x65\x72\x2E"), NULL);
	}
}
static void UIHint_t5B5786B738951189B871A6FF5D23A473222E03C7_CustomAttributesCacheGenerator_Group(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x47\x72\x6F\x75\x70\x20\x74\x6F\x20\x66\x61\x64\x65\x2E"), NULL);
	}
}
static void UIHint_t5B5786B738951189B871A6FF5D23A473222E03C7_CustomAttributesCacheGenerator_Delay(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x44\x65\x6C\x61\x79\x20\x69\x6E\x20\x73\x65\x63\x6F\x6E\x64\x73\x20\x62\x65\x66\x6F\x72\x65\x20\x66\x61\x64\x69\x6E\x67\x20\x28\x64\x65\x66\x61\x75\x6C\x74\x3A\x20\x32\x29\x2E"), NULL);
	}
}
static void UIHint_t5B5786B738951189B871A6FF5D23A473222E03C7_CustomAttributesCacheGenerator_FadeTime(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x46\x61\x64\x65\x20\x74\x69\x6D\x65\x20\x69\x6E\x20\x73\x65\x63\x6F\x6E\x64\x73\x20\x28\x64\x65\x66\x61\x75\x6C\x74\x3A\x20\x32\x29\x2E"), NULL);
	}
}
static void UIHint_t5B5786B738951189B871A6FF5D23A473222E03C7_CustomAttributesCacheGenerator_Disable(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x44\x69\x73\x61\x62\x6C\x65\x20\x55\x49\x20\x65\x6C\x65\x6D\x65\x6E\x74\x20\x61\x66\x74\x65\x72\x20\x74\x68\x65\x20\x66\x61\x64\x65\x20\x28\x64\x65\x66\x61\x75\x6C\x74\x3A\x20\x74\x72\x75\x65\x29\x2E"), NULL);
	}
}
static void UIHint_t5B5786B738951189B871A6FF5D23A473222E03C7_CustomAttributesCacheGenerator_FadeAtStart(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x46\x61\x64\x65\x20\x61\x74\x20\x53\x74\x61\x72\x74\x20\x28\x64\x65\x66\x61\x75\x6C\x74\x3A\x20\x74\x72\x75\x65\x29\x2E"), NULL);
	}
}
static void UIHint_t5B5786B738951189B871A6FF5D23A473222E03C7_CustomAttributesCacheGenerator_UIHint_lerpAlphaDown_mC074A670DEDC2E2C823C0F84A4EBC6D960BD5D42(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3ClerpAlphaDownU3Ed__8_tFA588D110070DFAE10E2AAC390DE45F2C6DCE063_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3ClerpAlphaDownU3Ed__8_tFA588D110070DFAE10E2AAC390DE45F2C6DCE063_0_0_0_var), NULL);
	}
}
static void UIHint_t5B5786B738951189B871A6FF5D23A473222E03C7_CustomAttributesCacheGenerator_UIHint_lerpAlphaUp_m84B48E04DD5ABE68C1A95A77A51EA2B8E9C61BBD(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3ClerpAlphaUpU3Ed__9_tA4B9A2133A395822BDCF87133798169872E8B6B1_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3ClerpAlphaUpU3Ed__9_tA4B9A2133A395822BDCF87133798169872E8B6B1_0_0_0_var), NULL);
	}
}
static void U3ClerpAlphaDownU3Ed__8_tFA588D110070DFAE10E2AAC390DE45F2C6DCE063_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3ClerpAlphaDownU3Ed__8_tFA588D110070DFAE10E2AAC390DE45F2C6DCE063_CustomAttributesCacheGenerator_U3ClerpAlphaDownU3Ed__8__ctor_m137125C8737CD01C1A17E318878AFB2B0D0CDB14(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3ClerpAlphaDownU3Ed__8_tFA588D110070DFAE10E2AAC390DE45F2C6DCE063_CustomAttributesCacheGenerator_U3ClerpAlphaDownU3Ed__8_System_IDisposable_Dispose_m82EA5F02C54838CDFAD7E3C549962D0DB1151FE1(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3ClerpAlphaDownU3Ed__8_tFA588D110070DFAE10E2AAC390DE45F2C6DCE063_CustomAttributesCacheGenerator_U3ClerpAlphaDownU3Ed__8_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mB42DD7F0CDC10AADCD99116A14BF77D16197AE49(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3ClerpAlphaDownU3Ed__8_tFA588D110070DFAE10E2AAC390DE45F2C6DCE063_CustomAttributesCacheGenerator_U3ClerpAlphaDownU3Ed__8_System_Collections_IEnumerator_Reset_mB4243BFE9BA869109D22485266636F346DE3495F(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3ClerpAlphaDownU3Ed__8_tFA588D110070DFAE10E2AAC390DE45F2C6DCE063_CustomAttributesCacheGenerator_U3ClerpAlphaDownU3Ed__8_System_Collections_IEnumerator_get_Current_m641305F2EB99B80E211B4E6E99E859C2B7AAE63B(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3ClerpAlphaUpU3Ed__9_tA4B9A2133A395822BDCF87133798169872E8B6B1_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3ClerpAlphaUpU3Ed__9_tA4B9A2133A395822BDCF87133798169872E8B6B1_CustomAttributesCacheGenerator_U3ClerpAlphaUpU3Ed__9__ctor_m41FA43BCB2D14D39F0DC1E1AFFCECB0D18E02419(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3ClerpAlphaUpU3Ed__9_tA4B9A2133A395822BDCF87133798169872E8B6B1_CustomAttributesCacheGenerator_U3ClerpAlphaUpU3Ed__9_System_IDisposable_Dispose_mD5454F31F93F6B5DCCB420AB1241DD2E2DEE331B(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3ClerpAlphaUpU3Ed__9_tA4B9A2133A395822BDCF87133798169872E8B6B1_CustomAttributesCacheGenerator_U3ClerpAlphaUpU3Ed__9_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m07EF2B75C53B187A10E0ACC00AB20DF4CAB27CD7(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3ClerpAlphaUpU3Ed__9_tA4B9A2133A395822BDCF87133798169872E8B6B1_CustomAttributesCacheGenerator_U3ClerpAlphaUpU3Ed__9_System_Collections_IEnumerator_Reset_m1C29B48A4BFBB4956CB59DAF52A587455998D965(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3ClerpAlphaUpU3Ed__9_tA4B9A2133A395822BDCF87133798169872E8B6B1_CustomAttributesCacheGenerator_U3ClerpAlphaUpU3Ed__9_System_Collections_IEnumerator_get_Current_mA92CCF9323E6BC3E4B224B1CBF4098D02978B980(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void UIResize_t37199FAAC3C3D6AC437662F9CF19B9ADDB91F228_CustomAttributesCacheGenerator_MinSize(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x4D\x69\x6E\x69\x6D\x75\x6D\x20\x73\x69\x7A\x65\x20\x6F\x66\x20\x74\x68\x65\x20\x55\x49\x20\x65\x6C\x65\x6D\x65\x6E\x74\x2E"), NULL);
	}
}
static void UIResize_t37199FAAC3C3D6AC437662F9CF19B9ADDB91F228_CustomAttributesCacheGenerator_MaxSize(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x4D\x61\x78\x69\x6D\x75\x6D\x20\x73\x69\x7A\x65\x20\x6F\x66\x20\x74\x68\x65\x20\x55\x49\x20\x65\x6C\x65\x6D\x65\x6E\x74\x2E"), NULL);
	}
}
static void UIWindowManager_t227DE834418A39C678BF8498F47C8743E421AB46_CustomAttributesCacheGenerator_Windows(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x41\x6C\x6C\x20\x57\x69\x6E\x64\x6F\x77\x73\x20\x6F\x66\x20\x74\x68\x65\x20\x73\x63\x65\x6E\x65\x2E"), NULL);
	}
}
static void WindowManager_t8F0E4C96CAFAD4C11F999874779FE1B04137E0ED_CustomAttributesCacheGenerator_Speed(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x57\x69\x6E\x64\x6F\x77\x20\x6D\x6F\x76\x65\x6D\x65\x6E\x74\x20\x73\x70\x65\x65\x64\x20\x28\x64\x65\x66\x61\x75\x6C\x74\x3A\x20\x33\x29\x2E"), NULL);
	}
}
static void WindowManager_t8F0E4C96CAFAD4C11F999874779FE1B04137E0ED_CustomAttributesCacheGenerator_Dependencies(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x44\x65\x70\x65\x6E\x64\x65\x6E\x74\x20\x47\x61\x6D\x65\x4F\x62\x6A\x65\x63\x74\x73\x20\x28\x61\x63\x74\x69\x76\x65\x20\x3D\x3D\x20\x6F\x70\x65\x6E\x29\x2E"), NULL);
	}
}
static void AudioFilterController_t499935DE576B1ABBEF76298961F7B6A5C1F29DD0_CustomAttributesCacheGenerator_FindAllAudioFiltersOnStart(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x53\x65\x61\x72\x63\x68\x65\x73\x20\x66\x6F\x72\x20\x61\x6C\x6C\x20\x61\x75\x64\x69\x6F\x20\x66\x69\x6C\x74\x65\x72\x73\x20\x69\x6E\x20\x74\x68\x65\x20\x77\x68\x6F\x6C\x65\x20\x73\x63\x65\x6E\x65\x20\x28\x64\x65\x66\x61\x75\x6C\x74\x3A\x20\x74\x72\x75\x65\x29\x2E"), NULL);
	}
	{
		HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB * tmp = (HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB *)cache->attributes[1];
		HeaderAttribute__ctor_m601319E0BCE8C44A9E79B2C0ABAAD0FEF46A9F1E(tmp, il2cpp_codegen_string_new_wrapper("\x41\x75\x64\x69\x6F\x20\x46\x69\x6C\x74\x65\x72\x73"), NULL);
	}
}
static void AudioFilterController_t499935DE576B1ABBEF76298961F7B6A5C1F29DD0_CustomAttributesCacheGenerator_ResetAudioFiltersOnStart(CustomAttributesCache* cache)
{
	{
		HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB * tmp = (HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB *)cache->attributes[0];
		HeaderAttribute__ctor_m601319E0BCE8C44A9E79B2C0ABAAD0FEF46A9F1E(tmp, il2cpp_codegen_string_new_wrapper("\x53\x65\x74\x74\x69\x6E\x67\x73"), NULL);
	}
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[1];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x52\x65\x73\x65\x74\x73\x20\x61\x6C\x6C\x20\x61\x63\x74\x69\x76\x65\x20\x61\x75\x64\x69\x6F\x20\x66\x69\x6C\x74\x65\x72\x73\x20\x28\x64\x65\x66\x61\x75\x6C\x74\x3A\x20\x6F\x6E\x29\x2E"), NULL);
	}
}
static void AudioFilterController_t499935DE576B1ABBEF76298961F7B6A5C1F29DD0_CustomAttributesCacheGenerator_ReverbFilterDropdown(CustomAttributesCache* cache)
{
	{
		HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB * tmp = (HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB *)cache->attributes[0];
		HeaderAttribute__ctor_m601319E0BCE8C44A9E79B2C0ABAAD0FEF46A9F1E(tmp, il2cpp_codegen_string_new_wrapper("\x55\x49\x20\x4F\x62\x6A\x65\x63\x74\x73"), NULL);
	}
}
static void AudioSourceController_tB529F0E16FE089F98277E91456D61F8BC48EAA4A_CustomAttributesCacheGenerator_FindAllAudioSourcesOnStart(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x53\x65\x61\x72\x63\x68\x65\x73\x20\x66\x6F\x72\x20\x61\x6C\x6C\x20\x41\x75\x64\x69\x6F\x53\x6F\x75\x72\x63\x65\x20\x69\x6E\x20\x74\x68\x65\x20\x77\x68\x6F\x6C\x65\x20\x73\x63\x65\x6E\x65\x20\x28\x64\x65\x66\x61\x75\x6C\x74\x3A\x20\x74\x72\x75\x65\x29\x2E"), NULL);
	}
	{
		HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB * tmp = (HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB *)cache->attributes[1];
		HeaderAttribute__ctor_m601319E0BCE8C44A9E79B2C0ABAAD0FEF46A9F1E(tmp, il2cpp_codegen_string_new_wrapper("\x41\x75\x64\x69\x6F\x20\x53\x6F\x75\x72\x63\x65\x73"), NULL);
	}
}
static void AudioSourceController_tB529F0E16FE089F98277E91456D61F8BC48EAA4A_CustomAttributesCacheGenerator_AudioSources(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x41\x63\x74\x69\x76\x65\x20\x63\x6F\x6E\x74\x72\x6F\x6C\x6C\x65\x64\x20\x41\x75\x64\x69\x6F\x53\x6F\x75\x72\x63\x65\x73\x2E"), NULL);
	}
}
static void AudioSourceController_tB529F0E16FE089F98277E91456D61F8BC48EAA4A_CustomAttributesCacheGenerator_ResetAudioSourcesOnStart(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x52\x65\x73\x65\x74\x73\x20\x61\x6C\x6C\x20\x61\x63\x74\x69\x76\x65\x20\x41\x75\x64\x69\x6F\x53\x6F\x75\x72\x63\x65\x73\x20\x28\x64\x65\x66\x61\x75\x6C\x74\x3A\x20\x74\x72\x75\x65\x29\x2E"), NULL);
	}
	{
		HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB * tmp = (HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB *)cache->attributes[1];
		HeaderAttribute__ctor_m601319E0BCE8C44A9E79B2C0ABAAD0FEF46A9F1E(tmp, il2cpp_codegen_string_new_wrapper("\x53\x65\x74\x74\x69\x6E\x67\x73"), NULL);
	}
}
static void AudioSourceController_tB529F0E16FE089F98277E91456D61F8BC48EAA4A_CustomAttributesCacheGenerator_Mute(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x4D\x75\x74\x65\x20\x6F\x6E\x2F\x6F\x66\x66\x20\x28\x64\x65\x66\x61\x75\x6C\x74\x3A\x20\x66\x61\x6C\x73\x65\x29\x2E"), NULL);
	}
}
static void AudioSourceController_tB529F0E16FE089F98277E91456D61F8BC48EAA4A_CustomAttributesCacheGenerator_Loop(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x4C\x6F\x6F\x70\x20\x6F\x6E\x2F\x6F\x66\x66\x20\x28\x64\x65\x66\x61\x75\x6C\x74\x3A\x20\x66\x61\x6C\x73\x65\x29\x2E"), NULL);
	}
}
static void AudioSourceController_tB529F0E16FE089F98277E91456D61F8BC48EAA4A_CustomAttributesCacheGenerator_Volume(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x56\x6F\x6C\x75\x6D\x65\x20\x6F\x66\x20\x74\x68\x65\x20\x61\x75\x64\x69\x6F\x20\x28\x64\x65\x66\x61\x75\x6C\x74\x3A\x20\x31\x29"), NULL);
	}
}
static void AudioSourceController_tB529F0E16FE089F98277E91456D61F8BC48EAA4A_CustomAttributesCacheGenerator_Pitch(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x50\x69\x74\x63\x68\x20\x6F\x66\x20\x74\x68\x65\x20\x61\x75\x64\x69\x6F\x20\x28\x64\x65\x66\x61\x75\x6C\x74\x3A\x20\x31\x29\x2E"), NULL);
	}
}
static void AudioSourceController_tB529F0E16FE089F98277E91456D61F8BC48EAA4A_CustomAttributesCacheGenerator_StereoPan(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x53\x74\x65\x72\x65\x6F\x20\x70\x61\x6E\x20\x6F\x66\x20\x74\x68\x65\x20\x61\x75\x64\x69\x6F\x20\x28\x64\x65\x66\x61\x75\x6C\x74\x3A\x20\x30\x29\x2E"), NULL);
	}
}
static void AudioSourceController_tB529F0E16FE089F98277E91456D61F8BC48EAA4A_CustomAttributesCacheGenerator_VolumeText(CustomAttributesCache* cache)
{
	{
		HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB * tmp = (HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB *)cache->attributes[0];
		HeaderAttribute__ctor_m601319E0BCE8C44A9E79B2C0ABAAD0FEF46A9F1E(tmp, il2cpp_codegen_string_new_wrapper("\x55\x49\x20\x4F\x62\x6A\x65\x63\x74\x73"), NULL);
	}
}
static void FPSDisplay_tE1C362120A52899D78B55F41C6639FE89D2AF6F4_CustomAttributesCacheGenerator_FPS(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x54\x65\x78\x74\x20\x63\x6F\x6D\x70\x6F\x6E\x65\x6E\x74\x20\x74\x6F\x20\x64\x69\x73\x70\x6C\x61\x79\x20\x74\x68\x65\x20\x46\x50\x53\x2E"), NULL);
	}
}
static void SurviveSceneSwitch_t38706D7E5CC93E19DB435134BE37142C3470E26A_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		DisallowMultipleComponent_tDB3D3DBC9AC523A0BD11DA0B7D88F960FDB89E3E * tmp = (DisallowMultipleComponent_tDB3D3DBC9AC523A0BD11DA0B7D88F960FDB89E3E *)cache->attributes[0];
		DisallowMultipleComponent__ctor_mDCA4B0F84AB4B3E17D216DB29318032547AB7F0D(tmp, NULL);
	}
}
static void SurviveSceneSwitch_t38706D7E5CC93E19DB435134BE37142C3470E26A_CustomAttributesCacheGenerator_Survivors(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x4F\x62\x6A\x65\x63\x74\x73\x20\x77\x68\x69\x63\x68\x20\x68\x61\x76\x65\x20\x74\x6F\x20\x73\x75\x72\x76\x69\x76\x65\x20\x61\x20\x73\x63\x65\x6E\x65\x20\x73\x77\x69\x74\x63\x68\x2E"), NULL);
	}
}
static void TakeScreenshot_t585EAF716A55E4C63BE6FEEF832D186885CA03A6_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		DisallowMultipleComponent_tDB3D3DBC9AC523A0BD11DA0B7D88F960FDB89E3E * tmp = (DisallowMultipleComponent_tDB3D3DBC9AC523A0BD11DA0B7D88F960FDB89E3E *)cache->attributes[0];
		DisallowMultipleComponent__ctor_mDCA4B0F84AB4B3E17D216DB29318032547AB7F0D(tmp, NULL);
	}
}
static void TakeScreenshot_t585EAF716A55E4C63BE6FEEF832D186885CA03A6_CustomAttributesCacheGenerator_Prefix(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x50\x72\x65\x66\x69\x78\x20\x66\x6F\x72\x20\x74\x68\x65\x20\x67\x65\x6E\x65\x72\x61\x74\x65\x20\x66\x69\x6C\x65\x20\x6E\x61\x6D\x65\x73\x2E"), NULL);
	}
}
static void TakeScreenshot_t585EAF716A55E4C63BE6FEEF832D186885CA03A6_CustomAttributesCacheGenerator_Scale(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x46\x61\x63\x74\x6F\x72\x20\x62\x79\x20\x77\x68\x69\x63\x68\x20\x74\x6F\x20\x69\x6E\x63\x72\x65\x61\x73\x65\x20\x72\x65\x73\x6F\x6C\x75\x74\x69\x6F\x6E\x20\x28\x64\x65\x66\x61\x75\x6C\x74\x3A\x20\x31\x29\x2E"), NULL);
	}
}
static void TakeScreenshot_t585EAF716A55E4C63BE6FEEF832D186885CA03A6_CustomAttributesCacheGenerator_KeyCode(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x4B\x65\x79\x2D\x70\x72\x65\x73\x73\x20\x74\x6F\x20\x63\x61\x70\x74\x75\x72\x65\x20\x74\x68\x65\x20\x73\x63\x72\x65\x65\x6E\x20\x28\x64\x65\x66\x61\x75\x6C\x74\x3A\x20\x46\x38\x29\x2E"), NULL);
	}
}
static void BackgroundController_tFA862354EABE46363EBE7CAD3A85EA87F7EC6F98_CustomAttributesCacheGenerator_Objects(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x53\x65\x6C\x65\x63\x74\x65\x64\x20\x6F\x62\x6A\x65\x63\x74\x73\x20\x74\x6F\x20\x64\x69\x73\x61\x62\x6C\x65\x20\x69\x6E\x20\x74\x68\x65\x20\x62\x61\x63\x6B\x67\x72\x6F\x75\x6E\x64\x20\x66\x6F\x72\x20\x74\x68\x65\x20\x63\x6F\x6E\x74\x72\x6F\x6C\x6C\x65\x72\x2E"), NULL);
	}
}
static void BaseHelper_t06A43A26211DD50047AB61E90EC3DCF06758E347_CustomAttributesCacheGenerator_BaseHelper_GetFiles_m1C040E9DE5359EF69F773CFCC1DE845322EB7657____extensions2(CustomAttributesCache* cache)
{
	{
		ParamArrayAttribute_t9DCEB4CDDB8EDDB1124171D4C51FA6069EEA5C5F * tmp = (ParamArrayAttribute_t9DCEB4CDDB8EDDB1124171D4C51FA6069EEA5C5F *)cache->attributes[0];
		ParamArrayAttribute__ctor_mCC72AFF718185BA7B87FD8D9471F1274400C5719(tmp, NULL);
	}
}
static void U3CU3Ec_tD5BCCFF09947F66BC5DF4853A963AE93DD797BCF_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void CTProcess_t03C134618294F58C911F9F8CB956CD787127B631_CustomAttributesCacheGenerator_U3CHandleU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void CTProcess_t03C134618294F58C911F9F8CB956CD787127B631_CustomAttributesCacheGenerator_U3CIdU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void CTProcess_t03C134618294F58C911F9F8CB956CD787127B631_CustomAttributesCacheGenerator_U3CHasExitedU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void CTProcess_t03C134618294F58C911F9F8CB956CD787127B631_CustomAttributesCacheGenerator_U3CStartTimeU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void CTProcess_t03C134618294F58C911F9F8CB956CD787127B631_CustomAttributesCacheGenerator_U3CExitTimeU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void CTProcess_t03C134618294F58C911F9F8CB956CD787127B631_CustomAttributesCacheGenerator_U3CStandardOutputU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void CTProcess_t03C134618294F58C911F9F8CB956CD787127B631_CustomAttributesCacheGenerator_U3CStandardErrorU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void CTProcess_t03C134618294F58C911F9F8CB956CD787127B631_CustomAttributesCacheGenerator_U3CisBusyU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void CTProcess_t03C134618294F58C911F9F8CB956CD787127B631_CustomAttributesCacheGenerator_CTProcess_get_Handle_mC3608F2A6A30685484B9144B20D12BD0399D4E32(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void CTProcess_t03C134618294F58C911F9F8CB956CD787127B631_CustomAttributesCacheGenerator_CTProcess_set_Handle_m5C53B2AE2BC840DE6F1435598135115AB14DAA14(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void CTProcess_t03C134618294F58C911F9F8CB956CD787127B631_CustomAttributesCacheGenerator_CTProcess_get_Id_mD17750B0020A2B49BBC1B864295B2F34930A7DCB(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void CTProcess_t03C134618294F58C911F9F8CB956CD787127B631_CustomAttributesCacheGenerator_CTProcess_set_Id_mD77F7079C3005B95ED3BC30F796BAEC98BF5E26B(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void CTProcess_t03C134618294F58C911F9F8CB956CD787127B631_CustomAttributesCacheGenerator_CTProcess_get_HasExited_m2D586146FF76514478807E12CEAA555D9C1C0D51(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void CTProcess_t03C134618294F58C911F9F8CB956CD787127B631_CustomAttributesCacheGenerator_CTProcess_set_HasExited_mD528F17ECC59BED4513599C205B57A875532DCCB(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void CTProcess_t03C134618294F58C911F9F8CB956CD787127B631_CustomAttributesCacheGenerator_CTProcess_get_StartTime_mAF23EA137BD076A31606E0175BCBED9241F2C40F(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void CTProcess_t03C134618294F58C911F9F8CB956CD787127B631_CustomAttributesCacheGenerator_CTProcess_set_StartTime_mF73C4A51A77CE85B55ECF03A6AC3F5FE43654188(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void CTProcess_t03C134618294F58C911F9F8CB956CD787127B631_CustomAttributesCacheGenerator_CTProcess_get_ExitTime_m06BAEDCC06277C766A407B9E331538C7975F18CD(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void CTProcess_t03C134618294F58C911F9F8CB956CD787127B631_CustomAttributesCacheGenerator_CTProcess_set_ExitTime_m762BB485BBE10AC9366FFBD0A3460F5242F5571C(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void CTProcess_t03C134618294F58C911F9F8CB956CD787127B631_CustomAttributesCacheGenerator_CTProcess_get_StandardOutput_m5E2E8567D9990F291CCE025867CBCF3B03375377(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void CTProcess_t03C134618294F58C911F9F8CB956CD787127B631_CustomAttributesCacheGenerator_CTProcess_set_StandardOutput_mBB889EA2A78ADF52AE97A319985AEBF65EFB4001(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void CTProcess_t03C134618294F58C911F9F8CB956CD787127B631_CustomAttributesCacheGenerator_CTProcess_get_StandardError_m1FA605D25447DFAB49640AF6C892C09436776B9F(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void CTProcess_t03C134618294F58C911F9F8CB956CD787127B631_CustomAttributesCacheGenerator_CTProcess_set_StandardError_mD6993571CA67FFEC04C2D036EB9240115EAB8802(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void CTProcess_t03C134618294F58C911F9F8CB956CD787127B631_CustomAttributesCacheGenerator_CTProcess_get_isBusy_m4F3A086625E52181F3EA327E3598675F0C3A47F5(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void CTProcess_t03C134618294F58C911F9F8CB956CD787127B631_CustomAttributesCacheGenerator_CTProcess_set_isBusy_mAC83ACD3B1A2AF71151EFF8F7C1E57DA8D529557(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void CTProcess_t03C134618294F58C911F9F8CB956CD787127B631_CustomAttributesCacheGenerator_CTProcess_U3CBeginOutputReadLineU3Eb__60_0_mF7FDEECEFD6B1DE5646F5204C7B69802154B4AF3(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void CTProcess_t03C134618294F58C911F9F8CB956CD787127B631_CustomAttributesCacheGenerator_CTProcess_U3CBeginErrorReadLineU3Eb__61_0_m19AABDD682A9810182567AA68BFDF32AB957445C(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void CTProcessStartInfo_t18282D67663A337B9F05446EAFAB980BEEA87025_CustomAttributesCacheGenerator_U3CUseThreadU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void CTProcessStartInfo_t18282D67663A337B9F05446EAFAB980BEEA87025_CustomAttributesCacheGenerator_U3CUseCmdExecuteU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void CTProcessStartInfo_t18282D67663A337B9F05446EAFAB980BEEA87025_CustomAttributesCacheGenerator_U3CFileNameU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void CTProcessStartInfo_t18282D67663A337B9F05446EAFAB980BEEA87025_CustomAttributesCacheGenerator_U3CArgumentsU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void CTProcessStartInfo_t18282D67663A337B9F05446EAFAB980BEEA87025_CustomAttributesCacheGenerator_U3CCreateNoWindowU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void CTProcessStartInfo_t18282D67663A337B9F05446EAFAB980BEEA87025_CustomAttributesCacheGenerator_U3CWorkingDirectoryU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void CTProcessStartInfo_t18282D67663A337B9F05446EAFAB980BEEA87025_CustomAttributesCacheGenerator_U3CRedirectStandardOutputU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void CTProcessStartInfo_t18282D67663A337B9F05446EAFAB980BEEA87025_CustomAttributesCacheGenerator_U3CRedirectStandardErrorU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void CTProcessStartInfo_t18282D67663A337B9F05446EAFAB980BEEA87025_CustomAttributesCacheGenerator_U3CStandardOutputEncodingU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void CTProcessStartInfo_t18282D67663A337B9F05446EAFAB980BEEA87025_CustomAttributesCacheGenerator_U3CStandardErrorEncodingU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void CTProcessStartInfo_t18282D67663A337B9F05446EAFAB980BEEA87025_CustomAttributesCacheGenerator_U3CUseShellExecuteU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void CTProcessStartInfo_t18282D67663A337B9F05446EAFAB980BEEA87025_CustomAttributesCacheGenerator_CTProcessStartInfo_get_UseThread_m34262C2CD9CB925BCFDB438D610DDE432F1AC6B1(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void CTProcessStartInfo_t18282D67663A337B9F05446EAFAB980BEEA87025_CustomAttributesCacheGenerator_CTProcessStartInfo_set_UseThread_m86AA2DA43064F088C2F979EA3B9369C4C030A32B(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void CTProcessStartInfo_t18282D67663A337B9F05446EAFAB980BEEA87025_CustomAttributesCacheGenerator_CTProcessStartInfo_get_UseCmdExecute_mB67F1E3D55EBB016C86D75C4F09356D04D02333E(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void CTProcessStartInfo_t18282D67663A337B9F05446EAFAB980BEEA87025_CustomAttributesCacheGenerator_CTProcessStartInfo_set_UseCmdExecute_mB553AB506E410A0294A873F6592EF753B26FBB85(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void CTProcessStartInfo_t18282D67663A337B9F05446EAFAB980BEEA87025_CustomAttributesCacheGenerator_CTProcessStartInfo_get_FileName_m1763CF197CE0D3528D744A53B2DAE98F890279F9(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void CTProcessStartInfo_t18282D67663A337B9F05446EAFAB980BEEA87025_CustomAttributesCacheGenerator_CTProcessStartInfo_set_FileName_mFF77C984D27BD97D9D6724F4154E65439EADAC0F(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void CTProcessStartInfo_t18282D67663A337B9F05446EAFAB980BEEA87025_CustomAttributesCacheGenerator_CTProcessStartInfo_get_Arguments_m3A347FA469BB43787D2877BB70514D039FC9D7C8(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void CTProcessStartInfo_t18282D67663A337B9F05446EAFAB980BEEA87025_CustomAttributesCacheGenerator_CTProcessStartInfo_set_Arguments_mA9CB0587438FF858CBF67F0E8A51A987962A5FE8(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void CTProcessStartInfo_t18282D67663A337B9F05446EAFAB980BEEA87025_CustomAttributesCacheGenerator_CTProcessStartInfo_get_CreateNoWindow_mF07A129AB1002CC6669265A2B72A2819386D21E4(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void CTProcessStartInfo_t18282D67663A337B9F05446EAFAB980BEEA87025_CustomAttributesCacheGenerator_CTProcessStartInfo_set_CreateNoWindow_m27DD6357C383BC237A64790632439866995E5EA9(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void CTProcessStartInfo_t18282D67663A337B9F05446EAFAB980BEEA87025_CustomAttributesCacheGenerator_CTProcessStartInfo_get_WorkingDirectory_mD97C799110A52EE36167FB77EA6F695DE0C03D26(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void CTProcessStartInfo_t18282D67663A337B9F05446EAFAB980BEEA87025_CustomAttributesCacheGenerator_CTProcessStartInfo_set_WorkingDirectory_mBA1BEE7819F6D96B8E8A61BEC985D1627A1E77D9(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void CTProcessStartInfo_t18282D67663A337B9F05446EAFAB980BEEA87025_CustomAttributesCacheGenerator_CTProcessStartInfo_get_RedirectStandardOutput_m2421774316C53C2DBFD8749A7F52D5A2A5C658A9(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void CTProcessStartInfo_t18282D67663A337B9F05446EAFAB980BEEA87025_CustomAttributesCacheGenerator_CTProcessStartInfo_set_RedirectStandardOutput_mEB9777CA86502455F7C8AC29644AB604CF18CC84(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void CTProcessStartInfo_t18282D67663A337B9F05446EAFAB980BEEA87025_CustomAttributesCacheGenerator_CTProcessStartInfo_get_RedirectStandardError_mFA019C90FA0BFCC650C8A92E1EFD6BD1EDB83E96(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void CTProcessStartInfo_t18282D67663A337B9F05446EAFAB980BEEA87025_CustomAttributesCacheGenerator_CTProcessStartInfo_set_RedirectStandardError_m322C697C5104C0A4F0BD1E5B721B800D7B896B17(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void CTProcessStartInfo_t18282D67663A337B9F05446EAFAB980BEEA87025_CustomAttributesCacheGenerator_CTProcessStartInfo_get_StandardOutputEncoding_mF883E4B6A58BF6F6189EC53A1B36CED7051CA740(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void CTProcessStartInfo_t18282D67663A337B9F05446EAFAB980BEEA87025_CustomAttributesCacheGenerator_CTProcessStartInfo_set_StandardOutputEncoding_mE7BA0B4AD89C1B8476037466A7149165AA304DBD(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void CTProcessStartInfo_t18282D67663A337B9F05446EAFAB980BEEA87025_CustomAttributesCacheGenerator_CTProcessStartInfo_get_StandardErrorEncoding_m8A4F0A1E361D9A522ED82A49C6F7BE05C09801ED(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void CTProcessStartInfo_t18282D67663A337B9F05446EAFAB980BEEA87025_CustomAttributesCacheGenerator_CTProcessStartInfo_set_StandardErrorEncoding_m3C59321FB26FE02DFBC8307F22D060FE9ADC2628(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void CTProcessStartInfo_t18282D67663A337B9F05446EAFAB980BEEA87025_CustomAttributesCacheGenerator_CTProcessStartInfo_get_UseShellExecute_m96AC26978B39FBA130D519C81194A5B6B101792B(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void CTProcessStartInfo_t18282D67663A337B9F05446EAFAB980BEEA87025_CustomAttributesCacheGenerator_CTProcessStartInfo_set_UseShellExecute_m2C838389B42C35BBB7CEA1BC337285F1771849E6(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void CTWebClient_t32C9C802CE03700DC8C37D1952056B588BE7D709_CustomAttributesCacheGenerator_U3CTimeoutU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void CTWebClient_t32C9C802CE03700DC8C37D1952056B588BE7D709_CustomAttributesCacheGenerator_U3CConnectionLimitU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void CTWebClient_t32C9C802CE03700DC8C37D1952056B588BE7D709_CustomAttributesCacheGenerator_CTWebClient_get_Timeout_m05E7AC7AD82CFD0A255A1D753B3AB413E0C15C9E(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void CTWebClient_t32C9C802CE03700DC8C37D1952056B588BE7D709_CustomAttributesCacheGenerator_CTWebClient_set_Timeout_mC93FB85FA7A3F30D35CE10A64A25ABD18D206079(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void CTWebClient_t32C9C802CE03700DC8C37D1952056B588BE7D709_CustomAttributesCacheGenerator_CTWebClient_get_ConnectionLimit_m74A0F4DC526B36E55EA0857E3F953453622189CE(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void CTWebClient_t32C9C802CE03700DC8C37D1952056B588BE7D709_CustomAttributesCacheGenerator_CTWebClient_set_ConnectionLimit_mFBDBF8F1C18BC022FC5B773E3874E8B9569F9068(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void FFTAnalyzer_t9DC55C861DFF72F4457F5CE0CADF148FEA618109_CustomAttributesCacheGenerator_Samples(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x41\x72\x72\x61\x79\x20\x66\x6F\x72\x20\x74\x68\x65\x20\x73\x61\x6D\x70\x6C\x65\x73\x2E\x20\x4D\x6F\x72\x65\x20\x73\x61\x6D\x70\x6C\x65\x73\x20\x6D\x65\x61\x6E\x20\x62\x65\x74\x74\x65\x72\x20\x61\x63\x63\x75\x72\x61\x63\x79\x20\x62\x75\x74\x20\x69\x74\x20\x61\x6C\x73\x6F\x20\x6E\x65\x65\x64\x73\x20\x6D\x6F\x72\x65\x20\x70\x65\x72\x66\x6F\x72\x6D\x61\x6E\x63\x65\x20\x28\x64\x65\x66\x61\x75\x6C\x74\x3A\x20\x32\x35\x36\x29"), NULL);
	}
}
static void FFTAnalyzer_t9DC55C861DFF72F4457F5CE0CADF148FEA618109_CustomAttributesCacheGenerator_Channel(CustomAttributesCache* cache)
{
	{
		RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5 * tmp = (RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5 *)cache->attributes[0];
		RangeAttribute__ctor_mC74D39A9F20DD2A0D4174F05785ABE4F0DAEF000(tmp, 0.0f, 1.0f, NULL);
	}
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[1];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x41\x6E\x61\x6C\x79\x7A\x65\x64\x20\x63\x68\x61\x6E\x6E\x65\x6C\x20\x28\x30\x20\x3D\x20\x72\x69\x67\x68\x74\x2C\x20\x31\x20\x3D\x20\x6C\x65\x66\x74\x2C\x20\x64\x65\x66\x61\x75\x6C\x74\x3A\x20\x30\x29\x2E"), NULL);
	}
}
static void FFTAnalyzer_t9DC55C861DFF72F4457F5CE0CADF148FEA618109_CustomAttributesCacheGenerator_FFTMode(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x46\x46\x54\x2D\x61\x6C\x67\x6F\x72\x69\x74\x68\x6D\x20\x74\x6F\x20\x61\x6E\x61\x6C\x79\x7A\x65\x20\x74\x68\x65\x20\x61\x75\x64\x69\x6F\x20\x28\x64\x65\x66\x61\x75\x6C\x74\x3A\x20\x42\x6C\x61\x63\x6B\x6D\x61\x6E\x48\x61\x72\x72\x69\x73\x29\x2E"), NULL);
	}
}
static void PlatformController_t0109B938FCC3CF39CF034B9211211DB02853B670_CustomAttributesCacheGenerator_Platforms(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x53\x65\x6C\x65\x63\x74\x65\x64\x20\x70\x6C\x61\x74\x66\x6F\x72\x6D\x73\x20\x66\x6F\x72\x20\x74\x68\x65\x20\x63\x6F\x6E\x74\x72\x6F\x6C\x6C\x65\x72\x2E"), NULL);
	}
	{
		HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB * tmp = (HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB *)cache->attributes[1];
		HeaderAttribute__ctor_m601319E0BCE8C44A9E79B2C0ABAAD0FEF46A9F1E(tmp, il2cpp_codegen_string_new_wrapper("\x43\x6F\x6E\x66\x69\x67\x75\x72\x61\x74\x69\x6F\x6E"), NULL);
	}
}
static void PlatformController_t0109B938FCC3CF39CF034B9211211DB02853B670_CustomAttributesCacheGenerator_Active(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x45\x6E\x61\x62\x6C\x65\x20\x6F\x72\x20\x64\x69\x73\x61\x62\x6C\x65\x20\x74\x68\x65\x20\x27\x4F\x62\x6A\x65\x63\x74\x73\x27\x20\x66\x6F\x72\x20\x74\x68\x65\x20\x73\x65\x6C\x65\x63\x74\x65\x64\x20\x27\x50\x6C\x61\x74\x66\x6F\x72\x6D\x73\x27\x20\x28\x64\x65\x66\x61\x75\x6C\x74\x3A\x20\x74\x72\x75\x65\x29\x2E"), NULL);
	}
}
static void PlatformController_t0109B938FCC3CF39CF034B9211211DB02853B670_CustomAttributesCacheGenerator_Objects(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x53\x65\x6C\x65\x63\x74\x65\x64\x20\x6F\x62\x6A\x65\x63\x74\x73\x20\x66\x6F\x72\x20\x74\x68\x65\x20\x63\x6F\x6E\x74\x72\x6F\x6C\x6C\x65\x72\x2E"), NULL);
	}
	{
		HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB * tmp = (HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB *)cache->attributes[1];
		HeaderAttribute__ctor_m601319E0BCE8C44A9E79B2C0ABAAD0FEF46A9F1E(tmp, il2cpp_codegen_string_new_wrapper("\x4F\x62\x6A\x65\x63\x74\x73"), NULL);
	}
}
static void RandomColor_tF868105D2E2823ACD264C32AACFC0C572FFC4C4A_CustomAttributesCacheGenerator_UseInterval(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x55\x73\x65\x20\x69\x6E\x74\x65\x72\x76\x61\x6C\x73\x20\x74\x6F\x20\x63\x68\x61\x6E\x67\x65\x20\x74\x68\x65\x20\x63\x6F\x6C\x6F\x72\x20\x28\x64\x65\x66\x61\x75\x6C\x74\x3A\x20\x74\x72\x75\x65\x29\x2E"), NULL);
	}
}
static void RandomColor_tF868105D2E2823ACD264C32AACFC0C572FFC4C4A_CustomAttributesCacheGenerator_ChangeInterval(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x52\x61\x6E\x64\x6F\x6D\x20\x63\x68\x61\x6E\x67\x65\x20\x69\x6E\x74\x65\x72\x76\x61\x6C\x20\x62\x65\x74\x77\x65\x65\x6E\x20\x6D\x69\x6E\x20\x28\x3D\x20\x78\x29\x20\x61\x6E\x64\x20\x6D\x61\x78\x20\x28\x3D\x20\x79\x29\x20\x69\x6E\x20\x73\x65\x63\x6F\x6E\x64\x73\x20\x28\x64\x65\x66\x61\x75\x6C\x74\x3A\x20\x78\x20\x3D\x20\x35\x2C\x20\x79\x20\x3D\x20\x31\x30\x29\x2E"), NULL);
	}
}
static void RandomColor_tF868105D2E2823ACD264C32AACFC0C572FFC4C4A_CustomAttributesCacheGenerator_HueRange(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x52\x61\x6E\x64\x6F\x6D\x20\x68\x75\x65\x20\x72\x61\x6E\x67\x65\x20\x62\x65\x74\x77\x65\x65\x6E\x20\x6D\x69\x6E\x20\x28\x3D\x20\x78\x29\x20\x61\x6E\x64\x20\x6D\x61\x78\x20\x28\x3D\x20\x79\x29\x20\x28\x64\x65\x66\x61\x75\x6C\x74\x3A\x20\x78\x20\x3D\x20\x30\x2C\x20\x79\x20\x3D\x20\x31\x29\x2E"), NULL);
	}
}
static void RandomColor_tF868105D2E2823ACD264C32AACFC0C572FFC4C4A_CustomAttributesCacheGenerator_SaturationRange(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x52\x61\x6E\x64\x6F\x6D\x20\x73\x61\x74\x75\x72\x61\x74\x69\x6F\x6E\x20\x72\x61\x6E\x67\x65\x20\x62\x65\x74\x77\x65\x65\x6E\x20\x6D\x69\x6E\x20\x28\x3D\x20\x78\x29\x20\x61\x6E\x64\x20\x6D\x61\x78\x20\x28\x3D\x20\x79\x29\x20\x28\x64\x65\x66\x61\x75\x6C\x74\x3A\x20\x78\x20\x3D\x20\x31\x2C\x20\x79\x20\x3D\x20\x31\x29\x2E"), NULL);
	}
}
static void RandomColor_tF868105D2E2823ACD264C32AACFC0C572FFC4C4A_CustomAttributesCacheGenerator_ValueRange(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x52\x61\x6E\x64\x6F\x6D\x20\x76\x61\x6C\x75\x65\x20\x72\x61\x6E\x67\x65\x20\x62\x65\x74\x77\x65\x65\x6E\x20\x6D\x69\x6E\x20\x28\x3D\x20\x78\x29\x20\x61\x6E\x64\x20\x6D\x61\x78\x20\x28\x3D\x20\x79\x29\x20\x28\x64\x65\x66\x61\x75\x6C\x74\x3A\x20\x78\x20\x3D\x20\x31\x2C\x20\x79\x20\x3D\x20\x31\x29\x2E"), NULL);
	}
}
static void RandomColor_tF868105D2E2823ACD264C32AACFC0C572FFC4C4A_CustomAttributesCacheGenerator_AlphaRange(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x52\x61\x6E\x64\x6F\x6D\x20\x61\x6C\x70\x68\x61\x20\x72\x61\x6E\x67\x65\x20\x62\x65\x74\x77\x65\x65\x6E\x20\x6D\x69\x6E\x20\x28\x3D\x20\x78\x29\x20\x61\x6E\x64\x20\x6D\x61\x78\x20\x28\x3D\x20\x79\x29\x20\x28\x64\x65\x66\x61\x75\x6C\x74\x3A\x20\x78\x20\x3D\x20\x31\x2C\x20\x79\x20\x3D\x20\x31\x29\x2E"), NULL);
	}
}
static void RandomColor_tF868105D2E2823ACD264C32AACFC0C572FFC4C4A_CustomAttributesCacheGenerator_GrayScale(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x55\x73\x65\x20\x67\x72\x61\x79\x20\x73\x63\x61\x6C\x65\x20\x63\x6F\x6C\x6F\x72\x73\x20\x28\x64\x65\x66\x61\x75\x6C\x74\x3A\x20\x66\x61\x6C\x73\x65\x29\x2E"), NULL);
	}
}
static void RandomColor_tF868105D2E2823ACD264C32AACFC0C572FFC4C4A_CustomAttributesCacheGenerator_Material(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x4D\x6F\x64\x69\x66\x79\x20\x74\x68\x65\x20\x63\x6F\x6C\x6F\x72\x20\x6F\x66\x20\x61\x20\x6D\x61\x74\x65\x72\x69\x61\x6C\x20\x69\x6E\x73\x74\x65\x61\x64\x20\x6F\x66\x20\x74\x68\x65\x20\x52\x65\x6E\x64\x65\x72\x65\x72\x20\x28\x64\x65\x66\x61\x75\x6C\x74\x3A\x20\x6E\x6F\x74\x20\x73\x65\x74\x2C\x20\x6F\x70\x74\x69\x6F\x6E\x61\x6C\x29\x2E"), NULL);
	}
}
static void RandomColor_tF868105D2E2823ACD264C32AACFC0C572FFC4C4A_CustomAttributesCacheGenerator_RandomColorAtStart(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x53\x65\x74\x20\x74\x68\x65\x20\x6F\x62\x6A\x65\x63\x74\x20\x74\x6F\x20\x61\x20\x72\x61\x6E\x64\x6F\x6D\x20\x63\x6F\x6C\x6F\x72\x20\x61\x74\x20\x53\x74\x61\x72\x74\x20\x28\x64\x65\x66\x61\x75\x6C\x74\x3A\x20\x66\x61\x6C\x73\x65\x29\x2E"), NULL);
	}
}
static void RandomRotator_t2F779136B71B324B8401B297A8978D14D82F4AF8_CustomAttributesCacheGenerator_UseInterval(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x55\x73\x65\x20\x69\x6E\x74\x65\x72\x76\x61\x6C\x73\x20\x74\x6F\x20\x63\x68\x61\x6E\x67\x65\x20\x74\x68\x65\x20\x72\x6F\x74\x61\x74\x69\x6F\x6E\x20\x28\x64\x65\x66\x61\x75\x6C\x74\x3A\x20\x74\x72\x75\x65\x29\x2E"), NULL);
	}
}
static void RandomRotator_t2F779136B71B324B8401B297A8978D14D82F4AF8_CustomAttributesCacheGenerator_ChangeInterval(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x52\x61\x6E\x64\x6F\x6D\x20\x63\x68\x61\x6E\x67\x65\x20\x69\x6E\x74\x65\x72\x76\x61\x6C\x20\x62\x65\x74\x77\x65\x65\x6E\x20\x6D\x69\x6E\x20\x28\x3D\x20\x78\x29\x20\x61\x6E\x64\x20\x6D\x61\x78\x20\x28\x3D\x20\x79\x29\x20\x69\x6E\x20\x73\x65\x63\x6F\x6E\x64\x73\x20\x28\x64\x65\x66\x61\x75\x6C\x74\x3A\x20\x78\x20\x3D\x20\x31\x30\x2C\x20\x79\x20\x3D\x20\x32\x30\x29\x2E"), NULL);
	}
}
static void RandomRotator_t2F779136B71B324B8401B297A8978D14D82F4AF8_CustomAttributesCacheGenerator_SpeedMin(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x4D\x69\x6E\x69\x6D\x75\x6D\x20\x72\x6F\x74\x61\x74\x69\x6F\x6E\x20\x73\x70\x65\x65\x64\x20\x70\x65\x72\x20\x61\x78\x69\x73\x20\x28\x64\x65\x66\x61\x75\x6C\x74\x3A\x20\x35\x20\x66\x6F\x72\x20\x61\x6C\x6C\x20\x61\x78\x69\x73\x29\x2E"), NULL);
	}
}
static void RandomRotator_t2F779136B71B324B8401B297A8978D14D82F4AF8_CustomAttributesCacheGenerator_SpeedMax(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x4D\x69\x6E\x69\x6D\x75\x6D\x20\x72\x6F\x74\x61\x74\x69\x6F\x6E\x20\x73\x70\x65\x65\x64\x20\x70\x65\x72\x20\x61\x78\x69\x73\x20\x28\x64\x65\x66\x61\x75\x6C\x74\x3A\x20\x31\x35\x20\x66\x6F\x72\x20\x61\x6C\x6C\x20\x61\x78\x69\x73\x29\x2E"), NULL);
	}
}
static void RandomRotator_t2F779136B71B324B8401B297A8978D14D82F4AF8_CustomAttributesCacheGenerator_RandomRotationAtStart(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x53\x65\x74\x20\x74\x68\x65\x20\x6F\x62\x6A\x65\x63\x74\x20\x74\x6F\x20\x61\x20\x72\x61\x6E\x64\x6F\x6D\x20\x72\x6F\x74\x61\x74\x69\x6F\x6E\x20\x61\x74\x20\x53\x74\x61\x72\x74\x20\x28\x64\x65\x66\x61\x75\x6C\x74\x3A\x20\x66\x61\x6C\x73\x65\x29\x2E"), NULL);
	}
}
static void RandomScaler_tBD5EA91666AEFA8CF01ED1DD1C749F50A5DB84D3_CustomAttributesCacheGenerator_UseInterval(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x55\x73\x65\x20\x69\x6E\x74\x65\x72\x76\x61\x6C\x73\x20\x74\x6F\x20\x63\x68\x61\x6E\x67\x65\x20\x74\x68\x65\x20\x73\x63\x61\x6C\x65\x20\x28\x64\x65\x66\x61\x75\x6C\x74\x3A\x20\x74\x72\x75\x65\x29\x2E"), NULL);
	}
}
static void RandomScaler_tBD5EA91666AEFA8CF01ED1DD1C749F50A5DB84D3_CustomAttributesCacheGenerator_ChangeInterval(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x52\x61\x6E\x64\x6F\x6D\x20\x63\x68\x61\x6E\x67\x65\x20\x69\x6E\x74\x65\x72\x76\x61\x6C\x20\x62\x65\x74\x77\x65\x65\x6E\x20\x6D\x69\x6E\x20\x28\x3D\x20\x78\x29\x20\x61\x6E\x64\x20\x6D\x61\x78\x20\x28\x3D\x20\x79\x29\x20\x69\x6E\x20\x73\x65\x63\x6F\x6E\x64\x73\x20\x28\x64\x65\x66\x61\x75\x6C\x74\x3A\x20\x78\x20\x3D\x20\x31\x30\x2C\x20\x79\x20\x3D\x20\x32\x30\x29\x2E"), NULL);
	}
}
static void RandomScaler_tBD5EA91666AEFA8CF01ED1DD1C749F50A5DB84D3_CustomAttributesCacheGenerator_ScaleMin(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x4D\x69\x6E\x69\x6D\x75\x6D\x20\x72\x6F\x74\x61\x74\x69\x6F\x6E\x20\x73\x70\x65\x65\x64\x20\x70\x65\x72\x20\x61\x78\x69\x73\x20\x28\x64\x65\x66\x61\x75\x6C\x74\x3A\x20\x35\x20\x66\x6F\x72\x20\x61\x6C\x6C\x20\x61\x78\x69\x73\x29\x2E"), NULL);
	}
}
static void RandomScaler_tBD5EA91666AEFA8CF01ED1DD1C749F50A5DB84D3_CustomAttributesCacheGenerator_ScaleMax(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x4D\x61\x78\x69\x6D\x75\x6D\x20\x73\x63\x61\x6C\x65\x20\x70\x65\x72\x20\x61\x78\x69\x73\x20\x28\x64\x65\x66\x61\x75\x6C\x74\x3A\x20\x30\x2E\x31\x20\x66\x6F\x72\x20\x61\x6C\x6C\x20\x61\x78\x69\x73\x29\x2E"), NULL);
	}
}
static void RandomScaler_tBD5EA91666AEFA8CF01ED1DD1C749F50A5DB84D3_CustomAttributesCacheGenerator_Uniform(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x55\x6E\x69\x66\x6F\x72\x6D\x20\x73\x63\x61\x6C\x69\x6E\x67\x20\x66\x6F\x72\x20\x61\x6C\x6C\x20\x61\x78\x69\x73\x20\x28\x78\x2D\x61\x78\x69\x73\x20\x76\x61\x6C\x75\x65\x73\x20\x77\x69\x6C\x6C\x20\x62\x65\x20\x75\x73\x65\x64\x2C\x20\x64\x65\x66\x61\x75\x6C\x74\x3A\x20\x74\x72\x75\x65\x29\x2E"), NULL);
	}
}
static void RandomScaler_tBD5EA91666AEFA8CF01ED1DD1C749F50A5DB84D3_CustomAttributesCacheGenerator_RandomScaleAtStart(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x53\x65\x74\x20\x74\x68\x65\x20\x6F\x62\x6A\x65\x63\x74\x20\x74\x6F\x20\x61\x20\x72\x61\x6E\x64\x6F\x6D\x20\x73\x63\x61\x6C\x65\x20\x61\x74\x20\x53\x74\x61\x72\x74\x20\x28\x64\x65\x66\x61\x75\x6C\x74\x3A\x20\x66\x61\x6C\x73\x65\x29\x2E"), NULL);
	}
}
static void SpectrumVisualizer_tE42A510857BC8EA32CF8F189BA9ABC5BED66AADA_CustomAttributesCacheGenerator_Analyzer(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x46\x46\x54\x2D\x61\x6E\x61\x6C\x79\x7A\x65\x72\x20\x77\x69\x74\x68\x20\x74\x68\x65\x20\x73\x70\x65\x63\x74\x72\x75\x6D\x20\x64\x61\x74\x61\x2E"), NULL);
	}
}
static void SpectrumVisualizer_tE42A510857BC8EA32CF8F189BA9ABC5BED66AADA_CustomAttributesCacheGenerator_VisualPrefab(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x50\x72\x65\x66\x61\x62\x20\x66\x6F\x72\x20\x74\x68\x65\x20\x66\x72\x65\x71\x75\x65\x6E\x63\x79\x20\x72\x65\x70\x72\x65\x73\x65\x6E\x74\x61\x74\x69\x6F\x6E\x2E"), NULL);
	}
}
static void SpectrumVisualizer_tE42A510857BC8EA32CF8F189BA9ABC5BED66AADA_CustomAttributesCacheGenerator_Width(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x57\x69\x64\x74\x68\x20\x70\x65\x72\x20\x70\x72\x65\x66\x61\x62\x2E"), NULL);
	}
}
static void SpectrumVisualizer_tE42A510857BC8EA32CF8F189BA9ABC5BED66AADA_CustomAttributesCacheGenerator_Gain(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x47\x61\x69\x6E\x2D\x70\x6F\x77\x65\x72\x20\x66\x6F\x72\x20\x74\x68\x65\x20\x66\x72\x65\x71\x75\x65\x6E\x63\x79\x2E"), NULL);
	}
}
static void SpectrumVisualizer_tE42A510857BC8EA32CF8F189BA9ABC5BED66AADA_CustomAttributesCacheGenerator_LeftToRight(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x46\x72\x65\x71\x75\x65\x6E\x63\x79\x20\x62\x61\x6E\x64\x20\x66\x72\x6F\x6D\x20\x6C\x65\x66\x74\x2D\x74\x6F\x2D\x72\x69\x67\x68\x74\x20\x28\x64\x65\x66\x61\x75\x6C\x74\x3A\x20\x74\x72\x75\x65\x29\x2E"), NULL);
	}
}
static void SpectrumVisualizer_tE42A510857BC8EA32CF8F189BA9ABC5BED66AADA_CustomAttributesCacheGenerator_Opacity(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x4F\x70\x61\x63\x69\x74\x79\x20\x6F\x66\x20\x74\x68\x65\x20\x6D\x61\x74\x65\x72\x69\x61\x6C\x20\x6F\x66\x20\x74\x68\x65\x20\x70\x72\x65\x66\x61\x62\x20\x28\x64\x65\x66\x61\x75\x6C\x74\x3A\x20\x31\x29\x2E"), NULL);
	}
	{
		RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5 * tmp = (RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5 *)cache->attributes[1];
		RangeAttribute__ctor_mC74D39A9F20DD2A0D4174F05785ABE4F0DAEF000(tmp, 0.0f, 1.0f, NULL);
	}
}
static void Example01Synthesis_t1DD770E55DA0B4B377769951B07466AEA8271C6F_CustomAttributesCacheGenerator_Example01Synthesis_Start_m08B4479794B479D161F1950CBACF9031DD93A38B(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CStartU3Ed__22_t3F06EB55E379386889BEE897F565664726CFB9B9_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CStartU3Ed__22_t3F06EB55E379386889BEE897F565664726CFB9B9_0_0_0_var), NULL);
	}
}
static void Example01Synthesis_t1DD770E55DA0B4B377769951B07466AEA8271C6F_CustomAttributesCacheGenerator_Example01Synthesis_GetVoices_m5222C0293EAA6FB209232F0A73D75DA1F22C5937(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CGetVoicesU3Ed__23_t4C74B1E95210BDA509B6AB73C0CE5286B905B3A1_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CGetVoicesU3Ed__23_t4C74B1E95210BDA509B6AB73C0CE5286B905B3A1_0_0_0_var), NULL);
	}
}
static void Example01Synthesis_t1DD770E55DA0B4B377769951B07466AEA8271C6F_CustomAttributesCacheGenerator_Example01Synthesis_SetPitch_mC504A0CAE1FDDA39B5B272BDB63B5B4C545017E7(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CSetPitchU3Ed__27_tEFABEA1BBCBB4634FFC95C1C6C6BC2E1AB296B1B_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CSetPitchU3Ed__27_tEFABEA1BBCBB4634FFC95C1C6C6BC2E1AB296B1B_0_0_0_var), NULL);
	}
}
static void Example01Synthesis_t1DD770E55DA0B4B377769951B07466AEA8271C6F_CustomAttributesCacheGenerator_Example01Synthesis_SetRate_mE3D46DECE1A051928BDAAC3C9635D6C7986DCC3E(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CSetRateU3Ed__28_t47111ED4117F0FE7EB7944C25EE6DE198191DDC9_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CSetRateU3Ed__28_t47111ED4117F0FE7EB7944C25EE6DE198191DDC9_0_0_0_var), NULL);
	}
}
static void Example01Synthesis_t1DD770E55DA0B4B377769951B07466AEA8271C6F_CustomAttributesCacheGenerator_Example01Synthesis_U3CStartU3Eb__22_0_m949EFCBA0A18AB6E4A3C65A79A54476776887C4A(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void Example01Synthesis_t1DD770E55DA0B4B377769951B07466AEA8271C6F_CustomAttributesCacheGenerator_Example01Synthesis_U3CStartU3Eb__22_1_mEC15BB057506B9CE63CECC952906E4682748D97E(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void Example01Synthesis_t1DD770E55DA0B4B377769951B07466AEA8271C6F_CustomAttributesCacheGenerator_Example01Synthesis_U3CStartU3Eb__22_2_mC83E3F257936B1EB16E78EA0219E028DD56CBF80(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void Example01Synthesis_t1DD770E55DA0B4B377769951B07466AEA8271C6F_CustomAttributesCacheGenerator_Example01Synthesis_U3CStartU3Eb__22_3_m1255DF35B820D4D4A18882F8EC138AB4D85A2671(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void Example01Synthesis_t1DD770E55DA0B4B377769951B07466AEA8271C6F_CustomAttributesCacheGenerator_Example01Synthesis_U3CStartU3Eb__22_4_m02C7CF99D3995A5F4C83ED560B26073641866E70(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void Example01Synthesis_t1DD770E55DA0B4B377769951B07466AEA8271C6F_CustomAttributesCacheGenerator_Example01Synthesis_U3CGetVoicesU3Eb__23_0_m1B6211E861319A5905671E0865865224B7A85CF2(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void Example01Synthesis_t1DD770E55DA0B4B377769951B07466AEA8271C6F_CustomAttributesCacheGenerator_Example01Synthesis_U3CSetIfReadyForDefaultVoiceU3Eb__24_0_mD6A9EC56F9C352204D6D74AA6C1E21F12E536013(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CStartU3Ed__22_t3F06EB55E379386889BEE897F565664726CFB9B9_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CStartU3Ed__22_t3F06EB55E379386889BEE897F565664726CFB9B9_CustomAttributesCacheGenerator_U3CStartU3Ed__22__ctor_m8F63FDE1DD445BADF80206E5F3E93DDD0680FE84(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CStartU3Ed__22_t3F06EB55E379386889BEE897F565664726CFB9B9_CustomAttributesCacheGenerator_U3CStartU3Ed__22_System_IDisposable_Dispose_mCEAC0024B672F6415833CDE42B9193B11409A425(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CStartU3Ed__22_t3F06EB55E379386889BEE897F565664726CFB9B9_CustomAttributesCacheGenerator_U3CStartU3Ed__22_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m3AD046D2F672A996C8C2414FFFF51CFEE0470407(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CStartU3Ed__22_t3F06EB55E379386889BEE897F565664726CFB9B9_CustomAttributesCacheGenerator_U3CStartU3Ed__22_System_Collections_IEnumerator_Reset_m85E9BF96346CE3D32AEDC2DD1A49612D23AC3EA4(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CStartU3Ed__22_t3F06EB55E379386889BEE897F565664726CFB9B9_CustomAttributesCacheGenerator_U3CStartU3Ed__22_System_Collections_IEnumerator_get_Current_mA1B1C2B5B63C73CB6AC75FA7BB6360950BBDA5B0(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CGetVoicesU3Ed__23_t4C74B1E95210BDA509B6AB73C0CE5286B905B3A1_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CGetVoicesU3Ed__23_t4C74B1E95210BDA509B6AB73C0CE5286B905B3A1_CustomAttributesCacheGenerator_U3CGetVoicesU3Ed__23__ctor_m09932D130C7602C2D2ABE2EDC806CC7F93359C34(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CGetVoicesU3Ed__23_t4C74B1E95210BDA509B6AB73C0CE5286B905B3A1_CustomAttributesCacheGenerator_U3CGetVoicesU3Ed__23_System_IDisposable_Dispose_m2DC14F993D619A7EF6FC06C8267F07CDB90FB520(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CGetVoicesU3Ed__23_t4C74B1E95210BDA509B6AB73C0CE5286B905B3A1_CustomAttributesCacheGenerator_U3CGetVoicesU3Ed__23_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m86945793027A7CCBFD284B2CA99CDB8F25F548F6(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CGetVoicesU3Ed__23_t4C74B1E95210BDA509B6AB73C0CE5286B905B3A1_CustomAttributesCacheGenerator_U3CGetVoicesU3Ed__23_System_Collections_IEnumerator_Reset_mC5A052AA5B31420FF5DB67F371DD9210CEA04744(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CGetVoicesU3Ed__23_t4C74B1E95210BDA509B6AB73C0CE5286B905B3A1_CustomAttributesCacheGenerator_U3CGetVoicesU3Ed__23_System_Collections_IEnumerator_get_Current_m39A69FC140927C187412E3E2DF98D7E1493A857F(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CSetPitchU3Ed__27_tEFABEA1BBCBB4634FFC95C1C6C6BC2E1AB296B1B_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CSetPitchU3Ed__27_tEFABEA1BBCBB4634FFC95C1C6C6BC2E1AB296B1B_CustomAttributesCacheGenerator_U3CSetPitchU3Ed__27__ctor_m003F38176D39FA4206F6183C0D39B1501CD8E9EE(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CSetPitchU3Ed__27_tEFABEA1BBCBB4634FFC95C1C6C6BC2E1AB296B1B_CustomAttributesCacheGenerator_U3CSetPitchU3Ed__27_System_IDisposable_Dispose_m680F577B98258C3E1CC4C275D39BCEEE86722879(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CSetPitchU3Ed__27_tEFABEA1BBCBB4634FFC95C1C6C6BC2E1AB296B1B_CustomAttributesCacheGenerator_U3CSetPitchU3Ed__27_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mD2A3DF093FD380180CCEF194C5D8964455D1ACC6(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CSetPitchU3Ed__27_tEFABEA1BBCBB4634FFC95C1C6C6BC2E1AB296B1B_CustomAttributesCacheGenerator_U3CSetPitchU3Ed__27_System_Collections_IEnumerator_Reset_m80B114456DDDB5BAF8B39F9ADBA407CF1606FA1B(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CSetPitchU3Ed__27_tEFABEA1BBCBB4634FFC95C1C6C6BC2E1AB296B1B_CustomAttributesCacheGenerator_U3CSetPitchU3Ed__27_System_Collections_IEnumerator_get_Current_m12C49895937FE64B85129B8C377E5A3AFF30595E(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CSetRateU3Ed__28_t47111ED4117F0FE7EB7944C25EE6DE198191DDC9_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CSetRateU3Ed__28_t47111ED4117F0FE7EB7944C25EE6DE198191DDC9_CustomAttributesCacheGenerator_U3CSetRateU3Ed__28__ctor_m00BBD7D8FD9177152A89D342340510FF249A036D(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CSetRateU3Ed__28_t47111ED4117F0FE7EB7944C25EE6DE198191DDC9_CustomAttributesCacheGenerator_U3CSetRateU3Ed__28_System_IDisposable_Dispose_m56D7E3D6BD827138BC5D04C0361D36F6F728BD37(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CSetRateU3Ed__28_t47111ED4117F0FE7EB7944C25EE6DE198191DDC9_CustomAttributesCacheGenerator_U3CSetRateU3Ed__28_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m464D800221095630A17ACFB9260ACD0394848238(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CSetRateU3Ed__28_t47111ED4117F0FE7EB7944C25EE6DE198191DDC9_CustomAttributesCacheGenerator_U3CSetRateU3Ed__28_System_Collections_IEnumerator_Reset_m4A460D7A3E015022450C8EBC12A92AC5E0E72327(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CSetRateU3Ed__28_t47111ED4117F0FE7EB7944C25EE6DE198191DDC9_CustomAttributesCacheGenerator_U3CSetRateU3Ed__28_System_Collections_IEnumerator_get_Current_m92D4A794D0F0532A96D04F1F96D0520515BB6BFE(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void Example02Proxy_tA1FB05491FD0E2A79271B8DCB0EEEA305B1C44E9_CustomAttributesCacheGenerator_Example02Proxy_Start_m37473AF15D3767E2869EBCEE623BAE1EA5A3DD74(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CStartU3Ed__21_tFD573D57D53E02687F393696FC999F7C0DDB846E_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CStartU3Ed__21_tFD573D57D53E02687F393696FC999F7C0DDB846E_0_0_0_var), NULL);
	}
}
static void Example02Proxy_tA1FB05491FD0E2A79271B8DCB0EEEA305B1C44E9_CustomAttributesCacheGenerator_Example02Proxy_GetVoices_m5CC03E5F7B7566BE7120A098AF29B93443E8FFEB(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CGetVoicesU3Ed__22_t63C88EA26724AA6C07C27964299A020FC5C9D217_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CGetVoicesU3Ed__22_t63C88EA26724AA6C07C27964299A020FC5C9D217_0_0_0_var), NULL);
	}
}
static void Example02Proxy_tA1FB05491FD0E2A79271B8DCB0EEEA305B1C44E9_CustomAttributesCacheGenerator_Example02Proxy_SetPitch_mB099B881684EA372B6A219FB562B7DE2FBB807EB(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CSetPitchU3Ed__25_t00215C2568848CF0F1B6EC7FD3965ED387698544_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CSetPitchU3Ed__25_t00215C2568848CF0F1B6EC7FD3965ED387698544_0_0_0_var), NULL);
	}
}
static void Example02Proxy_tA1FB05491FD0E2A79271B8DCB0EEEA305B1C44E9_CustomAttributesCacheGenerator_Example02Proxy_SetRate_m5D08473E4431DC7FC479BA30C3C46A7C1AA7496E(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CSetRateU3Ed__26_tB66C71381FE02958DCCB4A1A50724DFFA9A78477_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CSetRateU3Ed__26_tB66C71381FE02958DCCB4A1A50724DFFA9A78477_0_0_0_var), NULL);
	}
}
static void Example02Proxy_tA1FB05491FD0E2A79271B8DCB0EEEA305B1C44E9_CustomAttributesCacheGenerator_Example02Proxy_U3CStartU3Eb__21_0_m9D025B9B4BEF452A16A7982B1D97903206B6932E(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void Example02Proxy_tA1FB05491FD0E2A79271B8DCB0EEEA305B1C44E9_CustomAttributesCacheGenerator_Example02Proxy_U3CStartU3Eb__21_1_m995FFE286128B847EEFC2C8AC1466000A0049BDC(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void Example02Proxy_tA1FB05491FD0E2A79271B8DCB0EEEA305B1C44E9_CustomAttributesCacheGenerator_Example02Proxy_U3CStartU3Eb__21_2_mB754C6C46342D6CA55790E4A35598DE3CB0FAE37(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void Example02Proxy_tA1FB05491FD0E2A79271B8DCB0EEEA305B1C44E9_CustomAttributesCacheGenerator_Example02Proxy_U3CStartU3Eb__21_3_mB22FCD82AF377C1F1E2DB07466B114D5C04EFAFF(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void Example02Proxy_tA1FB05491FD0E2A79271B8DCB0EEEA305B1C44E9_CustomAttributesCacheGenerator_Example02Proxy_U3CStartU3Eb__21_4_m582889A0D069349173DEAECEBF17FBD8D31ACD2D(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void Example02Proxy_tA1FB05491FD0E2A79271B8DCB0EEEA305B1C44E9_CustomAttributesCacheGenerator_Example02Proxy_U3CGetVoicesU3Eb__22_0_m42F9EBAD38B20DA39C2048894A8D388D9DAFC868(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void Example02Proxy_tA1FB05491FD0E2A79271B8DCB0EEEA305B1C44E9_CustomAttributesCacheGenerator_Example02Proxy_U3CSetIfReadyForDefaultVoiceU3Eb__23_0_m91A0BD1BA1727D47E51FF7F98D861C1A3E4C998C(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CStartU3Ed__21_tFD573D57D53E02687F393696FC999F7C0DDB846E_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CStartU3Ed__21_tFD573D57D53E02687F393696FC999F7C0DDB846E_CustomAttributesCacheGenerator_U3CStartU3Ed__21__ctor_m34D03537D704CFF9A884EB90E3B2F486006DFB88(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CStartU3Ed__21_tFD573D57D53E02687F393696FC999F7C0DDB846E_CustomAttributesCacheGenerator_U3CStartU3Ed__21_System_IDisposable_Dispose_m3A2D102BC25169A90D6112DBA9F6CEDB2AB0D302(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CStartU3Ed__21_tFD573D57D53E02687F393696FC999F7C0DDB846E_CustomAttributesCacheGenerator_U3CStartU3Ed__21_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m93363FB8A68A91796C20BAFDA50A3AF8F123EC4A(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CStartU3Ed__21_tFD573D57D53E02687F393696FC999F7C0DDB846E_CustomAttributesCacheGenerator_U3CStartU3Ed__21_System_Collections_IEnumerator_Reset_m58501F0C8729C05E18577FD05B05586917C3AC30(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CStartU3Ed__21_tFD573D57D53E02687F393696FC999F7C0DDB846E_CustomAttributesCacheGenerator_U3CStartU3Ed__21_System_Collections_IEnumerator_get_Current_mA3E9E23A0E3353169EB46A29FA714C083DE7C027(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CGetVoicesU3Ed__22_t63C88EA26724AA6C07C27964299A020FC5C9D217_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CGetVoicesU3Ed__22_t63C88EA26724AA6C07C27964299A020FC5C9D217_CustomAttributesCacheGenerator_U3CGetVoicesU3Ed__22__ctor_m60E3A6BD3C508E0EE96E30CBB6A4C3E39A7B2565(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CGetVoicesU3Ed__22_t63C88EA26724AA6C07C27964299A020FC5C9D217_CustomAttributesCacheGenerator_U3CGetVoicesU3Ed__22_System_IDisposable_Dispose_mD45D8FFE08464334151FFC3DA894F01E8989F0F0(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CGetVoicesU3Ed__22_t63C88EA26724AA6C07C27964299A020FC5C9D217_CustomAttributesCacheGenerator_U3CGetVoicesU3Ed__22_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m203840620BCA464A576234B4503A43D2F4638E10(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CGetVoicesU3Ed__22_t63C88EA26724AA6C07C27964299A020FC5C9D217_CustomAttributesCacheGenerator_U3CGetVoicesU3Ed__22_System_Collections_IEnumerator_Reset_m134E6832088185074D6B6BC66965C2A0161DD2F0(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CGetVoicesU3Ed__22_t63C88EA26724AA6C07C27964299A020FC5C9D217_CustomAttributesCacheGenerator_U3CGetVoicesU3Ed__22_System_Collections_IEnumerator_get_Current_mAAE532A14E45572D45BD62073D30AB28453F23F6(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CSetPitchU3Ed__25_t00215C2568848CF0F1B6EC7FD3965ED387698544_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CSetPitchU3Ed__25_t00215C2568848CF0F1B6EC7FD3965ED387698544_CustomAttributesCacheGenerator_U3CSetPitchU3Ed__25__ctor_m7B27B1C736DD32C058AC4FEDE82949BFC73E63FE(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CSetPitchU3Ed__25_t00215C2568848CF0F1B6EC7FD3965ED387698544_CustomAttributesCacheGenerator_U3CSetPitchU3Ed__25_System_IDisposable_Dispose_mE29FBB4928139BE55235E76AC18A944D31A82F11(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CSetPitchU3Ed__25_t00215C2568848CF0F1B6EC7FD3965ED387698544_CustomAttributesCacheGenerator_U3CSetPitchU3Ed__25_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m349DED4BC47B17F94F69EB6F7730D002F9D2A916(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CSetPitchU3Ed__25_t00215C2568848CF0F1B6EC7FD3965ED387698544_CustomAttributesCacheGenerator_U3CSetPitchU3Ed__25_System_Collections_IEnumerator_Reset_m550C252F8CEFA833ECB2E74ED259602D355615BB(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CSetPitchU3Ed__25_t00215C2568848CF0F1B6EC7FD3965ED387698544_CustomAttributesCacheGenerator_U3CSetPitchU3Ed__25_System_Collections_IEnumerator_get_Current_m7DC2266D9F998979A595E0C599798F981ABC3D9B(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CSetRateU3Ed__26_tB66C71381FE02958DCCB4A1A50724DFFA9A78477_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CSetRateU3Ed__26_tB66C71381FE02958DCCB4A1A50724DFFA9A78477_CustomAttributesCacheGenerator_U3CSetRateU3Ed__26__ctor_m30B3B76B925AD990DF7BD5B8DC85B7676E5A19DC(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CSetRateU3Ed__26_tB66C71381FE02958DCCB4A1A50724DFFA9A78477_CustomAttributesCacheGenerator_U3CSetRateU3Ed__26_System_IDisposable_Dispose_m77B3B26C13157B46E0FECB42F5B0FCE7EDAAA07E(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CSetRateU3Ed__26_tB66C71381FE02958DCCB4A1A50724DFFA9A78477_CustomAttributesCacheGenerator_U3CSetRateU3Ed__26_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mEA8A2AC6CD13B5A30D676B3A36F2669CD9049E95(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CSetRateU3Ed__26_tB66C71381FE02958DCCB4A1A50724DFFA9A78477_CustomAttributesCacheGenerator_U3CSetRateU3Ed__26_System_Collections_IEnumerator_Reset_mC7EA2588C39A3A5DF74047F6295DFEA96E636F43(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CSetRateU3Ed__26_tB66C71381FE02958DCCB4A1A50724DFFA9A78477_CustomAttributesCacheGenerator_U3CSetRateU3Ed__26_System_Collections_IEnumerator_get_Current_m485B95A8B747FDF00C4F4C377B4238929F64A129(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void Example03ProxyManagement_t6DB1BBBA2B376ED4FB5958BBD2FF1498DB419E12_CustomAttributesCacheGenerator_Example03ProxyManagement_Start_mA4DFBA24A44B2606CADEBFC763757B9E1CC3C727(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CStartU3Ed__10_t5B930EBF98D13360F6F542B9931A660A8A3383D9_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CStartU3Ed__10_t5B930EBF98D13360F6F542B9931A660A8A3383D9_0_0_0_var), NULL);
	}
}
static void Example03ProxyManagement_t6DB1BBBA2B376ED4FB5958BBD2FF1498DB419E12_CustomAttributesCacheGenerator_Example03ProxyManagement_U3CStartU3Eb__10_0_mFE057FFD6EE14734A9598B1978E79B8F43AFD718(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void Example03ProxyManagement_t6DB1BBBA2B376ED4FB5958BBD2FF1498DB419E12_CustomAttributesCacheGenerator_Example03ProxyManagement_U3CStartU3Eb__10_1_m86FA78AD1A72F257BF6002F3BCABC772CB4C848B(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void Example03ProxyManagement_t6DB1BBBA2B376ED4FB5958BBD2FF1498DB419E12_CustomAttributesCacheGenerator_Example03ProxyManagement_U3CStartU3Eb__10_2_m6F92CB585A1F54D584FBBEE187E07D7E55C1DC4D(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void Example03ProxyManagement_t6DB1BBBA2B376ED4FB5958BBD2FF1498DB419E12_CustomAttributesCacheGenerator_Example03ProxyManagement_U3CStartU3Eb__10_3_mB46D74519F6952A179CD242E86D73F9D1AE74DE0(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void Example03ProxyManagement_t6DB1BBBA2B376ED4FB5958BBD2FF1498DB419E12_CustomAttributesCacheGenerator_Example03ProxyManagement_U3CStartU3Eb__10_4_m4D57CB44112E5A6ABA7D1836F603D5A410BF819B(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CStartU3Ed__10_t5B930EBF98D13360F6F542B9931A660A8A3383D9_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CStartU3Ed__10_t5B930EBF98D13360F6F542B9931A660A8A3383D9_CustomAttributesCacheGenerator_U3CStartU3Ed__10__ctor_mD2EBEF01559763AEB41147B69F400D9C37BA7249(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CStartU3Ed__10_t5B930EBF98D13360F6F542B9931A660A8A3383D9_CustomAttributesCacheGenerator_U3CStartU3Ed__10_System_IDisposable_Dispose_m5DAC313C765AD7414291F37C5AF18854D673A3A0(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CStartU3Ed__10_t5B930EBF98D13360F6F542B9931A660A8A3383D9_CustomAttributesCacheGenerator_U3CStartU3Ed__10_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m7E1CA9BC1A4949854DB2FC73BA73A82370745DC7(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CStartU3Ed__10_t5B930EBF98D13360F6F542B9931A660A8A3383D9_CustomAttributesCacheGenerator_U3CStartU3Ed__10_System_Collections_IEnumerator_Reset_m6854DE8042609BFACA888413C64DAA593D89FAF4(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CStartU3Ed__10_t5B930EBF98D13360F6F542B9931A660A8A3383D9_CustomAttributesCacheGenerator_U3CStartU3Ed__10_System_Collections_IEnumerator_get_Current_mA22569F7BDAE354C9B873EFC702C0ED39C4827BF(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void Example04SbaitsoClone_t9CC4EC7AD9FE0FCBB0D7B68DA7404F43CBFE2AA3_CustomAttributesCacheGenerator_Example04SbaitsoClone_CreateNameInputField_m7251F4E29A15E8ADA11E793B867807C1BDF53E93(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CCreateNameInputFieldU3Ed__13_tF99DAFC92BB23A5FF3D97BB4B86C12186720DDCE_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CCreateNameInputFieldU3Ed__13_tF99DAFC92BB23A5FF3D97BB4B86C12186720DDCE_0_0_0_var), NULL);
	}
}
static void Example04SbaitsoClone_t9CC4EC7AD9FE0FCBB0D7B68DA7404F43CBFE2AA3_CustomAttributesCacheGenerator_Example04SbaitsoClone_CreateTalkInputField_m31AE5BB29E181784A714AFDCDA0EDAEE14859DA3(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CCreateTalkInputFieldU3Ed__14_t8E84DBC2AA498A934C2001267574FE7D360F832D_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CCreateTalkInputFieldU3Ed__14_t8E84DBC2AA498A934C2001267574FE7D360F832D_0_0_0_var), NULL);
	}
}
static void Example04SbaitsoClone_t9CC4EC7AD9FE0FCBB0D7B68DA7404F43CBFE2AA3_CustomAttributesCacheGenerator_Example04SbaitsoClone_Start_m188E9E8B00F0A4C342849FEAE3D6EA1121799EC9(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CStartU3Ed__19_t61919FD2ED3E01C9A816502E48D6BBE8CC4D515B_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CStartU3Ed__19_t61919FD2ED3E01C9A816502E48D6BBE8CC4D515B_0_0_0_var), NULL);
	}
}
static void Example04SbaitsoClone_t9CC4EC7AD9FE0FCBB0D7B68DA7404F43CBFE2AA3_CustomAttributesCacheGenerator_Example04SbaitsoClone_GetVoices_m3536E808EF94AD1532169BF1E74B3E2FC6946077(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CGetVoicesU3Ed__21_tBCC92EF8427358114BDF9FE975C411924B4B8CF5_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CGetVoicesU3Ed__21_tBCC92EF8427358114BDF9FE975C411924B4B8CF5_0_0_0_var), NULL);
	}
}
static void Example04SbaitsoClone_t9CC4EC7AD9FE0FCBB0D7B68DA7404F43CBFE2AA3_CustomAttributesCacheGenerator_Example04SbaitsoClone_U3CStartU3Eb__19_0_m168596718F058D2D41F4C9484A840635C1BD625A(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void Example04SbaitsoClone_t9CC4EC7AD9FE0FCBB0D7B68DA7404F43CBFE2AA3_CustomAttributesCacheGenerator_Example04SbaitsoClone_U3CGetVoicesU3Eb__21_0_m6C4B0FBBB6A3F4EB878F0C7D48EF5FD29CB034C3(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CCreateNameInputFieldU3Ed__13_tF99DAFC92BB23A5FF3D97BB4B86C12186720DDCE_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CCreateNameInputFieldU3Ed__13_tF99DAFC92BB23A5FF3D97BB4B86C12186720DDCE_CustomAttributesCacheGenerator_U3CCreateNameInputFieldU3Ed__13__ctor_mDBA1413DC9CE6AB47ED198621A1984CF6F696778(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CCreateNameInputFieldU3Ed__13_tF99DAFC92BB23A5FF3D97BB4B86C12186720DDCE_CustomAttributesCacheGenerator_U3CCreateNameInputFieldU3Ed__13_System_IDisposable_Dispose_m76CEC2693009D059E51D719735A8C29C78F2D55F(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CCreateNameInputFieldU3Ed__13_tF99DAFC92BB23A5FF3D97BB4B86C12186720DDCE_CustomAttributesCacheGenerator_U3CCreateNameInputFieldU3Ed__13_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m8AD8BAE973E0D51E605F03CB874C65EDAE7B6303(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CCreateNameInputFieldU3Ed__13_tF99DAFC92BB23A5FF3D97BB4B86C12186720DDCE_CustomAttributesCacheGenerator_U3CCreateNameInputFieldU3Ed__13_System_Collections_IEnumerator_Reset_m29FA913BFECC9BC77B3D8C64D0B5F95E98B3B03B(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CCreateNameInputFieldU3Ed__13_tF99DAFC92BB23A5FF3D97BB4B86C12186720DDCE_CustomAttributesCacheGenerator_U3CCreateNameInputFieldU3Ed__13_System_Collections_IEnumerator_get_Current_m5F9AD182E107360333F5777C391A7412A5DF12F4(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CCreateTalkInputFieldU3Ed__14_t8E84DBC2AA498A934C2001267574FE7D360F832D_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CCreateTalkInputFieldU3Ed__14_t8E84DBC2AA498A934C2001267574FE7D360F832D_CustomAttributesCacheGenerator_U3CCreateTalkInputFieldU3Ed__14__ctor_m280826AEA884301256581138A0DC339D8C684623(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CCreateTalkInputFieldU3Ed__14_t8E84DBC2AA498A934C2001267574FE7D360F832D_CustomAttributesCacheGenerator_U3CCreateTalkInputFieldU3Ed__14_System_IDisposable_Dispose_mB8862146AB8A709DAAB288A0D45BE59D49D9C437(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CCreateTalkInputFieldU3Ed__14_t8E84DBC2AA498A934C2001267574FE7D360F832D_CustomAttributesCacheGenerator_U3CCreateTalkInputFieldU3Ed__14_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mC2563FF15E08EA33D9870DD936C3363F560EF051(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CCreateTalkInputFieldU3Ed__14_t8E84DBC2AA498A934C2001267574FE7D360F832D_CustomAttributesCacheGenerator_U3CCreateTalkInputFieldU3Ed__14_System_Collections_IEnumerator_Reset_mBFFDBE5B05883FD5E838BB70BAD45122DD3EB2D4(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CCreateTalkInputFieldU3Ed__14_t8E84DBC2AA498A934C2001267574FE7D360F832D_CustomAttributesCacheGenerator_U3CCreateTalkInputFieldU3Ed__14_System_Collections_IEnumerator_get_Current_m0535514B983DD7861146E646862AB810E742736D(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CStartU3Ed__19_t61919FD2ED3E01C9A816502E48D6BBE8CC4D515B_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CStartU3Ed__19_t61919FD2ED3E01C9A816502E48D6BBE8CC4D515B_CustomAttributesCacheGenerator_U3CStartU3Ed__19__ctor_m9279FC198E1CF92431BA6E3D7AAC85511DD0FFB7(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CStartU3Ed__19_t61919FD2ED3E01C9A816502E48D6BBE8CC4D515B_CustomAttributesCacheGenerator_U3CStartU3Ed__19_System_IDisposable_Dispose_m96082E25192AA2F4B3C5FD7E21EFF89E5823D3EF(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CStartU3Ed__19_t61919FD2ED3E01C9A816502E48D6BBE8CC4D515B_CustomAttributesCacheGenerator_U3CStartU3Ed__19_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mC09FF475BB8AB91B54583BB90F1C22F2B7F86D97(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CStartU3Ed__19_t61919FD2ED3E01C9A816502E48D6BBE8CC4D515B_CustomAttributesCacheGenerator_U3CStartU3Ed__19_System_Collections_IEnumerator_Reset_m8FF2E99C3BD4919C45F728E2ADF3FC20FD82D798(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CStartU3Ed__19_t61919FD2ED3E01C9A816502E48D6BBE8CC4D515B_CustomAttributesCacheGenerator_U3CStartU3Ed__19_System_Collections_IEnumerator_get_Current_m73BAD620D7E8A75EAC27FA0E5459D5E320A65689(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CGetVoicesU3Ed__21_tBCC92EF8427358114BDF9FE975C411924B4B8CF5_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CGetVoicesU3Ed__21_tBCC92EF8427358114BDF9FE975C411924B4B8CF5_CustomAttributesCacheGenerator_U3CGetVoicesU3Ed__21__ctor_m13BCD013A34C0770207E488A37BEA58FE7CEADF9(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CGetVoicesU3Ed__21_tBCC92EF8427358114BDF9FE975C411924B4B8CF5_CustomAttributesCacheGenerator_U3CGetVoicesU3Ed__21_System_IDisposable_Dispose_m918A7E0FA7B6541257B774BD803300F9CF988D32(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CGetVoicesU3Ed__21_tBCC92EF8427358114BDF9FE975C411924B4B8CF5_CustomAttributesCacheGenerator_U3CGetVoicesU3Ed__21_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m7655E6EBDBCC12EB165CBA18546B85DDDA414F99(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CGetVoicesU3Ed__21_tBCC92EF8427358114BDF9FE975C411924B4B8CF5_CustomAttributesCacheGenerator_U3CGetVoicesU3Ed__21_System_Collections_IEnumerator_Reset_m465989673030024C527CDB60A4963F20BB87CA4C(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CGetVoicesU3Ed__21_tBCC92EF8427358114BDF9FE975C411924B4B8CF5_CustomAttributesCacheGenerator_U3CGetVoicesU3Ed__21_System_Collections_IEnumerator_get_Current_mE6C19EFC27F7715437D206D11126D3F89EF09D1D(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void Example06NoGUI_tF68D48D8FA0C165BBCCB2C13AFE24F66AFBC6289_CustomAttributesCacheGenerator_Example06NoGUI_Start_mD573B09D7C17C0E1C75D8C9E5C7851E79FFB5671(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CStartU3Ed__10_t25226C3095C4900C7D3EC61B87E5A46ECF2CF359_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CStartU3Ed__10_t25226C3095C4900C7D3EC61B87E5A46ECF2CF359_0_0_0_var), NULL);
	}
}
static void Example06NoGUI_tF68D48D8FA0C165BBCCB2C13AFE24F66AFBC6289_CustomAttributesCacheGenerator_Example06NoGUI_GetVoices_m701DB6D05DCBA2C4E0C01EAB95DCF0DE17700F0C(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CGetVoicesU3Ed__11_t766D855FBC27F4F8B65AF9AB254CFF24E3F5F600_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CGetVoicesU3Ed__11_t766D855FBC27F4F8B65AF9AB254CFF24E3F5F600_0_0_0_var), NULL);
	}
}
static void Example06NoGUI_tF68D48D8FA0C165BBCCB2C13AFE24F66AFBC6289_CustomAttributesCacheGenerator_Example06NoGUI_U3CStartU3Eb__10_0_m72EF17BC2CD555921CE715E7C3F3F3219EF145EC(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void Example06NoGUI_tF68D48D8FA0C165BBCCB2C13AFE24F66AFBC6289_CustomAttributesCacheGenerator_Example06NoGUI_U3CGetVoicesU3Eb__11_0_m94B8870A2FEE4697A251BA871BC88897E0E6DD35(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CStartU3Ed__10_t25226C3095C4900C7D3EC61B87E5A46ECF2CF359_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CStartU3Ed__10_t25226C3095C4900C7D3EC61B87E5A46ECF2CF359_CustomAttributesCacheGenerator_U3CStartU3Ed__10__ctor_m8ED59AE01DDBDFB931EE86943CE3BC3848C6ADDD(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CStartU3Ed__10_t25226C3095C4900C7D3EC61B87E5A46ECF2CF359_CustomAttributesCacheGenerator_U3CStartU3Ed__10_System_IDisposable_Dispose_m42B1BC2D8C580E8807BD4FCD43E3929D47E29AEF(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CStartU3Ed__10_t25226C3095C4900C7D3EC61B87E5A46ECF2CF359_CustomAttributesCacheGenerator_U3CStartU3Ed__10_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mAEC49344EDB434739EC4154F2D0B6284D5F79863(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CStartU3Ed__10_t25226C3095C4900C7D3EC61B87E5A46ECF2CF359_CustomAttributesCacheGenerator_U3CStartU3Ed__10_System_Collections_IEnumerator_Reset_m54625ACABF650958F029E6CAA8623766896CE327(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CStartU3Ed__10_t25226C3095C4900C7D3EC61B87E5A46ECF2CF359_CustomAttributesCacheGenerator_U3CStartU3Ed__10_System_Collections_IEnumerator_get_Current_mCF54097F282F03AAEDDE41D140D95B5DC33E51A6(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CGetVoicesU3Ed__11_t766D855FBC27F4F8B65AF9AB254CFF24E3F5F600_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CGetVoicesU3Ed__11_t766D855FBC27F4F8B65AF9AB254CFF24E3F5F600_CustomAttributesCacheGenerator_U3CGetVoicesU3Ed__11__ctor_m2F1AF8243FA5D18261369A410EA1F6F68002DE42(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CGetVoicesU3Ed__11_t766D855FBC27F4F8B65AF9AB254CFF24E3F5F600_CustomAttributesCacheGenerator_U3CGetVoicesU3Ed__11_System_IDisposable_Dispose_m97EE3273B4579EB45B880904B822AD7D8D2A91D1(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CGetVoicesU3Ed__11_t766D855FBC27F4F8B65AF9AB254CFF24E3F5F600_CustomAttributesCacheGenerator_U3CGetVoicesU3Ed__11_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m8CD07E3E6785C4A14570F67E8B754BAD6A9C5C7C(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CGetVoicesU3Ed__11_t766D855FBC27F4F8B65AF9AB254CFF24E3F5F600_CustomAttributesCacheGenerator_U3CGetVoicesU3Ed__11_System_Collections_IEnumerator_Reset_m648E41DC38C5F7CEAEFEA8DD33C737DF7E7DE35E(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CGetVoicesU3Ed__11_t766D855FBC27F4F8B65AF9AB254CFF24E3F5F600_CustomAttributesCacheGenerator_U3CGetVoicesU3Ed__11_System_Collections_IEnumerator_get_Current_m9209DA2EE51310298CE318289E76D2ED29966EB5(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void Example07Simple_tB6BA83E60174A25CF753A57323D56DCA06F41E7A_CustomAttributesCacheGenerator_Example07Simple_Start_m5BEC553E42EFDA38D04730050A02F0468FEB6B85(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CStartU3Ed__9_t36FCBB780B64B702CF4ABD4EBB368E6B34586D7B_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CStartU3Ed__9_t36FCBB780B64B702CF4ABD4EBB368E6B34586D7B_0_0_0_var), NULL);
	}
}
static void Example07Simple_tB6BA83E60174A25CF753A57323D56DCA06F41E7A_CustomAttributesCacheGenerator_Example07Simple_GetVoices_m0C572C4EAEBD69CDD3A6CB7E53A6BD7874413D05(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CGetVoicesU3Ed__10_t89A30C17D49996122417B745EB21C1F275A12870_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CGetVoicesU3Ed__10_t89A30C17D49996122417B745EB21C1F275A12870_0_0_0_var), NULL);
	}
}
static void Example07Simple_tB6BA83E60174A25CF753A57323D56DCA06F41E7A_CustomAttributesCacheGenerator_Example07Simple_U3CStartU3Eb__9_0_m0E131621CB02A42CCFAD90DE45FDD289329E3035(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void Example07Simple_tB6BA83E60174A25CF753A57323D56DCA06F41E7A_CustomAttributesCacheGenerator_Example07Simple_U3CGetVoicesU3Eb__10_0_m0F7FC45ABC08DCB270C219EC8BD94A12EDDCC5E3(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CStartU3Ed__9_t36FCBB780B64B702CF4ABD4EBB368E6B34586D7B_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CStartU3Ed__9_t36FCBB780B64B702CF4ABD4EBB368E6B34586D7B_CustomAttributesCacheGenerator_U3CStartU3Ed__9__ctor_mF5D98CC9248DFAA670B1E1E8DC105AA80B6A3F7A(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CStartU3Ed__9_t36FCBB780B64B702CF4ABD4EBB368E6B34586D7B_CustomAttributesCacheGenerator_U3CStartU3Ed__9_System_IDisposable_Dispose_m0A53CD40C8A18435CED5140A56604E7716F04E54(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CStartU3Ed__9_t36FCBB780B64B702CF4ABD4EBB368E6B34586D7B_CustomAttributesCacheGenerator_U3CStartU3Ed__9_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m1A2A789E1298A2DF842422EC709EF1D338316C5B(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CStartU3Ed__9_t36FCBB780B64B702CF4ABD4EBB368E6B34586D7B_CustomAttributesCacheGenerator_U3CStartU3Ed__9_System_Collections_IEnumerator_Reset_m5A0F287025DEEE61494DC0B746B49472E5107D88(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CStartU3Ed__9_t36FCBB780B64B702CF4ABD4EBB368E6B34586D7B_CustomAttributesCacheGenerator_U3CStartU3Ed__9_System_Collections_IEnumerator_get_Current_mA91D397DE5EF1EC8FF80A4687A787A8BB5B0C9AB(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CGetVoicesU3Ed__10_t89A30C17D49996122417B745EB21C1F275A12870_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CGetVoicesU3Ed__10_t89A30C17D49996122417B745EB21C1F275A12870_CustomAttributesCacheGenerator_U3CGetVoicesU3Ed__10__ctor_m745BB18F289E59888EBC4DA97E51151670AB0B10(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CGetVoicesU3Ed__10_t89A30C17D49996122417B745EB21C1F275A12870_CustomAttributesCacheGenerator_U3CGetVoicesU3Ed__10_System_IDisposable_Dispose_m8EEED7ADFA85960ED3B9A9465D4E64C1C6338589(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CGetVoicesU3Ed__10_t89A30C17D49996122417B745EB21C1F275A12870_CustomAttributesCacheGenerator_U3CGetVoicesU3Ed__10_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mE112EB43A922DCD1F08557C0B2EB65ED0BF27869(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CGetVoicesU3Ed__10_t89A30C17D49996122417B745EB21C1F275A12870_CustomAttributesCacheGenerator_U3CGetVoicesU3Ed__10_System_Collections_IEnumerator_Reset_mE597695CBDFD7A5FF951AFF58AE3451AF0BECA1D(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CGetVoicesU3Ed__10_t89A30C17D49996122417B745EB21C1F275A12870_CustomAttributesCacheGenerator_U3CGetVoicesU3Ed__10_System_Collections_IEnumerator_get_Current_m642317F9B85A2F6F491C70917B7FC44742E9DBBF(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void ProxySpeechSynthesisPlugin_t450E4A4888CF497D59C596CEF838904AB9B95414_CustomAttributesCacheGenerator_ProxySpeechSynthesisPlugin_Init_m2D4B203DDA9D67986B9456F93CBE0194D2FAE051(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CInitU3Ed__12_tD8DCA3016689D1AC98B5293D2E98515302FC46F6_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CInitU3Ed__12_tD8DCA3016689D1AC98B5293D2E98515302FC46F6_0_0_0_var), NULL);
	}
}
static void ProxySpeechSynthesisPlugin_t450E4A4888CF497D59C596CEF838904AB9B95414_CustomAttributesCacheGenerator_ProxySpeechSynthesisPlugin_RunPendingCommands_m61C1E0632B9CE5E8FA6DF8112E2BD8C0A65CE50D(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CRunPendingCommandsU3Ed__13_t7581E10AF39B06A7884DCFFC6F826010995D4F7F_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CRunPendingCommandsU3Ed__13_t7581E10AF39B06A7884DCFFC6F826010995D4F7F_0_0_0_var), NULL);
	}
}
static void ProxySpeechSynthesisPlugin_t450E4A4888CF497D59C596CEF838904AB9B95414_CustomAttributesCacheGenerator_ProxySpeechSynthesisPlugin_ProxyUtterance_m00F041E6FB4EE1F8D0485464E200BBEF1AF98D49(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CProxyUtteranceU3Ed__14_t8193D4147E2CE182457EAEF45EDACC550B23BCCF_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CProxyUtteranceU3Ed__14_t8193D4147E2CE182457EAEF45EDACC550B23BCCF_0_0_0_var), NULL);
	}
}
static void ProxySpeechSynthesisPlugin_t450E4A4888CF497D59C596CEF838904AB9B95414_CustomAttributesCacheGenerator_ProxySpeechSynthesisPlugin_ProxyVoices_m0318C1715E4EDDCCE6B378C7DA66F82348EA530A(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CProxyVoicesU3Ed__15_tD895D1F2A7ED22311F8F67BAF0D4593B7AF528B9_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CProxyVoicesU3Ed__15_tD895D1F2A7ED22311F8F67BAF0D4593B7AF528B9_0_0_0_var), NULL);
	}
}
static void ProxySpeechSynthesisPlugin_t450E4A4888CF497D59C596CEF838904AB9B95414_CustomAttributesCacheGenerator_ProxySpeechSynthesisPlugin_ProxyOnEnd_mE91F7F13A45699A8A258E6BD90A4DDD310F9AE5E(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CProxyOnEndU3Ed__16_t496FEB12873920FC762FFCB8CFEC07F32B3F5DBC_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CProxyOnEndU3Ed__16_t496FEB12873920FC762FFCB8CFEC07F32B3F5DBC_0_0_0_var), NULL);
	}
}
static void U3CInitU3Ed__12_tD8DCA3016689D1AC98B5293D2E98515302FC46F6_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CInitU3Ed__12_tD8DCA3016689D1AC98B5293D2E98515302FC46F6_CustomAttributesCacheGenerator_U3CInitU3Ed__12__ctor_m76C78B57D3E077558A1B64BD3C60394348A55EB7(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CInitU3Ed__12_tD8DCA3016689D1AC98B5293D2E98515302FC46F6_CustomAttributesCacheGenerator_U3CInitU3Ed__12_System_IDisposable_Dispose_mCF76C3D929D98063E6D635DBE241A28010B2419C(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CInitU3Ed__12_tD8DCA3016689D1AC98B5293D2E98515302FC46F6_CustomAttributesCacheGenerator_U3CInitU3Ed__12_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mA1875409A854BC8BCBD1706ADC7DE32EC5FC79BF(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CInitU3Ed__12_tD8DCA3016689D1AC98B5293D2E98515302FC46F6_CustomAttributesCacheGenerator_U3CInitU3Ed__12_System_Collections_IEnumerator_Reset_m172521A167F15E22BE4AB993B191F3CB20ABE238(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CInitU3Ed__12_tD8DCA3016689D1AC98B5293D2E98515302FC46F6_CustomAttributesCacheGenerator_U3CInitU3Ed__12_System_Collections_IEnumerator_get_Current_mC582877BCF04920CDC9C5837E6B7AC27286781DF(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CRunPendingCommandsU3Ed__13_t7581E10AF39B06A7884DCFFC6F826010995D4F7F_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CRunPendingCommandsU3Ed__13_t7581E10AF39B06A7884DCFFC6F826010995D4F7F_CustomAttributesCacheGenerator_U3CRunPendingCommandsU3Ed__13__ctor_mF755B6F406B21FCDBA541DD1F627B61DBA9D7B38(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CRunPendingCommandsU3Ed__13_t7581E10AF39B06A7884DCFFC6F826010995D4F7F_CustomAttributesCacheGenerator_U3CRunPendingCommandsU3Ed__13_System_IDisposable_Dispose_m5553B88D973F8D223B7C2E1988E707DA5903BFEE(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CRunPendingCommandsU3Ed__13_t7581E10AF39B06A7884DCFFC6F826010995D4F7F_CustomAttributesCacheGenerator_U3CRunPendingCommandsU3Ed__13_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mE6332DB63594041697D2F788EB9A569EFA45F9E6(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CRunPendingCommandsU3Ed__13_t7581E10AF39B06A7884DCFFC6F826010995D4F7F_CustomAttributesCacheGenerator_U3CRunPendingCommandsU3Ed__13_System_Collections_IEnumerator_Reset_mDA11BAC1A537E93CA1D2FB62F55A1BF67AE7C422(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CRunPendingCommandsU3Ed__13_t7581E10AF39B06A7884DCFFC6F826010995D4F7F_CustomAttributesCacheGenerator_U3CRunPendingCommandsU3Ed__13_System_Collections_IEnumerator_get_Current_mA480802E726F9F7FE60F4D244CDC4C7E947A661B(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CProxyUtteranceU3Ed__14_t8193D4147E2CE182457EAEF45EDACC550B23BCCF_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CProxyUtteranceU3Ed__14_t8193D4147E2CE182457EAEF45EDACC550B23BCCF_CustomAttributesCacheGenerator_U3CProxyUtteranceU3Ed__14__ctor_m6B50B1BC665C70213F42952F27D32620696E2215(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CProxyUtteranceU3Ed__14_t8193D4147E2CE182457EAEF45EDACC550B23BCCF_CustomAttributesCacheGenerator_U3CProxyUtteranceU3Ed__14_System_IDisposable_Dispose_mBD5CFAAAECAC03CE4AEBE083261B789E45C93183(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CProxyUtteranceU3Ed__14_t8193D4147E2CE182457EAEF45EDACC550B23BCCF_CustomAttributesCacheGenerator_U3CProxyUtteranceU3Ed__14_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m0A1B3F6C03C24C597E853792C14E8CBC3EC4E787(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CProxyUtteranceU3Ed__14_t8193D4147E2CE182457EAEF45EDACC550B23BCCF_CustomAttributesCacheGenerator_U3CProxyUtteranceU3Ed__14_System_Collections_IEnumerator_Reset_m33B544F80C9BD6E23E7EC8CAF68D3BDDE5434177(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CProxyUtteranceU3Ed__14_t8193D4147E2CE182457EAEF45EDACC550B23BCCF_CustomAttributesCacheGenerator_U3CProxyUtteranceU3Ed__14_System_Collections_IEnumerator_get_Current_mFDE586AB0058DAE09E26C1649BF6CE1446B4B4C0(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CProxyVoicesU3Ed__15_tD895D1F2A7ED22311F8F67BAF0D4593B7AF528B9_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CProxyVoicesU3Ed__15_tD895D1F2A7ED22311F8F67BAF0D4593B7AF528B9_CustomAttributesCacheGenerator_U3CProxyVoicesU3Ed__15__ctor_m9D51FA49B63EA5B2D6D4741F9AD4638E9F3768D1(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CProxyVoicesU3Ed__15_tD895D1F2A7ED22311F8F67BAF0D4593B7AF528B9_CustomAttributesCacheGenerator_U3CProxyVoicesU3Ed__15_System_IDisposable_Dispose_m125C573F8DCD66E614ED372FE4BEECB946D19048(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CProxyVoicesU3Ed__15_tD895D1F2A7ED22311F8F67BAF0D4593B7AF528B9_CustomAttributesCacheGenerator_U3CProxyVoicesU3Ed__15_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m5AD44B4EDBDC4428C3026163414865806493DE1A(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CProxyVoicesU3Ed__15_tD895D1F2A7ED22311F8F67BAF0D4593B7AF528B9_CustomAttributesCacheGenerator_U3CProxyVoicesU3Ed__15_System_Collections_IEnumerator_Reset_mB95FFE93FD0F160DDC6170F5515C3F7122F676FD(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CProxyVoicesU3Ed__15_tD895D1F2A7ED22311F8F67BAF0D4593B7AF528B9_CustomAttributesCacheGenerator_U3CProxyVoicesU3Ed__15_System_Collections_IEnumerator_get_Current_mB48913C5ECA3605995DCCF3E055E8332181C0745(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CProxyOnEndU3Ed__16_t496FEB12873920FC762FFCB8CFEC07F32B3F5DBC_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CProxyOnEndU3Ed__16_t496FEB12873920FC762FFCB8CFEC07F32B3F5DBC_CustomAttributesCacheGenerator_U3CProxyOnEndU3Ed__16__ctor_m6D7E54EB078F354B70FD6C4FFDD910AE9485008A(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CProxyOnEndU3Ed__16_t496FEB12873920FC762FFCB8CFEC07F32B3F5DBC_CustomAttributesCacheGenerator_U3CProxyOnEndU3Ed__16_System_IDisposable_Dispose_m6A373202B6709BFEAF7D17EBD44817066FEAF057(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CProxyOnEndU3Ed__16_t496FEB12873920FC762FFCB8CFEC07F32B3F5DBC_CustomAttributesCacheGenerator_U3CProxyOnEndU3Ed__16_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m4335A867E6CDA8B392ECCC15F6E8C1ACFDE5BDFB(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CProxyOnEndU3Ed__16_t496FEB12873920FC762FFCB8CFEC07F32B3F5DBC_CustomAttributesCacheGenerator_U3CProxyOnEndU3Ed__16_System_Collections_IEnumerator_Reset_mF623461CC34757BD5120223664D337EF6492EAC4(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CProxyOnEndU3Ed__16_t496FEB12873920FC762FFCB8CFEC07F32B3F5DBC_CustomAttributesCacheGenerator_U3CProxyOnEndU3Ed__16_System_Collections_IEnumerator_get_Current_m7C1A32E5A25138F22368DD2B3C9E66C2BABFA8F9(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void SpeechSynthesisUtils_t09D6E455F5A2390DF9AF4AABD4E4D80EACC27BBA_CustomAttributesCacheGenerator_SpeechSynthesisUtils_SetActive_m1B7D834516CFBA8840500E95C0C190B85626AB8E____args1(CustomAttributesCache* cache)
{
	{
		ParamArrayAttribute_t9DCEB4CDDB8EDDB1124171D4C51FA6069EEA5C5F * tmp = (ParamArrayAttribute_t9DCEB4CDDB8EDDB1124171D4C51FA6069EEA5C5F *)cache->attributes[0];
		ParamArrayAttribute__ctor_mCC72AFF718185BA7B87FD8D9471F1274400C5719(tmp, NULL);
	}
}
static void SpeechSynthesisUtils_t09D6E455F5A2390DF9AF4AABD4E4D80EACC27BBA_CustomAttributesCacheGenerator_SpeechSynthesisUtils_SetInteractable_m0D993F7363AB78CFBA8052B611937D418C0ED8B4____args1(CustomAttributesCache* cache)
{
	{
		ParamArrayAttribute_t9DCEB4CDDB8EDDB1124171D4C51FA6069EEA5C5F * tmp = (ParamArrayAttribute_t9DCEB4CDDB8EDDB1124171D4C51FA6069EEA5C5F *)cache->attributes[0];
		ParamArrayAttribute__ctor_mCC72AFF718185BA7B87FD8D9471F1274400C5719(tmp, NULL);
	}
}
static void WebGLSpeechSynthesisPlugin_t8211A6663E20D5C0E8847B529BE5A82C197BCBE8_CustomAttributesCacheGenerator_WebGLSpeechSynthesisPlugin_ProxyOnEnd_m4C05BABE8475953BE0C7F45E9458ACAFC99B0144(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CProxyOnEndU3Ed__3_tB3A6640F746B233069CBE83B8CE0930F714BCAAA_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CProxyOnEndU3Ed__3_tB3A6640F746B233069CBE83B8CE0930F714BCAAA_0_0_0_var), NULL);
	}
}
static void U3CProxyOnEndU3Ed__3_tB3A6640F746B233069CBE83B8CE0930F714BCAAA_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CProxyOnEndU3Ed__3_tB3A6640F746B233069CBE83B8CE0930F714BCAAA_CustomAttributesCacheGenerator_U3CProxyOnEndU3Ed__3__ctor_mA20A9FD5B9CA14D3E246E77F2E753B84137DBA2C(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CProxyOnEndU3Ed__3_tB3A6640F746B233069CBE83B8CE0930F714BCAAA_CustomAttributesCacheGenerator_U3CProxyOnEndU3Ed__3_System_IDisposable_Dispose_mB6A59AC6CCFBEACE049F002037CA85CDD8C6C1A1(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CProxyOnEndU3Ed__3_tB3A6640F746B233069CBE83B8CE0930F714BCAAA_CustomAttributesCacheGenerator_U3CProxyOnEndU3Ed__3_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m23180814A7DB3E1CE1C1068FF09F1BB43810BC9C(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CProxyOnEndU3Ed__3_tB3A6640F746B233069CBE83B8CE0930F714BCAAA_CustomAttributesCacheGenerator_U3CProxyOnEndU3Ed__3_System_Collections_IEnumerator_Reset_m5214C4EBB3412E7401F9943FD182087458B09AED(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CProxyOnEndU3Ed__3_tB3A6640F746B233069CBE83B8CE0930F714BCAAA_CustomAttributesCacheGenerator_U3CProxyOnEndU3Ed__3_System_Collections_IEnumerator_get_Current_m1A3AC4C48B1827D4A820083059E5C907FE3395AB(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void Example01Dictation_tA035C7751CA3D6598C3FFC4BB7B4F62761657DF1_CustomAttributesCacheGenerator_Example01Dictation_Start_m2DD9CDBF6C2F5FDB3F69125D1188C11D1CF28ABE(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CStartU3Ed__10_t84D96784539E4E6C4F0B0D80CF2BE76078D3C65B_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CStartU3Ed__10_t84D96784539E4E6C4F0B0D80CF2BE76078D3C65B_0_0_0_var), NULL);
	}
}
static void Example01Dictation_tA035C7751CA3D6598C3FFC4BB7B4F62761657DF1_CustomAttributesCacheGenerator_Example01Dictation_U3CStartU3Eb__10_0_m324C5EB1646129DCE15C60593D843CF0CEEFBA00(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void Example01Dictation_tA035C7751CA3D6598C3FFC4BB7B4F62761657DF1_CustomAttributesCacheGenerator_Example01Dictation_U3CStartU3Eb__10_1_m4D6C29A76FA1387309A88B31FEB428A82B256190(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void Example01Dictation_tA035C7751CA3D6598C3FFC4BB7B4F62761657DF1_CustomAttributesCacheGenerator_Example01Dictation_U3CStartU3Eb__10_2_mA8B03413D355942B8391D9237DCDE718BEB8DCD6(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CStartU3Ed__10_t84D96784539E4E6C4F0B0D80CF2BE76078D3C65B_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CStartU3Ed__10_t84D96784539E4E6C4F0B0D80CF2BE76078D3C65B_CustomAttributesCacheGenerator_U3CStartU3Ed__10__ctor_m9A5ADA41AD9640CCCF47CF94DF79A682A6F3B310(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CStartU3Ed__10_t84D96784539E4E6C4F0B0D80CF2BE76078D3C65B_CustomAttributesCacheGenerator_U3CStartU3Ed__10_System_IDisposable_Dispose_m23F5925258986EB4AFC343608A32AF127CD7640B(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CStartU3Ed__10_t84D96784539E4E6C4F0B0D80CF2BE76078D3C65B_CustomAttributesCacheGenerator_U3CStartU3Ed__10_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m20845EAD2D482F70B9F51BCF13959349EFCE3AF2(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CStartU3Ed__10_t84D96784539E4E6C4F0B0D80CF2BE76078D3C65B_CustomAttributesCacheGenerator_U3CStartU3Ed__10_System_Collections_IEnumerator_Reset_mCAC9839D3729DBAFC1C7A33DEFCED8AE1E914CF8(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CStartU3Ed__10_t84D96784539E4E6C4F0B0D80CF2BE76078D3C65B_CustomAttributesCacheGenerator_U3CStartU3Ed__10_System_Collections_IEnumerator_get_Current_m961FFD432D91B04C6F056699AE4A60F11B7CDBBA(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void Example02SpeechCommands_tC44C4F95779FC04506C676C8557257A9F8F82836_CustomAttributesCacheGenerator_Example02SpeechCommands_Start_m3B392D38DE9534CB1DFDDB6ED8A309B09C10CAD1(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CStartU3Ed__9_t9F2C26950487FA4AA558AF9FDBC70EFC86CA1C3A_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CStartU3Ed__9_t9F2C26950487FA4AA558AF9FDBC70EFC86CA1C3A_0_0_0_var), NULL);
	}
}
static void Example02SpeechCommands_tC44C4F95779FC04506C676C8557257A9F8F82836_CustomAttributesCacheGenerator_Example02SpeechCommands_U3CStartU3Eb__9_0_mAF3718081A86C5DEE426CE29056C45F5D372FDA7(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void Example02SpeechCommands_tC44C4F95779FC04506C676C8557257A9F8F82836_CustomAttributesCacheGenerator_Example02SpeechCommands_U3CStartU3Eb__9_1_mCDAAE9C0F8DE0861443E9766CB560B80D6080D9E(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void Example02SpeechCommands_tC44C4F95779FC04506C676C8557257A9F8F82836_CustomAttributesCacheGenerator_Example02SpeechCommands_U3CStartU3Eb__9_2_m723537E14FC6C80D2737CC31D7FE370B62BBA08B(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CStartU3Ed__9_t9F2C26950487FA4AA558AF9FDBC70EFC86CA1C3A_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CStartU3Ed__9_t9F2C26950487FA4AA558AF9FDBC70EFC86CA1C3A_CustomAttributesCacheGenerator_U3CStartU3Ed__9__ctor_m1DCC4242CF1D0D731B6126E7F5B708942A1072DB(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CStartU3Ed__9_t9F2C26950487FA4AA558AF9FDBC70EFC86CA1C3A_CustomAttributesCacheGenerator_U3CStartU3Ed__9_System_IDisposable_Dispose_m5EB8C4ADEBBD2ED5CCBE2F328282892CB6884DF4(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CStartU3Ed__9_t9F2C26950487FA4AA558AF9FDBC70EFC86CA1C3A_CustomAttributesCacheGenerator_U3CStartU3Ed__9_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m43876813CB26CDF2721AF751DB3E93BB453730EC(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CStartU3Ed__9_t9F2C26950487FA4AA558AF9FDBC70EFC86CA1C3A_CustomAttributesCacheGenerator_U3CStartU3Ed__9_System_Collections_IEnumerator_Reset_m7617645E4D536DB41476515F5F849965F8335D85(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CStartU3Ed__9_t9F2C26950487FA4AA558AF9FDBC70EFC86CA1C3A_CustomAttributesCacheGenerator_U3CStartU3Ed__9_System_Collections_IEnumerator_get_Current_m6B1E12CCB938CD7A198A113D6EE2CC277AA32A67(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void Example02Word_tB6B0FA16CB679D32498336436DC942D6E87177B2_CustomAttributesCacheGenerator_Example02Word_DoHighlight_m670F10C8CF6785E291095095AEEDF3C8A30E285C(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CDoHighlightU3Ed__5_tF820C5AFB9FD264363835FEA11DC3D2FAB920E17_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CDoHighlightU3Ed__5_tF820C5AFB9FD264363835FEA11DC3D2FAB920E17_0_0_0_var), NULL);
	}
}
static void U3CDoHighlightU3Ed__5_tF820C5AFB9FD264363835FEA11DC3D2FAB920E17_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CDoHighlightU3Ed__5_tF820C5AFB9FD264363835FEA11DC3D2FAB920E17_CustomAttributesCacheGenerator_U3CDoHighlightU3Ed__5__ctor_mD5AE312E764E3292D8DD5D158217B95A9CF70565(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CDoHighlightU3Ed__5_tF820C5AFB9FD264363835FEA11DC3D2FAB920E17_CustomAttributesCacheGenerator_U3CDoHighlightU3Ed__5_System_IDisposable_Dispose_m2F8B3A79BFBE378256C8DB81C7E72C2793E1E7B6(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CDoHighlightU3Ed__5_tF820C5AFB9FD264363835FEA11DC3D2FAB920E17_CustomAttributesCacheGenerator_U3CDoHighlightU3Ed__5_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m3E50ECEB225715936D40ED6CFAA54EF88A73A6E5(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CDoHighlightU3Ed__5_tF820C5AFB9FD264363835FEA11DC3D2FAB920E17_CustomAttributesCacheGenerator_U3CDoHighlightU3Ed__5_System_Collections_IEnumerator_Reset_m58923D5EDD1686E38EACEBE6919E2C3818A069DD(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CDoHighlightU3Ed__5_tF820C5AFB9FD264363835FEA11DC3D2FAB920E17_CustomAttributesCacheGenerator_U3CDoHighlightU3Ed__5_System_Collections_IEnumerator_get_Current_m37093F8D22C85DD3FC980F42F64A16F25EB20E61(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void Example03ProxyCommands_t64DC44AB0195FEEAB0CBFFE8EF7C43C38CA8F209_CustomAttributesCacheGenerator_Example03ProxyCommands_Start_m75C3CACF576092A6AEDAB46949555EBF3BC5CE3A(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CStartU3Ed__9_t0AC6DFD8163B675E7D948040950A9AD90DE2F27F_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CStartU3Ed__9_t0AC6DFD8163B675E7D948040950A9AD90DE2F27F_0_0_0_var), NULL);
	}
}
static void Example03ProxyCommands_t64DC44AB0195FEEAB0CBFFE8EF7C43C38CA8F209_CustomAttributesCacheGenerator_Example03ProxyCommands_U3CStartU3Eb__9_0_m1A9B4FAC6449AC5A6B6F60CBA33E6A008B14B5D0(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void Example03ProxyCommands_t64DC44AB0195FEEAB0CBFFE8EF7C43C38CA8F209_CustomAttributesCacheGenerator_Example03ProxyCommands_U3CStartU3Eb__9_1_m59BF70008830BB6F5952C527F5E824A6609DE99A(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void Example03ProxyCommands_t64DC44AB0195FEEAB0CBFFE8EF7C43C38CA8F209_CustomAttributesCacheGenerator_Example03ProxyCommands_U3CStartU3Eb__9_2_m80788C4BD22A86C43951D086E1CA8E2D6B19C09B(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CStartU3Ed__9_t0AC6DFD8163B675E7D948040950A9AD90DE2F27F_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CStartU3Ed__9_t0AC6DFD8163B675E7D948040950A9AD90DE2F27F_CustomAttributesCacheGenerator_U3CStartU3Ed__9__ctor_m3F78AF5ED20089723D623C905AB064288A96CD0E(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CStartU3Ed__9_t0AC6DFD8163B675E7D948040950A9AD90DE2F27F_CustomAttributesCacheGenerator_U3CStartU3Ed__9_System_IDisposable_Dispose_m1C60169E1C0BB3885A9C16EF36572BA72860FA87(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CStartU3Ed__9_t0AC6DFD8163B675E7D948040950A9AD90DE2F27F_CustomAttributesCacheGenerator_U3CStartU3Ed__9_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m9D4EE49E43C98748228A6554736CAA494FFAF419(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CStartU3Ed__9_t0AC6DFD8163B675E7D948040950A9AD90DE2F27F_CustomAttributesCacheGenerator_U3CStartU3Ed__9_System_Collections_IEnumerator_Reset_mA96F9B0685F82F2F9B24923E4B4C58B943E84D55(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CStartU3Ed__9_t0AC6DFD8163B675E7D948040950A9AD90DE2F27F_CustomAttributesCacheGenerator_U3CStartU3Ed__9_System_Collections_IEnumerator_get_Current_m760831DCCDB96CF1185E0FBE602E4286E80C456F(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void Example04ProxyDictation_t85D92F502A55D4B6ED47CF8697F6D736FC91EE03_CustomAttributesCacheGenerator_Example04ProxyDictation_Start_mA1D2D0C1D6CDD82A052581989B6078A6E282C465(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CStartU3Ed__10_t4C950ADFDCBB18E14D5159D1D6BC6D49ABEC06D0_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CStartU3Ed__10_t4C950ADFDCBB18E14D5159D1D6BC6D49ABEC06D0_0_0_0_var), NULL);
	}
}
static void Example04ProxyDictation_t85D92F502A55D4B6ED47CF8697F6D736FC91EE03_CustomAttributesCacheGenerator_Example04ProxyDictation_U3CStartU3Eb__10_0_m135A7C20424A1634400FCF9A0B2F98FA8457F588(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void Example04ProxyDictation_t85D92F502A55D4B6ED47CF8697F6D736FC91EE03_CustomAttributesCacheGenerator_Example04ProxyDictation_U3CStartU3Eb__10_1_mCA1C4A134D714558B80BC0D4AA7CB81B1C901EE1(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void Example04ProxyDictation_t85D92F502A55D4B6ED47CF8697F6D736FC91EE03_CustomAttributesCacheGenerator_Example04ProxyDictation_U3CStartU3Eb__10_2_m7405692423C6979639B7A85D1643E52FC884B703(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CStartU3Ed__10_t4C950ADFDCBB18E14D5159D1D6BC6D49ABEC06D0_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CStartU3Ed__10_t4C950ADFDCBB18E14D5159D1D6BC6D49ABEC06D0_CustomAttributesCacheGenerator_U3CStartU3Ed__10__ctor_mD08E65BE8117B1AC5F05D4FE68ED1115D16B41AD(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CStartU3Ed__10_t4C950ADFDCBB18E14D5159D1D6BC6D49ABEC06D0_CustomAttributesCacheGenerator_U3CStartU3Ed__10_System_IDisposable_Dispose_m2E8EDC3CFF0CBE8EC3721E3E2C73DA2B36DB2A81(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CStartU3Ed__10_t4C950ADFDCBB18E14D5159D1D6BC6D49ABEC06D0_CustomAttributesCacheGenerator_U3CStartU3Ed__10_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mC8E450D1D2D986E4B2BB9CC67E3EB6C6AA0D2C2E(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CStartU3Ed__10_t4C950ADFDCBB18E14D5159D1D6BC6D49ABEC06D0_CustomAttributesCacheGenerator_U3CStartU3Ed__10_System_Collections_IEnumerator_Reset_m2E08DC86E082086DAEC6DFDFF85580F53EDCE9A6(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CStartU3Ed__10_t4C950ADFDCBB18E14D5159D1D6BC6D49ABEC06D0_CustomAttributesCacheGenerator_U3CStartU3Ed__10_System_Collections_IEnumerator_get_Current_mA280C39A17548B1911ACD6D3A44D3053E613B0FC(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void Example05ProxyManagement_t2FFB978A78BEABDF7CD9B526A9ABC9665F01B10A_CustomAttributesCacheGenerator_Example05ProxyManagement_Start_m918205768DD763A8F73CB618156AA6DDC9BAEC6A(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CStartU3Ed__7_t99F1576B8ECF7C51829E8FD496D881EC6A07024C_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CStartU3Ed__7_t99F1576B8ECF7C51829E8FD496D881EC6A07024C_0_0_0_var), NULL);
	}
}
static void Example05ProxyManagement_t2FFB978A78BEABDF7CD9B526A9ABC9665F01B10A_CustomAttributesCacheGenerator_Example05ProxyManagement_U3CStartU3Eb__7_0_m4E1A6040A0C7A2098FAB777BAC252F695DFECAAA(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void Example05ProxyManagement_t2FFB978A78BEABDF7CD9B526A9ABC9665F01B10A_CustomAttributesCacheGenerator_Example05ProxyManagement_U3CStartU3Eb__7_1_m0B63C58301AF6B06F1C7C41B45D1F69D58A79644(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void Example05ProxyManagement_t2FFB978A78BEABDF7CD9B526A9ABC9665F01B10A_CustomAttributesCacheGenerator_Example05ProxyManagement_U3CStartU3Eb__7_2_m969A65E0BCFC131DDD819D80D2B9868F4AF2FE8A(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void Example05ProxyManagement_t2FFB978A78BEABDF7CD9B526A9ABC9665F01B10A_CustomAttributesCacheGenerator_Example05ProxyManagement_U3CStartU3Eb__7_3_m0679A42A30B14C905B811C8AC0290ED3FA95AE3C(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void Example05ProxyManagement_t2FFB978A78BEABDF7CD9B526A9ABC9665F01B10A_CustomAttributesCacheGenerator_Example05ProxyManagement_U3CStartU3Eb__7_4_m623F6738E08C72416FF70B5E85F218BE31768A3A(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CStartU3Ed__7_t99F1576B8ECF7C51829E8FD496D881EC6A07024C_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CStartU3Ed__7_t99F1576B8ECF7C51829E8FD496D881EC6A07024C_CustomAttributesCacheGenerator_U3CStartU3Ed__7__ctor_mFFB60DBC0F017DF05F9FA1003C2B2738FBCFA8FC(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CStartU3Ed__7_t99F1576B8ECF7C51829E8FD496D881EC6A07024C_CustomAttributesCacheGenerator_U3CStartU3Ed__7_System_IDisposable_Dispose_m05D77DC058E9785F795D27EB7E08BABCB8EEAB0F(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CStartU3Ed__7_t99F1576B8ECF7C51829E8FD496D881EC6A07024C_CustomAttributesCacheGenerator_U3CStartU3Ed__7_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m3A38D5FDBB5F5E32BD913E93B26A37B8B50D7313(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CStartU3Ed__7_t99F1576B8ECF7C51829E8FD496D881EC6A07024C_CustomAttributesCacheGenerator_U3CStartU3Ed__7_System_Collections_IEnumerator_Reset_mB7C38F73E9B1A4D4DD842876612C0EC3A5793742(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CStartU3Ed__7_t99F1576B8ECF7C51829E8FD496D881EC6A07024C_CustomAttributesCacheGenerator_U3CStartU3Ed__7_System_Collections_IEnumerator_get_Current_m701771E3243E1E913ACEBC2E8795C89ACC649668(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void Example08NoGUIDictation_t23EBE643531A238ACE66B07CE63DB01CC248919D_CustomAttributesCacheGenerator_Example08NoGUIDictation_Start_m78D8A7861FB8F439E8E757AFB43C9CE12A8DE22D(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CStartU3Ed__1_t24B0D6D6CCC34B861307115FF573C6287C8DEBF8_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CStartU3Ed__1_t24B0D6D6CCC34B861307115FF573C6287C8DEBF8_0_0_0_var), NULL);
	}
}
static void U3CStartU3Ed__1_t24B0D6D6CCC34B861307115FF573C6287C8DEBF8_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CStartU3Ed__1_t24B0D6D6CCC34B861307115FF573C6287C8DEBF8_CustomAttributesCacheGenerator_U3CStartU3Ed__1__ctor_mE7E901F45124A48C59536990AA73C30B675E639B(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CStartU3Ed__1_t24B0D6D6CCC34B861307115FF573C6287C8DEBF8_CustomAttributesCacheGenerator_U3CStartU3Ed__1_System_IDisposable_Dispose_mD105462F2F676E0DBAA2F81DBE280BE7AA7FE238(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CStartU3Ed__1_t24B0D6D6CCC34B861307115FF573C6287C8DEBF8_CustomAttributesCacheGenerator_U3CStartU3Ed__1_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mFC700BEEB25737D16E6524C6E55100390A7C94C6(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CStartU3Ed__1_t24B0D6D6CCC34B861307115FF573C6287C8DEBF8_CustomAttributesCacheGenerator_U3CStartU3Ed__1_System_Collections_IEnumerator_Reset_mA53FBDA94F0F3114DCEE4EFE51DD76112B0F2D4C(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CStartU3Ed__1_t24B0D6D6CCC34B861307115FF573C6287C8DEBF8_CustomAttributesCacheGenerator_U3CStartU3Ed__1_System_Collections_IEnumerator_get_Current_mCE1ADD01EFF09234ED24546244C7B07E4D5B7206(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void Example09NoGUISpeechCommands_t2A3365BE122094335103D4DB284E91FFFB7B292D_CustomAttributesCacheGenerator_Example09NoGUISpeechCommands_Start_m67230CF71BF7306EE666D93170BD0854B411E556(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CStartU3Ed__2_t69FDEAF9EA13E082CF88C7B60B90C5A05CC6179C_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CStartU3Ed__2_t69FDEAF9EA13E082CF88C7B60B90C5A05CC6179C_0_0_0_var), NULL);
	}
}
static void U3CStartU3Ed__2_t69FDEAF9EA13E082CF88C7B60B90C5A05CC6179C_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CStartU3Ed__2_t69FDEAF9EA13E082CF88C7B60B90C5A05CC6179C_CustomAttributesCacheGenerator_U3CStartU3Ed__2__ctor_m960857B297E5A747E66E659907688D23C5E3A9A0(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CStartU3Ed__2_t69FDEAF9EA13E082CF88C7B60B90C5A05CC6179C_CustomAttributesCacheGenerator_U3CStartU3Ed__2_System_IDisposable_Dispose_m205623C92E170CE080D885A81AE3575797ED9EB8(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CStartU3Ed__2_t69FDEAF9EA13E082CF88C7B60B90C5A05CC6179C_CustomAttributesCacheGenerator_U3CStartU3Ed__2_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mEDD0D9D21F65BD4743C534205936CB8CB2CBCD41(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CStartU3Ed__2_t69FDEAF9EA13E082CF88C7B60B90C5A05CC6179C_CustomAttributesCacheGenerator_U3CStartU3Ed__2_System_Collections_IEnumerator_Reset_m2488C31C7C875031BCCBC0ACACEA4BCE6430FE2A(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CStartU3Ed__2_t69FDEAF9EA13E082CF88C7B60B90C5A05CC6179C_CustomAttributesCacheGenerator_U3CStartU3Ed__2_System_Collections_IEnumerator_get_Current_m46FBAA334FC377A24C2D933F71512447FB11FD5B(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void ProxySpeechDetectionPlugin_tD744B61330A08B3CBD1B2270611D582373FE1163_CustomAttributesCacheGenerator_ProxySpeechDetectionPlugin_Init_m84CA595544407D67541C32D252E8A9542EDEE55F(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CInitU3Ed__10_tDC9857230B9D766BA44C1A8CD9BC89DAF332560A_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CInitU3Ed__10_tDC9857230B9D766BA44C1A8CD9BC89DAF332560A_0_0_0_var), NULL);
	}
}
static void ProxySpeechDetectionPlugin_tD744B61330A08B3CBD1B2270611D582373FE1163_CustomAttributesCacheGenerator_ProxySpeechDetectionPlugin_RunPendingCommands_m66D935B982A0D79D6D7533FC6219B71B6759C03D(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CRunPendingCommandsU3Ed__12_t3B51F7A544880108D20AA679685AAD3200FD6E8D_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CRunPendingCommandsU3Ed__12_t3B51F7A544880108D20AA679685AAD3200FD6E8D_0_0_0_var), NULL);
	}
}
static void ProxySpeechDetectionPlugin_tD744B61330A08B3CBD1B2270611D582373FE1163_CustomAttributesCacheGenerator_ProxySpeechDetectionPlugin_GetResult_mC030AA32DAA8592997FA7A5F671B5B11B948F41A(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CGetResultU3Ed__13_t429D1A37625B8E217E88830CCFE7B4B9E5F3DCED_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CGetResultU3Ed__13_t429D1A37625B8E217E88830CCFE7B4B9E5F3DCED_0_0_0_var), NULL);
	}
}
static void ProxySpeechDetectionPlugin_tD744B61330A08B3CBD1B2270611D582373FE1163_CustomAttributesCacheGenerator_ProxySpeechDetectionPlugin_ProxyLanguages_m8E82A021B061AAB677F5036EF34520588DDD41E0(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CProxyLanguagesU3Ed__19_t3B013A40DC9B77C22F68441BA9F709DD9D113FE9_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CProxyLanguagesU3Ed__19_t3B013A40DC9B77C22F68441BA9F709DD9D113FE9_0_0_0_var), NULL);
	}
}
static void U3CInitU3Ed__10_tDC9857230B9D766BA44C1A8CD9BC89DAF332560A_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CInitU3Ed__10_tDC9857230B9D766BA44C1A8CD9BC89DAF332560A_CustomAttributesCacheGenerator_U3CInitU3Ed__10__ctor_m012BA9734608E5F6552327811617DA42F34B766A(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CInitU3Ed__10_tDC9857230B9D766BA44C1A8CD9BC89DAF332560A_CustomAttributesCacheGenerator_U3CInitU3Ed__10_System_IDisposable_Dispose_mBBB8890BC2D1590E2C1DDE80878680C2212EF200(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CInitU3Ed__10_tDC9857230B9D766BA44C1A8CD9BC89DAF332560A_CustomAttributesCacheGenerator_U3CInitU3Ed__10_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m54ADF1B4ABC962485DB751DCE71C27B0C17558DD(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CInitU3Ed__10_tDC9857230B9D766BA44C1A8CD9BC89DAF332560A_CustomAttributesCacheGenerator_U3CInitU3Ed__10_System_Collections_IEnumerator_Reset_mE083368AC1F41DD0E8F9B2988AB38626CDDB50B4(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CInitU3Ed__10_tDC9857230B9D766BA44C1A8CD9BC89DAF332560A_CustomAttributesCacheGenerator_U3CInitU3Ed__10_System_Collections_IEnumerator_get_Current_mE9D4A920E754B9F844722ABD42F7911A32B253AC(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CRunPendingCommandsU3Ed__12_t3B51F7A544880108D20AA679685AAD3200FD6E8D_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CRunPendingCommandsU3Ed__12_t3B51F7A544880108D20AA679685AAD3200FD6E8D_CustomAttributesCacheGenerator_U3CRunPendingCommandsU3Ed__12__ctor_mC11C018DC0A91D90202312BE9D6E7031F8B7A966(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CRunPendingCommandsU3Ed__12_t3B51F7A544880108D20AA679685AAD3200FD6E8D_CustomAttributesCacheGenerator_U3CRunPendingCommandsU3Ed__12_System_IDisposable_Dispose_m273B75505906AC0B3E987E74414503C5EAFD3029(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CRunPendingCommandsU3Ed__12_t3B51F7A544880108D20AA679685AAD3200FD6E8D_CustomAttributesCacheGenerator_U3CRunPendingCommandsU3Ed__12_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mF49722C3374CA4DCD5DEDA7A42E74B2E85449D0C(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CRunPendingCommandsU3Ed__12_t3B51F7A544880108D20AA679685AAD3200FD6E8D_CustomAttributesCacheGenerator_U3CRunPendingCommandsU3Ed__12_System_Collections_IEnumerator_Reset_mCE9FD8ED116F2DE4D31163FF91ED0F0BCC872409(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CRunPendingCommandsU3Ed__12_t3B51F7A544880108D20AA679685AAD3200FD6E8D_CustomAttributesCacheGenerator_U3CRunPendingCommandsU3Ed__12_System_Collections_IEnumerator_get_Current_m0214811E4DC232B9A5AD3775866A44B909549478(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CGetResultU3Ed__13_t429D1A37625B8E217E88830CCFE7B4B9E5F3DCED_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CGetResultU3Ed__13_t429D1A37625B8E217E88830CCFE7B4B9E5F3DCED_CustomAttributesCacheGenerator_U3CGetResultU3Ed__13__ctor_m0CDDF83A5EBDA6AC4DA49D5DF38A02D721246C3F(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CGetResultU3Ed__13_t429D1A37625B8E217E88830CCFE7B4B9E5F3DCED_CustomAttributesCacheGenerator_U3CGetResultU3Ed__13_System_IDisposable_Dispose_mD2C540480CC4924CBDA0D6CC8090D5E69A09E52A(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CGetResultU3Ed__13_t429D1A37625B8E217E88830CCFE7B4B9E5F3DCED_CustomAttributesCacheGenerator_U3CGetResultU3Ed__13_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m7EBC61FA9172162F7E2D417E3E9646E091814409(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CGetResultU3Ed__13_t429D1A37625B8E217E88830CCFE7B4B9E5F3DCED_CustomAttributesCacheGenerator_U3CGetResultU3Ed__13_System_Collections_IEnumerator_Reset_mBCB807344745DBFF79C784AA606095E3ACECD75C(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CGetResultU3Ed__13_t429D1A37625B8E217E88830CCFE7B4B9E5F3DCED_CustomAttributesCacheGenerator_U3CGetResultU3Ed__13_System_Collections_IEnumerator_get_Current_m6EB722F8467C0E7094AFC385192102522361A12E(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CProxyLanguagesU3Ed__19_t3B013A40DC9B77C22F68441BA9F709DD9D113FE9_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CProxyLanguagesU3Ed__19_t3B013A40DC9B77C22F68441BA9F709DD9D113FE9_CustomAttributesCacheGenerator_U3CProxyLanguagesU3Ed__19__ctor_m73D98C4B262426CBA8A82CFCB41B8CC9E0083405(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CProxyLanguagesU3Ed__19_t3B013A40DC9B77C22F68441BA9F709DD9D113FE9_CustomAttributesCacheGenerator_U3CProxyLanguagesU3Ed__19_System_IDisposable_Dispose_m7ABACFD849FD40300BF13D89CC177D0836FB0B4D(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CProxyLanguagesU3Ed__19_t3B013A40DC9B77C22F68441BA9F709DD9D113FE9_CustomAttributesCacheGenerator_U3CProxyLanguagesU3Ed__19_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m51D3D2D32D00F389B14479614A9A4F24C90D57D0(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CProxyLanguagesU3Ed__19_t3B013A40DC9B77C22F68441BA9F709DD9D113FE9_CustomAttributesCacheGenerator_U3CProxyLanguagesU3Ed__19_System_Collections_IEnumerator_Reset_m368C82C50D2515A4715969FA6E605EF35B095C30(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CProxyLanguagesU3Ed__19_t3B013A40DC9B77C22F68441BA9F709DD9D113FE9_CustomAttributesCacheGenerator_U3CProxyLanguagesU3Ed__19_System_Collections_IEnumerator_get_Current_m443AEE64E35421F4376968F384CE357F48E10A9E(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void SpeechDetectionUtils_t121D1188BECD5625D5D24645D4461A4F4EC4F1FE_CustomAttributesCacheGenerator_SpeechDetectionUtils_SetActive_mC1B4C840410E33AB9E47BD8C4FF38580CA0197CA____args1(CustomAttributesCache* cache)
{
	{
		ParamArrayAttribute_t9DCEB4CDDB8EDDB1124171D4C51FA6069EEA5C5F * tmp = (ParamArrayAttribute_t9DCEB4CDDB8EDDB1124171D4C51FA6069EEA5C5F *)cache->attributes[0];
		ParamArrayAttribute__ctor_mCC72AFF718185BA7B87FD8D9471F1274400C5719(tmp, NULL);
	}
}
static void SpeechDetectionUtils_t121D1188BECD5625D5D24645D4461A4F4EC4F1FE_CustomAttributesCacheGenerator_SpeechDetectionUtils_SetInteractable_m4A6F5FDC6777BFDD436E8EA4EBD628D0FB5A921E____args1(CustomAttributesCache* cache)
{
	{
		ParamArrayAttribute_t9DCEB4CDDB8EDDB1124171D4C51FA6069EEA5C5F * tmp = (ParamArrayAttribute_t9DCEB4CDDB8EDDB1124171D4C51FA6069EEA5C5F *)cache->attributes[0];
		ParamArrayAttribute__ctor_mCC72AFF718185BA7B87FD8D9471F1274400C5719(tmp, NULL);
	}
}
static void Example01DictationSynthesis_tABE5E8AB9ABB921D74D02390BBD785C542409B34_CustomAttributesCacheGenerator_Example01DictationSynthesis_Start_mB4882D5513B9316D6BAB403228D4AED406D59C1C(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CStartU3Ed__28_t15833A9AB280EADB404BBB5150CC5E571050157D_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CStartU3Ed__28_t15833A9AB280EADB404BBB5150CC5E571050157D_0_0_0_var), NULL);
	}
}
static void Example01DictationSynthesis_tABE5E8AB9ABB921D74D02390BBD785C542409B34_CustomAttributesCacheGenerator_Example01DictationSynthesis_GetVoices_m7A46A9BDFEAC0BC2BE84DA4D4E435C4709F1940B(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CGetVoicesU3Ed__32_tD206C9C44DE7C20EB7573ED4D8761B0FD13677FD_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CGetVoicesU3Ed__32_tD206C9C44DE7C20EB7573ED4D8761B0FD13677FD_0_0_0_var), NULL);
	}
}
static void Example01DictationSynthesis_tABE5E8AB9ABB921D74D02390BBD785C542409B34_CustomAttributesCacheGenerator_Example01DictationSynthesis_SetPitch_m387E57A36A344607A5BF04F1BF4DE73E58F1C212(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CSetPitchU3Ed__35_tF30264CE7FB8EFB8DD7CC3A54F6E538C7526D22E_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CSetPitchU3Ed__35_tF30264CE7FB8EFB8DD7CC3A54F6E538C7526D22E_0_0_0_var), NULL);
	}
}
static void Example01DictationSynthesis_tABE5E8AB9ABB921D74D02390BBD785C542409B34_CustomAttributesCacheGenerator_Example01DictationSynthesis_SetRate_m98FDF4E83F129860CF981AC241540A865844849C(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CSetRateU3Ed__36_tE587DF9904D56F1F471E8AAB2C639678AE1C2983_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CSetRateU3Ed__36_tE587DF9904D56F1F471E8AAB2C639678AE1C2983_0_0_0_var), NULL);
	}
}
static void Example01DictationSynthesis_tABE5E8AB9ABB921D74D02390BBD785C542409B34_CustomAttributesCacheGenerator_Example01DictationSynthesis_U3CStartU3Eb__28_0_m2BB9F8966F92CEEE45201B8B5EBC468EF95CC0A8(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void Example01DictationSynthesis_tABE5E8AB9ABB921D74D02390BBD785C542409B34_CustomAttributesCacheGenerator_Example01DictationSynthesis_U3CStartU3Eb__28_1_m40F45F64E6037C82A4D94DCB628D9B9D20F789B2(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void Example01DictationSynthesis_tABE5E8AB9ABB921D74D02390BBD785C542409B34_CustomAttributesCacheGenerator_Example01DictationSynthesis_U3CStartU3Eb__28_2_mF4484E4A96810FC30E1902285A2952CB7EFEF50C(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void Example01DictationSynthesis_tABE5E8AB9ABB921D74D02390BBD785C542409B34_CustomAttributesCacheGenerator_Example01DictationSynthesis_U3CStartU3Eb__28_3_m7D58A195ACB6F257BBB7646F8E3C6ADCC4E506D4(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void Example01DictationSynthesis_tABE5E8AB9ABB921D74D02390BBD785C542409B34_CustomAttributesCacheGenerator_Example01DictationSynthesis_U3CStartU3Eb__28_8_mB7702341DF0663D771005EC27CC628EA3825A6C1(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void Example01DictationSynthesis_tABE5E8AB9ABB921D74D02390BBD785C542409B34_CustomAttributesCacheGenerator_Example01DictationSynthesis_U3CStartU3Eb__28_9_m4109CE818814E955E1C68269A520B2AB34F0CBC1(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void Example01DictationSynthesis_tABE5E8AB9ABB921D74D02390BBD785C542409B34_CustomAttributesCacheGenerator_Example01DictationSynthesis_U3CStartU3Eb__28_4_mDF45F0C4ED5AB0BAAF9C49DC31880C30A2ABC532(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void Example01DictationSynthesis_tABE5E8AB9ABB921D74D02390BBD785C542409B34_CustomAttributesCacheGenerator_Example01DictationSynthesis_U3CStartU3Eb__28_5_mF1465E0ECA48721C2D79C05FC8673499A676639F(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void Example01DictationSynthesis_tABE5E8AB9ABB921D74D02390BBD785C542409B34_CustomAttributesCacheGenerator_Example01DictationSynthesis_U3CStartU3Eb__28_6_mF0B4E30B599D0E84B30C93594F4D8BB01070A61B(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void Example01DictationSynthesis_tABE5E8AB9ABB921D74D02390BBD785C542409B34_CustomAttributesCacheGenerator_Example01DictationSynthesis_U3CStartU3Eb__28_7_mB2DEFDEF9A51C0B5267A44A8DB5224DC798BAD6F(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void Example01DictationSynthesis_tABE5E8AB9ABB921D74D02390BBD785C542409B34_CustomAttributesCacheGenerator_Example01DictationSynthesis_U3CGetVoicesU3Eb__32_0_m7647381772F8947F21087CA9489F9A76C9AEEC41(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void Example01DictationSynthesis_tABE5E8AB9ABB921D74D02390BBD785C542409B34_CustomAttributesCacheGenerator_Example01DictationSynthesis_U3CSetIfReadyForDefaultVoiceU3Eb__33_0_mE07F5718D62BA9FC6AB3DC33A6BF537C091A9523(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CStartU3Ed__28_t15833A9AB280EADB404BBB5150CC5E571050157D_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CStartU3Ed__28_t15833A9AB280EADB404BBB5150CC5E571050157D_CustomAttributesCacheGenerator_U3CStartU3Ed__28__ctor_m6251A007ED7831DB20DE6DD12568AFC488C1B1A0(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CStartU3Ed__28_t15833A9AB280EADB404BBB5150CC5E571050157D_CustomAttributesCacheGenerator_U3CStartU3Ed__28_System_IDisposable_Dispose_mE1A0CB500E998B63CCC43B876F7F55F86C1B921B(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CStartU3Ed__28_t15833A9AB280EADB404BBB5150CC5E571050157D_CustomAttributesCacheGenerator_U3CStartU3Ed__28_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mB67313988CF183C834E335EE4EE373EE42FAAE53(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CStartU3Ed__28_t15833A9AB280EADB404BBB5150CC5E571050157D_CustomAttributesCacheGenerator_U3CStartU3Ed__28_System_Collections_IEnumerator_Reset_mE54EED999CFD46CE5C5FF21F95AF6153F165E5CE(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CStartU3Ed__28_t15833A9AB280EADB404BBB5150CC5E571050157D_CustomAttributesCacheGenerator_U3CStartU3Ed__28_System_Collections_IEnumerator_get_Current_m8A5EF102397A7D73091A8A9EECEB30078BF7B8CC(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CGetVoicesU3Ed__32_tD206C9C44DE7C20EB7573ED4D8761B0FD13677FD_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CGetVoicesU3Ed__32_tD206C9C44DE7C20EB7573ED4D8761B0FD13677FD_CustomAttributesCacheGenerator_U3CGetVoicesU3Ed__32__ctor_mF7508112903C4F9ED3CAF31218951EAE34D1DBEE(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CGetVoicesU3Ed__32_tD206C9C44DE7C20EB7573ED4D8761B0FD13677FD_CustomAttributesCacheGenerator_U3CGetVoicesU3Ed__32_System_IDisposable_Dispose_mEC4E1291389C3EA9CFA71F80146E8F16FA321323(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CGetVoicesU3Ed__32_tD206C9C44DE7C20EB7573ED4D8761B0FD13677FD_CustomAttributesCacheGenerator_U3CGetVoicesU3Ed__32_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m4EAEC51E6B7FBCD8B9A1CBCD16DA2966223124C8(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CGetVoicesU3Ed__32_tD206C9C44DE7C20EB7573ED4D8761B0FD13677FD_CustomAttributesCacheGenerator_U3CGetVoicesU3Ed__32_System_Collections_IEnumerator_Reset_m094A2A097F4044D384F0C23A2BD9629CB03E9B90(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CGetVoicesU3Ed__32_tD206C9C44DE7C20EB7573ED4D8761B0FD13677FD_CustomAttributesCacheGenerator_U3CGetVoicesU3Ed__32_System_Collections_IEnumerator_get_Current_mBBB5185B3F8DB86560A9DE3F3BBB2E7531E2C484(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CSetPitchU3Ed__35_tF30264CE7FB8EFB8DD7CC3A54F6E538C7526D22E_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CSetPitchU3Ed__35_tF30264CE7FB8EFB8DD7CC3A54F6E538C7526D22E_CustomAttributesCacheGenerator_U3CSetPitchU3Ed__35__ctor_m9D8D07FCA90D3E73D299B2D80FE6E7C891D77952(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CSetPitchU3Ed__35_tF30264CE7FB8EFB8DD7CC3A54F6E538C7526D22E_CustomAttributesCacheGenerator_U3CSetPitchU3Ed__35_System_IDisposable_Dispose_mB8668091EEFA11E8010416E9001FCF9EC09ECE72(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CSetPitchU3Ed__35_tF30264CE7FB8EFB8DD7CC3A54F6E538C7526D22E_CustomAttributesCacheGenerator_U3CSetPitchU3Ed__35_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m78E469CF14747B40EB8F0759E0639B39FB089C41(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CSetPitchU3Ed__35_tF30264CE7FB8EFB8DD7CC3A54F6E538C7526D22E_CustomAttributesCacheGenerator_U3CSetPitchU3Ed__35_System_Collections_IEnumerator_Reset_m66DB440A475F280C6F51FE8B7F3016EB7FE9151F(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CSetPitchU3Ed__35_tF30264CE7FB8EFB8DD7CC3A54F6E538C7526D22E_CustomAttributesCacheGenerator_U3CSetPitchU3Ed__35_System_Collections_IEnumerator_get_Current_m83AEA3365906FC77D9D1DE7859301D5621AB2E4B(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CSetRateU3Ed__36_tE587DF9904D56F1F471E8AAB2C639678AE1C2983_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CSetRateU3Ed__36_tE587DF9904D56F1F471E8AAB2C639678AE1C2983_CustomAttributesCacheGenerator_U3CSetRateU3Ed__36__ctor_m850DD6B944C189AFC8DD0EC75E985E279C02848D(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CSetRateU3Ed__36_tE587DF9904D56F1F471E8AAB2C639678AE1C2983_CustomAttributesCacheGenerator_U3CSetRateU3Ed__36_System_IDisposable_Dispose_mFE9B1BC95F527E94DB2F805C2E2DE218C08C36A0(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CSetRateU3Ed__36_tE587DF9904D56F1F471E8AAB2C639678AE1C2983_CustomAttributesCacheGenerator_U3CSetRateU3Ed__36_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m8ACBE875E3E52945765CB65BB421C1DB1758D2D3(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CSetRateU3Ed__36_tE587DF9904D56F1F471E8AAB2C639678AE1C2983_CustomAttributesCacheGenerator_U3CSetRateU3Ed__36_System_Collections_IEnumerator_Reset_mD3357A03BEE1F8589E4CC34678D40D0137D0FC3D(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CSetRateU3Ed__36_tE587DF9904D56F1F471E8AAB2C639678AE1C2983_CustomAttributesCacheGenerator_U3CSetRateU3Ed__36_System_Collections_IEnumerator_get_Current_m7934C30490D8913CC34E6F24824C5CFD13ED62C7(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void Example02DictationSbaitso_t8122EA3EC19C62514409A0B8D63C4B5E979DC181_CustomAttributesCacheGenerator_Example02DictationSbaitso_CreateNameInputField_m669A31FCB67151C44FA729295641A592875E8ECD(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CCreateNameInputFieldU3Ed__18_tCF407195076F20F3CC29E9320B5836378BF87FA4_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CCreateNameInputFieldU3Ed__18_tCF407195076F20F3CC29E9320B5836378BF87FA4_0_0_0_var), NULL);
	}
}
static void Example02DictationSbaitso_t8122EA3EC19C62514409A0B8D63C4B5E979DC181_CustomAttributesCacheGenerator_Example02DictationSbaitso_CreateTalkInputField_m93C3B2C862D8C5D6A359775752B6DAE8065D0021(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CCreateTalkInputFieldU3Ed__19_t5C727E8D85F30A4CC308C816B210A1FD28056A29_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CCreateTalkInputFieldU3Ed__19_t5C727E8D85F30A4CC308C816B210A1FD28056A29_0_0_0_var), NULL);
	}
}
static void Example02DictationSbaitso_t8122EA3EC19C62514409A0B8D63C4B5E979DC181_CustomAttributesCacheGenerator_Example02DictationSbaitso_Start_m7444A4D38B7A9C05C2917DAAC2EF163D756D7E74(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CStartU3Ed__21_tCD0B0FE65242BC30DD807F964ABE50628F2C6FD8_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CStartU3Ed__21_tCD0B0FE65242BC30DD807F964ABE50628F2C6FD8_0_0_0_var), NULL);
	}
}
static void Example02DictationSbaitso_t8122EA3EC19C62514409A0B8D63C4B5E979DC181_CustomAttributesCacheGenerator_Example02DictationSbaitso_GetVoices_m089DFE1583E1DD7E4BCCE8AD263845C0EB0AEE83(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CGetVoicesU3Ed__24_tC53D4CB8A21E838EE8088A9FDD86CA7324EB8050_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CGetVoicesU3Ed__24_tC53D4CB8A21E838EE8088A9FDD86CA7324EB8050_0_0_0_var), NULL);
	}
}
static void Example02DictationSbaitso_t8122EA3EC19C62514409A0B8D63C4B5E979DC181_CustomAttributesCacheGenerator_Example02DictationSbaitso_U3CStartU3Eb__21_0_mCCD542190710E61A9EDE15A3E559DC311ADD9BEB(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void Example02DictationSbaitso_t8122EA3EC19C62514409A0B8D63C4B5E979DC181_CustomAttributesCacheGenerator_Example02DictationSbaitso_U3CStartU3Eb__21_1_mF537CA2C485462C331E4C7316CCFEB0A6DC1F203(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void Example02DictationSbaitso_t8122EA3EC19C62514409A0B8D63C4B5E979DC181_CustomAttributesCacheGenerator_Example02DictationSbaitso_U3CStartU3Eb__21_2_mC0F51C19C5FF069B8BDCCAE56A1507FDE85B8772(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void Example02DictationSbaitso_t8122EA3EC19C62514409A0B8D63C4B5E979DC181_CustomAttributesCacheGenerator_Example02DictationSbaitso_U3CGetVoicesU3Eb__24_0_mB6088C47629D61E8ABE71DF63887EEBECC040C67(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CCreateNameInputFieldU3Ed__18_tCF407195076F20F3CC29E9320B5836378BF87FA4_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CCreateNameInputFieldU3Ed__18_tCF407195076F20F3CC29E9320B5836378BF87FA4_CustomAttributesCacheGenerator_U3CCreateNameInputFieldU3Ed__18__ctor_m4B5B254484C31B4D071462387837964CB5FB5FE0(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CCreateNameInputFieldU3Ed__18_tCF407195076F20F3CC29E9320B5836378BF87FA4_CustomAttributesCacheGenerator_U3CCreateNameInputFieldU3Ed__18_System_IDisposable_Dispose_mF82190DDE2697199F48366F177466E899B0022F2(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CCreateNameInputFieldU3Ed__18_tCF407195076F20F3CC29E9320B5836378BF87FA4_CustomAttributesCacheGenerator_U3CCreateNameInputFieldU3Ed__18_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mA7C8F9A3E9BCA451E48A0B59AF88D0D30FA8052D(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CCreateNameInputFieldU3Ed__18_tCF407195076F20F3CC29E9320B5836378BF87FA4_CustomAttributesCacheGenerator_U3CCreateNameInputFieldU3Ed__18_System_Collections_IEnumerator_Reset_m1EA0F77850A1CE0F7FA9D16059514D7039A976C6(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CCreateNameInputFieldU3Ed__18_tCF407195076F20F3CC29E9320B5836378BF87FA4_CustomAttributesCacheGenerator_U3CCreateNameInputFieldU3Ed__18_System_Collections_IEnumerator_get_Current_m6EB5B4889BD9E1D8F6101FFF052897D2508A69F0(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CCreateTalkInputFieldU3Ed__19_t5C727E8D85F30A4CC308C816B210A1FD28056A29_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CCreateTalkInputFieldU3Ed__19_t5C727E8D85F30A4CC308C816B210A1FD28056A29_CustomAttributesCacheGenerator_U3CCreateTalkInputFieldU3Ed__19__ctor_mCE780DD23754578C72F63AA3BE000D2EF21C457E(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CCreateTalkInputFieldU3Ed__19_t5C727E8D85F30A4CC308C816B210A1FD28056A29_CustomAttributesCacheGenerator_U3CCreateTalkInputFieldU3Ed__19_System_IDisposable_Dispose_mF2ACFC307D0C8C5D10CAC168C84B87629E31C5ED(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CCreateTalkInputFieldU3Ed__19_t5C727E8D85F30A4CC308C816B210A1FD28056A29_CustomAttributesCacheGenerator_U3CCreateTalkInputFieldU3Ed__19_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mE52A015117FA49929B6655BA390A090BC6BC3934(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CCreateTalkInputFieldU3Ed__19_t5C727E8D85F30A4CC308C816B210A1FD28056A29_CustomAttributesCacheGenerator_U3CCreateTalkInputFieldU3Ed__19_System_Collections_IEnumerator_Reset_m8F584B55249C63900EE385230905F4819BCB245C(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CCreateTalkInputFieldU3Ed__19_t5C727E8D85F30A4CC308C816B210A1FD28056A29_CustomAttributesCacheGenerator_U3CCreateTalkInputFieldU3Ed__19_System_Collections_IEnumerator_get_Current_mDBCA9A1BCCADCA02EC5ABFFD1073E495B6F7EDE6(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CStartU3Ed__21_tCD0B0FE65242BC30DD807F964ABE50628F2C6FD8_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CStartU3Ed__21_tCD0B0FE65242BC30DD807F964ABE50628F2C6FD8_CustomAttributesCacheGenerator_U3CStartU3Ed__21__ctor_mC72EA5BAA492982E48576F5F60FD8483732353B6(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CStartU3Ed__21_tCD0B0FE65242BC30DD807F964ABE50628F2C6FD8_CustomAttributesCacheGenerator_U3CStartU3Ed__21_System_IDisposable_Dispose_m12232760A14BC34C5C5EC1EACC05E1852D6A89A9(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CStartU3Ed__21_tCD0B0FE65242BC30DD807F964ABE50628F2C6FD8_CustomAttributesCacheGenerator_U3CStartU3Ed__21_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m535DFD5C67CB16D689578F83B00CEC23E4484FEA(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CStartU3Ed__21_tCD0B0FE65242BC30DD807F964ABE50628F2C6FD8_CustomAttributesCacheGenerator_U3CStartU3Ed__21_System_Collections_IEnumerator_Reset_m67EA047E1E606C626E1B1D7A7E58018554CC12A8(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CStartU3Ed__21_tCD0B0FE65242BC30DD807F964ABE50628F2C6FD8_CustomAttributesCacheGenerator_U3CStartU3Ed__21_System_Collections_IEnumerator_get_Current_mA401C2A6D290024BD88FC27BE57E64DDE97B5433(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CGetVoicesU3Ed__24_tC53D4CB8A21E838EE8088A9FDD86CA7324EB8050_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CGetVoicesU3Ed__24_tC53D4CB8A21E838EE8088A9FDD86CA7324EB8050_CustomAttributesCacheGenerator_U3CGetVoicesU3Ed__24__ctor_m833C2611DC287B9E47094CADC771D692419E63E6(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CGetVoicesU3Ed__24_tC53D4CB8A21E838EE8088A9FDD86CA7324EB8050_CustomAttributesCacheGenerator_U3CGetVoicesU3Ed__24_System_IDisposable_Dispose_m71DBB98BC3D4A498D09E055587E51F12F98331CC(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CGetVoicesU3Ed__24_tC53D4CB8A21E838EE8088A9FDD86CA7324EB8050_CustomAttributesCacheGenerator_U3CGetVoicesU3Ed__24_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m52B65A6687210AA4FB8BB4C690ADA13DF97D1061(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CGetVoicesU3Ed__24_tC53D4CB8A21E838EE8088A9FDD86CA7324EB8050_CustomAttributesCacheGenerator_U3CGetVoicesU3Ed__24_System_Collections_IEnumerator_Reset_m9F98C408BFCDFD6C23ED7A460E33534854BBC079(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CGetVoicesU3Ed__24_tC53D4CB8A21E838EE8088A9FDD86CA7324EB8050_CustomAttributesCacheGenerator_U3CGetVoicesU3Ed__24_System_Collections_IEnumerator_get_Current_mBF17624D10518A8301364E0EE6CED011DC123301(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CPrivateImplementationDetailsU3E_t2672E9E7EEBD118534BF12C6E47D2B3DD7D9E562_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
IL2CPP_EXTERN_C const CustomAttributesCacheGenerator g_AssemblyU2DCSharpU2Dfirstpass_AttributeGenerators[];
const CustomAttributesCacheGenerator g_AssemblyU2DCSharpU2Dfirstpass_AttributeGenerators[864] = 
{
	ExtensionMethods_t3A63E01D154FF156B5910983BBD9656044FCBAEF_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass5_0_tE5690FA9ACA4586CA14252530514637C1651F6FC_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass6_0_tE339584696C7D04D095E648B21D42B2849672875_CustomAttributesCacheGenerator,
	LiveSpeaker_t7B31D2171E070D53CE0B04874A700A898F2D72E7_CustomAttributesCacheGenerator,
	Speaker_t639C193C2402AD73ED45F6530A8FD0EB62D3ACA1_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass171_0_t1D4DEA10DFF9B2953F9E5C2D69FF13A70C813E7A_CustomAttributesCacheGenerator,
	U3CU3Ec_t286538CCBFD7D920B5EF3EBD00F0B6DA2C054F9A_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass190_0_t13E11E15D078B45788B97CABD4F8C24E1030AA18_CustomAttributesCacheGenerator,
	U3CGenerateU3Ed__27_t733346A9DBD137DA2DD7EA3252EFFE76500D3C5B_CustomAttributesCacheGenerator,
	U3CSpeakU3Ed__28_t51C9498C945D57FD321E65C4CD6972BB1933A896_CustomAttributesCacheGenerator,
	U3CSpeakNativeU3Ed__29_tAEFCE1D6AFE6BEB16F03F830C079DAA23C35B481_CustomAttributesCacheGenerator,
	AudioFileGenerator_t25C4CCBDEAE5E63CB0745BACFAEF787E31EEA298_CustomAttributesCacheGenerator,
	U3CgenerateU3Ed__19_t158A067925EC863CE90E4DCCF85D7536E97CDBA0_CustomAttributesCacheGenerator,
	ChangeGender_t7942C8B9C9685EF714B6674AC27C681AC3D91104_CustomAttributesCacheGenerator,
	Loudspeaker_t78C64F45415540BE87422E3A54942AA35E24ED42_CustomAttributesCacheGenerator,
	Paralanguage_t257B51534DB18301E85DF9EABF6265E9C7FC4F25_CustomAttributesCacheGenerator,
	U3CU3Ec_t721A41E997083B9226FF2AEB7F9CDE142231523C_CustomAttributesCacheGenerator,
	U3CprocessStackU3Ed__42_tED87FEFE7D9355FCAD33EAAEFFDA223FAB4C41D2_CustomAttributesCacheGenerator,
	Sequencer_t55938B3A6AE7BE5ECE446AC08D43795759152190_CustomAttributesCacheGenerator,
	U3CplayMeU3Ed__19_tDCB0B566C9E09125455A47D5F21A01BB9E163B27_CustomAttributesCacheGenerator,
	SpeechText_t89F39958695673475D1D42B203FA444F75E5ED39_CustomAttributesCacheGenerator,
	TextFileSpeaker_t28C4E74F2AA327B88BD90BBE4A123A435747410C_CustomAttributesCacheGenerator,
	VoiceInitalizer_t628535D1C01FC59180A5EE351F3502A45342556C_CustomAttributesCacheGenerator,
	U3CinitalizeVoicesU3Ed__10_t2A275B1AA68149AB13303E07E9688291E5EF2C20_CustomAttributesCacheGenerator,
	U3CU3Ec_tFA29053F24A1921702C048CAEE862DA27E2A9F58_CustomAttributesCacheGenerator,
	U3CplayAudioFileU3Ed__81_t16FD3617907DBC006BF7EDBFDD79142630A5DC55_CustomAttributesCacheGenerator,
	U3CU3Ec_t870715C29A8E9D55FA8B0E48F5CA97C1D03FEC5C_CustomAttributesCacheGenerator,
	U3CplayAudioFileU3Ed__79_t5D80B48C5103969538788365776FBFD2481F2989_CustomAttributesCacheGenerator,
	U3CU3Ec_tE90D39DB69EB0C0E2F146BB1C619945A4E114B80_CustomAttributesCacheGenerator,
	U3CSpeakNativeU3Ed__37_tB56F739052E648271E910C501E0E5DE0A2826C12_CustomAttributesCacheGenerator,
	U3CSpeakU3Ed__38_t0CAF7D4D1537BFF140CFFE747E35BD71F0C3FD33_CustomAttributesCacheGenerator,
	U3CGenerateU3Ed__39_t03292738BAB852A4B9E700EEBCEA2F4808456175_CustomAttributesCacheGenerator,
	U3CspeakU3Ed__41_t6D7ABE0037F6C095D53C8134C299473D203F6A54_CustomAttributesCacheGenerator,
	U3CSpeakNativeU3Ed__29_tAE002A72C2A3F020A17BF4A56FEBBC5EA2787303_CustomAttributesCacheGenerator,
	U3CSpeakU3Ed__30_tC962B657041FD44AF5A88FD52604438A78BA1649_CustomAttributesCacheGenerator,
	U3CGenerateU3Ed__31_tFA662A35AA694F207DCA5B6C83CA1964EBA15A73_CustomAttributesCacheGenerator,
	U3CspeakU3Ed__32_tDEC1697FFF0729D0B77A9482BE143E6DC4C893CD_CustomAttributesCacheGenerator,
	U3CU3Ec_tB1BC9FCD16045B07F039E456A3F897DE7E8C1CDF_CustomAttributesCacheGenerator,
	U3CgetVoicesU3Ed__33_t42277CC46AC28C4C054B2A9350319771F5FBA45D_CustomAttributesCacheGenerator,
	Dialog_t638F1505D57D2F294DDEA45174D9130410067CD7_CustomAttributesCacheGenerator,
	U3CDialogSequenceU3Ed__25_t44374F6C645D70CD0D9FC61B194C18AF79DBA159_CustomAttributesCacheGenerator,
	GUIAudioFilter_tCC8DE531770AD69322920FE7BBCC9EC5B5EB2249_CustomAttributesCacheGenerator,
	GUIDialog_tC739AB986043972A584C1DE7BE4EB16DD4E8464E_CustomAttributesCacheGenerator,
	GUIMain_tD82B775FB46C7E2456748B5A944CBBFF713ECB61_CustomAttributesCacheGenerator,
	GUIMultiAudioFilter_t9FDD491C7C801C47D7872FE2AD05D6CAC2A3557F_CustomAttributesCacheGenerator,
	GUIScenes_t6F72249B673EF03AF769EE6AC0EC241AA0F8E6CB_CustomAttributesCacheGenerator,
	GUISpeech_t415B11E6596BA1EFF7191AD10A721FEF21BF97A0_CustomAttributesCacheGenerator,
	NativeAudio_tC4F90593188637DF55A20E05E1764FFE6833715E_CustomAttributesCacheGenerator,
	PreGeneratedAudio_t042FA862F81AC8AC2E868CFF437A88BD6AFCD884_CustomAttributesCacheGenerator,
	SendMessage_t9D6A7D158B11451BF3D7DCC357330A2F4363BB94_CustomAttributesCacheGenerator,
	U3CSpeakerBU3Ed__8_t4FDA50AFC81C4F929F34CFAB06B4B0B6AF635A06_CustomAttributesCacheGenerator,
	SequenceCaller_t443E4818044C7EA60F108C026B31AF65E6CD0F3D_CustomAttributesCacheGenerator,
	Simple_t557AF9B6F3B5301F929702090D5291F7DBAA5490_CustomAttributesCacheGenerator,
	SimpleNative_t00B45673BD9201F12CD43BDD847E5C69489CD131_CustomAttributesCacheGenerator,
	SpeakWrapper_t46A78C1BDD6F731870403517E01C60528927C0D4_CustomAttributesCacheGenerator,
	MaterialChanger_tA6610F95A3FA18CC626F076A94014B8356AF88B4_CustomAttributesCacheGenerator,
	NativeController_t75220F3EAB005E617337B7EBD5673FE5E94A4B26_CustomAttributesCacheGenerator,
	PlatformController_t92E180723A1895FB7F7B46B4279A987A82ECC318_CustomAttributesCacheGenerator,
	iOSController_t7A78C41A4782DEDF08E61AE01F470467347F6879_CustomAttributesCacheGenerator,
	Speak_tC8B5D9021E212D52637E32CF9EC1CC0719DA4D8D_CustomAttributesCacheGenerator,
	Speak2D_t6FD5D57CCBF1A7AF85A9897757C662422B48DAA8_CustomAttributesCacheGenerator,
	SpeakSimple_tCF365AE8FFE5E1F8E5637CD7B5D295F648D314A6_CustomAttributesCacheGenerator,
	U3ClerpAlphaDownU3Ed__8_tFA588D110070DFAE10E2AAC390DE45F2C6DCE063_CustomAttributesCacheGenerator,
	U3ClerpAlphaUpU3Ed__9_tA4B9A2133A395822BDCF87133798169872E8B6B1_CustomAttributesCacheGenerator,
	SurviveSceneSwitch_t38706D7E5CC93E19DB435134BE37142C3470E26A_CustomAttributesCacheGenerator,
	TakeScreenshot_t585EAF716A55E4C63BE6FEEF832D186885CA03A6_CustomAttributesCacheGenerator,
	U3CU3Ec_tD5BCCFF09947F66BC5DF4853A963AE93DD797BCF_CustomAttributesCacheGenerator,
	U3CStartU3Ed__22_t3F06EB55E379386889BEE897F565664726CFB9B9_CustomAttributesCacheGenerator,
	U3CGetVoicesU3Ed__23_t4C74B1E95210BDA509B6AB73C0CE5286B905B3A1_CustomAttributesCacheGenerator,
	U3CSetPitchU3Ed__27_tEFABEA1BBCBB4634FFC95C1C6C6BC2E1AB296B1B_CustomAttributesCacheGenerator,
	U3CSetRateU3Ed__28_t47111ED4117F0FE7EB7944C25EE6DE198191DDC9_CustomAttributesCacheGenerator,
	U3CStartU3Ed__21_tFD573D57D53E02687F393696FC999F7C0DDB846E_CustomAttributesCacheGenerator,
	U3CGetVoicesU3Ed__22_t63C88EA26724AA6C07C27964299A020FC5C9D217_CustomAttributesCacheGenerator,
	U3CSetPitchU3Ed__25_t00215C2568848CF0F1B6EC7FD3965ED387698544_CustomAttributesCacheGenerator,
	U3CSetRateU3Ed__26_tB66C71381FE02958DCCB4A1A50724DFFA9A78477_CustomAttributesCacheGenerator,
	U3CStartU3Ed__10_t5B930EBF98D13360F6F542B9931A660A8A3383D9_CustomAttributesCacheGenerator,
	U3CCreateNameInputFieldU3Ed__13_tF99DAFC92BB23A5FF3D97BB4B86C12186720DDCE_CustomAttributesCacheGenerator,
	U3CCreateTalkInputFieldU3Ed__14_t8E84DBC2AA498A934C2001267574FE7D360F832D_CustomAttributesCacheGenerator,
	U3CStartU3Ed__19_t61919FD2ED3E01C9A816502E48D6BBE8CC4D515B_CustomAttributesCacheGenerator,
	U3CGetVoicesU3Ed__21_tBCC92EF8427358114BDF9FE975C411924B4B8CF5_CustomAttributesCacheGenerator,
	U3CStartU3Ed__10_t25226C3095C4900C7D3EC61B87E5A46ECF2CF359_CustomAttributesCacheGenerator,
	U3CGetVoicesU3Ed__11_t766D855FBC27F4F8B65AF9AB254CFF24E3F5F600_CustomAttributesCacheGenerator,
	U3CStartU3Ed__9_t36FCBB780B64B702CF4ABD4EBB368E6B34586D7B_CustomAttributesCacheGenerator,
	U3CGetVoicesU3Ed__10_t89A30C17D49996122417B745EB21C1F275A12870_CustomAttributesCacheGenerator,
	U3CInitU3Ed__12_tD8DCA3016689D1AC98B5293D2E98515302FC46F6_CustomAttributesCacheGenerator,
	U3CRunPendingCommandsU3Ed__13_t7581E10AF39B06A7884DCFFC6F826010995D4F7F_CustomAttributesCacheGenerator,
	U3CProxyUtteranceU3Ed__14_t8193D4147E2CE182457EAEF45EDACC550B23BCCF_CustomAttributesCacheGenerator,
	U3CProxyVoicesU3Ed__15_tD895D1F2A7ED22311F8F67BAF0D4593B7AF528B9_CustomAttributesCacheGenerator,
	U3CProxyOnEndU3Ed__16_t496FEB12873920FC762FFCB8CFEC07F32B3F5DBC_CustomAttributesCacheGenerator,
	U3CProxyOnEndU3Ed__3_tB3A6640F746B233069CBE83B8CE0930F714BCAAA_CustomAttributesCacheGenerator,
	U3CStartU3Ed__10_t84D96784539E4E6C4F0B0D80CF2BE76078D3C65B_CustomAttributesCacheGenerator,
	U3CStartU3Ed__9_t9F2C26950487FA4AA558AF9FDBC70EFC86CA1C3A_CustomAttributesCacheGenerator,
	U3CDoHighlightU3Ed__5_tF820C5AFB9FD264363835FEA11DC3D2FAB920E17_CustomAttributesCacheGenerator,
	U3CStartU3Ed__9_t0AC6DFD8163B675E7D948040950A9AD90DE2F27F_CustomAttributesCacheGenerator,
	U3CStartU3Ed__10_t4C950ADFDCBB18E14D5159D1D6BC6D49ABEC06D0_CustomAttributesCacheGenerator,
	U3CStartU3Ed__7_t99F1576B8ECF7C51829E8FD496D881EC6A07024C_CustomAttributesCacheGenerator,
	U3CStartU3Ed__1_t24B0D6D6CCC34B861307115FF573C6287C8DEBF8_CustomAttributesCacheGenerator,
	U3CStartU3Ed__2_t69FDEAF9EA13E082CF88C7B60B90C5A05CC6179C_CustomAttributesCacheGenerator,
	U3CInitU3Ed__10_tDC9857230B9D766BA44C1A8CD9BC89DAF332560A_CustomAttributesCacheGenerator,
	U3CRunPendingCommandsU3Ed__12_t3B51F7A544880108D20AA679685AAD3200FD6E8D_CustomAttributesCacheGenerator,
	U3CGetResultU3Ed__13_t429D1A37625B8E217E88830CCFE7B4B9E5F3DCED_CustomAttributesCacheGenerator,
	U3CProxyLanguagesU3Ed__19_t3B013A40DC9B77C22F68441BA9F709DD9D113FE9_CustomAttributesCacheGenerator,
	U3CStartU3Ed__28_t15833A9AB280EADB404BBB5150CC5E571050157D_CustomAttributesCacheGenerator,
	U3CGetVoicesU3Ed__32_tD206C9C44DE7C20EB7573ED4D8761B0FD13677FD_CustomAttributesCacheGenerator,
	U3CSetPitchU3Ed__35_tF30264CE7FB8EFB8DD7CC3A54F6E538C7526D22E_CustomAttributesCacheGenerator,
	U3CSetRateU3Ed__36_tE587DF9904D56F1F471E8AAB2C639678AE1C2983_CustomAttributesCacheGenerator,
	U3CCreateNameInputFieldU3Ed__18_tCF407195076F20F3CC29E9320B5836378BF87FA4_CustomAttributesCacheGenerator,
	U3CCreateTalkInputFieldU3Ed__19_t5C727E8D85F30A4CC308C816B210A1FD28056A29_CustomAttributesCacheGenerator,
	U3CStartU3Ed__21_tCD0B0FE65242BC30DD807F964ABE50628F2C6FD8_CustomAttributesCacheGenerator,
	U3CGetVoicesU3Ed__24_tC53D4CB8A21E838EE8088A9FDD86CA7324EB8050_CustomAttributesCacheGenerator,
	U3CPrivateImplementationDetailsU3E_t2672E9E7EEBD118534BF12C6E47D2B3DD7D9E562_CustomAttributesCacheGenerator,
	Speaker_t639C193C2402AD73ED45F6530A8FD0EB62D3ACA1_CustomAttributesCacheGenerator_CustomProvider,
	Speaker_t639C193C2402AD73ED45F6530A8FD0EB62D3ACA1_CustomAttributesCacheGenerator_CustomMode,
	Speaker_t639C193C2402AD73ED45F6530A8FD0EB62D3ACA1_CustomAttributesCacheGenerator_MaryTTSMode,
	Speaker_t639C193C2402AD73ED45F6530A8FD0EB62D3ACA1_CustomAttributesCacheGenerator_MaryTTSUrl,
	Speaker_t639C193C2402AD73ED45F6530A8FD0EB62D3ACA1_CustomAttributesCacheGenerator_MaryTTSPort,
	Speaker_t639C193C2402AD73ED45F6530A8FD0EB62D3ACA1_CustomAttributesCacheGenerator_MaryTTSUser,
	Speaker_t639C193C2402AD73ED45F6530A8FD0EB62D3ACA1_CustomAttributesCacheGenerator_MaryTTSPassword,
	Speaker_t639C193C2402AD73ED45F6530A8FD0EB62D3ACA1_CustomAttributesCacheGenerator_MaryTTSType,
	Speaker_t639C193C2402AD73ED45F6530A8FD0EB62D3ACA1_CustomAttributesCacheGenerator_ESpeakMode,
	Speaker_t639C193C2402AD73ED45F6530A8FD0EB62D3ACA1_CustomAttributesCacheGenerator_ESpeakModifier,
	Speaker_t639C193C2402AD73ED45F6530A8FD0EB62D3ACA1_CustomAttributesCacheGenerator_AutoClearTags,
	Speaker_t639C193C2402AD73ED45F6530A8FD0EB62D3ACA1_CustomAttributesCacheGenerator_WSANative,
	Speaker_t639C193C2402AD73ED45F6530A8FD0EB62D3ACA1_CustomAttributesCacheGenerator_SilenceOnDisable,
	Speaker_t639C193C2402AD73ED45F6530A8FD0EB62D3ACA1_CustomAttributesCacheGenerator_SilenceOnFocustLost,
	Speaker_t639C193C2402AD73ED45F6530A8FD0EB62D3ACA1_CustomAttributesCacheGenerator_DontDestroy,
	Speaker_t639C193C2402AD73ED45F6530A8FD0EB62D3ACA1_CustomAttributesCacheGenerator_U3CareVoicesReadyU3Ek__BackingField,
	Speaker_t639C193C2402AD73ED45F6530A8FD0EB62D3ACA1_CustomAttributesCacheGenerator_U3CenforcedStandaloneTTSU3Ek__BackingField,
	AudioFileGenerator_t25C4CCBDEAE5E63CB0745BACFAEF787E31EEA298_CustomAttributesCacheGenerator_TextFiles,
	AudioFileGenerator_t25C4CCBDEAE5E63CB0745BACFAEF787E31EEA298_CustomAttributesCacheGenerator_FileInsideAssets,
	AudioFileGenerator_t25C4CCBDEAE5E63CB0745BACFAEF787E31EEA298_CustomAttributesCacheGenerator_isNormalize,
	AudioFileGenerator_t25C4CCBDEAE5E63CB0745BACFAEF787E31EEA298_CustomAttributesCacheGenerator_GenerateOnStart,
	ChangeGender_t7942C8B9C9685EF714B6674AC27C681AC3D91104_CustomAttributesCacheGenerator_NewGender,
	ChangeGender_t7942C8B9C9685EF714B6674AC27C681AC3D91104_CustomAttributesCacheGenerator_RefreshOnVoicesReady,
	ChangeGender_t7942C8B9C9685EF714B6674AC27C681AC3D91104_CustomAttributesCacheGenerator_ESpeakOnly,
	Loudspeaker_t78C64F45415540BE87422E3A54942AA35E24ED42_CustomAttributesCacheGenerator_Source,
	Loudspeaker_t78C64F45415540BE87422E3A54942AA35E24ED42_CustomAttributesCacheGenerator_Synchronized,
	Loudspeaker_t78C64F45415540BE87422E3A54942AA35E24ED42_CustomAttributesCacheGenerator_SilenceSource,
	Paralanguage_t257B51534DB18301E85DF9EABF6265E9C7FC4F25_CustomAttributesCacheGenerator_Text,
	Paralanguage_t257B51534DB18301E85DF9EABF6265E9C7FC4F25_CustomAttributesCacheGenerator_Voices,
	Paralanguage_t257B51534DB18301E85DF9EABF6265E9C7FC4F25_CustomAttributesCacheGenerator_Mode,
	Paralanguage_t257B51534DB18301E85DF9EABF6265E9C7FC4F25_CustomAttributesCacheGenerator_Clips,
	Paralanguage_t257B51534DB18301E85DF9EABF6265E9C7FC4F25_CustomAttributesCacheGenerator_Rate,
	Paralanguage_t257B51534DB18301E85DF9EABF6265E9C7FC4F25_CustomAttributesCacheGenerator_Pitch,
	Paralanguage_t257B51534DB18301E85DF9EABF6265E9C7FC4F25_CustomAttributesCacheGenerator_Volume,
	Paralanguage_t257B51534DB18301E85DF9EABF6265E9C7FC4F25_CustomAttributesCacheGenerator_PlayOnStart,
	Paralanguage_t257B51534DB18301E85DF9EABF6265E9C7FC4F25_CustomAttributesCacheGenerator_Delay,
	Sequencer_t55938B3A6AE7BE5ECE446AC08D43795759152190_CustomAttributesCacheGenerator_Sequences,
	Sequencer_t55938B3A6AE7BE5ECE446AC08D43795759152190_CustomAttributesCacheGenerator_Delay,
	Sequencer_t55938B3A6AE7BE5ECE446AC08D43795759152190_CustomAttributesCacheGenerator_PlayOnStart,
	SpeechText_t89F39958695673475D1D42B203FA444F75E5ED39_CustomAttributesCacheGenerator_Text,
	SpeechText_t89F39958695673475D1D42B203FA444F75E5ED39_CustomAttributesCacheGenerator_Voices,
	SpeechText_t89F39958695673475D1D42B203FA444F75E5ED39_CustomAttributesCacheGenerator_Mode,
	SpeechText_t89F39958695673475D1D42B203FA444F75E5ED39_CustomAttributesCacheGenerator_Source,
	SpeechText_t89F39958695673475D1D42B203FA444F75E5ED39_CustomAttributesCacheGenerator_Rate,
	SpeechText_t89F39958695673475D1D42B203FA444F75E5ED39_CustomAttributesCacheGenerator_Pitch,
	SpeechText_t89F39958695673475D1D42B203FA444F75E5ED39_CustomAttributesCacheGenerator_Volume,
	SpeechText_t89F39958695673475D1D42B203FA444F75E5ED39_CustomAttributesCacheGenerator_PlayOnStart,
	SpeechText_t89F39958695673475D1D42B203FA444F75E5ED39_CustomAttributesCacheGenerator_Delay,
	SpeechText_t89F39958695673475D1D42B203FA444F75E5ED39_CustomAttributesCacheGenerator_GenerateAudioFile,
	SpeechText_t89F39958695673475D1D42B203FA444F75E5ED39_CustomAttributesCacheGenerator_FileName,
	SpeechText_t89F39958695673475D1D42B203FA444F75E5ED39_CustomAttributesCacheGenerator_FileInsideAssets,
	TextFileSpeaker_t28C4E74F2AA327B88BD90BBE4A123A435747410C_CustomAttributesCacheGenerator_TextFiles,
	TextFileSpeaker_t28C4E74F2AA327B88BD90BBE4A123A435747410C_CustomAttributesCacheGenerator_Voices,
	TextFileSpeaker_t28C4E74F2AA327B88BD90BBE4A123A435747410C_CustomAttributesCacheGenerator_Mode,
	TextFileSpeaker_t28C4E74F2AA327B88BD90BBE4A123A435747410C_CustomAttributesCacheGenerator_PlayOnStart,
	TextFileSpeaker_t28C4E74F2AA327B88BD90BBE4A123A435747410C_CustomAttributesCacheGenerator_PlayAllOnStart,
	TextFileSpeaker_t28C4E74F2AA327B88BD90BBE4A123A435747410C_CustomAttributesCacheGenerator_SpeakRandom,
	TextFileSpeaker_t28C4E74F2AA327B88BD90BBE4A123A435747410C_CustomAttributesCacheGenerator_Delay,
	TextFileSpeaker_t28C4E74F2AA327B88BD90BBE4A123A435747410C_CustomAttributesCacheGenerator_Source,
	TextFileSpeaker_t28C4E74F2AA327B88BD90BBE4A123A435747410C_CustomAttributesCacheGenerator_Rate,
	TextFileSpeaker_t28C4E74F2AA327B88BD90BBE4A123A435747410C_CustomAttributesCacheGenerator_Pitch,
	TextFileSpeaker_t28C4E74F2AA327B88BD90BBE4A123A435747410C_CustomAttributesCacheGenerator_Volume,
	VoiceInitalizer_t628535D1C01FC59180A5EE351F3502A45342556C_CustomAttributesCacheGenerator_Provider,
	VoiceInitalizer_t628535D1C01FC59180A5EE351F3502A45342556C_CustomAttributesCacheGenerator_VoiceNames,
	VoiceInitalizer_t628535D1C01FC59180A5EE351F3502A45342556C_CustomAttributesCacheGenerator_AllVoices,
	VoiceInitalizer_t628535D1C01FC59180A5EE351F3502A45342556C_CustomAttributesCacheGenerator_DestroyWhenFinished,
	Sequence_t9F70DC5B0D7CF74B0FC88E0784E5857D0A475F79_CustomAttributesCacheGenerator_Text,
	Sequence_t9F70DC5B0D7CF74B0FC88E0784E5857D0A475F79_CustomAttributesCacheGenerator_Voices,
	Sequence_t9F70DC5B0D7CF74B0FC88E0784E5857D0A475F79_CustomAttributesCacheGenerator_Mode,
	Sequence_t9F70DC5B0D7CF74B0FC88E0784E5857D0A475F79_CustomAttributesCacheGenerator_Source,
	Sequence_t9F70DC5B0D7CF74B0FC88E0784E5857D0A475F79_CustomAttributesCacheGenerator_Rate,
	Sequence_t9F70DC5B0D7CF74B0FC88E0784E5857D0A475F79_CustomAttributesCacheGenerator_Pitch,
	Sequence_t9F70DC5B0D7CF74B0FC88E0784E5857D0A475F79_CustomAttributesCacheGenerator_Volume,
	Sequence_t9F70DC5B0D7CF74B0FC88E0784E5857D0A475F79_CustomAttributesCacheGenerator_initalized,
	Voice_t4C22EF6D29FB2A9A2E0AD7E539BDCD5F93C63D01_CustomAttributesCacheGenerator_Name,
	Voice_t4C22EF6D29FB2A9A2E0AD7E539BDCD5F93C63D01_CustomAttributesCacheGenerator_Description,
	Voice_t4C22EF6D29FB2A9A2E0AD7E539BDCD5F93C63D01_CustomAttributesCacheGenerator_Gender,
	Voice_t4C22EF6D29FB2A9A2E0AD7E539BDCD5F93C63D01_CustomAttributesCacheGenerator_Age,
	Voice_t4C22EF6D29FB2A9A2E0AD7E539BDCD5F93C63D01_CustomAttributesCacheGenerator_Identifier,
	Voice_t4C22EF6D29FB2A9A2E0AD7E539BDCD5F93C63D01_CustomAttributesCacheGenerator_Vendor,
	Voice_t4C22EF6D29FB2A9A2E0AD7E539BDCD5F93C63D01_CustomAttributesCacheGenerator_Version,
	VoiceAlias_t721FA1FC67A2E220FE5CDEE91532251CEED5B588_CustomAttributesCacheGenerator_VoiceNameWindows,
	VoiceAlias_t721FA1FC67A2E220FE5CDEE91532251CEED5B588_CustomAttributesCacheGenerator_VoiceNameMac,
	VoiceAlias_t721FA1FC67A2E220FE5CDEE91532251CEED5B588_CustomAttributesCacheGenerator_VoiceNameLinux,
	VoiceAlias_t721FA1FC67A2E220FE5CDEE91532251CEED5B588_CustomAttributesCacheGenerator_VoiceNameAndroid,
	VoiceAlias_t721FA1FC67A2E220FE5CDEE91532251CEED5B588_CustomAttributesCacheGenerator_VoiceNameIOS,
	VoiceAlias_t721FA1FC67A2E220FE5CDEE91532251CEED5B588_CustomAttributesCacheGenerator_VoiceNameWSA,
	VoiceAlias_t721FA1FC67A2E220FE5CDEE91532251CEED5B588_CustomAttributesCacheGenerator_VoiceNameMaryTTS,
	VoiceAlias_t721FA1FC67A2E220FE5CDEE91532251CEED5B588_CustomAttributesCacheGenerator_VoiceNameCustom,
	VoiceAlias_t721FA1FC67A2E220FE5CDEE91532251CEED5B588_CustomAttributesCacheGenerator_Culture,
	VoiceAlias_t721FA1FC67A2E220FE5CDEE91532251CEED5B588_CustomAttributesCacheGenerator_Gender,
	Dialog_t638F1505D57D2F294DDEA45174D9130410067CD7_CustomAttributesCacheGenerator_CultureA,
	Dialog_t638F1505D57D2F294DDEA45174D9130410067CD7_CustomAttributesCacheGenerator_RateA,
	Dialog_t638F1505D57D2F294DDEA45174D9130410067CD7_CustomAttributesCacheGenerator_RateB,
	Dialog_t638F1505D57D2F294DDEA45174D9130410067CD7_CustomAttributesCacheGenerator_PitchA,
	Dialog_t638F1505D57D2F294DDEA45174D9130410067CD7_CustomAttributesCacheGenerator_PitchB,
	Dialog_t638F1505D57D2F294DDEA45174D9130410067CD7_CustomAttributesCacheGenerator_VolumeA,
	Dialog_t638F1505D57D2F294DDEA45174D9130410067CD7_CustomAttributesCacheGenerator_VolumeB,
	Dialog_t638F1505D57D2F294DDEA45174D9130410067CD7_CustomAttributesCacheGenerator_DialogPersonA,
	GUIAudioFilter_tCC8DE531770AD69322920FE7BBCC9EC5B5EB2249_CustomAttributesCacheGenerator_Source,
	GUIAudioFilter_tCC8DE531770AD69322920FE7BBCC9EC5B5EB2249_CustomAttributesCacheGenerator_ReverbFilter,
	GUIAudioFilter_tCC8DE531770AD69322920FE7BBCC9EC5B5EB2249_CustomAttributesCacheGenerator_Distortion,
	GUIDialog_tC739AB986043972A584C1DE7BE4EB16DD4E8464E_CustomAttributesCacheGenerator_DialogScript,
	GUIDialog_tC739AB986043972A584C1DE7BE4EB16DD4E8464E_CustomAttributesCacheGenerator_SpeakerColor,
	GUIMain_tD82B775FB46C7E2456748B5A944CBBFF713ECB61_CustomAttributesCacheGenerator_Name,
	GUIMultiAudioFilter_t9FDD491C7C801C47D7872FE2AD05D6CAC2A3557F_CustomAttributesCacheGenerator_Sources,
	GUIMultiAudioFilter_t9FDD491C7C801C47D7872FE2AD05D6CAC2A3557F_CustomAttributesCacheGenerator_ReverbFilters,
	GUIMultiAudioFilter_t9FDD491C7C801C47D7872FE2AD05D6CAC2A3557F_CustomAttributesCacheGenerator_Distortion,
	GUIScenes_t6F72249B673EF03AF769EE6AC0EC241AA0F8E6CB_CustomAttributesCacheGenerator_PreviousScene,
	GUIScenes_t6F72249B673EF03AF769EE6AC0EC241AA0F8E6CB_CustomAttributesCacheGenerator_PreviousSceneWebGL,
	GUIScenes_t6F72249B673EF03AF769EE6AC0EC241AA0F8E6CB_CustomAttributesCacheGenerator_NextScene,
	GUIScenes_t6F72249B673EF03AF769EE6AC0EC241AA0F8E6CB_CustomAttributesCacheGenerator_NextSceneWebGL,
	GUISpeech_t415B11E6596BA1EFF7191AD10A721FEF21BF97A0_CustomAttributesCacheGenerator_StartAsNative,
	GUISpeech_t415B11E6596BA1EFF7191AD10A721FEF21BF97A0_CustomAttributesCacheGenerator_ItemPrefab,
	GUISpeech_t415B11E6596BA1EFF7191AD10A721FEF21BF97A0_CustomAttributesCacheGenerator_Input,
	SendMessage_t9D6A7D158B11451BF3D7DCC357330A2F4363BB94_CustomAttributesCacheGenerator_TextA,
	SendMessage_t9D6A7D158B11451BF3D7DCC357330A2F4363BB94_CustomAttributesCacheGenerator_TextB,
	Simple_t557AF9B6F3B5301F929702090D5291F7DBAA5490_CustomAttributesCacheGenerator_SourceA,
	Simple_t557AF9B6F3B5301F929702090D5291F7DBAA5490_CustomAttributesCacheGenerator_RateSpeakerA,
	Simple_t557AF9B6F3B5301F929702090D5291F7DBAA5490_CustomAttributesCacheGenerator_RateSpeakerB,
	Simple_t557AF9B6F3B5301F929702090D5291F7DBAA5490_CustomAttributesCacheGenerator_TextSpeakerA,
	SimpleNative_t00B45673BD9201F12CD43BDD847E5C69489CD131_CustomAttributesCacheGenerator_RateSpeakerA,
	SimpleNative_t00B45673BD9201F12CD43BDD847E5C69489CD131_CustomAttributesCacheGenerator_RateSpeakerB,
	SimpleNative_t00B45673BD9201F12CD43BDD847E5C69489CD131_CustomAttributesCacheGenerator_RateSpeakerC,
	SimpleNative_t00B45673BD9201F12CD43BDD847E5C69489CD131_CustomAttributesCacheGenerator_TextSpeakerA,
	NativeController_t75220F3EAB005E617337B7EBD5673FE5E94A4B26_CustomAttributesCacheGenerator_Active,
	NativeController_t75220F3EAB005E617337B7EBD5673FE5E94A4B26_CustomAttributesCacheGenerator_Objects,
	Bots_tAA8E22B829835FB2E226753C04D58B3CA94A0083_CustomAttributesCacheGenerator_SourceA,
	Bots_tAA8E22B829835FB2E226753C04D58B3CA94A0083_CustomAttributesCacheGenerator_SourceB,
	Bots_tAA8E22B829835FB2E226753C04D58B3CA94A0083_CustomAttributesCacheGenerator_ConverstationsA,
	Bots_tAA8E22B829835FB2E226753C04D58B3CA94A0083_CustomAttributesCacheGenerator_ConverstationsB,
	Speak_tC8B5D9021E212D52637E32CF9EC1CC0719DA4D8D_CustomAttributesCacheGenerator_Source,
	Speak_tC8B5D9021E212D52637E32CF9EC1CC0719DA4D8D_CustomAttributesCacheGenerator_Salsa,
	Speak_tC8B5D9021E212D52637E32CF9EC1CC0719DA4D8D_CustomAttributesCacheGenerator_EnterText,
	Speak_tC8B5D9021E212D52637E32CF9EC1CC0719DA4D8D_CustomAttributesCacheGenerator_RateSlider,
	Speak_tC8B5D9021E212D52637E32CF9EC1CC0719DA4D8D_CustomAttributesCacheGenerator_PitchSlider,
	Speak2D_t6FD5D57CCBF1A7AF85A9897757C662422B48DAA8_CustomAttributesCacheGenerator_Source,
	Speak2D_t6FD5D57CCBF1A7AF85A9897757C662422B48DAA8_CustomAttributesCacheGenerator_Salsa,
	Speak2D_t6FD5D57CCBF1A7AF85A9897757C662422B48DAA8_CustomAttributesCacheGenerator_EnterText,
	Speak2D_t6FD5D57CCBF1A7AF85A9897757C662422B48DAA8_CustomAttributesCacheGenerator_RateSlider,
	Speak2D_t6FD5D57CCBF1A7AF85A9897757C662422B48DAA8_CustomAttributesCacheGenerator_PitchSlider,
	SpeakSimple_tCF365AE8FFE5E1F8E5637CD7B5D295F648D314A6_CustomAttributesCacheGenerator_Source,
	SpeakSimple_tCF365AE8FFE5E1F8E5637CD7B5D295F648D314A6_CustomAttributesCacheGenerator_EnterText,
	SpeakSimple_tCF365AE8FFE5E1F8E5637CD7B5D295F648D314A6_CustomAttributesCacheGenerator_RateSlider,
	SpeakSimple_tCF365AE8FFE5E1F8E5637CD7B5D295F648D314A6_CustomAttributesCacheGenerator_PitchSlider,
	UIFocus_t8FA57C45177DF0396463F9D4D9B37C37647BFF40_CustomAttributesCacheGenerator_ManagerName,
	UIHint_t5B5786B738951189B871A6FF5D23A473222E03C7_CustomAttributesCacheGenerator_Group,
	UIHint_t5B5786B738951189B871A6FF5D23A473222E03C7_CustomAttributesCacheGenerator_Delay,
	UIHint_t5B5786B738951189B871A6FF5D23A473222E03C7_CustomAttributesCacheGenerator_FadeTime,
	UIHint_t5B5786B738951189B871A6FF5D23A473222E03C7_CustomAttributesCacheGenerator_Disable,
	UIHint_t5B5786B738951189B871A6FF5D23A473222E03C7_CustomAttributesCacheGenerator_FadeAtStart,
	UIResize_t37199FAAC3C3D6AC437662F9CF19B9ADDB91F228_CustomAttributesCacheGenerator_MinSize,
	UIResize_t37199FAAC3C3D6AC437662F9CF19B9ADDB91F228_CustomAttributesCacheGenerator_MaxSize,
	UIWindowManager_t227DE834418A39C678BF8498F47C8743E421AB46_CustomAttributesCacheGenerator_Windows,
	WindowManager_t8F0E4C96CAFAD4C11F999874779FE1B04137E0ED_CustomAttributesCacheGenerator_Speed,
	WindowManager_t8F0E4C96CAFAD4C11F999874779FE1B04137E0ED_CustomAttributesCacheGenerator_Dependencies,
	AudioFilterController_t499935DE576B1ABBEF76298961F7B6A5C1F29DD0_CustomAttributesCacheGenerator_FindAllAudioFiltersOnStart,
	AudioFilterController_t499935DE576B1ABBEF76298961F7B6A5C1F29DD0_CustomAttributesCacheGenerator_ResetAudioFiltersOnStart,
	AudioFilterController_t499935DE576B1ABBEF76298961F7B6A5C1F29DD0_CustomAttributesCacheGenerator_ReverbFilterDropdown,
	AudioSourceController_tB529F0E16FE089F98277E91456D61F8BC48EAA4A_CustomAttributesCacheGenerator_FindAllAudioSourcesOnStart,
	AudioSourceController_tB529F0E16FE089F98277E91456D61F8BC48EAA4A_CustomAttributesCacheGenerator_AudioSources,
	AudioSourceController_tB529F0E16FE089F98277E91456D61F8BC48EAA4A_CustomAttributesCacheGenerator_ResetAudioSourcesOnStart,
	AudioSourceController_tB529F0E16FE089F98277E91456D61F8BC48EAA4A_CustomAttributesCacheGenerator_Mute,
	AudioSourceController_tB529F0E16FE089F98277E91456D61F8BC48EAA4A_CustomAttributesCacheGenerator_Loop,
	AudioSourceController_tB529F0E16FE089F98277E91456D61F8BC48EAA4A_CustomAttributesCacheGenerator_Volume,
	AudioSourceController_tB529F0E16FE089F98277E91456D61F8BC48EAA4A_CustomAttributesCacheGenerator_Pitch,
	AudioSourceController_tB529F0E16FE089F98277E91456D61F8BC48EAA4A_CustomAttributesCacheGenerator_StereoPan,
	AudioSourceController_tB529F0E16FE089F98277E91456D61F8BC48EAA4A_CustomAttributesCacheGenerator_VolumeText,
	FPSDisplay_tE1C362120A52899D78B55F41C6639FE89D2AF6F4_CustomAttributesCacheGenerator_FPS,
	SurviveSceneSwitch_t38706D7E5CC93E19DB435134BE37142C3470E26A_CustomAttributesCacheGenerator_Survivors,
	TakeScreenshot_t585EAF716A55E4C63BE6FEEF832D186885CA03A6_CustomAttributesCacheGenerator_Prefix,
	TakeScreenshot_t585EAF716A55E4C63BE6FEEF832D186885CA03A6_CustomAttributesCacheGenerator_Scale,
	TakeScreenshot_t585EAF716A55E4C63BE6FEEF832D186885CA03A6_CustomAttributesCacheGenerator_KeyCode,
	BackgroundController_tFA862354EABE46363EBE7CAD3A85EA87F7EC6F98_CustomAttributesCacheGenerator_Objects,
	CTProcess_t03C134618294F58C911F9F8CB956CD787127B631_CustomAttributesCacheGenerator_U3CHandleU3Ek__BackingField,
	CTProcess_t03C134618294F58C911F9F8CB956CD787127B631_CustomAttributesCacheGenerator_U3CIdU3Ek__BackingField,
	CTProcess_t03C134618294F58C911F9F8CB956CD787127B631_CustomAttributesCacheGenerator_U3CHasExitedU3Ek__BackingField,
	CTProcess_t03C134618294F58C911F9F8CB956CD787127B631_CustomAttributesCacheGenerator_U3CStartTimeU3Ek__BackingField,
	CTProcess_t03C134618294F58C911F9F8CB956CD787127B631_CustomAttributesCacheGenerator_U3CExitTimeU3Ek__BackingField,
	CTProcess_t03C134618294F58C911F9F8CB956CD787127B631_CustomAttributesCacheGenerator_U3CStandardOutputU3Ek__BackingField,
	CTProcess_t03C134618294F58C911F9F8CB956CD787127B631_CustomAttributesCacheGenerator_U3CStandardErrorU3Ek__BackingField,
	CTProcess_t03C134618294F58C911F9F8CB956CD787127B631_CustomAttributesCacheGenerator_U3CisBusyU3Ek__BackingField,
	CTProcessStartInfo_t18282D67663A337B9F05446EAFAB980BEEA87025_CustomAttributesCacheGenerator_U3CUseThreadU3Ek__BackingField,
	CTProcessStartInfo_t18282D67663A337B9F05446EAFAB980BEEA87025_CustomAttributesCacheGenerator_U3CUseCmdExecuteU3Ek__BackingField,
	CTProcessStartInfo_t18282D67663A337B9F05446EAFAB980BEEA87025_CustomAttributesCacheGenerator_U3CFileNameU3Ek__BackingField,
	CTProcessStartInfo_t18282D67663A337B9F05446EAFAB980BEEA87025_CustomAttributesCacheGenerator_U3CArgumentsU3Ek__BackingField,
	CTProcessStartInfo_t18282D67663A337B9F05446EAFAB980BEEA87025_CustomAttributesCacheGenerator_U3CCreateNoWindowU3Ek__BackingField,
	CTProcessStartInfo_t18282D67663A337B9F05446EAFAB980BEEA87025_CustomAttributesCacheGenerator_U3CWorkingDirectoryU3Ek__BackingField,
	CTProcessStartInfo_t18282D67663A337B9F05446EAFAB980BEEA87025_CustomAttributesCacheGenerator_U3CRedirectStandardOutputU3Ek__BackingField,
	CTProcessStartInfo_t18282D67663A337B9F05446EAFAB980BEEA87025_CustomAttributesCacheGenerator_U3CRedirectStandardErrorU3Ek__BackingField,
	CTProcessStartInfo_t18282D67663A337B9F05446EAFAB980BEEA87025_CustomAttributesCacheGenerator_U3CStandardOutputEncodingU3Ek__BackingField,
	CTProcessStartInfo_t18282D67663A337B9F05446EAFAB980BEEA87025_CustomAttributesCacheGenerator_U3CStandardErrorEncodingU3Ek__BackingField,
	CTProcessStartInfo_t18282D67663A337B9F05446EAFAB980BEEA87025_CustomAttributesCacheGenerator_U3CUseShellExecuteU3Ek__BackingField,
	CTWebClient_t32C9C802CE03700DC8C37D1952056B588BE7D709_CustomAttributesCacheGenerator_U3CTimeoutU3Ek__BackingField,
	CTWebClient_t32C9C802CE03700DC8C37D1952056B588BE7D709_CustomAttributesCacheGenerator_U3CConnectionLimitU3Ek__BackingField,
	FFTAnalyzer_t9DC55C861DFF72F4457F5CE0CADF148FEA618109_CustomAttributesCacheGenerator_Samples,
	FFTAnalyzer_t9DC55C861DFF72F4457F5CE0CADF148FEA618109_CustomAttributesCacheGenerator_Channel,
	FFTAnalyzer_t9DC55C861DFF72F4457F5CE0CADF148FEA618109_CustomAttributesCacheGenerator_FFTMode,
	PlatformController_t0109B938FCC3CF39CF034B9211211DB02853B670_CustomAttributesCacheGenerator_Platforms,
	PlatformController_t0109B938FCC3CF39CF034B9211211DB02853B670_CustomAttributesCacheGenerator_Active,
	PlatformController_t0109B938FCC3CF39CF034B9211211DB02853B670_CustomAttributesCacheGenerator_Objects,
	RandomColor_tF868105D2E2823ACD264C32AACFC0C572FFC4C4A_CustomAttributesCacheGenerator_UseInterval,
	RandomColor_tF868105D2E2823ACD264C32AACFC0C572FFC4C4A_CustomAttributesCacheGenerator_ChangeInterval,
	RandomColor_tF868105D2E2823ACD264C32AACFC0C572FFC4C4A_CustomAttributesCacheGenerator_HueRange,
	RandomColor_tF868105D2E2823ACD264C32AACFC0C572FFC4C4A_CustomAttributesCacheGenerator_SaturationRange,
	RandomColor_tF868105D2E2823ACD264C32AACFC0C572FFC4C4A_CustomAttributesCacheGenerator_ValueRange,
	RandomColor_tF868105D2E2823ACD264C32AACFC0C572FFC4C4A_CustomAttributesCacheGenerator_AlphaRange,
	RandomColor_tF868105D2E2823ACD264C32AACFC0C572FFC4C4A_CustomAttributesCacheGenerator_GrayScale,
	RandomColor_tF868105D2E2823ACD264C32AACFC0C572FFC4C4A_CustomAttributesCacheGenerator_Material,
	RandomColor_tF868105D2E2823ACD264C32AACFC0C572FFC4C4A_CustomAttributesCacheGenerator_RandomColorAtStart,
	RandomRotator_t2F779136B71B324B8401B297A8978D14D82F4AF8_CustomAttributesCacheGenerator_UseInterval,
	RandomRotator_t2F779136B71B324B8401B297A8978D14D82F4AF8_CustomAttributesCacheGenerator_ChangeInterval,
	RandomRotator_t2F779136B71B324B8401B297A8978D14D82F4AF8_CustomAttributesCacheGenerator_SpeedMin,
	RandomRotator_t2F779136B71B324B8401B297A8978D14D82F4AF8_CustomAttributesCacheGenerator_SpeedMax,
	RandomRotator_t2F779136B71B324B8401B297A8978D14D82F4AF8_CustomAttributesCacheGenerator_RandomRotationAtStart,
	RandomScaler_tBD5EA91666AEFA8CF01ED1DD1C749F50A5DB84D3_CustomAttributesCacheGenerator_UseInterval,
	RandomScaler_tBD5EA91666AEFA8CF01ED1DD1C749F50A5DB84D3_CustomAttributesCacheGenerator_ChangeInterval,
	RandomScaler_tBD5EA91666AEFA8CF01ED1DD1C749F50A5DB84D3_CustomAttributesCacheGenerator_ScaleMin,
	RandomScaler_tBD5EA91666AEFA8CF01ED1DD1C749F50A5DB84D3_CustomAttributesCacheGenerator_ScaleMax,
	RandomScaler_tBD5EA91666AEFA8CF01ED1DD1C749F50A5DB84D3_CustomAttributesCacheGenerator_Uniform,
	RandomScaler_tBD5EA91666AEFA8CF01ED1DD1C749F50A5DB84D3_CustomAttributesCacheGenerator_RandomScaleAtStart,
	SpectrumVisualizer_tE42A510857BC8EA32CF8F189BA9ABC5BED66AADA_CustomAttributesCacheGenerator_Analyzer,
	SpectrumVisualizer_tE42A510857BC8EA32CF8F189BA9ABC5BED66AADA_CustomAttributesCacheGenerator_VisualPrefab,
	SpectrumVisualizer_tE42A510857BC8EA32CF8F189BA9ABC5BED66AADA_CustomAttributesCacheGenerator_Width,
	SpectrumVisualizer_tE42A510857BC8EA32CF8F189BA9ABC5BED66AADA_CustomAttributesCacheGenerator_Gain,
	SpectrumVisualizer_tE42A510857BC8EA32CF8F189BA9ABC5BED66AADA_CustomAttributesCacheGenerator_LeftToRight,
	SpectrumVisualizer_tE42A510857BC8EA32CF8F189BA9ABC5BED66AADA_CustomAttributesCacheGenerator_Opacity,
	ExtensionMethods_t3A63E01D154FF156B5910983BBD9656044FCBAEF_CustomAttributesCacheGenerator_ExtensionMethods_CTToTitleCase_m0FAF40693A916D1BA985195E30AA7B9753C11DE2,
	ExtensionMethods_t3A63E01D154FF156B5910983BBD9656044FCBAEF_CustomAttributesCacheGenerator_ExtensionMethods_CTReverse_m375B1B5F94367BC73BC054481C4528E1829758A0,
	ExtensionMethods_t3A63E01D154FF156B5910983BBD9656044FCBAEF_CustomAttributesCacheGenerator_ExtensionMethods_CTReplace_mE45837F5BF2ECDD64251F7DC85303D36308AFBB9,
	ExtensionMethods_t3A63E01D154FF156B5910983BBD9656044FCBAEF_CustomAttributesCacheGenerator_ExtensionMethods_CTEquals_m70FD226B78B4DD7ED0672F326B16568EA3D9B446,
	ExtensionMethods_t3A63E01D154FF156B5910983BBD9656044FCBAEF_CustomAttributesCacheGenerator_ExtensionMethods_CTContains_m52B3F3D1019BBD1746371264A2671656CDADB3C2,
	ExtensionMethods_t3A63E01D154FF156B5910983BBD9656044FCBAEF_CustomAttributesCacheGenerator_ExtensionMethods_CTContainsAny_m50A29F87C6A50663D9B599FEA1D1359EC07969A3,
	ExtensionMethods_t3A63E01D154FF156B5910983BBD9656044FCBAEF_CustomAttributesCacheGenerator_ExtensionMethods_CTContainsAll_mAB9F9024F8E76F4449FE666602718634956C2464,
	ExtensionMethods_t3A63E01D154FF156B5910983BBD9656044FCBAEF_CustomAttributesCacheGenerator_ExtensionMethods_CTShuffle_m69617262C775678CA0CFFF468C20E275ECABD211,
	ExtensionMethods_t3A63E01D154FF156B5910983BBD9656044FCBAEF_CustomAttributesCacheGenerator_ExtensionMethods_CTDump_mB5A7A1F35FC14838220750C2889DE6D41C68F086,
	ExtensionMethods_t3A63E01D154FF156B5910983BBD9656044FCBAEF_CustomAttributesCacheGenerator_ExtensionMethods_CTDump_m9372AF9431740E725E0A0EE720EEB4B51EF8D626,
	ExtensionMethods_t3A63E01D154FF156B5910983BBD9656044FCBAEF_CustomAttributesCacheGenerator_ExtensionMethods_CTDump_mDD2831261D66781D294ADBFFBD9A397D4A5B4681,
	ExtensionMethods_t3A63E01D154FF156B5910983BBD9656044FCBAEF_CustomAttributesCacheGenerator_ExtensionMethods_CTDump_m2A792DFFEECEFF18E99A289916B9165C127E7883,
	ExtensionMethods_t3A63E01D154FF156B5910983BBD9656044FCBAEF_CustomAttributesCacheGenerator_ExtensionMethods_CTDump_mEDCBC052999C7F88ECF5897E865B6467FF1F452E,
	ExtensionMethods_t3A63E01D154FF156B5910983BBD9656044FCBAEF_CustomAttributesCacheGenerator_ExtensionMethods_CTToString_m181115D9882BBB5E201C31443FCFA49DBADE6D30,
	ExtensionMethods_t3A63E01D154FF156B5910983BBD9656044FCBAEF_CustomAttributesCacheGenerator_ExtensionMethods_CTShuffle_mB67EA3011C43B440B75C4B90B8719323BEE020E4,
	ExtensionMethods_t3A63E01D154FF156B5910983BBD9656044FCBAEF_CustomAttributesCacheGenerator_ExtensionMethods_CTDump_m63A8F721F15E1CB0491451A09378CBB63710F902,
	ExtensionMethods_t3A63E01D154FF156B5910983BBD9656044FCBAEF_CustomAttributesCacheGenerator_ExtensionMethods_CTDump_m59F8DE0A578B3B891B4D8C1ED38688E86940F976,
	ExtensionMethods_t3A63E01D154FF156B5910983BBD9656044FCBAEF_CustomAttributesCacheGenerator_ExtensionMethods_CTDump_m6E14577C017276ACD5DA587C029A664694E859C9,
	ExtensionMethods_t3A63E01D154FF156B5910983BBD9656044FCBAEF_CustomAttributesCacheGenerator_ExtensionMethods_CTDump_mD3A9D8E4C2FAF2BE628602C89238233DBE170FDE,
	ExtensionMethods_t3A63E01D154FF156B5910983BBD9656044FCBAEF_CustomAttributesCacheGenerator_ExtensionMethods_CTDump_mB00AF342A1C001008DDEE5422E6CE9386ADF044C,
	ExtensionMethods_t3A63E01D154FF156B5910983BBD9656044FCBAEF_CustomAttributesCacheGenerator_ExtensionMethods_CTToString_m21B12643EEC617E394A1A8C5DB57D3DA63EE0A36,
	ExtensionMethods_t3A63E01D154FF156B5910983BBD9656044FCBAEF_CustomAttributesCacheGenerator_ExtensionMethods_CTDump_mED08E2C0BAC319C99DF07AA4752ACCB2645C2E72,
	ExtensionMethods_t3A63E01D154FF156B5910983BBD9656044FCBAEF_CustomAttributesCacheGenerator_ExtensionMethods_CTAddRange_mA8E3A743EA575918A5B29284CB5CEDD965D88261,
	ExtensionMethods_t3A63E01D154FF156B5910983BBD9656044FCBAEF_CustomAttributesCacheGenerator_ExtensionMethods_CTIsVisibleFrom_m5E3DED552FD8B132DD4E2B700E4BC17BCE83E587,
	Speaker_t639C193C2402AD73ED45F6530A8FD0EB62D3ACA1_CustomAttributesCacheGenerator_Speaker_get_areVoicesReady_m55042C89D6ABB754FF15AC1E880F6FAC8CE6C318,
	Speaker_t639C193C2402AD73ED45F6530A8FD0EB62D3ACA1_CustomAttributesCacheGenerator_Speaker_set_areVoicesReady_m2F79E5855BD2C1E6BC59F3A29C7972FAB5C49DE7,
	Speaker_t639C193C2402AD73ED45F6530A8FD0EB62D3ACA1_CustomAttributesCacheGenerator_Speaker_get_enforcedStandaloneTTS_m2B69DCFD8736769BE2716D304570F88D3F915D6B,
	Speaker_t639C193C2402AD73ED45F6530A8FD0EB62D3ACA1_CustomAttributesCacheGenerator_Speaker_set_enforcedStandaloneTTS_m8E0627900FA976E4F96CE064B22F6A19A1952DC6,
	VoiceProviderExample_tF1FB762ADFC1220BD86B88A758A6FA10381825EB_CustomAttributesCacheGenerator_VoiceProviderExample_Generate_mCEF1A347318B74AA723F6ABFA9C2EB48DC2DF9C4,
	VoiceProviderExample_tF1FB762ADFC1220BD86B88A758A6FA10381825EB_CustomAttributesCacheGenerator_VoiceProviderExample_Speak_mBDB765FB3CC330D5B5FC491DC795FD5297EDF0E7,
	VoiceProviderExample_tF1FB762ADFC1220BD86B88A758A6FA10381825EB_CustomAttributesCacheGenerator_VoiceProviderExample_SpeakNative_m9AE9125ACB3493351913B96173D7DBC83E2EAF13,
	U3CGenerateU3Ed__27_t733346A9DBD137DA2DD7EA3252EFFE76500D3C5B_CustomAttributesCacheGenerator_U3CGenerateU3Ed__27__ctor_mBE7648CA9BC983832FB40122AE1F309C028FAE80,
	U3CGenerateU3Ed__27_t733346A9DBD137DA2DD7EA3252EFFE76500D3C5B_CustomAttributesCacheGenerator_U3CGenerateU3Ed__27_System_IDisposable_Dispose_m31A13EDD00230227067357AEDF3081F4DFE30A20,
	U3CGenerateU3Ed__27_t733346A9DBD137DA2DD7EA3252EFFE76500D3C5B_CustomAttributesCacheGenerator_U3CGenerateU3Ed__27_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m5D2AC763594283B97AF22C0ECE832F4019B1DB85,
	U3CGenerateU3Ed__27_t733346A9DBD137DA2DD7EA3252EFFE76500D3C5B_CustomAttributesCacheGenerator_U3CGenerateU3Ed__27_System_Collections_IEnumerator_Reset_mBD3F2BBF7EB868747C7D15542F2E9089C61F50C1,
	U3CGenerateU3Ed__27_t733346A9DBD137DA2DD7EA3252EFFE76500D3C5B_CustomAttributesCacheGenerator_U3CGenerateU3Ed__27_System_Collections_IEnumerator_get_Current_m1BEE20547347457E10EF46105AA485378FA8EEE3,
	U3CSpeakU3Ed__28_t51C9498C945D57FD321E65C4CD6972BB1933A896_CustomAttributesCacheGenerator_U3CSpeakU3Ed__28__ctor_m00F559E690F57E2B4D1C6180D7BCF76110BBC6E1,
	U3CSpeakU3Ed__28_t51C9498C945D57FD321E65C4CD6972BB1933A896_CustomAttributesCacheGenerator_U3CSpeakU3Ed__28_System_IDisposable_Dispose_mC282884D02DBBFF1E195D8728EA2B8FF2D68351D,
	U3CSpeakU3Ed__28_t51C9498C945D57FD321E65C4CD6972BB1933A896_CustomAttributesCacheGenerator_U3CSpeakU3Ed__28_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m155241265B291EBEE2FD2381BC48B220AA3BA054,
	U3CSpeakU3Ed__28_t51C9498C945D57FD321E65C4CD6972BB1933A896_CustomAttributesCacheGenerator_U3CSpeakU3Ed__28_System_Collections_IEnumerator_Reset_mC4A4CC3360ACD12464DEE845E9CE608105356EF3,
	U3CSpeakU3Ed__28_t51C9498C945D57FD321E65C4CD6972BB1933A896_CustomAttributesCacheGenerator_U3CSpeakU3Ed__28_System_Collections_IEnumerator_get_Current_mA9B5A00AE897B39C9DEA60B9E4DA3135421125EC,
	U3CSpeakNativeU3Ed__29_tAEFCE1D6AFE6BEB16F03F830C079DAA23C35B481_CustomAttributesCacheGenerator_U3CSpeakNativeU3Ed__29__ctor_mAD9F48CC410BA068A8CBF1E72BAD7EAFFE682B0A,
	U3CSpeakNativeU3Ed__29_tAEFCE1D6AFE6BEB16F03F830C079DAA23C35B481_CustomAttributesCacheGenerator_U3CSpeakNativeU3Ed__29_System_IDisposable_Dispose_m309B55ACD321D7AC45AD632C1B4EACED83D47100,
	U3CSpeakNativeU3Ed__29_tAEFCE1D6AFE6BEB16F03F830C079DAA23C35B481_CustomAttributesCacheGenerator_U3CSpeakNativeU3Ed__29_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m7C5EDA7375CBEE70E45B133C63BED52FBD448E07,
	U3CSpeakNativeU3Ed__29_tAEFCE1D6AFE6BEB16F03F830C079DAA23C35B481_CustomAttributesCacheGenerator_U3CSpeakNativeU3Ed__29_System_Collections_IEnumerator_Reset_m5F62E754EA07230AF596C779B5484274CCBFB409,
	U3CSpeakNativeU3Ed__29_tAEFCE1D6AFE6BEB16F03F830C079DAA23C35B481_CustomAttributesCacheGenerator_U3CSpeakNativeU3Ed__29_System_Collections_IEnumerator_get_Current_m64E0A5D15216AFA6D8650F2B8453143B89539613,
	AudioFileGenerator_t25C4CCBDEAE5E63CB0745BACFAEF787E31EEA298_CustomAttributesCacheGenerator_AudioFileGenerator_generate_m4FEFFBF2EFCD1E9AE3710DA8C6DA480C781F70F9,
	U3CgenerateU3Ed__19_t158A067925EC863CE90E4DCCF85D7536E97CDBA0_CustomAttributesCacheGenerator_U3CgenerateU3Ed__19__ctor_m304D082A586B1520066B6F1F5D70922AF6AF4358,
	U3CgenerateU3Ed__19_t158A067925EC863CE90E4DCCF85D7536E97CDBA0_CustomAttributesCacheGenerator_U3CgenerateU3Ed__19_System_IDisposable_Dispose_m9637C84D3DE5BA34D03CD47F7254960ED4DFFCB6,
	U3CgenerateU3Ed__19_t158A067925EC863CE90E4DCCF85D7536E97CDBA0_CustomAttributesCacheGenerator_U3CgenerateU3Ed__19_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m3C28CECD7AE53E425198C381D87112731DEA0FB7,
	U3CgenerateU3Ed__19_t158A067925EC863CE90E4DCCF85D7536E97CDBA0_CustomAttributesCacheGenerator_U3CgenerateU3Ed__19_System_Collections_IEnumerator_Reset_m45465FCA07B5C00BE97AA285379D2880E212DD73,
	U3CgenerateU3Ed__19_t158A067925EC863CE90E4DCCF85D7536E97CDBA0_CustomAttributesCacheGenerator_U3CgenerateU3Ed__19_System_Collections_IEnumerator_get_Current_m1990C2C28E0F67CD67DE84F9E86ABAEE4EDECC3F,
	Paralanguage_t257B51534DB18301E85DF9EABF6265E9C7FC4F25_CustomAttributesCacheGenerator_Paralanguage_processStack_mA8A64AFE252A5308590B2241692C7A106025887D,
	U3CprocessStackU3Ed__42_tED87FEFE7D9355FCAD33EAAEFFDA223FAB4C41D2_CustomAttributesCacheGenerator_U3CprocessStackU3Ed__42__ctor_m21BC01AD74ECA7C64B458339F0B9DCC19A45ADE0,
	U3CprocessStackU3Ed__42_tED87FEFE7D9355FCAD33EAAEFFDA223FAB4C41D2_CustomAttributesCacheGenerator_U3CprocessStackU3Ed__42_System_IDisposable_Dispose_m7430826A64DB1BA83762995F321AA81FDD62220A,
	U3CprocessStackU3Ed__42_tED87FEFE7D9355FCAD33EAAEFFDA223FAB4C41D2_CustomAttributesCacheGenerator_U3CprocessStackU3Ed__42_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m6F766B569CFE9E96C325BEEC079BE142E366415D,
	U3CprocessStackU3Ed__42_tED87FEFE7D9355FCAD33EAAEFFDA223FAB4C41D2_CustomAttributesCacheGenerator_U3CprocessStackU3Ed__42_System_Collections_IEnumerator_Reset_m93CF8EC321B392A350E480F18AA6D728FEA88BE6,
	U3CprocessStackU3Ed__42_tED87FEFE7D9355FCAD33EAAEFFDA223FAB4C41D2_CustomAttributesCacheGenerator_U3CprocessStackU3Ed__42_System_Collections_IEnumerator_get_Current_m8EC92988F024B6CA75A7A577E3BAC83ACC67253A,
	Sequencer_t55938B3A6AE7BE5ECE446AC08D43795759152190_CustomAttributesCacheGenerator_Sequencer_playMe_m092FC62EA99ECB5C44C73C2B37A12683DF2227B2,
	U3CplayMeU3Ed__19_tDCB0B566C9E09125455A47D5F21A01BB9E163B27_CustomAttributesCacheGenerator_U3CplayMeU3Ed__19__ctor_mA6F984F9A7E5AD2F6BA681C9E7893F88E1C3FBAC,
	U3CplayMeU3Ed__19_tDCB0B566C9E09125455A47D5F21A01BB9E163B27_CustomAttributesCacheGenerator_U3CplayMeU3Ed__19_System_IDisposable_Dispose_mCC358E65250BA6032B90AA940898A050746C8DE5,
	U3CplayMeU3Ed__19_tDCB0B566C9E09125455A47D5F21A01BB9E163B27_CustomAttributesCacheGenerator_U3CplayMeU3Ed__19_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m26711DB4168A39C095F497E8F84AAF6FB867E0D6,
	U3CplayMeU3Ed__19_tDCB0B566C9E09125455A47D5F21A01BB9E163B27_CustomAttributesCacheGenerator_U3CplayMeU3Ed__19_System_Collections_IEnumerator_Reset_mBAA7A47CC030736934F37ED90C374CEC7A67E6FB,
	U3CplayMeU3Ed__19_tDCB0B566C9E09125455A47D5F21A01BB9E163B27_CustomAttributesCacheGenerator_U3CplayMeU3Ed__19_System_Collections_IEnumerator_get_Current_m4F1613CD9A49140520F720F374C25D5614AABD7F,
	VoiceInitalizer_t628535D1C01FC59180A5EE351F3502A45342556C_CustomAttributesCacheGenerator_VoiceInitalizer_initalizeVoices_m00AE8AD949E2048D303BF3DCB9AFC0BF91CC35AF,
	U3CinitalizeVoicesU3Ed__10_t2A275B1AA68149AB13303E07E9688291E5EF2C20_CustomAttributesCacheGenerator_U3CinitalizeVoicesU3Ed__10__ctor_m8362902403B1FE161F594248B8AE6732B925DCBD,
	U3CinitalizeVoicesU3Ed__10_t2A275B1AA68149AB13303E07E9688291E5EF2C20_CustomAttributesCacheGenerator_U3CinitalizeVoicesU3Ed__10_System_IDisposable_Dispose_mF897C063121C49E81290DF42E4FB044C856A921F,
	U3CinitalizeVoicesU3Ed__10_t2A275B1AA68149AB13303E07E9688291E5EF2C20_CustomAttributesCacheGenerator_U3CinitalizeVoicesU3Ed__10_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m19AB22AD386CF1E797DEF859EBA73F5980575238,
	U3CinitalizeVoicesU3Ed__10_t2A275B1AA68149AB13303E07E9688291E5EF2C20_CustomAttributesCacheGenerator_U3CinitalizeVoicesU3Ed__10_System_Collections_IEnumerator_Reset_mA2AC727EFF62D2404A9D9E0A44431EB15FA6E21D,
	U3CinitalizeVoicesU3Ed__10_t2A275B1AA68149AB13303E07E9688291E5EF2C20_CustomAttributesCacheGenerator_U3CinitalizeVoicesU3Ed__10_System_Collections_IEnumerator_get_Current_m6EE41A71936F6DC8E38DE9780509A78D505FC0B2,
	BaseCustomVoiceProvider_tA11734995C990E2A5451C062F8CBBE1EA94E2C97_CustomAttributesCacheGenerator_BaseCustomVoiceProvider_playAudioFile_mFF2435C8CA85BA9B44A9967159EE60E7F1E38EF3,
	U3CplayAudioFileU3Ed__81_t16FD3617907DBC006BF7EDBFDD79142630A5DC55_CustomAttributesCacheGenerator_U3CplayAudioFileU3Ed__81__ctor_m12F9909D1F5B37CBBDF5768383EE59A10995FCA5,
	U3CplayAudioFileU3Ed__81_t16FD3617907DBC006BF7EDBFDD79142630A5DC55_CustomAttributesCacheGenerator_U3CplayAudioFileU3Ed__81_System_IDisposable_Dispose_m8F70B0ED165D9F8F2C0F4ED496647D22AE55E010,
	U3CplayAudioFileU3Ed__81_t16FD3617907DBC006BF7EDBFDD79142630A5DC55_CustomAttributesCacheGenerator_U3CplayAudioFileU3Ed__81_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mBD68E65ACF9A83E963B49E130B6E07ADB5D4FA19,
	U3CplayAudioFileU3Ed__81_t16FD3617907DBC006BF7EDBFDD79142630A5DC55_CustomAttributesCacheGenerator_U3CplayAudioFileU3Ed__81_System_Collections_IEnumerator_Reset_mFA6ACB523E034EF1620981E29B012849F698D0C5,
	U3CplayAudioFileU3Ed__81_t16FD3617907DBC006BF7EDBFDD79142630A5DC55_CustomAttributesCacheGenerator_U3CplayAudioFileU3Ed__81_System_Collections_IEnumerator_get_Current_m6007D9DD5B76EFE943D370BBEC3A868B0447FDF6,
	BaseVoiceProvider_t0427110C1F97E96455F9DE9BF0D4BB9ABDA63EBA_CustomAttributesCacheGenerator_BaseVoiceProvider_playAudioFile_m4CCFF1BB44ED2ACE5F10AB4AAEE81D2B7CD24DD7,
	U3CplayAudioFileU3Ed__79_t5D80B48C5103969538788365776FBFD2481F2989_CustomAttributesCacheGenerator_U3CplayAudioFileU3Ed__79__ctor_m1918F5820D88E3F6361AF83F51F6389CA0FE8495,
	U3CplayAudioFileU3Ed__79_t5D80B48C5103969538788365776FBFD2481F2989_CustomAttributesCacheGenerator_U3CplayAudioFileU3Ed__79_System_IDisposable_Dispose_m016A01B81977C3EED37BE31F4601782D790E2577,
	U3CplayAudioFileU3Ed__79_t5D80B48C5103969538788365776FBFD2481F2989_CustomAttributesCacheGenerator_U3CplayAudioFileU3Ed__79_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m2F7ED2CAB4FD2CE5ACDEE216AA960E8F48A1D4ED,
	U3CplayAudioFileU3Ed__79_t5D80B48C5103969538788365776FBFD2481F2989_CustomAttributesCacheGenerator_U3CplayAudioFileU3Ed__79_System_Collections_IEnumerator_Reset_m7EAB980BAE0EC597EB0D8AF89DD2E220F7600552,
	U3CplayAudioFileU3Ed__79_t5D80B48C5103969538788365776FBFD2481F2989_CustomAttributesCacheGenerator_U3CplayAudioFileU3Ed__79_System_Collections_IEnumerator_get_Current_m081CAEDEE10983E393B7386FBD4D29ACC25D70B3,
	VoiceProviderIOS_tF5BF7BFF6D3A9FB9D597BBC28387E6B27E524A97_CustomAttributesCacheGenerator_VoiceProviderIOS_SpeakNative_m5A460E4C43E3F6B3F5C5B50B2FBA960E906087A1,
	VoiceProviderIOS_tF5BF7BFF6D3A9FB9D597BBC28387E6B27E524A97_CustomAttributesCacheGenerator_VoiceProviderIOS_Speak_m5D3CE8F57DAE33BE09E8A2F7AF87281DC47181D0,
	VoiceProviderIOS_tF5BF7BFF6D3A9FB9D597BBC28387E6B27E524A97_CustomAttributesCacheGenerator_VoiceProviderIOS_Generate_m5C4D7AC723D419BA1EFBD9411242CBE29C6D09BB,
	VoiceProviderIOS_tF5BF7BFF6D3A9FB9D597BBC28387E6B27E524A97_CustomAttributesCacheGenerator_VoiceProviderIOS_speak_mFB7D7BA16B879B9C8E69B83CDA91D109189EB96E,
	U3CSpeakNativeU3Ed__37_tB56F739052E648271E910C501E0E5DE0A2826C12_CustomAttributesCacheGenerator_U3CSpeakNativeU3Ed__37__ctor_m523A47BABBCEB6115AB8BC5602D2194F4C9DEF4F,
	U3CSpeakNativeU3Ed__37_tB56F739052E648271E910C501E0E5DE0A2826C12_CustomAttributesCacheGenerator_U3CSpeakNativeU3Ed__37_System_IDisposable_Dispose_m8516463D2831A9F5409582D34F16DC2697018BD8,
	U3CSpeakNativeU3Ed__37_tB56F739052E648271E910C501E0E5DE0A2826C12_CustomAttributesCacheGenerator_U3CSpeakNativeU3Ed__37_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mF2CE12F2B43A8A935DAA332E01EF3DF9D3659427,
	U3CSpeakNativeU3Ed__37_tB56F739052E648271E910C501E0E5DE0A2826C12_CustomAttributesCacheGenerator_U3CSpeakNativeU3Ed__37_System_Collections_IEnumerator_Reset_mDE6E3EAB0C3383024A07F9518EC95E2615BE50A2,
	U3CSpeakNativeU3Ed__37_tB56F739052E648271E910C501E0E5DE0A2826C12_CustomAttributesCacheGenerator_U3CSpeakNativeU3Ed__37_System_Collections_IEnumerator_get_Current_mF4BB2E19D59C10E2E19E1A071D8B963C2318D72B,
	U3CSpeakU3Ed__38_t0CAF7D4D1537BFF140CFFE747E35BD71F0C3FD33_CustomAttributesCacheGenerator_U3CSpeakU3Ed__38__ctor_m17AA83B8F4292BE9A434FAD451B6042F85B1CA7A,
	U3CSpeakU3Ed__38_t0CAF7D4D1537BFF140CFFE747E35BD71F0C3FD33_CustomAttributesCacheGenerator_U3CSpeakU3Ed__38_System_IDisposable_Dispose_m1D62F9167AD94A284EB61F82C0A4F55B3CD87CFD,
	U3CSpeakU3Ed__38_t0CAF7D4D1537BFF140CFFE747E35BD71F0C3FD33_CustomAttributesCacheGenerator_U3CSpeakU3Ed__38_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m55A294AB03578EC259B3F90FF30ED7D6E9B850BB,
	U3CSpeakU3Ed__38_t0CAF7D4D1537BFF140CFFE747E35BD71F0C3FD33_CustomAttributesCacheGenerator_U3CSpeakU3Ed__38_System_Collections_IEnumerator_Reset_mB2BC205AFCE9DA70AAF9757596AB8EE0E2085E41,
	U3CSpeakU3Ed__38_t0CAF7D4D1537BFF140CFFE747E35BD71F0C3FD33_CustomAttributesCacheGenerator_U3CSpeakU3Ed__38_System_Collections_IEnumerator_get_Current_m3D7E6B2FE72C3643435F91F8C65FDBD74A4E2C6B,
	U3CGenerateU3Ed__39_t03292738BAB852A4B9E700EEBCEA2F4808456175_CustomAttributesCacheGenerator_U3CGenerateU3Ed__39__ctor_mD9733152DA5C477BC3EBDE9374773C3614007179,
	U3CGenerateU3Ed__39_t03292738BAB852A4B9E700EEBCEA2F4808456175_CustomAttributesCacheGenerator_U3CGenerateU3Ed__39_System_IDisposable_Dispose_m77EA5EA8743977C2BB472BD2FF79D131B2A25D38,
	U3CGenerateU3Ed__39_t03292738BAB852A4B9E700EEBCEA2F4808456175_CustomAttributesCacheGenerator_U3CGenerateU3Ed__39_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m55C35151E2EBDA7C7287CC1DB8AC808094F27B2A,
	U3CGenerateU3Ed__39_t03292738BAB852A4B9E700EEBCEA2F4808456175_CustomAttributesCacheGenerator_U3CGenerateU3Ed__39_System_Collections_IEnumerator_Reset_m4D4A7F1C40FB78E0D45F689E2638CF4203F0BB8B,
	U3CGenerateU3Ed__39_t03292738BAB852A4B9E700EEBCEA2F4808456175_CustomAttributesCacheGenerator_U3CGenerateU3Ed__39_System_Collections_IEnumerator_get_Current_mECE12334B8337427611893D644D1735115B00175,
	U3CspeakU3Ed__41_t6D7ABE0037F6C095D53C8134C299473D203F6A54_CustomAttributesCacheGenerator_U3CspeakU3Ed__41__ctor_m276AE39E567257B32C7896EA8EDDA24EC9D824ED,
	U3CspeakU3Ed__41_t6D7ABE0037F6C095D53C8134C299473D203F6A54_CustomAttributesCacheGenerator_U3CspeakU3Ed__41_System_IDisposable_Dispose_m0E4873CEED2CE27F21F5EB92F97879BFB53783C2,
	U3CspeakU3Ed__41_t6D7ABE0037F6C095D53C8134C299473D203F6A54_CustomAttributesCacheGenerator_U3CspeakU3Ed__41_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mEEA1EC15804398BACB904C1707E5281F50328F18,
	U3CspeakU3Ed__41_t6D7ABE0037F6C095D53C8134C299473D203F6A54_CustomAttributesCacheGenerator_U3CspeakU3Ed__41_System_Collections_IEnumerator_Reset_m93EA86B1BE247D3CE920516B739BB3E25B25C55F,
	U3CspeakU3Ed__41_t6D7ABE0037F6C095D53C8134C299473D203F6A54_CustomAttributesCacheGenerator_U3CspeakU3Ed__41_System_Collections_IEnumerator_get_Current_m95840D9DA7B2AD2A2C84637718BD96658116F22F,
	VoiceProviderMary_t4D4BF6869E86B5E6E86F4C105F3369AF9D343573_CustomAttributesCacheGenerator_VoiceProviderMary_SpeakNative_m331610A2ED4ACEA5F1B1E8285E531D9A812A09C2,
	VoiceProviderMary_t4D4BF6869E86B5E6E86F4C105F3369AF9D343573_CustomAttributesCacheGenerator_VoiceProviderMary_Speak_m891800543E668A5334BD528A4144C8785D80DAE7,
	VoiceProviderMary_t4D4BF6869E86B5E6E86F4C105F3369AF9D343573_CustomAttributesCacheGenerator_VoiceProviderMary_Generate_m5E6DF2FF9DB49139BB98D02BB3B8703159E50EB3,
	VoiceProviderMary_t4D4BF6869E86B5E6E86F4C105F3369AF9D343573_CustomAttributesCacheGenerator_VoiceProviderMary_speak_mEDD8B66B63C43FD85CAD280D309C4276729D3FD6,
	VoiceProviderMary_t4D4BF6869E86B5E6E86F4C105F3369AF9D343573_CustomAttributesCacheGenerator_VoiceProviderMary_getVoices_mA44E670C9E75CC20259D5BFBC4884064E4BB1DCC,
	U3CSpeakNativeU3Ed__29_tAE002A72C2A3F020A17BF4A56FEBBC5EA2787303_CustomAttributesCacheGenerator_U3CSpeakNativeU3Ed__29__ctor_m7470A0504C3BABDC965367131EA97B330ECAA0B3,
	U3CSpeakNativeU3Ed__29_tAE002A72C2A3F020A17BF4A56FEBBC5EA2787303_CustomAttributesCacheGenerator_U3CSpeakNativeU3Ed__29_System_IDisposable_Dispose_m02B9BF12673B6C720138AE5C87DCA36928F31A6B,
	U3CSpeakNativeU3Ed__29_tAE002A72C2A3F020A17BF4A56FEBBC5EA2787303_CustomAttributesCacheGenerator_U3CSpeakNativeU3Ed__29_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mB9405A8A86C3770CC1F111D0C4000A7A9B7D7A67,
	U3CSpeakNativeU3Ed__29_tAE002A72C2A3F020A17BF4A56FEBBC5EA2787303_CustomAttributesCacheGenerator_U3CSpeakNativeU3Ed__29_System_Collections_IEnumerator_Reset_mBC4615F0056EF0BC799A52C6A4149F468953E3B1,
	U3CSpeakNativeU3Ed__29_tAE002A72C2A3F020A17BF4A56FEBBC5EA2787303_CustomAttributesCacheGenerator_U3CSpeakNativeU3Ed__29_System_Collections_IEnumerator_get_Current_mFED8E5538033280FD348A7C8FFB81D8D26DC06A2,
	U3CSpeakU3Ed__30_tC962B657041FD44AF5A88FD52604438A78BA1649_CustomAttributesCacheGenerator_U3CSpeakU3Ed__30__ctor_m9587AD139AB417176D703120DF98958092A13743,
	U3CSpeakU3Ed__30_tC962B657041FD44AF5A88FD52604438A78BA1649_CustomAttributesCacheGenerator_U3CSpeakU3Ed__30_System_IDisposable_Dispose_m23CE40AF61F6D8649073C36D77C590953E895803,
	U3CSpeakU3Ed__30_tC962B657041FD44AF5A88FD52604438A78BA1649_CustomAttributesCacheGenerator_U3CSpeakU3Ed__30_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m4BE9CD55DFC56B895DDBBEDFFFF9D846C2F24096,
	U3CSpeakU3Ed__30_tC962B657041FD44AF5A88FD52604438A78BA1649_CustomAttributesCacheGenerator_U3CSpeakU3Ed__30_System_Collections_IEnumerator_Reset_mDDC9BFD5B6F9F4992FF961A1916F6CFFF370032B,
	U3CSpeakU3Ed__30_tC962B657041FD44AF5A88FD52604438A78BA1649_CustomAttributesCacheGenerator_U3CSpeakU3Ed__30_System_Collections_IEnumerator_get_Current_m80972AF0FFE79545A22C01A56202CEA16644B859,
	U3CGenerateU3Ed__31_tFA662A35AA694F207DCA5B6C83CA1964EBA15A73_CustomAttributesCacheGenerator_U3CGenerateU3Ed__31__ctor_m267A2910841978F5CDB6CCB831A10313B0798DEC,
	U3CGenerateU3Ed__31_tFA662A35AA694F207DCA5B6C83CA1964EBA15A73_CustomAttributesCacheGenerator_U3CGenerateU3Ed__31_System_IDisposable_Dispose_m22DC6783EC7327F0EFAFDF726143D978509C7C62,
	U3CGenerateU3Ed__31_tFA662A35AA694F207DCA5B6C83CA1964EBA15A73_CustomAttributesCacheGenerator_U3CGenerateU3Ed__31_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m6BA121BFF88A61F810A85519EA5166243AA8F2F7,
	U3CGenerateU3Ed__31_tFA662A35AA694F207DCA5B6C83CA1964EBA15A73_CustomAttributesCacheGenerator_U3CGenerateU3Ed__31_System_Collections_IEnumerator_Reset_mA8522467F71712CDE975F96D86B51B3F6C04B1CC,
	U3CGenerateU3Ed__31_tFA662A35AA694F207DCA5B6C83CA1964EBA15A73_CustomAttributesCacheGenerator_U3CGenerateU3Ed__31_System_Collections_IEnumerator_get_Current_m9EC33BA38BF83D3CA8FF84D807491EEBDC56CBD2,
	U3CspeakU3Ed__32_tDEC1697FFF0729D0B77A9482BE143E6DC4C893CD_CustomAttributesCacheGenerator_U3CspeakU3Ed__32__ctor_m39F1A6EF5BF1ECD0725D91A20A03439E3AD4E103,
	U3CspeakU3Ed__32_tDEC1697FFF0729D0B77A9482BE143E6DC4C893CD_CustomAttributesCacheGenerator_U3CspeakU3Ed__32_System_IDisposable_Dispose_m47A016B1C9DA6E21E7501088414B7AE4026C210E,
	U3CspeakU3Ed__32_tDEC1697FFF0729D0B77A9482BE143E6DC4C893CD_CustomAttributesCacheGenerator_U3CspeakU3Ed__32_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m8CFA25DFB6CD460D6C3AA1108C416152A15230F5,
	U3CspeakU3Ed__32_tDEC1697FFF0729D0B77A9482BE143E6DC4C893CD_CustomAttributesCacheGenerator_U3CspeakU3Ed__32_System_Collections_IEnumerator_Reset_m73F40ECB8D160F5A0111DCDC777F3BFB1CEEC6F3,
	U3CspeakU3Ed__32_tDEC1697FFF0729D0B77A9482BE143E6DC4C893CD_CustomAttributesCacheGenerator_U3CspeakU3Ed__32_System_Collections_IEnumerator_get_Current_m2D2650AC8FF888202F794982EB33EE9B57F98367,
	U3CgetVoicesU3Ed__33_t42277CC46AC28C4C054B2A9350319771F5FBA45D_CustomAttributesCacheGenerator_U3CgetVoicesU3Ed__33__ctor_m95F6F409F82D4F4973F63E456C242F66A76B3205,
	U3CgetVoicesU3Ed__33_t42277CC46AC28C4C054B2A9350319771F5FBA45D_CustomAttributesCacheGenerator_U3CgetVoicesU3Ed__33_System_IDisposable_Dispose_mF7D198C1948C01A6A4D9D46B84AB60DE30DF3C30,
	U3CgetVoicesU3Ed__33_t42277CC46AC28C4C054B2A9350319771F5FBA45D_CustomAttributesCacheGenerator_U3CgetVoicesU3Ed__33_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m2C5797A9C9F2E33DE06235C398E7B36C6EA1197B,
	U3CgetVoicesU3Ed__33_t42277CC46AC28C4C054B2A9350319771F5FBA45D_CustomAttributesCacheGenerator_U3CgetVoicesU3Ed__33_System_Collections_IEnumerator_Reset_m995AFECA0C5D06884A5DA8C78BF92AF75EAB19E1,
	U3CgetVoicesU3Ed__33_t42277CC46AC28C4C054B2A9350319771F5FBA45D_CustomAttributesCacheGenerator_U3CgetVoicesU3Ed__33_System_Collections_IEnumerator_get_Current_mA53C128274D93B9FE40D699DD1192FC1A9C5B829,
	Dialog_t638F1505D57D2F294DDEA45174D9130410067CD7_CustomAttributesCacheGenerator_Dialog_DialogSequence_m7D77C870918DB2F259C90731C7EE7E6E4F788B9D,
	U3CDialogSequenceU3Ed__25_t44374F6C645D70CD0D9FC61B194C18AF79DBA159_CustomAttributesCacheGenerator_U3CDialogSequenceU3Ed__25__ctor_m0B8FC59C5611D1416FB254E739F74FAC0B7241BF,
	U3CDialogSequenceU3Ed__25_t44374F6C645D70CD0D9FC61B194C18AF79DBA159_CustomAttributesCacheGenerator_U3CDialogSequenceU3Ed__25_System_IDisposable_Dispose_m43CAB6AFDA5A407257DCE2BB40AF5562F71833EA,
	U3CDialogSequenceU3Ed__25_t44374F6C645D70CD0D9FC61B194C18AF79DBA159_CustomAttributesCacheGenerator_U3CDialogSequenceU3Ed__25_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mE00F000EBAFBDDFF5D2C8F42E742D77F3DD80A97,
	U3CDialogSequenceU3Ed__25_t44374F6C645D70CD0D9FC61B194C18AF79DBA159_CustomAttributesCacheGenerator_U3CDialogSequenceU3Ed__25_System_Collections_IEnumerator_Reset_mD9737A7BD110755EF4E00DA6CB49C2B29F9D3B42,
	U3CDialogSequenceU3Ed__25_t44374F6C645D70CD0D9FC61B194C18AF79DBA159_CustomAttributesCacheGenerator_U3CDialogSequenceU3Ed__25_System_Collections_IEnumerator_get_Current_m1EF267AEC7A80B67C2C484CABB5254A8CA16068A,
	SendMessage_t9D6A7D158B11451BF3D7DCC357330A2F4363BB94_CustomAttributesCacheGenerator_SendMessage_SpeakerB_m6C538E9CD2CDABCC1F85DC7514AB9ABAC7523F90,
	U3CSpeakerBU3Ed__8_t4FDA50AFC81C4F929F34CFAB06B4B0B6AF635A06_CustomAttributesCacheGenerator_U3CSpeakerBU3Ed__8__ctor_m4A8ED00A523A95298E776A4A523FF2A04F3D6724,
	U3CSpeakerBU3Ed__8_t4FDA50AFC81C4F929F34CFAB06B4B0B6AF635A06_CustomAttributesCacheGenerator_U3CSpeakerBU3Ed__8_System_IDisposable_Dispose_mF7B8070E854813D83A83738E3A5AFF4A85A58C2F,
	U3CSpeakerBU3Ed__8_t4FDA50AFC81C4F929F34CFAB06B4B0B6AF635A06_CustomAttributesCacheGenerator_U3CSpeakerBU3Ed__8_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m410D072C0C20CC5489B8A8A4782DF66577B670DD,
	U3CSpeakerBU3Ed__8_t4FDA50AFC81C4F929F34CFAB06B4B0B6AF635A06_CustomAttributesCacheGenerator_U3CSpeakerBU3Ed__8_System_Collections_IEnumerator_Reset_m51FC493E4D2F9C532B5C80D086A39099F32302ED,
	U3CSpeakerBU3Ed__8_t4FDA50AFC81C4F929F34CFAB06B4B0B6AF635A06_CustomAttributesCacheGenerator_U3CSpeakerBU3Ed__8_System_Collections_IEnumerator_get_Current_mE1A5CEC7B178D65FF55AC03932D40A96DA8697A6,
	UIHint_t5B5786B738951189B871A6FF5D23A473222E03C7_CustomAttributesCacheGenerator_UIHint_lerpAlphaDown_mC074A670DEDC2E2C823C0F84A4EBC6D960BD5D42,
	UIHint_t5B5786B738951189B871A6FF5D23A473222E03C7_CustomAttributesCacheGenerator_UIHint_lerpAlphaUp_m84B48E04DD5ABE68C1A95A77A51EA2B8E9C61BBD,
	U3ClerpAlphaDownU3Ed__8_tFA588D110070DFAE10E2AAC390DE45F2C6DCE063_CustomAttributesCacheGenerator_U3ClerpAlphaDownU3Ed__8__ctor_m137125C8737CD01C1A17E318878AFB2B0D0CDB14,
	U3ClerpAlphaDownU3Ed__8_tFA588D110070DFAE10E2AAC390DE45F2C6DCE063_CustomAttributesCacheGenerator_U3ClerpAlphaDownU3Ed__8_System_IDisposable_Dispose_m82EA5F02C54838CDFAD7E3C549962D0DB1151FE1,
	U3ClerpAlphaDownU3Ed__8_tFA588D110070DFAE10E2AAC390DE45F2C6DCE063_CustomAttributesCacheGenerator_U3ClerpAlphaDownU3Ed__8_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mB42DD7F0CDC10AADCD99116A14BF77D16197AE49,
	U3ClerpAlphaDownU3Ed__8_tFA588D110070DFAE10E2AAC390DE45F2C6DCE063_CustomAttributesCacheGenerator_U3ClerpAlphaDownU3Ed__8_System_Collections_IEnumerator_Reset_mB4243BFE9BA869109D22485266636F346DE3495F,
	U3ClerpAlphaDownU3Ed__8_tFA588D110070DFAE10E2AAC390DE45F2C6DCE063_CustomAttributesCacheGenerator_U3ClerpAlphaDownU3Ed__8_System_Collections_IEnumerator_get_Current_m641305F2EB99B80E211B4E6E99E859C2B7AAE63B,
	U3ClerpAlphaUpU3Ed__9_tA4B9A2133A395822BDCF87133798169872E8B6B1_CustomAttributesCacheGenerator_U3ClerpAlphaUpU3Ed__9__ctor_m41FA43BCB2D14D39F0DC1E1AFFCECB0D18E02419,
	U3ClerpAlphaUpU3Ed__9_tA4B9A2133A395822BDCF87133798169872E8B6B1_CustomAttributesCacheGenerator_U3ClerpAlphaUpU3Ed__9_System_IDisposable_Dispose_mD5454F31F93F6B5DCCB420AB1241DD2E2DEE331B,
	U3ClerpAlphaUpU3Ed__9_tA4B9A2133A395822BDCF87133798169872E8B6B1_CustomAttributesCacheGenerator_U3ClerpAlphaUpU3Ed__9_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m07EF2B75C53B187A10E0ACC00AB20DF4CAB27CD7,
	U3ClerpAlphaUpU3Ed__9_tA4B9A2133A395822BDCF87133798169872E8B6B1_CustomAttributesCacheGenerator_U3ClerpAlphaUpU3Ed__9_System_Collections_IEnumerator_Reset_m1C29B48A4BFBB4956CB59DAF52A587455998D965,
	U3ClerpAlphaUpU3Ed__9_tA4B9A2133A395822BDCF87133798169872E8B6B1_CustomAttributesCacheGenerator_U3ClerpAlphaUpU3Ed__9_System_Collections_IEnumerator_get_Current_mA92CCF9323E6BC3E4B224B1CBF4098D02978B980,
	CTProcess_t03C134618294F58C911F9F8CB956CD787127B631_CustomAttributesCacheGenerator_CTProcess_get_Handle_mC3608F2A6A30685484B9144B20D12BD0399D4E32,
	CTProcess_t03C134618294F58C911F9F8CB956CD787127B631_CustomAttributesCacheGenerator_CTProcess_set_Handle_m5C53B2AE2BC840DE6F1435598135115AB14DAA14,
	CTProcess_t03C134618294F58C911F9F8CB956CD787127B631_CustomAttributesCacheGenerator_CTProcess_get_Id_mD17750B0020A2B49BBC1B864295B2F34930A7DCB,
	CTProcess_t03C134618294F58C911F9F8CB956CD787127B631_CustomAttributesCacheGenerator_CTProcess_set_Id_mD77F7079C3005B95ED3BC30F796BAEC98BF5E26B,
	CTProcess_t03C134618294F58C911F9F8CB956CD787127B631_CustomAttributesCacheGenerator_CTProcess_get_HasExited_m2D586146FF76514478807E12CEAA555D9C1C0D51,
	CTProcess_t03C134618294F58C911F9F8CB956CD787127B631_CustomAttributesCacheGenerator_CTProcess_set_HasExited_mD528F17ECC59BED4513599C205B57A875532DCCB,
	CTProcess_t03C134618294F58C911F9F8CB956CD787127B631_CustomAttributesCacheGenerator_CTProcess_get_StartTime_mAF23EA137BD076A31606E0175BCBED9241F2C40F,
	CTProcess_t03C134618294F58C911F9F8CB956CD787127B631_CustomAttributesCacheGenerator_CTProcess_set_StartTime_mF73C4A51A77CE85B55ECF03A6AC3F5FE43654188,
	CTProcess_t03C134618294F58C911F9F8CB956CD787127B631_CustomAttributesCacheGenerator_CTProcess_get_ExitTime_m06BAEDCC06277C766A407B9E331538C7975F18CD,
	CTProcess_t03C134618294F58C911F9F8CB956CD787127B631_CustomAttributesCacheGenerator_CTProcess_set_ExitTime_m762BB485BBE10AC9366FFBD0A3460F5242F5571C,
	CTProcess_t03C134618294F58C911F9F8CB956CD787127B631_CustomAttributesCacheGenerator_CTProcess_get_StandardOutput_m5E2E8567D9990F291CCE025867CBCF3B03375377,
	CTProcess_t03C134618294F58C911F9F8CB956CD787127B631_CustomAttributesCacheGenerator_CTProcess_set_StandardOutput_mBB889EA2A78ADF52AE97A319985AEBF65EFB4001,
	CTProcess_t03C134618294F58C911F9F8CB956CD787127B631_CustomAttributesCacheGenerator_CTProcess_get_StandardError_m1FA605D25447DFAB49640AF6C892C09436776B9F,
	CTProcess_t03C134618294F58C911F9F8CB956CD787127B631_CustomAttributesCacheGenerator_CTProcess_set_StandardError_mD6993571CA67FFEC04C2D036EB9240115EAB8802,
	CTProcess_t03C134618294F58C911F9F8CB956CD787127B631_CustomAttributesCacheGenerator_CTProcess_get_isBusy_m4F3A086625E52181F3EA327E3598675F0C3A47F5,
	CTProcess_t03C134618294F58C911F9F8CB956CD787127B631_CustomAttributesCacheGenerator_CTProcess_set_isBusy_mAC83ACD3B1A2AF71151EFF8F7C1E57DA8D529557,
	CTProcess_t03C134618294F58C911F9F8CB956CD787127B631_CustomAttributesCacheGenerator_CTProcess_U3CBeginOutputReadLineU3Eb__60_0_mF7FDEECEFD6B1DE5646F5204C7B69802154B4AF3,
	CTProcess_t03C134618294F58C911F9F8CB956CD787127B631_CustomAttributesCacheGenerator_CTProcess_U3CBeginErrorReadLineU3Eb__61_0_m19AABDD682A9810182567AA68BFDF32AB957445C,
	CTProcessStartInfo_t18282D67663A337B9F05446EAFAB980BEEA87025_CustomAttributesCacheGenerator_CTProcessStartInfo_get_UseThread_m34262C2CD9CB925BCFDB438D610DDE432F1AC6B1,
	CTProcessStartInfo_t18282D67663A337B9F05446EAFAB980BEEA87025_CustomAttributesCacheGenerator_CTProcessStartInfo_set_UseThread_m86AA2DA43064F088C2F979EA3B9369C4C030A32B,
	CTProcessStartInfo_t18282D67663A337B9F05446EAFAB980BEEA87025_CustomAttributesCacheGenerator_CTProcessStartInfo_get_UseCmdExecute_mB67F1E3D55EBB016C86D75C4F09356D04D02333E,
	CTProcessStartInfo_t18282D67663A337B9F05446EAFAB980BEEA87025_CustomAttributesCacheGenerator_CTProcessStartInfo_set_UseCmdExecute_mB553AB506E410A0294A873F6592EF753B26FBB85,
	CTProcessStartInfo_t18282D67663A337B9F05446EAFAB980BEEA87025_CustomAttributesCacheGenerator_CTProcessStartInfo_get_FileName_m1763CF197CE0D3528D744A53B2DAE98F890279F9,
	CTProcessStartInfo_t18282D67663A337B9F05446EAFAB980BEEA87025_CustomAttributesCacheGenerator_CTProcessStartInfo_set_FileName_mFF77C984D27BD97D9D6724F4154E65439EADAC0F,
	CTProcessStartInfo_t18282D67663A337B9F05446EAFAB980BEEA87025_CustomAttributesCacheGenerator_CTProcessStartInfo_get_Arguments_m3A347FA469BB43787D2877BB70514D039FC9D7C8,
	CTProcessStartInfo_t18282D67663A337B9F05446EAFAB980BEEA87025_CustomAttributesCacheGenerator_CTProcessStartInfo_set_Arguments_mA9CB0587438FF858CBF67F0E8A51A987962A5FE8,
	CTProcessStartInfo_t18282D67663A337B9F05446EAFAB980BEEA87025_CustomAttributesCacheGenerator_CTProcessStartInfo_get_CreateNoWindow_mF07A129AB1002CC6669265A2B72A2819386D21E4,
	CTProcessStartInfo_t18282D67663A337B9F05446EAFAB980BEEA87025_CustomAttributesCacheGenerator_CTProcessStartInfo_set_CreateNoWindow_m27DD6357C383BC237A64790632439866995E5EA9,
	CTProcessStartInfo_t18282D67663A337B9F05446EAFAB980BEEA87025_CustomAttributesCacheGenerator_CTProcessStartInfo_get_WorkingDirectory_mD97C799110A52EE36167FB77EA6F695DE0C03D26,
	CTProcessStartInfo_t18282D67663A337B9F05446EAFAB980BEEA87025_CustomAttributesCacheGenerator_CTProcessStartInfo_set_WorkingDirectory_mBA1BEE7819F6D96B8E8A61BEC985D1627A1E77D9,
	CTProcessStartInfo_t18282D67663A337B9F05446EAFAB980BEEA87025_CustomAttributesCacheGenerator_CTProcessStartInfo_get_RedirectStandardOutput_m2421774316C53C2DBFD8749A7F52D5A2A5C658A9,
	CTProcessStartInfo_t18282D67663A337B9F05446EAFAB980BEEA87025_CustomAttributesCacheGenerator_CTProcessStartInfo_set_RedirectStandardOutput_mEB9777CA86502455F7C8AC29644AB604CF18CC84,
	CTProcessStartInfo_t18282D67663A337B9F05446EAFAB980BEEA87025_CustomAttributesCacheGenerator_CTProcessStartInfo_get_RedirectStandardError_mFA019C90FA0BFCC650C8A92E1EFD6BD1EDB83E96,
	CTProcessStartInfo_t18282D67663A337B9F05446EAFAB980BEEA87025_CustomAttributesCacheGenerator_CTProcessStartInfo_set_RedirectStandardError_m322C697C5104C0A4F0BD1E5B721B800D7B896B17,
	CTProcessStartInfo_t18282D67663A337B9F05446EAFAB980BEEA87025_CustomAttributesCacheGenerator_CTProcessStartInfo_get_StandardOutputEncoding_mF883E4B6A58BF6F6189EC53A1B36CED7051CA740,
	CTProcessStartInfo_t18282D67663A337B9F05446EAFAB980BEEA87025_CustomAttributesCacheGenerator_CTProcessStartInfo_set_StandardOutputEncoding_mE7BA0B4AD89C1B8476037466A7149165AA304DBD,
	CTProcessStartInfo_t18282D67663A337B9F05446EAFAB980BEEA87025_CustomAttributesCacheGenerator_CTProcessStartInfo_get_StandardErrorEncoding_m8A4F0A1E361D9A522ED82A49C6F7BE05C09801ED,
	CTProcessStartInfo_t18282D67663A337B9F05446EAFAB980BEEA87025_CustomAttributesCacheGenerator_CTProcessStartInfo_set_StandardErrorEncoding_m3C59321FB26FE02DFBC8307F22D060FE9ADC2628,
	CTProcessStartInfo_t18282D67663A337B9F05446EAFAB980BEEA87025_CustomAttributesCacheGenerator_CTProcessStartInfo_get_UseShellExecute_m96AC26978B39FBA130D519C81194A5B6B101792B,
	CTProcessStartInfo_t18282D67663A337B9F05446EAFAB980BEEA87025_CustomAttributesCacheGenerator_CTProcessStartInfo_set_UseShellExecute_m2C838389B42C35BBB7CEA1BC337285F1771849E6,
	CTWebClient_t32C9C802CE03700DC8C37D1952056B588BE7D709_CustomAttributesCacheGenerator_CTWebClient_get_Timeout_m05E7AC7AD82CFD0A255A1D753B3AB413E0C15C9E,
	CTWebClient_t32C9C802CE03700DC8C37D1952056B588BE7D709_CustomAttributesCacheGenerator_CTWebClient_set_Timeout_mC93FB85FA7A3F30D35CE10A64A25ABD18D206079,
	CTWebClient_t32C9C802CE03700DC8C37D1952056B588BE7D709_CustomAttributesCacheGenerator_CTWebClient_get_ConnectionLimit_m74A0F4DC526B36E55EA0857E3F953453622189CE,
	CTWebClient_t32C9C802CE03700DC8C37D1952056B588BE7D709_CustomAttributesCacheGenerator_CTWebClient_set_ConnectionLimit_mFBDBF8F1C18BC022FC5B773E3874E8B9569F9068,
	Example01Synthesis_t1DD770E55DA0B4B377769951B07466AEA8271C6F_CustomAttributesCacheGenerator_Example01Synthesis_Start_m08B4479794B479D161F1950CBACF9031DD93A38B,
	Example01Synthesis_t1DD770E55DA0B4B377769951B07466AEA8271C6F_CustomAttributesCacheGenerator_Example01Synthesis_GetVoices_m5222C0293EAA6FB209232F0A73D75DA1F22C5937,
	Example01Synthesis_t1DD770E55DA0B4B377769951B07466AEA8271C6F_CustomAttributesCacheGenerator_Example01Synthesis_SetPitch_mC504A0CAE1FDDA39B5B272BDB63B5B4C545017E7,
	Example01Synthesis_t1DD770E55DA0B4B377769951B07466AEA8271C6F_CustomAttributesCacheGenerator_Example01Synthesis_SetRate_mE3D46DECE1A051928BDAAC3C9635D6C7986DCC3E,
	Example01Synthesis_t1DD770E55DA0B4B377769951B07466AEA8271C6F_CustomAttributesCacheGenerator_Example01Synthesis_U3CStartU3Eb__22_0_m949EFCBA0A18AB6E4A3C65A79A54476776887C4A,
	Example01Synthesis_t1DD770E55DA0B4B377769951B07466AEA8271C6F_CustomAttributesCacheGenerator_Example01Synthesis_U3CStartU3Eb__22_1_mEC15BB057506B9CE63CECC952906E4682748D97E,
	Example01Synthesis_t1DD770E55DA0B4B377769951B07466AEA8271C6F_CustomAttributesCacheGenerator_Example01Synthesis_U3CStartU3Eb__22_2_mC83E3F257936B1EB16E78EA0219E028DD56CBF80,
	Example01Synthesis_t1DD770E55DA0B4B377769951B07466AEA8271C6F_CustomAttributesCacheGenerator_Example01Synthesis_U3CStartU3Eb__22_3_m1255DF35B820D4D4A18882F8EC138AB4D85A2671,
	Example01Synthesis_t1DD770E55DA0B4B377769951B07466AEA8271C6F_CustomAttributesCacheGenerator_Example01Synthesis_U3CStartU3Eb__22_4_m02C7CF99D3995A5F4C83ED560B26073641866E70,
	Example01Synthesis_t1DD770E55DA0B4B377769951B07466AEA8271C6F_CustomAttributesCacheGenerator_Example01Synthesis_U3CGetVoicesU3Eb__23_0_m1B6211E861319A5905671E0865865224B7A85CF2,
	Example01Synthesis_t1DD770E55DA0B4B377769951B07466AEA8271C6F_CustomAttributesCacheGenerator_Example01Synthesis_U3CSetIfReadyForDefaultVoiceU3Eb__24_0_mD6A9EC56F9C352204D6D74AA6C1E21F12E536013,
	U3CStartU3Ed__22_t3F06EB55E379386889BEE897F565664726CFB9B9_CustomAttributesCacheGenerator_U3CStartU3Ed__22__ctor_m8F63FDE1DD445BADF80206E5F3E93DDD0680FE84,
	U3CStartU3Ed__22_t3F06EB55E379386889BEE897F565664726CFB9B9_CustomAttributesCacheGenerator_U3CStartU3Ed__22_System_IDisposable_Dispose_mCEAC0024B672F6415833CDE42B9193B11409A425,
	U3CStartU3Ed__22_t3F06EB55E379386889BEE897F565664726CFB9B9_CustomAttributesCacheGenerator_U3CStartU3Ed__22_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m3AD046D2F672A996C8C2414FFFF51CFEE0470407,
	U3CStartU3Ed__22_t3F06EB55E379386889BEE897F565664726CFB9B9_CustomAttributesCacheGenerator_U3CStartU3Ed__22_System_Collections_IEnumerator_Reset_m85E9BF96346CE3D32AEDC2DD1A49612D23AC3EA4,
	U3CStartU3Ed__22_t3F06EB55E379386889BEE897F565664726CFB9B9_CustomAttributesCacheGenerator_U3CStartU3Ed__22_System_Collections_IEnumerator_get_Current_mA1B1C2B5B63C73CB6AC75FA7BB6360950BBDA5B0,
	U3CGetVoicesU3Ed__23_t4C74B1E95210BDA509B6AB73C0CE5286B905B3A1_CustomAttributesCacheGenerator_U3CGetVoicesU3Ed__23__ctor_m09932D130C7602C2D2ABE2EDC806CC7F93359C34,
	U3CGetVoicesU3Ed__23_t4C74B1E95210BDA509B6AB73C0CE5286B905B3A1_CustomAttributesCacheGenerator_U3CGetVoicesU3Ed__23_System_IDisposable_Dispose_m2DC14F993D619A7EF6FC06C8267F07CDB90FB520,
	U3CGetVoicesU3Ed__23_t4C74B1E95210BDA509B6AB73C0CE5286B905B3A1_CustomAttributesCacheGenerator_U3CGetVoicesU3Ed__23_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m86945793027A7CCBFD284B2CA99CDB8F25F548F6,
	U3CGetVoicesU3Ed__23_t4C74B1E95210BDA509B6AB73C0CE5286B905B3A1_CustomAttributesCacheGenerator_U3CGetVoicesU3Ed__23_System_Collections_IEnumerator_Reset_mC5A052AA5B31420FF5DB67F371DD9210CEA04744,
	U3CGetVoicesU3Ed__23_t4C74B1E95210BDA509B6AB73C0CE5286B905B3A1_CustomAttributesCacheGenerator_U3CGetVoicesU3Ed__23_System_Collections_IEnumerator_get_Current_m39A69FC140927C187412E3E2DF98D7E1493A857F,
	U3CSetPitchU3Ed__27_tEFABEA1BBCBB4634FFC95C1C6C6BC2E1AB296B1B_CustomAttributesCacheGenerator_U3CSetPitchU3Ed__27__ctor_m003F38176D39FA4206F6183C0D39B1501CD8E9EE,
	U3CSetPitchU3Ed__27_tEFABEA1BBCBB4634FFC95C1C6C6BC2E1AB296B1B_CustomAttributesCacheGenerator_U3CSetPitchU3Ed__27_System_IDisposable_Dispose_m680F577B98258C3E1CC4C275D39BCEEE86722879,
	U3CSetPitchU3Ed__27_tEFABEA1BBCBB4634FFC95C1C6C6BC2E1AB296B1B_CustomAttributesCacheGenerator_U3CSetPitchU3Ed__27_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mD2A3DF093FD380180CCEF194C5D8964455D1ACC6,
	U3CSetPitchU3Ed__27_tEFABEA1BBCBB4634FFC95C1C6C6BC2E1AB296B1B_CustomAttributesCacheGenerator_U3CSetPitchU3Ed__27_System_Collections_IEnumerator_Reset_m80B114456DDDB5BAF8B39F9ADBA407CF1606FA1B,
	U3CSetPitchU3Ed__27_tEFABEA1BBCBB4634FFC95C1C6C6BC2E1AB296B1B_CustomAttributesCacheGenerator_U3CSetPitchU3Ed__27_System_Collections_IEnumerator_get_Current_m12C49895937FE64B85129B8C377E5A3AFF30595E,
	U3CSetRateU3Ed__28_t47111ED4117F0FE7EB7944C25EE6DE198191DDC9_CustomAttributesCacheGenerator_U3CSetRateU3Ed__28__ctor_m00BBD7D8FD9177152A89D342340510FF249A036D,
	U3CSetRateU3Ed__28_t47111ED4117F0FE7EB7944C25EE6DE198191DDC9_CustomAttributesCacheGenerator_U3CSetRateU3Ed__28_System_IDisposable_Dispose_m56D7E3D6BD827138BC5D04C0361D36F6F728BD37,
	U3CSetRateU3Ed__28_t47111ED4117F0FE7EB7944C25EE6DE198191DDC9_CustomAttributesCacheGenerator_U3CSetRateU3Ed__28_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m464D800221095630A17ACFB9260ACD0394848238,
	U3CSetRateU3Ed__28_t47111ED4117F0FE7EB7944C25EE6DE198191DDC9_CustomAttributesCacheGenerator_U3CSetRateU3Ed__28_System_Collections_IEnumerator_Reset_m4A460D7A3E015022450C8EBC12A92AC5E0E72327,
	U3CSetRateU3Ed__28_t47111ED4117F0FE7EB7944C25EE6DE198191DDC9_CustomAttributesCacheGenerator_U3CSetRateU3Ed__28_System_Collections_IEnumerator_get_Current_m92D4A794D0F0532A96D04F1F96D0520515BB6BFE,
	Example02Proxy_tA1FB05491FD0E2A79271B8DCB0EEEA305B1C44E9_CustomAttributesCacheGenerator_Example02Proxy_Start_m37473AF15D3767E2869EBCEE623BAE1EA5A3DD74,
	Example02Proxy_tA1FB05491FD0E2A79271B8DCB0EEEA305B1C44E9_CustomAttributesCacheGenerator_Example02Proxy_GetVoices_m5CC03E5F7B7566BE7120A098AF29B93443E8FFEB,
	Example02Proxy_tA1FB05491FD0E2A79271B8DCB0EEEA305B1C44E9_CustomAttributesCacheGenerator_Example02Proxy_SetPitch_mB099B881684EA372B6A219FB562B7DE2FBB807EB,
	Example02Proxy_tA1FB05491FD0E2A79271B8DCB0EEEA305B1C44E9_CustomAttributesCacheGenerator_Example02Proxy_SetRate_m5D08473E4431DC7FC479BA30C3C46A7C1AA7496E,
	Example02Proxy_tA1FB05491FD0E2A79271B8DCB0EEEA305B1C44E9_CustomAttributesCacheGenerator_Example02Proxy_U3CStartU3Eb__21_0_m9D025B9B4BEF452A16A7982B1D97903206B6932E,
	Example02Proxy_tA1FB05491FD0E2A79271B8DCB0EEEA305B1C44E9_CustomAttributesCacheGenerator_Example02Proxy_U3CStartU3Eb__21_1_m995FFE286128B847EEFC2C8AC1466000A0049BDC,
	Example02Proxy_tA1FB05491FD0E2A79271B8DCB0EEEA305B1C44E9_CustomAttributesCacheGenerator_Example02Proxy_U3CStartU3Eb__21_2_mB754C6C46342D6CA55790E4A35598DE3CB0FAE37,
	Example02Proxy_tA1FB05491FD0E2A79271B8DCB0EEEA305B1C44E9_CustomAttributesCacheGenerator_Example02Proxy_U3CStartU3Eb__21_3_mB22FCD82AF377C1F1E2DB07466B114D5C04EFAFF,
	Example02Proxy_tA1FB05491FD0E2A79271B8DCB0EEEA305B1C44E9_CustomAttributesCacheGenerator_Example02Proxy_U3CStartU3Eb__21_4_m582889A0D069349173DEAECEBF17FBD8D31ACD2D,
	Example02Proxy_tA1FB05491FD0E2A79271B8DCB0EEEA305B1C44E9_CustomAttributesCacheGenerator_Example02Proxy_U3CGetVoicesU3Eb__22_0_m42F9EBAD38B20DA39C2048894A8D388D9DAFC868,
	Example02Proxy_tA1FB05491FD0E2A79271B8DCB0EEEA305B1C44E9_CustomAttributesCacheGenerator_Example02Proxy_U3CSetIfReadyForDefaultVoiceU3Eb__23_0_m91A0BD1BA1727D47E51FF7F98D861C1A3E4C998C,
	U3CStartU3Ed__21_tFD573D57D53E02687F393696FC999F7C0DDB846E_CustomAttributesCacheGenerator_U3CStartU3Ed__21__ctor_m34D03537D704CFF9A884EB90E3B2F486006DFB88,
	U3CStartU3Ed__21_tFD573D57D53E02687F393696FC999F7C0DDB846E_CustomAttributesCacheGenerator_U3CStartU3Ed__21_System_IDisposable_Dispose_m3A2D102BC25169A90D6112DBA9F6CEDB2AB0D302,
	U3CStartU3Ed__21_tFD573D57D53E02687F393696FC999F7C0DDB846E_CustomAttributesCacheGenerator_U3CStartU3Ed__21_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m93363FB8A68A91796C20BAFDA50A3AF8F123EC4A,
	U3CStartU3Ed__21_tFD573D57D53E02687F393696FC999F7C0DDB846E_CustomAttributesCacheGenerator_U3CStartU3Ed__21_System_Collections_IEnumerator_Reset_m58501F0C8729C05E18577FD05B05586917C3AC30,
	U3CStartU3Ed__21_tFD573D57D53E02687F393696FC999F7C0DDB846E_CustomAttributesCacheGenerator_U3CStartU3Ed__21_System_Collections_IEnumerator_get_Current_mA3E9E23A0E3353169EB46A29FA714C083DE7C027,
	U3CGetVoicesU3Ed__22_t63C88EA26724AA6C07C27964299A020FC5C9D217_CustomAttributesCacheGenerator_U3CGetVoicesU3Ed__22__ctor_m60E3A6BD3C508E0EE96E30CBB6A4C3E39A7B2565,
	U3CGetVoicesU3Ed__22_t63C88EA26724AA6C07C27964299A020FC5C9D217_CustomAttributesCacheGenerator_U3CGetVoicesU3Ed__22_System_IDisposable_Dispose_mD45D8FFE08464334151FFC3DA894F01E8989F0F0,
	U3CGetVoicesU3Ed__22_t63C88EA26724AA6C07C27964299A020FC5C9D217_CustomAttributesCacheGenerator_U3CGetVoicesU3Ed__22_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m203840620BCA464A576234B4503A43D2F4638E10,
	U3CGetVoicesU3Ed__22_t63C88EA26724AA6C07C27964299A020FC5C9D217_CustomAttributesCacheGenerator_U3CGetVoicesU3Ed__22_System_Collections_IEnumerator_Reset_m134E6832088185074D6B6BC66965C2A0161DD2F0,
	U3CGetVoicesU3Ed__22_t63C88EA26724AA6C07C27964299A020FC5C9D217_CustomAttributesCacheGenerator_U3CGetVoicesU3Ed__22_System_Collections_IEnumerator_get_Current_mAAE532A14E45572D45BD62073D30AB28453F23F6,
	U3CSetPitchU3Ed__25_t00215C2568848CF0F1B6EC7FD3965ED387698544_CustomAttributesCacheGenerator_U3CSetPitchU3Ed__25__ctor_m7B27B1C736DD32C058AC4FEDE82949BFC73E63FE,
	U3CSetPitchU3Ed__25_t00215C2568848CF0F1B6EC7FD3965ED387698544_CustomAttributesCacheGenerator_U3CSetPitchU3Ed__25_System_IDisposable_Dispose_mE29FBB4928139BE55235E76AC18A944D31A82F11,
	U3CSetPitchU3Ed__25_t00215C2568848CF0F1B6EC7FD3965ED387698544_CustomAttributesCacheGenerator_U3CSetPitchU3Ed__25_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m349DED4BC47B17F94F69EB6F7730D002F9D2A916,
	U3CSetPitchU3Ed__25_t00215C2568848CF0F1B6EC7FD3965ED387698544_CustomAttributesCacheGenerator_U3CSetPitchU3Ed__25_System_Collections_IEnumerator_Reset_m550C252F8CEFA833ECB2E74ED259602D355615BB,
	U3CSetPitchU3Ed__25_t00215C2568848CF0F1B6EC7FD3965ED387698544_CustomAttributesCacheGenerator_U3CSetPitchU3Ed__25_System_Collections_IEnumerator_get_Current_m7DC2266D9F998979A595E0C599798F981ABC3D9B,
	U3CSetRateU3Ed__26_tB66C71381FE02958DCCB4A1A50724DFFA9A78477_CustomAttributesCacheGenerator_U3CSetRateU3Ed__26__ctor_m30B3B76B925AD990DF7BD5B8DC85B7676E5A19DC,
	U3CSetRateU3Ed__26_tB66C71381FE02958DCCB4A1A50724DFFA9A78477_CustomAttributesCacheGenerator_U3CSetRateU3Ed__26_System_IDisposable_Dispose_m77B3B26C13157B46E0FECB42F5B0FCE7EDAAA07E,
	U3CSetRateU3Ed__26_tB66C71381FE02958DCCB4A1A50724DFFA9A78477_CustomAttributesCacheGenerator_U3CSetRateU3Ed__26_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mEA8A2AC6CD13B5A30D676B3A36F2669CD9049E95,
	U3CSetRateU3Ed__26_tB66C71381FE02958DCCB4A1A50724DFFA9A78477_CustomAttributesCacheGenerator_U3CSetRateU3Ed__26_System_Collections_IEnumerator_Reset_mC7EA2588C39A3A5DF74047F6295DFEA96E636F43,
	U3CSetRateU3Ed__26_tB66C71381FE02958DCCB4A1A50724DFFA9A78477_CustomAttributesCacheGenerator_U3CSetRateU3Ed__26_System_Collections_IEnumerator_get_Current_m485B95A8B747FDF00C4F4C377B4238929F64A129,
	Example03ProxyManagement_t6DB1BBBA2B376ED4FB5958BBD2FF1498DB419E12_CustomAttributesCacheGenerator_Example03ProxyManagement_Start_mA4DFBA24A44B2606CADEBFC763757B9E1CC3C727,
	Example03ProxyManagement_t6DB1BBBA2B376ED4FB5958BBD2FF1498DB419E12_CustomAttributesCacheGenerator_Example03ProxyManagement_U3CStartU3Eb__10_0_mFE057FFD6EE14734A9598B1978E79B8F43AFD718,
	Example03ProxyManagement_t6DB1BBBA2B376ED4FB5958BBD2FF1498DB419E12_CustomAttributesCacheGenerator_Example03ProxyManagement_U3CStartU3Eb__10_1_m86FA78AD1A72F257BF6002F3BCABC772CB4C848B,
	Example03ProxyManagement_t6DB1BBBA2B376ED4FB5958BBD2FF1498DB419E12_CustomAttributesCacheGenerator_Example03ProxyManagement_U3CStartU3Eb__10_2_m6F92CB585A1F54D584FBBEE187E07D7E55C1DC4D,
	Example03ProxyManagement_t6DB1BBBA2B376ED4FB5958BBD2FF1498DB419E12_CustomAttributesCacheGenerator_Example03ProxyManagement_U3CStartU3Eb__10_3_mB46D74519F6952A179CD242E86D73F9D1AE74DE0,
	Example03ProxyManagement_t6DB1BBBA2B376ED4FB5958BBD2FF1498DB419E12_CustomAttributesCacheGenerator_Example03ProxyManagement_U3CStartU3Eb__10_4_m4D57CB44112E5A6ABA7D1836F603D5A410BF819B,
	U3CStartU3Ed__10_t5B930EBF98D13360F6F542B9931A660A8A3383D9_CustomAttributesCacheGenerator_U3CStartU3Ed__10__ctor_mD2EBEF01559763AEB41147B69F400D9C37BA7249,
	U3CStartU3Ed__10_t5B930EBF98D13360F6F542B9931A660A8A3383D9_CustomAttributesCacheGenerator_U3CStartU3Ed__10_System_IDisposable_Dispose_m5DAC313C765AD7414291F37C5AF18854D673A3A0,
	U3CStartU3Ed__10_t5B930EBF98D13360F6F542B9931A660A8A3383D9_CustomAttributesCacheGenerator_U3CStartU3Ed__10_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m7E1CA9BC1A4949854DB2FC73BA73A82370745DC7,
	U3CStartU3Ed__10_t5B930EBF98D13360F6F542B9931A660A8A3383D9_CustomAttributesCacheGenerator_U3CStartU3Ed__10_System_Collections_IEnumerator_Reset_m6854DE8042609BFACA888413C64DAA593D89FAF4,
	U3CStartU3Ed__10_t5B930EBF98D13360F6F542B9931A660A8A3383D9_CustomAttributesCacheGenerator_U3CStartU3Ed__10_System_Collections_IEnumerator_get_Current_mA22569F7BDAE354C9B873EFC702C0ED39C4827BF,
	Example04SbaitsoClone_t9CC4EC7AD9FE0FCBB0D7B68DA7404F43CBFE2AA3_CustomAttributesCacheGenerator_Example04SbaitsoClone_CreateNameInputField_m7251F4E29A15E8ADA11E793B867807C1BDF53E93,
	Example04SbaitsoClone_t9CC4EC7AD9FE0FCBB0D7B68DA7404F43CBFE2AA3_CustomAttributesCacheGenerator_Example04SbaitsoClone_CreateTalkInputField_m31AE5BB29E181784A714AFDCDA0EDAEE14859DA3,
	Example04SbaitsoClone_t9CC4EC7AD9FE0FCBB0D7B68DA7404F43CBFE2AA3_CustomAttributesCacheGenerator_Example04SbaitsoClone_Start_m188E9E8B00F0A4C342849FEAE3D6EA1121799EC9,
	Example04SbaitsoClone_t9CC4EC7AD9FE0FCBB0D7B68DA7404F43CBFE2AA3_CustomAttributesCacheGenerator_Example04SbaitsoClone_GetVoices_m3536E808EF94AD1532169BF1E74B3E2FC6946077,
	Example04SbaitsoClone_t9CC4EC7AD9FE0FCBB0D7B68DA7404F43CBFE2AA3_CustomAttributesCacheGenerator_Example04SbaitsoClone_U3CStartU3Eb__19_0_m168596718F058D2D41F4C9484A840635C1BD625A,
	Example04SbaitsoClone_t9CC4EC7AD9FE0FCBB0D7B68DA7404F43CBFE2AA3_CustomAttributesCacheGenerator_Example04SbaitsoClone_U3CGetVoicesU3Eb__21_0_m6C4B0FBBB6A3F4EB878F0C7D48EF5FD29CB034C3,
	U3CCreateNameInputFieldU3Ed__13_tF99DAFC92BB23A5FF3D97BB4B86C12186720DDCE_CustomAttributesCacheGenerator_U3CCreateNameInputFieldU3Ed__13__ctor_mDBA1413DC9CE6AB47ED198621A1984CF6F696778,
	U3CCreateNameInputFieldU3Ed__13_tF99DAFC92BB23A5FF3D97BB4B86C12186720DDCE_CustomAttributesCacheGenerator_U3CCreateNameInputFieldU3Ed__13_System_IDisposable_Dispose_m76CEC2693009D059E51D719735A8C29C78F2D55F,
	U3CCreateNameInputFieldU3Ed__13_tF99DAFC92BB23A5FF3D97BB4B86C12186720DDCE_CustomAttributesCacheGenerator_U3CCreateNameInputFieldU3Ed__13_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m8AD8BAE973E0D51E605F03CB874C65EDAE7B6303,
	U3CCreateNameInputFieldU3Ed__13_tF99DAFC92BB23A5FF3D97BB4B86C12186720DDCE_CustomAttributesCacheGenerator_U3CCreateNameInputFieldU3Ed__13_System_Collections_IEnumerator_Reset_m29FA913BFECC9BC77B3D8C64D0B5F95E98B3B03B,
	U3CCreateNameInputFieldU3Ed__13_tF99DAFC92BB23A5FF3D97BB4B86C12186720DDCE_CustomAttributesCacheGenerator_U3CCreateNameInputFieldU3Ed__13_System_Collections_IEnumerator_get_Current_m5F9AD182E107360333F5777C391A7412A5DF12F4,
	U3CCreateTalkInputFieldU3Ed__14_t8E84DBC2AA498A934C2001267574FE7D360F832D_CustomAttributesCacheGenerator_U3CCreateTalkInputFieldU3Ed__14__ctor_m280826AEA884301256581138A0DC339D8C684623,
	U3CCreateTalkInputFieldU3Ed__14_t8E84DBC2AA498A934C2001267574FE7D360F832D_CustomAttributesCacheGenerator_U3CCreateTalkInputFieldU3Ed__14_System_IDisposable_Dispose_mB8862146AB8A709DAAB288A0D45BE59D49D9C437,
	U3CCreateTalkInputFieldU3Ed__14_t8E84DBC2AA498A934C2001267574FE7D360F832D_CustomAttributesCacheGenerator_U3CCreateTalkInputFieldU3Ed__14_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mC2563FF15E08EA33D9870DD936C3363F560EF051,
	U3CCreateTalkInputFieldU3Ed__14_t8E84DBC2AA498A934C2001267574FE7D360F832D_CustomAttributesCacheGenerator_U3CCreateTalkInputFieldU3Ed__14_System_Collections_IEnumerator_Reset_mBFFDBE5B05883FD5E838BB70BAD45122DD3EB2D4,
	U3CCreateTalkInputFieldU3Ed__14_t8E84DBC2AA498A934C2001267574FE7D360F832D_CustomAttributesCacheGenerator_U3CCreateTalkInputFieldU3Ed__14_System_Collections_IEnumerator_get_Current_m0535514B983DD7861146E646862AB810E742736D,
	U3CStartU3Ed__19_t61919FD2ED3E01C9A816502E48D6BBE8CC4D515B_CustomAttributesCacheGenerator_U3CStartU3Ed__19__ctor_m9279FC198E1CF92431BA6E3D7AAC85511DD0FFB7,
	U3CStartU3Ed__19_t61919FD2ED3E01C9A816502E48D6BBE8CC4D515B_CustomAttributesCacheGenerator_U3CStartU3Ed__19_System_IDisposable_Dispose_m96082E25192AA2F4B3C5FD7E21EFF89E5823D3EF,
	U3CStartU3Ed__19_t61919FD2ED3E01C9A816502E48D6BBE8CC4D515B_CustomAttributesCacheGenerator_U3CStartU3Ed__19_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mC09FF475BB8AB91B54583BB90F1C22F2B7F86D97,
	U3CStartU3Ed__19_t61919FD2ED3E01C9A816502E48D6BBE8CC4D515B_CustomAttributesCacheGenerator_U3CStartU3Ed__19_System_Collections_IEnumerator_Reset_m8FF2E99C3BD4919C45F728E2ADF3FC20FD82D798,
	U3CStartU3Ed__19_t61919FD2ED3E01C9A816502E48D6BBE8CC4D515B_CustomAttributesCacheGenerator_U3CStartU3Ed__19_System_Collections_IEnumerator_get_Current_m73BAD620D7E8A75EAC27FA0E5459D5E320A65689,
	U3CGetVoicesU3Ed__21_tBCC92EF8427358114BDF9FE975C411924B4B8CF5_CustomAttributesCacheGenerator_U3CGetVoicesU3Ed__21__ctor_m13BCD013A34C0770207E488A37BEA58FE7CEADF9,
	U3CGetVoicesU3Ed__21_tBCC92EF8427358114BDF9FE975C411924B4B8CF5_CustomAttributesCacheGenerator_U3CGetVoicesU3Ed__21_System_IDisposable_Dispose_m918A7E0FA7B6541257B774BD803300F9CF988D32,
	U3CGetVoicesU3Ed__21_tBCC92EF8427358114BDF9FE975C411924B4B8CF5_CustomAttributesCacheGenerator_U3CGetVoicesU3Ed__21_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m7655E6EBDBCC12EB165CBA18546B85DDDA414F99,
	U3CGetVoicesU3Ed__21_tBCC92EF8427358114BDF9FE975C411924B4B8CF5_CustomAttributesCacheGenerator_U3CGetVoicesU3Ed__21_System_Collections_IEnumerator_Reset_m465989673030024C527CDB60A4963F20BB87CA4C,
	U3CGetVoicesU3Ed__21_tBCC92EF8427358114BDF9FE975C411924B4B8CF5_CustomAttributesCacheGenerator_U3CGetVoicesU3Ed__21_System_Collections_IEnumerator_get_Current_mE6C19EFC27F7715437D206D11126D3F89EF09D1D,
	Example06NoGUI_tF68D48D8FA0C165BBCCB2C13AFE24F66AFBC6289_CustomAttributesCacheGenerator_Example06NoGUI_Start_mD573B09D7C17C0E1C75D8C9E5C7851E79FFB5671,
	Example06NoGUI_tF68D48D8FA0C165BBCCB2C13AFE24F66AFBC6289_CustomAttributesCacheGenerator_Example06NoGUI_GetVoices_m701DB6D05DCBA2C4E0C01EAB95DCF0DE17700F0C,
	Example06NoGUI_tF68D48D8FA0C165BBCCB2C13AFE24F66AFBC6289_CustomAttributesCacheGenerator_Example06NoGUI_U3CStartU3Eb__10_0_m72EF17BC2CD555921CE715E7C3F3F3219EF145EC,
	Example06NoGUI_tF68D48D8FA0C165BBCCB2C13AFE24F66AFBC6289_CustomAttributesCacheGenerator_Example06NoGUI_U3CGetVoicesU3Eb__11_0_m94B8870A2FEE4697A251BA871BC88897E0E6DD35,
	U3CStartU3Ed__10_t25226C3095C4900C7D3EC61B87E5A46ECF2CF359_CustomAttributesCacheGenerator_U3CStartU3Ed__10__ctor_m8ED59AE01DDBDFB931EE86943CE3BC3848C6ADDD,
	U3CStartU3Ed__10_t25226C3095C4900C7D3EC61B87E5A46ECF2CF359_CustomAttributesCacheGenerator_U3CStartU3Ed__10_System_IDisposable_Dispose_m42B1BC2D8C580E8807BD4FCD43E3929D47E29AEF,
	U3CStartU3Ed__10_t25226C3095C4900C7D3EC61B87E5A46ECF2CF359_CustomAttributesCacheGenerator_U3CStartU3Ed__10_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mAEC49344EDB434739EC4154F2D0B6284D5F79863,
	U3CStartU3Ed__10_t25226C3095C4900C7D3EC61B87E5A46ECF2CF359_CustomAttributesCacheGenerator_U3CStartU3Ed__10_System_Collections_IEnumerator_Reset_m54625ACABF650958F029E6CAA8623766896CE327,
	U3CStartU3Ed__10_t25226C3095C4900C7D3EC61B87E5A46ECF2CF359_CustomAttributesCacheGenerator_U3CStartU3Ed__10_System_Collections_IEnumerator_get_Current_mCF54097F282F03AAEDDE41D140D95B5DC33E51A6,
	U3CGetVoicesU3Ed__11_t766D855FBC27F4F8B65AF9AB254CFF24E3F5F600_CustomAttributesCacheGenerator_U3CGetVoicesU3Ed__11__ctor_m2F1AF8243FA5D18261369A410EA1F6F68002DE42,
	U3CGetVoicesU3Ed__11_t766D855FBC27F4F8B65AF9AB254CFF24E3F5F600_CustomAttributesCacheGenerator_U3CGetVoicesU3Ed__11_System_IDisposable_Dispose_m97EE3273B4579EB45B880904B822AD7D8D2A91D1,
	U3CGetVoicesU3Ed__11_t766D855FBC27F4F8B65AF9AB254CFF24E3F5F600_CustomAttributesCacheGenerator_U3CGetVoicesU3Ed__11_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m8CD07E3E6785C4A14570F67E8B754BAD6A9C5C7C,
	U3CGetVoicesU3Ed__11_t766D855FBC27F4F8B65AF9AB254CFF24E3F5F600_CustomAttributesCacheGenerator_U3CGetVoicesU3Ed__11_System_Collections_IEnumerator_Reset_m648E41DC38C5F7CEAEFEA8DD33C737DF7E7DE35E,
	U3CGetVoicesU3Ed__11_t766D855FBC27F4F8B65AF9AB254CFF24E3F5F600_CustomAttributesCacheGenerator_U3CGetVoicesU3Ed__11_System_Collections_IEnumerator_get_Current_m9209DA2EE51310298CE318289E76D2ED29966EB5,
	Example07Simple_tB6BA83E60174A25CF753A57323D56DCA06F41E7A_CustomAttributesCacheGenerator_Example07Simple_Start_m5BEC553E42EFDA38D04730050A02F0468FEB6B85,
	Example07Simple_tB6BA83E60174A25CF753A57323D56DCA06F41E7A_CustomAttributesCacheGenerator_Example07Simple_GetVoices_m0C572C4EAEBD69CDD3A6CB7E53A6BD7874413D05,
	Example07Simple_tB6BA83E60174A25CF753A57323D56DCA06F41E7A_CustomAttributesCacheGenerator_Example07Simple_U3CStartU3Eb__9_0_m0E131621CB02A42CCFAD90DE45FDD289329E3035,
	Example07Simple_tB6BA83E60174A25CF753A57323D56DCA06F41E7A_CustomAttributesCacheGenerator_Example07Simple_U3CGetVoicesU3Eb__10_0_m0F7FC45ABC08DCB270C219EC8BD94A12EDDCC5E3,
	U3CStartU3Ed__9_t36FCBB780B64B702CF4ABD4EBB368E6B34586D7B_CustomAttributesCacheGenerator_U3CStartU3Ed__9__ctor_mF5D98CC9248DFAA670B1E1E8DC105AA80B6A3F7A,
	U3CStartU3Ed__9_t36FCBB780B64B702CF4ABD4EBB368E6B34586D7B_CustomAttributesCacheGenerator_U3CStartU3Ed__9_System_IDisposable_Dispose_m0A53CD40C8A18435CED5140A56604E7716F04E54,
	U3CStartU3Ed__9_t36FCBB780B64B702CF4ABD4EBB368E6B34586D7B_CustomAttributesCacheGenerator_U3CStartU3Ed__9_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m1A2A789E1298A2DF842422EC709EF1D338316C5B,
	U3CStartU3Ed__9_t36FCBB780B64B702CF4ABD4EBB368E6B34586D7B_CustomAttributesCacheGenerator_U3CStartU3Ed__9_System_Collections_IEnumerator_Reset_m5A0F287025DEEE61494DC0B746B49472E5107D88,
	U3CStartU3Ed__9_t36FCBB780B64B702CF4ABD4EBB368E6B34586D7B_CustomAttributesCacheGenerator_U3CStartU3Ed__9_System_Collections_IEnumerator_get_Current_mA91D397DE5EF1EC8FF80A4687A787A8BB5B0C9AB,
	U3CGetVoicesU3Ed__10_t89A30C17D49996122417B745EB21C1F275A12870_CustomAttributesCacheGenerator_U3CGetVoicesU3Ed__10__ctor_m745BB18F289E59888EBC4DA97E51151670AB0B10,
	U3CGetVoicesU3Ed__10_t89A30C17D49996122417B745EB21C1F275A12870_CustomAttributesCacheGenerator_U3CGetVoicesU3Ed__10_System_IDisposable_Dispose_m8EEED7ADFA85960ED3B9A9465D4E64C1C6338589,
	U3CGetVoicesU3Ed__10_t89A30C17D49996122417B745EB21C1F275A12870_CustomAttributesCacheGenerator_U3CGetVoicesU3Ed__10_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mE112EB43A922DCD1F08557C0B2EB65ED0BF27869,
	U3CGetVoicesU3Ed__10_t89A30C17D49996122417B745EB21C1F275A12870_CustomAttributesCacheGenerator_U3CGetVoicesU3Ed__10_System_Collections_IEnumerator_Reset_mE597695CBDFD7A5FF951AFF58AE3451AF0BECA1D,
	U3CGetVoicesU3Ed__10_t89A30C17D49996122417B745EB21C1F275A12870_CustomAttributesCacheGenerator_U3CGetVoicesU3Ed__10_System_Collections_IEnumerator_get_Current_m642317F9B85A2F6F491C70917B7FC44742E9DBBF,
	ProxySpeechSynthesisPlugin_t450E4A4888CF497D59C596CEF838904AB9B95414_CustomAttributesCacheGenerator_ProxySpeechSynthesisPlugin_Init_m2D4B203DDA9D67986B9456F93CBE0194D2FAE051,
	ProxySpeechSynthesisPlugin_t450E4A4888CF497D59C596CEF838904AB9B95414_CustomAttributesCacheGenerator_ProxySpeechSynthesisPlugin_RunPendingCommands_m61C1E0632B9CE5E8FA6DF8112E2BD8C0A65CE50D,
	ProxySpeechSynthesisPlugin_t450E4A4888CF497D59C596CEF838904AB9B95414_CustomAttributesCacheGenerator_ProxySpeechSynthesisPlugin_ProxyUtterance_m00F041E6FB4EE1F8D0485464E200BBEF1AF98D49,
	ProxySpeechSynthesisPlugin_t450E4A4888CF497D59C596CEF838904AB9B95414_CustomAttributesCacheGenerator_ProxySpeechSynthesisPlugin_ProxyVoices_m0318C1715E4EDDCCE6B378C7DA66F82348EA530A,
	ProxySpeechSynthesisPlugin_t450E4A4888CF497D59C596CEF838904AB9B95414_CustomAttributesCacheGenerator_ProxySpeechSynthesisPlugin_ProxyOnEnd_mE91F7F13A45699A8A258E6BD90A4DDD310F9AE5E,
	U3CInitU3Ed__12_tD8DCA3016689D1AC98B5293D2E98515302FC46F6_CustomAttributesCacheGenerator_U3CInitU3Ed__12__ctor_m76C78B57D3E077558A1B64BD3C60394348A55EB7,
	U3CInitU3Ed__12_tD8DCA3016689D1AC98B5293D2E98515302FC46F6_CustomAttributesCacheGenerator_U3CInitU3Ed__12_System_IDisposable_Dispose_mCF76C3D929D98063E6D635DBE241A28010B2419C,
	U3CInitU3Ed__12_tD8DCA3016689D1AC98B5293D2E98515302FC46F6_CustomAttributesCacheGenerator_U3CInitU3Ed__12_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mA1875409A854BC8BCBD1706ADC7DE32EC5FC79BF,
	U3CInitU3Ed__12_tD8DCA3016689D1AC98B5293D2E98515302FC46F6_CustomAttributesCacheGenerator_U3CInitU3Ed__12_System_Collections_IEnumerator_Reset_m172521A167F15E22BE4AB993B191F3CB20ABE238,
	U3CInitU3Ed__12_tD8DCA3016689D1AC98B5293D2E98515302FC46F6_CustomAttributesCacheGenerator_U3CInitU3Ed__12_System_Collections_IEnumerator_get_Current_mC582877BCF04920CDC9C5837E6B7AC27286781DF,
	U3CRunPendingCommandsU3Ed__13_t7581E10AF39B06A7884DCFFC6F826010995D4F7F_CustomAttributesCacheGenerator_U3CRunPendingCommandsU3Ed__13__ctor_mF755B6F406B21FCDBA541DD1F627B61DBA9D7B38,
	U3CRunPendingCommandsU3Ed__13_t7581E10AF39B06A7884DCFFC6F826010995D4F7F_CustomAttributesCacheGenerator_U3CRunPendingCommandsU3Ed__13_System_IDisposable_Dispose_m5553B88D973F8D223B7C2E1988E707DA5903BFEE,
	U3CRunPendingCommandsU3Ed__13_t7581E10AF39B06A7884DCFFC6F826010995D4F7F_CustomAttributesCacheGenerator_U3CRunPendingCommandsU3Ed__13_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mE6332DB63594041697D2F788EB9A569EFA45F9E6,
	U3CRunPendingCommandsU3Ed__13_t7581E10AF39B06A7884DCFFC6F826010995D4F7F_CustomAttributesCacheGenerator_U3CRunPendingCommandsU3Ed__13_System_Collections_IEnumerator_Reset_mDA11BAC1A537E93CA1D2FB62F55A1BF67AE7C422,
	U3CRunPendingCommandsU3Ed__13_t7581E10AF39B06A7884DCFFC6F826010995D4F7F_CustomAttributesCacheGenerator_U3CRunPendingCommandsU3Ed__13_System_Collections_IEnumerator_get_Current_mA480802E726F9F7FE60F4D244CDC4C7E947A661B,
	U3CProxyUtteranceU3Ed__14_t8193D4147E2CE182457EAEF45EDACC550B23BCCF_CustomAttributesCacheGenerator_U3CProxyUtteranceU3Ed__14__ctor_m6B50B1BC665C70213F42952F27D32620696E2215,
	U3CProxyUtteranceU3Ed__14_t8193D4147E2CE182457EAEF45EDACC550B23BCCF_CustomAttributesCacheGenerator_U3CProxyUtteranceU3Ed__14_System_IDisposable_Dispose_mBD5CFAAAECAC03CE4AEBE083261B789E45C93183,
	U3CProxyUtteranceU3Ed__14_t8193D4147E2CE182457EAEF45EDACC550B23BCCF_CustomAttributesCacheGenerator_U3CProxyUtteranceU3Ed__14_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m0A1B3F6C03C24C597E853792C14E8CBC3EC4E787,
	U3CProxyUtteranceU3Ed__14_t8193D4147E2CE182457EAEF45EDACC550B23BCCF_CustomAttributesCacheGenerator_U3CProxyUtteranceU3Ed__14_System_Collections_IEnumerator_Reset_m33B544F80C9BD6E23E7EC8CAF68D3BDDE5434177,
	U3CProxyUtteranceU3Ed__14_t8193D4147E2CE182457EAEF45EDACC550B23BCCF_CustomAttributesCacheGenerator_U3CProxyUtteranceU3Ed__14_System_Collections_IEnumerator_get_Current_mFDE586AB0058DAE09E26C1649BF6CE1446B4B4C0,
	U3CProxyVoicesU3Ed__15_tD895D1F2A7ED22311F8F67BAF0D4593B7AF528B9_CustomAttributesCacheGenerator_U3CProxyVoicesU3Ed__15__ctor_m9D51FA49B63EA5B2D6D4741F9AD4638E9F3768D1,
	U3CProxyVoicesU3Ed__15_tD895D1F2A7ED22311F8F67BAF0D4593B7AF528B9_CustomAttributesCacheGenerator_U3CProxyVoicesU3Ed__15_System_IDisposable_Dispose_m125C573F8DCD66E614ED372FE4BEECB946D19048,
	U3CProxyVoicesU3Ed__15_tD895D1F2A7ED22311F8F67BAF0D4593B7AF528B9_CustomAttributesCacheGenerator_U3CProxyVoicesU3Ed__15_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m5AD44B4EDBDC4428C3026163414865806493DE1A,
	U3CProxyVoicesU3Ed__15_tD895D1F2A7ED22311F8F67BAF0D4593B7AF528B9_CustomAttributesCacheGenerator_U3CProxyVoicesU3Ed__15_System_Collections_IEnumerator_Reset_mB95FFE93FD0F160DDC6170F5515C3F7122F676FD,
	U3CProxyVoicesU3Ed__15_tD895D1F2A7ED22311F8F67BAF0D4593B7AF528B9_CustomAttributesCacheGenerator_U3CProxyVoicesU3Ed__15_System_Collections_IEnumerator_get_Current_mB48913C5ECA3605995DCCF3E055E8332181C0745,
	U3CProxyOnEndU3Ed__16_t496FEB12873920FC762FFCB8CFEC07F32B3F5DBC_CustomAttributesCacheGenerator_U3CProxyOnEndU3Ed__16__ctor_m6D7E54EB078F354B70FD6C4FFDD910AE9485008A,
	U3CProxyOnEndU3Ed__16_t496FEB12873920FC762FFCB8CFEC07F32B3F5DBC_CustomAttributesCacheGenerator_U3CProxyOnEndU3Ed__16_System_IDisposable_Dispose_m6A373202B6709BFEAF7D17EBD44817066FEAF057,
	U3CProxyOnEndU3Ed__16_t496FEB12873920FC762FFCB8CFEC07F32B3F5DBC_CustomAttributesCacheGenerator_U3CProxyOnEndU3Ed__16_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m4335A867E6CDA8B392ECCC15F6E8C1ACFDE5BDFB,
	U3CProxyOnEndU3Ed__16_t496FEB12873920FC762FFCB8CFEC07F32B3F5DBC_CustomAttributesCacheGenerator_U3CProxyOnEndU3Ed__16_System_Collections_IEnumerator_Reset_mF623461CC34757BD5120223664D337EF6492EAC4,
	U3CProxyOnEndU3Ed__16_t496FEB12873920FC762FFCB8CFEC07F32B3F5DBC_CustomAttributesCacheGenerator_U3CProxyOnEndU3Ed__16_System_Collections_IEnumerator_get_Current_m7C1A32E5A25138F22368DD2B3C9E66C2BABFA8F9,
	WebGLSpeechSynthesisPlugin_t8211A6663E20D5C0E8847B529BE5A82C197BCBE8_CustomAttributesCacheGenerator_WebGLSpeechSynthesisPlugin_ProxyOnEnd_m4C05BABE8475953BE0C7F45E9458ACAFC99B0144,
	U3CProxyOnEndU3Ed__3_tB3A6640F746B233069CBE83B8CE0930F714BCAAA_CustomAttributesCacheGenerator_U3CProxyOnEndU3Ed__3__ctor_mA20A9FD5B9CA14D3E246E77F2E753B84137DBA2C,
	U3CProxyOnEndU3Ed__3_tB3A6640F746B233069CBE83B8CE0930F714BCAAA_CustomAttributesCacheGenerator_U3CProxyOnEndU3Ed__3_System_IDisposable_Dispose_mB6A59AC6CCFBEACE049F002037CA85CDD8C6C1A1,
	U3CProxyOnEndU3Ed__3_tB3A6640F746B233069CBE83B8CE0930F714BCAAA_CustomAttributesCacheGenerator_U3CProxyOnEndU3Ed__3_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m23180814A7DB3E1CE1C1068FF09F1BB43810BC9C,
	U3CProxyOnEndU3Ed__3_tB3A6640F746B233069CBE83B8CE0930F714BCAAA_CustomAttributesCacheGenerator_U3CProxyOnEndU3Ed__3_System_Collections_IEnumerator_Reset_m5214C4EBB3412E7401F9943FD182087458B09AED,
	U3CProxyOnEndU3Ed__3_tB3A6640F746B233069CBE83B8CE0930F714BCAAA_CustomAttributesCacheGenerator_U3CProxyOnEndU3Ed__3_System_Collections_IEnumerator_get_Current_m1A3AC4C48B1827D4A820083059E5C907FE3395AB,
	Example01Dictation_tA035C7751CA3D6598C3FFC4BB7B4F62761657DF1_CustomAttributesCacheGenerator_Example01Dictation_Start_m2DD9CDBF6C2F5FDB3F69125D1188C11D1CF28ABE,
	Example01Dictation_tA035C7751CA3D6598C3FFC4BB7B4F62761657DF1_CustomAttributesCacheGenerator_Example01Dictation_U3CStartU3Eb__10_0_m324C5EB1646129DCE15C60593D843CF0CEEFBA00,
	Example01Dictation_tA035C7751CA3D6598C3FFC4BB7B4F62761657DF1_CustomAttributesCacheGenerator_Example01Dictation_U3CStartU3Eb__10_1_m4D6C29A76FA1387309A88B31FEB428A82B256190,
	Example01Dictation_tA035C7751CA3D6598C3FFC4BB7B4F62761657DF1_CustomAttributesCacheGenerator_Example01Dictation_U3CStartU3Eb__10_2_mA8B03413D355942B8391D9237DCDE718BEB8DCD6,
	U3CStartU3Ed__10_t84D96784539E4E6C4F0B0D80CF2BE76078D3C65B_CustomAttributesCacheGenerator_U3CStartU3Ed__10__ctor_m9A5ADA41AD9640CCCF47CF94DF79A682A6F3B310,
	U3CStartU3Ed__10_t84D96784539E4E6C4F0B0D80CF2BE76078D3C65B_CustomAttributesCacheGenerator_U3CStartU3Ed__10_System_IDisposable_Dispose_m23F5925258986EB4AFC343608A32AF127CD7640B,
	U3CStartU3Ed__10_t84D96784539E4E6C4F0B0D80CF2BE76078D3C65B_CustomAttributesCacheGenerator_U3CStartU3Ed__10_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m20845EAD2D482F70B9F51BCF13959349EFCE3AF2,
	U3CStartU3Ed__10_t84D96784539E4E6C4F0B0D80CF2BE76078D3C65B_CustomAttributesCacheGenerator_U3CStartU3Ed__10_System_Collections_IEnumerator_Reset_mCAC9839D3729DBAFC1C7A33DEFCED8AE1E914CF8,
	U3CStartU3Ed__10_t84D96784539E4E6C4F0B0D80CF2BE76078D3C65B_CustomAttributesCacheGenerator_U3CStartU3Ed__10_System_Collections_IEnumerator_get_Current_m961FFD432D91B04C6F056699AE4A60F11B7CDBBA,
	Example02SpeechCommands_tC44C4F95779FC04506C676C8557257A9F8F82836_CustomAttributesCacheGenerator_Example02SpeechCommands_Start_m3B392D38DE9534CB1DFDDB6ED8A309B09C10CAD1,
	Example02SpeechCommands_tC44C4F95779FC04506C676C8557257A9F8F82836_CustomAttributesCacheGenerator_Example02SpeechCommands_U3CStartU3Eb__9_0_mAF3718081A86C5DEE426CE29056C45F5D372FDA7,
	Example02SpeechCommands_tC44C4F95779FC04506C676C8557257A9F8F82836_CustomAttributesCacheGenerator_Example02SpeechCommands_U3CStartU3Eb__9_1_mCDAAE9C0F8DE0861443E9766CB560B80D6080D9E,
	Example02SpeechCommands_tC44C4F95779FC04506C676C8557257A9F8F82836_CustomAttributesCacheGenerator_Example02SpeechCommands_U3CStartU3Eb__9_2_m723537E14FC6C80D2737CC31D7FE370B62BBA08B,
	U3CStartU3Ed__9_t9F2C26950487FA4AA558AF9FDBC70EFC86CA1C3A_CustomAttributesCacheGenerator_U3CStartU3Ed__9__ctor_m1DCC4242CF1D0D731B6126E7F5B708942A1072DB,
	U3CStartU3Ed__9_t9F2C26950487FA4AA558AF9FDBC70EFC86CA1C3A_CustomAttributesCacheGenerator_U3CStartU3Ed__9_System_IDisposable_Dispose_m5EB8C4ADEBBD2ED5CCBE2F328282892CB6884DF4,
	U3CStartU3Ed__9_t9F2C26950487FA4AA558AF9FDBC70EFC86CA1C3A_CustomAttributesCacheGenerator_U3CStartU3Ed__9_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m43876813CB26CDF2721AF751DB3E93BB453730EC,
	U3CStartU3Ed__9_t9F2C26950487FA4AA558AF9FDBC70EFC86CA1C3A_CustomAttributesCacheGenerator_U3CStartU3Ed__9_System_Collections_IEnumerator_Reset_m7617645E4D536DB41476515F5F849965F8335D85,
	U3CStartU3Ed__9_t9F2C26950487FA4AA558AF9FDBC70EFC86CA1C3A_CustomAttributesCacheGenerator_U3CStartU3Ed__9_System_Collections_IEnumerator_get_Current_m6B1E12CCB938CD7A198A113D6EE2CC277AA32A67,
	Example02Word_tB6B0FA16CB679D32498336436DC942D6E87177B2_CustomAttributesCacheGenerator_Example02Word_DoHighlight_m670F10C8CF6785E291095095AEEDF3C8A30E285C,
	U3CDoHighlightU3Ed__5_tF820C5AFB9FD264363835FEA11DC3D2FAB920E17_CustomAttributesCacheGenerator_U3CDoHighlightU3Ed__5__ctor_mD5AE312E764E3292D8DD5D158217B95A9CF70565,
	U3CDoHighlightU3Ed__5_tF820C5AFB9FD264363835FEA11DC3D2FAB920E17_CustomAttributesCacheGenerator_U3CDoHighlightU3Ed__5_System_IDisposable_Dispose_m2F8B3A79BFBE378256C8DB81C7E72C2793E1E7B6,
	U3CDoHighlightU3Ed__5_tF820C5AFB9FD264363835FEA11DC3D2FAB920E17_CustomAttributesCacheGenerator_U3CDoHighlightU3Ed__5_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m3E50ECEB225715936D40ED6CFAA54EF88A73A6E5,
	U3CDoHighlightU3Ed__5_tF820C5AFB9FD264363835FEA11DC3D2FAB920E17_CustomAttributesCacheGenerator_U3CDoHighlightU3Ed__5_System_Collections_IEnumerator_Reset_m58923D5EDD1686E38EACEBE6919E2C3818A069DD,
	U3CDoHighlightU3Ed__5_tF820C5AFB9FD264363835FEA11DC3D2FAB920E17_CustomAttributesCacheGenerator_U3CDoHighlightU3Ed__5_System_Collections_IEnumerator_get_Current_m37093F8D22C85DD3FC980F42F64A16F25EB20E61,
	Example03ProxyCommands_t64DC44AB0195FEEAB0CBFFE8EF7C43C38CA8F209_CustomAttributesCacheGenerator_Example03ProxyCommands_Start_m75C3CACF576092A6AEDAB46949555EBF3BC5CE3A,
	Example03ProxyCommands_t64DC44AB0195FEEAB0CBFFE8EF7C43C38CA8F209_CustomAttributesCacheGenerator_Example03ProxyCommands_U3CStartU3Eb__9_0_m1A9B4FAC6449AC5A6B6F60CBA33E6A008B14B5D0,
	Example03ProxyCommands_t64DC44AB0195FEEAB0CBFFE8EF7C43C38CA8F209_CustomAttributesCacheGenerator_Example03ProxyCommands_U3CStartU3Eb__9_1_m59BF70008830BB6F5952C527F5E824A6609DE99A,
	Example03ProxyCommands_t64DC44AB0195FEEAB0CBFFE8EF7C43C38CA8F209_CustomAttributesCacheGenerator_Example03ProxyCommands_U3CStartU3Eb__9_2_m80788C4BD22A86C43951D086E1CA8E2D6B19C09B,
	U3CStartU3Ed__9_t0AC6DFD8163B675E7D948040950A9AD90DE2F27F_CustomAttributesCacheGenerator_U3CStartU3Ed__9__ctor_m3F78AF5ED20089723D623C905AB064288A96CD0E,
	U3CStartU3Ed__9_t0AC6DFD8163B675E7D948040950A9AD90DE2F27F_CustomAttributesCacheGenerator_U3CStartU3Ed__9_System_IDisposable_Dispose_m1C60169E1C0BB3885A9C16EF36572BA72860FA87,
	U3CStartU3Ed__9_t0AC6DFD8163B675E7D948040950A9AD90DE2F27F_CustomAttributesCacheGenerator_U3CStartU3Ed__9_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m9D4EE49E43C98748228A6554736CAA494FFAF419,
	U3CStartU3Ed__9_t0AC6DFD8163B675E7D948040950A9AD90DE2F27F_CustomAttributesCacheGenerator_U3CStartU3Ed__9_System_Collections_IEnumerator_Reset_mA96F9B0685F82F2F9B24923E4B4C58B943E84D55,
	U3CStartU3Ed__9_t0AC6DFD8163B675E7D948040950A9AD90DE2F27F_CustomAttributesCacheGenerator_U3CStartU3Ed__9_System_Collections_IEnumerator_get_Current_m760831DCCDB96CF1185E0FBE602E4286E80C456F,
	Example04ProxyDictation_t85D92F502A55D4B6ED47CF8697F6D736FC91EE03_CustomAttributesCacheGenerator_Example04ProxyDictation_Start_mA1D2D0C1D6CDD82A052581989B6078A6E282C465,
	Example04ProxyDictation_t85D92F502A55D4B6ED47CF8697F6D736FC91EE03_CustomAttributesCacheGenerator_Example04ProxyDictation_U3CStartU3Eb__10_0_m135A7C20424A1634400FCF9A0B2F98FA8457F588,
	Example04ProxyDictation_t85D92F502A55D4B6ED47CF8697F6D736FC91EE03_CustomAttributesCacheGenerator_Example04ProxyDictation_U3CStartU3Eb__10_1_mCA1C4A134D714558B80BC0D4AA7CB81B1C901EE1,
	Example04ProxyDictation_t85D92F502A55D4B6ED47CF8697F6D736FC91EE03_CustomAttributesCacheGenerator_Example04ProxyDictation_U3CStartU3Eb__10_2_m7405692423C6979639B7A85D1643E52FC884B703,
	U3CStartU3Ed__10_t4C950ADFDCBB18E14D5159D1D6BC6D49ABEC06D0_CustomAttributesCacheGenerator_U3CStartU3Ed__10__ctor_mD08E65BE8117B1AC5F05D4FE68ED1115D16B41AD,
	U3CStartU3Ed__10_t4C950ADFDCBB18E14D5159D1D6BC6D49ABEC06D0_CustomAttributesCacheGenerator_U3CStartU3Ed__10_System_IDisposable_Dispose_m2E8EDC3CFF0CBE8EC3721E3E2C73DA2B36DB2A81,
	U3CStartU3Ed__10_t4C950ADFDCBB18E14D5159D1D6BC6D49ABEC06D0_CustomAttributesCacheGenerator_U3CStartU3Ed__10_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mC8E450D1D2D986E4B2BB9CC67E3EB6C6AA0D2C2E,
	U3CStartU3Ed__10_t4C950ADFDCBB18E14D5159D1D6BC6D49ABEC06D0_CustomAttributesCacheGenerator_U3CStartU3Ed__10_System_Collections_IEnumerator_Reset_m2E08DC86E082086DAEC6DFDFF85580F53EDCE9A6,
	U3CStartU3Ed__10_t4C950ADFDCBB18E14D5159D1D6BC6D49ABEC06D0_CustomAttributesCacheGenerator_U3CStartU3Ed__10_System_Collections_IEnumerator_get_Current_mA280C39A17548B1911ACD6D3A44D3053E613B0FC,
	Example05ProxyManagement_t2FFB978A78BEABDF7CD9B526A9ABC9665F01B10A_CustomAttributesCacheGenerator_Example05ProxyManagement_Start_m918205768DD763A8F73CB618156AA6DDC9BAEC6A,
	Example05ProxyManagement_t2FFB978A78BEABDF7CD9B526A9ABC9665F01B10A_CustomAttributesCacheGenerator_Example05ProxyManagement_U3CStartU3Eb__7_0_m4E1A6040A0C7A2098FAB777BAC252F695DFECAAA,
	Example05ProxyManagement_t2FFB978A78BEABDF7CD9B526A9ABC9665F01B10A_CustomAttributesCacheGenerator_Example05ProxyManagement_U3CStartU3Eb__7_1_m0B63C58301AF6B06F1C7C41B45D1F69D58A79644,
	Example05ProxyManagement_t2FFB978A78BEABDF7CD9B526A9ABC9665F01B10A_CustomAttributesCacheGenerator_Example05ProxyManagement_U3CStartU3Eb__7_2_m969A65E0BCFC131DDD819D80D2B9868F4AF2FE8A,
	Example05ProxyManagement_t2FFB978A78BEABDF7CD9B526A9ABC9665F01B10A_CustomAttributesCacheGenerator_Example05ProxyManagement_U3CStartU3Eb__7_3_m0679A42A30B14C905B811C8AC0290ED3FA95AE3C,
	Example05ProxyManagement_t2FFB978A78BEABDF7CD9B526A9ABC9665F01B10A_CustomAttributesCacheGenerator_Example05ProxyManagement_U3CStartU3Eb__7_4_m623F6738E08C72416FF70B5E85F218BE31768A3A,
	U3CStartU3Ed__7_t99F1576B8ECF7C51829E8FD496D881EC6A07024C_CustomAttributesCacheGenerator_U3CStartU3Ed__7__ctor_mFFB60DBC0F017DF05F9FA1003C2B2738FBCFA8FC,
	U3CStartU3Ed__7_t99F1576B8ECF7C51829E8FD496D881EC6A07024C_CustomAttributesCacheGenerator_U3CStartU3Ed__7_System_IDisposable_Dispose_m05D77DC058E9785F795D27EB7E08BABCB8EEAB0F,
	U3CStartU3Ed__7_t99F1576B8ECF7C51829E8FD496D881EC6A07024C_CustomAttributesCacheGenerator_U3CStartU3Ed__7_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m3A38D5FDBB5F5E32BD913E93B26A37B8B50D7313,
	U3CStartU3Ed__7_t99F1576B8ECF7C51829E8FD496D881EC6A07024C_CustomAttributesCacheGenerator_U3CStartU3Ed__7_System_Collections_IEnumerator_Reset_mB7C38F73E9B1A4D4DD842876612C0EC3A5793742,
	U3CStartU3Ed__7_t99F1576B8ECF7C51829E8FD496D881EC6A07024C_CustomAttributesCacheGenerator_U3CStartU3Ed__7_System_Collections_IEnumerator_get_Current_m701771E3243E1E913ACEBC2E8795C89ACC649668,
	Example08NoGUIDictation_t23EBE643531A238ACE66B07CE63DB01CC248919D_CustomAttributesCacheGenerator_Example08NoGUIDictation_Start_m78D8A7861FB8F439E8E757AFB43C9CE12A8DE22D,
	U3CStartU3Ed__1_t24B0D6D6CCC34B861307115FF573C6287C8DEBF8_CustomAttributesCacheGenerator_U3CStartU3Ed__1__ctor_mE7E901F45124A48C59536990AA73C30B675E639B,
	U3CStartU3Ed__1_t24B0D6D6CCC34B861307115FF573C6287C8DEBF8_CustomAttributesCacheGenerator_U3CStartU3Ed__1_System_IDisposable_Dispose_mD105462F2F676E0DBAA2F81DBE280BE7AA7FE238,
	U3CStartU3Ed__1_t24B0D6D6CCC34B861307115FF573C6287C8DEBF8_CustomAttributesCacheGenerator_U3CStartU3Ed__1_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mFC700BEEB25737D16E6524C6E55100390A7C94C6,
	U3CStartU3Ed__1_t24B0D6D6CCC34B861307115FF573C6287C8DEBF8_CustomAttributesCacheGenerator_U3CStartU3Ed__1_System_Collections_IEnumerator_Reset_mA53FBDA94F0F3114DCEE4EFE51DD76112B0F2D4C,
	U3CStartU3Ed__1_t24B0D6D6CCC34B861307115FF573C6287C8DEBF8_CustomAttributesCacheGenerator_U3CStartU3Ed__1_System_Collections_IEnumerator_get_Current_mCE1ADD01EFF09234ED24546244C7B07E4D5B7206,
	Example09NoGUISpeechCommands_t2A3365BE122094335103D4DB284E91FFFB7B292D_CustomAttributesCacheGenerator_Example09NoGUISpeechCommands_Start_m67230CF71BF7306EE666D93170BD0854B411E556,
	U3CStartU3Ed__2_t69FDEAF9EA13E082CF88C7B60B90C5A05CC6179C_CustomAttributesCacheGenerator_U3CStartU3Ed__2__ctor_m960857B297E5A747E66E659907688D23C5E3A9A0,
	U3CStartU3Ed__2_t69FDEAF9EA13E082CF88C7B60B90C5A05CC6179C_CustomAttributesCacheGenerator_U3CStartU3Ed__2_System_IDisposable_Dispose_m205623C92E170CE080D885A81AE3575797ED9EB8,
	U3CStartU3Ed__2_t69FDEAF9EA13E082CF88C7B60B90C5A05CC6179C_CustomAttributesCacheGenerator_U3CStartU3Ed__2_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mEDD0D9D21F65BD4743C534205936CB8CB2CBCD41,
	U3CStartU3Ed__2_t69FDEAF9EA13E082CF88C7B60B90C5A05CC6179C_CustomAttributesCacheGenerator_U3CStartU3Ed__2_System_Collections_IEnumerator_Reset_m2488C31C7C875031BCCBC0ACACEA4BCE6430FE2A,
	U3CStartU3Ed__2_t69FDEAF9EA13E082CF88C7B60B90C5A05CC6179C_CustomAttributesCacheGenerator_U3CStartU3Ed__2_System_Collections_IEnumerator_get_Current_m46FBAA334FC377A24C2D933F71512447FB11FD5B,
	ProxySpeechDetectionPlugin_tD744B61330A08B3CBD1B2270611D582373FE1163_CustomAttributesCacheGenerator_ProxySpeechDetectionPlugin_Init_m84CA595544407D67541C32D252E8A9542EDEE55F,
	ProxySpeechDetectionPlugin_tD744B61330A08B3CBD1B2270611D582373FE1163_CustomAttributesCacheGenerator_ProxySpeechDetectionPlugin_RunPendingCommands_m66D935B982A0D79D6D7533FC6219B71B6759C03D,
	ProxySpeechDetectionPlugin_tD744B61330A08B3CBD1B2270611D582373FE1163_CustomAttributesCacheGenerator_ProxySpeechDetectionPlugin_GetResult_mC030AA32DAA8592997FA7A5F671B5B11B948F41A,
	ProxySpeechDetectionPlugin_tD744B61330A08B3CBD1B2270611D582373FE1163_CustomAttributesCacheGenerator_ProxySpeechDetectionPlugin_ProxyLanguages_m8E82A021B061AAB677F5036EF34520588DDD41E0,
	U3CInitU3Ed__10_tDC9857230B9D766BA44C1A8CD9BC89DAF332560A_CustomAttributesCacheGenerator_U3CInitU3Ed__10__ctor_m012BA9734608E5F6552327811617DA42F34B766A,
	U3CInitU3Ed__10_tDC9857230B9D766BA44C1A8CD9BC89DAF332560A_CustomAttributesCacheGenerator_U3CInitU3Ed__10_System_IDisposable_Dispose_mBBB8890BC2D1590E2C1DDE80878680C2212EF200,
	U3CInitU3Ed__10_tDC9857230B9D766BA44C1A8CD9BC89DAF332560A_CustomAttributesCacheGenerator_U3CInitU3Ed__10_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m54ADF1B4ABC962485DB751DCE71C27B0C17558DD,
	U3CInitU3Ed__10_tDC9857230B9D766BA44C1A8CD9BC89DAF332560A_CustomAttributesCacheGenerator_U3CInitU3Ed__10_System_Collections_IEnumerator_Reset_mE083368AC1F41DD0E8F9B2988AB38626CDDB50B4,
	U3CInitU3Ed__10_tDC9857230B9D766BA44C1A8CD9BC89DAF332560A_CustomAttributesCacheGenerator_U3CInitU3Ed__10_System_Collections_IEnumerator_get_Current_mE9D4A920E754B9F844722ABD42F7911A32B253AC,
	U3CRunPendingCommandsU3Ed__12_t3B51F7A544880108D20AA679685AAD3200FD6E8D_CustomAttributesCacheGenerator_U3CRunPendingCommandsU3Ed__12__ctor_mC11C018DC0A91D90202312BE9D6E7031F8B7A966,
	U3CRunPendingCommandsU3Ed__12_t3B51F7A544880108D20AA679685AAD3200FD6E8D_CustomAttributesCacheGenerator_U3CRunPendingCommandsU3Ed__12_System_IDisposable_Dispose_m273B75505906AC0B3E987E74414503C5EAFD3029,
	U3CRunPendingCommandsU3Ed__12_t3B51F7A544880108D20AA679685AAD3200FD6E8D_CustomAttributesCacheGenerator_U3CRunPendingCommandsU3Ed__12_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mF49722C3374CA4DCD5DEDA7A42E74B2E85449D0C,
	U3CRunPendingCommandsU3Ed__12_t3B51F7A544880108D20AA679685AAD3200FD6E8D_CustomAttributesCacheGenerator_U3CRunPendingCommandsU3Ed__12_System_Collections_IEnumerator_Reset_mCE9FD8ED116F2DE4D31163FF91ED0F0BCC872409,
	U3CRunPendingCommandsU3Ed__12_t3B51F7A544880108D20AA679685AAD3200FD6E8D_CustomAttributesCacheGenerator_U3CRunPendingCommandsU3Ed__12_System_Collections_IEnumerator_get_Current_m0214811E4DC232B9A5AD3775866A44B909549478,
	U3CGetResultU3Ed__13_t429D1A37625B8E217E88830CCFE7B4B9E5F3DCED_CustomAttributesCacheGenerator_U3CGetResultU3Ed__13__ctor_m0CDDF83A5EBDA6AC4DA49D5DF38A02D721246C3F,
	U3CGetResultU3Ed__13_t429D1A37625B8E217E88830CCFE7B4B9E5F3DCED_CustomAttributesCacheGenerator_U3CGetResultU3Ed__13_System_IDisposable_Dispose_mD2C540480CC4924CBDA0D6CC8090D5E69A09E52A,
	U3CGetResultU3Ed__13_t429D1A37625B8E217E88830CCFE7B4B9E5F3DCED_CustomAttributesCacheGenerator_U3CGetResultU3Ed__13_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m7EBC61FA9172162F7E2D417E3E9646E091814409,
	U3CGetResultU3Ed__13_t429D1A37625B8E217E88830CCFE7B4B9E5F3DCED_CustomAttributesCacheGenerator_U3CGetResultU3Ed__13_System_Collections_IEnumerator_Reset_mBCB807344745DBFF79C784AA606095E3ACECD75C,
	U3CGetResultU3Ed__13_t429D1A37625B8E217E88830CCFE7B4B9E5F3DCED_CustomAttributesCacheGenerator_U3CGetResultU3Ed__13_System_Collections_IEnumerator_get_Current_m6EB722F8467C0E7094AFC385192102522361A12E,
	U3CProxyLanguagesU3Ed__19_t3B013A40DC9B77C22F68441BA9F709DD9D113FE9_CustomAttributesCacheGenerator_U3CProxyLanguagesU3Ed__19__ctor_m73D98C4B262426CBA8A82CFCB41B8CC9E0083405,
	U3CProxyLanguagesU3Ed__19_t3B013A40DC9B77C22F68441BA9F709DD9D113FE9_CustomAttributesCacheGenerator_U3CProxyLanguagesU3Ed__19_System_IDisposable_Dispose_m7ABACFD849FD40300BF13D89CC177D0836FB0B4D,
	U3CProxyLanguagesU3Ed__19_t3B013A40DC9B77C22F68441BA9F709DD9D113FE9_CustomAttributesCacheGenerator_U3CProxyLanguagesU3Ed__19_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m51D3D2D32D00F389B14479614A9A4F24C90D57D0,
	U3CProxyLanguagesU3Ed__19_t3B013A40DC9B77C22F68441BA9F709DD9D113FE9_CustomAttributesCacheGenerator_U3CProxyLanguagesU3Ed__19_System_Collections_IEnumerator_Reset_m368C82C50D2515A4715969FA6E605EF35B095C30,
	U3CProxyLanguagesU3Ed__19_t3B013A40DC9B77C22F68441BA9F709DD9D113FE9_CustomAttributesCacheGenerator_U3CProxyLanguagesU3Ed__19_System_Collections_IEnumerator_get_Current_m443AEE64E35421F4376968F384CE357F48E10A9E,
	Example01DictationSynthesis_tABE5E8AB9ABB921D74D02390BBD785C542409B34_CustomAttributesCacheGenerator_Example01DictationSynthesis_Start_mB4882D5513B9316D6BAB403228D4AED406D59C1C,
	Example01DictationSynthesis_tABE5E8AB9ABB921D74D02390BBD785C542409B34_CustomAttributesCacheGenerator_Example01DictationSynthesis_GetVoices_m7A46A9BDFEAC0BC2BE84DA4D4E435C4709F1940B,
	Example01DictationSynthesis_tABE5E8AB9ABB921D74D02390BBD785C542409B34_CustomAttributesCacheGenerator_Example01DictationSynthesis_SetPitch_m387E57A36A344607A5BF04F1BF4DE73E58F1C212,
	Example01DictationSynthesis_tABE5E8AB9ABB921D74D02390BBD785C542409B34_CustomAttributesCacheGenerator_Example01DictationSynthesis_SetRate_m98FDF4E83F129860CF981AC241540A865844849C,
	Example01DictationSynthesis_tABE5E8AB9ABB921D74D02390BBD785C542409B34_CustomAttributesCacheGenerator_Example01DictationSynthesis_U3CStartU3Eb__28_0_m2BB9F8966F92CEEE45201B8B5EBC468EF95CC0A8,
	Example01DictationSynthesis_tABE5E8AB9ABB921D74D02390BBD785C542409B34_CustomAttributesCacheGenerator_Example01DictationSynthesis_U3CStartU3Eb__28_1_m40F45F64E6037C82A4D94DCB628D9B9D20F789B2,
	Example01DictationSynthesis_tABE5E8AB9ABB921D74D02390BBD785C542409B34_CustomAttributesCacheGenerator_Example01DictationSynthesis_U3CStartU3Eb__28_2_mF4484E4A96810FC30E1902285A2952CB7EFEF50C,
	Example01DictationSynthesis_tABE5E8AB9ABB921D74D02390BBD785C542409B34_CustomAttributesCacheGenerator_Example01DictationSynthesis_U3CStartU3Eb__28_3_m7D58A195ACB6F257BBB7646F8E3C6ADCC4E506D4,
	Example01DictationSynthesis_tABE5E8AB9ABB921D74D02390BBD785C542409B34_CustomAttributesCacheGenerator_Example01DictationSynthesis_U3CStartU3Eb__28_8_mB7702341DF0663D771005EC27CC628EA3825A6C1,
	Example01DictationSynthesis_tABE5E8AB9ABB921D74D02390BBD785C542409B34_CustomAttributesCacheGenerator_Example01DictationSynthesis_U3CStartU3Eb__28_9_m4109CE818814E955E1C68269A520B2AB34F0CBC1,
	Example01DictationSynthesis_tABE5E8AB9ABB921D74D02390BBD785C542409B34_CustomAttributesCacheGenerator_Example01DictationSynthesis_U3CStartU3Eb__28_4_mDF45F0C4ED5AB0BAAF9C49DC31880C30A2ABC532,
	Example01DictationSynthesis_tABE5E8AB9ABB921D74D02390BBD785C542409B34_CustomAttributesCacheGenerator_Example01DictationSynthesis_U3CStartU3Eb__28_5_mF1465E0ECA48721C2D79C05FC8673499A676639F,
	Example01DictationSynthesis_tABE5E8AB9ABB921D74D02390BBD785C542409B34_CustomAttributesCacheGenerator_Example01DictationSynthesis_U3CStartU3Eb__28_6_mF0B4E30B599D0E84B30C93594F4D8BB01070A61B,
	Example01DictationSynthesis_tABE5E8AB9ABB921D74D02390BBD785C542409B34_CustomAttributesCacheGenerator_Example01DictationSynthesis_U3CStartU3Eb__28_7_mB2DEFDEF9A51C0B5267A44A8DB5224DC798BAD6F,
	Example01DictationSynthesis_tABE5E8AB9ABB921D74D02390BBD785C542409B34_CustomAttributesCacheGenerator_Example01DictationSynthesis_U3CGetVoicesU3Eb__32_0_m7647381772F8947F21087CA9489F9A76C9AEEC41,
	Example01DictationSynthesis_tABE5E8AB9ABB921D74D02390BBD785C542409B34_CustomAttributesCacheGenerator_Example01DictationSynthesis_U3CSetIfReadyForDefaultVoiceU3Eb__33_0_mE07F5718D62BA9FC6AB3DC33A6BF537C091A9523,
	U3CStartU3Ed__28_t15833A9AB280EADB404BBB5150CC5E571050157D_CustomAttributesCacheGenerator_U3CStartU3Ed__28__ctor_m6251A007ED7831DB20DE6DD12568AFC488C1B1A0,
	U3CStartU3Ed__28_t15833A9AB280EADB404BBB5150CC5E571050157D_CustomAttributesCacheGenerator_U3CStartU3Ed__28_System_IDisposable_Dispose_mE1A0CB500E998B63CCC43B876F7F55F86C1B921B,
	U3CStartU3Ed__28_t15833A9AB280EADB404BBB5150CC5E571050157D_CustomAttributesCacheGenerator_U3CStartU3Ed__28_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mB67313988CF183C834E335EE4EE373EE42FAAE53,
	U3CStartU3Ed__28_t15833A9AB280EADB404BBB5150CC5E571050157D_CustomAttributesCacheGenerator_U3CStartU3Ed__28_System_Collections_IEnumerator_Reset_mE54EED999CFD46CE5C5FF21F95AF6153F165E5CE,
	U3CStartU3Ed__28_t15833A9AB280EADB404BBB5150CC5E571050157D_CustomAttributesCacheGenerator_U3CStartU3Ed__28_System_Collections_IEnumerator_get_Current_m8A5EF102397A7D73091A8A9EECEB30078BF7B8CC,
	U3CGetVoicesU3Ed__32_tD206C9C44DE7C20EB7573ED4D8761B0FD13677FD_CustomAttributesCacheGenerator_U3CGetVoicesU3Ed__32__ctor_mF7508112903C4F9ED3CAF31218951EAE34D1DBEE,
	U3CGetVoicesU3Ed__32_tD206C9C44DE7C20EB7573ED4D8761B0FD13677FD_CustomAttributesCacheGenerator_U3CGetVoicesU3Ed__32_System_IDisposable_Dispose_mEC4E1291389C3EA9CFA71F80146E8F16FA321323,
	U3CGetVoicesU3Ed__32_tD206C9C44DE7C20EB7573ED4D8761B0FD13677FD_CustomAttributesCacheGenerator_U3CGetVoicesU3Ed__32_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m4EAEC51E6B7FBCD8B9A1CBCD16DA2966223124C8,
	U3CGetVoicesU3Ed__32_tD206C9C44DE7C20EB7573ED4D8761B0FD13677FD_CustomAttributesCacheGenerator_U3CGetVoicesU3Ed__32_System_Collections_IEnumerator_Reset_m094A2A097F4044D384F0C23A2BD9629CB03E9B90,
	U3CGetVoicesU3Ed__32_tD206C9C44DE7C20EB7573ED4D8761B0FD13677FD_CustomAttributesCacheGenerator_U3CGetVoicesU3Ed__32_System_Collections_IEnumerator_get_Current_mBBB5185B3F8DB86560A9DE3F3BBB2E7531E2C484,
	U3CSetPitchU3Ed__35_tF30264CE7FB8EFB8DD7CC3A54F6E538C7526D22E_CustomAttributesCacheGenerator_U3CSetPitchU3Ed__35__ctor_m9D8D07FCA90D3E73D299B2D80FE6E7C891D77952,
	U3CSetPitchU3Ed__35_tF30264CE7FB8EFB8DD7CC3A54F6E538C7526D22E_CustomAttributesCacheGenerator_U3CSetPitchU3Ed__35_System_IDisposable_Dispose_mB8668091EEFA11E8010416E9001FCF9EC09ECE72,
	U3CSetPitchU3Ed__35_tF30264CE7FB8EFB8DD7CC3A54F6E538C7526D22E_CustomAttributesCacheGenerator_U3CSetPitchU3Ed__35_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m78E469CF14747B40EB8F0759E0639B39FB089C41,
	U3CSetPitchU3Ed__35_tF30264CE7FB8EFB8DD7CC3A54F6E538C7526D22E_CustomAttributesCacheGenerator_U3CSetPitchU3Ed__35_System_Collections_IEnumerator_Reset_m66DB440A475F280C6F51FE8B7F3016EB7FE9151F,
	U3CSetPitchU3Ed__35_tF30264CE7FB8EFB8DD7CC3A54F6E538C7526D22E_CustomAttributesCacheGenerator_U3CSetPitchU3Ed__35_System_Collections_IEnumerator_get_Current_m83AEA3365906FC77D9D1DE7859301D5621AB2E4B,
	U3CSetRateU3Ed__36_tE587DF9904D56F1F471E8AAB2C639678AE1C2983_CustomAttributesCacheGenerator_U3CSetRateU3Ed__36__ctor_m850DD6B944C189AFC8DD0EC75E985E279C02848D,
	U3CSetRateU3Ed__36_tE587DF9904D56F1F471E8AAB2C639678AE1C2983_CustomAttributesCacheGenerator_U3CSetRateU3Ed__36_System_IDisposable_Dispose_mFE9B1BC95F527E94DB2F805C2E2DE218C08C36A0,
	U3CSetRateU3Ed__36_tE587DF9904D56F1F471E8AAB2C639678AE1C2983_CustomAttributesCacheGenerator_U3CSetRateU3Ed__36_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m8ACBE875E3E52945765CB65BB421C1DB1758D2D3,
	U3CSetRateU3Ed__36_tE587DF9904D56F1F471E8AAB2C639678AE1C2983_CustomAttributesCacheGenerator_U3CSetRateU3Ed__36_System_Collections_IEnumerator_Reset_mD3357A03BEE1F8589E4CC34678D40D0137D0FC3D,
	U3CSetRateU3Ed__36_tE587DF9904D56F1F471E8AAB2C639678AE1C2983_CustomAttributesCacheGenerator_U3CSetRateU3Ed__36_System_Collections_IEnumerator_get_Current_m7934C30490D8913CC34E6F24824C5CFD13ED62C7,
	Example02DictationSbaitso_t8122EA3EC19C62514409A0B8D63C4B5E979DC181_CustomAttributesCacheGenerator_Example02DictationSbaitso_CreateNameInputField_m669A31FCB67151C44FA729295641A592875E8ECD,
	Example02DictationSbaitso_t8122EA3EC19C62514409A0B8D63C4B5E979DC181_CustomAttributesCacheGenerator_Example02DictationSbaitso_CreateTalkInputField_m93C3B2C862D8C5D6A359775752B6DAE8065D0021,
	Example02DictationSbaitso_t8122EA3EC19C62514409A0B8D63C4B5E979DC181_CustomAttributesCacheGenerator_Example02DictationSbaitso_Start_m7444A4D38B7A9C05C2917DAAC2EF163D756D7E74,
	Example02DictationSbaitso_t8122EA3EC19C62514409A0B8D63C4B5E979DC181_CustomAttributesCacheGenerator_Example02DictationSbaitso_GetVoices_m089DFE1583E1DD7E4BCCE8AD263845C0EB0AEE83,
	Example02DictationSbaitso_t8122EA3EC19C62514409A0B8D63C4B5E979DC181_CustomAttributesCacheGenerator_Example02DictationSbaitso_U3CStartU3Eb__21_0_mCCD542190710E61A9EDE15A3E559DC311ADD9BEB,
	Example02DictationSbaitso_t8122EA3EC19C62514409A0B8D63C4B5E979DC181_CustomAttributesCacheGenerator_Example02DictationSbaitso_U3CStartU3Eb__21_1_mF537CA2C485462C331E4C7316CCFEB0A6DC1F203,
	Example02DictationSbaitso_t8122EA3EC19C62514409A0B8D63C4B5E979DC181_CustomAttributesCacheGenerator_Example02DictationSbaitso_U3CStartU3Eb__21_2_mC0F51C19C5FF069B8BDCCAE56A1507FDE85B8772,
	Example02DictationSbaitso_t8122EA3EC19C62514409A0B8D63C4B5E979DC181_CustomAttributesCacheGenerator_Example02DictationSbaitso_U3CGetVoicesU3Eb__24_0_mB6088C47629D61E8ABE71DF63887EEBECC040C67,
	U3CCreateNameInputFieldU3Ed__18_tCF407195076F20F3CC29E9320B5836378BF87FA4_CustomAttributesCacheGenerator_U3CCreateNameInputFieldU3Ed__18__ctor_m4B5B254484C31B4D071462387837964CB5FB5FE0,
	U3CCreateNameInputFieldU3Ed__18_tCF407195076F20F3CC29E9320B5836378BF87FA4_CustomAttributesCacheGenerator_U3CCreateNameInputFieldU3Ed__18_System_IDisposable_Dispose_mF82190DDE2697199F48366F177466E899B0022F2,
	U3CCreateNameInputFieldU3Ed__18_tCF407195076F20F3CC29E9320B5836378BF87FA4_CustomAttributesCacheGenerator_U3CCreateNameInputFieldU3Ed__18_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mA7C8F9A3E9BCA451E48A0B59AF88D0D30FA8052D,
	U3CCreateNameInputFieldU3Ed__18_tCF407195076F20F3CC29E9320B5836378BF87FA4_CustomAttributesCacheGenerator_U3CCreateNameInputFieldU3Ed__18_System_Collections_IEnumerator_Reset_m1EA0F77850A1CE0F7FA9D16059514D7039A976C6,
	U3CCreateNameInputFieldU3Ed__18_tCF407195076F20F3CC29E9320B5836378BF87FA4_CustomAttributesCacheGenerator_U3CCreateNameInputFieldU3Ed__18_System_Collections_IEnumerator_get_Current_m6EB5B4889BD9E1D8F6101FFF052897D2508A69F0,
	U3CCreateTalkInputFieldU3Ed__19_t5C727E8D85F30A4CC308C816B210A1FD28056A29_CustomAttributesCacheGenerator_U3CCreateTalkInputFieldU3Ed__19__ctor_mCE780DD23754578C72F63AA3BE000D2EF21C457E,
	U3CCreateTalkInputFieldU3Ed__19_t5C727E8D85F30A4CC308C816B210A1FD28056A29_CustomAttributesCacheGenerator_U3CCreateTalkInputFieldU3Ed__19_System_IDisposable_Dispose_mF2ACFC307D0C8C5D10CAC168C84B87629E31C5ED,
	U3CCreateTalkInputFieldU3Ed__19_t5C727E8D85F30A4CC308C816B210A1FD28056A29_CustomAttributesCacheGenerator_U3CCreateTalkInputFieldU3Ed__19_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mE52A015117FA49929B6655BA390A090BC6BC3934,
	U3CCreateTalkInputFieldU3Ed__19_t5C727E8D85F30A4CC308C816B210A1FD28056A29_CustomAttributesCacheGenerator_U3CCreateTalkInputFieldU3Ed__19_System_Collections_IEnumerator_Reset_m8F584B55249C63900EE385230905F4819BCB245C,
	U3CCreateTalkInputFieldU3Ed__19_t5C727E8D85F30A4CC308C816B210A1FD28056A29_CustomAttributesCacheGenerator_U3CCreateTalkInputFieldU3Ed__19_System_Collections_IEnumerator_get_Current_mDBCA9A1BCCADCA02EC5ABFFD1073E495B6F7EDE6,
	U3CStartU3Ed__21_tCD0B0FE65242BC30DD807F964ABE50628F2C6FD8_CustomAttributesCacheGenerator_U3CStartU3Ed__21__ctor_mC72EA5BAA492982E48576F5F60FD8483732353B6,
	U3CStartU3Ed__21_tCD0B0FE65242BC30DD807F964ABE50628F2C6FD8_CustomAttributesCacheGenerator_U3CStartU3Ed__21_System_IDisposable_Dispose_m12232760A14BC34C5C5EC1EACC05E1852D6A89A9,
	U3CStartU3Ed__21_tCD0B0FE65242BC30DD807F964ABE50628F2C6FD8_CustomAttributesCacheGenerator_U3CStartU3Ed__21_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m535DFD5C67CB16D689578F83B00CEC23E4484FEA,
	U3CStartU3Ed__21_tCD0B0FE65242BC30DD807F964ABE50628F2C6FD8_CustomAttributesCacheGenerator_U3CStartU3Ed__21_System_Collections_IEnumerator_Reset_m67EA047E1E606C626E1B1D7A7E58018554CC12A8,
	U3CStartU3Ed__21_tCD0B0FE65242BC30DD807F964ABE50628F2C6FD8_CustomAttributesCacheGenerator_U3CStartU3Ed__21_System_Collections_IEnumerator_get_Current_mA401C2A6D290024BD88FC27BE57E64DDE97B5433,
	U3CGetVoicesU3Ed__24_tC53D4CB8A21E838EE8088A9FDD86CA7324EB8050_CustomAttributesCacheGenerator_U3CGetVoicesU3Ed__24__ctor_m833C2611DC287B9E47094CADC771D692419E63E6,
	U3CGetVoicesU3Ed__24_tC53D4CB8A21E838EE8088A9FDD86CA7324EB8050_CustomAttributesCacheGenerator_U3CGetVoicesU3Ed__24_System_IDisposable_Dispose_m71DBB98BC3D4A498D09E055587E51F12F98331CC,
	U3CGetVoicesU3Ed__24_tC53D4CB8A21E838EE8088A9FDD86CA7324EB8050_CustomAttributesCacheGenerator_U3CGetVoicesU3Ed__24_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m52B65A6687210AA4FB8BB4C690ADA13DF97D1061,
	U3CGetVoicesU3Ed__24_tC53D4CB8A21E838EE8088A9FDD86CA7324EB8050_CustomAttributesCacheGenerator_U3CGetVoicesU3Ed__24_System_Collections_IEnumerator_Reset_m9F98C408BFCDFD6C23ED7A460E33534854BBC079,
	U3CGetVoicesU3Ed__24_tC53D4CB8A21E838EE8088A9FDD86CA7324EB8050_CustomAttributesCacheGenerator_U3CGetVoicesU3Ed__24_System_Collections_IEnumerator_get_Current_mBF17624D10518A8301364E0EE6CED011DC123301,
	BaseHelper_t06A43A26211DD50047AB61E90EC3DCF06758E347_CustomAttributesCacheGenerator_BaseHelper_GetFiles_m1C040E9DE5359EF69F773CFCC1DE845322EB7657____extensions2,
	SpeechSynthesisUtils_t09D6E455F5A2390DF9AF4AABD4E4D80EACC27BBA_CustomAttributesCacheGenerator_SpeechSynthesisUtils_SetActive_m1B7D834516CFBA8840500E95C0C190B85626AB8E____args1,
	SpeechSynthesisUtils_t09D6E455F5A2390DF9AF4AABD4E4D80EACC27BBA_CustomAttributesCacheGenerator_SpeechSynthesisUtils_SetInteractable_m0D993F7363AB78CFBA8052B611937D418C0ED8B4____args1,
	SpeechDetectionUtils_t121D1188BECD5625D5D24645D4461A4F4EC4F1FE_CustomAttributesCacheGenerator_SpeechDetectionUtils_SetActive_mC1B4C840410E33AB9E47BD8C4FF38580CA0197CA____args1,
	SpeechDetectionUtils_t121D1188BECD5625D5D24645D4461A4F4EC4F1FE_CustomAttributesCacheGenerator_SpeechDetectionUtils_SetInteractable_m4A6F5FDC6777BFDD436E8EA4EBD628D0FB5A921E____args1,
	AssemblyU2DCSharpU2Dfirstpass_CustomAttributesCacheGenerator,
};
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void RuntimeCompatibilityAttribute_set_WrapNonExceptionThrows_m8562196F90F3EBCEC23B5708EE0332842883C490_inline (RuntimeCompatibilityAttribute_tFF99AB2963098F9CBCD47A20D9FD3D51C17C1C80 * __this, bool ___value0, const RuntimeMethod* method)
{
	{
		bool L_0 = ___value0;
		__this->set_m_wrapNonExceptionThrows_0(L_0);
		return;
	}
}
