﻿#include "pch-c.h"
#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include "codegen/il2cpp-codegen-metadata.h"





// 0x00000001 System.Void linguaScelta::Start()
extern void linguaScelta_Start_m59313CA922B740E382F7F0E3F5E41CFEFEA88925 (void);
// 0x00000002 System.Void linguaScelta::Update()
extern void linguaScelta_Update_m58F06DC24362408FFF1B18E071D05D84833FA45C (void);
// 0x00000003 System.Void linguaScelta::.ctor()
extern void linguaScelta__ctor_mF8F3566234E092CE74571116F44C5D8AC8750225 (void);
// 0x00000004 System.String Crosstales.ExtensionMethods::CTToTitleCase(System.String)
extern void ExtensionMethods_CTToTitleCase_m0FAF40693A916D1BA985195E30AA7B9753C11DE2 (void);
// 0x00000005 System.String Crosstales.ExtensionMethods::CTReverse(System.String)
extern void ExtensionMethods_CTReverse_m375B1B5F94367BC73BC054481C4528E1829758A0 (void);
// 0x00000006 System.String Crosstales.ExtensionMethods::CTReplace(System.String,System.String,System.String,System.StringComparison)
extern void ExtensionMethods_CTReplace_mE45837F5BF2ECDD64251F7DC85303D36308AFBB9 (void);
// 0x00000007 System.Boolean Crosstales.ExtensionMethods::CTEquals(System.String,System.String,System.StringComparison)
extern void ExtensionMethods_CTEquals_m70FD226B78B4DD7ED0672F326B16568EA3D9B446 (void);
// 0x00000008 System.Boolean Crosstales.ExtensionMethods::CTContains(System.String,System.String,System.StringComparison)
extern void ExtensionMethods_CTContains_m52B3F3D1019BBD1746371264A2671656CDADB3C2 (void);
// 0x00000009 System.Boolean Crosstales.ExtensionMethods::CTContainsAny(System.String,System.String,System.Char)
extern void ExtensionMethods_CTContainsAny_m50A29F87C6A50663D9B599FEA1D1359EC07969A3 (void);
// 0x0000000A System.Boolean Crosstales.ExtensionMethods::CTContainsAll(System.String,System.String,System.Char)
extern void ExtensionMethods_CTContainsAll_mAB9F9024F8E76F4449FE666602718634956C2464 (void);
// 0x0000000B System.Void Crosstales.ExtensionMethods::CTShuffle(T[],System.Int32)
// 0x0000000C System.String Crosstales.ExtensionMethods::CTDump(T[],System.String,System.String)
// 0x0000000D System.String Crosstales.ExtensionMethods::CTDump(UnityEngine.Quaternion[])
extern void ExtensionMethods_CTDump_m9372AF9431740E725E0A0EE720EEB4B51EF8D626 (void);
// 0x0000000E System.String Crosstales.ExtensionMethods::CTDump(UnityEngine.Vector2[])
extern void ExtensionMethods_CTDump_mDD2831261D66781D294ADBFFBD9A397D4A5B4681 (void);
// 0x0000000F System.String Crosstales.ExtensionMethods::CTDump(UnityEngine.Vector3[])
extern void ExtensionMethods_CTDump_m2A792DFFEECEFF18E99A289916B9165C127E7883 (void);
// 0x00000010 System.String Crosstales.ExtensionMethods::CTDump(UnityEngine.Vector4[])
extern void ExtensionMethods_CTDump_mEDCBC052999C7F88ECF5897E865B6467FF1F452E (void);
// 0x00000011 System.String[] Crosstales.ExtensionMethods::CTToString(T[])
// 0x00000012 System.Void Crosstales.ExtensionMethods::CTShuffle(System.Collections.Generic.IList`1<T>,System.Int32)
// 0x00000013 System.String Crosstales.ExtensionMethods::CTDump(System.Collections.Generic.IList`1<T>,System.String,System.String)
// 0x00000014 System.String Crosstales.ExtensionMethods::CTDump(System.Collections.Generic.IList`1<UnityEngine.Quaternion>)
extern void ExtensionMethods_CTDump_m59F8DE0A578B3B891B4D8C1ED38688E86940F976 (void);
// 0x00000015 System.String Crosstales.ExtensionMethods::CTDump(System.Collections.Generic.IList`1<UnityEngine.Vector2>)
extern void ExtensionMethods_CTDump_m6E14577C017276ACD5DA587C029A664694E859C9 (void);
// 0x00000016 System.String Crosstales.ExtensionMethods::CTDump(System.Collections.Generic.IList`1<UnityEngine.Vector3>)
extern void ExtensionMethods_CTDump_mD3A9D8E4C2FAF2BE628602C89238233DBE170FDE (void);
// 0x00000017 System.String Crosstales.ExtensionMethods::CTDump(System.Collections.Generic.IList`1<UnityEngine.Vector4>)
extern void ExtensionMethods_CTDump_mB00AF342A1C001008DDEE5422E6CE9386ADF044C (void);
// 0x00000018 System.Collections.Generic.List`1<System.String> Crosstales.ExtensionMethods::CTToString(System.Collections.Generic.IList`1<T>)
// 0x00000019 System.String Crosstales.ExtensionMethods::CTDump(System.Collections.Generic.IDictionary`2<K,V>,System.String,System.String)
// 0x0000001A System.Void Crosstales.ExtensionMethods::CTAddRange(System.Collections.Generic.IDictionary`2<K,V>,System.Collections.Generic.IDictionary`2<K,V>)
// 0x0000001B System.Boolean Crosstales.ExtensionMethods::CTIsVisibleFrom(UnityEngine.Renderer,UnityEngine.Camera)
extern void ExtensionMethods_CTIsVisibleFrom_m5E3DED552FD8B132DD4E2B700E4BC17BCE83E587 (void);
// 0x0000001C System.Void Crosstales.ExtensionMethods/<>c__DisplayClass5_0::.ctor()
extern void U3CU3Ec__DisplayClass5_0__ctor_m911B11C9D277BA8C81084E9C791C959F916AFA27 (void);
// 0x0000001D System.Boolean Crosstales.ExtensionMethods/<>c__DisplayClass5_0::<CTContainsAny>b__0(System.String)
extern void U3CU3Ec__DisplayClass5_0_U3CCTContainsAnyU3Eb__0_mE625ED60C205B0632CF345CF3D244BBFF5B5F8D9 (void);
// 0x0000001E System.Void Crosstales.ExtensionMethods/<>c__DisplayClass6_0::.ctor()
extern void U3CU3Ec__DisplayClass6_0__ctor_m2C47077153CBA7277CC4630ED5F134A341294672 (void);
// 0x0000001F System.Boolean Crosstales.ExtensionMethods/<>c__DisplayClass6_0::<CTContainsAll>b__0(System.String)
extern void U3CU3Ec__DisplayClass6_0_U3CCTContainsAllU3Eb__0_mE8356EE209DE1428FEDE8E17E777F9E74DE056E1 (void);
// 0x00000020 System.Void Crosstales.RTVoice.VoicesReady::.ctor(System.Object,System.IntPtr)
extern void VoicesReady__ctor_m2E991762A67890BED2F92668E47C5E6A1685717A (void);
// 0x00000021 System.Void Crosstales.RTVoice.VoicesReady::Invoke()
extern void VoicesReady_Invoke_mC7C30D92DB231AE7DDACEBBE4584FA7D6610473A (void);
// 0x00000022 System.IAsyncResult Crosstales.RTVoice.VoicesReady::BeginInvoke(System.AsyncCallback,System.Object)
extern void VoicesReady_BeginInvoke_m605F7A91294E4373FF460D5C40894C7074825EDB (void);
// 0x00000023 System.Void Crosstales.RTVoice.VoicesReady::EndInvoke(System.IAsyncResult)
extern void VoicesReady_EndInvoke_m620BDD074D099E8EB6C1AFE42309899D7F88FD7E (void);
// 0x00000024 System.Void Crosstales.RTVoice.SpeakStart::.ctor(System.Object,System.IntPtr)
extern void SpeakStart__ctor_mC8F6A8E90D4E862BA229DD36DACAD43205808A9E (void);
// 0x00000025 System.Void Crosstales.RTVoice.SpeakStart::Invoke(Crosstales.RTVoice.Model.Wrapper)
extern void SpeakStart_Invoke_mB10CD8E3D429DD704FA20B63A1003025CFDF96E2 (void);
// 0x00000026 System.IAsyncResult Crosstales.RTVoice.SpeakStart::BeginInvoke(Crosstales.RTVoice.Model.Wrapper,System.AsyncCallback,System.Object)
extern void SpeakStart_BeginInvoke_mB59CFD4F5B882803EBF4FA7FC8E129052FA78498 (void);
// 0x00000027 System.Void Crosstales.RTVoice.SpeakStart::EndInvoke(System.IAsyncResult)
extern void SpeakStart_EndInvoke_m899FDCB6642DC34EE9398A00E2639E3D53609B3B (void);
// 0x00000028 System.Void Crosstales.RTVoice.SpeakComplete::.ctor(System.Object,System.IntPtr)
extern void SpeakComplete__ctor_mDCCB0317BD968FBC32591D07D2EFFA5ADBA5BCAD (void);
// 0x00000029 System.Void Crosstales.RTVoice.SpeakComplete::Invoke(Crosstales.RTVoice.Model.Wrapper)
extern void SpeakComplete_Invoke_m18B15193051921168800A643411DCD93A4C67BA0 (void);
// 0x0000002A System.IAsyncResult Crosstales.RTVoice.SpeakComplete::BeginInvoke(Crosstales.RTVoice.Model.Wrapper,System.AsyncCallback,System.Object)
extern void SpeakComplete_BeginInvoke_m6A37E9B8DF0A67B0E2CF6DCAA029FCF6D45BCD38 (void);
// 0x0000002B System.Void Crosstales.RTVoice.SpeakComplete::EndInvoke(System.IAsyncResult)
extern void SpeakComplete_EndInvoke_m9B50412701438A9941C0AB534668C9E3245E6168 (void);
// 0x0000002C System.Void Crosstales.RTVoice.SpeakCurrentWord::.ctor(System.Object,System.IntPtr)
extern void SpeakCurrentWord__ctor_mD3DD6540F9FA5A64376CA92E5C7F680C13DC8EFD (void);
// 0x0000002D System.Void Crosstales.RTVoice.SpeakCurrentWord::Invoke(Crosstales.RTVoice.Model.Wrapper,System.String[],System.Int32)
extern void SpeakCurrentWord_Invoke_m817FE783819F82D5F8C082B87E8477E7F392B5E7 (void);
// 0x0000002E System.IAsyncResult Crosstales.RTVoice.SpeakCurrentWord::BeginInvoke(Crosstales.RTVoice.Model.Wrapper,System.String[],System.Int32,System.AsyncCallback,System.Object)
extern void SpeakCurrentWord_BeginInvoke_m68009DEE673D3146AB9901156B42D0607D8D4D1D (void);
// 0x0000002F System.Void Crosstales.RTVoice.SpeakCurrentWord::EndInvoke(System.IAsyncResult)
extern void SpeakCurrentWord_EndInvoke_m492D4D826F7C0E500A8AE6149DFDFB689DED0CDC (void);
// 0x00000030 System.Void Crosstales.RTVoice.SpeakCurrentPhoneme::.ctor(System.Object,System.IntPtr)
extern void SpeakCurrentPhoneme__ctor_m85F9B5C461F9E02B0B9C1A353104E79DCE742781 (void);
// 0x00000031 System.Void Crosstales.RTVoice.SpeakCurrentPhoneme::Invoke(Crosstales.RTVoice.Model.Wrapper,System.String)
extern void SpeakCurrentPhoneme_Invoke_m79BAED65CCE030F2EFD292C804F64DF7E6312FE2 (void);
// 0x00000032 System.IAsyncResult Crosstales.RTVoice.SpeakCurrentPhoneme::BeginInvoke(Crosstales.RTVoice.Model.Wrapper,System.String,System.AsyncCallback,System.Object)
extern void SpeakCurrentPhoneme_BeginInvoke_m6A311B6BECAAF898B684AE011535825B01CC21DF (void);
// 0x00000033 System.Void Crosstales.RTVoice.SpeakCurrentPhoneme::EndInvoke(System.IAsyncResult)
extern void SpeakCurrentPhoneme_EndInvoke_mD7D228667F2C57C9945E1B929DB644FC20090762 (void);
// 0x00000034 System.Void Crosstales.RTVoice.SpeakCurrentViseme::.ctor(System.Object,System.IntPtr)
extern void SpeakCurrentViseme__ctor_m2AB40082B61DC510D9E15E35A7B70F748D76C3C3 (void);
// 0x00000035 System.Void Crosstales.RTVoice.SpeakCurrentViseme::Invoke(Crosstales.RTVoice.Model.Wrapper,System.String)
extern void SpeakCurrentViseme_Invoke_m90CEC03BD48D4712D5E37D2204050DC01A1365D9 (void);
// 0x00000036 System.IAsyncResult Crosstales.RTVoice.SpeakCurrentViseme::BeginInvoke(Crosstales.RTVoice.Model.Wrapper,System.String,System.AsyncCallback,System.Object)
extern void SpeakCurrentViseme_BeginInvoke_mAC51100A7F16AA28BB5E19ADD9CD1F00053D945E (void);
// 0x00000037 System.Void Crosstales.RTVoice.SpeakCurrentViseme::EndInvoke(System.IAsyncResult)
extern void SpeakCurrentViseme_EndInvoke_m38D4EAF67498A1DED2B994BDA9059D1D97E7FBA7 (void);
// 0x00000038 System.Void Crosstales.RTVoice.SpeakAudioGenerationStart::.ctor(System.Object,System.IntPtr)
extern void SpeakAudioGenerationStart__ctor_m085B870E9D2BFA2A3290CF8E94B0EB28D4E49AE9 (void);
// 0x00000039 System.Void Crosstales.RTVoice.SpeakAudioGenerationStart::Invoke(Crosstales.RTVoice.Model.Wrapper)
extern void SpeakAudioGenerationStart_Invoke_m018E44BCD9A5BCCDB2B9119CCC5E74DDC762D25E (void);
// 0x0000003A System.IAsyncResult Crosstales.RTVoice.SpeakAudioGenerationStart::BeginInvoke(Crosstales.RTVoice.Model.Wrapper,System.AsyncCallback,System.Object)
extern void SpeakAudioGenerationStart_BeginInvoke_m04DD1924D0976BD1020681FA6915C9C74384579E (void);
// 0x0000003B System.Void Crosstales.RTVoice.SpeakAudioGenerationStart::EndInvoke(System.IAsyncResult)
extern void SpeakAudioGenerationStart_EndInvoke_m63E807138EF86A43E0E525414861C8598A5747E4 (void);
// 0x0000003C System.Void Crosstales.RTVoice.SpeakAudioGenerationComplete::.ctor(System.Object,System.IntPtr)
extern void SpeakAudioGenerationComplete__ctor_m04EDFA1D52E9C8E55A384FC8AEEEAC4D73FBA374 (void);
// 0x0000003D System.Void Crosstales.RTVoice.SpeakAudioGenerationComplete::Invoke(Crosstales.RTVoice.Model.Wrapper)
extern void SpeakAudioGenerationComplete_Invoke_mCC83091AC88B694C3FEC1B62317EA5B1606744C3 (void);
// 0x0000003E System.IAsyncResult Crosstales.RTVoice.SpeakAudioGenerationComplete::BeginInvoke(Crosstales.RTVoice.Model.Wrapper,System.AsyncCallback,System.Object)
extern void SpeakAudioGenerationComplete_BeginInvoke_mD6FC7D4EA11E01A14D4D8D2EC7C588F21AEC4BA4 (void);
// 0x0000003F System.Void Crosstales.RTVoice.SpeakAudioGenerationComplete::EndInvoke(System.IAsyncResult)
extern void SpeakAudioGenerationComplete_EndInvoke_m846AD37D9A02B868C92080A30A720A528FBAF73B (void);
// 0x00000040 System.Void Crosstales.RTVoice.ErrorInfo::.ctor(System.Object,System.IntPtr)
extern void ErrorInfo__ctor_m40D60E1C66F63A6B8EA380C57C0B64668F29FB27 (void);
// 0x00000041 System.Void Crosstales.RTVoice.ErrorInfo::Invoke(Crosstales.RTVoice.Model.Wrapper,System.String)
extern void ErrorInfo_Invoke_m37FF498EA4FB828E412308686E5B66D1DE67E672 (void);
// 0x00000042 System.IAsyncResult Crosstales.RTVoice.ErrorInfo::BeginInvoke(Crosstales.RTVoice.Model.Wrapper,System.String,System.AsyncCallback,System.Object)
extern void ErrorInfo_BeginInvoke_mB1E6E5DC2F13C65B3819D170BF2B2BE881277C68 (void);
// 0x00000043 System.Void Crosstales.RTVoice.ErrorInfo::EndInvoke(System.IAsyncResult)
extern void ErrorInfo_EndInvoke_m2FE5921B97DFC0B9C694E9E0D602C5CE04841EE3 (void);
// 0x00000044 System.Void Crosstales.RTVoice.ProviderChange::.ctor(System.Object,System.IntPtr)
extern void ProviderChange__ctor_mBFDDB20BDBA8A5FF726D10D88591C08E61352AEB (void);
// 0x00000045 System.Void Crosstales.RTVoice.ProviderChange::Invoke(System.String)
extern void ProviderChange_Invoke_mF672B1FE34C1B96CCDE0AFAF459731E8CC6D6707 (void);
// 0x00000046 System.IAsyncResult Crosstales.RTVoice.ProviderChange::BeginInvoke(System.String,System.AsyncCallback,System.Object)
extern void ProviderChange_BeginInvoke_mE4461E00CE7B104260E406EA09901CDA5D49B9E6 (void);
// 0x00000047 System.Void Crosstales.RTVoice.ProviderChange::EndInvoke(System.IAsyncResult)
extern void ProviderChange_EndInvoke_m51464E4AFFA4F04524B00B1E9C00E4144B60DBC6 (void);
// 0x00000048 System.Void Crosstales.RTVoice.AudioFileGeneratorStart::.ctor(System.Object,System.IntPtr)
extern void AudioFileGeneratorStart__ctor_m8866386226604F73C003CA113B7C500545B41CE0 (void);
// 0x00000049 System.Void Crosstales.RTVoice.AudioFileGeneratorStart::Invoke()
extern void AudioFileGeneratorStart_Invoke_mDB4A7D256D502CFC6BE0449053620D9DB8A7E2E4 (void);
// 0x0000004A System.IAsyncResult Crosstales.RTVoice.AudioFileGeneratorStart::BeginInvoke(System.AsyncCallback,System.Object)
extern void AudioFileGeneratorStart_BeginInvoke_mC0ED8EFF08BD87E536E3CA907904B3C81CD220F2 (void);
// 0x0000004B System.Void Crosstales.RTVoice.AudioFileGeneratorStart::EndInvoke(System.IAsyncResult)
extern void AudioFileGeneratorStart_EndInvoke_m2D75793424160400D75FE4336D5DF9B379012402 (void);
// 0x0000004C System.Void Crosstales.RTVoice.AudioFileGeneratorComplete::.ctor(System.Object,System.IntPtr)
extern void AudioFileGeneratorComplete__ctor_mE0F014F21CD6577372BD0F821D85D5B4A1E9DE56 (void);
// 0x0000004D System.Void Crosstales.RTVoice.AudioFileGeneratorComplete::Invoke()
extern void AudioFileGeneratorComplete_Invoke_m00432CD9175DFF6DFE75B4BE4035B17844CB8F15 (void);
// 0x0000004E System.IAsyncResult Crosstales.RTVoice.AudioFileGeneratorComplete::BeginInvoke(System.AsyncCallback,System.Object)
extern void AudioFileGeneratorComplete_BeginInvoke_mD8C5D0D621B661099483A65EDCA4D9FC1B70BE49 (void);
// 0x0000004F System.Void Crosstales.RTVoice.AudioFileGeneratorComplete::EndInvoke(System.IAsyncResult)
extern void AudioFileGeneratorComplete_EndInvoke_m20F29FE67FBC71F62E78CCC58A758A99F7777ED8 (void);
// 0x00000050 System.Void Crosstales.RTVoice.ParalanguageStart::.ctor(System.Object,System.IntPtr)
extern void ParalanguageStart__ctor_m059C70F86F595A3903C6B9B26A2F5E475786EA94 (void);
// 0x00000051 System.Void Crosstales.RTVoice.ParalanguageStart::Invoke()
extern void ParalanguageStart_Invoke_m6175485770ABB06D1C6E594B691EB9D71A6B7FD5 (void);
// 0x00000052 System.IAsyncResult Crosstales.RTVoice.ParalanguageStart::BeginInvoke(System.AsyncCallback,System.Object)
extern void ParalanguageStart_BeginInvoke_m807A5A04827C83C63A66A21B06E4EF42C5A3CEB7 (void);
// 0x00000053 System.Void Crosstales.RTVoice.ParalanguageStart::EndInvoke(System.IAsyncResult)
extern void ParalanguageStart_EndInvoke_m378719583F5D00D1FE89EBE2B709C17681608FAE (void);
// 0x00000054 System.Void Crosstales.RTVoice.ParalanguageComplete::.ctor(System.Object,System.IntPtr)
extern void ParalanguageComplete__ctor_mCB3784A6774C1CFB65E09A68A7AF7A344271F561 (void);
// 0x00000055 System.Void Crosstales.RTVoice.ParalanguageComplete::Invoke()
extern void ParalanguageComplete_Invoke_mD7881B18B3A8FBBEFD8EB0DC96807A0BF1390B71 (void);
// 0x00000056 System.IAsyncResult Crosstales.RTVoice.ParalanguageComplete::BeginInvoke(System.AsyncCallback,System.Object)
extern void ParalanguageComplete_BeginInvoke_m2BC5C4EB9BB43D55DB1E3E377C0BCE85D1CEBA84 (void);
// 0x00000057 System.Void Crosstales.RTVoice.ParalanguageComplete::EndInvoke(System.IAsyncResult)
extern void ParalanguageComplete_EndInvoke_m5C1C7843CFF25D4656CF2A3D23838A25B8989DF4 (void);
// 0x00000058 System.Void Crosstales.RTVoice.SpeechTextStart::.ctor(System.Object,System.IntPtr)
extern void SpeechTextStart__ctor_m2B4286BDE7221D08A8A7F0B7E38539902257EF94 (void);
// 0x00000059 System.Void Crosstales.RTVoice.SpeechTextStart::Invoke()
extern void SpeechTextStart_Invoke_m5F4640E1AF60CCA9AB0212FC00F0ECBF949AE30B (void);
// 0x0000005A System.IAsyncResult Crosstales.RTVoice.SpeechTextStart::BeginInvoke(System.AsyncCallback,System.Object)
extern void SpeechTextStart_BeginInvoke_m88A250F116921AC77BD12F24A360F26B7AEBC392 (void);
// 0x0000005B System.Void Crosstales.RTVoice.SpeechTextStart::EndInvoke(System.IAsyncResult)
extern void SpeechTextStart_EndInvoke_mF44611A887C90B4368F16A9E4A5BF91D0C6964CA (void);
// 0x0000005C System.Void Crosstales.RTVoice.SpeechTextComplete::.ctor(System.Object,System.IntPtr)
extern void SpeechTextComplete__ctor_mB086443FA20BA098F0A53E3858A80ADE3A1D5CCE (void);
// 0x0000005D System.Void Crosstales.RTVoice.SpeechTextComplete::Invoke()
extern void SpeechTextComplete_Invoke_m28FB11109EADC3828CE1A2EAF8408510DB0B14B7 (void);
// 0x0000005E System.IAsyncResult Crosstales.RTVoice.SpeechTextComplete::BeginInvoke(System.AsyncCallback,System.Object)
extern void SpeechTextComplete_BeginInvoke_mAC7A21F2BBD455F444D88A7682BC111DBFB17179 (void);
// 0x0000005F System.Void Crosstales.RTVoice.SpeechTextComplete::EndInvoke(System.IAsyncResult)
extern void SpeechTextComplete_EndInvoke_mF35924B3725393FC4AAF6CDEF3A5C487CCC78A7E (void);
// 0x00000060 System.Void Crosstales.RTVoice.LiveSpeaker::SpeakNative(Crosstales.RTVoice.Model.Wrapper)
extern void LiveSpeaker_SpeakNative_m26C65EC54901B02AD269145B611FBD2C77B2C98B (void);
// 0x00000061 System.Void Crosstales.RTVoice.LiveSpeaker::SpeakNative(System.String)
extern void LiveSpeaker_SpeakNative_m1325133C367E141BA3BD51E6313FA62CAE2143B5 (void);
// 0x00000062 System.Void Crosstales.RTVoice.LiveSpeaker::SpeakNative(System.String[])
extern void LiveSpeaker_SpeakNative_mFA2E8646EE171F27AA70EB6A4B9BACD104A3B42E (void);
// 0x00000063 System.Void Crosstales.RTVoice.LiveSpeaker::Speak(Crosstales.RTVoice.Model.Wrapper)
extern void LiveSpeaker_Speak_mDE0121CBFD016B74DF2251276A02C2841A4651D1 (void);
// 0x00000064 System.Void Crosstales.RTVoice.LiveSpeaker::Speak(System.String)
extern void LiveSpeaker_Speak_mD9F352E1859BD25DA34CEC12B2D8F2636C0FF780 (void);
// 0x00000065 System.Void Crosstales.RTVoice.LiveSpeaker::Speak(System.String[])
extern void LiveSpeaker_Speak_mC3C7A08E0B9A3FAAAC311214A1B4B0CEA9EACA77 (void);
// 0x00000066 System.Void Crosstales.RTVoice.LiveSpeaker::Silence()
extern void LiveSpeaker_Silence_m4FCD3587003FC2DD23DF5548115EFEEB034E49E9 (void);
// 0x00000067 System.Void Crosstales.RTVoice.LiveSpeaker::SetVoices(System.String)
extern void LiveSpeaker_SetVoices_m8CC34539AA96CD8B5FE150B7D97B9D2C241E2CCE (void);
// 0x00000068 System.Void Crosstales.RTVoice.LiveSpeaker::WordSpoken(System.String)
extern void LiveSpeaker_WordSpoken_m26AB98FE814293A57D4745254EA2B0C0227953AF (void);
// 0x00000069 System.Void Crosstales.RTVoice.LiveSpeaker::SetState(System.String)
extern void LiveSpeaker_SetState_mA42E6FFB85C643B0849D24064BC120BA64F8CC81 (void);
// 0x0000006A System.Void Crosstales.RTVoice.LiveSpeaker::.ctor()
extern void LiveSpeaker__ctor_m549443194385E6C25A7D32945D3C2CC0458C9A86 (void);
// 0x0000006B System.Void Crosstales.RTVoice.LiveSpeaker::.cctor()
extern void LiveSpeaker__cctor_m4E86C8E8D7A8D5402563E30271545F2EE87C597E (void);
// 0x0000006C System.Void Crosstales.RTVoice.Speaker::add_OnVoicesReady(Crosstales.RTVoice.VoicesReady)
extern void Speaker_add_OnVoicesReady_m2C6536AC1C4E0D45B46F62320A5228D7703A977C (void);
// 0x0000006D System.Void Crosstales.RTVoice.Speaker::remove_OnVoicesReady(Crosstales.RTVoice.VoicesReady)
extern void Speaker_remove_OnVoicesReady_m7419320D3E1D51E3A1947C7D47BA952B15E13CAE (void);
// 0x0000006E System.Void Crosstales.RTVoice.Speaker::add_OnSpeakStart(Crosstales.RTVoice.SpeakStart)
extern void Speaker_add_OnSpeakStart_mB23F67B6DCDB1E55BDFB9EDE7AF3D595FCA553AE (void);
// 0x0000006F System.Void Crosstales.RTVoice.Speaker::remove_OnSpeakStart(Crosstales.RTVoice.SpeakStart)
extern void Speaker_remove_OnSpeakStart_m2C2E372ECFA4539BBBB6E60CE531684E9A526585 (void);
// 0x00000070 System.Void Crosstales.RTVoice.Speaker::add_OnSpeakComplete(Crosstales.RTVoice.SpeakComplete)
extern void Speaker_add_OnSpeakComplete_mB08AF256448A5039092006149E7F2E696C158FCD (void);
// 0x00000071 System.Void Crosstales.RTVoice.Speaker::remove_OnSpeakComplete(Crosstales.RTVoice.SpeakComplete)
extern void Speaker_remove_OnSpeakComplete_mB80E093C3FF9490783115A76BE7265D8F903497D (void);
// 0x00000072 System.Void Crosstales.RTVoice.Speaker::add_OnSpeakCurrentWord(Crosstales.RTVoice.SpeakCurrentWord)
extern void Speaker_add_OnSpeakCurrentWord_m2F8106FFAF9C5919D034F136D36EBA9AA73132F4 (void);
// 0x00000073 System.Void Crosstales.RTVoice.Speaker::remove_OnSpeakCurrentWord(Crosstales.RTVoice.SpeakCurrentWord)
extern void Speaker_remove_OnSpeakCurrentWord_mA5977059F97F2BA6CDB22C7B7DC6C88B7ADE5B6D (void);
// 0x00000074 System.Void Crosstales.RTVoice.Speaker::add_OnSpeakCurrentPhoneme(Crosstales.RTVoice.SpeakCurrentPhoneme)
extern void Speaker_add_OnSpeakCurrentPhoneme_m859ACD2CCD8B863B19E0EB0B2AA7365342D6A75A (void);
// 0x00000075 System.Void Crosstales.RTVoice.Speaker::remove_OnSpeakCurrentPhoneme(Crosstales.RTVoice.SpeakCurrentPhoneme)
extern void Speaker_remove_OnSpeakCurrentPhoneme_m72F6687DBF8F03FBABE61C4F7AD36FDB97F2C4D0 (void);
// 0x00000076 System.Void Crosstales.RTVoice.Speaker::add_OnSpeakCurrentViseme(Crosstales.RTVoice.SpeakCurrentViseme)
extern void Speaker_add_OnSpeakCurrentViseme_mCA6DFF18B8643C22207A12C58B4404CA897971EE (void);
// 0x00000077 System.Void Crosstales.RTVoice.Speaker::remove_OnSpeakCurrentViseme(Crosstales.RTVoice.SpeakCurrentViseme)
extern void Speaker_remove_OnSpeakCurrentViseme_m320534421F19BA06CE5ABE7AB3584C3C314AD38D (void);
// 0x00000078 System.Void Crosstales.RTVoice.Speaker::add_OnSpeakAudioGenerationStart(Crosstales.RTVoice.SpeakAudioGenerationStart)
extern void Speaker_add_OnSpeakAudioGenerationStart_m1AFD632C57F53D96D77A72F8D1A2AD747F1A0E87 (void);
// 0x00000079 System.Void Crosstales.RTVoice.Speaker::remove_OnSpeakAudioGenerationStart(Crosstales.RTVoice.SpeakAudioGenerationStart)
extern void Speaker_remove_OnSpeakAudioGenerationStart_m2542F624DA998E6D3008ED6C3B57A87D3DEC2C6C (void);
// 0x0000007A System.Void Crosstales.RTVoice.Speaker::add_OnSpeakAudioGenerationComplete(Crosstales.RTVoice.SpeakAudioGenerationComplete)
extern void Speaker_add_OnSpeakAudioGenerationComplete_m9998D72E8FBA1D6BFA226BF38766E54EBCF4F819 (void);
// 0x0000007B System.Void Crosstales.RTVoice.Speaker::remove_OnSpeakAudioGenerationComplete(Crosstales.RTVoice.SpeakAudioGenerationComplete)
extern void Speaker_remove_OnSpeakAudioGenerationComplete_m16495ADEF5818DBF2F83539A414CCE82E848BB89 (void);
// 0x0000007C System.Void Crosstales.RTVoice.Speaker::add_OnProviderChange(Crosstales.RTVoice.ProviderChange)
extern void Speaker_add_OnProviderChange_mD7F71DAE9793CAB6D3F1BCE44F5E7849ED0E4C6B (void);
// 0x0000007D System.Void Crosstales.RTVoice.Speaker::remove_OnProviderChange(Crosstales.RTVoice.ProviderChange)
extern void Speaker_remove_OnProviderChange_m82DCFCC8E4B2C7FAAB68EA9528A94CA9883D13D6 (void);
// 0x0000007E System.Void Crosstales.RTVoice.Speaker::add_OnErrorInfo(Crosstales.RTVoice.ErrorInfo)
extern void Speaker_add_OnErrorInfo_m98EE2D6190853E7F161B5B972FD774474E3BBE01 (void);
// 0x0000007F System.Void Crosstales.RTVoice.Speaker::remove_OnErrorInfo(Crosstales.RTVoice.ErrorInfo)
extern void Speaker_remove_OnErrorInfo_m34726E329BD8EB057FB1AAB19F1B756A88D5CAE5 (void);
// 0x00000080 System.Int32 Crosstales.RTVoice.Speaker::get_SpeechCount()
extern void Speaker_get_SpeechCount_mF7EA7146F93FB4134ADB9F1BC92CBA8C1E97872A (void);
// 0x00000081 System.Void Crosstales.RTVoice.Speaker::set_SpeechCount(System.Int32)
extern void Speaker_set_SpeechCount_m7403028ACC1F1F42274B7D8066020B24F1F07F11 (void);
// 0x00000082 System.Int32 Crosstales.RTVoice.Speaker::get_BusyCount()
extern void Speaker_get_BusyCount_m9C5E0DFCDBAA19CAC6C4783AFEABB682C409D083 (void);
// 0x00000083 System.Void Crosstales.RTVoice.Speaker::set_BusyCount(System.Int32)
extern void Speaker_set_BusyCount_m6D4AE1BB35A1C8CE8F61041E02865839F3E058BC (void);
// 0x00000084 System.Boolean Crosstales.RTVoice.Speaker::get_areVoicesReady()
extern void Speaker_get_areVoicesReady_m55042C89D6ABB754FF15AC1E880F6FAC8CE6C318 (void);
// 0x00000085 System.Void Crosstales.RTVoice.Speaker::set_areVoicesReady(System.Boolean)
extern void Speaker_set_areVoicesReady_m2F79E5855BD2C1E6BC59F3A29C7972FAB5C49DE7 (void);
// 0x00000086 Crosstales.RTVoice.Provider.BaseCustomVoiceProvider Crosstales.RTVoice.Speaker::get_CustomVoiceProvider()
extern void Speaker_get_CustomVoiceProvider_m8917B7A828D726AB9AFAB2C525CB7DADC7915AD9 (void);
// 0x00000087 System.Void Crosstales.RTVoice.Speaker::set_CustomVoiceProvider(Crosstales.RTVoice.Provider.BaseCustomVoiceProvider)
extern void Speaker_set_CustomVoiceProvider_mC27D0B7C08B9B6C399CCA2DD066D9112FBAD01F8 (void);
// 0x00000088 System.Boolean Crosstales.RTVoice.Speaker::get_isCustomMode()
extern void Speaker_get_isCustomMode_mE1B205DE1900F784556098EE85F428A443ADCBF1 (void);
// 0x00000089 System.Void Crosstales.RTVoice.Speaker::set_isCustomMode(System.Boolean)
extern void Speaker_set_isCustomMode_mE8DF7DEF5E17DF5748D3756E76C586DA31E70846 (void);
// 0x0000008A System.Boolean Crosstales.RTVoice.Speaker::get_isMaryMode()
extern void Speaker_get_isMaryMode_m5411FB3E9579589AFB66258B778C52A60C227E2B (void);
// 0x0000008B System.Void Crosstales.RTVoice.Speaker::set_isMaryMode(System.Boolean)
extern void Speaker_set_isMaryMode_m3105879511912234EE6B3C2EF889B8AC31FC1293 (void);
// 0x0000008C System.String Crosstales.RTVoice.Speaker::get_MaryUrl()
extern void Speaker_get_MaryUrl_m462763EB8DA96154B3F2C505C20E2B07BC12A73C (void);
// 0x0000008D System.Void Crosstales.RTVoice.Speaker::set_MaryUrl(System.String)
extern void Speaker_set_MaryUrl_mDA17CFFFF74ADD31ACF11218564A218FC1867277 (void);
// 0x0000008E System.Int32 Crosstales.RTVoice.Speaker::get_MaryPort()
extern void Speaker_get_MaryPort_m3C4D4CC2E4117B49E99931D711A6EC4AFFEB2352 (void);
// 0x0000008F System.Void Crosstales.RTVoice.Speaker::set_MaryPort(System.Int32)
extern void Speaker_set_MaryPort_m2AC97A8B3B3F8B3C8C86D9D7DF28A06DD6E4CCA4 (void);
// 0x00000090 System.String Crosstales.RTVoice.Speaker::get_MaryUser()
extern void Speaker_get_MaryUser_mF4DC7DCF158E813EF982B0DA73F68DD4E05E4643 (void);
// 0x00000091 System.Void Crosstales.RTVoice.Speaker::set_MaryUser(System.String)
extern void Speaker_set_MaryUser_mB01336A49FF20D601731F37E981E7D6212A23DE4 (void);
// 0x00000092 System.String Crosstales.RTVoice.Speaker::get_MaryPassword()
extern void Speaker_get_MaryPassword_mB423A30B194FFFF4FF12BF77B34D570E0236D082 (void);
// 0x00000093 System.Void Crosstales.RTVoice.Speaker::set_MaryPassword(System.String)
extern void Speaker_set_MaryPassword_mD6FC21E08ABBD4A4F88650C7A7526B0C6B51AB64 (void);
// 0x00000094 Crosstales.RTVoice.Model.Enum.MaryTTSType Crosstales.RTVoice.Speaker::get_MaryType()
extern void Speaker_get_MaryType_mFEAF3F5C79DD2BDA6C9264ADA009C504C09E74EE (void);
// 0x00000095 System.Void Crosstales.RTVoice.Speaker::set_MaryType(Crosstales.RTVoice.Model.Enum.MaryTTSType)
extern void Speaker_set_MaryType_mDA3D089DC92CECD4B0D66A5526AD27D1EDA29359 (void);
// 0x00000096 System.Boolean Crosstales.RTVoice.Speaker::get_isESpeakMode()
extern void Speaker_get_isESpeakMode_m472E561526F0DEBDD1E4CD432EE55F95EDDC7DBE (void);
// 0x00000097 System.Void Crosstales.RTVoice.Speaker::set_isESpeakMode(System.Boolean)
extern void Speaker_set_isESpeakMode_mDBD0268FC62F7334EA7A2FDD64EAA2D3D0303C04 (void);
// 0x00000098 Crosstales.RTVoice.Model.Enum.ESpeakModifiers Crosstales.RTVoice.Speaker::get_ESpeakMod()
extern void Speaker_get_ESpeakMod_m43C00A86F33FF91C54E08F83114797791B5CA880 (void);
// 0x00000099 System.Void Crosstales.RTVoice.Speaker::set_ESpeakMod(Crosstales.RTVoice.Model.Enum.ESpeakModifiers)
extern void Speaker_set_ESpeakMod_m174D2521A804215769E6F42A1FC278B78BD79EE1 (void);
// 0x0000009A System.Boolean Crosstales.RTVoice.Speaker::get_isWSANative()
extern void Speaker_get_isWSANative_mA4881F7111959D1EAD40EA42573937A06CA19110 (void);
// 0x0000009B System.Void Crosstales.RTVoice.Speaker::set_isWSANative(System.Boolean)
extern void Speaker_set_isWSANative_m4ADD170835113217FD8C02C357B8C5B56566EBF0 (void);
// 0x0000009C System.Boolean Crosstales.RTVoice.Speaker::get_isAutoClearTags()
extern void Speaker_get_isAutoClearTags_m5F6247D56A775D14D2DA15A36577F45EE2DDB75A (void);
// 0x0000009D System.Void Crosstales.RTVoice.Speaker::set_isAutoClearTags(System.Boolean)
extern void Speaker_set_isAutoClearTags_mEF89AA8D1A868F6039C254E6ABE5D509D96E0198 (void);
// 0x0000009E System.Boolean Crosstales.RTVoice.Speaker::get_isSilenceOnDisable()
extern void Speaker_get_isSilenceOnDisable_m7A06583380626B6F910F35D87DAD9EB06D26E453 (void);
// 0x0000009F System.Void Crosstales.RTVoice.Speaker::set_isSilenceOnDisable(System.Boolean)
extern void Speaker_set_isSilenceOnDisable_m944D119AD39924FBB54CA3E634A28253029ABDDC (void);
// 0x000000A0 System.Boolean Crosstales.RTVoice.Speaker::get_isSilenceOnFocustLost()
extern void Speaker_get_isSilenceOnFocustLost_m1C26DB3EF729A695F16DB3925A2057052985553B (void);
// 0x000000A1 System.Void Crosstales.RTVoice.Speaker::set_isSilenceOnFocustLost(System.Boolean)
extern void Speaker_set_isSilenceOnFocustLost_mC6EB5BF043580B22037A7E60887132E6DFACBB6E (void);
// 0x000000A2 System.Boolean Crosstales.RTVoice.Speaker::get_isTTSAvailable()
extern void Speaker_get_isTTSAvailable_mABB6E77CAF5AD9BCFC93186AFFF22AF454EF1065 (void);
// 0x000000A3 System.Boolean Crosstales.RTVoice.Speaker::get_isSpeaking()
extern void Speaker_get_isSpeaking_m451DF855F5FA987EB047E5BF15326C68EDEA5BCE (void);
// 0x000000A4 System.Boolean Crosstales.RTVoice.Speaker::get_isBusy()
extern void Speaker_get_isBusy_m1737B40C42A247CDFD42F313A321EB822E6C780D (void);
// 0x000000A5 System.Boolean Crosstales.RTVoice.Speaker::get_enforcedStandaloneTTS()
extern void Speaker_get_enforcedStandaloneTTS_m2B69DCFD8736769BE2716D304570F88D3F915D6B (void);
// 0x000000A6 System.Void Crosstales.RTVoice.Speaker::set_enforcedStandaloneTTS(System.Boolean)
extern void Speaker_set_enforcedStandaloneTTS_m8E0627900FA976E4F96CE064B22F6A19A1952DC6 (void);
// 0x000000A7 System.String Crosstales.RTVoice.Speaker::get_AudioFileExtension()
extern void Speaker_get_AudioFileExtension_mBF7A5F96E05921A6AA38548ED0FE5370E3D98302 (void);
// 0x000000A8 System.String Crosstales.RTVoice.Speaker::get_DefaultVoiceName()
extern void Speaker_get_DefaultVoiceName_m7403EA51852911F8951A3A6499EBB0F3434B36FC (void);
// 0x000000A9 System.Collections.Generic.List`1<Crosstales.RTVoice.Model.Voice> Crosstales.RTVoice.Speaker::get_Voices()
extern void Speaker_get_Voices_m79A2558BCEAF57EA7ED84768134D3EAFADEE4BFA (void);
// 0x000000AA System.Boolean Crosstales.RTVoice.Speaker::get_isWorkingInEditor()
extern void Speaker_get_isWorkingInEditor_mC146C7E042924073ABFEB1AF2A36669BAE203CEB (void);
// 0x000000AB System.Boolean Crosstales.RTVoice.Speaker::get_isWorkingInPlaymode()
extern void Speaker_get_isWorkingInPlaymode_m3B676C4F915CC220A82DC65213F36C9FB7B56748 (void);
// 0x000000AC System.Int32 Crosstales.RTVoice.Speaker::get_MaxTextLength()
extern void Speaker_get_MaxTextLength_mEF99D1D6EE86E97621779F9A34EA962657D2B34B (void);
// 0x000000AD System.Boolean Crosstales.RTVoice.Speaker::get_isSpeakNativeSupported()
extern void Speaker_get_isSpeakNativeSupported_mFC9D681F3A0B92EE6345F290554C05976653849E (void);
// 0x000000AE System.Boolean Crosstales.RTVoice.Speaker::get_isSpeakSupported()
extern void Speaker_get_isSpeakSupported_m288F80D10364982BDAE95DFFF6A828B4B14E0A24 (void);
// 0x000000AF System.Boolean Crosstales.RTVoice.Speaker::get_isPlatformSupported()
extern void Speaker_get_isPlatformSupported_mE0E9444A04687B754FCF743FE4ABF6FAC728A491 (void);
// 0x000000B0 System.Boolean Crosstales.RTVoice.Speaker::get_isSSMLSupported()
extern void Speaker_get_isSSMLSupported_mBB01EBFE8FC7C857D26BB48EC9FCE46CA1B6A840 (void);
// 0x000000B1 System.Boolean Crosstales.RTVoice.Speaker::get_isOnlineService()
extern void Speaker_get_isOnlineService_mD92FBCCE21144D3FC4CBA486E8807B0FD0B892FA (void);
// 0x000000B2 System.Boolean Crosstales.RTVoice.Speaker::get_hasCoRoutines()
extern void Speaker_get_hasCoRoutines_mC6184BFBE8F58505A13DDCB35B12B9884B2B22FF (void);
// 0x000000B3 System.Boolean Crosstales.RTVoice.Speaker::get_isIL2CPPSupported()
extern void Speaker_get_isIL2CPPSupported_mD5ACC31884F707892395AA0CAF68E540B4E7F70B (void);
// 0x000000B4 System.Collections.Generic.List`1<System.String> Crosstales.RTVoice.Speaker::get_Cultures()
extern void Speaker_get_Cultures_mCB38D08C1CB0E583F0318994A2F5328FDF1F8157 (void);
// 0x000000B5 System.Void Crosstales.RTVoice.Speaker::OnEnable()
extern void Speaker_OnEnable_m5889EC83209687C5F6588EA93170781700F7CFDB (void);
// 0x000000B6 System.Void Crosstales.RTVoice.Speaker::Update()
extern void Speaker_Update_mAA944A1246793F8F129520FE9C20BC14E2A1C121 (void);
// 0x000000B7 System.Void Crosstales.RTVoice.Speaker::OnDisable()
extern void Speaker_OnDisable_mFD34FF33EB4CA156039D4113C139B863CEBE20E8 (void);
// 0x000000B8 System.Void Crosstales.RTVoice.Speaker::OnApplicationQuit()
extern void Speaker_OnApplicationQuit_m63BFA73BE06ADF786E75BDFD85E90708C62CF44B (void);
// 0x000000B9 System.Void Crosstales.RTVoice.Speaker::OnApplicationFocus(System.Boolean)
extern void Speaker_OnApplicationFocus_m587F812882AED2A945E029DF95FDF93541BF69B3 (void);
// 0x000000BA System.Void Crosstales.RTVoice.Speaker::Reset()
extern void Speaker_Reset_m9A44736DAF32EE4F2B34A726B68AD6F416694F64 (void);
// 0x000000BB System.Single Crosstales.RTVoice.Speaker::ApproximateSpeechLength(System.String,System.Single,System.Single,System.Single)
extern void Speaker_ApproximateSpeechLength_m412C280EC1AAA4C44A4F5376525A10C601544B3B (void);
// 0x000000BC System.Boolean Crosstales.RTVoice.Speaker::isVoiceForGenderAvailable(Crosstales.RTVoice.Model.Enum.Gender,System.String)
extern void Speaker_isVoiceForGenderAvailable_mC37F524A619127C6C7692A6328E56D32ECC2F18E (void);
// 0x000000BD System.Collections.Generic.List`1<Crosstales.RTVoice.Model.Voice> Crosstales.RTVoice.Speaker::VoicesForGender(Crosstales.RTVoice.Model.Enum.Gender,System.String,System.Boolean)
extern void Speaker_VoicesForGender_m77B1EB3F245D2269446CE505C06664AE40293E4A (void);
// 0x000000BE Crosstales.RTVoice.Model.Voice Crosstales.RTVoice.Speaker::VoiceForGender(Crosstales.RTVoice.Model.Enum.Gender,System.String,System.Int32,System.String,System.Boolean)
extern void Speaker_VoiceForGender_mEE325533209C4A18547DE5F759DD95B847C31425 (void);
// 0x000000BF System.Boolean Crosstales.RTVoice.Speaker::isVoiceForCultureAvailable(System.String)
extern void Speaker_isVoiceForCultureAvailable_mFE795122FDE7094086837D9B324935C1AB8C891D (void);
// 0x000000C0 System.Collections.Generic.List`1<Crosstales.RTVoice.Model.Voice> Crosstales.RTVoice.Speaker::VoicesForCulture(System.String,System.Boolean)
extern void Speaker_VoicesForCulture_mC8E2CDEC9816223B1993B4C1A41143FBDEEB3EA0 (void);
// 0x000000C1 Crosstales.RTVoice.Model.Voice Crosstales.RTVoice.Speaker::VoiceForCulture(System.String,System.Int32,System.String,System.Boolean)
extern void Speaker_VoiceForCulture_m4DC4FFF248365A3647FBB1F21092A3BB6734B0FF (void);
// 0x000000C2 System.Boolean Crosstales.RTVoice.Speaker::isVoiceForNameAvailable(System.String,System.Boolean)
extern void Speaker_isVoiceForNameAvailable_m341CE2EAC3EAF53E49913119E8895352335D7334 (void);
// 0x000000C3 Crosstales.RTVoice.Model.Voice Crosstales.RTVoice.Speaker::VoiceForName(System.String,System.Boolean)
extern void Speaker_VoiceForName_m2EE14B5AE7D4E71D9D752B29D9FAA48E4962FC1C (void);
// 0x000000C4 System.String Crosstales.RTVoice.Speaker::SpeakNative(System.String,Crosstales.RTVoice.Model.Voice,System.Single,System.Single,System.Single,System.Boolean)
extern void Speaker_SpeakNative_m657220DB6F4C77A39206BA1E59D5DAAB83F01B74 (void);
// 0x000000C5 System.Void Crosstales.RTVoice.Speaker::SpeakNativeWithUID(Crosstales.RTVoice.Model.Wrapper)
extern void Speaker_SpeakNativeWithUID_m57589F7B8E625E31354F72888667A374823F63C4 (void);
// 0x000000C6 System.String Crosstales.RTVoice.Speaker::SpeakNative(Crosstales.RTVoice.Model.Wrapper)
extern void Speaker_SpeakNative_m5755877CAC5D3A7BB9CE12089690A48E741DDD20 (void);
// 0x000000C7 System.String Crosstales.RTVoice.Speaker::Speak(System.String,UnityEngine.AudioSource,Crosstales.RTVoice.Model.Voice,System.Boolean,System.Single,System.Single,System.Single,System.String,System.Boolean)
extern void Speaker_Speak_mDA3F118104E867A3C8F0C4490B48202F7902862C (void);
// 0x000000C8 System.Void Crosstales.RTVoice.Speaker::SpeakWithUID(Crosstales.RTVoice.Model.Wrapper)
extern void Speaker_SpeakWithUID_m8FD56871025B35320A9E13FAF7C909455EEB82B8 (void);
// 0x000000C9 System.String Crosstales.RTVoice.Speaker::Speak(Crosstales.RTVoice.Model.Wrapper)
extern void Speaker_Speak_m285139B02BE8D2669A1B38674EA0908CAB01133D (void);
// 0x000000CA System.Void Crosstales.RTVoice.Speaker::SpeakMarkedWordsWithUID(Crosstales.RTVoice.Model.Wrapper)
extern void Speaker_SpeakMarkedWordsWithUID_m42D29C28FA8F6E165D6E9A3568031D48D7D790BF (void);
// 0x000000CB System.Void Crosstales.RTVoice.Speaker::SpeakMarkedWordsWithUID(System.String,System.String,UnityEngine.AudioSource,Crosstales.RTVoice.Model.Voice,System.Single,System.Single,System.Boolean)
extern void Speaker_SpeakMarkedWordsWithUID_mB1725D1B6A33404F3DCDDC1692FFF76532987869 (void);
// 0x000000CC System.String Crosstales.RTVoice.Speaker::Generate(Crosstales.RTVoice.Model.Wrapper)
extern void Speaker_Generate_mC88CA0FAC1AA18A02681EBB6A942EC1153A0BB0B (void);
// 0x000000CD System.String Crosstales.RTVoice.Speaker::Generate(System.String,System.String,Crosstales.RTVoice.Model.Voice,System.Single,System.Single,System.Single,System.Boolean)
extern void Speaker_Generate_m2E0A8444D8F7FA2E89E8C12F715D38E9B437B70D (void);
// 0x000000CE System.Void Crosstales.RTVoice.Speaker::Silence()
extern void Speaker_Silence_m392C0C7A809450CFF342688A3B39BBA3831C843B (void);
// 0x000000CF System.Void Crosstales.RTVoice.Speaker::Silence(System.String)
extern void Speaker_Silence_m82C02F06AB135EB475EB2FCE7C15DBD12FD397D5 (void);
// 0x000000D0 System.Void Crosstales.RTVoice.Speaker::Pause(System.String)
extern void Speaker_Pause_m21C57142E8204AEE249B5E9DB9566848B6F7237B (void);
// 0x000000D1 System.Void Crosstales.RTVoice.Speaker::UnPause(System.String)
extern void Speaker_UnPause_m7844F76783BAE24C4E03D54B71F907713C70D363 (void);
// 0x000000D2 System.Void Crosstales.RTVoice.Speaker::ReloadProvider()
extern void Speaker_ReloadProvider_m119480D6BE9D9888C07410815C356BE4195A3818 (void);
// 0x000000D3 System.Void Crosstales.RTVoice.Speaker::DeleteAudioFiles()
extern void Speaker_DeleteAudioFiles_m4A14E58B2A2E8E080D5BB9A98FEC240A04188B70 (void);
// 0x000000D4 System.Void Crosstales.RTVoice.Speaker::deleteAudioFiles(System.String)
extern void Speaker_deleteAudioFiles_mE64F007ADDD2532C111CC9A56E01D92538C0EB0E (void);
// 0x000000D5 System.Void Crosstales.RTVoice.Speaker::initProvider()
extern void Speaker_initProvider_mCD737B611326BBA700740671C45C3689F78D40F8 (void);
// 0x000000D6 System.Void Crosstales.RTVoice.Speaker::initOSProvider()
extern void Speaker_initOSProvider_m27596A06A6889FDAD58587C8D2B3E7B800D2FF90 (void);
// 0x000000D7 System.Void Crosstales.RTVoice.Speaker::logWrapperIsNull()
extern void Speaker_logWrapperIsNull_m2CCFDDFE3899FA40CD7303C310A0317144317A16 (void);
// 0x000000D8 System.Void Crosstales.RTVoice.Speaker::logVPIsNull()
extern void Speaker_logVPIsNull_mCC3E11A1D9D1FE85294F2A35BFDA2A6BA4E65794 (void);
// 0x000000D9 System.Void Crosstales.RTVoice.Speaker::subscribeCustomEvents()
extern void Speaker_subscribeCustomEvents_mC4F65314DB36E65BFE827E55F82B55E32C2F9A44 (void);
// 0x000000DA System.Void Crosstales.RTVoice.Speaker::unsubscribeCustomEvents()
extern void Speaker_unsubscribeCustomEvents_mC6E53C1FED6A86B7928B15DD66E7485540FE35E1 (void);
// 0x000000DB System.Void Crosstales.RTVoice.Speaker::onVoicesReady()
extern void Speaker_onVoicesReady_mB9EE70544580BC798D052AA72829BC832E4F351C (void);
// 0x000000DC System.Void Crosstales.RTVoice.Speaker::onSpeakStart(Crosstales.RTVoice.Model.Wrapper)
extern void Speaker_onSpeakStart_mC2EFB288F33E5AF29194FB13DDFD34D25B8D66C9 (void);
// 0x000000DD System.Void Crosstales.RTVoice.Speaker::onSpeakComplete(Crosstales.RTVoice.Model.Wrapper)
extern void Speaker_onSpeakComplete_mA1FBF18B88A6296C94EEA6E2A8B04F80F2995684 (void);
// 0x000000DE System.Void Crosstales.RTVoice.Speaker::onSpeakCurrentWord(Crosstales.RTVoice.Model.Wrapper,System.String[],System.Int32)
extern void Speaker_onSpeakCurrentWord_mF5DE580865CD75E1CF0334E2EB2EA387F47E1FB4 (void);
// 0x000000DF System.Void Crosstales.RTVoice.Speaker::onSpeakCurrentPhoneme(Crosstales.RTVoice.Model.Wrapper,System.String)
extern void Speaker_onSpeakCurrentPhoneme_m8396CA7663C128B5BDAEAC2B2CE9F0ABA9A52193 (void);
// 0x000000E0 System.Void Crosstales.RTVoice.Speaker::onSpeakCurrentViseme(Crosstales.RTVoice.Model.Wrapper,System.String)
extern void Speaker_onSpeakCurrentViseme_m7AA2B1DF18C2F3B9FC1D721E1083D9AF942CF69C (void);
// 0x000000E1 System.Void Crosstales.RTVoice.Speaker::onSpeakAudioGenerationStart(Crosstales.RTVoice.Model.Wrapper)
extern void Speaker_onSpeakAudioGenerationStart_m4F80EE307EED26BF88B3AD3E36BA2CED8E662803 (void);
// 0x000000E2 System.Void Crosstales.RTVoice.Speaker::onSpeakAudioGenerationComplete(Crosstales.RTVoice.Model.Wrapper)
extern void Speaker_onSpeakAudioGenerationComplete_m8EB5C99D1002633FF2FA8C240C19E9F65E0E9F3E (void);
// 0x000000E3 System.Void Crosstales.RTVoice.Speaker::onErrorInfo(Crosstales.RTVoice.Model.Wrapper,System.String)
extern void Speaker_onErrorInfo_m5A161DC6416AC4BAEEA9E3B8D7BA75FBE6C96A11 (void);
// 0x000000E4 System.Void Crosstales.RTVoice.Speaker::.ctor()
extern void Speaker__ctor_mD33A4862B594ABE4453A8C547CEB97E2AAB44D79 (void);
// 0x000000E5 System.Void Crosstales.RTVoice.Speaker::.cctor()
extern void Speaker__cctor_m8BA554664C3BF38906C587BD65F3CC77A7289D40 (void);
// 0x000000E6 System.Void Crosstales.RTVoice.Speaker/<>c__DisplayClass171_0::.ctor()
extern void U3CU3Ec__DisplayClass171_0__ctor_mFD641F7697739DCC03DF7BB1679DD9909336BAFE (void);
// 0x000000E7 System.Boolean Crosstales.RTVoice.Speaker/<>c__DisplayClass171_0::<VoicesForCulture>b__0(Crosstales.RTVoice.Model.Voice)
extern void U3CU3Ec__DisplayClass171_0_U3CVoicesForCultureU3Eb__0_m96A3511B67132C662B4A06B0207A7670BEEAC058 (void);
// 0x000000E8 System.Void Crosstales.RTVoice.Speaker/<>c::.cctor()
extern void U3CU3Ec__cctor_m99B5A242DEE71B6DFC6DF8006C37510E40362CC4 (void);
// 0x000000E9 System.Void Crosstales.RTVoice.Speaker/<>c::.ctor()
extern void U3CU3Ec__ctor_mF9848D30ACD5EFC99FEA03F3E6F78B144A143AAF (void);
// 0x000000EA System.String Crosstales.RTVoice.Speaker/<>c::<VoicesForCulture>b__171_1(Crosstales.RTVoice.Model.Voice)
extern void U3CU3Ec_U3CVoicesForCultureU3Eb__171_1_mD53CF0784FB8B6D1FD437794BC0FE9DD1303FB95 (void);
// 0x000000EB System.Void Crosstales.RTVoice.Speaker/<>c__DisplayClass190_0::.ctor()
extern void U3CU3Ec__DisplayClass190_0__ctor_m1F7184DEC71431F28A6B7A2AB8935BD158A6212B (void);
// 0x000000EC System.Void Crosstales.RTVoice.Speaker/<>c__DisplayClass190_0::<DeleteAudioFiles>b__0()
extern void U3CU3Ec__DisplayClass190_0_U3CDeleteAudioFilesU3Eb__0_mF932CC0B8E8DC74E75C9EA73D32A831032500017 (void);
// 0x000000ED System.String Crosstales.RTVoice.VoiceProviderExample::get_AudioFileExtension()
extern void VoiceProviderExample_get_AudioFileExtension_m359944D955D65ACF22047912FF813415528A46F6 (void);
// 0x000000EE UnityEngine.AudioType Crosstales.RTVoice.VoiceProviderExample::get_AudioFileType()
extern void VoiceProviderExample_get_AudioFileType_mABEDD96356CBDEFABDC04FF1F0C69A7B8A47FDAC (void);
// 0x000000EF System.String Crosstales.RTVoice.VoiceProviderExample::get_DefaultVoiceName()
extern void VoiceProviderExample_get_DefaultVoiceName_m20E0967E6A0D39AAD2C7B4E98102E9DB6267E233 (void);
// 0x000000F0 System.Boolean Crosstales.RTVoice.VoiceProviderExample::get_isWorkingInEditor()
extern void VoiceProviderExample_get_isWorkingInEditor_mEF00E3D91C67A013FCF8B51AE2CC7BFF1EF26672 (void);
// 0x000000F1 System.Boolean Crosstales.RTVoice.VoiceProviderExample::get_isWorkingInPlaymode()
extern void VoiceProviderExample_get_isWorkingInPlaymode_mA9EB3E1DDD3EB653F30C96214254161DF01F87E6 (void);
// 0x000000F2 System.Boolean Crosstales.RTVoice.VoiceProviderExample::get_isPlatformSupported()
extern void VoiceProviderExample_get_isPlatformSupported_mB1181FD48BD9248A54DDF06B031802FBE25F3AAF (void);
// 0x000000F3 System.Int32 Crosstales.RTVoice.VoiceProviderExample::get_MaxTextLength()
extern void VoiceProviderExample_get_MaxTextLength_m803C12286B895D81B78D3130036D780FDCF5FCA9 (void);
// 0x000000F4 System.Boolean Crosstales.RTVoice.VoiceProviderExample::get_isSpeakNativeSupported()
extern void VoiceProviderExample_get_isSpeakNativeSupported_m9D97D112D128AD620C4443C7C9EB47A38C5A0DC5 (void);
// 0x000000F5 System.Boolean Crosstales.RTVoice.VoiceProviderExample::get_isSpeakSupported()
extern void VoiceProviderExample_get_isSpeakSupported_m26D549D961590292A057E5827A479D438093A33C (void);
// 0x000000F6 System.Boolean Crosstales.RTVoice.VoiceProviderExample::get_isSSMLSupported()
extern void VoiceProviderExample_get_isSSMLSupported_mD844EF4A880B4DFDCF5B9283FD4E32D8480ADB27 (void);
// 0x000000F7 System.Boolean Crosstales.RTVoice.VoiceProviderExample::get_isOnlineService()
extern void VoiceProviderExample_get_isOnlineService_mCEBA53777B091ACA5941AD5B8BDEF468AC44C517 (void);
// 0x000000F8 System.Boolean Crosstales.RTVoice.VoiceProviderExample::get_hasCoRoutines()
extern void VoiceProviderExample_get_hasCoRoutines_m1BAD5B5E827CD11C8AA78D4B3BE30E6D5CEA10CB (void);
// 0x000000F9 System.Boolean Crosstales.RTVoice.VoiceProviderExample::get_isIL2CPPSupported()
extern void VoiceProviderExample_get_isIL2CPPSupported_m62F23B593FAB84988DB332FC7656691E071B0666 (void);
// 0x000000FA System.Void Crosstales.RTVoice.VoiceProviderExample::Load()
extern void VoiceProviderExample_Load_m510C50681197E6243E41D164F9F43C3CDF799824 (void);
// 0x000000FB System.Collections.IEnumerator Crosstales.RTVoice.VoiceProviderExample::Generate(Crosstales.RTVoice.Model.Wrapper)
extern void VoiceProviderExample_Generate_mCEF1A347318B74AA723F6ABFA9C2EB48DC2DF9C4 (void);
// 0x000000FC System.Collections.IEnumerator Crosstales.RTVoice.VoiceProviderExample::Speak(Crosstales.RTVoice.Model.Wrapper)
extern void VoiceProviderExample_Speak_mBDB765FB3CC330D5B5FC491DC795FD5297EDF0E7 (void);
// 0x000000FD System.Collections.IEnumerator Crosstales.RTVoice.VoiceProviderExample::SpeakNative(Crosstales.RTVoice.Model.Wrapper)
extern void VoiceProviderExample_SpeakNative_m9AE9125ACB3493351913B96173D7DBC83E2EAF13 (void);
// 0x000000FE System.Void Crosstales.RTVoice.VoiceProviderExample::.ctor()
extern void VoiceProviderExample__ctor_mEB1B521BF2B43009161F427149A48F7E0717FEF7 (void);
// 0x000000FF System.Void Crosstales.RTVoice.VoiceProviderExample/<Generate>d__27::.ctor(System.Int32)
extern void U3CGenerateU3Ed__27__ctor_mBE7648CA9BC983832FB40122AE1F309C028FAE80 (void);
// 0x00000100 System.Void Crosstales.RTVoice.VoiceProviderExample/<Generate>d__27::System.IDisposable.Dispose()
extern void U3CGenerateU3Ed__27_System_IDisposable_Dispose_m31A13EDD00230227067357AEDF3081F4DFE30A20 (void);
// 0x00000101 System.Boolean Crosstales.RTVoice.VoiceProviderExample/<Generate>d__27::MoveNext()
extern void U3CGenerateU3Ed__27_MoveNext_m2BC80C6ED45CABECC111A777D929EAC1C52725BA (void);
// 0x00000102 System.Object Crosstales.RTVoice.VoiceProviderExample/<Generate>d__27::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CGenerateU3Ed__27_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m5D2AC763594283B97AF22C0ECE832F4019B1DB85 (void);
// 0x00000103 System.Void Crosstales.RTVoice.VoiceProviderExample/<Generate>d__27::System.Collections.IEnumerator.Reset()
extern void U3CGenerateU3Ed__27_System_Collections_IEnumerator_Reset_mBD3F2BBF7EB868747C7D15542F2E9089C61F50C1 (void);
// 0x00000104 System.Object Crosstales.RTVoice.VoiceProviderExample/<Generate>d__27::System.Collections.IEnumerator.get_Current()
extern void U3CGenerateU3Ed__27_System_Collections_IEnumerator_get_Current_m1BEE20547347457E10EF46105AA485378FA8EEE3 (void);
// 0x00000105 System.Void Crosstales.RTVoice.VoiceProviderExample/<Speak>d__28::.ctor(System.Int32)
extern void U3CSpeakU3Ed__28__ctor_m00F559E690F57E2B4D1C6180D7BCF76110BBC6E1 (void);
// 0x00000106 System.Void Crosstales.RTVoice.VoiceProviderExample/<Speak>d__28::System.IDisposable.Dispose()
extern void U3CSpeakU3Ed__28_System_IDisposable_Dispose_mC282884D02DBBFF1E195D8728EA2B8FF2D68351D (void);
// 0x00000107 System.Boolean Crosstales.RTVoice.VoiceProviderExample/<Speak>d__28::MoveNext()
extern void U3CSpeakU3Ed__28_MoveNext_mD4CE62F78D7F43EDED79FCD5B972986238D92775 (void);
// 0x00000108 System.Object Crosstales.RTVoice.VoiceProviderExample/<Speak>d__28::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CSpeakU3Ed__28_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m155241265B291EBEE2FD2381BC48B220AA3BA054 (void);
// 0x00000109 System.Void Crosstales.RTVoice.VoiceProviderExample/<Speak>d__28::System.Collections.IEnumerator.Reset()
extern void U3CSpeakU3Ed__28_System_Collections_IEnumerator_Reset_mC4A4CC3360ACD12464DEE845E9CE608105356EF3 (void);
// 0x0000010A System.Object Crosstales.RTVoice.VoiceProviderExample/<Speak>d__28::System.Collections.IEnumerator.get_Current()
extern void U3CSpeakU3Ed__28_System_Collections_IEnumerator_get_Current_mA9B5A00AE897B39C9DEA60B9E4DA3135421125EC (void);
// 0x0000010B System.Void Crosstales.RTVoice.VoiceProviderExample/<SpeakNative>d__29::.ctor(System.Int32)
extern void U3CSpeakNativeU3Ed__29__ctor_mAD9F48CC410BA068A8CBF1E72BAD7EAFFE682B0A (void);
// 0x0000010C System.Void Crosstales.RTVoice.VoiceProviderExample/<SpeakNative>d__29::System.IDisposable.Dispose()
extern void U3CSpeakNativeU3Ed__29_System_IDisposable_Dispose_m309B55ACD321D7AC45AD632C1B4EACED83D47100 (void);
// 0x0000010D System.Boolean Crosstales.RTVoice.VoiceProviderExample/<SpeakNative>d__29::MoveNext()
extern void U3CSpeakNativeU3Ed__29_MoveNext_m1E7F5074F853AD1623F3C02F25C95016623B575D (void);
// 0x0000010E System.Object Crosstales.RTVoice.VoiceProviderExample/<SpeakNative>d__29::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CSpeakNativeU3Ed__29_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m7C5EDA7375CBEE70E45B133C63BED52FBD448E07 (void);
// 0x0000010F System.Void Crosstales.RTVoice.VoiceProviderExample/<SpeakNative>d__29::System.Collections.IEnumerator.Reset()
extern void U3CSpeakNativeU3Ed__29_System_Collections_IEnumerator_Reset_m5F62E754EA07230AF596C779B5484274CCBFB409 (void);
// 0x00000110 System.Object Crosstales.RTVoice.VoiceProviderExample/<SpeakNative>d__29::System.Collections.IEnumerator.get_Current()
extern void U3CSpeakNativeU3Ed__29_System_Collections_IEnumerator_get_Current_m64E0A5D15216AFA6D8650F2B8453143B89539613 (void);
// 0x00000111 System.String Crosstales.RTVoice.Util.Config::get_AUDIOFILE_PATH()
extern void Config_get_AUDIOFILE_PATH_m42F7E5CF3F9B15AAA02D8A65141D60CA98ED25C7 (void);
// 0x00000112 System.Void Crosstales.RTVoice.Util.Config::set_AUDIOFILE_PATH(System.String)
extern void Config_set_AUDIOFILE_PATH_mF8DCEF7B6C8401E91989EED74BB4F038A54E23E5 (void);
// 0x00000113 System.String Crosstales.RTVoice.Util.Config::get_TTS_WINDOWS_EDITOR()
extern void Config_get_TTS_WINDOWS_EDITOR_mDC6567EB7C12D7F64FDF6CF06377465DF7AC7F65 (void);
// 0x00000114 System.String Crosstales.RTVoice.Util.Config::get_TTS_WINDOWS_EDITOR_x86()
extern void Config_get_TTS_WINDOWS_EDITOR_x86_m06A3291CE8C566BC341ADA6A77D1F9601275E6D8 (void);
// 0x00000115 System.Void Crosstales.RTVoice.Util.Config::Reset()
extern void Config_Reset_mA4E521995FDD11F2A5711A171F2EE71DE1A93379 (void);
// 0x00000116 System.Void Crosstales.RTVoice.Util.Config::Load()
extern void Config_Load_m70C1BAA1E4101A0A37E466DA54C5E01821A63F61 (void);
// 0x00000117 System.Void Crosstales.RTVoice.Util.Config::Save()
extern void Config_Save_mF4F0C7CC3728BA922747AB9CAF29CA276A30193C (void);
// 0x00000118 System.Void Crosstales.RTVoice.Util.Config::.cctor()
extern void Config__cctor_mEE2B7AE6033E0C138564A6FACB5A0319CDDBC053 (void);
// 0x00000119 System.Void Crosstales.RTVoice.Util.Constants::.ctor()
extern void Constants__ctor_m35F7FD108807C339B3EB9B3F290AB1709BA726BB (void);
// 0x0000011A System.Void Crosstales.RTVoice.Util.Constants::.cctor()
extern void Constants__cctor_m5A6095E120BD98AA88A791C270E1556E2B60ED70 (void);
// 0x0000011B System.Void Crosstales.RTVoice.Util.Helper::.cctor()
extern void Helper__cctor_m457937714ADA94D61825FB564985128B55046773 (void);
// 0x0000011C System.Boolean Crosstales.RTVoice.Util.Helper::get_hasBuiltInTTS()
extern void Helper_get_hasBuiltInTTS_m82B916D06756371DD8CE4B2C27F7512163C95C38 (void);
// 0x0000011D Crosstales.RTVoice.Model.Enum.ProviderType Crosstales.RTVoice.Util.Helper::get_CurrentProviderType()
extern void Helper_get_CurrentProviderType_mBC3BEEF008B7FF523816D31D4C21855C1D21AEA6 (void);
// 0x0000011E Crosstales.RTVoice.Model.Enum.Gender Crosstales.RTVoice.Util.Helper::StringToGender(System.String)
extern void Helper_StringToGender_m48F39C3BD1DEA77AC4C8BE17181B822500FAA077 (void);
// 0x0000011F Crosstales.RTVoice.Model.Enum.Gender Crosstales.RTVoice.Util.Helper::AppleVoiceNameToGender(System.String)
extern void Helper_AppleVoiceNameToGender_m2747FBA07F06C0468AB5344D38E8D2972786F954 (void);
// 0x00000120 Crosstales.RTVoice.Model.Enum.Gender Crosstales.RTVoice.Util.Helper::WSAVoiceNameToGender(System.String)
extern void Helper_WSAVoiceNameToGender_mB6F4279630D1BED7B1EE1FCBA0A6E588BE72B611 (void);
// 0x00000121 System.String Crosstales.RTVoice.Util.Helper::CleanText(System.String,System.Boolean,System.Boolean,System.Boolean)
extern void Helper_CleanText_m93A1DD6658F52066E80C7DEF6F54A5EBBEF986AF (void);
// 0x00000122 System.String Crosstales.RTVoice.Util.Helper::MarkSpokenText(System.String[],System.Int32,System.Boolean,System.String,System.String)
extern void Helper_MarkSpokenText_m927EDD18E2DFB0230279F9CB0491281BBE437194 (void);
// 0x00000123 System.Void Crosstales.RTVoice.Util.Helper::.ctor()
extern void Helper__ctor_m93B5A01DDC3FFC46837922F5E83089992E4086D1 (void);
// 0x00000124 UnityEngine.AudioClip Crosstales.RTVoice.Util.WavMaster::ToAudioClip(System.String,System.String)
extern void WavMaster_ToAudioClip_m65CF64BD05B7E577B889227DE04C6755CDDCCDA8 (void);
// 0x00000125 UnityEngine.AudioClip Crosstales.RTVoice.Util.WavMaster::ToAudioClip(System.Byte[],System.Int32,System.String)
extern void WavMaster_ToAudioClip_m9FBE5BD02C7A764FB8DE2BF83A2CB6885EEFC864 (void);
// 0x00000126 System.Byte[] Crosstales.RTVoice.Util.WavMaster::FromAudioClip(UnityEngine.AudioClip)
extern void WavMaster_FromAudioClip_mD3411FAAEDBB48354817156342A8E5F4ED8BA11B (void);
// 0x00000127 System.Byte[] Crosstales.RTVoice.Util.WavMaster::FromAudioClip(UnityEngine.AudioClip,System.String&,System.Boolean,System.String)
extern void WavMaster_FromAudioClip_m31105CFF0DCED281812F3C6F8ED00BA239FC0945 (void);
// 0x00000128 System.UInt16 Crosstales.RTVoice.Util.WavMaster::BitDepth(UnityEngine.AudioClip)
extern void WavMaster_BitDepth_mA01F95FAE0614DEEB3E02EBD0BDDA1623ADC7AD1 (void);
// 0x00000129 System.Single[] Crosstales.RTVoice.Util.WavMaster::convert8BitByteArrayToAudioClipData(System.Byte[],System.Int32,System.Int32)
extern void WavMaster_convert8BitByteArrayToAudioClipData_m3FB79D7CDE527A2D85D76C116A67BD7EE4FCBBAA (void);
// 0x0000012A System.Single[] Crosstales.RTVoice.Util.WavMaster::convert16BitByteArrayToAudioClipData(System.Byte[],System.Int32,System.Int32)
extern void WavMaster_convert16BitByteArrayToAudioClipData_m6E338CC1527CAB17CF3E2CC75C4DA61AE371EBDA (void);
// 0x0000012B System.Single[] Crosstales.RTVoice.Util.WavMaster::convert24BitByteArrayToAudioClipData(System.Byte[],System.Int32,System.Int32)
extern void WavMaster_convert24BitByteArrayToAudioClipData_mB1403BA352CF5E86F23B38891F4E45843B60C5DA (void);
// 0x0000012C System.Single[] Crosstales.RTVoice.Util.WavMaster::convert32BitByteArrayToAudioClipData(System.Byte[],System.Int32,System.Int32)
extern void WavMaster_convert32BitByteArrayToAudioClipData_mB834332E91F6009C9F22ADD04662F44F8AB2BA15 (void);
// 0x0000012D System.Int32 Crosstales.RTVoice.Util.WavMaster::writeFileHeader(System.IO.MemoryStream&,System.Int32)
extern void WavMaster_writeFileHeader_mFFB5D831814D2CDAB3BE1B2827D855C15670AB70 (void);
// 0x0000012E System.Int32 Crosstales.RTVoice.Util.WavMaster::writeFileFormat(System.IO.MemoryStream&,System.Int32,System.Int32,System.UInt16)
extern void WavMaster_writeFileFormat_mAAAD9178251EB06EE397C38159D877F866CDABA1 (void);
// 0x0000012F System.Int32 Crosstales.RTVoice.Util.WavMaster::writeFileData(System.IO.MemoryStream&,UnityEngine.AudioClip,System.UInt16)
extern void WavMaster_writeFileData_m4B480990298E7090D74468A05812A1FE2A56F39B (void);
// 0x00000130 System.Byte[] Crosstales.RTVoice.Util.WavMaster::convertAudioClipDataToInt16ByteArray(System.Single[])
extern void WavMaster_convertAudioClipDataToInt16ByteArray_m87AA4F874F401251336D669786133F628D619C3B (void);
// 0x00000131 System.Int32 Crosstales.RTVoice.Util.WavMaster::writeBytesToMemoryStream(System.IO.MemoryStream&,System.Byte[],System.String)
extern void WavMaster_writeBytesToMemoryStream_m794D53302594D25EE580A5C6A1638B2DE86DD762 (void);
// 0x00000132 System.Int32 Crosstales.RTVoice.Util.WavMaster::bytesPerSample(System.UInt16)
extern void WavMaster_bytesPerSample_m451465CA58E4ADB0FD1873136036164A94A055AE (void);
// 0x00000133 System.Int32 Crosstales.RTVoice.Util.WavMaster::BlockSize(System.UInt16)
extern void WavMaster_BlockSize_mEF780ED07E684D6310EAE4676928B1DF9ACC0EE9 (void);
// 0x00000134 System.String Crosstales.RTVoice.Util.WavMaster::formatCode(System.UInt16)
extern void WavMaster_formatCode_m4491A0826DE5BC50BCE0F9C4BF886455C5270BE1 (void);
// 0x00000135 System.Void Crosstales.RTVoice.Util.WavMaster::.ctor()
extern void WavMaster__ctor_m5BA3CD59FE47956E952FD47CC8B3FDC58D5A491E (void);
// 0x00000136 System.Void Crosstales.RTVoice.Tool.AudioFileGenerator::add_OnAudioFileGeneratorStart(Crosstales.RTVoice.AudioFileGeneratorStart)
extern void AudioFileGenerator_add_OnAudioFileGeneratorStart_m743ED03464D1055DCB460CCEFCBAD74E358920CA (void);
// 0x00000137 System.Void Crosstales.RTVoice.Tool.AudioFileGenerator::remove_OnAudioFileGeneratorStart(Crosstales.RTVoice.AudioFileGeneratorStart)
extern void AudioFileGenerator_remove_OnAudioFileGeneratorStart_m6D26647D06F077594C1958D4BC859F2096469F5B (void);
// 0x00000138 System.Void Crosstales.RTVoice.Tool.AudioFileGenerator::add_OnAudioFileGeneratorComplete(Crosstales.RTVoice.AudioFileGeneratorComplete)
extern void AudioFileGenerator_add_OnAudioFileGeneratorComplete_m2F2E2EE5F1C08D940F7566840708FB86CE653033 (void);
// 0x00000139 System.Void Crosstales.RTVoice.Tool.AudioFileGenerator::remove_OnAudioFileGeneratorComplete(Crosstales.RTVoice.AudioFileGeneratorComplete)
extern void AudioFileGenerator_remove_OnAudioFileGeneratorComplete_m4C3B41D883DEA640E251EB37BA493CF90FC7EAE6 (void);
// 0x0000013A System.Void Crosstales.RTVoice.Tool.AudioFileGenerator::OnEnable()
extern void AudioFileGenerator_OnEnable_m0E8F52CE84CD5D7350C75EFABCFF215E5EC1F5E1 (void);
// 0x0000013B System.Void Crosstales.RTVoice.Tool.AudioFileGenerator::OnDisable()
extern void AudioFileGenerator_OnDisable_m5B8DCF4A33BCBB1D595619C91964EAAE23B60170 (void);
// 0x0000013C System.Void Crosstales.RTVoice.Tool.AudioFileGenerator::OnValidate()
extern void AudioFileGenerator_OnValidate_m118FF61EEB020E3ECFF1A6644D643EA9160F8E69 (void);
// 0x0000013D System.Void Crosstales.RTVoice.Tool.AudioFileGenerator::Generate()
extern void AudioFileGenerator_Generate_mD4AB0C5327B213F50D7245A827629EA8BD0F92F6 (void);
// 0x0000013E System.Collections.IEnumerator Crosstales.RTVoice.Tool.AudioFileGenerator::generate()
extern void AudioFileGenerator_generate_m4FEFFBF2EFCD1E9AE3710DA8C6DA480C781F70F9 (void);
// 0x0000013F System.Void Crosstales.RTVoice.Tool.AudioFileGenerator::convert(System.String)
extern void AudioFileGenerator_convert_m85557D268CDF462EC447E7FB578E542F8773426C (void);
// 0x00000140 System.Void Crosstales.RTVoice.Tool.AudioFileGenerator::Normalize(System.String)
extern void AudioFileGenerator_Normalize_m2E2E2896E7425B6CE5F38D9AD0682171344C8DE7 (void);
// 0x00000141 System.Single Crosstales.RTVoice.Tool.AudioFileGenerator::GetMaxPeak(System.String)
extern void AudioFileGenerator_GetMaxPeak_m72CA42B5E8B15B5C405798EE9CC26DE214999487 (void);
// 0x00000142 Crosstales.RTVoice.Model.Wrapper Crosstales.RTVoice.Tool.AudioFileGenerator::prepare(System.String[],System.String)
extern void AudioFileGenerator_prepare_m1B317C05D8F67ED3C41A0FC1503960BB721F71DC (void);
// 0x00000143 System.Void Crosstales.RTVoice.Tool.AudioFileGenerator::onVoicesReady()
extern void AudioFileGenerator_onVoicesReady_m0C62C5B22F4A8C91574EEC80E10D05F5ABCBD4D5 (void);
// 0x00000144 System.Void Crosstales.RTVoice.Tool.AudioFileGenerator::onSpeakAudioGenerationComplete(Crosstales.RTVoice.Model.Wrapper)
extern void AudioFileGenerator_onSpeakAudioGenerationComplete_m5BD832FD9C7DD6FD0CC9AB1A5916575E10EB6212 (void);
// 0x00000145 System.Void Crosstales.RTVoice.Tool.AudioFileGenerator::onStart()
extern void AudioFileGenerator_onStart_m001F23D9773434D24FE46318E745A85FFB0538F6 (void);
// 0x00000146 System.Void Crosstales.RTVoice.Tool.AudioFileGenerator::onComplete()
extern void AudioFileGenerator_onComplete_mB02C329697B3C3A43E9FF5136C23FE3ED8F97B68 (void);
// 0x00000147 System.Void Crosstales.RTVoice.Tool.AudioFileGenerator::.ctor()
extern void AudioFileGenerator__ctor_m0FA32E200D5E84EF0558924637063979CD5237F1 (void);
// 0x00000148 System.Void Crosstales.RTVoice.Tool.AudioFileGenerator::.cctor()
extern void AudioFileGenerator__cctor_mBF501FD01085E2F052380C9C6C55C775A543A8D5 (void);
// 0x00000149 System.Void Crosstales.RTVoice.Tool.AudioFileGenerator/<generate>d__19::.ctor(System.Int32)
extern void U3CgenerateU3Ed__19__ctor_m304D082A586B1520066B6F1F5D70922AF6AF4358 (void);
// 0x0000014A System.Void Crosstales.RTVoice.Tool.AudioFileGenerator/<generate>d__19::System.IDisposable.Dispose()
extern void U3CgenerateU3Ed__19_System_IDisposable_Dispose_m9637C84D3DE5BA34D03CD47F7254960ED4DFFCB6 (void);
// 0x0000014B System.Boolean Crosstales.RTVoice.Tool.AudioFileGenerator/<generate>d__19::MoveNext()
extern void U3CgenerateU3Ed__19_MoveNext_mE689A3AEB6F43F1D6EA33E0B9010660F84363C55 (void);
// 0x0000014C System.Void Crosstales.RTVoice.Tool.AudioFileGenerator/<generate>d__19::<>m__Finally1()
extern void U3CgenerateU3Ed__19_U3CU3Em__Finally1_mD2D123331A4179A30AC59FEF240079C1978D21F7 (void);
// 0x0000014D System.Object Crosstales.RTVoice.Tool.AudioFileGenerator/<generate>d__19::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CgenerateU3Ed__19_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m3C28CECD7AE53E425198C381D87112731DEA0FB7 (void);
// 0x0000014E System.Void Crosstales.RTVoice.Tool.AudioFileGenerator/<generate>d__19::System.Collections.IEnumerator.Reset()
extern void U3CgenerateU3Ed__19_System_Collections_IEnumerator_Reset_m45465FCA07B5C00BE97AA285379D2880E212DD73 (void);
// 0x0000014F System.Object Crosstales.RTVoice.Tool.AudioFileGenerator/<generate>d__19::System.Collections.IEnumerator.get_Current()
extern void U3CgenerateU3Ed__19_System_Collections_IEnumerator_get_Current_m1990C2C28E0F67CD67DE84F9E86ABAEE4EDECC3F (void);
// 0x00000150 System.Void Crosstales.RTVoice.Tool.ChangeGender::Start()
extern void ChangeGender_Start_m5A0F0A65CD0663A25AC9F6EBA47B269B31AB77E0 (void);
// 0x00000151 System.Void Crosstales.RTVoice.Tool.ChangeGender::OnDestroy()
extern void ChangeGender_OnDestroy_m184EE2644851FC756F6CB29C1003B26879ACA91A (void);
// 0x00000152 System.Void Crosstales.RTVoice.Tool.ChangeGender::GenderChanged(System.Int32)
extern void ChangeGender_GenderChanged_mB0A1A752AEF791F40566145DD56F2F932991A8B7 (void);
// 0x00000153 System.Void Crosstales.RTVoice.Tool.ChangeGender::Change()
extern void ChangeGender_Change_m88A50EE20D991FD1BCB53655F9393131C90BEA87 (void);
// 0x00000154 System.Void Crosstales.RTVoice.Tool.ChangeGender::.ctor()
extern void ChangeGender__ctor_m5EA4FC2B9CEEB64303C6170D96BF2B443F7E9595 (void);
// 0x00000155 System.Boolean Crosstales.RTVoice.Tool.Loudspeaker::get_isSynchronized()
extern void Loudspeaker_get_isSynchronized_m3AF3CFB842F8E463303ACB21A936C8EA361E8FCD (void);
// 0x00000156 System.Void Crosstales.RTVoice.Tool.Loudspeaker::set_isSynchronized(System.Boolean)
extern void Loudspeaker_set_isSynchronized_m85DB18CB156FE3A4022EE66E3FDDFF4B153F1D4B (void);
// 0x00000157 System.Boolean Crosstales.RTVoice.Tool.Loudspeaker::get_isSilenceSource()
extern void Loudspeaker_get_isSilenceSource_m6CCC9AA9E6785EB4C2682977FDEFB98447ACB3D0 (void);
// 0x00000158 System.Void Crosstales.RTVoice.Tool.Loudspeaker::set_isSilenceSource(System.Boolean)
extern void Loudspeaker_set_isSilenceSource_m527FA908E5922D0BC8FCF8393D527713C6217C4A (void);
// 0x00000159 System.Void Crosstales.RTVoice.Tool.Loudspeaker::Awake()
extern void Loudspeaker_Awake_m6CEBD62FDC5D19707D7FD1407CCB30B70C6845F2 (void);
// 0x0000015A System.Void Crosstales.RTVoice.Tool.Loudspeaker::Start()
extern void Loudspeaker_Start_m2D1889D6CDB30CE97E63A50788F623AD2E524485 (void);
// 0x0000015B System.Void Crosstales.RTVoice.Tool.Loudspeaker::Update()
extern void Loudspeaker_Update_mD4DDBCD40FEC32428327BF4370EAD11C48AA0AF0 (void);
// 0x0000015C System.Void Crosstales.RTVoice.Tool.Loudspeaker::FixedUpdate()
extern void Loudspeaker_FixedUpdate_m7227004C15B7BD5ECF4B9C6B9C761AE60CEDFEFB (void);
// 0x0000015D System.Void Crosstales.RTVoice.Tool.Loudspeaker::OnDisable()
extern void Loudspeaker_OnDisable_m4ED73705A797F8D7CA2BB7F93D5153D68EF47D99 (void);
// 0x0000015E System.Void Crosstales.RTVoice.Tool.Loudspeaker::.ctor()
extern void Loudspeaker__ctor_m1B24204A0ABE94CA91D9A21A362562287DDEE085 (void);
// 0x0000015F System.Void Crosstales.RTVoice.Tool.Paralanguage::add_OnParalanguageStart(Crosstales.RTVoice.ParalanguageStart)
extern void Paralanguage_add_OnParalanguageStart_mC6A3DB677061F3141F320428585BC6D6EC7F7455 (void);
// 0x00000160 System.Void Crosstales.RTVoice.Tool.Paralanguage::remove_OnParalanguageStart(Crosstales.RTVoice.ParalanguageStart)
extern void Paralanguage_remove_OnParalanguageStart_m7D76377E8AAFC53BF601283F44CFFF8BD9164215 (void);
// 0x00000161 System.Void Crosstales.RTVoice.Tool.Paralanguage::add_OnParalanguageComplete(Crosstales.RTVoice.ParalanguageComplete)
extern void Paralanguage_add_OnParalanguageComplete_m3EA6F0F31FF1D046100A42088C5AEF87AB99255C (void);
// 0x00000162 System.Void Crosstales.RTVoice.Tool.Paralanguage::remove_OnParalanguageComplete(Crosstales.RTVoice.ParalanguageComplete)
extern void Paralanguage_remove_OnParalanguageComplete_m3C44D3A96D766C4AEDCF667EBB4315F713C64160 (void);
// 0x00000163 System.String Crosstales.RTVoice.Tool.Paralanguage::get_CurrentText()
extern void Paralanguage_get_CurrentText_m08B81A3AF4052EDCA4A15D19C8893F3835AA47F7 (void);
// 0x00000164 System.Void Crosstales.RTVoice.Tool.Paralanguage::set_CurrentText(System.String)
extern void Paralanguage_set_CurrentText_mDD1E5BD403E373D7DACA85CCD0CC4E6804FF8988 (void);
// 0x00000165 System.Single Crosstales.RTVoice.Tool.Paralanguage::get_CurrentRate()
extern void Paralanguage_get_CurrentRate_m44CAD57346341EF8B2367965B03FD921A2360A2C (void);
// 0x00000166 System.Void Crosstales.RTVoice.Tool.Paralanguage::set_CurrentRate(System.Single)
extern void Paralanguage_set_CurrentRate_mAA18A9E4868BBF64CBF856D99F032D7E8957042F (void);
// 0x00000167 System.Single Crosstales.RTVoice.Tool.Paralanguage::get_CurrentPitch()
extern void Paralanguage_get_CurrentPitch_m006F4DC8585ACD788EA915BD139F26682AC928B5 (void);
// 0x00000168 System.Void Crosstales.RTVoice.Tool.Paralanguage::set_CurrentPitch(System.Single)
extern void Paralanguage_set_CurrentPitch_m27FCE78DC46103743B3A409F09F29592827B7C74 (void);
// 0x00000169 System.Single Crosstales.RTVoice.Tool.Paralanguage::get_CurrentVolume()
extern void Paralanguage_get_CurrentVolume_m44F01DC0490780A2EB5A59FA5AAAEA499A5D2F95 (void);
// 0x0000016A System.Void Crosstales.RTVoice.Tool.Paralanguage::set_CurrentVolume(System.Single)
extern void Paralanguage_set_CurrentVolume_m1F12D3941B782AC761AFA719F0E26BF9D8103F5D (void);
// 0x0000016B System.Void Crosstales.RTVoice.Tool.Paralanguage::Awake()
extern void Paralanguage_Awake_mA3669AAAEA2F198F9C2B9D097C6968460452FBE0 (void);
// 0x0000016C System.Void Crosstales.RTVoice.Tool.Paralanguage::Start()
extern void Paralanguage_Start_mF2FD53BF02A9279D62BCB874E2F662B7D6137F5A (void);
// 0x0000016D System.Void Crosstales.RTVoice.Tool.Paralanguage::OnDestroy()
extern void Paralanguage_OnDestroy_m3C3220C9711D995873186EC17639715E28C234C6 (void);
// 0x0000016E System.Void Crosstales.RTVoice.Tool.Paralanguage::OnValidate()
extern void Paralanguage_OnValidate_m6C97C5EB5DB3A2139452300A8D70BC12C4D488B5 (void);
// 0x0000016F System.Void Crosstales.RTVoice.Tool.Paralanguage::Speak()
extern void Paralanguage_Speak_mE5417A97A1BAF7D9C35CFBAF2BCC6F1EEA7C7C00 (void);
// 0x00000170 System.Void Crosstales.RTVoice.Tool.Paralanguage::Silence()
extern void Paralanguage_Silence_m5AB91D89653137FA538D9ED1BF3B1DFC33DA5903 (void);
// 0x00000171 System.Collections.IEnumerator Crosstales.RTVoice.Tool.Paralanguage::processStack()
extern void Paralanguage_processStack_mA8A64AFE252A5308590B2241692C7A106025887D (void);
// 0x00000172 System.Void Crosstales.RTVoice.Tool.Paralanguage::play()
extern void Paralanguage_play_m5DEF563AE37DF397148D223F6EDBFE721EBF66B8 (void);
// 0x00000173 System.Void Crosstales.RTVoice.Tool.Paralanguage::onVoicesReady()
extern void Paralanguage_onVoicesReady_m4FE130AB28A35F38BE6E33D8E26DAB6D506F6818 (void);
// 0x00000174 System.Void Crosstales.RTVoice.Tool.Paralanguage::onSpeakComplete(Crosstales.RTVoice.Model.Wrapper)
extern void Paralanguage_onSpeakComplete_m9E5A6A36656CBB7000BFD0A81C7217020B54FBF5 (void);
// 0x00000175 System.Void Crosstales.RTVoice.Tool.Paralanguage::onStart()
extern void Paralanguage_onStart_m17A3DCAF2A3BE384A3665785E53EE689BDDAD0B6 (void);
// 0x00000176 System.Void Crosstales.RTVoice.Tool.Paralanguage::onComplete()
extern void Paralanguage_onComplete_m36CBEA470C658BD8499CF3B7DBC964B070031CD4 (void);
// 0x00000177 System.Void Crosstales.RTVoice.Tool.Paralanguage::.ctor()
extern void Paralanguage__ctor_m7DEB2597D7520D6F97F78AAE54D2B3169D15677B (void);
// 0x00000178 System.Void Crosstales.RTVoice.Tool.Paralanguage::.cctor()
extern void Paralanguage__cctor_m5EDDC2822ADCE7A104743AB8F42A143FE3B2E1FE (void);
// 0x00000179 System.Void Crosstales.RTVoice.Tool.Paralanguage/<>c::.cctor()
extern void U3CU3Ec__cctor_mFB6F640DF8F03E43B1BFEBBCAC764F6E2554F4E5 (void);
// 0x0000017A System.Void Crosstales.RTVoice.Tool.Paralanguage/<>c::.ctor()
extern void U3CU3Ec__ctor_m2CBBA0961378F8F0193D3F6A4284D29CC65F8F95 (void);
// 0x0000017B System.Boolean Crosstales.RTVoice.Tool.Paralanguage/<>c::<Speak>b__40_0(System.String)
extern void U3CU3Ec_U3CSpeakU3Eb__40_0_m2A211B89F4F07E7512F794F8CF0F0E5210559DC7 (void);
// 0x0000017C System.Void Crosstales.RTVoice.Tool.Paralanguage/<processStack>d__42::.ctor(System.Int32)
extern void U3CprocessStackU3Ed__42__ctor_m21BC01AD74ECA7C64B458339F0B9DCC19A45ADE0 (void);
// 0x0000017D System.Void Crosstales.RTVoice.Tool.Paralanguage/<processStack>d__42::System.IDisposable.Dispose()
extern void U3CprocessStackU3Ed__42_System_IDisposable_Dispose_m7430826A64DB1BA83762995F321AA81FDD62220A (void);
// 0x0000017E System.Boolean Crosstales.RTVoice.Tool.Paralanguage/<processStack>d__42::MoveNext()
extern void U3CprocessStackU3Ed__42_MoveNext_m2A77D1C62A50F49135D2148F51F0EBAE67ECB1FB (void);
// 0x0000017F System.Void Crosstales.RTVoice.Tool.Paralanguage/<processStack>d__42::<>m__Finally1()
extern void U3CprocessStackU3Ed__42_U3CU3Em__Finally1_m96E94618A0D81B80F36589D9177627358AE1817B (void);
// 0x00000180 System.Object Crosstales.RTVoice.Tool.Paralanguage/<processStack>d__42::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CprocessStackU3Ed__42_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m6F766B569CFE9E96C325BEEC079BE142E366415D (void);
// 0x00000181 System.Void Crosstales.RTVoice.Tool.Paralanguage/<processStack>d__42::System.Collections.IEnumerator.Reset()
extern void U3CprocessStackU3Ed__42_System_Collections_IEnumerator_Reset_m93CF8EC321B392A350E480F18AA6D728FEA88BE6 (void);
// 0x00000182 System.Object Crosstales.RTVoice.Tool.Paralanguage/<processStack>d__42::System.Collections.IEnumerator.get_Current()
extern void U3CprocessStackU3Ed__42_System_Collections_IEnumerator_get_Current_m8EC92988F024B6CA75A7A577E3BAC83ACC67253A (void);
// 0x00000183 Crosstales.RTVoice.Model.Sequence Crosstales.RTVoice.Tool.Sequencer::get_CurrentSequence()
extern void Sequencer_get_CurrentSequence_m165DA6BAA90B207B38946BFA27D9F37ED0257834 (void);
// 0x00000184 System.Void Crosstales.RTVoice.Tool.Sequencer::Start()
extern void Sequencer_Start_mF3DACD10DB960C5F630CA219EA6C878AEB7562D3 (void);
// 0x00000185 System.Void Crosstales.RTVoice.Tool.Sequencer::OnDestroy()
extern void Sequencer_OnDestroy_mAC4A80994005C6E68CCBBA3013D698C77C95B7CD (void);
// 0x00000186 System.Void Crosstales.RTVoice.Tool.Sequencer::OnValidate()
extern void Sequencer_OnValidate_mE2DC066F1B10EAC6BF1784EADF41B25F380E88C7 (void);
// 0x00000187 System.Void Crosstales.RTVoice.Tool.Sequencer::PlaySequence(System.Int32)
extern void Sequencer_PlaySequence_mAE1B49FA4D65D20C05E4529749049A797DB86050 (void);
// 0x00000188 System.Void Crosstales.RTVoice.Tool.Sequencer::PlayNextSequence()
extern void Sequencer_PlayNextSequence_m149D9ADC4468E2A67B6352B1E351E58A948AF1A1 (void);
// 0x00000189 System.Void Crosstales.RTVoice.Tool.Sequencer::PlayAllSequences()
extern void Sequencer_PlayAllSequences_m72925524F5DAEEDC888E007D03F46C5F530ACF01 (void);
// 0x0000018A System.Void Crosstales.RTVoice.Tool.Sequencer::StopAllSequences()
extern void Sequencer_StopAllSequences_m8530D5B34CE98195D0B6541456597570C2166ED8 (void);
// 0x0000018B System.Void Crosstales.RTVoice.Tool.Sequencer::speakCompleteMethod(Crosstales.RTVoice.Model.Wrapper)
extern void Sequencer_speakCompleteMethod_mC8A38A70CB2BC5E9D12824A82DA3548C4EBA45AE (void);
// 0x0000018C System.Void Crosstales.RTVoice.Tool.Sequencer::onVoicesReady()
extern void Sequencer_onVoicesReady_m7C1ADD070CC52CF71F63AC51C6AA531552BEC733 (void);
// 0x0000018D System.Void Crosstales.RTVoice.Tool.Sequencer::play()
extern void Sequencer_play_mAC593BF14E12952E5A8703AD60BE0C3CB2744C61 (void);
// 0x0000018E System.Collections.IEnumerator Crosstales.RTVoice.Tool.Sequencer::playMe(Crosstales.RTVoice.Model.Sequence)
extern void Sequencer_playMe_m092FC62EA99ECB5C44C73C2B37A12683DF2227B2 (void);
// 0x0000018F System.Void Crosstales.RTVoice.Tool.Sequencer::.ctor()
extern void Sequencer__ctor_mB258AC851439B150BEC0E349737D0AC7E3722C16 (void);
// 0x00000190 System.Void Crosstales.RTVoice.Tool.Sequencer/<playMe>d__19::.ctor(System.Int32)
extern void U3CplayMeU3Ed__19__ctor_mA6F984F9A7E5AD2F6BA681C9E7893F88E1C3FBAC (void);
// 0x00000191 System.Void Crosstales.RTVoice.Tool.Sequencer/<playMe>d__19::System.IDisposable.Dispose()
extern void U3CplayMeU3Ed__19_System_IDisposable_Dispose_mCC358E65250BA6032B90AA940898A050746C8DE5 (void);
// 0x00000192 System.Boolean Crosstales.RTVoice.Tool.Sequencer/<playMe>d__19::MoveNext()
extern void U3CplayMeU3Ed__19_MoveNext_mABEBBE14D5EFABD732E01DC8BAFA8EFD31412067 (void);
// 0x00000193 System.Object Crosstales.RTVoice.Tool.Sequencer/<playMe>d__19::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CplayMeU3Ed__19_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m26711DB4168A39C095F497E8F84AAF6FB867E0D6 (void);
// 0x00000194 System.Void Crosstales.RTVoice.Tool.Sequencer/<playMe>d__19::System.Collections.IEnumerator.Reset()
extern void U3CplayMeU3Ed__19_System_Collections_IEnumerator_Reset_mBAA7A47CC030736934F37ED90C374CEC7A67E6FB (void);
// 0x00000195 System.Object Crosstales.RTVoice.Tool.Sequencer/<playMe>d__19::System.Collections.IEnumerator.get_Current()
extern void U3CplayMeU3Ed__19_System_Collections_IEnumerator_get_Current_m4F1613CD9A49140520F720F374C25D5614AABD7F (void);
// 0x00000196 System.Void Crosstales.RTVoice.Tool.SpeechText::add_OnSpeechTextStart(Crosstales.RTVoice.SpeechTextStart)
extern void SpeechText_add_OnSpeechTextStart_mAD88344B3C4EF80BEC85D52219220E9D3979EB5D (void);
// 0x00000197 System.Void Crosstales.RTVoice.Tool.SpeechText::remove_OnSpeechTextStart(Crosstales.RTVoice.SpeechTextStart)
extern void SpeechText_remove_OnSpeechTextStart_m81270B2DC915AD36D9C4DB18113FCBE0CF7985C0 (void);
// 0x00000198 System.Void Crosstales.RTVoice.Tool.SpeechText::add_OnSpeechTextComplete(Crosstales.RTVoice.SpeechTextComplete)
extern void SpeechText_add_OnSpeechTextComplete_m1F5254AE381EB8F6C84D082456F9A6B0D28532A6 (void);
// 0x00000199 System.Void Crosstales.RTVoice.Tool.SpeechText::remove_OnSpeechTextComplete(Crosstales.RTVoice.SpeechTextComplete)
extern void SpeechText_remove_OnSpeechTextComplete_m5DDEAD2CFA71592FB055422E83F3B2C358E12C6C (void);
// 0x0000019A System.String Crosstales.RTVoice.Tool.SpeechText::get_CurrentText()
extern void SpeechText_get_CurrentText_mF23646EF4360713BF46B306E73C757920C3ABD99 (void);
// 0x0000019B System.Void Crosstales.RTVoice.Tool.SpeechText::set_CurrentText(System.String)
extern void SpeechText_set_CurrentText_mA007BBC0D4C7797C050EA0CBF73B3A0420D7DB7F (void);
// 0x0000019C System.Single Crosstales.RTVoice.Tool.SpeechText::get_CurrentRate()
extern void SpeechText_get_CurrentRate_m490404808F7FF657FFE366B3FC5B4D2D452C0645 (void);
// 0x0000019D System.Void Crosstales.RTVoice.Tool.SpeechText::set_CurrentRate(System.Single)
extern void SpeechText_set_CurrentRate_m271B86DD5483CF90D9F1D77A5B6313BD29328B9B (void);
// 0x0000019E System.Single Crosstales.RTVoice.Tool.SpeechText::get_CurrentPitch()
extern void SpeechText_get_CurrentPitch_mDD728ABB6B272F9ABF88A0C1A8B219791A11B1B0 (void);
// 0x0000019F System.Void Crosstales.RTVoice.Tool.SpeechText::set_CurrentPitch(System.Single)
extern void SpeechText_set_CurrentPitch_mF0EB8CDBD300CB2DD93425105438EE834816EA8D (void);
// 0x000001A0 System.Single Crosstales.RTVoice.Tool.SpeechText::get_CurrentVolume()
extern void SpeechText_get_CurrentVolume_mA87324BC6C9B72473982F494C32C0F6D6B795B31 (void);
// 0x000001A1 System.Void Crosstales.RTVoice.Tool.SpeechText::set_CurrentVolume(System.Single)
extern void SpeechText_set_CurrentVolume_m13BE2D7E86E7BD28BC79ED7CA2FA76A54397A00B (void);
// 0x000001A2 System.Void Crosstales.RTVoice.Tool.SpeechText::Start()
extern void SpeechText_Start_m8C188C483652992F937BC258F558CB7EBE5F0C06 (void);
// 0x000001A3 System.Void Crosstales.RTVoice.Tool.SpeechText::OnDestroy()
extern void SpeechText_OnDestroy_m1E44298ABA3163B356887D341BA5B42B831B4CDD (void);
// 0x000001A4 System.Void Crosstales.RTVoice.Tool.SpeechText::OnValidate()
extern void SpeechText_OnValidate_m09A7545866EEBF83089421BDC0FD8B96B85F320C (void);
// 0x000001A5 System.Void Crosstales.RTVoice.Tool.SpeechText::Speak()
extern void SpeechText_Speak_m4A5504A06B27BD4DA5E721B89D78EF14EADFFA67 (void);
// 0x000001A6 System.Void Crosstales.RTVoice.Tool.SpeechText::Silence()
extern void SpeechText_Silence_m41568A6D24DA3AA314307B64AE28F08E9AC693D1 (void);
// 0x000001A7 System.Void Crosstales.RTVoice.Tool.SpeechText::play()
extern void SpeechText_play_mBCA66CD764C335BF15C0A5682BAE390577D78801 (void);
// 0x000001A8 System.Void Crosstales.RTVoice.Tool.SpeechText::onVoicesReady()
extern void SpeechText_onVoicesReady_m1C251F52D3F7C7096EC8F74FB0BCAFB4F74C3AAA (void);
// 0x000001A9 System.Void Crosstales.RTVoice.Tool.SpeechText::onStart()
extern void SpeechText_onStart_mDAFC5680399169C6858DADA533FD0C059B733DF9 (void);
// 0x000001AA System.Void Crosstales.RTVoice.Tool.SpeechText::onComplete()
extern void SpeechText_onComplete_m85781970A6BAABE15C961686CE0F837BFE990BB3 (void);
// 0x000001AB System.Void Crosstales.RTVoice.Tool.SpeechText::.ctor()
extern void SpeechText__ctor_mB661E15424A4A131E95735CB8FC6767E00C4DCA0 (void);
// 0x000001AC System.Single Crosstales.RTVoice.Tool.TextFileSpeaker::get_CurrentRate()
extern void TextFileSpeaker_get_CurrentRate_mD9DDD93CC088991035F4A81830C3ED1219F3D822 (void);
// 0x000001AD System.Void Crosstales.RTVoice.Tool.TextFileSpeaker::set_CurrentRate(System.Single)
extern void TextFileSpeaker_set_CurrentRate_mA62F060BCC54416B5DAFC0B30E05C44D301D2B4E (void);
// 0x000001AE System.Single Crosstales.RTVoice.Tool.TextFileSpeaker::get_CurrentPitch()
extern void TextFileSpeaker_get_CurrentPitch_m6932776785831A7BB3A9EA567D2654ED5A568474 (void);
// 0x000001AF System.Void Crosstales.RTVoice.Tool.TextFileSpeaker::set_CurrentPitch(System.Single)
extern void TextFileSpeaker_set_CurrentPitch_m17B00EC36890AEADCCD703C69EC6C01780691D0F (void);
// 0x000001B0 System.Single Crosstales.RTVoice.Tool.TextFileSpeaker::get_CurrentVolume()
extern void TextFileSpeaker_get_CurrentVolume_m372117338742A92C50B229A03DA48705845904B2 (void);
// 0x000001B1 System.Void Crosstales.RTVoice.Tool.TextFileSpeaker::set_CurrentVolume(System.Single)
extern void TextFileSpeaker_set_CurrentVolume_m32EB142E872E3117873826C8943A65E8740B3896 (void);
// 0x000001B2 System.Void Crosstales.RTVoice.Tool.TextFileSpeaker::Start()
extern void TextFileSpeaker_Start_mA468524FDB99BE2156BC9647644C6A36346B84BF (void);
// 0x000001B3 System.Void Crosstales.RTVoice.Tool.TextFileSpeaker::Update()
extern void TextFileSpeaker_Update_mB426EA52F45C99E4A56EDD2DFA40EF7F2CE5C7AE (void);
// 0x000001B4 System.Void Crosstales.RTVoice.Tool.TextFileSpeaker::OnDestroy()
extern void TextFileSpeaker_OnDestroy_m4D77C0BF73A6296E30AB33F6014AE5F458BBAFAE (void);
// 0x000001B5 System.Void Crosstales.RTVoice.Tool.TextFileSpeaker::OnValidate()
extern void TextFileSpeaker_OnValidate_m78A8539BE1B00CB9FCA80FC62F8325A1C95C6964 (void);
// 0x000001B6 System.Void Crosstales.RTVoice.Tool.TextFileSpeaker::SpeakAll()
extern void TextFileSpeaker_SpeakAll_m1FE4226A4E4C57AA32ACFFFD2367D862F2BE2286 (void);
// 0x000001B7 System.Void Crosstales.RTVoice.Tool.TextFileSpeaker::StopAll()
extern void TextFileSpeaker_StopAll_mF9842DA4C01EDF7F3EC957E4E3AA8787BCB2113E (void);
// 0x000001B8 System.Void Crosstales.RTVoice.Tool.TextFileSpeaker::Next()
extern void TextFileSpeaker_Next_m0E079E3E91389F0BC292AD9B4B2A09C31B4E80B8 (void);
// 0x000001B9 System.Void Crosstales.RTVoice.Tool.TextFileSpeaker::Next(System.Boolean)
extern void TextFileSpeaker_Next_m1BF65C69162E6E4478DF3A6F3E22E5393A64C9B2 (void);
// 0x000001BA System.Void Crosstales.RTVoice.Tool.TextFileSpeaker::Previous()
extern void TextFileSpeaker_Previous_m2CACCCAD153146C73ED3C4E1850F318E1B2C3577 (void);
// 0x000001BB System.Void Crosstales.RTVoice.Tool.TextFileSpeaker::Previous(System.Boolean)
extern void TextFileSpeaker_Previous_m9B6391CAAA1D7BB858AC7F778EDBDBC25B85CD1C (void);
// 0x000001BC System.Void Crosstales.RTVoice.Tool.TextFileSpeaker::Speak()
extern void TextFileSpeaker_Speak_m89E4A18C148800BD33D4FAA81A0B3B5FB41FCC1D (void);
// 0x000001BD System.String Crosstales.RTVoice.Tool.TextFileSpeaker::SpeakText(System.Int32,System.Boolean)
extern void TextFileSpeaker_SpeakText_m754DE1D8064123172780DC3220424E743435322C (void);
// 0x000001BE System.Void Crosstales.RTVoice.Tool.TextFileSpeaker::Silence()
extern void TextFileSpeaker_Silence_mA210390D8CFADD1D3354692A0DCD296FF0B15710 (void);
// 0x000001BF System.Void Crosstales.RTVoice.Tool.TextFileSpeaker::Reload()
extern void TextFileSpeaker_Reload_m771FF796D776A7EFCE9E16892921F81A142CA8C1 (void);
// 0x000001C0 System.Void Crosstales.RTVoice.Tool.TextFileSpeaker::play()
extern void TextFileSpeaker_play_m236C38EF46FCD8C13D5891706C63154EA4CD762E (void);
// 0x000001C1 System.String Crosstales.RTVoice.Tool.TextFileSpeaker::speak(System.String)
extern void TextFileSpeaker_speak_m92F3BF16F09D64137208DDFBC6A9068BE5EA043D (void);
// 0x000001C2 System.Void Crosstales.RTVoice.Tool.TextFileSpeaker::onVoicesReady()
extern void TextFileSpeaker_onVoicesReady_mDA98A67CA24548ADF92512834891640FD13A12EF (void);
// 0x000001C3 System.Void Crosstales.RTVoice.Tool.TextFileSpeaker::onSpeakComplete(Crosstales.RTVoice.Model.Wrapper)
extern void TextFileSpeaker_onSpeakComplete_mE0F0772D538D81C9B60B13BFFAB1DB7E7DB8D7A3 (void);
// 0x000001C4 System.Void Crosstales.RTVoice.Tool.TextFileSpeaker::.ctor()
extern void TextFileSpeaker__ctor_m261D534F0FF01614BB4B24A764860B675DE9191B (void);
// 0x000001C5 System.Void Crosstales.RTVoice.Tool.TextFileSpeaker::.cctor()
extern void TextFileSpeaker__cctor_mABDA00E3F77458593AAB6B3A61E17B516D516628 (void);
// 0x000001C6 System.Void Crosstales.RTVoice.Tool.VoiceInitalizer::Start()
extern void VoiceInitalizer_Start_m10C8280AD42B10BD76A26995E6163A0223168881 (void);
// 0x000001C7 System.Void Crosstales.RTVoice.Tool.VoiceInitalizer::OnEnable()
extern void VoiceInitalizer_OnEnable_m4E4E62165F7BCFCBA92597936CCB30D97B4FA0C2 (void);
// 0x000001C8 System.Void Crosstales.RTVoice.Tool.VoiceInitalizer::OnDisable()
extern void VoiceInitalizer_OnDisable_m2A51022B621ECBC975902E71B0EA71164D79F113 (void);
// 0x000001C9 System.Collections.IEnumerator Crosstales.RTVoice.Tool.VoiceInitalizer::initalizeVoices()
extern void VoiceInitalizer_initalizeVoices_m00AE8AD949E2048D303BF3DCB9AFC0BF91CC35AF (void);
// 0x000001CA System.Void Crosstales.RTVoice.Tool.VoiceInitalizer::onVoicesReady()
extern void VoiceInitalizer_onVoicesReady_mDC3C534FACA43EC385F58A72A849D219DB25B0C0 (void);
// 0x000001CB System.Void Crosstales.RTVoice.Tool.VoiceInitalizer::onSpeakComplete(Crosstales.RTVoice.Model.Wrapper)
extern void VoiceInitalizer_onSpeakComplete_mF0619CC560466EDDCF6AADD4DB227EA7372A2BEA (void);
// 0x000001CC System.Void Crosstales.RTVoice.Tool.VoiceInitalizer::.ctor()
extern void VoiceInitalizer__ctor_mA883BEC77DF65B56E599EDBA9AD26D3A97B01D4E (void);
// 0x000001CD System.Void Crosstales.RTVoice.Tool.VoiceInitalizer/<initalizeVoices>d__10::.ctor(System.Int32)
extern void U3CinitalizeVoicesU3Ed__10__ctor_m8362902403B1FE161F594248B8AE6732B925DCBD (void);
// 0x000001CE System.Void Crosstales.RTVoice.Tool.VoiceInitalizer/<initalizeVoices>d__10::System.IDisposable.Dispose()
extern void U3CinitalizeVoicesU3Ed__10_System_IDisposable_Dispose_mF897C063121C49E81290DF42E4FB044C856A921F (void);
// 0x000001CF System.Boolean Crosstales.RTVoice.Tool.VoiceInitalizer/<initalizeVoices>d__10::MoveNext()
extern void U3CinitalizeVoicesU3Ed__10_MoveNext_m248C667C6E400CDC52E41C8598C7AEC009BE21E4 (void);
// 0x000001D0 System.Void Crosstales.RTVoice.Tool.VoiceInitalizer/<initalizeVoices>d__10::<>m__Finally1()
extern void U3CinitalizeVoicesU3Ed__10_U3CU3Em__Finally1_mBEB50D2E9EEB95E844E4B33F86727F24D4DFC403 (void);
// 0x000001D1 System.Object Crosstales.RTVoice.Tool.VoiceInitalizer/<initalizeVoices>d__10::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CinitalizeVoicesU3Ed__10_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m19AB22AD386CF1E797DEF859EBA73F5980575238 (void);
// 0x000001D2 System.Void Crosstales.RTVoice.Tool.VoiceInitalizer/<initalizeVoices>d__10::System.Collections.IEnumerator.Reset()
extern void U3CinitalizeVoicesU3Ed__10_System_Collections_IEnumerator_Reset_mA2AC727EFF62D2404A9D9E0A44431EB15FA6E21D (void);
// 0x000001D3 System.Object Crosstales.RTVoice.Tool.VoiceInitalizer/<initalizeVoices>d__10::System.Collections.IEnumerator.get_Current()
extern void U3CinitalizeVoicesU3Ed__10_System_Collections_IEnumerator_get_Current_m6EE41A71936F6DC8E38DE9780509A78D505FC0B2 (void);
// 0x000001D4 System.Void Crosstales.RTVoice.Provider.BaseCustomVoiceProvider::add_OnVoicesReady(Crosstales.RTVoice.VoicesReady)
extern void BaseCustomVoiceProvider_add_OnVoicesReady_m1DBC969BFDFB36363B0662FEC73BE5D26B3E90BB (void);
// 0x000001D5 System.Void Crosstales.RTVoice.Provider.BaseCustomVoiceProvider::remove_OnVoicesReady(Crosstales.RTVoice.VoicesReady)
extern void BaseCustomVoiceProvider_remove_OnVoicesReady_m5AD8F56BCA316037FE4793965A25A3121DEDB19E (void);
// 0x000001D6 System.Void Crosstales.RTVoice.Provider.BaseCustomVoiceProvider::add_OnSpeakStart(Crosstales.RTVoice.SpeakStart)
extern void BaseCustomVoiceProvider_add_OnSpeakStart_m8164FE728625CB0C2353BC240C2653584938C83E (void);
// 0x000001D7 System.Void Crosstales.RTVoice.Provider.BaseCustomVoiceProvider::remove_OnSpeakStart(Crosstales.RTVoice.SpeakStart)
extern void BaseCustomVoiceProvider_remove_OnSpeakStart_m283D1213D52EE7CFB76D02BFEC06DEFCC310789A (void);
// 0x000001D8 System.Void Crosstales.RTVoice.Provider.BaseCustomVoiceProvider::add_OnSpeakComplete(Crosstales.RTVoice.SpeakComplete)
extern void BaseCustomVoiceProvider_add_OnSpeakComplete_m2E1EA3C5C95BFDB3A9DA52153C6CFC7E154C16F2 (void);
// 0x000001D9 System.Void Crosstales.RTVoice.Provider.BaseCustomVoiceProvider::remove_OnSpeakComplete(Crosstales.RTVoice.SpeakComplete)
extern void BaseCustomVoiceProvider_remove_OnSpeakComplete_m54921CDA4F22A257902256CB4DD4AEB1095553BB (void);
// 0x000001DA System.Void Crosstales.RTVoice.Provider.BaseCustomVoiceProvider::add_OnSpeakCurrentWord(Crosstales.RTVoice.SpeakCurrentWord)
extern void BaseCustomVoiceProvider_add_OnSpeakCurrentWord_mBE89E0EBC3C734ED0BB1A58082AE892C537A947A (void);
// 0x000001DB System.Void Crosstales.RTVoice.Provider.BaseCustomVoiceProvider::remove_OnSpeakCurrentWord(Crosstales.RTVoice.SpeakCurrentWord)
extern void BaseCustomVoiceProvider_remove_OnSpeakCurrentWord_m66C2A70C8BC183FF0E67B05042A34EE8F87AD536 (void);
// 0x000001DC System.Void Crosstales.RTVoice.Provider.BaseCustomVoiceProvider::add_OnSpeakCurrentPhoneme(Crosstales.RTVoice.SpeakCurrentPhoneme)
extern void BaseCustomVoiceProvider_add_OnSpeakCurrentPhoneme_mF89C0103517EA50DA7462C30A4B7F57F6161C5CF (void);
// 0x000001DD System.Void Crosstales.RTVoice.Provider.BaseCustomVoiceProvider::remove_OnSpeakCurrentPhoneme(Crosstales.RTVoice.SpeakCurrentPhoneme)
extern void BaseCustomVoiceProvider_remove_OnSpeakCurrentPhoneme_m813D4FDA9FED7356D6CF8602D89CACC5FE6A5AB3 (void);
// 0x000001DE System.Void Crosstales.RTVoice.Provider.BaseCustomVoiceProvider::add_OnSpeakCurrentViseme(Crosstales.RTVoice.SpeakCurrentViseme)
extern void BaseCustomVoiceProvider_add_OnSpeakCurrentViseme_mCD2570C41EA9653F2368C36AEC967F25D6A11BF9 (void);
// 0x000001DF System.Void Crosstales.RTVoice.Provider.BaseCustomVoiceProvider::remove_OnSpeakCurrentViseme(Crosstales.RTVoice.SpeakCurrentViseme)
extern void BaseCustomVoiceProvider_remove_OnSpeakCurrentViseme_m87CE0C1717B04036D67535E202A945A30F6647B1 (void);
// 0x000001E0 System.Void Crosstales.RTVoice.Provider.BaseCustomVoiceProvider::add_OnSpeakAudioGenerationStart(Crosstales.RTVoice.SpeakAudioGenerationStart)
extern void BaseCustomVoiceProvider_add_OnSpeakAudioGenerationStart_m0BB94BA2E23782906DB3595530F8410395220626 (void);
// 0x000001E1 System.Void Crosstales.RTVoice.Provider.BaseCustomVoiceProvider::remove_OnSpeakAudioGenerationStart(Crosstales.RTVoice.SpeakAudioGenerationStart)
extern void BaseCustomVoiceProvider_remove_OnSpeakAudioGenerationStart_m6C21ACF04D5CF0451DE8AF5E6A66736856726470 (void);
// 0x000001E2 System.Void Crosstales.RTVoice.Provider.BaseCustomVoiceProvider::add_OnSpeakAudioGenerationComplete(Crosstales.RTVoice.SpeakAudioGenerationComplete)
extern void BaseCustomVoiceProvider_add_OnSpeakAudioGenerationComplete_m3C863C4E5619F1A7DE1AEA35BFB1531CAA323C5B (void);
// 0x000001E3 System.Void Crosstales.RTVoice.Provider.BaseCustomVoiceProvider::remove_OnSpeakAudioGenerationComplete(Crosstales.RTVoice.SpeakAudioGenerationComplete)
extern void BaseCustomVoiceProvider_remove_OnSpeakAudioGenerationComplete_m6666CD2372FD9020154D8265C12E56BE158459D1 (void);
// 0x000001E4 System.Void Crosstales.RTVoice.Provider.BaseCustomVoiceProvider::add_OnErrorInfo(Crosstales.RTVoice.ErrorInfo)
extern void BaseCustomVoiceProvider_add_OnErrorInfo_m4039CA411FABF95B6F72106FA3165763D853FF63 (void);
// 0x000001E5 System.Void Crosstales.RTVoice.Provider.BaseCustomVoiceProvider::remove_OnErrorInfo(Crosstales.RTVoice.ErrorInfo)
extern void BaseCustomVoiceProvider_remove_OnErrorInfo_m91F263673F0367F91FC819ECF70DE7AE3F9F0F4B (void);
// 0x000001E6 System.Boolean Crosstales.RTVoice.Provider.BaseCustomVoiceProvider::get_isActive()
extern void BaseCustomVoiceProvider_get_isActive_mBCBDA0BDC2ED2215BE880B27C35E9D3CB4225B7C (void);
// 0x000001E7 System.Void Crosstales.RTVoice.Provider.BaseCustomVoiceProvider::set_isActive(System.Boolean)
extern void BaseCustomVoiceProvider_set_isActive_m1B7ACF576574DF723169DF6BEBDA36DD27C09B0D (void);
// 0x000001E8 System.String Crosstales.RTVoice.Provider.BaseCustomVoiceProvider::get_AudioFileExtension()
// 0x000001E9 UnityEngine.AudioType Crosstales.RTVoice.Provider.BaseCustomVoiceProvider::get_AudioFileType()
// 0x000001EA System.String Crosstales.RTVoice.Provider.BaseCustomVoiceProvider::get_DefaultVoiceName()
// 0x000001EB System.Collections.Generic.List`1<Crosstales.RTVoice.Model.Voice> Crosstales.RTVoice.Provider.BaseCustomVoiceProvider::get_Voices()
extern void BaseCustomVoiceProvider_get_Voices_mA1D46DD83506359EC780D723214381023E42F1AF (void);
// 0x000001EC System.Boolean Crosstales.RTVoice.Provider.BaseCustomVoiceProvider::get_isWorkingInEditor()
// 0x000001ED System.Boolean Crosstales.RTVoice.Provider.BaseCustomVoiceProvider::get_isWorkingInPlaymode()
// 0x000001EE System.Int32 Crosstales.RTVoice.Provider.BaseCustomVoiceProvider::get_MaxTextLength()
// 0x000001EF System.Boolean Crosstales.RTVoice.Provider.BaseCustomVoiceProvider::get_isSpeakNativeSupported()
// 0x000001F0 System.Boolean Crosstales.RTVoice.Provider.BaseCustomVoiceProvider::get_isSpeakSupported()
// 0x000001F1 System.Boolean Crosstales.RTVoice.Provider.BaseCustomVoiceProvider::get_isPlatformSupported()
// 0x000001F2 System.Boolean Crosstales.RTVoice.Provider.BaseCustomVoiceProvider::get_isSSMLSupported()
// 0x000001F3 System.Boolean Crosstales.RTVoice.Provider.BaseCustomVoiceProvider::get_isOnlineService()
// 0x000001F4 System.Boolean Crosstales.RTVoice.Provider.BaseCustomVoiceProvider::get_hasCoRoutines()
// 0x000001F5 System.Boolean Crosstales.RTVoice.Provider.BaseCustomVoiceProvider::get_isIL2CPPSupported()
// 0x000001F6 System.Collections.Generic.List`1<System.String> Crosstales.RTVoice.Provider.BaseCustomVoiceProvider::get_Cultures()
extern void BaseCustomVoiceProvider_get_Cultures_m3AFEA7ADEEF9200842E2E7841FB434C478477A93 (void);
// 0x000001F7 System.Void Crosstales.RTVoice.Provider.BaseCustomVoiceProvider::Silence()
extern void BaseCustomVoiceProvider_Silence_m1E979BCAA78AF26BCB1854B164BE44B1C6BA92A6 (void);
// 0x000001F8 System.Void Crosstales.RTVoice.Provider.BaseCustomVoiceProvider::Silence(System.String)
extern void BaseCustomVoiceProvider_Silence_m93D3AC462E51C6FDE79F805EA6B21204A4E53E44 (void);
// 0x000001F9 System.Collections.IEnumerator Crosstales.RTVoice.Provider.BaseCustomVoiceProvider::SpeakNative(Crosstales.RTVoice.Model.Wrapper)
// 0x000001FA System.Collections.IEnumerator Crosstales.RTVoice.Provider.BaseCustomVoiceProvider::Speak(Crosstales.RTVoice.Model.Wrapper)
// 0x000001FB System.Collections.IEnumerator Crosstales.RTVoice.Provider.BaseCustomVoiceProvider::Generate(Crosstales.RTVoice.Model.Wrapper)
// 0x000001FC System.Void Crosstales.RTVoice.Provider.BaseCustomVoiceProvider::Load()
// 0x000001FD System.String Crosstales.RTVoice.Provider.BaseCustomVoiceProvider::getOutputFile(System.String,System.Boolean)
extern void BaseCustomVoiceProvider_getOutputFile_mF6C273D1AE2C6A02E124F3D658977AB654E6D7EC (void);
// 0x000001FE System.Collections.IEnumerator Crosstales.RTVoice.Provider.BaseCustomVoiceProvider::playAudioFile(Crosstales.RTVoice.Model.Wrapper,System.String,System.String,UnityEngine.AudioType,System.Boolean,System.Boolean,System.Collections.Generic.Dictionary`2<System.String,System.String>)
extern void BaseCustomVoiceProvider_playAudioFile_mFF2435C8CA85BA9B44A9967159EE60E7F1E38EF3 (void);
// 0x000001FF System.Void Crosstales.RTVoice.Provider.BaseCustomVoiceProvider::copyAudioFile(Crosstales.RTVoice.Model.Wrapper,System.String,System.Boolean,System.Byte[])
extern void BaseCustomVoiceProvider_copyAudioFile_m870E1A8C6BDEBA932F91693AA0A81F5CCF6470B2 (void);
// 0x00000200 System.Void Crosstales.RTVoice.Provider.BaseCustomVoiceProvider::processAudioFile(Crosstales.RTVoice.Model.Wrapper,System.String,System.Boolean,System.Byte[])
extern void BaseCustomVoiceProvider_processAudioFile_m2BA3FA65EBBEF4B1E49C6353E8BE4ADC9147DFD3 (void);
// 0x00000201 System.String Crosstales.RTVoice.Provider.BaseCustomVoiceProvider::getVoiceName(Crosstales.RTVoice.Model.Wrapper)
extern void BaseCustomVoiceProvider_getVoiceName_mBA076CF3753EBBBE07BA70C7E3F24017047182A1 (void);
// 0x00000202 System.Void Crosstales.RTVoice.Provider.BaseCustomVoiceProvider::onVoicesReady()
extern void BaseCustomVoiceProvider_onVoicesReady_m7C01DF04455E10E65021B0C3C6DCF62E7C89B1AE (void);
// 0x00000203 System.Void Crosstales.RTVoice.Provider.BaseCustomVoiceProvider::onSpeakStart(Crosstales.RTVoice.Model.Wrapper)
extern void BaseCustomVoiceProvider_onSpeakStart_m1A8523E4B5E8B727CA4F7779780A8C3974D4C100 (void);
// 0x00000204 System.Void Crosstales.RTVoice.Provider.BaseCustomVoiceProvider::onSpeakComplete(Crosstales.RTVoice.Model.Wrapper)
extern void BaseCustomVoiceProvider_onSpeakComplete_m7FB4B461E34757D57143B719E58360AD216C2A5A (void);
// 0x00000205 System.Void Crosstales.RTVoice.Provider.BaseCustomVoiceProvider::onSpeakCurrentWord(Crosstales.RTVoice.Model.Wrapper,System.String[],System.Int32)
extern void BaseCustomVoiceProvider_onSpeakCurrentWord_mB1ADF0ABF72C7E77958EA06D496B7157452B4BDA (void);
// 0x00000206 System.Void Crosstales.RTVoice.Provider.BaseCustomVoiceProvider::onSpeakCurrentPhoneme(Crosstales.RTVoice.Model.Wrapper,System.String)
extern void BaseCustomVoiceProvider_onSpeakCurrentPhoneme_m983DB99489CC88E37B88E8D37CD7C0AFFF237E29 (void);
// 0x00000207 System.Void Crosstales.RTVoice.Provider.BaseCustomVoiceProvider::onSpeakCurrentViseme(Crosstales.RTVoice.Model.Wrapper,System.String)
extern void BaseCustomVoiceProvider_onSpeakCurrentViseme_mEFE079A1160A431AF2FE3E025017A7928D849DA9 (void);
// 0x00000208 System.Void Crosstales.RTVoice.Provider.BaseCustomVoiceProvider::onSpeakAudioGenerationStart(Crosstales.RTVoice.Model.Wrapper)
extern void BaseCustomVoiceProvider_onSpeakAudioGenerationStart_m49535103542C77ACB83FCE1CF694E3AE5D23628B (void);
// 0x00000209 System.Void Crosstales.RTVoice.Provider.BaseCustomVoiceProvider::onSpeakAudioGenerationComplete(Crosstales.RTVoice.Model.Wrapper)
extern void BaseCustomVoiceProvider_onSpeakAudioGenerationComplete_mC680058C11F3A6D69E259CFFC3BBF87A0D56555B (void);
// 0x0000020A System.Void Crosstales.RTVoice.Provider.BaseCustomVoiceProvider::onErrorInfo(Crosstales.RTVoice.Model.Wrapper,System.String)
extern void BaseCustomVoiceProvider_onErrorInfo_mA9F2B8A482D2243D868299732C0F7E940FBFB37B (void);
// 0x0000020B System.Void Crosstales.RTVoice.Provider.BaseCustomVoiceProvider::.ctor()
extern void BaseCustomVoiceProvider__ctor_m3A0A015610C3C458916A01B22FBC4042595CD516 (void);
// 0x0000020C System.Void Crosstales.RTVoice.Provider.BaseCustomVoiceProvider::.cctor()
extern void BaseCustomVoiceProvider__cctor_m901EB50445E450A23463958F1B61C448B468F45A (void);
// 0x0000020D System.Void Crosstales.RTVoice.Provider.BaseCustomVoiceProvider/<>c::.cctor()
extern void U3CU3Ec__cctor_m13F9C9CC09AA996D96695EE2E6499C5844EB1C59 (void);
// 0x0000020E System.Void Crosstales.RTVoice.Provider.BaseCustomVoiceProvider/<>c::.ctor()
extern void U3CU3Ec__ctor_m2E80DCDED82CE2B83015A55142F93173880BC68B (void);
// 0x0000020F System.String Crosstales.RTVoice.Provider.BaseCustomVoiceProvider/<>c::<get_Cultures>b__73_0(Crosstales.RTVoice.Model.Voice)
extern void U3CU3Ec_U3Cget_CulturesU3Eb__73_0_m0C447A7328552F4245200CE176369BAA5BEB45FF (void);
// 0x00000210 Crosstales.RTVoice.Model.Voice Crosstales.RTVoice.Provider.BaseCustomVoiceProvider/<>c::<get_Cultures>b__73_1(System.Linq.IGrouping`2<System.String,Crosstales.RTVoice.Model.Voice>)
extern void U3CU3Ec_U3Cget_CulturesU3Eb__73_1_m30CE901560AC4A64345151AF668327079181D3FC (void);
// 0x00000211 System.String Crosstales.RTVoice.Provider.BaseCustomVoiceProvider/<>c::<get_Cultures>b__73_2(Crosstales.RTVoice.Model.Voice)
extern void U3CU3Ec_U3Cget_CulturesU3Eb__73_2_m9232C4822D48244E9FA771AE554B597C93CBDA1B (void);
// 0x00000212 System.Void Crosstales.RTVoice.Provider.BaseCustomVoiceProvider/<playAudioFile>d__81::.ctor(System.Int32)
extern void U3CplayAudioFileU3Ed__81__ctor_m12F9909D1F5B37CBBDF5768383EE59A10995FCA5 (void);
// 0x00000213 System.Void Crosstales.RTVoice.Provider.BaseCustomVoiceProvider/<playAudioFile>d__81::System.IDisposable.Dispose()
extern void U3CplayAudioFileU3Ed__81_System_IDisposable_Dispose_m8F70B0ED165D9F8F2C0F4ED496647D22AE55E010 (void);
// 0x00000214 System.Boolean Crosstales.RTVoice.Provider.BaseCustomVoiceProvider/<playAudioFile>d__81::MoveNext()
extern void U3CplayAudioFileU3Ed__81_MoveNext_mDE8FB54C3A65298993FE92ADA5B3A186C31B8118 (void);
// 0x00000215 System.Void Crosstales.RTVoice.Provider.BaseCustomVoiceProvider/<playAudioFile>d__81::<>m__Finally1()
extern void U3CplayAudioFileU3Ed__81_U3CU3Em__Finally1_mCBF2F2BEB5CB7094AB9CF96A530D80922B705751 (void);
// 0x00000216 System.Object Crosstales.RTVoice.Provider.BaseCustomVoiceProvider/<playAudioFile>d__81::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CplayAudioFileU3Ed__81_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mBD68E65ACF9A83E963B49E130B6E07ADB5D4FA19 (void);
// 0x00000217 System.Void Crosstales.RTVoice.Provider.BaseCustomVoiceProvider/<playAudioFile>d__81::System.Collections.IEnumerator.Reset()
extern void U3CplayAudioFileU3Ed__81_System_Collections_IEnumerator_Reset_mFA6ACB523E034EF1620981E29B012849F698D0C5 (void);
// 0x00000218 System.Object Crosstales.RTVoice.Provider.BaseCustomVoiceProvider/<playAudioFile>d__81::System.Collections.IEnumerator.get_Current()
extern void U3CplayAudioFileU3Ed__81_System_Collections_IEnumerator_get_Current_m6007D9DD5B76EFE943D370BBEC3A868B0447FDF6 (void);
// 0x00000219 System.Void Crosstales.RTVoice.Provider.BaseVoiceProvider::.ctor(UnityEngine.MonoBehaviour)
extern void BaseVoiceProvider__ctor_m02659306825EE660C477BC9F972DEF2F74B231D8 (void);
// 0x0000021A System.Void Crosstales.RTVoice.Provider.BaseVoiceProvider::add_OnVoicesReady(Crosstales.RTVoice.VoicesReady)
extern void BaseVoiceProvider_add_OnVoicesReady_mA4ED9500F520BD673FB32328663F78E779E835FA (void);
// 0x0000021B System.Void Crosstales.RTVoice.Provider.BaseVoiceProvider::remove_OnVoicesReady(Crosstales.RTVoice.VoicesReady)
extern void BaseVoiceProvider_remove_OnVoicesReady_m7AFA97326057C39069AAC20F504C2E1048D87E4C (void);
// 0x0000021C System.Void Crosstales.RTVoice.Provider.BaseVoiceProvider::add_OnSpeakStart(Crosstales.RTVoice.SpeakStart)
extern void BaseVoiceProvider_add_OnSpeakStart_m5299B03A0A29E73E8EA6AFA3086899032A585552 (void);
// 0x0000021D System.Void Crosstales.RTVoice.Provider.BaseVoiceProvider::remove_OnSpeakStart(Crosstales.RTVoice.SpeakStart)
extern void BaseVoiceProvider_remove_OnSpeakStart_m9D1E027A6825CAAA75B55513A9E65E7397CF18D1 (void);
// 0x0000021E System.Void Crosstales.RTVoice.Provider.BaseVoiceProvider::add_OnSpeakComplete(Crosstales.RTVoice.SpeakComplete)
extern void BaseVoiceProvider_add_OnSpeakComplete_m772B02A28442CB9225486190186E89C5C3F89139 (void);
// 0x0000021F System.Void Crosstales.RTVoice.Provider.BaseVoiceProvider::remove_OnSpeakComplete(Crosstales.RTVoice.SpeakComplete)
extern void BaseVoiceProvider_remove_OnSpeakComplete_m1D4D23293E2FA9CEC748C8D60E376946A52FFF4C (void);
// 0x00000220 System.Void Crosstales.RTVoice.Provider.BaseVoiceProvider::add_OnSpeakCurrentWord(Crosstales.RTVoice.SpeakCurrentWord)
extern void BaseVoiceProvider_add_OnSpeakCurrentWord_mF2E4791EA8FE8930CD84C5665F9BBDD19FBC61A7 (void);
// 0x00000221 System.Void Crosstales.RTVoice.Provider.BaseVoiceProvider::remove_OnSpeakCurrentWord(Crosstales.RTVoice.SpeakCurrentWord)
extern void BaseVoiceProvider_remove_OnSpeakCurrentWord_mA903F36E19B3F8B49DB4C34C460F96FDCDB9401D (void);
// 0x00000222 System.Void Crosstales.RTVoice.Provider.BaseVoiceProvider::add_OnSpeakCurrentPhoneme(Crosstales.RTVoice.SpeakCurrentPhoneme)
extern void BaseVoiceProvider_add_OnSpeakCurrentPhoneme_m78B03CFC4D891A3BC50FA558EAAD4DA9F6E11BF7 (void);
// 0x00000223 System.Void Crosstales.RTVoice.Provider.BaseVoiceProvider::remove_OnSpeakCurrentPhoneme(Crosstales.RTVoice.SpeakCurrentPhoneme)
extern void BaseVoiceProvider_remove_OnSpeakCurrentPhoneme_m78A32986E30B2B02902A083841A55DA19D490E27 (void);
// 0x00000224 System.Void Crosstales.RTVoice.Provider.BaseVoiceProvider::add_OnSpeakCurrentViseme(Crosstales.RTVoice.SpeakCurrentViseme)
extern void BaseVoiceProvider_add_OnSpeakCurrentViseme_m7D02269CC0039EF4783204CD8407CB4DF9C717AD (void);
// 0x00000225 System.Void Crosstales.RTVoice.Provider.BaseVoiceProvider::remove_OnSpeakCurrentViseme(Crosstales.RTVoice.SpeakCurrentViseme)
extern void BaseVoiceProvider_remove_OnSpeakCurrentViseme_mD5CCC83164B03252886CB34DC6BAB3114B247D9C (void);
// 0x00000226 System.Void Crosstales.RTVoice.Provider.BaseVoiceProvider::add_OnSpeakAudioGenerationStart(Crosstales.RTVoice.SpeakAudioGenerationStart)
extern void BaseVoiceProvider_add_OnSpeakAudioGenerationStart_mC0427F55B3A430AF8FFD1A15450FC0ADFC47B5F1 (void);
// 0x00000227 System.Void Crosstales.RTVoice.Provider.BaseVoiceProvider::remove_OnSpeakAudioGenerationStart(Crosstales.RTVoice.SpeakAudioGenerationStart)
extern void BaseVoiceProvider_remove_OnSpeakAudioGenerationStart_m239E423226B24DEDCD263BD2C1A8DF98C5DEAC36 (void);
// 0x00000228 System.Void Crosstales.RTVoice.Provider.BaseVoiceProvider::add_OnSpeakAudioGenerationComplete(Crosstales.RTVoice.SpeakAudioGenerationComplete)
extern void BaseVoiceProvider_add_OnSpeakAudioGenerationComplete_m3968C747AF6BCCBC2949A0A091D185119E55A5DC (void);
// 0x00000229 System.Void Crosstales.RTVoice.Provider.BaseVoiceProvider::remove_OnSpeakAudioGenerationComplete(Crosstales.RTVoice.SpeakAudioGenerationComplete)
extern void BaseVoiceProvider_remove_OnSpeakAudioGenerationComplete_mFD8880E0535CF6ED767D30F7F6377D8A1BEFCB1B (void);
// 0x0000022A System.Void Crosstales.RTVoice.Provider.BaseVoiceProvider::add_OnErrorInfo(Crosstales.RTVoice.ErrorInfo)
extern void BaseVoiceProvider_add_OnErrorInfo_m164FE7A339F2667D1C0B2A4F337606ABDAAEF695 (void);
// 0x0000022B System.Void Crosstales.RTVoice.Provider.BaseVoiceProvider::remove_OnErrorInfo(Crosstales.RTVoice.ErrorInfo)
extern void BaseVoiceProvider_remove_OnErrorInfo_m3ABA070A4209EC527ED0C80A7A4C6298544EB184 (void);
// 0x0000022C System.String Crosstales.RTVoice.Provider.BaseVoiceProvider::get_AudioFileExtension()
// 0x0000022D UnityEngine.AudioType Crosstales.RTVoice.Provider.BaseVoiceProvider::get_AudioFileType()
// 0x0000022E System.String Crosstales.RTVoice.Provider.BaseVoiceProvider::get_DefaultVoiceName()
// 0x0000022F System.Collections.Generic.List`1<Crosstales.RTVoice.Model.Voice> Crosstales.RTVoice.Provider.BaseVoiceProvider::get_Voices()
extern void BaseVoiceProvider_get_Voices_m52402F6778A5CF34DFEE6E35BF2424466000C915 (void);
// 0x00000230 System.Boolean Crosstales.RTVoice.Provider.BaseVoiceProvider::get_isWorkingInEditor()
// 0x00000231 System.Boolean Crosstales.RTVoice.Provider.BaseVoiceProvider::get_isWorkingInPlaymode()
// 0x00000232 System.Int32 Crosstales.RTVoice.Provider.BaseVoiceProvider::get_MaxTextLength()
// 0x00000233 System.Boolean Crosstales.RTVoice.Provider.BaseVoiceProvider::get_isSpeakNativeSupported()
// 0x00000234 System.Boolean Crosstales.RTVoice.Provider.BaseVoiceProvider::get_isSpeakSupported()
// 0x00000235 System.Boolean Crosstales.RTVoice.Provider.BaseVoiceProvider::get_isPlatformSupported()
// 0x00000236 System.Boolean Crosstales.RTVoice.Provider.BaseVoiceProvider::get_isSSMLSupported()
// 0x00000237 System.Boolean Crosstales.RTVoice.Provider.BaseVoiceProvider::get_isOnlineService()
// 0x00000238 System.Boolean Crosstales.RTVoice.Provider.BaseVoiceProvider::get_hasCoRoutines()
// 0x00000239 System.Boolean Crosstales.RTVoice.Provider.BaseVoiceProvider::get_isIL2CPPSupported()
// 0x0000023A System.Collections.Generic.List`1<System.String> Crosstales.RTVoice.Provider.BaseVoiceProvider::get_Cultures()
extern void BaseVoiceProvider_get_Cultures_mAF424DC51DE1BF5DD5E06C2EBC7897B38E89DA83 (void);
// 0x0000023B System.Void Crosstales.RTVoice.Provider.BaseVoiceProvider::Silence()
extern void BaseVoiceProvider_Silence_m286B3E6929DA4ECEE736E82A13538129446F7DFE (void);
// 0x0000023C System.Void Crosstales.RTVoice.Provider.BaseVoiceProvider::Silence(System.String)
extern void BaseVoiceProvider_Silence_m7B761381D853E888C6B2BD812FCE42A6B9B08675 (void);
// 0x0000023D System.Collections.IEnumerator Crosstales.RTVoice.Provider.BaseVoiceProvider::SpeakNative(Crosstales.RTVoice.Model.Wrapper)
// 0x0000023E System.Collections.IEnumerator Crosstales.RTVoice.Provider.BaseVoiceProvider::Speak(Crosstales.RTVoice.Model.Wrapper)
// 0x0000023F System.Collections.IEnumerator Crosstales.RTVoice.Provider.BaseVoiceProvider::Generate(Crosstales.RTVoice.Model.Wrapper)
// 0x00000240 System.String Crosstales.RTVoice.Provider.BaseVoiceProvider::getOutputFile(System.String,System.Boolean)
extern void BaseVoiceProvider_getOutputFile_m19D481792D3E2DF47E31529E31C6599827E02E1A (void);
// 0x00000241 System.Collections.IEnumerator Crosstales.RTVoice.Provider.BaseVoiceProvider::playAudioFile(Crosstales.RTVoice.Model.Wrapper,System.String,System.String,UnityEngine.AudioType,System.Boolean,System.Boolean,System.Collections.Generic.Dictionary`2<System.String,System.String>)
extern void BaseVoiceProvider_playAudioFile_m4CCFF1BB44ED2ACE5F10AB4AAEE81D2B7CD24DD7 (void);
// 0x00000242 System.Void Crosstales.RTVoice.Provider.BaseVoiceProvider::copyAudioFile(Crosstales.RTVoice.Model.Wrapper,System.String,System.Boolean,System.Byte[])
extern void BaseVoiceProvider_copyAudioFile_mC986277966319CE62EC8F450A26B0D842B4AF0AA (void);
// 0x00000243 System.Void Crosstales.RTVoice.Provider.BaseVoiceProvider::processAudioFile(Crosstales.RTVoice.Model.Wrapper,System.String,System.Boolean,System.Byte[])
extern void BaseVoiceProvider_processAudioFile_mD6ACD3BA6544012AC5FAE6BAA5B17F7F0583D278 (void);
// 0x00000244 System.String Crosstales.RTVoice.Provider.BaseVoiceProvider::getVoiceName(Crosstales.RTVoice.Model.Wrapper)
extern void BaseVoiceProvider_getVoiceName_m545EF29C42D9272162727415CC0F59B55B7EAE00 (void);
// 0x00000245 System.Void Crosstales.RTVoice.Provider.BaseVoiceProvider::onVoicesReady()
extern void BaseVoiceProvider_onVoicesReady_mF817D7438CBC4EC56307BBFCEFFBB2CA55D40BF8 (void);
// 0x00000246 System.Void Crosstales.RTVoice.Provider.BaseVoiceProvider::onSpeakStart(Crosstales.RTVoice.Model.Wrapper)
extern void BaseVoiceProvider_onSpeakStart_mC65B3113E5BF77122D14BB79750F45F8E453EF78 (void);
// 0x00000247 System.Void Crosstales.RTVoice.Provider.BaseVoiceProvider::onSpeakComplete(Crosstales.RTVoice.Model.Wrapper)
extern void BaseVoiceProvider_onSpeakComplete_m10E0CDF151A84C496DEB3A11CFDAC21742498201 (void);
// 0x00000248 System.Void Crosstales.RTVoice.Provider.BaseVoiceProvider::onSpeakCurrentWord(Crosstales.RTVoice.Model.Wrapper,System.String[],System.Int32)
extern void BaseVoiceProvider_onSpeakCurrentWord_mD46E845D857BA608FAF08C138F57CADD912CAA7B (void);
// 0x00000249 System.Void Crosstales.RTVoice.Provider.BaseVoiceProvider::onSpeakCurrentPhoneme(Crosstales.RTVoice.Model.Wrapper,System.String)
extern void BaseVoiceProvider_onSpeakCurrentPhoneme_m855E173EA80CE09DED011BE7D979A1257760849C (void);
// 0x0000024A System.Void Crosstales.RTVoice.Provider.BaseVoiceProvider::onSpeakCurrentViseme(Crosstales.RTVoice.Model.Wrapper,System.String)
extern void BaseVoiceProvider_onSpeakCurrentViseme_m4136BE82B59A2E83E99F4F3D4CEB374529CBD604 (void);
// 0x0000024B System.Void Crosstales.RTVoice.Provider.BaseVoiceProvider::onSpeakAudioGenerationStart(Crosstales.RTVoice.Model.Wrapper)
extern void BaseVoiceProvider_onSpeakAudioGenerationStart_m75AF6462CE93B8AD2B573CB0917C39D79F94503F (void);
// 0x0000024C System.Void Crosstales.RTVoice.Provider.BaseVoiceProvider::onSpeakAudioGenerationComplete(Crosstales.RTVoice.Model.Wrapper)
extern void BaseVoiceProvider_onSpeakAudioGenerationComplete_mEF256BA9AEE72692FC3F945DDA908EEAB60FF98F (void);
// 0x0000024D System.Void Crosstales.RTVoice.Provider.BaseVoiceProvider::onErrorInfo(Crosstales.RTVoice.Model.Wrapper,System.String)
extern void BaseVoiceProvider_onErrorInfo_mB680477B720E57C3E817EE57EC451BD074FE7063 (void);
// 0x0000024E System.Void Crosstales.RTVoice.Provider.BaseVoiceProvider::.cctor()
extern void BaseVoiceProvider__cctor_mE3106C4492FB8F6C8AB9E64B9E90305C883CEB67 (void);
// 0x0000024F System.Void Crosstales.RTVoice.Provider.BaseVoiceProvider/<>c::.cctor()
extern void U3CU3Ec__cctor_m0EF1C5CFE61420202344FE43A32A81940D81457C (void);
// 0x00000250 System.Void Crosstales.RTVoice.Provider.BaseVoiceProvider/<>c::.ctor()
extern void U3CU3Ec__ctor_mA840AB15FB28E0517609CEC87F3E2BA500F92B2B (void);
// 0x00000251 System.String Crosstales.RTVoice.Provider.BaseVoiceProvider/<>c::<get_Cultures>b__72_0(Crosstales.RTVoice.Model.Voice)
extern void U3CU3Ec_U3Cget_CulturesU3Eb__72_0_m0395F398BA09A22FCBF1581A6645F235A24060B6 (void);
// 0x00000252 Crosstales.RTVoice.Model.Voice Crosstales.RTVoice.Provider.BaseVoiceProvider/<>c::<get_Cultures>b__72_1(System.Linq.IGrouping`2<System.String,Crosstales.RTVoice.Model.Voice>)
extern void U3CU3Ec_U3Cget_CulturesU3Eb__72_1_m87D845FB3DF58DAAED80F6F1A66A076EA1F90D09 (void);
// 0x00000253 System.String Crosstales.RTVoice.Provider.BaseVoiceProvider/<>c::<get_Cultures>b__72_2(Crosstales.RTVoice.Model.Voice)
extern void U3CU3Ec_U3Cget_CulturesU3Eb__72_2_m13DF8C2968DA5E62E3C7E8D471CF98076D4EA309 (void);
// 0x00000254 System.Void Crosstales.RTVoice.Provider.BaseVoiceProvider/<playAudioFile>d__79::.ctor(System.Int32)
extern void U3CplayAudioFileU3Ed__79__ctor_m1918F5820D88E3F6361AF83F51F6389CA0FE8495 (void);
// 0x00000255 System.Void Crosstales.RTVoice.Provider.BaseVoiceProvider/<playAudioFile>d__79::System.IDisposable.Dispose()
extern void U3CplayAudioFileU3Ed__79_System_IDisposable_Dispose_m016A01B81977C3EED37BE31F4601782D790E2577 (void);
// 0x00000256 System.Boolean Crosstales.RTVoice.Provider.BaseVoiceProvider/<playAudioFile>d__79::MoveNext()
extern void U3CplayAudioFileU3Ed__79_MoveNext_m7A4970E998E5D19894D475E4A2A46E6849A3325E (void);
// 0x00000257 System.Void Crosstales.RTVoice.Provider.BaseVoiceProvider/<playAudioFile>d__79::<>m__Finally1()
extern void U3CplayAudioFileU3Ed__79_U3CU3Em__Finally1_m88F63303367156F30057965A5D2D1106BF1CA5B0 (void);
// 0x00000258 System.Object Crosstales.RTVoice.Provider.BaseVoiceProvider/<playAudioFile>d__79::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CplayAudioFileU3Ed__79_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m2F7ED2CAB4FD2CE5ACDEE216AA960E8F48A1D4ED (void);
// 0x00000259 System.Void Crosstales.RTVoice.Provider.BaseVoiceProvider/<playAudioFile>d__79::System.Collections.IEnumerator.Reset()
extern void U3CplayAudioFileU3Ed__79_System_Collections_IEnumerator_Reset_m7EAB980BAE0EC597EB0D8AF89DD2E220F7600552 (void);
// 0x0000025A System.Object Crosstales.RTVoice.Provider.BaseVoiceProvider/<playAudioFile>d__79::System.Collections.IEnumerator.get_Current()
extern void U3CplayAudioFileU3Ed__79_System_Collections_IEnumerator_get_Current_m081CAEDEE10983E393B7386FBD4D29ACC25D70B3 (void);
// 0x0000025B System.String Crosstales.RTVoice.Provider.IVoiceProvider::get_AudioFileExtension()
// 0x0000025C UnityEngine.AudioType Crosstales.RTVoice.Provider.IVoiceProvider::get_AudioFileType()
// 0x0000025D System.String Crosstales.RTVoice.Provider.IVoiceProvider::get_DefaultVoiceName()
// 0x0000025E System.Collections.Generic.List`1<Crosstales.RTVoice.Model.Voice> Crosstales.RTVoice.Provider.IVoiceProvider::get_Voices()
// 0x0000025F System.Int32 Crosstales.RTVoice.Provider.IVoiceProvider::get_MaxTextLength()
// 0x00000260 System.Boolean Crosstales.RTVoice.Provider.IVoiceProvider::get_isWorkingInEditor()
// 0x00000261 System.Boolean Crosstales.RTVoice.Provider.IVoiceProvider::get_isWorkingInPlaymode()
// 0x00000262 System.Boolean Crosstales.RTVoice.Provider.IVoiceProvider::get_isSpeakNativeSupported()
// 0x00000263 System.Boolean Crosstales.RTVoice.Provider.IVoiceProvider::get_isSpeakSupported()
// 0x00000264 System.Boolean Crosstales.RTVoice.Provider.IVoiceProvider::get_isPlatformSupported()
// 0x00000265 System.Boolean Crosstales.RTVoice.Provider.IVoiceProvider::get_isSSMLSupported()
// 0x00000266 System.Boolean Crosstales.RTVoice.Provider.IVoiceProvider::get_isOnlineService()
// 0x00000267 System.Boolean Crosstales.RTVoice.Provider.IVoiceProvider::get_hasCoRoutines()
// 0x00000268 System.Boolean Crosstales.RTVoice.Provider.IVoiceProvider::get_isIL2CPPSupported()
// 0x00000269 System.Collections.Generic.List`1<System.String> Crosstales.RTVoice.Provider.IVoiceProvider::get_Cultures()
// 0x0000026A System.Void Crosstales.RTVoice.Provider.IVoiceProvider::Silence()
// 0x0000026B System.Void Crosstales.RTVoice.Provider.IVoiceProvider::Silence(System.String)
// 0x0000026C System.Collections.IEnumerator Crosstales.RTVoice.Provider.IVoiceProvider::SpeakNative(Crosstales.RTVoice.Model.Wrapper)
// 0x0000026D System.Collections.IEnumerator Crosstales.RTVoice.Provider.IVoiceProvider::Speak(Crosstales.RTVoice.Model.Wrapper)
// 0x0000026E System.Collections.IEnumerator Crosstales.RTVoice.Provider.IVoiceProvider::Generate(Crosstales.RTVoice.Model.Wrapper)
// 0x0000026F System.Void Crosstales.RTVoice.Provider.VoiceProviderIOS::.ctor(UnityEngine.MonoBehaviour)
extern void VoiceProviderIOS__ctor_mC4BC5ADE92D96DE14AE80296C84759FDFBB7308F (void);
// 0x00000270 System.Void Crosstales.RTVoice.Provider.VoiceProviderIOS::SetVoices(System.String)
extern void VoiceProviderIOS_SetVoices_mD729F91B8AF3849C66C680358C5123DC40B647BD (void);
// 0x00000271 System.Void Crosstales.RTVoice.Provider.VoiceProviderIOS::SetState(System.String)
extern void VoiceProviderIOS_SetState_mCF102485F96BDF25C45619C62AC3E4E8589C1BE9 (void);
// 0x00000272 System.Void Crosstales.RTVoice.Provider.VoiceProviderIOS::WordSpoken()
extern void VoiceProviderIOS_WordSpoken_m8203813DD2CECBB24D0C5FE3ED9D432B31D80163 (void);
// 0x00000273 System.String Crosstales.RTVoice.Provider.VoiceProviderIOS::get_AudioFileExtension()
extern void VoiceProviderIOS_get_AudioFileExtension_m07A74BB0B3EA063D0B890C7D207693B256ED0CB5 (void);
// 0x00000274 UnityEngine.AudioType Crosstales.RTVoice.Provider.VoiceProviderIOS::get_AudioFileType()
extern void VoiceProviderIOS_get_AudioFileType_m26F7047CDECDD1768F824EFC7C608293CA4AD1BF (void);
// 0x00000275 System.String Crosstales.RTVoice.Provider.VoiceProviderIOS::get_DefaultVoiceName()
extern void VoiceProviderIOS_get_DefaultVoiceName_mD6EEDD36FB021F31D6555214478F223A97F1E22B (void);
// 0x00000276 System.Collections.Generic.List`1<Crosstales.RTVoice.Model.Voice> Crosstales.RTVoice.Provider.VoiceProviderIOS::get_Voices()
extern void VoiceProviderIOS_get_Voices_m25AF6145DAEED158E2EF09C4EB49ABE956A4EDF7 (void);
// 0x00000277 System.Boolean Crosstales.RTVoice.Provider.VoiceProviderIOS::get_isWorkingInEditor()
extern void VoiceProviderIOS_get_isWorkingInEditor_m888EF1A3B28487DC035062A8BEB7D89610B0093A (void);
// 0x00000278 System.Boolean Crosstales.RTVoice.Provider.VoiceProviderIOS::get_isWorkingInPlaymode()
extern void VoiceProviderIOS_get_isWorkingInPlaymode_m2A36B8954309B8F267E9399320B06247DADD59FC (void);
// 0x00000279 System.Int32 Crosstales.RTVoice.Provider.VoiceProviderIOS::get_MaxTextLength()
extern void VoiceProviderIOS_get_MaxTextLength_m610D3991B05C2CA749C2D765F9444DF92E11A9BA (void);
// 0x0000027A System.Boolean Crosstales.RTVoice.Provider.VoiceProviderIOS::get_isSpeakNativeSupported()
extern void VoiceProviderIOS_get_isSpeakNativeSupported_m7C0135D291D257AA2A4AAEA3686D9D2542BA4462 (void);
// 0x0000027B System.Boolean Crosstales.RTVoice.Provider.VoiceProviderIOS::get_isSpeakSupported()
extern void VoiceProviderIOS_get_isSpeakSupported_m2CB1F9C0CD50FA82075AF910D1554F2E9A0E5BC7 (void);
// 0x0000027C System.Boolean Crosstales.RTVoice.Provider.VoiceProviderIOS::get_isPlatformSupported()
extern void VoiceProviderIOS_get_isPlatformSupported_mFF553B9EB9ED371CDA3D06B03AE2063EAE1619A5 (void);
// 0x0000027D System.Boolean Crosstales.RTVoice.Provider.VoiceProviderIOS::get_isSSMLSupported()
extern void VoiceProviderIOS_get_isSSMLSupported_m42399ABAFF8AC75479D381F067E8DC9B3868B816 (void);
// 0x0000027E System.Boolean Crosstales.RTVoice.Provider.VoiceProviderIOS::get_isOnlineService()
extern void VoiceProviderIOS_get_isOnlineService_mDD2E89AE97570AC044A87164A4CCDA410F861D2F (void);
// 0x0000027F System.Boolean Crosstales.RTVoice.Provider.VoiceProviderIOS::get_hasCoRoutines()
extern void VoiceProviderIOS_get_hasCoRoutines_mE04140B5C1D213F7B3EE05E6869434209F669510 (void);
// 0x00000280 System.Boolean Crosstales.RTVoice.Provider.VoiceProviderIOS::get_isIL2CPPSupported()
extern void VoiceProviderIOS_get_isIL2CPPSupported_m9BB5CB9EA69CED7C7C525503A78C6EBCA4E1A2ED (void);
// 0x00000281 System.Collections.IEnumerator Crosstales.RTVoice.Provider.VoiceProviderIOS::SpeakNative(Crosstales.RTVoice.Model.Wrapper)
extern void VoiceProviderIOS_SpeakNative_m5A460E4C43E3F6B3F5C5B50B2FBA960E906087A1 (void);
// 0x00000282 System.Collections.IEnumerator Crosstales.RTVoice.Provider.VoiceProviderIOS::Speak(Crosstales.RTVoice.Model.Wrapper)
extern void VoiceProviderIOS_Speak_m5D3CE8F57DAE33BE09E8A2F7AF87281DC47181D0 (void);
// 0x00000283 System.Collections.IEnumerator Crosstales.RTVoice.Provider.VoiceProviderIOS::Generate(Crosstales.RTVoice.Model.Wrapper)
extern void VoiceProviderIOS_Generate_m5C4D7AC723D419BA1EFBD9411242CBE29C6D09BB (void);
// 0x00000284 System.Void Crosstales.RTVoice.Provider.VoiceProviderIOS::Silence()
extern void VoiceProviderIOS_Silence_mED79A411E6B142E5F3ECA22EC16CCAED8B847442 (void);
// 0x00000285 System.Collections.IEnumerator Crosstales.RTVoice.Provider.VoiceProviderIOS::speak(Crosstales.RTVoice.Model.Wrapper,System.Boolean)
extern void VoiceProviderIOS_speak_mFB7D7BA16B879B9C8E69B83CDA91D109189EB96E (void);
// 0x00000286 System.Single Crosstales.RTVoice.Provider.VoiceProviderIOS::calculateRate(System.Single)
extern void VoiceProviderIOS_calculateRate_m543B7DC43801E1CB3A84207F436870D1368B741A (void);
// 0x00000287 System.String Crosstales.RTVoice.Provider.VoiceProviderIOS::getVoiceId(Crosstales.RTVoice.Model.Wrapper)
extern void VoiceProviderIOS_getVoiceId_mE5A2247735EAB75CB1329FCD495DBC0C23EDABE8 (void);
// 0x00000288 System.Void Crosstales.RTVoice.Provider.VoiceProviderIOS::.cctor()
extern void VoiceProviderIOS__cctor_m4A4FFA425D5FDC8632E989BFA7D424366BB623E4 (void);
// 0x00000289 System.Void Crosstales.RTVoice.Provider.VoiceProviderIOS/<>c::.cctor()
extern void U3CU3Ec__cctor_mEA471FCF86D779A11EE155D62B0464BE482D6132 (void);
// 0x0000028A System.Void Crosstales.RTVoice.Provider.VoiceProviderIOS/<>c::.ctor()
extern void U3CU3Ec__ctor_mF737325D732B47B4EE8E1EC82D47AD742AF9A108 (void);
// 0x0000028B System.String Crosstales.RTVoice.Provider.VoiceProviderIOS/<>c::<SetVoices>b__6_0(Crosstales.RTVoice.Model.Voice)
extern void U3CU3Ec_U3CSetVoicesU3Eb__6_0_m6CB25C67465C0943C5D40010EC0A70B7D2419879 (void);
// 0x0000028C System.Void Crosstales.RTVoice.Provider.VoiceProviderIOS/<SpeakNative>d__37::.ctor(System.Int32)
extern void U3CSpeakNativeU3Ed__37__ctor_m523A47BABBCEB6115AB8BC5602D2194F4C9DEF4F (void);
// 0x0000028D System.Void Crosstales.RTVoice.Provider.VoiceProviderIOS/<SpeakNative>d__37::System.IDisposable.Dispose()
extern void U3CSpeakNativeU3Ed__37_System_IDisposable_Dispose_m8516463D2831A9F5409582D34F16DC2697018BD8 (void);
// 0x0000028E System.Boolean Crosstales.RTVoice.Provider.VoiceProviderIOS/<SpeakNative>d__37::MoveNext()
extern void U3CSpeakNativeU3Ed__37_MoveNext_m2DB830F1D63D93BA54D23768651BD95A7F90A608 (void);
// 0x0000028F System.Object Crosstales.RTVoice.Provider.VoiceProviderIOS/<SpeakNative>d__37::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CSpeakNativeU3Ed__37_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mF2CE12F2B43A8A935DAA332E01EF3DF9D3659427 (void);
// 0x00000290 System.Void Crosstales.RTVoice.Provider.VoiceProviderIOS/<SpeakNative>d__37::System.Collections.IEnumerator.Reset()
extern void U3CSpeakNativeU3Ed__37_System_Collections_IEnumerator_Reset_mDE6E3EAB0C3383024A07F9518EC95E2615BE50A2 (void);
// 0x00000291 System.Object Crosstales.RTVoice.Provider.VoiceProviderIOS/<SpeakNative>d__37::System.Collections.IEnumerator.get_Current()
extern void U3CSpeakNativeU3Ed__37_System_Collections_IEnumerator_get_Current_mF4BB2E19D59C10E2E19E1A071D8B963C2318D72B (void);
// 0x00000292 System.Void Crosstales.RTVoice.Provider.VoiceProviderIOS/<Speak>d__38::.ctor(System.Int32)
extern void U3CSpeakU3Ed__38__ctor_m17AA83B8F4292BE9A434FAD451B6042F85B1CA7A (void);
// 0x00000293 System.Void Crosstales.RTVoice.Provider.VoiceProviderIOS/<Speak>d__38::System.IDisposable.Dispose()
extern void U3CSpeakU3Ed__38_System_IDisposable_Dispose_m1D62F9167AD94A284EB61F82C0A4F55B3CD87CFD (void);
// 0x00000294 System.Boolean Crosstales.RTVoice.Provider.VoiceProviderIOS/<Speak>d__38::MoveNext()
extern void U3CSpeakU3Ed__38_MoveNext_m536B0376DA33C79026D993AF29A57DF0209A6AC9 (void);
// 0x00000295 System.Object Crosstales.RTVoice.Provider.VoiceProviderIOS/<Speak>d__38::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CSpeakU3Ed__38_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m55A294AB03578EC259B3F90FF30ED7D6E9B850BB (void);
// 0x00000296 System.Void Crosstales.RTVoice.Provider.VoiceProviderIOS/<Speak>d__38::System.Collections.IEnumerator.Reset()
extern void U3CSpeakU3Ed__38_System_Collections_IEnumerator_Reset_mB2BC205AFCE9DA70AAF9757596AB8EE0E2085E41 (void);
// 0x00000297 System.Object Crosstales.RTVoice.Provider.VoiceProviderIOS/<Speak>d__38::System.Collections.IEnumerator.get_Current()
extern void U3CSpeakU3Ed__38_System_Collections_IEnumerator_get_Current_m3D7E6B2FE72C3643435F91F8C65FDBD74A4E2C6B (void);
// 0x00000298 System.Void Crosstales.RTVoice.Provider.VoiceProviderIOS/<Generate>d__39::.ctor(System.Int32)
extern void U3CGenerateU3Ed__39__ctor_mD9733152DA5C477BC3EBDE9374773C3614007179 (void);
// 0x00000299 System.Void Crosstales.RTVoice.Provider.VoiceProviderIOS/<Generate>d__39::System.IDisposable.Dispose()
extern void U3CGenerateU3Ed__39_System_IDisposable_Dispose_m77EA5EA8743977C2BB472BD2FF79D131B2A25D38 (void);
// 0x0000029A System.Boolean Crosstales.RTVoice.Provider.VoiceProviderIOS/<Generate>d__39::MoveNext()
extern void U3CGenerateU3Ed__39_MoveNext_mC7E14CE9883319EEC46EE167548548E177923595 (void);
// 0x0000029B System.Object Crosstales.RTVoice.Provider.VoiceProviderIOS/<Generate>d__39::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CGenerateU3Ed__39_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m55C35151E2EBDA7C7287CC1DB8AC808094F27B2A (void);
// 0x0000029C System.Void Crosstales.RTVoice.Provider.VoiceProviderIOS/<Generate>d__39::System.Collections.IEnumerator.Reset()
extern void U3CGenerateU3Ed__39_System_Collections_IEnumerator_Reset_m4D4A7F1C40FB78E0D45F689E2638CF4203F0BB8B (void);
// 0x0000029D System.Object Crosstales.RTVoice.Provider.VoiceProviderIOS/<Generate>d__39::System.Collections.IEnumerator.get_Current()
extern void U3CGenerateU3Ed__39_System_Collections_IEnumerator_get_Current_mECE12334B8337427611893D644D1735115B00175 (void);
// 0x0000029E System.Void Crosstales.RTVoice.Provider.VoiceProviderIOS/<speak>d__41::.ctor(System.Int32)
extern void U3CspeakU3Ed__41__ctor_m276AE39E567257B32C7896EA8EDDA24EC9D824ED (void);
// 0x0000029F System.Void Crosstales.RTVoice.Provider.VoiceProviderIOS/<speak>d__41::System.IDisposable.Dispose()
extern void U3CspeakU3Ed__41_System_IDisposable_Dispose_m0E4873CEED2CE27F21F5EB92F97879BFB53783C2 (void);
// 0x000002A0 System.Boolean Crosstales.RTVoice.Provider.VoiceProviderIOS/<speak>d__41::MoveNext()
extern void U3CspeakU3Ed__41_MoveNext_m32CBA9644FBF1A2196C89F1678AE03DD31997ACE (void);
// 0x000002A1 System.Object Crosstales.RTVoice.Provider.VoiceProviderIOS/<speak>d__41::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CspeakU3Ed__41_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mEEA1EC15804398BACB904C1707E5281F50328F18 (void);
// 0x000002A2 System.Void Crosstales.RTVoice.Provider.VoiceProviderIOS/<speak>d__41::System.Collections.IEnumerator.Reset()
extern void U3CspeakU3Ed__41_System_Collections_IEnumerator_Reset_m93EA86B1BE247D3CE920516B739BB3E25B25C55F (void);
// 0x000002A3 System.Object Crosstales.RTVoice.Provider.VoiceProviderIOS/<speak>d__41::System.Collections.IEnumerator.get_Current()
extern void U3CspeakU3Ed__41_System_Collections_IEnumerator_get_Current_m95840D9DA7B2AD2A2C84637718BD96658116F22F (void);
// 0x000002A4 System.Void Crosstales.RTVoice.Provider.NativeMethods::Stop()
extern void NativeMethods_Stop_mB2EA6FF4519D74F0C0DF8A2104AF16FE63071F3F (void);
// 0x000002A5 System.Void Crosstales.RTVoice.Provider.NativeMethods::GetVoices()
extern void NativeMethods_GetVoices_m7906C96E78341A926EEA1C5A276A4528D85CE4AB (void);
// 0x000002A6 System.Void Crosstales.RTVoice.Provider.NativeMethods::Speak(System.String,System.String,System.Single,System.Single,System.Single)
extern void NativeMethods_Speak_mF3A31C5A982A4A0DDB94E67397E939D105103D51 (void);
// 0x000002A7 System.Void Crosstales.RTVoice.Provider.VoiceProviderMary::.ctor(UnityEngine.MonoBehaviour,System.String,System.Int32,System.String,System.String)
extern void VoiceProviderMary__ctor_mB6769A04B732E045ED7160EA34DE22311D2AC30B (void);
// 0x000002A8 System.String Crosstales.RTVoice.Provider.VoiceProviderMary::get_AudioFileExtension()
extern void VoiceProviderMary_get_AudioFileExtension_m1B8092196E9F35921D525DC3143AF18DCBC9F0F7 (void);
// 0x000002A9 UnityEngine.AudioType Crosstales.RTVoice.Provider.VoiceProviderMary::get_AudioFileType()
extern void VoiceProviderMary_get_AudioFileType_mA84BE50D872E8245689CB7ED9125FDDCB0E2A5F8 (void);
// 0x000002AA System.String Crosstales.RTVoice.Provider.VoiceProviderMary::get_DefaultVoiceName()
extern void VoiceProviderMary_get_DefaultVoiceName_m8C8D8F8EEF1ABE901E460D50E752EED88F06168F (void);
// 0x000002AB System.Boolean Crosstales.RTVoice.Provider.VoiceProviderMary::get_isWorkingInEditor()
extern void VoiceProviderMary_get_isWorkingInEditor_m21F72BBD9F25B5EE735B980BB2082E6AE9DAEC36 (void);
// 0x000002AC System.Boolean Crosstales.RTVoice.Provider.VoiceProviderMary::get_isWorkingInPlaymode()
extern void VoiceProviderMary_get_isWorkingInPlaymode_mB724D830F7622218E0604C9DF9FF0CC82DD247DC (void);
// 0x000002AD System.Int32 Crosstales.RTVoice.Provider.VoiceProviderMary::get_MaxTextLength()
extern void VoiceProviderMary_get_MaxTextLength_m93FC45A4EF1AF0F0756A1B443C2C7C9D7B65999D (void);
// 0x000002AE System.Boolean Crosstales.RTVoice.Provider.VoiceProviderMary::get_isSpeakNativeSupported()
extern void VoiceProviderMary_get_isSpeakNativeSupported_m95F7D6E5F773316CBBB491C91E2C3055066D3DBD (void);
// 0x000002AF System.Boolean Crosstales.RTVoice.Provider.VoiceProviderMary::get_isSpeakSupported()
extern void VoiceProviderMary_get_isSpeakSupported_m07AA500CF2852BC3D98BA43D38A379FFCDAA715E (void);
// 0x000002B0 System.Boolean Crosstales.RTVoice.Provider.VoiceProviderMary::get_isPlatformSupported()
extern void VoiceProviderMary_get_isPlatformSupported_mB980866B022E1053B7D533354DDB2B78DE855D5D (void);
// 0x000002B1 System.Boolean Crosstales.RTVoice.Provider.VoiceProviderMary::get_isSSMLSupported()
extern void VoiceProviderMary_get_isSSMLSupported_m258E7AAE088712C0C3EA2E58CF8DC8F63D1DC832 (void);
// 0x000002B2 System.Boolean Crosstales.RTVoice.Provider.VoiceProviderMary::get_isOnlineService()
extern void VoiceProviderMary_get_isOnlineService_m720CF799EB99CDC8BE1F1F8E92CBCC71B2CFFAC5 (void);
// 0x000002B3 System.Boolean Crosstales.RTVoice.Provider.VoiceProviderMary::get_hasCoRoutines()
extern void VoiceProviderMary_get_hasCoRoutines_m26CD140877B7336FC8F6F0A015ACF0D482851B24 (void);
// 0x000002B4 System.Boolean Crosstales.RTVoice.Provider.VoiceProviderMary::get_isIL2CPPSupported()
extern void VoiceProviderMary_get_isIL2CPPSupported_m3CCA21A436E168D0309009A449D7CEDEE167C696 (void);
// 0x000002B5 System.Collections.IEnumerator Crosstales.RTVoice.Provider.VoiceProviderMary::SpeakNative(Crosstales.RTVoice.Model.Wrapper)
extern void VoiceProviderMary_SpeakNative_m331610A2ED4ACEA5F1B1E8285E531D9A812A09C2 (void);
// 0x000002B6 System.Collections.IEnumerator Crosstales.RTVoice.Provider.VoiceProviderMary::Speak(Crosstales.RTVoice.Model.Wrapper)
extern void VoiceProviderMary_Speak_m891800543E668A5334BD528A4144C8785D80DAE7 (void);
// 0x000002B7 System.Collections.IEnumerator Crosstales.RTVoice.Provider.VoiceProviderMary::Generate(Crosstales.RTVoice.Model.Wrapper)
extern void VoiceProviderMary_Generate_m5E6DF2FF9DB49139BB98D02BB3B8703159E50EB3 (void);
// 0x000002B8 System.Collections.IEnumerator Crosstales.RTVoice.Provider.VoiceProviderMary::speak(Crosstales.RTVoice.Model.Wrapper,System.Boolean)
extern void VoiceProviderMary_speak_mEDD8B66B63C43FD85CAD280D309C4276729D3FD6 (void);
// 0x000002B9 System.Collections.IEnumerator Crosstales.RTVoice.Provider.VoiceProviderMary::getVoices()
extern void VoiceProviderMary_getVoices_mA44E670C9E75CC20259D5BFBC4884064E4BB1DCC (void);
// 0x000002BA System.String Crosstales.RTVoice.Provider.VoiceProviderMary::prepareProsody(Crosstales.RTVoice.Model.Wrapper)
extern void VoiceProviderMary_prepareProsody_mC908FAA976B4484D18FC8A46611AFBBF088DEA96 (void);
// 0x000002BB System.String Crosstales.RTVoice.Provider.VoiceProviderMary::getVoiceCulture(Crosstales.RTVoice.Model.Wrapper)
extern void VoiceProviderMary_getVoiceCulture_m2263C6C1C91C4F198C212990512CC920EFDC12DC (void);
// 0x000002BC System.Void Crosstales.RTVoice.Provider.VoiceProviderMary/<SpeakNative>d__29::.ctor(System.Int32)
extern void U3CSpeakNativeU3Ed__29__ctor_m7470A0504C3BABDC965367131EA97B330ECAA0B3 (void);
// 0x000002BD System.Void Crosstales.RTVoice.Provider.VoiceProviderMary/<SpeakNative>d__29::System.IDisposable.Dispose()
extern void U3CSpeakNativeU3Ed__29_System_IDisposable_Dispose_m02B9BF12673B6C720138AE5C87DCA36928F31A6B (void);
// 0x000002BE System.Boolean Crosstales.RTVoice.Provider.VoiceProviderMary/<SpeakNative>d__29::MoveNext()
extern void U3CSpeakNativeU3Ed__29_MoveNext_m05BE0511D45764D10AB861EFB3339ED18AFDE054 (void);
// 0x000002BF System.Object Crosstales.RTVoice.Provider.VoiceProviderMary/<SpeakNative>d__29::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CSpeakNativeU3Ed__29_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mB9405A8A86C3770CC1F111D0C4000A7A9B7D7A67 (void);
// 0x000002C0 System.Void Crosstales.RTVoice.Provider.VoiceProviderMary/<SpeakNative>d__29::System.Collections.IEnumerator.Reset()
extern void U3CSpeakNativeU3Ed__29_System_Collections_IEnumerator_Reset_mBC4615F0056EF0BC799A52C6A4149F468953E3B1 (void);
// 0x000002C1 System.Object Crosstales.RTVoice.Provider.VoiceProviderMary/<SpeakNative>d__29::System.Collections.IEnumerator.get_Current()
extern void U3CSpeakNativeU3Ed__29_System_Collections_IEnumerator_get_Current_mFED8E5538033280FD348A7C8FFB81D8D26DC06A2 (void);
// 0x000002C2 System.Void Crosstales.RTVoice.Provider.VoiceProviderMary/<Speak>d__30::.ctor(System.Int32)
extern void U3CSpeakU3Ed__30__ctor_m9587AD139AB417176D703120DF98958092A13743 (void);
// 0x000002C3 System.Void Crosstales.RTVoice.Provider.VoiceProviderMary/<Speak>d__30::System.IDisposable.Dispose()
extern void U3CSpeakU3Ed__30_System_IDisposable_Dispose_m23CE40AF61F6D8649073C36D77C590953E895803 (void);
// 0x000002C4 System.Boolean Crosstales.RTVoice.Provider.VoiceProviderMary/<Speak>d__30::MoveNext()
extern void U3CSpeakU3Ed__30_MoveNext_mBE5D478FD118D12D9131E5F87F6284B4B33F47B7 (void);
// 0x000002C5 System.Object Crosstales.RTVoice.Provider.VoiceProviderMary/<Speak>d__30::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CSpeakU3Ed__30_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m4BE9CD55DFC56B895DDBBEDFFFF9D846C2F24096 (void);
// 0x000002C6 System.Void Crosstales.RTVoice.Provider.VoiceProviderMary/<Speak>d__30::System.Collections.IEnumerator.Reset()
extern void U3CSpeakU3Ed__30_System_Collections_IEnumerator_Reset_mDDC9BFD5B6F9F4992FF961A1916F6CFFF370032B (void);
// 0x000002C7 System.Object Crosstales.RTVoice.Provider.VoiceProviderMary/<Speak>d__30::System.Collections.IEnumerator.get_Current()
extern void U3CSpeakU3Ed__30_System_Collections_IEnumerator_get_Current_m80972AF0FFE79545A22C01A56202CEA16644B859 (void);
// 0x000002C8 System.Void Crosstales.RTVoice.Provider.VoiceProviderMary/<Generate>d__31::.ctor(System.Int32)
extern void U3CGenerateU3Ed__31__ctor_m267A2910841978F5CDB6CCB831A10313B0798DEC (void);
// 0x000002C9 System.Void Crosstales.RTVoice.Provider.VoiceProviderMary/<Generate>d__31::System.IDisposable.Dispose()
extern void U3CGenerateU3Ed__31_System_IDisposable_Dispose_m22DC6783EC7327F0EFAFDF726143D978509C7C62 (void);
// 0x000002CA System.Boolean Crosstales.RTVoice.Provider.VoiceProviderMary/<Generate>d__31::MoveNext()
extern void U3CGenerateU3Ed__31_MoveNext_mA83E3FB408DEF5A6E09CB0CBEC66506FBDC92CA0 (void);
// 0x000002CB System.Void Crosstales.RTVoice.Provider.VoiceProviderMary/<Generate>d__31::<>m__Finally1()
extern void U3CGenerateU3Ed__31_U3CU3Em__Finally1_mDA25C0FD49EDC94E346AFFCA4B00F774AE5A0D99 (void);
// 0x000002CC System.Object Crosstales.RTVoice.Provider.VoiceProviderMary/<Generate>d__31::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CGenerateU3Ed__31_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m6BA121BFF88A61F810A85519EA5166243AA8F2F7 (void);
// 0x000002CD System.Void Crosstales.RTVoice.Provider.VoiceProviderMary/<Generate>d__31::System.Collections.IEnumerator.Reset()
extern void U3CGenerateU3Ed__31_System_Collections_IEnumerator_Reset_mA8522467F71712CDE975F96D86B51B3F6C04B1CC (void);
// 0x000002CE System.Object Crosstales.RTVoice.Provider.VoiceProviderMary/<Generate>d__31::System.Collections.IEnumerator.get_Current()
extern void U3CGenerateU3Ed__31_System_Collections_IEnumerator_get_Current_m9EC33BA38BF83D3CA8FF84D807491EEBDC56CBD2 (void);
// 0x000002CF System.Void Crosstales.RTVoice.Provider.VoiceProviderMary/<speak>d__32::.ctor(System.Int32)
extern void U3CspeakU3Ed__32__ctor_m39F1A6EF5BF1ECD0725D91A20A03439E3AD4E103 (void);
// 0x000002D0 System.Void Crosstales.RTVoice.Provider.VoiceProviderMary/<speak>d__32::System.IDisposable.Dispose()
extern void U3CspeakU3Ed__32_System_IDisposable_Dispose_m47A016B1C9DA6E21E7501088414B7AE4026C210E (void);
// 0x000002D1 System.Boolean Crosstales.RTVoice.Provider.VoiceProviderMary/<speak>d__32::MoveNext()
extern void U3CspeakU3Ed__32_MoveNext_m24A5A0CC2FFB038864D2BF34084D91512FDB498A (void);
// 0x000002D2 System.Object Crosstales.RTVoice.Provider.VoiceProviderMary/<speak>d__32::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CspeakU3Ed__32_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m8CFA25DFB6CD460D6C3AA1108C416152A15230F5 (void);
// 0x000002D3 System.Void Crosstales.RTVoice.Provider.VoiceProviderMary/<speak>d__32::System.Collections.IEnumerator.Reset()
extern void U3CspeakU3Ed__32_System_Collections_IEnumerator_Reset_m73F40ECB8D160F5A0111DCDC777F3BFB1CEEC6F3 (void);
// 0x000002D4 System.Object Crosstales.RTVoice.Provider.VoiceProviderMary/<speak>d__32::System.Collections.IEnumerator.get_Current()
extern void U3CspeakU3Ed__32_System_Collections_IEnumerator_get_Current_m2D2650AC8FF888202F794982EB33EE9B57F98367 (void);
// 0x000002D5 System.Void Crosstales.RTVoice.Provider.VoiceProviderMary/<>c::.cctor()
extern void U3CU3Ec__cctor_m7CB3E48BA99465FB4BCCD49E7F8EC4A255DAE92B (void);
// 0x000002D6 System.Void Crosstales.RTVoice.Provider.VoiceProviderMary/<>c::.ctor()
extern void U3CU3Ec__ctor_mD3D5DF15838DA3DB7A52BC69F204B58E3F90BB40 (void);
// 0x000002D7 System.String Crosstales.RTVoice.Provider.VoiceProviderMary/<>c::<getVoices>b__33_0(Crosstales.RTVoice.Model.Voice)
extern void U3CU3Ec_U3CgetVoicesU3Eb__33_0_m07CDC9ECA52CE7037EB04790B0A364271331C9ED (void);
// 0x000002D8 System.Void Crosstales.RTVoice.Provider.VoiceProviderMary/<getVoices>d__33::.ctor(System.Int32)
extern void U3CgetVoicesU3Ed__33__ctor_m95F6F409F82D4F4973F63E456C242F66A76B3205 (void);
// 0x000002D9 System.Void Crosstales.RTVoice.Provider.VoiceProviderMary/<getVoices>d__33::System.IDisposable.Dispose()
extern void U3CgetVoicesU3Ed__33_System_IDisposable_Dispose_mF7D198C1948C01A6A4D9D46B84AB60DE30DF3C30 (void);
// 0x000002DA System.Boolean Crosstales.RTVoice.Provider.VoiceProviderMary/<getVoices>d__33::MoveNext()
extern void U3CgetVoicesU3Ed__33_MoveNext_mF8F4466E2C8CC6587D824C6B7623E60C582C389C (void);
// 0x000002DB System.Void Crosstales.RTVoice.Provider.VoiceProviderMary/<getVoices>d__33::<>m__Finally1()
extern void U3CgetVoicesU3Ed__33_U3CU3Em__Finally1_m2A7E2EC60FF2A44DB6861F37ED48C1F9EEF4D8DA (void);
// 0x000002DC System.Object Crosstales.RTVoice.Provider.VoiceProviderMary/<getVoices>d__33::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CgetVoicesU3Ed__33_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m2C5797A9C9F2E33DE06235C398E7B36C6EA1197B (void);
// 0x000002DD System.Void Crosstales.RTVoice.Provider.VoiceProviderMary/<getVoices>d__33::System.Collections.IEnumerator.Reset()
extern void U3CgetVoicesU3Ed__33_System_Collections_IEnumerator_Reset_m995AFECA0C5D06884A5DA8C78BF92AF75EAB19E1 (void);
// 0x000002DE System.Object Crosstales.RTVoice.Provider.VoiceProviderMary/<getVoices>d__33::System.Collections.IEnumerator.get_Current()
extern void U3CgetVoicesU3Ed__33_System_Collections_IEnumerator_get_Current_mA53C128274D93B9FE40D699DD1192FC1A9C5B829 (void);
// 0x000002DF System.String Crosstales.RTVoice.Model.Sequence::ToString()
extern void Sequence_ToString_m3BFC91D430D8A907A09D5F8C4AA292AC082FDBAC (void);
// 0x000002E0 System.Void Crosstales.RTVoice.Model.Sequence::.ctor()
extern void Sequence__ctor_m5E98C6F80D8D65FC7C5B9CF2B73E4CE293F9EC3A (void);
// 0x000002E1 System.String Crosstales.RTVoice.Model.Voice::get_Culture()
extern void Voice_get_Culture_m7EF31333CBC90A35D123D49227AB96C62D4D95BA (void);
// 0x000002E2 System.Void Crosstales.RTVoice.Model.Voice::set_Culture(System.String)
extern void Voice_set_Culture_mCEB168A68A0FACB0992C681FB408C26278A513DC (void);
// 0x000002E3 System.Void Crosstales.RTVoice.Model.Voice::.ctor(System.String,System.String,Crosstales.RTVoice.Model.Enum.Gender,System.String,System.String,System.String,System.String,System.String)
extern void Voice__ctor_mC0087A480AD46373F19FCB3BD5E1C2D412B4C89B (void);
// 0x000002E4 System.String Crosstales.RTVoice.Model.Voice::ToString()
extern void Voice_ToString_m46193BE390246ECF3591CA5DA9BA64A9032FAD3B (void);
// 0x000002E5 System.String Crosstales.RTVoice.Model.VoiceAlias::get_VoiceName()
extern void VoiceAlias_get_VoiceName_m0CBFE27D846026BB82F5E682FEB0A97AD27D3737 (void);
// 0x000002E6 Crosstales.RTVoice.Model.Voice Crosstales.RTVoice.Model.VoiceAlias::get_Voice()
extern void VoiceAlias_get_Voice_mF91385FF9A70D7C329435302B581BECE81FBBC5A (void);
// 0x000002E7 System.String Crosstales.RTVoice.Model.VoiceAlias::ToString()
extern void VoiceAlias_ToString_m9DF65F2BAA5B3C76D48EACA25FF8E62640CD0832 (void);
// 0x000002E8 System.Void Crosstales.RTVoice.Model.VoiceAlias::.ctor()
extern void VoiceAlias__ctor_m71C4E357B220016FB187704DB63C9861AE8ED688 (void);
// 0x000002E9 System.String Crosstales.RTVoice.Model.Wrapper::get_Text()
extern void Wrapper_get_Text_mC219083BF6B4E0500CABF0B432D2662D131638B5 (void);
// 0x000002EA System.Void Crosstales.RTVoice.Model.Wrapper::set_Text(System.String)
extern void Wrapper_set_Text_m8E355EFA3581435BF6FE74AFBBEC1932E58EA35E (void);
// 0x000002EB System.Single Crosstales.RTVoice.Model.Wrapper::get_Rate()
extern void Wrapper_get_Rate_m3EE43D41BD63635B196B1094235560C723880ACC (void);
// 0x000002EC System.Void Crosstales.RTVoice.Model.Wrapper::set_Rate(System.Single)
extern void Wrapper_set_Rate_m7B8C8D39414EA93CFB0397500F1B8932A45D7BED (void);
// 0x000002ED System.Single Crosstales.RTVoice.Model.Wrapper::get_Pitch()
extern void Wrapper_get_Pitch_mCCF5C76983AC9D8DE7C0C2D0F3C8BBE096B53CF0 (void);
// 0x000002EE System.Void Crosstales.RTVoice.Model.Wrapper::set_Pitch(System.Single)
extern void Wrapper_set_Pitch_mE1195A0F2B8E098BC6570A24E96F2D3D850B231E (void);
// 0x000002EF System.Single Crosstales.RTVoice.Model.Wrapper::get_Volume()
extern void Wrapper_get_Volume_mF10B58AC0E2C9C244ED7104CAAEC93354BA85AE8 (void);
// 0x000002F0 System.Void Crosstales.RTVoice.Model.Wrapper::set_Volume(System.Single)
extern void Wrapper_set_Volume_m72C546D43C36D860514E42F8177FBD3B15B16463 (void);
// 0x000002F1 System.DateTime Crosstales.RTVoice.Model.Wrapper::get_Created()
extern void Wrapper_get_Created_m3DE70C26881F1035F2F5885BD7F85A03EDB46C2B (void);
// 0x000002F2 System.Void Crosstales.RTVoice.Model.Wrapper::.ctor()
extern void Wrapper__ctor_m7E197A775CE393F579FEAEBC0BE3919FE04B30A2 (void);
// 0x000002F3 System.Void Crosstales.RTVoice.Model.Wrapper::.ctor(System.String,Crosstales.RTVoice.Model.Voice,System.Single,System.Single,System.Single,System.Boolean)
extern void Wrapper__ctor_m0F9313C5EDAC0D90060DD6C4D68C9DCC00575D1D (void);
// 0x000002F4 System.Void Crosstales.RTVoice.Model.Wrapper::.ctor(System.String,Crosstales.RTVoice.Model.Voice,System.Single,System.Single,System.Single,UnityEngine.AudioSource,System.Boolean,System.String,System.Boolean)
extern void Wrapper__ctor_m56EC0540CC35B2E1CC6ABDE2A11AD6F0F7BC3D3F (void);
// 0x000002F5 System.Void Crosstales.RTVoice.Model.Wrapper::.ctor(System.String,System.String,Crosstales.RTVoice.Model.Voice,System.Single,System.Single,System.Single,UnityEngine.AudioSource,System.Boolean,System.String,System.Boolean)
extern void Wrapper__ctor_m47F6FD774724B8F43E6DA2C1A0042B2E2911C8A3 (void);
// 0x000002F6 System.String Crosstales.RTVoice.Model.Wrapper::ToString()
extern void Wrapper_ToString_m3708C530727B7346770AA39D631DA4D1BE32DE8F (void);
// 0x000002F7 System.Void Crosstales.RTVoice.Demo.Dialog::OnEnable()
extern void Dialog_OnEnable_mF4D606A6B5ACEE60DF0AA2C1FBA7F566BF9AD99D (void);
// 0x000002F8 System.Void Crosstales.RTVoice.Demo.Dialog::OnDisable()
extern void Dialog_OnDisable_m1D578120E599BAAC330DFD3EF110F060E38655D6 (void);
// 0x000002F9 System.Collections.IEnumerator Crosstales.RTVoice.Demo.Dialog::DialogSequence()
extern void Dialog_DialogSequence_m7D77C870918DB2F259C90731C7EE7E6E4F788B9D (void);
// 0x000002FA System.Void Crosstales.RTVoice.Demo.Dialog::speakStartMethod(Crosstales.RTVoice.Model.Wrapper)
extern void Dialog_speakStartMethod_m0F084991EF63B99FC9FB4E1DA8A3D4B2A0BC8CD4 (void);
// 0x000002FB System.Void Crosstales.RTVoice.Demo.Dialog::speakCompleteMethod(Crosstales.RTVoice.Model.Wrapper)
extern void Dialog_speakCompleteMethod_m51E4AE3F7D8720106FD31B48FB3A4403CC7E251C (void);
// 0x000002FC System.Void Crosstales.RTVoice.Demo.Dialog::.ctor()
extern void Dialog__ctor_m8B687AF79B41A93CA26166999F7AE0E4BFCE7B51 (void);
// 0x000002FD System.Void Crosstales.RTVoice.Demo.Dialog/<DialogSequence>d__25::.ctor(System.Int32)
extern void U3CDialogSequenceU3Ed__25__ctor_m0B8FC59C5611D1416FB254E739F74FAC0B7241BF (void);
// 0x000002FE System.Void Crosstales.RTVoice.Demo.Dialog/<DialogSequence>d__25::System.IDisposable.Dispose()
extern void U3CDialogSequenceU3Ed__25_System_IDisposable_Dispose_m43CAB6AFDA5A407257DCE2BB40AF5562F71833EA (void);
// 0x000002FF System.Boolean Crosstales.RTVoice.Demo.Dialog/<DialogSequence>d__25::MoveNext()
extern void U3CDialogSequenceU3Ed__25_MoveNext_mA0D0362F52647EFDCE638259C5B5D9D75E43D516 (void);
// 0x00000300 System.Object Crosstales.RTVoice.Demo.Dialog/<DialogSequence>d__25::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CDialogSequenceU3Ed__25_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mE00F000EBAFBDDFF5D2C8F42E742D77F3DD80A97 (void);
// 0x00000301 System.Void Crosstales.RTVoice.Demo.Dialog/<DialogSequence>d__25::System.Collections.IEnumerator.Reset()
extern void U3CDialogSequenceU3Ed__25_System_Collections_IEnumerator_Reset_mD9737A7BD110755EF4E00DA6CB49C2B29F9D3B42 (void);
// 0x00000302 System.Object Crosstales.RTVoice.Demo.Dialog/<DialogSequence>d__25::System.Collections.IEnumerator.get_Current()
extern void U3CDialogSequenceU3Ed__25_System_Collections_IEnumerator_get_Current_m1EF267AEC7A80B67C2C484CABB5254A8CA16068A (void);
// 0x00000303 System.Void Crosstales.RTVoice.Demo.GUIAudioFilter::Start()
extern void GUIAudioFilter_Start_m6886E6993EFAA97A88E35853BD32694588CB94B6 (void);
// 0x00000304 System.Void Crosstales.RTVoice.Demo.GUIAudioFilter::ResetFilters()
extern void GUIAudioFilter_ResetFilters_m5B7B75EFEF1DC5B2669AD6285BB2455B1C282685 (void);
// 0x00000305 System.Void Crosstales.RTVoice.Demo.GUIAudioFilter::ReverbFilterDropdownChanged(System.Int32)
extern void GUIAudioFilter_ReverbFilterDropdownChanged_mC842F786BBC73DF7AFD6A6CFAED3E5F26866CC32 (void);
// 0x00000306 System.Void Crosstales.RTVoice.Demo.GUIAudioFilter::ChorusFilterEnabled(System.Boolean)
extern void GUIAudioFilter_ChorusFilterEnabled_mA057786817E3577C6E379E2B8AAB916C6DF704F2 (void);
// 0x00000307 System.Void Crosstales.RTVoice.Demo.GUIAudioFilter::EchoFilterEnabled(System.Boolean)
extern void GUIAudioFilter_EchoFilterEnabled_m36AB7310EF7486367CB129CD64DE045A6E50D43F (void);
// 0x00000308 System.Void Crosstales.RTVoice.Demo.GUIAudioFilter::DistortionFilterEnabled(System.Boolean)
extern void GUIAudioFilter_DistortionFilterEnabled_m5BD844B929184E0C0E8DA4AEF8CC5E814741D1BC (void);
// 0x00000309 System.Void Crosstales.RTVoice.Demo.GUIAudioFilter::DistortionFilterChanged(System.Single)
extern void GUIAudioFilter_DistortionFilterChanged_mE8F55A16742AD05A91A963342AA2F90422AADE27 (void);
// 0x0000030A System.Void Crosstales.RTVoice.Demo.GUIAudioFilter::LowPassFilterEnabled(System.Boolean)
extern void GUIAudioFilter_LowPassFilterEnabled_mCB6829F29BDD0222AA90FB2E04F524B0919B7E43 (void);
// 0x0000030B System.Void Crosstales.RTVoice.Demo.GUIAudioFilter::LowPassFilterChanged(System.Single)
extern void GUIAudioFilter_LowPassFilterChanged_m5AF438F7E83057F53B2C6860B73EBAEA3BB946EE (void);
// 0x0000030C System.Void Crosstales.RTVoice.Demo.GUIAudioFilter::HighPassFilterEnabled(System.Boolean)
extern void GUIAudioFilter_HighPassFilterEnabled_m558C059BDFCDAF936DFFF2C210BB320A58200F82 (void);
// 0x0000030D System.Void Crosstales.RTVoice.Demo.GUIAudioFilter::HighPassFilterChanged(System.Single)
extern void GUIAudioFilter_HighPassFilterChanged_m14DFE353513153224A9F55A8F0448587C73697B4 (void);
// 0x0000030E System.Void Crosstales.RTVoice.Demo.GUIAudioFilter::VolumeChanged(System.Single)
extern void GUIAudioFilter_VolumeChanged_mED7D295CCF857329A976FF00678393F8F337A53C (void);
// 0x0000030F System.Void Crosstales.RTVoice.Demo.GUIAudioFilter::PitchChanged(System.Single)
extern void GUIAudioFilter_PitchChanged_m1CF6C6569BC7813D3AE47FD43866FAC7D2D51D73 (void);
// 0x00000310 System.Void Crosstales.RTVoice.Demo.GUIAudioFilter::.ctor()
extern void GUIAudioFilter__ctor_mAD15F7AB21627DB82A885D489B6813165E9C5948 (void);
// 0x00000311 System.Void Crosstales.RTVoice.Demo.GUIDialog::Start()
extern void GUIDialog_Start_mCA0C179522C0F844FD0DFDDAF0C4C6574378138A (void);
// 0x00000312 System.Void Crosstales.RTVoice.Demo.GUIDialog::Update()
extern void GUIDialog_Update_mC65E8C71A4DEB40DB3418FFDACC0BDB6D7C3E22F (void);
// 0x00000313 System.Void Crosstales.RTVoice.Demo.GUIDialog::StartDialog()
extern void GUIDialog_StartDialog_m63685F675276934820904EEBDC99094045F25E0C (void);
// 0x00000314 System.Void Crosstales.RTVoice.Demo.GUIDialog::Silence()
extern void GUIDialog_Silence_m46AC2A08EF788D8BAFF997CA04EAA1F61793225D (void);
// 0x00000315 System.Void Crosstales.RTVoice.Demo.GUIDialog::ChangeRateA(System.Single)
extern void GUIDialog_ChangeRateA_mA3107EE6AEFBE02E4F30635110A1FC14FE11CEEF (void);
// 0x00000316 System.Void Crosstales.RTVoice.Demo.GUIDialog::ChangeRateB(System.Single)
extern void GUIDialog_ChangeRateB_m58D330297F83EFFCFDFA5D826BB4665D440B362E (void);
// 0x00000317 System.Void Crosstales.RTVoice.Demo.GUIDialog::ChangePitchA(System.Single)
extern void GUIDialog_ChangePitchA_m93E38C9465EFECAB55E859343A04864AE5C123FD (void);
// 0x00000318 System.Void Crosstales.RTVoice.Demo.GUIDialog::ChangePitchB(System.Single)
extern void GUIDialog_ChangePitchB_m3501392CF0B0288A085A77F9C9AD230E78CA7647 (void);
// 0x00000319 System.Void Crosstales.RTVoice.Demo.GUIDialog::ChangeVolumeA(System.Single)
extern void GUIDialog_ChangeVolumeA_m54E24B19077D69E663562267BD1C432F5DD15166 (void);
// 0x0000031A System.Void Crosstales.RTVoice.Demo.GUIDialog::ChangeVolumeB(System.Single)
extern void GUIDialog_ChangeVolumeB_mD5B9714E92C44574635F44CABB82D83C63A3140B (void);
// 0x0000031B System.Void Crosstales.RTVoice.Demo.GUIDialog::GenderAChanged(System.Int32)
extern void GUIDialog_GenderAChanged_m4BE6FB99D875C0449F2B00824FFEEEEFA344509A (void);
// 0x0000031C System.Void Crosstales.RTVoice.Demo.GUIDialog::GenderBChanged(System.Int32)
extern void GUIDialog_GenderBChanged_m05238AF8A2E8CAA58CBA178265122CBB1D69FA31 (void);
// 0x0000031D System.Void Crosstales.RTVoice.Demo.GUIDialog::.ctor()
extern void GUIDialog__ctor_m8B580017C6CC95CE22552613753F0DEA5841AD3D (void);
// 0x0000031E System.Void Crosstales.RTVoice.Demo.GUIMain::Start()
extern void GUIMain_Start_m7372CBDB7A3BEE35A1F901ED11E91A4B7BE76AB6 (void);
// 0x0000031F System.Void Crosstales.RTVoice.Demo.GUIMain::Update()
extern void GUIMain_Update_m72E8B10794645412E510C88E2DFC8E7E7C3C9DBB (void);
// 0x00000320 System.Void Crosstales.RTVoice.Demo.GUIMain::OnEnable()
extern void GUIMain_OnEnable_m71648F88ED37B3A3D538055C6E8C193DC6DE1A70 (void);
// 0x00000321 System.Void Crosstales.RTVoice.Demo.GUIMain::OnDisable()
extern void GUIMain_OnDisable_m8DB75C519991AEDE69E6B0E15C34FAF713F8A253 (void);
// 0x00000322 System.Void Crosstales.RTVoice.Demo.GUIMain::OpenAssetURL()
extern void GUIMain_OpenAssetURL_m554C00A6470EE66EFF410EA0A0C2E2D21DA0FBEE (void);
// 0x00000323 System.Void Crosstales.RTVoice.Demo.GUIMain::OpenCTURL()
extern void GUIMain_OpenCTURL_mD019EF2753C79EAF07E2456F7D5EDD3DAA395199 (void);
// 0x00000324 System.Void Crosstales.RTVoice.Demo.GUIMain::Silence()
extern void GUIMain_Silence_m8466BA5A8B457FC7F43F5D674A1E8ADFD296794E (void);
// 0x00000325 System.Void Crosstales.RTVoice.Demo.GUIMain::Quit()
extern void GUIMain_Quit_m8AA76677E434D057F894A3C7BA90D2EDA9C26BBB (void);
// 0x00000326 System.Void Crosstales.RTVoice.Demo.GUIMain::onVoicesReady()
extern void GUIMain_onVoicesReady_m3CF21F9CF961535E17AED05352981AAB93D2A018 (void);
// 0x00000327 System.Void Crosstales.RTVoice.Demo.GUIMain::onErrorInfo(Crosstales.RTVoice.Model.Wrapper,System.String)
extern void GUIMain_onErrorInfo_mCFE0DB61E576EFE7FAF23DEC977A40A74C853144 (void);
// 0x00000328 System.Void Crosstales.RTVoice.Demo.GUIMain::onSpeakStart(Crosstales.RTVoice.Model.Wrapper)
extern void GUIMain_onSpeakStart_m27417E9D67714F474F87678BA70857D82B703709 (void);
// 0x00000329 System.Void Crosstales.RTVoice.Demo.GUIMain::.ctor()
extern void GUIMain__ctor_mF791B68B3C8C966D9516650EAEDEAEFDF1984495 (void);
// 0x0000032A System.Void Crosstales.RTVoice.Demo.GUIMultiAudioFilter::Start()
extern void GUIMultiAudioFilter_Start_m719D50C0D1CB458A9B3EBE7120B5E65FFB641F9E (void);
// 0x0000032B System.Void Crosstales.RTVoice.Demo.GUIMultiAudioFilter::ResetFilters()
extern void GUIMultiAudioFilter_ResetFilters_mB25D0B9790143F454CF32C4DF406DC144716B5E2 (void);
// 0x0000032C System.Void Crosstales.RTVoice.Demo.GUIMultiAudioFilter::ClearFilters()
extern void GUIMultiAudioFilter_ClearFilters_mF7041117B32EBC701756A3C688CCD82CFA93EDB9 (void);
// 0x0000032D System.Void Crosstales.RTVoice.Demo.GUIMultiAudioFilter::ReverbFilterDropdownChanged(System.Int32)
extern void GUIMultiAudioFilter_ReverbFilterDropdownChanged_mD3EEC03542C77928753FB7D6610E402AE3A31DD9 (void);
// 0x0000032E System.Void Crosstales.RTVoice.Demo.GUIMultiAudioFilter::ChorusFilterEnabled(System.Boolean)
extern void GUIMultiAudioFilter_ChorusFilterEnabled_m05B90822856588B3760D6C7DBA51B07B56AF8BE2 (void);
// 0x0000032F System.Void Crosstales.RTVoice.Demo.GUIMultiAudioFilter::EchoFilterEnabled(System.Boolean)
extern void GUIMultiAudioFilter_EchoFilterEnabled_m889EB93C4985313A63C0CD5BEB5E95BDFEDE9836 (void);
// 0x00000330 System.Void Crosstales.RTVoice.Demo.GUIMultiAudioFilter::DistortionFilterEnabled(System.Boolean)
extern void GUIMultiAudioFilter_DistortionFilterEnabled_m770295A6352C2A3791935C4B057D4CE9750DABEA (void);
// 0x00000331 System.Void Crosstales.RTVoice.Demo.GUIMultiAudioFilter::DistortionFilterChanged(System.Single)
extern void GUIMultiAudioFilter_DistortionFilterChanged_mD45256892D27B2312090C1E57B9D306E85A8052E (void);
// 0x00000332 System.Void Crosstales.RTVoice.Demo.GUIMultiAudioFilter::LowPassFilterEnabled(System.Boolean)
extern void GUIMultiAudioFilter_LowPassFilterEnabled_m453847453A00A1E47DD34953533728683EC2DEA6 (void);
// 0x00000333 System.Void Crosstales.RTVoice.Demo.GUIMultiAudioFilter::LowPassFilterChanged(System.Single)
extern void GUIMultiAudioFilter_LowPassFilterChanged_m3E8483EF19A5CB5F320F467977D8849773708EB0 (void);
// 0x00000334 System.Void Crosstales.RTVoice.Demo.GUIMultiAudioFilter::HighPassFilterEnabled(System.Boolean)
extern void GUIMultiAudioFilter_HighPassFilterEnabled_mC5C2DC19BB57E71E9569FF50D6CA25E6514BAB2A (void);
// 0x00000335 System.Void Crosstales.RTVoice.Demo.GUIMultiAudioFilter::HighPassFilterChanged(System.Single)
extern void GUIMultiAudioFilter_HighPassFilterChanged_m38973C6189E6C57B046C448AA1159A45C292034E (void);
// 0x00000336 System.Void Crosstales.RTVoice.Demo.GUIMultiAudioFilter::VolumeChanged(System.Single)
extern void GUIMultiAudioFilter_VolumeChanged_mAB7E263CD1396F8CD4FD3645CA57E6F97B716A0A (void);
// 0x00000337 System.Void Crosstales.RTVoice.Demo.GUIMultiAudioFilter::PitchChanged(System.Single)
extern void GUIMultiAudioFilter_PitchChanged_mA53D86BB7EC027060E4633C885B21A4B923164B0 (void);
// 0x00000338 System.Void Crosstales.RTVoice.Demo.GUIMultiAudioFilter::.ctor()
extern void GUIMultiAudioFilter__ctor_mBB6980B1EA20B1CAEAD03378673028A2DC92D631 (void);
// 0x00000339 System.Void Crosstales.RTVoice.Demo.GUIScenes::LoadPrevoiusScene()
extern void GUIScenes_LoadPrevoiusScene_m61DD41DB71B23058F4C9FCF166F444AF85AFE50D (void);
// 0x0000033A System.Void Crosstales.RTVoice.Demo.GUIScenes::LoadNextScene()
extern void GUIScenes_LoadNextScene_mCC0676F07E77B77C52C9C04FAA4BF5F4CC9EEF50 (void);
// 0x0000033B System.Void Crosstales.RTVoice.Demo.GUIScenes::.ctor()
extern void GUIScenes__ctor_m893D0F0F9040E5D1315BE3CDAFFD76BC9CC8356B (void);
// 0x0000033C System.Void Crosstales.RTVoice.Demo.GUISpeech::Start()
extern void GUISpeech_Start_m914C19EE24E5A0879D9A62F750D1CAB9CE320508 (void);
// 0x0000033D System.Void Crosstales.RTVoice.Demo.GUISpeech::Update()
extern void GUISpeech_Update_mE76474936989A2D20782BB168AAE115E9419F12C (void);
// 0x0000033E System.Void Crosstales.RTVoice.Demo.GUISpeech::OnEnable()
extern void GUISpeech_OnEnable_mCFABDC217D6DE585904E61E9A51ED848DBC781C5 (void);
// 0x0000033F System.Void Crosstales.RTVoice.Demo.GUISpeech::OnDisable()
extern void GUISpeech_OnDisable_m50F82241C76C641B860E923A54D7DDCAC32E89CD (void);
// 0x00000340 System.Void Crosstales.RTVoice.Demo.GUISpeech::OnDestroy()
extern void GUISpeech_OnDestroy_mC12F6145213B57E9BFFDECA747286F3E5F1FD224 (void);
// 0x00000341 System.Void Crosstales.RTVoice.Demo.GUISpeech::Silence()
extern void GUISpeech_Silence_mFE4D5660663F0D8CC964D1669B58EDEB3E9C9D9F (void);
// 0x00000342 System.Void Crosstales.RTVoice.Demo.GUISpeech::ChangeRate(System.Single)
extern void GUISpeech_ChangeRate_mB10F8E108D948502D0A7AC43371282439087307C (void);
// 0x00000343 System.Void Crosstales.RTVoice.Demo.GUISpeech::ChangeVolume(System.Single)
extern void GUISpeech_ChangeVolume_m4E6E48931AFC02E853E1FB1863980C3662758F6B (void);
// 0x00000344 System.Void Crosstales.RTVoice.Demo.GUISpeech::ChangePitch(System.Single)
extern void GUISpeech_ChangePitch_mF562535E186F337153000EAC3122B55533B7A738 (void);
// 0x00000345 System.Void Crosstales.RTVoice.Demo.GUISpeech::ChangeNative(System.Boolean)
extern void GUISpeech_ChangeNative_m40909DF4D07E0BD6AC806B503BD7626EF73C950B (void);
// 0x00000346 System.Void Crosstales.RTVoice.Demo.GUISpeech::ChangeMaryTTS(System.Boolean)
extern void GUISpeech_ChangeMaryTTS_m3734C7B7D05B88D387C99EFC25C1EA4989FE61C3 (void);
// 0x00000347 System.Void Crosstales.RTVoice.Demo.GUISpeech::GenderChanged(System.Int32)
extern void GUISpeech_GenderChanged_m66F2A0B8BFF5C8A165A18923A0AA8595EE97F25F (void);
// 0x00000348 System.Void Crosstales.RTVoice.Demo.GUISpeech::onProviderChange(System.String)
extern void GUISpeech_onProviderChange_m44CE107A6F1EF38552772B03F302E0B7F10242E4 (void);
// 0x00000349 System.Void Crosstales.RTVoice.Demo.GUISpeech::clearVoicesList()
extern void GUISpeech_clearVoicesList_m5FF61890C20A56018A64FF93A854863481D4C2CD (void);
// 0x0000034A System.Void Crosstales.RTVoice.Demo.GUISpeech::buildVoicesList()
extern void GUISpeech_buildVoicesList_m78FF9167787CBFA9FA6697455BA80674584DC6A0 (void);
// 0x0000034B System.Void Crosstales.RTVoice.Demo.GUISpeech::.ctor()
extern void GUISpeech__ctor_m3C22432AAF13674A9235983819D228F00ED98015 (void);
// 0x0000034C System.Void Crosstales.RTVoice.Demo.GUISpeech::.cctor()
extern void GUISpeech__cctor_m93D43CA4562A587B863A7257A4DC469185150F0D (void);
// 0x0000034D System.Void Crosstales.RTVoice.Demo.NativeAudio::Start()
extern void NativeAudio_Start_m7B15600F3DA329521D8E905267BDF649DFEC2C20 (void);
// 0x0000034E System.Void Crosstales.RTVoice.Demo.NativeAudio::OnEnable()
extern void NativeAudio_OnEnable_mC4AA950771A508BC7831368EE19ABCD209324AC5 (void);
// 0x0000034F System.Void Crosstales.RTVoice.Demo.NativeAudio::OnDisable()
extern void NativeAudio_OnDisable_m9047B791EFD42EE37923E238FDC1D8436BDE8204 (void);
// 0x00000350 System.Void Crosstales.RTVoice.Demo.NativeAudio::StartTTS()
extern void NativeAudio_StartTTS_mFF83EF78D245FB3FF2F487BC008AF5774410E35D (void);
// 0x00000351 System.Void Crosstales.RTVoice.Demo.NativeAudio::Silence()
extern void NativeAudio_Silence_mA913AE2A926FF0863617E78716D52B5858B9D22F (void);
// 0x00000352 System.Void Crosstales.RTVoice.Demo.NativeAudio::play(Crosstales.RTVoice.Model.Wrapper)
extern void NativeAudio_play_m1194349F63CBB1714BB15FAC0C461F49848CC01B (void);
// 0x00000353 System.Void Crosstales.RTVoice.Demo.NativeAudio::stop(Crosstales.RTVoice.Model.Wrapper)
extern void NativeAudio_stop_mE4B57E83EDE491A1A55FEB1A353BDB2BD58890A7 (void);
// 0x00000354 System.Void Crosstales.RTVoice.Demo.NativeAudio::.ctor()
extern void NativeAudio__ctor_mCE75BDEC29D267A10F70A9C6012F0832A959AC90 (void);
// 0x00000355 System.Void Crosstales.RTVoice.Demo.PreGeneratedAudio::Start()
extern void PreGeneratedAudio_Start_mC5269E7BD3DB9E19E4035465C8B1E23F9D6D6648 (void);
// 0x00000356 System.Void Crosstales.RTVoice.Demo.PreGeneratedAudio::Update()
extern void PreGeneratedAudio_Update_mB90A2E9062E06701873103FA31666C0A5CDF76C7 (void);
// 0x00000357 System.Void Crosstales.RTVoice.Demo.PreGeneratedAudio::OnEnable()
extern void PreGeneratedAudio_OnEnable_mA75658E67F3EAFE883740B7D8E5091A208A808E2 (void);
// 0x00000358 System.Void Crosstales.RTVoice.Demo.PreGeneratedAudio::OnDisable()
extern void PreGeneratedAudio_OnDisable_m5081BB0ECF34C45BAA4BB27910A65B6C8ACC56BB (void);
// 0x00000359 System.Void Crosstales.RTVoice.Demo.PreGeneratedAudio::Play()
extern void PreGeneratedAudio_Play_m2074B06CADE69F9395EE1134517D47DDFB881794 (void);
// 0x0000035A System.Void Crosstales.RTVoice.Demo.PreGeneratedAudio::Silence()
extern void PreGeneratedAudio_Silence_m61A13A1764338E59F6F3369DA9919AF33881EE9F (void);
// 0x0000035B System.Void Crosstales.RTVoice.Demo.PreGeneratedAudio::Stop()
extern void PreGeneratedAudio_Stop_m0A2E1752832E34681E9959DC232FE7EB43B24009 (void);
// 0x0000035C System.Void Crosstales.RTVoice.Demo.PreGeneratedAudio::speakAudioGenerationCompleteMethod(Crosstales.RTVoice.Model.Wrapper)
extern void PreGeneratedAudio_speakAudioGenerationCompleteMethod_m69B145D1B28D832030161F7637A5BCABD58852FA (void);
// 0x0000035D System.Void Crosstales.RTVoice.Demo.PreGeneratedAudio::.ctor()
extern void PreGeneratedAudio__ctor_m600793BB8FCE1D9B19514AD052CAE7B04C3C98DB (void);
// 0x0000035E System.Void Crosstales.RTVoice.Demo.SendMessage::Start()
extern void SendMessage_Start_mC3B0C517710E9385582737FD01CB8AF207642318 (void);
// 0x0000035F System.Void Crosstales.RTVoice.Demo.SendMessage::Play()
extern void SendMessage_Play_m33614691088DB64C1EC850C0D9F9D4E59D87B4FD (void);
// 0x00000360 System.Void Crosstales.RTVoice.Demo.SendMessage::SpeakerA()
extern void SendMessage_SpeakerA_mBAF98AAA8F2B3BA9C1C4E411369581DACB6F788A (void);
// 0x00000361 System.Collections.IEnumerator Crosstales.RTVoice.Demo.SendMessage::SpeakerB()
extern void SendMessage_SpeakerB_m6C538E9CD2CDABCC1F85DC7514AB9ABAC7523F90 (void);
// 0x00000362 System.Void Crosstales.RTVoice.Demo.SendMessage::Silence()
extern void SendMessage_Silence_mFE3E5EDEFDF09D408486752F4EA63CF1B716622B (void);
// 0x00000363 System.Void Crosstales.RTVoice.Demo.SendMessage::.ctor()
extern void SendMessage__ctor_mC33F30828D6C01539FFDD42A74E3A6BF40DB2858 (void);
// 0x00000364 System.Void Crosstales.RTVoice.Demo.SendMessage/<SpeakerB>d__8::.ctor(System.Int32)
extern void U3CSpeakerBU3Ed__8__ctor_m4A8ED00A523A95298E776A4A523FF2A04F3D6724 (void);
// 0x00000365 System.Void Crosstales.RTVoice.Demo.SendMessage/<SpeakerB>d__8::System.IDisposable.Dispose()
extern void U3CSpeakerBU3Ed__8_System_IDisposable_Dispose_mF7B8070E854813D83A83738E3A5AFF4A85A58C2F (void);
// 0x00000366 System.Boolean Crosstales.RTVoice.Demo.SendMessage/<SpeakerB>d__8::MoveNext()
extern void U3CSpeakerBU3Ed__8_MoveNext_mA8E71C308F4A8390282D16D7ADDD7CC2B6DB59C4 (void);
// 0x00000367 System.Object Crosstales.RTVoice.Demo.SendMessage/<SpeakerB>d__8::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CSpeakerBU3Ed__8_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m410D072C0C20CC5489B8A8A4782DF66577B670DD (void);
// 0x00000368 System.Void Crosstales.RTVoice.Demo.SendMessage/<SpeakerB>d__8::System.Collections.IEnumerator.Reset()
extern void U3CSpeakerBU3Ed__8_System_Collections_IEnumerator_Reset_m51FC493E4D2F9C532B5C80D086A39099F32302ED (void);
// 0x00000369 System.Object Crosstales.RTVoice.Demo.SendMessage/<SpeakerB>d__8::System.Collections.IEnumerator.get_Current()
extern void U3CSpeakerBU3Ed__8_System_Collections_IEnumerator_get_Current_mE1A5CEC7B178D65FF55AC03932D40A96DA8697A6 (void);
// 0x0000036A System.Void Crosstales.RTVoice.Demo.SequenceCaller::Start()
extern void SequenceCaller_Start_m41332745CA30FEA1C0829BEA4135F5AF9E6D21DB (void);
// 0x0000036B System.Void Crosstales.RTVoice.Demo.SequenceCaller::playNextSequence()
extern void SequenceCaller_playNextSequence_m93D85D4C5AE0F3CA407524A645E9FF0957E2EE32 (void);
// 0x0000036C System.Void Crosstales.RTVoice.Demo.SequenceCaller::.ctor()
extern void SequenceCaller__ctor_mA530CC0AF0D6691536792A0B0CD5F19E90F78473 (void);
// 0x0000036D System.Void Crosstales.RTVoice.Demo.Simple::Start()
extern void Simple_Start_m1C9FA4A3B0B27B5B1C506CCF283EE97DDB018408 (void);
// 0x0000036E System.Void Crosstales.RTVoice.Demo.Simple::OnEnable()
extern void Simple_OnEnable_mCD821BE1DC04000AE21CDA672EBFF1F624E0668A (void);
// 0x0000036F System.Void Crosstales.RTVoice.Demo.Simple::OnDisable()
extern void Simple_OnDisable_mB8005CEC41D155AB7BD8FB97E17FFB3928B937F2 (void);
// 0x00000370 System.Void Crosstales.RTVoice.Demo.Simple::Play()
extern void Simple_Play_m9CD479D8F27512D450A898A14DA9924EBAFFFA40 (void);
// 0x00000371 System.Void Crosstales.RTVoice.Demo.Simple::SpeakerA()
extern void Simple_SpeakerA_mB1991EE6CADA025EE2E97D71140F44A5660A9DE9 (void);
// 0x00000372 System.Void Crosstales.RTVoice.Demo.Simple::SpeakerB()
extern void Simple_SpeakerB_mC0390702E90CE0271A3334027138A6172B62C813 (void);
// 0x00000373 System.Void Crosstales.RTVoice.Demo.Simple::Silence()
extern void Simple_Silence_m4F72A39DDBE67998AD6D9F8581E51CA3D3FCC12F (void);
// 0x00000374 System.Void Crosstales.RTVoice.Demo.Simple::speakAudio()
extern void Simple_speakAudio_m581798F2DBE6342E307BB14B0691461CC8828637 (void);
// 0x00000375 System.Void Crosstales.RTVoice.Demo.Simple::speakAudioGenerationStartMethod(Crosstales.RTVoice.Model.Wrapper)
extern void Simple_speakAudioGenerationStartMethod_m296BDE5A9B768EA4F0660FBB470DEB88D02CBBCE (void);
// 0x00000376 System.Void Crosstales.RTVoice.Demo.Simple::speakAudioGenerationCompleteMethod(Crosstales.RTVoice.Model.Wrapper)
extern void Simple_speakAudioGenerationCompleteMethod_m38E89410F3E4BBA6D3651782F6D03633A3520EB6 (void);
// 0x00000377 System.Void Crosstales.RTVoice.Demo.Simple::speakStartMethod(Crosstales.RTVoice.Model.Wrapper)
extern void Simple_speakStartMethod_mA826DE4011A5B0694B60B634FD60926D6481E840 (void);
// 0x00000378 System.Void Crosstales.RTVoice.Demo.Simple::speakCompleteMethod(Crosstales.RTVoice.Model.Wrapper)
extern void Simple_speakCompleteMethod_m1211637DC968CF06BCD5CF8134208E492CA9AFA0 (void);
// 0x00000379 System.Void Crosstales.RTVoice.Demo.Simple::speakCurrentWordMethod(Crosstales.RTVoice.Model.Wrapper,System.String[],System.Int32)
extern void Simple_speakCurrentWordMethod_m796213B862E640057D70B16F81EA8EFAEF015421 (void);
// 0x0000037A System.Void Crosstales.RTVoice.Demo.Simple::speakCurrentPhonemeMethod(Crosstales.RTVoice.Model.Wrapper,System.String)
extern void Simple_speakCurrentPhonemeMethod_mF32175D48763E587F075E3581153707F39FF182C (void);
// 0x0000037B System.Void Crosstales.RTVoice.Demo.Simple::speakCurrentVisemeMethod(Crosstales.RTVoice.Model.Wrapper,System.String)
extern void Simple_speakCurrentVisemeMethod_m4C8425A23CBDE1E1E934C573C9B0F5FBC586AB51 (void);
// 0x0000037C System.Void Crosstales.RTVoice.Demo.Simple::.ctor()
extern void Simple__ctor_m24CAE42DE5A97D4A78A0E2F5908A395D68F5939C (void);
// 0x0000037D System.Void Crosstales.RTVoice.Demo.SimpleNative::Start()
extern void SimpleNative_Start_mFF5D40B326D469F0F6E40D718D3BF26ED6F436C4 (void);
// 0x0000037E System.Void Crosstales.RTVoice.Demo.SimpleNative::OnEnable()
extern void SimpleNative_OnEnable_mC6A69A656CD8AD8992B01497F65A46812CCB4280 (void);
// 0x0000037F System.Void Crosstales.RTVoice.Demo.SimpleNative::OnDisable()
extern void SimpleNative_OnDisable_mE36D0762819EE1FFC2F956D19F6F11E6B39ADED3 (void);
// 0x00000380 System.Void Crosstales.RTVoice.Demo.SimpleNative::Play()
extern void SimpleNative_Play_mA6B8662197FE766631A78D838D4EFC079AE5606F (void);
// 0x00000381 System.Void Crosstales.RTVoice.Demo.SimpleNative::SpeakerA()
extern void SimpleNative_SpeakerA_m42BED662EB5E4E00DD1B8BCE9E31F4E750610314 (void);
// 0x00000382 System.Void Crosstales.RTVoice.Demo.SimpleNative::SpeakerB()
extern void SimpleNative_SpeakerB_m3D766AC08EA62475859730378366748FACA1AAEF (void);
// 0x00000383 System.Void Crosstales.RTVoice.Demo.SimpleNative::SpeakerC()
extern void SimpleNative_SpeakerC_m7EEF291A446D634CA9C2F9C1550F6DB2C7CC5AFE (void);
// 0x00000384 System.Void Crosstales.RTVoice.Demo.SimpleNative::Silence()
extern void SimpleNative_Silence_m00617B943FB58A35B777087EFAB7DC0390EF488D (void);
// 0x00000385 System.Void Crosstales.RTVoice.Demo.SimpleNative::speakStartMethod(Crosstales.RTVoice.Model.Wrapper)
extern void SimpleNative_speakStartMethod_m1D0B71EE7F27497F37530BDF4B7127D8CD04AA01 (void);
// 0x00000386 System.Void Crosstales.RTVoice.Demo.SimpleNative::speakCompleteMethod(Crosstales.RTVoice.Model.Wrapper)
extern void SimpleNative_speakCompleteMethod_m07B8077C2EE5E7C8FEF6B8F4270E64724521B0A2 (void);
// 0x00000387 System.Void Crosstales.RTVoice.Demo.SimpleNative::speakCurrentWordMethod(Crosstales.RTVoice.Model.Wrapper,System.String[],System.Int32)
extern void SimpleNative_speakCurrentWordMethod_mE89C5A7A54ABEC4BBCC453BA18BA4CF487A2790A (void);
// 0x00000388 System.Void Crosstales.RTVoice.Demo.SimpleNative::speakCurrentPhonemeMethod(Crosstales.RTVoice.Model.Wrapper,System.String)
extern void SimpleNative_speakCurrentPhonemeMethod_mC9734C375F0EDAF7439911F1E8ACE438AF3DAA6D (void);
// 0x00000389 System.Void Crosstales.RTVoice.Demo.SimpleNative::speakCurrentVisemeMethod(Crosstales.RTVoice.Model.Wrapper,System.String)
extern void SimpleNative_speakCurrentVisemeMethod_m0CB68E84B8A430FD69E468C3B22BE3B9A24C4520 (void);
// 0x0000038A System.Void Crosstales.RTVoice.Demo.SimpleNative::.ctor()
extern void SimpleNative__ctor_m31127891658699460AE9778FC73FDD04A949DD92 (void);
// 0x0000038B System.Void Crosstales.RTVoice.Demo.SpeakWrapper::Start()
extern void SpeakWrapper_Start_m9CFA5554AD17DA428A699DE3C595AFA320BF4BF1 (void);
// 0x0000038C System.Void Crosstales.RTVoice.Demo.SpeakWrapper::Speak()
extern void SpeakWrapper_Speak_m8DA6C583BDA8A3859D1CE56C1942CF61C37A14CF (void);
// 0x0000038D System.Void Crosstales.RTVoice.Demo.SpeakWrapper::.ctor()
extern void SpeakWrapper__ctor_m3B9EB9D815A23AA5688525C8BCEE0CB464FF52D6 (void);
// 0x0000038E System.Void Crosstales.RTVoice.Demo.Util.MaterialChanger::Start()
extern void MaterialChanger_Start_m0C2461F4790C9494834984CF01A5BE5EB9494649 (void);
// 0x0000038F System.Void Crosstales.RTVoice.Demo.Util.MaterialChanger::Update()
extern void MaterialChanger_Update_mC340AC29ED31872B79723D5FA28155F3DD532A1A (void);
// 0x00000390 System.Void Crosstales.RTVoice.Demo.Util.MaterialChanger::.ctor()
extern void MaterialChanger__ctor_m58C145E9E4EDAD6376465516297155BB1C0F3C31 (void);
// 0x00000391 System.Void Crosstales.RTVoice.Demo.Util.NativeController::Update()
extern void NativeController_Update_m1271D05F8D9E1578C8E2F32B20F8C63D74F1CA84 (void);
// 0x00000392 System.Void Crosstales.RTVoice.Demo.Util.NativeController::.ctor()
extern void NativeController__ctor_m25EA044D304AD6E5549D2FF71D3E17973E75C74C (void);
// 0x00000393 System.Void Crosstales.RTVoice.Demo.Util.PlatformController::Start()
extern void PlatformController_Start_m1DA2083109765F28D7EF847CC5F5E8EC64CE46C5 (void);
// 0x00000394 System.Void Crosstales.RTVoice.Demo.Util.PlatformController::OnEnable()
extern void PlatformController_OnEnable_mEA0A4644683108150CA9835B725AC111FEEFA546 (void);
// 0x00000395 System.Void Crosstales.RTVoice.Demo.Util.PlatformController::OnDisable()
extern void PlatformController_OnDisable_m3B6D967163C1F5735A4F0AF05A73705332CA7213 (void);
// 0x00000396 System.Void Crosstales.RTVoice.Demo.Util.PlatformController::onProviderChange(System.String)
extern void PlatformController_onProviderChange_m8CAF3F07F85DCB20C87E119D6169EBE98588D6DA (void);
// 0x00000397 System.Void Crosstales.RTVoice.Demo.Util.PlatformController::.ctor()
extern void PlatformController__ctor_m7817457510FE2BE551FED2BF9ABF6891D34ACA9E (void);
// 0x00000398 System.Void Crosstales.RTVoice.Demo.Util.iOSController::Start()
extern void iOSController_Start_m477DA0F805B79FC61CB161673F5F83E7BB0FBE38 (void);
// 0x00000399 System.Void Crosstales.RTVoice.Demo.Util.iOSController::OnDestroy()
extern void iOSController_OnDestroy_m5D07FE3DA9084A89A0EADF5091515FFCD1E2F4A5 (void);
// 0x0000039A System.Void Crosstales.RTVoice.Demo.Util.iOSController::.ctor()
extern void iOSController__ctor_mF3C2B153851AE3D5EB35156F0B238F2AC14ECBAE (void);
// 0x0000039B System.Void Crosstales.RTVoice.SALSA.Bots::OnEnable()
extern void Bots_OnEnable_m98DF8572242B3BAB9A7D291A66553C92B7D4FF31 (void);
// 0x0000039C System.Void Crosstales.RTVoice.SALSA.Bots::OnDisable()
extern void Bots_OnDisable_m3A2168E0D981363BA3B0C42546C0350DBB4B7BBF (void);
// 0x0000039D System.Void Crosstales.RTVoice.SALSA.Bots::Update()
extern void Bots_Update_m4E417CBE773593D77E9C9535C1D8CDA3B9512097 (void);
// 0x0000039E System.Void Crosstales.RTVoice.SALSA.Bots::speakCompleteMethod(Crosstales.RTVoice.Model.Wrapper)
extern void Bots_speakCompleteMethod_m7818F9F9C31F2013363107CF30783D85C54BC9C4 (void);
// 0x0000039F System.Void Crosstales.RTVoice.SALSA.Bots::.ctor()
extern void Bots__ctor_m605DCE9CBB7C8CC6D471608FCDB8D956D775A035 (void);
// 0x000003A0 System.Void Crosstales.RTVoice.SALSA.Speak::Start()
extern void Speak_Start_m45972028AA87462EE3A3187C648D033FAD75B351 (void);
// 0x000003A1 System.Void Crosstales.RTVoice.SALSA.Speak::OnEnable()
extern void Speak_OnEnable_m33CC7D1069143D300F4068FC1A5B5281A07FB576 (void);
// 0x000003A2 System.Void Crosstales.RTVoice.SALSA.Speak::OnDisable()
extern void Speak_OnDisable_m980C018063639E610F9215382EDE5EAFD3BD9337 (void);
// 0x000003A3 System.Void Crosstales.RTVoice.SALSA.Speak::Talk()
extern void Speak_Talk_mBBC47C45CB25E800DFA738D10A54CA87318CD93D (void);
// 0x000003A4 System.Void Crosstales.RTVoice.SALSA.Speak::speakAudioGenerationCompleteMethod(Crosstales.RTVoice.Model.Wrapper)
extern void Speak_speakAudioGenerationCompleteMethod_mF525F97FFC2C1B1D85D6F059EE6C1AEDAF3BAB13 (void);
// 0x000003A5 System.Void Crosstales.RTVoice.SALSA.Speak::.ctor()
extern void Speak__ctor_m6C8AF93900DA7F9F618BC91C7421448BF4486818 (void);
// 0x000003A6 System.Void Crosstales.RTVoice.SALSA.Speak2D::Start()
extern void Speak2D_Start_mEA469FB054C5161F568D599A19F7C44807C13CDC (void);
// 0x000003A7 System.Void Crosstales.RTVoice.SALSA.Speak2D::OnEnable()
extern void Speak2D_OnEnable_mF685910CAC43E9EB012E0F1085E8EFE06C9043A0 (void);
// 0x000003A8 System.Void Crosstales.RTVoice.SALSA.Speak2D::OnDisable()
extern void Speak2D_OnDisable_m0BDAB3F8590A468C22779C2BC384C23067FB1048 (void);
// 0x000003A9 System.Void Crosstales.RTVoice.SALSA.Speak2D::Talk()
extern void Speak2D_Talk_mAC06A03B03A34401BC6BF5A8517B4E95AD472606 (void);
// 0x000003AA System.Void Crosstales.RTVoice.SALSA.Speak2D::speakAudioGenerationCompleteMethod(Crosstales.RTVoice.Model.Wrapper)
extern void Speak2D_speakAudioGenerationCompleteMethod_m4FF0ABF0BA1BAF93CDB12CA2E4D73B0476D456CE (void);
// 0x000003AB System.Void Crosstales.RTVoice.SALSA.Speak2D::.ctor()
extern void Speak2D__ctor_mABEE6F3AD07A1A74069360891AE1F4F12704E6F5 (void);
// 0x000003AC System.Void Crosstales.RTVoice.SALSA.SpeakSimple::Silence()
extern void SpeakSimple_Silence_m64D382560126AC78C96FB47ADC1EA49B753D7F43 (void);
// 0x000003AD System.Void Crosstales.RTVoice.SALSA.SpeakSimple::Talk()
extern void SpeakSimple_Talk_m44E11EC19A6470CD0845F3B226BD04EB6EBF4055 (void);
// 0x000003AE System.Void Crosstales.RTVoice.SALSA.SpeakSimple::.ctor()
extern void SpeakSimple__ctor_m3379ADE145FF7388D2A087F1865C27568B50E4A6 (void);
// 0x000003AF System.Void Crosstales.UI.Social::Facebook()
extern void Social_Facebook_mC2E09264868CD8006AD4D69B817D855488422A7C (void);
// 0x000003B0 System.Void Crosstales.UI.Social::Twitter()
extern void Social_Twitter_m492C7F14348366FA7719E69749DBB30A76DA3B8A (void);
// 0x000003B1 System.Void Crosstales.UI.Social::LinkedIn()
extern void Social_LinkedIn_m69D1E11EDE1C1748E8ED8463D54DD59513371D94 (void);
// 0x000003B2 System.Void Crosstales.UI.Social::Youtube()
extern void Social_Youtube_m5C4E721503773BADFE0544DD381114729E7F31A5 (void);
// 0x000003B3 System.Void Crosstales.UI.Social::Discord()
extern void Social_Discord_m5C364639DE37DCF942CCF9A29944B535E5E39EC1 (void);
// 0x000003B4 System.Void Crosstales.UI.Social::.ctor()
extern void Social__ctor_m5661EFE8FEE7EC2324FDAA88191E3B5C6F284F67 (void);
// 0x000003B5 System.Void Crosstales.UI.StaticManager::Quit()
extern void StaticManager_Quit_mAC39DA93EB2E5BA9D210BDE9C23A09A4D4266504 (void);
// 0x000003B6 System.Void Crosstales.UI.StaticManager::OpenCrosstales()
extern void StaticManager_OpenCrosstales_m5E5E7FC3F7DE7D9ED7330C6D4B60E5A686E9F5BF (void);
// 0x000003B7 System.Void Crosstales.UI.StaticManager::OpenAssetstore()
extern void StaticManager_OpenAssetstore_m949B8AD95A36C1364BEA2416A9E32D134340890F (void);
// 0x000003B8 System.Void Crosstales.UI.StaticManager::.ctor()
extern void StaticManager__ctor_m745654B0948B4DF9A2FD0FB65806A03164ABCC65 (void);
// 0x000003B9 System.Void Crosstales.UI.UIDrag::Start()
extern void UIDrag_Start_mDB1DCDDEB984EDD5DF799896F85B936BFB18D54D (void);
// 0x000003BA System.Void Crosstales.UI.UIDrag::BeginDrag()
extern void UIDrag_BeginDrag_m583EA2730DACC92BC96F551DABD1FBF04EA5FF8B (void);
// 0x000003BB System.Void Crosstales.UI.UIDrag::OnDrag()
extern void UIDrag_OnDrag_mD59F3EA41622E034504FFFB95F247C6CA398BF2A (void);
// 0x000003BC System.Void Crosstales.UI.UIDrag::.ctor()
extern void UIDrag__ctor_mF4C53D49822FE9B02461E092AE6F7AC45A011113 (void);
// 0x000003BD System.Void Crosstales.UI.UIFocus::Start()
extern void UIFocus_Start_m880CB1FB7349D96ED9EB3CF27BE27A16BD2E5F24 (void);
// 0x000003BE System.Void Crosstales.UI.UIFocus::OnPanelEnter()
extern void UIFocus_OnPanelEnter_m33CF9E28098755A82CE6764377DF302C10F71973 (void);
// 0x000003BF System.Void Crosstales.UI.UIFocus::.ctor()
extern void UIFocus__ctor_mDEF7E588D05EBA2BF8ECFB87875BCD5A614829A0 (void);
// 0x000003C0 System.Void Crosstales.UI.UIHint::Start()
extern void UIHint_Start_m96229DC6152E6DC547F659396A38AB349084473D (void);
// 0x000003C1 System.Void Crosstales.UI.UIHint::FadeUp()
extern void UIHint_FadeUp_m9D94298919C4378BE30465D8A82923CBA316D3C1 (void);
// 0x000003C2 System.Void Crosstales.UI.UIHint::FadeDown()
extern void UIHint_FadeDown_m3C90D1A432527CE936339156444BD7D108D729C7 (void);
// 0x000003C3 System.Collections.IEnumerator Crosstales.UI.UIHint::lerpAlphaDown(System.Single,System.Single,System.Single,System.Single,UnityEngine.CanvasGroup)
extern void UIHint_lerpAlphaDown_mC074A670DEDC2E2C823C0F84A4EBC6D960BD5D42 (void);
// 0x000003C4 System.Collections.IEnumerator Crosstales.UI.UIHint::lerpAlphaUp(System.Single,System.Single,System.Single,System.Single,UnityEngine.CanvasGroup)
extern void UIHint_lerpAlphaUp_m84B48E04DD5ABE68C1A95A77A51EA2B8E9C61BBD (void);
// 0x000003C5 System.Void Crosstales.UI.UIHint::.ctor()
extern void UIHint__ctor_mD54D6ED200DAA952DD7D97643AE270D572902BCD (void);
// 0x000003C6 System.Void Crosstales.UI.UIHint/<lerpAlphaDown>d__8::.ctor(System.Int32)
extern void U3ClerpAlphaDownU3Ed__8__ctor_m137125C8737CD01C1A17E318878AFB2B0D0CDB14 (void);
// 0x000003C7 System.Void Crosstales.UI.UIHint/<lerpAlphaDown>d__8::System.IDisposable.Dispose()
extern void U3ClerpAlphaDownU3Ed__8_System_IDisposable_Dispose_m82EA5F02C54838CDFAD7E3C549962D0DB1151FE1 (void);
// 0x000003C8 System.Boolean Crosstales.UI.UIHint/<lerpAlphaDown>d__8::MoveNext()
extern void U3ClerpAlphaDownU3Ed__8_MoveNext_m954DF2644E926149C6AC6AA1695D54705969FCAE (void);
// 0x000003C9 System.Object Crosstales.UI.UIHint/<lerpAlphaDown>d__8::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3ClerpAlphaDownU3Ed__8_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mB42DD7F0CDC10AADCD99116A14BF77D16197AE49 (void);
// 0x000003CA System.Void Crosstales.UI.UIHint/<lerpAlphaDown>d__8::System.Collections.IEnumerator.Reset()
extern void U3ClerpAlphaDownU3Ed__8_System_Collections_IEnumerator_Reset_mB4243BFE9BA869109D22485266636F346DE3495F (void);
// 0x000003CB System.Object Crosstales.UI.UIHint/<lerpAlphaDown>d__8::System.Collections.IEnumerator.get_Current()
extern void U3ClerpAlphaDownU3Ed__8_System_Collections_IEnumerator_get_Current_m641305F2EB99B80E211B4E6E99E859C2B7AAE63B (void);
// 0x000003CC System.Void Crosstales.UI.UIHint/<lerpAlphaUp>d__9::.ctor(System.Int32)
extern void U3ClerpAlphaUpU3Ed__9__ctor_m41FA43BCB2D14D39F0DC1E1AFFCECB0D18E02419 (void);
// 0x000003CD System.Void Crosstales.UI.UIHint/<lerpAlphaUp>d__9::System.IDisposable.Dispose()
extern void U3ClerpAlphaUpU3Ed__9_System_IDisposable_Dispose_mD5454F31F93F6B5DCCB420AB1241DD2E2DEE331B (void);
// 0x000003CE System.Boolean Crosstales.UI.UIHint/<lerpAlphaUp>d__9::MoveNext()
extern void U3ClerpAlphaUpU3Ed__9_MoveNext_mB9C62184F82A3D1EF71E0A03CE960D9F020EA556 (void);
// 0x000003CF System.Object Crosstales.UI.UIHint/<lerpAlphaUp>d__9::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3ClerpAlphaUpU3Ed__9_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m07EF2B75C53B187A10E0ACC00AB20DF4CAB27CD7 (void);
// 0x000003D0 System.Void Crosstales.UI.UIHint/<lerpAlphaUp>d__9::System.Collections.IEnumerator.Reset()
extern void U3ClerpAlphaUpU3Ed__9_System_Collections_IEnumerator_Reset_m1C29B48A4BFBB4956CB59DAF52A587455998D965 (void);
// 0x000003D1 System.Object Crosstales.UI.UIHint/<lerpAlphaUp>d__9::System.Collections.IEnumerator.get_Current()
extern void U3ClerpAlphaUpU3Ed__9_System_Collections_IEnumerator_get_Current_mA92CCF9323E6BC3E4B224B1CBF4098D02978B980 (void);
// 0x000003D2 System.Void Crosstales.UI.UIResize::Awake()
extern void UIResize_Awake_m84548F0C66BF42D9B3D85FDC887AF9F592314470 (void);
// 0x000003D3 System.Void Crosstales.UI.UIResize::OnPointerDown(UnityEngine.EventSystems.PointerEventData)
extern void UIResize_OnPointerDown_m1E7B045EF724B9EFEF997DE80322BBBC494E202C (void);
// 0x000003D4 System.Void Crosstales.UI.UIResize::OnDrag(UnityEngine.EventSystems.PointerEventData)
extern void UIResize_OnDrag_m8F2FC62C5CD268AA28FB7B2EED64DDCA2079BB9E (void);
// 0x000003D5 System.Void Crosstales.UI.UIResize::.ctor()
extern void UIResize__ctor_m4B0D088874E80F5BF9D3321949439328C8BA3744 (void);
// 0x000003D6 System.Void Crosstales.UI.UIWindowManager::Start()
extern void UIWindowManager_Start_mFE0D244F094A34B2D47F20D9DA45C19166630F1A (void);
// 0x000003D7 System.Void Crosstales.UI.UIWindowManager::ChangeState(UnityEngine.GameObject)
extern void UIWindowManager_ChangeState_m98045ADD1B1A1380FF64496BFC4756B5F1051C59 (void);
// 0x000003D8 System.Void Crosstales.UI.UIWindowManager::.ctor()
extern void UIWindowManager__ctor_m12928DA297937399EBC6FD9D27BEEF646E068ADE (void);
// 0x000003D9 System.Void Crosstales.UI.WindowManager::Start()
extern void WindowManager_Start_m1AFD4847D6D468A27278A2D72509D0D28362A7B5 (void);
// 0x000003DA System.Void Crosstales.UI.WindowManager::Update()
extern void WindowManager_Update_m17970A83622480894EA2395FF8A4C5560CEB8115 (void);
// 0x000003DB System.Void Crosstales.UI.WindowManager::SwitchPanel()
extern void WindowManager_SwitchPanel_m6B16C8B9EA436D051DF3D864DD4C97B915F4DF55 (void);
// 0x000003DC System.Void Crosstales.UI.WindowManager::OpenPanel()
extern void WindowManager_OpenPanel_mCAACC7E648449EC4F47E3FDBC730E00B319A680B (void);
// 0x000003DD System.Void Crosstales.UI.WindowManager::ClosePanel()
extern void WindowManager_ClosePanel_m5C051458EB873C2A040A526707370BD675CACD21 (void);
// 0x000003DE System.Void Crosstales.UI.WindowManager::.ctor()
extern void WindowManager__ctor_m7A48D60A914B455DE9A65EF38F6718DD9E92B782 (void);
// 0x000003DF System.Void Crosstales.UI.Util.AudioFilterController::Start()
extern void AudioFilterController_Start_m46A3ECF78AEAA513D900EE873BF9949CAD847D8E (void);
// 0x000003E0 System.Void Crosstales.UI.Util.AudioFilterController::Update()
extern void AudioFilterController_Update_m72BAE1F4036EE7CA06B582ED18530AFF9A05C299 (void);
// 0x000003E1 System.Void Crosstales.UI.Util.AudioFilterController::FindAllAudioFilters()
extern void AudioFilterController_FindAllAudioFilters_m034DF26E117870682598FD340BAA92DC5E89BBAA (void);
// 0x000003E2 System.Void Crosstales.UI.Util.AudioFilterController::ResetAudioFilters()
extern void AudioFilterController_ResetAudioFilters_m40FB21296CCD12E6653E5F77C78690B4A994A781 (void);
// 0x000003E3 System.Void Crosstales.UI.Util.AudioFilterController::ReverbFilterDropdownChanged(System.Int32)
extern void AudioFilterController_ReverbFilterDropdownChanged_mAAC257AF30A94C6356960601CCFB334C5079267E (void);
// 0x000003E4 System.Void Crosstales.UI.Util.AudioFilterController::ChorusFilterEnabled(System.Boolean)
extern void AudioFilterController_ChorusFilterEnabled_mAA060E1FF642CD40B9F6CB484D32037FA5183BA1 (void);
// 0x000003E5 System.Void Crosstales.UI.Util.AudioFilterController::EchoFilterEnabled(System.Boolean)
extern void AudioFilterController_EchoFilterEnabled_m50CFF045EF70D8BFDE81F8870BB4814E4244974E (void);
// 0x000003E6 System.Void Crosstales.UI.Util.AudioFilterController::DistortionFilterEnabled(System.Boolean)
extern void AudioFilterController_DistortionFilterEnabled_mF9263E851AFEF9562F7CBF3D09ECE96622C08C12 (void);
// 0x000003E7 System.Void Crosstales.UI.Util.AudioFilterController::DistortionFilterChanged(System.Single)
extern void AudioFilterController_DistortionFilterChanged_m1687F0C6E768C574E49CF0FA67252982E33952FE (void);
// 0x000003E8 System.Void Crosstales.UI.Util.AudioFilterController::LowPassFilterEnabled(System.Boolean)
extern void AudioFilterController_LowPassFilterEnabled_m51DFC7705017F3ADE4F45F3DBFA1196373AC376E (void);
// 0x000003E9 System.Void Crosstales.UI.Util.AudioFilterController::LowPassFilterChanged(System.Single)
extern void AudioFilterController_LowPassFilterChanged_mBF89E179EF3BC9D410692081F15C0BB3D328D2F6 (void);
// 0x000003EA System.Void Crosstales.UI.Util.AudioFilterController::HighPassFilterEnabled(System.Boolean)
extern void AudioFilterController_HighPassFilterEnabled_m92EC04676167F39543722982FF01F16733CCCCCA (void);
// 0x000003EB System.Void Crosstales.UI.Util.AudioFilterController::HighPassFilterChanged(System.Single)
extern void AudioFilterController_HighPassFilterChanged_mC2A99980EF8375A12DE85CE405AC42B3E75001CD (void);
// 0x000003EC System.Void Crosstales.UI.Util.AudioFilterController::.ctor()
extern void AudioFilterController__ctor_m1088182076695D4E2EB7279B7D327D3B4E7162F8 (void);
// 0x000003ED System.Void Crosstales.UI.Util.AudioSourceController::Update()
extern void AudioSourceController_Update_m70490F7F5C8D24C507159A0673DA13A22C6A4E3B (void);
// 0x000003EE System.Void Crosstales.UI.Util.AudioSourceController::FindAllAudioSources()
extern void AudioSourceController_FindAllAudioSources_mFB4794AB3C518C5C5FECF97A18C9FA265200C2FA (void);
// 0x000003EF System.Void Crosstales.UI.Util.AudioSourceController::ResetAllAudioSources()
extern void AudioSourceController_ResetAllAudioSources_m993ADD16753B6D9A0D4069265D2C656F35333EC6 (void);
// 0x000003F0 System.Void Crosstales.UI.Util.AudioSourceController::MuteEnabled(System.Boolean)
extern void AudioSourceController_MuteEnabled_mDD74B20BD420EAC500C27B65D56643D108FB8040 (void);
// 0x000003F1 System.Void Crosstales.UI.Util.AudioSourceController::LoopEnabled(System.Boolean)
extern void AudioSourceController_LoopEnabled_m9F760D3EC7FF088B79B46713A26A4C25E0563A1E (void);
// 0x000003F2 System.Void Crosstales.UI.Util.AudioSourceController::VolumeChanged(System.Single)
extern void AudioSourceController_VolumeChanged_m6620B0D291A9288CFDAF7FE00CCDE144205C2C2B (void);
// 0x000003F3 System.Void Crosstales.UI.Util.AudioSourceController::PitchChanged(System.Single)
extern void AudioSourceController_PitchChanged_m5FB7E6D12DCF4E48D4B89C11A3753608C1A5F532 (void);
// 0x000003F4 System.Void Crosstales.UI.Util.AudioSourceController::StereoPanChanged(System.Single)
extern void AudioSourceController_StereoPanChanged_m57837284AAA56B5857B000C1771FAD9A5A862FE0 (void);
// 0x000003F5 System.Void Crosstales.UI.Util.AudioSourceController::.ctor()
extern void AudioSourceController__ctor_m4BD866B5A5C1DDF43D69854995780B526BFEEB6D (void);
// 0x000003F6 System.Void Crosstales.UI.Util.FPSDisplay::Update()
extern void FPSDisplay_Update_m3DA30CE583BE8D5C3CF67953C12F519B237E77B5 (void);
// 0x000003F7 System.Void Crosstales.UI.Util.FPSDisplay::.ctor()
extern void FPSDisplay__ctor_mE69C95D9F1718F2320861BEDD4AE137497BE4942 (void);
// 0x000003F8 System.Void Crosstales.UI.Util.ScrollRectHandler::Start()
extern void ScrollRectHandler_Start_m0D04331DA1FFEA8B60FC0ED473CAD5E51613B8CB (void);
// 0x000003F9 System.Void Crosstales.UI.Util.ScrollRectHandler::.ctor()
extern void ScrollRectHandler__ctor_mC6321DE1AAC54297ED22ED0319EB34682DE8DF12 (void);
// 0x000003FA System.Void Crosstales.Common.Util.SurviveSceneSwitch::Awake()
extern void SurviveSceneSwitch_Awake_mFD738893FC03E2CA8366A3204CBDAC07DECC1045 (void);
// 0x000003FB System.Void Crosstales.Common.Util.SurviveSceneSwitch::Start()
extern void SurviveSceneSwitch_Start_m44E86BFCF43176D436D60606B744027B87D7FD76 (void);
// 0x000003FC System.Void Crosstales.Common.Util.SurviveSceneSwitch::Update()
extern void SurviveSceneSwitch_Update_mEDD0951497831E3F6CCDA1CAAB4A1D8D40AC9DD5 (void);
// 0x000003FD System.Void Crosstales.Common.Util.SurviveSceneSwitch::.ctor()
extern void SurviveSceneSwitch__ctor_m9854DC25CA3D5BDEDC4044CE30938F23124ED793 (void);
// 0x000003FE System.Void Crosstales.Common.Util.TakeScreenshot::Start()
extern void TakeScreenshot_Start_mAA66A595B3DAF7CE42C4ABE8F8D2C5E5697EFDD1 (void);
// 0x000003FF System.Void Crosstales.Common.Util.TakeScreenshot::Update()
extern void TakeScreenshot_Update_m38664624F52BD6B321A126F6FC5846D6313F4948 (void);
// 0x00000400 System.Void Crosstales.Common.Util.TakeScreenshot::Capture()
extern void TakeScreenshot_Capture_m709EDE234BAE9A8D77B3548B28E11CC5C3898281 (void);
// 0x00000401 System.Void Crosstales.Common.Util.TakeScreenshot::.ctor()
extern void TakeScreenshot__ctor_mB92BF7FD062841C52807F9187CD0AD9DCEC8D8C7 (void);
// 0x00000402 System.Void Crosstales.Common.Util.BackgroundController::Start()
extern void BackgroundController_Start_m314E46A6144FE74BE06AEE5E05D83CE640E17D08 (void);
// 0x00000403 System.Void Crosstales.Common.Util.BackgroundController::FixedUpdate()
extern void BackgroundController_FixedUpdate_mD05CBA5EBA4998873FA97D27323F0D756DCFB9FC (void);
// 0x00000404 System.Void Crosstales.Common.Util.BackgroundController::.ctor()
extern void BackgroundController__ctor_mACA32916E8B5713D92710C0C6DA69E89DE6293D9 (void);
// 0x00000405 System.String Crosstales.Common.Util.BaseConstants::get_PREFIX_FILE()
extern void BaseConstants_get_PREFIX_FILE_mF05531A4738FEE2B43B56F613C8543B56677A58C (void);
// 0x00000406 System.String Crosstales.Common.Util.BaseConstants::get_APPLICATION_PATH()
extern void BaseConstants_get_APPLICATION_PATH_mE87E6E9FF38895E8F8E1D78DB610DD417DDC85BB (void);
// 0x00000407 System.Void Crosstales.Common.Util.BaseConstants::.ctor()
extern void BaseConstants__ctor_m423B5C856F2C469942209CF30A993C3BD4A969E3 (void);
// 0x00000408 System.Void Crosstales.Common.Util.BaseConstants::.cctor()
extern void BaseConstants__cctor_m08244E973BC852F1CD2C3F222AA017980377C085 (void);
// 0x00000409 System.Boolean Crosstales.Common.Util.BaseHelper::get_isInternetAvailable()
extern void BaseHelper_get_isInternetAvailable_mF070BABEACA6E81C6C55C8FC65B28351FB75A1F7 (void);
// 0x0000040A System.Boolean Crosstales.Common.Util.BaseHelper::get_isWindowsPlatform()
extern void BaseHelper_get_isWindowsPlatform_mA89734268B9F4512EED642DF4FD30718BF0698DB (void);
// 0x0000040B System.Boolean Crosstales.Common.Util.BaseHelper::get_isMacOSPlatform()
extern void BaseHelper_get_isMacOSPlatform_mC7A38F4150094D9E1C539292EF669C43FA2567B0 (void);
// 0x0000040C System.Boolean Crosstales.Common.Util.BaseHelper::get_isLinuxPlatform()
extern void BaseHelper_get_isLinuxPlatform_m417DC75B6784EF54E5B5BE01CD05705C3BD1D7DE (void);
// 0x0000040D System.Boolean Crosstales.Common.Util.BaseHelper::get_isStandalonePlatform()
extern void BaseHelper_get_isStandalonePlatform_mF704F98AD14E6122F66B2A8AE7AC406129FEA067 (void);
// 0x0000040E System.Boolean Crosstales.Common.Util.BaseHelper::get_isAndroidPlatform()
extern void BaseHelper_get_isAndroidPlatform_mCE5CA1352C1F80F40F41A29BF1C27292E840394F (void);
// 0x0000040F System.Boolean Crosstales.Common.Util.BaseHelper::get_isIOSPlatform()
extern void BaseHelper_get_isIOSPlatform_mC37175C891506DEB337DA607BC64D69709286A75 (void);
// 0x00000410 System.Boolean Crosstales.Common.Util.BaseHelper::get_isTvOSPlatform()
extern void BaseHelper_get_isTvOSPlatform_m9875AE921ECCE6CC71E9839BFAE77AE234876AFD (void);
// 0x00000411 System.Boolean Crosstales.Common.Util.BaseHelper::get_isWSAPlatform()
extern void BaseHelper_get_isWSAPlatform_m6C69C0C0891C3113A253E643E0821F65BD87B5EA (void);
// 0x00000412 System.Boolean Crosstales.Common.Util.BaseHelper::get_isXboxOnePlatform()
extern void BaseHelper_get_isXboxOnePlatform_mC6F3A25D6E8CFC89ECBA413420C9B8AC9BA4BAD9 (void);
// 0x00000413 System.Boolean Crosstales.Common.Util.BaseHelper::get_isPS4Platform()
extern void BaseHelper_get_isPS4Platform_m0E05F11A7177556AB547379BF883888973A5BD6E (void);
// 0x00000414 System.Boolean Crosstales.Common.Util.BaseHelper::get_isWebGLPlatform()
extern void BaseHelper_get_isWebGLPlatform_m33211FB09A47645AA051EC76948852BE0025D0EE (void);
// 0x00000415 System.Boolean Crosstales.Common.Util.BaseHelper::get_isWebPlatform()
extern void BaseHelper_get_isWebPlatform_m5A3E8B17AD90A821B6DC419FBECC6CB800ECBFB3 (void);
// 0x00000416 System.Boolean Crosstales.Common.Util.BaseHelper::get_isWindowsBasedPlatform()
extern void BaseHelper_get_isWindowsBasedPlatform_mF5EEC2933706EDA1EDFEB2F1FD8E3FE8A41431A5 (void);
// 0x00000417 System.Boolean Crosstales.Common.Util.BaseHelper::get_isWSABasedPlatform()
extern void BaseHelper_get_isWSABasedPlatform_mA559B2E3E0D1646C0C427B113909B963B3D40A74 (void);
// 0x00000418 System.Boolean Crosstales.Common.Util.BaseHelper::get_isAppleBasedPlatform()
extern void BaseHelper_get_isAppleBasedPlatform_m831F2787A6CD87908977B20FB1F2233D84D17E62 (void);
// 0x00000419 System.Boolean Crosstales.Common.Util.BaseHelper::get_isIOSBasedPlatform()
extern void BaseHelper_get_isIOSBasedPlatform_m5DAABF1EC5DC3B8947CC4B636EB59C8AD81C4A14 (void);
// 0x0000041A System.Boolean Crosstales.Common.Util.BaseHelper::get_isEditor()
extern void BaseHelper_get_isEditor_m0454F7BB9528D9AEEEA24C95BC322A44839DF874 (void);
// 0x0000041B System.Boolean Crosstales.Common.Util.BaseHelper::get_isWindowsEditor()
extern void BaseHelper_get_isWindowsEditor_m1ADAFD1FB58136C7BE74018F625BF8C9ED655DFE (void);
// 0x0000041C System.Boolean Crosstales.Common.Util.BaseHelper::get_isMacOSEditor()
extern void BaseHelper_get_isMacOSEditor_m96E71133D83F40307304E28B3F103B14ECEAB0C4 (void);
// 0x0000041D System.Boolean Crosstales.Common.Util.BaseHelper::get_isLinuxEditor()
extern void BaseHelper_get_isLinuxEditor_m0FEEFCA649971D8A75CEF0B563B6965866EF5240 (void);
// 0x0000041E System.Boolean Crosstales.Common.Util.BaseHelper::get_isEditorMode()
extern void BaseHelper_get_isEditorMode_m72F760C50ED845E97E3A98BAEAC42398B3BD2C11 (void);
// 0x0000041F System.Boolean Crosstales.Common.Util.BaseHelper::get_isIL2CPP()
extern void BaseHelper_get_isIL2CPP_m266EBBA79AEE873C4A8D4D820D19CC7BFB4376A8 (void);
// 0x00000420 Crosstales.Common.Model.Enum.Platform Crosstales.Common.Util.BaseHelper::get_CurrentPlatform()
extern void BaseHelper_get_CurrentPlatform_m69DEAD89DD0EB052044CCE8FA2E94578A17609AF (void);
// 0x00000421 System.String Crosstales.Common.Util.BaseHelper::CreateString(System.String,System.Int32)
extern void BaseHelper_CreateString_m8187B680400CD1F2A77511DEFBC91152D1F6D9EB (void);
// 0x00000422 System.Boolean Crosstales.Common.Util.BaseHelper::hasActiveClip(UnityEngine.AudioSource)
extern void BaseHelper_hasActiveClip_m4C947A64FD354BCBAEA2078E0E1A919578D9CCBA (void);
// 0x00000423 System.Boolean Crosstales.Common.Util.BaseHelper::RemoteCertificateValidationCallback(System.Object,System.Security.Cryptography.X509Certificates.X509Certificate,System.Security.Cryptography.X509Certificates.X509Chain,System.Net.Security.SslPolicyErrors)
extern void BaseHelper_RemoteCertificateValidationCallback_m8F0689C2DA5F597E2B631A7A0F66E67F1CE48613 (void);
// 0x00000424 System.String Crosstales.Common.Util.BaseHelper::ValidatePath(System.String,System.Boolean)
extern void BaseHelper_ValidatePath_m7CCEC41977D556D08B0267C3D40E999803F3A225 (void);
// 0x00000425 System.String Crosstales.Common.Util.BaseHelper::ValidateFile(System.String)
extern void BaseHelper_ValidateFile_mEAF8DC5346F6B807D1C8C7C6ED24F88DB0F31242 (void);
// 0x00000426 System.String[] Crosstales.Common.Util.BaseHelper::GetFiles(System.String,System.Boolean,System.String[])
extern void BaseHelper_GetFiles_m1C040E9DE5359EF69F773CFCC1DE845322EB7657 (void);
// 0x00000427 System.String[] Crosstales.Common.Util.BaseHelper::GetDirectories(System.String,System.Boolean)
extern void BaseHelper_GetDirectories_mCBFE3ADE894BF3CE041C0650024856AEF01A1A04 (void);
// 0x00000428 System.String Crosstales.Common.Util.BaseHelper::ValidURLFromFilePath(System.String)
extern void BaseHelper_ValidURLFromFilePath_m9235C556A23015F250EA2818797280491489EFC8 (void);
// 0x00000429 System.String Crosstales.Common.Util.BaseHelper::CleanUrl(System.String,System.Boolean,System.Boolean,System.Boolean)
extern void BaseHelper_CleanUrl_mFB396194548994FD98A15424C1244FB487A8F699 (void);
// 0x0000042A System.String Crosstales.Common.Util.BaseHelper::ClearTags(System.String)
extern void BaseHelper_ClearTags_mD93B30D4885D2E5DCCA1672188CD20EB76B01C4F (void);
// 0x0000042B System.String Crosstales.Common.Util.BaseHelper::ClearSpaces(System.String)
extern void BaseHelper_ClearSpaces_m235DA279F449F5AD4E61A2F82D5C414DB86322CD (void);
// 0x0000042C System.String Crosstales.Common.Util.BaseHelper::ClearLineEndings(System.String)
extern void BaseHelper_ClearLineEndings_m8A5A3B1AFCE29A057A7D0EF2DE7576734251456D (void);
// 0x0000042D System.Collections.Generic.List`1<System.String> Crosstales.Common.Util.BaseHelper::SplitStringToLines(System.String,System.Boolean,System.Int32,System.Int32)
extern void BaseHelper_SplitStringToLines_mE1E3904BDE694387F27CDAD680DEF2A490ADC9FF (void);
// 0x0000042E System.String Crosstales.Common.Util.BaseHelper::FormatBytesToHRF(System.Int64)
extern void BaseHelper_FormatBytesToHRF_m80A776532503F8E7E052C62C139F3ABB8E475692 (void);
// 0x0000042F System.String Crosstales.Common.Util.BaseHelper::FormatSecondsToHourMinSec(System.Double)
extern void BaseHelper_FormatSecondsToHourMinSec_m3822B32615C718A3D267851AD914A984189F59F8 (void);
// 0x00000430 UnityEngine.Color Crosstales.Common.Util.BaseHelper::HSVToRGB(System.Single,System.Single,System.Single,System.Single)
extern void BaseHelper_HSVToRGB_m20DA8918AE1E0B80A7E6DA9AE4F20A63CB4840F4 (void);
// 0x00000431 System.Boolean Crosstales.Common.Util.BaseHelper::isValidURL(System.String)
extern void BaseHelper_isValidURL_m6EDE78C570F5411F5A3DE99052A83079DC26B59B (void);
// 0x00000432 System.Void Crosstales.Common.Util.BaseHelper::FileCopy(System.String,System.String,System.Boolean)
extern void BaseHelper_FileCopy_m8AB3ED391A4D1FFB9298A21A8ABBCB532C2FDD09 (void);
// 0x00000433 System.Void Crosstales.Common.Util.BaseHelper::.ctor()
extern void BaseHelper__ctor_mEF441B4F46F49B04BF884A05686974DED620C20C (void);
// 0x00000434 System.Void Crosstales.Common.Util.BaseHelper::.cctor()
extern void BaseHelper__cctor_m8F4F00BB889949CF9761BA21335D45D71CEB629D (void);
// 0x00000435 System.Void Crosstales.Common.Util.BaseHelper/<>c::.cctor()
extern void U3CU3Ec__cctor_m9CAEDD2D496807D1F6D85855AFA7AE14980E6C6B (void);
// 0x00000436 System.Void Crosstales.Common.Util.BaseHelper/<>c::.ctor()
extern void U3CU3Ec__ctor_m4871038D3BC7C094E3FA3BA86564AAB953E35B3F (void);
// 0x00000437 System.String Crosstales.Common.Util.BaseHelper/<>c::<GetFiles>b__59_0(System.String)
extern void U3CU3Ec_U3CGetFilesU3Eb__59_0_mE8CAFED60DE2976F8C3F60C65EF8FA97AA7279CA (void);
// 0x00000438 System.Void Crosstales.Common.Util.CTPlayerPrefs::.cctor()
extern void CTPlayerPrefs__cctor_m02F3035D4A6B31999D6F667215A2690748135CCA (void);
// 0x00000439 System.Boolean Crosstales.Common.Util.CTPlayerPrefs::HasKey(System.String)
extern void CTPlayerPrefs_HasKey_m343530DB4F0540483EBAA38852F22F4BDC494AB8 (void);
// 0x0000043A System.Void Crosstales.Common.Util.CTPlayerPrefs::DeleteAll()
extern void CTPlayerPrefs_DeleteAll_m691A7F70901A1FDE9195F9B8D6CB9D79F67EFDD6 (void);
// 0x0000043B System.Void Crosstales.Common.Util.CTPlayerPrefs::DeleteKey(System.String)
extern void CTPlayerPrefs_DeleteKey_mE952677FC2CF7F1F6A21B435FFC7B700247487DF (void);
// 0x0000043C System.Void Crosstales.Common.Util.CTPlayerPrefs::Save()
extern void CTPlayerPrefs_Save_m4B4CD209257285EB483F161A464D599D614B69D4 (void);
// 0x0000043D System.String Crosstales.Common.Util.CTPlayerPrefs::GetString(System.String)
extern void CTPlayerPrefs_GetString_m4A24B355B496A4C92544CEE431188605D675A7CA (void);
// 0x0000043E System.Single Crosstales.Common.Util.CTPlayerPrefs::GetFloat(System.String)
extern void CTPlayerPrefs_GetFloat_mC82EA1654D6558D8854AC0B2FB6C1EFD273E8BAA (void);
// 0x0000043F System.Int32 Crosstales.Common.Util.CTPlayerPrefs::GetInt(System.String)
extern void CTPlayerPrefs_GetInt_m1AF5A20A28BAE9A740A05FCE2261C8BC4EE85C1A (void);
// 0x00000440 System.Boolean Crosstales.Common.Util.CTPlayerPrefs::GetBool(System.String)
extern void CTPlayerPrefs_GetBool_mCBBC02069CA83ABC141BA0EBA91D5D2EDE3B6CD9 (void);
// 0x00000441 System.DateTime Crosstales.Common.Util.CTPlayerPrefs::GetDate(System.String)
extern void CTPlayerPrefs_GetDate_mD193EF979FE51EC4CA7E507BD3F7DC7C0746A5C3 (void);
// 0x00000442 System.Void Crosstales.Common.Util.CTPlayerPrefs::SetString(System.String,System.String)
extern void CTPlayerPrefs_SetString_mB5D86CF95542A08310B0FE892F040B246770C234 (void);
// 0x00000443 System.Void Crosstales.Common.Util.CTPlayerPrefs::SetFloat(System.String,System.Single)
extern void CTPlayerPrefs_SetFloat_m257A15CE9D9E94A0A32E829BAAEC438502558CA8 (void);
// 0x00000444 System.Void Crosstales.Common.Util.CTPlayerPrefs::SetInt(System.String,System.Int32)
extern void CTPlayerPrefs_SetInt_mE40F4D337B12BD39EE80C14CE386A3D703E245EE (void);
// 0x00000445 System.Void Crosstales.Common.Util.CTPlayerPrefs::SetBool(System.String,System.Boolean)
extern void CTPlayerPrefs_SetBool_m4D6D3AB173DD3E9996FB57DCB2E47B88D1F00690 (void);
// 0x00000446 System.Void Crosstales.Common.Util.CTPlayerPrefs::SetDate(System.String,System.DateTime)
extern void CTPlayerPrefs_SetDate_mB2C8336DC4873AB8D8119D727B0CDDFA87990594 (void);
// 0x00000447 System.Void Crosstales.Common.Util.CTProcess::add_Exited(System.EventHandler)
extern void CTProcess_add_Exited_m2522C9CC2EBACBAC756B5AEF8D857746B1B5A227 (void);
// 0x00000448 System.Void Crosstales.Common.Util.CTProcess::remove_Exited(System.EventHandler)
extern void CTProcess_remove_Exited_mC1354807453D23C4A3C8EDD365BBE1B688BE89B8 (void);
// 0x00000449 System.Void Crosstales.Common.Util.CTProcess::add_OutputDataReceived(System.Diagnostics.DataReceivedEventHandler)
extern void CTProcess_add_OutputDataReceived_m625D0759C272BCB29A6A7ADCF4214F3BA2D332D3 (void);
// 0x0000044A System.Void Crosstales.Common.Util.CTProcess::remove_OutputDataReceived(System.Diagnostics.DataReceivedEventHandler)
extern void CTProcess_remove_OutputDataReceived_m4BB9465533EAD71763935B84E2765D38B559C7D5 (void);
// 0x0000044B System.Void Crosstales.Common.Util.CTProcess::add_ErrorDataReceived(System.Diagnostics.DataReceivedEventHandler)
extern void CTProcess_add_ErrorDataReceived_mEBC45F29474ED0E1E3CA3E226271FA0ABEFE91B1 (void);
// 0x0000044C System.Void Crosstales.Common.Util.CTProcess::remove_ErrorDataReceived(System.Diagnostics.DataReceivedEventHandler)
extern void CTProcess_remove_ErrorDataReceived_mF924C5DA48FCE375169CC53B954C3BF4ABD493E3 (void);
// 0x0000044D System.IntPtr Crosstales.Common.Util.CTProcess::get_Handle()
extern void CTProcess_get_Handle_mC3608F2A6A30685484B9144B20D12BD0399D4E32 (void);
// 0x0000044E System.Void Crosstales.Common.Util.CTProcess::set_Handle(System.IntPtr)
extern void CTProcess_set_Handle_m5C53B2AE2BC840DE6F1435598135115AB14DAA14 (void);
// 0x0000044F System.Int32 Crosstales.Common.Util.CTProcess::get_Id()
extern void CTProcess_get_Id_mD17750B0020A2B49BBC1B864295B2F34930A7DCB (void);
// 0x00000450 System.Void Crosstales.Common.Util.CTProcess::set_Id(System.Int32)
extern void CTProcess_set_Id_mD77F7079C3005B95ED3BC30F796BAEC98BF5E26B (void);
// 0x00000451 Crosstales.Common.Util.CTProcessStartInfo Crosstales.Common.Util.CTProcess::get_StartInfo()
extern void CTProcess_get_StartInfo_mAB73C444D69AF78E2E2111B2680435469C30BDD4 (void);
// 0x00000452 System.Void Crosstales.Common.Util.CTProcess::set_StartInfo(Crosstales.Common.Util.CTProcessStartInfo)
extern void CTProcess_set_StartInfo_m135807252222DD15C7321BB09EF834CCFA3E0881 (void);
// 0x00000453 System.Boolean Crosstales.Common.Util.CTProcess::get_HasExited()
extern void CTProcess_get_HasExited_m2D586146FF76514478807E12CEAA555D9C1C0D51 (void);
// 0x00000454 System.Void Crosstales.Common.Util.CTProcess::set_HasExited(System.Boolean)
extern void CTProcess_set_HasExited_mD528F17ECC59BED4513599C205B57A875532DCCB (void);
// 0x00000455 System.UInt32 Crosstales.Common.Util.CTProcess::get_ExitCode()
extern void CTProcess_get_ExitCode_mE192F777FF0E784A80117D0CB5A541CBDFF26804 (void);
// 0x00000456 System.DateTime Crosstales.Common.Util.CTProcess::get_StartTime()
extern void CTProcess_get_StartTime_mAF23EA137BD076A31606E0175BCBED9241F2C40F (void);
// 0x00000457 System.Void Crosstales.Common.Util.CTProcess::set_StartTime(System.DateTime)
extern void CTProcess_set_StartTime_mF73C4A51A77CE85B55ECF03A6AC3F5FE43654188 (void);
// 0x00000458 System.DateTime Crosstales.Common.Util.CTProcess::get_ExitTime()
extern void CTProcess_get_ExitTime_m06BAEDCC06277C766A407B9E331538C7975F18CD (void);
// 0x00000459 System.Void Crosstales.Common.Util.CTProcess::set_ExitTime(System.DateTime)
extern void CTProcess_set_ExitTime_m762BB485BBE10AC9366FFBD0A3460F5242F5571C (void);
// 0x0000045A System.IO.StreamReader Crosstales.Common.Util.CTProcess::get_StandardOutput()
extern void CTProcess_get_StandardOutput_m5E2E8567D9990F291CCE025867CBCF3B03375377 (void);
// 0x0000045B System.Void Crosstales.Common.Util.CTProcess::set_StandardOutput(System.IO.StreamReader)
extern void CTProcess_set_StandardOutput_mBB889EA2A78ADF52AE97A319985AEBF65EFB4001 (void);
// 0x0000045C System.IO.StreamReader Crosstales.Common.Util.CTProcess::get_StandardError()
extern void CTProcess_get_StandardError_m1FA605D25447DFAB49640AF6C892C09436776B9F (void);
// 0x0000045D System.Void Crosstales.Common.Util.CTProcess::set_StandardError(System.IO.StreamReader)
extern void CTProcess_set_StandardError_mD6993571CA67FFEC04C2D036EB9240115EAB8802 (void);
// 0x0000045E System.Boolean Crosstales.Common.Util.CTProcess::get_isBusy()
extern void CTProcess_get_isBusy_m4F3A086625E52181F3EA327E3598675F0C3A47F5 (void);
// 0x0000045F System.Void Crosstales.Common.Util.CTProcess::set_isBusy(System.Boolean)
extern void CTProcess_set_isBusy_mAC83ACD3B1A2AF71151EFF8F7C1E57DA8D529557 (void);
// 0x00000460 System.Void Crosstales.Common.Util.CTProcess::Start()
extern void CTProcess_Start_m4986165458B86387398FF3644EB0A0643104C301 (void);
// 0x00000461 System.Void Crosstales.Common.Util.CTProcess::Start(Crosstales.Common.Util.CTProcessStartInfo)
extern void CTProcess_Start_m3842565EDD270C8E632F3194CFCC00DFE2AB73DB (void);
// 0x00000462 System.Void Crosstales.Common.Util.CTProcess::cleanup()
extern void CTProcess_cleanup_m8C1CC32693352813FE9C6AA08F01F0A3CF195451 (void);
// 0x00000463 System.Void Crosstales.Common.Util.CTProcess::Kill()
extern void CTProcess_Kill_mAA92AF83D1CEC3BDF79B85B8F0BB5AE934A880F7 (void);
// 0x00000464 System.Void Crosstales.Common.Util.CTProcess::WaitForExit(System.Int32)
extern void CTProcess_WaitForExit_mF88C9E305793BB013BE8F743CF17430AEC35E3B5 (void);
// 0x00000465 System.Void Crosstales.Common.Util.CTProcess::BeginOutputReadLine()
extern void CTProcess_BeginOutputReadLine_m90ABA2BAA358F225728ED0541DCFB3C4A9F0C61E (void);
// 0x00000466 System.Void Crosstales.Common.Util.CTProcess::BeginErrorReadLine()
extern void CTProcess_BeginErrorReadLine_mCEC3034D82DBC636D9F9C034FAF64A051EDE618B (void);
// 0x00000467 System.Void Crosstales.Common.Util.CTProcess::Dispose()
extern void CTProcess_Dispose_mCB5F569B860DFD16AE79679A374CF9AD6E092527 (void);
// 0x00000468 System.Void Crosstales.Common.Util.CTProcess::watchStdOut()
extern void CTProcess_watchStdOut_m12CBF4165FE4408453AF4425FD4DE1D97818C55A (void);
// 0x00000469 System.Void Crosstales.Common.Util.CTProcess::watchStdErr()
extern void CTProcess_watchStdErr_m27D5B56D6C1B8FECDEB906CD7435E37889FDB6C7 (void);
// 0x0000046A System.Diagnostics.DataReceivedEventArgs Crosstales.Common.Util.CTProcess::createMockDataReceivedEventArgs(System.String)
extern void CTProcess_createMockDataReceivedEventArgs_mB8587D380D6A6E68D561D6F08A015D9A81122F3E (void);
// 0x0000046B System.Void Crosstales.Common.Util.CTProcess::onExited()
extern void CTProcess_onExited_m9A22B8DAE6ABF12300A6CD2ECF97FC194681E325 (void);
// 0x0000046C System.Void Crosstales.Common.Util.CTProcess::.ctor()
extern void CTProcess__ctor_m906058D39A9F785FDF8EF726A3CFC05AC1DCFDEE (void);
// 0x0000046D System.Void Crosstales.Common.Util.CTProcess::.cctor()
extern void CTProcess__cctor_m96CC56A262D1B4AF43DEDB27B4B9616EE0A921B2 (void);
// 0x0000046E System.Void Crosstales.Common.Util.CTProcess::<BeginOutputReadLine>b__60_0()
extern void CTProcess_U3CBeginOutputReadLineU3Eb__60_0_mF7FDEECEFD6B1DE5646F5204C7B69802154B4AF3 (void);
// 0x0000046F System.Void Crosstales.Common.Util.CTProcess::<BeginErrorReadLine>b__61_0()
extern void CTProcess_U3CBeginErrorReadLineU3Eb__61_0_m19AABDD682A9810182567AA68BFDF32AB957445C (void);
// 0x00000470 System.Void Crosstales.Common.Util.CTProcessStartInfo::.ctor()
extern void CTProcessStartInfo__ctor_mC456AD13C5B40D29591D4324AE88C400CAE483BD (void);
// 0x00000471 System.Boolean Crosstales.Common.Util.CTProcessStartInfo::get_UseThread()
extern void CTProcessStartInfo_get_UseThread_m34262C2CD9CB925BCFDB438D610DDE432F1AC6B1 (void);
// 0x00000472 System.Void Crosstales.Common.Util.CTProcessStartInfo::set_UseThread(System.Boolean)
extern void CTProcessStartInfo_set_UseThread_m86AA2DA43064F088C2F979EA3B9369C4C030A32B (void);
// 0x00000473 System.Boolean Crosstales.Common.Util.CTProcessStartInfo::get_UseCmdExecute()
extern void CTProcessStartInfo_get_UseCmdExecute_mB67F1E3D55EBB016C86D75C4F09356D04D02333E (void);
// 0x00000474 System.Void Crosstales.Common.Util.CTProcessStartInfo::set_UseCmdExecute(System.Boolean)
extern void CTProcessStartInfo_set_UseCmdExecute_mB553AB506E410A0294A873F6592EF753B26FBB85 (void);
// 0x00000475 System.String Crosstales.Common.Util.CTProcessStartInfo::get_FileName()
extern void CTProcessStartInfo_get_FileName_m1763CF197CE0D3528D744A53B2DAE98F890279F9 (void);
// 0x00000476 System.Void Crosstales.Common.Util.CTProcessStartInfo::set_FileName(System.String)
extern void CTProcessStartInfo_set_FileName_mFF77C984D27BD97D9D6724F4154E65439EADAC0F (void);
// 0x00000477 System.String Crosstales.Common.Util.CTProcessStartInfo::get_Arguments()
extern void CTProcessStartInfo_get_Arguments_m3A347FA469BB43787D2877BB70514D039FC9D7C8 (void);
// 0x00000478 System.Void Crosstales.Common.Util.CTProcessStartInfo::set_Arguments(System.String)
extern void CTProcessStartInfo_set_Arguments_mA9CB0587438FF858CBF67F0E8A51A987962A5FE8 (void);
// 0x00000479 System.Boolean Crosstales.Common.Util.CTProcessStartInfo::get_CreateNoWindow()
extern void CTProcessStartInfo_get_CreateNoWindow_mF07A129AB1002CC6669265A2B72A2819386D21E4 (void);
// 0x0000047A System.Void Crosstales.Common.Util.CTProcessStartInfo::set_CreateNoWindow(System.Boolean)
extern void CTProcessStartInfo_set_CreateNoWindow_m27DD6357C383BC237A64790632439866995E5EA9 (void);
// 0x0000047B System.String Crosstales.Common.Util.CTProcessStartInfo::get_WorkingDirectory()
extern void CTProcessStartInfo_get_WorkingDirectory_mD97C799110A52EE36167FB77EA6F695DE0C03D26 (void);
// 0x0000047C System.Void Crosstales.Common.Util.CTProcessStartInfo::set_WorkingDirectory(System.String)
extern void CTProcessStartInfo_set_WorkingDirectory_mBA1BEE7819F6D96B8E8A61BEC985D1627A1E77D9 (void);
// 0x0000047D System.Boolean Crosstales.Common.Util.CTProcessStartInfo::get_RedirectStandardOutput()
extern void CTProcessStartInfo_get_RedirectStandardOutput_m2421774316C53C2DBFD8749A7F52D5A2A5C658A9 (void);
// 0x0000047E System.Void Crosstales.Common.Util.CTProcessStartInfo::set_RedirectStandardOutput(System.Boolean)
extern void CTProcessStartInfo_set_RedirectStandardOutput_mEB9777CA86502455F7C8AC29644AB604CF18CC84 (void);
// 0x0000047F System.Boolean Crosstales.Common.Util.CTProcessStartInfo::get_RedirectStandardError()
extern void CTProcessStartInfo_get_RedirectStandardError_mFA019C90FA0BFCC650C8A92E1EFD6BD1EDB83E96 (void);
// 0x00000480 System.Void Crosstales.Common.Util.CTProcessStartInfo::set_RedirectStandardError(System.Boolean)
extern void CTProcessStartInfo_set_RedirectStandardError_m322C697C5104C0A4F0BD1E5B721B800D7B896B17 (void);
// 0x00000481 System.Text.Encoding Crosstales.Common.Util.CTProcessStartInfo::get_StandardOutputEncoding()
extern void CTProcessStartInfo_get_StandardOutputEncoding_mF883E4B6A58BF6F6189EC53A1B36CED7051CA740 (void);
// 0x00000482 System.Void Crosstales.Common.Util.CTProcessStartInfo::set_StandardOutputEncoding(System.Text.Encoding)
extern void CTProcessStartInfo_set_StandardOutputEncoding_mE7BA0B4AD89C1B8476037466A7149165AA304DBD (void);
// 0x00000483 System.Text.Encoding Crosstales.Common.Util.CTProcessStartInfo::get_StandardErrorEncoding()
extern void CTProcessStartInfo_get_StandardErrorEncoding_m8A4F0A1E361D9A522ED82A49C6F7BE05C09801ED (void);
// 0x00000484 System.Void Crosstales.Common.Util.CTProcessStartInfo::set_StandardErrorEncoding(System.Text.Encoding)
extern void CTProcessStartInfo_set_StandardErrorEncoding_m3C59321FB26FE02DFBC8307F22D060FE9ADC2628 (void);
// 0x00000485 System.Boolean Crosstales.Common.Util.CTProcessStartInfo::get_UseShellExecute()
extern void CTProcessStartInfo_get_UseShellExecute_m96AC26978B39FBA130D519C81194A5B6B101792B (void);
// 0x00000486 System.Void Crosstales.Common.Util.CTProcessStartInfo::set_UseShellExecute(System.Boolean)
extern void CTProcessStartInfo_set_UseShellExecute_m2C838389B42C35BBB7CEA1BC337285F1771849E6 (void);
// 0x00000487 System.Int32 Crosstales.Common.Util.CTWebClient::get_Timeout()
extern void CTWebClient_get_Timeout_m05E7AC7AD82CFD0A255A1D753B3AB413E0C15C9E (void);
// 0x00000488 System.Void Crosstales.Common.Util.CTWebClient::set_Timeout(System.Int32)
extern void CTWebClient_set_Timeout_mC93FB85FA7A3F30D35CE10A64A25ABD18D206079 (void);
// 0x00000489 System.Int32 Crosstales.Common.Util.CTWebClient::get_ConnectionLimit()
extern void CTWebClient_get_ConnectionLimit_m74A0F4DC526B36E55EA0857E3F953453622189CE (void);
// 0x0000048A System.Void Crosstales.Common.Util.CTWebClient::set_ConnectionLimit(System.Int32)
extern void CTWebClient_set_ConnectionLimit_mFBDBF8F1C18BC022FC5B773E3874E8B9569F9068 (void);
// 0x0000048B System.Void Crosstales.Common.Util.CTWebClient::.ctor()
extern void CTWebClient__ctor_m2C17945D74024B83C76B5BB86DBF0DF78E40B537 (void);
// 0x0000048C System.Void Crosstales.Common.Util.CTWebClient::.ctor(System.Int32,System.Int32)
extern void CTWebClient__ctor_m60AC97676C10006B910233B2C508C5CE4E37B67E (void);
// 0x0000048D System.Net.WebRequest Crosstales.Common.Util.CTWebClient::CTGetWebRequest(System.String)
extern void CTWebClient_CTGetWebRequest_m73349F6C28991D04220A3D78ADA73D30D5400260 (void);
// 0x0000048E System.Net.WebRequest Crosstales.Common.Util.CTWebClient::GetWebRequest(System.Uri)
extern void CTWebClient_GetWebRequest_m6AB86C07D3BA594406B31E2C21ACAF1F4AFF8650 (void);
// 0x0000048F System.Void Crosstales.Common.Util.FFTAnalyzer::Update()
extern void FFTAnalyzer_Update_m273EE0E828B6CBC5E1ADA8D77BB751CC54389D98 (void);
// 0x00000490 System.Void Crosstales.Common.Util.FFTAnalyzer::.ctor()
extern void FFTAnalyzer__ctor_m79834DBD77EE0536F5996182B5A318AA1BFCA1D8 (void);
// 0x00000491 System.Void Crosstales.Common.Util.PlatformController::Start()
extern void PlatformController_Start_m45C32ED17D6746F9DC0C139B05EDC89CEFBF0C3C (void);
// 0x00000492 System.Void Crosstales.Common.Util.PlatformController::selectPlatform()
extern void PlatformController_selectPlatform_m44CD923C6E139A6DB86AC3D1BCAE13099EDBCC3B (void);
// 0x00000493 System.Void Crosstales.Common.Util.PlatformController::activateGO()
extern void PlatformController_activateGO_m8F5A1B0D38F30CE1D2528D23CCD21FD150046B67 (void);
// 0x00000494 System.Void Crosstales.Common.Util.PlatformController::.ctor()
extern void PlatformController__ctor_m246265302FC8CBCE52B8C4DB078CB7B5593B4FAE (void);
// 0x00000495 System.Void Crosstales.Common.Util.RandomColor::Start()
extern void RandomColor_Start_m391CF84B08071FF3ACB0963EB28D2730BAD3F21C (void);
// 0x00000496 System.Void Crosstales.Common.Util.RandomColor::Update()
extern void RandomColor_Update_m80AF1317FD5A6E02EE266F7916C32167C811A267 (void);
// 0x00000497 System.Void Crosstales.Common.Util.RandomColor::.ctor()
extern void RandomColor__ctor_m9D12F994D18E58B4E6917402BF320A05B4C07D38 (void);
// 0x00000498 System.Void Crosstales.Common.Util.RandomRotator::Start()
extern void RandomRotator_Start_mD8CAF4274B1003904752C232D60FF6D740AE46D8 (void);
// 0x00000499 System.Void Crosstales.Common.Util.RandomRotator::Update()
extern void RandomRotator_Update_m2B257E670DEC91784B8CF373497F18B3B88FBEA5 (void);
// 0x0000049A System.Void Crosstales.Common.Util.RandomRotator::.ctor()
extern void RandomRotator__ctor_m5B8F4189F25BED4B6E89D524DA329BF1F641416A (void);
// 0x0000049B System.Void Crosstales.Common.Util.RandomScaler::Start()
extern void RandomScaler_Start_m846A6856675D0177F6879AF36DB91392267F17A0 (void);
// 0x0000049C System.Void Crosstales.Common.Util.RandomScaler::Update()
extern void RandomScaler_Update_m7DF95D0941A662AA475C77084EBA89A60C0908B0 (void);
// 0x0000049D System.Void Crosstales.Common.Util.RandomScaler::.ctor()
extern void RandomScaler__ctor_mF4467ADA2B0DB1F1B451D3568A5CAF22042121D7 (void);
// 0x0000049E System.Void Crosstales.Common.Util.SerializableDictionary`2::.ctor()
// 0x0000049F System.Void Crosstales.Common.Util.SerializableDictionary`2::.ctor(System.Collections.Generic.IDictionary`2<TKey,TVal>)
// 0x000004A0 System.Void Crosstales.Common.Util.SerializableDictionary`2::.ctor(System.Collections.Generic.IEqualityComparer`1<TKey>)
// 0x000004A1 System.Void Crosstales.Common.Util.SerializableDictionary`2::.ctor(System.Int32)
// 0x000004A2 System.Void Crosstales.Common.Util.SerializableDictionary`2::.ctor(System.Collections.Generic.IDictionary`2<TKey,TVal>,System.Collections.Generic.IEqualityComparer`1<TKey>)
// 0x000004A3 System.Void Crosstales.Common.Util.SerializableDictionary`2::.ctor(System.Int32,System.Collections.Generic.IEqualityComparer`1<TKey>)
// 0x000004A4 System.Void Crosstales.Common.Util.SerializableDictionary`2::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
// 0x000004A5 System.Void Crosstales.Common.Util.SerializableDictionary`2::System.Runtime.Serialization.ISerializable.GetObjectData(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
// 0x000004A6 System.Void Crosstales.Common.Util.SerializableDictionary`2::System.Xml.Serialization.IXmlSerializable.WriteXml(System.Xml.XmlWriter)
// 0x000004A7 System.Void Crosstales.Common.Util.SerializableDictionary`2::System.Xml.Serialization.IXmlSerializable.ReadXml(System.Xml.XmlReader)
// 0x000004A8 System.Xml.Schema.XmlSchema Crosstales.Common.Util.SerializableDictionary`2::System.Xml.Serialization.IXmlSerializable.GetSchema()
// 0x000004A9 System.Xml.Serialization.XmlSerializer Crosstales.Common.Util.SerializableDictionary`2::get_ValueSerializer()
// 0x000004AA System.Xml.Serialization.XmlSerializer Crosstales.Common.Util.SerializableDictionary`2::get_KeySerializer()
// 0x000004AB System.Runtime.Serialization.Formatters.Binary.BinaryFormatter Crosstales.Common.Util.SerializeDeSerialize::get_binaryFormatter()
extern void SerializeDeSerialize_get_binaryFormatter_mC4767CFD40FDE7AFFBF0470F2B2EC2A624AC2A17 (void);
// 0x000004AC System.Void Crosstales.Common.Util.SerializeDeSerialize::SerializeToFile(T,System.String)
// 0x000004AD System.Byte[] Crosstales.Common.Util.SerializeDeSerialize::SerializeToByteArray(T)
// 0x000004AE T Crosstales.Common.Util.SerializeDeSerialize::DeserializeFromFile(System.String)
// 0x000004AF T Crosstales.Common.Util.SerializeDeSerialize::DeserializeFromByteArray(System.Byte[])
// 0x000004B0 System.Void Crosstales.Common.Util.SpectrumVisualizer::Start()
extern void SpectrumVisualizer_Start_mD5896C0A90B9B0713E0EAF1780AD2A8CE8AEE124 (void);
// 0x000004B1 System.Void Crosstales.Common.Util.SpectrumVisualizer::Update()
extern void SpectrumVisualizer_Update_m01893AC967BC7986B454BF0BA963EFD0114C8157 (void);
// 0x000004B2 System.Void Crosstales.Common.Util.SpectrumVisualizer::.ctor()
extern void SpectrumVisualizer__ctor_mD85D85B41B224B493E008B394E4059C5926C140F (void);
// 0x000004B3 System.Void Crosstales.Common.Util.XmlHelper::SerializeToFile(T,System.String)
// 0x000004B4 T Crosstales.Common.Util.XmlHelper::DeserializeFromFile(System.String,System.Boolean)
// 0x000004B5 System.String Crosstales.Common.Util.XmlHelper::SerializeToString(T)
// 0x000004B6 T Crosstales.Common.Util.XmlHelper::DeserializeFromString(System.String,System.Boolean)
// 0x000004B7 T Crosstales.Common.Util.XmlHelper::DeserializeFromResource(System.String,System.Boolean)
// 0x000004B8 System.String UnityWebGLSpeechSynthesis.AISbaitso::GetResponse(System.String)
extern void AISbaitso_GetResponse_m92F8D67E443B1F70BDEF0E2F1C9B2BBBFC0E3880 (void);
// 0x000004B9 System.String UnityWebGLSpeechSynthesis.AISbaitso::GetResponseToQuestion(System.String)
extern void AISbaitso_GetResponseToQuestion_mA07B1BEB1020EEE9B2646D42A386EE2A69510509 (void);
// 0x000004BA System.Boolean UnityWebGLSpeechSynthesis.AISbaitso::Has(System.String,System.String[])
extern void AISbaitso_Has_m7AD3A568B623DDCDA86DBEF00C5F058C256EF1D6 (void);
// 0x000004BB System.Void UnityWebGLSpeechSynthesis.AISbaitso::.cctor()
extern void AISbaitso__cctor_m5DF3CCEAB881D3D50553D42187D98574276BF062 (void);
// 0x000004BC System.Void UnityWebGLSpeechSynthesis.BaseSpeechSynthesisPlugin::AddListenerSynthesisOnEnd(UnityWebGLSpeechSynthesis.BaseSpeechSynthesisPlugin/DelegateHandleSynthesisOnEnd)
extern void BaseSpeechSynthesisPlugin_AddListenerSynthesisOnEnd_m349CC7EE80F2CB6FC118DCBA35E219B51A2BDB4F (void);
// 0x000004BD System.Void UnityWebGLSpeechSynthesis.BaseSpeechSynthesisPlugin::RemoveListenerSynthesisOnEnd(UnityWebGLSpeechSynthesis.BaseSpeechSynthesisPlugin/DelegateHandleSynthesisOnEnd)
extern void BaseSpeechSynthesisPlugin_RemoveListenerSynthesisOnEnd_m1C5815D8BA832170A0663F65A4FC291FC9EA08C6 (void);
// 0x000004BE System.Void UnityWebGLSpeechSynthesis.BaseSpeechSynthesisPlugin::.ctor()
extern void BaseSpeechSynthesisPlugin__ctor_m84F6B50B8CEEAA97B481661B44602BB7A1DC1630 (void);
// 0x000004BF System.Void UnityWebGLSpeechSynthesis.BaseSpeechSynthesisPlugin::.cctor()
extern void BaseSpeechSynthesisPlugin__cctor_m0772D220AFCB787BAB691542B6C5F03F1930257A (void);
// 0x000004C0 System.Void UnityWebGLSpeechSynthesis.BaseSpeechSynthesisPlugin/DelegateHandleSynthesisOnEnd::.ctor(System.Object,System.IntPtr)
extern void DelegateHandleSynthesisOnEnd__ctor_m87F5D8EBC016944834D816CBA11DD7D5F4B16AD8 (void);
// 0x000004C1 System.Void UnityWebGLSpeechSynthesis.BaseSpeechSynthesisPlugin/DelegateHandleSynthesisOnEnd::Invoke(UnityWebGLSpeechSynthesis.SpeechSynthesisEvent)
extern void DelegateHandleSynthesisOnEnd_Invoke_m62188BF2D20FDD789C24674F84BF9CFED0425468 (void);
// 0x000004C2 System.IAsyncResult UnityWebGLSpeechSynthesis.BaseSpeechSynthesisPlugin/DelegateHandleSynthesisOnEnd::BeginInvoke(UnityWebGLSpeechSynthesis.SpeechSynthesisEvent,System.AsyncCallback,System.Object)
extern void DelegateHandleSynthesisOnEnd_BeginInvoke_m6C0B3FF3CE090B6C527F9A0204A7708773810A41 (void);
// 0x000004C3 System.Void UnityWebGLSpeechSynthesis.BaseSpeechSynthesisPlugin/DelegateHandleSynthesisOnEnd::EndInvoke(System.IAsyncResult)
extern void DelegateHandleSynthesisOnEnd_EndInvoke_mBBDC80EEC03EE6936AC5C46494A718C2FDE192D5 (void);
// 0x000004C4 System.Void UnityWebGLSpeechSynthesis.BaseSpeechSynthesisPlugin/IdEventArgs::.ctor()
extern void IdEventArgs__ctor_m3F4EDCAC9D21D6A153E0348D146091C35C48F402 (void);
// 0x000004C5 System.Void UnityWebGLSpeechSynthesis.BaseSpeechSynthesisPlugin/SpeechSynthesisUtteranceEventArgs::.ctor()
extern void SpeechSynthesisUtteranceEventArgs__ctor_mC45FC5899DDF647619C228F6326E9A43FF0E0634 (void);
// 0x000004C6 System.Void UnityWebGLSpeechSynthesis.BaseSpeechSynthesisPlugin/VoiceResultArgs::.ctor()
extern void VoiceResultArgs__ctor_m277AD6EEBCFA017FAAD2C579B7DFAA265422CB65 (void);
// 0x000004C7 System.Void UnityWebGLSpeechSynthesis.EditorProxySpeechSynthesisPlugin::Start()
extern void EditorProxySpeechSynthesisPlugin_Start_mAB6CC9376C62684D77940A14182C0B235A4B5A18 (void);
// 0x000004C8 System.Void UnityWebGLSpeechSynthesis.EditorProxySpeechSynthesisPlugin::SafeStartCoroutine(System.String,System.Collections.IEnumerator)
extern void EditorProxySpeechSynthesisPlugin_SafeStartCoroutine_m4E70BB8A1E92CAF5FFCAC69B8071BD7161890A8A (void);
// 0x000004C9 UnityWebGLSpeechSynthesis.IWWW UnityWebGLSpeechSynthesis.EditorProxySpeechSynthesisPlugin::CreateWWW(System.String)
extern void EditorProxySpeechSynthesisPlugin_CreateWWW_mB458D106E623F047352A52FE0E520A5DFE8207DB (void);
// 0x000004CA System.Boolean UnityWebGLSpeechSynthesis.EditorProxySpeechSynthesisPlugin::IsEnabled()
extern void EditorProxySpeechSynthesisPlugin_IsEnabled_m8A9D31B5EE1BF6E82743051EA357B1AB6008556D (void);
// 0x000004CB System.Void UnityWebGLSpeechSynthesis.EditorProxySpeechSynthesisPlugin::SetEnabled(System.Boolean)
extern void EditorProxySpeechSynthesisPlugin_SetEnabled_m1651BB6A12BDA66C5C022C79B42BB85028291A56 (void);
// 0x000004CC UnityWebGLSpeechSynthesis.EditorProxySpeechSynthesisPlugin UnityWebGLSpeechSynthesis.EditorProxySpeechSynthesisPlugin::GetInstance()
extern void EditorProxySpeechSynthesisPlugin_GetInstance_m355F6B059375F3BF4C4C7FB01807D0F3F05B80F8 (void);
// 0x000004CD System.Void UnityWebGLSpeechSynthesis.EditorProxySpeechSynthesisPlugin::EditorUpdate()
extern void EditorProxySpeechSynthesisPlugin_EditorUpdate_mA08840880880CBAE6B9B15DC071A1D2AC75CFA3C (void);
// 0x000004CE System.Void UnityWebGLSpeechSynthesis.EditorProxySpeechSynthesisPlugin::StartEditorUpdates()
extern void EditorProxySpeechSynthesisPlugin_StartEditorUpdates_mEC4D0E6DB82FF7FF59325D83E8166B12484A6D40 (void);
// 0x000004CF System.Void UnityWebGLSpeechSynthesis.EditorProxySpeechSynthesisPlugin::StopEditorUpdates()
extern void EditorProxySpeechSynthesisPlugin_StopEditorUpdates_m9B63B85069F734E126F5D6950280B05D967F09AF (void);
// 0x000004D0 System.Void UnityWebGLSpeechSynthesis.EditorProxySpeechSynthesisPlugin::.ctor()
extern void EditorProxySpeechSynthesisPlugin__ctor_m58C8375E9EADEBDC2A52F6EA6DF2960358F6F108 (void);
// 0x000004D1 UnityWebGLSpeechSynthesis.Example01Synthesis UnityWebGLSpeechSynthesis.Example01Synthesis::GetInstance()
extern void Example01Synthesis_GetInstance_m3DF6430F3260EAA457ACEE38BCA5D2C7A8CE20F8 (void);
// 0x000004D2 System.Void UnityWebGLSpeechSynthesis.Example01Synthesis::Awake()
extern void Example01Synthesis_Awake_m7B07EE004260C539987F4DEC7050C3DCFE5AE156 (void);
// 0x000004D3 System.Collections.IEnumerator UnityWebGLSpeechSynthesis.Example01Synthesis::Start()
extern void Example01Synthesis_Start_m08B4479794B479D161F1950CBACF9031DD93A38B (void);
// 0x000004D4 System.Collections.IEnumerator UnityWebGLSpeechSynthesis.Example01Synthesis::GetVoices()
extern void Example01Synthesis_GetVoices_m5222C0293EAA6FB209232F0A73D75DA1F22C5937 (void);
// 0x000004D5 System.Void UnityWebGLSpeechSynthesis.Example01Synthesis::SetIfReadyForDefaultVoice()
extern void Example01Synthesis_SetIfReadyForDefaultVoice_mCF86709148341DB44F362D6C26222C06C04CF34C (void);
// 0x000004D6 System.Void UnityWebGLSpeechSynthesis.Example01Synthesis::Speak()
extern void Example01Synthesis_Speak_m155718B53A66899F8776D360F3CC9965DBCB0552 (void);
// 0x000004D7 System.Void UnityWebGLSpeechSynthesis.Example01Synthesis::SpeakText(System.String)
extern void Example01Synthesis_SpeakText_mA0D5C6BCE8473243C73D916354393FFC00961250 (void);
// 0x000004D8 System.Collections.IEnumerator UnityWebGLSpeechSynthesis.Example01Synthesis::SetPitch(System.Single)
extern void Example01Synthesis_SetPitch_mC504A0CAE1FDDA39B5B272BDB63B5B4C545017E7 (void);
// 0x000004D9 System.Collections.IEnumerator UnityWebGLSpeechSynthesis.Example01Synthesis::SetRate(System.Single)
extern void Example01Synthesis_SetRate_mE3D46DECE1A051928BDAAC3C9635D6C7986DCC3E (void);
// 0x000004DA System.Void UnityWebGLSpeechSynthesis.Example01Synthesis::FixedUpdate()
extern void Example01Synthesis_FixedUpdate_mDA413F5BFE18B9FDA8630A255D22FE0E6717B090 (void);
// 0x000004DB System.Void UnityWebGLSpeechSynthesis.Example01Synthesis::.ctor()
extern void Example01Synthesis__ctor_mD2D26210701B0F8751AA940BE2DF9912A53D08C3 (void);
// 0x000004DC System.Void UnityWebGLSpeechSynthesis.Example01Synthesis::.cctor()
extern void Example01Synthesis__cctor_mE809C8CB54E090AD9C1E8830C2555CAD2D854B36 (void);
// 0x000004DD System.Void UnityWebGLSpeechSynthesis.Example01Synthesis::<Start>b__22_0(UnityWebGLSpeechSynthesis.SpeechSynthesisUtterance)
extern void Example01Synthesis_U3CStartU3Eb__22_0_m949EFCBA0A18AB6E4A3C65A79A54476776887C4A (void);
// 0x000004DE System.Void UnityWebGLSpeechSynthesis.Example01Synthesis::<Start>b__22_1(System.Single)
extern void Example01Synthesis_U3CStartU3Eb__22_1_mEC15BB057506B9CE63CECC952906E4682748D97E (void);
// 0x000004DF System.Void UnityWebGLSpeechSynthesis.Example01Synthesis::<Start>b__22_2(System.Single)
extern void Example01Synthesis_U3CStartU3Eb__22_2_mC83E3F257936B1EB16E78EA0219E028DD56CBF80 (void);
// 0x000004E0 System.Void UnityWebGLSpeechSynthesis.Example01Synthesis::<Start>b__22_3()
extern void Example01Synthesis_U3CStartU3Eb__22_3_m1255DF35B820D4D4A18882F8EC138AB4D85A2671 (void);
// 0x000004E1 System.Void UnityWebGLSpeechSynthesis.Example01Synthesis::<Start>b__22_4()
extern void Example01Synthesis_U3CStartU3Eb__22_4_m02C7CF99D3995A5F4C83ED560B26073641866E70 (void);
// 0x000004E2 System.Void UnityWebGLSpeechSynthesis.Example01Synthesis::<GetVoices>b__23_0(UnityWebGLSpeechSynthesis.VoiceResult)
extern void Example01Synthesis_U3CGetVoicesU3Eb__23_0_m1B6211E861319A5905671E0865865224B7A85CF2 (void);
// 0x000004E3 System.Void UnityWebGLSpeechSynthesis.Example01Synthesis::<SetIfReadyForDefaultVoice>b__24_0(System.Int32)
extern void Example01Synthesis_U3CSetIfReadyForDefaultVoiceU3Eb__24_0_mD6A9EC56F9C352204D6D74AA6C1E21F12E536013 (void);
// 0x000004E4 System.Void UnityWebGLSpeechSynthesis.Example01Synthesis/<Start>d__22::.ctor(System.Int32)
extern void U3CStartU3Ed__22__ctor_m8F63FDE1DD445BADF80206E5F3E93DDD0680FE84 (void);
// 0x000004E5 System.Void UnityWebGLSpeechSynthesis.Example01Synthesis/<Start>d__22::System.IDisposable.Dispose()
extern void U3CStartU3Ed__22_System_IDisposable_Dispose_mCEAC0024B672F6415833CDE42B9193B11409A425 (void);
// 0x000004E6 System.Boolean UnityWebGLSpeechSynthesis.Example01Synthesis/<Start>d__22::MoveNext()
extern void U3CStartU3Ed__22_MoveNext_m82F14BFF6A9A255849EE5800831175CAEE9A3F41 (void);
// 0x000004E7 System.Object UnityWebGLSpeechSynthesis.Example01Synthesis/<Start>d__22::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CStartU3Ed__22_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m3AD046D2F672A996C8C2414FFFF51CFEE0470407 (void);
// 0x000004E8 System.Void UnityWebGLSpeechSynthesis.Example01Synthesis/<Start>d__22::System.Collections.IEnumerator.Reset()
extern void U3CStartU3Ed__22_System_Collections_IEnumerator_Reset_m85E9BF96346CE3D32AEDC2DD1A49612D23AC3EA4 (void);
// 0x000004E9 System.Object UnityWebGLSpeechSynthesis.Example01Synthesis/<Start>d__22::System.Collections.IEnumerator.get_Current()
extern void U3CStartU3Ed__22_System_Collections_IEnumerator_get_Current_mA1B1C2B5B63C73CB6AC75FA7BB6360950BBDA5B0 (void);
// 0x000004EA System.Void UnityWebGLSpeechSynthesis.Example01Synthesis/<GetVoices>d__23::.ctor(System.Int32)
extern void U3CGetVoicesU3Ed__23__ctor_m09932D130C7602C2D2ABE2EDC806CC7F93359C34 (void);
// 0x000004EB System.Void UnityWebGLSpeechSynthesis.Example01Synthesis/<GetVoices>d__23::System.IDisposable.Dispose()
extern void U3CGetVoicesU3Ed__23_System_IDisposable_Dispose_m2DC14F993D619A7EF6FC06C8267F07CDB90FB520 (void);
// 0x000004EC System.Boolean UnityWebGLSpeechSynthesis.Example01Synthesis/<GetVoices>d__23::MoveNext()
extern void U3CGetVoicesU3Ed__23_MoveNext_m8CFE63BE1BED153DA6C340BE6AE44AA6BFB29D0A (void);
// 0x000004ED System.Object UnityWebGLSpeechSynthesis.Example01Synthesis/<GetVoices>d__23::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CGetVoicesU3Ed__23_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m86945793027A7CCBFD284B2CA99CDB8F25F548F6 (void);
// 0x000004EE System.Void UnityWebGLSpeechSynthesis.Example01Synthesis/<GetVoices>d__23::System.Collections.IEnumerator.Reset()
extern void U3CGetVoicesU3Ed__23_System_Collections_IEnumerator_Reset_mC5A052AA5B31420FF5DB67F371DD9210CEA04744 (void);
// 0x000004EF System.Object UnityWebGLSpeechSynthesis.Example01Synthesis/<GetVoices>d__23::System.Collections.IEnumerator.get_Current()
extern void U3CGetVoicesU3Ed__23_System_Collections_IEnumerator_get_Current_m39A69FC140927C187412E3E2DF98D7E1493A857F (void);
// 0x000004F0 System.Void UnityWebGLSpeechSynthesis.Example01Synthesis/<SetPitch>d__27::.ctor(System.Int32)
extern void U3CSetPitchU3Ed__27__ctor_m003F38176D39FA4206F6183C0D39B1501CD8E9EE (void);
// 0x000004F1 System.Void UnityWebGLSpeechSynthesis.Example01Synthesis/<SetPitch>d__27::System.IDisposable.Dispose()
extern void U3CSetPitchU3Ed__27_System_IDisposable_Dispose_m680F577B98258C3E1CC4C275D39BCEEE86722879 (void);
// 0x000004F2 System.Boolean UnityWebGLSpeechSynthesis.Example01Synthesis/<SetPitch>d__27::MoveNext()
extern void U3CSetPitchU3Ed__27_MoveNext_m1001B876947EDCC6ABAE10A3657C7942328D2755 (void);
// 0x000004F3 System.Object UnityWebGLSpeechSynthesis.Example01Synthesis/<SetPitch>d__27::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CSetPitchU3Ed__27_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mD2A3DF093FD380180CCEF194C5D8964455D1ACC6 (void);
// 0x000004F4 System.Void UnityWebGLSpeechSynthesis.Example01Synthesis/<SetPitch>d__27::System.Collections.IEnumerator.Reset()
extern void U3CSetPitchU3Ed__27_System_Collections_IEnumerator_Reset_m80B114456DDDB5BAF8B39F9ADBA407CF1606FA1B (void);
// 0x000004F5 System.Object UnityWebGLSpeechSynthesis.Example01Synthesis/<SetPitch>d__27::System.Collections.IEnumerator.get_Current()
extern void U3CSetPitchU3Ed__27_System_Collections_IEnumerator_get_Current_m12C49895937FE64B85129B8C377E5A3AFF30595E (void);
// 0x000004F6 System.Void UnityWebGLSpeechSynthesis.Example01Synthesis/<SetRate>d__28::.ctor(System.Int32)
extern void U3CSetRateU3Ed__28__ctor_m00BBD7D8FD9177152A89D342340510FF249A036D (void);
// 0x000004F7 System.Void UnityWebGLSpeechSynthesis.Example01Synthesis/<SetRate>d__28::System.IDisposable.Dispose()
extern void U3CSetRateU3Ed__28_System_IDisposable_Dispose_m56D7E3D6BD827138BC5D04C0361D36F6F728BD37 (void);
// 0x000004F8 System.Boolean UnityWebGLSpeechSynthesis.Example01Synthesis/<SetRate>d__28::MoveNext()
extern void U3CSetRateU3Ed__28_MoveNext_m5D79517D9ACEB5CA8B0AA83389400727EE7A4E81 (void);
// 0x000004F9 System.Object UnityWebGLSpeechSynthesis.Example01Synthesis/<SetRate>d__28::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CSetRateU3Ed__28_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m464D800221095630A17ACFB9260ACD0394848238 (void);
// 0x000004FA System.Void UnityWebGLSpeechSynthesis.Example01Synthesis/<SetRate>d__28::System.Collections.IEnumerator.Reset()
extern void U3CSetRateU3Ed__28_System_Collections_IEnumerator_Reset_m4A460D7A3E015022450C8EBC12A92AC5E0E72327 (void);
// 0x000004FB System.Object UnityWebGLSpeechSynthesis.Example01Synthesis/<SetRate>d__28::System.Collections.IEnumerator.get_Current()
extern void U3CSetRateU3Ed__28_System_Collections_IEnumerator_get_Current_m92D4A794D0F0532A96D04F1F96D0520515BB6BFE (void);
// 0x000004FC UnityWebGLSpeechSynthesis.Example02Proxy UnityWebGLSpeechSynthesis.Example02Proxy::GetInstance()
extern void Example02Proxy_GetInstance_m7A8A611D91925D17FFC3EEA257ED54DE9149665E (void);
// 0x000004FD System.Void UnityWebGLSpeechSynthesis.Example02Proxy::Awake()
extern void Example02Proxy_Awake_mE67BE75F01BF4DC8709C9DE897DB3B7ED3ECA098 (void);
// 0x000004FE System.Collections.IEnumerator UnityWebGLSpeechSynthesis.Example02Proxy::Start()
extern void Example02Proxy_Start_m37473AF15D3767E2869EBCEE623BAE1EA5A3DD74 (void);
// 0x000004FF System.Collections.IEnumerator UnityWebGLSpeechSynthesis.Example02Proxy::GetVoices()
extern void Example02Proxy_GetVoices_m5CC03E5F7B7566BE7120A098AF29B93443E8FFEB (void);
// 0x00000500 System.Void UnityWebGLSpeechSynthesis.Example02Proxy::SetIfReadyForDefaultVoice()
extern void Example02Proxy_SetIfReadyForDefaultVoice_m63BB446DB1739490DC8EE511D5F38CD0C8EF1181 (void);
// 0x00000501 System.Void UnityWebGLSpeechSynthesis.Example02Proxy::Speak()
extern void Example02Proxy_Speak_mBF06DB3BA5C77BC5D1462002F3AD60F05CF33080 (void);
// 0x00000502 System.Collections.IEnumerator UnityWebGLSpeechSynthesis.Example02Proxy::SetPitch(System.Single)
extern void Example02Proxy_SetPitch_mB099B881684EA372B6A219FB562B7DE2FBB807EB (void);
// 0x00000503 System.Collections.IEnumerator UnityWebGLSpeechSynthesis.Example02Proxy::SetRate(System.Single)
extern void Example02Proxy_SetRate_m5D08473E4431DC7FC479BA30C3C46A7C1AA7496E (void);
// 0x00000504 System.Void UnityWebGLSpeechSynthesis.Example02Proxy::FixedUpdate()
extern void Example02Proxy_FixedUpdate_mCE8168F5C14E32DE8400EB9F918F484051BD786B (void);
// 0x00000505 System.Void UnityWebGLSpeechSynthesis.Example02Proxy::.ctor()
extern void Example02Proxy__ctor_m24BD4FB850E20623BEE8F117FBC92E739B9C6DBD (void);
// 0x00000506 System.Void UnityWebGLSpeechSynthesis.Example02Proxy::.cctor()
extern void Example02Proxy__cctor_m0EE76FFC56B0E08CC88FFCB532D10A1F39A44EF1 (void);
// 0x00000507 System.Void UnityWebGLSpeechSynthesis.Example02Proxy::<Start>b__21_0(UnityWebGLSpeechSynthesis.SpeechSynthesisUtterance)
extern void Example02Proxy_U3CStartU3Eb__21_0_m9D025B9B4BEF452A16A7982B1D97903206B6932E (void);
// 0x00000508 System.Void UnityWebGLSpeechSynthesis.Example02Proxy::<Start>b__21_1(System.Single)
extern void Example02Proxy_U3CStartU3Eb__21_1_m995FFE286128B847EEFC2C8AC1466000A0049BDC (void);
// 0x00000509 System.Void UnityWebGLSpeechSynthesis.Example02Proxy::<Start>b__21_2(System.Single)
extern void Example02Proxy_U3CStartU3Eb__21_2_mB754C6C46342D6CA55790E4A35598DE3CB0FAE37 (void);
// 0x0000050A System.Void UnityWebGLSpeechSynthesis.Example02Proxy::<Start>b__21_3()
extern void Example02Proxy_U3CStartU3Eb__21_3_mB22FCD82AF377C1F1E2DB07466B114D5C04EFAFF (void);
// 0x0000050B System.Void UnityWebGLSpeechSynthesis.Example02Proxy::<Start>b__21_4()
extern void Example02Proxy_U3CStartU3Eb__21_4_m582889A0D069349173DEAECEBF17FBD8D31ACD2D (void);
// 0x0000050C System.Void UnityWebGLSpeechSynthesis.Example02Proxy::<GetVoices>b__22_0(UnityWebGLSpeechSynthesis.VoiceResult)
extern void Example02Proxy_U3CGetVoicesU3Eb__22_0_m42F9EBAD38B20DA39C2048894A8D388D9DAFC868 (void);
// 0x0000050D System.Void UnityWebGLSpeechSynthesis.Example02Proxy::<SetIfReadyForDefaultVoice>b__23_0(System.Int32)
extern void Example02Proxy_U3CSetIfReadyForDefaultVoiceU3Eb__23_0_m91A0BD1BA1727D47E51FF7F98D861C1A3E4C998C (void);
// 0x0000050E System.Void UnityWebGLSpeechSynthesis.Example02Proxy/<Start>d__21::.ctor(System.Int32)
extern void U3CStartU3Ed__21__ctor_m34D03537D704CFF9A884EB90E3B2F486006DFB88 (void);
// 0x0000050F System.Void UnityWebGLSpeechSynthesis.Example02Proxy/<Start>d__21::System.IDisposable.Dispose()
extern void U3CStartU3Ed__21_System_IDisposable_Dispose_m3A2D102BC25169A90D6112DBA9F6CEDB2AB0D302 (void);
// 0x00000510 System.Boolean UnityWebGLSpeechSynthesis.Example02Proxy/<Start>d__21::MoveNext()
extern void U3CStartU3Ed__21_MoveNext_m067CD367062F353368FED180ABBB77DB57546F99 (void);
// 0x00000511 System.Object UnityWebGLSpeechSynthesis.Example02Proxy/<Start>d__21::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CStartU3Ed__21_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m93363FB8A68A91796C20BAFDA50A3AF8F123EC4A (void);
// 0x00000512 System.Void UnityWebGLSpeechSynthesis.Example02Proxy/<Start>d__21::System.Collections.IEnumerator.Reset()
extern void U3CStartU3Ed__21_System_Collections_IEnumerator_Reset_m58501F0C8729C05E18577FD05B05586917C3AC30 (void);
// 0x00000513 System.Object UnityWebGLSpeechSynthesis.Example02Proxy/<Start>d__21::System.Collections.IEnumerator.get_Current()
extern void U3CStartU3Ed__21_System_Collections_IEnumerator_get_Current_mA3E9E23A0E3353169EB46A29FA714C083DE7C027 (void);
// 0x00000514 System.Void UnityWebGLSpeechSynthesis.Example02Proxy/<GetVoices>d__22::.ctor(System.Int32)
extern void U3CGetVoicesU3Ed__22__ctor_m60E3A6BD3C508E0EE96E30CBB6A4C3E39A7B2565 (void);
// 0x00000515 System.Void UnityWebGLSpeechSynthesis.Example02Proxy/<GetVoices>d__22::System.IDisposable.Dispose()
extern void U3CGetVoicesU3Ed__22_System_IDisposable_Dispose_mD45D8FFE08464334151FFC3DA894F01E8989F0F0 (void);
// 0x00000516 System.Boolean UnityWebGLSpeechSynthesis.Example02Proxy/<GetVoices>d__22::MoveNext()
extern void U3CGetVoicesU3Ed__22_MoveNext_m18E1E909414D4890F0216EE8B065199D9471E793 (void);
// 0x00000517 System.Object UnityWebGLSpeechSynthesis.Example02Proxy/<GetVoices>d__22::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CGetVoicesU3Ed__22_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m203840620BCA464A576234B4503A43D2F4638E10 (void);
// 0x00000518 System.Void UnityWebGLSpeechSynthesis.Example02Proxy/<GetVoices>d__22::System.Collections.IEnumerator.Reset()
extern void U3CGetVoicesU3Ed__22_System_Collections_IEnumerator_Reset_m134E6832088185074D6B6BC66965C2A0161DD2F0 (void);
// 0x00000519 System.Object UnityWebGLSpeechSynthesis.Example02Proxy/<GetVoices>d__22::System.Collections.IEnumerator.get_Current()
extern void U3CGetVoicesU3Ed__22_System_Collections_IEnumerator_get_Current_mAAE532A14E45572D45BD62073D30AB28453F23F6 (void);
// 0x0000051A System.Void UnityWebGLSpeechSynthesis.Example02Proxy/<SetPitch>d__25::.ctor(System.Int32)
extern void U3CSetPitchU3Ed__25__ctor_m7B27B1C736DD32C058AC4FEDE82949BFC73E63FE (void);
// 0x0000051B System.Void UnityWebGLSpeechSynthesis.Example02Proxy/<SetPitch>d__25::System.IDisposable.Dispose()
extern void U3CSetPitchU3Ed__25_System_IDisposable_Dispose_mE29FBB4928139BE55235E76AC18A944D31A82F11 (void);
// 0x0000051C System.Boolean UnityWebGLSpeechSynthesis.Example02Proxy/<SetPitch>d__25::MoveNext()
extern void U3CSetPitchU3Ed__25_MoveNext_m0C172E1649ACF901C5D8F528BE229C3D286C4C6A (void);
// 0x0000051D System.Object UnityWebGLSpeechSynthesis.Example02Proxy/<SetPitch>d__25::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CSetPitchU3Ed__25_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m349DED4BC47B17F94F69EB6F7730D002F9D2A916 (void);
// 0x0000051E System.Void UnityWebGLSpeechSynthesis.Example02Proxy/<SetPitch>d__25::System.Collections.IEnumerator.Reset()
extern void U3CSetPitchU3Ed__25_System_Collections_IEnumerator_Reset_m550C252F8CEFA833ECB2E74ED259602D355615BB (void);
// 0x0000051F System.Object UnityWebGLSpeechSynthesis.Example02Proxy/<SetPitch>d__25::System.Collections.IEnumerator.get_Current()
extern void U3CSetPitchU3Ed__25_System_Collections_IEnumerator_get_Current_m7DC2266D9F998979A595E0C599798F981ABC3D9B (void);
// 0x00000520 System.Void UnityWebGLSpeechSynthesis.Example02Proxy/<SetRate>d__26::.ctor(System.Int32)
extern void U3CSetRateU3Ed__26__ctor_m30B3B76B925AD990DF7BD5B8DC85B7676E5A19DC (void);
// 0x00000521 System.Void UnityWebGLSpeechSynthesis.Example02Proxy/<SetRate>d__26::System.IDisposable.Dispose()
extern void U3CSetRateU3Ed__26_System_IDisposable_Dispose_m77B3B26C13157B46E0FECB42F5B0FCE7EDAAA07E (void);
// 0x00000522 System.Boolean UnityWebGLSpeechSynthesis.Example02Proxy/<SetRate>d__26::MoveNext()
extern void U3CSetRateU3Ed__26_MoveNext_m477A5B9C84EC9C9F308CA9B2365173F1F0DA487D (void);
// 0x00000523 System.Object UnityWebGLSpeechSynthesis.Example02Proxy/<SetRate>d__26::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CSetRateU3Ed__26_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mEA8A2AC6CD13B5A30D676B3A36F2669CD9049E95 (void);
// 0x00000524 System.Void UnityWebGLSpeechSynthesis.Example02Proxy/<SetRate>d__26::System.Collections.IEnumerator.Reset()
extern void U3CSetRateU3Ed__26_System_Collections_IEnumerator_Reset_mC7EA2588C39A3A5DF74047F6295DFEA96E636F43 (void);
// 0x00000525 System.Object UnityWebGLSpeechSynthesis.Example02Proxy/<SetRate>d__26::System.Collections.IEnumerator.get_Current()
extern void U3CSetRateU3Ed__26_System_Collections_IEnumerator_get_Current_m485B95A8B747FDF00C4F4C377B4238929F64A129 (void);
// 0x00000526 UnityWebGLSpeechSynthesis.Example03ProxyManagement UnityWebGLSpeechSynthesis.Example03ProxyManagement::GetInstance()
extern void Example03ProxyManagement_GetInstance_mA83F0825EF8D2EB90587B79D32949A3C1EAFD17C (void);
// 0x00000527 System.Void UnityWebGLSpeechSynthesis.Example03ProxyManagement::Awake()
extern void Example03ProxyManagement_Awake_mE02347603F2C32C44184FA6C3731FCEB265EF637 (void);
// 0x00000528 System.Collections.IEnumerator UnityWebGLSpeechSynthesis.Example03ProxyManagement::Start()
extern void Example03ProxyManagement_Start_mA4DFBA24A44B2606CADEBFC763757B9E1CC3C727 (void);
// 0x00000529 System.Void UnityWebGLSpeechSynthesis.Example03ProxyManagement::.ctor()
extern void Example03ProxyManagement__ctor_m8E9ED007A8C8399BD55B275A5BFEB01FB56CA25F (void);
// 0x0000052A System.Void UnityWebGLSpeechSynthesis.Example03ProxyManagement::.cctor()
extern void Example03ProxyManagement__cctor_mE6EF9C0C6550956291F358232A1510381C796970 (void);
// 0x0000052B System.Void UnityWebGLSpeechSynthesis.Example03ProxyManagement::<Start>b__10_0()
extern void Example03ProxyManagement_U3CStartU3Eb__10_0_mFE057FFD6EE14734A9598B1978E79B8F43AFD718 (void);
// 0x0000052C System.Void UnityWebGLSpeechSynthesis.Example03ProxyManagement::<Start>b__10_1()
extern void Example03ProxyManagement_U3CStartU3Eb__10_1_m86FA78AD1A72F257BF6002F3BCABC772CB4C848B (void);
// 0x0000052D System.Void UnityWebGLSpeechSynthesis.Example03ProxyManagement::<Start>b__10_2()
extern void Example03ProxyManagement_U3CStartU3Eb__10_2_m6F92CB585A1F54D584FBBEE187E07D7E55C1DC4D (void);
// 0x0000052E System.Void UnityWebGLSpeechSynthesis.Example03ProxyManagement::<Start>b__10_3()
extern void Example03ProxyManagement_U3CStartU3Eb__10_3_mB46D74519F6952A179CD242E86D73F9D1AE74DE0 (void);
// 0x0000052F System.Void UnityWebGLSpeechSynthesis.Example03ProxyManagement::<Start>b__10_4()
extern void Example03ProxyManagement_U3CStartU3Eb__10_4_m4D57CB44112E5A6ABA7D1836F603D5A410BF819B (void);
// 0x00000530 System.Void UnityWebGLSpeechSynthesis.Example03ProxyManagement/<Start>d__10::.ctor(System.Int32)
extern void U3CStartU3Ed__10__ctor_mD2EBEF01559763AEB41147B69F400D9C37BA7249 (void);
// 0x00000531 System.Void UnityWebGLSpeechSynthesis.Example03ProxyManagement/<Start>d__10::System.IDisposable.Dispose()
extern void U3CStartU3Ed__10_System_IDisposable_Dispose_m5DAC313C765AD7414291F37C5AF18854D673A3A0 (void);
// 0x00000532 System.Boolean UnityWebGLSpeechSynthesis.Example03ProxyManagement/<Start>d__10::MoveNext()
extern void U3CStartU3Ed__10_MoveNext_m4FB6245C073BBFFF290C93449D6F76E0E11429C4 (void);
// 0x00000533 System.Object UnityWebGLSpeechSynthesis.Example03ProxyManagement/<Start>d__10::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CStartU3Ed__10_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m7E1CA9BC1A4949854DB2FC73BA73A82370745DC7 (void);
// 0x00000534 System.Void UnityWebGLSpeechSynthesis.Example03ProxyManagement/<Start>d__10::System.Collections.IEnumerator.Reset()
extern void U3CStartU3Ed__10_System_Collections_IEnumerator_Reset_m6854DE8042609BFACA888413C64DAA593D89FAF4 (void);
// 0x00000535 System.Object UnityWebGLSpeechSynthesis.Example03ProxyManagement/<Start>d__10::System.Collections.IEnumerator.get_Current()
extern void U3CStartU3Ed__10_System_Collections_IEnumerator_get_Current_mA22569F7BDAE354C9B873EFC702C0ED39C4827BF (void);
// 0x00000536 System.Void UnityWebGLSpeechSynthesis.Example04SbaitsoClone::CreateText(System.String)
extern void Example04SbaitsoClone_CreateText_mA138C7E85EEBDA9344B7570E3803DE82A32DBAEF (void);
// 0x00000537 System.Collections.IEnumerator UnityWebGLSpeechSynthesis.Example04SbaitsoClone::CreateNameInputField()
extern void Example04SbaitsoClone_CreateNameInputField_m7251F4E29A15E8ADA11E793B867807C1BDF53E93 (void);
// 0x00000538 System.Collections.IEnumerator UnityWebGLSpeechSynthesis.Example04SbaitsoClone::CreateTalkInputField()
extern void Example04SbaitsoClone_CreateTalkInputField_m31AE5BB29E181784A714AFDCDA0EDAEE14859DA3 (void);
// 0x00000539 System.Void UnityWebGLSpeechSynthesis.Example04SbaitsoClone::CreateTextAndSpeak(System.String)
extern void Example04SbaitsoClone_CreateTextAndSpeak_m2EB3BCE8CD75ECA7D403B20E8E6A68D69F2A22D6 (void);
// 0x0000053A UnityWebGLSpeechSynthesis.Example04SbaitsoClone UnityWebGLSpeechSynthesis.Example04SbaitsoClone::GetInstance()
extern void Example04SbaitsoClone_GetInstance_m1070D238AD5E91A1701EE0F6D3FD5B0EF107428F (void);
// 0x0000053B System.Void UnityWebGLSpeechSynthesis.Example04SbaitsoClone::Awake()
extern void Example04SbaitsoClone_Awake_m7E9ED3948685ABBF3A065CB26FD516E390B5AB49 (void);
// 0x0000053C System.Collections.IEnumerator UnityWebGLSpeechSynthesis.Example04SbaitsoClone::Start()
extern void Example04SbaitsoClone_Start_m188E9E8B00F0A4C342849FEAE3D6EA1121799EC9 (void);
// 0x0000053D System.Void UnityWebGLSpeechSynthesis.Example04SbaitsoClone::HandleSynthesisOnEnd(UnityWebGLSpeechSynthesis.SpeechSynthesisEvent)
extern void Example04SbaitsoClone_HandleSynthesisOnEnd_m3BC32436F9B441EF014E374B5989090ABF71102B (void);
// 0x0000053E System.Collections.IEnumerator UnityWebGLSpeechSynthesis.Example04SbaitsoClone::GetVoices()
extern void Example04SbaitsoClone_GetVoices_m3536E808EF94AD1532169BF1E74B3E2FC6946077 (void);
// 0x0000053F System.Void UnityWebGLSpeechSynthesis.Example04SbaitsoClone::FixedUpdate()
extern void Example04SbaitsoClone_FixedUpdate_mE82ACF725D6F5FF21E162E5342BB32435D97D4F2 (void);
// 0x00000540 System.Void UnityWebGLSpeechSynthesis.Example04SbaitsoClone::Speak(System.String)
extern void Example04SbaitsoClone_Speak_mDBEFCE1D0191F85968B3D961838AABC637E288A2 (void);
// 0x00000541 System.Void UnityWebGLSpeechSynthesis.Example04SbaitsoClone::.ctor()
extern void Example04SbaitsoClone__ctor_m85792C9C617E44DFAD75E94B0388AA7848B402E4 (void);
// 0x00000542 System.Void UnityWebGLSpeechSynthesis.Example04SbaitsoClone::.cctor()
extern void Example04SbaitsoClone__cctor_mE6480B645577DA9C3D29FE6667F235035F2661FF (void);
// 0x00000543 System.Void UnityWebGLSpeechSynthesis.Example04SbaitsoClone::<Start>b__19_0(UnityWebGLSpeechSynthesis.SpeechSynthesisUtterance)
extern void Example04SbaitsoClone_U3CStartU3Eb__19_0_m168596718F058D2D41F4C9484A840635C1BD625A (void);
// 0x00000544 System.Void UnityWebGLSpeechSynthesis.Example04SbaitsoClone::<GetVoices>b__21_0(UnityWebGLSpeechSynthesis.VoiceResult)
extern void Example04SbaitsoClone_U3CGetVoicesU3Eb__21_0_m6C4B0FBBB6A3F4EB878F0C7D48EF5FD29CB034C3 (void);
// 0x00000545 System.Void UnityWebGLSpeechSynthesis.Example04SbaitsoClone/<CreateNameInputField>d__13::.ctor(System.Int32)
extern void U3CCreateNameInputFieldU3Ed__13__ctor_mDBA1413DC9CE6AB47ED198621A1984CF6F696778 (void);
// 0x00000546 System.Void UnityWebGLSpeechSynthesis.Example04SbaitsoClone/<CreateNameInputField>d__13::System.IDisposable.Dispose()
extern void U3CCreateNameInputFieldU3Ed__13_System_IDisposable_Dispose_m76CEC2693009D059E51D719735A8C29C78F2D55F (void);
// 0x00000547 System.Boolean UnityWebGLSpeechSynthesis.Example04SbaitsoClone/<CreateNameInputField>d__13::MoveNext()
extern void U3CCreateNameInputFieldU3Ed__13_MoveNext_m68879B0FE314F281992056AC4B48C9117FFCCC6D (void);
// 0x00000548 System.Object UnityWebGLSpeechSynthesis.Example04SbaitsoClone/<CreateNameInputField>d__13::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CCreateNameInputFieldU3Ed__13_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m8AD8BAE973E0D51E605F03CB874C65EDAE7B6303 (void);
// 0x00000549 System.Void UnityWebGLSpeechSynthesis.Example04SbaitsoClone/<CreateNameInputField>d__13::System.Collections.IEnumerator.Reset()
extern void U3CCreateNameInputFieldU3Ed__13_System_Collections_IEnumerator_Reset_m29FA913BFECC9BC77B3D8C64D0B5F95E98B3B03B (void);
// 0x0000054A System.Object UnityWebGLSpeechSynthesis.Example04SbaitsoClone/<CreateNameInputField>d__13::System.Collections.IEnumerator.get_Current()
extern void U3CCreateNameInputFieldU3Ed__13_System_Collections_IEnumerator_get_Current_m5F9AD182E107360333F5777C391A7412A5DF12F4 (void);
// 0x0000054B System.Void UnityWebGLSpeechSynthesis.Example04SbaitsoClone/<CreateTalkInputField>d__14::.ctor(System.Int32)
extern void U3CCreateTalkInputFieldU3Ed__14__ctor_m280826AEA884301256581138A0DC339D8C684623 (void);
// 0x0000054C System.Void UnityWebGLSpeechSynthesis.Example04SbaitsoClone/<CreateTalkInputField>d__14::System.IDisposable.Dispose()
extern void U3CCreateTalkInputFieldU3Ed__14_System_IDisposable_Dispose_mB8862146AB8A709DAAB288A0D45BE59D49D9C437 (void);
// 0x0000054D System.Boolean UnityWebGLSpeechSynthesis.Example04SbaitsoClone/<CreateTalkInputField>d__14::MoveNext()
extern void U3CCreateTalkInputFieldU3Ed__14_MoveNext_m38D954D24C4532401AA7B2598B7BEDDE07A1405A (void);
// 0x0000054E System.Object UnityWebGLSpeechSynthesis.Example04SbaitsoClone/<CreateTalkInputField>d__14::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CCreateTalkInputFieldU3Ed__14_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mC2563FF15E08EA33D9870DD936C3363F560EF051 (void);
// 0x0000054F System.Void UnityWebGLSpeechSynthesis.Example04SbaitsoClone/<CreateTalkInputField>d__14::System.Collections.IEnumerator.Reset()
extern void U3CCreateTalkInputFieldU3Ed__14_System_Collections_IEnumerator_Reset_mBFFDBE5B05883FD5E838BB70BAD45122DD3EB2D4 (void);
// 0x00000550 System.Object UnityWebGLSpeechSynthesis.Example04SbaitsoClone/<CreateTalkInputField>d__14::System.Collections.IEnumerator.get_Current()
extern void U3CCreateTalkInputFieldU3Ed__14_System_Collections_IEnumerator_get_Current_m0535514B983DD7861146E646862AB810E742736D (void);
// 0x00000551 System.Void UnityWebGLSpeechSynthesis.Example04SbaitsoClone/<Start>d__19::.ctor(System.Int32)
extern void U3CStartU3Ed__19__ctor_m9279FC198E1CF92431BA6E3D7AAC85511DD0FFB7 (void);
// 0x00000552 System.Void UnityWebGLSpeechSynthesis.Example04SbaitsoClone/<Start>d__19::System.IDisposable.Dispose()
extern void U3CStartU3Ed__19_System_IDisposable_Dispose_m96082E25192AA2F4B3C5FD7E21EFF89E5823D3EF (void);
// 0x00000553 System.Boolean UnityWebGLSpeechSynthesis.Example04SbaitsoClone/<Start>d__19::MoveNext()
extern void U3CStartU3Ed__19_MoveNext_m4FCAD168BFCD1C9DA52122C1EE1F032AA6BF2D6F (void);
// 0x00000554 System.Object UnityWebGLSpeechSynthesis.Example04SbaitsoClone/<Start>d__19::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CStartU3Ed__19_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mC09FF475BB8AB91B54583BB90F1C22F2B7F86D97 (void);
// 0x00000555 System.Void UnityWebGLSpeechSynthesis.Example04SbaitsoClone/<Start>d__19::System.Collections.IEnumerator.Reset()
extern void U3CStartU3Ed__19_System_Collections_IEnumerator_Reset_m8FF2E99C3BD4919C45F728E2ADF3FC20FD82D798 (void);
// 0x00000556 System.Object UnityWebGLSpeechSynthesis.Example04SbaitsoClone/<Start>d__19::System.Collections.IEnumerator.get_Current()
extern void U3CStartU3Ed__19_System_Collections_IEnumerator_get_Current_m73BAD620D7E8A75EAC27FA0E5459D5E320A65689 (void);
// 0x00000557 System.Void UnityWebGLSpeechSynthesis.Example04SbaitsoClone/<GetVoices>d__21::.ctor(System.Int32)
extern void U3CGetVoicesU3Ed__21__ctor_m13BCD013A34C0770207E488A37BEA58FE7CEADF9 (void);
// 0x00000558 System.Void UnityWebGLSpeechSynthesis.Example04SbaitsoClone/<GetVoices>d__21::System.IDisposable.Dispose()
extern void U3CGetVoicesU3Ed__21_System_IDisposable_Dispose_m918A7E0FA7B6541257B774BD803300F9CF988D32 (void);
// 0x00000559 System.Boolean UnityWebGLSpeechSynthesis.Example04SbaitsoClone/<GetVoices>d__21::MoveNext()
extern void U3CGetVoicesU3Ed__21_MoveNext_mCD8CEBF773342D6585528FD6C51832ECED82BBE3 (void);
// 0x0000055A System.Object UnityWebGLSpeechSynthesis.Example04SbaitsoClone/<GetVoices>d__21::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CGetVoicesU3Ed__21_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m7655E6EBDBCC12EB165CBA18546B85DDDA414F99 (void);
// 0x0000055B System.Void UnityWebGLSpeechSynthesis.Example04SbaitsoClone/<GetVoices>d__21::System.Collections.IEnumerator.Reset()
extern void U3CGetVoicesU3Ed__21_System_Collections_IEnumerator_Reset_m465989673030024C527CDB60A4963F20BB87CA4C (void);
// 0x0000055C System.Object UnityWebGLSpeechSynthesis.Example04SbaitsoClone/<GetVoices>d__21::System.Collections.IEnumerator.get_Current()
extern void U3CGetVoicesU3Ed__21_System_Collections_IEnumerator_get_Current_mE6C19EFC27F7715437D206D11126D3F89EF09D1D (void);
// 0x0000055D UnityWebGLSpeechSynthesis.Example06NoGUI UnityWebGLSpeechSynthesis.Example06NoGUI::GetInstance()
extern void Example06NoGUI_GetInstance_mFC2491C2C24D8DD6725D4FCAD6DFD4029B91438E (void);
// 0x0000055E System.Void UnityWebGLSpeechSynthesis.Example06NoGUI::Awake()
extern void Example06NoGUI_Awake_mB67D05A40002752FE7088877CF03FC0B342C6F7D (void);
// 0x0000055F System.Collections.IEnumerator UnityWebGLSpeechSynthesis.Example06NoGUI::Start()
extern void Example06NoGUI_Start_mD573B09D7C17C0E1C75D8C9E5C7851E79FFB5671 (void);
// 0x00000560 System.Collections.IEnumerator UnityWebGLSpeechSynthesis.Example06NoGUI::GetVoices()
extern void Example06NoGUI_GetVoices_m701DB6D05DCBA2C4E0C01EAB95DCF0DE17700F0C (void);
// 0x00000561 System.Void UnityWebGLSpeechSynthesis.Example06NoGUI::Speak()
extern void Example06NoGUI_Speak_m7B9F138752054134C7D3FFA2FFAD7B48F2543E96 (void);
// 0x00000562 System.Void UnityWebGLSpeechSynthesis.Example06NoGUI::FixedUpdate()
extern void Example06NoGUI_FixedUpdate_m785051DCFAEF079DB606DE10F5A2E4C90A8481E3 (void);
// 0x00000563 System.Void UnityWebGLSpeechSynthesis.Example06NoGUI::OnSpeechAPILoaded()
extern void Example06NoGUI_OnSpeechAPILoaded_m7782991EC7A23FAD066CD0CA14DD50A7AADFEA6C (void);
// 0x00000564 System.Void UnityWebGLSpeechSynthesis.Example06NoGUI::HandleSynthesisOnEnd(UnityWebGLSpeechSynthesis.SpeechSynthesisEvent)
extern void Example06NoGUI_HandleSynthesisOnEnd_mB9A55F9B24A3EBB4F07C91BE5E778432913E49A2 (void);
// 0x00000565 System.Void UnityWebGLSpeechSynthesis.Example06NoGUI::.ctor()
extern void Example06NoGUI__ctor_m21E4BBE8422A0EFDF3C34F6EDAE3E088A71C09BD (void);
// 0x00000566 System.Void UnityWebGLSpeechSynthesis.Example06NoGUI::.cctor()
extern void Example06NoGUI__cctor_mF4EC294B98C5DEA7B7A41B2100E7037295DF260B (void);
// 0x00000567 System.Void UnityWebGLSpeechSynthesis.Example06NoGUI::<Start>b__10_0(UnityWebGLSpeechSynthesis.SpeechSynthesisUtterance)
extern void Example06NoGUI_U3CStartU3Eb__10_0_m72EF17BC2CD555921CE715E7C3F3F3219EF145EC (void);
// 0x00000568 System.Void UnityWebGLSpeechSynthesis.Example06NoGUI::<GetVoices>b__11_0(UnityWebGLSpeechSynthesis.VoiceResult)
extern void Example06NoGUI_U3CGetVoicesU3Eb__11_0_m94B8870A2FEE4697A251BA871BC88897E0E6DD35 (void);
// 0x00000569 System.Void UnityWebGLSpeechSynthesis.Example06NoGUI/<Start>d__10::.ctor(System.Int32)
extern void U3CStartU3Ed__10__ctor_m8ED59AE01DDBDFB931EE86943CE3BC3848C6ADDD (void);
// 0x0000056A System.Void UnityWebGLSpeechSynthesis.Example06NoGUI/<Start>d__10::System.IDisposable.Dispose()
extern void U3CStartU3Ed__10_System_IDisposable_Dispose_m42B1BC2D8C580E8807BD4FCD43E3929D47E29AEF (void);
// 0x0000056B System.Boolean UnityWebGLSpeechSynthesis.Example06NoGUI/<Start>d__10::MoveNext()
extern void U3CStartU3Ed__10_MoveNext_m2707806E878501ECC3E5703613221DD6A325E8E4 (void);
// 0x0000056C System.Object UnityWebGLSpeechSynthesis.Example06NoGUI/<Start>d__10::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CStartU3Ed__10_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mAEC49344EDB434739EC4154F2D0B6284D5F79863 (void);
// 0x0000056D System.Void UnityWebGLSpeechSynthesis.Example06NoGUI/<Start>d__10::System.Collections.IEnumerator.Reset()
extern void U3CStartU3Ed__10_System_Collections_IEnumerator_Reset_m54625ACABF650958F029E6CAA8623766896CE327 (void);
// 0x0000056E System.Object UnityWebGLSpeechSynthesis.Example06NoGUI/<Start>d__10::System.Collections.IEnumerator.get_Current()
extern void U3CStartU3Ed__10_System_Collections_IEnumerator_get_Current_mCF54097F282F03AAEDDE41D140D95B5DC33E51A6 (void);
// 0x0000056F System.Void UnityWebGLSpeechSynthesis.Example06NoGUI/<GetVoices>d__11::.ctor(System.Int32)
extern void U3CGetVoicesU3Ed__11__ctor_m2F1AF8243FA5D18261369A410EA1F6F68002DE42 (void);
// 0x00000570 System.Void UnityWebGLSpeechSynthesis.Example06NoGUI/<GetVoices>d__11::System.IDisposable.Dispose()
extern void U3CGetVoicesU3Ed__11_System_IDisposable_Dispose_m97EE3273B4579EB45B880904B822AD7D8D2A91D1 (void);
// 0x00000571 System.Boolean UnityWebGLSpeechSynthesis.Example06NoGUI/<GetVoices>d__11::MoveNext()
extern void U3CGetVoicesU3Ed__11_MoveNext_mFB1FE6DC652FE43800AEA3A463ACAA777B7E47F7 (void);
// 0x00000572 System.Object UnityWebGLSpeechSynthesis.Example06NoGUI/<GetVoices>d__11::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CGetVoicesU3Ed__11_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m8CD07E3E6785C4A14570F67E8B754BAD6A9C5C7C (void);
// 0x00000573 System.Void UnityWebGLSpeechSynthesis.Example06NoGUI/<GetVoices>d__11::System.Collections.IEnumerator.Reset()
extern void U3CGetVoicesU3Ed__11_System_Collections_IEnumerator_Reset_m648E41DC38C5F7CEAEFEA8DD33C737DF7E7DE35E (void);
// 0x00000574 System.Object UnityWebGLSpeechSynthesis.Example06NoGUI/<GetVoices>d__11::System.Collections.IEnumerator.get_Current()
extern void U3CGetVoicesU3Ed__11_System_Collections_IEnumerator_get_Current_m9209DA2EE51310298CE318289E76D2ED29966EB5 (void);
// 0x00000575 System.Void UnityWebGLSpeechSynthesis.Example07Buttons::OnGUI()
extern void Example07Buttons_OnGUI_m428630080659246823F50B71F5619842852DDDCA (void);
// 0x00000576 System.Void UnityWebGLSpeechSynthesis.Example07Buttons::.ctor()
extern void Example07Buttons__ctor_mF146DDCE9898DD4B09B551E5900D043301A6E4BF (void);
// 0x00000577 System.Void UnityWebGLSpeechSynthesis.Example07Buttons::.cctor()
extern void Example07Buttons__cctor_m02647D43A86237F185AD96C9949A92B79BBE8C6B (void);
// 0x00000578 UnityWebGLSpeechSynthesis.Example07Simple UnityWebGLSpeechSynthesis.Example07Simple::GetInstance()
extern void Example07Simple_GetInstance_mBD4FC3372615B9F34C0233C5E3F4E91B65A6FBE6 (void);
// 0x00000579 System.Void UnityWebGLSpeechSynthesis.Example07Simple::Awake()
extern void Example07Simple_Awake_mC53FCE95F1A2C21D8957533CB1BDA431989C83D3 (void);
// 0x0000057A System.Collections.IEnumerator UnityWebGLSpeechSynthesis.Example07Simple::Start()
extern void Example07Simple_Start_m5BEC553E42EFDA38D04730050A02F0468FEB6B85 (void);
// 0x0000057B System.Collections.IEnumerator UnityWebGLSpeechSynthesis.Example07Simple::GetVoices()
extern void Example07Simple_GetVoices_m0C572C4EAEBD69CDD3A6CB7E53A6BD7874413D05 (void);
// 0x0000057C System.Void UnityWebGLSpeechSynthesis.Example07Simple::Speak(System.String)
extern void Example07Simple_Speak_m6F9FE36067901C88FA02F332A8512A0233256B55 (void);
// 0x0000057D System.Void UnityWebGLSpeechSynthesis.Example07Simple::FixedUpdate()
extern void Example07Simple_FixedUpdate_m3615F1BE37961A3E2FFADA4F8471DEC199B614C6 (void);
// 0x0000057E System.Void UnityWebGLSpeechSynthesis.Example07Simple::.ctor()
extern void Example07Simple__ctor_m0942AC3C570C63C4E5FECAE7A9EE9DE7916E5880 (void);
// 0x0000057F System.Void UnityWebGLSpeechSynthesis.Example07Simple::.cctor()
extern void Example07Simple__cctor_m017E3E4D6626B7B1AF4BAEECE33F35F4A1AE06CD (void);
// 0x00000580 System.Void UnityWebGLSpeechSynthesis.Example07Simple::<Start>b__9_0(UnityWebGLSpeechSynthesis.SpeechSynthesisUtterance)
extern void Example07Simple_U3CStartU3Eb__9_0_m0E131621CB02A42CCFAD90DE45FDD289329E3035 (void);
// 0x00000581 System.Void UnityWebGLSpeechSynthesis.Example07Simple::<GetVoices>b__10_0(UnityWebGLSpeechSynthesis.VoiceResult)
extern void Example07Simple_U3CGetVoicesU3Eb__10_0_m0F7FC45ABC08DCB270C219EC8BD94A12EDDCC5E3 (void);
// 0x00000582 System.Void UnityWebGLSpeechSynthesis.Example07Simple/<Start>d__9::.ctor(System.Int32)
extern void U3CStartU3Ed__9__ctor_mF5D98CC9248DFAA670B1E1E8DC105AA80B6A3F7A (void);
// 0x00000583 System.Void UnityWebGLSpeechSynthesis.Example07Simple/<Start>d__9::System.IDisposable.Dispose()
extern void U3CStartU3Ed__9_System_IDisposable_Dispose_m0A53CD40C8A18435CED5140A56604E7716F04E54 (void);
// 0x00000584 System.Boolean UnityWebGLSpeechSynthesis.Example07Simple/<Start>d__9::MoveNext()
extern void U3CStartU3Ed__9_MoveNext_m32152B56ECBF2EA2BD1AD1E9A1B12FD560913D80 (void);
// 0x00000585 System.Object UnityWebGLSpeechSynthesis.Example07Simple/<Start>d__9::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CStartU3Ed__9_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m1A2A789E1298A2DF842422EC709EF1D338316C5B (void);
// 0x00000586 System.Void UnityWebGLSpeechSynthesis.Example07Simple/<Start>d__9::System.Collections.IEnumerator.Reset()
extern void U3CStartU3Ed__9_System_Collections_IEnumerator_Reset_m5A0F287025DEEE61494DC0B746B49472E5107D88 (void);
// 0x00000587 System.Object UnityWebGLSpeechSynthesis.Example07Simple/<Start>d__9::System.Collections.IEnumerator.get_Current()
extern void U3CStartU3Ed__9_System_Collections_IEnumerator_get_Current_mA91D397DE5EF1EC8FF80A4687A787A8BB5B0C9AB (void);
// 0x00000588 System.Void UnityWebGLSpeechSynthesis.Example07Simple/<GetVoices>d__10::.ctor(System.Int32)
extern void U3CGetVoicesU3Ed__10__ctor_m745BB18F289E59888EBC4DA97E51151670AB0B10 (void);
// 0x00000589 System.Void UnityWebGLSpeechSynthesis.Example07Simple/<GetVoices>d__10::System.IDisposable.Dispose()
extern void U3CGetVoicesU3Ed__10_System_IDisposable_Dispose_m8EEED7ADFA85960ED3B9A9465D4E64C1C6338589 (void);
// 0x0000058A System.Boolean UnityWebGLSpeechSynthesis.Example07Simple/<GetVoices>d__10::MoveNext()
extern void U3CGetVoicesU3Ed__10_MoveNext_m9A7B8E4DE9BFE8FCADB6E5C7647E35A1C7F287D5 (void);
// 0x0000058B System.Object UnityWebGLSpeechSynthesis.Example07Simple/<GetVoices>d__10::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CGetVoicesU3Ed__10_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mE112EB43A922DCD1F08557C0B2EB65ED0BF27869 (void);
// 0x0000058C System.Void UnityWebGLSpeechSynthesis.Example07Simple/<GetVoices>d__10::System.Collections.IEnumerator.Reset()
extern void U3CGetVoicesU3Ed__10_System_Collections_IEnumerator_Reset_mE597695CBDFD7A5FF951AFF58AE3451AF0BECA1D (void);
// 0x0000058D System.Object UnityWebGLSpeechSynthesis.Example07Simple/<GetVoices>d__10::System.Collections.IEnumerator.get_Current()
extern void U3CGetVoicesU3Ed__10_System_Collections_IEnumerator_get_Current_m642317F9B85A2F6F491C70917B7FC44742E9DBBF (void);
// 0x0000058E System.Void UnityWebGLSpeechSynthesis.ISpeechSynthesisPlugin::AddListenerSynthesisOnEnd(UnityWebGLSpeechSynthesis.BaseSpeechSynthesisPlugin/DelegateHandleSynthesisOnEnd)
// 0x0000058F System.Void UnityWebGLSpeechSynthesis.ISpeechSynthesisPlugin::Cancel()
// 0x00000590 System.Void UnityWebGLSpeechSynthesis.ISpeechSynthesisPlugin::CreateSpeechSynthesisUtterance(System.Action`1<UnityWebGLSpeechSynthesis.SpeechSynthesisUtterance>)
// 0x00000591 System.Void UnityWebGLSpeechSynthesis.ISpeechSynthesisPlugin::GetVoices(System.Action`1<UnityWebGLSpeechSynthesis.VoiceResult>)
// 0x00000592 System.Boolean UnityWebGLSpeechSynthesis.ISpeechSynthesisPlugin::IsAvailable()
// 0x00000593 System.Void UnityWebGLSpeechSynthesis.ISpeechSynthesisPlugin::ManagementCloseBrowserTab()
// 0x00000594 System.Void UnityWebGLSpeechSynthesis.ISpeechSynthesisPlugin::ManagementCloseProxy()
// 0x00000595 System.Void UnityWebGLSpeechSynthesis.ISpeechSynthesisPlugin::ManagementLaunchProxy()
// 0x00000596 System.Void UnityWebGLSpeechSynthesis.ISpeechSynthesisPlugin::ManagementOpenBrowserTab()
// 0x00000597 System.Void UnityWebGLSpeechSynthesis.ISpeechSynthesisPlugin::ManagementSetProxyPort(System.Int32)
// 0x00000598 System.Void UnityWebGLSpeechSynthesis.ISpeechSynthesisPlugin::RemoveListenerSynthesisOnEnd(UnityWebGLSpeechSynthesis.BaseSpeechSynthesisPlugin/DelegateHandleSynthesisOnEnd)
// 0x00000599 System.Void UnityWebGLSpeechSynthesis.ISpeechSynthesisPlugin::SetPitch(UnityWebGLSpeechSynthesis.SpeechSynthesisUtterance,System.Single)
// 0x0000059A System.Void UnityWebGLSpeechSynthesis.ISpeechSynthesisPlugin::SetRate(UnityWebGLSpeechSynthesis.SpeechSynthesisUtterance,System.Single)
// 0x0000059B System.Void UnityWebGLSpeechSynthesis.ISpeechSynthesisPlugin::SetText(UnityWebGLSpeechSynthesis.SpeechSynthesisUtterance,System.String)
// 0x0000059C System.Void UnityWebGLSpeechSynthesis.ISpeechSynthesisPlugin::SetVoice(UnityWebGLSpeechSynthesis.SpeechSynthesisUtterance,UnityWebGLSpeechSynthesis.Voice)
// 0x0000059D System.Void UnityWebGLSpeechSynthesis.ISpeechSynthesisPlugin::SetVolume(UnityWebGLSpeechSynthesis.SpeechSynthesisUtterance,System.Single)
// 0x0000059E System.Void UnityWebGLSpeechSynthesis.ISpeechSynthesisPlugin::Speak(UnityWebGLSpeechSynthesis.SpeechSynthesisUtterance)
// 0x0000059F System.Boolean UnityWebGLSpeechSynthesis.IWWW::IsDone()
// 0x000005A0 System.String UnityWebGLSpeechSynthesis.IWWW::GetError()
// 0x000005A1 System.String UnityWebGLSpeechSynthesis.IWWW::GetText()
// 0x000005A2 System.Void UnityWebGLSpeechSynthesis.IWWW::Dispose()
// 0x000005A3 UnityWebGLSpeechSynthesis.ProxySpeechSynthesisPlugin UnityWebGLSpeechSynthesis.ProxySpeechSynthesisPlugin::GetInstance()
extern void ProxySpeechSynthesisPlugin_GetInstance_mA29C631545B88F6A4706F99C06ABE5B5F43C6F81 (void);
// 0x000005A4 System.Void UnityWebGLSpeechSynthesis.ProxySpeechSynthesisPlugin::SafeStartCoroutine(System.String,System.Collections.IEnumerator)
extern void ProxySpeechSynthesisPlugin_SafeStartCoroutine_mEDF885D8A43EA20C87E80CDE07BEBD7A3D2800E3 (void);
// 0x000005A5 System.Void UnityWebGLSpeechSynthesis.ProxySpeechSynthesisPlugin::Awake()
extern void ProxySpeechSynthesisPlugin_Awake_mE1E2B83E58B334F10543CA56B06B393F1ABE7C24 (void);
// 0x000005A6 System.Void UnityWebGLSpeechSynthesis.ProxySpeechSynthesisPlugin::Start()
extern void ProxySpeechSynthesisPlugin_Start_mFC70CB6F8F4186F71EFB5323D599A10B462905C6 (void);
// 0x000005A7 System.Collections.IEnumerator UnityWebGLSpeechSynthesis.ProxySpeechSynthesisPlugin::Init()
extern void ProxySpeechSynthesisPlugin_Init_m2D4B203DDA9D67986B9456F93CBE0194D2FAE051 (void);
// 0x000005A8 System.Collections.IEnumerator UnityWebGLSpeechSynthesis.ProxySpeechSynthesisPlugin::RunPendingCommands()
extern void ProxySpeechSynthesisPlugin_RunPendingCommands_m61C1E0632B9CE5E8FA6DF8112E2BD8C0A65CE50D (void);
// 0x000005A9 System.Collections.IEnumerator UnityWebGLSpeechSynthesis.ProxySpeechSynthesisPlugin::ProxyUtterance()
extern void ProxySpeechSynthesisPlugin_ProxyUtterance_m00F041E6FB4EE1F8D0485464E200BBEF1AF98D49 (void);
// 0x000005AA System.Collections.IEnumerator UnityWebGLSpeechSynthesis.ProxySpeechSynthesisPlugin::ProxyVoices()
extern void ProxySpeechSynthesisPlugin_ProxyVoices_m0318C1715E4EDDCCE6B378C7DA66F82348EA530A (void);
// 0x000005AB System.Collections.IEnumerator UnityWebGLSpeechSynthesis.ProxySpeechSynthesisPlugin::ProxyOnEnd()
extern void ProxySpeechSynthesisPlugin_ProxyOnEnd_mE91F7F13A45699A8A258E6BD90A4DDD310F9AE5E (void);
// 0x000005AC System.Boolean UnityWebGLSpeechSynthesis.ProxySpeechSynthesisPlugin::IsAvailable()
extern void ProxySpeechSynthesisPlugin_IsAvailable_m2339C084BC48372BABD47C1E5D13F1F2C04A1E20 (void);
// 0x000005AD System.Void UnityWebGLSpeechSynthesis.ProxySpeechSynthesisPlugin::AddCommand(System.String)
extern void ProxySpeechSynthesisPlugin_AddCommand_m630DF09D9ABEC3A3E745FC2A0BDB144FDC4845E6 (void);
// 0x000005AE System.Void UnityWebGLSpeechSynthesis.ProxySpeechSynthesisPlugin::CreateSpeechSynthesisUtterance(System.Action`1<UnityWebGLSpeechSynthesis.SpeechSynthesisUtterance>)
extern void ProxySpeechSynthesisPlugin_CreateSpeechSynthesisUtterance_mB3C4330DD0C925654FFA6597A60234BE11643F5A (void);
// 0x000005AF System.Void UnityWebGLSpeechSynthesis.ProxySpeechSynthesisPlugin::GetVoices(System.Action`1<UnityWebGLSpeechSynthesis.VoiceResult>)
extern void ProxySpeechSynthesisPlugin_GetVoices_m536DF84B9C887A5CC8C016E210DC510CBD4085EB (void);
// 0x000005B0 System.Void UnityWebGLSpeechSynthesis.ProxySpeechSynthesisPlugin::SetPitch(UnityWebGLSpeechSynthesis.SpeechSynthesisUtterance,System.Single)
extern void ProxySpeechSynthesisPlugin_SetPitch_m3FEBAE6D195D93E92F4DDB3A30A61CFA532FF09D (void);
// 0x000005B1 System.Void UnityWebGLSpeechSynthesis.ProxySpeechSynthesisPlugin::SetRate(UnityWebGLSpeechSynthesis.SpeechSynthesisUtterance,System.Single)
extern void ProxySpeechSynthesisPlugin_SetRate_mB3B8BC0B1095261298AEC1BFA84FAB3F1B2B7EB2 (void);
// 0x000005B2 System.Void UnityWebGLSpeechSynthesis.ProxySpeechSynthesisPlugin::SetText(UnityWebGLSpeechSynthesis.SpeechSynthesisUtterance,System.String)
extern void ProxySpeechSynthesisPlugin_SetText_m375FE5DE718612EA5796C224E3375802D1E6B630 (void);
// 0x000005B3 System.Void UnityWebGLSpeechSynthesis.ProxySpeechSynthesisPlugin::SetVolume(UnityWebGLSpeechSynthesis.SpeechSynthesisUtterance,System.Single)
extern void ProxySpeechSynthesisPlugin_SetVolume_m563C7A0E1C9EB3B5C0A05FF58122FEC7BCE2E877 (void);
// 0x000005B4 System.Void UnityWebGLSpeechSynthesis.ProxySpeechSynthesisPlugin::Speak(UnityWebGLSpeechSynthesis.SpeechSynthesisUtterance)
extern void ProxySpeechSynthesisPlugin_Speak_mE0B9C1B2C5916FD3FD96766C053E64CC24B0DD77 (void);
// 0x000005B5 System.Void UnityWebGLSpeechSynthesis.ProxySpeechSynthesisPlugin::Cancel()
extern void ProxySpeechSynthesisPlugin_Cancel_m3C2F6018BDDE58013B78783F722277BE7A6B3172 (void);
// 0x000005B6 System.Void UnityWebGLSpeechSynthesis.ProxySpeechSynthesisPlugin::SetVoice(UnityWebGLSpeechSynthesis.SpeechSynthesisUtterance,UnityWebGLSpeechSynthesis.Voice)
extern void ProxySpeechSynthesisPlugin_SetVoice_mDB2790E1D1CFDE447D35A4895ABF81F0F9140F2E (void);
// 0x000005B7 System.Void UnityWebGLSpeechSynthesis.ProxySpeechSynthesisPlugin::ManagementCloseBrowserTab()
extern void ProxySpeechSynthesisPlugin_ManagementCloseBrowserTab_m33EDDEBBAFDD91595ED3276A16D9C50E7AE8F354 (void);
// 0x000005B8 System.Void UnityWebGLSpeechSynthesis.ProxySpeechSynthesisPlugin::ManagementCloseProxy()
extern void ProxySpeechSynthesisPlugin_ManagementCloseProxy_m956A8494881AD80E2D4B22C4249F52175EF2A053 (void);
// 0x000005B9 System.Void UnityWebGLSpeechSynthesis.ProxySpeechSynthesisPlugin::ManagementLaunchProxy()
extern void ProxySpeechSynthesisPlugin_ManagementLaunchProxy_mD5F417468293698CCEDEA69B3D09D439B11D54C2 (void);
// 0x000005BA System.Void UnityWebGLSpeechSynthesis.ProxySpeechSynthesisPlugin::ManagementOpenBrowserTab()
extern void ProxySpeechSynthesisPlugin_ManagementOpenBrowserTab_mDC40A4645A84070F50BAC8AB989F5D13BE480999 (void);
// 0x000005BB System.Void UnityWebGLSpeechSynthesis.ProxySpeechSynthesisPlugin::ManagementSetProxyPort(System.Int32)
extern void ProxySpeechSynthesisPlugin_ManagementSetProxyPort_m903352FB0F065AD0C41E0016086C64B54E0D9F8B (void);
// 0x000005BC UnityWebGLSpeechSynthesis.IWWW UnityWebGLSpeechSynthesis.ProxySpeechSynthesisPlugin::CreateWWW(System.String)
extern void ProxySpeechSynthesisPlugin_CreateWWW_m97ED0681D0D5AECAACAD32B244272CB266587A79 (void);
// 0x000005BD System.Void UnityWebGLSpeechSynthesis.ProxySpeechSynthesisPlugin::.ctor()
extern void ProxySpeechSynthesisPlugin__ctor_m0FDDA29B2A2A424D8E1B00E1CB8EB77B3F57F004 (void);
// 0x000005BE System.Void UnityWebGLSpeechSynthesis.ProxySpeechSynthesisPlugin::.cctor()
extern void ProxySpeechSynthesisPlugin__cctor_mE25689F12571B40E0EF7F6253F70B8928E637DB9 (void);
// 0x000005BF System.Void UnityWebGLSpeechSynthesis.ProxySpeechSynthesisPlugin/<Init>d__12::.ctor(System.Int32)
extern void U3CInitU3Ed__12__ctor_m76C78B57D3E077558A1B64BD3C60394348A55EB7 (void);
// 0x000005C0 System.Void UnityWebGLSpeechSynthesis.ProxySpeechSynthesisPlugin/<Init>d__12::System.IDisposable.Dispose()
extern void U3CInitU3Ed__12_System_IDisposable_Dispose_mCF76C3D929D98063E6D635DBE241A28010B2419C (void);
// 0x000005C1 System.Boolean UnityWebGLSpeechSynthesis.ProxySpeechSynthesisPlugin/<Init>d__12::MoveNext()
extern void U3CInitU3Ed__12_MoveNext_mD0624FC908B6C59554A8BA1B80609E72C429A88F (void);
// 0x000005C2 System.Object UnityWebGLSpeechSynthesis.ProxySpeechSynthesisPlugin/<Init>d__12::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CInitU3Ed__12_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mA1875409A854BC8BCBD1706ADC7DE32EC5FC79BF (void);
// 0x000005C3 System.Void UnityWebGLSpeechSynthesis.ProxySpeechSynthesisPlugin/<Init>d__12::System.Collections.IEnumerator.Reset()
extern void U3CInitU3Ed__12_System_Collections_IEnumerator_Reset_m172521A167F15E22BE4AB993B191F3CB20ABE238 (void);
// 0x000005C4 System.Object UnityWebGLSpeechSynthesis.ProxySpeechSynthesisPlugin/<Init>d__12::System.Collections.IEnumerator.get_Current()
extern void U3CInitU3Ed__12_System_Collections_IEnumerator_get_Current_mC582877BCF04920CDC9C5837E6B7AC27286781DF (void);
// 0x000005C5 System.Void UnityWebGLSpeechSynthesis.ProxySpeechSynthesisPlugin/<RunPendingCommands>d__13::.ctor(System.Int32)
extern void U3CRunPendingCommandsU3Ed__13__ctor_mF755B6F406B21FCDBA541DD1F627B61DBA9D7B38 (void);
// 0x000005C6 System.Void UnityWebGLSpeechSynthesis.ProxySpeechSynthesisPlugin/<RunPendingCommands>d__13::System.IDisposable.Dispose()
extern void U3CRunPendingCommandsU3Ed__13_System_IDisposable_Dispose_m5553B88D973F8D223B7C2E1988E707DA5903BFEE (void);
// 0x000005C7 System.Boolean UnityWebGLSpeechSynthesis.ProxySpeechSynthesisPlugin/<RunPendingCommands>d__13::MoveNext()
extern void U3CRunPendingCommandsU3Ed__13_MoveNext_m939262D23606AE7B45930DC58411C81AA4962941 (void);
// 0x000005C8 System.Object UnityWebGLSpeechSynthesis.ProxySpeechSynthesisPlugin/<RunPendingCommands>d__13::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CRunPendingCommandsU3Ed__13_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mE6332DB63594041697D2F788EB9A569EFA45F9E6 (void);
// 0x000005C9 System.Void UnityWebGLSpeechSynthesis.ProxySpeechSynthesisPlugin/<RunPendingCommands>d__13::System.Collections.IEnumerator.Reset()
extern void U3CRunPendingCommandsU3Ed__13_System_Collections_IEnumerator_Reset_mDA11BAC1A537E93CA1D2FB62F55A1BF67AE7C422 (void);
// 0x000005CA System.Object UnityWebGLSpeechSynthesis.ProxySpeechSynthesisPlugin/<RunPendingCommands>d__13::System.Collections.IEnumerator.get_Current()
extern void U3CRunPendingCommandsU3Ed__13_System_Collections_IEnumerator_get_Current_mA480802E726F9F7FE60F4D244CDC4C7E947A661B (void);
// 0x000005CB System.Void UnityWebGLSpeechSynthesis.ProxySpeechSynthesisPlugin/<ProxyUtterance>d__14::.ctor(System.Int32)
extern void U3CProxyUtteranceU3Ed__14__ctor_m6B50B1BC665C70213F42952F27D32620696E2215 (void);
// 0x000005CC System.Void UnityWebGLSpeechSynthesis.ProxySpeechSynthesisPlugin/<ProxyUtterance>d__14::System.IDisposable.Dispose()
extern void U3CProxyUtteranceU3Ed__14_System_IDisposable_Dispose_mBD5CFAAAECAC03CE4AEBE083261B789E45C93183 (void);
// 0x000005CD System.Boolean UnityWebGLSpeechSynthesis.ProxySpeechSynthesisPlugin/<ProxyUtterance>d__14::MoveNext()
extern void U3CProxyUtteranceU3Ed__14_MoveNext_mF7DDA12DE889747A313305E01612059A4B7F8FBA (void);
// 0x000005CE System.Object UnityWebGLSpeechSynthesis.ProxySpeechSynthesisPlugin/<ProxyUtterance>d__14::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CProxyUtteranceU3Ed__14_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m0A1B3F6C03C24C597E853792C14E8CBC3EC4E787 (void);
// 0x000005CF System.Void UnityWebGLSpeechSynthesis.ProxySpeechSynthesisPlugin/<ProxyUtterance>d__14::System.Collections.IEnumerator.Reset()
extern void U3CProxyUtteranceU3Ed__14_System_Collections_IEnumerator_Reset_m33B544F80C9BD6E23E7EC8CAF68D3BDDE5434177 (void);
// 0x000005D0 System.Object UnityWebGLSpeechSynthesis.ProxySpeechSynthesisPlugin/<ProxyUtterance>d__14::System.Collections.IEnumerator.get_Current()
extern void U3CProxyUtteranceU3Ed__14_System_Collections_IEnumerator_get_Current_mFDE586AB0058DAE09E26C1649BF6CE1446B4B4C0 (void);
// 0x000005D1 System.Void UnityWebGLSpeechSynthesis.ProxySpeechSynthesisPlugin/<ProxyVoices>d__15::.ctor(System.Int32)
extern void U3CProxyVoicesU3Ed__15__ctor_m9D51FA49B63EA5B2D6D4741F9AD4638E9F3768D1 (void);
// 0x000005D2 System.Void UnityWebGLSpeechSynthesis.ProxySpeechSynthesisPlugin/<ProxyVoices>d__15::System.IDisposable.Dispose()
extern void U3CProxyVoicesU3Ed__15_System_IDisposable_Dispose_m125C573F8DCD66E614ED372FE4BEECB946D19048 (void);
// 0x000005D3 System.Boolean UnityWebGLSpeechSynthesis.ProxySpeechSynthesisPlugin/<ProxyVoices>d__15::MoveNext()
extern void U3CProxyVoicesU3Ed__15_MoveNext_m4424B1E154EA2BF3165FC891664A2B12562ADCA5 (void);
// 0x000005D4 System.Object UnityWebGLSpeechSynthesis.ProxySpeechSynthesisPlugin/<ProxyVoices>d__15::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CProxyVoicesU3Ed__15_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m5AD44B4EDBDC4428C3026163414865806493DE1A (void);
// 0x000005D5 System.Void UnityWebGLSpeechSynthesis.ProxySpeechSynthesisPlugin/<ProxyVoices>d__15::System.Collections.IEnumerator.Reset()
extern void U3CProxyVoicesU3Ed__15_System_Collections_IEnumerator_Reset_mB95FFE93FD0F160DDC6170F5515C3F7122F676FD (void);
// 0x000005D6 System.Object UnityWebGLSpeechSynthesis.ProxySpeechSynthesisPlugin/<ProxyVoices>d__15::System.Collections.IEnumerator.get_Current()
extern void U3CProxyVoicesU3Ed__15_System_Collections_IEnumerator_get_Current_mB48913C5ECA3605995DCCF3E055E8332181C0745 (void);
// 0x000005D7 System.Void UnityWebGLSpeechSynthesis.ProxySpeechSynthesisPlugin/<ProxyOnEnd>d__16::.ctor(System.Int32)
extern void U3CProxyOnEndU3Ed__16__ctor_m6D7E54EB078F354B70FD6C4FFDD910AE9485008A (void);
// 0x000005D8 System.Void UnityWebGLSpeechSynthesis.ProxySpeechSynthesisPlugin/<ProxyOnEnd>d__16::System.IDisposable.Dispose()
extern void U3CProxyOnEndU3Ed__16_System_IDisposable_Dispose_m6A373202B6709BFEAF7D17EBD44817066FEAF057 (void);
// 0x000005D9 System.Boolean UnityWebGLSpeechSynthesis.ProxySpeechSynthesisPlugin/<ProxyOnEnd>d__16::MoveNext()
extern void U3CProxyOnEndU3Ed__16_MoveNext_mB8464C1990059F3AE5DDDF72B1D0A27678B5C902 (void);
// 0x000005DA System.Object UnityWebGLSpeechSynthesis.ProxySpeechSynthesisPlugin/<ProxyOnEnd>d__16::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CProxyOnEndU3Ed__16_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m4335A867E6CDA8B392ECCC15F6E8C1ACFDE5BDFB (void);
// 0x000005DB System.Void UnityWebGLSpeechSynthesis.ProxySpeechSynthesisPlugin/<ProxyOnEnd>d__16::System.Collections.IEnumerator.Reset()
extern void U3CProxyOnEndU3Ed__16_System_Collections_IEnumerator_Reset_mF623461CC34757BD5120223664D337EF6492EAC4 (void);
// 0x000005DC System.Object UnityWebGLSpeechSynthesis.ProxySpeechSynthesisPlugin/<ProxyOnEnd>d__16::System.Collections.IEnumerator.get_Current()
extern void U3CProxyOnEndU3Ed__16_System_Collections_IEnumerator_get_Current_m7C1A32E5A25138F22368DD2B3C9E66C2BABFA8F9 (void);
// 0x000005DD System.Void UnityWebGLSpeechSynthesis.SpeechProxyConfig::.ctor()
extern void SpeechProxyConfig__ctor_m83F5FBA39D5E6C885C924916F18814B8E58905CE (void);
// 0x000005DE System.Void UnityWebGLSpeechSynthesis.SpeechSynthesisEvent::.ctor()
extern void SpeechSynthesisEvent__ctor_m6DB0A931E0C44C922FE95BDA0891D0E75BF00325 (void);
// 0x000005DF UnityWebGLSpeechSynthesis.ISpeechSynthesisPlugin UnityWebGLSpeechSynthesis.SpeechSynthesisUtils::GetInstance()
extern void SpeechSynthesisUtils_GetInstance_mF0760DAD3871D091AD9DC7DB95B14A40E8F81ADB (void);
// 0x000005E0 System.Void UnityWebGLSpeechSynthesis.SpeechSynthesisUtils::PausePlayerPrefs(System.Boolean)
extern void SpeechSynthesisUtils_PausePlayerPrefs_mA60F8D30DA7BDA4844A267FCF67415611064F19D (void);
// 0x000005E1 System.Void UnityWebGLSpeechSynthesis.SpeechSynthesisUtils::SetActive(System.Boolean,UnityEngine.Component[])
extern void SpeechSynthesisUtils_SetActive_m1B7D834516CFBA8840500E95C0C190B85626AB8E (void);
// 0x000005E2 System.Void UnityWebGLSpeechSynthesis.SpeechSynthesisUtils::SetInteractable(System.Boolean,UnityEngine.UI.Selectable[])
extern void SpeechSynthesisUtils_SetInteractable_m0D993F7363AB78CFBA8052B611937D418C0ED8B4 (void);
// 0x000005E3 System.Void UnityWebGLSpeechSynthesis.SpeechSynthesisUtils::PopulateDropdown(UnityEngine.UI.Dropdown,System.Collections.Generic.List`1<System.String>)
extern void SpeechSynthesisUtils_PopulateDropdown_m405E52E5E43F58408CF50515D96F83540D4B5DC0 (void);
// 0x000005E4 System.Void UnityWebGLSpeechSynthesis.SpeechSynthesisUtils::PopulateVoicesDropdown(UnityEngine.UI.Dropdown,UnityWebGLSpeechSynthesis.VoiceResult)
extern void SpeechSynthesisUtils_PopulateVoicesDropdown_mDD787015DE9EF937F6A6BC310BC9A73819351B4E (void);
// 0x000005E5 System.Void UnityWebGLSpeechSynthesis.SpeechSynthesisUtils::PopulateVoices(System.String[]&,System.Int32&,UnityWebGLSpeechSynthesis.VoiceResult)
extern void SpeechSynthesisUtils_PopulateVoices_m314B354E515DB2FD31547CE35FD1DCA465E1D7C4 (void);
// 0x000005E6 System.Void UnityWebGLSpeechSynthesis.SpeechSynthesisUtils::SelectIndex(UnityEngine.UI.Dropdown,System.Int32)
extern void SpeechSynthesisUtils_SelectIndex_mC8BC962701E4A7B7D714A8F51BA8A496E4CD66F7 (void);
// 0x000005E7 UnityWebGLSpeechSynthesis.Voice UnityWebGLSpeechSynthesis.SpeechSynthesisUtils::GetVoice(UnityWebGLSpeechSynthesis.VoiceResult,System.String)
extern void SpeechSynthesisUtils_GetVoice_mF9645724EEFEE83D45CD15B056749851D6412900 (void);
// 0x000005E8 System.Void UnityWebGLSpeechSynthesis.SpeechSynthesisUtils::HandleVoiceChangedDropdown(UnityEngine.UI.Dropdown,UnityWebGLSpeechSynthesis.VoiceResult,UnityWebGLSpeechSynthesis.SpeechSynthesisUtterance,UnityWebGLSpeechSynthesis.ISpeechSynthesisPlugin)
extern void SpeechSynthesisUtils_HandleVoiceChangedDropdown_m23C69E841750D18AC8EBFADE7B67DA902B0995DA (void);
// 0x000005E9 System.Void UnityWebGLSpeechSynthesis.SpeechSynthesisUtils::HandleVoiceChanged(System.String[],System.Int32,UnityWebGLSpeechSynthesis.VoiceResult,UnityWebGLSpeechSynthesis.SpeechSynthesisUtterance,UnityWebGLSpeechSynthesis.ISpeechSynthesisPlugin)
extern void SpeechSynthesisUtils_HandleVoiceChanged_mC439AE0C169DC3EE107D8DD8FD9360B352C9D95C (void);
// 0x000005EA System.String UnityWebGLSpeechSynthesis.SpeechSynthesisUtils::GetDefaultVoice()
extern void SpeechSynthesisUtils_GetDefaultVoice_mCDDDE3DFAD4F6AC07D5764BF128EF4D5826E53C4 (void);
// 0x000005EB System.Void UnityWebGLSpeechSynthesis.SpeechSynthesisUtils::SetDefaultVoice(System.String)
extern void SpeechSynthesisUtils_SetDefaultVoice_m4D1FB4EACDDED194FA334052CEAC0EDF3AE212E2 (void);
// 0x000005EC System.Void UnityWebGLSpeechSynthesis.SpeechSynthesisUtils::RestoreVoice(UnityEngine.UI.Dropdown)
extern void SpeechSynthesisUtils_RestoreVoice_m297E80F5714FF9CA35D0396D4F6B01BDC7C92FBB (void);
// 0x000005ED System.Void UnityWebGLSpeechSynthesis.SpeechSynthesisUtils::RestoreVoice(System.String[],System.Int32&)
extern void SpeechSynthesisUtils_RestoreVoice_m3AFD16FBCC3EECEED184A1272A2EFF879BF88FA8 (void);
// 0x000005EE System.Void UnityWebGLSpeechSynthesis.SpeechSynthesisUtils::.ctor()
extern void SpeechSynthesisUtils__ctor_mCB48E8486A0C4DE8239B4DE311D202D5E4B6D4E0 (void);
// 0x000005EF System.Void UnityWebGLSpeechSynthesis.SpeechSynthesisUtils::.cctor()
extern void SpeechSynthesisUtils__cctor_mDE271808ACCE3AC77794BA0960094ECB6E0BAC28 (void);
// 0x000005F0 System.Void UnityWebGLSpeechSynthesis.SpeechSynthesisUtterance::.ctor()
extern void SpeechSynthesisUtterance__ctor_mFDC477D9C9F4621C62052D5378CEF9AF9ADE1CB0 (void);
// 0x000005F1 System.Void UnityWebGLSpeechSynthesis.Voice::.ctor()
extern void Voice__ctor_mA25ACC72E5B712E6A3D51C764A65B500BC44D7D1 (void);
// 0x000005F2 System.Void UnityWebGLSpeechSynthesis.VoiceResult::.ctor()
extern void VoiceResult__ctor_m7928C0A714CA6C607887D95771D8863B8C519953 (void);
// 0x000005F3 System.Void UnityWebGLSpeechSynthesis.WWWEditMode::.ctor(System.String)
extern void WWWEditMode__ctor_m7C7C2909B8F91A833D966AA588238A33182354D3 (void);
// 0x000005F4 System.Void UnityWebGLSpeechSynthesis.WWWEditMode::HandleBeginGetResponse(System.IAsyncResult)
extern void WWWEditMode_HandleBeginGetResponse_mF7F03D1407EFB3D4EFDA93F1465C3EBC61094D42 (void);
// 0x000005F5 System.Boolean UnityWebGLSpeechSynthesis.WWWEditMode::IsDone()
extern void WWWEditMode_IsDone_mA190AA90959B4F9BCBD22CC6131BD5155327C8F4 (void);
// 0x000005F6 System.String UnityWebGLSpeechSynthesis.WWWEditMode::GetError()
extern void WWWEditMode_GetError_m917FDD40D73F38B0FF28E4753DD26B727DF10079 (void);
// 0x000005F7 System.String UnityWebGLSpeechSynthesis.WWWEditMode::GetText()
extern void WWWEditMode_GetText_m27F240974F97E82D92FFA2D0DE0304F02591CF25 (void);
// 0x000005F8 System.Void UnityWebGLSpeechSynthesis.WWWEditMode::Dispose()
extern void WWWEditMode_Dispose_m3C11A0715D7E48C9938E17DF04B307CC1BD3A53B (void);
// 0x000005F9 System.Void UnityWebGLSpeechSynthesis.WWWPlayMode::.ctor(System.String)
extern void WWWPlayMode__ctor_m28FAC7243624E2410B378B46B29BC601ACC55AA8 (void);
// 0x000005FA System.Boolean UnityWebGLSpeechSynthesis.WWWPlayMode::IsDone()
extern void WWWPlayMode_IsDone_m43652044816DB354C86CA847F8CA224B7EED99FA (void);
// 0x000005FB System.String UnityWebGLSpeechSynthesis.WWWPlayMode::GetError()
extern void WWWPlayMode_GetError_m1B57A2BDE11EE5422FC393DCB8C7026281333FF1 (void);
// 0x000005FC System.String UnityWebGLSpeechSynthesis.WWWPlayMode::GetText()
extern void WWWPlayMode_GetText_m620FB72732ADFFF4466FCF0D1C0B02FB48252141 (void);
// 0x000005FD System.Void UnityWebGLSpeechSynthesis.WWWPlayMode::Dispose()
extern void WWWPlayMode_Dispose_mA5F46E794C93945FEC64055EBFBF08946B41C006 (void);
// 0x000005FE UnityWebGLSpeechSynthesis.WebGLSpeechSynthesisPlugin UnityWebGLSpeechSynthesis.WebGLSpeechSynthesisPlugin::GetInstance()
extern void WebGLSpeechSynthesisPlugin_GetInstance_mB76D5CD707E31CFAEDE26265FDDABE60E346721E (void);
// 0x000005FF System.Void UnityWebGLSpeechSynthesis.WebGLSpeechSynthesisPlugin::Awake()
extern void WebGLSpeechSynthesisPlugin_Awake_mC658261283083CDDDB089B72F5FB75888D87129F (void);
// 0x00000600 System.Collections.IEnumerator UnityWebGLSpeechSynthesis.WebGLSpeechSynthesisPlugin::ProxyOnEnd()
extern void WebGLSpeechSynthesisPlugin_ProxyOnEnd_m4C05BABE8475953BE0C7F45E9458ACAFC99B0144 (void);
// 0x00000601 System.Boolean UnityWebGLSpeechSynthesis.WebGLSpeechSynthesisPlugin::IsAvailable()
extern void WebGLSpeechSynthesisPlugin_IsAvailable_m40B918FD79FD8052856994E785700DEE42850D1D (void);
// 0x00000602 System.Void UnityWebGLSpeechSynthesis.WebGLSpeechSynthesisPlugin::CreateSpeechSynthesisUtterance(System.Action`1<UnityWebGLSpeechSynthesis.SpeechSynthesisUtterance>)
extern void WebGLSpeechSynthesisPlugin_CreateSpeechSynthesisUtterance_m0A2EA33249ED681A7F6387AC24A92B31D0246920 (void);
// 0x00000603 System.Void UnityWebGLSpeechSynthesis.WebGLSpeechSynthesisPlugin::Speak(UnityWebGLSpeechSynthesis.SpeechSynthesisUtterance)
extern void WebGLSpeechSynthesisPlugin_Speak_mBD7D119BB6372FD256FC6F0CA967EDDCAD7B7AE9 (void);
// 0x00000604 System.Void UnityWebGLSpeechSynthesis.WebGLSpeechSynthesisPlugin::Cancel()
extern void WebGLSpeechSynthesisPlugin_Cancel_mAF54BB415AADC04955A91912D11E53D4CE410E70 (void);
// 0x00000605 System.Void UnityWebGLSpeechSynthesis.WebGLSpeechSynthesisPlugin::GetVoices(System.Action`1<UnityWebGLSpeechSynthesis.VoiceResult>)
extern void WebGLSpeechSynthesisPlugin_GetVoices_m9921EC51AB98DA88673ECA146CC34C6EE449558D (void);
// 0x00000606 System.Void UnityWebGLSpeechSynthesis.WebGLSpeechSynthesisPlugin::SetPitch(UnityWebGLSpeechSynthesis.SpeechSynthesisUtterance,System.Single)
extern void WebGLSpeechSynthesisPlugin_SetPitch_m66B3D107675AE33D0C6E8D2951B4445EA745B8EB (void);
// 0x00000607 System.Void UnityWebGLSpeechSynthesis.WebGLSpeechSynthesisPlugin::SetRate(UnityWebGLSpeechSynthesis.SpeechSynthesisUtterance,System.Single)
extern void WebGLSpeechSynthesisPlugin_SetRate_mD4C3D4C637ED5DE6E17335665DA9D32E6459E105 (void);
// 0x00000608 System.Void UnityWebGLSpeechSynthesis.WebGLSpeechSynthesisPlugin::SetText(UnityWebGLSpeechSynthesis.SpeechSynthesisUtterance,System.String)
extern void WebGLSpeechSynthesisPlugin_SetText_mAE428BC902365D0D7DD8BAD344466EB3A12C644C (void);
// 0x00000609 System.Void UnityWebGLSpeechSynthesis.WebGLSpeechSynthesisPlugin::SetVoice(UnityWebGLSpeechSynthesis.SpeechSynthesisUtterance,UnityWebGLSpeechSynthesis.Voice)
extern void WebGLSpeechSynthesisPlugin_SetVoice_mB581C4C0EE0B28772D53020584360FC25BEE3EA4 (void);
// 0x0000060A System.Void UnityWebGLSpeechSynthesis.WebGLSpeechSynthesisPlugin::SetVolume(UnityWebGLSpeechSynthesis.SpeechSynthesisUtterance,System.Single)
extern void WebGLSpeechSynthesisPlugin_SetVolume_mBA7FA80B657D6175CC602FB3D9DEFDA9C135197D (void);
// 0x0000060B System.Void UnityWebGLSpeechSynthesis.WebGLSpeechSynthesisPlugin::ManagementCloseBrowserTab()
extern void WebGLSpeechSynthesisPlugin_ManagementCloseBrowserTab_m4DC92F5F6B2584095047B97DCF212FE0AFFB625E (void);
// 0x0000060C System.Void UnityWebGLSpeechSynthesis.WebGLSpeechSynthesisPlugin::ManagementCloseProxy()
extern void WebGLSpeechSynthesisPlugin_ManagementCloseProxy_m8E211226659F25595EF8E03E64E7C13B3E1B4478 (void);
// 0x0000060D System.Void UnityWebGLSpeechSynthesis.WebGLSpeechSynthesisPlugin::ManagementLaunchProxy()
extern void WebGLSpeechSynthesisPlugin_ManagementLaunchProxy_mFFC7670DF7658F39526F7AC5EFDF3390AFDC4F8F (void);
// 0x0000060E System.Void UnityWebGLSpeechSynthesis.WebGLSpeechSynthesisPlugin::ManagementOpenBrowserTab()
extern void WebGLSpeechSynthesisPlugin_ManagementOpenBrowserTab_mF05FC671A42E0C0A711BB3392B1FB4DD494811C1 (void);
// 0x0000060F System.Void UnityWebGLSpeechSynthesis.WebGLSpeechSynthesisPlugin::ManagementSetProxyPort(System.Int32)
extern void WebGLSpeechSynthesisPlugin_ManagementSetProxyPort_m6597EEB35D4C6E2C79C095782DAB3BE83A1B54A7 (void);
// 0x00000610 System.Void UnityWebGLSpeechSynthesis.WebGLSpeechSynthesisPlugin::.ctor()
extern void WebGLSpeechSynthesisPlugin__ctor_m3CD65A2EE5289FAA6BD067D9ED181420F68268B9 (void);
// 0x00000611 System.Void UnityWebGLSpeechSynthesis.WebGLSpeechSynthesisPlugin::.cctor()
extern void WebGLSpeechSynthesisPlugin__cctor_m2989B07DD89844AF59E1CB0E209CE32C24FAA0C4 (void);
// 0x00000612 System.Void UnityWebGLSpeechSynthesis.WebGLSpeechSynthesisPlugin/<ProxyOnEnd>d__3::.ctor(System.Int32)
extern void U3CProxyOnEndU3Ed__3__ctor_mA20A9FD5B9CA14D3E246E77F2E753B84137DBA2C (void);
// 0x00000613 System.Void UnityWebGLSpeechSynthesis.WebGLSpeechSynthesisPlugin/<ProxyOnEnd>d__3::System.IDisposable.Dispose()
extern void U3CProxyOnEndU3Ed__3_System_IDisposable_Dispose_mB6A59AC6CCFBEACE049F002037CA85CDD8C6C1A1 (void);
// 0x00000614 System.Boolean UnityWebGLSpeechSynthesis.WebGLSpeechSynthesisPlugin/<ProxyOnEnd>d__3::MoveNext()
extern void U3CProxyOnEndU3Ed__3_MoveNext_mC75D2C44B36B522D9AED0BBA156E820D518D08BC (void);
// 0x00000615 System.Object UnityWebGLSpeechSynthesis.WebGLSpeechSynthesisPlugin/<ProxyOnEnd>d__3::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CProxyOnEndU3Ed__3_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m23180814A7DB3E1CE1C1068FF09F1BB43810BC9C (void);
// 0x00000616 System.Void UnityWebGLSpeechSynthesis.WebGLSpeechSynthesisPlugin/<ProxyOnEnd>d__3::System.Collections.IEnumerator.Reset()
extern void U3CProxyOnEndU3Ed__3_System_Collections_IEnumerator_Reset_m5214C4EBB3412E7401F9943FD182087458B09AED (void);
// 0x00000617 System.Object UnityWebGLSpeechSynthesis.WebGLSpeechSynthesisPlugin/<ProxyOnEnd>d__3::System.Collections.IEnumerator.get_Current()
extern void U3CProxyOnEndU3Ed__3_System_Collections_IEnumerator_get_Current_m1A3AC4C48B1827D4A820083059E5C907FE3395AB (void);
// 0x00000618 System.Void UnityWebGLSpeechDetection.BaseSpeechDetectionPlugin::AddListenerOnDetectionResult(UnityWebGLSpeechDetection.BaseSpeechDetectionPlugin/DelegateHandleDetectionResult)
extern void BaseSpeechDetectionPlugin_AddListenerOnDetectionResult_m42E61B495DFEABD3AD6F1D70AAACB3AC5330F864 (void);
// 0x00000619 System.Void UnityWebGLSpeechDetection.BaseSpeechDetectionPlugin::RemoveListenerOnDetectionResult(UnityWebGLSpeechDetection.BaseSpeechDetectionPlugin/DelegateHandleDetectionResult)
extern void BaseSpeechDetectionPlugin_RemoveListenerOnDetectionResult_m2B1D66D423ABBC6BE2FA854955DEF563EC63C988 (void);
// 0x0000061A System.Void UnityWebGLSpeechDetection.BaseSpeechDetectionPlugin::AddListenerOnLanguageChanged(UnityWebGLSpeechDetection.BaseSpeechDetectionPlugin/DelegateHandleLanguageChanged)
extern void BaseSpeechDetectionPlugin_AddListenerOnLanguageChanged_mC14E94C923CA77139B35DCC39C68412C19549C98 (void);
// 0x0000061B System.Void UnityWebGLSpeechDetection.BaseSpeechDetectionPlugin::RemoveListenerOnLanguageChanged(UnityWebGLSpeechDetection.BaseSpeechDetectionPlugin/DelegateHandleLanguageChanged)
extern void BaseSpeechDetectionPlugin_RemoveListenerOnLanguageChanged_mA705570825BE11E33CA90A1A23FC50FEBC6E3268 (void);
// 0x0000061C System.Void UnityWebGLSpeechDetection.BaseSpeechDetectionPlugin::Invoke(UnityWebGLSpeechDetection.DetectionResult)
extern void BaseSpeechDetectionPlugin_Invoke_m9C67F6EB6DA8D87804D0715827F5F2EBB9DC70F2 (void);
// 0x0000061D System.Void UnityWebGLSpeechDetection.BaseSpeechDetectionPlugin::Invoke(UnityWebGLSpeechDetection.LanguageChangedResult)
extern void BaseSpeechDetectionPlugin_Invoke_mD6A22D8913372F39C9E8E19718E8FBF734C630E4 (void);
// 0x0000061E System.Void UnityWebGLSpeechDetection.BaseSpeechDetectionPlugin::.ctor()
extern void BaseSpeechDetectionPlugin__ctor_mFFEE525772A6B2C26BD34768B4D50E068F844749 (void);
// 0x0000061F System.Void UnityWebGLSpeechDetection.BaseSpeechDetectionPlugin::.cctor()
extern void BaseSpeechDetectionPlugin__cctor_m913107AE7AEE3347521A46412B972BC2383744BD (void);
// 0x00000620 System.Void UnityWebGLSpeechDetection.BaseSpeechDetectionPlugin/DelegateHandleDetectionResult::.ctor(System.Object,System.IntPtr)
extern void DelegateHandleDetectionResult__ctor_m4C86F1A7834CED4272DE63D92B66BBA0EB0488A9 (void);
// 0x00000621 System.Boolean UnityWebGLSpeechDetection.BaseSpeechDetectionPlugin/DelegateHandleDetectionResult::Invoke(UnityWebGLSpeechDetection.DetectionResult)
extern void DelegateHandleDetectionResult_Invoke_mAA07164CA25FFE631CCB089EEC6E374E2660913C (void);
// 0x00000622 System.IAsyncResult UnityWebGLSpeechDetection.BaseSpeechDetectionPlugin/DelegateHandleDetectionResult::BeginInvoke(UnityWebGLSpeechDetection.DetectionResult,System.AsyncCallback,System.Object)
extern void DelegateHandleDetectionResult_BeginInvoke_m2C9218C85D1A917D5861402FE8B3CF0B5015E8F9 (void);
// 0x00000623 System.Boolean UnityWebGLSpeechDetection.BaseSpeechDetectionPlugin/DelegateHandleDetectionResult::EndInvoke(System.IAsyncResult)
extern void DelegateHandleDetectionResult_EndInvoke_m25EB0514D452F3F9A8A5970124C8FCFA1E5D555A (void);
// 0x00000624 System.Void UnityWebGLSpeechDetection.BaseSpeechDetectionPlugin/DelegateHandleLanguageChanged::.ctor(System.Object,System.IntPtr)
extern void DelegateHandleLanguageChanged__ctor_m6499688350D63108B4AF54E31E7A6803A35EAAA5 (void);
// 0x00000625 System.Void UnityWebGLSpeechDetection.BaseSpeechDetectionPlugin/DelegateHandleLanguageChanged::Invoke(UnityWebGLSpeechDetection.LanguageChangedResult)
extern void DelegateHandleLanguageChanged_Invoke_mDF6A5A08DBC352DA6566D19570BC90F63BA15595 (void);
// 0x00000626 System.IAsyncResult UnityWebGLSpeechDetection.BaseSpeechDetectionPlugin/DelegateHandleLanguageChanged::BeginInvoke(UnityWebGLSpeechDetection.LanguageChangedResult,System.AsyncCallback,System.Object)
extern void DelegateHandleLanguageChanged_BeginInvoke_m8D74C95B84655435CB02DF3C3C322D408E00C039 (void);
// 0x00000627 System.Void UnityWebGLSpeechDetection.BaseSpeechDetectionPlugin/DelegateHandleLanguageChanged::EndInvoke(System.IAsyncResult)
extern void DelegateHandleLanguageChanged_EndInvoke_m099E443D595FF4788A4E5EA516C4D9974C91257D (void);
// 0x00000628 System.Void UnityWebGLSpeechDetection.DetectionResult::.ctor()
extern void DetectionResult__ctor_m5DC7429E1E9CDE34C352247FEEED76744948EB45 (void);
// 0x00000629 System.Void UnityWebGLSpeechDetection.Dialect::.ctor()
extern void Dialect__ctor_m3FD26A2B2644B4C3CBA0B4C41E0374F9AF72DE9B (void);
// 0x0000062A System.Void UnityWebGLSpeechDetection.EditorProxySpeechDetectionPlugin::SafeStartCoroutine(System.String,System.Collections.IEnumerator)
extern void EditorProxySpeechDetectionPlugin_SafeStartCoroutine_mFE221995227718C4AEC36CB28C6EFD550214A7C1 (void);
// 0x0000062B UnityWebGLSpeechDetection.IWWW UnityWebGLSpeechDetection.EditorProxySpeechDetectionPlugin::CreateWWW(System.String)
extern void EditorProxySpeechDetectionPlugin_CreateWWW_m30A47FA73FF2066B97CDE646495D1A2582F9775D (void);
// 0x0000062C System.Boolean UnityWebGLSpeechDetection.EditorProxySpeechDetectionPlugin::IsEnabled()
extern void EditorProxySpeechDetectionPlugin_IsEnabled_m1B8DFCD1D13F3ADD6C6555E16D78C7440E456E9D (void);
// 0x0000062D System.Void UnityWebGLSpeechDetection.EditorProxySpeechDetectionPlugin::CleanUp()
extern void EditorProxySpeechDetectionPlugin_CleanUp_mBDF56D27EAF3CB6CB1783C96F43F7F03FE73AE67 (void);
// 0x0000062E System.Void UnityWebGLSpeechDetection.EditorProxySpeechDetectionPlugin::SetEnabled(System.Boolean)
extern void EditorProxySpeechDetectionPlugin_SetEnabled_mBEF5480A07F2F1FDAC4CF658C2D88EC80B723442 (void);
// 0x0000062F UnityWebGLSpeechDetection.EditorProxySpeechDetectionPlugin UnityWebGLSpeechDetection.EditorProxySpeechDetectionPlugin::GetInstance()
extern void EditorProxySpeechDetectionPlugin_GetInstance_m0DCB70A51416E7D49917770D68BED1E47936501E (void);
// 0x00000630 System.Void UnityWebGLSpeechDetection.EditorProxySpeechDetectionPlugin::StartEditorUpdates()
extern void EditorProxySpeechDetectionPlugin_StartEditorUpdates_mBCDE2EB79BA19FD752C899FAD38D1150F5CE9B26 (void);
// 0x00000631 System.Void UnityWebGLSpeechDetection.EditorProxySpeechDetectionPlugin::StopEditorUpdates()
extern void EditorProxySpeechDetectionPlugin_StopEditorUpdates_m81A1494C1C8348D3EB8C490A86BCA51B2732B04F (void);
// 0x00000632 System.Void UnityWebGLSpeechDetection.EditorProxySpeechDetectionPlugin::.ctor()
extern void EditorProxySpeechDetectionPlugin__ctor_m970AAF0F5792051F1A777BEB6EA912171A900759 (void);
// 0x00000633 System.Void UnityWebGLSpeechDetection.Example01Dictation::Awake()
extern void Example01Dictation_Awake_mD2BEC27FA16FBABEAACD6422C948E0292117C423 (void);
// 0x00000634 System.Collections.IEnumerator UnityWebGLSpeechDetection.Example01Dictation::Start()
extern void Example01Dictation_Start_m2DD9CDBF6C2F5FDB3F69125D1188C11D1CF28ABE (void);
// 0x00000635 System.Boolean UnityWebGLSpeechDetection.Example01Dictation::HandleDetectionResult(UnityWebGLSpeechDetection.DetectionResult)
extern void Example01Dictation_HandleDetectionResult_mF6F15C2BDC86BE68AA06CCC7F9DC68E16A94165A (void);
// 0x00000636 System.Void UnityWebGLSpeechDetection.Example01Dictation::.ctor()
extern void Example01Dictation__ctor_m9D3675019889A753C9238CA009EC9A9319F8A2D3 (void);
// 0x00000637 System.Void UnityWebGLSpeechDetection.Example01Dictation::<Start>b__10_0(UnityWebGLSpeechDetection.LanguageResult)
extern void Example01Dictation_U3CStartU3Eb__10_0_m324C5EB1646129DCE15C60593D843CF0CEEFBA00 (void);
// 0x00000638 System.Void UnityWebGLSpeechDetection.Example01Dictation::<Start>b__10_1(System.Int32)
extern void Example01Dictation_U3CStartU3Eb__10_1_m4D6C29A76FA1387309A88B31FEB428A82B256190 (void);
// 0x00000639 System.Void UnityWebGLSpeechDetection.Example01Dictation::<Start>b__10_2(System.Int32)
extern void Example01Dictation_U3CStartU3Eb__10_2_mA8B03413D355942B8391D9237DCDE718BEB8DCD6 (void);
// 0x0000063A System.Void UnityWebGLSpeechDetection.Example01Dictation/<Start>d__10::.ctor(System.Int32)
extern void U3CStartU3Ed__10__ctor_m9A5ADA41AD9640CCCF47CF94DF79A682A6F3B310 (void);
// 0x0000063B System.Void UnityWebGLSpeechDetection.Example01Dictation/<Start>d__10::System.IDisposable.Dispose()
extern void U3CStartU3Ed__10_System_IDisposable_Dispose_m23F5925258986EB4AFC343608A32AF127CD7640B (void);
// 0x0000063C System.Boolean UnityWebGLSpeechDetection.Example01Dictation/<Start>d__10::MoveNext()
extern void U3CStartU3Ed__10_MoveNext_m4A59099BF6E78F4666138CB26E5183C39430CC71 (void);
// 0x0000063D System.Object UnityWebGLSpeechDetection.Example01Dictation/<Start>d__10::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CStartU3Ed__10_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m20845EAD2D482F70B9F51BCF13959349EFCE3AF2 (void);
// 0x0000063E System.Void UnityWebGLSpeechDetection.Example01Dictation/<Start>d__10::System.Collections.IEnumerator.Reset()
extern void U3CStartU3Ed__10_System_Collections_IEnumerator_Reset_mCAC9839D3729DBAFC1C7A33DEFCED8AE1E914CF8 (void);
// 0x0000063F System.Object UnityWebGLSpeechDetection.Example01Dictation/<Start>d__10::System.Collections.IEnumerator.get_Current()
extern void U3CStartU3Ed__10_System_Collections_IEnumerator_get_Current_m961FFD432D91B04C6F056699AE4A60F11B7CDBBA (void);
// 0x00000640 System.Void UnityWebGLSpeechDetection.Example02SpeechCommands::Awake()
extern void Example02SpeechCommands_Awake_mB03CB2D341E0EC4C7B81020C205299F9BC53FA38 (void);
// 0x00000641 System.Collections.IEnumerator UnityWebGLSpeechDetection.Example02SpeechCommands::Start()
extern void Example02SpeechCommands_Start_m3B392D38DE9534CB1DFDDB6ED8A309B09C10CAD1 (void);
// 0x00000642 System.Boolean UnityWebGLSpeechDetection.Example02SpeechCommands::HandleDetectionResult(UnityWebGLSpeechDetection.DetectionResult)
extern void Example02SpeechCommands_HandleDetectionResult_mC0AE593BCF82C93EA94B84D80EFF3D5F403222BD (void);
// 0x00000643 System.Void UnityWebGLSpeechDetection.Example02SpeechCommands::.ctor()
extern void Example02SpeechCommands__ctor_mC85A7B2ED8EACA682A8D5160F84AAB8A8805C612 (void);
// 0x00000644 System.Void UnityWebGLSpeechDetection.Example02SpeechCommands::<Start>b__9_0(UnityWebGLSpeechDetection.LanguageResult)
extern void Example02SpeechCommands_U3CStartU3Eb__9_0_mAF3718081A86C5DEE426CE29056C45F5D372FDA7 (void);
// 0x00000645 System.Void UnityWebGLSpeechDetection.Example02SpeechCommands::<Start>b__9_1(System.Int32)
extern void Example02SpeechCommands_U3CStartU3Eb__9_1_mCDAAE9C0F8DE0861443E9766CB560B80D6080D9E (void);
// 0x00000646 System.Void UnityWebGLSpeechDetection.Example02SpeechCommands::<Start>b__9_2(System.Int32)
extern void Example02SpeechCommands_U3CStartU3Eb__9_2_m723537E14FC6C80D2737CC31D7FE370B62BBA08B (void);
// 0x00000647 System.Void UnityWebGLSpeechDetection.Example02SpeechCommands/<Start>d__9::.ctor(System.Int32)
extern void U3CStartU3Ed__9__ctor_m1DCC4242CF1D0D731B6126E7F5B708942A1072DB (void);
// 0x00000648 System.Void UnityWebGLSpeechDetection.Example02SpeechCommands/<Start>d__9::System.IDisposable.Dispose()
extern void U3CStartU3Ed__9_System_IDisposable_Dispose_m5EB8C4ADEBBD2ED5CCBE2F328282892CB6884DF4 (void);
// 0x00000649 System.Boolean UnityWebGLSpeechDetection.Example02SpeechCommands/<Start>d__9::MoveNext()
extern void U3CStartU3Ed__9_MoveNext_mC44C0A80540663842F9445FE32F1F3A4969080F8 (void);
// 0x0000064A System.Object UnityWebGLSpeechDetection.Example02SpeechCommands/<Start>d__9::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CStartU3Ed__9_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m43876813CB26CDF2721AF751DB3E93BB453730EC (void);
// 0x0000064B System.Void UnityWebGLSpeechDetection.Example02SpeechCommands/<Start>d__9::System.Collections.IEnumerator.Reset()
extern void U3CStartU3Ed__9_System_Collections_IEnumerator_Reset_m7617645E4D536DB41476515F5F849965F8335D85 (void);
// 0x0000064C System.Object UnityWebGLSpeechDetection.Example02SpeechCommands/<Start>d__9::System.Collections.IEnumerator.get_Current()
extern void U3CStartU3Ed__9_System_Collections_IEnumerator_get_Current_m6B1E12CCB938CD7A198A113D6EE2CC277AA32A67 (void);
// 0x0000064D System.Collections.IEnumerator UnityWebGLSpeechDetection.Example02Word::DoHighlight()
extern void Example02Word_DoHighlight_m670F10C8CF6785E291095095AEEDF3C8A30E285C (void);
// 0x0000064E System.Void UnityWebGLSpeechDetection.Example02Word::Highlight()
extern void Example02Word_Highlight_mBEB6545949E897507A55751CF96B839A437BBB0C (void);
// 0x0000064F System.Void UnityWebGLSpeechDetection.Example02Word::.ctor()
extern void Example02Word__ctor_mA989753AC23D5F19AA4D4B5C4E19DD78EF33B10F (void);
// 0x00000650 System.Void UnityWebGLSpeechDetection.Example02Word::.cctor()
extern void Example02Word__cctor_m8A3770B8AF8717525F4CDD18992690C15C2F00CF (void);
// 0x00000651 System.Void UnityWebGLSpeechDetection.Example02Word/<DoHighlight>d__5::.ctor(System.Int32)
extern void U3CDoHighlightU3Ed__5__ctor_mD5AE312E764E3292D8DD5D158217B95A9CF70565 (void);
// 0x00000652 System.Void UnityWebGLSpeechDetection.Example02Word/<DoHighlight>d__5::System.IDisposable.Dispose()
extern void U3CDoHighlightU3Ed__5_System_IDisposable_Dispose_m2F8B3A79BFBE378256C8DB81C7E72C2793E1E7B6 (void);
// 0x00000653 System.Boolean UnityWebGLSpeechDetection.Example02Word/<DoHighlight>d__5::MoveNext()
extern void U3CDoHighlightU3Ed__5_MoveNext_m5F0734B423DD8323E878A55B7A2BE89B7C4983CE (void);
// 0x00000654 System.Object UnityWebGLSpeechDetection.Example02Word/<DoHighlight>d__5::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CDoHighlightU3Ed__5_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m3E50ECEB225715936D40ED6CFAA54EF88A73A6E5 (void);
// 0x00000655 System.Void UnityWebGLSpeechDetection.Example02Word/<DoHighlight>d__5::System.Collections.IEnumerator.Reset()
extern void U3CDoHighlightU3Ed__5_System_Collections_IEnumerator_Reset_m58923D5EDD1686E38EACEBE6919E2C3818A069DD (void);
// 0x00000656 System.Object UnityWebGLSpeechDetection.Example02Word/<DoHighlight>d__5::System.Collections.IEnumerator.get_Current()
extern void U3CDoHighlightU3Ed__5_System_Collections_IEnumerator_get_Current_m37093F8D22C85DD3FC980F42F64A16F25EB20E61 (void);
// 0x00000657 System.Void UnityWebGLSpeechDetection.Example03ProxyCommands::Awake()
extern void Example03ProxyCommands_Awake_m8AA631439B136C53DDD8C700D3AE31B434A84266 (void);
// 0x00000658 System.Collections.IEnumerator UnityWebGLSpeechDetection.Example03ProxyCommands::Start()
extern void Example03ProxyCommands_Start_m75C3CACF576092A6AEDAB46949555EBF3BC5CE3A (void);
// 0x00000659 System.Boolean UnityWebGLSpeechDetection.Example03ProxyCommands::HandleDetectionResult(UnityWebGLSpeechDetection.DetectionResult)
extern void Example03ProxyCommands_HandleDetectionResult_mD0FBD9A210EE8B7C2A10E183F6CAD7FD3600398C (void);
// 0x0000065A System.Void UnityWebGLSpeechDetection.Example03ProxyCommands::.ctor()
extern void Example03ProxyCommands__ctor_mE8C706E926EFB5979D9C5BA341E26953D415D867 (void);
// 0x0000065B System.Void UnityWebGLSpeechDetection.Example03ProxyCommands::<Start>b__9_0(UnityWebGLSpeechDetection.LanguageResult)
extern void Example03ProxyCommands_U3CStartU3Eb__9_0_m1A9B4FAC6449AC5A6B6F60CBA33E6A008B14B5D0 (void);
// 0x0000065C System.Void UnityWebGLSpeechDetection.Example03ProxyCommands::<Start>b__9_1(System.Int32)
extern void Example03ProxyCommands_U3CStartU3Eb__9_1_m59BF70008830BB6F5952C527F5E824A6609DE99A (void);
// 0x0000065D System.Void UnityWebGLSpeechDetection.Example03ProxyCommands::<Start>b__9_2(System.Int32)
extern void Example03ProxyCommands_U3CStartU3Eb__9_2_m80788C4BD22A86C43951D086E1CA8E2D6B19C09B (void);
// 0x0000065E System.Void UnityWebGLSpeechDetection.Example03ProxyCommands/<Start>d__9::.ctor(System.Int32)
extern void U3CStartU3Ed__9__ctor_m3F78AF5ED20089723D623C905AB064288A96CD0E (void);
// 0x0000065F System.Void UnityWebGLSpeechDetection.Example03ProxyCommands/<Start>d__9::System.IDisposable.Dispose()
extern void U3CStartU3Ed__9_System_IDisposable_Dispose_m1C60169E1C0BB3885A9C16EF36572BA72860FA87 (void);
// 0x00000660 System.Boolean UnityWebGLSpeechDetection.Example03ProxyCommands/<Start>d__9::MoveNext()
extern void U3CStartU3Ed__9_MoveNext_mE7C788145A84FA6363B0186199294C8FA34497AC (void);
// 0x00000661 System.Object UnityWebGLSpeechDetection.Example03ProxyCommands/<Start>d__9::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CStartU3Ed__9_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m9D4EE49E43C98748228A6554736CAA494FFAF419 (void);
// 0x00000662 System.Void UnityWebGLSpeechDetection.Example03ProxyCommands/<Start>d__9::System.Collections.IEnumerator.Reset()
extern void U3CStartU3Ed__9_System_Collections_IEnumerator_Reset_mA96F9B0685F82F2F9B24923E4B4C58B943E84D55 (void);
// 0x00000663 System.Object UnityWebGLSpeechDetection.Example03ProxyCommands/<Start>d__9::System.Collections.IEnumerator.get_Current()
extern void U3CStartU3Ed__9_System_Collections_IEnumerator_get_Current_m760831DCCDB96CF1185E0FBE602E4286E80C456F (void);
// 0x00000664 System.Void UnityWebGLSpeechDetection.Example04ProxyDictation::Awake()
extern void Example04ProxyDictation_Awake_m57676EA57D43985745FACE8378B5BEF448EE1274 (void);
// 0x00000665 System.Collections.IEnumerator UnityWebGLSpeechDetection.Example04ProxyDictation::Start()
extern void Example04ProxyDictation_Start_mA1D2D0C1D6CDD82A052581989B6078A6E282C465 (void);
// 0x00000666 System.Boolean UnityWebGLSpeechDetection.Example04ProxyDictation::HandleDetectionResult(UnityWebGLSpeechDetection.DetectionResult)
extern void Example04ProxyDictation_HandleDetectionResult_m37F71371773FA8A892DC54B7EB333C92624D4E5C (void);
// 0x00000667 System.Void UnityWebGLSpeechDetection.Example04ProxyDictation::.ctor()
extern void Example04ProxyDictation__ctor_m142B76CFC9D5E2C47E2BC23EE9F1224129C6A2E0 (void);
// 0x00000668 System.Void UnityWebGLSpeechDetection.Example04ProxyDictation::<Start>b__10_0(UnityWebGLSpeechDetection.LanguageResult)
extern void Example04ProxyDictation_U3CStartU3Eb__10_0_m135A7C20424A1634400FCF9A0B2F98FA8457F588 (void);
// 0x00000669 System.Void UnityWebGLSpeechDetection.Example04ProxyDictation::<Start>b__10_1(System.Int32)
extern void Example04ProxyDictation_U3CStartU3Eb__10_1_mCA1C4A134D714558B80BC0D4AA7CB81B1C901EE1 (void);
// 0x0000066A System.Void UnityWebGLSpeechDetection.Example04ProxyDictation::<Start>b__10_2(System.Int32)
extern void Example04ProxyDictation_U3CStartU3Eb__10_2_m7405692423C6979639B7A85D1643E52FC884B703 (void);
// 0x0000066B System.Void UnityWebGLSpeechDetection.Example04ProxyDictation/<Start>d__10::.ctor(System.Int32)
extern void U3CStartU3Ed__10__ctor_mD08E65BE8117B1AC5F05D4FE68ED1115D16B41AD (void);
// 0x0000066C System.Void UnityWebGLSpeechDetection.Example04ProxyDictation/<Start>d__10::System.IDisposable.Dispose()
extern void U3CStartU3Ed__10_System_IDisposable_Dispose_m2E8EDC3CFF0CBE8EC3721E3E2C73DA2B36DB2A81 (void);
// 0x0000066D System.Boolean UnityWebGLSpeechDetection.Example04ProxyDictation/<Start>d__10::MoveNext()
extern void U3CStartU3Ed__10_MoveNext_mD989784207C7831F28429F79AD1BD3DFC3971588 (void);
// 0x0000066E System.Object UnityWebGLSpeechDetection.Example04ProxyDictation/<Start>d__10::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CStartU3Ed__10_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mC8E450D1D2D986E4B2BB9CC67E3EB6C6AA0D2C2E (void);
// 0x0000066F System.Void UnityWebGLSpeechDetection.Example04ProxyDictation/<Start>d__10::System.Collections.IEnumerator.Reset()
extern void U3CStartU3Ed__10_System_Collections_IEnumerator_Reset_m2E08DC86E082086DAEC6DFDFF85580F53EDCE9A6 (void);
// 0x00000670 System.Object UnityWebGLSpeechDetection.Example04ProxyDictation/<Start>d__10::System.Collections.IEnumerator.get_Current()
extern void U3CStartU3Ed__10_System_Collections_IEnumerator_get_Current_mA280C39A17548B1911ACD6D3A44D3053E613B0FC (void);
// 0x00000671 System.Collections.IEnumerator UnityWebGLSpeechDetection.Example05ProxyManagement::Start()
extern void Example05ProxyManagement_Start_m918205768DD763A8F73CB618156AA6DDC9BAEC6A (void);
// 0x00000672 System.Void UnityWebGLSpeechDetection.Example05ProxyManagement::.ctor()
extern void Example05ProxyManagement__ctor_mDA0D54E30190108ACEEADE32483593F10DE79A84 (void);
// 0x00000673 System.Void UnityWebGLSpeechDetection.Example05ProxyManagement::<Start>b__7_0()
extern void Example05ProxyManagement_U3CStartU3Eb__7_0_m4E1A6040A0C7A2098FAB777BAC252F695DFECAAA (void);
// 0x00000674 System.Void UnityWebGLSpeechDetection.Example05ProxyManagement::<Start>b__7_1()
extern void Example05ProxyManagement_U3CStartU3Eb__7_1_m0B63C58301AF6B06F1C7C41B45D1F69D58A79644 (void);
// 0x00000675 System.Void UnityWebGLSpeechDetection.Example05ProxyManagement::<Start>b__7_2()
extern void Example05ProxyManagement_U3CStartU3Eb__7_2_m969A65E0BCFC131DDD819D80D2B9868F4AF2FE8A (void);
// 0x00000676 System.Void UnityWebGLSpeechDetection.Example05ProxyManagement::<Start>b__7_3()
extern void Example05ProxyManagement_U3CStartU3Eb__7_3_m0679A42A30B14C905B811C8AC0290ED3FA95AE3C (void);
// 0x00000677 System.Void UnityWebGLSpeechDetection.Example05ProxyManagement::<Start>b__7_4()
extern void Example05ProxyManagement_U3CStartU3Eb__7_4_m623F6738E08C72416FF70B5E85F218BE31768A3A (void);
// 0x00000678 System.Void UnityWebGLSpeechDetection.Example05ProxyManagement/<Start>d__7::.ctor(System.Int32)
extern void U3CStartU3Ed__7__ctor_mFFB60DBC0F017DF05F9FA1003C2B2738FBCFA8FC (void);
// 0x00000679 System.Void UnityWebGLSpeechDetection.Example05ProxyManagement/<Start>d__7::System.IDisposable.Dispose()
extern void U3CStartU3Ed__7_System_IDisposable_Dispose_m05D77DC058E9785F795D27EB7E08BABCB8EEAB0F (void);
// 0x0000067A System.Boolean UnityWebGLSpeechDetection.Example05ProxyManagement/<Start>d__7::MoveNext()
extern void U3CStartU3Ed__7_MoveNext_m36294F98198D42D4DEA6FEA798CED9DC2BCBD4A0 (void);
// 0x0000067B System.Object UnityWebGLSpeechDetection.Example05ProxyManagement/<Start>d__7::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CStartU3Ed__7_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m3A38D5FDBB5F5E32BD913E93B26A37B8B50D7313 (void);
// 0x0000067C System.Void UnityWebGLSpeechDetection.Example05ProxyManagement/<Start>d__7::System.Collections.IEnumerator.Reset()
extern void U3CStartU3Ed__7_System_Collections_IEnumerator_Reset_mB7C38F73E9B1A4D4DD842876612C0EC3A5793742 (void);
// 0x0000067D System.Object UnityWebGLSpeechDetection.Example05ProxyManagement/<Start>d__7::System.Collections.IEnumerator.get_Current()
extern void U3CStartU3Ed__7_System_Collections_IEnumerator_get_Current_m701771E3243E1E913ACEBC2E8795C89ACC649668 (void);
// 0x0000067E System.Collections.IEnumerator UnityWebGLSpeechDetection.Example08NoGUIDictation::Start()
extern void Example08NoGUIDictation_Start_m78D8A7861FB8F439E8E757AFB43C9CE12A8DE22D (void);
// 0x0000067F System.Boolean UnityWebGLSpeechDetection.Example08NoGUIDictation::HandleDetectionResult(UnityWebGLSpeechDetection.DetectionResult)
extern void Example08NoGUIDictation_HandleDetectionResult_mD482872E11AFF33EBE2190E66C8B7D76DAC16718 (void);
// 0x00000680 System.Void UnityWebGLSpeechDetection.Example08NoGUIDictation::.ctor()
extern void Example08NoGUIDictation__ctor_m710A8510CB65A42B23BDE09A3A7C1D4E1A4BE21F (void);
// 0x00000681 System.Void UnityWebGLSpeechDetection.Example08NoGUIDictation/<Start>d__1::.ctor(System.Int32)
extern void U3CStartU3Ed__1__ctor_mE7E901F45124A48C59536990AA73C30B675E639B (void);
// 0x00000682 System.Void UnityWebGLSpeechDetection.Example08NoGUIDictation/<Start>d__1::System.IDisposable.Dispose()
extern void U3CStartU3Ed__1_System_IDisposable_Dispose_mD105462F2F676E0DBAA2F81DBE280BE7AA7FE238 (void);
// 0x00000683 System.Boolean UnityWebGLSpeechDetection.Example08NoGUIDictation/<Start>d__1::MoveNext()
extern void U3CStartU3Ed__1_MoveNext_m6C25221981C8D4FB4E5A9B4A43495303B32817EC (void);
// 0x00000684 System.Object UnityWebGLSpeechDetection.Example08NoGUIDictation/<Start>d__1::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CStartU3Ed__1_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mFC700BEEB25737D16E6524C6E55100390A7C94C6 (void);
// 0x00000685 System.Void UnityWebGLSpeechDetection.Example08NoGUIDictation/<Start>d__1::System.Collections.IEnumerator.Reset()
extern void U3CStartU3Ed__1_System_Collections_IEnumerator_Reset_mA53FBDA94F0F3114DCEE4EFE51DD76112B0F2D4C (void);
// 0x00000686 System.Object UnityWebGLSpeechDetection.Example08NoGUIDictation/<Start>d__1::System.Collections.IEnumerator.get_Current()
extern void U3CStartU3Ed__1_System_Collections_IEnumerator_get_Current_mCE1ADD01EFF09234ED24546244C7B07E4D5B7206 (void);
// 0x00000687 System.Collections.IEnumerator UnityWebGLSpeechDetection.Example09NoGUISpeechCommands::Start()
extern void Example09NoGUISpeechCommands_Start_m67230CF71BF7306EE666D93170BD0854B411E556 (void);
// 0x00000688 System.Boolean UnityWebGLSpeechDetection.Example09NoGUISpeechCommands::HandleDetectionResult(UnityWebGLSpeechDetection.DetectionResult)
extern void Example09NoGUISpeechCommands_HandleDetectionResult_m0A8DE1839CAE706CB9980688050AADCDDB24D70C (void);
// 0x00000689 System.Void UnityWebGLSpeechDetection.Example09NoGUISpeechCommands::.ctor()
extern void Example09NoGUISpeechCommands__ctor_mD5278A63D83791067114DE72431B4D98BC76CDA5 (void);
// 0x0000068A System.Void UnityWebGLSpeechDetection.Example09NoGUISpeechCommands/<Start>d__2::.ctor(System.Int32)
extern void U3CStartU3Ed__2__ctor_m960857B297E5A747E66E659907688D23C5E3A9A0 (void);
// 0x0000068B System.Void UnityWebGLSpeechDetection.Example09NoGUISpeechCommands/<Start>d__2::System.IDisposable.Dispose()
extern void U3CStartU3Ed__2_System_IDisposable_Dispose_m205623C92E170CE080D885A81AE3575797ED9EB8 (void);
// 0x0000068C System.Boolean UnityWebGLSpeechDetection.Example09NoGUISpeechCommands/<Start>d__2::MoveNext()
extern void U3CStartU3Ed__2_MoveNext_mE6C92440345B2AA33E6D1534087E514AE4D71A6C (void);
// 0x0000068D System.Object UnityWebGLSpeechDetection.Example09NoGUISpeechCommands/<Start>d__2::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CStartU3Ed__2_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mEDD0D9D21F65BD4743C534205936CB8CB2CBCD41 (void);
// 0x0000068E System.Void UnityWebGLSpeechDetection.Example09NoGUISpeechCommands/<Start>d__2::System.Collections.IEnumerator.Reset()
extern void U3CStartU3Ed__2_System_Collections_IEnumerator_Reset_m2488C31C7C875031BCCBC0ACACEA4BCE6430FE2A (void);
// 0x0000068F System.Object UnityWebGLSpeechDetection.Example09NoGUISpeechCommands/<Start>d__2::System.Collections.IEnumerator.get_Current()
extern void U3CStartU3Ed__2_System_Collections_IEnumerator_get_Current_m46FBAA334FC377A24C2D933F71512447FB11FD5B (void);
// 0x00000690 System.Void UnityWebGLSpeechDetection.ISpeechDetectionPlugin::Abort()
// 0x00000691 System.Void UnityWebGLSpeechDetection.ISpeechDetectionPlugin::AddListenerOnDetectionResult(UnityWebGLSpeechDetection.BaseSpeechDetectionPlugin/DelegateHandleDetectionResult)
// 0x00000692 System.Void UnityWebGLSpeechDetection.ISpeechDetectionPlugin::GetLanguages(System.Action`1<UnityWebGLSpeechDetection.LanguageResult>)
// 0x00000693 System.Boolean UnityWebGLSpeechDetection.ISpeechDetectionPlugin::IsAvailable()
// 0x00000694 System.Void UnityWebGLSpeechDetection.ISpeechDetectionPlugin::ManagementCloseBrowserTab()
// 0x00000695 System.Void UnityWebGLSpeechDetection.ISpeechDetectionPlugin::ManagementCloseProxy()
// 0x00000696 System.Void UnityWebGLSpeechDetection.ISpeechDetectionPlugin::ManagementLaunchProxy()
// 0x00000697 System.Void UnityWebGLSpeechDetection.ISpeechDetectionPlugin::ManagementOpenBrowserTab()
// 0x00000698 System.Void UnityWebGLSpeechDetection.ISpeechDetectionPlugin::ManagementSetProxyPort(System.Int32)
// 0x00000699 System.Void UnityWebGLSpeechDetection.ISpeechDetectionPlugin::RemoveListenerOnDetectionResult(UnityWebGLSpeechDetection.BaseSpeechDetectionPlugin/DelegateHandleDetectionResult)
// 0x0000069A System.Void UnityWebGLSpeechDetection.ISpeechDetectionPlugin::SetLanguage(System.String)
// 0x0000069B System.Void UnityWebGLSpeechDetection.ISpeechDetectionPlugin::Invoke(UnityWebGLSpeechDetection.LanguageChangedResult)
// 0x0000069C System.Boolean UnityWebGLSpeechDetection.IWWW::IsDone()
// 0x0000069D System.String UnityWebGLSpeechDetection.IWWW::GetError()
// 0x0000069E System.String UnityWebGLSpeechDetection.IWWW::GetText()
// 0x0000069F System.Void UnityWebGLSpeechDetection.IWWW::Dispose()
// 0x000006A0 System.Void UnityWebGLSpeechDetection.Language::.ctor()
extern void Language__ctor_mE3BE69C41FC505B06DF40959E533EAD7AA9A56EB (void);
// 0x000006A1 System.Void UnityWebGLSpeechDetection.LanguageChangedResult::.ctor()
extern void LanguageChangedResult__ctor_m38B3305857327A461D132803B89D09828F55A8E0 (void);
// 0x000006A2 System.Void UnityWebGLSpeechDetection.LanguageResult::.ctor()
extern void LanguageResult__ctor_m5036D73693F9648D2AD597CF336FA4ED0ED9D358 (void);
// 0x000006A3 UnityWebGLSpeechDetection.ProxySpeechDetectionPlugin UnityWebGLSpeechDetection.ProxySpeechDetectionPlugin::GetInstance()
extern void ProxySpeechDetectionPlugin_GetInstance_m081B9659988F4795E1C7B209347B4C83F3031800 (void);
// 0x000006A4 System.Void UnityWebGLSpeechDetection.ProxySpeechDetectionPlugin::SafeStartCoroutine(System.String,System.Collections.IEnumerator)
extern void ProxySpeechDetectionPlugin_SafeStartCoroutine_mFAA3F5F699EA8DF7FBCEC638192630B698233AB0 (void);
// 0x000006A5 System.Void UnityWebGLSpeechDetection.ProxySpeechDetectionPlugin::Awake()
extern void ProxySpeechDetectionPlugin_Awake_m83D10DB4ADBC6FFFD9058490FAD0EF58DEDB3D7A (void);
// 0x000006A6 System.Void UnityWebGLSpeechDetection.ProxySpeechDetectionPlugin::Start()
extern void ProxySpeechDetectionPlugin_Start_m62213B390AA9020A40EE280629E9C6DDACD99593 (void);
// 0x000006A7 System.Collections.IEnumerator UnityWebGLSpeechDetection.ProxySpeechDetectionPlugin::Init()
extern void ProxySpeechDetectionPlugin_Init_m84CA595544407D67541C32D252E8A9542EDEE55F (void);
// 0x000006A8 UnityWebGLSpeechDetection.IWWW UnityWebGLSpeechDetection.ProxySpeechDetectionPlugin::CreateWWW(System.String)
extern void ProxySpeechDetectionPlugin_CreateWWW_m5F98DCEE52E200F1E455A8878F34D795465C0FA7 (void);
// 0x000006A9 System.Collections.IEnumerator UnityWebGLSpeechDetection.ProxySpeechDetectionPlugin::RunPendingCommands()
extern void ProxySpeechDetectionPlugin_RunPendingCommands_m66D935B982A0D79D6D7533FC6219B71B6759C03D (void);
// 0x000006AA System.Collections.IEnumerator UnityWebGLSpeechDetection.ProxySpeechDetectionPlugin::GetResult()
extern void ProxySpeechDetectionPlugin_GetResult_mC030AA32DAA8592997FA7A5F671B5B11B948F41A (void);
// 0x000006AB System.Void UnityWebGLSpeechDetection.ProxySpeechDetectionPlugin::Abort()
extern void ProxySpeechDetectionPlugin_Abort_m89CBB2706E455720F5DCF1DBF8E24CCB1D388624 (void);
// 0x000006AC System.Boolean UnityWebGLSpeechDetection.ProxySpeechDetectionPlugin::IsAvailable()
extern void ProxySpeechDetectionPlugin_IsAvailable_m19247712B89D89A67887AAB42BD42377A0402744 (void);
// 0x000006AD System.Void UnityWebGLSpeechDetection.ProxySpeechDetectionPlugin::DebugCommands()
extern void ProxySpeechDetectionPlugin_DebugCommands_mFEA75E22D980034AE0E08A7A9FFBEE4087F0028F (void);
// 0x000006AE System.Void UnityWebGLSpeechDetection.ProxySpeechDetectionPlugin::AddCommand(System.String)
extern void ProxySpeechDetectionPlugin_AddCommand_mE47386ADE04DB85272125597F38AE07BE19D0345 (void);
// 0x000006AF System.Void UnityWebGLSpeechDetection.ProxySpeechDetectionPlugin::SetLanguage(System.String)
extern void ProxySpeechDetectionPlugin_SetLanguage_mBC1E9E299141F10ED238BEBC87B00AE4ECA6D3D3 (void);
// 0x000006B0 System.Collections.IEnumerator UnityWebGLSpeechDetection.ProxySpeechDetectionPlugin::ProxyLanguages(System.Action`1<UnityWebGLSpeechDetection.LanguageResult>)
extern void ProxySpeechDetectionPlugin_ProxyLanguages_m8E82A021B061AAB677F5036EF34520588DDD41E0 (void);
// 0x000006B1 System.Void UnityWebGLSpeechDetection.ProxySpeechDetectionPlugin::GetLanguages(System.Action`1<UnityWebGLSpeechDetection.LanguageResult>)
extern void ProxySpeechDetectionPlugin_GetLanguages_mEAEAE16D347DE3D1F011DBE277A155964DBF7B71 (void);
// 0x000006B2 System.Void UnityWebGLSpeechDetection.ProxySpeechDetectionPlugin::ManagementCloseBrowserTab()
extern void ProxySpeechDetectionPlugin_ManagementCloseBrowserTab_mA746439C5B793536BD09912762EF694A67489980 (void);
// 0x000006B3 System.Void UnityWebGLSpeechDetection.ProxySpeechDetectionPlugin::ManagementCloseProxy()
extern void ProxySpeechDetectionPlugin_ManagementCloseProxy_m4894209F12C85636554CB2575C26C71428BB65A9 (void);
// 0x000006B4 System.Void UnityWebGLSpeechDetection.ProxySpeechDetectionPlugin::ManagementLaunchProxy()
extern void ProxySpeechDetectionPlugin_ManagementLaunchProxy_m5D784B1D33662F25981D170330D5589EAA513A41 (void);
// 0x000006B5 System.Void UnityWebGLSpeechDetection.ProxySpeechDetectionPlugin::ManagementOpenBrowserTab()
extern void ProxySpeechDetectionPlugin_ManagementOpenBrowserTab_mC5698D7B5112170D37FFF0661C7642C54F958196 (void);
// 0x000006B6 System.Void UnityWebGLSpeechDetection.ProxySpeechDetectionPlugin::ManagementSetProxyPort(System.Int32)
extern void ProxySpeechDetectionPlugin_ManagementSetProxyPort_m93083D52E953E53726F6750284352114BC1ED3C0 (void);
// 0x000006B7 System.Void UnityWebGLSpeechDetection.ProxySpeechDetectionPlugin::.ctor()
extern void ProxySpeechDetectionPlugin__ctor_m2F96A86390CF297D99077D6631C55A00418086B7 (void);
// 0x000006B8 System.Void UnityWebGLSpeechDetection.ProxySpeechDetectionPlugin::.cctor()
extern void ProxySpeechDetectionPlugin__cctor_m49D485EB0189933383D40A6B633BECB327259F64 (void);
// 0x000006B9 System.Void UnityWebGLSpeechDetection.ProxySpeechDetectionPlugin/<Init>d__10::.ctor(System.Int32)
extern void U3CInitU3Ed__10__ctor_m012BA9734608E5F6552327811617DA42F34B766A (void);
// 0x000006BA System.Void UnityWebGLSpeechDetection.ProxySpeechDetectionPlugin/<Init>d__10::System.IDisposable.Dispose()
extern void U3CInitU3Ed__10_System_IDisposable_Dispose_mBBB8890BC2D1590E2C1DDE80878680C2212EF200 (void);
// 0x000006BB System.Boolean UnityWebGLSpeechDetection.ProxySpeechDetectionPlugin/<Init>d__10::MoveNext()
extern void U3CInitU3Ed__10_MoveNext_m364B843FE6861871C5513DE512E58E52C160D904 (void);
// 0x000006BC System.Object UnityWebGLSpeechDetection.ProxySpeechDetectionPlugin/<Init>d__10::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CInitU3Ed__10_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m54ADF1B4ABC962485DB751DCE71C27B0C17558DD (void);
// 0x000006BD System.Void UnityWebGLSpeechDetection.ProxySpeechDetectionPlugin/<Init>d__10::System.Collections.IEnumerator.Reset()
extern void U3CInitU3Ed__10_System_Collections_IEnumerator_Reset_mE083368AC1F41DD0E8F9B2988AB38626CDDB50B4 (void);
// 0x000006BE System.Object UnityWebGLSpeechDetection.ProxySpeechDetectionPlugin/<Init>d__10::System.Collections.IEnumerator.get_Current()
extern void U3CInitU3Ed__10_System_Collections_IEnumerator_get_Current_mE9D4A920E754B9F844722ABD42F7911A32B253AC (void);
// 0x000006BF System.Void UnityWebGLSpeechDetection.ProxySpeechDetectionPlugin/<RunPendingCommands>d__12::.ctor(System.Int32)
extern void U3CRunPendingCommandsU3Ed__12__ctor_mC11C018DC0A91D90202312BE9D6E7031F8B7A966 (void);
// 0x000006C0 System.Void UnityWebGLSpeechDetection.ProxySpeechDetectionPlugin/<RunPendingCommands>d__12::System.IDisposable.Dispose()
extern void U3CRunPendingCommandsU3Ed__12_System_IDisposable_Dispose_m273B75505906AC0B3E987E74414503C5EAFD3029 (void);
// 0x000006C1 System.Boolean UnityWebGLSpeechDetection.ProxySpeechDetectionPlugin/<RunPendingCommands>d__12::MoveNext()
extern void U3CRunPendingCommandsU3Ed__12_MoveNext_m8DFEFB3CDDC7F6B53ACE468F716BFFFCFEF4461A (void);
// 0x000006C2 System.Object UnityWebGLSpeechDetection.ProxySpeechDetectionPlugin/<RunPendingCommands>d__12::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CRunPendingCommandsU3Ed__12_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mF49722C3374CA4DCD5DEDA7A42E74B2E85449D0C (void);
// 0x000006C3 System.Void UnityWebGLSpeechDetection.ProxySpeechDetectionPlugin/<RunPendingCommands>d__12::System.Collections.IEnumerator.Reset()
extern void U3CRunPendingCommandsU3Ed__12_System_Collections_IEnumerator_Reset_mCE9FD8ED116F2DE4D31163FF91ED0F0BCC872409 (void);
// 0x000006C4 System.Object UnityWebGLSpeechDetection.ProxySpeechDetectionPlugin/<RunPendingCommands>d__12::System.Collections.IEnumerator.get_Current()
extern void U3CRunPendingCommandsU3Ed__12_System_Collections_IEnumerator_get_Current_m0214811E4DC232B9A5AD3775866A44B909549478 (void);
// 0x000006C5 System.Void UnityWebGLSpeechDetection.ProxySpeechDetectionPlugin/<GetResult>d__13::.ctor(System.Int32)
extern void U3CGetResultU3Ed__13__ctor_m0CDDF83A5EBDA6AC4DA49D5DF38A02D721246C3F (void);
// 0x000006C6 System.Void UnityWebGLSpeechDetection.ProxySpeechDetectionPlugin/<GetResult>d__13::System.IDisposable.Dispose()
extern void U3CGetResultU3Ed__13_System_IDisposable_Dispose_mD2C540480CC4924CBDA0D6CC8090D5E69A09E52A (void);
// 0x000006C7 System.Boolean UnityWebGLSpeechDetection.ProxySpeechDetectionPlugin/<GetResult>d__13::MoveNext()
extern void U3CGetResultU3Ed__13_MoveNext_m937D2D23BECF682693D1E40DA6D4F3BD47BE8EDC (void);
// 0x000006C8 System.Object UnityWebGLSpeechDetection.ProxySpeechDetectionPlugin/<GetResult>d__13::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CGetResultU3Ed__13_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m7EBC61FA9172162F7E2D417E3E9646E091814409 (void);
// 0x000006C9 System.Void UnityWebGLSpeechDetection.ProxySpeechDetectionPlugin/<GetResult>d__13::System.Collections.IEnumerator.Reset()
extern void U3CGetResultU3Ed__13_System_Collections_IEnumerator_Reset_mBCB807344745DBFF79C784AA606095E3ACECD75C (void);
// 0x000006CA System.Object UnityWebGLSpeechDetection.ProxySpeechDetectionPlugin/<GetResult>d__13::System.Collections.IEnumerator.get_Current()
extern void U3CGetResultU3Ed__13_System_Collections_IEnumerator_get_Current_m6EB722F8467C0E7094AFC385192102522361A12E (void);
// 0x000006CB System.Void UnityWebGLSpeechDetection.ProxySpeechDetectionPlugin/<ProxyLanguages>d__19::.ctor(System.Int32)
extern void U3CProxyLanguagesU3Ed__19__ctor_m73D98C4B262426CBA8A82CFCB41B8CC9E0083405 (void);
// 0x000006CC System.Void UnityWebGLSpeechDetection.ProxySpeechDetectionPlugin/<ProxyLanguages>d__19::System.IDisposable.Dispose()
extern void U3CProxyLanguagesU3Ed__19_System_IDisposable_Dispose_m7ABACFD849FD40300BF13D89CC177D0836FB0B4D (void);
// 0x000006CD System.Boolean UnityWebGLSpeechDetection.ProxySpeechDetectionPlugin/<ProxyLanguages>d__19::MoveNext()
extern void U3CProxyLanguagesU3Ed__19_MoveNext_m39D3FC07827123BE2576D7949189E142764DC34B (void);
// 0x000006CE System.Object UnityWebGLSpeechDetection.ProxySpeechDetectionPlugin/<ProxyLanguages>d__19::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CProxyLanguagesU3Ed__19_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m51D3D2D32D00F389B14479614A9A4F24C90D57D0 (void);
// 0x000006CF System.Void UnityWebGLSpeechDetection.ProxySpeechDetectionPlugin/<ProxyLanguages>d__19::System.Collections.IEnumerator.Reset()
extern void U3CProxyLanguagesU3Ed__19_System_Collections_IEnumerator_Reset_m368C82C50D2515A4715969FA6E605EF35B095C30 (void);
// 0x000006D0 System.Object UnityWebGLSpeechDetection.ProxySpeechDetectionPlugin/<ProxyLanguages>d__19::System.Collections.IEnumerator.get_Current()
extern void U3CProxyLanguagesU3Ed__19_System_Collections_IEnumerator_get_Current_m443AEE64E35421F4376968F384CE357F48E10A9E (void);
// 0x000006D1 UnityWebGLSpeechDetection.ISpeechDetectionPlugin UnityWebGLSpeechDetection.SpeechDetectionUtils::GetInstance()
extern void SpeechDetectionUtils_GetInstance_m86F66DD4AA98B058DE486420FB3974233E91044E (void);
// 0x000006D2 System.Void UnityWebGLSpeechDetection.SpeechDetectionUtils::PausePlayerPrefs(System.Boolean)
extern void SpeechDetectionUtils_PausePlayerPrefs_m1760596FCAE1D6EE09CECAC0D544865F4763EA35 (void);
// 0x000006D3 System.Void UnityWebGLSpeechDetection.SpeechDetectionUtils::SetActive(System.Boolean,UnityEngine.Component[])
extern void SpeechDetectionUtils_SetActive_mC1B4C840410E33AB9E47BD8C4FF38580CA0197CA (void);
// 0x000006D4 System.Void UnityWebGLSpeechDetection.SpeechDetectionUtils::SetInteractable(System.Boolean,UnityEngine.UI.Selectable[])
extern void SpeechDetectionUtils_SetInteractable_m4A6F5FDC6777BFDD436E8EA4EBD628D0FB5A921E (void);
// 0x000006D5 System.Void UnityWebGLSpeechDetection.SpeechDetectionUtils::PopulateDropdown(UnityEngine.UI.Dropdown,System.Collections.Generic.List`1<System.String>)
extern void SpeechDetectionUtils_PopulateDropdown_mE0525A4C8C593BCE3597FFBB614FF49A94E74148 (void);
// 0x000006D6 System.Void UnityWebGLSpeechDetection.SpeechDetectionUtils::SelectIndex(UnityEngine.UI.Dropdown,System.Int32)
extern void SpeechDetectionUtils_SelectIndex_m142FB8FD3DA1A0DF553E6C32BF9260CC61700CBF (void);
// 0x000006D7 System.Collections.Generic.List`1<System.String> UnityWebGLSpeechDetection.SpeechDetectionUtils::GetDefaultLanguageOptions()
extern void SpeechDetectionUtils_GetDefaultLanguageOptions_m10274403341FEED5147A41DE3DE0E3D8398C7070 (void);
// 0x000006D8 System.Collections.Generic.List`1<System.String> UnityWebGLSpeechDetection.SpeechDetectionUtils::GetDefaultDialectOptions()
extern void SpeechDetectionUtils_GetDefaultDialectOptions_m138A28CC22E888FFA8411401A7E0AA3E0E42784E (void);
// 0x000006D9 UnityWebGLSpeechDetection.Language UnityWebGLSpeechDetection.SpeechDetectionUtils::GetLanguage(UnityWebGLSpeechDetection.LanguageResult,System.String)
extern void SpeechDetectionUtils_GetLanguage_m23ACFC8523F94B1A5D89BFC931322281AFE5776B (void);
// 0x000006DA System.String UnityWebGLSpeechDetection.SpeechDetectionUtils::GetLanguageDisplay(UnityWebGLSpeechDetection.LanguageResult,System.String)
extern void SpeechDetectionUtils_GetLanguageDisplay_m9EA8BDF6F74E12BF46866E51C4F5002BD370E3D2 (void);
// 0x000006DB UnityWebGLSpeechDetection.Dialect UnityWebGLSpeechDetection.SpeechDetectionUtils::GetDialect(UnityWebGLSpeechDetection.LanguageResult,System.String)
extern void SpeechDetectionUtils_GetDialect_mC7428159E85CAEEB0F14E43AE9408C95BE81E4E9 (void);
// 0x000006DC System.String UnityWebGLSpeechDetection.SpeechDetectionUtils::GetDialectDisplay(UnityWebGLSpeechDetection.LanguageResult,System.String)
extern void SpeechDetectionUtils_GetDialectDisplay_mF79C18E3ED00088D0F62979D24AB7D4F603FE7F7 (void);
// 0x000006DD System.Void UnityWebGLSpeechDetection.SpeechDetectionUtils::DisableDialects(UnityEngine.UI.Dropdown)
extern void SpeechDetectionUtils_DisableDialects_m4ABC9BBEF61C26B33F163D28AB77F70B824FEBF8 (void);
// 0x000006DE System.Void UnityWebGLSpeechDetection.SpeechDetectionUtils::PopulateLanguages(System.String[]&,System.Int32&,UnityWebGLSpeechDetection.LanguageResult)
extern void SpeechDetectionUtils_PopulateLanguages_m5A188128F823952281473E44A9ABC6CFEA29C0BB (void);
// 0x000006DF System.Void UnityWebGLSpeechDetection.SpeechDetectionUtils::PopulateLanguagesDropdown(UnityEngine.UI.Dropdown,UnityWebGLSpeechDetection.LanguageResult)
extern void SpeechDetectionUtils_PopulateLanguagesDropdown_m0C8E1154FAFC57D2544F465CBEB237729F0EEFA5 (void);
// 0x000006E0 System.Void UnityWebGLSpeechDetection.SpeechDetectionUtils::HandleLanguageChanged(UnityEngine.UI.Dropdown,UnityEngine.UI.Dropdown,UnityWebGLSpeechDetection.LanguageResult,UnityWebGLSpeechDetection.ISpeechDetectionPlugin)
extern void SpeechDetectionUtils_HandleLanguageChanged_m3A5E598DE0C3EBDD1EC6987151998A2C6B6DDA3B (void);
// 0x000006E1 System.Void UnityWebGLSpeechDetection.SpeechDetectionUtils::HandleLanguageChanged(System.String[],System.Int32,System.String[]&,System.Int32&,UnityWebGLSpeechDetection.LanguageResult,UnityWebGLSpeechDetection.ISpeechDetectionPlugin)
extern void SpeechDetectionUtils_HandleLanguageChanged_m10A85CE235F3A2F4EB58958E1C77ACE49CEA7DA9 (void);
// 0x000006E2 System.Void UnityWebGLSpeechDetection.SpeechDetectionUtils::HandleDialectChanged(UnityEngine.UI.Dropdown,UnityWebGLSpeechDetection.LanguageResult,UnityWebGLSpeechDetection.ISpeechDetectionPlugin)
extern void SpeechDetectionUtils_HandleDialectChanged_m1921EA2983012936EE64203BF759548CE4CACCB2 (void);
// 0x000006E3 System.Void UnityWebGLSpeechDetection.SpeechDetectionUtils::HandleDialectChanged(System.String[],System.Int32,UnityWebGLSpeechDetection.LanguageResult,UnityWebGLSpeechDetection.ISpeechDetectionPlugin)
extern void SpeechDetectionUtils_HandleDialectChanged_mE0C3D3B782A7579D0C32FDF40CC758FB9B3ECFA0 (void);
// 0x000006E4 System.String UnityWebGLSpeechDetection.SpeechDetectionUtils::GetDefaultLanguage()
extern void SpeechDetectionUtils_GetDefaultLanguage_mCA50ADFE7F67D52BA64FDFDB4C59AE02EEE85722 (void);
// 0x000006E5 System.Void UnityWebGLSpeechDetection.SpeechDetectionUtils::SetDefaultLanguage(System.String)
extern void SpeechDetectionUtils_SetDefaultLanguage_m455E05A5BF4FAA712DE6FD79949B9D331C42D1BB (void);
// 0x000006E6 System.String UnityWebGLSpeechDetection.SpeechDetectionUtils::GetDefaultDialect()
extern void SpeechDetectionUtils_GetDefaultDialect_mD8DFBFAAD50CF7C4BBE2341D6AB9424C55D33E60 (void);
// 0x000006E7 System.Void UnityWebGLSpeechDetection.SpeechDetectionUtils::SetDefaultDialect(System.String)
extern void SpeechDetectionUtils_SetDefaultDialect_m39CDC254772357DEF97F4D334E314FDCB75BBCD0 (void);
// 0x000006E8 System.Void UnityWebGLSpeechDetection.SpeechDetectionUtils::RestoreLanguage(UnityEngine.UI.Dropdown)
extern void SpeechDetectionUtils_RestoreLanguage_mBFD43321F4E9F81DBF97C1939B99C75A31BEB1BD (void);
// 0x000006E9 System.Void UnityWebGLSpeechDetection.SpeechDetectionUtils::RestoreLanguage(System.String[],System.Int32&)
extern void SpeechDetectionUtils_RestoreLanguage_m041CCF1EBBB8DA11BEA0C15D5AD95085CBA3C386 (void);
// 0x000006EA System.Void UnityWebGLSpeechDetection.SpeechDetectionUtils::RestoreDialect(UnityEngine.UI.Dropdown)
extern void SpeechDetectionUtils_RestoreDialect_m1EFFADD244486F80BE3E73A7736B2537F17D45F1 (void);
// 0x000006EB System.Void UnityWebGLSpeechDetection.SpeechDetectionUtils::RestoreDialect(System.String[],System.Int32&)
extern void SpeechDetectionUtils_RestoreDialect_m8BA5D41E16E154E23F7A8BFFB5E3998362120804 (void);
// 0x000006EC System.Boolean UnityWebGLSpeechDetection.SpeechDetectionUtils::HandleOnLanguageChanged(System.Int32&,System.String[],System.Int32&,System.String[],UnityWebGLSpeechDetection.LanguageResult,UnityWebGLSpeechDetection.ISpeechDetectionPlugin,UnityWebGLSpeechDetection.LanguageChangedResult)
extern void SpeechDetectionUtils_HandleOnLanguageChanged_mA89667F3EBA71A953D7C6D22F6F95CE04002BA04 (void);
// 0x000006ED System.Void UnityWebGLSpeechDetection.SpeechDetectionUtils::.ctor()
extern void SpeechDetectionUtils__ctor_m455C69836CC1AC6178EB05355A8C35D4CFB9929F (void);
// 0x000006EE System.Void UnityWebGLSpeechDetection.SpeechDetectionUtils::.cctor()
extern void SpeechDetectionUtils__cctor_mA312DFBC237322C8E94F40DE745B7F9826A9F07C (void);
// 0x000006EF System.Void UnityWebGLSpeechDetection.SpeechProxyConfig::.ctor()
extern void SpeechProxyConfig__ctor_m631792703A397FD98A8A38FCD6B538DA6A595A72 (void);
// 0x000006F0 System.Void UnityWebGLSpeechDetection.SpeechRecognitionAlternative::.ctor()
extern void SpeechRecognitionAlternative__ctor_mAC45640199215EFC36CF8E744CB6567B8DC51F80 (void);
// 0x000006F1 System.Void UnityWebGLSpeechDetection.SpeechRecognitionResult::.ctor()
extern void SpeechRecognitionResult__ctor_m40C2AB5648C0B02112823BF098E922073D2DC761 (void);
// 0x000006F2 System.Void UnityWebGLSpeechDetection.WWWEditMode::.ctor(System.String)
extern void WWWEditMode__ctor_mAE738C3EEF803D1701AE157067F9C988A25171BE (void);
// 0x000006F3 System.Void UnityWebGLSpeechDetection.WWWEditMode::HandleBeginGetResponse(System.IAsyncResult)
extern void WWWEditMode_HandleBeginGetResponse_m3A6B35B3ADF86D626F091A820E7BEFEC1EB32F8E (void);
// 0x000006F4 System.Boolean UnityWebGLSpeechDetection.WWWEditMode::IsDone()
extern void WWWEditMode_IsDone_mCE0A3604D99AABC806AE3162311DCD14E49E66CD (void);
// 0x000006F5 System.String UnityWebGLSpeechDetection.WWWEditMode::GetError()
extern void WWWEditMode_GetError_mDE1F06A24A7548CDC9A9C690F8C30A743173CB8F (void);
// 0x000006F6 System.String UnityWebGLSpeechDetection.WWWEditMode::GetText()
extern void WWWEditMode_GetText_mE7B6EB2AD493E2DC6C7019450C4B610E74664703 (void);
// 0x000006F7 System.Void UnityWebGLSpeechDetection.WWWEditMode::Dispose()
extern void WWWEditMode_Dispose_m050C725951671D100E1C322EAF19A5A90636E80C (void);
// 0x000006F8 System.Void UnityWebGLSpeechDetection.WWWPlayMode::.ctor(System.String)
extern void WWWPlayMode__ctor_mA09C34F908890A9E0AB4B41E58DF937F25A3BFFF (void);
// 0x000006F9 System.Boolean UnityWebGLSpeechDetection.WWWPlayMode::IsDone()
extern void WWWPlayMode_IsDone_mF03CD99A8A00C2C85D8A8E1AA88426C2E09BD829 (void);
// 0x000006FA System.String UnityWebGLSpeechDetection.WWWPlayMode::GetError()
extern void WWWPlayMode_GetError_mAB650946E549F10E7E72EBC7DF138B2D72E48F86 (void);
// 0x000006FB System.String UnityWebGLSpeechDetection.WWWPlayMode::GetText()
extern void WWWPlayMode_GetText_mC12A436A7034055056F62CD644E8D4202A1EBBAF (void);
// 0x000006FC System.Void UnityWebGLSpeechDetection.WWWPlayMode::Dispose()
extern void WWWPlayMode_Dispose_m180D815AD96FF80FAE853C4DCA681B8D18A1D3A1 (void);
// 0x000006FD UnityWebGLSpeechDetection.WebGLSpeechDetectionPlugin UnityWebGLSpeechDetection.WebGLSpeechDetectionPlugin::GetInstance()
extern void WebGLSpeechDetectionPlugin_GetInstance_mAF00BB3954646ECC8A6250920FC045F260C405FF (void);
// 0x000006FE System.Boolean UnityWebGLSpeechDetection.WebGLSpeechDetectionPlugin::IsAvailable()
extern void WebGLSpeechDetectionPlugin_IsAvailable_m0053943FA3FFC06714678552A4416A42664887A2 (void);
// 0x000006FF System.Void UnityWebGLSpeechDetection.WebGLSpeechDetectionPlugin::Abort()
extern void WebGLSpeechDetectionPlugin_Abort_m2FE8783886E36E1D46A50DE727D69FCC0B16B009 (void);
// 0x00000700 System.Void UnityWebGLSpeechDetection.WebGLSpeechDetectionPlugin::StartRecognition()
extern void WebGLSpeechDetectionPlugin_StartRecognition_m32772E7334B6368FC84969615B4273B7D3C07B64 (void);
// 0x00000701 System.Void UnityWebGLSpeechDetection.WebGLSpeechDetectionPlugin::StopRecognition()
extern void WebGLSpeechDetectionPlugin_StopRecognition_m9CF0D3BAB199469B26F30F9C26ADC10A287D347D (void);
// 0x00000702 System.Void UnityWebGLSpeechDetection.WebGLSpeechDetectionPlugin::GetLanguages(System.Action`1<UnityWebGLSpeechDetection.LanguageResult>)
extern void WebGLSpeechDetectionPlugin_GetLanguages_mB33FCEA6A5BF93848B6D5D5AB94DEB7B05101985 (void);
// 0x00000703 System.Void UnityWebGLSpeechDetection.WebGLSpeechDetectionPlugin::SetLanguage(System.String)
extern void WebGLSpeechDetectionPlugin_SetLanguage_m92CFEEC61B7D7BB0F6713F583B754A71AE05C5B1 (void);
// 0x00000704 System.Void UnityWebGLSpeechDetection.WebGLSpeechDetectionPlugin::Awake()
extern void WebGLSpeechDetectionPlugin_Awake_m98B553DD80AEB0BC67D1DCDB6BFE867BB25B4366 (void);
// 0x00000705 System.Void UnityWebGLSpeechDetection.WebGLSpeechDetectionPlugin::ManagementCloseBrowserTab()
extern void WebGLSpeechDetectionPlugin_ManagementCloseBrowserTab_m10B278A9EE93E9541E631732968314D6CB40D085 (void);
// 0x00000706 System.Void UnityWebGLSpeechDetection.WebGLSpeechDetectionPlugin::ManagementCloseProxy()
extern void WebGLSpeechDetectionPlugin_ManagementCloseProxy_m1A696915B0BBC37247593BB37F74CF0955CD8B2B (void);
// 0x00000707 System.Void UnityWebGLSpeechDetection.WebGLSpeechDetectionPlugin::ManagementLaunchProxy()
extern void WebGLSpeechDetectionPlugin_ManagementLaunchProxy_m96A960A3B42682AFA9B187BEF68F4AC4EB484F8A (void);
// 0x00000708 System.Void UnityWebGLSpeechDetection.WebGLSpeechDetectionPlugin::ManagementOpenBrowserTab()
extern void WebGLSpeechDetectionPlugin_ManagementOpenBrowserTab_m7DBBCB6A0793A5E42E50FA8E97DA483665C2162D (void);
// 0x00000709 System.Void UnityWebGLSpeechDetection.WebGLSpeechDetectionPlugin::ManagementSetProxyPort(System.Int32)
extern void WebGLSpeechDetectionPlugin_ManagementSetProxyPort_mABE7801B5CD17EC3ABCBFC8ADF08DE89F14559C6 (void);
// 0x0000070A System.Void UnityWebGLSpeechDetection.WebGLSpeechDetectionPlugin::.ctor()
extern void WebGLSpeechDetectionPlugin__ctor_m4AE99E3CC7C462ACC856F1395C43E374D5D590DE (void);
// 0x0000070B System.Void UnityWebGLSpeechDetection.WebGLSpeechDetectionPlugin::.cctor()
extern void WebGLSpeechDetectionPlugin__cctor_m3D79521306D985DCD9BB4DDA7DFAFED707D81D6E (void);
// 0x0000070C System.Collections.IEnumerator UnityWebGLSpeech.Example01DictationSynthesis::Start()
extern void Example01DictationSynthesis_Start_mB4882D5513B9316D6BAB403228D4AED406D59C1C (void);
// 0x0000070D System.Void UnityWebGLSpeech.Example01DictationSynthesis::HandleSynthesisOnEnd(UnityWebGLSpeechSynthesis.SpeechSynthesisEvent)
extern void Example01DictationSynthesis_HandleSynthesisOnEnd_mECB16A9F1A518F016716DE5C109C7C3ECAC8DD26 (void);
// 0x0000070E System.Void UnityWebGLSpeech.Example01DictationSynthesis::Update()
extern void Example01DictationSynthesis_Update_mB8BA0E710FDCE505D22F7623EC8E196395E09CF0 (void);
// 0x0000070F System.Boolean UnityWebGLSpeech.Example01DictationSynthesis::HandleDetectionResult(UnityWebGLSpeechDetection.DetectionResult)
extern void Example01DictationSynthesis_HandleDetectionResult_mA565FA06C67D65DC1615F17CB88267A19E6249F3 (void);
// 0x00000710 System.Collections.IEnumerator UnityWebGLSpeech.Example01DictationSynthesis::GetVoices()
extern void Example01DictationSynthesis_GetVoices_m7A46A9BDFEAC0BC2BE84DA4D4E435C4709F1940B (void);
// 0x00000711 System.Void UnityWebGLSpeech.Example01DictationSynthesis::SetIfReadyForDefaultVoice()
extern void Example01DictationSynthesis_SetIfReadyForDefaultVoice_mBA8D700EACA086D8A59308FE40832788991C7C85 (void);
// 0x00000712 System.Void UnityWebGLSpeech.Example01DictationSynthesis::Speak()
extern void Example01DictationSynthesis_Speak_m7572ACE032FFAE632F8B4A4CCC156F31C533F988 (void);
// 0x00000713 System.Collections.IEnumerator UnityWebGLSpeech.Example01DictationSynthesis::SetPitch(System.Single)
extern void Example01DictationSynthesis_SetPitch_m387E57A36A344607A5BF04F1BF4DE73E58F1C212 (void);
// 0x00000714 System.Collections.IEnumerator UnityWebGLSpeech.Example01DictationSynthesis::SetRate(System.Single)
extern void Example01DictationSynthesis_SetRate_m98FDF4E83F129860CF981AC241540A865844849C (void);
// 0x00000715 System.Void UnityWebGLSpeech.Example01DictationSynthesis::FixedUpdate()
extern void Example01DictationSynthesis_FixedUpdate_m0C2DDEBFF345B0D7D9C1419FB03FC67EDCB58CEE (void);
// 0x00000716 System.Void UnityWebGLSpeech.Example01DictationSynthesis::.ctor()
extern void Example01DictationSynthesis__ctor_m6BAFC9D572002D94CB59C37FAA0BF8E833920234 (void);
// 0x00000717 System.Void UnityWebGLSpeech.Example01DictationSynthesis::<Start>b__28_0()
extern void Example01DictationSynthesis_U3CStartU3Eb__28_0_m2BB9F8966F92CEEE45201B8B5EBC468EF95CC0A8 (void);
// 0x00000718 System.Void UnityWebGLSpeech.Example01DictationSynthesis::<Start>b__28_1()
extern void Example01DictationSynthesis_U3CStartU3Eb__28_1_m40F45F64E6037C82A4D94DCB628D9B9D20F789B2 (void);
// 0x00000719 System.Void UnityWebGLSpeech.Example01DictationSynthesis::<Start>b__28_2()
extern void Example01DictationSynthesis_U3CStartU3Eb__28_2_mF4484E4A96810FC30E1902285A2952CB7EFEF50C (void);
// 0x0000071A System.Void UnityWebGLSpeech.Example01DictationSynthesis::<Start>b__28_3(UnityWebGLSpeechDetection.LanguageResult)
extern void Example01DictationSynthesis_U3CStartU3Eb__28_3_m7D58A195ACB6F257BBB7646F8E3C6ADCC4E506D4 (void);
// 0x0000071B System.Void UnityWebGLSpeech.Example01DictationSynthesis::<Start>b__28_8(System.Int32)
extern void Example01DictationSynthesis_U3CStartU3Eb__28_8_mB7702341DF0663D771005EC27CC628EA3825A6C1 (void);
// 0x0000071C System.Void UnityWebGLSpeech.Example01DictationSynthesis::<Start>b__28_9(System.Int32)
extern void Example01DictationSynthesis_U3CStartU3Eb__28_9_m4109CE818814E955E1C68269A520B2AB34F0CBC1 (void);
// 0x0000071D System.Void UnityWebGLSpeech.Example01DictationSynthesis::<Start>b__28_4(UnityWebGLSpeechSynthesis.SpeechSynthesisUtterance)
extern void Example01DictationSynthesis_U3CStartU3Eb__28_4_mDF45F0C4ED5AB0BAAF9C49DC31880C30A2ABC532 (void);
// 0x0000071E System.Void UnityWebGLSpeech.Example01DictationSynthesis::<Start>b__28_5(System.Single)
extern void Example01DictationSynthesis_U3CStartU3Eb__28_5_mF1465E0ECA48721C2D79C05FC8673499A676639F (void);
// 0x0000071F System.Void UnityWebGLSpeech.Example01DictationSynthesis::<Start>b__28_6(System.Single)
extern void Example01DictationSynthesis_U3CStartU3Eb__28_6_mF0B4E30B599D0E84B30C93594F4D8BB01070A61B (void);
// 0x00000720 System.Void UnityWebGLSpeech.Example01DictationSynthesis::<Start>b__28_7(System.Single)
extern void Example01DictationSynthesis_U3CStartU3Eb__28_7_mB2DEFDEF9A51C0B5267A44A8DB5224DC798BAD6F (void);
// 0x00000721 System.Void UnityWebGLSpeech.Example01DictationSynthesis::<GetVoices>b__32_0(UnityWebGLSpeechSynthesis.VoiceResult)
extern void Example01DictationSynthesis_U3CGetVoicesU3Eb__32_0_m7647381772F8947F21087CA9489F9A76C9AEEC41 (void);
// 0x00000722 System.Void UnityWebGLSpeech.Example01DictationSynthesis::<SetIfReadyForDefaultVoice>b__33_0(System.Int32)
extern void Example01DictationSynthesis_U3CSetIfReadyForDefaultVoiceU3Eb__33_0_mE07F5718D62BA9FC6AB3DC33A6BF537C091A9523 (void);
// 0x00000723 System.Void UnityWebGLSpeech.Example01DictationSynthesis/<Start>d__28::.ctor(System.Int32)
extern void U3CStartU3Ed__28__ctor_m6251A007ED7831DB20DE6DD12568AFC488C1B1A0 (void);
// 0x00000724 System.Void UnityWebGLSpeech.Example01DictationSynthesis/<Start>d__28::System.IDisposable.Dispose()
extern void U3CStartU3Ed__28_System_IDisposable_Dispose_mE1A0CB500E998B63CCC43B876F7F55F86C1B921B (void);
// 0x00000725 System.Boolean UnityWebGLSpeech.Example01DictationSynthesis/<Start>d__28::MoveNext()
extern void U3CStartU3Ed__28_MoveNext_mF1650806095FF54E87D3A23311017158651C457D (void);
// 0x00000726 System.Object UnityWebGLSpeech.Example01DictationSynthesis/<Start>d__28::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CStartU3Ed__28_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mB67313988CF183C834E335EE4EE373EE42FAAE53 (void);
// 0x00000727 System.Void UnityWebGLSpeech.Example01DictationSynthesis/<Start>d__28::System.Collections.IEnumerator.Reset()
extern void U3CStartU3Ed__28_System_Collections_IEnumerator_Reset_mE54EED999CFD46CE5C5FF21F95AF6153F165E5CE (void);
// 0x00000728 System.Object UnityWebGLSpeech.Example01DictationSynthesis/<Start>d__28::System.Collections.IEnumerator.get_Current()
extern void U3CStartU3Ed__28_System_Collections_IEnumerator_get_Current_m8A5EF102397A7D73091A8A9EECEB30078BF7B8CC (void);
// 0x00000729 System.Void UnityWebGLSpeech.Example01DictationSynthesis/<GetVoices>d__32::.ctor(System.Int32)
extern void U3CGetVoicesU3Ed__32__ctor_mF7508112903C4F9ED3CAF31218951EAE34D1DBEE (void);
// 0x0000072A System.Void UnityWebGLSpeech.Example01DictationSynthesis/<GetVoices>d__32::System.IDisposable.Dispose()
extern void U3CGetVoicesU3Ed__32_System_IDisposable_Dispose_mEC4E1291389C3EA9CFA71F80146E8F16FA321323 (void);
// 0x0000072B System.Boolean UnityWebGLSpeech.Example01DictationSynthesis/<GetVoices>d__32::MoveNext()
extern void U3CGetVoicesU3Ed__32_MoveNext_m9579F8C70BFC4FEFDAC453E735492782C7E4EFE9 (void);
// 0x0000072C System.Object UnityWebGLSpeech.Example01DictationSynthesis/<GetVoices>d__32::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CGetVoicesU3Ed__32_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m4EAEC51E6B7FBCD8B9A1CBCD16DA2966223124C8 (void);
// 0x0000072D System.Void UnityWebGLSpeech.Example01DictationSynthesis/<GetVoices>d__32::System.Collections.IEnumerator.Reset()
extern void U3CGetVoicesU3Ed__32_System_Collections_IEnumerator_Reset_m094A2A097F4044D384F0C23A2BD9629CB03E9B90 (void);
// 0x0000072E System.Object UnityWebGLSpeech.Example01DictationSynthesis/<GetVoices>d__32::System.Collections.IEnumerator.get_Current()
extern void U3CGetVoicesU3Ed__32_System_Collections_IEnumerator_get_Current_mBBB5185B3F8DB86560A9DE3F3BBB2E7531E2C484 (void);
// 0x0000072F System.Void UnityWebGLSpeech.Example01DictationSynthesis/<SetPitch>d__35::.ctor(System.Int32)
extern void U3CSetPitchU3Ed__35__ctor_m9D8D07FCA90D3E73D299B2D80FE6E7C891D77952 (void);
// 0x00000730 System.Void UnityWebGLSpeech.Example01DictationSynthesis/<SetPitch>d__35::System.IDisposable.Dispose()
extern void U3CSetPitchU3Ed__35_System_IDisposable_Dispose_mB8668091EEFA11E8010416E9001FCF9EC09ECE72 (void);
// 0x00000731 System.Boolean UnityWebGLSpeech.Example01DictationSynthesis/<SetPitch>d__35::MoveNext()
extern void U3CSetPitchU3Ed__35_MoveNext_m088FB80C4C2F972C3E83DB3805C494BD1E576C94 (void);
// 0x00000732 System.Object UnityWebGLSpeech.Example01DictationSynthesis/<SetPitch>d__35::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CSetPitchU3Ed__35_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m78E469CF14747B40EB8F0759E0639B39FB089C41 (void);
// 0x00000733 System.Void UnityWebGLSpeech.Example01DictationSynthesis/<SetPitch>d__35::System.Collections.IEnumerator.Reset()
extern void U3CSetPitchU3Ed__35_System_Collections_IEnumerator_Reset_m66DB440A475F280C6F51FE8B7F3016EB7FE9151F (void);
// 0x00000734 System.Object UnityWebGLSpeech.Example01DictationSynthesis/<SetPitch>d__35::System.Collections.IEnumerator.get_Current()
extern void U3CSetPitchU3Ed__35_System_Collections_IEnumerator_get_Current_m83AEA3365906FC77D9D1DE7859301D5621AB2E4B (void);
// 0x00000735 System.Void UnityWebGLSpeech.Example01DictationSynthesis/<SetRate>d__36::.ctor(System.Int32)
extern void U3CSetRateU3Ed__36__ctor_m850DD6B944C189AFC8DD0EC75E985E279C02848D (void);
// 0x00000736 System.Void UnityWebGLSpeech.Example01DictationSynthesis/<SetRate>d__36::System.IDisposable.Dispose()
extern void U3CSetRateU3Ed__36_System_IDisposable_Dispose_mFE9B1BC95F527E94DB2F805C2E2DE218C08C36A0 (void);
// 0x00000737 System.Boolean UnityWebGLSpeech.Example01DictationSynthesis/<SetRate>d__36::MoveNext()
extern void U3CSetRateU3Ed__36_MoveNext_mB1DF57598D5B98C623A9EACCE88D79D33947D4AE (void);
// 0x00000738 System.Object UnityWebGLSpeech.Example01DictationSynthesis/<SetRate>d__36::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CSetRateU3Ed__36_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m8ACBE875E3E52945765CB65BB421C1DB1758D2D3 (void);
// 0x00000739 System.Void UnityWebGLSpeech.Example01DictationSynthesis/<SetRate>d__36::System.Collections.IEnumerator.Reset()
extern void U3CSetRateU3Ed__36_System_Collections_IEnumerator_Reset_mD3357A03BEE1F8589E4CC34678D40D0137D0FC3D (void);
// 0x0000073A System.Object UnityWebGLSpeech.Example01DictationSynthesis/<SetRate>d__36::System.Collections.IEnumerator.get_Current()
extern void U3CSetRateU3Ed__36_System_Collections_IEnumerator_get_Current_m7934C30490D8913CC34E6F24824C5CFD13ED62C7 (void);
// 0x0000073B System.Void UnityWebGLSpeech.Example02DictationSbaitso::CreateText(System.String,UnityEngine.Color)
extern void Example02DictationSbaitso_CreateText_m5DF09DB8E8A11F824B00E0760C6DCE127C52E2ED (void);
// 0x0000073C System.Collections.IEnumerator UnityWebGLSpeech.Example02DictationSbaitso::CreateNameInputField()
extern void Example02DictationSbaitso_CreateNameInputField_m669A31FCB67151C44FA729295641A592875E8ECD (void);
// 0x0000073D System.Collections.IEnumerator UnityWebGLSpeech.Example02DictationSbaitso::CreateTalkInputField()
extern void Example02DictationSbaitso_CreateTalkInputField_m93C3B2C862D8C5D6A359775752B6DAE8065D0021 (void);
// 0x0000073E System.Void UnityWebGLSpeech.Example02DictationSbaitso::CreateTextAndSpeak(System.String)
extern void Example02DictationSbaitso_CreateTextAndSpeak_m66A5C20C2CBF012197901BCB2CDA03437907EFA0 (void);
// 0x0000073F System.Collections.IEnumerator UnityWebGLSpeech.Example02DictationSbaitso::Start()
extern void Example02DictationSbaitso_Start_m7444A4D38B7A9C05C2917DAAC2EF163D756D7E74 (void);
// 0x00000740 System.Boolean UnityWebGLSpeech.Example02DictationSbaitso::HandleDetectionResult(UnityWebGLSpeechDetection.DetectionResult)
extern void Example02DictationSbaitso_HandleDetectionResult_m87B55B402CCCC347A334ABD1DF94D27E2E03BAB7 (void);
// 0x00000741 System.Void UnityWebGLSpeech.Example02DictationSbaitso::HandleSynthesisOnEnd(UnityWebGLSpeechSynthesis.SpeechSynthesisEvent)
extern void Example02DictationSbaitso_HandleSynthesisOnEnd_m2E8D363B2844290C42841A463C80946C3C253459 (void);
// 0x00000742 System.Collections.IEnumerator UnityWebGLSpeech.Example02DictationSbaitso::GetVoices()
extern void Example02DictationSbaitso_GetVoices_m089DFE1583E1DD7E4BCCE8AD263845C0EB0AEE83 (void);
// 0x00000743 System.Void UnityWebGLSpeech.Example02DictationSbaitso::FixedUpdate()
extern void Example02DictationSbaitso_FixedUpdate_m00184E30E60EB7B7B8325928E22560B4B4A06846 (void);
// 0x00000744 System.Void UnityWebGLSpeech.Example02DictationSbaitso::Speak(System.String)
extern void Example02DictationSbaitso_Speak_mAC90064AAA111649D1AB5A8E5502C6C1BDCA27B2 (void);
// 0x00000745 System.Void UnityWebGLSpeech.Example02DictationSbaitso::.ctor()
extern void Example02DictationSbaitso__ctor_m8421E653FEC99F7F41AF7062B15FE458A4858F70 (void);
// 0x00000746 System.Void UnityWebGLSpeech.Example02DictationSbaitso::<Start>b__21_0()
extern void Example02DictationSbaitso_U3CStartU3Eb__21_0_mCCD542190710E61A9EDE15A3E559DC311ADD9BEB (void);
// 0x00000747 System.Void UnityWebGLSpeech.Example02DictationSbaitso::<Start>b__21_1()
extern void Example02DictationSbaitso_U3CStartU3Eb__21_1_mF537CA2C485462C331E4C7316CCFEB0A6DC1F203 (void);
// 0x00000748 System.Void UnityWebGLSpeech.Example02DictationSbaitso::<Start>b__21_2(UnityWebGLSpeechSynthesis.SpeechSynthesisUtterance)
extern void Example02DictationSbaitso_U3CStartU3Eb__21_2_mC0F51C19C5FF069B8BDCCAE56A1507FDE85B8772 (void);
// 0x00000749 System.Void UnityWebGLSpeech.Example02DictationSbaitso::<GetVoices>b__24_0(UnityWebGLSpeechSynthesis.VoiceResult)
extern void Example02DictationSbaitso_U3CGetVoicesU3Eb__24_0_mB6088C47629D61E8ABE71DF63887EEBECC040C67 (void);
// 0x0000074A System.Void UnityWebGLSpeech.Example02DictationSbaitso/<CreateNameInputField>d__18::.ctor(System.Int32)
extern void U3CCreateNameInputFieldU3Ed__18__ctor_m4B5B254484C31B4D071462387837964CB5FB5FE0 (void);
// 0x0000074B System.Void UnityWebGLSpeech.Example02DictationSbaitso/<CreateNameInputField>d__18::System.IDisposable.Dispose()
extern void U3CCreateNameInputFieldU3Ed__18_System_IDisposable_Dispose_mF82190DDE2697199F48366F177466E899B0022F2 (void);
// 0x0000074C System.Boolean UnityWebGLSpeech.Example02DictationSbaitso/<CreateNameInputField>d__18::MoveNext()
extern void U3CCreateNameInputFieldU3Ed__18_MoveNext_m9478A7053CE77BB6ED7D92AE6DB36FE9A0FF14C6 (void);
// 0x0000074D System.Object UnityWebGLSpeech.Example02DictationSbaitso/<CreateNameInputField>d__18::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CCreateNameInputFieldU3Ed__18_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mA7C8F9A3E9BCA451E48A0B59AF88D0D30FA8052D (void);
// 0x0000074E System.Void UnityWebGLSpeech.Example02DictationSbaitso/<CreateNameInputField>d__18::System.Collections.IEnumerator.Reset()
extern void U3CCreateNameInputFieldU3Ed__18_System_Collections_IEnumerator_Reset_m1EA0F77850A1CE0F7FA9D16059514D7039A976C6 (void);
// 0x0000074F System.Object UnityWebGLSpeech.Example02DictationSbaitso/<CreateNameInputField>d__18::System.Collections.IEnumerator.get_Current()
extern void U3CCreateNameInputFieldU3Ed__18_System_Collections_IEnumerator_get_Current_m6EB5B4889BD9E1D8F6101FFF052897D2508A69F0 (void);
// 0x00000750 System.Void UnityWebGLSpeech.Example02DictationSbaitso/<CreateTalkInputField>d__19::.ctor(System.Int32)
extern void U3CCreateTalkInputFieldU3Ed__19__ctor_mCE780DD23754578C72F63AA3BE000D2EF21C457E (void);
// 0x00000751 System.Void UnityWebGLSpeech.Example02DictationSbaitso/<CreateTalkInputField>d__19::System.IDisposable.Dispose()
extern void U3CCreateTalkInputFieldU3Ed__19_System_IDisposable_Dispose_mF2ACFC307D0C8C5D10CAC168C84B87629E31C5ED (void);
// 0x00000752 System.Boolean UnityWebGLSpeech.Example02DictationSbaitso/<CreateTalkInputField>d__19::MoveNext()
extern void U3CCreateTalkInputFieldU3Ed__19_MoveNext_mEFD711E0D39AA10DAEA0F2AF34384047AC746534 (void);
// 0x00000753 System.Object UnityWebGLSpeech.Example02DictationSbaitso/<CreateTalkInputField>d__19::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CCreateTalkInputFieldU3Ed__19_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mE52A015117FA49929B6655BA390A090BC6BC3934 (void);
// 0x00000754 System.Void UnityWebGLSpeech.Example02DictationSbaitso/<CreateTalkInputField>d__19::System.Collections.IEnumerator.Reset()
extern void U3CCreateTalkInputFieldU3Ed__19_System_Collections_IEnumerator_Reset_m8F584B55249C63900EE385230905F4819BCB245C (void);
// 0x00000755 System.Object UnityWebGLSpeech.Example02DictationSbaitso/<CreateTalkInputField>d__19::System.Collections.IEnumerator.get_Current()
extern void U3CCreateTalkInputFieldU3Ed__19_System_Collections_IEnumerator_get_Current_mDBCA9A1BCCADCA02EC5ABFFD1073E495B6F7EDE6 (void);
// 0x00000756 System.Void UnityWebGLSpeech.Example02DictationSbaitso/<Start>d__21::.ctor(System.Int32)
extern void U3CStartU3Ed__21__ctor_mC72EA5BAA492982E48576F5F60FD8483732353B6 (void);
// 0x00000757 System.Void UnityWebGLSpeech.Example02DictationSbaitso/<Start>d__21::System.IDisposable.Dispose()
extern void U3CStartU3Ed__21_System_IDisposable_Dispose_m12232760A14BC34C5C5EC1EACC05E1852D6A89A9 (void);
// 0x00000758 System.Boolean UnityWebGLSpeech.Example02DictationSbaitso/<Start>d__21::MoveNext()
extern void U3CStartU3Ed__21_MoveNext_m7B440ADBFAA34F1850E917C578D4542AD458AC5A (void);
// 0x00000759 System.Object UnityWebGLSpeech.Example02DictationSbaitso/<Start>d__21::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CStartU3Ed__21_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m535DFD5C67CB16D689578F83B00CEC23E4484FEA (void);
// 0x0000075A System.Void UnityWebGLSpeech.Example02DictationSbaitso/<Start>d__21::System.Collections.IEnumerator.Reset()
extern void U3CStartU3Ed__21_System_Collections_IEnumerator_Reset_m67EA047E1E606C626E1B1D7A7E58018554CC12A8 (void);
// 0x0000075B System.Object UnityWebGLSpeech.Example02DictationSbaitso/<Start>d__21::System.Collections.IEnumerator.get_Current()
extern void U3CStartU3Ed__21_System_Collections_IEnumerator_get_Current_mA401C2A6D290024BD88FC27BE57E64DDE97B5433 (void);
// 0x0000075C System.Void UnityWebGLSpeech.Example02DictationSbaitso/<GetVoices>d__24::.ctor(System.Int32)
extern void U3CGetVoicesU3Ed__24__ctor_m833C2611DC287B9E47094CADC771D692419E63E6 (void);
// 0x0000075D System.Void UnityWebGLSpeech.Example02DictationSbaitso/<GetVoices>d__24::System.IDisposable.Dispose()
extern void U3CGetVoicesU3Ed__24_System_IDisposable_Dispose_m71DBB98BC3D4A498D09E055587E51F12F98331CC (void);
// 0x0000075E System.Boolean UnityWebGLSpeech.Example02DictationSbaitso/<GetVoices>d__24::MoveNext()
extern void U3CGetVoicesU3Ed__24_MoveNext_mAAE3F7A02BB4CC58CCCC989DB6BD987BB63CCCD8 (void);
// 0x0000075F System.Object UnityWebGLSpeech.Example02DictationSbaitso/<GetVoices>d__24::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CGetVoicesU3Ed__24_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m52B65A6687210AA4FB8BB4C690ADA13DF97D1061 (void);
// 0x00000760 System.Void UnityWebGLSpeech.Example02DictationSbaitso/<GetVoices>d__24::System.Collections.IEnumerator.Reset()
extern void U3CGetVoicesU3Ed__24_System_Collections_IEnumerator_Reset_m9F98C408BFCDFD6C23ED7A460E33534854BBC079 (void);
// 0x00000761 System.Object UnityWebGLSpeech.Example02DictationSbaitso/<GetVoices>d__24::System.Collections.IEnumerator.get_Current()
extern void U3CGetVoicesU3Ed__24_System_Collections_IEnumerator_get_Current_mBF17624D10518A8301364E0EE6CED011DC123301 (void);
// 0x00000762 System.Void CrazyMinnow.SALSA.OneClicks.OneClickBase::NewConfiguration(CrazyMinnow.SALSA.OneClicks.OneClickConfiguration/ConfigType)
extern void OneClickBase_NewConfiguration_m5B556E2708C7BAD895B40097F426ED8A17823DFE (void);
// 0x00000763 System.Void CrazyMinnow.SALSA.OneClicks.OneClickBase::AddSmrSearch(System.String)
extern void OneClickBase_AddSmrSearch_mFDB3C8112B382431780D0C0F94F58832610F80B8 (void);
// 0x00000764 System.Void CrazyMinnow.SALSA.OneClicks.OneClickBase::NewExpression(System.String)
extern void OneClickBase_NewExpression_mBF526AED5FC9AFF661FEF599E694215DADEB9BEF (void);
// 0x00000765 System.Void CrazyMinnow.SALSA.OneClicks.OneClickBase::AddShapeComponent(System.String[],System.Single,System.Single,System.Single,System.String,System.Single,System.Boolean)
extern void OneClickBase_AddShapeComponent_mFADA98251C1C2DEF86CC46DAC24A792DB4832A29 (void);
// 0x00000766 System.Void CrazyMinnow.SALSA.OneClicks.OneClickBase::AddUepPoseComponent(System.String,System.Single,System.Single,System.Single,System.String,System.Single)
extern void OneClickBase_AddUepPoseComponent_m555480449B5D5AFFD1E4B8F860BF1828425EDE2D (void);
// 0x00000767 System.Void CrazyMinnow.SALSA.OneClicks.OneClickBase::AddBoneComponent(System.String,CrazyMinnow.SALSA.TformBase,System.Single,System.Single,System.Single,System.String,System.Boolean,System.Boolean,System.Boolean)
extern void OneClickBase_AddBoneComponent_m59C2366E42758F5BD3C4992FF09D35F49846F712 (void);
// 0x00000768 System.Void CrazyMinnow.SALSA.OneClicks.OneClickBase::AddEmoteFlags(System.Boolean,System.Boolean,System.Boolean,System.Single,System.Boolean)
extern void OneClickBase_AddEmoteFlags_m1FA7A7B679AFC6BB5EA92F35857F31ECFFFF54C0 (void);
// 0x00000769 System.Void CrazyMinnow.SALSA.OneClicks.OneClickBase::DoOneClickiness(UnityEngine.GameObject,UnityEngine.AudioClip)
extern void OneClickBase_DoOneClickiness_mBF6AD23DEF0283BD29C1CC20DCC4B64387F3330C (void);
// 0x0000076A System.Void CrazyMinnow.SALSA.OneClicks.OneClickBase::Init()
extern void OneClickBase_Init_m659D9827623C96804BF09FECA49DEA0888720CAF (void);
// 0x0000076B System.Void CrazyMinnow.SALSA.OneClicks.OneClickBase::ResetForNextConfiguration()
extern void OneClickBase_ResetForNextConfiguration_mD78BDCFC3261C91C4D8C90926D9EFB8A555B9711 (void);
// 0x0000076C System.Void CrazyMinnow.SALSA.OneClicks.OneClickBase::ConfigureModule(CrazyMinnow.SALSA.OneClicks.OneClickConfiguration)
extern void OneClickBase_ConfigureModule_m9EC19B9A036BC3FC6C380A29A2055B0BED19702B (void);
// 0x0000076D System.Int32 CrazyMinnow.SALSA.OneClicks.OneClickBase::RegexFindBlendshapeName(UnityEngine.SkinnedMeshRenderer,System.String)
extern void OneClickBase_RegexFindBlendshapeName_m553122048E9780E7E5FAB387DAA9444B4F51E49A (void);
// 0x0000076E System.String[] CrazyMinnow.SALSA.OneClicks.OneClickBase::GetBlendshapeNames(UnityEngine.SkinnedMeshRenderer)
extern void OneClickBase_GetBlendshapeNames_m9455F0238F22BC0EA65D6773BEEC70FF1D9B198E (void);
// 0x0000076F CrazyMinnow.SALSA.TformBase CrazyMinnow.SALSA.OneClicks.OneClickBase::ConvertBoneToTform(UnityEngine.Transform)
extern void OneClickBase_ConvertBoneToTform_mE70FEEA8BA9C1DF6CD49C7C45A4A8E22DC54E62C (void);
// 0x00000770 UnityEngine.Transform CrazyMinnow.SALSA.OneClicks.OneClickBase::FindBone(System.String)
extern void OneClickBase_FindBone_m58D56D663C673F23BBC02B2712E67283B881EAE4 (void);
// 0x00000771 System.Void CrazyMinnow.SALSA.OneClicks.OneClickBase::ApplyCommonSettingsToComponent(CrazyMinnow.SALSA.OneClicks.OneClickConfiguration,CrazyMinnow.SALSA.Expression,System.Int32,CrazyMinnow.SALSA.OneClicks.OneClickComponent,System.Int32)
extern void OneClickBase_ApplyCommonSettingsToComponent_m649B8669D74BB0B951B19E682C1C2B7D316FAE28 (void);
// 0x00000772 System.Void CrazyMinnow.SALSA.OneClicks.OneClickBase::CreateNewComponent(System.Int32,CrazyMinnow.SALSA.Expression)
extern void OneClickBase_CreateNewComponent_mA0D124D4C6FEFC0DF0AE825C276469B9A99AD5B2 (void);
// 0x00000773 System.Void CrazyMinnow.SALSA.OneClicks.OneClickBase::.ctor()
extern void OneClickBase__ctor_mE60AC0F3F342FE0B92127CBDD93772C1D77A4AAC (void);
// 0x00000774 System.Void CrazyMinnow.SALSA.OneClicks.OneClickBase::.cctor()
extern void OneClickBase__cctor_m649E5B11980B53D5D631DC9E5346C00D95A4847B (void);
// 0x00000775 System.Void CrazyMinnow.SALSA.OneClicks.OneClickConfiguration::.ctor(CrazyMinnow.SALSA.OneClicks.OneClickConfiguration/ConfigType)
extern void OneClickConfiguration__ctor_m20499092FEBCC7E68BC05B656D4B23E41104D7E5 (void);
// 0x00000776 System.Void CrazyMinnow.SALSA.OneClicks.OneClickExpression::.ctor(System.String,System.Collections.Generic.List`1<CrazyMinnow.SALSA.OneClicks.OneClickComponent>)
extern void OneClickExpression__ctor_mD1FF5C71EEAA9ACD0660219B7CB9F73314B32B5B (void);
// 0x00000777 System.Void CrazyMinnow.SALSA.OneClicks.OneClickExpression::SetEmoterBools(System.Boolean,System.Boolean,System.Boolean,System.Single,System.Boolean)
extern void OneClickExpression_SetEmoterBools_m35A4E005358F131A41C8CF2B29D118EE31036ECC (void);
// 0x00000778 System.Void CrazyMinnow.SALSA.OneClicks.OneClickComponent::.ctor(System.String,System.String[],System.Single,System.Single,System.Single,System.Single,CrazyMinnow.SALSA.OneClicks.OneClickComponent/ComponentType,System.Boolean)
extern void OneClickComponent__ctor_m14FCA04E623512CE159FADECD336C4EFC59B0243 (void);
// 0x00000779 System.Void CrazyMinnow.SALSA.OneClicks.OneClickComponent::.ctor(System.String,System.String,System.Single,System.Single,System.Single,System.Single,CrazyMinnow.SALSA.OneClicks.OneClickComponent/ComponentType)
extern void OneClickComponent__ctor_m271849BBC4F322250343F86186EBE439A82DFE1B (void);
// 0x0000077A System.Void CrazyMinnow.SALSA.OneClicks.OneClickComponent::.ctor(System.String,System.String,CrazyMinnow.SALSA.TformBase,System.Boolean,System.Boolean,System.Boolean,System.Single,System.Single,System.Single,CrazyMinnow.SALSA.OneClicks.OneClickComponent/ComponentType)
extern void OneClickComponent__ctor_m37AA14B0C3D69007E15280978B6D4D7611C9DB4C (void);
// 0x0000077B System.Void CrazyMinnow.SALSA.OneClicks.OneClickBoxHead::Setup(UnityEngine.GameObject,UnityEngine.AudioClip)
extern void OneClickBoxHead_Setup_mB27D13C280F04C364B694576DE8FB7BAE5602672 (void);
// 0x0000077C System.Void CrazyMinnow.SALSA.OneClicks.OneClickBoxHead::.ctor()
extern void OneClickBoxHead__ctor_m58E8206392DF1268EA1DD36C07ADA02D4ED75FD4 (void);
// 0x0000077D System.Void CrazyMinnow.SALSA.OneClicks.OneClickBoxHeadEyes::Setup(UnityEngine.GameObject)
extern void OneClickBoxHeadEyes_Setup_mEE7FC2304E0EFC63A06726F747498051679C2CEA (void);
// 0x0000077E System.Void CrazyMinnow.SALSA.OneClicks.OneClickBoxHeadEyes::.ctor()
extern void OneClickBoxHeadEyes__ctor_mBAABE084D851340A63F3E77D08BE0CFAC7DC400D (void);
// 0x0000077F System.UInt32 <PrivateImplementationDetails>::ComputeStringHash(System.String)
extern void U3CPrivateImplementationDetailsU3E_ComputeStringHash_m93FD10C4CC1978E570BB23B36EB64D5DC41D3E8B (void);
static Il2CppMethodPointer s_methodPointers[1919] = 
{
	linguaScelta_Start_m59313CA922B740E382F7F0E3F5E41CFEFEA88925,
	linguaScelta_Update_m58F06DC24362408FFF1B18E071D05D84833FA45C,
	linguaScelta__ctor_mF8F3566234E092CE74571116F44C5D8AC8750225,
	ExtensionMethods_CTToTitleCase_m0FAF40693A916D1BA985195E30AA7B9753C11DE2,
	ExtensionMethods_CTReverse_m375B1B5F94367BC73BC054481C4528E1829758A0,
	ExtensionMethods_CTReplace_mE45837F5BF2ECDD64251F7DC85303D36308AFBB9,
	ExtensionMethods_CTEquals_m70FD226B78B4DD7ED0672F326B16568EA3D9B446,
	ExtensionMethods_CTContains_m52B3F3D1019BBD1746371264A2671656CDADB3C2,
	ExtensionMethods_CTContainsAny_m50A29F87C6A50663D9B599FEA1D1359EC07969A3,
	ExtensionMethods_CTContainsAll_mAB9F9024F8E76F4449FE666602718634956C2464,
	NULL,
	NULL,
	ExtensionMethods_CTDump_m9372AF9431740E725E0A0EE720EEB4B51EF8D626,
	ExtensionMethods_CTDump_mDD2831261D66781D294ADBFFBD9A397D4A5B4681,
	ExtensionMethods_CTDump_m2A792DFFEECEFF18E99A289916B9165C127E7883,
	ExtensionMethods_CTDump_mEDCBC052999C7F88ECF5897E865B6467FF1F452E,
	NULL,
	NULL,
	NULL,
	ExtensionMethods_CTDump_m59F8DE0A578B3B891B4D8C1ED38688E86940F976,
	ExtensionMethods_CTDump_m6E14577C017276ACD5DA587C029A664694E859C9,
	ExtensionMethods_CTDump_mD3A9D8E4C2FAF2BE628602C89238233DBE170FDE,
	ExtensionMethods_CTDump_mB00AF342A1C001008DDEE5422E6CE9386ADF044C,
	NULL,
	NULL,
	NULL,
	ExtensionMethods_CTIsVisibleFrom_m5E3DED552FD8B132DD4E2B700E4BC17BCE83E587,
	U3CU3Ec__DisplayClass5_0__ctor_m911B11C9D277BA8C81084E9C791C959F916AFA27,
	U3CU3Ec__DisplayClass5_0_U3CCTContainsAnyU3Eb__0_mE625ED60C205B0632CF345CF3D244BBFF5B5F8D9,
	U3CU3Ec__DisplayClass6_0__ctor_m2C47077153CBA7277CC4630ED5F134A341294672,
	U3CU3Ec__DisplayClass6_0_U3CCTContainsAllU3Eb__0_mE8356EE209DE1428FEDE8E17E777F9E74DE056E1,
	VoicesReady__ctor_m2E991762A67890BED2F92668E47C5E6A1685717A,
	VoicesReady_Invoke_mC7C30D92DB231AE7DDACEBBE4584FA7D6610473A,
	VoicesReady_BeginInvoke_m605F7A91294E4373FF460D5C40894C7074825EDB,
	VoicesReady_EndInvoke_m620BDD074D099E8EB6C1AFE42309899D7F88FD7E,
	SpeakStart__ctor_mC8F6A8E90D4E862BA229DD36DACAD43205808A9E,
	SpeakStart_Invoke_mB10CD8E3D429DD704FA20B63A1003025CFDF96E2,
	SpeakStart_BeginInvoke_mB59CFD4F5B882803EBF4FA7FC8E129052FA78498,
	SpeakStart_EndInvoke_m899FDCB6642DC34EE9398A00E2639E3D53609B3B,
	SpeakComplete__ctor_mDCCB0317BD968FBC32591D07D2EFFA5ADBA5BCAD,
	SpeakComplete_Invoke_m18B15193051921168800A643411DCD93A4C67BA0,
	SpeakComplete_BeginInvoke_m6A37E9B8DF0A67B0E2CF6DCAA029FCF6D45BCD38,
	SpeakComplete_EndInvoke_m9B50412701438A9941C0AB534668C9E3245E6168,
	SpeakCurrentWord__ctor_mD3DD6540F9FA5A64376CA92E5C7F680C13DC8EFD,
	SpeakCurrentWord_Invoke_m817FE783819F82D5F8C082B87E8477E7F392B5E7,
	SpeakCurrentWord_BeginInvoke_m68009DEE673D3146AB9901156B42D0607D8D4D1D,
	SpeakCurrentWord_EndInvoke_m492D4D826F7C0E500A8AE6149DFDFB689DED0CDC,
	SpeakCurrentPhoneme__ctor_m85F9B5C461F9E02B0B9C1A353104E79DCE742781,
	SpeakCurrentPhoneme_Invoke_m79BAED65CCE030F2EFD292C804F64DF7E6312FE2,
	SpeakCurrentPhoneme_BeginInvoke_m6A311B6BECAAF898B684AE011535825B01CC21DF,
	SpeakCurrentPhoneme_EndInvoke_mD7D228667F2C57C9945E1B929DB644FC20090762,
	SpeakCurrentViseme__ctor_m2AB40082B61DC510D9E15E35A7B70F748D76C3C3,
	SpeakCurrentViseme_Invoke_m90CEC03BD48D4712D5E37D2204050DC01A1365D9,
	SpeakCurrentViseme_BeginInvoke_mAC51100A7F16AA28BB5E19ADD9CD1F00053D945E,
	SpeakCurrentViseme_EndInvoke_m38D4EAF67498A1DED2B994BDA9059D1D97E7FBA7,
	SpeakAudioGenerationStart__ctor_m085B870E9D2BFA2A3290CF8E94B0EB28D4E49AE9,
	SpeakAudioGenerationStart_Invoke_m018E44BCD9A5BCCDB2B9119CCC5E74DDC762D25E,
	SpeakAudioGenerationStart_BeginInvoke_m04DD1924D0976BD1020681FA6915C9C74384579E,
	SpeakAudioGenerationStart_EndInvoke_m63E807138EF86A43E0E525414861C8598A5747E4,
	SpeakAudioGenerationComplete__ctor_m04EDFA1D52E9C8E55A384FC8AEEEAC4D73FBA374,
	SpeakAudioGenerationComplete_Invoke_mCC83091AC88B694C3FEC1B62317EA5B1606744C3,
	SpeakAudioGenerationComplete_BeginInvoke_mD6FC7D4EA11E01A14D4D8D2EC7C588F21AEC4BA4,
	SpeakAudioGenerationComplete_EndInvoke_m846AD37D9A02B868C92080A30A720A528FBAF73B,
	ErrorInfo__ctor_m40D60E1C66F63A6B8EA380C57C0B64668F29FB27,
	ErrorInfo_Invoke_m37FF498EA4FB828E412308686E5B66D1DE67E672,
	ErrorInfo_BeginInvoke_mB1E6E5DC2F13C65B3819D170BF2B2BE881277C68,
	ErrorInfo_EndInvoke_m2FE5921B97DFC0B9C694E9E0D602C5CE04841EE3,
	ProviderChange__ctor_mBFDDB20BDBA8A5FF726D10D88591C08E61352AEB,
	ProviderChange_Invoke_mF672B1FE34C1B96CCDE0AFAF459731E8CC6D6707,
	ProviderChange_BeginInvoke_mE4461E00CE7B104260E406EA09901CDA5D49B9E6,
	ProviderChange_EndInvoke_m51464E4AFFA4F04524B00B1E9C00E4144B60DBC6,
	AudioFileGeneratorStart__ctor_m8866386226604F73C003CA113B7C500545B41CE0,
	AudioFileGeneratorStart_Invoke_mDB4A7D256D502CFC6BE0449053620D9DB8A7E2E4,
	AudioFileGeneratorStart_BeginInvoke_mC0ED8EFF08BD87E536E3CA907904B3C81CD220F2,
	AudioFileGeneratorStart_EndInvoke_m2D75793424160400D75FE4336D5DF9B379012402,
	AudioFileGeneratorComplete__ctor_mE0F014F21CD6577372BD0F821D85D5B4A1E9DE56,
	AudioFileGeneratorComplete_Invoke_m00432CD9175DFF6DFE75B4BE4035B17844CB8F15,
	AudioFileGeneratorComplete_BeginInvoke_mD8C5D0D621B661099483A65EDCA4D9FC1B70BE49,
	AudioFileGeneratorComplete_EndInvoke_m20F29FE67FBC71F62E78CCC58A758A99F7777ED8,
	ParalanguageStart__ctor_m059C70F86F595A3903C6B9B26A2F5E475786EA94,
	ParalanguageStart_Invoke_m6175485770ABB06D1C6E594B691EB9D71A6B7FD5,
	ParalanguageStart_BeginInvoke_m807A5A04827C83C63A66A21B06E4EF42C5A3CEB7,
	ParalanguageStart_EndInvoke_m378719583F5D00D1FE89EBE2B709C17681608FAE,
	ParalanguageComplete__ctor_mCB3784A6774C1CFB65E09A68A7AF7A344271F561,
	ParalanguageComplete_Invoke_mD7881B18B3A8FBBEFD8EB0DC96807A0BF1390B71,
	ParalanguageComplete_BeginInvoke_m2BC5C4EB9BB43D55DB1E3E377C0BCE85D1CEBA84,
	ParalanguageComplete_EndInvoke_m5C1C7843CFF25D4656CF2A3D23838A25B8989DF4,
	SpeechTextStart__ctor_m2B4286BDE7221D08A8A7F0B7E38539902257EF94,
	SpeechTextStart_Invoke_m5F4640E1AF60CCA9AB0212FC00F0ECBF949AE30B,
	SpeechTextStart_BeginInvoke_m88A250F116921AC77BD12F24A360F26B7AEBC392,
	SpeechTextStart_EndInvoke_mF44611A887C90B4368F16A9E4A5BF91D0C6964CA,
	SpeechTextComplete__ctor_mB086443FA20BA098F0A53E3858A80ADE3A1D5CCE,
	SpeechTextComplete_Invoke_m28FB11109EADC3828CE1A2EAF8408510DB0B14B7,
	SpeechTextComplete_BeginInvoke_mAC7A21F2BBD455F444D88A7682BC111DBFB17179,
	SpeechTextComplete_EndInvoke_mF35924B3725393FC4AAF6CDEF3A5C487CCC78A7E,
	LiveSpeaker_SpeakNative_m26C65EC54901B02AD269145B611FBD2C77B2C98B,
	LiveSpeaker_SpeakNative_m1325133C367E141BA3BD51E6313FA62CAE2143B5,
	LiveSpeaker_SpeakNative_mFA2E8646EE171F27AA70EB6A4B9BACD104A3B42E,
	LiveSpeaker_Speak_mDE0121CBFD016B74DF2251276A02C2841A4651D1,
	LiveSpeaker_Speak_mD9F352E1859BD25DA34CEC12B2D8F2636C0FF780,
	LiveSpeaker_Speak_mC3C7A08E0B9A3FAAAC311214A1B4B0CEA9EACA77,
	LiveSpeaker_Silence_m4FCD3587003FC2DD23DF5548115EFEEB034E49E9,
	LiveSpeaker_SetVoices_m8CC34539AA96CD8B5FE150B7D97B9D2C241E2CCE,
	LiveSpeaker_WordSpoken_m26AB98FE814293A57D4745254EA2B0C0227953AF,
	LiveSpeaker_SetState_mA42E6FFB85C643B0849D24064BC120BA64F8CC81,
	LiveSpeaker__ctor_m549443194385E6C25A7D32945D3C2CC0458C9A86,
	LiveSpeaker__cctor_m4E86C8E8D7A8D5402563E30271545F2EE87C597E,
	Speaker_add_OnVoicesReady_m2C6536AC1C4E0D45B46F62320A5228D7703A977C,
	Speaker_remove_OnVoicesReady_m7419320D3E1D51E3A1947C7D47BA952B15E13CAE,
	Speaker_add_OnSpeakStart_mB23F67B6DCDB1E55BDFB9EDE7AF3D595FCA553AE,
	Speaker_remove_OnSpeakStart_m2C2E372ECFA4539BBBB6E60CE531684E9A526585,
	Speaker_add_OnSpeakComplete_mB08AF256448A5039092006149E7F2E696C158FCD,
	Speaker_remove_OnSpeakComplete_mB80E093C3FF9490783115A76BE7265D8F903497D,
	Speaker_add_OnSpeakCurrentWord_m2F8106FFAF9C5919D034F136D36EBA9AA73132F4,
	Speaker_remove_OnSpeakCurrentWord_mA5977059F97F2BA6CDB22C7B7DC6C88B7ADE5B6D,
	Speaker_add_OnSpeakCurrentPhoneme_m859ACD2CCD8B863B19E0EB0B2AA7365342D6A75A,
	Speaker_remove_OnSpeakCurrentPhoneme_m72F6687DBF8F03FBABE61C4F7AD36FDB97F2C4D0,
	Speaker_add_OnSpeakCurrentViseme_mCA6DFF18B8643C22207A12C58B4404CA897971EE,
	Speaker_remove_OnSpeakCurrentViseme_m320534421F19BA06CE5ABE7AB3584C3C314AD38D,
	Speaker_add_OnSpeakAudioGenerationStart_m1AFD632C57F53D96D77A72F8D1A2AD747F1A0E87,
	Speaker_remove_OnSpeakAudioGenerationStart_m2542F624DA998E6D3008ED6C3B57A87D3DEC2C6C,
	Speaker_add_OnSpeakAudioGenerationComplete_m9998D72E8FBA1D6BFA226BF38766E54EBCF4F819,
	Speaker_remove_OnSpeakAudioGenerationComplete_m16495ADEF5818DBF2F83539A414CCE82E848BB89,
	Speaker_add_OnProviderChange_mD7F71DAE9793CAB6D3F1BCE44F5E7849ED0E4C6B,
	Speaker_remove_OnProviderChange_m82DCFCC8E4B2C7FAAB68EA9528A94CA9883D13D6,
	Speaker_add_OnErrorInfo_m98EE2D6190853E7F161B5B972FD774474E3BBE01,
	Speaker_remove_OnErrorInfo_m34726E329BD8EB057FB1AAB19F1B756A88D5CAE5,
	Speaker_get_SpeechCount_mF7EA7146F93FB4134ADB9F1BC92CBA8C1E97872A,
	Speaker_set_SpeechCount_m7403028ACC1F1F42274B7D8066020B24F1F07F11,
	Speaker_get_BusyCount_m9C5E0DFCDBAA19CAC6C4783AFEABB682C409D083,
	Speaker_set_BusyCount_m6D4AE1BB35A1C8CE8F61041E02865839F3E058BC,
	Speaker_get_areVoicesReady_m55042C89D6ABB754FF15AC1E880F6FAC8CE6C318,
	Speaker_set_areVoicesReady_m2F79E5855BD2C1E6BC59F3A29C7972FAB5C49DE7,
	Speaker_get_CustomVoiceProvider_m8917B7A828D726AB9AFAB2C525CB7DADC7915AD9,
	Speaker_set_CustomVoiceProvider_mC27D0B7C08B9B6C399CCA2DD066D9112FBAD01F8,
	Speaker_get_isCustomMode_mE1B205DE1900F784556098EE85F428A443ADCBF1,
	Speaker_set_isCustomMode_mE8DF7DEF5E17DF5748D3756E76C586DA31E70846,
	Speaker_get_isMaryMode_m5411FB3E9579589AFB66258B778C52A60C227E2B,
	Speaker_set_isMaryMode_m3105879511912234EE6B3C2EF889B8AC31FC1293,
	Speaker_get_MaryUrl_m462763EB8DA96154B3F2C505C20E2B07BC12A73C,
	Speaker_set_MaryUrl_mDA17CFFFF74ADD31ACF11218564A218FC1867277,
	Speaker_get_MaryPort_m3C4D4CC2E4117B49E99931D711A6EC4AFFEB2352,
	Speaker_set_MaryPort_m2AC97A8B3B3F8B3C8C86D9D7DF28A06DD6E4CCA4,
	Speaker_get_MaryUser_mF4DC7DCF158E813EF982B0DA73F68DD4E05E4643,
	Speaker_set_MaryUser_mB01336A49FF20D601731F37E981E7D6212A23DE4,
	Speaker_get_MaryPassword_mB423A30B194FFFF4FF12BF77B34D570E0236D082,
	Speaker_set_MaryPassword_mD6FC21E08ABBD4A4F88650C7A7526B0C6B51AB64,
	Speaker_get_MaryType_mFEAF3F5C79DD2BDA6C9264ADA009C504C09E74EE,
	Speaker_set_MaryType_mDA3D089DC92CECD4B0D66A5526AD27D1EDA29359,
	Speaker_get_isESpeakMode_m472E561526F0DEBDD1E4CD432EE55F95EDDC7DBE,
	Speaker_set_isESpeakMode_mDBD0268FC62F7334EA7A2FDD64EAA2D3D0303C04,
	Speaker_get_ESpeakMod_m43C00A86F33FF91C54E08F83114797791B5CA880,
	Speaker_set_ESpeakMod_m174D2521A804215769E6F42A1FC278B78BD79EE1,
	Speaker_get_isWSANative_mA4881F7111959D1EAD40EA42573937A06CA19110,
	Speaker_set_isWSANative_m4ADD170835113217FD8C02C357B8C5B56566EBF0,
	Speaker_get_isAutoClearTags_m5F6247D56A775D14D2DA15A36577F45EE2DDB75A,
	Speaker_set_isAutoClearTags_mEF89AA8D1A868F6039C254E6ABE5D509D96E0198,
	Speaker_get_isSilenceOnDisable_m7A06583380626B6F910F35D87DAD9EB06D26E453,
	Speaker_set_isSilenceOnDisable_m944D119AD39924FBB54CA3E634A28253029ABDDC,
	Speaker_get_isSilenceOnFocustLost_m1C26DB3EF729A695F16DB3925A2057052985553B,
	Speaker_set_isSilenceOnFocustLost_mC6EB5BF043580B22037A7E60887132E6DFACBB6E,
	Speaker_get_isTTSAvailable_mABB6E77CAF5AD9BCFC93186AFFF22AF454EF1065,
	Speaker_get_isSpeaking_m451DF855F5FA987EB047E5BF15326C68EDEA5BCE,
	Speaker_get_isBusy_m1737B40C42A247CDFD42F313A321EB822E6C780D,
	Speaker_get_enforcedStandaloneTTS_m2B69DCFD8736769BE2716D304570F88D3F915D6B,
	Speaker_set_enforcedStandaloneTTS_m8E0627900FA976E4F96CE064B22F6A19A1952DC6,
	Speaker_get_AudioFileExtension_mBF7A5F96E05921A6AA38548ED0FE5370E3D98302,
	Speaker_get_DefaultVoiceName_m7403EA51852911F8951A3A6499EBB0F3434B36FC,
	Speaker_get_Voices_m79A2558BCEAF57EA7ED84768134D3EAFADEE4BFA,
	Speaker_get_isWorkingInEditor_mC146C7E042924073ABFEB1AF2A36669BAE203CEB,
	Speaker_get_isWorkingInPlaymode_m3B676C4F915CC220A82DC65213F36C9FB7B56748,
	Speaker_get_MaxTextLength_mEF99D1D6EE86E97621779F9A34EA962657D2B34B,
	Speaker_get_isSpeakNativeSupported_mFC9D681F3A0B92EE6345F290554C05976653849E,
	Speaker_get_isSpeakSupported_m288F80D10364982BDAE95DFFF6A828B4B14E0A24,
	Speaker_get_isPlatformSupported_mE0E9444A04687B754FCF743FE4ABF6FAC728A491,
	Speaker_get_isSSMLSupported_mBB01EBFE8FC7C857D26BB48EC9FCE46CA1B6A840,
	Speaker_get_isOnlineService_mD92FBCCE21144D3FC4CBA486E8807B0FD0B892FA,
	Speaker_get_hasCoRoutines_mC6184BFBE8F58505A13DDCB35B12B9884B2B22FF,
	Speaker_get_isIL2CPPSupported_mD5ACC31884F707892395AA0CAF68E540B4E7F70B,
	Speaker_get_Cultures_mCB38D08C1CB0E583F0318994A2F5328FDF1F8157,
	Speaker_OnEnable_m5889EC83209687C5F6588EA93170781700F7CFDB,
	Speaker_Update_mAA944A1246793F8F129520FE9C20BC14E2A1C121,
	Speaker_OnDisable_mFD34FF33EB4CA156039D4113C139B863CEBE20E8,
	Speaker_OnApplicationQuit_m63BFA73BE06ADF786E75BDFD85E90708C62CF44B,
	Speaker_OnApplicationFocus_m587F812882AED2A945E029DF95FDF93541BF69B3,
	Speaker_Reset_m9A44736DAF32EE4F2B34A726B68AD6F416694F64,
	Speaker_ApproximateSpeechLength_m412C280EC1AAA4C44A4F5376525A10C601544B3B,
	Speaker_isVoiceForGenderAvailable_mC37F524A619127C6C7692A6328E56D32ECC2F18E,
	Speaker_VoicesForGender_m77B1EB3F245D2269446CE505C06664AE40293E4A,
	Speaker_VoiceForGender_mEE325533209C4A18547DE5F759DD95B847C31425,
	Speaker_isVoiceForCultureAvailable_mFE795122FDE7094086837D9B324935C1AB8C891D,
	Speaker_VoicesForCulture_mC8E2CDEC9816223B1993B4C1A41143FBDEEB3EA0,
	Speaker_VoiceForCulture_m4DC4FFF248365A3647FBB1F21092A3BB6734B0FF,
	Speaker_isVoiceForNameAvailable_m341CE2EAC3EAF53E49913119E8895352335D7334,
	Speaker_VoiceForName_m2EE14B5AE7D4E71D9D752B29D9FAA48E4962FC1C,
	Speaker_SpeakNative_m657220DB6F4C77A39206BA1E59D5DAAB83F01B74,
	Speaker_SpeakNativeWithUID_m57589F7B8E625E31354F72888667A374823F63C4,
	Speaker_SpeakNative_m5755877CAC5D3A7BB9CE12089690A48E741DDD20,
	Speaker_Speak_mDA3F118104E867A3C8F0C4490B48202F7902862C,
	Speaker_SpeakWithUID_m8FD56871025B35320A9E13FAF7C909455EEB82B8,
	Speaker_Speak_m285139B02BE8D2669A1B38674EA0908CAB01133D,
	Speaker_SpeakMarkedWordsWithUID_m42D29C28FA8F6E165D6E9A3568031D48D7D790BF,
	Speaker_SpeakMarkedWordsWithUID_mB1725D1B6A33404F3DCDDC1692FFF76532987869,
	Speaker_Generate_mC88CA0FAC1AA18A02681EBB6A942EC1153A0BB0B,
	Speaker_Generate_m2E0A8444D8F7FA2E89E8C12F715D38E9B437B70D,
	Speaker_Silence_m392C0C7A809450CFF342688A3B39BBA3831C843B,
	Speaker_Silence_m82C02F06AB135EB475EB2FCE7C15DBD12FD397D5,
	Speaker_Pause_m21C57142E8204AEE249B5E9DB9566848B6F7237B,
	Speaker_UnPause_m7844F76783BAE24C4E03D54B71F907713C70D363,
	Speaker_ReloadProvider_m119480D6BE9D9888C07410815C356BE4195A3818,
	Speaker_DeleteAudioFiles_m4A14E58B2A2E8E080D5BB9A98FEC240A04188B70,
	Speaker_deleteAudioFiles_mE64F007ADDD2532C111CC9A56E01D92538C0EB0E,
	Speaker_initProvider_mCD737B611326BBA700740671C45C3689F78D40F8,
	Speaker_initOSProvider_m27596A06A6889FDAD58587C8D2B3E7B800D2FF90,
	Speaker_logWrapperIsNull_m2CCFDDFE3899FA40CD7303C310A0317144317A16,
	Speaker_logVPIsNull_mCC3E11A1D9D1FE85294F2A35BFDA2A6BA4E65794,
	Speaker_subscribeCustomEvents_mC4F65314DB36E65BFE827E55F82B55E32C2F9A44,
	Speaker_unsubscribeCustomEvents_mC6E53C1FED6A86B7928B15DD66E7485540FE35E1,
	Speaker_onVoicesReady_mB9EE70544580BC798D052AA72829BC832E4F351C,
	Speaker_onSpeakStart_mC2EFB288F33E5AF29194FB13DDFD34D25B8D66C9,
	Speaker_onSpeakComplete_mA1FBF18B88A6296C94EEA6E2A8B04F80F2995684,
	Speaker_onSpeakCurrentWord_mF5DE580865CD75E1CF0334E2EB2EA387F47E1FB4,
	Speaker_onSpeakCurrentPhoneme_m8396CA7663C128B5BDAEAC2B2CE9F0ABA9A52193,
	Speaker_onSpeakCurrentViseme_m7AA2B1DF18C2F3B9FC1D721E1083D9AF942CF69C,
	Speaker_onSpeakAudioGenerationStart_m4F80EE307EED26BF88B3AD3E36BA2CED8E662803,
	Speaker_onSpeakAudioGenerationComplete_m8EB5C99D1002633FF2FA8C240C19E9F65E0E9F3E,
	Speaker_onErrorInfo_m5A161DC6416AC4BAEEA9E3B8D7BA75FBE6C96A11,
	Speaker__ctor_mD33A4862B594ABE4453A8C547CEB97E2AAB44D79,
	Speaker__cctor_m8BA554664C3BF38906C587BD65F3CC77A7289D40,
	U3CU3Ec__DisplayClass171_0__ctor_mFD641F7697739DCC03DF7BB1679DD9909336BAFE,
	U3CU3Ec__DisplayClass171_0_U3CVoicesForCultureU3Eb__0_m96A3511B67132C662B4A06B0207A7670BEEAC058,
	U3CU3Ec__cctor_m99B5A242DEE71B6DFC6DF8006C37510E40362CC4,
	U3CU3Ec__ctor_mF9848D30ACD5EFC99FEA03F3E6F78B144A143AAF,
	U3CU3Ec_U3CVoicesForCultureU3Eb__171_1_mD53CF0784FB8B6D1FD437794BC0FE9DD1303FB95,
	U3CU3Ec__DisplayClass190_0__ctor_m1F7184DEC71431F28A6B7A2AB8935BD158A6212B,
	U3CU3Ec__DisplayClass190_0_U3CDeleteAudioFilesU3Eb__0_mF932CC0B8E8DC74E75C9EA73D32A831032500017,
	VoiceProviderExample_get_AudioFileExtension_m359944D955D65ACF22047912FF813415528A46F6,
	VoiceProviderExample_get_AudioFileType_mABEDD96356CBDEFABDC04FF1F0C69A7B8A47FDAC,
	VoiceProviderExample_get_DefaultVoiceName_m20E0967E6A0D39AAD2C7B4E98102E9DB6267E233,
	VoiceProviderExample_get_isWorkingInEditor_mEF00E3D91C67A013FCF8B51AE2CC7BFF1EF26672,
	VoiceProviderExample_get_isWorkingInPlaymode_mA9EB3E1DDD3EB653F30C96214254161DF01F87E6,
	VoiceProviderExample_get_isPlatformSupported_mB1181FD48BD9248A54DDF06B031802FBE25F3AAF,
	VoiceProviderExample_get_MaxTextLength_m803C12286B895D81B78D3130036D780FDCF5FCA9,
	VoiceProviderExample_get_isSpeakNativeSupported_m9D97D112D128AD620C4443C7C9EB47A38C5A0DC5,
	VoiceProviderExample_get_isSpeakSupported_m26D549D961590292A057E5827A479D438093A33C,
	VoiceProviderExample_get_isSSMLSupported_mD844EF4A880B4DFDCF5B9283FD4E32D8480ADB27,
	VoiceProviderExample_get_isOnlineService_mCEBA53777B091ACA5941AD5B8BDEF468AC44C517,
	VoiceProviderExample_get_hasCoRoutines_m1BAD5B5E827CD11C8AA78D4B3BE30E6D5CEA10CB,
	VoiceProviderExample_get_isIL2CPPSupported_m62F23B593FAB84988DB332FC7656691E071B0666,
	VoiceProviderExample_Load_m510C50681197E6243E41D164F9F43C3CDF799824,
	VoiceProviderExample_Generate_mCEF1A347318B74AA723F6ABFA9C2EB48DC2DF9C4,
	VoiceProviderExample_Speak_mBDB765FB3CC330D5B5FC491DC795FD5297EDF0E7,
	VoiceProviderExample_SpeakNative_m9AE9125ACB3493351913B96173D7DBC83E2EAF13,
	VoiceProviderExample__ctor_mEB1B521BF2B43009161F427149A48F7E0717FEF7,
	U3CGenerateU3Ed__27__ctor_mBE7648CA9BC983832FB40122AE1F309C028FAE80,
	U3CGenerateU3Ed__27_System_IDisposable_Dispose_m31A13EDD00230227067357AEDF3081F4DFE30A20,
	U3CGenerateU3Ed__27_MoveNext_m2BC80C6ED45CABECC111A777D929EAC1C52725BA,
	U3CGenerateU3Ed__27_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m5D2AC763594283B97AF22C0ECE832F4019B1DB85,
	U3CGenerateU3Ed__27_System_Collections_IEnumerator_Reset_mBD3F2BBF7EB868747C7D15542F2E9089C61F50C1,
	U3CGenerateU3Ed__27_System_Collections_IEnumerator_get_Current_m1BEE20547347457E10EF46105AA485378FA8EEE3,
	U3CSpeakU3Ed__28__ctor_m00F559E690F57E2B4D1C6180D7BCF76110BBC6E1,
	U3CSpeakU3Ed__28_System_IDisposable_Dispose_mC282884D02DBBFF1E195D8728EA2B8FF2D68351D,
	U3CSpeakU3Ed__28_MoveNext_mD4CE62F78D7F43EDED79FCD5B972986238D92775,
	U3CSpeakU3Ed__28_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m155241265B291EBEE2FD2381BC48B220AA3BA054,
	U3CSpeakU3Ed__28_System_Collections_IEnumerator_Reset_mC4A4CC3360ACD12464DEE845E9CE608105356EF3,
	U3CSpeakU3Ed__28_System_Collections_IEnumerator_get_Current_mA9B5A00AE897B39C9DEA60B9E4DA3135421125EC,
	U3CSpeakNativeU3Ed__29__ctor_mAD9F48CC410BA068A8CBF1E72BAD7EAFFE682B0A,
	U3CSpeakNativeU3Ed__29_System_IDisposable_Dispose_m309B55ACD321D7AC45AD632C1B4EACED83D47100,
	U3CSpeakNativeU3Ed__29_MoveNext_m1E7F5074F853AD1623F3C02F25C95016623B575D,
	U3CSpeakNativeU3Ed__29_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m7C5EDA7375CBEE70E45B133C63BED52FBD448E07,
	U3CSpeakNativeU3Ed__29_System_Collections_IEnumerator_Reset_m5F62E754EA07230AF596C779B5484274CCBFB409,
	U3CSpeakNativeU3Ed__29_System_Collections_IEnumerator_get_Current_m64E0A5D15216AFA6D8650F2B8453143B89539613,
	Config_get_AUDIOFILE_PATH_m42F7E5CF3F9B15AAA02D8A65141D60CA98ED25C7,
	Config_set_AUDIOFILE_PATH_mF8DCEF7B6C8401E91989EED74BB4F038A54E23E5,
	Config_get_TTS_WINDOWS_EDITOR_mDC6567EB7C12D7F64FDF6CF06377465DF7AC7F65,
	Config_get_TTS_WINDOWS_EDITOR_x86_m06A3291CE8C566BC341ADA6A77D1F9601275E6D8,
	Config_Reset_mA4E521995FDD11F2A5711A171F2EE71DE1A93379,
	Config_Load_m70C1BAA1E4101A0A37E466DA54C5E01821A63F61,
	Config_Save_mF4F0C7CC3728BA922747AB9CAF29CA276A30193C,
	Config__cctor_mEE2B7AE6033E0C138564A6FACB5A0319CDDBC053,
	Constants__ctor_m35F7FD108807C339B3EB9B3F290AB1709BA726BB,
	Constants__cctor_m5A6095E120BD98AA88A791C270E1556E2B60ED70,
	Helper__cctor_m457937714ADA94D61825FB564985128B55046773,
	Helper_get_hasBuiltInTTS_m82B916D06756371DD8CE4B2C27F7512163C95C38,
	Helper_get_CurrentProviderType_mBC3BEEF008B7FF523816D31D4C21855C1D21AEA6,
	Helper_StringToGender_m48F39C3BD1DEA77AC4C8BE17181B822500FAA077,
	Helper_AppleVoiceNameToGender_m2747FBA07F06C0468AB5344D38E8D2972786F954,
	Helper_WSAVoiceNameToGender_mB6F4279630D1BED7B1EE1FCBA0A6E588BE72B611,
	Helper_CleanText_m93A1DD6658F52066E80C7DEF6F54A5EBBEF986AF,
	Helper_MarkSpokenText_m927EDD18E2DFB0230279F9CB0491281BBE437194,
	Helper__ctor_m93B5A01DDC3FFC46837922F5E83089992E4086D1,
	WavMaster_ToAudioClip_m65CF64BD05B7E577B889227DE04C6755CDDCCDA8,
	WavMaster_ToAudioClip_m9FBE5BD02C7A764FB8DE2BF83A2CB6885EEFC864,
	WavMaster_FromAudioClip_mD3411FAAEDBB48354817156342A8E5F4ED8BA11B,
	WavMaster_FromAudioClip_m31105CFF0DCED281812F3C6F8ED00BA239FC0945,
	WavMaster_BitDepth_mA01F95FAE0614DEEB3E02EBD0BDDA1623ADC7AD1,
	WavMaster_convert8BitByteArrayToAudioClipData_m3FB79D7CDE527A2D85D76C116A67BD7EE4FCBBAA,
	WavMaster_convert16BitByteArrayToAudioClipData_m6E338CC1527CAB17CF3E2CC75C4DA61AE371EBDA,
	WavMaster_convert24BitByteArrayToAudioClipData_mB1403BA352CF5E86F23B38891F4E45843B60C5DA,
	WavMaster_convert32BitByteArrayToAudioClipData_mB834332E91F6009C9F22ADD04662F44F8AB2BA15,
	WavMaster_writeFileHeader_mFFB5D831814D2CDAB3BE1B2827D855C15670AB70,
	WavMaster_writeFileFormat_mAAAD9178251EB06EE397C38159D877F866CDABA1,
	WavMaster_writeFileData_m4B480990298E7090D74468A05812A1FE2A56F39B,
	WavMaster_convertAudioClipDataToInt16ByteArray_m87AA4F874F401251336D669786133F628D619C3B,
	WavMaster_writeBytesToMemoryStream_m794D53302594D25EE580A5C6A1638B2DE86DD762,
	WavMaster_bytesPerSample_m451465CA58E4ADB0FD1873136036164A94A055AE,
	WavMaster_BlockSize_mEF780ED07E684D6310EAE4676928B1DF9ACC0EE9,
	WavMaster_formatCode_m4491A0826DE5BC50BCE0F9C4BF886455C5270BE1,
	WavMaster__ctor_m5BA3CD59FE47956E952FD47CC8B3FDC58D5A491E,
	AudioFileGenerator_add_OnAudioFileGeneratorStart_m743ED03464D1055DCB460CCEFCBAD74E358920CA,
	AudioFileGenerator_remove_OnAudioFileGeneratorStart_m6D26647D06F077594C1958D4BC859F2096469F5B,
	AudioFileGenerator_add_OnAudioFileGeneratorComplete_m2F2E2EE5F1C08D940F7566840708FB86CE653033,
	AudioFileGenerator_remove_OnAudioFileGeneratorComplete_m4C3B41D883DEA640E251EB37BA493CF90FC7EAE6,
	AudioFileGenerator_OnEnable_m0E8F52CE84CD5D7350C75EFABCFF215E5EC1F5E1,
	AudioFileGenerator_OnDisable_m5B8DCF4A33BCBB1D595619C91964EAAE23B60170,
	AudioFileGenerator_OnValidate_m118FF61EEB020E3ECFF1A6644D643EA9160F8E69,
	AudioFileGenerator_Generate_mD4AB0C5327B213F50D7245A827629EA8BD0F92F6,
	AudioFileGenerator_generate_m4FEFFBF2EFCD1E9AE3710DA8C6DA480C781F70F9,
	AudioFileGenerator_convert_m85557D268CDF462EC447E7FB578E542F8773426C,
	AudioFileGenerator_Normalize_m2E2E2896E7425B6CE5F38D9AD0682171344C8DE7,
	AudioFileGenerator_GetMaxPeak_m72CA42B5E8B15B5C405798EE9CC26DE214999487,
	AudioFileGenerator_prepare_m1B317C05D8F67ED3C41A0FC1503960BB721F71DC,
	AudioFileGenerator_onVoicesReady_m0C62C5B22F4A8C91574EEC80E10D05F5ABCBD4D5,
	AudioFileGenerator_onSpeakAudioGenerationComplete_m5BD832FD9C7DD6FD0CC9AB1A5916575E10EB6212,
	AudioFileGenerator_onStart_m001F23D9773434D24FE46318E745A85FFB0538F6,
	AudioFileGenerator_onComplete_mB02C329697B3C3A43E9FF5136C23FE3ED8F97B68,
	AudioFileGenerator__ctor_m0FA32E200D5E84EF0558924637063979CD5237F1,
	AudioFileGenerator__cctor_mBF501FD01085E2F052380C9C6C55C775A543A8D5,
	U3CgenerateU3Ed__19__ctor_m304D082A586B1520066B6F1F5D70922AF6AF4358,
	U3CgenerateU3Ed__19_System_IDisposable_Dispose_m9637C84D3DE5BA34D03CD47F7254960ED4DFFCB6,
	U3CgenerateU3Ed__19_MoveNext_mE689A3AEB6F43F1D6EA33E0B9010660F84363C55,
	U3CgenerateU3Ed__19_U3CU3Em__Finally1_mD2D123331A4179A30AC59FEF240079C1978D21F7,
	U3CgenerateU3Ed__19_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m3C28CECD7AE53E425198C381D87112731DEA0FB7,
	U3CgenerateU3Ed__19_System_Collections_IEnumerator_Reset_m45465FCA07B5C00BE97AA285379D2880E212DD73,
	U3CgenerateU3Ed__19_System_Collections_IEnumerator_get_Current_m1990C2C28E0F67CD67DE84F9E86ABAEE4EDECC3F,
	ChangeGender_Start_m5A0F0A65CD0663A25AC9F6EBA47B269B31AB77E0,
	ChangeGender_OnDestroy_m184EE2644851FC756F6CB29C1003B26879ACA91A,
	ChangeGender_GenderChanged_mB0A1A752AEF791F40566145DD56F2F932991A8B7,
	ChangeGender_Change_m88A50EE20D991FD1BCB53655F9393131C90BEA87,
	ChangeGender__ctor_m5EA4FC2B9CEEB64303C6170D96BF2B443F7E9595,
	Loudspeaker_get_isSynchronized_m3AF3CFB842F8E463303ACB21A936C8EA361E8FCD,
	Loudspeaker_set_isSynchronized_m85DB18CB156FE3A4022EE66E3FDDFF4B153F1D4B,
	Loudspeaker_get_isSilenceSource_m6CCC9AA9E6785EB4C2682977FDEFB98447ACB3D0,
	Loudspeaker_set_isSilenceSource_m527FA908E5922D0BC8FCF8393D527713C6217C4A,
	Loudspeaker_Awake_m6CEBD62FDC5D19707D7FD1407CCB30B70C6845F2,
	Loudspeaker_Start_m2D1889D6CDB30CE97E63A50788F623AD2E524485,
	Loudspeaker_Update_mD4DDBCD40FEC32428327BF4370EAD11C48AA0AF0,
	Loudspeaker_FixedUpdate_m7227004C15B7BD5ECF4B9C6B9C761AE60CEDFEFB,
	Loudspeaker_OnDisable_m4ED73705A797F8D7CA2BB7F93D5153D68EF47D99,
	Loudspeaker__ctor_m1B24204A0ABE94CA91D9A21A362562287DDEE085,
	Paralanguage_add_OnParalanguageStart_mC6A3DB677061F3141F320428585BC6D6EC7F7455,
	Paralanguage_remove_OnParalanguageStart_m7D76377E8AAFC53BF601283F44CFFF8BD9164215,
	Paralanguage_add_OnParalanguageComplete_m3EA6F0F31FF1D046100A42088C5AEF87AB99255C,
	Paralanguage_remove_OnParalanguageComplete_m3C44D3A96D766C4AEDCF667EBB4315F713C64160,
	Paralanguage_get_CurrentText_m08B81A3AF4052EDCA4A15D19C8893F3835AA47F7,
	Paralanguage_set_CurrentText_mDD1E5BD403E373D7DACA85CCD0CC4E6804FF8988,
	Paralanguage_get_CurrentRate_m44CAD57346341EF8B2367965B03FD921A2360A2C,
	Paralanguage_set_CurrentRate_mAA18A9E4868BBF64CBF856D99F032D7E8957042F,
	Paralanguage_get_CurrentPitch_m006F4DC8585ACD788EA915BD139F26682AC928B5,
	Paralanguage_set_CurrentPitch_m27FCE78DC46103743B3A409F09F29592827B7C74,
	Paralanguage_get_CurrentVolume_m44F01DC0490780A2EB5A59FA5AAAEA499A5D2F95,
	Paralanguage_set_CurrentVolume_m1F12D3941B782AC761AFA719F0E26BF9D8103F5D,
	Paralanguage_Awake_mA3669AAAEA2F198F9C2B9D097C6968460452FBE0,
	Paralanguage_Start_mF2FD53BF02A9279D62BCB874E2F662B7D6137F5A,
	Paralanguage_OnDestroy_m3C3220C9711D995873186EC17639715E28C234C6,
	Paralanguage_OnValidate_m6C97C5EB5DB3A2139452300A8D70BC12C4D488B5,
	Paralanguage_Speak_mE5417A97A1BAF7D9C35CFBAF2BCC6F1EEA7C7C00,
	Paralanguage_Silence_m5AB91D89653137FA538D9ED1BF3B1DFC33DA5903,
	Paralanguage_processStack_mA8A64AFE252A5308590B2241692C7A106025887D,
	Paralanguage_play_m5DEF563AE37DF397148D223F6EDBFE721EBF66B8,
	Paralanguage_onVoicesReady_m4FE130AB28A35F38BE6E33D8E26DAB6D506F6818,
	Paralanguage_onSpeakComplete_m9E5A6A36656CBB7000BFD0A81C7217020B54FBF5,
	Paralanguage_onStart_m17A3DCAF2A3BE384A3665785E53EE689BDDAD0B6,
	Paralanguage_onComplete_m36CBEA470C658BD8499CF3B7DBC964B070031CD4,
	Paralanguage__ctor_m7DEB2597D7520D6F97F78AAE54D2B3169D15677B,
	Paralanguage__cctor_m5EDDC2822ADCE7A104743AB8F42A143FE3B2E1FE,
	U3CU3Ec__cctor_mFB6F640DF8F03E43B1BFEBBCAC764F6E2554F4E5,
	U3CU3Ec__ctor_m2CBBA0961378F8F0193D3F6A4284D29CC65F8F95,
	U3CU3Ec_U3CSpeakU3Eb__40_0_m2A211B89F4F07E7512F794F8CF0F0E5210559DC7,
	U3CprocessStackU3Ed__42__ctor_m21BC01AD74ECA7C64B458339F0B9DCC19A45ADE0,
	U3CprocessStackU3Ed__42_System_IDisposable_Dispose_m7430826A64DB1BA83762995F321AA81FDD62220A,
	U3CprocessStackU3Ed__42_MoveNext_m2A77D1C62A50F49135D2148F51F0EBAE67ECB1FB,
	U3CprocessStackU3Ed__42_U3CU3Em__Finally1_m96E94618A0D81B80F36589D9177627358AE1817B,
	U3CprocessStackU3Ed__42_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m6F766B569CFE9E96C325BEEC079BE142E366415D,
	U3CprocessStackU3Ed__42_System_Collections_IEnumerator_Reset_m93CF8EC321B392A350E480F18AA6D728FEA88BE6,
	U3CprocessStackU3Ed__42_System_Collections_IEnumerator_get_Current_m8EC92988F024B6CA75A7A577E3BAC83ACC67253A,
	Sequencer_get_CurrentSequence_m165DA6BAA90B207B38946BFA27D9F37ED0257834,
	Sequencer_Start_mF3DACD10DB960C5F630CA219EA6C878AEB7562D3,
	Sequencer_OnDestroy_mAC4A80994005C6E68CCBBA3013D698C77C95B7CD,
	Sequencer_OnValidate_mE2DC066F1B10EAC6BF1784EADF41B25F380E88C7,
	Sequencer_PlaySequence_mAE1B49FA4D65D20C05E4529749049A797DB86050,
	Sequencer_PlayNextSequence_m149D9ADC4468E2A67B6352B1E351E58A948AF1A1,
	Sequencer_PlayAllSequences_m72925524F5DAEEDC888E007D03F46C5F530ACF01,
	Sequencer_StopAllSequences_m8530D5B34CE98195D0B6541456597570C2166ED8,
	Sequencer_speakCompleteMethod_mC8A38A70CB2BC5E9D12824A82DA3548C4EBA45AE,
	Sequencer_onVoicesReady_m7C1ADD070CC52CF71F63AC51C6AA531552BEC733,
	Sequencer_play_mAC593BF14E12952E5A8703AD60BE0C3CB2744C61,
	Sequencer_playMe_m092FC62EA99ECB5C44C73C2B37A12683DF2227B2,
	Sequencer__ctor_mB258AC851439B150BEC0E349737D0AC7E3722C16,
	U3CplayMeU3Ed__19__ctor_mA6F984F9A7E5AD2F6BA681C9E7893F88E1C3FBAC,
	U3CplayMeU3Ed__19_System_IDisposable_Dispose_mCC358E65250BA6032B90AA940898A050746C8DE5,
	U3CplayMeU3Ed__19_MoveNext_mABEBBE14D5EFABD732E01DC8BAFA8EFD31412067,
	U3CplayMeU3Ed__19_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m26711DB4168A39C095F497E8F84AAF6FB867E0D6,
	U3CplayMeU3Ed__19_System_Collections_IEnumerator_Reset_mBAA7A47CC030736934F37ED90C374CEC7A67E6FB,
	U3CplayMeU3Ed__19_System_Collections_IEnumerator_get_Current_m4F1613CD9A49140520F720F374C25D5614AABD7F,
	SpeechText_add_OnSpeechTextStart_mAD88344B3C4EF80BEC85D52219220E9D3979EB5D,
	SpeechText_remove_OnSpeechTextStart_m81270B2DC915AD36D9C4DB18113FCBE0CF7985C0,
	SpeechText_add_OnSpeechTextComplete_m1F5254AE381EB8F6C84D082456F9A6B0D28532A6,
	SpeechText_remove_OnSpeechTextComplete_m5DDEAD2CFA71592FB055422E83F3B2C358E12C6C,
	SpeechText_get_CurrentText_mF23646EF4360713BF46B306E73C757920C3ABD99,
	SpeechText_set_CurrentText_mA007BBC0D4C7797C050EA0CBF73B3A0420D7DB7F,
	SpeechText_get_CurrentRate_m490404808F7FF657FFE366B3FC5B4D2D452C0645,
	SpeechText_set_CurrentRate_m271B86DD5483CF90D9F1D77A5B6313BD29328B9B,
	SpeechText_get_CurrentPitch_mDD728ABB6B272F9ABF88A0C1A8B219791A11B1B0,
	SpeechText_set_CurrentPitch_mF0EB8CDBD300CB2DD93425105438EE834816EA8D,
	SpeechText_get_CurrentVolume_mA87324BC6C9B72473982F494C32C0F6D6B795B31,
	SpeechText_set_CurrentVolume_m13BE2D7E86E7BD28BC79ED7CA2FA76A54397A00B,
	SpeechText_Start_m8C188C483652992F937BC258F558CB7EBE5F0C06,
	SpeechText_OnDestroy_m1E44298ABA3163B356887D341BA5B42B831B4CDD,
	SpeechText_OnValidate_m09A7545866EEBF83089421BDC0FD8B96B85F320C,
	SpeechText_Speak_m4A5504A06B27BD4DA5E721B89D78EF14EADFFA67,
	SpeechText_Silence_m41568A6D24DA3AA314307B64AE28F08E9AC693D1,
	SpeechText_play_mBCA66CD764C335BF15C0A5682BAE390577D78801,
	SpeechText_onVoicesReady_m1C251F52D3F7C7096EC8F74FB0BCAFB4F74C3AAA,
	SpeechText_onStart_mDAFC5680399169C6858DADA533FD0C059B733DF9,
	SpeechText_onComplete_m85781970A6BAABE15C961686CE0F837BFE990BB3,
	SpeechText__ctor_mB661E15424A4A131E95735CB8FC6767E00C4DCA0,
	TextFileSpeaker_get_CurrentRate_mD9DDD93CC088991035F4A81830C3ED1219F3D822,
	TextFileSpeaker_set_CurrentRate_mA62F060BCC54416B5DAFC0B30E05C44D301D2B4E,
	TextFileSpeaker_get_CurrentPitch_m6932776785831A7BB3A9EA567D2654ED5A568474,
	TextFileSpeaker_set_CurrentPitch_m17B00EC36890AEADCCD703C69EC6C01780691D0F,
	TextFileSpeaker_get_CurrentVolume_m372117338742A92C50B229A03DA48705845904B2,
	TextFileSpeaker_set_CurrentVolume_m32EB142E872E3117873826C8943A65E8740B3896,
	TextFileSpeaker_Start_mA468524FDB99BE2156BC9647644C6A36346B84BF,
	TextFileSpeaker_Update_mB426EA52F45C99E4A56EDD2DFA40EF7F2CE5C7AE,
	TextFileSpeaker_OnDestroy_m4D77C0BF73A6296E30AB33F6014AE5F458BBAFAE,
	TextFileSpeaker_OnValidate_m78A8539BE1B00CB9FCA80FC62F8325A1C95C6964,
	TextFileSpeaker_SpeakAll_m1FE4226A4E4C57AA32ACFFFD2367D862F2BE2286,
	TextFileSpeaker_StopAll_mF9842DA4C01EDF7F3EC957E4E3AA8787BCB2113E,
	TextFileSpeaker_Next_m0E079E3E91389F0BC292AD9B4B2A09C31B4E80B8,
	TextFileSpeaker_Next_m1BF65C69162E6E4478DF3A6F3E22E5393A64C9B2,
	TextFileSpeaker_Previous_m2CACCCAD153146C73ED3C4E1850F318E1B2C3577,
	TextFileSpeaker_Previous_m9B6391CAAA1D7BB858AC7F778EDBDBC25B85CD1C,
	TextFileSpeaker_Speak_m89E4A18C148800BD33D4FAA81A0B3B5FB41FCC1D,
	TextFileSpeaker_SpeakText_m754DE1D8064123172780DC3220424E743435322C,
	TextFileSpeaker_Silence_mA210390D8CFADD1D3354692A0DCD296FF0B15710,
	TextFileSpeaker_Reload_m771FF796D776A7EFCE9E16892921F81A142CA8C1,
	TextFileSpeaker_play_m236C38EF46FCD8C13D5891706C63154EA4CD762E,
	TextFileSpeaker_speak_m92F3BF16F09D64137208DDFBC6A9068BE5EA043D,
	TextFileSpeaker_onVoicesReady_mDA98A67CA24548ADF92512834891640FD13A12EF,
	TextFileSpeaker_onSpeakComplete_mE0F0772D538D81C9B60B13BFFAB1DB7E7DB8D7A3,
	TextFileSpeaker__ctor_m261D534F0FF01614BB4B24A764860B675DE9191B,
	TextFileSpeaker__cctor_mABDA00E3F77458593AAB6B3A61E17B516D516628,
	VoiceInitalizer_Start_m10C8280AD42B10BD76A26995E6163A0223168881,
	VoiceInitalizer_OnEnable_m4E4E62165F7BCFCBA92597936CCB30D97B4FA0C2,
	VoiceInitalizer_OnDisable_m2A51022B621ECBC975902E71B0EA71164D79F113,
	VoiceInitalizer_initalizeVoices_m00AE8AD949E2048D303BF3DCB9AFC0BF91CC35AF,
	VoiceInitalizer_onVoicesReady_mDC3C534FACA43EC385F58A72A849D219DB25B0C0,
	VoiceInitalizer_onSpeakComplete_mF0619CC560466EDDCF6AADD4DB227EA7372A2BEA,
	VoiceInitalizer__ctor_mA883BEC77DF65B56E599EDBA9AD26D3A97B01D4E,
	U3CinitalizeVoicesU3Ed__10__ctor_m8362902403B1FE161F594248B8AE6732B925DCBD,
	U3CinitalizeVoicesU3Ed__10_System_IDisposable_Dispose_mF897C063121C49E81290DF42E4FB044C856A921F,
	U3CinitalizeVoicesU3Ed__10_MoveNext_m248C667C6E400CDC52E41C8598C7AEC009BE21E4,
	U3CinitalizeVoicesU3Ed__10_U3CU3Em__Finally1_mBEB50D2E9EEB95E844E4B33F86727F24D4DFC403,
	U3CinitalizeVoicesU3Ed__10_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m19AB22AD386CF1E797DEF859EBA73F5980575238,
	U3CinitalizeVoicesU3Ed__10_System_Collections_IEnumerator_Reset_mA2AC727EFF62D2404A9D9E0A44431EB15FA6E21D,
	U3CinitalizeVoicesU3Ed__10_System_Collections_IEnumerator_get_Current_m6EE41A71936F6DC8E38DE9780509A78D505FC0B2,
	BaseCustomVoiceProvider_add_OnVoicesReady_m1DBC969BFDFB36363B0662FEC73BE5D26B3E90BB,
	BaseCustomVoiceProvider_remove_OnVoicesReady_m5AD8F56BCA316037FE4793965A25A3121DEDB19E,
	BaseCustomVoiceProvider_add_OnSpeakStart_m8164FE728625CB0C2353BC240C2653584938C83E,
	BaseCustomVoiceProvider_remove_OnSpeakStart_m283D1213D52EE7CFB76D02BFEC06DEFCC310789A,
	BaseCustomVoiceProvider_add_OnSpeakComplete_m2E1EA3C5C95BFDB3A9DA52153C6CFC7E154C16F2,
	BaseCustomVoiceProvider_remove_OnSpeakComplete_m54921CDA4F22A257902256CB4DD4AEB1095553BB,
	BaseCustomVoiceProvider_add_OnSpeakCurrentWord_mBE89E0EBC3C734ED0BB1A58082AE892C537A947A,
	BaseCustomVoiceProvider_remove_OnSpeakCurrentWord_m66C2A70C8BC183FF0E67B05042A34EE8F87AD536,
	BaseCustomVoiceProvider_add_OnSpeakCurrentPhoneme_mF89C0103517EA50DA7462C30A4B7F57F6161C5CF,
	BaseCustomVoiceProvider_remove_OnSpeakCurrentPhoneme_m813D4FDA9FED7356D6CF8602D89CACC5FE6A5AB3,
	BaseCustomVoiceProvider_add_OnSpeakCurrentViseme_mCD2570C41EA9653F2368C36AEC967F25D6A11BF9,
	BaseCustomVoiceProvider_remove_OnSpeakCurrentViseme_m87CE0C1717B04036D67535E202A945A30F6647B1,
	BaseCustomVoiceProvider_add_OnSpeakAudioGenerationStart_m0BB94BA2E23782906DB3595530F8410395220626,
	BaseCustomVoiceProvider_remove_OnSpeakAudioGenerationStart_m6C21ACF04D5CF0451DE8AF5E6A66736856726470,
	BaseCustomVoiceProvider_add_OnSpeakAudioGenerationComplete_m3C863C4E5619F1A7DE1AEA35BFB1531CAA323C5B,
	BaseCustomVoiceProvider_remove_OnSpeakAudioGenerationComplete_m6666CD2372FD9020154D8265C12E56BE158459D1,
	BaseCustomVoiceProvider_add_OnErrorInfo_m4039CA411FABF95B6F72106FA3165763D853FF63,
	BaseCustomVoiceProvider_remove_OnErrorInfo_m91F263673F0367F91FC819ECF70DE7AE3F9F0F4B,
	BaseCustomVoiceProvider_get_isActive_mBCBDA0BDC2ED2215BE880B27C35E9D3CB4225B7C,
	BaseCustomVoiceProvider_set_isActive_m1B7ACF576574DF723169DF6BEBDA36DD27C09B0D,
	NULL,
	NULL,
	NULL,
	BaseCustomVoiceProvider_get_Voices_mA1D46DD83506359EC780D723214381023E42F1AF,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	BaseCustomVoiceProvider_get_Cultures_m3AFEA7ADEEF9200842E2E7841FB434C478477A93,
	BaseCustomVoiceProvider_Silence_m1E979BCAA78AF26BCB1854B164BE44B1C6BA92A6,
	BaseCustomVoiceProvider_Silence_m93D3AC462E51C6FDE79F805EA6B21204A4E53E44,
	NULL,
	NULL,
	NULL,
	NULL,
	BaseCustomVoiceProvider_getOutputFile_mF6C273D1AE2C6A02E124F3D658977AB654E6D7EC,
	BaseCustomVoiceProvider_playAudioFile_mFF2435C8CA85BA9B44A9967159EE60E7F1E38EF3,
	BaseCustomVoiceProvider_copyAudioFile_m870E1A8C6BDEBA932F91693AA0A81F5CCF6470B2,
	BaseCustomVoiceProvider_processAudioFile_m2BA3FA65EBBEF4B1E49C6353E8BE4ADC9147DFD3,
	BaseCustomVoiceProvider_getVoiceName_mBA076CF3753EBBBE07BA70C7E3F24017047182A1,
	BaseCustomVoiceProvider_onVoicesReady_m7C01DF04455E10E65021B0C3C6DCF62E7C89B1AE,
	BaseCustomVoiceProvider_onSpeakStart_m1A8523E4B5E8B727CA4F7779780A8C3974D4C100,
	BaseCustomVoiceProvider_onSpeakComplete_m7FB4B461E34757D57143B719E58360AD216C2A5A,
	BaseCustomVoiceProvider_onSpeakCurrentWord_mB1ADF0ABF72C7E77958EA06D496B7157452B4BDA,
	BaseCustomVoiceProvider_onSpeakCurrentPhoneme_m983DB99489CC88E37B88E8D37CD7C0AFFF237E29,
	BaseCustomVoiceProvider_onSpeakCurrentViseme_mEFE079A1160A431AF2FE3E025017A7928D849DA9,
	BaseCustomVoiceProvider_onSpeakAudioGenerationStart_m49535103542C77ACB83FCE1CF694E3AE5D23628B,
	BaseCustomVoiceProvider_onSpeakAudioGenerationComplete_mC680058C11F3A6D69E259CFFC3BBF87A0D56555B,
	BaseCustomVoiceProvider_onErrorInfo_mA9F2B8A482D2243D868299732C0F7E940FBFB37B,
	BaseCustomVoiceProvider__ctor_m3A0A015610C3C458916A01B22FBC4042595CD516,
	BaseCustomVoiceProvider__cctor_m901EB50445E450A23463958F1B61C448B468F45A,
	U3CU3Ec__cctor_m13F9C9CC09AA996D96695EE2E6499C5844EB1C59,
	U3CU3Ec__ctor_m2E80DCDED82CE2B83015A55142F93173880BC68B,
	U3CU3Ec_U3Cget_CulturesU3Eb__73_0_m0C447A7328552F4245200CE176369BAA5BEB45FF,
	U3CU3Ec_U3Cget_CulturesU3Eb__73_1_m30CE901560AC4A64345151AF668327079181D3FC,
	U3CU3Ec_U3Cget_CulturesU3Eb__73_2_m9232C4822D48244E9FA771AE554B597C93CBDA1B,
	U3CplayAudioFileU3Ed__81__ctor_m12F9909D1F5B37CBBDF5768383EE59A10995FCA5,
	U3CplayAudioFileU3Ed__81_System_IDisposable_Dispose_m8F70B0ED165D9F8F2C0F4ED496647D22AE55E010,
	U3CplayAudioFileU3Ed__81_MoveNext_mDE8FB54C3A65298993FE92ADA5B3A186C31B8118,
	U3CplayAudioFileU3Ed__81_U3CU3Em__Finally1_mCBF2F2BEB5CB7094AB9CF96A530D80922B705751,
	U3CplayAudioFileU3Ed__81_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mBD68E65ACF9A83E963B49E130B6E07ADB5D4FA19,
	U3CplayAudioFileU3Ed__81_System_Collections_IEnumerator_Reset_mFA6ACB523E034EF1620981E29B012849F698D0C5,
	U3CplayAudioFileU3Ed__81_System_Collections_IEnumerator_get_Current_m6007D9DD5B76EFE943D370BBEC3A868B0447FDF6,
	BaseVoiceProvider__ctor_m02659306825EE660C477BC9F972DEF2F74B231D8,
	BaseVoiceProvider_add_OnVoicesReady_mA4ED9500F520BD673FB32328663F78E779E835FA,
	BaseVoiceProvider_remove_OnVoicesReady_m7AFA97326057C39069AAC20F504C2E1048D87E4C,
	BaseVoiceProvider_add_OnSpeakStart_m5299B03A0A29E73E8EA6AFA3086899032A585552,
	BaseVoiceProvider_remove_OnSpeakStart_m9D1E027A6825CAAA75B55513A9E65E7397CF18D1,
	BaseVoiceProvider_add_OnSpeakComplete_m772B02A28442CB9225486190186E89C5C3F89139,
	BaseVoiceProvider_remove_OnSpeakComplete_m1D4D23293E2FA9CEC748C8D60E376946A52FFF4C,
	BaseVoiceProvider_add_OnSpeakCurrentWord_mF2E4791EA8FE8930CD84C5665F9BBDD19FBC61A7,
	BaseVoiceProvider_remove_OnSpeakCurrentWord_mA903F36E19B3F8B49DB4C34C460F96FDCDB9401D,
	BaseVoiceProvider_add_OnSpeakCurrentPhoneme_m78B03CFC4D891A3BC50FA558EAAD4DA9F6E11BF7,
	BaseVoiceProvider_remove_OnSpeakCurrentPhoneme_m78A32986E30B2B02902A083841A55DA19D490E27,
	BaseVoiceProvider_add_OnSpeakCurrentViseme_m7D02269CC0039EF4783204CD8407CB4DF9C717AD,
	BaseVoiceProvider_remove_OnSpeakCurrentViseme_mD5CCC83164B03252886CB34DC6BAB3114B247D9C,
	BaseVoiceProvider_add_OnSpeakAudioGenerationStart_mC0427F55B3A430AF8FFD1A15450FC0ADFC47B5F1,
	BaseVoiceProvider_remove_OnSpeakAudioGenerationStart_m239E423226B24DEDCD263BD2C1A8DF98C5DEAC36,
	BaseVoiceProvider_add_OnSpeakAudioGenerationComplete_m3968C747AF6BCCBC2949A0A091D185119E55A5DC,
	BaseVoiceProvider_remove_OnSpeakAudioGenerationComplete_mFD8880E0535CF6ED767D30F7F6377D8A1BEFCB1B,
	BaseVoiceProvider_add_OnErrorInfo_m164FE7A339F2667D1C0B2A4F337606ABDAAEF695,
	BaseVoiceProvider_remove_OnErrorInfo_m3ABA070A4209EC527ED0C80A7A4C6298544EB184,
	NULL,
	NULL,
	NULL,
	BaseVoiceProvider_get_Voices_m52402F6778A5CF34DFEE6E35BF2424466000C915,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	BaseVoiceProvider_get_Cultures_mAF424DC51DE1BF5DD5E06C2EBC7897B38E89DA83,
	BaseVoiceProvider_Silence_m286B3E6929DA4ECEE736E82A13538129446F7DFE,
	BaseVoiceProvider_Silence_m7B761381D853E888C6B2BD812FCE42A6B9B08675,
	NULL,
	NULL,
	NULL,
	BaseVoiceProvider_getOutputFile_m19D481792D3E2DF47E31529E31C6599827E02E1A,
	BaseVoiceProvider_playAudioFile_m4CCFF1BB44ED2ACE5F10AB4AAEE81D2B7CD24DD7,
	BaseVoiceProvider_copyAudioFile_mC986277966319CE62EC8F450A26B0D842B4AF0AA,
	BaseVoiceProvider_processAudioFile_mD6ACD3BA6544012AC5FAE6BAA5B17F7F0583D278,
	BaseVoiceProvider_getVoiceName_m545EF29C42D9272162727415CC0F59B55B7EAE00,
	BaseVoiceProvider_onVoicesReady_mF817D7438CBC4EC56307BBFCEFFBB2CA55D40BF8,
	BaseVoiceProvider_onSpeakStart_mC65B3113E5BF77122D14BB79750F45F8E453EF78,
	BaseVoiceProvider_onSpeakComplete_m10E0CDF151A84C496DEB3A11CFDAC21742498201,
	BaseVoiceProvider_onSpeakCurrentWord_mD46E845D857BA608FAF08C138F57CADD912CAA7B,
	BaseVoiceProvider_onSpeakCurrentPhoneme_m855E173EA80CE09DED011BE7D979A1257760849C,
	BaseVoiceProvider_onSpeakCurrentViseme_m4136BE82B59A2E83E99F4F3D4CEB374529CBD604,
	BaseVoiceProvider_onSpeakAudioGenerationStart_m75AF6462CE93B8AD2B573CB0917C39D79F94503F,
	BaseVoiceProvider_onSpeakAudioGenerationComplete_mEF256BA9AEE72692FC3F945DDA908EEAB60FF98F,
	BaseVoiceProvider_onErrorInfo_mB680477B720E57C3E817EE57EC451BD074FE7063,
	BaseVoiceProvider__cctor_mE3106C4492FB8F6C8AB9E64B9E90305C883CEB67,
	U3CU3Ec__cctor_m0EF1C5CFE61420202344FE43A32A81940D81457C,
	U3CU3Ec__ctor_mA840AB15FB28E0517609CEC87F3E2BA500F92B2B,
	U3CU3Ec_U3Cget_CulturesU3Eb__72_0_m0395F398BA09A22FCBF1581A6645F235A24060B6,
	U3CU3Ec_U3Cget_CulturesU3Eb__72_1_m87D845FB3DF58DAAED80F6F1A66A076EA1F90D09,
	U3CU3Ec_U3Cget_CulturesU3Eb__72_2_m13DF8C2968DA5E62E3C7E8D471CF98076D4EA309,
	U3CplayAudioFileU3Ed__79__ctor_m1918F5820D88E3F6361AF83F51F6389CA0FE8495,
	U3CplayAudioFileU3Ed__79_System_IDisposable_Dispose_m016A01B81977C3EED37BE31F4601782D790E2577,
	U3CplayAudioFileU3Ed__79_MoveNext_m7A4970E998E5D19894D475E4A2A46E6849A3325E,
	U3CplayAudioFileU3Ed__79_U3CU3Em__Finally1_m88F63303367156F30057965A5D2D1106BF1CA5B0,
	U3CplayAudioFileU3Ed__79_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m2F7ED2CAB4FD2CE5ACDEE216AA960E8F48A1D4ED,
	U3CplayAudioFileU3Ed__79_System_Collections_IEnumerator_Reset_m7EAB980BAE0EC597EB0D8AF89DD2E220F7600552,
	U3CplayAudioFileU3Ed__79_System_Collections_IEnumerator_get_Current_m081CAEDEE10983E393B7386FBD4D29ACC25D70B3,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	VoiceProviderIOS__ctor_mC4BC5ADE92D96DE14AE80296C84759FDFBB7308F,
	VoiceProviderIOS_SetVoices_mD729F91B8AF3849C66C680358C5123DC40B647BD,
	VoiceProviderIOS_SetState_mCF102485F96BDF25C45619C62AC3E4E8589C1BE9,
	VoiceProviderIOS_WordSpoken_m8203813DD2CECBB24D0C5FE3ED9D432B31D80163,
	VoiceProviderIOS_get_AudioFileExtension_m07A74BB0B3EA063D0B890C7D207693B256ED0CB5,
	VoiceProviderIOS_get_AudioFileType_m26F7047CDECDD1768F824EFC7C608293CA4AD1BF,
	VoiceProviderIOS_get_DefaultVoiceName_mD6EEDD36FB021F31D6555214478F223A97F1E22B,
	VoiceProviderIOS_get_Voices_m25AF6145DAEED158E2EF09C4EB49ABE956A4EDF7,
	VoiceProviderIOS_get_isWorkingInEditor_m888EF1A3B28487DC035062A8BEB7D89610B0093A,
	VoiceProviderIOS_get_isWorkingInPlaymode_m2A36B8954309B8F267E9399320B06247DADD59FC,
	VoiceProviderIOS_get_MaxTextLength_m610D3991B05C2CA749C2D765F9444DF92E11A9BA,
	VoiceProviderIOS_get_isSpeakNativeSupported_m7C0135D291D257AA2A4AAEA3686D9D2542BA4462,
	VoiceProviderIOS_get_isSpeakSupported_m2CB1F9C0CD50FA82075AF910D1554F2E9A0E5BC7,
	VoiceProviderIOS_get_isPlatformSupported_mFF553B9EB9ED371CDA3D06B03AE2063EAE1619A5,
	VoiceProviderIOS_get_isSSMLSupported_m42399ABAFF8AC75479D381F067E8DC9B3868B816,
	VoiceProviderIOS_get_isOnlineService_mDD2E89AE97570AC044A87164A4CCDA410F861D2F,
	VoiceProviderIOS_get_hasCoRoutines_mE04140B5C1D213F7B3EE05E6869434209F669510,
	VoiceProviderIOS_get_isIL2CPPSupported_m9BB5CB9EA69CED7C7C525503A78C6EBCA4E1A2ED,
	VoiceProviderIOS_SpeakNative_m5A460E4C43E3F6B3F5C5B50B2FBA960E906087A1,
	VoiceProviderIOS_Speak_m5D3CE8F57DAE33BE09E8A2F7AF87281DC47181D0,
	VoiceProviderIOS_Generate_m5C4D7AC723D419BA1EFBD9411242CBE29C6D09BB,
	VoiceProviderIOS_Silence_mED79A411E6B142E5F3ECA22EC16CCAED8B847442,
	VoiceProviderIOS_speak_mFB7D7BA16B879B9C8E69B83CDA91D109189EB96E,
	VoiceProviderIOS_calculateRate_m543B7DC43801E1CB3A84207F436870D1368B741A,
	VoiceProviderIOS_getVoiceId_mE5A2247735EAB75CB1329FCD495DBC0C23EDABE8,
	VoiceProviderIOS__cctor_m4A4FFA425D5FDC8632E989BFA7D424366BB623E4,
	U3CU3Ec__cctor_mEA471FCF86D779A11EE155D62B0464BE482D6132,
	U3CU3Ec__ctor_mF737325D732B47B4EE8E1EC82D47AD742AF9A108,
	U3CU3Ec_U3CSetVoicesU3Eb__6_0_m6CB25C67465C0943C5D40010EC0A70B7D2419879,
	U3CSpeakNativeU3Ed__37__ctor_m523A47BABBCEB6115AB8BC5602D2194F4C9DEF4F,
	U3CSpeakNativeU3Ed__37_System_IDisposable_Dispose_m8516463D2831A9F5409582D34F16DC2697018BD8,
	U3CSpeakNativeU3Ed__37_MoveNext_m2DB830F1D63D93BA54D23768651BD95A7F90A608,
	U3CSpeakNativeU3Ed__37_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mF2CE12F2B43A8A935DAA332E01EF3DF9D3659427,
	U3CSpeakNativeU3Ed__37_System_Collections_IEnumerator_Reset_mDE6E3EAB0C3383024A07F9518EC95E2615BE50A2,
	U3CSpeakNativeU3Ed__37_System_Collections_IEnumerator_get_Current_mF4BB2E19D59C10E2E19E1A071D8B963C2318D72B,
	U3CSpeakU3Ed__38__ctor_m17AA83B8F4292BE9A434FAD451B6042F85B1CA7A,
	U3CSpeakU3Ed__38_System_IDisposable_Dispose_m1D62F9167AD94A284EB61F82C0A4F55B3CD87CFD,
	U3CSpeakU3Ed__38_MoveNext_m536B0376DA33C79026D993AF29A57DF0209A6AC9,
	U3CSpeakU3Ed__38_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m55A294AB03578EC259B3F90FF30ED7D6E9B850BB,
	U3CSpeakU3Ed__38_System_Collections_IEnumerator_Reset_mB2BC205AFCE9DA70AAF9757596AB8EE0E2085E41,
	U3CSpeakU3Ed__38_System_Collections_IEnumerator_get_Current_m3D7E6B2FE72C3643435F91F8C65FDBD74A4E2C6B,
	U3CGenerateU3Ed__39__ctor_mD9733152DA5C477BC3EBDE9374773C3614007179,
	U3CGenerateU3Ed__39_System_IDisposable_Dispose_m77EA5EA8743977C2BB472BD2FF79D131B2A25D38,
	U3CGenerateU3Ed__39_MoveNext_mC7E14CE9883319EEC46EE167548548E177923595,
	U3CGenerateU3Ed__39_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m55C35151E2EBDA7C7287CC1DB8AC808094F27B2A,
	U3CGenerateU3Ed__39_System_Collections_IEnumerator_Reset_m4D4A7F1C40FB78E0D45F689E2638CF4203F0BB8B,
	U3CGenerateU3Ed__39_System_Collections_IEnumerator_get_Current_mECE12334B8337427611893D644D1735115B00175,
	U3CspeakU3Ed__41__ctor_m276AE39E567257B32C7896EA8EDDA24EC9D824ED,
	U3CspeakU3Ed__41_System_IDisposable_Dispose_m0E4873CEED2CE27F21F5EB92F97879BFB53783C2,
	U3CspeakU3Ed__41_MoveNext_m32CBA9644FBF1A2196C89F1678AE03DD31997ACE,
	U3CspeakU3Ed__41_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mEEA1EC15804398BACB904C1707E5281F50328F18,
	U3CspeakU3Ed__41_System_Collections_IEnumerator_Reset_m93EA86B1BE247D3CE920516B739BB3E25B25C55F,
	U3CspeakU3Ed__41_System_Collections_IEnumerator_get_Current_m95840D9DA7B2AD2A2C84637718BD96658116F22F,
	NativeMethods_Stop_mB2EA6FF4519D74F0C0DF8A2104AF16FE63071F3F,
	NativeMethods_GetVoices_m7906C96E78341A926EEA1C5A276A4528D85CE4AB,
	NativeMethods_Speak_mF3A31C5A982A4A0DDB94E67397E939D105103D51,
	VoiceProviderMary__ctor_mB6769A04B732E045ED7160EA34DE22311D2AC30B,
	VoiceProviderMary_get_AudioFileExtension_m1B8092196E9F35921D525DC3143AF18DCBC9F0F7,
	VoiceProviderMary_get_AudioFileType_mA84BE50D872E8245689CB7ED9125FDDCB0E2A5F8,
	VoiceProviderMary_get_DefaultVoiceName_m8C8D8F8EEF1ABE901E460D50E752EED88F06168F,
	VoiceProviderMary_get_isWorkingInEditor_m21F72BBD9F25B5EE735B980BB2082E6AE9DAEC36,
	VoiceProviderMary_get_isWorkingInPlaymode_mB724D830F7622218E0604C9DF9FF0CC82DD247DC,
	VoiceProviderMary_get_MaxTextLength_m93FC45A4EF1AF0F0756A1B443C2C7C9D7B65999D,
	VoiceProviderMary_get_isSpeakNativeSupported_m95F7D6E5F773316CBBB491C91E2C3055066D3DBD,
	VoiceProviderMary_get_isSpeakSupported_m07AA500CF2852BC3D98BA43D38A379FFCDAA715E,
	VoiceProviderMary_get_isPlatformSupported_mB980866B022E1053B7D533354DDB2B78DE855D5D,
	VoiceProviderMary_get_isSSMLSupported_m258E7AAE088712C0C3EA2E58CF8DC8F63D1DC832,
	VoiceProviderMary_get_isOnlineService_m720CF799EB99CDC8BE1F1F8E92CBCC71B2CFFAC5,
	VoiceProviderMary_get_hasCoRoutines_m26CD140877B7336FC8F6F0A015ACF0D482851B24,
	VoiceProviderMary_get_isIL2CPPSupported_m3CCA21A436E168D0309009A449D7CEDEE167C696,
	VoiceProviderMary_SpeakNative_m331610A2ED4ACEA5F1B1E8285E531D9A812A09C2,
	VoiceProviderMary_Speak_m891800543E668A5334BD528A4144C8785D80DAE7,
	VoiceProviderMary_Generate_m5E6DF2FF9DB49139BB98D02BB3B8703159E50EB3,
	VoiceProviderMary_speak_mEDD8B66B63C43FD85CAD280D309C4276729D3FD6,
	VoiceProviderMary_getVoices_mA44E670C9E75CC20259D5BFBC4884064E4BB1DCC,
	VoiceProviderMary_prepareProsody_mC908FAA976B4484D18FC8A46611AFBBF088DEA96,
	VoiceProviderMary_getVoiceCulture_m2263C6C1C91C4F198C212990512CC920EFDC12DC,
	U3CSpeakNativeU3Ed__29__ctor_m7470A0504C3BABDC965367131EA97B330ECAA0B3,
	U3CSpeakNativeU3Ed__29_System_IDisposable_Dispose_m02B9BF12673B6C720138AE5C87DCA36928F31A6B,
	U3CSpeakNativeU3Ed__29_MoveNext_m05BE0511D45764D10AB861EFB3339ED18AFDE054,
	U3CSpeakNativeU3Ed__29_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mB9405A8A86C3770CC1F111D0C4000A7A9B7D7A67,
	U3CSpeakNativeU3Ed__29_System_Collections_IEnumerator_Reset_mBC4615F0056EF0BC799A52C6A4149F468953E3B1,
	U3CSpeakNativeU3Ed__29_System_Collections_IEnumerator_get_Current_mFED8E5538033280FD348A7C8FFB81D8D26DC06A2,
	U3CSpeakU3Ed__30__ctor_m9587AD139AB417176D703120DF98958092A13743,
	U3CSpeakU3Ed__30_System_IDisposable_Dispose_m23CE40AF61F6D8649073C36D77C590953E895803,
	U3CSpeakU3Ed__30_MoveNext_mBE5D478FD118D12D9131E5F87F6284B4B33F47B7,
	U3CSpeakU3Ed__30_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m4BE9CD55DFC56B895DDBBEDFFFF9D846C2F24096,
	U3CSpeakU3Ed__30_System_Collections_IEnumerator_Reset_mDDC9BFD5B6F9F4992FF961A1916F6CFFF370032B,
	U3CSpeakU3Ed__30_System_Collections_IEnumerator_get_Current_m80972AF0FFE79545A22C01A56202CEA16644B859,
	U3CGenerateU3Ed__31__ctor_m267A2910841978F5CDB6CCB831A10313B0798DEC,
	U3CGenerateU3Ed__31_System_IDisposable_Dispose_m22DC6783EC7327F0EFAFDF726143D978509C7C62,
	U3CGenerateU3Ed__31_MoveNext_mA83E3FB408DEF5A6E09CB0CBEC66506FBDC92CA0,
	U3CGenerateU3Ed__31_U3CU3Em__Finally1_mDA25C0FD49EDC94E346AFFCA4B00F774AE5A0D99,
	U3CGenerateU3Ed__31_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m6BA121BFF88A61F810A85519EA5166243AA8F2F7,
	U3CGenerateU3Ed__31_System_Collections_IEnumerator_Reset_mA8522467F71712CDE975F96D86B51B3F6C04B1CC,
	U3CGenerateU3Ed__31_System_Collections_IEnumerator_get_Current_m9EC33BA38BF83D3CA8FF84D807491EEBDC56CBD2,
	U3CspeakU3Ed__32__ctor_m39F1A6EF5BF1ECD0725D91A20A03439E3AD4E103,
	U3CspeakU3Ed__32_System_IDisposable_Dispose_m47A016B1C9DA6E21E7501088414B7AE4026C210E,
	U3CspeakU3Ed__32_MoveNext_m24A5A0CC2FFB038864D2BF34084D91512FDB498A,
	U3CspeakU3Ed__32_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m8CFA25DFB6CD460D6C3AA1108C416152A15230F5,
	U3CspeakU3Ed__32_System_Collections_IEnumerator_Reset_m73F40ECB8D160F5A0111DCDC777F3BFB1CEEC6F3,
	U3CspeakU3Ed__32_System_Collections_IEnumerator_get_Current_m2D2650AC8FF888202F794982EB33EE9B57F98367,
	U3CU3Ec__cctor_m7CB3E48BA99465FB4BCCD49E7F8EC4A255DAE92B,
	U3CU3Ec__ctor_mD3D5DF15838DA3DB7A52BC69F204B58E3F90BB40,
	U3CU3Ec_U3CgetVoicesU3Eb__33_0_m07CDC9ECA52CE7037EB04790B0A364271331C9ED,
	U3CgetVoicesU3Ed__33__ctor_m95F6F409F82D4F4973F63E456C242F66A76B3205,
	U3CgetVoicesU3Ed__33_System_IDisposable_Dispose_mF7D198C1948C01A6A4D9D46B84AB60DE30DF3C30,
	U3CgetVoicesU3Ed__33_MoveNext_mF8F4466E2C8CC6587D824C6B7623E60C582C389C,
	U3CgetVoicesU3Ed__33_U3CU3Em__Finally1_m2A7E2EC60FF2A44DB6861F37ED48C1F9EEF4D8DA,
	U3CgetVoicesU3Ed__33_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m2C5797A9C9F2E33DE06235C398E7B36C6EA1197B,
	U3CgetVoicesU3Ed__33_System_Collections_IEnumerator_Reset_m995AFECA0C5D06884A5DA8C78BF92AF75EAB19E1,
	U3CgetVoicesU3Ed__33_System_Collections_IEnumerator_get_Current_mA53C128274D93B9FE40D699DD1192FC1A9C5B829,
	Sequence_ToString_m3BFC91D430D8A907A09D5F8C4AA292AC082FDBAC,
	Sequence__ctor_m5E98C6F80D8D65FC7C5B9CF2B73E4CE293F9EC3A,
	Voice_get_Culture_m7EF31333CBC90A35D123D49227AB96C62D4D95BA,
	Voice_set_Culture_mCEB168A68A0FACB0992C681FB408C26278A513DC,
	Voice__ctor_mC0087A480AD46373F19FCB3BD5E1C2D412B4C89B,
	Voice_ToString_m46193BE390246ECF3591CA5DA9BA64A9032FAD3B,
	VoiceAlias_get_VoiceName_m0CBFE27D846026BB82F5E682FEB0A97AD27D3737,
	VoiceAlias_get_Voice_mF91385FF9A70D7C329435302B581BECE81FBBC5A,
	VoiceAlias_ToString_m9DF65F2BAA5B3C76D48EACA25FF8E62640CD0832,
	VoiceAlias__ctor_m71C4E357B220016FB187704DB63C9861AE8ED688,
	Wrapper_get_Text_mC219083BF6B4E0500CABF0B432D2662D131638B5,
	Wrapper_set_Text_m8E355EFA3581435BF6FE74AFBBEC1932E58EA35E,
	Wrapper_get_Rate_m3EE43D41BD63635B196B1094235560C723880ACC,
	Wrapper_set_Rate_m7B8C8D39414EA93CFB0397500F1B8932A45D7BED,
	Wrapper_get_Pitch_mCCF5C76983AC9D8DE7C0C2D0F3C8BBE096B53CF0,
	Wrapper_set_Pitch_mE1195A0F2B8E098BC6570A24E96F2D3D850B231E,
	Wrapper_get_Volume_mF10B58AC0E2C9C244ED7104CAAEC93354BA85AE8,
	Wrapper_set_Volume_m72C546D43C36D860514E42F8177FBD3B15B16463,
	Wrapper_get_Created_m3DE70C26881F1035F2F5885BD7F85A03EDB46C2B,
	Wrapper__ctor_m7E197A775CE393F579FEAEBC0BE3919FE04B30A2,
	Wrapper__ctor_m0F9313C5EDAC0D90060DD6C4D68C9DCC00575D1D,
	Wrapper__ctor_m56EC0540CC35B2E1CC6ABDE2A11AD6F0F7BC3D3F,
	Wrapper__ctor_m47F6FD774724B8F43E6DA2C1A0042B2E2911C8A3,
	Wrapper_ToString_m3708C530727B7346770AA39D631DA4D1BE32DE8F,
	Dialog_OnEnable_mF4D606A6B5ACEE60DF0AA2C1FBA7F566BF9AD99D,
	Dialog_OnDisable_m1D578120E599BAAC330DFD3EF110F060E38655D6,
	Dialog_DialogSequence_m7D77C870918DB2F259C90731C7EE7E6E4F788B9D,
	Dialog_speakStartMethod_m0F084991EF63B99FC9FB4E1DA8A3D4B2A0BC8CD4,
	Dialog_speakCompleteMethod_m51E4AE3F7D8720106FD31B48FB3A4403CC7E251C,
	Dialog__ctor_m8B687AF79B41A93CA26166999F7AE0E4BFCE7B51,
	U3CDialogSequenceU3Ed__25__ctor_m0B8FC59C5611D1416FB254E739F74FAC0B7241BF,
	U3CDialogSequenceU3Ed__25_System_IDisposable_Dispose_m43CAB6AFDA5A407257DCE2BB40AF5562F71833EA,
	U3CDialogSequenceU3Ed__25_MoveNext_mA0D0362F52647EFDCE638259C5B5D9D75E43D516,
	U3CDialogSequenceU3Ed__25_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mE00F000EBAFBDDFF5D2C8F42E742D77F3DD80A97,
	U3CDialogSequenceU3Ed__25_System_Collections_IEnumerator_Reset_mD9737A7BD110755EF4E00DA6CB49C2B29F9D3B42,
	U3CDialogSequenceU3Ed__25_System_Collections_IEnumerator_get_Current_m1EF267AEC7A80B67C2C484CABB5254A8CA16068A,
	GUIAudioFilter_Start_m6886E6993EFAA97A88E35853BD32694588CB94B6,
	GUIAudioFilter_ResetFilters_m5B7B75EFEF1DC5B2669AD6285BB2455B1C282685,
	GUIAudioFilter_ReverbFilterDropdownChanged_mC842F786BBC73DF7AFD6A6CFAED3E5F26866CC32,
	GUIAudioFilter_ChorusFilterEnabled_mA057786817E3577C6E379E2B8AAB916C6DF704F2,
	GUIAudioFilter_EchoFilterEnabled_m36AB7310EF7486367CB129CD64DE045A6E50D43F,
	GUIAudioFilter_DistortionFilterEnabled_m5BD844B929184E0C0E8DA4AEF8CC5E814741D1BC,
	GUIAudioFilter_DistortionFilterChanged_mE8F55A16742AD05A91A963342AA2F90422AADE27,
	GUIAudioFilter_LowPassFilterEnabled_mCB6829F29BDD0222AA90FB2E04F524B0919B7E43,
	GUIAudioFilter_LowPassFilterChanged_m5AF438F7E83057F53B2C6860B73EBAEA3BB946EE,
	GUIAudioFilter_HighPassFilterEnabled_m558C059BDFCDAF936DFFF2C210BB320A58200F82,
	GUIAudioFilter_HighPassFilterChanged_m14DFE353513153224A9F55A8F0448587C73697B4,
	GUIAudioFilter_VolumeChanged_mED7D295CCF857329A976FF00678393F8F337A53C,
	GUIAudioFilter_PitchChanged_m1CF6C6569BC7813D3AE47FD43866FAC7D2D51D73,
	GUIAudioFilter__ctor_mAD15F7AB21627DB82A885D489B6813165E9C5948,
	GUIDialog_Start_mCA0C179522C0F844FD0DFDDAF0C4C6574378138A,
	GUIDialog_Update_mC65E8C71A4DEB40DB3418FFDACC0BDB6D7C3E22F,
	GUIDialog_StartDialog_m63685F675276934820904EEBDC99094045F25E0C,
	GUIDialog_Silence_m46AC2A08EF788D8BAFF997CA04EAA1F61793225D,
	GUIDialog_ChangeRateA_mA3107EE6AEFBE02E4F30635110A1FC14FE11CEEF,
	GUIDialog_ChangeRateB_m58D330297F83EFFCFDFA5D826BB4665D440B362E,
	GUIDialog_ChangePitchA_m93E38C9465EFECAB55E859343A04864AE5C123FD,
	GUIDialog_ChangePitchB_m3501392CF0B0288A085A77F9C9AD230E78CA7647,
	GUIDialog_ChangeVolumeA_m54E24B19077D69E663562267BD1C432F5DD15166,
	GUIDialog_ChangeVolumeB_mD5B9714E92C44574635F44CABB82D83C63A3140B,
	GUIDialog_GenderAChanged_m4BE6FB99D875C0449F2B00824FFEEEEFA344509A,
	GUIDialog_GenderBChanged_m05238AF8A2E8CAA58CBA178265122CBB1D69FA31,
	GUIDialog__ctor_m8B580017C6CC95CE22552613753F0DEA5841AD3D,
	GUIMain_Start_m7372CBDB7A3BEE35A1F901ED11E91A4B7BE76AB6,
	GUIMain_Update_m72E8B10794645412E510C88E2DFC8E7E7C3C9DBB,
	GUIMain_OnEnable_m71648F88ED37B3A3D538055C6E8C193DC6DE1A70,
	GUIMain_OnDisable_m8DB75C519991AEDE69E6B0E15C34FAF713F8A253,
	GUIMain_OpenAssetURL_m554C00A6470EE66EFF410EA0A0C2E2D21DA0FBEE,
	GUIMain_OpenCTURL_mD019EF2753C79EAF07E2456F7D5EDD3DAA395199,
	GUIMain_Silence_m8466BA5A8B457FC7F43F5D674A1E8ADFD296794E,
	GUIMain_Quit_m8AA76677E434D057F894A3C7BA90D2EDA9C26BBB,
	GUIMain_onVoicesReady_m3CF21F9CF961535E17AED05352981AAB93D2A018,
	GUIMain_onErrorInfo_mCFE0DB61E576EFE7FAF23DEC977A40A74C853144,
	GUIMain_onSpeakStart_m27417E9D67714F474F87678BA70857D82B703709,
	GUIMain__ctor_mF791B68B3C8C966D9516650EAEDEAEFDF1984495,
	GUIMultiAudioFilter_Start_m719D50C0D1CB458A9B3EBE7120B5E65FFB641F9E,
	GUIMultiAudioFilter_ResetFilters_mB25D0B9790143F454CF32C4DF406DC144716B5E2,
	GUIMultiAudioFilter_ClearFilters_mF7041117B32EBC701756A3C688CCD82CFA93EDB9,
	GUIMultiAudioFilter_ReverbFilterDropdownChanged_mD3EEC03542C77928753FB7D6610E402AE3A31DD9,
	GUIMultiAudioFilter_ChorusFilterEnabled_m05B90822856588B3760D6C7DBA51B07B56AF8BE2,
	GUIMultiAudioFilter_EchoFilterEnabled_m889EB93C4985313A63C0CD5BEB5E95BDFEDE9836,
	GUIMultiAudioFilter_DistortionFilterEnabled_m770295A6352C2A3791935C4B057D4CE9750DABEA,
	GUIMultiAudioFilter_DistortionFilterChanged_mD45256892D27B2312090C1E57B9D306E85A8052E,
	GUIMultiAudioFilter_LowPassFilterEnabled_m453847453A00A1E47DD34953533728683EC2DEA6,
	GUIMultiAudioFilter_LowPassFilterChanged_m3E8483EF19A5CB5F320F467977D8849773708EB0,
	GUIMultiAudioFilter_HighPassFilterEnabled_mC5C2DC19BB57E71E9569FF50D6CA25E6514BAB2A,
	GUIMultiAudioFilter_HighPassFilterChanged_m38973C6189E6C57B046C448AA1159A45C292034E,
	GUIMultiAudioFilter_VolumeChanged_mAB7E263CD1396F8CD4FD3645CA57E6F97B716A0A,
	GUIMultiAudioFilter_PitchChanged_mA53D86BB7EC027060E4633C885B21A4B923164B0,
	GUIMultiAudioFilter__ctor_mBB6980B1EA20B1CAEAD03378673028A2DC92D631,
	GUIScenes_LoadPrevoiusScene_m61DD41DB71B23058F4C9FCF166F444AF85AFE50D,
	GUIScenes_LoadNextScene_mCC0676F07E77B77C52C9C04FAA4BF5F4CC9EEF50,
	GUIScenes__ctor_m893D0F0F9040E5D1315BE3CDAFFD76BC9CC8356B,
	GUISpeech_Start_m914C19EE24E5A0879D9A62F750D1CAB9CE320508,
	GUISpeech_Update_mE76474936989A2D20782BB168AAE115E9419F12C,
	GUISpeech_OnEnable_mCFABDC217D6DE585904E61E9A51ED848DBC781C5,
	GUISpeech_OnDisable_m50F82241C76C641B860E923A54D7DDCAC32E89CD,
	GUISpeech_OnDestroy_mC12F6145213B57E9BFFDECA747286F3E5F1FD224,
	GUISpeech_Silence_mFE4D5660663F0D8CC964D1669B58EDEB3E9C9D9F,
	GUISpeech_ChangeRate_mB10F8E108D948502D0A7AC43371282439087307C,
	GUISpeech_ChangeVolume_m4E6E48931AFC02E853E1FB1863980C3662758F6B,
	GUISpeech_ChangePitch_mF562535E186F337153000EAC3122B55533B7A738,
	GUISpeech_ChangeNative_m40909DF4D07E0BD6AC806B503BD7626EF73C950B,
	GUISpeech_ChangeMaryTTS_m3734C7B7D05B88D387C99EFC25C1EA4989FE61C3,
	GUISpeech_GenderChanged_m66F2A0B8BFF5C8A165A18923A0AA8595EE97F25F,
	GUISpeech_onProviderChange_m44CE107A6F1EF38552772B03F302E0B7F10242E4,
	GUISpeech_clearVoicesList_m5FF61890C20A56018A64FF93A854863481D4C2CD,
	GUISpeech_buildVoicesList_m78FF9167787CBFA9FA6697455BA80674584DC6A0,
	GUISpeech__ctor_m3C22432AAF13674A9235983819D228F00ED98015,
	GUISpeech__cctor_m93D43CA4562A587B863A7257A4DC469185150F0D,
	NativeAudio_Start_m7B15600F3DA329521D8E905267BDF649DFEC2C20,
	NativeAudio_OnEnable_mC4AA950771A508BC7831368EE19ABCD209324AC5,
	NativeAudio_OnDisable_m9047B791EFD42EE37923E238FDC1D8436BDE8204,
	NativeAudio_StartTTS_mFF83EF78D245FB3FF2F487BC008AF5774410E35D,
	NativeAudio_Silence_mA913AE2A926FF0863617E78716D52B5858B9D22F,
	NativeAudio_play_m1194349F63CBB1714BB15FAC0C461F49848CC01B,
	NativeAudio_stop_mE4B57E83EDE491A1A55FEB1A353BDB2BD58890A7,
	NativeAudio__ctor_mCE75BDEC29D267A10F70A9C6012F0832A959AC90,
	PreGeneratedAudio_Start_mC5269E7BD3DB9E19E4035465C8B1E23F9D6D6648,
	PreGeneratedAudio_Update_mB90A2E9062E06701873103FA31666C0A5CDF76C7,
	PreGeneratedAudio_OnEnable_mA75658E67F3EAFE883740B7D8E5091A208A808E2,
	PreGeneratedAudio_OnDisable_m5081BB0ECF34C45BAA4BB27910A65B6C8ACC56BB,
	PreGeneratedAudio_Play_m2074B06CADE69F9395EE1134517D47DDFB881794,
	PreGeneratedAudio_Silence_m61A13A1764338E59F6F3369DA9919AF33881EE9F,
	PreGeneratedAudio_Stop_m0A2E1752832E34681E9959DC232FE7EB43B24009,
	PreGeneratedAudio_speakAudioGenerationCompleteMethod_m69B145D1B28D832030161F7637A5BCABD58852FA,
	PreGeneratedAudio__ctor_m600793BB8FCE1D9B19514AD052CAE7B04C3C98DB,
	SendMessage_Start_mC3B0C517710E9385582737FD01CB8AF207642318,
	SendMessage_Play_m33614691088DB64C1EC850C0D9F9D4E59D87B4FD,
	SendMessage_SpeakerA_mBAF98AAA8F2B3BA9C1C4E411369581DACB6F788A,
	SendMessage_SpeakerB_m6C538E9CD2CDABCC1F85DC7514AB9ABAC7523F90,
	SendMessage_Silence_mFE3E5EDEFDF09D408486752F4EA63CF1B716622B,
	SendMessage__ctor_mC33F30828D6C01539FFDD42A74E3A6BF40DB2858,
	U3CSpeakerBU3Ed__8__ctor_m4A8ED00A523A95298E776A4A523FF2A04F3D6724,
	U3CSpeakerBU3Ed__8_System_IDisposable_Dispose_mF7B8070E854813D83A83738E3A5AFF4A85A58C2F,
	U3CSpeakerBU3Ed__8_MoveNext_mA8E71C308F4A8390282D16D7ADDD7CC2B6DB59C4,
	U3CSpeakerBU3Ed__8_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m410D072C0C20CC5489B8A8A4782DF66577B670DD,
	U3CSpeakerBU3Ed__8_System_Collections_IEnumerator_Reset_m51FC493E4D2F9C532B5C80D086A39099F32302ED,
	U3CSpeakerBU3Ed__8_System_Collections_IEnumerator_get_Current_mE1A5CEC7B178D65FF55AC03932D40A96DA8697A6,
	SequenceCaller_Start_m41332745CA30FEA1C0829BEA4135F5AF9E6D21DB,
	SequenceCaller_playNextSequence_m93D85D4C5AE0F3CA407524A645E9FF0957E2EE32,
	SequenceCaller__ctor_mA530CC0AF0D6691536792A0B0CD5F19E90F78473,
	Simple_Start_m1C9FA4A3B0B27B5B1C506CCF283EE97DDB018408,
	Simple_OnEnable_mCD821BE1DC04000AE21CDA672EBFF1F624E0668A,
	Simple_OnDisable_mB8005CEC41D155AB7BD8FB97E17FFB3928B937F2,
	Simple_Play_m9CD479D8F27512D450A898A14DA9924EBAFFFA40,
	Simple_SpeakerA_mB1991EE6CADA025EE2E97D71140F44A5660A9DE9,
	Simple_SpeakerB_mC0390702E90CE0271A3334027138A6172B62C813,
	Simple_Silence_m4F72A39DDBE67998AD6D9F8581E51CA3D3FCC12F,
	Simple_speakAudio_m581798F2DBE6342E307BB14B0691461CC8828637,
	Simple_speakAudioGenerationStartMethod_m296BDE5A9B768EA4F0660FBB470DEB88D02CBBCE,
	Simple_speakAudioGenerationCompleteMethod_m38E89410F3E4BBA6D3651782F6D03633A3520EB6,
	Simple_speakStartMethod_mA826DE4011A5B0694B60B634FD60926D6481E840,
	Simple_speakCompleteMethod_m1211637DC968CF06BCD5CF8134208E492CA9AFA0,
	Simple_speakCurrentWordMethod_m796213B862E640057D70B16F81EA8EFAEF015421,
	Simple_speakCurrentPhonemeMethod_mF32175D48763E587F075E3581153707F39FF182C,
	Simple_speakCurrentVisemeMethod_m4C8425A23CBDE1E1E934C573C9B0F5FBC586AB51,
	Simple__ctor_m24CAE42DE5A97D4A78A0E2F5908A395D68F5939C,
	SimpleNative_Start_mFF5D40B326D469F0F6E40D718D3BF26ED6F436C4,
	SimpleNative_OnEnable_mC6A69A656CD8AD8992B01497F65A46812CCB4280,
	SimpleNative_OnDisable_mE36D0762819EE1FFC2F956D19F6F11E6B39ADED3,
	SimpleNative_Play_mA6B8662197FE766631A78D838D4EFC079AE5606F,
	SimpleNative_SpeakerA_m42BED662EB5E4E00DD1B8BCE9E31F4E750610314,
	SimpleNative_SpeakerB_m3D766AC08EA62475859730378366748FACA1AAEF,
	SimpleNative_SpeakerC_m7EEF291A446D634CA9C2F9C1550F6DB2C7CC5AFE,
	SimpleNative_Silence_m00617B943FB58A35B777087EFAB7DC0390EF488D,
	SimpleNative_speakStartMethod_m1D0B71EE7F27497F37530BDF4B7127D8CD04AA01,
	SimpleNative_speakCompleteMethod_m07B8077C2EE5E7C8FEF6B8F4270E64724521B0A2,
	SimpleNative_speakCurrentWordMethod_mE89C5A7A54ABEC4BBCC453BA18BA4CF487A2790A,
	SimpleNative_speakCurrentPhonemeMethod_mC9734C375F0EDAF7439911F1E8ACE438AF3DAA6D,
	SimpleNative_speakCurrentVisemeMethod_m0CB68E84B8A430FD69E468C3B22BE3B9A24C4520,
	SimpleNative__ctor_m31127891658699460AE9778FC73FDD04A949DD92,
	SpeakWrapper_Start_m9CFA5554AD17DA428A699DE3C595AFA320BF4BF1,
	SpeakWrapper_Speak_m8DA6C583BDA8A3859D1CE56C1942CF61C37A14CF,
	SpeakWrapper__ctor_m3B9EB9D815A23AA5688525C8BCEE0CB464FF52D6,
	MaterialChanger_Start_m0C2461F4790C9494834984CF01A5BE5EB9494649,
	MaterialChanger_Update_mC340AC29ED31872B79723D5FA28155F3DD532A1A,
	MaterialChanger__ctor_m58C145E9E4EDAD6376465516297155BB1C0F3C31,
	NativeController_Update_m1271D05F8D9E1578C8E2F32B20F8C63D74F1CA84,
	NativeController__ctor_m25EA044D304AD6E5549D2FF71D3E17973E75C74C,
	PlatformController_Start_m1DA2083109765F28D7EF847CC5F5E8EC64CE46C5,
	PlatformController_OnEnable_mEA0A4644683108150CA9835B725AC111FEEFA546,
	PlatformController_OnDisable_m3B6D967163C1F5735A4F0AF05A73705332CA7213,
	PlatformController_onProviderChange_m8CAF3F07F85DCB20C87E119D6169EBE98588D6DA,
	PlatformController__ctor_m7817457510FE2BE551FED2BF9ABF6891D34ACA9E,
	iOSController_Start_m477DA0F805B79FC61CB161673F5F83E7BB0FBE38,
	iOSController_OnDestroy_m5D07FE3DA9084A89A0EADF5091515FFCD1E2F4A5,
	iOSController__ctor_mF3C2B153851AE3D5EB35156F0B238F2AC14ECBAE,
	Bots_OnEnable_m98DF8572242B3BAB9A7D291A66553C92B7D4FF31,
	Bots_OnDisable_m3A2168E0D981363BA3B0C42546C0350DBB4B7BBF,
	Bots_Update_m4E417CBE773593D77E9C9535C1D8CDA3B9512097,
	Bots_speakCompleteMethod_m7818F9F9C31F2013363107CF30783D85C54BC9C4,
	Bots__ctor_m605DCE9CBB7C8CC6D471608FCDB8D956D775A035,
	Speak_Start_m45972028AA87462EE3A3187C648D033FAD75B351,
	Speak_OnEnable_m33CC7D1069143D300F4068FC1A5B5281A07FB576,
	Speak_OnDisable_m980C018063639E610F9215382EDE5EAFD3BD9337,
	Speak_Talk_mBBC47C45CB25E800DFA738D10A54CA87318CD93D,
	Speak_speakAudioGenerationCompleteMethod_mF525F97FFC2C1B1D85D6F059EE6C1AEDAF3BAB13,
	Speak__ctor_m6C8AF93900DA7F9F618BC91C7421448BF4486818,
	Speak2D_Start_mEA469FB054C5161F568D599A19F7C44807C13CDC,
	Speak2D_OnEnable_mF685910CAC43E9EB012E0F1085E8EFE06C9043A0,
	Speak2D_OnDisable_m0BDAB3F8590A468C22779C2BC384C23067FB1048,
	Speak2D_Talk_mAC06A03B03A34401BC6BF5A8517B4E95AD472606,
	Speak2D_speakAudioGenerationCompleteMethod_m4FF0ABF0BA1BAF93CDB12CA2E4D73B0476D456CE,
	Speak2D__ctor_mABEE6F3AD07A1A74069360891AE1F4F12704E6F5,
	SpeakSimple_Silence_m64D382560126AC78C96FB47ADC1EA49B753D7F43,
	SpeakSimple_Talk_m44E11EC19A6470CD0845F3B226BD04EB6EBF4055,
	SpeakSimple__ctor_m3379ADE145FF7388D2A087F1865C27568B50E4A6,
	Social_Facebook_mC2E09264868CD8006AD4D69B817D855488422A7C,
	Social_Twitter_m492C7F14348366FA7719E69749DBB30A76DA3B8A,
	Social_LinkedIn_m69D1E11EDE1C1748E8ED8463D54DD59513371D94,
	Social_Youtube_m5C4E721503773BADFE0544DD381114729E7F31A5,
	Social_Discord_m5C364639DE37DCF942CCF9A29944B535E5E39EC1,
	Social__ctor_m5661EFE8FEE7EC2324FDAA88191E3B5C6F284F67,
	StaticManager_Quit_mAC39DA93EB2E5BA9D210BDE9C23A09A4D4266504,
	StaticManager_OpenCrosstales_m5E5E7FC3F7DE7D9ED7330C6D4B60E5A686E9F5BF,
	StaticManager_OpenAssetstore_m949B8AD95A36C1364BEA2416A9E32D134340890F,
	StaticManager__ctor_m745654B0948B4DF9A2FD0FB65806A03164ABCC65,
	UIDrag_Start_mDB1DCDDEB984EDD5DF799896F85B936BFB18D54D,
	UIDrag_BeginDrag_m583EA2730DACC92BC96F551DABD1FBF04EA5FF8B,
	UIDrag_OnDrag_mD59F3EA41622E034504FFFB95F247C6CA398BF2A,
	UIDrag__ctor_mF4C53D49822FE9B02461E092AE6F7AC45A011113,
	UIFocus_Start_m880CB1FB7349D96ED9EB3CF27BE27A16BD2E5F24,
	UIFocus_OnPanelEnter_m33CF9E28098755A82CE6764377DF302C10F71973,
	UIFocus__ctor_mDEF7E588D05EBA2BF8ECFB87875BCD5A614829A0,
	UIHint_Start_m96229DC6152E6DC547F659396A38AB349084473D,
	UIHint_FadeUp_m9D94298919C4378BE30465D8A82923CBA316D3C1,
	UIHint_FadeDown_m3C90D1A432527CE936339156444BD7D108D729C7,
	UIHint_lerpAlphaDown_mC074A670DEDC2E2C823C0F84A4EBC6D960BD5D42,
	UIHint_lerpAlphaUp_m84B48E04DD5ABE68C1A95A77A51EA2B8E9C61BBD,
	UIHint__ctor_mD54D6ED200DAA952DD7D97643AE270D572902BCD,
	U3ClerpAlphaDownU3Ed__8__ctor_m137125C8737CD01C1A17E318878AFB2B0D0CDB14,
	U3ClerpAlphaDownU3Ed__8_System_IDisposable_Dispose_m82EA5F02C54838CDFAD7E3C549962D0DB1151FE1,
	U3ClerpAlphaDownU3Ed__8_MoveNext_m954DF2644E926149C6AC6AA1695D54705969FCAE,
	U3ClerpAlphaDownU3Ed__8_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mB42DD7F0CDC10AADCD99116A14BF77D16197AE49,
	U3ClerpAlphaDownU3Ed__8_System_Collections_IEnumerator_Reset_mB4243BFE9BA869109D22485266636F346DE3495F,
	U3ClerpAlphaDownU3Ed__8_System_Collections_IEnumerator_get_Current_m641305F2EB99B80E211B4E6E99E859C2B7AAE63B,
	U3ClerpAlphaUpU3Ed__9__ctor_m41FA43BCB2D14D39F0DC1E1AFFCECB0D18E02419,
	U3ClerpAlphaUpU3Ed__9_System_IDisposable_Dispose_mD5454F31F93F6B5DCCB420AB1241DD2E2DEE331B,
	U3ClerpAlphaUpU3Ed__9_MoveNext_mB9C62184F82A3D1EF71E0A03CE960D9F020EA556,
	U3ClerpAlphaUpU3Ed__9_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m07EF2B75C53B187A10E0ACC00AB20DF4CAB27CD7,
	U3ClerpAlphaUpU3Ed__9_System_Collections_IEnumerator_Reset_m1C29B48A4BFBB4956CB59DAF52A587455998D965,
	U3ClerpAlphaUpU3Ed__9_System_Collections_IEnumerator_get_Current_mA92CCF9323E6BC3E4B224B1CBF4098D02978B980,
	UIResize_Awake_m84548F0C66BF42D9B3D85FDC887AF9F592314470,
	UIResize_OnPointerDown_m1E7B045EF724B9EFEF997DE80322BBBC494E202C,
	UIResize_OnDrag_m8F2FC62C5CD268AA28FB7B2EED64DDCA2079BB9E,
	UIResize__ctor_m4B0D088874E80F5BF9D3321949439328C8BA3744,
	UIWindowManager_Start_mFE0D244F094A34B2D47F20D9DA45C19166630F1A,
	UIWindowManager_ChangeState_m98045ADD1B1A1380FF64496BFC4756B5F1051C59,
	UIWindowManager__ctor_m12928DA297937399EBC6FD9D27BEEF646E068ADE,
	WindowManager_Start_m1AFD4847D6D468A27278A2D72509D0D28362A7B5,
	WindowManager_Update_m17970A83622480894EA2395FF8A4C5560CEB8115,
	WindowManager_SwitchPanel_m6B16C8B9EA436D051DF3D864DD4C97B915F4DF55,
	WindowManager_OpenPanel_mCAACC7E648449EC4F47E3FDBC730E00B319A680B,
	WindowManager_ClosePanel_m5C051458EB873C2A040A526707370BD675CACD21,
	WindowManager__ctor_m7A48D60A914B455DE9A65EF38F6718DD9E92B782,
	AudioFilterController_Start_m46A3ECF78AEAA513D900EE873BF9949CAD847D8E,
	AudioFilterController_Update_m72BAE1F4036EE7CA06B582ED18530AFF9A05C299,
	AudioFilterController_FindAllAudioFilters_m034DF26E117870682598FD340BAA92DC5E89BBAA,
	AudioFilterController_ResetAudioFilters_m40FB21296CCD12E6653E5F77C78690B4A994A781,
	AudioFilterController_ReverbFilterDropdownChanged_mAAC257AF30A94C6356960601CCFB334C5079267E,
	AudioFilterController_ChorusFilterEnabled_mAA060E1FF642CD40B9F6CB484D32037FA5183BA1,
	AudioFilterController_EchoFilterEnabled_m50CFF045EF70D8BFDE81F8870BB4814E4244974E,
	AudioFilterController_DistortionFilterEnabled_mF9263E851AFEF9562F7CBF3D09ECE96622C08C12,
	AudioFilterController_DistortionFilterChanged_m1687F0C6E768C574E49CF0FA67252982E33952FE,
	AudioFilterController_LowPassFilterEnabled_m51DFC7705017F3ADE4F45F3DBFA1196373AC376E,
	AudioFilterController_LowPassFilterChanged_mBF89E179EF3BC9D410692081F15C0BB3D328D2F6,
	AudioFilterController_HighPassFilterEnabled_m92EC04676167F39543722982FF01F16733CCCCCA,
	AudioFilterController_HighPassFilterChanged_mC2A99980EF8375A12DE85CE405AC42B3E75001CD,
	AudioFilterController__ctor_m1088182076695D4E2EB7279B7D327D3B4E7162F8,
	AudioSourceController_Update_m70490F7F5C8D24C507159A0673DA13A22C6A4E3B,
	AudioSourceController_FindAllAudioSources_mFB4794AB3C518C5C5FECF97A18C9FA265200C2FA,
	AudioSourceController_ResetAllAudioSources_m993ADD16753B6D9A0D4069265D2C656F35333EC6,
	AudioSourceController_MuteEnabled_mDD74B20BD420EAC500C27B65D56643D108FB8040,
	AudioSourceController_LoopEnabled_m9F760D3EC7FF088B79B46713A26A4C25E0563A1E,
	AudioSourceController_VolumeChanged_m6620B0D291A9288CFDAF7FE00CCDE144205C2C2B,
	AudioSourceController_PitchChanged_m5FB7E6D12DCF4E48D4B89C11A3753608C1A5F532,
	AudioSourceController_StereoPanChanged_m57837284AAA56B5857B000C1771FAD9A5A862FE0,
	AudioSourceController__ctor_m4BD866B5A5C1DDF43D69854995780B526BFEEB6D,
	FPSDisplay_Update_m3DA30CE583BE8D5C3CF67953C12F519B237E77B5,
	FPSDisplay__ctor_mE69C95D9F1718F2320861BEDD4AE137497BE4942,
	ScrollRectHandler_Start_m0D04331DA1FFEA8B60FC0ED473CAD5E51613B8CB,
	ScrollRectHandler__ctor_mC6321DE1AAC54297ED22ED0319EB34682DE8DF12,
	SurviveSceneSwitch_Awake_mFD738893FC03E2CA8366A3204CBDAC07DECC1045,
	SurviveSceneSwitch_Start_m44E86BFCF43176D436D60606B744027B87D7FD76,
	SurviveSceneSwitch_Update_mEDD0951497831E3F6CCDA1CAAB4A1D8D40AC9DD5,
	SurviveSceneSwitch__ctor_m9854DC25CA3D5BDEDC4044CE30938F23124ED793,
	TakeScreenshot_Start_mAA66A595B3DAF7CE42C4ABE8F8D2C5E5697EFDD1,
	TakeScreenshot_Update_m38664624F52BD6B321A126F6FC5846D6313F4948,
	TakeScreenshot_Capture_m709EDE234BAE9A8D77B3548B28E11CC5C3898281,
	TakeScreenshot__ctor_mB92BF7FD062841C52807F9187CD0AD9DCEC8D8C7,
	BackgroundController_Start_m314E46A6144FE74BE06AEE5E05D83CE640E17D08,
	BackgroundController_FixedUpdate_mD05CBA5EBA4998873FA97D27323F0D756DCFB9FC,
	BackgroundController__ctor_mACA32916E8B5713D92710C0C6DA69E89DE6293D9,
	BaseConstants_get_PREFIX_FILE_mF05531A4738FEE2B43B56F613C8543B56677A58C,
	BaseConstants_get_APPLICATION_PATH_mE87E6E9FF38895E8F8E1D78DB610DD417DDC85BB,
	BaseConstants__ctor_m423B5C856F2C469942209CF30A993C3BD4A969E3,
	BaseConstants__cctor_m08244E973BC852F1CD2C3F222AA017980377C085,
	BaseHelper_get_isInternetAvailable_mF070BABEACA6E81C6C55C8FC65B28351FB75A1F7,
	BaseHelper_get_isWindowsPlatform_mA89734268B9F4512EED642DF4FD30718BF0698DB,
	BaseHelper_get_isMacOSPlatform_mC7A38F4150094D9E1C539292EF669C43FA2567B0,
	BaseHelper_get_isLinuxPlatform_m417DC75B6784EF54E5B5BE01CD05705C3BD1D7DE,
	BaseHelper_get_isStandalonePlatform_mF704F98AD14E6122F66B2A8AE7AC406129FEA067,
	BaseHelper_get_isAndroidPlatform_mCE5CA1352C1F80F40F41A29BF1C27292E840394F,
	BaseHelper_get_isIOSPlatform_mC37175C891506DEB337DA607BC64D69709286A75,
	BaseHelper_get_isTvOSPlatform_m9875AE921ECCE6CC71E9839BFAE77AE234876AFD,
	BaseHelper_get_isWSAPlatform_m6C69C0C0891C3113A253E643E0821F65BD87B5EA,
	BaseHelper_get_isXboxOnePlatform_mC6F3A25D6E8CFC89ECBA413420C9B8AC9BA4BAD9,
	BaseHelper_get_isPS4Platform_m0E05F11A7177556AB547379BF883888973A5BD6E,
	BaseHelper_get_isWebGLPlatform_m33211FB09A47645AA051EC76948852BE0025D0EE,
	BaseHelper_get_isWebPlatform_m5A3E8B17AD90A821B6DC419FBECC6CB800ECBFB3,
	BaseHelper_get_isWindowsBasedPlatform_mF5EEC2933706EDA1EDFEB2F1FD8E3FE8A41431A5,
	BaseHelper_get_isWSABasedPlatform_mA559B2E3E0D1646C0C427B113909B963B3D40A74,
	BaseHelper_get_isAppleBasedPlatform_m831F2787A6CD87908977B20FB1F2233D84D17E62,
	BaseHelper_get_isIOSBasedPlatform_m5DAABF1EC5DC3B8947CC4B636EB59C8AD81C4A14,
	BaseHelper_get_isEditor_m0454F7BB9528D9AEEEA24C95BC322A44839DF874,
	BaseHelper_get_isWindowsEditor_m1ADAFD1FB58136C7BE74018F625BF8C9ED655DFE,
	BaseHelper_get_isMacOSEditor_m96E71133D83F40307304E28B3F103B14ECEAB0C4,
	BaseHelper_get_isLinuxEditor_m0FEEFCA649971D8A75CEF0B563B6965866EF5240,
	BaseHelper_get_isEditorMode_m72F760C50ED845E97E3A98BAEAC42398B3BD2C11,
	BaseHelper_get_isIL2CPP_m266EBBA79AEE873C4A8D4D820D19CC7BFB4376A8,
	BaseHelper_get_CurrentPlatform_m69DEAD89DD0EB052044CCE8FA2E94578A17609AF,
	BaseHelper_CreateString_m8187B680400CD1F2A77511DEFBC91152D1F6D9EB,
	BaseHelper_hasActiveClip_m4C947A64FD354BCBAEA2078E0E1A919578D9CCBA,
	BaseHelper_RemoteCertificateValidationCallback_m8F0689C2DA5F597E2B631A7A0F66E67F1CE48613,
	BaseHelper_ValidatePath_m7CCEC41977D556D08B0267C3D40E999803F3A225,
	BaseHelper_ValidateFile_mEAF8DC5346F6B807D1C8C7C6ED24F88DB0F31242,
	BaseHelper_GetFiles_m1C040E9DE5359EF69F773CFCC1DE845322EB7657,
	BaseHelper_GetDirectories_mCBFE3ADE894BF3CE041C0650024856AEF01A1A04,
	BaseHelper_ValidURLFromFilePath_m9235C556A23015F250EA2818797280491489EFC8,
	BaseHelper_CleanUrl_mFB396194548994FD98A15424C1244FB487A8F699,
	BaseHelper_ClearTags_mD93B30D4885D2E5DCCA1672188CD20EB76B01C4F,
	BaseHelper_ClearSpaces_m235DA279F449F5AD4E61A2F82D5C414DB86322CD,
	BaseHelper_ClearLineEndings_m8A5A3B1AFCE29A057A7D0EF2DE7576734251456D,
	BaseHelper_SplitStringToLines_mE1E3904BDE694387F27CDAD680DEF2A490ADC9FF,
	BaseHelper_FormatBytesToHRF_m80A776532503F8E7E052C62C139F3ABB8E475692,
	BaseHelper_FormatSecondsToHourMinSec_m3822B32615C718A3D267851AD914A984189F59F8,
	BaseHelper_HSVToRGB_m20DA8918AE1E0B80A7E6DA9AE4F20A63CB4840F4,
	BaseHelper_isValidURL_m6EDE78C570F5411F5A3DE99052A83079DC26B59B,
	BaseHelper_FileCopy_m8AB3ED391A4D1FFB9298A21A8ABBCB532C2FDD09,
	BaseHelper__ctor_mEF441B4F46F49B04BF884A05686974DED620C20C,
	BaseHelper__cctor_m8F4F00BB889949CF9761BA21335D45D71CEB629D,
	U3CU3Ec__cctor_m9CAEDD2D496807D1F6D85855AFA7AE14980E6C6B,
	U3CU3Ec__ctor_m4871038D3BC7C094E3FA3BA86564AAB953E35B3F,
	U3CU3Ec_U3CGetFilesU3Eb__59_0_mE8CAFED60DE2976F8C3F60C65EF8FA97AA7279CA,
	CTPlayerPrefs__cctor_m02F3035D4A6B31999D6F667215A2690748135CCA,
	CTPlayerPrefs_HasKey_m343530DB4F0540483EBAA38852F22F4BDC494AB8,
	CTPlayerPrefs_DeleteAll_m691A7F70901A1FDE9195F9B8D6CB9D79F67EFDD6,
	CTPlayerPrefs_DeleteKey_mE952677FC2CF7F1F6A21B435FFC7B700247487DF,
	CTPlayerPrefs_Save_m4B4CD209257285EB483F161A464D599D614B69D4,
	CTPlayerPrefs_GetString_m4A24B355B496A4C92544CEE431188605D675A7CA,
	CTPlayerPrefs_GetFloat_mC82EA1654D6558D8854AC0B2FB6C1EFD273E8BAA,
	CTPlayerPrefs_GetInt_m1AF5A20A28BAE9A740A05FCE2261C8BC4EE85C1A,
	CTPlayerPrefs_GetBool_mCBBC02069CA83ABC141BA0EBA91D5D2EDE3B6CD9,
	CTPlayerPrefs_GetDate_mD193EF979FE51EC4CA7E507BD3F7DC7C0746A5C3,
	CTPlayerPrefs_SetString_mB5D86CF95542A08310B0FE892F040B246770C234,
	CTPlayerPrefs_SetFloat_m257A15CE9D9E94A0A32E829BAAEC438502558CA8,
	CTPlayerPrefs_SetInt_mE40F4D337B12BD39EE80C14CE386A3D703E245EE,
	CTPlayerPrefs_SetBool_m4D6D3AB173DD3E9996FB57DCB2E47B88D1F00690,
	CTPlayerPrefs_SetDate_mB2C8336DC4873AB8D8119D727B0CDDFA87990594,
	CTProcess_add_Exited_m2522C9CC2EBACBAC756B5AEF8D857746B1B5A227,
	CTProcess_remove_Exited_mC1354807453D23C4A3C8EDD365BBE1B688BE89B8,
	CTProcess_add_OutputDataReceived_m625D0759C272BCB29A6A7ADCF4214F3BA2D332D3,
	CTProcess_remove_OutputDataReceived_m4BB9465533EAD71763935B84E2765D38B559C7D5,
	CTProcess_add_ErrorDataReceived_mEBC45F29474ED0E1E3CA3E226271FA0ABEFE91B1,
	CTProcess_remove_ErrorDataReceived_mF924C5DA48FCE375169CC53B954C3BF4ABD493E3,
	CTProcess_get_Handle_mC3608F2A6A30685484B9144B20D12BD0399D4E32,
	CTProcess_set_Handle_m5C53B2AE2BC840DE6F1435598135115AB14DAA14,
	CTProcess_get_Id_mD17750B0020A2B49BBC1B864295B2F34930A7DCB,
	CTProcess_set_Id_mD77F7079C3005B95ED3BC30F796BAEC98BF5E26B,
	CTProcess_get_StartInfo_mAB73C444D69AF78E2E2111B2680435469C30BDD4,
	CTProcess_set_StartInfo_m135807252222DD15C7321BB09EF834CCFA3E0881,
	CTProcess_get_HasExited_m2D586146FF76514478807E12CEAA555D9C1C0D51,
	CTProcess_set_HasExited_mD528F17ECC59BED4513599C205B57A875532DCCB,
	CTProcess_get_ExitCode_mE192F777FF0E784A80117D0CB5A541CBDFF26804,
	CTProcess_get_StartTime_mAF23EA137BD076A31606E0175BCBED9241F2C40F,
	CTProcess_set_StartTime_mF73C4A51A77CE85B55ECF03A6AC3F5FE43654188,
	CTProcess_get_ExitTime_m06BAEDCC06277C766A407B9E331538C7975F18CD,
	CTProcess_set_ExitTime_m762BB485BBE10AC9366FFBD0A3460F5242F5571C,
	CTProcess_get_StandardOutput_m5E2E8567D9990F291CCE025867CBCF3B03375377,
	CTProcess_set_StandardOutput_mBB889EA2A78ADF52AE97A319985AEBF65EFB4001,
	CTProcess_get_StandardError_m1FA605D25447DFAB49640AF6C892C09436776B9F,
	CTProcess_set_StandardError_mD6993571CA67FFEC04C2D036EB9240115EAB8802,
	CTProcess_get_isBusy_m4F3A086625E52181F3EA327E3598675F0C3A47F5,
	CTProcess_set_isBusy_mAC83ACD3B1A2AF71151EFF8F7C1E57DA8D529557,
	CTProcess_Start_m4986165458B86387398FF3644EB0A0643104C301,
	CTProcess_Start_m3842565EDD270C8E632F3194CFCC00DFE2AB73DB,
	CTProcess_cleanup_m8C1CC32693352813FE9C6AA08F01F0A3CF195451,
	CTProcess_Kill_mAA92AF83D1CEC3BDF79B85B8F0BB5AE934A880F7,
	CTProcess_WaitForExit_mF88C9E305793BB013BE8F743CF17430AEC35E3B5,
	CTProcess_BeginOutputReadLine_m90ABA2BAA358F225728ED0541DCFB3C4A9F0C61E,
	CTProcess_BeginErrorReadLine_mCEC3034D82DBC636D9F9C034FAF64A051EDE618B,
	CTProcess_Dispose_mCB5F569B860DFD16AE79679A374CF9AD6E092527,
	CTProcess_watchStdOut_m12CBF4165FE4408453AF4425FD4DE1D97818C55A,
	CTProcess_watchStdErr_m27D5B56D6C1B8FECDEB906CD7435E37889FDB6C7,
	CTProcess_createMockDataReceivedEventArgs_mB8587D380D6A6E68D561D6F08A015D9A81122F3E,
	CTProcess_onExited_m9A22B8DAE6ABF12300A6CD2ECF97FC194681E325,
	CTProcess__ctor_m906058D39A9F785FDF8EF726A3CFC05AC1DCFDEE,
	CTProcess__cctor_m96CC56A262D1B4AF43DEDB27B4B9616EE0A921B2,
	CTProcess_U3CBeginOutputReadLineU3Eb__60_0_mF7FDEECEFD6B1DE5646F5204C7B69802154B4AF3,
	CTProcess_U3CBeginErrorReadLineU3Eb__61_0_m19AABDD682A9810182567AA68BFDF32AB957445C,
	CTProcessStartInfo__ctor_mC456AD13C5B40D29591D4324AE88C400CAE483BD,
	CTProcessStartInfo_get_UseThread_m34262C2CD9CB925BCFDB438D610DDE432F1AC6B1,
	CTProcessStartInfo_set_UseThread_m86AA2DA43064F088C2F979EA3B9369C4C030A32B,
	CTProcessStartInfo_get_UseCmdExecute_mB67F1E3D55EBB016C86D75C4F09356D04D02333E,
	CTProcessStartInfo_set_UseCmdExecute_mB553AB506E410A0294A873F6592EF753B26FBB85,
	CTProcessStartInfo_get_FileName_m1763CF197CE0D3528D744A53B2DAE98F890279F9,
	CTProcessStartInfo_set_FileName_mFF77C984D27BD97D9D6724F4154E65439EADAC0F,
	CTProcessStartInfo_get_Arguments_m3A347FA469BB43787D2877BB70514D039FC9D7C8,
	CTProcessStartInfo_set_Arguments_mA9CB0587438FF858CBF67F0E8A51A987962A5FE8,
	CTProcessStartInfo_get_CreateNoWindow_mF07A129AB1002CC6669265A2B72A2819386D21E4,
	CTProcessStartInfo_set_CreateNoWindow_m27DD6357C383BC237A64790632439866995E5EA9,
	CTProcessStartInfo_get_WorkingDirectory_mD97C799110A52EE36167FB77EA6F695DE0C03D26,
	CTProcessStartInfo_set_WorkingDirectory_mBA1BEE7819F6D96B8E8A61BEC985D1627A1E77D9,
	CTProcessStartInfo_get_RedirectStandardOutput_m2421774316C53C2DBFD8749A7F52D5A2A5C658A9,
	CTProcessStartInfo_set_RedirectStandardOutput_mEB9777CA86502455F7C8AC29644AB604CF18CC84,
	CTProcessStartInfo_get_RedirectStandardError_mFA019C90FA0BFCC650C8A92E1EFD6BD1EDB83E96,
	CTProcessStartInfo_set_RedirectStandardError_m322C697C5104C0A4F0BD1E5B721B800D7B896B17,
	CTProcessStartInfo_get_StandardOutputEncoding_mF883E4B6A58BF6F6189EC53A1B36CED7051CA740,
	CTProcessStartInfo_set_StandardOutputEncoding_mE7BA0B4AD89C1B8476037466A7149165AA304DBD,
	CTProcessStartInfo_get_StandardErrorEncoding_m8A4F0A1E361D9A522ED82A49C6F7BE05C09801ED,
	CTProcessStartInfo_set_StandardErrorEncoding_m3C59321FB26FE02DFBC8307F22D060FE9ADC2628,
	CTProcessStartInfo_get_UseShellExecute_m96AC26978B39FBA130D519C81194A5B6B101792B,
	CTProcessStartInfo_set_UseShellExecute_m2C838389B42C35BBB7CEA1BC337285F1771849E6,
	CTWebClient_get_Timeout_m05E7AC7AD82CFD0A255A1D753B3AB413E0C15C9E,
	CTWebClient_set_Timeout_mC93FB85FA7A3F30D35CE10A64A25ABD18D206079,
	CTWebClient_get_ConnectionLimit_m74A0F4DC526B36E55EA0857E3F953453622189CE,
	CTWebClient_set_ConnectionLimit_mFBDBF8F1C18BC022FC5B773E3874E8B9569F9068,
	CTWebClient__ctor_m2C17945D74024B83C76B5BB86DBF0DF78E40B537,
	CTWebClient__ctor_m60AC97676C10006B910233B2C508C5CE4E37B67E,
	CTWebClient_CTGetWebRequest_m73349F6C28991D04220A3D78ADA73D30D5400260,
	CTWebClient_GetWebRequest_m6AB86C07D3BA594406B31E2C21ACAF1F4AFF8650,
	FFTAnalyzer_Update_m273EE0E828B6CBC5E1ADA8D77BB751CC54389D98,
	FFTAnalyzer__ctor_m79834DBD77EE0536F5996182B5A318AA1BFCA1D8,
	PlatformController_Start_m45C32ED17D6746F9DC0C139B05EDC89CEFBF0C3C,
	PlatformController_selectPlatform_m44CD923C6E139A6DB86AC3D1BCAE13099EDBCC3B,
	PlatformController_activateGO_m8F5A1B0D38F30CE1D2528D23CCD21FD150046B67,
	PlatformController__ctor_m246265302FC8CBCE52B8C4DB078CB7B5593B4FAE,
	RandomColor_Start_m391CF84B08071FF3ACB0963EB28D2730BAD3F21C,
	RandomColor_Update_m80AF1317FD5A6E02EE266F7916C32167C811A267,
	RandomColor__ctor_m9D12F994D18E58B4E6917402BF320A05B4C07D38,
	RandomRotator_Start_mD8CAF4274B1003904752C232D60FF6D740AE46D8,
	RandomRotator_Update_m2B257E670DEC91784B8CF373497F18B3B88FBEA5,
	RandomRotator__ctor_m5B8F4189F25BED4B6E89D524DA329BF1F641416A,
	RandomScaler_Start_m846A6856675D0177F6879AF36DB91392267F17A0,
	RandomScaler_Update_m7DF95D0941A662AA475C77084EBA89A60C0908B0,
	RandomScaler__ctor_mF4467ADA2B0DB1F1B451D3568A5CAF22042121D7,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	SerializeDeSerialize_get_binaryFormatter_mC4767CFD40FDE7AFFBF0470F2B2EC2A624AC2A17,
	NULL,
	NULL,
	NULL,
	NULL,
	SpectrumVisualizer_Start_mD5896C0A90B9B0713E0EAF1780AD2A8CE8AEE124,
	SpectrumVisualizer_Update_m01893AC967BC7986B454BF0BA963EFD0114C8157,
	SpectrumVisualizer__ctor_mD85D85B41B224B493E008B394E4059C5926C140F,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	AISbaitso_GetResponse_m92F8D67E443B1F70BDEF0E2F1C9B2BBBFC0E3880,
	AISbaitso_GetResponseToQuestion_mA07B1BEB1020EEE9B2646D42A386EE2A69510509,
	AISbaitso_Has_m7AD3A568B623DDCDA86DBEF00C5F058C256EF1D6,
	AISbaitso__cctor_m5DF3CCEAB881D3D50553D42187D98574276BF062,
	BaseSpeechSynthesisPlugin_AddListenerSynthesisOnEnd_m349CC7EE80F2CB6FC118DCBA35E219B51A2BDB4F,
	BaseSpeechSynthesisPlugin_RemoveListenerSynthesisOnEnd_m1C5815D8BA832170A0663F65A4FC291FC9EA08C6,
	BaseSpeechSynthesisPlugin__ctor_m84F6B50B8CEEAA97B481661B44602BB7A1DC1630,
	BaseSpeechSynthesisPlugin__cctor_m0772D220AFCB787BAB691542B6C5F03F1930257A,
	DelegateHandleSynthesisOnEnd__ctor_m87F5D8EBC016944834D816CBA11DD7D5F4B16AD8,
	DelegateHandleSynthesisOnEnd_Invoke_m62188BF2D20FDD789C24674F84BF9CFED0425468,
	DelegateHandleSynthesisOnEnd_BeginInvoke_m6C0B3FF3CE090B6C527F9A0204A7708773810A41,
	DelegateHandleSynthesisOnEnd_EndInvoke_mBBDC80EEC03EE6936AC5C46494A718C2FDE192D5,
	IdEventArgs__ctor_m3F4EDCAC9D21D6A153E0348D146091C35C48F402,
	SpeechSynthesisUtteranceEventArgs__ctor_mC45FC5899DDF647619C228F6326E9A43FF0E0634,
	VoiceResultArgs__ctor_m277AD6EEBCFA017FAAD2C579B7DFAA265422CB65,
	EditorProxySpeechSynthesisPlugin_Start_mAB6CC9376C62684D77940A14182C0B235A4B5A18,
	EditorProxySpeechSynthesisPlugin_SafeStartCoroutine_m4E70BB8A1E92CAF5FFCAC69B8071BD7161890A8A,
	EditorProxySpeechSynthesisPlugin_CreateWWW_mB458D106E623F047352A52FE0E520A5DFE8207DB,
	EditorProxySpeechSynthesisPlugin_IsEnabled_m8A9D31B5EE1BF6E82743051EA357B1AB6008556D,
	EditorProxySpeechSynthesisPlugin_SetEnabled_m1651BB6A12BDA66C5C022C79B42BB85028291A56,
	EditorProxySpeechSynthesisPlugin_GetInstance_m355F6B059375F3BF4C4C7FB01807D0F3F05B80F8,
	EditorProxySpeechSynthesisPlugin_EditorUpdate_mA08840880880CBAE6B9B15DC071A1D2AC75CFA3C,
	EditorProxySpeechSynthesisPlugin_StartEditorUpdates_mEC4D0E6DB82FF7FF59325D83E8166B12484A6D40,
	EditorProxySpeechSynthesisPlugin_StopEditorUpdates_m9B63B85069F734E126F5D6950280B05D967F09AF,
	EditorProxySpeechSynthesisPlugin__ctor_m58C8375E9EADEBDC2A52F6EA6DF2960358F6F108,
	Example01Synthesis_GetInstance_m3DF6430F3260EAA457ACEE38BCA5D2C7A8CE20F8,
	Example01Synthesis_Awake_m7B07EE004260C539987F4DEC7050C3DCFE5AE156,
	Example01Synthesis_Start_m08B4479794B479D161F1950CBACF9031DD93A38B,
	Example01Synthesis_GetVoices_m5222C0293EAA6FB209232F0A73D75DA1F22C5937,
	Example01Synthesis_SetIfReadyForDefaultVoice_mCF86709148341DB44F362D6C26222C06C04CF34C,
	Example01Synthesis_Speak_m155718B53A66899F8776D360F3CC9965DBCB0552,
	Example01Synthesis_SpeakText_mA0D5C6BCE8473243C73D916354393FFC00961250,
	Example01Synthesis_SetPitch_mC504A0CAE1FDDA39B5B272BDB63B5B4C545017E7,
	Example01Synthesis_SetRate_mE3D46DECE1A051928BDAAC3C9635D6C7986DCC3E,
	Example01Synthesis_FixedUpdate_mDA413F5BFE18B9FDA8630A255D22FE0E6717B090,
	Example01Synthesis__ctor_mD2D26210701B0F8751AA940BE2DF9912A53D08C3,
	Example01Synthesis__cctor_mE809C8CB54E090AD9C1E8830C2555CAD2D854B36,
	Example01Synthesis_U3CStartU3Eb__22_0_m949EFCBA0A18AB6E4A3C65A79A54476776887C4A,
	Example01Synthesis_U3CStartU3Eb__22_1_mEC15BB057506B9CE63CECC952906E4682748D97E,
	Example01Synthesis_U3CStartU3Eb__22_2_mC83E3F257936B1EB16E78EA0219E028DD56CBF80,
	Example01Synthesis_U3CStartU3Eb__22_3_m1255DF35B820D4D4A18882F8EC138AB4D85A2671,
	Example01Synthesis_U3CStartU3Eb__22_4_m02C7CF99D3995A5F4C83ED560B26073641866E70,
	Example01Synthesis_U3CGetVoicesU3Eb__23_0_m1B6211E861319A5905671E0865865224B7A85CF2,
	Example01Synthesis_U3CSetIfReadyForDefaultVoiceU3Eb__24_0_mD6A9EC56F9C352204D6D74AA6C1E21F12E536013,
	U3CStartU3Ed__22__ctor_m8F63FDE1DD445BADF80206E5F3E93DDD0680FE84,
	U3CStartU3Ed__22_System_IDisposable_Dispose_mCEAC0024B672F6415833CDE42B9193B11409A425,
	U3CStartU3Ed__22_MoveNext_m82F14BFF6A9A255849EE5800831175CAEE9A3F41,
	U3CStartU3Ed__22_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m3AD046D2F672A996C8C2414FFFF51CFEE0470407,
	U3CStartU3Ed__22_System_Collections_IEnumerator_Reset_m85E9BF96346CE3D32AEDC2DD1A49612D23AC3EA4,
	U3CStartU3Ed__22_System_Collections_IEnumerator_get_Current_mA1B1C2B5B63C73CB6AC75FA7BB6360950BBDA5B0,
	U3CGetVoicesU3Ed__23__ctor_m09932D130C7602C2D2ABE2EDC806CC7F93359C34,
	U3CGetVoicesU3Ed__23_System_IDisposable_Dispose_m2DC14F993D619A7EF6FC06C8267F07CDB90FB520,
	U3CGetVoicesU3Ed__23_MoveNext_m8CFE63BE1BED153DA6C340BE6AE44AA6BFB29D0A,
	U3CGetVoicesU3Ed__23_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m86945793027A7CCBFD284B2CA99CDB8F25F548F6,
	U3CGetVoicesU3Ed__23_System_Collections_IEnumerator_Reset_mC5A052AA5B31420FF5DB67F371DD9210CEA04744,
	U3CGetVoicesU3Ed__23_System_Collections_IEnumerator_get_Current_m39A69FC140927C187412E3E2DF98D7E1493A857F,
	U3CSetPitchU3Ed__27__ctor_m003F38176D39FA4206F6183C0D39B1501CD8E9EE,
	U3CSetPitchU3Ed__27_System_IDisposable_Dispose_m680F577B98258C3E1CC4C275D39BCEEE86722879,
	U3CSetPitchU3Ed__27_MoveNext_m1001B876947EDCC6ABAE10A3657C7942328D2755,
	U3CSetPitchU3Ed__27_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mD2A3DF093FD380180CCEF194C5D8964455D1ACC6,
	U3CSetPitchU3Ed__27_System_Collections_IEnumerator_Reset_m80B114456DDDB5BAF8B39F9ADBA407CF1606FA1B,
	U3CSetPitchU3Ed__27_System_Collections_IEnumerator_get_Current_m12C49895937FE64B85129B8C377E5A3AFF30595E,
	U3CSetRateU3Ed__28__ctor_m00BBD7D8FD9177152A89D342340510FF249A036D,
	U3CSetRateU3Ed__28_System_IDisposable_Dispose_m56D7E3D6BD827138BC5D04C0361D36F6F728BD37,
	U3CSetRateU3Ed__28_MoveNext_m5D79517D9ACEB5CA8B0AA83389400727EE7A4E81,
	U3CSetRateU3Ed__28_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m464D800221095630A17ACFB9260ACD0394848238,
	U3CSetRateU3Ed__28_System_Collections_IEnumerator_Reset_m4A460D7A3E015022450C8EBC12A92AC5E0E72327,
	U3CSetRateU3Ed__28_System_Collections_IEnumerator_get_Current_m92D4A794D0F0532A96D04F1F96D0520515BB6BFE,
	Example02Proxy_GetInstance_m7A8A611D91925D17FFC3EEA257ED54DE9149665E,
	Example02Proxy_Awake_mE67BE75F01BF4DC8709C9DE897DB3B7ED3ECA098,
	Example02Proxy_Start_m37473AF15D3767E2869EBCEE623BAE1EA5A3DD74,
	Example02Proxy_GetVoices_m5CC03E5F7B7566BE7120A098AF29B93443E8FFEB,
	Example02Proxy_SetIfReadyForDefaultVoice_m63BB446DB1739490DC8EE511D5F38CD0C8EF1181,
	Example02Proxy_Speak_mBF06DB3BA5C77BC5D1462002F3AD60F05CF33080,
	Example02Proxy_SetPitch_mB099B881684EA372B6A219FB562B7DE2FBB807EB,
	Example02Proxy_SetRate_m5D08473E4431DC7FC479BA30C3C46A7C1AA7496E,
	Example02Proxy_FixedUpdate_mCE8168F5C14E32DE8400EB9F918F484051BD786B,
	Example02Proxy__ctor_m24BD4FB850E20623BEE8F117FBC92E739B9C6DBD,
	Example02Proxy__cctor_m0EE76FFC56B0E08CC88FFCB532D10A1F39A44EF1,
	Example02Proxy_U3CStartU3Eb__21_0_m9D025B9B4BEF452A16A7982B1D97903206B6932E,
	Example02Proxy_U3CStartU3Eb__21_1_m995FFE286128B847EEFC2C8AC1466000A0049BDC,
	Example02Proxy_U3CStartU3Eb__21_2_mB754C6C46342D6CA55790E4A35598DE3CB0FAE37,
	Example02Proxy_U3CStartU3Eb__21_3_mB22FCD82AF377C1F1E2DB07466B114D5C04EFAFF,
	Example02Proxy_U3CStartU3Eb__21_4_m582889A0D069349173DEAECEBF17FBD8D31ACD2D,
	Example02Proxy_U3CGetVoicesU3Eb__22_0_m42F9EBAD38B20DA39C2048894A8D388D9DAFC868,
	Example02Proxy_U3CSetIfReadyForDefaultVoiceU3Eb__23_0_m91A0BD1BA1727D47E51FF7F98D861C1A3E4C998C,
	U3CStartU3Ed__21__ctor_m34D03537D704CFF9A884EB90E3B2F486006DFB88,
	U3CStartU3Ed__21_System_IDisposable_Dispose_m3A2D102BC25169A90D6112DBA9F6CEDB2AB0D302,
	U3CStartU3Ed__21_MoveNext_m067CD367062F353368FED180ABBB77DB57546F99,
	U3CStartU3Ed__21_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m93363FB8A68A91796C20BAFDA50A3AF8F123EC4A,
	U3CStartU3Ed__21_System_Collections_IEnumerator_Reset_m58501F0C8729C05E18577FD05B05586917C3AC30,
	U3CStartU3Ed__21_System_Collections_IEnumerator_get_Current_mA3E9E23A0E3353169EB46A29FA714C083DE7C027,
	U3CGetVoicesU3Ed__22__ctor_m60E3A6BD3C508E0EE96E30CBB6A4C3E39A7B2565,
	U3CGetVoicesU3Ed__22_System_IDisposable_Dispose_mD45D8FFE08464334151FFC3DA894F01E8989F0F0,
	U3CGetVoicesU3Ed__22_MoveNext_m18E1E909414D4890F0216EE8B065199D9471E793,
	U3CGetVoicesU3Ed__22_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m203840620BCA464A576234B4503A43D2F4638E10,
	U3CGetVoicesU3Ed__22_System_Collections_IEnumerator_Reset_m134E6832088185074D6B6BC66965C2A0161DD2F0,
	U3CGetVoicesU3Ed__22_System_Collections_IEnumerator_get_Current_mAAE532A14E45572D45BD62073D30AB28453F23F6,
	U3CSetPitchU3Ed__25__ctor_m7B27B1C736DD32C058AC4FEDE82949BFC73E63FE,
	U3CSetPitchU3Ed__25_System_IDisposable_Dispose_mE29FBB4928139BE55235E76AC18A944D31A82F11,
	U3CSetPitchU3Ed__25_MoveNext_m0C172E1649ACF901C5D8F528BE229C3D286C4C6A,
	U3CSetPitchU3Ed__25_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m349DED4BC47B17F94F69EB6F7730D002F9D2A916,
	U3CSetPitchU3Ed__25_System_Collections_IEnumerator_Reset_m550C252F8CEFA833ECB2E74ED259602D355615BB,
	U3CSetPitchU3Ed__25_System_Collections_IEnumerator_get_Current_m7DC2266D9F998979A595E0C599798F981ABC3D9B,
	U3CSetRateU3Ed__26__ctor_m30B3B76B925AD990DF7BD5B8DC85B7676E5A19DC,
	U3CSetRateU3Ed__26_System_IDisposable_Dispose_m77B3B26C13157B46E0FECB42F5B0FCE7EDAAA07E,
	U3CSetRateU3Ed__26_MoveNext_m477A5B9C84EC9C9F308CA9B2365173F1F0DA487D,
	U3CSetRateU3Ed__26_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mEA8A2AC6CD13B5A30D676B3A36F2669CD9049E95,
	U3CSetRateU3Ed__26_System_Collections_IEnumerator_Reset_mC7EA2588C39A3A5DF74047F6295DFEA96E636F43,
	U3CSetRateU3Ed__26_System_Collections_IEnumerator_get_Current_m485B95A8B747FDF00C4F4C377B4238929F64A129,
	Example03ProxyManagement_GetInstance_mA83F0825EF8D2EB90587B79D32949A3C1EAFD17C,
	Example03ProxyManagement_Awake_mE02347603F2C32C44184FA6C3731FCEB265EF637,
	Example03ProxyManagement_Start_mA4DFBA24A44B2606CADEBFC763757B9E1CC3C727,
	Example03ProxyManagement__ctor_m8E9ED007A8C8399BD55B275A5BFEB01FB56CA25F,
	Example03ProxyManagement__cctor_mE6EF9C0C6550956291F358232A1510381C796970,
	Example03ProxyManagement_U3CStartU3Eb__10_0_mFE057FFD6EE14734A9598B1978E79B8F43AFD718,
	Example03ProxyManagement_U3CStartU3Eb__10_1_m86FA78AD1A72F257BF6002F3BCABC772CB4C848B,
	Example03ProxyManagement_U3CStartU3Eb__10_2_m6F92CB585A1F54D584FBBEE187E07D7E55C1DC4D,
	Example03ProxyManagement_U3CStartU3Eb__10_3_mB46D74519F6952A179CD242E86D73F9D1AE74DE0,
	Example03ProxyManagement_U3CStartU3Eb__10_4_m4D57CB44112E5A6ABA7D1836F603D5A410BF819B,
	U3CStartU3Ed__10__ctor_mD2EBEF01559763AEB41147B69F400D9C37BA7249,
	U3CStartU3Ed__10_System_IDisposable_Dispose_m5DAC313C765AD7414291F37C5AF18854D673A3A0,
	U3CStartU3Ed__10_MoveNext_m4FB6245C073BBFFF290C93449D6F76E0E11429C4,
	U3CStartU3Ed__10_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m7E1CA9BC1A4949854DB2FC73BA73A82370745DC7,
	U3CStartU3Ed__10_System_Collections_IEnumerator_Reset_m6854DE8042609BFACA888413C64DAA593D89FAF4,
	U3CStartU3Ed__10_System_Collections_IEnumerator_get_Current_mA22569F7BDAE354C9B873EFC702C0ED39C4827BF,
	Example04SbaitsoClone_CreateText_mA138C7E85EEBDA9344B7570E3803DE82A32DBAEF,
	Example04SbaitsoClone_CreateNameInputField_m7251F4E29A15E8ADA11E793B867807C1BDF53E93,
	Example04SbaitsoClone_CreateTalkInputField_m31AE5BB29E181784A714AFDCDA0EDAEE14859DA3,
	Example04SbaitsoClone_CreateTextAndSpeak_m2EB3BCE8CD75ECA7D403B20E8E6A68D69F2A22D6,
	Example04SbaitsoClone_GetInstance_m1070D238AD5E91A1701EE0F6D3FD5B0EF107428F,
	Example04SbaitsoClone_Awake_m7E9ED3948685ABBF3A065CB26FD516E390B5AB49,
	Example04SbaitsoClone_Start_m188E9E8B00F0A4C342849FEAE3D6EA1121799EC9,
	Example04SbaitsoClone_HandleSynthesisOnEnd_m3BC32436F9B441EF014E374B5989090ABF71102B,
	Example04SbaitsoClone_GetVoices_m3536E808EF94AD1532169BF1E74B3E2FC6946077,
	Example04SbaitsoClone_FixedUpdate_mE82ACF725D6F5FF21E162E5342BB32435D97D4F2,
	Example04SbaitsoClone_Speak_mDBEFCE1D0191F85968B3D961838AABC637E288A2,
	Example04SbaitsoClone__ctor_m85792C9C617E44DFAD75E94B0388AA7848B402E4,
	Example04SbaitsoClone__cctor_mE6480B645577DA9C3D29FE6667F235035F2661FF,
	Example04SbaitsoClone_U3CStartU3Eb__19_0_m168596718F058D2D41F4C9484A840635C1BD625A,
	Example04SbaitsoClone_U3CGetVoicesU3Eb__21_0_m6C4B0FBBB6A3F4EB878F0C7D48EF5FD29CB034C3,
	U3CCreateNameInputFieldU3Ed__13__ctor_mDBA1413DC9CE6AB47ED198621A1984CF6F696778,
	U3CCreateNameInputFieldU3Ed__13_System_IDisposable_Dispose_m76CEC2693009D059E51D719735A8C29C78F2D55F,
	U3CCreateNameInputFieldU3Ed__13_MoveNext_m68879B0FE314F281992056AC4B48C9117FFCCC6D,
	U3CCreateNameInputFieldU3Ed__13_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m8AD8BAE973E0D51E605F03CB874C65EDAE7B6303,
	U3CCreateNameInputFieldU3Ed__13_System_Collections_IEnumerator_Reset_m29FA913BFECC9BC77B3D8C64D0B5F95E98B3B03B,
	U3CCreateNameInputFieldU3Ed__13_System_Collections_IEnumerator_get_Current_m5F9AD182E107360333F5777C391A7412A5DF12F4,
	U3CCreateTalkInputFieldU3Ed__14__ctor_m280826AEA884301256581138A0DC339D8C684623,
	U3CCreateTalkInputFieldU3Ed__14_System_IDisposable_Dispose_mB8862146AB8A709DAAB288A0D45BE59D49D9C437,
	U3CCreateTalkInputFieldU3Ed__14_MoveNext_m38D954D24C4532401AA7B2598B7BEDDE07A1405A,
	U3CCreateTalkInputFieldU3Ed__14_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mC2563FF15E08EA33D9870DD936C3363F560EF051,
	U3CCreateTalkInputFieldU3Ed__14_System_Collections_IEnumerator_Reset_mBFFDBE5B05883FD5E838BB70BAD45122DD3EB2D4,
	U3CCreateTalkInputFieldU3Ed__14_System_Collections_IEnumerator_get_Current_m0535514B983DD7861146E646862AB810E742736D,
	U3CStartU3Ed__19__ctor_m9279FC198E1CF92431BA6E3D7AAC85511DD0FFB7,
	U3CStartU3Ed__19_System_IDisposable_Dispose_m96082E25192AA2F4B3C5FD7E21EFF89E5823D3EF,
	U3CStartU3Ed__19_MoveNext_m4FCAD168BFCD1C9DA52122C1EE1F032AA6BF2D6F,
	U3CStartU3Ed__19_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mC09FF475BB8AB91B54583BB90F1C22F2B7F86D97,
	U3CStartU3Ed__19_System_Collections_IEnumerator_Reset_m8FF2E99C3BD4919C45F728E2ADF3FC20FD82D798,
	U3CStartU3Ed__19_System_Collections_IEnumerator_get_Current_m73BAD620D7E8A75EAC27FA0E5459D5E320A65689,
	U3CGetVoicesU3Ed__21__ctor_m13BCD013A34C0770207E488A37BEA58FE7CEADF9,
	U3CGetVoicesU3Ed__21_System_IDisposable_Dispose_m918A7E0FA7B6541257B774BD803300F9CF988D32,
	U3CGetVoicesU3Ed__21_MoveNext_mCD8CEBF773342D6585528FD6C51832ECED82BBE3,
	U3CGetVoicesU3Ed__21_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m7655E6EBDBCC12EB165CBA18546B85DDDA414F99,
	U3CGetVoicesU3Ed__21_System_Collections_IEnumerator_Reset_m465989673030024C527CDB60A4963F20BB87CA4C,
	U3CGetVoicesU3Ed__21_System_Collections_IEnumerator_get_Current_mE6C19EFC27F7715437D206D11126D3F89EF09D1D,
	Example06NoGUI_GetInstance_mFC2491C2C24D8DD6725D4FCAD6DFD4029B91438E,
	Example06NoGUI_Awake_mB67D05A40002752FE7088877CF03FC0B342C6F7D,
	Example06NoGUI_Start_mD573B09D7C17C0E1C75D8C9E5C7851E79FFB5671,
	Example06NoGUI_GetVoices_m701DB6D05DCBA2C4E0C01EAB95DCF0DE17700F0C,
	Example06NoGUI_Speak_m7B9F138752054134C7D3FFA2FFAD7B48F2543E96,
	Example06NoGUI_FixedUpdate_m785051DCFAEF079DB606DE10F5A2E4C90A8481E3,
	Example06NoGUI_OnSpeechAPILoaded_m7782991EC7A23FAD066CD0CA14DD50A7AADFEA6C,
	Example06NoGUI_HandleSynthesisOnEnd_mB9A55F9B24A3EBB4F07C91BE5E778432913E49A2,
	Example06NoGUI__ctor_m21E4BBE8422A0EFDF3C34F6EDAE3E088A71C09BD,
	Example06NoGUI__cctor_mF4EC294B98C5DEA7B7A41B2100E7037295DF260B,
	Example06NoGUI_U3CStartU3Eb__10_0_m72EF17BC2CD555921CE715E7C3F3F3219EF145EC,
	Example06NoGUI_U3CGetVoicesU3Eb__11_0_m94B8870A2FEE4697A251BA871BC88897E0E6DD35,
	U3CStartU3Ed__10__ctor_m8ED59AE01DDBDFB931EE86943CE3BC3848C6ADDD,
	U3CStartU3Ed__10_System_IDisposable_Dispose_m42B1BC2D8C580E8807BD4FCD43E3929D47E29AEF,
	U3CStartU3Ed__10_MoveNext_m2707806E878501ECC3E5703613221DD6A325E8E4,
	U3CStartU3Ed__10_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mAEC49344EDB434739EC4154F2D0B6284D5F79863,
	U3CStartU3Ed__10_System_Collections_IEnumerator_Reset_m54625ACABF650958F029E6CAA8623766896CE327,
	U3CStartU3Ed__10_System_Collections_IEnumerator_get_Current_mCF54097F282F03AAEDDE41D140D95B5DC33E51A6,
	U3CGetVoicesU3Ed__11__ctor_m2F1AF8243FA5D18261369A410EA1F6F68002DE42,
	U3CGetVoicesU3Ed__11_System_IDisposable_Dispose_m97EE3273B4579EB45B880904B822AD7D8D2A91D1,
	U3CGetVoicesU3Ed__11_MoveNext_mFB1FE6DC652FE43800AEA3A463ACAA777B7E47F7,
	U3CGetVoicesU3Ed__11_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m8CD07E3E6785C4A14570F67E8B754BAD6A9C5C7C,
	U3CGetVoicesU3Ed__11_System_Collections_IEnumerator_Reset_m648E41DC38C5F7CEAEFEA8DD33C737DF7E7DE35E,
	U3CGetVoicesU3Ed__11_System_Collections_IEnumerator_get_Current_m9209DA2EE51310298CE318289E76D2ED29966EB5,
	Example07Buttons_OnGUI_m428630080659246823F50B71F5619842852DDDCA,
	Example07Buttons__ctor_mF146DDCE9898DD4B09B551E5900D043301A6E4BF,
	Example07Buttons__cctor_m02647D43A86237F185AD96C9949A92B79BBE8C6B,
	Example07Simple_GetInstance_mBD4FC3372615B9F34C0233C5E3F4E91B65A6FBE6,
	Example07Simple_Awake_mC53FCE95F1A2C21D8957533CB1BDA431989C83D3,
	Example07Simple_Start_m5BEC553E42EFDA38D04730050A02F0468FEB6B85,
	Example07Simple_GetVoices_m0C572C4EAEBD69CDD3A6CB7E53A6BD7874413D05,
	Example07Simple_Speak_m6F9FE36067901C88FA02F332A8512A0233256B55,
	Example07Simple_FixedUpdate_m3615F1BE37961A3E2FFADA4F8471DEC199B614C6,
	Example07Simple__ctor_m0942AC3C570C63C4E5FECAE7A9EE9DE7916E5880,
	Example07Simple__cctor_m017E3E4D6626B7B1AF4BAEECE33F35F4A1AE06CD,
	Example07Simple_U3CStartU3Eb__9_0_m0E131621CB02A42CCFAD90DE45FDD289329E3035,
	Example07Simple_U3CGetVoicesU3Eb__10_0_m0F7FC45ABC08DCB270C219EC8BD94A12EDDCC5E3,
	U3CStartU3Ed__9__ctor_mF5D98CC9248DFAA670B1E1E8DC105AA80B6A3F7A,
	U3CStartU3Ed__9_System_IDisposable_Dispose_m0A53CD40C8A18435CED5140A56604E7716F04E54,
	U3CStartU3Ed__9_MoveNext_m32152B56ECBF2EA2BD1AD1E9A1B12FD560913D80,
	U3CStartU3Ed__9_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m1A2A789E1298A2DF842422EC709EF1D338316C5B,
	U3CStartU3Ed__9_System_Collections_IEnumerator_Reset_m5A0F287025DEEE61494DC0B746B49472E5107D88,
	U3CStartU3Ed__9_System_Collections_IEnumerator_get_Current_mA91D397DE5EF1EC8FF80A4687A787A8BB5B0C9AB,
	U3CGetVoicesU3Ed__10__ctor_m745BB18F289E59888EBC4DA97E51151670AB0B10,
	U3CGetVoicesU3Ed__10_System_IDisposable_Dispose_m8EEED7ADFA85960ED3B9A9465D4E64C1C6338589,
	U3CGetVoicesU3Ed__10_MoveNext_m9A7B8E4DE9BFE8FCADB6E5C7647E35A1C7F287D5,
	U3CGetVoicesU3Ed__10_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mE112EB43A922DCD1F08557C0B2EB65ED0BF27869,
	U3CGetVoicesU3Ed__10_System_Collections_IEnumerator_Reset_mE597695CBDFD7A5FF951AFF58AE3451AF0BECA1D,
	U3CGetVoicesU3Ed__10_System_Collections_IEnumerator_get_Current_m642317F9B85A2F6F491C70917B7FC44742E9DBBF,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	ProxySpeechSynthesisPlugin_GetInstance_mA29C631545B88F6A4706F99C06ABE5B5F43C6F81,
	ProxySpeechSynthesisPlugin_SafeStartCoroutine_mEDF885D8A43EA20C87E80CDE07BEBD7A3D2800E3,
	ProxySpeechSynthesisPlugin_Awake_mE1E2B83E58B334F10543CA56B06B393F1ABE7C24,
	ProxySpeechSynthesisPlugin_Start_mFC70CB6F8F4186F71EFB5323D599A10B462905C6,
	ProxySpeechSynthesisPlugin_Init_m2D4B203DDA9D67986B9456F93CBE0194D2FAE051,
	ProxySpeechSynthesisPlugin_RunPendingCommands_m61C1E0632B9CE5E8FA6DF8112E2BD8C0A65CE50D,
	ProxySpeechSynthesisPlugin_ProxyUtterance_m00F041E6FB4EE1F8D0485464E200BBEF1AF98D49,
	ProxySpeechSynthesisPlugin_ProxyVoices_m0318C1715E4EDDCCE6B378C7DA66F82348EA530A,
	ProxySpeechSynthesisPlugin_ProxyOnEnd_mE91F7F13A45699A8A258E6BD90A4DDD310F9AE5E,
	ProxySpeechSynthesisPlugin_IsAvailable_m2339C084BC48372BABD47C1E5D13F1F2C04A1E20,
	ProxySpeechSynthesisPlugin_AddCommand_m630DF09D9ABEC3A3E745FC2A0BDB144FDC4845E6,
	ProxySpeechSynthesisPlugin_CreateSpeechSynthesisUtterance_mB3C4330DD0C925654FFA6597A60234BE11643F5A,
	ProxySpeechSynthesisPlugin_GetVoices_m536DF84B9C887A5CC8C016E210DC510CBD4085EB,
	ProxySpeechSynthesisPlugin_SetPitch_m3FEBAE6D195D93E92F4DDB3A30A61CFA532FF09D,
	ProxySpeechSynthesisPlugin_SetRate_mB3B8BC0B1095261298AEC1BFA84FAB3F1B2B7EB2,
	ProxySpeechSynthesisPlugin_SetText_m375FE5DE718612EA5796C224E3375802D1E6B630,
	ProxySpeechSynthesisPlugin_SetVolume_m563C7A0E1C9EB3B5C0A05FF58122FEC7BCE2E877,
	ProxySpeechSynthesisPlugin_Speak_mE0B9C1B2C5916FD3FD96766C053E64CC24B0DD77,
	ProxySpeechSynthesisPlugin_Cancel_m3C2F6018BDDE58013B78783F722277BE7A6B3172,
	ProxySpeechSynthesisPlugin_SetVoice_mDB2790E1D1CFDE447D35A4895ABF81F0F9140F2E,
	ProxySpeechSynthesisPlugin_ManagementCloseBrowserTab_m33EDDEBBAFDD91595ED3276A16D9C50E7AE8F354,
	ProxySpeechSynthesisPlugin_ManagementCloseProxy_m956A8494881AD80E2D4B22C4249F52175EF2A053,
	ProxySpeechSynthesisPlugin_ManagementLaunchProxy_mD5F417468293698CCEDEA69B3D09D439B11D54C2,
	ProxySpeechSynthesisPlugin_ManagementOpenBrowserTab_mDC40A4645A84070F50BAC8AB989F5D13BE480999,
	ProxySpeechSynthesisPlugin_ManagementSetProxyPort_m903352FB0F065AD0C41E0016086C64B54E0D9F8B,
	ProxySpeechSynthesisPlugin_CreateWWW_m97ED0681D0D5AECAACAD32B244272CB266587A79,
	ProxySpeechSynthesisPlugin__ctor_m0FDDA29B2A2A424D8E1B00E1CB8EB77B3F57F004,
	ProxySpeechSynthesisPlugin__cctor_mE25689F12571B40E0EF7F6253F70B8928E637DB9,
	U3CInitU3Ed__12__ctor_m76C78B57D3E077558A1B64BD3C60394348A55EB7,
	U3CInitU3Ed__12_System_IDisposable_Dispose_mCF76C3D929D98063E6D635DBE241A28010B2419C,
	U3CInitU3Ed__12_MoveNext_mD0624FC908B6C59554A8BA1B80609E72C429A88F,
	U3CInitU3Ed__12_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mA1875409A854BC8BCBD1706ADC7DE32EC5FC79BF,
	U3CInitU3Ed__12_System_Collections_IEnumerator_Reset_m172521A167F15E22BE4AB993B191F3CB20ABE238,
	U3CInitU3Ed__12_System_Collections_IEnumerator_get_Current_mC582877BCF04920CDC9C5837E6B7AC27286781DF,
	U3CRunPendingCommandsU3Ed__13__ctor_mF755B6F406B21FCDBA541DD1F627B61DBA9D7B38,
	U3CRunPendingCommandsU3Ed__13_System_IDisposable_Dispose_m5553B88D973F8D223B7C2E1988E707DA5903BFEE,
	U3CRunPendingCommandsU3Ed__13_MoveNext_m939262D23606AE7B45930DC58411C81AA4962941,
	U3CRunPendingCommandsU3Ed__13_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mE6332DB63594041697D2F788EB9A569EFA45F9E6,
	U3CRunPendingCommandsU3Ed__13_System_Collections_IEnumerator_Reset_mDA11BAC1A537E93CA1D2FB62F55A1BF67AE7C422,
	U3CRunPendingCommandsU3Ed__13_System_Collections_IEnumerator_get_Current_mA480802E726F9F7FE60F4D244CDC4C7E947A661B,
	U3CProxyUtteranceU3Ed__14__ctor_m6B50B1BC665C70213F42952F27D32620696E2215,
	U3CProxyUtteranceU3Ed__14_System_IDisposable_Dispose_mBD5CFAAAECAC03CE4AEBE083261B789E45C93183,
	U3CProxyUtteranceU3Ed__14_MoveNext_mF7DDA12DE889747A313305E01612059A4B7F8FBA,
	U3CProxyUtteranceU3Ed__14_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m0A1B3F6C03C24C597E853792C14E8CBC3EC4E787,
	U3CProxyUtteranceU3Ed__14_System_Collections_IEnumerator_Reset_m33B544F80C9BD6E23E7EC8CAF68D3BDDE5434177,
	U3CProxyUtteranceU3Ed__14_System_Collections_IEnumerator_get_Current_mFDE586AB0058DAE09E26C1649BF6CE1446B4B4C0,
	U3CProxyVoicesU3Ed__15__ctor_m9D51FA49B63EA5B2D6D4741F9AD4638E9F3768D1,
	U3CProxyVoicesU3Ed__15_System_IDisposable_Dispose_m125C573F8DCD66E614ED372FE4BEECB946D19048,
	U3CProxyVoicesU3Ed__15_MoveNext_m4424B1E154EA2BF3165FC891664A2B12562ADCA5,
	U3CProxyVoicesU3Ed__15_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m5AD44B4EDBDC4428C3026163414865806493DE1A,
	U3CProxyVoicesU3Ed__15_System_Collections_IEnumerator_Reset_mB95FFE93FD0F160DDC6170F5515C3F7122F676FD,
	U3CProxyVoicesU3Ed__15_System_Collections_IEnumerator_get_Current_mB48913C5ECA3605995DCCF3E055E8332181C0745,
	U3CProxyOnEndU3Ed__16__ctor_m6D7E54EB078F354B70FD6C4FFDD910AE9485008A,
	U3CProxyOnEndU3Ed__16_System_IDisposable_Dispose_m6A373202B6709BFEAF7D17EBD44817066FEAF057,
	U3CProxyOnEndU3Ed__16_MoveNext_mB8464C1990059F3AE5DDDF72B1D0A27678B5C902,
	U3CProxyOnEndU3Ed__16_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m4335A867E6CDA8B392ECCC15F6E8C1ACFDE5BDFB,
	U3CProxyOnEndU3Ed__16_System_Collections_IEnumerator_Reset_mF623461CC34757BD5120223664D337EF6492EAC4,
	U3CProxyOnEndU3Ed__16_System_Collections_IEnumerator_get_Current_m7C1A32E5A25138F22368DD2B3C9E66C2BABFA8F9,
	SpeechProxyConfig__ctor_m83F5FBA39D5E6C885C924916F18814B8E58905CE,
	SpeechSynthesisEvent__ctor_m6DB0A931E0C44C922FE95BDA0891D0E75BF00325,
	SpeechSynthesisUtils_GetInstance_mF0760DAD3871D091AD9DC7DB95B14A40E8F81ADB,
	SpeechSynthesisUtils_PausePlayerPrefs_mA60F8D30DA7BDA4844A267FCF67415611064F19D,
	SpeechSynthesisUtils_SetActive_m1B7D834516CFBA8840500E95C0C190B85626AB8E,
	SpeechSynthesisUtils_SetInteractable_m0D993F7363AB78CFBA8052B611937D418C0ED8B4,
	SpeechSynthesisUtils_PopulateDropdown_m405E52E5E43F58408CF50515D96F83540D4B5DC0,
	SpeechSynthesisUtils_PopulateVoicesDropdown_mDD787015DE9EF937F6A6BC310BC9A73819351B4E,
	SpeechSynthesisUtils_PopulateVoices_m314B354E515DB2FD31547CE35FD1DCA465E1D7C4,
	SpeechSynthesisUtils_SelectIndex_mC8BC962701E4A7B7D714A8F51BA8A496E4CD66F7,
	SpeechSynthesisUtils_GetVoice_mF9645724EEFEE83D45CD15B056749851D6412900,
	SpeechSynthesisUtils_HandleVoiceChangedDropdown_m23C69E841750D18AC8EBFADE7B67DA902B0995DA,
	SpeechSynthesisUtils_HandleVoiceChanged_mC439AE0C169DC3EE107D8DD8FD9360B352C9D95C,
	SpeechSynthesisUtils_GetDefaultVoice_mCDDDE3DFAD4F6AC07D5764BF128EF4D5826E53C4,
	SpeechSynthesisUtils_SetDefaultVoice_m4D1FB4EACDDED194FA334052CEAC0EDF3AE212E2,
	SpeechSynthesisUtils_RestoreVoice_m297E80F5714FF9CA35D0396D4F6B01BDC7C92FBB,
	SpeechSynthesisUtils_RestoreVoice_m3AFD16FBCC3EECEED184A1272A2EFF879BF88FA8,
	SpeechSynthesisUtils__ctor_mCB48E8486A0C4DE8239B4DE311D202D5E4B6D4E0,
	SpeechSynthesisUtils__cctor_mDE271808ACCE3AC77794BA0960094ECB6E0BAC28,
	SpeechSynthesisUtterance__ctor_mFDC477D9C9F4621C62052D5378CEF9AF9ADE1CB0,
	Voice__ctor_mA25ACC72E5B712E6A3D51C764A65B500BC44D7D1,
	VoiceResult__ctor_m7928C0A714CA6C607887D95771D8863B8C519953,
	WWWEditMode__ctor_m7C7C2909B8F91A833D966AA588238A33182354D3,
	WWWEditMode_HandleBeginGetResponse_mF7F03D1407EFB3D4EFDA93F1465C3EBC61094D42,
	WWWEditMode_IsDone_mA190AA90959B4F9BCBD22CC6131BD5155327C8F4,
	WWWEditMode_GetError_m917FDD40D73F38B0FF28E4753DD26B727DF10079,
	WWWEditMode_GetText_m27F240974F97E82D92FFA2D0DE0304F02591CF25,
	WWWEditMode_Dispose_m3C11A0715D7E48C9938E17DF04B307CC1BD3A53B,
	WWWPlayMode__ctor_m28FAC7243624E2410B378B46B29BC601ACC55AA8,
	WWWPlayMode_IsDone_m43652044816DB354C86CA847F8CA224B7EED99FA,
	WWWPlayMode_GetError_m1B57A2BDE11EE5422FC393DCB8C7026281333FF1,
	WWWPlayMode_GetText_m620FB72732ADFFF4466FCF0D1C0B02FB48252141,
	WWWPlayMode_Dispose_mA5F46E794C93945FEC64055EBFBF08946B41C006,
	WebGLSpeechSynthesisPlugin_GetInstance_mB76D5CD707E31CFAEDE26265FDDABE60E346721E,
	WebGLSpeechSynthesisPlugin_Awake_mC658261283083CDDDB089B72F5FB75888D87129F,
	WebGLSpeechSynthesisPlugin_ProxyOnEnd_m4C05BABE8475953BE0C7F45E9458ACAFC99B0144,
	WebGLSpeechSynthesisPlugin_IsAvailable_m40B918FD79FD8052856994E785700DEE42850D1D,
	WebGLSpeechSynthesisPlugin_CreateSpeechSynthesisUtterance_m0A2EA33249ED681A7F6387AC24A92B31D0246920,
	WebGLSpeechSynthesisPlugin_Speak_mBD7D119BB6372FD256FC6F0CA967EDDCAD7B7AE9,
	WebGLSpeechSynthesisPlugin_Cancel_mAF54BB415AADC04955A91912D11E53D4CE410E70,
	WebGLSpeechSynthesisPlugin_GetVoices_m9921EC51AB98DA88673ECA146CC34C6EE449558D,
	WebGLSpeechSynthesisPlugin_SetPitch_m66B3D107675AE33D0C6E8D2951B4445EA745B8EB,
	WebGLSpeechSynthesisPlugin_SetRate_mD4C3D4C637ED5DE6E17335665DA9D32E6459E105,
	WebGLSpeechSynthesisPlugin_SetText_mAE428BC902365D0D7DD8BAD344466EB3A12C644C,
	WebGLSpeechSynthesisPlugin_SetVoice_mB581C4C0EE0B28772D53020584360FC25BEE3EA4,
	WebGLSpeechSynthesisPlugin_SetVolume_mBA7FA80B657D6175CC602FB3D9DEFDA9C135197D,
	WebGLSpeechSynthesisPlugin_ManagementCloseBrowserTab_m4DC92F5F6B2584095047B97DCF212FE0AFFB625E,
	WebGLSpeechSynthesisPlugin_ManagementCloseProxy_m8E211226659F25595EF8E03E64E7C13B3E1B4478,
	WebGLSpeechSynthesisPlugin_ManagementLaunchProxy_mFFC7670DF7658F39526F7AC5EFDF3390AFDC4F8F,
	WebGLSpeechSynthesisPlugin_ManagementOpenBrowserTab_mF05FC671A42E0C0A711BB3392B1FB4DD494811C1,
	WebGLSpeechSynthesisPlugin_ManagementSetProxyPort_m6597EEB35D4C6E2C79C095782DAB3BE83A1B54A7,
	WebGLSpeechSynthesisPlugin__ctor_m3CD65A2EE5289FAA6BD067D9ED181420F68268B9,
	WebGLSpeechSynthesisPlugin__cctor_m2989B07DD89844AF59E1CB0E209CE32C24FAA0C4,
	U3CProxyOnEndU3Ed__3__ctor_mA20A9FD5B9CA14D3E246E77F2E753B84137DBA2C,
	U3CProxyOnEndU3Ed__3_System_IDisposable_Dispose_mB6A59AC6CCFBEACE049F002037CA85CDD8C6C1A1,
	U3CProxyOnEndU3Ed__3_MoveNext_mC75D2C44B36B522D9AED0BBA156E820D518D08BC,
	U3CProxyOnEndU3Ed__3_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m23180814A7DB3E1CE1C1068FF09F1BB43810BC9C,
	U3CProxyOnEndU3Ed__3_System_Collections_IEnumerator_Reset_m5214C4EBB3412E7401F9943FD182087458B09AED,
	U3CProxyOnEndU3Ed__3_System_Collections_IEnumerator_get_Current_m1A3AC4C48B1827D4A820083059E5C907FE3395AB,
	BaseSpeechDetectionPlugin_AddListenerOnDetectionResult_m42E61B495DFEABD3AD6F1D70AAACB3AC5330F864,
	BaseSpeechDetectionPlugin_RemoveListenerOnDetectionResult_m2B1D66D423ABBC6BE2FA854955DEF563EC63C988,
	BaseSpeechDetectionPlugin_AddListenerOnLanguageChanged_mC14E94C923CA77139B35DCC39C68412C19549C98,
	BaseSpeechDetectionPlugin_RemoveListenerOnLanguageChanged_mA705570825BE11E33CA90A1A23FC50FEBC6E3268,
	BaseSpeechDetectionPlugin_Invoke_m9C67F6EB6DA8D87804D0715827F5F2EBB9DC70F2,
	BaseSpeechDetectionPlugin_Invoke_mD6A22D8913372F39C9E8E19718E8FBF734C630E4,
	BaseSpeechDetectionPlugin__ctor_mFFEE525772A6B2C26BD34768B4D50E068F844749,
	BaseSpeechDetectionPlugin__cctor_m913107AE7AEE3347521A46412B972BC2383744BD,
	DelegateHandleDetectionResult__ctor_m4C86F1A7834CED4272DE63D92B66BBA0EB0488A9,
	DelegateHandleDetectionResult_Invoke_mAA07164CA25FFE631CCB089EEC6E374E2660913C,
	DelegateHandleDetectionResult_BeginInvoke_m2C9218C85D1A917D5861402FE8B3CF0B5015E8F9,
	DelegateHandleDetectionResult_EndInvoke_m25EB0514D452F3F9A8A5970124C8FCFA1E5D555A,
	DelegateHandleLanguageChanged__ctor_m6499688350D63108B4AF54E31E7A6803A35EAAA5,
	DelegateHandleLanguageChanged_Invoke_mDF6A5A08DBC352DA6566D19570BC90F63BA15595,
	DelegateHandleLanguageChanged_BeginInvoke_m8D74C95B84655435CB02DF3C3C322D408E00C039,
	DelegateHandleLanguageChanged_EndInvoke_m099E443D595FF4788A4E5EA516C4D9974C91257D,
	DetectionResult__ctor_m5DC7429E1E9CDE34C352247FEEED76744948EB45,
	Dialect__ctor_m3FD26A2B2644B4C3CBA0B4C41E0374F9AF72DE9B,
	EditorProxySpeechDetectionPlugin_SafeStartCoroutine_mFE221995227718C4AEC36CB28C6EFD550214A7C1,
	EditorProxySpeechDetectionPlugin_CreateWWW_m30A47FA73FF2066B97CDE646495D1A2582F9775D,
	EditorProxySpeechDetectionPlugin_IsEnabled_m1B8DFCD1D13F3ADD6C6555E16D78C7440E456E9D,
	EditorProxySpeechDetectionPlugin_CleanUp_mBDF56D27EAF3CB6CB1783C96F43F7F03FE73AE67,
	EditorProxySpeechDetectionPlugin_SetEnabled_mBEF5480A07F2F1FDAC4CF658C2D88EC80B723442,
	EditorProxySpeechDetectionPlugin_GetInstance_m0DCB70A51416E7D49917770D68BED1E47936501E,
	EditorProxySpeechDetectionPlugin_StartEditorUpdates_mBCDE2EB79BA19FD752C899FAD38D1150F5CE9B26,
	EditorProxySpeechDetectionPlugin_StopEditorUpdates_m81A1494C1C8348D3EB8C490A86BCA51B2732B04F,
	EditorProxySpeechDetectionPlugin__ctor_m970AAF0F5792051F1A777BEB6EA912171A900759,
	Example01Dictation_Awake_mD2BEC27FA16FBABEAACD6422C948E0292117C423,
	Example01Dictation_Start_m2DD9CDBF6C2F5FDB3F69125D1188C11D1CF28ABE,
	Example01Dictation_HandleDetectionResult_mF6F15C2BDC86BE68AA06CCC7F9DC68E16A94165A,
	Example01Dictation__ctor_m9D3675019889A753C9238CA009EC9A9319F8A2D3,
	Example01Dictation_U3CStartU3Eb__10_0_m324C5EB1646129DCE15C60593D843CF0CEEFBA00,
	Example01Dictation_U3CStartU3Eb__10_1_m4D6C29A76FA1387309A88B31FEB428A82B256190,
	Example01Dictation_U3CStartU3Eb__10_2_mA8B03413D355942B8391D9237DCDE718BEB8DCD6,
	U3CStartU3Ed__10__ctor_m9A5ADA41AD9640CCCF47CF94DF79A682A6F3B310,
	U3CStartU3Ed__10_System_IDisposable_Dispose_m23F5925258986EB4AFC343608A32AF127CD7640B,
	U3CStartU3Ed__10_MoveNext_m4A59099BF6E78F4666138CB26E5183C39430CC71,
	U3CStartU3Ed__10_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m20845EAD2D482F70B9F51BCF13959349EFCE3AF2,
	U3CStartU3Ed__10_System_Collections_IEnumerator_Reset_mCAC9839D3729DBAFC1C7A33DEFCED8AE1E914CF8,
	U3CStartU3Ed__10_System_Collections_IEnumerator_get_Current_m961FFD432D91B04C6F056699AE4A60F11B7CDBBA,
	Example02SpeechCommands_Awake_mB03CB2D341E0EC4C7B81020C205299F9BC53FA38,
	Example02SpeechCommands_Start_m3B392D38DE9534CB1DFDDB6ED8A309B09C10CAD1,
	Example02SpeechCommands_HandleDetectionResult_mC0AE593BCF82C93EA94B84D80EFF3D5F403222BD,
	Example02SpeechCommands__ctor_mC85A7B2ED8EACA682A8D5160F84AAB8A8805C612,
	Example02SpeechCommands_U3CStartU3Eb__9_0_mAF3718081A86C5DEE426CE29056C45F5D372FDA7,
	Example02SpeechCommands_U3CStartU3Eb__9_1_mCDAAE9C0F8DE0861443E9766CB560B80D6080D9E,
	Example02SpeechCommands_U3CStartU3Eb__9_2_m723537E14FC6C80D2737CC31D7FE370B62BBA08B,
	U3CStartU3Ed__9__ctor_m1DCC4242CF1D0D731B6126E7F5B708942A1072DB,
	U3CStartU3Ed__9_System_IDisposable_Dispose_m5EB8C4ADEBBD2ED5CCBE2F328282892CB6884DF4,
	U3CStartU3Ed__9_MoveNext_mC44C0A80540663842F9445FE32F1F3A4969080F8,
	U3CStartU3Ed__9_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m43876813CB26CDF2721AF751DB3E93BB453730EC,
	U3CStartU3Ed__9_System_Collections_IEnumerator_Reset_m7617645E4D536DB41476515F5F849965F8335D85,
	U3CStartU3Ed__9_System_Collections_IEnumerator_get_Current_m6B1E12CCB938CD7A198A113D6EE2CC277AA32A67,
	Example02Word_DoHighlight_m670F10C8CF6785E291095095AEEDF3C8A30E285C,
	Example02Word_Highlight_mBEB6545949E897507A55751CF96B839A437BBB0C,
	Example02Word__ctor_mA989753AC23D5F19AA4D4B5C4E19DD78EF33B10F,
	Example02Word__cctor_m8A3770B8AF8717525F4CDD18992690C15C2F00CF,
	U3CDoHighlightU3Ed__5__ctor_mD5AE312E764E3292D8DD5D158217B95A9CF70565,
	U3CDoHighlightU3Ed__5_System_IDisposable_Dispose_m2F8B3A79BFBE378256C8DB81C7E72C2793E1E7B6,
	U3CDoHighlightU3Ed__5_MoveNext_m5F0734B423DD8323E878A55B7A2BE89B7C4983CE,
	U3CDoHighlightU3Ed__5_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m3E50ECEB225715936D40ED6CFAA54EF88A73A6E5,
	U3CDoHighlightU3Ed__5_System_Collections_IEnumerator_Reset_m58923D5EDD1686E38EACEBE6919E2C3818A069DD,
	U3CDoHighlightU3Ed__5_System_Collections_IEnumerator_get_Current_m37093F8D22C85DD3FC980F42F64A16F25EB20E61,
	Example03ProxyCommands_Awake_m8AA631439B136C53DDD8C700D3AE31B434A84266,
	Example03ProxyCommands_Start_m75C3CACF576092A6AEDAB46949555EBF3BC5CE3A,
	Example03ProxyCommands_HandleDetectionResult_mD0FBD9A210EE8B7C2A10E183F6CAD7FD3600398C,
	Example03ProxyCommands__ctor_mE8C706E926EFB5979D9C5BA341E26953D415D867,
	Example03ProxyCommands_U3CStartU3Eb__9_0_m1A9B4FAC6449AC5A6B6F60CBA33E6A008B14B5D0,
	Example03ProxyCommands_U3CStartU3Eb__9_1_m59BF70008830BB6F5952C527F5E824A6609DE99A,
	Example03ProxyCommands_U3CStartU3Eb__9_2_m80788C4BD22A86C43951D086E1CA8E2D6B19C09B,
	U3CStartU3Ed__9__ctor_m3F78AF5ED20089723D623C905AB064288A96CD0E,
	U3CStartU3Ed__9_System_IDisposable_Dispose_m1C60169E1C0BB3885A9C16EF36572BA72860FA87,
	U3CStartU3Ed__9_MoveNext_mE7C788145A84FA6363B0186199294C8FA34497AC,
	U3CStartU3Ed__9_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m9D4EE49E43C98748228A6554736CAA494FFAF419,
	U3CStartU3Ed__9_System_Collections_IEnumerator_Reset_mA96F9B0685F82F2F9B24923E4B4C58B943E84D55,
	U3CStartU3Ed__9_System_Collections_IEnumerator_get_Current_m760831DCCDB96CF1185E0FBE602E4286E80C456F,
	Example04ProxyDictation_Awake_m57676EA57D43985745FACE8378B5BEF448EE1274,
	Example04ProxyDictation_Start_mA1D2D0C1D6CDD82A052581989B6078A6E282C465,
	Example04ProxyDictation_HandleDetectionResult_m37F71371773FA8A892DC54B7EB333C92624D4E5C,
	Example04ProxyDictation__ctor_m142B76CFC9D5E2C47E2BC23EE9F1224129C6A2E0,
	Example04ProxyDictation_U3CStartU3Eb__10_0_m135A7C20424A1634400FCF9A0B2F98FA8457F588,
	Example04ProxyDictation_U3CStartU3Eb__10_1_mCA1C4A134D714558B80BC0D4AA7CB81B1C901EE1,
	Example04ProxyDictation_U3CStartU3Eb__10_2_m7405692423C6979639B7A85D1643E52FC884B703,
	U3CStartU3Ed__10__ctor_mD08E65BE8117B1AC5F05D4FE68ED1115D16B41AD,
	U3CStartU3Ed__10_System_IDisposable_Dispose_m2E8EDC3CFF0CBE8EC3721E3E2C73DA2B36DB2A81,
	U3CStartU3Ed__10_MoveNext_mD989784207C7831F28429F79AD1BD3DFC3971588,
	U3CStartU3Ed__10_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mC8E450D1D2D986E4B2BB9CC67E3EB6C6AA0D2C2E,
	U3CStartU3Ed__10_System_Collections_IEnumerator_Reset_m2E08DC86E082086DAEC6DFDFF85580F53EDCE9A6,
	U3CStartU3Ed__10_System_Collections_IEnumerator_get_Current_mA280C39A17548B1911ACD6D3A44D3053E613B0FC,
	Example05ProxyManagement_Start_m918205768DD763A8F73CB618156AA6DDC9BAEC6A,
	Example05ProxyManagement__ctor_mDA0D54E30190108ACEEADE32483593F10DE79A84,
	Example05ProxyManagement_U3CStartU3Eb__7_0_m4E1A6040A0C7A2098FAB777BAC252F695DFECAAA,
	Example05ProxyManagement_U3CStartU3Eb__7_1_m0B63C58301AF6B06F1C7C41B45D1F69D58A79644,
	Example05ProxyManagement_U3CStartU3Eb__7_2_m969A65E0BCFC131DDD819D80D2B9868F4AF2FE8A,
	Example05ProxyManagement_U3CStartU3Eb__7_3_m0679A42A30B14C905B811C8AC0290ED3FA95AE3C,
	Example05ProxyManagement_U3CStartU3Eb__7_4_m623F6738E08C72416FF70B5E85F218BE31768A3A,
	U3CStartU3Ed__7__ctor_mFFB60DBC0F017DF05F9FA1003C2B2738FBCFA8FC,
	U3CStartU3Ed__7_System_IDisposable_Dispose_m05D77DC058E9785F795D27EB7E08BABCB8EEAB0F,
	U3CStartU3Ed__7_MoveNext_m36294F98198D42D4DEA6FEA798CED9DC2BCBD4A0,
	U3CStartU3Ed__7_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m3A38D5FDBB5F5E32BD913E93B26A37B8B50D7313,
	U3CStartU3Ed__7_System_Collections_IEnumerator_Reset_mB7C38F73E9B1A4D4DD842876612C0EC3A5793742,
	U3CStartU3Ed__7_System_Collections_IEnumerator_get_Current_m701771E3243E1E913ACEBC2E8795C89ACC649668,
	Example08NoGUIDictation_Start_m78D8A7861FB8F439E8E757AFB43C9CE12A8DE22D,
	Example08NoGUIDictation_HandleDetectionResult_mD482872E11AFF33EBE2190E66C8B7D76DAC16718,
	Example08NoGUIDictation__ctor_m710A8510CB65A42B23BDE09A3A7C1D4E1A4BE21F,
	U3CStartU3Ed__1__ctor_mE7E901F45124A48C59536990AA73C30B675E639B,
	U3CStartU3Ed__1_System_IDisposable_Dispose_mD105462F2F676E0DBAA2F81DBE280BE7AA7FE238,
	U3CStartU3Ed__1_MoveNext_m6C25221981C8D4FB4E5A9B4A43495303B32817EC,
	U3CStartU3Ed__1_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mFC700BEEB25737D16E6524C6E55100390A7C94C6,
	U3CStartU3Ed__1_System_Collections_IEnumerator_Reset_mA53FBDA94F0F3114DCEE4EFE51DD76112B0F2D4C,
	U3CStartU3Ed__1_System_Collections_IEnumerator_get_Current_mCE1ADD01EFF09234ED24546244C7B07E4D5B7206,
	Example09NoGUISpeechCommands_Start_m67230CF71BF7306EE666D93170BD0854B411E556,
	Example09NoGUISpeechCommands_HandleDetectionResult_m0A8DE1839CAE706CB9980688050AADCDDB24D70C,
	Example09NoGUISpeechCommands__ctor_mD5278A63D83791067114DE72431B4D98BC76CDA5,
	U3CStartU3Ed__2__ctor_m960857B297E5A747E66E659907688D23C5E3A9A0,
	U3CStartU3Ed__2_System_IDisposable_Dispose_m205623C92E170CE080D885A81AE3575797ED9EB8,
	U3CStartU3Ed__2_MoveNext_mE6C92440345B2AA33E6D1534087E514AE4D71A6C,
	U3CStartU3Ed__2_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mEDD0D9D21F65BD4743C534205936CB8CB2CBCD41,
	U3CStartU3Ed__2_System_Collections_IEnumerator_Reset_m2488C31C7C875031BCCBC0ACACEA4BCE6430FE2A,
	U3CStartU3Ed__2_System_Collections_IEnumerator_get_Current_m46FBAA334FC377A24C2D933F71512447FB11FD5B,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	Language__ctor_mE3BE69C41FC505B06DF40959E533EAD7AA9A56EB,
	LanguageChangedResult__ctor_m38B3305857327A461D132803B89D09828F55A8E0,
	LanguageResult__ctor_m5036D73693F9648D2AD597CF336FA4ED0ED9D358,
	ProxySpeechDetectionPlugin_GetInstance_m081B9659988F4795E1C7B209347B4C83F3031800,
	ProxySpeechDetectionPlugin_SafeStartCoroutine_mFAA3F5F699EA8DF7FBCEC638192630B698233AB0,
	ProxySpeechDetectionPlugin_Awake_m83D10DB4ADBC6FFFD9058490FAD0EF58DEDB3D7A,
	ProxySpeechDetectionPlugin_Start_m62213B390AA9020A40EE280629E9C6DDACD99593,
	ProxySpeechDetectionPlugin_Init_m84CA595544407D67541C32D252E8A9542EDEE55F,
	ProxySpeechDetectionPlugin_CreateWWW_m5F98DCEE52E200F1E455A8878F34D795465C0FA7,
	ProxySpeechDetectionPlugin_RunPendingCommands_m66D935B982A0D79D6D7533FC6219B71B6759C03D,
	ProxySpeechDetectionPlugin_GetResult_mC030AA32DAA8592997FA7A5F671B5B11B948F41A,
	ProxySpeechDetectionPlugin_Abort_m89CBB2706E455720F5DCF1DBF8E24CCB1D388624,
	ProxySpeechDetectionPlugin_IsAvailable_m19247712B89D89A67887AAB42BD42377A0402744,
	ProxySpeechDetectionPlugin_DebugCommands_mFEA75E22D980034AE0E08A7A9FFBEE4087F0028F,
	ProxySpeechDetectionPlugin_AddCommand_mE47386ADE04DB85272125597F38AE07BE19D0345,
	ProxySpeechDetectionPlugin_SetLanguage_mBC1E9E299141F10ED238BEBC87B00AE4ECA6D3D3,
	ProxySpeechDetectionPlugin_ProxyLanguages_m8E82A021B061AAB677F5036EF34520588DDD41E0,
	ProxySpeechDetectionPlugin_GetLanguages_mEAEAE16D347DE3D1F011DBE277A155964DBF7B71,
	ProxySpeechDetectionPlugin_ManagementCloseBrowserTab_mA746439C5B793536BD09912762EF694A67489980,
	ProxySpeechDetectionPlugin_ManagementCloseProxy_m4894209F12C85636554CB2575C26C71428BB65A9,
	ProxySpeechDetectionPlugin_ManagementLaunchProxy_m5D784B1D33662F25981D170330D5589EAA513A41,
	ProxySpeechDetectionPlugin_ManagementOpenBrowserTab_mC5698D7B5112170D37FFF0661C7642C54F958196,
	ProxySpeechDetectionPlugin_ManagementSetProxyPort_m93083D52E953E53726F6750284352114BC1ED3C0,
	ProxySpeechDetectionPlugin__ctor_m2F96A86390CF297D99077D6631C55A00418086B7,
	ProxySpeechDetectionPlugin__cctor_m49D485EB0189933383D40A6B633BECB327259F64,
	U3CInitU3Ed__10__ctor_m012BA9734608E5F6552327811617DA42F34B766A,
	U3CInitU3Ed__10_System_IDisposable_Dispose_mBBB8890BC2D1590E2C1DDE80878680C2212EF200,
	U3CInitU3Ed__10_MoveNext_m364B843FE6861871C5513DE512E58E52C160D904,
	U3CInitU3Ed__10_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m54ADF1B4ABC962485DB751DCE71C27B0C17558DD,
	U3CInitU3Ed__10_System_Collections_IEnumerator_Reset_mE083368AC1F41DD0E8F9B2988AB38626CDDB50B4,
	U3CInitU3Ed__10_System_Collections_IEnumerator_get_Current_mE9D4A920E754B9F844722ABD42F7911A32B253AC,
	U3CRunPendingCommandsU3Ed__12__ctor_mC11C018DC0A91D90202312BE9D6E7031F8B7A966,
	U3CRunPendingCommandsU3Ed__12_System_IDisposable_Dispose_m273B75505906AC0B3E987E74414503C5EAFD3029,
	U3CRunPendingCommandsU3Ed__12_MoveNext_m8DFEFB3CDDC7F6B53ACE468F716BFFFCFEF4461A,
	U3CRunPendingCommandsU3Ed__12_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mF49722C3374CA4DCD5DEDA7A42E74B2E85449D0C,
	U3CRunPendingCommandsU3Ed__12_System_Collections_IEnumerator_Reset_mCE9FD8ED116F2DE4D31163FF91ED0F0BCC872409,
	U3CRunPendingCommandsU3Ed__12_System_Collections_IEnumerator_get_Current_m0214811E4DC232B9A5AD3775866A44B909549478,
	U3CGetResultU3Ed__13__ctor_m0CDDF83A5EBDA6AC4DA49D5DF38A02D721246C3F,
	U3CGetResultU3Ed__13_System_IDisposable_Dispose_mD2C540480CC4924CBDA0D6CC8090D5E69A09E52A,
	U3CGetResultU3Ed__13_MoveNext_m937D2D23BECF682693D1E40DA6D4F3BD47BE8EDC,
	U3CGetResultU3Ed__13_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m7EBC61FA9172162F7E2D417E3E9646E091814409,
	U3CGetResultU3Ed__13_System_Collections_IEnumerator_Reset_mBCB807344745DBFF79C784AA606095E3ACECD75C,
	U3CGetResultU3Ed__13_System_Collections_IEnumerator_get_Current_m6EB722F8467C0E7094AFC385192102522361A12E,
	U3CProxyLanguagesU3Ed__19__ctor_m73D98C4B262426CBA8A82CFCB41B8CC9E0083405,
	U3CProxyLanguagesU3Ed__19_System_IDisposable_Dispose_m7ABACFD849FD40300BF13D89CC177D0836FB0B4D,
	U3CProxyLanguagesU3Ed__19_MoveNext_m39D3FC07827123BE2576D7949189E142764DC34B,
	U3CProxyLanguagesU3Ed__19_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m51D3D2D32D00F389B14479614A9A4F24C90D57D0,
	U3CProxyLanguagesU3Ed__19_System_Collections_IEnumerator_Reset_m368C82C50D2515A4715969FA6E605EF35B095C30,
	U3CProxyLanguagesU3Ed__19_System_Collections_IEnumerator_get_Current_m443AEE64E35421F4376968F384CE357F48E10A9E,
	SpeechDetectionUtils_GetInstance_m86F66DD4AA98B058DE486420FB3974233E91044E,
	SpeechDetectionUtils_PausePlayerPrefs_m1760596FCAE1D6EE09CECAC0D544865F4763EA35,
	SpeechDetectionUtils_SetActive_mC1B4C840410E33AB9E47BD8C4FF38580CA0197CA,
	SpeechDetectionUtils_SetInteractable_m4A6F5FDC6777BFDD436E8EA4EBD628D0FB5A921E,
	SpeechDetectionUtils_PopulateDropdown_mE0525A4C8C593BCE3597FFBB614FF49A94E74148,
	SpeechDetectionUtils_SelectIndex_m142FB8FD3DA1A0DF553E6C32BF9260CC61700CBF,
	SpeechDetectionUtils_GetDefaultLanguageOptions_m10274403341FEED5147A41DE3DE0E3D8398C7070,
	SpeechDetectionUtils_GetDefaultDialectOptions_m138A28CC22E888FFA8411401A7E0AA3E0E42784E,
	SpeechDetectionUtils_GetLanguage_m23ACFC8523F94B1A5D89BFC931322281AFE5776B,
	SpeechDetectionUtils_GetLanguageDisplay_m9EA8BDF6F74E12BF46866E51C4F5002BD370E3D2,
	SpeechDetectionUtils_GetDialect_mC7428159E85CAEEB0F14E43AE9408C95BE81E4E9,
	SpeechDetectionUtils_GetDialectDisplay_mF79C18E3ED00088D0F62979D24AB7D4F603FE7F7,
	SpeechDetectionUtils_DisableDialects_m4ABC9BBEF61C26B33F163D28AB77F70B824FEBF8,
	SpeechDetectionUtils_PopulateLanguages_m5A188128F823952281473E44A9ABC6CFEA29C0BB,
	SpeechDetectionUtils_PopulateLanguagesDropdown_m0C8E1154FAFC57D2544F465CBEB237729F0EEFA5,
	SpeechDetectionUtils_HandleLanguageChanged_m3A5E598DE0C3EBDD1EC6987151998A2C6B6DDA3B,
	SpeechDetectionUtils_HandleLanguageChanged_m10A85CE235F3A2F4EB58958E1C77ACE49CEA7DA9,
	SpeechDetectionUtils_HandleDialectChanged_m1921EA2983012936EE64203BF759548CE4CACCB2,
	SpeechDetectionUtils_HandleDialectChanged_mE0C3D3B782A7579D0C32FDF40CC758FB9B3ECFA0,
	SpeechDetectionUtils_GetDefaultLanguage_mCA50ADFE7F67D52BA64FDFDB4C59AE02EEE85722,
	SpeechDetectionUtils_SetDefaultLanguage_m455E05A5BF4FAA712DE6FD79949B9D331C42D1BB,
	SpeechDetectionUtils_GetDefaultDialect_mD8DFBFAAD50CF7C4BBE2341D6AB9424C55D33E60,
	SpeechDetectionUtils_SetDefaultDialect_m39CDC254772357DEF97F4D334E314FDCB75BBCD0,
	SpeechDetectionUtils_RestoreLanguage_mBFD43321F4E9F81DBF97C1939B99C75A31BEB1BD,
	SpeechDetectionUtils_RestoreLanguage_m041CCF1EBBB8DA11BEA0C15D5AD95085CBA3C386,
	SpeechDetectionUtils_RestoreDialect_m1EFFADD244486F80BE3E73A7736B2537F17D45F1,
	SpeechDetectionUtils_RestoreDialect_m8BA5D41E16E154E23F7A8BFFB5E3998362120804,
	SpeechDetectionUtils_HandleOnLanguageChanged_mA89667F3EBA71A953D7C6D22F6F95CE04002BA04,
	SpeechDetectionUtils__ctor_m455C69836CC1AC6178EB05355A8C35D4CFB9929F,
	SpeechDetectionUtils__cctor_mA312DFBC237322C8E94F40DE745B7F9826A9F07C,
	SpeechProxyConfig__ctor_m631792703A397FD98A8A38FCD6B538DA6A595A72,
	SpeechRecognitionAlternative__ctor_mAC45640199215EFC36CF8E744CB6567B8DC51F80,
	SpeechRecognitionResult__ctor_m40C2AB5648C0B02112823BF098E922073D2DC761,
	WWWEditMode__ctor_mAE738C3EEF803D1701AE157067F9C988A25171BE,
	WWWEditMode_HandleBeginGetResponse_m3A6B35B3ADF86D626F091A820E7BEFEC1EB32F8E,
	WWWEditMode_IsDone_mCE0A3604D99AABC806AE3162311DCD14E49E66CD,
	WWWEditMode_GetError_mDE1F06A24A7548CDC9A9C690F8C30A743173CB8F,
	WWWEditMode_GetText_mE7B6EB2AD493E2DC6C7019450C4B610E74664703,
	WWWEditMode_Dispose_m050C725951671D100E1C322EAF19A5A90636E80C,
	WWWPlayMode__ctor_mA09C34F908890A9E0AB4B41E58DF937F25A3BFFF,
	WWWPlayMode_IsDone_mF03CD99A8A00C2C85D8A8E1AA88426C2E09BD829,
	WWWPlayMode_GetError_mAB650946E549F10E7E72EBC7DF138B2D72E48F86,
	WWWPlayMode_GetText_mC12A436A7034055056F62CD644E8D4202A1EBBAF,
	WWWPlayMode_Dispose_m180D815AD96FF80FAE853C4DCA681B8D18A1D3A1,
	WebGLSpeechDetectionPlugin_GetInstance_mAF00BB3954646ECC8A6250920FC045F260C405FF,
	WebGLSpeechDetectionPlugin_IsAvailable_m0053943FA3FFC06714678552A4416A42664887A2,
	WebGLSpeechDetectionPlugin_Abort_m2FE8783886E36E1D46A50DE727D69FCC0B16B009,
	WebGLSpeechDetectionPlugin_StartRecognition_m32772E7334B6368FC84969615B4273B7D3C07B64,
	WebGLSpeechDetectionPlugin_StopRecognition_m9CF0D3BAB199469B26F30F9C26ADC10A287D347D,
	WebGLSpeechDetectionPlugin_GetLanguages_mB33FCEA6A5BF93848B6D5D5AB94DEB7B05101985,
	WebGLSpeechDetectionPlugin_SetLanguage_m92CFEEC61B7D7BB0F6713F583B754A71AE05C5B1,
	WebGLSpeechDetectionPlugin_Awake_m98B553DD80AEB0BC67D1DCDB6BFE867BB25B4366,
	WebGLSpeechDetectionPlugin_ManagementCloseBrowserTab_m10B278A9EE93E9541E631732968314D6CB40D085,
	WebGLSpeechDetectionPlugin_ManagementCloseProxy_m1A696915B0BBC37247593BB37F74CF0955CD8B2B,
	WebGLSpeechDetectionPlugin_ManagementLaunchProxy_m96A960A3B42682AFA9B187BEF68F4AC4EB484F8A,
	WebGLSpeechDetectionPlugin_ManagementOpenBrowserTab_m7DBBCB6A0793A5E42E50FA8E97DA483665C2162D,
	WebGLSpeechDetectionPlugin_ManagementSetProxyPort_mABE7801B5CD17EC3ABCBFC8ADF08DE89F14559C6,
	WebGLSpeechDetectionPlugin__ctor_m4AE99E3CC7C462ACC856F1395C43E374D5D590DE,
	WebGLSpeechDetectionPlugin__cctor_m3D79521306D985DCD9BB4DDA7DFAFED707D81D6E,
	Example01DictationSynthesis_Start_mB4882D5513B9316D6BAB403228D4AED406D59C1C,
	Example01DictationSynthesis_HandleSynthesisOnEnd_mECB16A9F1A518F016716DE5C109C7C3ECAC8DD26,
	Example01DictationSynthesis_Update_mB8BA0E710FDCE505D22F7623EC8E196395E09CF0,
	Example01DictationSynthesis_HandleDetectionResult_mA565FA06C67D65DC1615F17CB88267A19E6249F3,
	Example01DictationSynthesis_GetVoices_m7A46A9BDFEAC0BC2BE84DA4D4E435C4709F1940B,
	Example01DictationSynthesis_SetIfReadyForDefaultVoice_mBA8D700EACA086D8A59308FE40832788991C7C85,
	Example01DictationSynthesis_Speak_m7572ACE032FFAE632F8B4A4CCC156F31C533F988,
	Example01DictationSynthesis_SetPitch_m387E57A36A344607A5BF04F1BF4DE73E58F1C212,
	Example01DictationSynthesis_SetRate_m98FDF4E83F129860CF981AC241540A865844849C,
	Example01DictationSynthesis_FixedUpdate_m0C2DDEBFF345B0D7D9C1419FB03FC67EDCB58CEE,
	Example01DictationSynthesis__ctor_m6BAFC9D572002D94CB59C37FAA0BF8E833920234,
	Example01DictationSynthesis_U3CStartU3Eb__28_0_m2BB9F8966F92CEEE45201B8B5EBC468EF95CC0A8,
	Example01DictationSynthesis_U3CStartU3Eb__28_1_m40F45F64E6037C82A4D94DCB628D9B9D20F789B2,
	Example01DictationSynthesis_U3CStartU3Eb__28_2_mF4484E4A96810FC30E1902285A2952CB7EFEF50C,
	Example01DictationSynthesis_U3CStartU3Eb__28_3_m7D58A195ACB6F257BBB7646F8E3C6ADCC4E506D4,
	Example01DictationSynthesis_U3CStartU3Eb__28_8_mB7702341DF0663D771005EC27CC628EA3825A6C1,
	Example01DictationSynthesis_U3CStartU3Eb__28_9_m4109CE818814E955E1C68269A520B2AB34F0CBC1,
	Example01DictationSynthesis_U3CStartU3Eb__28_4_mDF45F0C4ED5AB0BAAF9C49DC31880C30A2ABC532,
	Example01DictationSynthesis_U3CStartU3Eb__28_5_mF1465E0ECA48721C2D79C05FC8673499A676639F,
	Example01DictationSynthesis_U3CStartU3Eb__28_6_mF0B4E30B599D0E84B30C93594F4D8BB01070A61B,
	Example01DictationSynthesis_U3CStartU3Eb__28_7_mB2DEFDEF9A51C0B5267A44A8DB5224DC798BAD6F,
	Example01DictationSynthesis_U3CGetVoicesU3Eb__32_0_m7647381772F8947F21087CA9489F9A76C9AEEC41,
	Example01DictationSynthesis_U3CSetIfReadyForDefaultVoiceU3Eb__33_0_mE07F5718D62BA9FC6AB3DC33A6BF537C091A9523,
	U3CStartU3Ed__28__ctor_m6251A007ED7831DB20DE6DD12568AFC488C1B1A0,
	U3CStartU3Ed__28_System_IDisposable_Dispose_mE1A0CB500E998B63CCC43B876F7F55F86C1B921B,
	U3CStartU3Ed__28_MoveNext_mF1650806095FF54E87D3A23311017158651C457D,
	U3CStartU3Ed__28_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mB67313988CF183C834E335EE4EE373EE42FAAE53,
	U3CStartU3Ed__28_System_Collections_IEnumerator_Reset_mE54EED999CFD46CE5C5FF21F95AF6153F165E5CE,
	U3CStartU3Ed__28_System_Collections_IEnumerator_get_Current_m8A5EF102397A7D73091A8A9EECEB30078BF7B8CC,
	U3CGetVoicesU3Ed__32__ctor_mF7508112903C4F9ED3CAF31218951EAE34D1DBEE,
	U3CGetVoicesU3Ed__32_System_IDisposable_Dispose_mEC4E1291389C3EA9CFA71F80146E8F16FA321323,
	U3CGetVoicesU3Ed__32_MoveNext_m9579F8C70BFC4FEFDAC453E735492782C7E4EFE9,
	U3CGetVoicesU3Ed__32_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m4EAEC51E6B7FBCD8B9A1CBCD16DA2966223124C8,
	U3CGetVoicesU3Ed__32_System_Collections_IEnumerator_Reset_m094A2A097F4044D384F0C23A2BD9629CB03E9B90,
	U3CGetVoicesU3Ed__32_System_Collections_IEnumerator_get_Current_mBBB5185B3F8DB86560A9DE3F3BBB2E7531E2C484,
	U3CSetPitchU3Ed__35__ctor_m9D8D07FCA90D3E73D299B2D80FE6E7C891D77952,
	U3CSetPitchU3Ed__35_System_IDisposable_Dispose_mB8668091EEFA11E8010416E9001FCF9EC09ECE72,
	U3CSetPitchU3Ed__35_MoveNext_m088FB80C4C2F972C3E83DB3805C494BD1E576C94,
	U3CSetPitchU3Ed__35_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m78E469CF14747B40EB8F0759E0639B39FB089C41,
	U3CSetPitchU3Ed__35_System_Collections_IEnumerator_Reset_m66DB440A475F280C6F51FE8B7F3016EB7FE9151F,
	U3CSetPitchU3Ed__35_System_Collections_IEnumerator_get_Current_m83AEA3365906FC77D9D1DE7859301D5621AB2E4B,
	U3CSetRateU3Ed__36__ctor_m850DD6B944C189AFC8DD0EC75E985E279C02848D,
	U3CSetRateU3Ed__36_System_IDisposable_Dispose_mFE9B1BC95F527E94DB2F805C2E2DE218C08C36A0,
	U3CSetRateU3Ed__36_MoveNext_mB1DF57598D5B98C623A9EACCE88D79D33947D4AE,
	U3CSetRateU3Ed__36_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m8ACBE875E3E52945765CB65BB421C1DB1758D2D3,
	U3CSetRateU3Ed__36_System_Collections_IEnumerator_Reset_mD3357A03BEE1F8589E4CC34678D40D0137D0FC3D,
	U3CSetRateU3Ed__36_System_Collections_IEnumerator_get_Current_m7934C30490D8913CC34E6F24824C5CFD13ED62C7,
	Example02DictationSbaitso_CreateText_m5DF09DB8E8A11F824B00E0760C6DCE127C52E2ED,
	Example02DictationSbaitso_CreateNameInputField_m669A31FCB67151C44FA729295641A592875E8ECD,
	Example02DictationSbaitso_CreateTalkInputField_m93C3B2C862D8C5D6A359775752B6DAE8065D0021,
	Example02DictationSbaitso_CreateTextAndSpeak_m66A5C20C2CBF012197901BCB2CDA03437907EFA0,
	Example02DictationSbaitso_Start_m7444A4D38B7A9C05C2917DAAC2EF163D756D7E74,
	Example02DictationSbaitso_HandleDetectionResult_m87B55B402CCCC347A334ABD1DF94D27E2E03BAB7,
	Example02DictationSbaitso_HandleSynthesisOnEnd_m2E8D363B2844290C42841A463C80946C3C253459,
	Example02DictationSbaitso_GetVoices_m089DFE1583E1DD7E4BCCE8AD263845C0EB0AEE83,
	Example02DictationSbaitso_FixedUpdate_m00184E30E60EB7B7B8325928E22560B4B4A06846,
	Example02DictationSbaitso_Speak_mAC90064AAA111649D1AB5A8E5502C6C1BDCA27B2,
	Example02DictationSbaitso__ctor_m8421E653FEC99F7F41AF7062B15FE458A4858F70,
	Example02DictationSbaitso_U3CStartU3Eb__21_0_mCCD542190710E61A9EDE15A3E559DC311ADD9BEB,
	Example02DictationSbaitso_U3CStartU3Eb__21_1_mF537CA2C485462C331E4C7316CCFEB0A6DC1F203,
	Example02DictationSbaitso_U3CStartU3Eb__21_2_mC0F51C19C5FF069B8BDCCAE56A1507FDE85B8772,
	Example02DictationSbaitso_U3CGetVoicesU3Eb__24_0_mB6088C47629D61E8ABE71DF63887EEBECC040C67,
	U3CCreateNameInputFieldU3Ed__18__ctor_m4B5B254484C31B4D071462387837964CB5FB5FE0,
	U3CCreateNameInputFieldU3Ed__18_System_IDisposable_Dispose_mF82190DDE2697199F48366F177466E899B0022F2,
	U3CCreateNameInputFieldU3Ed__18_MoveNext_m9478A7053CE77BB6ED7D92AE6DB36FE9A0FF14C6,
	U3CCreateNameInputFieldU3Ed__18_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mA7C8F9A3E9BCA451E48A0B59AF88D0D30FA8052D,
	U3CCreateNameInputFieldU3Ed__18_System_Collections_IEnumerator_Reset_m1EA0F77850A1CE0F7FA9D16059514D7039A976C6,
	U3CCreateNameInputFieldU3Ed__18_System_Collections_IEnumerator_get_Current_m6EB5B4889BD9E1D8F6101FFF052897D2508A69F0,
	U3CCreateTalkInputFieldU3Ed__19__ctor_mCE780DD23754578C72F63AA3BE000D2EF21C457E,
	U3CCreateTalkInputFieldU3Ed__19_System_IDisposable_Dispose_mF2ACFC307D0C8C5D10CAC168C84B87629E31C5ED,
	U3CCreateTalkInputFieldU3Ed__19_MoveNext_mEFD711E0D39AA10DAEA0F2AF34384047AC746534,
	U3CCreateTalkInputFieldU3Ed__19_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mE52A015117FA49929B6655BA390A090BC6BC3934,
	U3CCreateTalkInputFieldU3Ed__19_System_Collections_IEnumerator_Reset_m8F584B55249C63900EE385230905F4819BCB245C,
	U3CCreateTalkInputFieldU3Ed__19_System_Collections_IEnumerator_get_Current_mDBCA9A1BCCADCA02EC5ABFFD1073E495B6F7EDE6,
	U3CStartU3Ed__21__ctor_mC72EA5BAA492982E48576F5F60FD8483732353B6,
	U3CStartU3Ed__21_System_IDisposable_Dispose_m12232760A14BC34C5C5EC1EACC05E1852D6A89A9,
	U3CStartU3Ed__21_MoveNext_m7B440ADBFAA34F1850E917C578D4542AD458AC5A,
	U3CStartU3Ed__21_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m535DFD5C67CB16D689578F83B00CEC23E4484FEA,
	U3CStartU3Ed__21_System_Collections_IEnumerator_Reset_m67EA047E1E606C626E1B1D7A7E58018554CC12A8,
	U3CStartU3Ed__21_System_Collections_IEnumerator_get_Current_mA401C2A6D290024BD88FC27BE57E64DDE97B5433,
	U3CGetVoicesU3Ed__24__ctor_m833C2611DC287B9E47094CADC771D692419E63E6,
	U3CGetVoicesU3Ed__24_System_IDisposable_Dispose_m71DBB98BC3D4A498D09E055587E51F12F98331CC,
	U3CGetVoicesU3Ed__24_MoveNext_mAAE3F7A02BB4CC58CCCC989DB6BD987BB63CCCD8,
	U3CGetVoicesU3Ed__24_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m52B65A6687210AA4FB8BB4C690ADA13DF97D1061,
	U3CGetVoicesU3Ed__24_System_Collections_IEnumerator_Reset_m9F98C408BFCDFD6C23ED7A460E33534854BBC079,
	U3CGetVoicesU3Ed__24_System_Collections_IEnumerator_get_Current_mBF17624D10518A8301364E0EE6CED011DC123301,
	OneClickBase_NewConfiguration_m5B556E2708C7BAD895B40097F426ED8A17823DFE,
	OneClickBase_AddSmrSearch_mFDB3C8112B382431780D0C0F94F58832610F80B8,
	OneClickBase_NewExpression_mBF526AED5FC9AFF661FEF599E694215DADEB9BEF,
	OneClickBase_AddShapeComponent_mFADA98251C1C2DEF86CC46DAC24A792DB4832A29,
	OneClickBase_AddUepPoseComponent_m555480449B5D5AFFD1E4B8F860BF1828425EDE2D,
	OneClickBase_AddBoneComponent_m59C2366E42758F5BD3C4992FF09D35F49846F712,
	OneClickBase_AddEmoteFlags_m1FA7A7B679AFC6BB5EA92F35857F31ECFFFF54C0,
	OneClickBase_DoOneClickiness_mBF6AD23DEF0283BD29C1CC20DCC4B64387F3330C,
	OneClickBase_Init_m659D9827623C96804BF09FECA49DEA0888720CAF,
	OneClickBase_ResetForNextConfiguration_mD78BDCFC3261C91C4D8C90926D9EFB8A555B9711,
	OneClickBase_ConfigureModule_m9EC19B9A036BC3FC6C380A29A2055B0BED19702B,
	OneClickBase_RegexFindBlendshapeName_m553122048E9780E7E5FAB387DAA9444B4F51E49A,
	OneClickBase_GetBlendshapeNames_m9455F0238F22BC0EA65D6773BEEC70FF1D9B198E,
	OneClickBase_ConvertBoneToTform_mE70FEEA8BA9C1DF6CD49C7C45A4A8E22DC54E62C,
	OneClickBase_FindBone_m58D56D663C673F23BBC02B2712E67283B881EAE4,
	OneClickBase_ApplyCommonSettingsToComponent_m649B8669D74BB0B951B19E682C1C2B7D316FAE28,
	OneClickBase_CreateNewComponent_mA0D124D4C6FEFC0DF0AE825C276469B9A99AD5B2,
	OneClickBase__ctor_mE60AC0F3F342FE0B92127CBDD93772C1D77A4AAC,
	OneClickBase__cctor_m649E5B11980B53D5D631DC9E5346C00D95A4847B,
	OneClickConfiguration__ctor_m20499092FEBCC7E68BC05B656D4B23E41104D7E5,
	OneClickExpression__ctor_mD1FF5C71EEAA9ACD0660219B7CB9F73314B32B5B,
	OneClickExpression_SetEmoterBools_m35A4E005358F131A41C8CF2B29D118EE31036ECC,
	OneClickComponent__ctor_m14FCA04E623512CE159FADECD336C4EFC59B0243,
	OneClickComponent__ctor_m271849BBC4F322250343F86186EBE439A82DFE1B,
	OneClickComponent__ctor_m37AA14B0C3D69007E15280978B6D4D7611C9DB4C,
	OneClickBoxHead_Setup_mB27D13C280F04C364B694576DE8FB7BAE5602672,
	OneClickBoxHead__ctor_m58E8206392DF1268EA1DD36C07ADA02D4ED75FD4,
	OneClickBoxHeadEyes_Setup_mEE7FC2304E0EFC63A06726F747498051679C2CEA,
	OneClickBoxHeadEyes__ctor_mBAABE084D851340A63F3E77D08BE0CFAC7DC400D,
	U3CPrivateImplementationDetailsU3E_ComputeStringHash_m93FD10C4CC1978E570BB23B36EB64D5DC41D3E8B,
};
static const int32_t s_InvokerIndices[1919] = 
{
	3479,
	3479,
	3479,
	5066,
	5066,
	4110,
	4419,
	4419,
	4418,
	4418,
	-1,
	-1,
	5066,
	5066,
	5066,
	5066,
	-1,
	-1,
	-1,
	5066,
	5066,
	5066,
	5066,
	-1,
	-1,
	-1,
	4759,
	3479,
	2545,
	3479,
	2545,
	1745,
	3479,
	1315,
	2887,
	1745,
	2887,
	942,
	2887,
	1745,
	2887,
	942,
	2887,
	1745,
	1140,
	325,
	2887,
	1745,
	1747,
	599,
	2887,
	1745,
	1747,
	599,
	2887,
	1745,
	2887,
	942,
	2887,
	1745,
	2887,
	942,
	2887,
	1745,
	1747,
	599,
	2887,
	1745,
	2887,
	942,
	2887,
	1745,
	3479,
	1315,
	2887,
	1745,
	3479,
	1315,
	2887,
	1745,
	3479,
	1315,
	2887,
	1745,
	3479,
	1315,
	2887,
	1745,
	3479,
	1315,
	2887,
	1745,
	3479,
	1315,
	2887,
	2887,
	2887,
	2887,
	2887,
	2887,
	2887,
	3479,
	2887,
	2887,
	2887,
	3479,
	5226,
	5159,
	5159,
	5159,
	5159,
	5159,
	5159,
	5159,
	5159,
	5159,
	5159,
	5159,
	5159,
	5159,
	5159,
	5159,
	5159,
	5159,
	5159,
	5159,
	5159,
	5202,
	5156,
	5202,
	5156,
	5216,
	5166,
	5208,
	5159,
	5216,
	5166,
	5216,
	5166,
	5208,
	5159,
	5202,
	5156,
	5208,
	5159,
	5208,
	5159,
	5202,
	5156,
	5216,
	5166,
	5202,
	5156,
	5216,
	5166,
	5216,
	5166,
	5216,
	5166,
	5216,
	5166,
	5216,
	5216,
	5216,
	5216,
	5166,
	5208,
	5208,
	5208,
	5216,
	5216,
	5202,
	5216,
	5216,
	5216,
	5216,
	5216,
	5216,
	5216,
	5208,
	3479,
	3479,
	3479,
	3479,
	2920,
	5226,
	4187,
	4750,
	4349,
	3847,
	5111,
	4680,
	4100,
	4760,
	4680,
	3703,
	5159,
	5066,
	3565,
	5159,
	5066,
	5159,
	3658,
	5066,
	3630,
	5226,
	5159,
	5159,
	5159,
	5226,
	5226,
	5159,
	5226,
	5226,
	5226,
	5226,
	5226,
	5226,
	5226,
	5159,
	5159,
	4511,
	4862,
	4862,
	5159,
	5159,
	4862,
	3479,
	5226,
	3479,
	2545,
	5226,
	3479,
	2300,
	3479,
	3479,
	3414,
	3397,
	3414,
	3451,
	3451,
	3451,
	3397,
	3451,
	3451,
	3451,
	3451,
	3451,
	3451,
	3479,
	2300,
	2300,
	2300,
	3479,
	2870,
	3479,
	3451,
	3414,
	3479,
	3414,
	2870,
	3479,
	3451,
	3414,
	3479,
	3414,
	2870,
	3479,
	3451,
	3414,
	3479,
	3414,
	5208,
	5159,
	5208,
	5208,
	5226,
	5226,
	5226,
	5226,
	3479,
	5226,
	5226,
	5216,
	5202,
	4998,
	4998,
	4998,
	4121,
	3859,
	3479,
	4679,
	4366,
	5066,
	4089,
	4984,
	4365,
	4365,
	4365,
	4365,
	4599,
	3966,
	4281,
	5066,
	4282,
	4993,
	4993,
	5062,
	3479,
	2887,
	2887,
	2887,
	2887,
	3479,
	3479,
	3479,
	3479,
	3414,
	2887,
	2887,
	2655,
	1315,
	3479,
	2887,
	3479,
	3479,
	3479,
	5226,
	2870,
	3479,
	3451,
	3479,
	3414,
	3479,
	3414,
	3479,
	3479,
	2870,
	3479,
	3479,
	3451,
	2920,
	3451,
	2920,
	3479,
	3479,
	3479,
	3479,
	3479,
	3479,
	2887,
	2887,
	2887,
	2887,
	3414,
	2887,
	3453,
	2922,
	3453,
	2922,
	3453,
	2922,
	3479,
	3479,
	3479,
	3479,
	3479,
	3479,
	3414,
	3479,
	3479,
	2887,
	3479,
	3479,
	3479,
	5226,
	5226,
	3479,
	2545,
	2870,
	3479,
	3451,
	3479,
	3414,
	3479,
	3414,
	3414,
	3479,
	3479,
	3479,
	2870,
	3479,
	3479,
	3479,
	2887,
	3479,
	3479,
	2300,
	3479,
	2870,
	3479,
	3451,
	3414,
	3479,
	3414,
	2887,
	2887,
	2887,
	2887,
	3414,
	2887,
	3453,
	2922,
	3453,
	2922,
	3453,
	2922,
	3479,
	3479,
	3479,
	3479,
	3479,
	3479,
	3479,
	3479,
	3479,
	3479,
	3453,
	2922,
	3453,
	2922,
	3453,
	2922,
	3479,
	3479,
	3479,
	3479,
	3479,
	3479,
	3479,
	2920,
	3479,
	2920,
	3479,
	1305,
	3479,
	3479,
	3479,
	2300,
	3479,
	2887,
	3479,
	5226,
	3479,
	3479,
	3479,
	3414,
	3479,
	2887,
	3479,
	2870,
	3479,
	3451,
	3479,
	3414,
	3479,
	3414,
	2887,
	2887,
	2887,
	2887,
	2887,
	2887,
	2887,
	2887,
	2887,
	2887,
	2887,
	2887,
	2887,
	2887,
	2887,
	2887,
	2887,
	2887,
	3451,
	2920,
	3414,
	3397,
	3414,
	3414,
	3451,
	3451,
	3397,
	3451,
	3451,
	3451,
	3451,
	3451,
	3451,
	3451,
	3414,
	3479,
	2887,
	2300,
	2300,
	2300,
	3479,
	1316,
	111,
	786,
	786,
	2300,
	3479,
	2887,
	2887,
	1140,
	1747,
	1747,
	2887,
	2887,
	1747,
	3479,
	5226,
	5226,
	3479,
	2300,
	2300,
	2300,
	2870,
	3479,
	3451,
	3479,
	3414,
	3479,
	3414,
	2887,
	5159,
	5159,
	5159,
	5159,
	5159,
	5159,
	5159,
	5159,
	5159,
	5159,
	5159,
	5159,
	5159,
	5159,
	5159,
	5159,
	5159,
	5159,
	3414,
	3397,
	3414,
	3414,
	3451,
	3451,
	3397,
	3451,
	3451,
	3451,
	3451,
	3451,
	3451,
	3451,
	3414,
	3479,
	2887,
	2300,
	2300,
	2300,
	1316,
	111,
	786,
	786,
	2300,
	5226,
	5159,
	5159,
	4511,
	4862,
	4862,
	5159,
	5159,
	4862,
	5226,
	5226,
	3479,
	2300,
	2300,
	2300,
	2870,
	3479,
	3451,
	3479,
	3414,
	3479,
	3414,
	3414,
	3397,
	3414,
	3414,
	3397,
	3451,
	3451,
	3451,
	3451,
	3451,
	3451,
	3451,
	3451,
	3451,
	3414,
	3479,
	2887,
	2300,
	2300,
	2300,
	2887,
	5159,
	5159,
	5226,
	3414,
	3397,
	3414,
	3414,
	3451,
	3451,
	3397,
	3451,
	3451,
	3451,
	3451,
	3451,
	3451,
	3451,
	2300,
	2300,
	2300,
	3479,
	1316,
	5129,
	2300,
	5226,
	5226,
	3479,
	2300,
	2870,
	3479,
	3451,
	3414,
	3479,
	3414,
	2870,
	3479,
	3451,
	3414,
	3479,
	3414,
	2870,
	3479,
	3451,
	3414,
	3479,
	3414,
	2870,
	3479,
	3451,
	3414,
	3479,
	3414,
	5226,
	5226,
	3949,
	401,
	3414,
	3397,
	3414,
	3451,
	3451,
	3397,
	3451,
	3451,
	3451,
	3451,
	3451,
	3451,
	3451,
	2300,
	2300,
	2300,
	1316,
	3414,
	5066,
	2300,
	2870,
	3479,
	3451,
	3414,
	3479,
	3414,
	2870,
	3479,
	3451,
	3414,
	3479,
	3414,
	2870,
	3479,
	3451,
	3479,
	3414,
	3479,
	3414,
	2870,
	3479,
	3451,
	3414,
	3479,
	3414,
	5226,
	3479,
	2300,
	2870,
	3479,
	3451,
	3479,
	3414,
	3479,
	3414,
	3414,
	3479,
	3414,
	2887,
	85,
	3414,
	3414,
	3414,
	3414,
	3479,
	3414,
	2887,
	3453,
	2922,
	3453,
	2922,
	3453,
	2922,
	3368,
	3479,
	266,
	48,
	25,
	3414,
	3479,
	3479,
	3414,
	2887,
	2887,
	3479,
	2870,
	3479,
	3451,
	3414,
	3479,
	3414,
	3479,
	3479,
	2870,
	2920,
	2920,
	2920,
	2922,
	2920,
	2922,
	2920,
	2922,
	2922,
	2922,
	3479,
	3479,
	3479,
	3479,
	3479,
	2922,
	2922,
	2922,
	2922,
	2922,
	2922,
	2870,
	2870,
	3479,
	3479,
	3479,
	3479,
	3479,
	3479,
	3479,
	3479,
	3479,
	3479,
	1747,
	2887,
	3479,
	3479,
	3479,
	3479,
	2870,
	2920,
	2920,
	2920,
	2922,
	2920,
	2922,
	2920,
	2922,
	2922,
	2922,
	3479,
	3479,
	3479,
	3479,
	3479,
	3479,
	3479,
	3479,
	3479,
	3479,
	2922,
	2922,
	2922,
	2920,
	2920,
	2870,
	2887,
	3479,
	3479,
	3479,
	5226,
	3479,
	3479,
	3479,
	3479,
	3479,
	2887,
	2887,
	3479,
	3479,
	3479,
	3479,
	3479,
	3479,
	3479,
	3479,
	2887,
	3479,
	3479,
	3479,
	3479,
	3414,
	3479,
	3479,
	2870,
	3479,
	3451,
	3414,
	3479,
	3414,
	3479,
	3479,
	3479,
	3479,
	3479,
	3479,
	3479,
	3479,
	3479,
	3479,
	3479,
	2887,
	2887,
	2887,
	2887,
	1140,
	1747,
	1747,
	3479,
	3479,
	3479,
	3479,
	3479,
	3479,
	3479,
	3479,
	3479,
	2887,
	2887,
	1140,
	1747,
	1747,
	3479,
	3479,
	3479,
	3479,
	3479,
	3479,
	3479,
	3479,
	3479,
	3479,
	3479,
	3479,
	2887,
	3479,
	3479,
	3479,
	3479,
	3479,
	3479,
	3479,
	2887,
	3479,
	3479,
	3479,
	3479,
	3479,
	2887,
	3479,
	3479,
	3479,
	3479,
	3479,
	2887,
	3479,
	3479,
	3479,
	3479,
	3479,
	3479,
	3479,
	3479,
	3479,
	3479,
	3479,
	3479,
	3479,
	3479,
	3479,
	3479,
	3479,
	3479,
	3479,
	3479,
	3479,
	3479,
	3479,
	3479,
	340,
	340,
	3479,
	2870,
	3479,
	3451,
	3414,
	3479,
	3414,
	2870,
	3479,
	3451,
	3414,
	3479,
	3414,
	3479,
	2887,
	2887,
	3479,
	3479,
	2887,
	3479,
	3479,
	3479,
	3479,
	3479,
	3479,
	3479,
	3479,
	3479,
	3479,
	3479,
	2870,
	2920,
	2920,
	2920,
	2922,
	2920,
	2922,
	2920,
	2922,
	3479,
	3479,
	3479,
	3479,
	2920,
	2920,
	2922,
	2922,
	2922,
	3479,
	3479,
	3479,
	3479,
	3479,
	3479,
	3479,
	3479,
	3479,
	3479,
	3479,
	3479,
	3479,
	3479,
	3479,
	3479,
	5208,
	5208,
	3479,
	5226,
	5216,
	5216,
	5216,
	5216,
	5216,
	5216,
	5216,
	5216,
	5216,
	5216,
	5216,
	5216,
	5216,
	5216,
	5216,
	5216,
	5216,
	5216,
	5216,
	5216,
	5216,
	5216,
	5216,
	5202,
	4675,
	5111,
	4169,
	4680,
	5066,
	4374,
	4680,
	5066,
	4121,
	5066,
	5066,
	5066,
	4117,
	5064,
	5057,
	3959,
	5111,
	4514,
	3479,
	5226,
	5226,
	3479,
	2300,
	5226,
	5111,
	5226,
	5159,
	5226,
	5066,
	5126,
	4998,
	5111,
	4933,
	4862,
	4867,
	4857,
	4866,
	4853,
	2887,
	2887,
	2887,
	2887,
	2887,
	2887,
	3399,
	2872,
	3397,
	2870,
	3414,
	2887,
	3451,
	2920,
	3397,
	3368,
	2842,
	3368,
	2842,
	3414,
	2887,
	3414,
	2887,
	3451,
	2920,
	3479,
	2887,
	3479,
	3479,
	2870,
	3479,
	3479,
	3479,
	3479,
	3479,
	2300,
	3479,
	3479,
	5226,
	3479,
	3479,
	3479,
	3451,
	2920,
	3451,
	2920,
	3414,
	2887,
	3414,
	2887,
	3451,
	2920,
	3414,
	2887,
	3451,
	2920,
	3451,
	2920,
	3414,
	2887,
	3414,
	2887,
	3451,
	2920,
	3397,
	2870,
	3397,
	2870,
	3479,
	1612,
	2300,
	2300,
	3479,
	3479,
	3479,
	3479,
	3479,
	3479,
	3479,
	3479,
	3479,
	3479,
	3479,
	3479,
	3479,
	3479,
	3479,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	5208,
	-1,
	-1,
	-1,
	-1,
	3479,
	3479,
	3479,
	-1,
	-1,
	-1,
	-1,
	-1,
	5066,
	5066,
	4759,
	5226,
	2887,
	2887,
	3479,
	5226,
	1745,
	2887,
	942,
	2887,
	3479,
	3479,
	3479,
	3479,
	1747,
	2300,
	5216,
	5166,
	5208,
	3479,
	3479,
	3479,
	3479,
	5208,
	3479,
	3414,
	3414,
	3479,
	3479,
	2887,
	2303,
	2303,
	3479,
	3479,
	5226,
	2887,
	2922,
	2922,
	3479,
	3479,
	2887,
	2870,
	2870,
	3479,
	3451,
	3414,
	3479,
	3414,
	2870,
	3479,
	3451,
	3414,
	3479,
	3414,
	2870,
	3479,
	3451,
	3414,
	3479,
	3414,
	2870,
	3479,
	3451,
	3414,
	3479,
	3414,
	5208,
	3479,
	3414,
	3414,
	3479,
	3479,
	2303,
	2303,
	3479,
	3479,
	5226,
	2887,
	2922,
	2922,
	3479,
	3479,
	2887,
	2870,
	2870,
	3479,
	3451,
	3414,
	3479,
	3414,
	2870,
	3479,
	3451,
	3414,
	3479,
	3414,
	2870,
	3479,
	3451,
	3414,
	3479,
	3414,
	2870,
	3479,
	3451,
	3414,
	3479,
	3414,
	5208,
	3479,
	3414,
	3479,
	5226,
	3479,
	3479,
	3479,
	3479,
	3479,
	2870,
	3479,
	3451,
	3414,
	3479,
	3414,
	2887,
	3414,
	3414,
	2887,
	5208,
	3479,
	3414,
	2887,
	3414,
	3479,
	2887,
	3479,
	5226,
	2887,
	2887,
	2870,
	3479,
	3451,
	3414,
	3479,
	3414,
	2870,
	3479,
	3451,
	3414,
	3479,
	3414,
	2870,
	3479,
	3451,
	3414,
	3479,
	3414,
	2870,
	3479,
	3451,
	3414,
	3479,
	3414,
	5208,
	3479,
	3414,
	3414,
	3479,
	3479,
	3479,
	2887,
	3479,
	5226,
	2887,
	2887,
	2870,
	3479,
	3451,
	3414,
	3479,
	3414,
	2870,
	3479,
	3451,
	3414,
	3479,
	3414,
	3479,
	3479,
	5226,
	5208,
	3479,
	3414,
	3414,
	2887,
	3479,
	3479,
	5226,
	2887,
	2887,
	2870,
	3479,
	3451,
	3414,
	3479,
	3414,
	2870,
	3479,
	3451,
	3414,
	3479,
	3414,
	2887,
	3479,
	2887,
	2887,
	3451,
	3479,
	3479,
	3479,
	3479,
	2870,
	2887,
	1754,
	1754,
	1747,
	1747,
	1754,
	2887,
	3451,
	3414,
	3414,
	3479,
	5208,
	1747,
	3479,
	3479,
	3414,
	3414,
	3414,
	3414,
	3414,
	3451,
	2887,
	2887,
	2887,
	1754,
	1754,
	1747,
	1754,
	2887,
	3479,
	1747,
	3479,
	3479,
	3479,
	3479,
	2870,
	2300,
	3479,
	5226,
	2870,
	3479,
	3451,
	3414,
	3479,
	3414,
	2870,
	3479,
	3451,
	3414,
	3479,
	3414,
	2870,
	3479,
	3451,
	3414,
	3479,
	3414,
	2870,
	3479,
	3451,
	3414,
	3479,
	3414,
	2870,
	3479,
	3451,
	3414,
	3479,
	3414,
	3479,
	3479,
	5208,
	5166,
	4875,
	4875,
	4862,
	4862,
	4455,
	4857,
	4679,
	4234,
	3937,
	5208,
	5159,
	5159,
	4851,
	3479,
	5226,
	3479,
	3479,
	3479,
	2887,
	2887,
	3451,
	3414,
	3414,
	3479,
	2887,
	3451,
	3414,
	3414,
	3479,
	5208,
	3479,
	3414,
	3451,
	2887,
	2887,
	3479,
	2887,
	1754,
	1754,
	1747,
	1747,
	1754,
	3479,
	3479,
	3479,
	3479,
	2870,
	3479,
	5226,
	2870,
	3479,
	3451,
	3414,
	3479,
	3414,
	2887,
	2887,
	2887,
	2887,
	2887,
	2887,
	3479,
	5226,
	1745,
	2545,
	942,
	2545,
	1745,
	2887,
	942,
	2887,
	3479,
	3479,
	1747,
	2300,
	5216,
	3479,
	5166,
	5208,
	3479,
	3479,
	3479,
	3479,
	3414,
	2545,
	3479,
	2887,
	2870,
	2870,
	2870,
	3479,
	3451,
	3414,
	3479,
	3414,
	3479,
	3414,
	2545,
	3479,
	2887,
	2870,
	2870,
	2870,
	3479,
	3451,
	3414,
	3479,
	3414,
	3414,
	3479,
	3479,
	5226,
	2870,
	3479,
	3451,
	3414,
	3479,
	3414,
	3479,
	3414,
	2545,
	3479,
	2887,
	2870,
	2870,
	2870,
	3479,
	3451,
	3414,
	3479,
	3414,
	3479,
	3414,
	2545,
	3479,
	2887,
	2870,
	2870,
	2870,
	3479,
	3451,
	3414,
	3479,
	3414,
	3414,
	3479,
	3479,
	3479,
	3479,
	3479,
	3479,
	2870,
	3479,
	3451,
	3414,
	3479,
	3414,
	3414,
	2545,
	3479,
	2870,
	3479,
	3451,
	3414,
	3479,
	3414,
	3414,
	2545,
	3479,
	2870,
	3479,
	3451,
	3414,
	3479,
	3414,
	3479,
	2887,
	2887,
	3451,
	3479,
	3479,
	3479,
	3479,
	2870,
	2887,
	2887,
	2887,
	3451,
	3414,
	3414,
	3479,
	3479,
	3479,
	3479,
	5208,
	1747,
	3479,
	3479,
	3414,
	2300,
	3414,
	3414,
	3479,
	3451,
	3479,
	2887,
	2887,
	2300,
	2887,
	3479,
	3479,
	3479,
	3479,
	2870,
	3479,
	5226,
	2870,
	3479,
	3451,
	3414,
	3479,
	3414,
	2870,
	3479,
	3451,
	3414,
	3479,
	3414,
	2870,
	3479,
	3451,
	3414,
	3479,
	3414,
	2870,
	3479,
	3451,
	3414,
	3479,
	3414,
	5208,
	5166,
	4875,
	4875,
	4862,
	4857,
	5208,
	5208,
	4679,
	4679,
	4679,
	4679,
	5159,
	4455,
	4862,
	4234,
	3736,
	4513,
	4224,
	5208,
	5159,
	5208,
	5159,
	5159,
	4851,
	5159,
	4851,
	3634,
	3479,
	5226,
	3479,
	3479,
	3479,
	2887,
	2887,
	3451,
	3414,
	3414,
	3479,
	2887,
	3451,
	3414,
	3414,
	3479,
	5208,
	3451,
	3479,
	3479,
	3479,
	2887,
	2887,
	3479,
	3479,
	3479,
	3479,
	3479,
	2870,
	3479,
	5226,
	3414,
	2887,
	3479,
	2545,
	3414,
	3479,
	3479,
	2303,
	2303,
	3479,
	3479,
	3479,
	3479,
	3479,
	2887,
	2870,
	2870,
	2887,
	2922,
	2922,
	2922,
	2887,
	2870,
	2870,
	3479,
	3451,
	3414,
	3479,
	3414,
	2870,
	3479,
	3451,
	3414,
	3479,
	3414,
	2870,
	3479,
	3451,
	3414,
	3479,
	3414,
	2870,
	3479,
	3451,
	3414,
	3479,
	3414,
	1738,
	3414,
	3414,
	2887,
	3414,
	2545,
	2887,
	3414,
	3479,
	2887,
	3479,
	3479,
	3479,
	2887,
	2887,
	2870,
	3479,
	3451,
	3414,
	3479,
	3414,
	2870,
	3479,
	3451,
	3414,
	3479,
	3414,
	2870,
	3479,
	3451,
	3414,
	3479,
	3414,
	2870,
	3479,
	3451,
	3414,
	3479,
	3414,
	5156,
	5159,
	5159,
	3660,
	3752,
	3575,
	3955,
	4862,
	5226,
	5226,
	5159,
	4614,
	5066,
	5131,
	5066,
	3942,
	4843,
	3479,
	5226,
	2870,
	1747,
	440,
	92,
	161,
	27,
	4862,
	3479,
	5159,
	3479,
	4998,
};
static const Il2CppTokenRangePair s_rgctxIndices[17] = 
{
	{ 0x02000083, { 23, 23 } },
	{ 0x0600000C, { 0, 1 } },
	{ 0x06000011, { 1, 1 } },
	{ 0x06000012, { 2, 2 } },
	{ 0x06000013, { 4, 3 } },
	{ 0x06000018, { 7, 4 } },
	{ 0x06000019, { 11, 6 } },
	{ 0x0600001A, { 17, 6 } },
	{ 0x060004AC, { 46, 1 } },
	{ 0x060004AD, { 47, 1 } },
	{ 0x060004AE, { 48, 1 } },
	{ 0x060004AF, { 49, 1 } },
	{ 0x060004B3, { 50, 2 } },
	{ 0x060004B4, { 52, 1 } },
	{ 0x060004B5, { 53, 1 } },
	{ 0x060004B6, { 54, 2 } },
	{ 0x060004B7, { 56, 1 } },
};
static const Il2CppRGCTXDefinition s_rgctxValues[57] = 
{
	{ (Il2CppRGCTXDataType)2, 132 },
	{ (Il2CppRGCTXDataType)2, 133 },
	{ (Il2CppRGCTXDataType)2, 2588 },
	{ (Il2CppRGCTXDataType)2, 3275 },
	{ (Il2CppRGCTXDataType)2, 2805 },
	{ (Il2CppRGCTXDataType)2, 2989 },
	{ (Il2CppRGCTXDataType)2, 131 },
	{ (Il2CppRGCTXDataType)2, 2587 },
	{ (Il2CppRGCTXDataType)2, 2804 },
	{ (Il2CppRGCTXDataType)2, 2988 },
	{ (Il2CppRGCTXDataType)2, 130 },
	{ (Il2CppRGCTXDataType)2, 2915 },
	{ (Il2CppRGCTXDataType)2, 3065 },
	{ (Il2CppRGCTXDataType)3, 12412 },
	{ (Il2CppRGCTXDataType)2, 424 },
	{ (Il2CppRGCTXDataType)3, 12413 },
	{ (Il2CppRGCTXDataType)2, 871 },
	{ (Il2CppRGCTXDataType)2, 2916 },
	{ (Il2CppRGCTXDataType)2, 3066 },
	{ (Il2CppRGCTXDataType)3, 12414 },
	{ (Il2CppRGCTXDataType)2, 2727 },
	{ (Il2CppRGCTXDataType)3, 12415 },
	{ (Il2CppRGCTXDataType)2, 425 },
	{ (Il2CppRGCTXDataType)3, 3547 },
	{ (Il2CppRGCTXDataType)2, 1745 },
	{ (Il2CppRGCTXDataType)3, 3548 },
	{ (Il2CppRGCTXDataType)3, 3549 },
	{ (Il2CppRGCTXDataType)3, 3550 },
	{ (Il2CppRGCTXDataType)3, 3551 },
	{ (Il2CppRGCTXDataType)3, 3552 },
	{ (Il2CppRGCTXDataType)1, 3512 },
	{ (Il2CppRGCTXDataType)2, 3512 },
	{ (Il2CppRGCTXDataType)3, 12427 },
	{ (Il2CppRGCTXDataType)3, 12428 },
	{ (Il2CppRGCTXDataType)3, 3553 },
	{ (Il2CppRGCTXDataType)3, 3583 },
	{ (Il2CppRGCTXDataType)3, 3556 },
	{ (Il2CppRGCTXDataType)3, 7844 },
	{ (Il2CppRGCTXDataType)3, 7843 },
	{ (Il2CppRGCTXDataType)2, 2045 },
	{ (Il2CppRGCTXDataType)3, 19434 },
	{ (Il2CppRGCTXDataType)2, 599 },
	{ (Il2CppRGCTXDataType)3, 19435 },
	{ (Il2CppRGCTXDataType)2, 941 },
	{ (Il2CppRGCTXDataType)1, 941 },
	{ (Il2CppRGCTXDataType)1, 599 },
	{ (Il2CppRGCTXDataType)2, 335 },
	{ (Il2CppRGCTXDataType)2, 334 },
	{ (Il2CppRGCTXDataType)2, 337 },
	{ (Il2CppRGCTXDataType)2, 336 },
	{ (Il2CppRGCTXDataType)2, 388 },
	{ (Il2CppRGCTXDataType)3, 26322 },
	{ (Il2CppRGCTXDataType)3, 26317 },
	{ (Il2CppRGCTXDataType)2, 387 },
	{ (Il2CppRGCTXDataType)1, 391 },
	{ (Il2CppRGCTXDataType)2, 391 },
	{ (Il2CppRGCTXDataType)3, 26318 },
};
extern const CustomAttributesCacheGenerator g_AssemblyU2DCSharpU2Dfirstpass_AttributeGenerators[];
IL2CPP_EXTERN_C const Il2CppCodeGenModule g_AssemblyU2DCSharpU2Dfirstpass_CodeGenModule;
const Il2CppCodeGenModule g_AssemblyU2DCSharpU2Dfirstpass_CodeGenModule = 
{
	"Assembly-CSharp-firstpass.dll",
	1919,
	s_methodPointers,
	0,
	NULL,
	s_InvokerIndices,
	0,
	NULL,
	17,
	s_rgctxIndices,
	57,
	s_rgctxValues,
	NULL,
	g_AssemblyU2DCSharpU2Dfirstpass_AttributeGenerators,
	NULL, // module initializer,
	NULL,
	NULL,
	NULL,
};
