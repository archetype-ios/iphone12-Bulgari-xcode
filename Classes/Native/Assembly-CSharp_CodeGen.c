﻿#include "pch-c.h"
#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include "codegen/il2cpp-codegen-metadata.h"





// 0x00000001 System.Void WebAudioActionExample::Start()
extern void WebAudioActionExample_Start_m520A7BF8B9F6C1C6722AE202F18DE074A5D22A53 (void);
// 0x00000002 System.Void WebAudioActionExample::Update()
extern void WebAudioActionExample_Update_mA50491CC6D1CC38B73BBD40773DE96592A818D31 (void);
// 0x00000003 System.Collections.IEnumerator WebAudioActionExample::delayAudioWeb(System.Single)
extern void WebAudioActionExample_delayAudioWeb_m1E19E01C2B6F78F3A6DACA39E36E0B0E2A9833A2 (void);
// 0x00000004 System.Void WebAudioActionExample::playMyAudio(System.String)
extern void WebAudioActionExample_playMyAudio_m5AA4ABB566730A4C6DC41415D0B0C2631535A359 (void);
// 0x00000005 System.Void WebAudioActionExample::ToggleFetch(SpeechDetection)
extern void WebAudioActionExample_ToggleFetch_m7902D8850B518B6422F52226A1C6F387B86E2825 (void);
// 0x00000006 System.Void WebAudioActionExample::GetAudioClip(System.String,UnityEngine.AudioType)
extern void WebAudioActionExample_GetAudioClip_m75452A554F88957E8EF4A1E68A81A0A5ECCB4595 (void);
// 0x00000007 System.Void WebAudioActionExample::OnSuccess(UnityEngine.AudioClip)
extern void WebAudioActionExample_OnSuccess_m2F9BAD7CF8313944778889FF36E30066F4FD304C (void);
// 0x00000008 System.Void WebAudioActionExample::OnFailed(CrazyMinnow.AmplitudeWebGL.WebAudioUrls.Notification)
extern void WebAudioActionExample_OnFailed_mC5BA7C0AE6097ADF28A4ED86217A437FD0619D3E (void);
// 0x00000009 System.Void WebAudioActionExample::.ctor()
extern void WebAudioActionExample__ctor_m2E0C9889A785EA88BB2E814A39C2F749B22CCBA4 (void);
// 0x0000000A System.Void WebAudioActionExample/<delayAudioWeb>d__14::.ctor(System.Int32)
extern void U3CdelayAudioWebU3Ed__14__ctor_m3AA6C070450EF01955EF5D793E777B5552E71000 (void);
// 0x0000000B System.Void WebAudioActionExample/<delayAudioWeb>d__14::System.IDisposable.Dispose()
extern void U3CdelayAudioWebU3Ed__14_System_IDisposable_Dispose_m57DE3ECAF415939BA57B92E8314593062F3D2A78 (void);
// 0x0000000C System.Boolean WebAudioActionExample/<delayAudioWeb>d__14::MoveNext()
extern void U3CdelayAudioWebU3Ed__14_MoveNext_mAD2B0B36E401C812B966270B37C6786FCC5E4F0D (void);
// 0x0000000D System.Object WebAudioActionExample/<delayAudioWeb>d__14::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CdelayAudioWebU3Ed__14_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m433CB2171CF6E1957477168B3CC8AC3701B90158 (void);
// 0x0000000E System.Void WebAudioActionExample/<delayAudioWeb>d__14::System.Collections.IEnumerator.Reset()
extern void U3CdelayAudioWebU3Ed__14_System_Collections_IEnumerator_Reset_m72A7F23479569D7C4F4F1A9C8F77BF8470D66B89 (void);
// 0x0000000F System.Object WebAudioActionExample/<delayAudioWeb>d__14::System.Collections.IEnumerator.get_Current()
extern void U3CdelayAudioWebU3Ed__14_System_Collections_IEnumerator_get_Current_m024134DB8FAE671E8FA571CA0748217221FA4FC5 (void);
// 0x00000010 System.Void WebAudioBatchExample::Start()
extern void WebAudioBatchExample_Start_m1CEC8884AE94B9C92EB9C4A01F7A0CB69044F7EF (void);
// 0x00000011 System.Void WebAudioBatchExample::Update()
extern void WebAudioBatchExample_Update_mF3AB99FCE85A61839726DE55781CE4049330B057 (void);
// 0x00000012 System.Void WebAudioBatchExample::PushAudioUrlToQueue(CrazyMinnow.AmplitudeWebGL.WebAudioUrls.AudioUrl)
extern void WebAudioBatchExample_PushAudioUrlToQueue_m64F0984821D402B4EF538DDFA624FB13754D8B4C (void);
// 0x00000013 System.Void WebAudioBatchExample::GetAudioClip()
extern void WebAudioBatchExample_GetAudioClip_mE1FAFEA41FEF737F6C8DBA1D653AC0E7051C013F (void);
// 0x00000014 System.Void WebAudioBatchExample::OnSuccess(UnityEngine.AudioClip)
extern void WebAudioBatchExample_OnSuccess_mD019682A9C1CAB60CEF1079E71CD887391761520 (void);
// 0x00000015 System.Void WebAudioBatchExample::OnFailed(CrazyMinnow.AmplitudeWebGL.WebAudioUrls.Notification)
extern void WebAudioBatchExample_OnFailed_m1D9B4B9B9146284B54A07FFBC7749D4E47B08F55 (void);
// 0x00000016 System.Void WebAudioBatchExample::.ctor()
extern void WebAudioBatchExample__ctor_m37516A360B4E15CBE3712D183E84C91407A177CA (void);
// 0x00000017 System.Void SALSA_Template_EventControllerSubscriber::OnEnable()
extern void SALSA_Template_EventControllerSubscriber_OnEnable_mB6649D7D3E3A026EEA456196939D6A4B64FC921E (void);
// 0x00000018 System.Void SALSA_Template_EventControllerSubscriber::OnDisable()
extern void SALSA_Template_EventControllerSubscriber_OnDisable_m416FA6A8104B86BF45BCDBF391C5233C8033AE9E (void);
// 0x00000019 System.Void SALSA_Template_EventControllerSubscriber::OnAnimationStarting(System.Object,CrazyMinnow.SALSA.EventController/EventControllerNotificationArgs)
extern void SALSA_Template_EventControllerSubscriber_OnAnimationStarting_mB08B03CF07F2639CC08354B819EFF7A7EFCDF660 (void);
// 0x0000001A System.Void SALSA_Template_EventControllerSubscriber::OnAnimationON(System.Object,CrazyMinnow.SALSA.EventController/EventControllerNotificationArgs)
extern void SALSA_Template_EventControllerSubscriber_OnAnimationON_m52E405BC8FA4F0032C9778ECA538884B5D6A380E (void);
// 0x0000001B System.Void SALSA_Template_EventControllerSubscriber::OnAnimationEnding(System.Object,CrazyMinnow.SALSA.EventController/EventControllerNotificationArgs)
extern void SALSA_Template_EventControllerSubscriber_OnAnimationEnding_m97D0D9EEC53BB13FB3A84A8A2DB9834FA902A4C0 (void);
// 0x0000001C System.Void SALSA_Template_EventControllerSubscriber::OnAnimationOFF(System.Object,CrazyMinnow.SALSA.EventController/EventControllerNotificationArgs)
extern void SALSA_Template_EventControllerSubscriber_OnAnimationOFF_m10571050F35F15FCD9DA77050325BDD983342478 (void);
// 0x0000001D System.Void SALSA_Template_EventControllerSubscriber::.ctor()
extern void SALSA_Template_EventControllerSubscriber__ctor_m53D942182B15797AE2684742632DF341377082F4 (void);
// 0x0000001E System.Void SALSA_Template_SalsaEventSubscriber::OnEnable()
extern void SALSA_Template_SalsaEventSubscriber_OnEnable_m94A019D5443ED1B1AD2592265517A745F71AD09F (void);
// 0x0000001F System.Void SALSA_Template_SalsaEventSubscriber::OnDisable()
extern void SALSA_Template_SalsaEventSubscriber_OnDisable_mA29752E8EF1E71DA7A4F41C316F487FA050DEE80 (void);
// 0x00000020 System.Void SALSA_Template_SalsaEventSubscriber::OnStoppedSalsaing(System.Object,CrazyMinnow.SALSA.Salsa/SalsaNotificationArgs)
extern void SALSA_Template_SalsaEventSubscriber_OnStoppedSalsaing_mFE4FE659CB7BE04B16A6D951990B69D8135365FF (void);
// 0x00000021 System.Void SALSA_Template_SalsaEventSubscriber::OnStartedSalsaing(System.Object,CrazyMinnow.SALSA.Salsa/SalsaNotificationArgs)
extern void SALSA_Template_SalsaEventSubscriber_OnStartedSalsaing_m99DED93FD77CF6D8A72D84DEACAD0B903B1DBFD9 (void);
// 0x00000022 System.Void SALSA_Template_SalsaEventSubscriber::.ctor()
extern void SALSA_Template_SalsaEventSubscriber__ctor_mD8ACCD47157F8DBD752790095821DE39A7FB9C34 (void);
// 0x00000023 System.Void SALSA_Template_SalsaVisemeTriggerEventSubscriber::OnEnable()
extern void SALSA_Template_SalsaVisemeTriggerEventSubscriber_OnEnable_m00DC9D18B5E991800A35552CECC62B8C4902F9B2 (void);
// 0x00000024 System.Void SALSA_Template_SalsaVisemeTriggerEventSubscriber::OnDisable()
extern void SALSA_Template_SalsaVisemeTriggerEventSubscriber_OnDisable_mC4549D9BFAB298009D7990E173CC5DA8AABAEAA6 (void);
// 0x00000025 System.Void SALSA_Template_SalsaVisemeTriggerEventSubscriber::SalsaOnVisemeTriggered(System.Object,CrazyMinnow.SALSA.Salsa/SalsaNotificationArgs)
extern void SALSA_Template_SalsaVisemeTriggerEventSubscriber_SalsaOnVisemeTriggered_m2A6D575DE39D79FA7B2676F54712CEF650A97201 (void);
// 0x00000026 System.Void SALSA_Template_SalsaVisemeTriggerEventSubscriber::.ctor()
extern void SALSA_Template_SalsaVisemeTriggerEventSubscriber__ctor_m75629EB0AFBC47F4E066FD0F4F570E3283947DF7 (void);
// 0x00000027 System.Void Breathe::Start()
extern void Breathe_Start_m8D4C7456E8CFD5AE95F03929963046E0FB88C29F (void);
// 0x00000028 System.Void Breathe::Update()
extern void Breathe_Update_m95AAD266506234658D72A172EA47AF4A42496A81 (void);
// 0x00000029 System.Void Breathe::.ctor()
extern void Breathe__ctor_mC71EAC917BF49532042E70D3EA8446E39D51E130 (void);
// 0x0000002A System.Void LightMover::Start()
extern void LightMover_Start_mD72AE2058B16ECC907FA2F94428F45E9911C1B7A (void);
// 0x0000002B System.Void LightMover::Update()
extern void LightMover_Update_m80CD35B5C65D116E8B977382A5843C549449816B (void);
// 0x0000002C System.Collections.IEnumerator LightMover::Rando()
extern void LightMover_Rando_m2335CCCA2B020B0478A0FDC8E93A7A1C091FCACC (void);
// 0x0000002D System.Void LightMover::.ctor()
extern void LightMover__ctor_m8ACFB59609F815667BCC20468A4A4BAC0B61320F (void);
// 0x0000002E System.Void LightMover/<Rando>d__5::.ctor(System.Int32)
extern void U3CRandoU3Ed__5__ctor_m1403ED7FD2349C2CB16CEA7D4C00520A38CA3EEC (void);
// 0x0000002F System.Void LightMover/<Rando>d__5::System.IDisposable.Dispose()
extern void U3CRandoU3Ed__5_System_IDisposable_Dispose_m73AE8C4BF7C348A094C55ADD9634534E5B081270 (void);
// 0x00000030 System.Boolean LightMover/<Rando>d__5::MoveNext()
extern void U3CRandoU3Ed__5_MoveNext_m1B3060CE052AED0659E1A3715106527C2FC30E0A (void);
// 0x00000031 System.Object LightMover/<Rando>d__5::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CRandoU3Ed__5_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m1AE9A7A8229C4C92D44B2DBDA606E5D5DECE43A5 (void);
// 0x00000032 System.Void LightMover/<Rando>d__5::System.Collections.IEnumerator.Reset()
extern void U3CRandoU3Ed__5_System_Collections_IEnumerator_Reset_m1A73670DB4DF628509B735CBEA3A26E070FF0DA6 (void);
// 0x00000033 System.Object LightMover/<Rando>d__5::System.Collections.IEnumerator.get_Current()
extern void U3CRandoU3Ed__5_System_Collections_IEnumerator_get_Current_mC4E9F4D960D2E0EA3109670068E88A32274C2EA6 (void);
// 0x00000034 System.Void LookAt::Start()
extern void LookAt_Start_m17791867B3D5AC7D09956B53884C99059A44C69E (void);
// 0x00000035 System.Void LookAt::Update()
extern void LookAt_Update_mF3460A983189AE9E63FAEE1C2E719D80BBFD4499 (void);
// 0x00000036 System.Void LookAt::.ctor()
extern void LookAt__ctor_m794401EFA0AB058C33C45BA86E49541B3B0F39DC (void);
// 0x00000037 System.Void MapChanger::Start()
extern void MapChanger_Start_m4DF9217889BEE970359C440CE49D689D29D66500 (void);
// 0x00000038 System.Void MapChanger::Update()
extern void MapChanger_Update_m0EF2ACD3BCCD4051E5343CD7BAFBCA0B8FF10904 (void);
// 0x00000039 System.Void MapChanger::OnGUI()
extern void MapChanger_OnGUI_m068D591CA8FD71CDDA062DA8BFD41AD6EFA94219 (void);
// 0x0000003A System.Void MapChanger::.ctor()
extern void MapChanger__ctor_m0C7D09A7F04868AC6DFB47E3C27EF9F84764D152 (void);
// 0x0000003B System.Void Probe::Awake()
extern void Probe_Awake_m89C4BBC848164D7B93EA1ED97936369DCBB5B676 (void);
// 0x0000003C System.Void Probe::Update()
extern void Probe_Update_m70C8F77A4CE9A3B50E4B9CA6897291B09CD7F094 (void);
// 0x0000003D System.Void Probe::.ctor()
extern void Probe__ctor_m2CE9B908BD06F078539259E005220E7AB1C60572 (void);
// 0x0000003E System.Single[] AudioTools::IncreaseVolume(System.Single[],System.Single)
extern void AudioTools_IncreaseVolume_m688AC39B7537437623CC2764353363A4F5D05952 (void);
// 0x0000003F System.Void mYmENU::Start()
extern void mYmENU_Start_m8443DA7EB9B0BA77D4989139BDFE338E95DD0C57 (void);
// 0x00000040 System.Void mYmENU::Update()
extern void mYmENU_Update_mD9465CD472EDB105002B8E40A8021E66D2DCF5DA (void);
// 0x00000041 System.Void mYmENU::LoadScene()
extern void mYmENU_LoadScene_mB44D21237A663F784033BD74F1AC0BE6CAD34DAE (void);
// 0x00000042 System.Void mYmENU::.ctor()
extern void mYmENU__ctor_mC2C1DC644C5EF3A881E92A6F4BB58B0CE904551D (void);
// 0x00000043 System.Void ExampleWheelController::Start()
extern void ExampleWheelController_Start_mAD76D68B46BA3AD276D093C1DAB4D8C781C9419A (void);
// 0x00000044 System.Void ExampleWheelController::Update()
extern void ExampleWheelController_Update_mBFE66DA1650BB9834C338777827D9E93E5165DFA (void);
// 0x00000045 System.Void ExampleWheelController::.ctor()
extern void ExampleWheelController__ctor_mE2773CAA6EF9FFDDC7312C1F287DB441F791695C (void);
// 0x00000046 System.Void ExampleWheelController/Uniforms::.cctor()
extern void Uniforms__cctor_m718B59E42C2DEAF29E07EC5506DACBE52FCB5746 (void);
// 0x00000047 System.Void MainScript::LogMessage(System.String,System.String)
extern void MainScript_LogMessage_m7953CB447ABFC4D0E286219425CF26684DB6F6B0 (void);
// 0x00000048 System.Void MainScript::Get()
extern void MainScript_Get_m3BD5249433D12EE1562CB113FF95FD302207B2A0 (void);
// 0x00000049 System.Void MainScript::Post()
extern void MainScript_Post_mF7C21819A39C80CBEF1E5471FF7A505A6CF9B687 (void);
// 0x0000004A System.Void MainScript::Put()
extern void MainScript_Put_m2E077AE7D135014788A9F219D4FA32B618BED552 (void);
// 0x0000004B System.Void MainScript::Delete()
extern void MainScript_Delete_m3CEEF47B740A70E766D3CF74E6C1D37E13C0A8C8 (void);
// 0x0000004C System.Void MainScript::AbortRequest()
extern void MainScript_AbortRequest_mDCEC8008FD946FB21C66D8DE04AA652FD091AC5E (void);
// 0x0000004D System.Void MainScript::DownloadFile()
extern void MainScript_DownloadFile_mF54C07D06969AD450838E27A885FB450BDA6971C (void);
// 0x0000004E System.Void MainScript::.ctor()
extern void MainScript__ctor_mDE68DE7EC111F10F50287CE15AC2100BAC26DF23 (void);
// 0x0000004F System.Void MainScript::<Post>b__4_0(Models.Post)
extern void MainScript_U3CPostU3Eb__4_0_m8B0202AA78218D3787871313668C76F260A52D42 (void);
// 0x00000050 System.Void MainScript::<Post>b__4_1(System.Exception)
extern void MainScript_U3CPostU3Eb__4_1_m2C8C662A8A4EE9A219C715C38E88E9D156CC0777 (void);
// 0x00000051 System.Void MainScript::<Put>b__5_0(Proyecto26.RequestException,Proyecto26.ResponseHelper,Models.Post)
extern void MainScript_U3CPutU3Eb__5_0_m556845DFA7BB10FCFF4A3A04B17EF6BC8AA77CAE (void);
// 0x00000052 System.Void MainScript::<Delete>b__6_0(Proyecto26.RequestException,Proyecto26.ResponseHelper)
extern void MainScript_U3CDeleteU3Eb__6_0_m3092FBC0D31C4B83C0C7E537BFD404FD28F0BB8E (void);
// 0x00000053 System.Void MainScript::<DownloadFile>b__8_0(Proyecto26.ResponseHelper)
extern void MainScript_U3CDownloadFileU3Eb__8_0_m8D61A6C39FFBB97884CF0935DA9BF297B5DE8393 (void);
// 0x00000054 System.Void MainScript::<DownloadFile>b__8_1(System.Exception)
extern void MainScript_U3CDownloadFileU3Eb__8_1_m99B4D340A42D8D8B784669EDB99B721CAD853FB6 (void);
// 0x00000055 System.Void MainScript/<>c__DisplayClass3_0::.ctor()
extern void U3CU3Ec__DisplayClass3_0__ctor_m46C50E0A5C12FA151CC55E0B928E27838925B4FC (void);
// 0x00000056 RSG.IPromise`1<Models.Todo[]> MainScript/<>c__DisplayClass3_0::<Get>b__0(Models.Post[])
extern void U3CU3Ec__DisplayClass3_0_U3CGetU3Eb__0_m1E95C1F0B2D8E5BC302A5ADCA1ABAE0293AEA82D (void);
// 0x00000057 RSG.IPromise`1<Models.User[]> MainScript/<>c__DisplayClass3_0::<Get>b__1(Models.Todo[])
extern void U3CU3Ec__DisplayClass3_0_U3CGetU3Eb__1_mB5A9CA352F06FB80FDC47E6FE114495BA4687AC1 (void);
// 0x00000058 RSG.IPromise`1<Models.Photo[]> MainScript/<>c__DisplayClass3_0::<Get>b__2(Models.User[])
extern void U3CU3Ec__DisplayClass3_0_U3CGetU3Eb__2_mB93ACE707409C85F0542FD7665B0BF499473D98D (void);
// 0x00000059 System.Void MainScript/<>c__DisplayClass3_0::<Get>b__3(Models.Photo[])
extern void U3CU3Ec__DisplayClass3_0_U3CGetU3Eb__3_m381C9E851E8FB988B02A1076B749138ABC954BDB (void);
// 0x0000005A System.Void MainScript/<>c__DisplayClass3_0::<Get>b__4(System.Exception)
extern void U3CU3Ec__DisplayClass3_0_U3CGetU3Eb__4_m44FBEB10E4BAEB006FC92154DB40AB3CB7BC4DE0 (void);
// 0x0000005B System.Void MainScript/<>c::.cctor()
extern void U3CU3Ec__cctor_m4FA3DF2408BEA55F36632D2A36DDEA9E1475BBA3 (void);
// 0x0000005C System.Void MainScript/<>c::.ctor()
extern void U3CU3Ec__ctor_m6B5FCC705564205B21810FFBA043CB8DA659F8E1 (void);
// 0x0000005D System.Void MainScript/<>c::<Put>b__5_1(Proyecto26.RequestException,System.Int32)
extern void U3CU3Ec_U3CPutU3Eb__5_1_mC12F48C710EDD364D74E0411BCE9CB0F55326D2F (void);
// 0x0000005E System.Void ArrayLenghtExample::Update()
extern void ArrayLenghtExample_Update_m439A2360ABB7F38336FC1763B3251D37764119C1 (void);
// 0x0000005F System.Void ArrayLenghtExample::.ctor()
extern void ArrayLenghtExample__ctor_m8559D0D49A2BB7EFDFDD61E2CC8D54DB71840176 (void);
// 0x00000060 System.Void Fashionmanager::Start()
extern void Fashionmanager_Start_m7840AD390EC9CC1481D8912CAE6FAC17C8C8536B (void);
// 0x00000061 System.Void Fashionmanager::Update()
extern void Fashionmanager_Update_m6E657E7ED7E231D179A3C112BBFE2333117A7E80 (void);
// 0x00000062 System.Void Fashionmanager::.ctor()
extern void Fashionmanager__ctor_m3B067E91A0E4C80C06482F0F0BE43522768F89D1 (void);
// 0x00000063 System.Void MenuManager::Start()
extern void MenuManager_Start_m4E4A5EF33C27448D7F34FD29B93589635F1B2EE2 (void);
// 0x00000064 System.Void MenuManager::GoBack()
extern void MenuManager_GoBack_mED70223B46A8AF47FFD83ADA5978C1335BA581B3 (void);
// 0x00000065 System.Void MenuManager::GoToMenu(UnityEngine.GameObject)
extern void MenuManager_GoToMenu_mD3F22584A78A4D0FFE0D024660B4ABC6130795D4 (void);
// 0x00000066 System.Void MenuManager::Animate(UnityEngine.GameObject,System.Boolean)
extern void MenuManager_Animate_m28495AA50A493ABA5454D5766FC9C548EA3C2C54 (void);
// 0x00000067 System.Void MenuManager::LoadScene()
extern void MenuManager_LoadScene_m7DBD3691722FA968ABA290C6C16FACA0650B74EE (void);
// 0x00000068 System.Void MenuManager::LoadChroma()
extern void MenuManager_LoadChroma_mA9A328B7D4020C6519D663B46F21F186F0A57F8A (void);
// 0x00000069 System.Void MenuManager::LoadVideo()
extern void MenuManager_LoadVideo_mDAF29DD24C4E6FF581942CED89CC35B834D907C6 (void);
// 0x0000006A System.Void MenuManager::Awake()
extern void MenuManager_Awake_mEFBAF3F8CBDEF5A033B3BAD9CA897801135B6463 (void);
// 0x0000006B System.Void MenuManager::.ctor()
extern void MenuManager__ctor_m8F61CC885B72291B54C1C6EC368AE303EA856529 (void);
// 0x0000006C System.Void Pause::Start()
extern void Pause_Start_mAF3951FCCD204C9094502892B23E1BB368C938BC (void);
// 0x0000006D System.Void Pause::Update()
extern void Pause_Update_m2BEBA55A60F1B469B29DD631505236F1B5F21FEE (void);
// 0x0000006E System.Void Pause::.ctor()
extern void Pause__ctor_m0A16764376F8C9A6D19DE0BB24A41FA81F587928 (void);
// 0x0000006F System.Void animator_clickController::Start()
extern void animator_clickController_Start_mBE1F9E31C6AD21F147AD13D1DA5F95E1E3390862 (void);
// 0x00000070 System.Void animator_clickController::Update()
extern void animator_clickController_Update_m91995767D34A551CC334C5A5600C09BD60573E34 (void);
// 0x00000071 System.Void animator_clickController::.ctor()
extern void animator_clickController__ctor_mE64FB9AD14ED760CC29EF32E209349C2DBF431A8 (void);
// 0x00000072 System.Void backgrounManager::Awake()
extern void backgrounManager_Awake_m4EEF27EBDEC805826663EE644346F79E62236E30 (void);
// 0x00000073 System.Void backgrounManager::Update()
extern void backgrounManager_Update_m4FB45C070AD0F22ADB799B3B94DBD0F856D62546 (void);
// 0x00000074 System.Void backgrounManager::greenBg()
extern void backgrounManager_greenBg_mC0E17D486AEABF43CC5F408F5BC2D1A84875655D (void);
// 0x00000075 System.Void backgrounManager::.ctor()
extern void backgrounManager__ctor_mD003C8D4654C9CB7687FE2244FD38A1B3D16AAEC (void);
// 0x00000076 System.Void cam_flyaway::Start()
extern void cam_flyaway_Start_mC7C9351D51B0780AABAE6A2DBAB14A55EB349FBD (void);
// 0x00000077 System.Void cam_flyaway::Update()
extern void cam_flyaway_Update_mE15153479D2889C0C61340C5D39EE25B3CDC6765 (void);
// 0x00000078 System.Void cam_flyaway::.ctor()
extern void cam_flyaway__ctor_m2E0B19657E7E75BC6B6AD6860DEBD85DC4A49C4C (void);
// 0x00000079 System.Collections.IEnumerator clickAnimatorParametr::waitclick()
extern void clickAnimatorParametr_waitclick_m084799C820BA08D53EB503E16D16C2C2DEDA7689 (void);
// 0x0000007A System.Void clickAnimatorParametr::Start()
extern void clickAnimatorParametr_Start_mFDC1CD2712202AA920C38E2B5109B437CE9F0F69 (void);
// 0x0000007B System.Void clickAnimatorParametr::Update()
extern void clickAnimatorParametr_Update_mD8FB42A7250BAE9F99D163364742A1F155A7FF21 (void);
// 0x0000007C System.Void clickAnimatorParametr::.ctor()
extern void clickAnimatorParametr__ctor_m85C40615274AE3798F0B3F8705BD8C06CEC4DA64 (void);
// 0x0000007D System.Void clickAnimatorParametr/<waitclick>d__3::.ctor(System.Int32)
extern void U3CwaitclickU3Ed__3__ctor_mA830488CFBA1E55D7FFAFC22226E78380257A5B0 (void);
// 0x0000007E System.Void clickAnimatorParametr/<waitclick>d__3::System.IDisposable.Dispose()
extern void U3CwaitclickU3Ed__3_System_IDisposable_Dispose_m3A1B2C90D476875637B69557F82A9BD37A0AE36B (void);
// 0x0000007F System.Boolean clickAnimatorParametr/<waitclick>d__3::MoveNext()
extern void U3CwaitclickU3Ed__3_MoveNext_mFD3158638EE4D965FEC903E8D1A5A54D98ABD5DB (void);
// 0x00000080 System.Object clickAnimatorParametr/<waitclick>d__3::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CwaitclickU3Ed__3_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mB511A71DDCEBDB0D88E2C392EA1FA088C3D9B402 (void);
// 0x00000081 System.Void clickAnimatorParametr/<waitclick>d__3::System.Collections.IEnumerator.Reset()
extern void U3CwaitclickU3Ed__3_System_Collections_IEnumerator_Reset_m8311FF5D6F36EE0D15CA64DEF8A7CECCDDFC4605 (void);
// 0x00000082 System.Object clickAnimatorParametr/<waitclick>d__3::System.Collections.IEnumerator.get_Current()
extern void U3CwaitclickU3Ed__3_System_Collections_IEnumerator_get_Current_mABF464AD1627A8BD8D5857C3B67C3CFB9297FA27 (void);
// 0x00000083 System.Void listTimelineController::Start()
extern void listTimelineController_Start_mE9426B939F4884E11A9138A4FF4AAE8DA6139545 (void);
// 0x00000084 System.Void listTimelineController::Play()
extern void listTimelineController_Play_m6BC4FB0B461B5C7BB1FBF56F8AE0F3A810E32492 (void);
// 0x00000085 System.Void listTimelineController::Update()
extern void listTimelineController_Update_mB4BAEF8477C68DE0A70E2F80DFA7FACE712C1943 (void);
// 0x00000086 System.Void listTimelineController::.ctor()
extern void listTimelineController__ctor_mE67DDFB8C79953E33276252565CED06EDA365879 (void);
// 0x00000087 System.Void quitpresentazione::Start()
extern void quitpresentazione_Start_mD1BC003BD2D4D9F40A2DC56A3827D0F86A169FF4 (void);
// 0x00000088 System.Void quitpresentazione::Update()
extern void quitpresentazione_Update_mCF4C9EDA4132BEDDEF978C0FE2B5E2C607677C22 (void);
// 0x00000089 System.Void quitpresentazione::.ctor()
extern void quitpresentazione__ctor_mC1DCA6B5834D4A7AE95217C9BCB86C3B36EE1331 (void);
// 0x0000008A System.Void switchDress::Start()
extern void switchDress_Start_m34F362F0E40E35D4EF75F321D8210CF0FEBE1C1E (void);
// 0x0000008B System.Void switchDress::Update()
extern void switchDress_Update_m65A656821DE4BEDF1C20CE71A0FDF1C10C8109B6 (void);
// 0x0000008C System.Void switchDress::.ctor()
extern void switchDress__ctor_mFD6EA473E3822BE5261918B65B69A5E8764F7ACF (void);
// 0x0000008D System.Void talknorma::Start()
extern void talknorma_Start_m6EE2E56716E2FB1DAAF1D91C54ADBF0B1F6F3877 (void);
// 0x0000008E System.Void talknorma::Update()
extern void talknorma_Update_m7919C3B5B861CCCB27F912BB4BC4E4ACCBEADFF3 (void);
// 0x0000008F System.Void talknorma::.ctor()
extern void talknorma__ctor_m597E37CC82955980B90DA2A88C894DFE6A31225B (void);
// 0x00000090 System.Void timelineController::Play()
extern void timelineController_Play_m61EBE6F06FF37BB2A3DE17E8521DFC4701A90174 (void);
// 0x00000091 System.Void timelineController::Start()
extern void timelineController_Start_m165B9B4F66B5F011CA8FBF45D1B1ACFAF0B0C128 (void);
// 0x00000092 System.Void timelineController::Update()
extern void timelineController_Update_m70BADEAF07E27255C34919253C0FC5B684EC2AF9 (void);
// 0x00000093 System.Void timelineController::.ctor()
extern void timelineController__ctor_m2CF666BF13E36839D7982412435FB8C221EEA738 (void);
// 0x00000094 System.Void ActiveStateToggler::ToggleActive()
extern void ActiveStateToggler_ToggleActive_mE5AFDC4E770D3AB465A4EFEC92CA40F962BCE9FA (void);
// 0x00000095 System.Void ActiveStateToggler::.ctor()
extern void ActiveStateToggler__ctor_m8085169E76AB400B373EFC0E5E4028E189A25028 (void);
// 0x00000096 System.Void ApplicationManager::Quit()
extern void ApplicationManager_Quit_m403368607C71AACA27341CA2B11F90B15D9C2275 (void);
// 0x00000097 System.Void ApplicationManager::.ctor()
extern void ApplicationManager__ctor_m4D923E22E12C36CD444B73E88487BB4E77A83E62 (void);
// 0x00000098 System.Void AssetManager::Start()
extern void AssetManager_Start_m8FEBC0A4B22F47E6DB32C56C17A3105FB2B83D47 (void);
// 0x00000099 System.Void AssetManager::Update()
extern void AssetManager_Update_m2B32C31905FD36C44721D7DBDDF6FB199E5FA0B5 (void);
// 0x0000009A System.Void AssetManager::.ctor()
extern void AssetManager__ctor_mC11699C1F3965E8487903A31B3AE1089213B541A (void);
// 0x0000009B System.Void ChangeColor::OnEnable()
extern void ChangeColor_OnEnable_m7FE31A95B4478A63871D6365942BC37DE75B985D (void);
// 0x0000009C System.Void ChangeColor::SetRed(System.Single)
extern void ChangeColor_SetRed_mB0090838752887384E425FEAA937ABBEE13E4EB8 (void);
// 0x0000009D System.Void ChangeColor::SetGreen(System.Single)
extern void ChangeColor_SetGreen_m05B35D3F91BCA56B345321414A034A7D92D0E45B (void);
// 0x0000009E System.Void ChangeColor::SetBlue(System.Single)
extern void ChangeColor_SetBlue_m57D6E496618765E4DDE654275D7589AB1C409630 (void);
// 0x0000009F System.Void ChangeColor::OnValueChanged(System.Single,System.Int32)
extern void ChangeColor_OnValueChanged_mFA920A494A5F9749FCDBD6BFB7EC8E3D9B6172DB (void);
// 0x000000A0 System.Void ChangeColor::OnPointerClick(UnityEngine.EventSystems.PointerEventData)
extern void ChangeColor_OnPointerClick_m7AB5B0F618C688037FB77A27B05BBCB8AA957A1F (void);
// 0x000000A1 System.Void ChangeColor::.ctor()
extern void ChangeColor__ctor_m41B10590AF4632D7F8E7ABDD4130C5B1067561A6 (void);
// 0x000000A2 System.Void DragMe::OnBeginDrag(UnityEngine.EventSystems.PointerEventData)
extern void DragMe_OnBeginDrag_mFC5EDC3D150A4B114381C991228B71BFCC9760C9 (void);
// 0x000000A3 System.Void DragMe::OnDrag(UnityEngine.EventSystems.PointerEventData)
extern void DragMe_OnDrag_m7988B9DAA4376D14957C051110A049215892025D (void);
// 0x000000A4 System.Void DragMe::SetDraggedPosition(UnityEngine.EventSystems.PointerEventData)
extern void DragMe_SetDraggedPosition_m7C8DB86B92268E42387E0E6479521095429EABDE (void);
// 0x000000A5 System.Void DragMe::OnEndDrag(UnityEngine.EventSystems.PointerEventData)
extern void DragMe_OnEndDrag_m24576A6489ED4643BE5BAB5FE4246EF5857AFC3E (void);
// 0x000000A6 T DragMe::FindInParents(UnityEngine.GameObject)
// 0x000000A7 System.Void DragMe::.ctor()
extern void DragMe__ctor_mB0FCFD436CFBA38B6A39024D5BB21AE21D0CF978 (void);
// 0x000000A8 System.Void DragPanel::Awake()
extern void DragPanel_Awake_m7730D945BF0A144028A6F53F10306D326CEE9980 (void);
// 0x000000A9 System.Void DragPanel::OnPointerDown(UnityEngine.EventSystems.PointerEventData)
extern void DragPanel_OnPointerDown_mFDF80CB62D238558FC69270E3F05C9BADE87DAF3 (void);
// 0x000000AA System.Void DragPanel::OnDrag(UnityEngine.EventSystems.PointerEventData)
extern void DragPanel_OnDrag_m11327A95544774C1FA3EFC7E5B9B2D685C6DA6A7 (void);
// 0x000000AB System.Void DragPanel::ClampToWindow()
extern void DragPanel_ClampToWindow_m4F9FC1B0D2B94CA8060853823AAEE496AAE38AE7 (void);
// 0x000000AC System.Void DragPanel::.ctor()
extern void DragPanel__ctor_mBDA15B1E481A3D5538BD1B319AE4B3B4ABF64F7F (void);
// 0x000000AD System.Void DropMe::OnEnable()
extern void DropMe_OnEnable_mE82969854A6362AF87DE93F1A9CBFE6B83F504E5 (void);
// 0x000000AE System.Void DropMe::OnDrop(UnityEngine.EventSystems.PointerEventData)
extern void DropMe_OnDrop_m3AE8F24C2C9D91DFF973EEF324CC9CB5FAD9401D (void);
// 0x000000AF System.Void DropMe::OnPointerEnter(UnityEngine.EventSystems.PointerEventData)
extern void DropMe_OnPointerEnter_m2F6C117AF60413EBC4FB03EEB4D09EB40B3317B4 (void);
// 0x000000B0 System.Void DropMe::OnPointerExit(UnityEngine.EventSystems.PointerEventData)
extern void DropMe_OnPointerExit_m31E7FEB688CA54F16B81A3CA8A4B21D676EDB651 (void);
// 0x000000B1 UnityEngine.Sprite DropMe::GetDropSprite(UnityEngine.EventSystems.PointerEventData)
extern void DropMe_GetDropSprite_m2C3723D7AB5974D991F47C67F8BFB465D85DE166 (void);
// 0x000000B2 System.Void DropMe::.ctor()
extern void DropMe__ctor_mA62B4643146CD6841C0D898D5686FEF75A7E5DF9 (void);
// 0x000000B3 System.Void PanelManager::OnEnable()
extern void PanelManager_OnEnable_m09547ED27D87F696B41F1FAF5EFC3826F3204E2D (void);
// 0x000000B4 System.Void PanelManager::OpenPanel(UnityEngine.Animator)
extern void PanelManager_OpenPanel_m55C75D9E37F2CDE8B1670FBF979329E5D6D13E0F (void);
// 0x000000B5 UnityEngine.GameObject PanelManager::FindFirstEnabledSelectable(UnityEngine.GameObject)
extern void PanelManager_FindFirstEnabledSelectable_m59C12281C9EAED20F4FBB9C820D4EAD2C2DA117B (void);
// 0x000000B6 System.Void PanelManager::CloseCurrent()
extern void PanelManager_CloseCurrent_m20DE00E5BAE15FA1D4A052E555A3CC5C58A6A695 (void);
// 0x000000B7 System.Collections.IEnumerator PanelManager::DisablePanelDeleyed(UnityEngine.Animator)
extern void PanelManager_DisablePanelDeleyed_mB120A21FD25F2168724F35B9FAD134487EF0507A (void);
// 0x000000B8 System.Void PanelManager::SetSelected(UnityEngine.GameObject)
extern void PanelManager_SetSelected_m33A82F0C75C5439BE6C1FEE4B93BF4E3949FE882 (void);
// 0x000000B9 System.Void PanelManager::.ctor()
extern void PanelManager__ctor_m945C0470B90313D67EDCED0D97AFADB78A190AFD (void);
// 0x000000BA System.Void PanelManager/<DisablePanelDeleyed>d__10::.ctor(System.Int32)
extern void U3CDisablePanelDeleyedU3Ed__10__ctor_m3595FCFEB5B1694DEAE36303E82C43199E309861 (void);
// 0x000000BB System.Void PanelManager/<DisablePanelDeleyed>d__10::System.IDisposable.Dispose()
extern void U3CDisablePanelDeleyedU3Ed__10_System_IDisposable_Dispose_m37D3EA493F4986A2E0A6263B72E96EF00126946B (void);
// 0x000000BC System.Boolean PanelManager/<DisablePanelDeleyed>d__10::MoveNext()
extern void U3CDisablePanelDeleyedU3Ed__10_MoveNext_m7BFBF535CE4EBEABFFB4B592D5BA81B83152B2AD (void);
// 0x000000BD System.Object PanelManager/<DisablePanelDeleyed>d__10::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CDisablePanelDeleyedU3Ed__10_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m82D3317E9507246F91AABBA087291CE3DFAD9B4C (void);
// 0x000000BE System.Void PanelManager/<DisablePanelDeleyed>d__10::System.Collections.IEnumerator.Reset()
extern void U3CDisablePanelDeleyedU3Ed__10_System_Collections_IEnumerator_Reset_mCCE2A629729D15A72499C8B3EBE2380829AA7291 (void);
// 0x000000BF System.Object PanelManager/<DisablePanelDeleyed>d__10::System.Collections.IEnumerator.get_Current()
extern void U3CDisablePanelDeleyedU3Ed__10_System_Collections_IEnumerator_get_Current_m271F5CC65D4EAF362EEEB37323D10ABBE73D0554 (void);
// 0x000000C0 System.Void ResizePanel::Awake()
extern void ResizePanel_Awake_m02B9AC5CDBA67698A54F038360EBBE1A5358C034 (void);
// 0x000000C1 System.Void ResizePanel::OnPointerDown(UnityEngine.EventSystems.PointerEventData)
extern void ResizePanel_OnPointerDown_m37657BDB0494DD539375450934A44FDAAF3B9249 (void);
// 0x000000C2 System.Void ResizePanel::OnDrag(UnityEngine.EventSystems.PointerEventData)
extern void ResizePanel_OnDrag_mE54AEB640A9F9432EB5E64251BC10D2C20667A9E (void);
// 0x000000C3 System.Void ResizePanel::.ctor()
extern void ResizePanel__ctor_m12B4ECE6819037FC06C27BE7CBEF5B3112870FDB (void);
// 0x000000C4 System.Void ScrollDetailTexture::OnEnable()
extern void ScrollDetailTexture_OnEnable_m8B3390F8C092EA8409C0FA2617A5B130D6943DE1 (void);
// 0x000000C5 System.Void ScrollDetailTexture::OnDisable()
extern void ScrollDetailTexture_OnDisable_mC96EA86291FB8B4919FE11018EB8E7849311059F (void);
// 0x000000C6 System.Void ScrollDetailTexture::Update()
extern void ScrollDetailTexture_Update_m8B1F71303E9E2692FFED74C314E3183C40BDC2FF (void);
// 0x000000C7 System.Void ScrollDetailTexture::.ctor()
extern void ScrollDetailTexture__ctor_m82FA53B10261E10F9FC5A42CEBB20E3B4EA79EBE (void);
// 0x000000C8 System.Void ShowSliderValue::UpdateLabel(System.Single)
extern void ShowSliderValue_UpdateLabel_m6BF2232BF66791803BDFF15885544AA28F9DE766 (void);
// 0x000000C9 System.Void ShowSliderValue::.ctor()
extern void ShowSliderValue__ctor_m92B3943A578957A0E0036A16DC9024C213FAF38D (void);
// 0x000000CA System.Void StreamVideo::Start()
extern void StreamVideo_Start_m856EBA64997835A1514D498BA1F81F90E0D0C865 (void);
// 0x000000CB System.Collections.IEnumerator StreamVideo::playVideo()
extern void StreamVideo_playVideo_m665494A62132E352E3D9992154A5BA5A6263E0F1 (void);
// 0x000000CC System.Void StreamVideo::.ctor()
extern void StreamVideo__ctor_mE3613E1CD24FBE07CD46C88F6E03E29C7C796C18 (void);
// 0x000000CD System.Void StreamVideo/<playVideo>d__6::.ctor(System.Int32)
extern void U3CplayVideoU3Ed__6__ctor_mFDB672CF9D2AE25511238488FDCA4FCA83005F5A (void);
// 0x000000CE System.Void StreamVideo/<playVideo>d__6::System.IDisposable.Dispose()
extern void U3CplayVideoU3Ed__6_System_IDisposable_Dispose_mEA5585FE473EAA0C3887FDC55F7FFF74D648C338 (void);
// 0x000000CF System.Boolean StreamVideo/<playVideo>d__6::MoveNext()
extern void U3CplayVideoU3Ed__6_MoveNext_mAFE58DDB33CCAC21758665ED5EE7B601DED94262 (void);
// 0x000000D0 System.Object StreamVideo/<playVideo>d__6::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CplayVideoU3Ed__6_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mC32C8B8FE648421EC1D29C2DE18689FBCFEC4BD1 (void);
// 0x000000D1 System.Void StreamVideo/<playVideo>d__6::System.Collections.IEnumerator.Reset()
extern void U3CplayVideoU3Ed__6_System_Collections_IEnumerator_Reset_m1E6B4CB9B2DA5A4F5AF0E8B55458CF92037AEE9D (void);
// 0x000000D2 System.Object StreamVideo/<playVideo>d__6::System.Collections.IEnumerator.get_Current()
extern void U3CplayVideoU3Ed__6_System_Collections_IEnumerator_get_Current_m1ABB3BCE3C8DDA8C375F9A7B2CFE414851BF7D23 (void);
// 0x000000D3 System.Void TiltWindow::Start()
extern void TiltWindow_Start_mCDDE54F1278FA28BDEAB725B8283FF494D6D0ACC (void);
// 0x000000D4 System.Void TiltWindow::Update()
extern void TiltWindow_Update_mD571F534039FB9C6D19D024371F16CCDA3F2EAFA (void);
// 0x000000D5 System.Void TiltWindow::.ctor()
extern void TiltWindow__ctor_mEA9ECC1BB8D20A2225044D6F394A7AEBDFDBD47A (void);
// 0x000000D6 System.Void IconPreview::Awake()
extern void IconPreview_Awake_mD330471AA6E0259A3EE92FADE843189B6463D21E (void);
// 0x000000D7 System.Void IconPreview::Update()
extern void IconPreview_Update_mE7394435FCE9210899203B0724F7E2717868CF04 (void);
// 0x000000D8 System.Void IconPreview::.ctor()
extern void IconPreview__ctor_m6739A07D0628A74DB7748300B21999D113B0FDBE (void);
// 0x000000D9 System.Void ChatController::OnEnable()
extern void ChatController_OnEnable_mBC3BFC05A1D47069DFA58A4F0E39CFF8FAD44FF2 (void);
// 0x000000DA System.Void ChatController::OnDisable()
extern void ChatController_OnDisable_m268A7488A3FDEB850FC3BC91A0BBDA767D42876B (void);
// 0x000000DB System.Void ChatController::AddToChatOutput(System.String)
extern void ChatController_AddToChatOutput_m43856EEA133E04C24701A2616E7F10D7FFA68371 (void);
// 0x000000DC System.Void ChatController::.ctor()
extern void ChatController__ctor_m3B66A5F749B457D865E8BDA1DE481C8CF1158026 (void);
// 0x000000DD System.Void EnvMapAnimator::Awake()
extern void EnvMapAnimator_Awake_mFFC04BC5320F83CA8C45FF36A11BF43AC17C6B93 (void);
// 0x000000DE System.Collections.IEnumerator EnvMapAnimator::Start()
extern void EnvMapAnimator_Start_mC0D348CAB0F0DC920EF3D3A008688B533F66D1BE (void);
// 0x000000DF System.Void EnvMapAnimator::.ctor()
extern void EnvMapAnimator__ctor_mC151D673A394E2E7CEA8774C4004985028C5C3EC (void);
// 0x000000E0 System.Void EnvMapAnimator/<Start>d__4::.ctor(System.Int32)
extern void U3CStartU3Ed__4__ctor_m4D65F4FC2207AE4B6BE963AF9B5EDC55C7E29B23 (void);
// 0x000000E1 System.Void EnvMapAnimator/<Start>d__4::System.IDisposable.Dispose()
extern void U3CStartU3Ed__4_System_IDisposable_Dispose_mFEE2ACED70A3D825988E28CC61FEF8DCD7660A5B (void);
// 0x000000E2 System.Boolean EnvMapAnimator/<Start>d__4::MoveNext()
extern void U3CStartU3Ed__4_MoveNext_m2F1A8053A32AD86DA80F86391EC32EDC1C396AEE (void);
// 0x000000E3 System.Object EnvMapAnimator/<Start>d__4::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CStartU3Ed__4_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mF03E2A57659B8F598AC450183F55D43F903C0A1E (void);
// 0x000000E4 System.Void EnvMapAnimator/<Start>d__4::System.Collections.IEnumerator.Reset()
extern void U3CStartU3Ed__4_System_Collections_IEnumerator_Reset_m268BA538CF6812C56EB281C0CE29D5AA2E9A2CAB (void);
// 0x000000E5 System.Object EnvMapAnimator/<Start>d__4::System.Collections.IEnumerator.get_Current()
extern void U3CStartU3Ed__4_System_Collections_IEnumerator_get_Current_mFE9CA9F52F9AFA1E4C9DBADA2064F854D6931CFF (void);
// 0x000000E6 System.Collections.IEnumerator API_Webgl_CustomScript::Start()
extern void API_Webgl_CustomScript_Start_m992496291BD9EE4C71D6558ED0B5FD133650F37C (void);
// 0x000000E7 System.Void API_Webgl_CustomScript::Update()
extern void API_Webgl_CustomScript_Update_m2BA9B158877F6CBF505439F210F95A1D01A47808 (void);
// 0x000000E8 System.Boolean API_Webgl_CustomScript::HandleDetectionResult(UnityWebGLSpeechDetection.DetectionResult)
extern void API_Webgl_CustomScript_HandleDetectionResult_m00CA3C68F85F757E38B2E51F8C0FC89A022B174D (void);
// 0x000000E9 System.Void API_Webgl_CustomScript::.ctor()
extern void API_Webgl_CustomScript__ctor_mFE18591EAB468A9FC08AAE8D51AA5BD0C63FE240 (void);
// 0x000000EA System.Void API_Webgl_CustomScript/<Start>d__1::.ctor(System.Int32)
extern void U3CStartU3Ed__1__ctor_m5E90A5E31AFFDA9A00027F23432E83AD003F8F06 (void);
// 0x000000EB System.Void API_Webgl_CustomScript/<Start>d__1::System.IDisposable.Dispose()
extern void U3CStartU3Ed__1_System_IDisposable_Dispose_m0CCCE8D014E1F539B5CE96B2FA024CD9DAE4E52A (void);
// 0x000000EC System.Boolean API_Webgl_CustomScript/<Start>d__1::MoveNext()
extern void U3CStartU3Ed__1_MoveNext_m508623E249221F0110E563DA4222AD1013B6F755 (void);
// 0x000000ED System.Object API_Webgl_CustomScript/<Start>d__1::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CStartU3Ed__1_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mE32C41EA69F6F619888FF1E1C03D8BC2639C1D30 (void);
// 0x000000EE System.Void API_Webgl_CustomScript/<Start>d__1::System.Collections.IEnumerator.Reset()
extern void U3CStartU3Ed__1_System_Collections_IEnumerator_Reset_m790784997CB2EEB06544021FE7691779CB59D373 (void);
// 0x000000EF System.Object API_Webgl_CustomScript/<Start>d__1::System.Collections.IEnumerator.get_Current()
extern void U3CStartU3Ed__1_System_Collections_IEnumerator_get_Current_mFE4B07ABE524624161A7BAFFE8B6C2B3B2A9B8F3 (void);
// 0x000000F0 System.Void AlphaDictation::CreateText(System.String,UnityEngine.Color)
extern void AlphaDictation_CreateText_m46C4F0FFDAA30AC7E8B92D500EB5FFE5170F81E7 (void);
// 0x000000F1 System.Collections.IEnumerator AlphaDictation::CreateNameInputField()
extern void AlphaDictation_CreateNameInputField_m7AC3A3048D731EC81C9E6EDB7A937701691E4BB0 (void);
// 0x000000F2 System.Collections.IEnumerator AlphaDictation::CreateTalkInputField()
extern void AlphaDictation_CreateTalkInputField_m212356FCFBE4942C8C91B9085FFC3DD655B1700A (void);
// 0x000000F3 System.Void AlphaDictation::CreateTextAndSpeak(System.String)
extern void AlphaDictation_CreateTextAndSpeak_m523F7F9250A8652685FB2DDA7BC985A9873B48C3 (void);
// 0x000000F4 System.Collections.IEnumerator AlphaDictation::Start()
extern void AlphaDictation_Start_mE73F0C092900182FAEAB2154C77FA9B969076BD4 (void);
// 0x000000F5 System.Boolean AlphaDictation::HandleDetectionResult(UnityWebGLSpeechDetection.DetectionResult)
extern void AlphaDictation_HandleDetectionResult_mC6E2E77A757423D220C9358275F6B2E6ED4D8252 (void);
// 0x000000F6 System.Void AlphaDictation::HandleSynthesisOnEnd(UnityWebGLSpeechSynthesis.SpeechSynthesisEvent)
extern void AlphaDictation_HandleSynthesisOnEnd_mA834E1DC2E59E56945999805AD56E94366BF21DB (void);
// 0x000000F7 System.Collections.IEnumerator AlphaDictation::GetVoices()
extern void AlphaDictation_GetVoices_mABA61640ADF4F82FCDC628FD7BEEAD5DDF980132 (void);
// 0x000000F8 System.Void AlphaDictation::FixedUpdate()
extern void AlphaDictation_FixedUpdate_mB8B21C223460615C2107C4C5200540AA2739710D (void);
// 0x000000F9 System.Void AlphaDictation::Speak(System.String)
extern void AlphaDictation_Speak_mC4290CA03C4DC651005EB265A22545C5AE0D4B51 (void);
// 0x000000FA System.Void AlphaDictation::.ctor()
extern void AlphaDictation__ctor_m713E7930D50FDF1FA2ABEA40B76693C867465EDA (void);
// 0x000000FB System.Void AlphaDictation::<Start>b__21_0()
extern void AlphaDictation_U3CStartU3Eb__21_0_mA7498778E41A70AC06A961C5F9C1F899DBE77578 (void);
// 0x000000FC System.Void AlphaDictation::<Start>b__21_1()
extern void AlphaDictation_U3CStartU3Eb__21_1_m64B2BD3408CA0A3B2952E02DE572BC79487C16CB (void);
// 0x000000FD System.Void AlphaDictation::<Start>b__21_2(UnityWebGLSpeechSynthesis.SpeechSynthesisUtterance)
extern void AlphaDictation_U3CStartU3Eb__21_2_mC4886CB5A7ABF8351AA5EAA455A504AD4E8A7C2B (void);
// 0x000000FE System.Void AlphaDictation::<GetVoices>b__24_0(UnityWebGLSpeechSynthesis.VoiceResult)
extern void AlphaDictation_U3CGetVoicesU3Eb__24_0_mB54B6275ADFCC20BD44B8B91DB44B766BA086B92 (void);
// 0x000000FF System.Void AlphaDictation/<CreateNameInputField>d__18::.ctor(System.Int32)
extern void U3CCreateNameInputFieldU3Ed__18__ctor_m16DD53F38890499364303711C09C1C5214EEDD70 (void);
// 0x00000100 System.Void AlphaDictation/<CreateNameInputField>d__18::System.IDisposable.Dispose()
extern void U3CCreateNameInputFieldU3Ed__18_System_IDisposable_Dispose_m0361E7C9EDBF31EB17C87B8B7D2D5D8372BF4EF7 (void);
// 0x00000101 System.Boolean AlphaDictation/<CreateNameInputField>d__18::MoveNext()
extern void U3CCreateNameInputFieldU3Ed__18_MoveNext_mEF5B0F14C068D7406691FDE9FD44A352D5263906 (void);
// 0x00000102 System.Object AlphaDictation/<CreateNameInputField>d__18::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CCreateNameInputFieldU3Ed__18_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mD961FEBA5850489423C74E8A9ECF65799EDABA19 (void);
// 0x00000103 System.Void AlphaDictation/<CreateNameInputField>d__18::System.Collections.IEnumerator.Reset()
extern void U3CCreateNameInputFieldU3Ed__18_System_Collections_IEnumerator_Reset_mF531741BA7DF17D9D80B7D368C54765DC2DDD36A (void);
// 0x00000104 System.Object AlphaDictation/<CreateNameInputField>d__18::System.Collections.IEnumerator.get_Current()
extern void U3CCreateNameInputFieldU3Ed__18_System_Collections_IEnumerator_get_Current_mC900B40596EAC5FB8A8E6D8F97C7DE06C32769EF (void);
// 0x00000105 System.Void AlphaDictation/<CreateTalkInputField>d__19::.ctor(System.Int32)
extern void U3CCreateTalkInputFieldU3Ed__19__ctor_m7EF2DFD1A550B11488B93EAF9CCBDEDDBBD5070F (void);
// 0x00000106 System.Void AlphaDictation/<CreateTalkInputField>d__19::System.IDisposable.Dispose()
extern void U3CCreateTalkInputFieldU3Ed__19_System_IDisposable_Dispose_mA785F12D160C5605B61C8E05F42FF2DEE32FD5D3 (void);
// 0x00000107 System.Boolean AlphaDictation/<CreateTalkInputField>d__19::MoveNext()
extern void U3CCreateTalkInputFieldU3Ed__19_MoveNext_m99F33A207FEBB6E605F2CABA6E1F0EAE35F722F2 (void);
// 0x00000108 System.Object AlphaDictation/<CreateTalkInputField>d__19::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CCreateTalkInputFieldU3Ed__19_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m3D9B40DD836926B602CA3956E729D17831707F2E (void);
// 0x00000109 System.Void AlphaDictation/<CreateTalkInputField>d__19::System.Collections.IEnumerator.Reset()
extern void U3CCreateTalkInputFieldU3Ed__19_System_Collections_IEnumerator_Reset_m6030C1BC4160045FECCE7404FB1258B3326A4EFE (void);
// 0x0000010A System.Object AlphaDictation/<CreateTalkInputField>d__19::System.Collections.IEnumerator.get_Current()
extern void U3CCreateTalkInputFieldU3Ed__19_System_Collections_IEnumerator_get_Current_m8E8A6F01565CDD65153C8335CC73EBA5F83050E9 (void);
// 0x0000010B System.Void AlphaDictation/<Start>d__21::.ctor(System.Int32)
extern void U3CStartU3Ed__21__ctor_m26D519F01F14E6D96913DE072F29AB761206406A (void);
// 0x0000010C System.Void AlphaDictation/<Start>d__21::System.IDisposable.Dispose()
extern void U3CStartU3Ed__21_System_IDisposable_Dispose_m83494FD180C6AB29D8ADD11E43F3BFF286086BB6 (void);
// 0x0000010D System.Boolean AlphaDictation/<Start>d__21::MoveNext()
extern void U3CStartU3Ed__21_MoveNext_m7F80D98E4B5DED0B33D0A9C1750DBAA690325D5F (void);
// 0x0000010E System.Object AlphaDictation/<Start>d__21::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CStartU3Ed__21_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mB9FD60E1B238D04F2053FBB9F7D909F7F0407903 (void);
// 0x0000010F System.Void AlphaDictation/<Start>d__21::System.Collections.IEnumerator.Reset()
extern void U3CStartU3Ed__21_System_Collections_IEnumerator_Reset_mB8E6F4D18EBDD4308E0EC58B5857A9E2DD150002 (void);
// 0x00000110 System.Object AlphaDictation/<Start>d__21::System.Collections.IEnumerator.get_Current()
extern void U3CStartU3Ed__21_System_Collections_IEnumerator_get_Current_mAA0AC8FE81AE618E6E533FFFB3A3EB512247C5D7 (void);
// 0x00000111 System.Void AlphaDictation/<GetVoices>d__24::.ctor(System.Int32)
extern void U3CGetVoicesU3Ed__24__ctor_m6EC67E3F6B6F579CFBFA5D72A871657B6E041DF7 (void);
// 0x00000112 System.Void AlphaDictation/<GetVoices>d__24::System.IDisposable.Dispose()
extern void U3CGetVoicesU3Ed__24_System_IDisposable_Dispose_m2D86E52EE2167BF97EBFD117EB53F27F9B14FA7D (void);
// 0x00000113 System.Boolean AlphaDictation/<GetVoices>d__24::MoveNext()
extern void U3CGetVoicesU3Ed__24_MoveNext_mC85ABFF0235E61E1014895917343284664C70818 (void);
// 0x00000114 System.Object AlphaDictation/<GetVoices>d__24::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CGetVoicesU3Ed__24_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m2ADDAD12E3D1D614D1896E530F3D1AB0822FF876 (void);
// 0x00000115 System.Void AlphaDictation/<GetVoices>d__24::System.Collections.IEnumerator.Reset()
extern void U3CGetVoicesU3Ed__24_System_Collections_IEnumerator_Reset_mD951BF2ADA17869A6F1798A98F277F1254A1AFD7 (void);
// 0x00000116 System.Object AlphaDictation/<GetVoices>d__24::System.Collections.IEnumerator.get_Current()
extern void U3CGetVoicesU3Ed__24_System_Collections_IEnumerator_get_Current_m534DD093F2D41F07001B5D87DC34CA74D7446D81 (void);
// 0x00000117 System.Threading.Tasks.Task AsyncTest::Start()
extern void AsyncTest_Start_m2907A9617EA115AB97EE9D38B5A418C255D9E244 (void);
// 0x00000118 System.Void AsyncTest::OnRequestFinished(BestHTTP.HTTPRequest,BestHTTP.HTTPResponse)
extern void AsyncTest_OnRequestFinished_m71140E44A0FB04D9B8134425ABA355EAED483AB9 (void);
// 0x00000119 System.Void AsyncTest::chiamata()
extern void AsyncTest_chiamata_mD2A738D13AF489CDA32EAC0FE95F1DCFA3A557F9 (void);
// 0x0000011A System.Void AsyncTest::.ctor()
extern void AsyncTest__ctor_mF169445CD9FF6C4DFEFB8F0916CC70249577D1CB (void);
// 0x0000011B System.Threading.Tasks.Task AsyncTest::<chiamata>b__2_0()
extern void AsyncTest_U3CchiamataU3Eb__2_0_m08D9EA2FBFB5A5593F9C6CF3615B51497563B2AF (void);
// 0x0000011C System.Threading.Tasks.Task AsyncTest::<chiamata>b__2_1()
extern void AsyncTest_U3CchiamataU3Eb__2_1_m82F6E12D3D29F080A8F6417994BB79A23E428EFA (void);
// 0x0000011D System.Void AsyncTest/<Start>d__0::MoveNext()
extern void U3CStartU3Ed__0_MoveNext_mBDA40DCEF83D7125F6056B9FEDBFAC0514D18D79 (void);
// 0x0000011E System.Void AsyncTest/<Start>d__0::SetStateMachine(System.Runtime.CompilerServices.IAsyncStateMachine)
extern void U3CStartU3Ed__0_SetStateMachine_m13EE83714ABD4D5D9AB518554FDFA48D6E3D5B3D (void);
// 0x0000011F System.Collections.IEnumerator AudioWebDownload::Start()
extern void AudioWebDownload_Start_m5C68F3BB98AC3F3DC750CCD1782FC55699B1FE00 (void);
// 0x00000120 System.Void AudioWebDownload::playAudio()
extern void AudioWebDownload_playAudio_mF23FEFDEBF16708B9308AAE47EAC88CA49440978 (void);
// 0x00000121 System.Void AudioWebDownload::Play()
extern void AudioWebDownload_Play_m1F8AA0EC4D598F7EE4D9306655A9344DF8998D15 (void);
// 0x00000122 System.Void AudioWebDownload::.ctor()
extern void AudioWebDownload__ctor_m021D371A9D02D77FFFB0D510C5DAFEACB446594B (void);
// 0x00000123 System.Void AudioWebDownload/<Start>d__4::.ctor(System.Int32)
extern void U3CStartU3Ed__4__ctor_m91EA2977411F2D5F1F38F22A96463D6922ADC6A9 (void);
// 0x00000124 System.Void AudioWebDownload/<Start>d__4::System.IDisposable.Dispose()
extern void U3CStartU3Ed__4_System_IDisposable_Dispose_m6068A7F795E708511DCA473A8774621B114C08BB (void);
// 0x00000125 System.Boolean AudioWebDownload/<Start>d__4::MoveNext()
extern void U3CStartU3Ed__4_MoveNext_m5347AD2C086F6401688826A7E66C111BDD0365F6 (void);
// 0x00000126 System.Void AudioWebDownload/<Start>d__4::<>m__Finally1()
extern void U3CStartU3Ed__4_U3CU3Em__Finally1_mA5B1FCF05C223A6056AAEDC45D59ADD6BD1B8AA8 (void);
// 0x00000127 System.Object AudioWebDownload/<Start>d__4::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CStartU3Ed__4_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mE29825A2EEC44D0C234AA720FF5AF59EEC4CCEFD (void);
// 0x00000128 System.Void AudioWebDownload/<Start>d__4::System.Collections.IEnumerator.Reset()
extern void U3CStartU3Ed__4_System_Collections_IEnumerator_Reset_m8D91B10117F29DBCE66FB0AB79F45716B5B3A852 (void);
// 0x00000129 System.Object AudioWebDownload/<Start>d__4::System.Collections.IEnumerator.get_Current()
extern void U3CStartU3Ed__4_System_Collections_IEnumerator_get_Current_m31F531ABC8E70793483ADE592ADFCA9BC7B32F6A (void);
// 0x0000012A DataContainer DataContainer::Load(System.String)
extern void DataContainer_Load_m0884A3D313193B52821F89E2030774753E15A52F (void);
// 0x0000012B System.Void DataContainer::.ctor()
extern void DataContainer__ctor_m26CBEA5E0F69B8BB6AB9132B38BB8C0EC088985C (void);
// 0x0000012C System.Void Emotification::Start()
extern void Emotification_Start_m467B40A90243A74151BFA1F1A8B9E6ADA35BA4B8 (void);
// 0x0000012D System.Void Emotification::Update()
extern void Emotification_Update_mBF64972B9C1FFEAF8676EB4E2D4058DD90BE8B90 (void);
// 0x0000012E System.Void Emotification::.ctor()
extern void Emotification__ctor_mEE7AE1D9B8031C2446B78BE5A1FDD317E216C287 (void);
// 0x0000012F System.Void ExecuteCommandTool::Start()
extern void ExecuteCommandTool_Start_mD24F530E0F93CDEC1E6D0DAAA1FAD80BD5BB381F (void);
// 0x00000130 System.Void ExecuteCommandTool::ExecuteCommand(System.String)
extern void ExecuteCommandTool_ExecuteCommand_m9C048A35B986A440672EBDF2EF469AFE7545184F (void);
// 0x00000131 System.Void ExecuteCommandTool::Command(System.String)
extern void ExecuteCommandTool_Command_m4FB509EDB60381862DAA3FD0C4703C0B12A11809 (void);
// 0x00000132 System.Void ExecuteCommandTool::calcola()
extern void ExecuteCommandTool_calcola_m7BA14E1784C884230AB17B40C9150335012C560E (void);
// 0x00000133 System.Void ExecuteCommandTool::browser()
extern void ExecuteCommandTool_browser_m6253EC25E1572198449CA967ED3793C862684DEB (void);
// 0x00000134 System.Void ExecuteCommandTool::miometod()
extern void ExecuteCommandTool_miometod_mCAA2C1E31E7138D465C3265780ED7636A2F777DF (void);
// 0x00000135 System.Void ExecuteCommandTool::batch()
extern void ExecuteCommandTool_batch_m3FE965B114A3F6305270B4BE27AE8F40F735C52A (void);
// 0x00000136 System.Void ExecuteCommandTool::RunFile()
extern void ExecuteCommandTool_RunFile_m221F4A9EBE910ADBAE3700ECADB8EC32B43814C6 (void);
// 0x00000137 System.Void ExecuteCommandTool::batchWav2lip()
extern void ExecuteCommandTool_batchWav2lip_m179062873C8FE7CCA191B410322EFB276EFEE0D6 (void);
// 0x00000138 System.Void ExecuteCommandTool::trasnfer2server()
extern void ExecuteCommandTool_trasnfer2server_m6ED2EA83F57B22FCDB268DDF44A5DDB786356DB0 (void);
// 0x00000139 System.Void ExecuteCommandTool::server2unity()
extern void ExecuteCommandTool_server2unity_m31A0F987DCD427DE6B13EB4F2C5DC612A0379C80 (void);
// 0x0000013A System.Void ExecuteCommandTool::.ctor()
extern void ExecuteCommandTool__ctor_mD6318799D6E4877E4460870BFCA1122144361547 (void);
// 0x0000013B System.Void ExecuteCommandTool/<>c__DisplayClass8_0::.ctor()
extern void U3CU3Ec__DisplayClass8_0__ctor_mEC30FB793086E401C4330816CB76F33E3CEEFD96 (void);
// 0x0000013C System.Void ExecuteCommandTool/<>c__DisplayClass8_0::<ExecuteCommand>b__0()
extern void U3CU3Ec__DisplayClass8_0_U3CExecuteCommandU3Eb__0_m70E0A94262DF0551A399396D9B9E946453F40E51 (void);
// 0x0000013D System.Void Loader::Start()
extern void Loader_Start_m31D4A12C74578B3A6BDC94F52BAEAF3639A71D1C (void);
// 0x0000013E System.Void Loader::.ctor()
extern void Loader__ctor_m15D78DFE09D5A9C562B7E58B0D4C392427D1E674 (void);
// 0x0000013F ProductConfig ProductConfig::get_Instance()
extern void ProductConfig_get_Instance_mEBB02C79BF1291144EC952F122635CDDC2EAE0AA (void);
// 0x00000140 System.Void ProductConfig::add_onAppConfigured(ProductConfig/AppConfigured)
extern void ProductConfig_add_onAppConfigured_m225A66D36308969E5527B66D8A87BA1F478448CF (void);
// 0x00000141 System.Void ProductConfig::remove_onAppConfigured(ProductConfig/AppConfigured)
extern void ProductConfig_remove_onAppConfigured_m5ED96BA9E7C9F8E0F0642F5E1C8272D65B7EEF74 (void);
// 0x00000142 System.Void ProductConfig::Awake()
extern void ProductConfig_Awake_mAD7E981E1889AF56A52539476628DB74C200C537 (void);
// 0x00000143 System.Void ProductConfig::ProductConfig_onAppConfigured(ConfigDataClass)
extern void ProductConfig_ProductConfig_onAppConfigured_mBD8B08EB8CE5B3BD8CFA4753C484D23472D3E656 (void);
// 0x00000144 System.Void ProductConfig::OnDestroy()
extern void ProductConfig_OnDestroy_m6CECCF21C6DC81F5E8718EF3E53C08F2C653CE6B (void);
// 0x00000145 System.Void ProductConfig::Start()
extern void ProductConfig_Start_mEB3078D001663BF597FE8628917800D4F631B3DE (void);
// 0x00000146 ConfigDataClass ProductConfig::ReadPrefsJson()
extern void ProductConfig_ReadPrefsJson_m058F9FDB22B8999DE2D1FA247770F5E3AE58DF06 (void);
// 0x00000147 ConfigDataClass ProductConfig::ReadEngineJson(ProductConfig/JsonType)
extern void ProductConfig_ReadEngineJson_m0387D0BF66D8059E62D46F51E8650EF00E202C55 (void);
// 0x00000148 System.Void ProductConfig::.ctor()
extern void ProductConfig__ctor_m7E47D39B606AA615DA54A0A677969E052CDBAC08 (void);
// 0x00000149 System.Void ProductConfig/AppConfigured::.ctor(System.Object,System.IntPtr)
extern void AppConfigured__ctor_m4AA784B7528682370CC3B861F796B7F4C7E1FE88 (void);
// 0x0000014A System.Void ProductConfig/AppConfigured::Invoke(ConfigDataClass)
extern void AppConfigured_Invoke_m401451E66161F97C89123DEB8CEBC6D4759F5F12 (void);
// 0x0000014B System.IAsyncResult ProductConfig/AppConfigured::BeginInvoke(ConfigDataClass,System.AsyncCallback,System.Object)
extern void AppConfigured_BeginInvoke_m14583E82793A869D64AA503CB66CAF33981DD6E8 (void);
// 0x0000014C System.Void ProductConfig/AppConfigured::EndInvoke(System.IAsyncResult)
extern void AppConfigured_EndInvoke_m99CB9889EABA1D324E3646BE642EF15302E65DC2 (void);
// 0x0000014D System.Void ConfigDataClass::.ctor()
extern void ConfigDataClass__ctor_m1C9E04789516FEEF3BD42A5E22F9C58179CA9202 (void);
// 0x0000014E System.String VampJson::get_data()
extern void VampJson_get_data_m8144B4629CB7511325960930B6FC1E1704B34279 (void);
// 0x0000014F System.Void VampJson::set_data(System.String)
extern void VampJson_set_data_m324D78FD2D84110350689B0D420658A842FD37E3 (void);
// 0x00000150 System.Void VampJson::.ctor()
extern void VampJson__ctor_m63821C612E5F5FB5707EF789D7C8882E6A84755F (void);
// 0x00000151 System.Void ResponseMessage::.ctor(System.String)
extern void ResponseMessage__ctor_m323FBDA168075443A9A5D23022FA7BD8D2C415BD (void);
// 0x00000152 System.String ResponseMessage/VampJson::get_data()
extern void VampJson_get_data_mC83BA77A1EDE4B5D73280878433071E94FB4E8DF (void);
// 0x00000153 System.Void ResponseMessage/VampJson::set_data(System.String)
extern void VampJson_set_data_mD9931EC77A8E455CFD4431703C74E962F2B4CDB8 (void);
// 0x00000154 System.Void ResponseMessage/VampJson::.ctor()
extern void VampJson__ctor_m100A27BC9E36685BBCC44BC4C294A70716579FCC (void);
// 0x00000155 System.Void ServerGET::Start()
extern void ServerGET_Start_m214FD6BA943E096C665081C97E3A35F0176EAF27 (void);
// 0x00000156 System.Void ServerGET::mandaTellme()
extern void ServerGET_mandaTellme_m7CFA9D14C649AC6EC533CBE87DC14A802567F65C (void);
// 0x00000157 System.Void ServerGET::mandaStart()
extern void ServerGET_mandaStart_mDB49AFB0A02556E80B024A685096389FFA73FB98 (void);
// 0x00000158 System.Collections.IEnumerator ServerGET::sendit()
extern void ServerGET_sendit_m1EA29AAF88B44756F00A3B7F35EFE305996ACDB2 (void);
// 0x00000159 System.Collections.IEnumerator ServerGET::sendStart()
extern void ServerGET_sendStart_mB210B583791D836A00986A0A8C3F6725D854B9E6 (void);
// 0x0000015A System.Void ServerGET::OnRequestFinished(BestHTTP.HTTPRequest,BestHTTP.HTTPResponse)
extern void ServerGET_OnRequestFinished_mBE5B5E12989EB93DDCE356EFEC9F9103867E874C (void);
// 0x0000015B System.Void ServerGET::.ctor()
extern void ServerGET__ctor_m64078CD815F5B8E107D4AE2CDE13FB3D8B2FBEB9 (void);
// 0x0000015C System.Void ServerGET::<mandaTellme>b__2_0()
extern void ServerGET_U3CmandaTellmeU3Eb__2_0_mA36FA1317E057FFF14B768B4780362CBF2833EF3 (void);
// 0x0000015D System.Void ServerGET::<mandaStart>b__3_0()
extern void ServerGET_U3CmandaStartU3Eb__3_0_m1C16D722E0FDD4F011F761BC580A4CC0C0EF7D6A (void);
// 0x0000015E System.Void ServerGET/<sendit>d__4::.ctor(System.Int32)
extern void U3CsenditU3Ed__4__ctor_mE07C479F2D6D08CEE765BC0A16075B269195DE04 (void);
// 0x0000015F System.Void ServerGET/<sendit>d__4::System.IDisposable.Dispose()
extern void U3CsenditU3Ed__4_System_IDisposable_Dispose_mEAB91ACF97C985B4818468F0570A6BB1B2D2CAEE (void);
// 0x00000160 System.Boolean ServerGET/<sendit>d__4::MoveNext()
extern void U3CsenditU3Ed__4_MoveNext_mF74C75DD4E4185EDBA7BEBEE0C13C1924F55DFEF (void);
// 0x00000161 System.Object ServerGET/<sendit>d__4::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CsenditU3Ed__4_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m0075B1BB7202B48EAF8DF68BD1E75E7479DF09DE (void);
// 0x00000162 System.Void ServerGET/<sendit>d__4::System.Collections.IEnumerator.Reset()
extern void U3CsenditU3Ed__4_System_Collections_IEnumerator_Reset_m3C35FCF6F7379FB5FE4D93030DFAA521BCF53796 (void);
// 0x00000163 System.Object ServerGET/<sendit>d__4::System.Collections.IEnumerator.get_Current()
extern void U3CsenditU3Ed__4_System_Collections_IEnumerator_get_Current_m2418856C8CBC200779218B14301B6585C7E81BB8 (void);
// 0x00000164 System.Void ServerGET/<sendStart>d__5::.ctor(System.Int32)
extern void U3CsendStartU3Ed__5__ctor_mD6894369E0CB939F1F4BF9E740CD24EDC91BAA97 (void);
// 0x00000165 System.Void ServerGET/<sendStart>d__5::System.IDisposable.Dispose()
extern void U3CsendStartU3Ed__5_System_IDisposable_Dispose_m12FED3311D45913F73A5B237F6A82857641407A9 (void);
// 0x00000166 System.Boolean ServerGET/<sendStart>d__5::MoveNext()
extern void U3CsendStartU3Ed__5_MoveNext_m3A2EE043DCDEFEBAE3ED357E8C1A91352AC0C774 (void);
// 0x00000167 System.Object ServerGET/<sendStart>d__5::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CsendStartU3Ed__5_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m98D1A599E6148119D2BEC23D1B565C3684A94E98 (void);
// 0x00000168 System.Void ServerGET/<sendStart>d__5::System.Collections.IEnumerator.Reset()
extern void U3CsendStartU3Ed__5_System_Collections_IEnumerator_Reset_mD881B26362ABCA5EADB3A324451DF16766FF345A (void);
// 0x00000169 System.Object ServerGET/<sendStart>d__5::System.Collections.IEnumerator.get_Current()
extern void U3CsendStartU3Ed__5_System_Collections_IEnumerator_get_Current_m8E257796E3411938AA72D419B4B6965E8AA3A78D (void);
// 0x0000016A System.Void SessionLog::WriteToLogFile(System.String)
extern void SessionLog_WriteToLogFile_m625DCAD01A9E75512ABDB79895F03EB48E008ACE (void);
// 0x0000016B System.Void SessionLog::.ctor()
extern void SessionLog__ctor_m8427FF253053ACA1AF3519E7E73C929335F8E292 (void);
// 0x0000016C System.Void SpeechDetection::Awake()
extern void SpeechDetection_Awake_m0A80DC2A0BEAD54DB64F1834C7D41EF1798087DC (void);
// 0x0000016D System.Collections.IEnumerator SpeechDetection::Start()
extern void SpeechDetection_Start_m964CB86ED3FB5503009540A9FCAF2C1BC58EC52D (void);
// 0x0000016E System.Net.WebSockets.WebSocket SpeechDetection::WebSocket(System.String)
extern void SpeechDetection_WebSocket_m1196E409C1FECF0EC616386C809AEEC3DEB00640 (void);
// 0x0000016F System.Void SpeechDetection::EnableDetectionHandle(System.Boolean)
extern void SpeechDetection_EnableDetectionHandle_m49950EB1CE75DF37E2CA956C5A6321C867A2D154 (void);
// 0x00000170 System.Collections.IEnumerator SpeechDetection::Interrompi()
extern void SpeechDetection_Interrompi_m8B8B8998BAA77C96F91DC39A22CD61DF9D0E597A (void);
// 0x00000171 System.Void SpeechDetection::Update()
extern void SpeechDetection_Update_mDDD932189D9A2D28CDFC3DEA2A7A5BEC67B2CAA6 (void);
// 0x00000172 System.Collections.IEnumerator SpeechDetection::WaitDetecte()
extern void SpeechDetection_WaitDetecte_mD06C349890C0CA117F3B5EC2E65DE5F8081BF0C5 (void);
// 0x00000173 System.Collections.IEnumerator SpeechDetection::delay(System.Int32)
extern void SpeechDetection_delay_m183FFB8CEE5DDD89082F2CE0C2A4D2DB1DC65368 (void);
// 0x00000174 System.Collections.IEnumerator SpeechDetection::delayAnswer(System.Int32)
extern void SpeechDetection_delayAnswer_m843ACAC286411C73DB04E624B52DF75E524E5376 (void);
// 0x00000175 System.Boolean SpeechDetection::HandleDetectionResult(UnityWebGLSpeechDetection.DetectionResult)
extern void SpeechDetection_HandleDetectionResult_m8F9D847517E729BE94E47CC28D2D988C89603B98 (void);
// 0x00000176 System.Void SpeechDetection::settaLingua(System.Int32)
extern void SpeechDetection_settaLingua_mD6794B022337169B42AEE0C7C9206AF425E3D8CA (void);
// 0x00000177 System.Void SpeechDetection::.ctor()
extern void SpeechDetection__ctor_m6A5EDF54B97BF4FEFA63A4CDD84D38520291E979 (void);
// 0x00000178 System.Void SpeechDetection::<Start>b__15_0(UnityWebGLSpeechDetection.LanguageResult)
extern void SpeechDetection_U3CStartU3Eb__15_0_m85EB3162E958E0310FFD0E9793D32D5EA273A9CF (void);
// 0x00000179 System.Void SpeechDetection::<Start>b__15_1(System.Int32)
extern void SpeechDetection_U3CStartU3Eb__15_1_m8B33085CD6325B0D6AAB6D98724D7695B2737933 (void);
// 0x0000017A System.Void SpeechDetection::<Start>b__15_2(System.Int32)
extern void SpeechDetection_U3CStartU3Eb__15_2_mDB40EC83F38DDEEEA4EA54ABEC0D7C5C859DBF98 (void);
// 0x0000017B System.Void SpeechDetection/<Start>d__15::.ctor(System.Int32)
extern void U3CStartU3Ed__15__ctor_mC9AE0236D7B588698073D4B89CB654641A6DB0AE (void);
// 0x0000017C System.Void SpeechDetection/<Start>d__15::System.IDisposable.Dispose()
extern void U3CStartU3Ed__15_System_IDisposable_Dispose_m6ABD141C75095FBC2065E255D6C4448AC3060984 (void);
// 0x0000017D System.Boolean SpeechDetection/<Start>d__15::MoveNext()
extern void U3CStartU3Ed__15_MoveNext_m9D44B4F04AD99F4842E24B2A0812078B1898FF9D (void);
// 0x0000017E System.Object SpeechDetection/<Start>d__15::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CStartU3Ed__15_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mCE9755FDED93AE39AAA993068440542D54FFA2C7 (void);
// 0x0000017F System.Void SpeechDetection/<Start>d__15::System.Collections.IEnumerator.Reset()
extern void U3CStartU3Ed__15_System_Collections_IEnumerator_Reset_m065FA6FC5BA16F1A873BF95F9F9A535B2E997D5E (void);
// 0x00000180 System.Object SpeechDetection/<Start>d__15::System.Collections.IEnumerator.get_Current()
extern void U3CStartU3Ed__15_System_Collections_IEnumerator_get_Current_m9079006723B5335C044B979CBEB474EF6BD19C84 (void);
// 0x00000181 System.Void SpeechDetection/<Interrompi>d__18::.ctor(System.Int32)
extern void U3CInterrompiU3Ed__18__ctor_mC4BF20D4B4F0924F4316A332EA064658ECDF3B8F (void);
// 0x00000182 System.Void SpeechDetection/<Interrompi>d__18::System.IDisposable.Dispose()
extern void U3CInterrompiU3Ed__18_System_IDisposable_Dispose_m1E8E4915DAD9852F47258A33747876E6D9363E5A (void);
// 0x00000183 System.Boolean SpeechDetection/<Interrompi>d__18::MoveNext()
extern void U3CInterrompiU3Ed__18_MoveNext_m5D306ED87C95B41F64362772D1058825769C2776 (void);
// 0x00000184 System.Object SpeechDetection/<Interrompi>d__18::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CInterrompiU3Ed__18_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m3CB514C370F4075CED65FAA6F8713C5881C501A7 (void);
// 0x00000185 System.Void SpeechDetection/<Interrompi>d__18::System.Collections.IEnumerator.Reset()
extern void U3CInterrompiU3Ed__18_System_Collections_IEnumerator_Reset_mB1124F4AFA3CBBE56267B4AE3B3E6FE1E17B8903 (void);
// 0x00000186 System.Object SpeechDetection/<Interrompi>d__18::System.Collections.IEnumerator.get_Current()
extern void U3CInterrompiU3Ed__18_System_Collections_IEnumerator_get_Current_m583EF39D69E9F1241AE2B86CECA06D88FF507673 (void);
// 0x00000187 System.Void SpeechDetection/<WaitDetecte>d__20::.ctor(System.Int32)
extern void U3CWaitDetecteU3Ed__20__ctor_m1A67F7878DA418826DFF5D95A0BFBB2574D910C4 (void);
// 0x00000188 System.Void SpeechDetection/<WaitDetecte>d__20::System.IDisposable.Dispose()
extern void U3CWaitDetecteU3Ed__20_System_IDisposable_Dispose_m43C5ACD018DA8ABBA17B6FE01CEA7F2EF50358E5 (void);
// 0x00000189 System.Boolean SpeechDetection/<WaitDetecte>d__20::MoveNext()
extern void U3CWaitDetecteU3Ed__20_MoveNext_m2B1BE065562C52649D6794675B21335F65BED889 (void);
// 0x0000018A System.Object SpeechDetection/<WaitDetecte>d__20::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CWaitDetecteU3Ed__20_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m6ACF176E8490275E029219E9FD34C0E7E840023C (void);
// 0x0000018B System.Void SpeechDetection/<WaitDetecte>d__20::System.Collections.IEnumerator.Reset()
extern void U3CWaitDetecteU3Ed__20_System_Collections_IEnumerator_Reset_m19BCEE584D5A53FE393B353AD316C26394B15AA3 (void);
// 0x0000018C System.Object SpeechDetection/<WaitDetecte>d__20::System.Collections.IEnumerator.get_Current()
extern void U3CWaitDetecteU3Ed__20_System_Collections_IEnumerator_get_Current_m0257432B6E2C13570BA9D06616B0047DF5427830 (void);
// 0x0000018D System.Void SpeechDetection/<delay>d__21::.ctor(System.Int32)
extern void U3CdelayU3Ed__21__ctor_m4972F51E4B93A2627E1963D38EBB5678F80CC498 (void);
// 0x0000018E System.Void SpeechDetection/<delay>d__21::System.IDisposable.Dispose()
extern void U3CdelayU3Ed__21_System_IDisposable_Dispose_m887E96F8864511CBDF1F1F49312EB454D89CFBF4 (void);
// 0x0000018F System.Boolean SpeechDetection/<delay>d__21::MoveNext()
extern void U3CdelayU3Ed__21_MoveNext_m07CA5215F58141DB89E06467826DDEE03A5EC1AC (void);
// 0x00000190 System.Object SpeechDetection/<delay>d__21::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CdelayU3Ed__21_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m886749BC4D4F2A41AE65E71E9C803BC67CFE1F9C (void);
// 0x00000191 System.Void SpeechDetection/<delay>d__21::System.Collections.IEnumerator.Reset()
extern void U3CdelayU3Ed__21_System_Collections_IEnumerator_Reset_m3255BDF07E7B37EB9F97B3DA74FF5E650FA89F2B (void);
// 0x00000192 System.Object SpeechDetection/<delay>d__21::System.Collections.IEnumerator.get_Current()
extern void U3CdelayU3Ed__21_System_Collections_IEnumerator_get_Current_m16CB6970AAF99BF95E44CBF1B101CC1BFF577454 (void);
// 0x00000193 System.Void SpeechDetection/<delayAnswer>d__22::.ctor(System.Int32)
extern void U3CdelayAnswerU3Ed__22__ctor_mA746C78FB7F20405F54BE83796272BA0F8C1CEF9 (void);
// 0x00000194 System.Void SpeechDetection/<delayAnswer>d__22::System.IDisposable.Dispose()
extern void U3CdelayAnswerU3Ed__22_System_IDisposable_Dispose_m21DC4C5108F00FFF3D5678EB7E31A5172FDD35F9 (void);
// 0x00000195 System.Boolean SpeechDetection/<delayAnswer>d__22::MoveNext()
extern void U3CdelayAnswerU3Ed__22_MoveNext_mC4D22297822A1D3508D82F9BDE2642792A8774C0 (void);
// 0x00000196 System.Object SpeechDetection/<delayAnswer>d__22::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CdelayAnswerU3Ed__22_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m72CB275E58D203B0A5C803951F7D4EE7422FCDD8 (void);
// 0x00000197 System.Void SpeechDetection/<delayAnswer>d__22::System.Collections.IEnumerator.Reset()
extern void U3CdelayAnswerU3Ed__22_System_Collections_IEnumerator_Reset_m93850936565CD71FF2F17D4BEF4321C1A05C449A (void);
// 0x00000198 System.Object SpeechDetection/<delayAnswer>d__22::System.Collections.IEnumerator.get_Current()
extern void U3CdelayAnswerU3Ed__22_System_Collections_IEnumerator_get_Current_m3A0860DD62ED37606BAF36C70E4659B3E18340A4 (void);
// 0x00000199 System.Void TestScript::Start()
extern void TestScript_Start_m5EADB03D1F6C909A74ABA654FA9F2162CD2DA750 (void);
// 0x0000019A System.Void TestScript::.ctor()
extern void TestScript__ctor_m58A026B07FA116838E76CF62F4630A99FB96365C (void);
// 0x0000019B System.Void Tg_Loader::Awake()
extern void Tg_Loader_Awake_mF5AD0A3AC72F3B7BD341C74012E3194AA0C08B68 (void);
// 0x0000019C System.Void Tg_Loader::Start()
extern void Tg_Loader_Start_mB0602917F605650FF8C59A90098A2A5A35143C19 (void);
// 0x0000019D System.Collections.IEnumerator Tg_Loader::processVideo()
extern void Tg_Loader_processVideo_mD605E0E448B6A69D8726C1BA1D65A3DAB17AB5DF (void);
// 0x0000019E System.Void Tg_Loader::quit()
extern void Tg_Loader_quit_m9119A499A217F4D589F1C03F50EF1BD84337E54D (void);
// 0x0000019F System.Collections.IEnumerator Tg_Loader::execute()
extern void Tg_Loader_execute_m55D106AD2047D2ECC981B04076BE3A2719ED3489 (void);
// 0x000001A0 System.Void Tg_Loader::CopyVideo()
extern void Tg_Loader_CopyVideo_mA1C006CAB312C57E5DDC19B5B8182AEAD490DD98 (void);
// 0x000001A1 System.Void Tg_Loader::Update()
extern void Tg_Loader_Update_mD2D821AB2E3D937E7259C1F49F1D9E19FCC8AF8F (void);
// 0x000001A2 System.Void Tg_Loader::mediaFolders()
extern void Tg_Loader_mediaFolders_mB6BDAFFA0EA1D843770DDC9D978659E51FB566BC (void);
// 0x000001A3 System.Collections.IEnumerator Tg_Loader::show()
extern void Tg_Loader_show_m313D3E3554FC596FBA175EB79C04DFA481965ED9 (void);
// 0x000001A4 System.Void Tg_Loader::genera()
extern void Tg_Loader_genera_m1902CBDE3685FBD4BD6652FD178C86CB4BBCB071 (void);
// 0x000001A5 System.Collections.IEnumerator Tg_Loader::jukebox()
extern void Tg_Loader_jukebox_m016BB104F339C981ED8B6ED2D2D633E23132657F (void);
// 0x000001A6 System.Collections.IEnumerator Tg_Loader::videoRunnerS(System.Int32)
extern void Tg_Loader_videoRunnerS_m282046B2D95397FEF9608E076865ADE28D78A6E3 (void);
// 0x000001A7 System.Collections.IEnumerator Tg_Loader::renderVideo()
extern void Tg_Loader_renderVideo_m25A0FD4111D75349E8731C8E34D44D89596E6F2A (void);
// 0x000001A8 System.Collections.IEnumerator Tg_Loader::GetMyAudioClip(System.String)
extern void Tg_Loader_GetMyAudioClip_m35188C6DF6958545FB1B1BA4435558BD15629A37 (void);
// 0x000001A9 System.Void Tg_Loader::EndReached(UnityEngine.Video.VideoPlayer)
extern void Tg_Loader_EndReached_m2C703B71FCFF3CF9EA2F557CB4CAAC0AF32DC756 (void);
// 0x000001AA System.Collections.IEnumerator Tg_Loader::LoadUrlImage()
extern void Tg_Loader_LoadUrlImage_m44DC8C20A059F5D2235ED1FCF301161E9AA73DE4 (void);
// 0x000001AB System.Void Tg_Loader::ParseFile(System.String)
extern void Tg_Loader_ParseFile_mDE501F1209DD61E886C5C0CBFF9CE0DBD89C91F6 (void);
// 0x000001AC System.Void Tg_Loader::OnRequestFinished(BestHTTP.HTTPRequest,BestHTTP.HTTPResponse)
extern void Tg_Loader_OnRequestFinished_m110A8D30271106ADA81425D0B4A7ED117EB01C87 (void);
// 0x000001AD System.Void Tg_Loader::OnRequestFinished2(BestHTTP.HTTPRequest,BestHTTP.HTTPResponse)
extern void Tg_Loader_OnRequestFinished2_m53712D6E15AE43D475930401AEE59024CC80ACBB (void);
// 0x000001AE System.Collections.IEnumerator Tg_Loader::callVag()
extern void Tg_Loader_callVag_m17EF8B802D02053FACE4BF714FA90AA24A72CDF6 (void);
// 0x000001AF System.Void Tg_Loader::.ctor()
extern void Tg_Loader__ctor_m4EE6E4524974A5ACCAD1B1BA45DD53709F78911D (void);
// 0x000001B0 System.Void Tg_Loader/<processVideo>d__53::.ctor(System.Int32)
extern void U3CprocessVideoU3Ed__53__ctor_m7B70B7D8BF77E0FE33C6537F49E42DECC9E332B1 (void);
// 0x000001B1 System.Void Tg_Loader/<processVideo>d__53::System.IDisposable.Dispose()
extern void U3CprocessVideoU3Ed__53_System_IDisposable_Dispose_m37FE66AB29FCB306DCAF376FB882E412E22AFE8D (void);
// 0x000001B2 System.Boolean Tg_Loader/<processVideo>d__53::MoveNext()
extern void U3CprocessVideoU3Ed__53_MoveNext_mC5A0860E3885710853B3FBFDE2F95C906D5C5B22 (void);
// 0x000001B3 System.Object Tg_Loader/<processVideo>d__53::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CprocessVideoU3Ed__53_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m1C6D4906559004D0AECF5EE7A7C57988BA7F1088 (void);
// 0x000001B4 System.Void Tg_Loader/<processVideo>d__53::System.Collections.IEnumerator.Reset()
extern void U3CprocessVideoU3Ed__53_System_Collections_IEnumerator_Reset_mD22DA273ADD91480522C7DA18F33AC51D6B385C9 (void);
// 0x000001B5 System.Object Tg_Loader/<processVideo>d__53::System.Collections.IEnumerator.get_Current()
extern void U3CprocessVideoU3Ed__53_System_Collections_IEnumerator_get_Current_m4F1A2B4117A7C2EC833EF6AE1A5850D342ACFE07 (void);
// 0x000001B6 System.Void Tg_Loader/<execute>d__55::.ctor(System.Int32)
extern void U3CexecuteU3Ed__55__ctor_m885AE33F260C60F219FD2B844EB98EBAF611BFF2 (void);
// 0x000001B7 System.Void Tg_Loader/<execute>d__55::System.IDisposable.Dispose()
extern void U3CexecuteU3Ed__55_System_IDisposable_Dispose_m34E1554BD78DCEF5CB43E68D4C8CB2F780E823B1 (void);
// 0x000001B8 System.Boolean Tg_Loader/<execute>d__55::MoveNext()
extern void U3CexecuteU3Ed__55_MoveNext_m7F309D410CA598066B927D55993C4053C461FAA1 (void);
// 0x000001B9 System.Object Tg_Loader/<execute>d__55::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CexecuteU3Ed__55_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m288B19DEE73CCD61964017D6100AFCF20CB78E7D (void);
// 0x000001BA System.Void Tg_Loader/<execute>d__55::System.Collections.IEnumerator.Reset()
extern void U3CexecuteU3Ed__55_System_Collections_IEnumerator_Reset_m6A9EA4A1FB29D28B11DA5790C9B37518FA34AC81 (void);
// 0x000001BB System.Object Tg_Loader/<execute>d__55::System.Collections.IEnumerator.get_Current()
extern void U3CexecuteU3Ed__55_System_Collections_IEnumerator_get_Current_m6273F0AEF4942EFD70F45599A1B9C4D6EEBE3EAA (void);
// 0x000001BC System.Void Tg_Loader/<show>d__59::.ctor(System.Int32)
extern void U3CshowU3Ed__59__ctor_mED8B04C32888BA300218A44889A2FA6E78DBD710 (void);
// 0x000001BD System.Void Tg_Loader/<show>d__59::System.IDisposable.Dispose()
extern void U3CshowU3Ed__59_System_IDisposable_Dispose_m6D7FBE31991420375ADB065BE2501AB8E107F00A (void);
// 0x000001BE System.Boolean Tg_Loader/<show>d__59::MoveNext()
extern void U3CshowU3Ed__59_MoveNext_m8FD67620C4BDDF6C236E0E78293296A394196C79 (void);
// 0x000001BF System.Object Tg_Loader/<show>d__59::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CshowU3Ed__59_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mF5565A2CDFD058A3644B0C1BB91FEC826D12F9C6 (void);
// 0x000001C0 System.Void Tg_Loader/<show>d__59::System.Collections.IEnumerator.Reset()
extern void U3CshowU3Ed__59_System_Collections_IEnumerator_Reset_mCA2AAA628E2B90C96B6F0FC5D97ED1BEF9BA294C (void);
// 0x000001C1 System.Object Tg_Loader/<show>d__59::System.Collections.IEnumerator.get_Current()
extern void U3CshowU3Ed__59_System_Collections_IEnumerator_get_Current_m17809F1B25774F439939ABB1FFA63DDDEFD7A790 (void);
// 0x000001C2 System.Void Tg_Loader/<jukebox>d__63::.ctor(System.Int32)
extern void U3CjukeboxU3Ed__63__ctor_m788936E37197E6C962E65ABD255191A13B7CACDF (void);
// 0x000001C3 System.Void Tg_Loader/<jukebox>d__63::System.IDisposable.Dispose()
extern void U3CjukeboxU3Ed__63_System_IDisposable_Dispose_mDD459F3537DD1B15C788A209BC7D1767883D0849 (void);
// 0x000001C4 System.Boolean Tg_Loader/<jukebox>d__63::MoveNext()
extern void U3CjukeboxU3Ed__63_MoveNext_m6FD3167752F55BD9AB3675D98AC63547413D836C (void);
// 0x000001C5 System.Object Tg_Loader/<jukebox>d__63::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CjukeboxU3Ed__63_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m72BD4D30CE93F47422A50C9D496F82AF021B171B (void);
// 0x000001C6 System.Void Tg_Loader/<jukebox>d__63::System.Collections.IEnumerator.Reset()
extern void U3CjukeboxU3Ed__63_System_Collections_IEnumerator_Reset_m702048AD1849520971A30FF9A525EC9E0654CD9F (void);
// 0x000001C7 System.Object Tg_Loader/<jukebox>d__63::System.Collections.IEnumerator.get_Current()
extern void U3CjukeboxU3Ed__63_System_Collections_IEnumerator_get_Current_m02B826D98C5608960FA65922030CA48AB17C0B18 (void);
// 0x000001C8 System.Void Tg_Loader/<videoRunnerS>d__64::.ctor(System.Int32)
extern void U3CvideoRunnerSU3Ed__64__ctor_m67B95AC50CE8C446A7C3D63DDF3C80EF4A4FBD80 (void);
// 0x000001C9 System.Void Tg_Loader/<videoRunnerS>d__64::System.IDisposable.Dispose()
extern void U3CvideoRunnerSU3Ed__64_System_IDisposable_Dispose_m0CD164B57208BFAC22F5B760A75552826800D423 (void);
// 0x000001CA System.Boolean Tg_Loader/<videoRunnerS>d__64::MoveNext()
extern void U3CvideoRunnerSU3Ed__64_MoveNext_m690D04967ABA68549A936070189AF5CCB5A5427A (void);
// 0x000001CB System.Object Tg_Loader/<videoRunnerS>d__64::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CvideoRunnerSU3Ed__64_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mBA9B20D22D57AF509774B5EB5E49A7760E1BD4CE (void);
// 0x000001CC System.Void Tg_Loader/<videoRunnerS>d__64::System.Collections.IEnumerator.Reset()
extern void U3CvideoRunnerSU3Ed__64_System_Collections_IEnumerator_Reset_mF71A27C1DD781C6B04C4B72FBF148E550681745C (void);
// 0x000001CD System.Object Tg_Loader/<videoRunnerS>d__64::System.Collections.IEnumerator.get_Current()
extern void U3CvideoRunnerSU3Ed__64_System_Collections_IEnumerator_get_Current_m38928416452416CAF750A970B477DD913AB62DA3 (void);
// 0x000001CE System.Void Tg_Loader/<renderVideo>d__65::.ctor(System.Int32)
extern void U3CrenderVideoU3Ed__65__ctor_mBE99CFA8D2EDC000725338649BA61259B214AB5E (void);
// 0x000001CF System.Void Tg_Loader/<renderVideo>d__65::System.IDisposable.Dispose()
extern void U3CrenderVideoU3Ed__65_System_IDisposable_Dispose_m8465EBA8E2A7CE6985EABB44296BC6859A0EBAA6 (void);
// 0x000001D0 System.Boolean Tg_Loader/<renderVideo>d__65::MoveNext()
extern void U3CrenderVideoU3Ed__65_MoveNext_m7E93D9F87DB90414865F56BE30B82E474DBAB39C (void);
// 0x000001D1 System.Object Tg_Loader/<renderVideo>d__65::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CrenderVideoU3Ed__65_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mB9AE8F7F4219F73097D0AF7F3AC907724C3FCD61 (void);
// 0x000001D2 System.Void Tg_Loader/<renderVideo>d__65::System.Collections.IEnumerator.Reset()
extern void U3CrenderVideoU3Ed__65_System_Collections_IEnumerator_Reset_m6F4C4ABE6EAE1125927F81BE3F98F537ED0B569F (void);
// 0x000001D3 System.Object Tg_Loader/<renderVideo>d__65::System.Collections.IEnumerator.get_Current()
extern void U3CrenderVideoU3Ed__65_System_Collections_IEnumerator_get_Current_mCA747C01DE5A1D63D3C3B02780016299904DF0FB (void);
// 0x000001D4 System.Void Tg_Loader/<GetMyAudioClip>d__66::.ctor(System.Int32)
extern void U3CGetMyAudioClipU3Ed__66__ctor_m57CD51B53186CD7850110F3E359E1B4CF083CFAE (void);
// 0x000001D5 System.Void Tg_Loader/<GetMyAudioClip>d__66::System.IDisposable.Dispose()
extern void U3CGetMyAudioClipU3Ed__66_System_IDisposable_Dispose_mC22B8F7AEC1FCB124388C1B7AAC4ABD270BABA6F (void);
// 0x000001D6 System.Boolean Tg_Loader/<GetMyAudioClip>d__66::MoveNext()
extern void U3CGetMyAudioClipU3Ed__66_MoveNext_m5C0D21B299F6581FC59397447A73F6E8D4AF5343 (void);
// 0x000001D7 System.Object Tg_Loader/<GetMyAudioClip>d__66::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CGetMyAudioClipU3Ed__66_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mF6043BA370A11DD4D0A7333309082073A8457E4E (void);
// 0x000001D8 System.Void Tg_Loader/<GetMyAudioClip>d__66::System.Collections.IEnumerator.Reset()
extern void U3CGetMyAudioClipU3Ed__66_System_Collections_IEnumerator_Reset_mE97DFD9B9BFA341BC9FC781A2BF96D49D6CDFD98 (void);
// 0x000001D9 System.Object Tg_Loader/<GetMyAudioClip>d__66::System.Collections.IEnumerator.get_Current()
extern void U3CGetMyAudioClipU3Ed__66_System_Collections_IEnumerator_get_Current_m8CB1CC81A5E3F3D1E865F39AEDF615C1DD2816F2 (void);
// 0x000001DA System.Void Tg_Loader/<LoadUrlImage>d__68::.ctor(System.Int32)
extern void U3CLoadUrlImageU3Ed__68__ctor_m403B159AE1C276523A88D3CA0FEA0DF2F55064C1 (void);
// 0x000001DB System.Void Tg_Loader/<LoadUrlImage>d__68::System.IDisposable.Dispose()
extern void U3CLoadUrlImageU3Ed__68_System_IDisposable_Dispose_mE4DEE79133EEB29626B08AD2F105582D10307573 (void);
// 0x000001DC System.Boolean Tg_Loader/<LoadUrlImage>d__68::MoveNext()
extern void U3CLoadUrlImageU3Ed__68_MoveNext_mDC991CABC2CACC0931FB1D2937A472EE86D8F3B5 (void);
// 0x000001DD System.Object Tg_Loader/<LoadUrlImage>d__68::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CLoadUrlImageU3Ed__68_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mD7961709D007A131EBCFB1D2FDAC358B2A414373 (void);
// 0x000001DE System.Void Tg_Loader/<LoadUrlImage>d__68::System.Collections.IEnumerator.Reset()
extern void U3CLoadUrlImageU3Ed__68_System_Collections_IEnumerator_Reset_m055D45D65A5024E94C9D251BA1798D6753FF1FF9 (void);
// 0x000001DF System.Object Tg_Loader/<LoadUrlImage>d__68::System.Collections.IEnumerator.get_Current()
extern void U3CLoadUrlImageU3Ed__68_System_Collections_IEnumerator_get_Current_mE2FA24BB2ED24E0FEAA6D8C3BA07B34969CACD75 (void);
// 0x000001E0 System.Void Tg_Loader/<>c::.cctor()
extern void U3CU3Ec__cctor_m24AC40C463D0D71908BE3C91E3274D64D0A42D5E (void);
// 0x000001E1 System.Void Tg_Loader/<>c::.ctor()
extern void U3CU3Ec__ctor_mA32FC2881122187E8A4CC946785E489F46DC8454 (void);
// 0x000001E2 System.String[] Tg_Loader/<>c::<ParseFile>b__69_0(System.String)
extern void U3CU3Ec_U3CParseFileU3Eb__69_0_mDCEC96DADDB5E4F2A6170B58F30FC011182424C8 (void);
// 0x000001E3 System.Boolean Tg_Loader/<>c::<ParseFile>b__69_1(System.String[])
extern void U3CU3Ec_U3CParseFileU3Eb__69_1_m998C4700B033561B487A648DDD94210AFC59540C (void);
// 0x000001E4 System.String Tg_Loader/<>c::<ParseFile>b__69_2(System.String[])
extern void U3CU3Ec_U3CParseFileU3Eb__69_2_m8ECDFAE6B47D9AEB5455407B05438ED4F57EBE66 (void);
// 0x000001E5 System.String Tg_Loader/<>c::<ParseFile>b__69_3(System.String[])
extern void U3CU3Ec_U3CParseFileU3Eb__69_3_m8E680D83C4790B2075934788B651AE8DBFC59C0C (void);
// 0x000001E6 System.String[] Tg_Loader/<>c::<ParseFile>b__69_4(System.String)
extern void U3CU3Ec_U3CParseFileU3Eb__69_4_m657B0A9C0A21682E166DD8E8F5D9BC2E24C3797C (void);
// 0x000001E7 System.Boolean Tg_Loader/<>c::<ParseFile>b__69_5(System.String[])
extern void U3CU3Ec_U3CParseFileU3Eb__69_5_m4D7C76EDDE00FE2030D2E057019224DD2DA16AC9 (void);
// 0x000001E8 System.String Tg_Loader/<>c::<ParseFile>b__69_6(System.String[])
extern void U3CU3Ec_U3CParseFileU3Eb__69_6_m6BEE3D7A47BE8DCACDBAD7E9F0CD4B2AD4768E29 (void);
// 0x000001E9 System.String Tg_Loader/<>c::<ParseFile>b__69_7(System.String[])
extern void U3CU3Ec_U3CParseFileU3Eb__69_7_m3517A4DF12AD7FB872B4F1A9BBC7A0CA36CD7F03 (void);
// 0x000001EA System.Void Tg_Loader/<callVag>d__72::.ctor(System.Int32)
extern void U3CcallVagU3Ed__72__ctor_m15E218A419C37BA70C856A045D1411D20A138D62 (void);
// 0x000001EB System.Void Tg_Loader/<callVag>d__72::System.IDisposable.Dispose()
extern void U3CcallVagU3Ed__72_System_IDisposable_Dispose_mE652410C5E070E38174A8796B5BF07B1079570EF (void);
// 0x000001EC System.Boolean Tg_Loader/<callVag>d__72::MoveNext()
extern void U3CcallVagU3Ed__72_MoveNext_mC0C1FCFD1ED4F3B395C1522E6A95ECEBD99C3B7F (void);
// 0x000001ED System.Object Tg_Loader/<callVag>d__72::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CcallVagU3Ed__72_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mB62BC9086B174EA111632FADC2884C460F1A6460 (void);
// 0x000001EE System.Void Tg_Loader/<callVag>d__72::System.Collections.IEnumerator.Reset()
extern void U3CcallVagU3Ed__72_System_Collections_IEnumerator_Reset_mC64D65BC96BE24DF9A18FE5673CFE248F4DE54A0 (void);
// 0x000001EF System.Object Tg_Loader/<callVag>d__72::System.Collections.IEnumerator.get_Current()
extern void U3CcallVagU3Ed__72_System_Collections_IEnumerator_get_Current_m9994C3DC29325A4E463DC31B715136A91257FEA5 (void);
// 0x000001F0 System.Void VideoDownloader::Start()
extern void VideoDownloader_Start_m981B7942A953FF6074D6EE3EB3720311918B6945 (void);
// 0x000001F1 System.Void VideoDownloader::go()
extern void VideoDownloader_go_m1327A11969E9C5E6C844765D09CCCB9DC467C72C (void);
// 0x000001F2 System.Collections.IEnumerator VideoDownloader::playVideo(System.Boolean)
extern void VideoDownloader_playVideo_m464C5DC3E3CBF21319197C6E8925A69011937C5C (void);
// 0x000001F3 System.Collections.IEnumerator VideoDownloader::DownloadVideo()
extern void VideoDownloader_DownloadVideo_m719763A83CB4BBEA82DF740B54CE0AA7D21D2C2B (void);
// 0x000001F4 System.Collections.IEnumerator VideoDownloader::wwwDown()
extern void VideoDownloader_wwwDown_m88F7A3D54F0CC998DB5854A764E1B6FDD11F6D04 (void);
// 0x000001F5 System.Void VideoDownloader::.ctor()
extern void VideoDownloader__ctor_m2DE017284C80E89EC8D9DDB9800B99027E7540DF (void);
// 0x000001F6 System.Void VideoDownloader/<playVideo>d__10::.ctor(System.Int32)
extern void U3CplayVideoU3Ed__10__ctor_m48975DF9569E6A0218A13559373B12BC4D27170B (void);
// 0x000001F7 System.Void VideoDownloader/<playVideo>d__10::System.IDisposable.Dispose()
extern void U3CplayVideoU3Ed__10_System_IDisposable_Dispose_mA1CD8004810E0FE8E5191C673EFF8C069FEE47B7 (void);
// 0x000001F8 System.Boolean VideoDownloader/<playVideo>d__10::MoveNext()
extern void U3CplayVideoU3Ed__10_MoveNext_mC664DC7BC632818612305FB2F7A42C8F4E1CE65A (void);
// 0x000001F9 System.Object VideoDownloader/<playVideo>d__10::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CplayVideoU3Ed__10_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m2058D973233485B77635A9630795C8319CF63170 (void);
// 0x000001FA System.Void VideoDownloader/<playVideo>d__10::System.Collections.IEnumerator.Reset()
extern void U3CplayVideoU3Ed__10_System_Collections_IEnumerator_Reset_mA64E97F2F124BABECF11154E0E411F6364940F88 (void);
// 0x000001FB System.Object VideoDownloader/<playVideo>d__10::System.Collections.IEnumerator.get_Current()
extern void U3CplayVideoU3Ed__10_System_Collections_IEnumerator_get_Current_m4B13895F46BA6B8932236B660F9C52C4D74D772B (void);
// 0x000001FC System.Void VideoDownloader/<DownloadVideo>d__11::.ctor(System.Int32)
extern void U3CDownloadVideoU3Ed__11__ctor_mA5D00A3222F41CB502644E4F5C1AA011838AE73E (void);
// 0x000001FD System.Void VideoDownloader/<DownloadVideo>d__11::System.IDisposable.Dispose()
extern void U3CDownloadVideoU3Ed__11_System_IDisposable_Dispose_mB55FB69115EC712D31633C5E75C6A4A42C0EB767 (void);
// 0x000001FE System.Boolean VideoDownloader/<DownloadVideo>d__11::MoveNext()
extern void U3CDownloadVideoU3Ed__11_MoveNext_m5D848DB95441751CF5ABBDA6F7AA73217F2CF6B7 (void);
// 0x000001FF System.Object VideoDownloader/<DownloadVideo>d__11::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CDownloadVideoU3Ed__11_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mDE7DB4F243F436C386E8764D2043BE462F5756A2 (void);
// 0x00000200 System.Void VideoDownloader/<DownloadVideo>d__11::System.Collections.IEnumerator.Reset()
extern void U3CDownloadVideoU3Ed__11_System_Collections_IEnumerator_Reset_m8139EF4BAA0F855AD1DA43CC07DAD62C7E88BE79 (void);
// 0x00000201 System.Object VideoDownloader/<DownloadVideo>d__11::System.Collections.IEnumerator.get_Current()
extern void U3CDownloadVideoU3Ed__11_System_Collections_IEnumerator_get_Current_m4270645782C032FF5BCAFDD9F3FCFEA1D8BCFE49 (void);
// 0x00000202 System.Void VideoDownloader/<wwwDown>d__12::.ctor(System.Int32)
extern void U3CwwwDownU3Ed__12__ctor_mB02E1A9B8595456FDDA6BCC21A45844C806CE592 (void);
// 0x00000203 System.Void VideoDownloader/<wwwDown>d__12::System.IDisposable.Dispose()
extern void U3CwwwDownU3Ed__12_System_IDisposable_Dispose_m9F170D89BBB1EDDD61A5B103A61DAE081B6D1324 (void);
// 0x00000204 System.Boolean VideoDownloader/<wwwDown>d__12::MoveNext()
extern void U3CwwwDownU3Ed__12_MoveNext_mAB551D8FACEF500ABAEE46E502D3165076C5E29E (void);
// 0x00000205 System.Object VideoDownloader/<wwwDown>d__12::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CwwwDownU3Ed__12_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m9D49DDACCDA9C860FE8957F4B402CE0BCA608859 (void);
// 0x00000206 System.Void VideoDownloader/<wwwDown>d__12::System.Collections.IEnumerator.Reset()
extern void U3CwwwDownU3Ed__12_System_Collections_IEnumerator_Reset_mC73066870C1B2956AC57AC9FAFD000EC970BAC09 (void);
// 0x00000207 System.Object VideoDownloader/<wwwDown>d__12::System.Collections.IEnumerator.get_Current()
extern void U3CwwwDownU3Ed__12_System_Collections_IEnumerator_get_Current_m773C343AAC269EECC3E25A1FA999D78EE6479A94 (void);
// 0x00000208 System.Void Web_PUT_Uploader::Start()
extern void Web_PUT_Uploader_Start_mD07675BE02DB53BFDE382EC2F7C987E173338AF6 (void);
// 0x00000209 System.Collections.IEnumerator Web_PUT_Uploader::Upload()
extern void Web_PUT_Uploader_Upload_mBF754827DE2B7976A8E46A639DCD938E44833AA6 (void);
// 0x0000020A System.Void Web_PUT_Uploader::.ctor()
extern void Web_PUT_Uploader__ctor_mF88145246EAD5E7C054D6A9BEAC2DFA4052053B6 (void);
// 0x0000020B System.Void Web_PUT_Uploader/<Upload>d__1::.ctor(System.Int32)
extern void U3CUploadU3Ed__1__ctor_mB0FECAF97BE4A068EDAAC30E6324B3E31F8164E2 (void);
// 0x0000020C System.Void Web_PUT_Uploader/<Upload>d__1::System.IDisposable.Dispose()
extern void U3CUploadU3Ed__1_System_IDisposable_Dispose_mC480BEB8A2C0442E587E792F4090A02A78DC4734 (void);
// 0x0000020D System.Boolean Web_PUT_Uploader/<Upload>d__1::MoveNext()
extern void U3CUploadU3Ed__1_MoveNext_m27B122245D63433BFC88CB9C2CE370A6841B82B9 (void);
// 0x0000020E System.Object Web_PUT_Uploader/<Upload>d__1::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CUploadU3Ed__1_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m792CB3C128BEDEB3DD57FD5AF92D25550125389C (void);
// 0x0000020F System.Void Web_PUT_Uploader/<Upload>d__1::System.Collections.IEnumerator.Reset()
extern void U3CUploadU3Ed__1_System_Collections_IEnumerator_Reset_m5C31F98908F060ECF2EE769D3BBDAC0A8741EBBA (void);
// 0x00000210 System.Object Web_PUT_Uploader/<Upload>d__1::System.Collections.IEnumerator.get_Current()
extern void U3CUploadU3Ed__1_System_Collections_IEnumerator_get_Current_m28E0F47DB35842744912550EC682E295694E5222 (void);
// 0x00000211 System.Void WriteToFile::Start()
extern void WriteToFile_Start_mC9EF0CEDE2F185996B2712E971D985CE0BB35293 (void);
// 0x00000212 System.Void WriteToFile::Update()
extern void WriteToFile_Update_mCECB51DDCB8A8F806CB38C3CB2A83D8FDB15B842 (void);
// 0x00000213 System.Void WriteToFile::UpdateSpeechDetected(System.String)
extern void WriteToFile_UpdateSpeechDetected_m895F73FEB26EF77DD61AEA9DC1005FBCE4B4799C (void);
// 0x00000214 System.Void WriteToFile::SendToServerFile()
extern void WriteToFile_SendToServerFile_m977EF05F9079844F1F05EC65D085F84CE4D2E4D0 (void);
// 0x00000215 System.Void WriteToFile::GetFromServerFile()
extern void WriteToFile_GetFromServerFile_mF25678AD251EEF3E4AE248CA88F9ED08435D136D (void);
// 0x00000216 System.Void WriteToFile::WriteToLocalFile()
extern void WriteToFile_WriteToLocalFile_m1C36142BEC937E9093EF53D00F49D2F6AA351DF6 (void);
// 0x00000217 System.String WriteToFile::SendSocket_Webgl_Rest(System.String)
extern void WriteToFile_SendSocket_Webgl_Rest_mC18A0269B6A431B2017AB66534DE2BA44F0B17AA (void);
// 0x00000218 System.Collections.IEnumerator WriteToFile::sendTextToFile()
extern void WriteToFile_sendTextToFile_m3F01F72F56C5AC7DE226C0F09C04F6243751D6D9 (void);
// 0x00000219 System.Collections.IEnumerator WriteToFile::_sendTextToFile()
extern void WriteToFile__sendTextToFile_m32AD1B51D54993753117D111C89CD17AA36E6CFC (void);
// 0x0000021A System.Collections.IEnumerator WriteToFile::getTextFromFile()
extern void WriteToFile_getTextFromFile_mE25957A8B76F6D530B69C7CAB54E82B14CEB0D8B (void);
// 0x0000021B System.Collections.IEnumerator WriteToFile::LoadUrlImage()
extern void WriteToFile_LoadUrlImage_m927ADBC753815ACBC4F44A95707C265149E5A41E (void);
// 0x0000021C System.String WriteToFile::GetMessage(System.String)
extern void WriteToFile_GetMessage_m73CBDFFD048DE6334EDD6C19C373854089777295 (void);
// 0x0000021D System.Void WriteToFile::.ctor()
extern void WriteToFile__ctor_m366A4DB41C8CBAF4ABC8F030C48C4224A6568F1A (void);
// 0x0000021E System.Void WriteToFile/<>c::.cctor()
extern void U3CU3Ec__cctor_m8DD5F7B81D82EBF4219819D8D2A7071BAD3C8DCB (void);
// 0x0000021F System.Void WriteToFile/<>c::.ctor()
extern void U3CU3Ec__ctor_m5DC2214FBEBEBCD4EEAC781FAE943B05974F9BB9 (void);
// 0x00000220 System.Boolean WriteToFile/<>c::<SendSocket_Webgl_Rest>b__24_0(System.Object,System.Security.Cryptography.X509Certificates.X509Certificate,System.Security.Cryptography.X509Certificates.X509Chain,System.Net.Security.SslPolicyErrors)
extern void U3CU3Ec_U3CSendSocket_Webgl_RestU3Eb__24_0_mABF1EEA8CCC68ACE6328DC07F7A0A7BF76149A9B (void);
// 0x00000221 System.Boolean WriteToFile/<>c::<_sendTextToFile>b__26_0(System.Object,System.Security.Cryptography.X509Certificates.X509Certificate,System.Security.Cryptography.X509Certificates.X509Chain,System.Net.Security.SslPolicyErrors)
extern void U3CU3Ec_U3C_sendTextToFileU3Eb__26_0_mCC834519DD98659AD0D448117EFFDACA74476F0B (void);
// 0x00000222 System.Void WriteToFile/<sendTextToFile>d__25::.ctor(System.Int32)
extern void U3CsendTextToFileU3Ed__25__ctor_m5CA84019D801CE1AE60A7736EE43041D3482BB55 (void);
// 0x00000223 System.Void WriteToFile/<sendTextToFile>d__25::System.IDisposable.Dispose()
extern void U3CsendTextToFileU3Ed__25_System_IDisposable_Dispose_m5B6067FFCDF7ED61689C9DA39056B8C5AB5A3768 (void);
// 0x00000224 System.Boolean WriteToFile/<sendTextToFile>d__25::MoveNext()
extern void U3CsendTextToFileU3Ed__25_MoveNext_mFA1CC8ACB450A11F354BDD5CFAB7587CE8AA9C8F (void);
// 0x00000225 System.Void WriteToFile/<sendTextToFile>d__25::<>m__Finally1()
extern void U3CsendTextToFileU3Ed__25_U3CU3Em__Finally1_m8015E290EEE91E319D12D9C8E4AACF8060860F1A (void);
// 0x00000226 System.Object WriteToFile/<sendTextToFile>d__25::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CsendTextToFileU3Ed__25_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mF579AE8FE6888EA8DA2F4E76C8D56CCB80641E39 (void);
// 0x00000227 System.Void WriteToFile/<sendTextToFile>d__25::System.Collections.IEnumerator.Reset()
extern void U3CsendTextToFileU3Ed__25_System_Collections_IEnumerator_Reset_mA35A79D89500DBDE609032742D0A20C9C07DCCEE (void);
// 0x00000228 System.Object WriteToFile/<sendTextToFile>d__25::System.Collections.IEnumerator.get_Current()
extern void U3CsendTextToFileU3Ed__25_System_Collections_IEnumerator_get_Current_mE96BCA88012F1F105791A2008A849CA51B5B7365 (void);
// 0x00000229 System.Void WriteToFile/<_sendTextToFile>d__26::.ctor(System.Int32)
extern void U3C_sendTextToFileU3Ed__26__ctor_mAB45EDBB9B379E41B188DBD26506F62F6E07D71B (void);
// 0x0000022A System.Void WriteToFile/<_sendTextToFile>d__26::System.IDisposable.Dispose()
extern void U3C_sendTextToFileU3Ed__26_System_IDisposable_Dispose_m8C9F2E27544C986C5DEC10AAA70C01DF34D9098B (void);
// 0x0000022B System.Boolean WriteToFile/<_sendTextToFile>d__26::MoveNext()
extern void U3C_sendTextToFileU3Ed__26_MoveNext_m0FCF5889984BCF28A1F4C59FFA6823E0FB69CFC7 (void);
// 0x0000022C System.Object WriteToFile/<_sendTextToFile>d__26::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3C_sendTextToFileU3Ed__26_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m36F7678FAE257FAED4BF7E351E5F0AAAE6E18B42 (void);
// 0x0000022D System.Void WriteToFile/<_sendTextToFile>d__26::System.Collections.IEnumerator.Reset()
extern void U3C_sendTextToFileU3Ed__26_System_Collections_IEnumerator_Reset_m5ABBE0B9D68C0EAE2C2B2DFC1A0C3B754A98BA1E (void);
// 0x0000022E System.Object WriteToFile/<_sendTextToFile>d__26::System.Collections.IEnumerator.get_Current()
extern void U3C_sendTextToFileU3Ed__26_System_Collections_IEnumerator_get_Current_mAA6EB7EEEB6EB8EB2D26B6AE55707DFFEC36096A (void);
// 0x0000022F System.Void WriteToFile/<getTextFromFile>d__27::.ctor(System.Int32)
extern void U3CgetTextFromFileU3Ed__27__ctor_m1996F982FF8DE37EC6A3311041031D1CFF978160 (void);
// 0x00000230 System.Void WriteToFile/<getTextFromFile>d__27::System.IDisposable.Dispose()
extern void U3CgetTextFromFileU3Ed__27_System_IDisposable_Dispose_m5BEE6B4F978B9012A2145393E3E28DBB721629F7 (void);
// 0x00000231 System.Boolean WriteToFile/<getTextFromFile>d__27::MoveNext()
extern void U3CgetTextFromFileU3Ed__27_MoveNext_mC2E33995CD095700C10FAFA885A18D0E80DB42CE (void);
// 0x00000232 System.Object WriteToFile/<getTextFromFile>d__27::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CgetTextFromFileU3Ed__27_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m2F5564C3E17D6DE1641D6D438A16A9671D6DF424 (void);
// 0x00000233 System.Void WriteToFile/<getTextFromFile>d__27::System.Collections.IEnumerator.Reset()
extern void U3CgetTextFromFileU3Ed__27_System_Collections_IEnumerator_Reset_m90DEE2DEA6F4D21D4634806909720CBEF37AC4FC (void);
// 0x00000234 System.Object WriteToFile/<getTextFromFile>d__27::System.Collections.IEnumerator.get_Current()
extern void U3CgetTextFromFileU3Ed__27_System_Collections_IEnumerator_get_Current_m3CAE87CA87CE43722580119C4FC7D3DC30C33676 (void);
// 0x00000235 System.Void WriteToFile/<LoadUrlImage>d__28::.ctor(System.Int32)
extern void U3CLoadUrlImageU3Ed__28__ctor_m4D0E48BFFBC2FC935E4C4346DAAD2486048F4D61 (void);
// 0x00000236 System.Void WriteToFile/<LoadUrlImage>d__28::System.IDisposable.Dispose()
extern void U3CLoadUrlImageU3Ed__28_System_IDisposable_Dispose_m8AE6CAE8E82011B361B6035FA48D11B2653E052D (void);
// 0x00000237 System.Boolean WriteToFile/<LoadUrlImage>d__28::MoveNext()
extern void U3CLoadUrlImageU3Ed__28_MoveNext_m0F77B36D4F4770D1958EC3D057349B3C0A8F9167 (void);
// 0x00000238 System.Object WriteToFile/<LoadUrlImage>d__28::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CLoadUrlImageU3Ed__28_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m1F8F91750191D9B7E7DAF80EEFD98A76C58D47C1 (void);
// 0x00000239 System.Void WriteToFile/<LoadUrlImage>d__28::System.Collections.IEnumerator.Reset()
extern void U3CLoadUrlImageU3Ed__28_System_Collections_IEnumerator_Reset_m53BE2A5492898B0852E5A7C936591F207CA17B87 (void);
// 0x0000023A System.Object WriteToFile/<LoadUrlImage>d__28::System.Collections.IEnumerator.get_Current()
extern void U3CLoadUrlImageU3Ed__28_System_Collections_IEnumerator_get_Current_mA4391FEBC347FD18BCABF41300102777467F0AC1 (void);
// 0x0000023B System.Void XMLProtocol::.ctor()
extern void XMLProtocol__ctor_m51C46F8787202A3C17925D7111B81A0EF3B6B569 (void);
// 0x0000023C System.Void animatorEngine::Start()
extern void animatorEngine_Start_mC80CB028703CB0C21E5B31B2B3A66B852F642B80 (void);
// 0x0000023D System.Void animatorEngine::triggerPersonaggio()
extern void animatorEngine_triggerPersonaggio_m48817FBE94D9CA2C05A4DA17B2509C939926EABA (void);
// 0x0000023E System.Void animatorEngine::setTriggerAnimator(System.String)
extern void animatorEngine_setTriggerAnimator_m3AA43F1B83785553DA3D8007A583767AC314F338 (void);
// 0x0000023F System.Void animatorEngine::animaPersonaggio(System.String)
extern void animatorEngine_animaPersonaggio_m5F8CF655F4FB45507D097E3B5F551E3EDDD6A9FC (void);
// 0x00000240 System.Collections.IEnumerator animatorEngine::cambia_espressione()
extern void animatorEngine_cambia_espressione_m1AB373B0E36FB31CFDAB7F504E7B49828129662C (void);
// 0x00000241 System.Void animatorEngine::espressioniFacciali()
extern void animatorEngine_espressioniFacciali_m67CEBD66750D5D065E939273619E5E2BD4D0BFF3 (void);
// 0x00000242 System.Collections.IEnumerator animatorEngine::setEmotionStates()
extern void animatorEngine_setEmotionStates_m133520155E108B929AD62EA0A6BFBFD41A724330 (void);
// 0x00000243 System.Void animatorEngine::.ctor()
extern void animatorEngine__ctor_m9DD455CC225EE8E019ED7B045E74A1F98D639FDD (void);
// 0x00000244 System.Void animatorEngine/<cambia_espressione>d__6::.ctor(System.Int32)
extern void U3Ccambia_espressioneU3Ed__6__ctor_m37941ACC1E2EC38754327F3A7615973A5F429219 (void);
// 0x00000245 System.Void animatorEngine/<cambia_espressione>d__6::System.IDisposable.Dispose()
extern void U3Ccambia_espressioneU3Ed__6_System_IDisposable_Dispose_mB674941E9C97E1E818F8C1277B9748F353A484C6 (void);
// 0x00000246 System.Boolean animatorEngine/<cambia_espressione>d__6::MoveNext()
extern void U3Ccambia_espressioneU3Ed__6_MoveNext_m2176A187DF33EDA4290358A1B8319132F9CE7C10 (void);
// 0x00000247 System.Object animatorEngine/<cambia_espressione>d__6::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3Ccambia_espressioneU3Ed__6_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mA88DEFCB74CBE4F92B568E027D6616395650CC6D (void);
// 0x00000248 System.Void animatorEngine/<cambia_espressione>d__6::System.Collections.IEnumerator.Reset()
extern void U3Ccambia_espressioneU3Ed__6_System_Collections_IEnumerator_Reset_m2DFC0C7C71CDAF37DF1A65B7425E90D5F175D7FC (void);
// 0x00000249 System.Object animatorEngine/<cambia_espressione>d__6::System.Collections.IEnumerator.get_Current()
extern void U3Ccambia_espressioneU3Ed__6_System_Collections_IEnumerator_get_Current_mC47E1B6CE7ADEEFD114DF06155249C36008C0024 (void);
// 0x0000024A System.Void animatorEngine/<setEmotionStates>d__8::.ctor(System.Int32)
extern void U3CsetEmotionStatesU3Ed__8__ctor_mF84C7F02F7D8A0CA096DA9D4F146B519BF400AF0 (void);
// 0x0000024B System.Void animatorEngine/<setEmotionStates>d__8::System.IDisposable.Dispose()
extern void U3CsetEmotionStatesU3Ed__8_System_IDisposable_Dispose_m873CA8C25018771442F848D5343D093AD67CF013 (void);
// 0x0000024C System.Boolean animatorEngine/<setEmotionStates>d__8::MoveNext()
extern void U3CsetEmotionStatesU3Ed__8_MoveNext_m2F0E73AE5FD286FA52791AB374FE795768A990F2 (void);
// 0x0000024D System.Object animatorEngine/<setEmotionStates>d__8::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CsetEmotionStatesU3Ed__8_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mA2F305461E014333419F46D70F68050F92CC8B9C (void);
// 0x0000024E System.Void animatorEngine/<setEmotionStates>d__8::System.Collections.IEnumerator.Reset()
extern void U3CsetEmotionStatesU3Ed__8_System_Collections_IEnumerator_Reset_m02D991E812CFAAAC73A3DA52F299AA23525A3BB7 (void);
// 0x0000024F System.Object animatorEngine/<setEmotionStates>d__8::System.Collections.IEnumerator.get_Current()
extern void U3CsetEmotionStatesU3Ed__8_System_Collections_IEnumerator_get_Current_m49E11D945FBE50359C677211178C7096F0F494E4 (void);
// 0x00000250 System.Void audioConvert::Start()
extern void audioConvert_Start_mA98AD88ADDB98CCF1C72656977E25E11D99007EC (void);
// 0x00000251 System.Void audioConvert::Update()
extern void audioConvert_Update_mCD8F3433C50EEEA8EA7B718D1C1CDE85F96F6C86 (void);
// 0x00000252 System.Void audioConvert::.ctor()
extern void audioConvert__ctor_m0A93DA43F6D54357C8AEA2738A240A46BEECE9A9 (void);
// 0x00000253 System.Void blendShapeExample::Awake()
extern void blendShapeExample_Awake_mCF676437EF9D90EC93782490892AF482675DDC0C (void);
// 0x00000254 System.Void blendShapeExample::Start()
extern void blendShapeExample_Start_m2663D7C178438BD6D3BD23F22C57EEE1EB3B11FC (void);
// 0x00000255 System.Void blendShapeExample::Update()
extern void blendShapeExample_Update_mD15B5A3FB15D8AAD0235E423C3C74631A9D790DA (void);
// 0x00000256 System.Void blendShapeExample::.ctor()
extern void blendShapeExample__ctor_mF7FF586DA9C64F78DA39C24749C53BF137732509 (void);
// 0x00000257 System.Void clipSwitcher::Start()
extern void clipSwitcher_Start_m38AC67C2A9CB737EA1DC0619A5C72F50EFB2E0CA (void);
// 0x00000258 System.Void clipSwitcher::Update()
extern void clipSwitcher_Update_m764606566967531974A5F7CB035535479DE38FC2 (void);
// 0x00000259 System.Void clipSwitcher::.ctor()
extern void clipSwitcher__ctor_m6DA74610F0F498224F64166C8BFA7EE899F1E7E6 (void);
// 0x0000025A System.Void constraints::Start()
extern void constraints_Start_m092A655C3E727266A67CC25FAE62197820866F5C (void);
// 0x0000025B System.Void constraints::Update()
extern void constraints_Update_mDABF1459EF8A25239FB5438B6CABC90CBD51D503 (void);
// 0x0000025C System.Void constraints::.ctor()
extern void constraints__ctor_m7CCA24C67313F5790DE35D3DA28045FEEFE220EA (void);
// 0x0000025D System.Void doMainThread::Update()
extern void doMainThread_Update_m34C2C2817775E05F992FDDBD43EE5D4051822D6D (void);
// 0x0000025E System.Void doMainThread::.ctor()
extern void doMainThread__ctor_m6FD6BB2BCE7DA34000C33FA669D7C3CA4B80682C (void);
// 0x0000025F System.Void doMainThread::.cctor()
extern void doMainThread__cctor_m23374A28634144E63EAF541B730854B80B2E37CD (void);
// 0x00000260 System.Void esempioAudioPlay::Start()
extern void esempioAudioPlay_Start_m139818446A75AE7A76B05082F27F0729B0956E0B (void);
// 0x00000261 System.Void esempioAudioPlay::OnGUI()
extern void esempioAudioPlay_OnGUI_m86A951562F40E0B55DEA581AFFE6A8E3E929F0C3 (void);
// 0x00000262 System.Void esempioAudioPlay::.ctor()
extern void esempioAudioPlay__ctor_m4CFA94F70B93AFF4D9BCF6B08FD312A078201642 (void);
// 0x00000263 System.Void exampleClass::Update()
extern void exampleClass_Update_m0DFB12E8AAEFDDEF904AD10A9040F9F18C21B5A5 (void);
// 0x00000264 System.Void exampleClass::Start()
extern void exampleClass_Start_mE068BC0C37959E7A262BA1B27A86804A513EACD1 (void);
// 0x00000265 System.Collections.IEnumerator exampleClass::WaitAndPrint(System.Single)
extern void exampleClass_WaitAndPrint_m30E47268495281CB0951B0E1F2F248B6928237C7 (void);
// 0x00000266 System.Void exampleClass::.ctor()
extern void exampleClass__ctor_mE212F3C2CD04FC56A13000F8DED71092FEBD6A5B (void);
// 0x00000267 System.Void exampleClass/<WaitAndPrint>d__4::.ctor(System.Int32)
extern void U3CWaitAndPrintU3Ed__4__ctor_mB34E8AC7D21DC592BF53C2929D36790E60DD9086 (void);
// 0x00000268 System.Void exampleClass/<WaitAndPrint>d__4::System.IDisposable.Dispose()
extern void U3CWaitAndPrintU3Ed__4_System_IDisposable_Dispose_m9E29FFB94187C72312B70ACA4CF9F7532FF9D0BA (void);
// 0x00000269 System.Boolean exampleClass/<WaitAndPrint>d__4::MoveNext()
extern void U3CWaitAndPrintU3Ed__4_MoveNext_m8999ECD7672AF76D103D6F0F32F99ADBE8E7CB80 (void);
// 0x0000026A System.Object exampleClass/<WaitAndPrint>d__4::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CWaitAndPrintU3Ed__4_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m1143F5D55709B0DEF0F3A95E5EE814371120E30A (void);
// 0x0000026B System.Void exampleClass/<WaitAndPrint>d__4::System.Collections.IEnumerator.Reset()
extern void U3CWaitAndPrintU3Ed__4_System_Collections_IEnumerator_Reset_m29D707B34A5DB5F8C69BB6586FD29B6069C46B85 (void);
// 0x0000026C System.Object exampleClass/<WaitAndPrint>d__4::System.Collections.IEnumerator.get_Current()
extern void U3CWaitAndPrintU3Ed__4_System_Collections_IEnumerator_get_Current_m484CB267B62893FBD4BD76BA337B8CAD696F0EF0 (void);
// 0x0000026D System.Void exeLauncher::Start()
extern void exeLauncher_Start_mF47C931BF21E799EB06E2767AC8E0318209354CD (void);
// 0x0000026E System.Void exeLauncher::calcola()
extern void exeLauncher_calcola_mC340B56D339C55B7C6B85F2723E170281ED61214 (void);
// 0x0000026F System.Void exeLauncher::AvviaServizi()
extern void exeLauncher_AvviaServizi_mA3FA8E5938A675E555EE7002CA4B8D64B54576AE (void);
// 0x00000270 System.Collections.IEnumerator exeLauncher::launchChrome()
extern void exeLauncher_launchChrome_mBCA6C8195269C8B0F28EB1293334D8302ACB7A4F (void);
// 0x00000271 System.Void exeLauncher::tastiera()
extern void exeLauncher_tastiera_mD0E0222AE231EA2F72155DA39EDB87D2A049BF21 (void);
// 0x00000272 System.Void exeLauncher::mandaWhatsup()
extern void exeLauncher_mandaWhatsup_m58BDE69EF4EE453C13290CA37A6693FB17D00149 (void);
// 0x00000273 System.Void exeLauncher::preferenze()
extern void exeLauncher_preferenze_m6380D683A77C8DBC19010759AB895D1371727AC0 (void);
// 0x00000274 System.Void exeLauncher::operatore()
extern void exeLauncher_operatore_m36AC70D7A78A0DB1B4D7417E5ED78ED0E462EA5F (void);
// 0x00000275 System.Collections.IEnumerator exeLauncher::_sendTextToFile()
extern void exeLauncher__sendTextToFile_mE36165DEE6D627D7AA689A4064CBD37A9A9DE715 (void);
// 0x00000276 System.Void exeLauncher::.ctor()
extern void exeLauncher__ctor_mF9ADCAA5310C435045EE16F529C175651A0A13EF (void);
// 0x00000277 System.Void exeLauncher/<launchChrome>d__14::.ctor(System.Int32)
extern void U3ClaunchChromeU3Ed__14__ctor_mA94C1B58D26CD87E44002F155E0BEB916DC5906C (void);
// 0x00000278 System.Void exeLauncher/<launchChrome>d__14::System.IDisposable.Dispose()
extern void U3ClaunchChromeU3Ed__14_System_IDisposable_Dispose_mCE3086F48436531A2829EEB9FD4F534563422E3E (void);
// 0x00000279 System.Boolean exeLauncher/<launchChrome>d__14::MoveNext()
extern void U3ClaunchChromeU3Ed__14_MoveNext_m9CF0CC710BE88340A215A8968BA59768D28B7168 (void);
// 0x0000027A System.Object exeLauncher/<launchChrome>d__14::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3ClaunchChromeU3Ed__14_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mE96CC10804F0AAD4E6547A009D03A5381E6F2F98 (void);
// 0x0000027B System.Void exeLauncher/<launchChrome>d__14::System.Collections.IEnumerator.Reset()
extern void U3ClaunchChromeU3Ed__14_System_Collections_IEnumerator_Reset_m3EAFEB2C8B9B725C3C67C225B265339539DC7AF9 (void);
// 0x0000027C System.Object exeLauncher/<launchChrome>d__14::System.Collections.IEnumerator.get_Current()
extern void U3ClaunchChromeU3Ed__14_System_Collections_IEnumerator_get_Current_mC524A488EC7A15ED76D300C708933BD753E99DAE (void);
// 0x0000027D System.Void exeLauncher/<_sendTextToFile>d__19::.ctor(System.Int32)
extern void U3C_sendTextToFileU3Ed__19__ctor_mE9FFABEB76B5B9CCBA6EC682C48E72A9DCB697D0 (void);
// 0x0000027E System.Void exeLauncher/<_sendTextToFile>d__19::System.IDisposable.Dispose()
extern void U3C_sendTextToFileU3Ed__19_System_IDisposable_Dispose_mD837E0E5FAE686F346B65EB74CFEC09737C6D85A (void);
// 0x0000027F System.Boolean exeLauncher/<_sendTextToFile>d__19::MoveNext()
extern void U3C_sendTextToFileU3Ed__19_MoveNext_m5EB1EA190AB3ABC98B1A2B6CEB50A055BB475B32 (void);
// 0x00000280 System.Object exeLauncher/<_sendTextToFile>d__19::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3C_sendTextToFileU3Ed__19_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mF2C1F447744A21075C51143C24705339639EBA5D (void);
// 0x00000281 System.Void exeLauncher/<_sendTextToFile>d__19::System.Collections.IEnumerator.Reset()
extern void U3C_sendTextToFileU3Ed__19_System_Collections_IEnumerator_Reset_m25ABAE019024D27530C31311990CF4CA72CDD346 (void);
// 0x00000282 System.Object exeLauncher/<_sendTextToFile>d__19::System.Collections.IEnumerator.get_Current()
extern void U3C_sendTextToFileU3Ed__19_System_Collections_IEnumerator_get_Current_m9E64F4BA0D1979835A44686AF23109DDDD13B788 (void);
// 0x00000283 System.Void globalStatus::.cctor()
extern void globalStatus__cctor_m4681F2EF2427E4CB68EB708F1F59FBEF31280A6A (void);
// 0x00000284 System.Void globalStatusEmozioni::.ctor()
extern void globalStatusEmozioni__ctor_mD8FB012EF4E15B5E08E6557BBA718E736869751C (void);
// 0x00000285 System.Void globalStatusEmozioni::.cctor()
extern void globalStatusEmozioni__cctor_m2ADB64A9441211DDFD58DA325832340188ACCB87 (void);
// 0x00000286 System.Void Keyboard::scriviKey()
extern void Keyboard_scriviKey_mAB3406F91ACDBC113D8167B3B3196460FB29C10A (void);
// 0x00000287 System.Void Keyboard::Update()
extern void Keyboard_Update_m3D5A2385BA1DFC226C5A0976A74DD5F378488E82 (void);
// 0x00000288 System.Void Keyboard::.ctor()
extern void Keyboard__ctor_mA16C7D9533AB869EDEF4DB1DBACD90819A2B4F2E (void);
// 0x00000289 System.Void keyboard::Start()
extern void keyboard_Start_m7795BC139A4BD0C3FE61A57ABF9AC16F90256BEE (void);
// 0x0000028A System.Void keyboard::Update()
extern void keyboard_Update_m3AE9F04A8442AF29FE1E21CCA891FAEB0BB4124D (void);
// 0x0000028B System.Void keyboard::.ctor()
extern void keyboard__ctor_m8EF72015AC926D09FBBF6119D3E4DBB54269BF25 (void);
// 0x0000028C System.Void mouthController::Start()
extern void mouthController_Start_m26059457DE0285D18F7ECDA5D2BB319FED2C6269 (void);
// 0x0000028D System.Void mouthController::Update()
extern void mouthController_Update_m5D7BBFD8E556D24273F963D885E2C7B257A04C37 (void);
// 0x0000028E System.Void mouthController::.ctor()
extern void mouthController__ctor_m3E457B683F82AC63A4A522BFFF63B8AEB44CC10C (void);
// 0x0000028F System.Void openLink::OnPointerClick(UnityEngine.EventSystems.PointerEventData)
extern void openLink_OnPointerClick_m8C5AADC0651CDAFB701716398718CBCF3E4C1398 (void);
// 0x00000290 System.Collections.Generic.List`1<UnityEngine.Color32[]> openLink::SetLinkToColor(System.Int32,UnityEngine.Color32)
extern void openLink_SetLinkToColor_m84F23CF13681BD5DB1A7E25962D9111F73087262 (void);
// 0x00000291 System.Void openLink::.ctor()
extern void openLink__ctor_mEC3671EE29544BA4C194039ED0B20652084C638D (void);
// 0x00000292 System.Void overrideText::Start()
extern void overrideText_Start_m6150B2946CF045C31BE0876A5BC2F3B56877E8FC (void);
// 0x00000293 System.Void overrideText::Update()
extern void overrideText_Update_m385DC9642358D59B4DD55A9C380AD8B835E979F7 (void);
// 0x00000294 System.Void overrideText::.ctor()
extern void overrideText__ctor_m4EED847BD01D416F69D6B8DBAC6F71091CFF637E (void);
// 0x00000295 System.Void sequencer::Start()
extern void sequencer_Start_m44B2EA4667EA89B1F179075E5BE1C7595A6F7394 (void);
// 0x00000296 System.Void sequencer::Update()
extern void sequencer_Update_m535C42335A7B2DC1D6D66F5F135EA62CD89670F7 (void);
// 0x00000297 System.Void sequencer::VideoAnswer()
extern void sequencer_VideoAnswer_mC501069F7DF027136AF3AC222AC0D91E74C6B89E (void);
// 0x00000298 System.Void sequencer::VideoAnswerSad()
extern void sequencer_VideoAnswerSad_mBE7CAAB9A1E188A2D7E9CD46A662CEB318EB10AE (void);
// 0x00000299 System.Collections.IEnumerator sequencer::delayPlay()
extern void sequencer_delayPlay_m2C27D9DEE2EF80430BD8E79D65DC69BDF36BDB56 (void);
// 0x0000029A System.Void sequencer::videoFakeSync()
extern void sequencer_videoFakeSync_m6344060F4F752FEBD2AFAF3187469D5DD3F746F4 (void);
// 0x0000029B System.Void sequencer::videoBottoni()
extern void sequencer_videoBottoni_m45758BE12A0380B723C159515B742ED5A7019E01 (void);
// 0x0000029C System.Void sequencer::intro()
extern void sequencer_intro_m1B99E6801F07FBE2BA9B462242F959A666DF17D8 (void);
// 0x0000029D System.Void sequencer::EndIntroReached(UnityEngine.Video.VideoPlayer)
extern void sequencer_EndIntroReached_mE5D5F901B3DDB9D7DDF9BFDD0EB5650F65AE529E (void);
// 0x0000029E System.Void sequencer::EndFinalIntroReached(UnityEngine.Video.VideoPlayer)
extern void sequencer_EndFinalIntroReached_m0A7678C38FE63487B0C4073CDFEE75C88AD28952 (void);
// 0x0000029F System.Collections.IEnumerator sequencer::introRoutine(System.Double)
extern void sequencer_introRoutine_mF694342DA868D09838A3FA0E48663B0239FEC192 (void);
// 0x000002A0 System.Collections.IEnumerator sequencer::introRoutinePart_2(System.Double)
extern void sequencer_introRoutinePart_2_mE423BE36BD46E168F951569F11E464DBCAA8D2AA (void);
// 0x000002A1 System.Collections.IEnumerator sequencer::pausaVideo()
extern void sequencer_pausaVideo_m95D64B2D60518300F1975020A6934E4477B28253 (void);
// 0x000002A2 System.Void sequencer::StopAll()
extern void sequencer_StopAll_m7FD2057845EE6C17D77F97D84EE0AF1C6097F25F (void);
// 0x000002A3 System.Collections.IEnumerator sequencer::clipBlendRoutine(System.Int32)
extern void sequencer_clipBlendRoutine_m5777CB407F60EA559971DC0D5A0780DB9CA6F21A (void);
// 0x000002A4 System.Collections.IEnumerator sequencer::playVideoTemp(System.String)
extern void sequencer_playVideoTemp_mB6DF86BDB360FCBBD7D1548EFAC4201029283510 (void);
// 0x000002A5 System.Collections.IEnumerator sequencer::playVideoWeb(System.String)
extern void sequencer_playVideoWeb_mF5AD6092008B0A53439AFCBC40E54E94D4845C40 (void);
// 0x000002A6 System.Void sequencer::loopsmileCall()
extern void sequencer_loopsmileCall_m35A836088CAA74A08DAAA049E4C312F81CB8EE46 (void);
// 0x000002A7 System.Void sequencer::saluti()
extern void sequencer_saluti_mFF9C0F8E08F9D5F333411EFC5F0D26680124EFCE (void);
// 0x000002A8 System.Collections.IEnumerator sequencer::playVideoAfterOgg(System.String)
extern void sequencer_playVideoAfterOgg_mF2B0432AD6E93B47B7A6061BA0408756605D217A (void);
// 0x000002A9 System.Void sequencer::VideoPlayer_errorReceived(UnityEngine.Video.VideoPlayer,System.String)
extern void sequencer_VideoPlayer_errorReceived_m5FD7B2D21B4784009E999730DC5AB34BF8DFDB2B (void);
// 0x000002AA System.Void sequencer::Attacco_seekCompleted(UnityEngine.Video.VideoPlayer)
extern void sequencer_Attacco_seekCompleted_m3D78B8C0129A3CA2B5F85E0EAA05E3B4B019064E (void);
// 0x000002AB System.Void sequencer::EndServerAnswer(UnityEngine.Video.VideoPlayer)
extern void sequencer_EndServerAnswer_m1CBE8E9EFA06CCF2DF16715C0A69832F5C1D34B7 (void);
// 0x000002AC System.Collections.IEnumerator sequencer::serverAnswer()
extern void sequencer_serverAnswer_m733203EB6644DCE032EA6DD2F9C62B21906C5DD7 (void);
// 0x000002AD System.Void sequencer::Prepared(UnityEngine.Video.VideoPlayer)
extern void sequencer_Prepared_m2279CFD5C1AAA16B41961279EBA6B7BAA23E4387 (void);
// 0x000002AE System.Void sequencer::EndReached(UnityEngine.Video.VideoPlayer)
extern void sequencer_EndReached_m2BBF7350C4D32232764DA0A498CF130D6E93DB0E (void);
// 0x000002AF System.Void sequencer::EndSmileReach(UnityEngine.Video.VideoPlayer)
extern void sequencer_EndSmileReach_m7245202758A4FBECBC5B8E51282B2CF84D14A7C6 (void);
// 0x000002B0 System.Void sequencer::EndLooping(UnityEngine.Video.VideoPlayer)
extern void sequencer_EndLooping_mA8A174E81B6B0C65B7DB372E1000D43DC23CEB63 (void);
// 0x000002B1 System.Collections.IEnumerator sequencer::loopVideoPlay()
extern void sequencer_loopVideoPlay_m93CD8FA5AA4518DDFE6CA097569BD60EBB7C7E91 (void);
// 0x000002B2 System.Collections.IEnumerator sequencer::disableMic()
extern void sequencer_disableMic_m8B1850F9C05C5C7DEDE1833D262C9749EC7C9E57 (void);
// 0x000002B3 System.Void sequencer::openAndroidApp()
extern void sequencer_openAndroidApp_m0708DDA16AFD5F04205EA2F65C7E0C91C1B6B1B1 (void);
// 0x000002B4 System.Void sequencer::.ctor()
extern void sequencer__ctor_mF866D4A45110DB69B67995C303CB5121B7431BF5 (void);
// 0x000002B5 System.Void sequencer/<delayPlay>d__29::.ctor(System.Int32)
extern void U3CdelayPlayU3Ed__29__ctor_m023458AEFE269E6C89507FB7218EB590E87E955C (void);
// 0x000002B6 System.Void sequencer/<delayPlay>d__29::System.IDisposable.Dispose()
extern void U3CdelayPlayU3Ed__29_System_IDisposable_Dispose_m8E5CB4A389E9B5BDCD052E072E4B28EA900D9A96 (void);
// 0x000002B7 System.Boolean sequencer/<delayPlay>d__29::MoveNext()
extern void U3CdelayPlayU3Ed__29_MoveNext_m73D0FDE4170A15C42C87A1CFE4474E1A7A859C58 (void);
// 0x000002B8 System.Object sequencer/<delayPlay>d__29::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CdelayPlayU3Ed__29_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mCF01DD925FF5A2BC33C4536771F7BB737197835D (void);
// 0x000002B9 System.Void sequencer/<delayPlay>d__29::System.Collections.IEnumerator.Reset()
extern void U3CdelayPlayU3Ed__29_System_Collections_IEnumerator_Reset_mE83F5461ACB3605475EBB779B0DDF046AF4F9C82 (void);
// 0x000002BA System.Object sequencer/<delayPlay>d__29::System.Collections.IEnumerator.get_Current()
extern void U3CdelayPlayU3Ed__29_System_Collections_IEnumerator_get_Current_mF094F9F11945B9C641F4022CC73F84B3FB0EF9D4 (void);
// 0x000002BB System.Void sequencer/<introRoutine>d__35::.ctor(System.Int32)
extern void U3CintroRoutineU3Ed__35__ctor_mD4511881F41425ED17013437A26238EFC5F557D9 (void);
// 0x000002BC System.Void sequencer/<introRoutine>d__35::System.IDisposable.Dispose()
extern void U3CintroRoutineU3Ed__35_System_IDisposable_Dispose_m75C57FD288A1193B11EBD5282EF84E447C7D75A3 (void);
// 0x000002BD System.Boolean sequencer/<introRoutine>d__35::MoveNext()
extern void U3CintroRoutineU3Ed__35_MoveNext_mCC9A692709EF57A1B637336A5F2862CE4810DD97 (void);
// 0x000002BE System.Object sequencer/<introRoutine>d__35::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CintroRoutineU3Ed__35_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mE567ED81FED53E5DC22ACD282AD8908AE93F4707 (void);
// 0x000002BF System.Void sequencer/<introRoutine>d__35::System.Collections.IEnumerator.Reset()
extern void U3CintroRoutineU3Ed__35_System_Collections_IEnumerator_Reset_mD6490C3D42B4A674F63C339FA17FA374FD95A288 (void);
// 0x000002C0 System.Object sequencer/<introRoutine>d__35::System.Collections.IEnumerator.get_Current()
extern void U3CintroRoutineU3Ed__35_System_Collections_IEnumerator_get_Current_m07BB886C11DD160FE11410B0855CD58D0E43A595 (void);
// 0x000002C1 System.Void sequencer/<introRoutinePart_2>d__36::.ctor(System.Int32)
extern void U3CintroRoutinePart_2U3Ed__36__ctor_m1CE24248CAD65B75F5AC3B9C57CCF4C972976CB3 (void);
// 0x000002C2 System.Void sequencer/<introRoutinePart_2>d__36::System.IDisposable.Dispose()
extern void U3CintroRoutinePart_2U3Ed__36_System_IDisposable_Dispose_mD9E5A332FF0A47F6511EFC435A6C07919CB157E6 (void);
// 0x000002C3 System.Boolean sequencer/<introRoutinePart_2>d__36::MoveNext()
extern void U3CintroRoutinePart_2U3Ed__36_MoveNext_m14EE9FD537E85B879F85056A879C4C5D0D42749B (void);
// 0x000002C4 System.Object sequencer/<introRoutinePart_2>d__36::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CintroRoutinePart_2U3Ed__36_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m3D544CA3FEEF56A4D99DC4621BC765425545A8CC (void);
// 0x000002C5 System.Void sequencer/<introRoutinePart_2>d__36::System.Collections.IEnumerator.Reset()
extern void U3CintroRoutinePart_2U3Ed__36_System_Collections_IEnumerator_Reset_mB12C6AC3D5C6545E4364B05824A5BCA7E283BD04 (void);
// 0x000002C6 System.Object sequencer/<introRoutinePart_2>d__36::System.Collections.IEnumerator.get_Current()
extern void U3CintroRoutinePart_2U3Ed__36_System_Collections_IEnumerator_get_Current_mEFEC7F42E9F209C8EA4A6A3D5491076CF5F19394 (void);
// 0x000002C7 System.Void sequencer/<pausaVideo>d__37::.ctor(System.Int32)
extern void U3CpausaVideoU3Ed__37__ctor_m6A237DD1D0C2BA335BAD8B14547A2DE735E707FE (void);
// 0x000002C8 System.Void sequencer/<pausaVideo>d__37::System.IDisposable.Dispose()
extern void U3CpausaVideoU3Ed__37_System_IDisposable_Dispose_m7DE30BAF2006A568D628AB0B8128BDE5254FDA44 (void);
// 0x000002C9 System.Boolean sequencer/<pausaVideo>d__37::MoveNext()
extern void U3CpausaVideoU3Ed__37_MoveNext_m0B37DA99178FD7B669AE44C5E17D3F5E605D04DD (void);
// 0x000002CA System.Object sequencer/<pausaVideo>d__37::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CpausaVideoU3Ed__37_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m4BB781283CF74A13FB5C3D1BC74DE1C392CD41FD (void);
// 0x000002CB System.Void sequencer/<pausaVideo>d__37::System.Collections.IEnumerator.Reset()
extern void U3CpausaVideoU3Ed__37_System_Collections_IEnumerator_Reset_m2ACE47CA8797EE478BF541C7A45C908AE8E30409 (void);
// 0x000002CC System.Object sequencer/<pausaVideo>d__37::System.Collections.IEnumerator.get_Current()
extern void U3CpausaVideoU3Ed__37_System_Collections_IEnumerator_get_Current_m8A9C24E784FB6EFCBC6F7764270FD1C4EB898264 (void);
// 0x000002CD System.Void sequencer/<clipBlendRoutine>d__39::.ctor(System.Int32)
extern void U3CclipBlendRoutineU3Ed__39__ctor_m0513167C124D47880554B065F732E32E60B72EFA (void);
// 0x000002CE System.Void sequencer/<clipBlendRoutine>d__39::System.IDisposable.Dispose()
extern void U3CclipBlendRoutineU3Ed__39_System_IDisposable_Dispose_mA8DCD77584821A23A580B8C361B5782460484AAC (void);
// 0x000002CF System.Boolean sequencer/<clipBlendRoutine>d__39::MoveNext()
extern void U3CclipBlendRoutineU3Ed__39_MoveNext_mBF9F452148A4653632034BAE3DA4DFAED8147157 (void);
// 0x000002D0 System.Object sequencer/<clipBlendRoutine>d__39::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CclipBlendRoutineU3Ed__39_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m9212C3113F1D91F3EF359FFAF2ADEA4083CA4FAE (void);
// 0x000002D1 System.Void sequencer/<clipBlendRoutine>d__39::System.Collections.IEnumerator.Reset()
extern void U3CclipBlendRoutineU3Ed__39_System_Collections_IEnumerator_Reset_m1491D7E4C12382C1EDB32D776FFBD7854DECC066 (void);
// 0x000002D2 System.Object sequencer/<clipBlendRoutine>d__39::System.Collections.IEnumerator.get_Current()
extern void U3CclipBlendRoutineU3Ed__39_System_Collections_IEnumerator_get_Current_mAE6D78CDCB120494C5D87AC011688A836D0DBF87 (void);
// 0x000002D3 System.Void sequencer/<playVideoTemp>d__40::.ctor(System.Int32)
extern void U3CplayVideoTempU3Ed__40__ctor_m5DB1DE4D9F1096B68A80D91596E1C6FE65192F43 (void);
// 0x000002D4 System.Void sequencer/<playVideoTemp>d__40::System.IDisposable.Dispose()
extern void U3CplayVideoTempU3Ed__40_System_IDisposable_Dispose_m200C5562175D9711226E368E8A347B65BDBF3048 (void);
// 0x000002D5 System.Boolean sequencer/<playVideoTemp>d__40::MoveNext()
extern void U3CplayVideoTempU3Ed__40_MoveNext_m372EC5CF1247FB1CE529B8BAB44179BD6FDD8DD7 (void);
// 0x000002D6 System.Object sequencer/<playVideoTemp>d__40::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CplayVideoTempU3Ed__40_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mDE4662AD9BB3CF9931EF82ADAD99444B9F6C053C (void);
// 0x000002D7 System.Void sequencer/<playVideoTemp>d__40::System.Collections.IEnumerator.Reset()
extern void U3CplayVideoTempU3Ed__40_System_Collections_IEnumerator_Reset_m9E57D1B6D239620707494DB871C165AA69C4CAA6 (void);
// 0x000002D8 System.Object sequencer/<playVideoTemp>d__40::System.Collections.IEnumerator.get_Current()
extern void U3CplayVideoTempU3Ed__40_System_Collections_IEnumerator_get_Current_m856C864380F20D4EDE78DDC337EE71984A2990E5 (void);
// 0x000002D9 System.Void sequencer/<playVideoWeb>d__41::.ctor(System.Int32)
extern void U3CplayVideoWebU3Ed__41__ctor_m03DBC883D90CB73D90E1CF808348B6F42AA51BC0 (void);
// 0x000002DA System.Void sequencer/<playVideoWeb>d__41::System.IDisposable.Dispose()
extern void U3CplayVideoWebU3Ed__41_System_IDisposable_Dispose_m6A1C0908A37CE8395C4B07E71F078AA871E7BE43 (void);
// 0x000002DB System.Boolean sequencer/<playVideoWeb>d__41::MoveNext()
extern void U3CplayVideoWebU3Ed__41_MoveNext_m16F20FCD6F2854AA18B66C9D62DFD2C7E67C0EE3 (void);
// 0x000002DC System.Object sequencer/<playVideoWeb>d__41::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CplayVideoWebU3Ed__41_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m8FA3A494572BB7E37C49A591A12DCE9BE0124858 (void);
// 0x000002DD System.Void sequencer/<playVideoWeb>d__41::System.Collections.IEnumerator.Reset()
extern void U3CplayVideoWebU3Ed__41_System_Collections_IEnumerator_Reset_m23DC531DD6A1FC5FDED8BBCD9F2241DF6C0FDAB3 (void);
// 0x000002DE System.Object sequencer/<playVideoWeb>d__41::System.Collections.IEnumerator.get_Current()
extern void U3CplayVideoWebU3Ed__41_System_Collections_IEnumerator_get_Current_m85A6DD711F0C01831828EBFAE741995EB4253E34 (void);
// 0x000002DF System.Void sequencer/<playVideoAfterOgg>d__44::.ctor(System.Int32)
extern void U3CplayVideoAfterOggU3Ed__44__ctor_m53D6EC5B96A0E3C345DABD4F4D0E8340B32AFA26 (void);
// 0x000002E0 System.Void sequencer/<playVideoAfterOgg>d__44::System.IDisposable.Dispose()
extern void U3CplayVideoAfterOggU3Ed__44_System_IDisposable_Dispose_m308BB69BA17323764DC46B437B2088610322EDC4 (void);
// 0x000002E1 System.Boolean sequencer/<playVideoAfterOgg>d__44::MoveNext()
extern void U3CplayVideoAfterOggU3Ed__44_MoveNext_m00D63ADECC606F0D21102B92BCE5CCAA6C6A75F2 (void);
// 0x000002E2 System.Object sequencer/<playVideoAfterOgg>d__44::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CplayVideoAfterOggU3Ed__44_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mB34F583ADD2FE9A78CF25A847EE032A8F24D5F44 (void);
// 0x000002E3 System.Void sequencer/<playVideoAfterOgg>d__44::System.Collections.IEnumerator.Reset()
extern void U3CplayVideoAfterOggU3Ed__44_System_Collections_IEnumerator_Reset_m4144611BFCEDEFDF31045F7F4363B4A210EB36F2 (void);
// 0x000002E4 System.Object sequencer/<playVideoAfterOgg>d__44::System.Collections.IEnumerator.get_Current()
extern void U3CplayVideoAfterOggU3Ed__44_System_Collections_IEnumerator_get_Current_mEC07399EB4789093884702EDB15B6ADF6AC7028A (void);
// 0x000002E5 System.Void sequencer/<serverAnswer>d__48::.ctor(System.Int32)
extern void U3CserverAnswerU3Ed__48__ctor_m6A7A92FEEA87A2E8E92DDB303E5D3785CC7942E9 (void);
// 0x000002E6 System.Void sequencer/<serverAnswer>d__48::System.IDisposable.Dispose()
extern void U3CserverAnswerU3Ed__48_System_IDisposable_Dispose_mC484F81B2511141889656C56B2D9808DBA35256A (void);
// 0x000002E7 System.Boolean sequencer/<serverAnswer>d__48::MoveNext()
extern void U3CserverAnswerU3Ed__48_MoveNext_m2136857CB980AD4C5E8A2EAFC21C05C51C026B92 (void);
// 0x000002E8 System.Object sequencer/<serverAnswer>d__48::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CserverAnswerU3Ed__48_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m014F771A4989B9F2938634B712BB4B50699FAFDD (void);
// 0x000002E9 System.Void sequencer/<serverAnswer>d__48::System.Collections.IEnumerator.Reset()
extern void U3CserverAnswerU3Ed__48_System_Collections_IEnumerator_Reset_mCDAD61CD7BB37FE52EFE010336E45DF69DB5E25F (void);
// 0x000002EA System.Object sequencer/<serverAnswer>d__48::System.Collections.IEnumerator.get_Current()
extern void U3CserverAnswerU3Ed__48_System_Collections_IEnumerator_get_Current_m1548162E65DA70F46F48C11113719A77EBBA6555 (void);
// 0x000002EB System.Void sequencer/<loopVideoPlay>d__53::.ctor(System.Int32)
extern void U3CloopVideoPlayU3Ed__53__ctor_m70D40300314FFD6B16AED50E3542946D07197535 (void);
// 0x000002EC System.Void sequencer/<loopVideoPlay>d__53::System.IDisposable.Dispose()
extern void U3CloopVideoPlayU3Ed__53_System_IDisposable_Dispose_m26A043E63DD995EA9575BA5BE1185B876211B4D1 (void);
// 0x000002ED System.Boolean sequencer/<loopVideoPlay>d__53::MoveNext()
extern void U3CloopVideoPlayU3Ed__53_MoveNext_m6BB8D9A55CB2C93DCFAC6AF282F9D16B8E22B6A5 (void);
// 0x000002EE System.Object sequencer/<loopVideoPlay>d__53::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CloopVideoPlayU3Ed__53_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mEFD5BCB35D349C5A91E6B2C02F9BD3AC09A71AFC (void);
// 0x000002EF System.Void sequencer/<loopVideoPlay>d__53::System.Collections.IEnumerator.Reset()
extern void U3CloopVideoPlayU3Ed__53_System_Collections_IEnumerator_Reset_m1D4755D5A33141216DB874577F07BC1B5FAB65BB (void);
// 0x000002F0 System.Object sequencer/<loopVideoPlay>d__53::System.Collections.IEnumerator.get_Current()
extern void U3CloopVideoPlayU3Ed__53_System_Collections_IEnumerator_get_Current_m128DB3233632224FEAC6FF021B83A9EA6D6C84FB (void);
// 0x000002F1 System.Void sequencer/<disableMic>d__54::.ctor(System.Int32)
extern void U3CdisableMicU3Ed__54__ctor_m29BFBD22091AF6A22313F28A619FB7F1089779C0 (void);
// 0x000002F2 System.Void sequencer/<disableMic>d__54::System.IDisposable.Dispose()
extern void U3CdisableMicU3Ed__54_System_IDisposable_Dispose_m52DDC7F3C2823F6D01F9379E295935587AE5D284 (void);
// 0x000002F3 System.Boolean sequencer/<disableMic>d__54::MoveNext()
extern void U3CdisableMicU3Ed__54_MoveNext_mDF20A351C2D4A49B6A0EEFE73280EE445C5C0070 (void);
// 0x000002F4 System.Object sequencer/<disableMic>d__54::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CdisableMicU3Ed__54_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mAC3CF3E21B98C243CD7C30E5923D066452A190AF (void);
// 0x000002F5 System.Void sequencer/<disableMic>d__54::System.Collections.IEnumerator.Reset()
extern void U3CdisableMicU3Ed__54_System_Collections_IEnumerator_Reset_m9F909940FD093B5EF92539B5F150A693AD1B90EA (void);
// 0x000002F6 System.Object sequencer/<disableMic>d__54::System.Collections.IEnumerator.get_Current()
extern void U3CdisableMicU3Ed__54_System_Collections_IEnumerator_get_Current_m3E4DFB35BF5BF11128B24968423BBD9BF2F24AFF (void);
// 0x000002F7 System.Void stateMachine::stoppaDetection()
extern void stateMachine_stoppaDetection_mB7D04DEED6218242C96FFB1CE248FBF45B4C0A1D (void);
// 0x000002F8 System.Void stateMachine::.ctor()
extern void stateMachine__ctor_m0915C2A8F1B05569C3557CB569D43E973D606A3A (void);
// 0x000002F9 System.String stringaUnivoca::ComputeHash(System.String)
extern void stringaUnivoca_ComputeHash_mDE4C8E9C4EF20A3FF1412E422A37184C7AED8DB4 (void);
// 0x000002FA System.Void stringaUnivoca::Start()
extern void stringaUnivoca_Start_mAD21C041B109D6FBA0356E6C674077B24D03FD67 (void);
// 0x000002FB System.Void stringaUnivoca::.ctor()
extern void stringaUnivoca__ctor_m0387A6028500A77743195875A93057773E8DE859 (void);
// 0x000002FC System.Void tastieraAndroid::Start()
extern void tastieraAndroid_Start_mEF6EE8B0BB3C11AF51F3B27791D87457409A0748 (void);
// 0x000002FD System.Void tastieraAndroid::scriviKey()
extern void tastieraAndroid_scriviKey_m3EF72BD3069B33D1F7D1A9971B58CDE945860C91 (void);
// 0x000002FE System.Void tastieraAndroid::sendKeyboard()
extern void tastieraAndroid_sendKeyboard_m9B980AA9F15BE0CB35A881A78DB1E38DA8EBDE0C (void);
// 0x000002FF System.Void tastieraAndroid::Update()
extern void tastieraAndroid_Update_mFEEAA7DCA0BA0653C6DB2ADA328B3700E44D7E45 (void);
// 0x00000300 System.Collections.IEnumerator tastieraAndroid::mandaEngineKeyboard()
extern void tastieraAndroid_mandaEngineKeyboard_m8B94726EBA2064621D43EC48324DD9DC485E8CB0 (void);
// 0x00000301 System.Void tastieraAndroid::.ctor()
extern void tastieraAndroid__ctor_mFB3D68AB0542B5974A0B51F814A65603C19FF649 (void);
// 0x00000302 System.Void tastieraAndroid/<mandaEngineKeyboard>d__7::.ctor(System.Int32)
extern void U3CmandaEngineKeyboardU3Ed__7__ctor_mA34BC55FF6FE43098072C9FAB99A4278F901B48E (void);
// 0x00000303 System.Void tastieraAndroid/<mandaEngineKeyboard>d__7::System.IDisposable.Dispose()
extern void U3CmandaEngineKeyboardU3Ed__7_System_IDisposable_Dispose_mDBB540F6CD0AACAC51C9BFAA1551C810F5726E20 (void);
// 0x00000304 System.Boolean tastieraAndroid/<mandaEngineKeyboard>d__7::MoveNext()
extern void U3CmandaEngineKeyboardU3Ed__7_MoveNext_m4A5FB063C3BEC62AD02268EC4B7DFDE38BDCD412 (void);
// 0x00000305 System.Object tastieraAndroid/<mandaEngineKeyboard>d__7::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CmandaEngineKeyboardU3Ed__7_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m33E0B67C389877142FB78A7CDD956ACA9F1CFE6C (void);
// 0x00000306 System.Void tastieraAndroid/<mandaEngineKeyboard>d__7::System.Collections.IEnumerator.Reset()
extern void U3CmandaEngineKeyboardU3Ed__7_System_Collections_IEnumerator_Reset_m665DDB67539ACABA1230337C5962CE3D1508CCDE (void);
// 0x00000307 System.Object tastieraAndroid/<mandaEngineKeyboard>d__7::System.Collections.IEnumerator.get_Current()
extern void U3CmandaEngineKeyboardU3Ed__7_System_Collections_IEnumerator_get_Current_m9BF8AB2847D11FE6587683203B39421E2EA71A01 (void);
// 0x00000308 System.Void textFadein::Start()
extern void textFadein_Start_mFE97649FB0528D95C75DDD86338C532E0B2FB042 (void);
// 0x00000309 System.Void textFadein::animaTesti()
extern void textFadein_animaTesti_mD2B040FD63DB912F6F9AE13BDFBF813BA1034593 (void);
// 0x0000030A System.Void textFadein::.ctor()
extern void textFadein__ctor_mD803BE7CA3EB995768AC395C1F6ABB2F15C271C6 (void);
// 0x0000030B System.Collections.IEnumerator tgMachine::playVideoWeb(System.String)
extern void tgMachine_playVideoWeb_m854F5591CFB54AB7DB63163307980E95509E2A1D (void);
// 0x0000030C System.Void tgMachine::EndReached(UnityEngine.Video.VideoPlayer)
extern void tgMachine_EndReached_m77B3A1C86FF6CB6E388DB2E28F28CD91D5E99F33 (void);
// 0x0000030D System.Void tgMachine::Start()
extern void tgMachine_Start_m43F13A903C986E4874258A2E7FB32E376DD131AA (void);
// 0x0000030E System.Void tgMachine::.ctor()
extern void tgMachine__ctor_m06D8F6E445AF3C7BB374A8A96679A1CB29F5CD8C (void);
// 0x0000030F System.Void tgMachine/<playVideoWeb>d__4::.ctor(System.Int32)
extern void U3CplayVideoWebU3Ed__4__ctor_mFDDFCC811DD203AB80E56BCE5919B3F6499A0D0F (void);
// 0x00000310 System.Void tgMachine/<playVideoWeb>d__4::System.IDisposable.Dispose()
extern void U3CplayVideoWebU3Ed__4_System_IDisposable_Dispose_mA98BBBCA44DEC675DDF92323FF15401827DFAF2D (void);
// 0x00000311 System.Boolean tgMachine/<playVideoWeb>d__4::MoveNext()
extern void U3CplayVideoWebU3Ed__4_MoveNext_m7A62325F7B8583323C13B2CBFA286FBEDBC0B9FD (void);
// 0x00000312 System.Object tgMachine/<playVideoWeb>d__4::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CplayVideoWebU3Ed__4_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mBE034BE6A3F16D48E59ACFB54593D2CAEBCE4EC5 (void);
// 0x00000313 System.Void tgMachine/<playVideoWeb>d__4::System.Collections.IEnumerator.Reset()
extern void U3CplayVideoWebU3Ed__4_System_Collections_IEnumerator_Reset_mB77D255826B74B07B73795021853D04BF5562382 (void);
// 0x00000314 System.Object tgMachine/<playVideoWeb>d__4::System.Collections.IEnumerator.get_Current()
extern void U3CplayVideoWebU3Ed__4_System_Collections_IEnumerator_get_Current_mC3EE2120157846EBF1EBD006C5DE866B5A38019E (void);
// 0x00000315 System.Void touchKeyboard::OnGUI()
extern void touchKeyboard_OnGUI_mB6C97AFDFE7ECDCFC55A614DA1788A13097C1ED9 (void);
// 0x00000316 System.Void touchKeyboard::.ctor()
extern void touchKeyboard__ctor_m2B1BAE54BD4EF12A33F109772F673D4DD89CE317 (void);
// 0x00000317 System.Void unity2engine::Start()
extern void unity2engine_Start_m5175819313CE50497C155746A23470ACC7D13BBB (void);
// 0x00000318 System.Collections.IEnumerator unity2engine::startSend()
extern void unity2engine_startSend_m321911D47EBFCE1E23A32EFAC18185CC2506D90A (void);
// 0x00000319 System.Void unity2engine::startButton()
extern void unity2engine_startButton_m2CB7D7427131CADC8CFBC7FA292CBC1859134ECA (void);
// 0x0000031A System.Void unity2engine::sendManually()
extern void unity2engine_sendManually_m5B339B0C9D831A5761D054E79E2EB9053A93C1AE (void);
// 0x0000031B System.Void unity2engine::sendTellme()
extern void unity2engine_sendTellme_m55CD37C6B59AB9C1246FB2D5C25C8E5C583D75C8 (void);
// 0x0000031C System.Void unity2engine::Update()
extern void unity2engine_Update_m4DE0DC87A8FCBED30BFDD5BC12CDCCA38BF3999F (void);
// 0x0000031D System.Void unity2engine::rispostaProxy()
extern void unity2engine_rispostaProxy_m4DF4A1C0E37B49871650116FBC37A9B8BDF11613 (void);
// 0x0000031E System.Void unity2engine::OnRequestFinished(BestHTTP.HTTPRequest,BestHTTP.HTTPResponse)
extern void unity2engine_OnRequestFinished_mEBC636E25FB27E4FCB7B2C821F1A9648E43FC0E2 (void);
// 0x0000031F System.Collections.IEnumerator unity2engine::Send(System.String)
extern void unity2engine_Send_m434835E81BE8D4F5E4E4BBB9D1A8AF2C6B4FBAA7 (void);
// 0x00000320 System.Collections.IEnumerator unity2engine::LoadUrlImage()
extern void unity2engine_LoadUrlImage_mE1F0A9052142EB6EB137801B6930C6EF81C0DD95 (void);
// 0x00000321 System.Void unity2engine::stoppami()
extern void unity2engine_stoppami_m5B833D6E8705B0C8537FE075EEB3EA7638B4146D (void);
// 0x00000322 System.Collections.IEnumerator unity2engine::GetMyAudioClip()
extern void unity2engine_GetMyAudioClip_mBC12DA73CBC3E9893E1F2A0DD81A9359A63AE040 (void);
// 0x00000323 System.Collections.IEnumerator unity2engine::resetSpeec()
extern void unity2engine_resetSpeec_m1CAE9638961AFB8832AAB68A63D9062A0DC05151 (void);
// 0x00000324 System.Void unity2engine::stopVoice()
extern void unity2engine_stopVoice_mFC27B1C4ADECEBC1504CDB18C11695E183BC4917 (void);
// 0x00000325 System.Void unity2engine::quit()
extern void unity2engine_quit_mCDDF0869EB661CC8337DC8A882ECCC4DF2C081E9 (void);
// 0x00000326 System.Collections.IEnumerator unity2engine::disabilitaMic()
extern void unity2engine_disabilitaMic_m74084AB89F41943C2A6E7BDE61D8899DB6B8CDEE (void);
// 0x00000327 System.Void unity2engine::changeServer()
extern void unity2engine_changeServer_m14D344C93610422269CBDC2A12D549CB2E7CC8BB (void);
// 0x00000328 System.Collections.IEnumerator unity2engine::sendNextCall()
extern void unity2engine_sendNextCall_m302AA3682E0C8BE49DE5D9AC06011ACD228D5D8F (void);
// 0x00000329 System.Void unity2engine::.ctor()
extern void unity2engine__ctor_m9E79A3E125FFF7D49B24F36DE071F211893165B6 (void);
// 0x0000032A System.Void unity2engine::<Update>b__47_0()
extern void unity2engine_U3CUpdateU3Eb__47_0_m2B41AAD71A3507D1F5AAD9C0F316AD37367501BB (void);
// 0x0000032B System.Void unity2engine::<rispostaProxy>b__48_0()
extern void unity2engine_U3CrispostaProxyU3Eb__48_0_m0197B58F268D4B94BF5852F2B92F7B53697F6E6D (void);
// 0x0000032C System.Collections.IEnumerator unity2engine::<rispostaProxy>g__delayAnswerDialog|48_1()
extern void unity2engine_U3CrispostaProxyU3Eg__delayAnswerDialogU7C48_1_m49B440A7640DFB04DA419AD1D84F4F6C8F3EE065 (void);
// 0x0000032D System.Void unity2engine::<rispostaProxy>b__48_2()
extern void unity2engine_U3CrispostaProxyU3Eb__48_2_m04AE9DD66D2DC7ADEC3BBAA14735C2CA5ED85632 (void);
// 0x0000032E System.Void unity2engine::<rispostaProxy>b__48_3()
extern void unity2engine_U3CrispostaProxyU3Eb__48_3_m8720BDF3113659B89A9D05195A78444B6171DC7D (void);
// 0x0000032F System.Void unity2engine::<GetMyAudioClip>b__53_0()
extern void unity2engine_U3CGetMyAudioClipU3Eb__53_0_m300B40256A4AC105E60EB63A9353390C0A0548EE (void);
// 0x00000330 System.Void unity2engine/<startSend>d__43::.ctor(System.Int32)
extern void U3CstartSendU3Ed__43__ctor_mABE45B7A5D4DFE8CB7CC21A67A059CC4DC81F28F (void);
// 0x00000331 System.Void unity2engine/<startSend>d__43::System.IDisposable.Dispose()
extern void U3CstartSendU3Ed__43_System_IDisposable_Dispose_m1A65B71059DD4A8036AD70192B26CEC5AC2B1C3F (void);
// 0x00000332 System.Boolean unity2engine/<startSend>d__43::MoveNext()
extern void U3CstartSendU3Ed__43_MoveNext_m457A6142454ADD744C073C91ACCEB146BD4E0082 (void);
// 0x00000333 System.Object unity2engine/<startSend>d__43::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CstartSendU3Ed__43_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m41BA86D16BB674D64E8E2D6B8EE8471FB91F7CF0 (void);
// 0x00000334 System.Void unity2engine/<startSend>d__43::System.Collections.IEnumerator.Reset()
extern void U3CstartSendU3Ed__43_System_Collections_IEnumerator_Reset_m478B1F7CFBEA44DE9BA70326A9FEE9A0DE5175A0 (void);
// 0x00000335 System.Object unity2engine/<startSend>d__43::System.Collections.IEnumerator.get_Current()
extern void U3CstartSendU3Ed__43_System_Collections_IEnumerator_get_Current_m82AA44A6D44A1821CDD808C30707B3DE522AEA49 (void);
// 0x00000336 System.Void unity2engine/<>c__DisplayClass45_0::.ctor()
extern void U3CU3Ec__DisplayClass45_0__ctor_m8B28D1040E90986EF352BE4A86A67A4E9E5C1A3C (void);
// 0x00000337 System.Void unity2engine/<>c__DisplayClass45_0::<sendManually>b__0()
extern void U3CU3Ec__DisplayClass45_0_U3CsendManuallyU3Eb__0_m14679AC3D5C767ADA07474605922D8C90632718F (void);
// 0x00000338 System.Void unity2engine/<>c__DisplayClass46_0::.ctor()
extern void U3CU3Ec__DisplayClass46_0__ctor_m4EFBF0AB5674AD2B4313883762DF58FAB283E8D0 (void);
// 0x00000339 System.Void unity2engine/<>c__DisplayClass46_0::<sendTellme>b__0()
extern void U3CU3Ec__DisplayClass46_0_U3CsendTellmeU3Eb__0_m12A29FB81F91368978DA25A88F8334710C5735E4 (void);
// 0x0000033A System.Void unity2engine/<Send>d__50::.ctor(System.Int32)
extern void U3CSendU3Ed__50__ctor_m5511BA409171BB1D2955B7EC06615B79F9A95641 (void);
// 0x0000033B System.Void unity2engine/<Send>d__50::System.IDisposable.Dispose()
extern void U3CSendU3Ed__50_System_IDisposable_Dispose_mB2CEBC254E504CE801A1DE9BCF462A2FFBFF768F (void);
// 0x0000033C System.Boolean unity2engine/<Send>d__50::MoveNext()
extern void U3CSendU3Ed__50_MoveNext_m2AA60459AC5E2DCF1CF85A1D57D72CC4EEF97583 (void);
// 0x0000033D System.Object unity2engine/<Send>d__50::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CSendU3Ed__50_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m26BF695B36BCE2579D017BF81731AF3913FE2BE7 (void);
// 0x0000033E System.Void unity2engine/<Send>d__50::System.Collections.IEnumerator.Reset()
extern void U3CSendU3Ed__50_System_Collections_IEnumerator_Reset_m9DB1F403F265F482CF5A80E898D67FE1C10D32E5 (void);
// 0x0000033F System.Object unity2engine/<Send>d__50::System.Collections.IEnumerator.get_Current()
extern void U3CSendU3Ed__50_System_Collections_IEnumerator_get_Current_mFAE7C5F81C2E395631303EDA9D43CD079EB5A32B (void);
// 0x00000340 System.Void unity2engine/<LoadUrlImage>d__51::.ctor(System.Int32)
extern void U3CLoadUrlImageU3Ed__51__ctor_mA86D936CAEE70220FCC1A4D38B8128815DF58E67 (void);
// 0x00000341 System.Void unity2engine/<LoadUrlImage>d__51::System.IDisposable.Dispose()
extern void U3CLoadUrlImageU3Ed__51_System_IDisposable_Dispose_m386E166BE1079315E128C8E00B7E29BC4EE1544B (void);
// 0x00000342 System.Boolean unity2engine/<LoadUrlImage>d__51::MoveNext()
extern void U3CLoadUrlImageU3Ed__51_MoveNext_m4A91E41809E754F6285D48178543E98EAB24CC58 (void);
// 0x00000343 System.Object unity2engine/<LoadUrlImage>d__51::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CLoadUrlImageU3Ed__51_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mFA84CE1307DB3465668DEB293FCB28AE1651859E (void);
// 0x00000344 System.Void unity2engine/<LoadUrlImage>d__51::System.Collections.IEnumerator.Reset()
extern void U3CLoadUrlImageU3Ed__51_System_Collections_IEnumerator_Reset_m2EC283C3C6365FA41B51DB5EE873AC6DA0EE2F9C (void);
// 0x00000345 System.Object unity2engine/<LoadUrlImage>d__51::System.Collections.IEnumerator.get_Current()
extern void U3CLoadUrlImageU3Ed__51_System_Collections_IEnumerator_get_Current_m617F5B8C0D6243FB933E392A56D92CB6F2D019EB (void);
// 0x00000346 System.Void unity2engine/<GetMyAudioClip>d__53::.ctor(System.Int32)
extern void U3CGetMyAudioClipU3Ed__53__ctor_mA2CDB381213F68AC219C244C04003E5525E7B79E (void);
// 0x00000347 System.Void unity2engine/<GetMyAudioClip>d__53::System.IDisposable.Dispose()
extern void U3CGetMyAudioClipU3Ed__53_System_IDisposable_Dispose_mAA34BD019AB2C8BCD5978E2A4DFA46F77C708BEB (void);
// 0x00000348 System.Boolean unity2engine/<GetMyAudioClip>d__53::MoveNext()
extern void U3CGetMyAudioClipU3Ed__53_MoveNext_m18E9475D7C5CC4FA4D3C60A3AD1A2953AD870C43 (void);
// 0x00000349 System.Object unity2engine/<GetMyAudioClip>d__53::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CGetMyAudioClipU3Ed__53_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mF79557ECC35B7E252751E6A790658A9DE32CC143 (void);
// 0x0000034A System.Void unity2engine/<GetMyAudioClip>d__53::System.Collections.IEnumerator.Reset()
extern void U3CGetMyAudioClipU3Ed__53_System_Collections_IEnumerator_Reset_mAA5114FD75953F9B3EE32CA740ACB812ED056EBB (void);
// 0x0000034B System.Object unity2engine/<GetMyAudioClip>d__53::System.Collections.IEnumerator.get_Current()
extern void U3CGetMyAudioClipU3Ed__53_System_Collections_IEnumerator_get_Current_mB52588418975771332C78D37CC81672F01571C3F (void);
// 0x0000034C System.Void unity2engine/<resetSpeec>d__54::.ctor(System.Int32)
extern void U3CresetSpeecU3Ed__54__ctor_m80FD103D8EDCD4188C1146F2B5B9883B59113040 (void);
// 0x0000034D System.Void unity2engine/<resetSpeec>d__54::System.IDisposable.Dispose()
extern void U3CresetSpeecU3Ed__54_System_IDisposable_Dispose_m959473E11085E6F96D6B7C0D750CE50576526417 (void);
// 0x0000034E System.Boolean unity2engine/<resetSpeec>d__54::MoveNext()
extern void U3CresetSpeecU3Ed__54_MoveNext_m0B0D5E146545ED26E4DBB6F4412A7D6E316A63CB (void);
// 0x0000034F System.Object unity2engine/<resetSpeec>d__54::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CresetSpeecU3Ed__54_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m6A40EC4795918DDC43840B6F2FE1646876EB98D7 (void);
// 0x00000350 System.Void unity2engine/<resetSpeec>d__54::System.Collections.IEnumerator.Reset()
extern void U3CresetSpeecU3Ed__54_System_Collections_IEnumerator_Reset_m6E55BAF67AFC3B3A7BE5109493C5DF4EE38EAE1A (void);
// 0x00000351 System.Object unity2engine/<resetSpeec>d__54::System.Collections.IEnumerator.get_Current()
extern void U3CresetSpeecU3Ed__54_System_Collections_IEnumerator_get_Current_mEA9BEC7C98C1517E059C529A76CA9E01462FAE21 (void);
// 0x00000352 System.Void unity2engine/<disabilitaMic>d__57::.ctor(System.Int32)
extern void U3CdisabilitaMicU3Ed__57__ctor_m879FB5BD8ED3F548FCD93504C71B04D19D395303 (void);
// 0x00000353 System.Void unity2engine/<disabilitaMic>d__57::System.IDisposable.Dispose()
extern void U3CdisabilitaMicU3Ed__57_System_IDisposable_Dispose_mA12069290B5D0B7D598A670277EFBCC5047AE453 (void);
// 0x00000354 System.Boolean unity2engine/<disabilitaMic>d__57::MoveNext()
extern void U3CdisabilitaMicU3Ed__57_MoveNext_m5F2549822F11220E0F60345BC0BC72553B659A13 (void);
// 0x00000355 System.Object unity2engine/<disabilitaMic>d__57::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CdisabilitaMicU3Ed__57_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m7AAD39ACF4E129473817CDEA08D87E66632AD2CA (void);
// 0x00000356 System.Void unity2engine/<disabilitaMic>d__57::System.Collections.IEnumerator.Reset()
extern void U3CdisabilitaMicU3Ed__57_System_Collections_IEnumerator_Reset_m8D57CF9DC2383B7023F1D35C3DA9F85B5EC56BC8 (void);
// 0x00000357 System.Object unity2engine/<disabilitaMic>d__57::System.Collections.IEnumerator.get_Current()
extern void U3CdisabilitaMicU3Ed__57_System_Collections_IEnumerator_get_Current_m85EA32C68D8EAD2C1AD3F525BD0D6C62B35FC8D5 (void);
// 0x00000358 System.Void unity2engine/<sendNextCall>d__59::.ctor(System.Int32)
extern void U3CsendNextCallU3Ed__59__ctor_m5F8A96508471B1B1520CED4AD07D62A467E6C2CD (void);
// 0x00000359 System.Void unity2engine/<sendNextCall>d__59::System.IDisposable.Dispose()
extern void U3CsendNextCallU3Ed__59_System_IDisposable_Dispose_m1A30D808BE5628F58CE2C8C00621980D3A7FE361 (void);
// 0x0000035A System.Boolean unity2engine/<sendNextCall>d__59::MoveNext()
extern void U3CsendNextCallU3Ed__59_MoveNext_m35D68003DF2A346167FC126B2514F5C6FBF57C88 (void);
// 0x0000035B System.Object unity2engine/<sendNextCall>d__59::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CsendNextCallU3Ed__59_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mFB110A759F853D0834BFF8F662E0E99027E814BE (void);
// 0x0000035C System.Void unity2engine/<sendNextCall>d__59::System.Collections.IEnumerator.Reset()
extern void U3CsendNextCallU3Ed__59_System_Collections_IEnumerator_Reset_mD7E10B923091F7B297B6D2A5445E081A286E8A12 (void);
// 0x0000035D System.Object unity2engine/<sendNextCall>d__59::System.Collections.IEnumerator.get_Current()
extern void U3CsendNextCallU3Ed__59_System_Collections_IEnumerator_get_Current_m1017D6432A8000B5E78AD544A327DE8AF8E40AC0 (void);
// 0x0000035E System.Void unity2engine/<<rispostaProxy>g__delayAnswerDialog|48_1>d::.ctor(System.Int32)
extern void U3CU3CrispostaProxyU3Eg__delayAnswerDialogU7C48_1U3Ed__ctor_m7DEC9C3073DCE991F97956BFD402FAABFEC283ED (void);
// 0x0000035F System.Void unity2engine/<<rispostaProxy>g__delayAnswerDialog|48_1>d::System.IDisposable.Dispose()
extern void U3CU3CrispostaProxyU3Eg__delayAnswerDialogU7C48_1U3Ed_System_IDisposable_Dispose_m9158747AF96044DCCA0E22DB9E362C155692DF77 (void);
// 0x00000360 System.Boolean unity2engine/<<rispostaProxy>g__delayAnswerDialog|48_1>d::MoveNext()
extern void U3CU3CrispostaProxyU3Eg__delayAnswerDialogU7C48_1U3Ed_MoveNext_m2848B0B330C614DC9533BE19FD4013275F46BBEF (void);
// 0x00000361 System.Object unity2engine/<<rispostaProxy>g__delayAnswerDialog|48_1>d::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CU3CrispostaProxyU3Eg__delayAnswerDialogU7C48_1U3Ed_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m6B39C334166543EB98E9258AD17C3B5866C3079D (void);
// 0x00000362 System.Void unity2engine/<<rispostaProxy>g__delayAnswerDialog|48_1>d::System.Collections.IEnumerator.Reset()
extern void U3CU3CrispostaProxyU3Eg__delayAnswerDialogU7C48_1U3Ed_System_Collections_IEnumerator_Reset_mBDFBAA86A871C69C49CC7A8AAA16748DB8965441 (void);
// 0x00000363 System.Object unity2engine/<<rispostaProxy>g__delayAnswerDialog|48_1>d::System.Collections.IEnumerator.get_Current()
extern void U3CU3CrispostaProxyU3Eg__delayAnswerDialogU7C48_1U3Ed_System_Collections_IEnumerator_get_Current_m0D8F77607022AA470B75662B49795A061AC9640C (void);
// 0x00000364 System.Void videoPlayManager::Update()
extern void videoPlayManager_Update_mD229529B877AC71064CD4CD084950BE3779D2E49 (void);
// 0x00000365 System.Collections.IEnumerator videoPlayManager::playVideoTemp(System.String)
extern void videoPlayManager_playVideoTemp_mA1AEB0A158F357CEB7B403C1B52F40B20F726A2A (void);
// 0x00000366 System.Collections.IEnumerator videoPlayManager::playVideoTempDebug()
extern void videoPlayManager_playVideoTempDebug_mA2C1D0EA019F115C7B1B251D6E3D86AA664BD6FE (void);
// 0x00000367 System.Collections.IEnumerator videoPlayManager::playHalf()
extern void videoPlayManager_playHalf_m259FCC0B88F9B9E2DEDE448142FC7E7AC08ADAE1 (void);
// 0x00000368 System.Void videoPlayManager::EndReached(UnityEngine.Video.VideoPlayer)
extern void videoPlayManager_EndReached_mFFA64140AEB73FF22F2422B136DFE7AFE0F69255 (void);
// 0x00000369 System.Void videoPlayManager::EndLoopReached(UnityEngine.Video.VideoPlayer)
extern void videoPlayManager_EndLoopReached_mF2C1045EFF0F9676278DE837BC177558F0488A04 (void);
// 0x0000036A System.Collections.IEnumerator videoPlayManager::closeVideo()
extern void videoPlayManager_closeVideo_mEB6BB921CD5893C54BBFB02F8A0A3A3B0D624FB1 (void);
// 0x0000036B System.Collections.IEnumerator videoPlayManager::prepareVideo()
extern void videoPlayManager_prepareVideo_mC2D50395D9848930F076DA5A1A265FFED084ED73 (void);
// 0x0000036C System.Void videoPlayManager::playClippa()
extern void videoPlayManager_playClippa_m6D6DAB4D013CC0D3B02A9214B53E70A7E45D318E (void);
// 0x0000036D System.Void videoPlayManager::Start()
extern void videoPlayManager_Start_mA17355AE81D3EE8F6BE8BD81B59647DA8187BB9C (void);
// 0x0000036E System.Void videoPlayManager::.ctor()
extern void videoPlayManager__ctor_m1C03C9D18EDB8D36F62477CC11255634A240E01A (void);
// 0x0000036F System.Void videoPlayManager/<playVideoTemp>d__9::.ctor(System.Int32)
extern void U3CplayVideoTempU3Ed__9__ctor_mA25E1BFDEE14F31D96EDCC1379AD59595406D0BB (void);
// 0x00000370 System.Void videoPlayManager/<playVideoTemp>d__9::System.IDisposable.Dispose()
extern void U3CplayVideoTempU3Ed__9_System_IDisposable_Dispose_mB7FC958AB60F24DA21E0F6D5CBA7533167027B74 (void);
// 0x00000371 System.Boolean videoPlayManager/<playVideoTemp>d__9::MoveNext()
extern void U3CplayVideoTempU3Ed__9_MoveNext_m87B4218212236BBE8400D83973D9EB6C54AC5196 (void);
// 0x00000372 System.Object videoPlayManager/<playVideoTemp>d__9::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CplayVideoTempU3Ed__9_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m566974F7912B2FB6C4CE6184A4855E89B44B2EC3 (void);
// 0x00000373 System.Void videoPlayManager/<playVideoTemp>d__9::System.Collections.IEnumerator.Reset()
extern void U3CplayVideoTempU3Ed__9_System_Collections_IEnumerator_Reset_m1848685EF5E4C0A786ACA426C217B2E6F773D8F2 (void);
// 0x00000374 System.Object videoPlayManager/<playVideoTemp>d__9::System.Collections.IEnumerator.get_Current()
extern void U3CplayVideoTempU3Ed__9_System_Collections_IEnumerator_get_Current_m4B98D4B0178E6BCAE109F9C0FC8917C2BE380105 (void);
// 0x00000375 System.Void videoPlayManager/<playVideoTempDebug>d__10::.ctor(System.Int32)
extern void U3CplayVideoTempDebugU3Ed__10__ctor_m205A0380F7F90DDD5093ADC36D692E2902B590EE (void);
// 0x00000376 System.Void videoPlayManager/<playVideoTempDebug>d__10::System.IDisposable.Dispose()
extern void U3CplayVideoTempDebugU3Ed__10_System_IDisposable_Dispose_m10AD654020361853C3AF65FF7AB6D4109FA23D0A (void);
// 0x00000377 System.Boolean videoPlayManager/<playVideoTempDebug>d__10::MoveNext()
extern void U3CplayVideoTempDebugU3Ed__10_MoveNext_m4E9A647F4BC8510B2860D42B61B0F9071BD17340 (void);
// 0x00000378 System.Object videoPlayManager/<playVideoTempDebug>d__10::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CplayVideoTempDebugU3Ed__10_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m2B184855AE0236F734B376CA1D14B684A9F14460 (void);
// 0x00000379 System.Void videoPlayManager/<playVideoTempDebug>d__10::System.Collections.IEnumerator.Reset()
extern void U3CplayVideoTempDebugU3Ed__10_System_Collections_IEnumerator_Reset_m13BAB237EEC71EA8243CB68B8D461DF5B2587A02 (void);
// 0x0000037A System.Object videoPlayManager/<playVideoTempDebug>d__10::System.Collections.IEnumerator.get_Current()
extern void U3CplayVideoTempDebugU3Ed__10_System_Collections_IEnumerator_get_Current_m0EF65A6FF3B7555B8FD2808295E4F9731026CB74 (void);
// 0x0000037B System.Void videoPlayManager/<playHalf>d__11::.ctor(System.Int32)
extern void U3CplayHalfU3Ed__11__ctor_m14E8757DE092AC0FA256267397586BF238953DC2 (void);
// 0x0000037C System.Void videoPlayManager/<playHalf>d__11::System.IDisposable.Dispose()
extern void U3CplayHalfU3Ed__11_System_IDisposable_Dispose_mC0F3DCA5D04B2A172FAB282BD950A0FEA8B2604C (void);
// 0x0000037D System.Boolean videoPlayManager/<playHalf>d__11::MoveNext()
extern void U3CplayHalfU3Ed__11_MoveNext_mD8ADEA9220F35125D2A959F6650F5B33F66CE837 (void);
// 0x0000037E System.Object videoPlayManager/<playHalf>d__11::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CplayHalfU3Ed__11_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mD1E8DDB8029F3516686393A264D808A54E631900 (void);
// 0x0000037F System.Void videoPlayManager/<playHalf>d__11::System.Collections.IEnumerator.Reset()
extern void U3CplayHalfU3Ed__11_System_Collections_IEnumerator_Reset_m941B7C51362BE7ED32FAD78A03A5E55A8B57E265 (void);
// 0x00000380 System.Object videoPlayManager/<playHalf>d__11::System.Collections.IEnumerator.get_Current()
extern void U3CplayHalfU3Ed__11_System_Collections_IEnumerator_get_Current_m222A406BEE6B957CC04213404CDE0A2F065DD915 (void);
// 0x00000381 System.Void videoPlayManager/<closeVideo>d__14::.ctor(System.Int32)
extern void U3CcloseVideoU3Ed__14__ctor_m62F71233100BFB92436AF800319981A1843A8E70 (void);
// 0x00000382 System.Void videoPlayManager/<closeVideo>d__14::System.IDisposable.Dispose()
extern void U3CcloseVideoU3Ed__14_System_IDisposable_Dispose_m7BA2FE06A34AA7D25B44A0530E4B0C492AA08967 (void);
// 0x00000383 System.Boolean videoPlayManager/<closeVideo>d__14::MoveNext()
extern void U3CcloseVideoU3Ed__14_MoveNext_m598CF056ABECD6D327A0BA80F68B1D268CC4CBA2 (void);
// 0x00000384 System.Object videoPlayManager/<closeVideo>d__14::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CcloseVideoU3Ed__14_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m1345F35C619FDD292BDDFAD401DD9D52FDCB35FA (void);
// 0x00000385 System.Void videoPlayManager/<closeVideo>d__14::System.Collections.IEnumerator.Reset()
extern void U3CcloseVideoU3Ed__14_System_Collections_IEnumerator_Reset_mE69FD69FC79D5DA8478575CFD234DADF20D20D01 (void);
// 0x00000386 System.Object videoPlayManager/<closeVideo>d__14::System.Collections.IEnumerator.get_Current()
extern void U3CcloseVideoU3Ed__14_System_Collections_IEnumerator_get_Current_mE3B6D4FC9881998E91B65CBC12274A10640A0ECB (void);
// 0x00000387 System.Void videoPlayManager/<prepareVideo>d__15::.ctor(System.Int32)
extern void U3CprepareVideoU3Ed__15__ctor_mE3D473058FCF64ECD2F96BDED323E8B5F036AD74 (void);
// 0x00000388 System.Void videoPlayManager/<prepareVideo>d__15::System.IDisposable.Dispose()
extern void U3CprepareVideoU3Ed__15_System_IDisposable_Dispose_m8401F0BEA8363C3CEBFC094D8AF91DF3381D20D9 (void);
// 0x00000389 System.Boolean videoPlayManager/<prepareVideo>d__15::MoveNext()
extern void U3CprepareVideoU3Ed__15_MoveNext_mEED81159BCA037BACDE4D6A6B20DD0DBA02FD88D (void);
// 0x0000038A System.Object videoPlayManager/<prepareVideo>d__15::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CprepareVideoU3Ed__15_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m816179CAC29DD055098B694C6EDF902E5900A72F (void);
// 0x0000038B System.Void videoPlayManager/<prepareVideo>d__15::System.Collections.IEnumerator.Reset()
extern void U3CprepareVideoU3Ed__15_System_Collections_IEnumerator_Reset_m5EA75C0A274DF34BED9F22608285CD2A841D6EBF (void);
// 0x0000038C System.Object videoPlayManager/<prepareVideo>d__15::System.Collections.IEnumerator.get_Current()
extern void U3CprepareVideoU3Ed__15_System_Collections_IEnumerator_get_Current_mC5F7D452BD004CE0BA47C1CC711DF200A247864F (void);
// 0x0000038D System.Void videoSequencer::Start()
extern void videoSequencer_Start_mB23D407476876245637EF3E6D8FD9C3D055558D9 (void);
// 0x0000038E System.Collections.IEnumerator videoSequencer::playVideo(System.Boolean)
extern void videoSequencer_playVideo_mE4FA3DD412FE13A1DBE9EC1169C1460CD5C33374 (void);
// 0x0000038F System.Void videoSequencer::.ctor()
extern void videoSequencer__ctor_mB3F2CE16043374E87D94476758E6AC926051D1E7 (void);
// 0x00000390 System.Void videoSequencer/<playVideo>d__5::.ctor(System.Int32)
extern void U3CplayVideoU3Ed__5__ctor_m00B3388F912439CE2FF9DFA734F37005794806A3 (void);
// 0x00000391 System.Void videoSequencer/<playVideo>d__5::System.IDisposable.Dispose()
extern void U3CplayVideoU3Ed__5_System_IDisposable_Dispose_m65C9226619D8F46157CDB526B292D0217D17EF3C (void);
// 0x00000392 System.Boolean videoSequencer/<playVideo>d__5::MoveNext()
extern void U3CplayVideoU3Ed__5_MoveNext_m32197296869AF17D942C7528CE1FDBAFBFD8C222 (void);
// 0x00000393 System.Object videoSequencer/<playVideo>d__5::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CplayVideoU3Ed__5_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m9E4C3CD352511F808CD8537F9A86AF7F863077EF (void);
// 0x00000394 System.Void videoSequencer/<playVideo>d__5::System.Collections.IEnumerator.Reset()
extern void U3CplayVideoU3Ed__5_System_Collections_IEnumerator_Reset_m228B5CCDF6DA29AB5C0BB04D9DC08161A8419BED (void);
// 0x00000395 System.Object videoSequencer/<playVideo>d__5::System.Collections.IEnumerator.get_Current()
extern void U3CplayVideoU3Ed__5_System_Collections_IEnumerator_get_Current_mCED4A54324DF9D93832EDF01F77605FAC2464F3B (void);
// 0x00000396 System.Void aliasing::Start()
extern void aliasing_Start_m32F9CB9275F87F217C9A116E3DE800B7044A4A63 (void);
// 0x00000397 System.Void aliasing::Update()
extern void aliasing_Update_mC8C290FCB0D6FB3D208EF8069780534A086A84B7 (void);
// 0x00000398 System.Void aliasing::.ctor()
extern void aliasing__ctor_mEE63D43E883AD51E2E8780F1B38B684AE036E4A5 (void);
// 0x00000399 System.Void disableEnableAudio::Start()
extern void disableEnableAudio_Start_mF24740D7E7C304BAA1ADAED68FA462642C8F1357 (void);
// 0x0000039A System.Void disableEnableAudio::disableAudioRec()
extern void disableEnableAudio_disableAudioRec_m517C35AB19D4C5062D8319EF4D0ADBB5EB6EE20C (void);
// 0x0000039B System.Collections.IEnumerator disableEnableAudio::delay(System.Int32)
extern void disableEnableAudio_delay_mC5461D5A21F48828D010C6ABD533A8FCAD022BF8 (void);
// 0x0000039C System.Void disableEnableAudio::.ctor()
extern void disableEnableAudio__ctor_m1A75E575058F9E9069695D5FC2FF42A32C23A59A (void);
// 0x0000039D System.Void disableEnableAudio/<delay>d__4::.ctor(System.Int32)
extern void U3CdelayU3Ed__4__ctor_mD6DB4C2A2D072FAC408B455DA1D2BD1C0D5863E8 (void);
// 0x0000039E System.Void disableEnableAudio/<delay>d__4::System.IDisposable.Dispose()
extern void U3CdelayU3Ed__4_System_IDisposable_Dispose_mDD9B74D48087F66A9C041A64746BA6C46C19F72F (void);
// 0x0000039F System.Boolean disableEnableAudio/<delay>d__4::MoveNext()
extern void U3CdelayU3Ed__4_MoveNext_m0AA5743CF7E88594BE59643BE0101D7905E73936 (void);
// 0x000003A0 System.Object disableEnableAudio/<delay>d__4::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CdelayU3Ed__4_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m02A170592C68E149E49D7461108F904716CAE1F1 (void);
// 0x000003A1 System.Void disableEnableAudio/<delay>d__4::System.Collections.IEnumerator.Reset()
extern void U3CdelayU3Ed__4_System_Collections_IEnumerator_Reset_mAF189F8F46DF5BADE6C2ADDBD64EE0EE7DB00CB6 (void);
// 0x000003A2 System.Object disableEnableAudio/<delay>d__4::System.Collections.IEnumerator.get_Current()
extern void U3CdelayU3Ed__4_System_Collections_IEnumerator_get_Current_m4781475F251F2C885549C56096A2E15E2876F648 (void);
// 0x000003A3 System.Void SceneController::awake()
extern void SceneController_awake_mA47004C4963E3D21FC298E078522923CCE8CA56A (void);
// 0x000003A4 System.Void SceneController::Update()
extern void SceneController_Update_m4E482D951B939F4031265B2193BC129EAC917AD7 (void);
// 0x000003A5 System.Void SceneController::.ctor()
extern void SceneController__ctor_mD43BE19257E8699CFE380AD920050CE78721E604 (void);
// 0x000003A6 System.Void load::Start()
extern void load_Start_m67E642A2D27E744C2DD1553EC699B37B3217F6BB (void);
// 0x000003A7 System.Void load::Update()
extern void load_Update_mBA00AA2B3558B5E3759A76B4E4D5EFC302A0B7FF (void);
// 0x000003A8 System.Void load::.ctor()
extern void load__ctor_mACD0C604AEFEB63F31D7749644CD519BE1007957 (void);
// 0x000003A9 System.Void load4principle::Start()
extern void load4principle_Start_mEDF4CFCF73F250FC8F8076FE15605C3EDBE57AA5 (void);
// 0x000003AA System.Void load4principle::Update()
extern void load4principle_Update_m704E9C74957BB53AC4059D9469BC87C005628BB7 (void);
// 0x000003AB System.Void load4principle::.ctor()
extern void load4principle__ctor_m3A9D435000D9ED330ACEFCB3EC9B0E31BFCAB549 (void);
// 0x000003AC System.Void soundtravel::Start()
extern void soundtravel_Start_m7B9DE1B6B90B1B199E03A33E41715CB8D49F2700 (void);
// 0x000003AD System.Void soundtravel::Update()
extern void soundtravel_Update_mD90ACBE57D58A63F5E955FD20779B573A9A34CD1 (void);
// 0x000003AE System.Void soundtravel::.ctor()
extern void soundtravel__ctor_mEB87DCEDC95D08CAA9CB3FDB2E41BF9F58175020 (void);
// 0x000003AF System.Collections.IEnumerator variazione::Start()
extern void variazione_Start_m31019B4E4C9FDBF1F1B0B33E2E78974351D3001E (void);
// 0x000003B0 System.Void variazione::Update()
extern void variazione_Update_m1297AE0151C0BD7F9B53EEDF160CF8216F39E815 (void);
// 0x000003B1 System.Collections.IEnumerator variazione::WaitAndPrint(System.Single)
extern void variazione_WaitAndPrint_m38FFAF59698F6009BD14FE7C2145E6DB2941FB8A (void);
// 0x000003B2 System.Void variazione::.ctor()
extern void variazione__ctor_m6A7F8043D77D1692A1F53EE2DDF88B81CB52B1AF (void);
// 0x000003B3 System.Void variazione/<Start>d__4::.ctor(System.Int32)
extern void U3CStartU3Ed__4__ctor_m27EE29CF5AAC580BB19C67E82250A6788A54A94A (void);
// 0x000003B4 System.Void variazione/<Start>d__4::System.IDisposable.Dispose()
extern void U3CStartU3Ed__4_System_IDisposable_Dispose_mF7394E30CF998B7B304A715FE81B82AC8E427F6C (void);
// 0x000003B5 System.Boolean variazione/<Start>d__4::MoveNext()
extern void U3CStartU3Ed__4_MoveNext_m75ED99DAFCFA53F1A9956306C21E1D3BBDF3149E (void);
// 0x000003B6 System.Object variazione/<Start>d__4::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CStartU3Ed__4_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mE5845366DA537A53692735DD7D5F183C4F4B5D8F (void);
// 0x000003B7 System.Void variazione/<Start>d__4::System.Collections.IEnumerator.Reset()
extern void U3CStartU3Ed__4_System_Collections_IEnumerator_Reset_mB9A0A536A6FC4775A24355A5FFD7FC991664C303 (void);
// 0x000003B8 System.Object variazione/<Start>d__4::System.Collections.IEnumerator.get_Current()
extern void U3CStartU3Ed__4_System_Collections_IEnumerator_get_Current_mB5CA2F0BEBC84005DACE725143D53ECE31D69E43 (void);
// 0x000003B9 System.Void variazione/<WaitAndPrint>d__6::.ctor(System.Int32)
extern void U3CWaitAndPrintU3Ed__6__ctor_m4D4390329168BAB6B32E48982CA150FF4684685C (void);
// 0x000003BA System.Void variazione/<WaitAndPrint>d__6::System.IDisposable.Dispose()
extern void U3CWaitAndPrintU3Ed__6_System_IDisposable_Dispose_m3CF5BBB5785F05DA969896B0BF6A27B79EEDE871 (void);
// 0x000003BB System.Boolean variazione/<WaitAndPrint>d__6::MoveNext()
extern void U3CWaitAndPrintU3Ed__6_MoveNext_mB6A99D0EB0E9A57F191FCC2EA489B62F19FD8790 (void);
// 0x000003BC System.Object variazione/<WaitAndPrint>d__6::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CWaitAndPrintU3Ed__6_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mF153EF72BD1C389E5DC76BFE11518647EA1C4C30 (void);
// 0x000003BD System.Void variazione/<WaitAndPrint>d__6::System.Collections.IEnumerator.Reset()
extern void U3CWaitAndPrintU3Ed__6_System_Collections_IEnumerator_Reset_m5A5A9D921C78F9F9412DFD0D3BE532FD43B4F0E2 (void);
// 0x000003BE System.Object variazione/<WaitAndPrint>d__6::System.Collections.IEnumerator.get_Current()
extern void U3CWaitAndPrintU3Ed__6_System_Collections_IEnumerator_get_Current_m97E9AD5E843D0727F9EBD43BE5D9FBA34410E8F2 (void);
// 0x000003BF System.Void UnityWebGLSpeechDetection.micController::Awake()
extern void micController_Awake_m950344DD270F1A375AA6D7BA4DF376B80BB6EED2 (void);
// 0x000003C0 System.Collections.IEnumerator UnityWebGLSpeechDetection.micController::Start()
extern void micController_Start_m2CE7D573AF10FFF50F4F6C6662EE2615AD8FFB39 (void);
// 0x000003C1 System.Boolean UnityWebGLSpeechDetection.micController::HandleDetectionResult(UnityWebGLSpeechDetection.DetectionResult)
extern void micController_HandleDetectionResult_mA99A0C098823BD612C634FBCBAAA87D38F2DD49C (void);
// 0x000003C2 System.Void UnityWebGLSpeechDetection.micController::interrompiDisab()
extern void micController_interrompiDisab_m80F76EAC94E0DED1DED94ED560CB93B377431C6A (void);
// 0x000003C3 System.Void UnityWebGLSpeechDetection.micController::terzoNomatchAzioni()
extern void micController_terzoNomatchAzioni_m9618D6733A2018AC74AE126D6B04AC23D8A9754E (void);
// 0x000003C4 System.Void UnityWebGLSpeechDetection.micController::EndReached(UnityEngine.Video.VideoPlayer)
extern void micController_EndReached_m443717AEF48134F6235CC2EA1DD36EB75E8CCE6B (void);
// 0x000003C5 System.Void UnityWebGLSpeechDetection.micController::nomeAccettato()
extern void micController_nomeAccettato_m3774E0DA7707673CEFB14B1C4584F38F98ABEFF0 (void);
// 0x000003C6 System.Void UnityWebGLSpeechDetection.micController::numeroAccettato()
extern void micController_numeroAccettato_mA05190B597F03CA8FF9C48A1FEE416D976158FA9 (void);
// 0x000003C7 System.Collections.IEnumerator UnityWebGLSpeechDetection.micController::whatsapp()
extern void micController_whatsapp_m763EDEF0E10A41CA57C1EB000BA2E10CF2D88691 (void);
// 0x000003C8 System.Collections.IEnumerator UnityWebGLSpeechDetection.micController::stoppati()
extern void micController_stoppati_mF38A912EAFC291C7A60A48DC363A99D1E5124185 (void);
// 0x000003C9 System.Void UnityWebGLSpeechDetection.micController::interrompimi()
extern void micController_interrompimi_m141A3F49E2A373B092EAD1C8A3ADC003695CECBF (void);
// 0x000003CA System.Collections.IEnumerator UnityWebGLSpeechDetection.micController::messaggioInvioBrochure()
extern void micController_messaggioInvioBrochure_mED877F47A372C5690CE275746C4E52C959597AC9 (void);
// 0x000003CB System.Void UnityWebGLSpeechDetection.micController::mandaConferma()
extern void micController_mandaConferma_m17DE2BD73491E05A90BCDBF72FFE57D263049425 (void);
// 0x000003CC System.Void UnityWebGLSpeechDetection.micController::Update()
extern void micController_Update_mF1B1B13660055773C447F1B8F28A3845F6AF4764 (void);
// 0x000003CD System.Void UnityWebGLSpeechDetection.micController::.ctor()
extern void micController__ctor_m72348C6DDA4F766D0E00AFD5F377336BC0C6EAC2 (void);
// 0x000003CE System.Void UnityWebGLSpeechDetection.micController::<Start>b__34_0(UnityWebGLSpeechDetection.LanguageResult)
extern void micController_U3CStartU3Eb__34_0_m9EBD4700C0EC351DE682F41C974B1CA5E4E8ECCD (void);
// 0x000003CF System.Void UnityWebGLSpeechDetection.micController::<Start>b__34_1(System.Int32)
extern void micController_U3CStartU3Eb__34_1_mF5D4D8DA66B276C856F1E202AB39C117096B0933 (void);
// 0x000003D0 System.Void UnityWebGLSpeechDetection.micController::<Start>b__34_2(System.Int32)
extern void micController_U3CStartU3Eb__34_2_mA3981D138FDA1E614EF78710C40BB59E7F8C1BAE (void);
// 0x000003D1 System.Void UnityWebGLSpeechDetection.micController/<Start>d__34::.ctor(System.Int32)
extern void U3CStartU3Ed__34__ctor_m61C18E778D4C5AE15B75FE3CB2065D568347C96A (void);
// 0x000003D2 System.Void UnityWebGLSpeechDetection.micController/<Start>d__34::System.IDisposable.Dispose()
extern void U3CStartU3Ed__34_System_IDisposable_Dispose_m4A033915BC64DDC54D96C772CEA1D33117F24A32 (void);
// 0x000003D3 System.Boolean UnityWebGLSpeechDetection.micController/<Start>d__34::MoveNext()
extern void U3CStartU3Ed__34_MoveNext_mBE4FE80B5B57EB56468CAC97325E2D3425AE8E5F (void);
// 0x000003D4 System.Object UnityWebGLSpeechDetection.micController/<Start>d__34::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CStartU3Ed__34_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m8D2B6B0CC17948635BBECA686EED7A5AB1FEC3BD (void);
// 0x000003D5 System.Void UnityWebGLSpeechDetection.micController/<Start>d__34::System.Collections.IEnumerator.Reset()
extern void U3CStartU3Ed__34_System_Collections_IEnumerator_Reset_m60C60E66D9BADC265905FD9E1ED67FDB7D8515E5 (void);
// 0x000003D6 System.Object UnityWebGLSpeechDetection.micController/<Start>d__34::System.Collections.IEnumerator.get_Current()
extern void U3CStartU3Ed__34_System_Collections_IEnumerator_get_Current_m9094F8D72E04247CBBD44D8ED8E65C6822239DB5 (void);
// 0x000003D7 System.Void UnityWebGLSpeechDetection.micController/<whatsapp>d__41::.ctor(System.Int32)
extern void U3CwhatsappU3Ed__41__ctor_m74E09730CEAC086E195EE169F691DDA9322A6509 (void);
// 0x000003D8 System.Void UnityWebGLSpeechDetection.micController/<whatsapp>d__41::System.IDisposable.Dispose()
extern void U3CwhatsappU3Ed__41_System_IDisposable_Dispose_mA9CC498E1D3222F749413F2DF5D0322A991889E4 (void);
// 0x000003D9 System.Boolean UnityWebGLSpeechDetection.micController/<whatsapp>d__41::MoveNext()
extern void U3CwhatsappU3Ed__41_MoveNext_m5AB9FB600D15C3FB1606B2ECEAA74E457DCED52D (void);
// 0x000003DA System.Object UnityWebGLSpeechDetection.micController/<whatsapp>d__41::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CwhatsappU3Ed__41_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mB3B9C47DC7615DA62187DA4AAB81AA408305D3BD (void);
// 0x000003DB System.Void UnityWebGLSpeechDetection.micController/<whatsapp>d__41::System.Collections.IEnumerator.Reset()
extern void U3CwhatsappU3Ed__41_System_Collections_IEnumerator_Reset_mF91F61D50FAE3E2E65CDD503C934FD619B8F776A (void);
// 0x000003DC System.Object UnityWebGLSpeechDetection.micController/<whatsapp>d__41::System.Collections.IEnumerator.get_Current()
extern void U3CwhatsappU3Ed__41_System_Collections_IEnumerator_get_Current_mAC018A9AFFC5463B889E9D52EBDA33EDBA750B4F (void);
// 0x000003DD System.Void UnityWebGLSpeechDetection.micController/<stoppati>d__42::.ctor(System.Int32)
extern void U3CstoppatiU3Ed__42__ctor_m07127D75FB4BF5EC244B49BF6737D8A825EDB0F7 (void);
// 0x000003DE System.Void UnityWebGLSpeechDetection.micController/<stoppati>d__42::System.IDisposable.Dispose()
extern void U3CstoppatiU3Ed__42_System_IDisposable_Dispose_m0AF950F187C2DA1723400DAFEE05B7CE8B8662C7 (void);
// 0x000003DF System.Boolean UnityWebGLSpeechDetection.micController/<stoppati>d__42::MoveNext()
extern void U3CstoppatiU3Ed__42_MoveNext_m56C536FF453E44A853A5696609FCA9D12EF42C08 (void);
// 0x000003E0 System.Object UnityWebGLSpeechDetection.micController/<stoppati>d__42::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CstoppatiU3Ed__42_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m2A2D2B0FE64C2B951EBDD45465CE3F116B6C6E80 (void);
// 0x000003E1 System.Void UnityWebGLSpeechDetection.micController/<stoppati>d__42::System.Collections.IEnumerator.Reset()
extern void U3CstoppatiU3Ed__42_System_Collections_IEnumerator_Reset_m4FC7CEF78B2D035040B648FB29D70AE7009FF229 (void);
// 0x000003E2 System.Object UnityWebGLSpeechDetection.micController/<stoppati>d__42::System.Collections.IEnumerator.get_Current()
extern void U3CstoppatiU3Ed__42_System_Collections_IEnumerator_get_Current_m7626B063192E312E3E18EDFD5675318E986E1489 (void);
// 0x000003E3 System.Void UnityWebGLSpeechDetection.micController/<messaggioInvioBrochure>d__44::.ctor(System.Int32)
extern void U3CmessaggioInvioBrochureU3Ed__44__ctor_mC5C861AFFF08FF6A66E0B1604E6E22CA2339087B (void);
// 0x000003E4 System.Void UnityWebGLSpeechDetection.micController/<messaggioInvioBrochure>d__44::System.IDisposable.Dispose()
extern void U3CmessaggioInvioBrochureU3Ed__44_System_IDisposable_Dispose_m9E32FE4DC0A5965CF51554B03F040579016CEAB7 (void);
// 0x000003E5 System.Boolean UnityWebGLSpeechDetection.micController/<messaggioInvioBrochure>d__44::MoveNext()
extern void U3CmessaggioInvioBrochureU3Ed__44_MoveNext_mF4D497B117A5C302A9DDCAA34A9C1D3183BE8C51 (void);
// 0x000003E6 System.Object UnityWebGLSpeechDetection.micController/<messaggioInvioBrochure>d__44::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CmessaggioInvioBrochureU3Ed__44_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m76E0EB7C41673B7DB03154497385715070C721D8 (void);
// 0x000003E7 System.Void UnityWebGLSpeechDetection.micController/<messaggioInvioBrochure>d__44::System.Collections.IEnumerator.Reset()
extern void U3CmessaggioInvioBrochureU3Ed__44_System_Collections_IEnumerator_Reset_m307936C90A499E64E03A97625D8157EF4F0C1E77 (void);
// 0x000003E8 System.Object UnityWebGLSpeechDetection.micController/<messaggioInvioBrochure>d__44::System.Collections.IEnumerator.get_Current()
extern void U3CmessaggioInvioBrochureU3Ed__44_System_Collections_IEnumerator_get_Current_m2549A86672E78591D2132D73869CC13CBB022B26 (void);
// 0x000003E9 System.Char TMPro.TMP_DigitValidator::Validate(System.String&,System.Int32&,System.Char)
extern void TMP_DigitValidator_Validate_m5D303EB8CD6E9E7D526D633BA1021884051FE226 (void);
// 0x000003EA System.Void TMPro.TMP_DigitValidator::.ctor()
extern void TMP_DigitValidator__ctor_m1E838567EF38170662F1BAF52A847CC2C258333E (void);
// 0x000003EB System.Char TMPro.TMP_PhoneNumberValidator::Validate(System.String&,System.Int32&,System.Char)
extern void TMP_PhoneNumberValidator_Validate_mF0E90A277E9E91BC213DD02DC60088D03C9436B1 (void);
// 0x000003EC System.Void TMPro.TMP_PhoneNumberValidator::.ctor()
extern void TMP_PhoneNumberValidator__ctor_mB3C36CAAE3B52554C44A1D19194F0176B5A8EED3 (void);
// 0x000003ED TMPro.TMP_TextEventHandler/CharacterSelectionEvent TMPro.TMP_TextEventHandler::get_onCharacterSelection()
extern void TMP_TextEventHandler_get_onCharacterSelection_m90C39320C726E8E542D91F4BBD690697349F5385 (void);
// 0x000003EE System.Void TMPro.TMP_TextEventHandler::set_onCharacterSelection(TMPro.TMP_TextEventHandler/CharacterSelectionEvent)
extern void TMP_TextEventHandler_set_onCharacterSelection_m5ED7658EB101C6740A921FA150DE18C443BDA0C4 (void);
// 0x000003EF TMPro.TMP_TextEventHandler/SpriteSelectionEvent TMPro.TMP_TextEventHandler::get_onSpriteSelection()
extern void TMP_TextEventHandler_get_onSpriteSelection_m0E645AE1DFE19B011A3319474D0CF0DA612C8B7B (void);
// 0x000003F0 System.Void TMPro.TMP_TextEventHandler::set_onSpriteSelection(TMPro.TMP_TextEventHandler/SpriteSelectionEvent)
extern void TMP_TextEventHandler_set_onSpriteSelection_m4AFC6772C3357218956A5D33B3CD19F3AAF39788 (void);
// 0x000003F1 TMPro.TMP_TextEventHandler/WordSelectionEvent TMPro.TMP_TextEventHandler::get_onWordSelection()
extern void TMP_TextEventHandler_get_onWordSelection_mA42B89A37810FB659FCFA8539339A3BB8037203A (void);
// 0x000003F2 System.Void TMPro.TMP_TextEventHandler::set_onWordSelection(TMPro.TMP_TextEventHandler/WordSelectionEvent)
extern void TMP_TextEventHandler_set_onWordSelection_m4A839FAFFABAFECD073B82BA8826E1CD033C0076 (void);
// 0x000003F3 TMPro.TMP_TextEventHandler/LineSelectionEvent TMPro.TMP_TextEventHandler::get_onLineSelection()
extern void TMP_TextEventHandler_get_onLineSelection_mB701B6C713AD4EFC61E1B30A564EE54ADE31F58D (void);
// 0x000003F4 System.Void TMPro.TMP_TextEventHandler::set_onLineSelection(TMPro.TMP_TextEventHandler/LineSelectionEvent)
extern void TMP_TextEventHandler_set_onLineSelection_m0B2337598350E51D0A17B8FCB3AAA533F312F3DA (void);
// 0x000003F5 TMPro.TMP_TextEventHandler/LinkSelectionEvent TMPro.TMP_TextEventHandler::get_onLinkSelection()
extern void TMP_TextEventHandler_get_onLinkSelection_mF5C3875D661F5B1E3712566FE16D332EA37D15BB (void);
// 0x000003F6 System.Void TMPro.TMP_TextEventHandler::set_onLinkSelection(TMPro.TMP_TextEventHandler/LinkSelectionEvent)
extern void TMP_TextEventHandler_set_onLinkSelection_m200566EDEB2C9299647F3EEAC588B51818D360A5 (void);
// 0x000003F7 System.Void TMPro.TMP_TextEventHandler::Awake()
extern void TMP_TextEventHandler_Awake_m43EB03A4A6776A624F79457EC49E78E7B5BA1C70 (void);
// 0x000003F8 System.Void TMPro.TMP_TextEventHandler::LateUpdate()
extern void TMP_TextEventHandler_LateUpdate_mE1D989C40DA8E54E116E3C60217DFCAADD6FDE11 (void);
// 0x000003F9 System.Void TMPro.TMP_TextEventHandler::OnPointerEnter(UnityEngine.EventSystems.PointerEventData)
extern void TMP_TextEventHandler_OnPointerEnter_m8FC88F25858B24CE68BE80C727A3F0227A8EE5AC (void);
// 0x000003FA System.Void TMPro.TMP_TextEventHandler::OnPointerExit(UnityEngine.EventSystems.PointerEventData)
extern void TMP_TextEventHandler_OnPointerExit_mBB2A74D55741F631A678A5D6997D40247FE75D44 (void);
// 0x000003FB System.Void TMPro.TMP_TextEventHandler::SendOnCharacterSelection(System.Char,System.Int32)
extern void TMP_TextEventHandler_SendOnCharacterSelection_m78983D3590F1B0C242BEAB0A11FDBDABDD1814EC (void);
// 0x000003FC System.Void TMPro.TMP_TextEventHandler::SendOnSpriteSelection(System.Char,System.Int32)
extern void TMP_TextEventHandler_SendOnSpriteSelection_m10C257A74F121B95E7077F7E488FBC52380A6C53 (void);
// 0x000003FD System.Void TMPro.TMP_TextEventHandler::SendOnWordSelection(System.String,System.Int32,System.Int32)
extern void TMP_TextEventHandler_SendOnWordSelection_m95A4E5A00E339E5B8BA9AA63B98DB3E81C8B8F66 (void);
// 0x000003FE System.Void TMPro.TMP_TextEventHandler::SendOnLineSelection(System.String,System.Int32,System.Int32)
extern void TMP_TextEventHandler_SendOnLineSelection_mE85BA6ECE1188B666FD36839B8970C3E0130BC43 (void);
// 0x000003FF System.Void TMPro.TMP_TextEventHandler::SendOnLinkSelection(System.String,System.String,System.Int32)
extern void TMP_TextEventHandler_SendOnLinkSelection_m299E6620DFD835C8258A3F48B4EA076C304E9B77 (void);
// 0x00000400 System.Void TMPro.TMP_TextEventHandler::.ctor()
extern void TMP_TextEventHandler__ctor_m6345DC1CEA2E4209E928AD1E61C3ACA4227DD9B8 (void);
// 0x00000401 System.Void TMPro.TMP_TextEventHandler/CharacterSelectionEvent::.ctor()
extern void CharacterSelectionEvent__ctor_m06BC183AF31BA4A2055A44514BC3FF0539DD04C7 (void);
// 0x00000402 System.Void TMPro.TMP_TextEventHandler/SpriteSelectionEvent::.ctor()
extern void SpriteSelectionEvent__ctor_m0F760052E9A5AF44A7AF7AC006CB4B24809590F2 (void);
// 0x00000403 System.Void TMPro.TMP_TextEventHandler/WordSelectionEvent::.ctor()
extern void WordSelectionEvent__ctor_m106CDEB17C520C9D20CE7120DE6BBBDEDB48886C (void);
// 0x00000404 System.Void TMPro.TMP_TextEventHandler/LineSelectionEvent::.ctor()
extern void LineSelectionEvent__ctor_m5D735FDA9B71B9147C6F791B331498F145D75018 (void);
// 0x00000405 System.Void TMPro.TMP_TextEventHandler/LinkSelectionEvent::.ctor()
extern void LinkSelectionEvent__ctor_m7AB7977D0D0F8883C5A98DD6BB2D390BC3CAB8E0 (void);
// 0x00000406 System.Collections.IEnumerator TMPro.Examples.Benchmark01::Start()
extern void Benchmark01_Start_mE7E5146B0D8D926CC410CAA48F3B617A0A7D955C (void);
// 0x00000407 System.Void TMPro.Examples.Benchmark01::.ctor()
extern void Benchmark01__ctor_mB92568AA7A9E13B92315B6270FCA23584A7D0F7F (void);
// 0x00000408 System.Void TMPro.Examples.Benchmark01/<Start>d__10::.ctor(System.Int32)
extern void U3CStartU3Ed__10__ctor_mBE5D8B4B98C372BD6DC3936437999DF3048DE3AB (void);
// 0x00000409 System.Void TMPro.Examples.Benchmark01/<Start>d__10::System.IDisposable.Dispose()
extern void U3CStartU3Ed__10_System_IDisposable_Dispose_m1F4180A7FDAE9AC5F11F30B67F813B3D7CD56D73 (void);
// 0x0000040A System.Boolean TMPro.Examples.Benchmark01/<Start>d__10::MoveNext()
extern void U3CStartU3Ed__10_MoveNext_mF3B1D38CD37FD5187C3192141DB380747C66AD3C (void);
// 0x0000040B System.Object TMPro.Examples.Benchmark01/<Start>d__10::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CStartU3Ed__10_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m22B2975D42326E3DE437174BBE4F0E8790CB6591 (void);
// 0x0000040C System.Void TMPro.Examples.Benchmark01/<Start>d__10::System.Collections.IEnumerator.Reset()
extern void U3CStartU3Ed__10_System_Collections_IEnumerator_Reset_m514DA545ECA10AA62BFC239BB582FF0017B91D2B (void);
// 0x0000040D System.Object TMPro.Examples.Benchmark01/<Start>d__10::System.Collections.IEnumerator.get_Current()
extern void U3CStartU3Ed__10_System_Collections_IEnumerator_get_Current_m6904A738F04118BA24E473F1809B3D69E711627E (void);
// 0x0000040E System.Collections.IEnumerator TMPro.Examples.Benchmark01_UGUI::Start()
extern void Benchmark01_UGUI_Start_m2F8F9E1798943DA8086A7C7E73BA9D7C67482BD4 (void);
// 0x0000040F System.Void TMPro.Examples.Benchmark01_UGUI::.ctor()
extern void Benchmark01_UGUI__ctor_mACFE8D997EAB50FDBD7F671C1A25A892D9F78376 (void);
// 0x00000410 System.Void TMPro.Examples.Benchmark01_UGUI/<Start>d__10::.ctor(System.Int32)
extern void U3CStartU3Ed__10__ctor_m653B757B49A674E239C804FFF4EDEF325B5DB651 (void);
// 0x00000411 System.Void TMPro.Examples.Benchmark01_UGUI/<Start>d__10::System.IDisposable.Dispose()
extern void U3CStartU3Ed__10_System_IDisposable_Dispose_mAD0BC985E1E01C8369A7A955A60D3E545B0B3561 (void);
// 0x00000412 System.Boolean TMPro.Examples.Benchmark01_UGUI/<Start>d__10::MoveNext()
extern void U3CStartU3Ed__10_MoveNext_mF9B2C8D290035BC9A79C4347772B5B7B9D404DE1 (void);
// 0x00000413 System.Object TMPro.Examples.Benchmark01_UGUI/<Start>d__10::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CStartU3Ed__10_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mF438D42D7C5A811FF9A283F8780F015FF55826CD (void);
// 0x00000414 System.Void TMPro.Examples.Benchmark01_UGUI/<Start>d__10::System.Collections.IEnumerator.Reset()
extern void U3CStartU3Ed__10_System_Collections_IEnumerator_Reset_m65545EB5B6DDDDAB5ACF92600137C1C754B322BA (void);
// 0x00000415 System.Object TMPro.Examples.Benchmark01_UGUI/<Start>d__10::System.Collections.IEnumerator.get_Current()
extern void U3CStartU3Ed__10_System_Collections_IEnumerator_get_Current_mD4523BE1D1014AAF1C0709EB767C0B0F47D739D2 (void);
// 0x00000416 System.Void TMPro.Examples.Benchmark02::Start()
extern void Benchmark02_Start_m315C0DF3A9AC66B4F5802FDAED056E7E5F3D2046 (void);
// 0x00000417 System.Void TMPro.Examples.Benchmark02::.ctor()
extern void Benchmark02__ctor_m526B38D3B7E75905208E4E57B168D8AC1900F4E5 (void);
// 0x00000418 System.Void TMPro.Examples.Benchmark03::Awake()
extern void Benchmark03_Awake_mD23429C1EE428EEF4ED9A753385BF194BAA41A77 (void);
// 0x00000419 System.Void TMPro.Examples.Benchmark03::Start()
extern void Benchmark03_Start_m19E638DED29CB7F96362F3C346DC47217C4AF516 (void);
// 0x0000041A System.Void TMPro.Examples.Benchmark03::.ctor()
extern void Benchmark03__ctor_m89F9102259BE1694EC418E9023DC563F3999B0BE (void);
// 0x0000041B System.Void TMPro.Examples.Benchmark04::Start()
extern void Benchmark04_Start_mEABD467C12547066E33359FDC433E8B5BAD43DB9 (void);
// 0x0000041C System.Void TMPro.Examples.Benchmark04::.ctor()
extern void Benchmark04__ctor_m5015375E0C8D19060CB5DE14CBF4AC02DDD70E7C (void);
// 0x0000041D System.Void TMPro.Examples.CameraController::Awake()
extern void CameraController_Awake_m420393892377B9703EC97764B34E32890FC5283E (void);
// 0x0000041E System.Void TMPro.Examples.CameraController::Start()
extern void CameraController_Start_m32D90EE7232BE6DE08D224F824F8EB9571655610 (void);
// 0x0000041F System.Void TMPro.Examples.CameraController::LateUpdate()
extern void CameraController_LateUpdate_m6D81DEBA4E8B443CF2AD8288F0E76E3A6B3B5373 (void);
// 0x00000420 System.Void TMPro.Examples.CameraController::GetPlayerInput()
extern void CameraController_GetPlayerInput_m6695FE20CFC691585A6AC279EDB338EC9DD13FE3 (void);
// 0x00000421 System.Void TMPro.Examples.CameraController::.ctor()
extern void CameraController__ctor_m09187FB27B590118043D4DC7B89B93164124C124 (void);
// 0x00000422 System.Void TMPro.Examples.ObjectSpin::Awake()
extern void ObjectSpin_Awake_mC1EB9630B6D3BAE645D3DD79C264F71F7B18A1AA (void);
// 0x00000423 System.Void TMPro.Examples.ObjectSpin::Update()
extern void ObjectSpin_Update_mD39DCBA0789DC0116037C442F0BA1EE6752E36D3 (void);
// 0x00000424 System.Void TMPro.Examples.ObjectSpin::.ctor()
extern void ObjectSpin__ctor_m1827B9648659746252026432DFED907AEC6007FC (void);
// 0x00000425 System.Void TMPro.Examples.ShaderPropAnimator::Awake()
extern void ShaderPropAnimator_Awake_mE04C66A41CA53AB73733E7D2CCD21B30A360C5A8 (void);
// 0x00000426 System.Void TMPro.Examples.ShaderPropAnimator::Start()
extern void ShaderPropAnimator_Start_mC5AC59C59AF2F71E0CF65F11ACD787488140EDD2 (void);
// 0x00000427 System.Collections.IEnumerator TMPro.Examples.ShaderPropAnimator::AnimateProperties()
extern void ShaderPropAnimator_AnimateProperties_mAF49CD157AD41377CE00AA10F0C06C8BF5AA0469 (void);
// 0x00000428 System.Void TMPro.Examples.ShaderPropAnimator::.ctor()
extern void ShaderPropAnimator__ctor_mAA626BC8AEEB00C5AE362FE8690D3F2200CE6E64 (void);
// 0x00000429 System.Void TMPro.Examples.ShaderPropAnimator/<AnimateProperties>d__6::.ctor(System.Int32)
extern void U3CAnimatePropertiesU3Ed__6__ctor_mFAC0F8A7368D9D35AD2780C118E13414DA79B56A (void);
// 0x0000042A System.Void TMPro.Examples.ShaderPropAnimator/<AnimateProperties>d__6::System.IDisposable.Dispose()
extern void U3CAnimatePropertiesU3Ed__6_System_IDisposable_Dispose_m628EECBFCBC49087298185F17BC2AE7A73FC8A7B (void);
// 0x0000042B System.Boolean TMPro.Examples.ShaderPropAnimator/<AnimateProperties>d__6::MoveNext()
extern void U3CAnimatePropertiesU3Ed__6_MoveNext_m8A58CCFDAE59F55AB1BB2C103800886E62C807CF (void);
// 0x0000042C System.Object TMPro.Examples.ShaderPropAnimator/<AnimateProperties>d__6::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CAnimatePropertiesU3Ed__6_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m0742A0FA62C518EBD119ED5FEBF849F918E12390 (void);
// 0x0000042D System.Void TMPro.Examples.ShaderPropAnimator/<AnimateProperties>d__6::System.Collections.IEnumerator.Reset()
extern void U3CAnimatePropertiesU3Ed__6_System_Collections_IEnumerator_Reset_mC076F281DD17668D6CE42EB04EA974DE1FFB3F6B (void);
// 0x0000042E System.Object TMPro.Examples.ShaderPropAnimator/<AnimateProperties>d__6::System.Collections.IEnumerator.get_Current()
extern void U3CAnimatePropertiesU3Ed__6_System_Collections_IEnumerator_get_Current_mA585E786DA86D2589EBCA029AD9E64E8B5135362 (void);
// 0x0000042F System.Void TMPro.Examples.SimpleScript::Start()
extern void SimpleScript_Start_m75CD9CEDCAFA9A991753478D7C28407C7329FB8F (void);
// 0x00000430 System.Void TMPro.Examples.SimpleScript::Update()
extern void SimpleScript_Update_mE8C3930DCC1767C2DF59B622FABD188E6FB91BE5 (void);
// 0x00000431 System.Void TMPro.Examples.SimpleScript::.ctor()
extern void SimpleScript__ctor_mEB370A69544227EF04C48C604A28502ADAA73697 (void);
// 0x00000432 System.Void TMPro.Examples.SkewTextExample::Awake()
extern void SkewTextExample_Awake_m6FBA7E7DC9AFDD3099F0D0B9CDE91574551105DB (void);
// 0x00000433 System.Void TMPro.Examples.SkewTextExample::Start()
extern void SkewTextExample_Start_m536F82F3A5D229332694688C59F396E07F88153F (void);
// 0x00000434 UnityEngine.AnimationCurve TMPro.Examples.SkewTextExample::CopyAnimationCurve(UnityEngine.AnimationCurve)
extern void SkewTextExample_CopyAnimationCurve_m555177255F5828DBC7E677ED06F7EFFC052886B3 (void);
// 0x00000435 System.Collections.IEnumerator TMPro.Examples.SkewTextExample::WarpText()
extern void SkewTextExample_WarpText_m0E0C46988600673F0E5DFA3133534DC6CA5950D3 (void);
// 0x00000436 System.Void TMPro.Examples.SkewTextExample::.ctor()
extern void SkewTextExample__ctor_m945059906734DD38CF860CD32B3E07635D0E1F86 (void);
// 0x00000437 System.Void TMPro.Examples.SkewTextExample/<WarpText>d__7::.ctor(System.Int32)
extern void U3CWarpTextU3Ed__7__ctor_mEAD3C39209B75514446A44B6C2FA76F8097EBD6F (void);
// 0x00000438 System.Void TMPro.Examples.SkewTextExample/<WarpText>d__7::System.IDisposable.Dispose()
extern void U3CWarpTextU3Ed__7_System_IDisposable_Dispose_m32AA7120BE15547799BDC515FA3486488952BDCF (void);
// 0x00000439 System.Boolean TMPro.Examples.SkewTextExample/<WarpText>d__7::MoveNext()
extern void U3CWarpTextU3Ed__7_MoveNext_mB832E4A3DFDDFECC460A510BBC664F218B3D7FCF (void);
// 0x0000043A System.Object TMPro.Examples.SkewTextExample/<WarpText>d__7::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CWarpTextU3Ed__7_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m3CF506B740C16FEDE58B6E1BE4556CAC2C6AEAD1 (void);
// 0x0000043B System.Void TMPro.Examples.SkewTextExample/<WarpText>d__7::System.Collections.IEnumerator.Reset()
extern void U3CWarpTextU3Ed__7_System_Collections_IEnumerator_Reset_mDDCDBB794F21DF70178A75747D8D8398F38A9C2F (void);
// 0x0000043C System.Object TMPro.Examples.SkewTextExample/<WarpText>d__7::System.Collections.IEnumerator.get_Current()
extern void U3CWarpTextU3Ed__7_System_Collections_IEnumerator_get_Current_m6D89CF1910F76577E23C924B535E93FD1B01CF0A (void);
// 0x0000043D System.Void TMPro.Examples.TMP_ExampleScript_01::Awake()
extern void TMP_ExampleScript_01_Awake_m381EF5B50E1D012B8CA1883DEFF604EEAE6D95F4 (void);
// 0x0000043E System.Void TMPro.Examples.TMP_ExampleScript_01::Update()
extern void TMP_ExampleScript_01_Update_m31611F22A608784F164A24444195B33B714567FB (void);
// 0x0000043F System.Void TMPro.Examples.TMP_ExampleScript_01::.ctor()
extern void TMP_ExampleScript_01__ctor_m3641F2E0D25B6666CE77773A4A493C4C4D3D3A12 (void);
// 0x00000440 System.Void TMPro.Examples.TMP_FrameRateCounter::Awake()
extern void TMP_FrameRateCounter_Awake_m958B668086DB4A38D9C56E3F8C2DCCB6FF11FAA3 (void);
// 0x00000441 System.Void TMPro.Examples.TMP_FrameRateCounter::Start()
extern void TMP_FrameRateCounter_Start_mC642CA714D0FB8482D2AC6192D976DA179EE3720 (void);
// 0x00000442 System.Void TMPro.Examples.TMP_FrameRateCounter::Update()
extern void TMP_FrameRateCounter_Update_m5E41B55573D86C3D345BFE11C489B00229BCEE21 (void);
// 0x00000443 System.Void TMPro.Examples.TMP_FrameRateCounter::Set_FrameCounter_Position(TMPro.Examples.TMP_FrameRateCounter/FpsCounterAnchorPositions)
extern void TMP_FrameRateCounter_Set_FrameCounter_Position_mA72ECDE464E3290E4F533491FC8113B8D4068BE1 (void);
// 0x00000444 System.Void TMPro.Examples.TMP_FrameRateCounter::.ctor()
extern void TMP_FrameRateCounter__ctor_mC79D5BF3FAE2BB7D22D9955A75E3483725BA619C (void);
// 0x00000445 System.Void TMPro.Examples.TMP_TextEventCheck::OnEnable()
extern void TMP_TextEventCheck_OnEnable_mE6D5125EEE0720E6F414978A496066E7CF1592DF (void);
// 0x00000446 System.Void TMPro.Examples.TMP_TextEventCheck::OnDisable()
extern void TMP_TextEventCheck_OnDisable_m94F087C259890C48D4F5464ABA4CD1D0E5863A9A (void);
// 0x00000447 System.Void TMPro.Examples.TMP_TextEventCheck::OnCharacterSelection(System.Char,System.Int32)
extern void TMP_TextEventCheck_OnCharacterSelection_mDA044F0808D61A99CDA374075943CEB1C92C253D (void);
// 0x00000448 System.Void TMPro.Examples.TMP_TextEventCheck::OnSpriteSelection(System.Char,System.Int32)
extern void TMP_TextEventCheck_OnSpriteSelection_mE8FFA550F38F5447CA37205698A30A41ADE901E0 (void);
// 0x00000449 System.Void TMPro.Examples.TMP_TextEventCheck::OnWordSelection(System.String,System.Int32,System.Int32)
extern void TMP_TextEventCheck_OnWordSelection_m6037166D18A938678A2B06F28A5DCB3E09FAC61B (void);
// 0x0000044A System.Void TMPro.Examples.TMP_TextEventCheck::OnLineSelection(System.String,System.Int32,System.Int32)
extern void TMP_TextEventCheck_OnLineSelection_mD2BAA6C8ABD61F0442A40C5448FA7774D782C363 (void);
// 0x0000044B System.Void TMPro.Examples.TMP_TextEventCheck::OnLinkSelection(System.String,System.String,System.Int32)
extern void TMP_TextEventCheck_OnLinkSelection_m184B11D5E35AC4EA7CDCE3340AB9FEE3C14BF41C (void);
// 0x0000044C System.Void TMPro.Examples.TMP_TextEventCheck::.ctor()
extern void TMP_TextEventCheck__ctor_mA5E82EE7CE8F8836FC6CF3823CBC7356005B898B (void);
// 0x0000044D System.Void TMPro.Examples.TMP_TextInfoDebugTool::.ctor()
extern void TMP_TextInfoDebugTool__ctor_mF6AA30660FBD4CE708B6147833853498993CB9EE (void);
// 0x0000044E System.Void TMPro.Examples.TMP_TextSelector_A::Awake()
extern void TMP_TextSelector_A_Awake_mD7252A5075E30E3BF9BEF4353F7AA2A9203FC943 (void);
// 0x0000044F System.Void TMPro.Examples.TMP_TextSelector_A::LateUpdate()
extern void TMP_TextSelector_A_LateUpdate_mDBBC09726332EDDEAF7C30AB6C08FB33261F79FD (void);
// 0x00000450 System.Void TMPro.Examples.TMP_TextSelector_A::OnPointerEnter(UnityEngine.EventSystems.PointerEventData)
extern void TMP_TextSelector_A_OnPointerEnter_mC19B85FB5B4E6EB47832C39F0115A513B85060D0 (void);
// 0x00000451 System.Void TMPro.Examples.TMP_TextSelector_A::OnPointerExit(UnityEngine.EventSystems.PointerEventData)
extern void TMP_TextSelector_A_OnPointerExit_mE51C850F54B18A5C96E3BE015DFBB0F079E5AE66 (void);
// 0x00000452 System.Void TMPro.Examples.TMP_TextSelector_A::.ctor()
extern void TMP_TextSelector_A__ctor_mEB83D3952B32CEE9A871EC60E3AE9B79048302BF (void);
// 0x00000453 System.Void TMPro.Examples.TMP_TextSelector_B::Awake()
extern void TMP_TextSelector_B_Awake_m7E413A43C54DF9E0FE70C4E69FC682391B47205A (void);
// 0x00000454 System.Void TMPro.Examples.TMP_TextSelector_B::OnEnable()
extern void TMP_TextSelector_B_OnEnable_m0828D13E2D407B90038442228D54FB0D7B3D29FB (void);
// 0x00000455 System.Void TMPro.Examples.TMP_TextSelector_B::OnDisable()
extern void TMP_TextSelector_B_OnDisable_m2B559A85B52C8CAFC7350CC7B4F8E5BC773EF781 (void);
// 0x00000456 System.Void TMPro.Examples.TMP_TextSelector_B::ON_TEXT_CHANGED(UnityEngine.Object)
extern void TMP_TextSelector_B_ON_TEXT_CHANGED_m1597DBE7C7EBE7CFA4DC395897A4779387B59910 (void);
// 0x00000457 System.Void TMPro.Examples.TMP_TextSelector_B::LateUpdate()
extern void TMP_TextSelector_B_LateUpdate_m8BA10E368C7F3483E9EC05589BBE45D7EFC75691 (void);
// 0x00000458 System.Void TMPro.Examples.TMP_TextSelector_B::OnPointerEnter(UnityEngine.EventSystems.PointerEventData)
extern void TMP_TextSelector_B_OnPointerEnter_m0A0064632E4C0E0ADCD4358AD5BC168BFB74AC4D (void);
// 0x00000459 System.Void TMPro.Examples.TMP_TextSelector_B::OnPointerExit(UnityEngine.EventSystems.PointerEventData)
extern void TMP_TextSelector_B_OnPointerExit_m667659102B5B17FBAF56593B7034E5EC1C48D43F (void);
// 0x0000045A System.Void TMPro.Examples.TMP_TextSelector_B::OnPointerClick(UnityEngine.EventSystems.PointerEventData)
extern void TMP_TextSelector_B_OnPointerClick_m8DB2D22EE2F4D3965115791C81F985D82021469F (void);
// 0x0000045B System.Void TMPro.Examples.TMP_TextSelector_B::OnPointerUp(UnityEngine.EventSystems.PointerEventData)
extern void TMP_TextSelector_B_OnPointerUp_m303500BE309B56F3ADCE8B461CEC50C8D5ED70BC (void);
// 0x0000045C System.Void TMPro.Examples.TMP_TextSelector_B::RestoreCachedVertexAttributes(System.Int32)
extern void TMP_TextSelector_B_RestoreCachedVertexAttributes_m3D637CF2C6CA663922EBE56FFD672BE582E9F1F2 (void);
// 0x0000045D System.Void TMPro.Examples.TMP_TextSelector_B::.ctor()
extern void TMP_TextSelector_B__ctor_mE654F9F1570570C4BACDA79640B8DB3033D91C33 (void);
// 0x0000045E System.Void TMPro.Examples.TMP_UiFrameRateCounter::Awake()
extern void TMP_UiFrameRateCounter_Awake_m83DBE22B6CC551BE5E385048B92067679716179E (void);
// 0x0000045F System.Void TMPro.Examples.TMP_UiFrameRateCounter::Start()
extern void TMP_UiFrameRateCounter_Start_m59DA342A492C9EF8AD5C4512753211BF3796C944 (void);
// 0x00000460 System.Void TMPro.Examples.TMP_UiFrameRateCounter::Update()
extern void TMP_UiFrameRateCounter_Update_m28FC233C475AA15A3BE399BF9345977089997749 (void);
// 0x00000461 System.Void TMPro.Examples.TMP_UiFrameRateCounter::Set_FrameCounter_Position(TMPro.Examples.TMP_UiFrameRateCounter/FpsCounterAnchorPositions)
extern void TMP_UiFrameRateCounter_Set_FrameCounter_Position_m2025933902F312C0EC4B6A864196A8BA8D545647 (void);
// 0x00000462 System.Void TMPro.Examples.TMP_UiFrameRateCounter::.ctor()
extern void TMP_UiFrameRateCounter__ctor_m5774BF4B9389770FC34991095557B24417600F84 (void);
// 0x00000463 System.Void TMPro.Examples.TMPro_InstructionOverlay::Awake()
extern void TMPro_InstructionOverlay_Awake_m2444EA8749D72BCEA4DC30A328E3745AB19062EC (void);
// 0x00000464 System.Void TMPro.Examples.TMPro_InstructionOverlay::Set_FrameCounter_Position(TMPro.Examples.TMPro_InstructionOverlay/FpsCounterAnchorPositions)
extern void TMPro_InstructionOverlay_Set_FrameCounter_Position_m62B897E4DB2D6B1936833EC54D9C246D68D50EEB (void);
// 0x00000465 System.Void TMPro.Examples.TMPro_InstructionOverlay::.ctor()
extern void TMPro_InstructionOverlay__ctor_m7AB5B851B4BFB07547E460D6B7B9D969DEB4A7CF (void);
// 0x00000466 System.Void TMPro.Examples.TeleType::Awake()
extern void TeleType_Awake_m099FCB202F2A8299B133DC56ECB9D01A16C08DFE (void);
// 0x00000467 System.Collections.IEnumerator TMPro.Examples.TeleType::Start()
extern void TeleType_Start_m72DE9DE597F4FA0B1CA02115CBC1E74EB53F63F7 (void);
// 0x00000468 System.Void TMPro.Examples.TeleType::.ctor()
extern void TeleType__ctor_m12A79B34F66CBFDCA8F582F0689D1731586B90AF (void);
// 0x00000469 System.Void TMPro.Examples.TeleType/<Start>d__4::.ctor(System.Int32)
extern void U3CStartU3Ed__4__ctor_mF3575DBEBF4F153F6A899AF3362940298C11B629 (void);
// 0x0000046A System.Void TMPro.Examples.TeleType/<Start>d__4::System.IDisposable.Dispose()
extern void U3CStartU3Ed__4_System_IDisposable_Dispose_m6DABFDBC2A313BF6DD90AA1C43B4EF9D6249CBE9 (void);
// 0x0000046B System.Boolean TMPro.Examples.TeleType/<Start>d__4::MoveNext()
extern void U3CStartU3Ed__4_MoveNext_mEF7A3215376BDFB52C3DA9D26FF0459074E89715 (void);
// 0x0000046C System.Object TMPro.Examples.TeleType/<Start>d__4::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CStartU3Ed__4_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m66DC06D52865D30DAA78DBD70B6554D13E2EA70B (void);
// 0x0000046D System.Void TMPro.Examples.TeleType/<Start>d__4::System.Collections.IEnumerator.Reset()
extern void U3CStartU3Ed__4_System_Collections_IEnumerator_Reset_mE854A2CECA0E01F3C2D90DA4F720CF0F387994F8 (void);
// 0x0000046E System.Object TMPro.Examples.TeleType/<Start>d__4::System.Collections.IEnumerator.get_Current()
extern void U3CStartU3Ed__4_System_Collections_IEnumerator_get_Current_m77D3936B94684C817EE9F6D2C903238A457D5261 (void);
// 0x0000046F System.Void TMPro.Examples.TextConsoleSimulator::Awake()
extern void TextConsoleSimulator_Awake_mA51EE182A528CCA5CAEA8DBE8AD30E43FDFAE7B0 (void);
// 0x00000470 System.Void TMPro.Examples.TextConsoleSimulator::Start()
extern void TextConsoleSimulator_Start_m429701BE4C9A52AA6B257172A4D85D58E091E7DA (void);
// 0x00000471 System.Void TMPro.Examples.TextConsoleSimulator::OnEnable()
extern void TextConsoleSimulator_OnEnable_m3397FBCDA8D9DA264D2149288BE4DCF63368AB50 (void);
// 0x00000472 System.Void TMPro.Examples.TextConsoleSimulator::OnDisable()
extern void TextConsoleSimulator_OnDisable_m43DF869E864789E215873192ECFCDC51ABA79712 (void);
// 0x00000473 System.Void TMPro.Examples.TextConsoleSimulator::ON_TEXT_CHANGED(UnityEngine.Object)
extern void TextConsoleSimulator_ON_TEXT_CHANGED_m51F06EE5DD9B32FEFB529743F209C6E6C41BE3B8 (void);
// 0x00000474 System.Collections.IEnumerator TMPro.Examples.TextConsoleSimulator::RevealCharacters(TMPro.TMP_Text)
extern void TextConsoleSimulator_RevealCharacters_mE8E415644F7BD2056D0809F7BB0FA9A2F9FE8534 (void);
// 0x00000475 System.Collections.IEnumerator TMPro.Examples.TextConsoleSimulator::RevealWords(TMPro.TMP_Text)
extern void TextConsoleSimulator_RevealWords_mFDBC863D30BC63ADCF2860F303AF252E27D0F4F4 (void);
// 0x00000476 System.Void TMPro.Examples.TextConsoleSimulator::.ctor()
extern void TextConsoleSimulator__ctor_m4719FB9D4D89F37234757D93876DF0193E8E2848 (void);
// 0x00000477 System.Void TMPro.Examples.TextConsoleSimulator/<RevealCharacters>d__7::.ctor(System.Int32)
extern void U3CRevealCharactersU3Ed__7__ctor_mD45A85F5F50909F70C80AC8CE460F4FD261CDE9D (void);
// 0x00000478 System.Void TMPro.Examples.TextConsoleSimulator/<RevealCharacters>d__7::System.IDisposable.Dispose()
extern void U3CRevealCharactersU3Ed__7_System_IDisposable_Dispose_m85F270FDC11A4D79E9CF47AADC9FA1FBF032F86C (void);
// 0x00000479 System.Boolean TMPro.Examples.TextConsoleSimulator/<RevealCharacters>d__7::MoveNext()
extern void U3CRevealCharactersU3Ed__7_MoveNext_mA9A6555E50889A7AA73F20749F25989165023DE3 (void);
// 0x0000047A System.Object TMPro.Examples.TextConsoleSimulator/<RevealCharacters>d__7::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CRevealCharactersU3Ed__7_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m5BBAE6868EB7F1C11BE5DF001641E18C9D625F83 (void);
// 0x0000047B System.Void TMPro.Examples.TextConsoleSimulator/<RevealCharacters>d__7::System.Collections.IEnumerator.Reset()
extern void U3CRevealCharactersU3Ed__7_System_Collections_IEnumerator_Reset_mA9F4893381AB24E01EAEBFC38A88AF363A9A0691 (void);
// 0x0000047C System.Object TMPro.Examples.TextConsoleSimulator/<RevealCharacters>d__7::System.Collections.IEnumerator.get_Current()
extern void U3CRevealCharactersU3Ed__7_System_Collections_IEnumerator_get_Current_mFD0B7538B1A650FB389FFE9296B0E51AEA5B6B6F (void);
// 0x0000047D System.Void TMPro.Examples.TextConsoleSimulator/<RevealWords>d__8::.ctor(System.Int32)
extern void U3CRevealWordsU3Ed__8__ctor_m22FF9E770988107A928C5D1EA639F60239BFFEF0 (void);
// 0x0000047E System.Void TMPro.Examples.TextConsoleSimulator/<RevealWords>d__8::System.IDisposable.Dispose()
extern void U3CRevealWordsU3Ed__8_System_IDisposable_Dispose_m3EAF6EF4A8C99A71FEED251BF3F28A26BF6AD7F8 (void);
// 0x0000047F System.Boolean TMPro.Examples.TextConsoleSimulator/<RevealWords>d__8::MoveNext()
extern void U3CRevealWordsU3Ed__8_MoveNext_m3811753E2384D4CBBAD6BD712EBA4FAF00D73210 (void);
// 0x00000480 System.Object TMPro.Examples.TextConsoleSimulator/<RevealWords>d__8::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CRevealWordsU3Ed__8_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mA3F93E4AA532F15D52D68B7121805C014AB2D7FB (void);
// 0x00000481 System.Void TMPro.Examples.TextConsoleSimulator/<RevealWords>d__8::System.Collections.IEnumerator.Reset()
extern void U3CRevealWordsU3Ed__8_System_Collections_IEnumerator_Reset_mCDC8982948ED5F7743567569CA1D4A354218714F (void);
// 0x00000482 System.Object TMPro.Examples.TextConsoleSimulator/<RevealWords>d__8::System.Collections.IEnumerator.get_Current()
extern void U3CRevealWordsU3Ed__8_System_Collections_IEnumerator_get_Current_mF39E492D576810F58DB9031556CFD6806FD32E27 (void);
// 0x00000483 System.Void TMPro.Examples.TextMeshProFloatingText::Awake()
extern void TextMeshProFloatingText_Awake_m679E597FF18E192C1FBD263D0E5ECEA392412046 (void);
// 0x00000484 System.Void TMPro.Examples.TextMeshProFloatingText::Start()
extern void TextMeshProFloatingText_Start_m68E511DEEDA883FE0F0999B329451F3A8A7269B1 (void);
// 0x00000485 System.Collections.IEnumerator TMPro.Examples.TextMeshProFloatingText::DisplayTextMeshProFloatingText()
extern void TextMeshProFloatingText_DisplayTextMeshProFloatingText_m3C381B8A53C58CF001D7A9212B8BDA6F368CC2C7 (void);
// 0x00000486 System.Collections.IEnumerator TMPro.Examples.TextMeshProFloatingText::DisplayTextMeshFloatingText()
extern void TextMeshProFloatingText_DisplayTextMeshFloatingText_m22691E6EA41B7FCF782B03954865D8E3B890E4DF (void);
// 0x00000487 System.Void TMPro.Examples.TextMeshProFloatingText::.ctor()
extern void TextMeshProFloatingText__ctor_m968E2691E21A93C010CF8205BC3666ADF712457E (void);
// 0x00000488 System.Void TMPro.Examples.TextMeshProFloatingText/<DisplayTextMeshProFloatingText>d__12::.ctor(System.Int32)
extern void U3CDisplayTextMeshProFloatingTextU3Ed__12__ctor_mA27BBC06A409A9D9FB03E0D2C74677486B80D168 (void);
// 0x00000489 System.Void TMPro.Examples.TextMeshProFloatingText/<DisplayTextMeshProFloatingText>d__12::System.IDisposable.Dispose()
extern void U3CDisplayTextMeshProFloatingTextU3Ed__12_System_IDisposable_Dispose_m510FB73335FCBCBEC6AC61A4F2A0217722BF27FC (void);
// 0x0000048A System.Boolean TMPro.Examples.TextMeshProFloatingText/<DisplayTextMeshProFloatingText>d__12::MoveNext()
extern void U3CDisplayTextMeshProFloatingTextU3Ed__12_MoveNext_m8DFB6850F5F9B8DF05173D4CB9C76B997EC87B57 (void);
// 0x0000048B System.Object TMPro.Examples.TextMeshProFloatingText/<DisplayTextMeshProFloatingText>d__12::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CDisplayTextMeshProFloatingTextU3Ed__12_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mCD7B5218C81C0872AB501CD54626F1284A4F73BF (void);
// 0x0000048C System.Void TMPro.Examples.TextMeshProFloatingText/<DisplayTextMeshProFloatingText>d__12::System.Collections.IEnumerator.Reset()
extern void U3CDisplayTextMeshProFloatingTextU3Ed__12_System_Collections_IEnumerator_Reset_mD2D32B94CA8D5A6D502BA38BDE1969C856368F73 (void);
// 0x0000048D System.Object TMPro.Examples.TextMeshProFloatingText/<DisplayTextMeshProFloatingText>d__12::System.Collections.IEnumerator.get_Current()
extern void U3CDisplayTextMeshProFloatingTextU3Ed__12_System_Collections_IEnumerator_get_Current_m59B2488B0E3C72CE97659E8D19BB13C6E0A44E90 (void);
// 0x0000048E System.Void TMPro.Examples.TextMeshProFloatingText/<DisplayTextMeshFloatingText>d__13::.ctor(System.Int32)
extern void U3CDisplayTextMeshFloatingTextU3Ed__13__ctor_m359EAC129649F98C759B341846A6DC07F95230D2 (void);
// 0x0000048F System.Void TMPro.Examples.TextMeshProFloatingText/<DisplayTextMeshFloatingText>d__13::System.IDisposable.Dispose()
extern void U3CDisplayTextMeshFloatingTextU3Ed__13_System_IDisposable_Dispose_m36DE8722B670A7DDD9E70107024590B20A4E0B18 (void);
// 0x00000490 System.Boolean TMPro.Examples.TextMeshProFloatingText/<DisplayTextMeshFloatingText>d__13::MoveNext()
extern void U3CDisplayTextMeshFloatingTextU3Ed__13_MoveNext_mB7D320C272AAA835590B8460DA89DEBC0A243815 (void);
// 0x00000491 System.Object TMPro.Examples.TextMeshProFloatingText/<DisplayTextMeshFloatingText>d__13::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CDisplayTextMeshFloatingTextU3Ed__13_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m891591B80D28B1DF2CF9EB78C19DA672A81231A2 (void);
// 0x00000492 System.Void TMPro.Examples.TextMeshProFloatingText/<DisplayTextMeshFloatingText>d__13::System.Collections.IEnumerator.Reset()
extern void U3CDisplayTextMeshFloatingTextU3Ed__13_System_Collections_IEnumerator_Reset_m5EF21CA1E0C64E67D18D9355B5E064C93452F5B2 (void);
// 0x00000493 System.Object TMPro.Examples.TextMeshProFloatingText/<DisplayTextMeshFloatingText>d__13::System.Collections.IEnumerator.get_Current()
extern void U3CDisplayTextMeshFloatingTextU3Ed__13_System_Collections_IEnumerator_get_Current_m4850A72D887C4DA3F53CA1A1A2BEAD5C5C6605A1 (void);
// 0x00000494 System.Void TMPro.Examples.TextMeshSpawner::Awake()
extern void TextMeshSpawner_Awake_mDF8CCC9C6B7380D6FA027263CDC3BC00EF75AEFB (void);
// 0x00000495 System.Void TMPro.Examples.TextMeshSpawner::Start()
extern void TextMeshSpawner_Start_m7CC21883CA786A846A44921D2E37C201C25439BA (void);
// 0x00000496 System.Void TMPro.Examples.TextMeshSpawner::.ctor()
extern void TextMeshSpawner__ctor_m1255777673BE591F62B4FC55EB999DBD7A7CB5A1 (void);
// 0x00000497 System.Void TMPro.Examples.VertexColorCycler::Awake()
extern void VertexColorCycler_Awake_m84B4548078500DA811F3ADFF66186373BE8EDABC (void);
// 0x00000498 System.Void TMPro.Examples.VertexColorCycler::Start()
extern void VertexColorCycler_Start_mC3FF90808EDD6A02F08375E909254778D5268B66 (void);
// 0x00000499 System.Collections.IEnumerator TMPro.Examples.VertexColorCycler::AnimateVertexColors()
extern void VertexColorCycler_AnimateVertexColors_m6960F777E4876DFCA726BCAE7A8163850D58FA42 (void);
// 0x0000049A System.Void TMPro.Examples.VertexColorCycler::.ctor()
extern void VertexColorCycler__ctor_m09990A8066C8A957A96CDBEBDA980399283B45E9 (void);
// 0x0000049B System.Void TMPro.Examples.VertexColorCycler/<AnimateVertexColors>d__3::.ctor(System.Int32)
extern void U3CAnimateVertexColorsU3Ed__3__ctor_m0038B1054BCC928D35F8C0021ED7D2E1C533E35F (void);
// 0x0000049C System.Void TMPro.Examples.VertexColorCycler/<AnimateVertexColors>d__3::System.IDisposable.Dispose()
extern void U3CAnimateVertexColorsU3Ed__3_System_IDisposable_Dispose_m5BAD394A0B09B3E0FF19E91521E02C2B3ADD6007 (void);
// 0x0000049D System.Boolean TMPro.Examples.VertexColorCycler/<AnimateVertexColors>d__3::MoveNext()
extern void U3CAnimateVertexColorsU3Ed__3_MoveNext_m2842AF5B12AFC17112D1AE75E46AB1B12776D2A6 (void);
// 0x0000049E System.Object TMPro.Examples.VertexColorCycler/<AnimateVertexColors>d__3::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CAnimateVertexColorsU3Ed__3_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mAE810D7968957A09C88E61C29DAAEC68E4AF1E51 (void);
// 0x0000049F System.Void TMPro.Examples.VertexColorCycler/<AnimateVertexColors>d__3::System.Collections.IEnumerator.Reset()
extern void U3CAnimateVertexColorsU3Ed__3_System_Collections_IEnumerator_Reset_m72ABAC50E9E4D972FB44CAFF387F3E23FEC5D932 (void);
// 0x000004A0 System.Object TMPro.Examples.VertexColorCycler/<AnimateVertexColors>d__3::System.Collections.IEnumerator.get_Current()
extern void U3CAnimateVertexColorsU3Ed__3_System_Collections_IEnumerator_get_Current_m8DE595A1D01F3A507A356F8BCE020D0851412B52 (void);
// 0x000004A1 System.Void TMPro.Examples.VertexJitter::Awake()
extern void VertexJitter_Awake_m9C5586A35BD9C928455D6479C93C1CE095447D8F (void);
// 0x000004A2 System.Void TMPro.Examples.VertexJitter::OnEnable()
extern void VertexJitter_OnEnable_m97ED60DBD350C72D1436ADFB8009A6F33A78C825 (void);
// 0x000004A3 System.Void TMPro.Examples.VertexJitter::OnDisable()
extern void VertexJitter_OnDisable_mF8D32D6E02E41A73C3E958FB6EE5D1D659D2A846 (void);
// 0x000004A4 System.Void TMPro.Examples.VertexJitter::Start()
extern void VertexJitter_Start_m8A0FED7ED16F90DBF287E09BFCBD7B26E07DBF97 (void);
// 0x000004A5 System.Void TMPro.Examples.VertexJitter::ON_TEXT_CHANGED(UnityEngine.Object)
extern void VertexJitter_ON_TEXT_CHANGED_mC7AB0113F6823D4FF415A096314B55D9C1BE9549 (void);
// 0x000004A6 System.Collections.IEnumerator TMPro.Examples.VertexJitter::AnimateVertexColors()
extern void VertexJitter_AnimateVertexColors_mECAE037FC0CBA52CAC71C0B61E88829FF18BCC16 (void);
// 0x000004A7 System.Void TMPro.Examples.VertexJitter::.ctor()
extern void VertexJitter__ctor_m550C9169D6FCD6F60D6AABCB8B4955DF58A12DCE (void);
// 0x000004A8 System.Void TMPro.Examples.VertexJitter/<AnimateVertexColors>d__11::.ctor(System.Int32)
extern void U3CAnimateVertexColorsU3Ed__11__ctor_m0222C3457F5ACA497FE3A8EC829DE4AD11A169F8 (void);
// 0x000004A9 System.Void TMPro.Examples.VertexJitter/<AnimateVertexColors>d__11::System.IDisposable.Dispose()
extern void U3CAnimateVertexColorsU3Ed__11_System_IDisposable_Dispose_m717E79A39A8161ADDA9E62F7CDFB67B8F2D65099 (void);
// 0x000004AA System.Boolean TMPro.Examples.VertexJitter/<AnimateVertexColors>d__11::MoveNext()
extern void U3CAnimateVertexColorsU3Ed__11_MoveNext_m169A75C7E2147372CB933520B670AF77907C1C6B (void);
// 0x000004AB System.Object TMPro.Examples.VertexJitter/<AnimateVertexColors>d__11::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CAnimateVertexColorsU3Ed__11_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m920EBA43D59A1A88E27FED92CF0AC0DF90179479 (void);
// 0x000004AC System.Void TMPro.Examples.VertexJitter/<AnimateVertexColors>d__11::System.Collections.IEnumerator.Reset()
extern void U3CAnimateVertexColorsU3Ed__11_System_Collections_IEnumerator_Reset_m6C045A6DD0B60F1512457448E615877EAB86D75D (void);
// 0x000004AD System.Object TMPro.Examples.VertexJitter/<AnimateVertexColors>d__11::System.Collections.IEnumerator.get_Current()
extern void U3CAnimateVertexColorsU3Ed__11_System_Collections_IEnumerator_get_Current_m017345FE58B497EAFC9D0CB1FB733F76EB3449AF (void);
// 0x000004AE System.Void TMPro.Examples.VertexShakeA::Awake()
extern void VertexShakeA_Awake_m005C6F9EE8A8FD816CD3A736D6AF199CD2B615A2 (void);
// 0x000004AF System.Void TMPro.Examples.VertexShakeA::OnEnable()
extern void VertexShakeA_OnEnable_m9F60F3951A7D0DF4201A0409F0ADC05243D324EA (void);
// 0x000004B0 System.Void TMPro.Examples.VertexShakeA::OnDisable()
extern void VertexShakeA_OnDisable_m59B86895C03B278B2E634A7C68CC942344A8D2D2 (void);
// 0x000004B1 System.Void TMPro.Examples.VertexShakeA::Start()
extern void VertexShakeA_Start_m3E623C28F873F54F4AC7579433C7C50B2A3319DE (void);
// 0x000004B2 System.Void TMPro.Examples.VertexShakeA::ON_TEXT_CHANGED(UnityEngine.Object)
extern void VertexShakeA_ON_TEXT_CHANGED_m7AD31F3239351E0916E4D02148F46A80DC148D7E (void);
// 0x000004B3 System.Collections.IEnumerator TMPro.Examples.VertexShakeA::AnimateVertexColors()
extern void VertexShakeA_AnimateVertexColors_mC451E2732A4E3E90E2553811AD75903EEF974599 (void);
// 0x000004B4 System.Void TMPro.Examples.VertexShakeA::.ctor()
extern void VertexShakeA__ctor_m7E6FD1700BD08616532AF22B3B489A18B88DFB62 (void);
// 0x000004B5 System.Void TMPro.Examples.VertexShakeA/<AnimateVertexColors>d__11::.ctor(System.Int32)
extern void U3CAnimateVertexColorsU3Ed__11__ctor_m92612416BEC0EBF5E9849FB603629C0F2F95FEF2 (void);
// 0x000004B6 System.Void TMPro.Examples.VertexShakeA/<AnimateVertexColors>d__11::System.IDisposable.Dispose()
extern void U3CAnimateVertexColorsU3Ed__11_System_IDisposable_Dispose_m77D966994D4717EAFD8EFE169F3E8A4EE8B05B81 (void);
// 0x000004B7 System.Boolean TMPro.Examples.VertexShakeA/<AnimateVertexColors>d__11::MoveNext()
extern void U3CAnimateVertexColorsU3Ed__11_MoveNext_mCEEDE03667D329386BB4AE7B8252B7A9B54F443F (void);
// 0x000004B8 System.Object TMPro.Examples.VertexShakeA/<AnimateVertexColors>d__11::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CAnimateVertexColorsU3Ed__11_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mDCCD3645ACF9B18D760B341C863F853996FA9BCE (void);
// 0x000004B9 System.Void TMPro.Examples.VertexShakeA/<AnimateVertexColors>d__11::System.Collections.IEnumerator.Reset()
extern void U3CAnimateVertexColorsU3Ed__11_System_Collections_IEnumerator_Reset_mB9444B5E58B4E97105C447F547C0F74C51BCFBFA (void);
// 0x000004BA System.Object TMPro.Examples.VertexShakeA/<AnimateVertexColors>d__11::System.Collections.IEnumerator.get_Current()
extern void U3CAnimateVertexColorsU3Ed__11_System_Collections_IEnumerator_get_Current_m5CC464185D5C0251C6206E20AFFA681BA0525A7E (void);
// 0x000004BB System.Void TMPro.Examples.VertexShakeB::Awake()
extern void VertexShakeB_Awake_m99C9A0474DBFE462DC88C898F958C1805376F9D5 (void);
// 0x000004BC System.Void TMPro.Examples.VertexShakeB::OnEnable()
extern void VertexShakeB_OnEnable_m64299258797D745CDFE7F3CBEC4708BDBC7C3971 (void);
// 0x000004BD System.Void TMPro.Examples.VertexShakeB::OnDisable()
extern void VertexShakeB_OnDisable_m07D520A8D7BCD8D188CE9F5CC7845F47D5AD6EF4 (void);
// 0x000004BE System.Void TMPro.Examples.VertexShakeB::Start()
extern void VertexShakeB_Start_m9642281210FA5F701A324B78850331E4638B2DD1 (void);
// 0x000004BF System.Void TMPro.Examples.VertexShakeB::ON_TEXT_CHANGED(UnityEngine.Object)
extern void VertexShakeB_ON_TEXT_CHANGED_m5650D69D258D0EEF35AF390D6D5086B327B308A5 (void);
// 0x000004C0 System.Collections.IEnumerator TMPro.Examples.VertexShakeB::AnimateVertexColors()
extern void VertexShakeB_AnimateVertexColors_mDF7A7E7028361D288DF588D9301541E4FA1EFA87 (void);
// 0x000004C1 System.Void TMPro.Examples.VertexShakeB::.ctor()
extern void VertexShakeB__ctor_m605A778C36B506B5763A1AE17B01F8DCCBCD51EC (void);
// 0x000004C2 System.Void TMPro.Examples.VertexShakeB/<AnimateVertexColors>d__10::.ctor(System.Int32)
extern void U3CAnimateVertexColorsU3Ed__10__ctor_m3E3B1D286DEBED2BC028AD490308568B930C3760 (void);
// 0x000004C3 System.Void TMPro.Examples.VertexShakeB/<AnimateVertexColors>d__10::System.IDisposable.Dispose()
extern void U3CAnimateVertexColorsU3Ed__10_System_IDisposable_Dispose_m0F9D4B2A6ED0500C2120DBA29932CC279E8908DC (void);
// 0x000004C4 System.Boolean TMPro.Examples.VertexShakeB/<AnimateVertexColors>d__10::MoveNext()
extern void U3CAnimateVertexColorsU3Ed__10_MoveNext_m0534E436514E663BF024364E524EE5716FF15C8E (void);
// 0x000004C5 System.Object TMPro.Examples.VertexShakeB/<AnimateVertexColors>d__10::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CAnimateVertexColorsU3Ed__10_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mF2A8BAFC0261ACBBE8EEA74DE4B498D30C68AE3D (void);
// 0x000004C6 System.Void TMPro.Examples.VertexShakeB/<AnimateVertexColors>d__10::System.Collections.IEnumerator.Reset()
extern void U3CAnimateVertexColorsU3Ed__10_System_Collections_IEnumerator_Reset_m5F12D7C6F2523BEB6326FE49AE972116D6157CBB (void);
// 0x000004C7 System.Object TMPro.Examples.VertexShakeB/<AnimateVertexColors>d__10::System.Collections.IEnumerator.get_Current()
extern void U3CAnimateVertexColorsU3Ed__10_System_Collections_IEnumerator_get_Current_mB9C12786011A3B78517AEFAE8D5A78B95A4219AD (void);
// 0x000004C8 System.Void TMPro.Examples.VertexZoom::Awake()
extern void VertexZoom_Awake_m1B5D386B98CF2EB05A8155B238D6F6E8275D181C (void);
// 0x000004C9 System.Void TMPro.Examples.VertexZoom::OnEnable()
extern void VertexZoom_OnEnable_m7F980FC038FC2534C428A5FD33E3E13AEAEB4EEC (void);
// 0x000004CA System.Void TMPro.Examples.VertexZoom::OnDisable()
extern void VertexZoom_OnDisable_m880C38B62B4BB05AF49F12F75B83FFA2F0519884 (void);
// 0x000004CB System.Void TMPro.Examples.VertexZoom::Start()
extern void VertexZoom_Start_m10A182DCEF8D430DADAFBFFA3F04171F8E60C84C (void);
// 0x000004CC System.Void TMPro.Examples.VertexZoom::ON_TEXT_CHANGED(UnityEngine.Object)
extern void VertexZoom_ON_TEXT_CHANGED_mB696E443C61D2212AC93A7615AA58106DD25250F (void);
// 0x000004CD System.Collections.IEnumerator TMPro.Examples.VertexZoom::AnimateVertexColors()
extern void VertexZoom_AnimateVertexColors_mEFBA6C940DFECB485236C247358A32011A7963ED (void);
// 0x000004CE System.Void TMPro.Examples.VertexZoom::.ctor()
extern void VertexZoom__ctor_mE7B36F2D3EC8EF397AB0AD7A19E988C06927A3B2 (void);
// 0x000004CF System.Void TMPro.Examples.VertexZoom/<>c__DisplayClass10_0::.ctor()
extern void U3CU3Ec__DisplayClass10_0__ctor_m4EC5F8042B5AC645EA7D0913E50FD22144DBD348 (void);
// 0x000004D0 System.Int32 TMPro.Examples.VertexZoom/<>c__DisplayClass10_0::<AnimateVertexColors>b__0(System.Int32,System.Int32)
extern void U3CU3Ec__DisplayClass10_0_U3CAnimateVertexColorsU3Eb__0_m959A7E1D6162B5AAF28B953AFB04D7943BCAB107 (void);
// 0x000004D1 System.Void TMPro.Examples.VertexZoom/<AnimateVertexColors>d__10::.ctor(System.Int32)
extern void U3CAnimateVertexColorsU3Ed__10__ctor_m537CF0A5ADF5BBC2DF784BF526E3F32EB528E1B2 (void);
// 0x000004D2 System.Void TMPro.Examples.VertexZoom/<AnimateVertexColors>d__10::System.IDisposable.Dispose()
extern void U3CAnimateVertexColorsU3Ed__10_System_IDisposable_Dispose_mF0CD944C393771101D2718A660E1DFF22385819F (void);
// 0x000004D3 System.Boolean TMPro.Examples.VertexZoom/<AnimateVertexColors>d__10::MoveNext()
extern void U3CAnimateVertexColorsU3Ed__10_MoveNext_m8340EFEB2B2CF6EA1545CFB6967968CA171FEE66 (void);
// 0x000004D4 System.Object TMPro.Examples.VertexZoom/<AnimateVertexColors>d__10::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CAnimateVertexColorsU3Ed__10_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m093E922A5A046910B2A7EE826D804CA3064A7BD9 (void);
// 0x000004D5 System.Void TMPro.Examples.VertexZoom/<AnimateVertexColors>d__10::System.Collections.IEnumerator.Reset()
extern void U3CAnimateVertexColorsU3Ed__10_System_Collections_IEnumerator_Reset_mDED63FD52741D58D8D5A3E53415F91B6560F680C (void);
// 0x000004D6 System.Object TMPro.Examples.VertexZoom/<AnimateVertexColors>d__10::System.Collections.IEnumerator.get_Current()
extern void U3CAnimateVertexColorsU3Ed__10_System_Collections_IEnumerator_get_Current_m933EBF7C4300C7C1E3EE60E4741110025877CA5F (void);
// 0x000004D7 System.Void TMPro.Examples.WarpTextExample::Awake()
extern void WarpTextExample_Awake_mEDB072A485F3F37270FC736E1A201624D696B4B0 (void);
// 0x000004D8 System.Void TMPro.Examples.WarpTextExample::Start()
extern void WarpTextExample_Start_m36448AEA494658D7C129C3B71BF3A64CC4DFEDC4 (void);
// 0x000004D9 UnityEngine.AnimationCurve TMPro.Examples.WarpTextExample::CopyAnimationCurve(UnityEngine.AnimationCurve)
extern void WarpTextExample_CopyAnimationCurve_m744026E638662DA48AC81ED21E0589E3E4E05FAB (void);
// 0x000004DA System.Collections.IEnumerator TMPro.Examples.WarpTextExample::WarpText()
extern void WarpTextExample_WarpText_mA29C98CF3B92F253C2DD2BADFC76618F8324BEF2 (void);
// 0x000004DB System.Void TMPro.Examples.WarpTextExample::.ctor()
extern void WarpTextExample__ctor_m62299DFDB704027267321007E16DB87D93D0F099 (void);
// 0x000004DC System.Void TMPro.Examples.WarpTextExample/<WarpText>d__8::.ctor(System.Int32)
extern void U3CWarpTextU3Ed__8__ctor_m9FADE04C27A0034C5A276232FCA187AECDC6BF49 (void);
// 0x000004DD System.Void TMPro.Examples.WarpTextExample/<WarpText>d__8::System.IDisposable.Dispose()
extern void U3CWarpTextU3Ed__8_System_IDisposable_Dispose_m5AB0DCF4B3DE6C290487F3DBBC7BAE931130DE60 (void);
// 0x000004DE System.Boolean TMPro.Examples.WarpTextExample/<WarpText>d__8::MoveNext()
extern void U3CWarpTextU3Ed__8_MoveNext_m9C83C8455FF8ADFBAA8D05958C3048612A96EB84 (void);
// 0x000004DF System.Object TMPro.Examples.WarpTextExample/<WarpText>d__8::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CWarpTextU3Ed__8_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m9D5776AF80C4227401ADD7E13D98F2530CB9E7A1 (void);
// 0x000004E0 System.Void TMPro.Examples.WarpTextExample/<WarpText>d__8::System.Collections.IEnumerator.Reset()
extern void U3CWarpTextU3Ed__8_System_Collections_IEnumerator_Reset_m4F2FBBE7B375E6FC837736B4275A7B601A01F535 (void);
// 0x000004E1 System.Object TMPro.Examples.WarpTextExample/<WarpText>d__8::System.Collections.IEnumerator.get_Current()
extern void U3CWarpTextU3Ed__8_System_Collections_IEnumerator_get_Current_m01FA935D93D737C9DDD88A051F7DFC393DD7BD25 (void);
// 0x000004E2 RockVR.Video.VideoCaptureCtrlBase/StatusType RockVR.Video.AudioCapture::get_status()
extern void AudioCapture_get_status_m56E2B766A9CA188CD8EEEF520EC058584D874E3E (void);
// 0x000004E3 System.Void RockVR.Video.AudioCapture::set_status(RockVR.Video.VideoCaptureCtrlBase/StatusType)
extern void AudioCapture_set_status_m1E39D8508A0944F2C764CFDF6DA72F40968C3146 (void);
// 0x000004E4 System.String RockVR.Video.AudioCapture::get_filePath()
extern void AudioCapture_get_filePath_m6C2DE6B6A65A63FAA8847535E5569932F33DACBB (void);
// 0x000004E5 System.Void RockVR.Video.AudioCapture::set_filePath(System.String)
extern void AudioCapture_set_filePath_m3C77C213075EF3E5DB4BD5A308A20F39131349D1 (void);
// 0x000004E6 System.Void RockVR.Video.AudioCapture::Cleanup()
extern void AudioCapture_Cleanup_mF14AD3F474ADC5C4D1339819EC3BA0D0BCE42A0B (void);
// 0x000004E7 System.Void RockVR.Video.AudioCapture::StartCapture()
extern void AudioCapture_StartCapture_m5188B27DFE105A71AF588263A321D4CCB0E30E61 (void);
// 0x000004E8 System.Void RockVR.Video.AudioCapture::StopCapture()
extern void AudioCapture_StopCapture_mB341C1E4C3421ACEF0631D1B557B45AFDEB2A00A (void);
// 0x000004E9 System.Void RockVR.Video.AudioCapture::PauseCapture()
extern void AudioCapture_PauseCapture_mA4EE944AB2C907AD36A99DC79817A612E6983834 (void);
// 0x000004EA System.Void RockVR.Video.AudioCapture::Awake()
extern void AudioCapture_Awake_m3AE496DBED033831A4A4A9903DE29A932C2A9A2D (void);
// 0x000004EB System.Void RockVR.Video.AudioCapture::OnAudioFilterRead(System.Single[],System.Int32)
extern void AudioCapture_OnAudioFilterRead_m76DD79CB5D3AAAD7E176709E7DED0A7F5EE3E70F (void);
// 0x000004EC System.IntPtr RockVR.Video.AudioCapture::AudioCaptureLib_Get(System.Int32,System.String,System.String)
extern void AudioCapture_AudioCaptureLib_Get_mC6416E539ADEA8AB5BC892E63693E6CA041BB6F3 (void);
// 0x000004ED System.Void RockVR.Video.AudioCapture::AudioCaptureLib_WriteFrame(System.IntPtr,System.Byte[])
extern void AudioCapture_AudioCaptureLib_WriteFrame_m85D72E9D49593C492A636C8F0EF13869295D8AC4 (void);
// 0x000004EE System.Void RockVR.Video.AudioCapture::AudioCaptureLib_Close(System.IntPtr)
extern void AudioCapture_AudioCaptureLib_Close_mAF6668A52955C0CA54417614820ED61D295774BB (void);
// 0x000004EF System.Void RockVR.Video.AudioCapture::AudioCaptureLib_Clean(System.IntPtr)
extern void AudioCapture_AudioCaptureLib_Clean_mE2FE8BF6BB2904D7288F8193ED1C4EF26C02FE02 (void);
// 0x000004F0 System.Void RockVR.Video.AudioCapture::.ctor()
extern void AudioCapture__ctor_mBE1317E3F3A8AEA7D03034F1636032DCBC05500F (void);
// 0x000004F1 System.Int32 RockVR.Video.VideoCaptureBase::get_frameWidth()
extern void VideoCaptureBase_get_frameWidth_m30887D181D279743F9B485B95E85A2B4E4752BF4 (void);
// 0x000004F2 System.Int32 RockVR.Video.VideoCaptureBase::get_frameHeight()
extern void VideoCaptureBase_get_frameHeight_m10D46EDB9C07F1A5B9DB992C10C0CCB91550E0E9 (void);
// 0x000004F3 System.Int32 RockVR.Video.VideoCaptureBase::get_cubemapSize()
extern void VideoCaptureBase_get_cubemapSize_m2C45427F1F8C5A27632BFA644D4C62DADBC2BA32 (void);
// 0x000004F4 System.Int32 RockVR.Video.VideoCaptureBase::get_antiAliasing()
extern void VideoCaptureBase_get_antiAliasing_m83B4F7A5CA63AD77B1363B1726A7C9B94E5C0701 (void);
// 0x000004F5 System.Int32 RockVR.Video.VideoCaptureBase::get_bitrate()
extern void VideoCaptureBase_get_bitrate_mF6142599C5560AF81A078B6D621D8FB5C5258600 (void);
// 0x000004F6 System.Int32 RockVR.Video.VideoCaptureBase::get_targetFramerate()
extern void VideoCaptureBase_get_targetFramerate_m752C9DA32FD5639809165B961DDB111A78C42CD1 (void);
// 0x000004F7 System.String RockVR.Video.VideoCaptureBase::get_filePath()
extern void VideoCaptureBase_get_filePath_m47CAA68BE4F7B187DDDB38BDE19486F7817420FA (void);
// 0x000004F8 System.Void RockVR.Video.VideoCaptureBase::set_filePath(System.String)
extern void VideoCaptureBase_set_filePath_m1B925667D6E4D151D80A876FBE350DB8B2AD6E23 (void);
// 0x000004F9 System.Void RockVR.Video.VideoCaptureBase::Awake()
extern void VideoCaptureBase_Awake_m26142FBDEF8B003D2C38743F5A8AFD350C2F451D (void);
// 0x000004FA System.Void RockVR.Video.VideoCaptureBase::StartCapture()
extern void VideoCaptureBase_StartCapture_m3013969305F830E5642868367E385E7717B23E41 (void);
// 0x000004FB System.Void RockVR.Video.VideoCaptureBase::StopCapture()
extern void VideoCaptureBase_StopCapture_m00700F62005DE97952D4C6BBDF1B892D68E8D480 (void);
// 0x000004FC System.Void RockVR.Video.VideoCaptureBase::ToggleCapture()
extern void VideoCaptureBase_ToggleCapture_mC1E80E2AF344349F4AE1CA86A76F98E9DCB92790 (void);
// 0x000004FD System.Boolean RockVR.Video.VideoCaptureBase::get_isPanorama()
extern void VideoCaptureBase_get_isPanorama_m67127DF463C86EE71AF549BE7768DBE87EF45A0F (void);
// 0x000004FE System.Boolean RockVR.Video.VideoCaptureBase::get_isLiveStreaming()
extern void VideoCaptureBase_get_isLiveStreaming_m66A77B20D419128C73442F613C560AF83B0BB8F9 (void);
// 0x000004FF System.Void RockVR.Video.VideoCaptureBase::RenderTextureToPNG(UnityEngine.RenderTexture,System.String)
extern void VideoCaptureBase_RenderTextureToPNG_m56673E7AAB44BCA00C726529BA64218DB72EC493 (void);
// 0x00000500 System.Void RockVR.Video.VideoCaptureBase::TextureToPNG(UnityEngine.Texture2D,System.String)
extern void VideoCaptureBase_TextureToPNG_mE45B601DF15672A295267B3147C201CA338B5693 (void);
// 0x00000501 System.Void RockVR.Video.VideoCaptureBase::SetStereoVideoFormat(UnityEngine.RenderTexture)
extern void VideoCaptureBase_SetStereoVideoFormat_mFD413EEBA2351A1EC822E8C5D731D9DC05A26C6F (void);
// 0x00000502 System.Void RockVR.Video.VideoCaptureBase::RenderToCubemapWithGUI(UnityEngine.Camera,UnityEngine.RenderTexture)
extern void VideoCaptureBase_RenderToCubemapWithGUI_m736B83E8E174A7F7CFA0A2FB44A4F9582396FB59 (void);
// 0x00000503 System.Void RockVR.Video.VideoCaptureBase::RenderCameraToRenderTexture(UnityEngine.Camera,UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern void VideoCaptureBase_RenderCameraToRenderTexture_m6D3A6B9553445C19CEEAFD6AA3FD87E8392F8871 (void);
// 0x00000504 System.Void RockVR.Video.VideoCaptureBase::.ctor()
extern void VideoCaptureBase__ctor_mCE9E51D0F97FC2934841D9C918AFFF791D5DF2B4 (void);
// 0x00000505 RockVR.Video.VideoCaptureCtrlBase/StatusType RockVR.Video.VideoCaptureCtrlBase::get_status()
extern void VideoCaptureCtrlBase_get_status_m190249C80EBE548327CCBFCC9912C84E798E2F4A (void);
// 0x00000506 System.Void RockVR.Video.VideoCaptureCtrlBase::set_status(RockVR.Video.VideoCaptureCtrlBase/StatusType)
extern void VideoCaptureCtrlBase_set_status_m8F4B13A5B5A945FDE87A2D04FF6EFD0B8C6D4D88 (void);
// 0x00000507 RockVR.Video.VideoCaptureBase[] RockVR.Video.VideoCaptureCtrlBase::get_videoCaptures()
extern void VideoCaptureCtrlBase_get_videoCaptures_m425D14FFED1EC140FB1898456BE5FA4EF1AC2762 (void);
// 0x00000508 System.Void RockVR.Video.VideoCaptureCtrlBase::set_videoCaptures(RockVR.Video.VideoCaptureBase[])
extern void VideoCaptureCtrlBase_set_videoCaptures_mCECF5016B63BC308926B1B659F743E7D890982DE (void);
// 0x00000509 System.Void RockVR.Video.VideoCaptureCtrlBase::StartCapture()
extern void VideoCaptureCtrlBase_StartCapture_mAB78E3CED54F6F05A3F90FFA59735C3139139A90 (void);
// 0x0000050A System.Void RockVR.Video.VideoCaptureCtrlBase::StopCapture()
extern void VideoCaptureCtrlBase_StopCapture_m52AC4978D064A7EC4119B68CCBCE5A413F220608 (void);
// 0x0000050B System.Void RockVR.Video.VideoCaptureCtrlBase::ToggleCapture()
extern void VideoCaptureCtrlBase_ToggleCapture_m11A8ADBDAF5FCCB738363A801008A5A41658EF9C (void);
// 0x0000050C System.Void RockVR.Video.VideoCaptureCtrlBase::Start()
extern void VideoCaptureCtrlBase_Start_m79329B3C3B3D91EB18B7E16CA95B65A53BADEB0F (void);
// 0x0000050D System.Void RockVR.Video.VideoCaptureCtrlBase::Update()
extern void VideoCaptureCtrlBase_Update_mCF94B098F3F5966FE3D1BAF4BF8983AED0D9E045 (void);
// 0x0000050E System.Void RockVR.Video.VideoCaptureCtrlBase::.ctor()
extern void VideoCaptureCtrlBase__ctor_m266CCCCFB5986CE05F5CA1D2CECF96A588C67441 (void);
// 0x0000050F System.String RockVR.Video.PathConfig::get_SaveFolder()
extern void PathConfig_get_SaveFolder_m0CF2F311D40D2E5FE2E62722DAB322D1569D4FED (void);
// 0x00000510 System.Void RockVR.Video.PathConfig::set_SaveFolder(System.String)
extern void PathConfig_set_SaveFolder_m58DE48455B2BB95CBD45E811B6222FD9E89DBEF9 (void);
// 0x00000511 System.String RockVR.Video.PathConfig::get_ffmpegPath()
extern void PathConfig_get_ffmpegPath_mA4D263DF1E51B9D85F5C79D5FD06334DE1D7668E (void);
// 0x00000512 System.String RockVR.Video.PathConfig::get_injectorPath()
extern void PathConfig_get_injectorPath_m1F02FB0B75BFA7DD82A2024CB38450C1BD3107AF (void);
// 0x00000513 System.Void RockVR.Video.PathConfig::.ctor()
extern void PathConfig__ctor_m9978E41B8C7D808D5609185295AFE1560B4CB54C (void);
// 0x00000514 System.Void RockVR.Video.PathConfig::.cctor()
extern void PathConfig__cctor_m5FE032CEC5C7BADEA5A6918E6687E0119D2F61BB (void);
// 0x00000515 UnityEngine.Camera RockVR.Video.Screenshot::get_Camera()
extern void Screenshot_get_Camera_m8AA0892EBC1793EF667760BDC715C677224BF76D (void);
// 0x00000516 System.Void RockVR.Video.Screenshot::Start()
extern void Screenshot_Start_m27E1947C3576649271CF2527C84D964B7FA6C98F (void);
// 0x00000517 System.Collections.IEnumerator RockVR.Video.Screenshot::AutoTakeScreenshot(System.Int32)
extern void Screenshot_AutoTakeScreenshot_m20BE31EE8B6C8C332BC70B394395A2B196149930 (void);
// 0x00000518 System.Void RockVR.Video.Screenshot::TakeScreenshot()
extern void Screenshot_TakeScreenshot_mA3C2AF43612E83C3B4D6FE9A3A0B79C7BECD8316 (void);
// 0x00000519 System.Void RockVR.Video.Screenshot::.ctor()
extern void Screenshot__ctor_mE1CA0DE1BF8D9913273E4B9CC0A8F23CD731AB82 (void);
// 0x0000051A System.Void RockVR.Video.Screenshot/<AutoTakeScreenshot>d__8::.ctor(System.Int32)
extern void U3CAutoTakeScreenshotU3Ed__8__ctor_m071D1E2C42EDE895DC697003F8C091490440B15C (void);
// 0x0000051B System.Void RockVR.Video.Screenshot/<AutoTakeScreenshot>d__8::System.IDisposable.Dispose()
extern void U3CAutoTakeScreenshotU3Ed__8_System_IDisposable_Dispose_mC3F85BEB0DC8C76723BCBEFB55DCA5D5E3EACEF6 (void);
// 0x0000051C System.Boolean RockVR.Video.Screenshot/<AutoTakeScreenshot>d__8::MoveNext()
extern void U3CAutoTakeScreenshotU3Ed__8_MoveNext_mA180AF291ED09DA0DF95FFB9BC63CCB9A1B7B428 (void);
// 0x0000051D System.Object RockVR.Video.Screenshot/<AutoTakeScreenshot>d__8::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CAutoTakeScreenshotU3Ed__8_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mBF203EDCD3A1CC0A2EE0A54492365E267841918C (void);
// 0x0000051E System.Void RockVR.Video.Screenshot/<AutoTakeScreenshot>d__8::System.Collections.IEnumerator.Reset()
extern void U3CAutoTakeScreenshotU3Ed__8_System_Collections_IEnumerator_Reset_mBC7EFA60581C7596B406B799B67D488D2B917F92 (void);
// 0x0000051F System.Object RockVR.Video.Screenshot/<AutoTakeScreenshot>d__8::System.Collections.IEnumerator.get_Current()
extern void U3CAutoTakeScreenshotU3Ed__8_System_Collections_IEnumerator_get_Current_mA5BA749D7AD517F4B917E12CB106FB3757E9A266 (void);
// 0x00000520 System.String RockVR.Video.StringUtils::GetTimeString()
extern void StringUtils_GetTimeString_m8323FDF230F189CEAD9194244B40DE4641AA52BC (void);
// 0x00000521 System.String RockVR.Video.StringUtils::GetRandomString(System.Int32)
extern void StringUtils_GetRandomString_m809A91C40CA40734027F2C44F2DFE2F82EBF98B5 (void);
// 0x00000522 System.String RockVR.Video.StringUtils::GetMp4FileName(System.String)
extern void StringUtils_GetMp4FileName_m53C1E9EAE2871481860D5F9CD1D5DC8D0271DA0A (void);
// 0x00000523 System.String RockVR.Video.StringUtils::GetH264FileName(System.String)
extern void StringUtils_GetH264FileName_m75D0F7B3F82A35E5026CA241D2885C68B32DE221 (void);
// 0x00000524 System.String RockVR.Video.StringUtils::GetWavFileName(System.String)
extern void StringUtils_GetWavFileName_m04F1694AFB96F0E9A9FD4C3FDD38FFF7ED42C4A6 (void);
// 0x00000525 System.String RockVR.Video.StringUtils::GetPngFileName(System.String)
extern void StringUtils_GetPngFileName_m09B8E114051FD1F22BC37D6CF34537D558ECA2ED (void);
// 0x00000526 System.String RockVR.Video.StringUtils::GetJpgFileName(System.String)
extern void StringUtils_GetJpgFileName_m1ECB4D352EAB7AFEA6BD1EB4ADF452E1280D81C7 (void);
// 0x00000527 System.Boolean RockVR.Video.StringUtils::IsRtmpAddress(System.String)
extern void StringUtils_IsRtmpAddress_mFF0E958604290D307A63D39274D0C0E09ECDA210 (void);
// 0x00000528 System.Void RockVR.Video.StringUtils::.ctor()
extern void StringUtils__ctor_mA5F77CB798471C593A60F0C1D23A04F69FB0C207 (void);
// 0x00000529 System.Void RockVR.Video.StringUtils/<>c__DisplayClass1_0::.ctor()
extern void U3CU3Ec__DisplayClass1_0__ctor_m1A5723050A445DAAB02C6F44A677EB843AF07AC1 (void);
// 0x0000052A System.Char RockVR.Video.StringUtils/<>c__DisplayClass1_0::<GetRandomString>b__0(System.String)
extern void U3CU3Ec__DisplayClass1_0_U3CGetRandomStringU3Eb__0_mEF0E5699C07EC858EEE2EBADF93720F70D5F3075 (void);
// 0x0000052B System.Boolean RockVR.Video.MathUtils::CheckPowerOfTwo(System.Int32)
extern void MathUtils_CheckPowerOfTwo_m69CA923FD548C300A6347B16335BC79BDB2DBAE6 (void);
// 0x0000052C System.Void RockVR.Video.MathUtils::.ctor()
extern void MathUtils__ctor_mDA4F39AAABFCBCC9E717EBFB91476DB42BCC8890 (void);
// 0x0000052D RockVR.Video.VideoCaptureCtrlBase/StatusType RockVR.Video.VideoCapture::get_status()
extern void VideoCapture_get_status_m418342C3D1B1C60BC7E9E46783F045B5E5826BBB (void);
// 0x0000052E System.Void RockVR.Video.VideoCapture::set_status(RockVR.Video.VideoCaptureCtrlBase/StatusType)
extern void VideoCapture_set_status_m26E046134DC8C0161A319480FABAB776E9FDDCAF (void);
// 0x0000052F System.Void RockVR.Video.VideoCapture::Cleanup()
extern void VideoCapture_Cleanup_mAA4078713D9DC15EBDE3B69E05DF59AD1F0DBB7B (void);
// 0x00000530 System.Void RockVR.Video.VideoCapture::StartCapture()
extern void VideoCapture_StartCapture_mFA8B0914AE97F23EF05EF56C3427E5F894157D2B (void);
// 0x00000531 System.Void RockVR.Video.VideoCapture::StopCapture()
extern void VideoCapture_StopCapture_m7D077BB0470389C711A58AD88210196174EB26AD (void);
// 0x00000532 System.Void RockVR.Video.VideoCapture::ToggleCapture()
extern void VideoCapture_ToggleCapture_mADD2E3677B84C769B464C5F818EF6CCC3AEAC823 (void);
// 0x00000533 System.Void RockVR.Video.VideoCapture::Awake()
extern void VideoCapture_Awake_m573170C5B7D125834549905CA9B89FC9D2F77493 (void);
// 0x00000534 System.Void RockVR.Video.VideoCapture::OnRenderImage(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern void VideoCapture_OnRenderImage_m1683B212DC56A4B929B01DA16CBDE0744308A926 (void);
// 0x00000535 System.Void RockVR.Video.VideoCapture::LateUpdate()
extern void VideoCapture_LateUpdate_m690B612A2F3EFD01ACB99C369DE37733612BC08D (void);
// 0x00000536 System.Void RockVR.Video.VideoCapture::OnDestroy()
extern void VideoCapture_OnDestroy_m0949F83B32C88FE70F7B8F92DC3B06FA34A5F00B (void);
// 0x00000537 System.Void RockVR.Video.VideoCapture::OnApplicationQuit()
extern void VideoCapture_OnApplicationQuit_mEBEADA3C27D07F2E33C89D1A4ABC4342C726DC8C (void);
// 0x00000538 System.Collections.IEnumerator RockVR.Video.VideoCapture::CaptureFrameAsync()
extern void VideoCapture_CaptureFrameAsync_m5B486B78D00834A6FE1F63AD950F212171A3F1C5 (void);
// 0x00000539 System.Void RockVR.Video.VideoCapture::CaptureCubemapFrameSync()
extern void VideoCapture_CaptureCubemapFrameSync_m43D14136A6CA0BD5A144594B4BE27B5C688FB047 (void);
// 0x0000053A System.Void RockVR.Video.VideoCapture::CopyFrameTexture()
extern void VideoCapture_CopyFrameTexture_m5DD2F546239F24A875A7419C4C8F445115A66D85 (void);
// 0x0000053B System.Void RockVR.Video.VideoCapture::EnqueueFrameTexture()
extern void VideoCapture_EnqueueFrameTexture_m0A708D1AEACAF83A5DD7FC24E89C39E5B24EF9B0 (void);
// 0x0000053C System.Void RockVR.Video.VideoCapture::FrameEncodeThreadFunction()
extern void VideoCapture_FrameEncodeThreadFunction_m45C399B289D66F66D6231E03197C68862577CA6B (void);
// 0x0000053D System.IntPtr RockVR.Video.VideoCapture::VideoCaptureLib_Get(System.Int32,System.Int32,System.Int32,System.Int32,System.String,System.String)
extern void VideoCapture_VideoCaptureLib_Get_m7C957A27D8F125D85970AA8A9A03EB261443FD9F (void);
// 0x0000053E System.Void RockVR.Video.VideoCapture::VideoCaptureLib_WriteFrames(System.IntPtr,System.Byte[],System.Int32)
extern void VideoCapture_VideoCaptureLib_WriteFrames_mB47A170A22C613403FE0C3088A21EC903C3E9576 (void);
// 0x0000053F System.Void RockVR.Video.VideoCapture::VideoCaptureLib_Close(System.IntPtr)
extern void VideoCapture_VideoCaptureLib_Close_m1DB9B37045C3AE8D0D94F4DB3B0C93929D7FD93B (void);
// 0x00000540 System.Void RockVR.Video.VideoCapture::VideoCaptureLib_Clean(System.IntPtr)
extern void VideoCapture_VideoCaptureLib_Clean_m6B4F7316E874E3AD4C06C0A0222EED9624F04F3A (void);
// 0x00000541 System.IntPtr RockVR.Video.VideoCapture::VideoStreamingLib_Get(System.Int32,System.Int32,System.Int32,System.Int32,System.String,System.String)
extern void VideoCapture_VideoStreamingLib_Get_mF8CA45471D2BED0520BBC731F2DE7C744B896149 (void);
// 0x00000542 System.Void RockVR.Video.VideoCapture::VideoStreamingLib_WriteFrames(System.IntPtr,System.Byte[],System.Int32)
extern void VideoCapture_VideoStreamingLib_WriteFrames_m50F8E681EE1FB01E1A202A4C8B65E0178172F02E (void);
// 0x00000543 System.Void RockVR.Video.VideoCapture::VideoStreamingLib_Close(System.IntPtr)
extern void VideoCapture_VideoStreamingLib_Close_m7EC2D8677E013017E5308266C5CDDE308A460A08 (void);
// 0x00000544 System.Void RockVR.Video.VideoCapture::VideoStreamingLib_Clean(System.IntPtr)
extern void VideoCapture_VideoStreamingLib_Clean_m446C144B2BF2CAACD9069B885B40DF330FF0972E (void);
// 0x00000545 System.Void RockVR.Video.VideoCapture::.ctor()
extern void VideoCapture__ctor_mA2EBE79FC8C3F61757616A24BE04917249932172 (void);
// 0x00000546 System.Void RockVR.Video.VideoCapture/FrameData::.ctor(System.Byte[],System.Int32)
extern void FrameData__ctor_m998B6E32258EF155CA31AFB8D53307247807765F (void);
// 0x00000547 System.Void RockVR.Video.VideoCapture/<CaptureFrameAsync>d__26::.ctor(System.Int32)
extern void U3CCaptureFrameAsyncU3Ed__26__ctor_m8DB8A61D8D83A2B2BFFF4F5E4AEB7E5CA9865AA9 (void);
// 0x00000548 System.Void RockVR.Video.VideoCapture/<CaptureFrameAsync>d__26::System.IDisposable.Dispose()
extern void U3CCaptureFrameAsyncU3Ed__26_System_IDisposable_Dispose_m16FB3151DF9C1A0FFFC4B5B4E520EFDA57CD8C21 (void);
// 0x00000549 System.Boolean RockVR.Video.VideoCapture/<CaptureFrameAsync>d__26::MoveNext()
extern void U3CCaptureFrameAsyncU3Ed__26_MoveNext_m7976231B758894E9A8204F2DCD2962B8A7ED7FAB (void);
// 0x0000054A System.Object RockVR.Video.VideoCapture/<CaptureFrameAsync>d__26::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CCaptureFrameAsyncU3Ed__26_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mA5975049F28F465987C255BEAB8EBB096909FA2A (void);
// 0x0000054B System.Void RockVR.Video.VideoCapture/<CaptureFrameAsync>d__26::System.Collections.IEnumerator.Reset()
extern void U3CCaptureFrameAsyncU3Ed__26_System_Collections_IEnumerator_Reset_m06A761BB7C018F2FD9999B313FB3C97CE008BFD8 (void);
// 0x0000054C System.Object RockVR.Video.VideoCapture/<CaptureFrameAsync>d__26::System.Collections.IEnumerator.get_Current()
extern void U3CCaptureFrameAsyncU3Ed__26_System_Collections_IEnumerator_get_Current_mA5CCE329EB1C82067D0B5B4B71633F9928B59CB6 (void);
// 0x0000054D System.Void RockVR.Video.VideoMuxing::.ctor(RockVR.Video.VideoCapture,RockVR.Video.AudioCapture)
extern void VideoMuxing__ctor_mC6C234BCFADE86E96280CBC9128E41D8A6EDC591 (void);
// 0x0000054E System.Boolean RockVR.Video.VideoMuxing::Muxing()
extern void VideoMuxing_Muxing_m76DF75EA615A331EF71647A056319EC0A1C6233A (void);
// 0x0000054F System.IntPtr RockVR.Video.VideoMuxing::MuxingLib_Get(System.Int32,System.String,System.String,System.String,System.String)
extern void VideoMuxing_MuxingLib_Get_mDEF72C2AD83F3282B529B7DD2DF92C621D09E698 (void);
// 0x00000550 System.Void RockVR.Video.VideoMuxing::MuxingLib_Muxing(System.IntPtr)
extern void VideoMuxing_MuxingLib_Muxing_m14184A0F6A3D39460F25F294CCBA7E4621AEDE1F (void);
// 0x00000551 System.Void RockVR.Video.VideoMuxing::MuxingLib_Clean(System.IntPtr)
extern void VideoMuxing_MuxingLib_Clean_m9A58C8796563F662B722FC07816F861DFC70D8CD (void);
// 0x00000552 RockVR.Video.AudioCapture RockVR.Video.VideoCaptureCtrl::get_audioCapture()
extern void VideoCaptureCtrl_get_audioCapture_m4A232276C577E7264F2EAFEA2F95753FA9DC98E7 (void);
// 0x00000553 System.Void RockVR.Video.VideoCaptureCtrl::set_audioCapture(RockVR.Video.AudioCapture)
extern void VideoCaptureCtrl_set_audioCapture_m46849A247E5EEBC041724FA2D6F6303C78444ADA (void);
// 0x00000554 System.Void RockVR.Video.VideoCaptureCtrl::StartCapture()
extern void VideoCaptureCtrl_StartCapture_m0A9F0D1E820CA25530CAE0049D559A914B0A2A45 (void);
// 0x00000555 System.Void RockVR.Video.VideoCaptureCtrl::StopCapture()
extern void VideoCaptureCtrl_StopCapture_m81A2FEB4B393092B0AB397893E45CF32282616ED (void);
// 0x00000556 System.Void RockVR.Video.VideoCaptureCtrl::ToggleCapture()
extern void VideoCaptureCtrl_ToggleCapture_mDFD84FF882F3E76F7921D2C355E5FA63647F4EFE (void);
// 0x00000557 System.Void RockVR.Video.VideoCaptureCtrl::OnVideoCaptureComplete()
extern void VideoCaptureCtrl_OnVideoCaptureComplete_mDDFA7A70FBA21CDFB92D86CFFB017A41A1D4FDCB (void);
// 0x00000558 System.Void RockVR.Video.VideoCaptureCtrl::OnAudioCaptureComplete()
extern void VideoCaptureCtrl_OnAudioCaptureComplete_m4E4CD80551FD5289B5C076E897719BCC4384EBBE (void);
// 0x00000559 System.Void RockVR.Video.VideoCaptureCtrl::VideoMergeThreadFunction()
extern void VideoCaptureCtrl_VideoMergeThreadFunction_mF4424620F3EE51C9ED0D59391844A2816D9D354F (void);
// 0x0000055A System.Void RockVR.Video.VideoCaptureCtrl::GarbageCollectionThreadFunction()
extern void VideoCaptureCtrl_GarbageCollectionThreadFunction_mEA0F32FD0D20B0E9BF68B4495AD46F5D6E7AEEFC (void);
// 0x0000055B System.Void RockVR.Video.VideoCaptureCtrl::Cleanup()
extern void VideoCaptureCtrl_Cleanup_m0849D4B6C027CDBC7ECCC719D66F5428F76C4135 (void);
// 0x0000055C System.Boolean RockVR.Video.VideoCaptureCtrl::IsCaptureAudio()
extern void VideoCaptureCtrl_IsCaptureAudio_m6BE853F2364BA9E96FC8071A59A93A6F1B110548 (void);
// 0x0000055D System.Void RockVR.Video.VideoCaptureCtrl::Awake()
extern void VideoCaptureCtrl_Awake_mCBC3C6675D44D08C27C71693506C00EAD0C722A9 (void);
// 0x0000055E System.Void RockVR.Video.VideoCaptureCtrl::OnApplicationQuit()
extern void VideoCaptureCtrl_OnApplicationQuit_m5B2DC8C4BA56FAC028DFAC997780F2F421AEB1B1 (void);
// 0x0000055F System.Void RockVR.Video.VideoCaptureCtrl::.ctor()
extern void VideoCaptureCtrl__ctor_m84BBF5B8A47B2DD9BB1BEAC87FF551E0AC7B17C4 (void);
// 0x00000560 RockVR.Video.AudioCapture RockVR.Video.VideoCapturePro::get_audioCapture()
extern void VideoCapturePro_get_audioCapture_m70A99B46ACE208E98BE2F502278A473D49A556C9 (void);
// 0x00000561 System.Void RockVR.Video.VideoCapturePro::set_audioCapture(RockVR.Video.AudioCapture)
extern void VideoCapturePro_set_audioCapture_mE1236BB3108F81FEF011245319FD14136F9DF6D9 (void);
// 0x00000562 System.Void RockVR.Video.VideoCapturePro::StartCapture()
extern void VideoCapturePro_StartCapture_mA8972C8249C32D126C12D9DE0E8301F4C404E8A5 (void);
// 0x00000563 System.Void RockVR.Video.VideoCapturePro::StopCapture()
extern void VideoCapturePro_StopCapture_mB08D850A7B29215F943724D2E06779B2E1F045A3 (void);
// 0x00000564 System.Void RockVR.Video.VideoCapturePro::ToggleCapture()
extern void VideoCapturePro_ToggleCapture_m2E45E17E9E4D3C58E52AD39A0720AB77DAB5ACF3 (void);
// 0x00000565 System.Void RockVR.Video.VideoCapturePro::Awake()
extern void VideoCapturePro_Awake_mBF4C62C65E393B906EF119FE7F00088D1C234F7E (void);
// 0x00000566 System.Void RockVR.Video.VideoCapturePro::Update()
extern void VideoCapturePro_Update_mDAA5DBA319EF1FFCA9C8785F4F35408E502FB128 (void);
// 0x00000567 System.Void RockVR.Video.VideoCapturePro::OnRenderImage(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern void VideoCapturePro_OnRenderImage_m9413EA436097B53B7852BBB703A3C6098DAED94A (void);
// 0x00000568 System.Void RockVR.Video.VideoCapturePro::OnDestroy()
extern void VideoCapturePro_OnDestroy_m45E97C6F035AFF20DB8E7ADF68FDD5C4D5F3AF20 (void);
// 0x00000569 System.Void RockVR.Video.VideoCapturePro::OnApplicationQuit()
extern void VideoCapturePro_OnApplicationQuit_m9A2D6D4B2BB05E39580B5130A70895F53DAF9E04 (void);
// 0x0000056A System.Void RockVR.Video.VideoCapturePro::LiveThreadFunction()
extern void VideoCapturePro_LiveThreadFunction_m3CBC70316522EDA7EF26989EEE66535EE38BF2EC (void);
// 0x0000056B System.Void RockVR.Video.VideoCapturePro::MuxingThreadFunction()
extern void VideoCapturePro_MuxingThreadFunction_m07CD6F4713982381EA84DEEFF5A98C5E4CC56B23 (void);
// 0x0000056C System.Void RockVR.Video.VideoCapturePro::AudioThreadFunction()
extern void VideoCapturePro_AudioThreadFunction_m1B3C74EC8DC859262E1A39A5C25A5F642102EE80 (void);
// 0x0000056D System.Boolean RockVR.Video.VideoCapturePro::SetOutputSize()
extern void VideoCapturePro_SetOutputSize_m35E201238671B0FE561EE7978504A7B40C531426 (void);
// 0x0000056E System.Void RockVR.Video.VideoCapturePro::Screenshot()
extern void VideoCapturePro_Screenshot_m58EB28374775084138D420520DCC1EE187170DB3 (void);
// 0x0000056F System.Collections.IEnumerator RockVR.Video.VideoCapturePro::CaptureScreenshot()
extern void VideoCapturePro_CaptureScreenshot_m5D753812398655B257B81EB830E8D7529733301E (void);
// 0x00000570 System.Void RockVR.Video.VideoCapturePro::Cleanup()
extern void VideoCapturePro_Cleanup_mA14DE6156B54032B923C39148BCAD5E34B0FB9BA (void);
// 0x00000571 System.Void RockVR.Video.VideoCapturePro::RenderCubeFace(UnityEngine.CubemapFace,System.Single,System.Single,System.Single,System.Single)
extern void VideoCapturePro_RenderCubeFace_m3EEA8DDC4FCD4830D6365A4360C5E8258E208B9D (void);
// 0x00000572 System.Void RockVR.Video.VideoCapturePro::SetMaterialParameters(UnityEngine.Material)
extern void VideoCapturePro_SetMaterialParameters_m34DCBCE12E5AB63D2AEA5264490CBFA9E6F1D1C6 (void);
// 0x00000573 System.Void RockVR.Video.VideoCapturePro::DisplayCubeMap(UnityEngine.RenderTexture)
extern void VideoCapturePro_DisplayCubeMap_m9E99754F5CF85E1322306FF1A0B6DDF16E6790C6 (void);
// 0x00000574 System.Void RockVR.Video.VideoCapturePro::DisplayEquirect(UnityEngine.RenderTexture)
extern void VideoCapturePro_DisplayEquirect_m08495B1C95BCF7F6051FB582B01BA00B0C393BA5 (void);
// 0x00000575 System.Void RockVR.Video.VideoCapturePro::GPUCaptureLib_StartEncoding(System.IntPtr,System.String,System.Boolean,System.Int32,System.Boolean,System.Boolean)
extern void VideoCapturePro_GPUCaptureLib_StartEncoding_m1F74D2EB8A75B949CA5AFE40B577249EDC70C30B (void);
// 0x00000576 System.Void RockVR.Video.VideoCapturePro::GPUCaptureLib_AudioEncoding()
extern void VideoCapturePro_GPUCaptureLib_AudioEncoding_mF915DDF8D770A23C05BECACBD80761A0B6108274 (void);
// 0x00000577 System.Void RockVR.Video.VideoCapturePro::GPUCaptureLib_StopEncoding()
extern void VideoCapturePro_GPUCaptureLib_StopEncoding_m8AA2D90DA016ACEDC44945B0A81385D6BF1CA070 (void);
// 0x00000578 System.Void RockVR.Video.VideoCapturePro::GPUCaptureLib_MuxingData(System.String,System.String,System.String)
extern void VideoCapturePro_GPUCaptureLib_MuxingData_m72642DE5FEE516E4085D2A9EE3736627BB875D82 (void);
// 0x00000579 System.Void RockVR.Video.VideoCapturePro::GPUCaptureLib_MuxingClean()
extern void VideoCapturePro_GPUCaptureLib_MuxingClean_m0F722160AA1567E4C3195CB85FA726B2CB87AC07 (void);
// 0x0000057A System.Void RockVR.Video.VideoCapturePro::GPUCaptureLib_StartLiveStream(System.String,System.String,System.Int32)
extern void VideoCapturePro_GPUCaptureLib_StartLiveStream_m2678180E0CC80C7A0BE5863572C126867244DF8C (void);
// 0x0000057B System.Void RockVR.Video.VideoCapturePro::GPUCaptureLib_StopLiveStream()
extern void VideoCapturePro_GPUCaptureLib_StopLiveStream_mFCF82720A11FB6F92AB5C78BB9B1097DAE152689 (void);
// 0x0000057C System.Void RockVR.Video.VideoCapturePro::GPUCaptureLib_SaveScreenShot(System.IntPtr,System.String,System.Boolean)
extern void VideoCapturePro_GPUCaptureLib_SaveScreenShot_m290B5EBD2F645CD34E94CE375BF429D05D807E13 (void);
// 0x0000057D System.Void RockVR.Video.VideoCapturePro::.ctor()
extern void VideoCapturePro__ctor_m422DD25D5F24C7F16D0E8CD6D64D251C89E635CD (void);
// 0x0000057E System.Void RockVR.Video.VideoCapturePro/<CaptureScreenshot>d__44::.ctor(System.Int32)
extern void U3CCaptureScreenshotU3Ed__44__ctor_m5440C36152A5DFFC0AD853BB1035B90027D895BD (void);
// 0x0000057F System.Void RockVR.Video.VideoCapturePro/<CaptureScreenshot>d__44::System.IDisposable.Dispose()
extern void U3CCaptureScreenshotU3Ed__44_System_IDisposable_Dispose_m358F68199E88F6738A424253CB009B5375E784E9 (void);
// 0x00000580 System.Boolean RockVR.Video.VideoCapturePro/<CaptureScreenshot>d__44::MoveNext()
extern void U3CCaptureScreenshotU3Ed__44_MoveNext_m07826777E6F5C4060A590BA884832AF0FFC563B3 (void);
// 0x00000581 System.Object RockVR.Video.VideoCapturePro/<CaptureScreenshot>d__44::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CCaptureScreenshotU3Ed__44_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m8387B032366EAA7E7B95E28FC8B0522201E747CC (void);
// 0x00000582 System.Void RockVR.Video.VideoCapturePro/<CaptureScreenshot>d__44::System.Collections.IEnumerator.Reset()
extern void U3CCaptureScreenshotU3Ed__44_System_Collections_IEnumerator_Reset_mD8684A9E7E36362EC57DA9D67DC4108DB49B44A4 (void);
// 0x00000583 System.Object RockVR.Video.VideoCapturePro/<CaptureScreenshot>d__44::System.Collections.IEnumerator.get_Current()
extern void U3CCaptureScreenshotU3Ed__44_System_Collections_IEnumerator_get_Current_m0833A58D437ECE623F0F58A11D42A2180F16C44F (void);
// 0x00000584 System.Void RockVR.Video.VideoCaptureProCtrl::Awake()
extern void VideoCaptureProCtrl_Awake_mE8A2AE48EC6A91D7A5A8C8B55586C0F451534977 (void);
// 0x00000585 System.Void RockVR.Video.VideoCaptureProCtrl::StartCapture()
extern void VideoCaptureProCtrl_StartCapture_mFCD460982A34F6F3E89EA4CE9CCD770D0F594703 (void);
// 0x00000586 System.Void RockVR.Video.VideoCaptureProCtrl::StopCapture()
extern void VideoCaptureProCtrl_StopCapture_m601F492C7676D4819F234A29F766167BB6DD32C7 (void);
// 0x00000587 System.Void RockVR.Video.VideoCaptureProCtrl::ToggleCapture()
extern void VideoCaptureProCtrl_ToggleCapture_mB0A42003B41193A40FA4D1D4E0F7D7C981925AB4 (void);
// 0x00000588 System.Collections.IEnumerator RockVR.Video.VideoCaptureProCtrl::CheckCapturingFinish()
extern void VideoCaptureProCtrl_CheckCapturingFinish_m998A3B5259F0FC34E5F099D82B2345BD299327E4 (void);
// 0x00000589 System.Collections.IEnumerator RockVR.Video.VideoCaptureProCtrl::SendUploadRequest()
extern void VideoCaptureProCtrl_SendUploadRequest_m2DC30E61C69030A926BCBA105BB42168AC0FBBC6 (void);
// 0x0000058A System.Void RockVR.Video.VideoCaptureProCtrl::.ctor()
extern void VideoCaptureProCtrl__ctor_mB195B1D23B924AF20A1B0F50525CA0DE130F0A7F (void);
// 0x0000058B System.Void RockVR.Video.VideoCaptureProCtrl/<CheckCapturingFinish>d__4::.ctor(System.Int32)
extern void U3CCheckCapturingFinishU3Ed__4__ctor_mC2626C87443EE0FDD5331037FF0E0F2B2F3445D6 (void);
// 0x0000058C System.Void RockVR.Video.VideoCaptureProCtrl/<CheckCapturingFinish>d__4::System.IDisposable.Dispose()
extern void U3CCheckCapturingFinishU3Ed__4_System_IDisposable_Dispose_m8A648104C06258C26D1C34B22EF1346077DBA08F (void);
// 0x0000058D System.Boolean RockVR.Video.VideoCaptureProCtrl/<CheckCapturingFinish>d__4::MoveNext()
extern void U3CCheckCapturingFinishU3Ed__4_MoveNext_mD3E37D1AFE0A0DD95B5BDE2315DDB257D829A872 (void);
// 0x0000058E System.Object RockVR.Video.VideoCaptureProCtrl/<CheckCapturingFinish>d__4::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CCheckCapturingFinishU3Ed__4_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mA5A291D14C23284FEB8ABAE89D6FC5E9CF6C4B5F (void);
// 0x0000058F System.Void RockVR.Video.VideoCaptureProCtrl/<CheckCapturingFinish>d__4::System.Collections.IEnumerator.Reset()
extern void U3CCheckCapturingFinishU3Ed__4_System_Collections_IEnumerator_Reset_mF613813F0DAA0E1178BF7A8BCC7675A5A35F05AB (void);
// 0x00000590 System.Object RockVR.Video.VideoCaptureProCtrl/<CheckCapturingFinish>d__4::System.Collections.IEnumerator.get_Current()
extern void U3CCheckCapturingFinishU3Ed__4_System_Collections_IEnumerator_get_Current_m3C4EEFC55C1050F47D7AD9AB56E5754DD5C971C8 (void);
// 0x00000591 System.Void RockVR.Video.VideoCaptureProCtrl/<SendUploadRequest>d__5::.ctor(System.Int32)
extern void U3CSendUploadRequestU3Ed__5__ctor_m86D838FC6D0B76A2698AF726CAA88A2364144C48 (void);
// 0x00000592 System.Void RockVR.Video.VideoCaptureProCtrl/<SendUploadRequest>d__5::System.IDisposable.Dispose()
extern void U3CSendUploadRequestU3Ed__5_System_IDisposable_Dispose_m75EAA0B82B0D3724100C0F0EDA07A8D060F116A0 (void);
// 0x00000593 System.Boolean RockVR.Video.VideoCaptureProCtrl/<SendUploadRequest>d__5::MoveNext()
extern void U3CSendUploadRequestU3Ed__5_MoveNext_m9257F53A88E2355A27E8F6A5F87FC01E779BDD7C (void);
// 0x00000594 System.Object RockVR.Video.VideoCaptureProCtrl/<SendUploadRequest>d__5::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CSendUploadRequestU3Ed__5_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m716AD8D50AD356974FD83DC3D0C1BCDB2966B275 (void);
// 0x00000595 System.Void RockVR.Video.VideoCaptureProCtrl/<SendUploadRequest>d__5::System.Collections.IEnumerator.Reset()
extern void U3CSendUploadRequestU3Ed__5_System_Collections_IEnumerator_Reset_m3EBCCC7DCCC6E2A46F6BD2A7070CCBCBFEA4EBCB (void);
// 0x00000596 System.Object RockVR.Video.VideoCaptureProCtrl/<SendUploadRequest>d__5::System.Collections.IEnumerator.get_Current()
extern void U3CSendUploadRequestU3Ed__5_System_Collections_IEnumerator_get_Current_mC068709B92FDBD47434B495D4B13A050AEB156F0 (void);
// 0x00000597 System.Void RockVR.Video.VideoPlayer::Awake()
extern void VideoPlayer_Awake_m37E1582665642F406AB2AEBDE0E392EA1BE6AE7F (void);
// 0x00000598 System.Void RockVR.Video.VideoPlayer::SetRootFolder()
extern void VideoPlayer_SetRootFolder_mDC38604AFAB4B5C74B73D70CCF676E0E04394ADB (void);
// 0x00000599 System.Void RockVR.Video.VideoPlayer::PlayVideo()
extern void VideoPlayer_PlayVideo_m8C30CA0222E263520E0FA290C5CD219E2C19CF0E (void);
// 0x0000059A System.Void RockVR.Video.VideoPlayer::NextVideo()
extern void VideoPlayer_NextVideo_mECC9B185712F59750FDC16AD1F0290126C20D2A7 (void);
// 0x0000059B System.Void RockVR.Video.VideoPlayer::.ctor()
extern void VideoPlayer__ctor_m2381501972737BEF5FF684325047C3228526408C (void);
// 0x0000059C System.Void RockVR.Video.Demo.AutoRotate::Start()
extern void AutoRotate_Start_m7A38F11117A8B47DBFC9C385120D90414B2C35BE (void);
// 0x0000059D System.Void RockVR.Video.Demo.AutoRotate::Update()
extern void AutoRotate_Update_m32C70E5891369AC4A54E1D21286043986535B642 (void);
// 0x0000059E System.Void RockVR.Video.Demo.AutoRotate::.ctor()
extern void AutoRotate__ctor_mE2D6A62ADE0168480EDB9388683F7E2B3DBEF24E (void);
// 0x0000059F System.Void RockVR.Video.Demo.VideoCaptureNormaUI::Awake()
extern void VideoCaptureNormaUI_Awake_mCEEAD79DA9680E4009CAED0FB4852F96C761E89D (void);
// 0x000005A0 System.Void RockVR.Video.Demo.VideoCaptureNormaUI::OnGUI()
extern void VideoCaptureNormaUI_OnGUI_m44C9DDCD617F3E6D3E543A12F10CC47911082C17 (void);
// 0x000005A1 System.Void RockVR.Video.Demo.VideoCaptureNormaUI::.ctor()
extern void VideoCaptureNormaUI__ctor_mF0776E817775669D8A9575AE9E1F311DB0705BA0 (void);
// 0x000005A2 System.Void RockVR.Video.Demo.VideoCaptureProUI::Awake()
extern void VideoCaptureProUI_Awake_mA3B070462E1EA63AD829BC780C9EFB5752FF0A46 (void);
// 0x000005A3 System.Void RockVR.Video.Demo.VideoCaptureProUI::OnGUI()
extern void VideoCaptureProUI_OnGUI_m10995E86AC9F19952E96AD7D89776445927CDF81 (void);
// 0x000005A4 System.Void RockVR.Video.Demo.VideoCaptureProUI::.ctor()
extern void VideoCaptureProUI__ctor_m30B95DA5A2979F8C36FCF40EC05B7D00D7F81E4C (void);
// 0x000005A5 System.Void RockVR.Video.Demo.VideoCaptureUI::Awake()
extern void VideoCaptureUI_Awake_m279C3293583F40EC8BBBBBC23AE06F08FD61F015 (void);
// 0x000005A6 System.Void RockVR.Video.Demo.VideoCaptureUI::OnGUI()
extern void VideoCaptureUI_OnGUI_mB85C7F8CE04B62563D0AB98A67CF42E9EA5E0949 (void);
// 0x000005A7 System.Void RockVR.Video.Demo.VideoCaptureUI::.ctor()
extern void VideoCaptureUI__ctor_m5C434FFE0B4F469A426F5F2A41CCC9964BDF25B5 (void);
// 0x000005A8 System.Void RockVR.Common.EventDelegate::.ctor()
extern void EventDelegate__ctor_m06965DDFFC2F7A9853A94C36CFB170615EC0FC8F (void);
// 0x000005A9 System.Void RockVR.Common.EventDelegate/ErrorDelegate::.ctor(System.Object,System.IntPtr)
extern void ErrorDelegate__ctor_m7FCF08BB7D614FAA1CCD31147E0CB6BEC95BF438 (void);
// 0x000005AA System.Void RockVR.Common.EventDelegate/ErrorDelegate::Invoke(System.Int32)
extern void ErrorDelegate_Invoke_m18868889100082913D01DC5A8DFDF5AC5369B1B5 (void);
// 0x000005AB System.IAsyncResult RockVR.Common.EventDelegate/ErrorDelegate::BeginInvoke(System.Int32,System.AsyncCallback,System.Object)
extern void ErrorDelegate_BeginInvoke_mF75AD985DE85036FC9CD15CB61E843658B5639DC (void);
// 0x000005AC System.Void RockVR.Common.EventDelegate/ErrorDelegate::EndInvoke(System.IAsyncResult)
extern void ErrorDelegate_EndInvoke_mF3EBBAAEF551D523CDF11810C96BC95C79B5939F (void);
// 0x000005AD System.Void RockVR.Common.EventDelegate/CompleteDelegate::.ctor(System.Object,System.IntPtr)
extern void CompleteDelegate__ctor_m4281FD3CD64A394E5DD908C9E96EFEE87E2BEDAE (void);
// 0x000005AE System.Void RockVR.Common.EventDelegate/CompleteDelegate::Invoke()
extern void CompleteDelegate_Invoke_m2B967011A5EC153DD0E4388C109848D7433445AA (void);
// 0x000005AF System.IAsyncResult RockVR.Common.EventDelegate/CompleteDelegate::BeginInvoke(System.AsyncCallback,System.Object)
extern void CompleteDelegate_BeginInvoke_mAB776D7C091DC78D22D91E0A3CF14E68289EACFC (void);
// 0x000005B0 System.Void RockVR.Common.EventDelegate/CompleteDelegate::EndInvoke(System.IAsyncResult)
extern void CompleteDelegate_EndInvoke_mA9EE3DD1D8CBC08B0A40B5350BFC4FFBA6827824 (void);
// 0x000005B1 System.Void RockVR.Common.FPSDisplay::Start()
extern void FPSDisplay_Start_mF5790ACD1FF8D265B1C687E29E3FD234036582D8 (void);
// 0x000005B2 System.Void RockVR.Common.FPSDisplay::Update()
extern void FPSDisplay_Update_mB24EA6A5FCAA6A46FF0E2085CCCB623C46AD88E9 (void);
// 0x000005B3 System.Void RockVR.Common.FPSDisplay::.ctor()
extern void FPSDisplay__ctor_m33EE5DFBCC1A3E63DB8796646BB5641FFDBD2727 (void);
// 0x000005B4 System.Boolean RockVR.Common.Platform::IsSupported(RockVR.Common.PlatformType)
extern void Platform_IsSupported_mA0FC8553677FFD47C335863EC9E6C5DE288B4754 (void);
// 0x000005B5 System.Void RockVR.Common.Platform::.ctor()
extern void Platform__ctor_m23F1463A7D76B01DF03466F64F545C90265850D5 (void);
// 0x000005B6 System.Void RockVR.Common.CmdProcess::Run(System.String,System.String)
extern void CmdProcess_Run_mC30E3303B51DDEA442A57E761468C900E901BA3C (void);
// 0x000005B7 System.Void RockVR.Common.CmdProcess::.ctor()
extern void CmdProcess__ctor_m58402B7736247188A60799F5EE17DA178C9B6AA4 (void);
// 0x000005B8 T RockVR.Common.Singleton`1::get_instance()
// 0x000005B9 System.Void RockVR.Common.Singleton`1::Awake()
// 0x000005BA System.Void RockVR.Common.Singleton`1::OnApplicationQuit()
// 0x000005BB System.Void RockVR.Common.Singleton`1::.ctor()
// 0x000005BC System.Void RockVR.Common.Singleton`1::.cctor()
// 0x000005BD System.Int32 RSG.IPromise`1::get_Id()
// 0x000005BE RSG.IPromise`1<PromisedT> RSG.IPromise`1::WithName(System.String)
// 0x000005BF System.Void RSG.IPromise`1::Done(System.Action`1<PromisedT>,System.Action`1<System.Exception>)
// 0x000005C0 System.Void RSG.IPromise`1::Done(System.Action`1<PromisedT>)
// 0x000005C1 System.Void RSG.IPromise`1::Done()
// 0x000005C2 RSG.IPromise RSG.IPromise`1::Catch(System.Action`1<System.Exception>)
// 0x000005C3 RSG.IPromise`1<PromisedT> RSG.IPromise`1::Catch(System.Func`2<System.Exception,PromisedT>)
// 0x000005C4 RSG.IPromise`1<ConvertedT> RSG.IPromise`1::Then(System.Func`2<PromisedT,RSG.IPromise`1<ConvertedT>>)
// 0x000005C5 RSG.IPromise RSG.IPromise`1::Then(System.Func`2<PromisedT,RSG.IPromise>)
// 0x000005C6 RSG.IPromise RSG.IPromise`1::Then(System.Action`1<PromisedT>)
// 0x000005C7 RSG.IPromise`1<ConvertedT> RSG.IPromise`1::Then(System.Func`2<PromisedT,RSG.IPromise`1<ConvertedT>>,System.Func`2<System.Exception,RSG.IPromise`1<ConvertedT>>)
// 0x000005C8 RSG.IPromise RSG.IPromise`1::Then(System.Func`2<PromisedT,RSG.IPromise>,System.Action`1<System.Exception>)
// 0x000005C9 RSG.IPromise RSG.IPromise`1::Then(System.Action`1<PromisedT>,System.Action`1<System.Exception>)
// 0x000005CA RSG.IPromise`1<ConvertedT> RSG.IPromise`1::Then(System.Func`2<PromisedT,RSG.IPromise`1<ConvertedT>>,System.Func`2<System.Exception,RSG.IPromise`1<ConvertedT>>,System.Action`1<System.Single>)
// 0x000005CB RSG.IPromise RSG.IPromise`1::Then(System.Func`2<PromisedT,RSG.IPromise>,System.Action`1<System.Exception>,System.Action`1<System.Single>)
// 0x000005CC RSG.IPromise RSG.IPromise`1::Then(System.Action`1<PromisedT>,System.Action`1<System.Exception>,System.Action`1<System.Single>)
// 0x000005CD RSG.IPromise`1<ConvertedT> RSG.IPromise`1::Then(System.Func`2<PromisedT,ConvertedT>)
// 0x000005CE RSG.IPromise`1<System.Collections.Generic.IEnumerable`1<ConvertedT>> RSG.IPromise`1::ThenAll(System.Func`2<PromisedT,System.Collections.Generic.IEnumerable`1<RSG.IPromise`1<ConvertedT>>>)
// 0x000005CF RSG.IPromise RSG.IPromise`1::ThenAll(System.Func`2<PromisedT,System.Collections.Generic.IEnumerable`1<RSG.IPromise>>)
// 0x000005D0 RSG.IPromise`1<ConvertedT> RSG.IPromise`1::ThenRace(System.Func`2<PromisedT,System.Collections.Generic.IEnumerable`1<RSG.IPromise`1<ConvertedT>>>)
// 0x000005D1 RSG.IPromise RSG.IPromise`1::ThenRace(System.Func`2<PromisedT,System.Collections.Generic.IEnumerable`1<RSG.IPromise>>)
// 0x000005D2 RSG.IPromise`1<PromisedT> RSG.IPromise`1::Finally(System.Action)
// 0x000005D3 RSG.IPromise RSG.IPromise`1::ContinueWith(System.Func`1<RSG.IPromise>)
// 0x000005D4 RSG.IPromise`1<ConvertedT> RSG.IPromise`1::ContinueWith(System.Func`1<RSG.IPromise`1<ConvertedT>>)
// 0x000005D5 RSG.IPromise`1<PromisedT> RSG.IPromise`1::Progress(System.Action`1<System.Single>)
// 0x000005D6 System.Void RSG.IRejectable::Reject(System.Exception)
// 0x000005D7 System.Int32 RSG.IPendingPromise`1::get_Id()
// 0x000005D8 System.Void RSG.IPendingPromise`1::Resolve(PromisedT)
// 0x000005D9 System.Void RSG.IPendingPromise`1::ReportProgress(System.Single)
// 0x000005DA System.Int32 RSG.Promise`1::get_Id()
// 0x000005DB System.String RSG.Promise`1::get_Name()
// 0x000005DC System.Void RSG.Promise`1::set_Name(System.String)
// 0x000005DD RSG.PromiseState RSG.Promise`1::get_CurState()
// 0x000005DE System.Void RSG.Promise`1::set_CurState(RSG.PromiseState)
// 0x000005DF System.Void RSG.Promise`1::.ctor()
// 0x000005E0 System.Void RSG.Promise`1::.ctor(System.Action`2<System.Action`1<PromisedT>,System.Action`1<System.Exception>>)
// 0x000005E1 System.Void RSG.Promise`1::AddRejectHandler(System.Action`1<System.Exception>,RSG.IRejectable)
// 0x000005E2 System.Void RSG.Promise`1::AddResolveHandler(System.Action`1<PromisedT>,RSG.IRejectable)
// 0x000005E3 System.Void RSG.Promise`1::AddProgressHandler(System.Action`1<System.Single>,RSG.IRejectable)
// 0x000005E4 System.Void RSG.Promise`1::InvokeHandler(System.Action`1<T>,RSG.IRejectable,T)
// 0x000005E5 System.Void RSG.Promise`1::ClearHandlers()
// 0x000005E6 System.Void RSG.Promise`1::InvokeRejectHandlers(System.Exception)
// 0x000005E7 System.Void RSG.Promise`1::InvokeResolveHandlers(PromisedT)
// 0x000005E8 System.Void RSG.Promise`1::InvokeProgressHandlers(System.Single)
// 0x000005E9 System.Void RSG.Promise`1::Reject(System.Exception)
// 0x000005EA System.Void RSG.Promise`1::Resolve(PromisedT)
// 0x000005EB System.Void RSG.Promise`1::ReportProgress(System.Single)
// 0x000005EC System.Void RSG.Promise`1::Done(System.Action`1<PromisedT>,System.Action`1<System.Exception>)
// 0x000005ED System.Void RSG.Promise`1::Done(System.Action`1<PromisedT>)
// 0x000005EE System.Void RSG.Promise`1::Done()
// 0x000005EF RSG.IPromise`1<PromisedT> RSG.Promise`1::WithName(System.String)
// 0x000005F0 RSG.IPromise RSG.Promise`1::Catch(System.Action`1<System.Exception>)
// 0x000005F1 RSG.IPromise`1<PromisedT> RSG.Promise`1::Catch(System.Func`2<System.Exception,PromisedT>)
// 0x000005F2 RSG.IPromise`1<ConvertedT> RSG.Promise`1::Then(System.Func`2<PromisedT,RSG.IPromise`1<ConvertedT>>)
// 0x000005F3 RSG.IPromise RSG.Promise`1::Then(System.Func`2<PromisedT,RSG.IPromise>)
// 0x000005F4 RSG.IPromise RSG.Promise`1::Then(System.Action`1<PromisedT>)
// 0x000005F5 RSG.IPromise`1<ConvertedT> RSG.Promise`1::Then(System.Func`2<PromisedT,RSG.IPromise`1<ConvertedT>>,System.Func`2<System.Exception,RSG.IPromise`1<ConvertedT>>)
// 0x000005F6 RSG.IPromise RSG.Promise`1::Then(System.Func`2<PromisedT,RSG.IPromise>,System.Action`1<System.Exception>)
// 0x000005F7 RSG.IPromise RSG.Promise`1::Then(System.Action`1<PromisedT>,System.Action`1<System.Exception>)
// 0x000005F8 RSG.IPromise`1<ConvertedT> RSG.Promise`1::Then(System.Func`2<PromisedT,RSG.IPromise`1<ConvertedT>>,System.Func`2<System.Exception,RSG.IPromise`1<ConvertedT>>,System.Action`1<System.Single>)
// 0x000005F9 RSG.IPromise RSG.Promise`1::Then(System.Func`2<PromisedT,RSG.IPromise>,System.Action`1<System.Exception>,System.Action`1<System.Single>)
// 0x000005FA RSG.IPromise RSG.Promise`1::Then(System.Action`1<PromisedT>,System.Action`1<System.Exception>,System.Action`1<System.Single>)
// 0x000005FB RSG.IPromise`1<ConvertedT> RSG.Promise`1::Then(System.Func`2<PromisedT,ConvertedT>)
// 0x000005FC System.Void RSG.Promise`1::ActionHandlers(RSG.IRejectable,System.Action`1<PromisedT>,System.Action`1<System.Exception>)
// 0x000005FD System.Void RSG.Promise`1::ProgressHandlers(RSG.IRejectable,System.Action`1<System.Single>)
// 0x000005FE RSG.IPromise`1<System.Collections.Generic.IEnumerable`1<ConvertedT>> RSG.Promise`1::ThenAll(System.Func`2<PromisedT,System.Collections.Generic.IEnumerable`1<RSG.IPromise`1<ConvertedT>>>)
// 0x000005FF RSG.IPromise RSG.Promise`1::ThenAll(System.Func`2<PromisedT,System.Collections.Generic.IEnumerable`1<RSG.IPromise>>)
// 0x00000600 RSG.IPromise`1<System.Collections.Generic.IEnumerable`1<PromisedT>> RSG.Promise`1::All(RSG.IPromise`1<PromisedT>[])
// 0x00000601 RSG.IPromise`1<System.Collections.Generic.IEnumerable`1<PromisedT>> RSG.Promise`1::All(System.Collections.Generic.IEnumerable`1<RSG.IPromise`1<PromisedT>>)
// 0x00000602 RSG.IPromise`1<ConvertedT> RSG.Promise`1::ThenRace(System.Func`2<PromisedT,System.Collections.Generic.IEnumerable`1<RSG.IPromise`1<ConvertedT>>>)
// 0x00000603 RSG.IPromise RSG.Promise`1::ThenRace(System.Func`2<PromisedT,System.Collections.Generic.IEnumerable`1<RSG.IPromise>>)
// 0x00000604 RSG.IPromise`1<PromisedT> RSG.Promise`1::Race(RSG.IPromise`1<PromisedT>[])
// 0x00000605 RSG.IPromise`1<PromisedT> RSG.Promise`1::Race(System.Collections.Generic.IEnumerable`1<RSG.IPromise`1<PromisedT>>)
// 0x00000606 RSG.IPromise`1<PromisedT> RSG.Promise`1::Resolved(PromisedT)
// 0x00000607 RSG.IPromise`1<PromisedT> RSG.Promise`1::Rejected(System.Exception)
// 0x00000608 RSG.IPromise`1<PromisedT> RSG.Promise`1::Finally(System.Action)
// 0x00000609 RSG.IPromise RSG.Promise`1::ContinueWith(System.Func`1<RSG.IPromise>)
// 0x0000060A RSG.IPromise`1<ConvertedT> RSG.Promise`1::ContinueWith(System.Func`1<RSG.IPromise`1<ConvertedT>>)
// 0x0000060B RSG.IPromise`1<PromisedT> RSG.Promise`1::Progress(System.Action`1<System.Single>)
// 0x0000060C System.Void RSG.Promise`1::<Done>b__30_0(System.Exception)
// 0x0000060D System.Void RSG.Promise`1::<Done>b__31_0(System.Exception)
// 0x0000060E System.Void RSG.Promise`1::<Done>b__32_0(System.Exception)
// 0x0000060F System.Void RSG.Promise`1/<>c__DisplayClass24_0::.ctor()
// 0x00000610 System.Void RSG.Promise`1/<>c__DisplayClass24_0::<InvokeRejectHandlers>b__0(RSG.RejectHandler)
// 0x00000611 System.Void RSG.Promise`1/<>c__DisplayClass26_0::.ctor()
// 0x00000612 System.Void RSG.Promise`1/<>c__DisplayClass26_0::<InvokeProgressHandlers>b__0(RSG.ProgressHandler)
// 0x00000613 System.Void RSG.Promise`1/<>c__DisplayClass34_0::.ctor()
// 0x00000614 System.Void RSG.Promise`1/<>c__DisplayClass34_0::<Catch>b__0(PromisedT)
// 0x00000615 System.Void RSG.Promise`1/<>c__DisplayClass34_0::<Catch>b__1(System.Exception)
// 0x00000616 System.Void RSG.Promise`1/<>c__DisplayClass34_0::<Catch>b__2(System.Single)
// 0x00000617 System.Void RSG.Promise`1/<>c__DisplayClass35_0::.ctor()
// 0x00000618 System.Void RSG.Promise`1/<>c__DisplayClass35_0::<Catch>b__0(PromisedT)
// 0x00000619 System.Void RSG.Promise`1/<>c__DisplayClass35_0::<Catch>b__1(System.Exception)
// 0x0000061A System.Void RSG.Promise`1/<>c__DisplayClass35_0::<Catch>b__2(System.Single)
// 0x0000061B System.Void RSG.Promise`1/<>c__DisplayClass42_0`1::.ctor()
// 0x0000061C System.Void RSG.Promise`1/<>c__DisplayClass42_0`1::<Then>b__0(PromisedT)
// 0x0000061D System.Void RSG.Promise`1/<>c__DisplayClass42_0`1::<Then>b__2(System.Single)
// 0x0000061E System.Void RSG.Promise`1/<>c__DisplayClass42_0`1::<Then>b__3(ConvertedT)
// 0x0000061F System.Void RSG.Promise`1/<>c__DisplayClass42_0`1::<Then>b__4(System.Exception)
// 0x00000620 System.Void RSG.Promise`1/<>c__DisplayClass42_0`1::<Then>b__1(System.Exception)
// 0x00000621 System.Void RSG.Promise`1/<>c__DisplayClass42_0`1::<Then>b__5(ConvertedT)
// 0x00000622 System.Void RSG.Promise`1/<>c__DisplayClass42_0`1::<Then>b__6(System.Exception)
// 0x00000623 System.Void RSG.Promise`1/<>c__DisplayClass43_0::.ctor()
// 0x00000624 System.Void RSG.Promise`1/<>c__DisplayClass43_0::<Then>b__0(PromisedT)
// 0x00000625 System.Void RSG.Promise`1/<>c__DisplayClass43_0::<Then>b__2(System.Single)
// 0x00000626 System.Void RSG.Promise`1/<>c__DisplayClass43_0::<Then>b__3()
// 0x00000627 System.Void RSG.Promise`1/<>c__DisplayClass43_0::<Then>b__4(System.Exception)
// 0x00000628 System.Void RSG.Promise`1/<>c__DisplayClass43_0::<Then>b__1(System.Exception)
// 0x00000629 System.Void RSG.Promise`1/<>c__DisplayClass44_0::.ctor()
// 0x0000062A System.Void RSG.Promise`1/<>c__DisplayClass44_0::<Then>b__0(PromisedT)
// 0x0000062B System.Void RSG.Promise`1/<>c__DisplayClass44_0::<Then>b__1(System.Exception)
// 0x0000062C System.Void RSG.Promise`1/<>c__DisplayClass45_0`1::.ctor()
// 0x0000062D RSG.IPromise`1<ConvertedT> RSG.Promise`1/<>c__DisplayClass45_0`1::<Then>b__0(PromisedT)
// 0x0000062E System.Void RSG.Promise`1/<>c__DisplayClass48_0`1::.ctor()
// 0x0000062F RSG.IPromise`1<System.Collections.Generic.IEnumerable`1<ConvertedT>> RSG.Promise`1/<>c__DisplayClass48_0`1::<ThenAll>b__0(PromisedT)
// 0x00000630 System.Void RSG.Promise`1/<>c__DisplayClass49_0::.ctor()
// 0x00000631 RSG.IPromise RSG.Promise`1/<>c__DisplayClass49_0::<ThenAll>b__0(PromisedT)
// 0x00000632 System.Void RSG.Promise`1/<>c__DisplayClass51_0::.ctor()
// 0x00000633 System.Void RSG.Promise`1/<>c__DisplayClass51_0::<All>b__0(RSG.IPromise`1<PromisedT>,System.Int32)
// 0x00000634 System.Void RSG.Promise`1/<>c__DisplayClass51_0::<All>b__3(System.Exception)
// 0x00000635 System.Void RSG.Promise`1/<>c__DisplayClass51_1::.ctor()
// 0x00000636 System.Void RSG.Promise`1/<>c__DisplayClass51_1::<All>b__1(System.Single)
// 0x00000637 System.Void RSG.Promise`1/<>c__DisplayClass51_1::<All>b__2(PromisedT)
// 0x00000638 System.Void RSG.Promise`1/<>c__DisplayClass52_0`1::.ctor()
// 0x00000639 RSG.IPromise`1<ConvertedT> RSG.Promise`1/<>c__DisplayClass52_0`1::<ThenRace>b__0(PromisedT)
// 0x0000063A System.Void RSG.Promise`1/<>c__DisplayClass53_0::.ctor()
// 0x0000063B RSG.IPromise RSG.Promise`1/<>c__DisplayClass53_0::<ThenRace>b__0(PromisedT)
// 0x0000063C System.Void RSG.Promise`1/<>c__DisplayClass55_0::.ctor()
// 0x0000063D System.Void RSG.Promise`1/<>c__DisplayClass55_0::<Race>b__0(RSG.IPromise`1<PromisedT>,System.Int32)
// 0x0000063E System.Void RSG.Promise`1/<>c__DisplayClass55_0::<Race>b__2(PromisedT)
// 0x0000063F System.Void RSG.Promise`1/<>c__DisplayClass55_0::<Race>b__3(System.Exception)
// 0x00000640 System.Void RSG.Promise`1/<>c__DisplayClass55_1::.ctor()
// 0x00000641 System.Void RSG.Promise`1/<>c__DisplayClass55_1::<Race>b__1(System.Single)
// 0x00000642 System.Void RSG.Promise`1/<>c__DisplayClass58_0::.ctor()
// 0x00000643 System.Void RSG.Promise`1/<>c__DisplayClass58_0::<Finally>b__0(PromisedT)
// 0x00000644 System.Void RSG.Promise`1/<>c__DisplayClass58_0::<Finally>b__1(System.Exception)
// 0x00000645 PromisedT RSG.Promise`1/<>c__DisplayClass58_0::<Finally>b__2(PromisedT)
// 0x00000646 System.Void RSG.Promise`1/<>c__DisplayClass59_0::.ctor()
// 0x00000647 System.Void RSG.Promise`1/<>c__DisplayClass59_0::<ContinueWith>b__0(PromisedT)
// 0x00000648 System.Void RSG.Promise`1/<>c__DisplayClass59_0::<ContinueWith>b__1(System.Exception)
// 0x00000649 System.Void RSG.Promise`1/<>c__DisplayClass60_0`1::.ctor()
// 0x0000064A System.Void RSG.Promise`1/<>c__DisplayClass60_0`1::<ContinueWith>b__0(PromisedT)
// 0x0000064B System.Void RSG.Promise`1/<>c__DisplayClass60_0`1::<ContinueWith>b__1(System.Exception)
// 0x0000064C RSG.IPromise`1<RSG.Tuple`2<T1,T2>> RSG.PromiseHelpers::All(RSG.IPromise`1<T1>,RSG.IPromise`1<T2>)
// 0x0000064D RSG.IPromise`1<RSG.Tuple`3<T1,T2,T3>> RSG.PromiseHelpers::All(RSG.IPromise`1<T1>,RSG.IPromise`1<T2>,RSG.IPromise`1<T3>)
// 0x0000064E RSG.IPromise`1<RSG.Tuple`4<T1,T2,T3,T4>> RSG.PromiseHelpers::All(RSG.IPromise`1<T1>,RSG.IPromise`1<T2>,RSG.IPromise`1<T3>,RSG.IPromise`1<T4>)
// 0x0000064F System.Void RSG.PromiseHelpers/<>c__DisplayClass0_0`2::.ctor()
// 0x00000650 System.Void RSG.PromiseHelpers/<>c__DisplayClass0_0`2::<All>b__0(T1)
// 0x00000651 System.Void RSG.PromiseHelpers/<>c__DisplayClass0_0`2::<All>b__1(System.Exception)
// 0x00000652 System.Void RSG.PromiseHelpers/<>c__DisplayClass0_0`2::<All>b__2(T2)
// 0x00000653 System.Void RSG.PromiseHelpers/<>c__DisplayClass0_0`2::<All>b__3(System.Exception)
// 0x00000654 System.Void RSG.PromiseHelpers/<>c__1`3::.cctor()
// 0x00000655 System.Void RSG.PromiseHelpers/<>c__1`3::.ctor()
// 0x00000656 RSG.Tuple`3<T1,T2,T3> RSG.PromiseHelpers/<>c__1`3::<All>b__1_0(RSG.Tuple`2<RSG.Tuple`2<T1,T2>,T3>)
// 0x00000657 System.Void RSG.PromiseHelpers/<>c__2`4::.cctor()
// 0x00000658 System.Void RSG.PromiseHelpers/<>c__2`4::.ctor()
// 0x00000659 RSG.Tuple`4<T1,T2,T3,T4> RSG.PromiseHelpers/<>c__2`4::<All>b__2_0(RSG.Tuple`2<RSG.Tuple`2<T1,T2>,RSG.Tuple`2<T3,T4>>)
// 0x0000065A System.Void RSG.PromiseCancelledException::.ctor()
extern void PromiseCancelledException__ctor_mDA55F2F9C6DF87917C0A40BC149A437C750C66FD (void);
// 0x0000065B System.Void RSG.PromiseCancelledException::.ctor(System.String)
extern void PromiseCancelledException__ctor_m0733E2CEAEE71A6A1A22E96FFF62D85627437BF1 (void);
// 0x0000065C System.Void RSG.PredicateWait::.ctor()
extern void PredicateWait__ctor_mCD6942E554ED82AF36B3F07828C4E5BCA7F0D318 (void);
// 0x0000065D RSG.IPromise RSG.IPromiseTimer::WaitFor(System.Single)
// 0x0000065E RSG.IPromise RSG.IPromiseTimer::WaitUntil(System.Func`2<RSG.TimeData,System.Boolean>)
// 0x0000065F RSG.IPromise RSG.IPromiseTimer::WaitWhile(System.Func`2<RSG.TimeData,System.Boolean>)
// 0x00000660 System.Void RSG.IPromiseTimer::Update(System.Single)
// 0x00000661 System.Boolean RSG.IPromiseTimer::Cancel(RSG.IPromise)
// 0x00000662 RSG.IPromise RSG.PromiseTimer::WaitFor(System.Single)
extern void PromiseTimer_WaitFor_m99B243125D5686DC36844FA69D796651E12C9B76 (void);
// 0x00000663 RSG.IPromise RSG.PromiseTimer::WaitWhile(System.Func`2<RSG.TimeData,System.Boolean>)
extern void PromiseTimer_WaitWhile_m8EF7C405289D3A2AE7F7185AE8E4E901BF504EC9 (void);
// 0x00000664 RSG.IPromise RSG.PromiseTimer::WaitUntil(System.Func`2<RSG.TimeData,System.Boolean>)
extern void PromiseTimer_WaitUntil_m7AC7AE2AFC6CB5E43A789DE5A6E683D62D23FBFC (void);
// 0x00000665 System.Boolean RSG.PromiseTimer::Cancel(RSG.IPromise)
extern void PromiseTimer_Cancel_m87F8524626DDC28FB1ECA1968F73A2D18540704E (void);
// 0x00000666 System.Collections.Generic.LinkedListNode`1<RSG.PredicateWait> RSG.PromiseTimer::FindInWaiting(RSG.IPromise)
extern void PromiseTimer_FindInWaiting_mA1660E092E5065B151FB000488B2C87FF4C6C8F1 (void);
// 0x00000667 System.Void RSG.PromiseTimer::Update(System.Single)
extern void PromiseTimer_Update_m9C84E7E7B30B14A7C13510CC22AB6032C7C0874F (void);
// 0x00000668 System.Collections.Generic.LinkedListNode`1<RSG.PredicateWait> RSG.PromiseTimer::RemoveNode(System.Collections.Generic.LinkedListNode`1<RSG.PredicateWait>)
extern void PromiseTimer_RemoveNode_mF32CC3AE04AE1AFBCEF4B8DA7DFD9E2AF02A6E89 (void);
// 0x00000669 System.Void RSG.PromiseTimer::.ctor()
extern void PromiseTimer__ctor_m3F1298B21CD5644C3AE646911AD13B6FCCD19BE6 (void);
// 0x0000066A System.Void RSG.PromiseTimer/<>c__DisplayClass3_0::.ctor()
extern void U3CU3Ec__DisplayClass3_0__ctor_mC51BCEA4436030F27897F20585D501ABF744C78E (void);
// 0x0000066B System.Boolean RSG.PromiseTimer/<>c__DisplayClass3_0::<WaitFor>b__0(RSG.TimeData)
extern void U3CU3Ec__DisplayClass3_0_U3CWaitForU3Eb__0_m7EEE8458FA739B7F87A8EF705E05039C9EFB483D (void);
// 0x0000066C System.Void RSG.PromiseTimer/<>c__DisplayClass4_0::.ctor()
extern void U3CU3Ec__DisplayClass4_0__ctor_m8884F549B84F64B7E7D4B61E036A398416091313 (void);
// 0x0000066D System.Boolean RSG.PromiseTimer/<>c__DisplayClass4_0::<WaitWhile>b__0(RSG.TimeData)
extern void U3CU3Ec__DisplayClass4_0_U3CWaitWhileU3Eb__0_m8DF4AC7205D6CD8FF4E8AE640F92F6AB4A9B59F7 (void);
// 0x0000066E System.Int32 RSG.IPromise::get_Id()
// 0x0000066F RSG.IPromise RSG.IPromise::WithName(System.String)
// 0x00000670 System.Void RSG.IPromise::Done(System.Action,System.Action`1<System.Exception>)
// 0x00000671 System.Void RSG.IPromise::Done(System.Action)
// 0x00000672 System.Void RSG.IPromise::Done()
// 0x00000673 RSG.IPromise RSG.IPromise::Catch(System.Action`1<System.Exception>)
// 0x00000674 RSG.IPromise`1<ConvertedT> RSG.IPromise::Then(System.Func`1<RSG.IPromise`1<ConvertedT>>)
// 0x00000675 RSG.IPromise RSG.IPromise::Then(System.Func`1<RSG.IPromise>)
// 0x00000676 RSG.IPromise RSG.IPromise::Then(System.Action)
// 0x00000677 RSG.IPromise`1<ConvertedT> RSG.IPromise::Then(System.Func`1<RSG.IPromise`1<ConvertedT>>,System.Func`2<System.Exception,RSG.IPromise`1<ConvertedT>>)
// 0x00000678 RSG.IPromise RSG.IPromise::Then(System.Func`1<RSG.IPromise>,System.Action`1<System.Exception>)
// 0x00000679 RSG.IPromise RSG.IPromise::Then(System.Action,System.Action`1<System.Exception>)
// 0x0000067A RSG.IPromise`1<ConvertedT> RSG.IPromise::Then(System.Func`1<RSG.IPromise`1<ConvertedT>>,System.Func`2<System.Exception,RSG.IPromise`1<ConvertedT>>,System.Action`1<System.Single>)
// 0x0000067B RSG.IPromise RSG.IPromise::Then(System.Func`1<RSG.IPromise>,System.Action`1<System.Exception>,System.Action`1<System.Single>)
// 0x0000067C RSG.IPromise RSG.IPromise::Then(System.Action,System.Action`1<System.Exception>,System.Action`1<System.Single>)
// 0x0000067D RSG.IPromise RSG.IPromise::ThenAll(System.Func`1<System.Collections.Generic.IEnumerable`1<RSG.IPromise>>)
// 0x0000067E RSG.IPromise`1<System.Collections.Generic.IEnumerable`1<ConvertedT>> RSG.IPromise::ThenAll(System.Func`1<System.Collections.Generic.IEnumerable`1<RSG.IPromise`1<ConvertedT>>>)
// 0x0000067F RSG.IPromise RSG.IPromise::ThenSequence(System.Func`1<System.Collections.Generic.IEnumerable`1<System.Func`1<RSG.IPromise>>>)
// 0x00000680 RSG.IPromise RSG.IPromise::ThenRace(System.Func`1<System.Collections.Generic.IEnumerable`1<RSG.IPromise>>)
// 0x00000681 RSG.IPromise`1<ConvertedT> RSG.IPromise::ThenRace(System.Func`1<System.Collections.Generic.IEnumerable`1<RSG.IPromise`1<ConvertedT>>>)
// 0x00000682 RSG.IPromise RSG.IPromise::Finally(System.Action)
// 0x00000683 RSG.IPromise RSG.IPromise::ContinueWith(System.Func`1<RSG.IPromise>)
// 0x00000684 RSG.IPromise`1<ConvertedT> RSG.IPromise::ContinueWith(System.Func`1<RSG.IPromise`1<ConvertedT>>)
// 0x00000685 RSG.IPromise RSG.IPromise::Progress(System.Action`1<System.Single>)
// 0x00000686 System.Int32 RSG.IPendingPromise::get_Id()
// 0x00000687 System.Void RSG.IPendingPromise::Resolve()
// 0x00000688 System.Void RSG.IPendingPromise::ReportProgress(System.Single)
// 0x00000689 System.Int32 RSG.IPromiseInfo::get_Id()
// 0x0000068A System.String RSG.IPromiseInfo::get_Name()
// 0x0000068B System.Void RSG.ExceptionEventArgs::.ctor(System.Exception)
extern void ExceptionEventArgs__ctor_m7023DA6E99C0B23D96D0AAB0500F16B19345A30C (void);
// 0x0000068C System.Exception RSG.ExceptionEventArgs::get_Exception()
extern void ExceptionEventArgs_get_Exception_m79958EAB2CAA0E6C91E10902A3ABCC242012745A (void);
// 0x0000068D System.Void RSG.ExceptionEventArgs::set_Exception(System.Exception)
extern void ExceptionEventArgs_set_Exception_mBEFA8B8A6990A708691413D5E188FD73D6036A88 (void);
// 0x0000068E System.Void RSG.Promise::add_UnhandledException(System.EventHandler`1<RSG.ExceptionEventArgs>)
extern void Promise_add_UnhandledException_m501ED8710D326733D0A4B095516E79AD93771A17 (void);
// 0x0000068F System.Void RSG.Promise::remove_UnhandledException(System.EventHandler`1<RSG.ExceptionEventArgs>)
extern void Promise_remove_UnhandledException_m0A7255398B93C672C3F4AD9AE8CC1260E6BDC493 (void);
// 0x00000690 System.Collections.Generic.IEnumerable`1<RSG.IPromiseInfo> RSG.Promise::GetPendingPromises()
extern void Promise_GetPendingPromises_mCD78AA3FD69C8809D95E0FBBC6DC24DB648CD1E4 (void);
// 0x00000691 System.Int32 RSG.Promise::get_Id()
extern void Promise_get_Id_m2A159B9C83B813983859B9F8497B92A636D26198 (void);
// 0x00000692 System.String RSG.Promise::get_Name()
extern void Promise_get_Name_m2681487A6A6188C86D6B1FEE6F4E42F0817F0AF1 (void);
// 0x00000693 System.Void RSG.Promise::set_Name(System.String)
extern void Promise_set_Name_m04BD79E80089945F37642A501B2E0C621CFAADFF (void);
// 0x00000694 RSG.PromiseState RSG.Promise::get_CurState()
extern void Promise_get_CurState_mBE60E772D62CB2C8449BF67F3A2106C0E0FA720D (void);
// 0x00000695 System.Void RSG.Promise::set_CurState(RSG.PromiseState)
extern void Promise_set_CurState_mC9014B9835DB4BDFE7B3E1AFC0B61A249DB0B148 (void);
// 0x00000696 System.Void RSG.Promise::.ctor()
extern void Promise__ctor_m72891E8C449A8F0285F2BBEE6596EEE91D1B69D0 (void);
// 0x00000697 System.Void RSG.Promise::.ctor(System.Action`2<System.Action,System.Action`1<System.Exception>>)
extern void Promise__ctor_m0F170C315BB66058A0B8DB59D2FB2E269460DB4A (void);
// 0x00000698 System.Int32 RSG.Promise::NextId()
extern void Promise_NextId_m91D574BF971140F5BCA824858A1F23785E625936 (void);
// 0x00000699 System.Void RSG.Promise::AddRejectHandler(System.Action`1<System.Exception>,RSG.IRejectable)
extern void Promise_AddRejectHandler_m8A5C912F1C8D8FC752EAAFCC3397992F84DE2C83 (void);
// 0x0000069A System.Void RSG.Promise::AddResolveHandler(System.Action,RSG.IRejectable)
extern void Promise_AddResolveHandler_m39C28AC656C57C1865250657901C41569B49B87A (void);
// 0x0000069B System.Void RSG.Promise::AddProgressHandler(System.Action`1<System.Single>,RSG.IRejectable)
extern void Promise_AddProgressHandler_mC3147E564182B45C04B4AD18F55200ADE09494A9 (void);
// 0x0000069C System.Void RSG.Promise::InvokeRejectHandler(System.Action`1<System.Exception>,RSG.IRejectable,System.Exception)
extern void Promise_InvokeRejectHandler_mD002929AA239DA2F2D3CBA6B828BCC190E2FA010 (void);
// 0x0000069D System.Void RSG.Promise::InvokeResolveHandler(System.Action,RSG.IRejectable)
extern void Promise_InvokeResolveHandler_m8253411D16B16952735214A8D4840B69CBC19367 (void);
// 0x0000069E System.Void RSG.Promise::InvokeProgressHandler(System.Action`1<System.Single>,RSG.IRejectable,System.Single)
extern void Promise_InvokeProgressHandler_m2767B570D7BE8E457C295823CFD48F4DF079AD1F (void);
// 0x0000069F System.Void RSG.Promise::ClearHandlers()
extern void Promise_ClearHandlers_m77595F54867BFFB97E19A6354940D3C574E8AF30 (void);
// 0x000006A0 System.Void RSG.Promise::InvokeRejectHandlers(System.Exception)
extern void Promise_InvokeRejectHandlers_m78C4A77BB7F1728BCF63373111A992940FD73FBA (void);
// 0x000006A1 System.Void RSG.Promise::InvokeResolveHandlers()
extern void Promise_InvokeResolveHandlers_m071F1B5884447CEE1F7D19154AB430522BE6C208 (void);
// 0x000006A2 System.Void RSG.Promise::InvokeProgressHandlers(System.Single)
extern void Promise_InvokeProgressHandlers_m33BBE047F8B32BDC15B943203CF4D653C10F5F3D (void);
// 0x000006A3 System.Void RSG.Promise::Reject(System.Exception)
extern void Promise_Reject_mA334C811DFA609A9294A15B8C4FE3D06CFE59E4B (void);
// 0x000006A4 System.Void RSG.Promise::Resolve()
extern void Promise_Resolve_mB395CBF764AD56F64C953F240F5525C4F24AA176 (void);
// 0x000006A5 System.Void RSG.Promise::ReportProgress(System.Single)
extern void Promise_ReportProgress_m7496AD24A305E551C317FAF83A339D50555EC2F0 (void);
// 0x000006A6 System.Void RSG.Promise::Done(System.Action,System.Action`1<System.Exception>)
extern void Promise_Done_mBF4D40ECA5734EFE1213BD2AD11124841BCA6C63 (void);
// 0x000006A7 System.Void RSG.Promise::Done(System.Action)
extern void Promise_Done_mA20B58D299DFC458330AB673679F52760076ED6D (void);
// 0x000006A8 System.Void RSG.Promise::Done()
extern void Promise_Done_mB94F090BD0EDC421C98291DF2437788619416322 (void);
// 0x000006A9 RSG.IPromise RSG.Promise::WithName(System.String)
extern void Promise_WithName_m9A8610E5066D79F98133FBDE0C427032A9EF04DB (void);
// 0x000006AA RSG.IPromise RSG.Promise::Catch(System.Action`1<System.Exception>)
extern void Promise_Catch_m34A1A6F483209E344CEE20CAECA131319E25A35F (void);
// 0x000006AB RSG.IPromise`1<ConvertedT> RSG.Promise::Then(System.Func`1<RSG.IPromise`1<ConvertedT>>)
// 0x000006AC RSG.IPromise RSG.Promise::Then(System.Func`1<RSG.IPromise>)
extern void Promise_Then_m660D492D7CB977D2658FDC71CA16C5F1C7C80BFB (void);
// 0x000006AD RSG.IPromise RSG.Promise::Then(System.Action)
extern void Promise_Then_mE35145837C7EBB840D64C24A92FFEC013AA4E52F (void);
// 0x000006AE RSG.IPromise`1<ConvertedT> RSG.Promise::Then(System.Func`1<RSG.IPromise`1<ConvertedT>>,System.Func`2<System.Exception,RSG.IPromise`1<ConvertedT>>)
// 0x000006AF RSG.IPromise RSG.Promise::Then(System.Func`1<RSG.IPromise>,System.Action`1<System.Exception>)
extern void Promise_Then_m9C5987E6126BE35855402986BE5878DDDA5DDD2F (void);
// 0x000006B0 RSG.IPromise RSG.Promise::Then(System.Action,System.Action`1<System.Exception>)
extern void Promise_Then_m1F2DA120D93DB9D1801A4410BACE242992BFDE5D (void);
// 0x000006B1 RSG.IPromise`1<ConvertedT> RSG.Promise::Then(System.Func`1<RSG.IPromise`1<ConvertedT>>,System.Func`2<System.Exception,RSG.IPromise`1<ConvertedT>>,System.Action`1<System.Single>)
// 0x000006B2 RSG.IPromise RSG.Promise::Then(System.Func`1<RSG.IPromise>,System.Action`1<System.Exception>,System.Action`1<System.Single>)
extern void Promise_Then_mE03C61503CE58EA2981EF96852E6F252B6F29736 (void);
// 0x000006B3 RSG.IPromise RSG.Promise::Then(System.Action,System.Action`1<System.Exception>,System.Action`1<System.Single>)
extern void Promise_Then_m6693CA7F4467B53237CE92F7F44C36CD44FABE0D (void);
// 0x000006B4 System.Void RSG.Promise::ActionHandlers(RSG.IRejectable,System.Action,System.Action`1<System.Exception>)
extern void Promise_ActionHandlers_m226D1CB18140648C8EC32E3E2FA7DF0730478F26 (void);
// 0x000006B5 System.Void RSG.Promise::ProgressHandlers(RSG.IRejectable,System.Action`1<System.Single>)
extern void Promise_ProgressHandlers_m21C8B94D38E0FBF1F14B24FCFFFEC214D9D80912 (void);
// 0x000006B6 RSG.IPromise RSG.Promise::ThenAll(System.Func`1<System.Collections.Generic.IEnumerable`1<RSG.IPromise>>)
extern void Promise_ThenAll_m444DBB4B6489921A44CDCF98AB8D3F2708372D66 (void);
// 0x000006B7 RSG.IPromise`1<System.Collections.Generic.IEnumerable`1<ConvertedT>> RSG.Promise::ThenAll(System.Func`1<System.Collections.Generic.IEnumerable`1<RSG.IPromise`1<ConvertedT>>>)
// 0x000006B8 RSG.IPromise RSG.Promise::All(RSG.IPromise[])
extern void Promise_All_m4021CBAF737AB75139BE836A21E02B28B238AD21 (void);
// 0x000006B9 RSG.IPromise RSG.Promise::All(System.Collections.Generic.IEnumerable`1<RSG.IPromise>)
extern void Promise_All_mD80850322247A01D346893F8962617690B9D2606 (void);
// 0x000006BA RSG.IPromise RSG.Promise::ThenSequence(System.Func`1<System.Collections.Generic.IEnumerable`1<System.Func`1<RSG.IPromise>>>)
extern void Promise_ThenSequence_mE42D6CD848060EB59C10345B5A14B16724D35F9B (void);
// 0x000006BB RSG.IPromise RSG.Promise::Sequence(System.Func`1<RSG.IPromise>[])
extern void Promise_Sequence_m9F9C078AA9DB0404DAB83831A3EF769379E58906 (void);
// 0x000006BC RSG.IPromise RSG.Promise::Sequence(System.Collections.Generic.IEnumerable`1<System.Func`1<RSG.IPromise>>)
extern void Promise_Sequence_m646E63C1583234FE0B9EC2F642D5B1EB7A830E52 (void);
// 0x000006BD RSG.IPromise RSG.Promise::ThenRace(System.Func`1<System.Collections.Generic.IEnumerable`1<RSG.IPromise>>)
extern void Promise_ThenRace_mF04FB2EDFB9FF1589EF714F409994945E123EE7D (void);
// 0x000006BE RSG.IPromise`1<ConvertedT> RSG.Promise::ThenRace(System.Func`1<System.Collections.Generic.IEnumerable`1<RSG.IPromise`1<ConvertedT>>>)
// 0x000006BF RSG.IPromise RSG.Promise::Race(RSG.IPromise[])
extern void Promise_Race_m7A61FE78FC907244940AE23BE119699E657860BB (void);
// 0x000006C0 RSG.IPromise RSG.Promise::Race(System.Collections.Generic.IEnumerable`1<RSG.IPromise>)
extern void Promise_Race_m422B8D03C310B70EC818A1A5F7A8E12C836FD877 (void);
// 0x000006C1 RSG.IPromise RSG.Promise::Resolved()
extern void Promise_Resolved_m27D7349EFBB518D89093ADB573BD7D7F836B24DE (void);
// 0x000006C2 RSG.IPromise RSG.Promise::Rejected(System.Exception)
extern void Promise_Rejected_mF6CA3689BE449406F16A5D145B1DD4FD13C41890 (void);
// 0x000006C3 RSG.IPromise RSG.Promise::Finally(System.Action)
extern void Promise_Finally_m762D2CD8F1F48788A1B94FB46CB338D69083230B (void);
// 0x000006C4 RSG.IPromise RSG.Promise::ContinueWith(System.Func`1<RSG.IPromise>)
extern void Promise_ContinueWith_m1A53BD142DC421621617AE8E0BE6DA2053D96D8A (void);
// 0x000006C5 RSG.IPromise`1<ConvertedT> RSG.Promise::ContinueWith(System.Func`1<RSG.IPromise`1<ConvertedT>>)
// 0x000006C6 RSG.IPromise RSG.Promise::Progress(System.Action`1<System.Single>)
extern void Promise_Progress_m53E64BDA8764C5E88057601FAB54BF75FD0B368A (void);
// 0x000006C7 System.Void RSG.Promise::PropagateUnhandledException(System.Object,System.Exception)
extern void Promise_PropagateUnhandledException_m30B4923F2598FA6B4F7CD5964E00760B5D9F10B7 (void);
// 0x000006C8 System.Void RSG.Promise::.cctor()
extern void Promise__cctor_mB03C9B64D5DF3D552E96C4FCFA942B128173413D (void);
// 0x000006C9 System.Void RSG.Promise::<InvokeResolveHandlers>b__35_0(RSG.Promise/ResolveHandler)
extern void Promise_U3CInvokeResolveHandlersU3Eb__35_0_mE4D9BB0CB11B33C2E7F12BFE44103CC546FD2E2A (void);
// 0x000006CA System.Void RSG.Promise::<Done>b__40_0(System.Exception)
extern void Promise_U3CDoneU3Eb__40_0_m03D038B6097EEEF2B736CCE4A1D9154518BE3E01 (void);
// 0x000006CB System.Void RSG.Promise::<Done>b__41_0(System.Exception)
extern void Promise_U3CDoneU3Eb__41_0_mDE5A60CCBCC7F17ADD74A395C92F553EAAA52817 (void);
// 0x000006CC System.Void RSG.Promise::<Done>b__42_0(System.Exception)
extern void Promise_U3CDoneU3Eb__42_0_m10D5F4FC16A8A05494ED431EA28AA288DB13AE3B (void);
// 0x000006CD System.Void RSG.Promise/<>c__DisplayClass34_0::.ctor()
extern void U3CU3Ec__DisplayClass34_0__ctor_mF81EF54B39A864EB5E463DA04D72B5F8A0CF4DFC (void);
// 0x000006CE System.Void RSG.Promise/<>c__DisplayClass34_0::<InvokeRejectHandlers>b__0(RSG.RejectHandler)
extern void U3CU3Ec__DisplayClass34_0_U3CInvokeRejectHandlersU3Eb__0_mC2CB7940F03A8D527C6C9BA7793BA6C6A07E13FC (void);
// 0x000006CF System.Void RSG.Promise/<>c__DisplayClass36_0::.ctor()
extern void U3CU3Ec__DisplayClass36_0__ctor_m18834E008AB1CCA3258C2388F1158B37AB6AFAC1 (void);
// 0x000006D0 System.Void RSG.Promise/<>c__DisplayClass36_0::<InvokeProgressHandlers>b__0(RSG.ProgressHandler)
extern void U3CU3Ec__DisplayClass36_0_U3CInvokeProgressHandlersU3Eb__0_mB466D8A8500579A62E0EF36D3766DB1A59E89F41 (void);
// 0x000006D1 System.Void RSG.Promise/<>c__DisplayClass44_0::.ctor()
extern void U3CU3Ec__DisplayClass44_0__ctor_mCB6B49A87238E9E939A3FB884973D5B8085A1805 (void);
// 0x000006D2 System.Void RSG.Promise/<>c__DisplayClass44_0::<Catch>b__0()
extern void U3CU3Ec__DisplayClass44_0_U3CCatchU3Eb__0_m59D9BB020E9BAA19BF3A55E1A66B4BBF0115A4F3 (void);
// 0x000006D3 System.Void RSG.Promise/<>c__DisplayClass44_0::<Catch>b__1(System.Exception)
extern void U3CU3Ec__DisplayClass44_0_U3CCatchU3Eb__1_mC2F86C6B2CC1463D07838F8B6E71CF95A00D6B1C (void);
// 0x000006D4 System.Void RSG.Promise/<>c__DisplayClass44_0::<Catch>b__2(System.Single)
extern void U3CU3Ec__DisplayClass44_0_U3CCatchU3Eb__2_m1C82A7D09BAFC515977C21BBAD3E8EE6E6C8B665 (void);
// 0x000006D5 System.Void RSG.Promise/<>c__DisplayClass51_0`1::.ctor()
// 0x000006D6 System.Void RSG.Promise/<>c__DisplayClass51_0`1::<Then>b__0()
// 0x000006D7 System.Void RSG.Promise/<>c__DisplayClass51_0`1::<Then>b__2(System.Single)
// 0x000006D8 System.Void RSG.Promise/<>c__DisplayClass51_0`1::<Then>b__3(ConvertedT)
// 0x000006D9 System.Void RSG.Promise/<>c__DisplayClass51_0`1::<Then>b__4(System.Exception)
// 0x000006DA System.Void RSG.Promise/<>c__DisplayClass51_0`1::<Then>b__1(System.Exception)
// 0x000006DB System.Void RSG.Promise/<>c__DisplayClass51_0`1::<Then>b__5(ConvertedT)
// 0x000006DC System.Void RSG.Promise/<>c__DisplayClass51_0`1::<Then>b__6(System.Exception)
// 0x000006DD System.Void RSG.Promise/<>c__DisplayClass52_0::.ctor()
extern void U3CU3Ec__DisplayClass52_0__ctor_mB894C314363743106885723C9A014E58D79394DE (void);
// 0x000006DE System.Void RSG.Promise/<>c__DisplayClass52_0::<Then>b__0()
extern void U3CU3Ec__DisplayClass52_0_U3CThenU3Eb__0_mFAB16B8D05F5B6D1B0820FEAECBB318B28D6F95F (void);
// 0x000006DF System.Void RSG.Promise/<>c__DisplayClass52_0::<Then>b__2(System.Single)
extern void U3CU3Ec__DisplayClass52_0_U3CThenU3Eb__2_m68317E06CF2A6CB4E45914984E5E7B1C8E250B3A (void);
// 0x000006E0 System.Void RSG.Promise/<>c__DisplayClass52_0::<Then>b__3()
extern void U3CU3Ec__DisplayClass52_0_U3CThenU3Eb__3_m1DD830DC1CA6AEAB53B32028949E3349E37146EE (void);
// 0x000006E1 System.Void RSG.Promise/<>c__DisplayClass52_0::<Then>b__4(System.Exception)
extern void U3CU3Ec__DisplayClass52_0_U3CThenU3Eb__4_m6F52D5A374717AC944B68D544EF57498A6CF0C12 (void);
// 0x000006E2 System.Void RSG.Promise/<>c__DisplayClass52_0::<Then>b__1(System.Exception)
extern void U3CU3Ec__DisplayClass52_0_U3CThenU3Eb__1_mE79481476F323A2E8C64CF6853F48919B454B238 (void);
// 0x000006E3 System.Void RSG.Promise/<>c__DisplayClass53_0::.ctor()
extern void U3CU3Ec__DisplayClass53_0__ctor_m8AD8D6610D93664501D710EB55F2D75DE59813DC (void);
// 0x000006E4 System.Void RSG.Promise/<>c__DisplayClass53_0::<Then>b__0()
extern void U3CU3Ec__DisplayClass53_0_U3CThenU3Eb__0_m089AF19F35A75DF89A101BC920FA8F3CBF856F03 (void);
// 0x000006E5 System.Void RSG.Promise/<>c__DisplayClass53_0::<Then>b__1(System.Exception)
extern void U3CU3Ec__DisplayClass53_0_U3CThenU3Eb__1_m6BF2C7A1D0A0D3699E5339E92DFF00CA7B60BBE2 (void);
// 0x000006E6 System.Void RSG.Promise/<>c__DisplayClass56_0::.ctor()
extern void U3CU3Ec__DisplayClass56_0__ctor_mA16A09395360FDB88DAEB794DAF2A6BAF4CAC8AE (void);
// 0x000006E7 RSG.IPromise RSG.Promise/<>c__DisplayClass56_0::<ThenAll>b__0()
extern void U3CU3Ec__DisplayClass56_0_U3CThenAllU3Eb__0_m76809680DE80AD5F5596FA06F41BA163AC6D1C30 (void);
// 0x000006E8 System.Void RSG.Promise/<>c__DisplayClass57_0`1::.ctor()
// 0x000006E9 RSG.IPromise`1<System.Collections.Generic.IEnumerable`1<ConvertedT>> RSG.Promise/<>c__DisplayClass57_0`1::<ThenAll>b__0()
// 0x000006EA System.Void RSG.Promise/<>c__DisplayClass59_0::.ctor()
extern void U3CU3Ec__DisplayClass59_0__ctor_mBA66AC86289688F1C468B96C50BE5358F31A082F (void);
// 0x000006EB System.Void RSG.Promise/<>c__DisplayClass59_0::<All>b__0(RSG.IPromise,System.Int32)
extern void U3CU3Ec__DisplayClass59_0_U3CAllU3Eb__0_mA4CF65F285BDA1A6D803D917E9FD265051D6BEB9 (void);
// 0x000006EC System.Void RSG.Promise/<>c__DisplayClass59_0::<All>b__3(System.Exception)
extern void U3CU3Ec__DisplayClass59_0_U3CAllU3Eb__3_m8253EC136EA78D646B69616B15FA235271A992AA (void);
// 0x000006ED System.Void RSG.Promise/<>c__DisplayClass59_1::.ctor()
extern void U3CU3Ec__DisplayClass59_1__ctor_m526DF9DF8BBEBFA28914FB4F54E3A1AF03B414BB (void);
// 0x000006EE System.Void RSG.Promise/<>c__DisplayClass59_1::<All>b__1(System.Single)
extern void U3CU3Ec__DisplayClass59_1_U3CAllU3Eb__1_m593233FA219F817C7E2E48E4B57F1967B608EDAE (void);
// 0x000006EF System.Void RSG.Promise/<>c__DisplayClass59_1::<All>b__2()
extern void U3CU3Ec__DisplayClass59_1_U3CAllU3Eb__2_m5AC61534D5AE0A52E1F555A299AD9A673E2DFD45 (void);
// 0x000006F0 System.Void RSG.Promise/<>c__DisplayClass60_0::.ctor()
extern void U3CU3Ec__DisplayClass60_0__ctor_m99C72D8D0D1E5F3744F901AF2CE1D01B10074CD3 (void);
// 0x000006F1 RSG.IPromise RSG.Promise/<>c__DisplayClass60_0::<ThenSequence>b__0()
extern void U3CU3Ec__DisplayClass60_0_U3CThenSequenceU3Eb__0_m6FC48A06C04839828FBCC417B074F35B1849CBF1 (void);
// 0x000006F2 System.Void RSG.Promise/<>c__DisplayClass62_0::.ctor()
extern void U3CU3Ec__DisplayClass62_0__ctor_m4E600EF746E7384F9FF81A106144CCC5CA295EB6 (void);
// 0x000006F3 RSG.IPromise RSG.Promise/<>c__DisplayClass62_0::<Sequence>b__0(RSG.IPromise,System.Func`1<RSG.IPromise>)
extern void U3CU3Ec__DisplayClass62_0_U3CSequenceU3Eb__0_m3BD1BDD0E7EEF8F00E0C7DE8CD8AA502555164A8 (void);
// 0x000006F4 System.Void RSG.Promise/<>c__DisplayClass62_0::<Sequence>b__1()
extern void U3CU3Ec__DisplayClass62_0_U3CSequenceU3Eb__1_m030831A862679BA67E0AFFF2A793684303FDC197 (void);
// 0x000006F5 System.Void RSG.Promise/<>c__DisplayClass62_1::.ctor()
extern void U3CU3Ec__DisplayClass62_1__ctor_m4E3A09413376A37B8429009826FB5E58FB5918D7 (void);
// 0x000006F6 RSG.IPromise RSG.Promise/<>c__DisplayClass62_1::<Sequence>b__2()
extern void U3CU3Ec__DisplayClass62_1_U3CSequenceU3Eb__2_m38F09C7AB72B5AAAEE2B1F47D8B8E46A8B244D76 (void);
// 0x000006F7 System.Void RSG.Promise/<>c__DisplayClass62_1::<Sequence>b__3(System.Single)
extern void U3CU3Ec__DisplayClass62_1_U3CSequenceU3Eb__3_m45E00529CA4F236640E37AD28227E06C03260F75 (void);
// 0x000006F8 System.Void RSG.Promise/<>c__DisplayClass63_0::.ctor()
extern void U3CU3Ec__DisplayClass63_0__ctor_mA3AE9325599E7C269CF28464DFD338E2FB36D87B (void);
// 0x000006F9 RSG.IPromise RSG.Promise/<>c__DisplayClass63_0::<ThenRace>b__0()
extern void U3CU3Ec__DisplayClass63_0_U3CThenRaceU3Eb__0_m5B8C751B1FAD053AE58DE02DF3FEEA12AE43EC44 (void);
// 0x000006FA System.Void RSG.Promise/<>c__DisplayClass64_0`1::.ctor()
// 0x000006FB RSG.IPromise`1<ConvertedT> RSG.Promise/<>c__DisplayClass64_0`1::<ThenRace>b__0()
// 0x000006FC System.Void RSG.Promise/<>c__DisplayClass66_0::.ctor()
extern void U3CU3Ec__DisplayClass66_0__ctor_m20042739F9915BDA4DCECD38A6FAEC9A32FC9FD4 (void);
// 0x000006FD System.Void RSG.Promise/<>c__DisplayClass66_0::<Race>b__0(RSG.IPromise,System.Int32)
extern void U3CU3Ec__DisplayClass66_0_U3CRaceU3Eb__0_m3C1812F10C134DFFDE20266483FB8CEE43E00D28 (void);
// 0x000006FE System.Void RSG.Promise/<>c__DisplayClass66_0::<Race>b__2(System.Exception)
extern void U3CU3Ec__DisplayClass66_0_U3CRaceU3Eb__2_mD8A7C646663CD58D24E0B9E5AD99A223042BCC7B (void);
// 0x000006FF System.Void RSG.Promise/<>c__DisplayClass66_0::<Race>b__3()
extern void U3CU3Ec__DisplayClass66_0_U3CRaceU3Eb__3_m5925431199B068785646FFD433C27ACF0DA6C7D2 (void);
// 0x00000700 System.Void RSG.Promise/<>c__DisplayClass66_1::.ctor()
extern void U3CU3Ec__DisplayClass66_1__ctor_m864691E480DAEE7AEEAD46F8FB882828DB249E77 (void);
// 0x00000701 System.Void RSG.Promise/<>c__DisplayClass66_1::<Race>b__1(System.Single)
extern void U3CU3Ec__DisplayClass66_1_U3CRaceU3Eb__1_mAA09AC32AAC0FB42ED3E102028E9F62480131D48 (void);
// 0x00000702 System.Void RSG.Promise/<>c__DisplayClass69_0::.ctor()
extern void U3CU3Ec__DisplayClass69_0__ctor_m1076FE9DB847E9C246A4B77B4A9AD68DA316DAFC (void);
// 0x00000703 System.Void RSG.Promise/<>c__DisplayClass69_0::<Finally>b__0()
extern void U3CU3Ec__DisplayClass69_0_U3CFinallyU3Eb__0_m05B68401734C79A8332FD12FBF32C5C1DB2FCF61 (void);
// 0x00000704 System.Void RSG.Promise/<>c__DisplayClass69_0::<Finally>b__1(System.Exception)
extern void U3CU3Ec__DisplayClass69_0_U3CFinallyU3Eb__1_m805874267E687B76A8017DEFD8A11A96DA9FA8AD (void);
// 0x00000705 System.Void RSG.Promise/<>c__DisplayClass70_0::.ctor()
extern void U3CU3Ec__DisplayClass70_0__ctor_mD191E17C3CB17FB74E6636B739DFFE3332CF62FD (void);
// 0x00000706 System.Void RSG.Promise/<>c__DisplayClass70_0::<ContinueWith>b__0()
extern void U3CU3Ec__DisplayClass70_0_U3CContinueWithU3Eb__0_m251D75318366BEDFA9CC05A71A3B108CBEBDE4D6 (void);
// 0x00000707 System.Void RSG.Promise/<>c__DisplayClass70_0::<ContinueWith>b__1(System.Exception)
extern void U3CU3Ec__DisplayClass70_0_U3CContinueWithU3Eb__1_m8EF480C7A95C97BAE71BB581C32C4B1A14B98178 (void);
// 0x00000708 System.Void RSG.Promise/<>c__DisplayClass71_0`1::.ctor()
// 0x00000709 System.Void RSG.Promise/<>c__DisplayClass71_0`1::<ContinueWith>b__0()
// 0x0000070A System.Void RSG.Promise/<>c__DisplayClass71_0`1::<ContinueWith>b__1(System.Exception)
// 0x0000070B RSG.Tuple`2<T1,T2> RSG.Tuple::Create(T1,T2)
// 0x0000070C RSG.Tuple`3<T1,T2,T3> RSG.Tuple::Create(T1,T2,T3)
// 0x0000070D RSG.Tuple`4<T1,T2,T3,T4> RSG.Tuple::Create(T1,T2,T3,T4)
// 0x0000070E System.Void RSG.Tuple::.ctor()
extern void Tuple__ctor_mB90BC7455CD38661E7B174497948846C3717B84A (void);
// 0x0000070F System.Void RSG.Tuple`2::.ctor(T1,T2)
// 0x00000710 T1 RSG.Tuple`2::get_Item1()
// 0x00000711 System.Void RSG.Tuple`2::set_Item1(T1)
// 0x00000712 T2 RSG.Tuple`2::get_Item2()
// 0x00000713 System.Void RSG.Tuple`2::set_Item2(T2)
// 0x00000714 System.Void RSG.Tuple`3::.ctor(T1,T2,T3)
// 0x00000715 T1 RSG.Tuple`3::get_Item1()
// 0x00000716 System.Void RSG.Tuple`3::set_Item1(T1)
// 0x00000717 T2 RSG.Tuple`3::get_Item2()
// 0x00000718 System.Void RSG.Tuple`3::set_Item2(T2)
// 0x00000719 T3 RSG.Tuple`3::get_Item3()
// 0x0000071A System.Void RSG.Tuple`3::set_Item3(T3)
// 0x0000071B System.Void RSG.Tuple`4::.ctor(T1,T2,T3,T4)
// 0x0000071C T1 RSG.Tuple`4::get_Item1()
// 0x0000071D System.Void RSG.Tuple`4::set_Item1(T1)
// 0x0000071E T2 RSG.Tuple`4::get_Item2()
// 0x0000071F System.Void RSG.Tuple`4::set_Item2(T2)
// 0x00000720 T3 RSG.Tuple`4::get_Item3()
// 0x00000721 System.Void RSG.Tuple`4::set_Item3(T3)
// 0x00000722 T4 RSG.Tuple`4::get_Item4()
// 0x00000723 System.Void RSG.Tuple`4::set_Item4(T4)
// 0x00000724 System.Void RSG.Exceptions.PromiseException::.ctor()
extern void PromiseException__ctor_mC1C353F7C09B485B8EFCF3B176412385745A37B2 (void);
// 0x00000725 System.Void RSG.Exceptions.PromiseException::.ctor(System.String)
extern void PromiseException__ctor_m25FEF871F4EADD0813603091A36151F62E072F34 (void);
// 0x00000726 System.Void RSG.Exceptions.PromiseException::.ctor(System.String,System.Exception)
extern void PromiseException__ctor_m378D7284D56C1662B14AFD057C60ACBBABCF8895 (void);
// 0x00000727 System.Void RSG.Exceptions.PromiseStateException::.ctor()
extern void PromiseStateException__ctor_m0841D9ED95CB98A821651EB934050C7F99325539 (void);
// 0x00000728 System.Void RSG.Exceptions.PromiseStateException::.ctor(System.String)
extern void PromiseStateException__ctor_mB10D86B4B0D696B724F6A344C9DA8BA582785E8A (void);
// 0x00000729 System.Void RSG.Exceptions.PromiseStateException::.ctor(System.String,System.Exception)
extern void PromiseStateException__ctor_m09D7E625AC5B60FBAA33F53C52F2C5B9784C3AA9 (void);
// 0x0000072A System.Void RSG.Promises.EnumerableExt::Each(System.Collections.Generic.IEnumerable`1<T>,System.Action`1<T>)
// 0x0000072B System.Void RSG.Promises.EnumerableExt::Each(System.Collections.Generic.IEnumerable`1<T>,System.Action`2<T,System.Int32>)
// 0x0000072C System.Collections.Generic.IEnumerable`1<T> RSG.Promises.EnumerableExt::FromItems(T[])
// 0x0000072D System.Void RSG.Promises.EnumerableExt/<FromItems>d__2`1::.ctor(System.Int32)
// 0x0000072E System.Void RSG.Promises.EnumerableExt/<FromItems>d__2`1::System.IDisposable.Dispose()
// 0x0000072F System.Boolean RSG.Promises.EnumerableExt/<FromItems>d__2`1::MoveNext()
// 0x00000730 T RSG.Promises.EnumerableExt/<FromItems>d__2`1::System.Collections.Generic.IEnumerator<T>.get_Current()
// 0x00000731 System.Void RSG.Promises.EnumerableExt/<FromItems>d__2`1::System.Collections.IEnumerator.Reset()
// 0x00000732 System.Object RSG.Promises.EnumerableExt/<FromItems>d__2`1::System.Collections.IEnumerator.get_Current()
// 0x00000733 System.Collections.Generic.IEnumerator`1<T> RSG.Promises.EnumerableExt/<FromItems>d__2`1::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
// 0x00000734 System.Collections.IEnumerator RSG.Promises.EnumerableExt/<FromItems>d__2`1::System.Collections.IEnumerable.GetEnumerator()
// 0x00000735 System.Collections.IEnumerator Proyecto26.HttpBase::CreateRequestAndRetry(Proyecto26.RequestHelper,System.Action`2<Proyecto26.RequestException,Proyecto26.ResponseHelper>)
extern void HttpBase_CreateRequestAndRetry_m6DC921EB7E83F0FB058D9C9889B15D489092D059 (void);
// 0x00000736 UnityEngine.Networking.UnityWebRequest Proyecto26.HttpBase::CreateRequest(Proyecto26.RequestHelper)
extern void HttpBase_CreateRequest_m395D82BBC11CA3C9D8FA5DCA55BC4E5AFE6AC451 (void);
// 0x00000737 Proyecto26.RequestException Proyecto26.HttpBase::CreateException(Proyecto26.RequestHelper,UnityEngine.Networking.UnityWebRequest)
extern void HttpBase_CreateException_m4F00611C1B083E175A53008F96FCDAEED39E094E (void);
// 0x00000738 System.Void Proyecto26.HttpBase::DebugLog(System.Boolean,System.Object,System.Boolean)
extern void HttpBase_DebugLog_mB403310A43B2C0F73923BE8135DBA43E5FECF8C3 (void);
// 0x00000739 System.Collections.IEnumerator Proyecto26.HttpBase::DefaultUnityWebRequest(Proyecto26.RequestHelper,System.Action`2<Proyecto26.RequestException,Proyecto26.ResponseHelper>)
extern void HttpBase_DefaultUnityWebRequest_mA2CA028D057F027E7D8283E63643F0AF2974B069 (void);
// 0x0000073A System.Collections.IEnumerator Proyecto26.HttpBase::DefaultUnityWebRequest(Proyecto26.RequestHelper,System.Action`3<Proyecto26.RequestException,Proyecto26.ResponseHelper,TResponse>)
// 0x0000073B System.Collections.IEnumerator Proyecto26.HttpBase::DefaultUnityWebRequest(Proyecto26.RequestHelper,System.Action`3<Proyecto26.RequestException,Proyecto26.ResponseHelper,TResponse[]>)
// 0x0000073C System.Void Proyecto26.HttpBase/<CreateRequestAndRetry>d__0::.ctor(System.Int32)
extern void U3CCreateRequestAndRetryU3Ed__0__ctor_mA038A5FC9BE11F7D821C2EB89C903BD81DD6673F (void);
// 0x0000073D System.Void Proyecto26.HttpBase/<CreateRequestAndRetry>d__0::System.IDisposable.Dispose()
extern void U3CCreateRequestAndRetryU3Ed__0_System_IDisposable_Dispose_m0F0654EB7C2275B8DDFC1B070E8B145B2C5FBCAC (void);
// 0x0000073E System.Boolean Proyecto26.HttpBase/<CreateRequestAndRetry>d__0::MoveNext()
extern void U3CCreateRequestAndRetryU3Ed__0_MoveNext_mC05783571C265F2A6348BF44253BDF379AD516DD (void);
// 0x0000073F System.Void Proyecto26.HttpBase/<CreateRequestAndRetry>d__0::<>m__Finally1()
extern void U3CCreateRequestAndRetryU3Ed__0_U3CU3Em__Finally1_m65EAC36497FC4D9964288E99CD0E428ABC806C46 (void);
// 0x00000740 System.Object Proyecto26.HttpBase/<CreateRequestAndRetry>d__0::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CCreateRequestAndRetryU3Ed__0_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m585D7434321501EABFADD21A314DCBB231EC3252 (void);
// 0x00000741 System.Void Proyecto26.HttpBase/<CreateRequestAndRetry>d__0::System.Collections.IEnumerator.Reset()
extern void U3CCreateRequestAndRetryU3Ed__0_System_Collections_IEnumerator_Reset_m09C357E4FCCD4430591B46B1960B5DC7D4BFE376 (void);
// 0x00000742 System.Object Proyecto26.HttpBase/<CreateRequestAndRetry>d__0::System.Collections.IEnumerator.get_Current()
extern void U3CCreateRequestAndRetryU3Ed__0_System_Collections_IEnumerator_get_Current_m4D5CD62D3F82FDDE9C67A9FD2E2894857D7746A6 (void);
// 0x00000743 System.Void Proyecto26.HttpBase/<>c__DisplayClass5_0`1::.ctor()
// 0x00000744 System.Void Proyecto26.HttpBase/<>c__DisplayClass5_0`1::<DefaultUnityWebRequest>b__0(Proyecto26.RequestException,Proyecto26.ResponseHelper)
// 0x00000745 System.Void Proyecto26.HttpBase/<>c__DisplayClass6_0`1::.ctor()
// 0x00000746 System.Void Proyecto26.HttpBase/<>c__DisplayClass6_0`1::<DefaultUnityWebRequest>b__0(Proyecto26.RequestException,Proyecto26.ResponseHelper)
// 0x00000747 T[] Proyecto26.JsonHelper::ArrayFromJson(System.String)
// 0x00000748 T[] Proyecto26.JsonHelper::FromJsonString(System.String)
// 0x00000749 System.String Proyecto26.JsonHelper::ArrayToJsonString(T[])
// 0x0000074A System.String Proyecto26.JsonHelper::ArrayToJsonString(T[],System.Boolean)
// 0x0000074B System.Void Proyecto26.JsonHelper/Wrapper`1::.ctor()
// 0x0000074C System.Boolean Proyecto26.RequestException::get_IsHttpError()
extern void RequestException_get_IsHttpError_mF1D87818843F7F4BFA1BB1B1F1EAD6925497CF30 (void);
// 0x0000074D System.Void Proyecto26.RequestException::set_IsHttpError(System.Boolean)
extern void RequestException_set_IsHttpError_mF311DBD704CCC61C23A270F8BCD1E7E8E5F17469 (void);
// 0x0000074E System.Boolean Proyecto26.RequestException::get_IsNetworkError()
extern void RequestException_get_IsNetworkError_mF6C31F24DCF7B5F649A4CABF455EBB03CB53434C (void);
// 0x0000074F System.Void Proyecto26.RequestException::set_IsNetworkError(System.Boolean)
extern void RequestException_set_IsNetworkError_mB4D9148437DD8A59B2969764E067E35E085ABB15 (void);
// 0x00000750 System.Int64 Proyecto26.RequestException::get_StatusCode()
extern void RequestException_get_StatusCode_m786C170A63F36EF40CF6F4347F5EACC85925E1F1 (void);
// 0x00000751 System.Void Proyecto26.RequestException::set_StatusCode(System.Int64)
extern void RequestException_set_StatusCode_mFD7034224DB7D4C27984CE0DA365D02173432309 (void);
// 0x00000752 System.String Proyecto26.RequestException::get_ServerMessage()
extern void RequestException_get_ServerMessage_m14FEBDED7437643B057376759428572897C03597 (void);
// 0x00000753 System.Void Proyecto26.RequestException::set_ServerMessage(System.String)
extern void RequestException_set_ServerMessage_m3DE596FD331A4F694B415FA3C8886FDC5F861DE9 (void);
// 0x00000754 System.String Proyecto26.RequestException::get_Response()
extern void RequestException_get_Response_m4FE8480DACD2F2347FDE3BC8F0843885E10912E5 (void);
// 0x00000755 System.Void Proyecto26.RequestException::set_Response(System.String)
extern void RequestException_set_Response_mEF4EB83001EB93F345CD9FB6214237F3888EDA25 (void);
// 0x00000756 System.Void Proyecto26.RequestException::.ctor()
extern void RequestException__ctor_m4255A573E554424A1B5C9F6E2D58590C50877C85 (void);
// 0x00000757 System.Void Proyecto26.RequestException::.ctor(System.String)
extern void RequestException__ctor_m2A12EC5D24756FA04C4D080FFD4DB9264B01753B (void);
// 0x00000758 System.Void Proyecto26.RequestException::.ctor(System.String,System.Object[])
extern void RequestException__ctor_m230AC0532C60E770A87DB74A57630F5464269D23 (void);
// 0x00000759 System.Void Proyecto26.RequestException::.ctor(System.String,System.Boolean,System.Boolean,System.Int64,System.String)
extern void RequestException__ctor_mD2293F4AA83F9A3A3EA0E48219894E91DF485FCC (void);
// 0x0000075A System.String Proyecto26.RequestHelper::get_Uri()
extern void RequestHelper_get_Uri_m579870F497E54040498F01E0F03D8E385B75682B (void);
// 0x0000075B System.Void Proyecto26.RequestHelper::set_Uri(System.String)
extern void RequestHelper_set_Uri_mED832E7E6B02422B23573A3BE154D41312A32BE3 (void);
// 0x0000075C System.String Proyecto26.RequestHelper::get_Method()
extern void RequestHelper_get_Method_m3CAF08A8E99D378C7693940ED9BBC576130C5558 (void);
// 0x0000075D System.Void Proyecto26.RequestHelper::set_Method(System.String)
extern void RequestHelper_set_Method_m93E8A5F66E37E3459E8F1689E272C9EA2303EEA2 (void);
// 0x0000075E System.Object Proyecto26.RequestHelper::get_Body()
extern void RequestHelper_get_Body_mE96291B55EA0C31805FDF261E1A6E0E0A04B9B4D (void);
// 0x0000075F System.Void Proyecto26.RequestHelper::set_Body(System.Object)
extern void RequestHelper_set_Body_m6C34BB52EE69E8A7D5B2FC14CE8478A4CCCB223A (void);
// 0x00000760 System.String Proyecto26.RequestHelper::get_BodyString()
extern void RequestHelper_get_BodyString_m583C6B16055049B23992E90B9BE9240D2494C872 (void);
// 0x00000761 System.Void Proyecto26.RequestHelper::set_BodyString(System.String)
extern void RequestHelper_set_BodyString_m264B491596B1E5D59DEBEDFA6D2C07EC37F6AF25 (void);
// 0x00000762 System.Byte[] Proyecto26.RequestHelper::get_BodyRaw()
extern void RequestHelper_get_BodyRaw_m0567B940B2FD769D548A63A3937EDAB8AF8BEB07 (void);
// 0x00000763 System.Void Proyecto26.RequestHelper::set_BodyRaw(System.Byte[])
extern void RequestHelper_set_BodyRaw_m4BB1B19C3109ED8297827657F5855DCAF7EF315E (void);
// 0x00000764 System.Nullable`1<System.Int32> Proyecto26.RequestHelper::get_Timeout()
extern void RequestHelper_get_Timeout_m5EB987EDD3D72C0AFC56600A506DED4393B31D03 (void);
// 0x00000765 System.Void Proyecto26.RequestHelper::set_Timeout(System.Nullable`1<System.Int32>)
extern void RequestHelper_set_Timeout_m8769C12B49481D0B8BC11B3227F802FDF049707F (void);
// 0x00000766 System.String Proyecto26.RequestHelper::get_ContentType()
extern void RequestHelper_get_ContentType_mF1D15920E2C975B8788B61B57B6A0EC713C5EB97 (void);
// 0x00000767 System.Void Proyecto26.RequestHelper::set_ContentType(System.String)
extern void RequestHelper_set_ContentType_m53EBFC2111ABCD757D3B613E943FED0F2A916B1F (void);
// 0x00000768 System.Int32 Proyecto26.RequestHelper::get_Retries()
extern void RequestHelper_get_Retries_mFE6F51F6EE991FEE0982128A8305F013668B7D5B (void);
// 0x00000769 System.Void Proyecto26.RequestHelper::set_Retries(System.Int32)
extern void RequestHelper_set_Retries_m8E91AB08F2D651BE88B57973D472A87CFE403AE1 (void);
// 0x0000076A System.Single Proyecto26.RequestHelper::get_RetrySecondsDelay()
extern void RequestHelper_get_RetrySecondsDelay_mF461806AF11B3F89D745861D3EE818A9548705F5 (void);
// 0x0000076B System.Void Proyecto26.RequestHelper::set_RetrySecondsDelay(System.Single)
extern void RequestHelper_set_RetrySecondsDelay_mF554D9A11BAD76DF31F07A5FA92B67E34FF7D6F8 (void);
// 0x0000076C System.Action`2<Proyecto26.RequestException,System.Int32> Proyecto26.RequestHelper::get_RetryCallback()
extern void RequestHelper_get_RetryCallback_m8487D96D1889FAC85CC99D59CE9AAD81AF975858 (void);
// 0x0000076D System.Void Proyecto26.RequestHelper::set_RetryCallback(System.Action`2<Proyecto26.RequestException,System.Int32>)
extern void RequestHelper_set_RetryCallback_m8B02AFF7AB55609093C48182D79501B34C2F2646 (void);
// 0x0000076E System.Boolean Proyecto26.RequestHelper::get_EnableDebug()
extern void RequestHelper_get_EnableDebug_m031DA45E2494D29141E793FBFE2BEF047DCF7472 (void);
// 0x0000076F System.Void Proyecto26.RequestHelper::set_EnableDebug(System.Boolean)
extern void RequestHelper_set_EnableDebug_m7DEAE27887D92A686E341EB14010DE5A4B3E4DF9 (void);
// 0x00000770 System.Nullable`1<System.Boolean> Proyecto26.RequestHelper::get_UseHttpContinue()
extern void RequestHelper_get_UseHttpContinue_m64713A0BA2260B82D1587517ADF0671DB60B8D66 (void);
// 0x00000771 System.Void Proyecto26.RequestHelper::set_UseHttpContinue(System.Nullable`1<System.Boolean>)
extern void RequestHelper_set_UseHttpContinue_m454427FB941A608ABD370EC1E64F0C5166183E48 (void);
// 0x00000772 System.Nullable`1<System.Int32> Proyecto26.RequestHelper::get_RedirectLimit()
extern void RequestHelper_get_RedirectLimit_m7628CFD271A1A06DBF264C289268CE52DA66DB82 (void);
// 0x00000773 System.Void Proyecto26.RequestHelper::set_RedirectLimit(System.Nullable`1<System.Int32>)
extern void RequestHelper_set_RedirectLimit_m32E15A1AE611D405855B008D58576C8839929DC3 (void);
// 0x00000774 System.Boolean Proyecto26.RequestHelper::get_IgnoreHttpException()
extern void RequestHelper_get_IgnoreHttpException_m0BA6B17AACCF9C74FEFD730806A64B67E4505FEA (void);
// 0x00000775 System.Void Proyecto26.RequestHelper::set_IgnoreHttpException(System.Boolean)
extern void RequestHelper_set_IgnoreHttpException_m6653B079C6C70E478895D1EF4BFAC9FE5D5D2209 (void);
// 0x00000776 UnityEngine.WWWForm Proyecto26.RequestHelper::get_FormData()
extern void RequestHelper_get_FormData_m1CE699C5BC879C7AEFBB1084CF0A1928CD02B931 (void);
// 0x00000777 System.Void Proyecto26.RequestHelper::set_FormData(UnityEngine.WWWForm)
extern void RequestHelper_set_FormData_mD33B2EEF5749F7898DB81BD2ACEB0F7EA9E97EC4 (void);
// 0x00000778 System.Collections.Generic.Dictionary`2<System.String,System.String> Proyecto26.RequestHelper::get_SimpleForm()
extern void RequestHelper_get_SimpleForm_mE158581E8CAC46C434A072324A6290CBE4878C78 (void);
// 0x00000779 System.Void Proyecto26.RequestHelper::set_SimpleForm(System.Collections.Generic.Dictionary`2<System.String,System.String>)
extern void RequestHelper_set_SimpleForm_m885368EFE7D8FF37C0E9CBC36A8B551562042165 (void);
// 0x0000077A System.Collections.Generic.List`1<UnityEngine.Networking.IMultipartFormSection> Proyecto26.RequestHelper::get_FormSections()
extern void RequestHelper_get_FormSections_m4157D4255B827B864F882669136AB30BA93316F0 (void);
// 0x0000077B System.Void Proyecto26.RequestHelper::set_FormSections(System.Collections.Generic.List`1<UnityEngine.Networking.IMultipartFormSection>)
extern void RequestHelper_set_FormSections_m955F901BFE0A3D2FFE42F223BB2005BA2028337E (void);
// 0x0000077C UnityEngine.Networking.CertificateHandler Proyecto26.RequestHelper::get_CertificateHandler()
extern void RequestHelper_get_CertificateHandler_mBF95069255782425F21F491C5A51F3BBCEE956B1 (void);
// 0x0000077D System.Void Proyecto26.RequestHelper::set_CertificateHandler(UnityEngine.Networking.CertificateHandler)
extern void RequestHelper_set_CertificateHandler_m1B5296D19D58F16EF358706D8AC3817F0ED0B1C5 (void);
// 0x0000077E UnityEngine.Networking.UploadHandler Proyecto26.RequestHelper::get_UploadHandler()
extern void RequestHelper_get_UploadHandler_m5C72A7EF49F678C8CB02FA0620C0B41D1CF8AE78 (void);
// 0x0000077F System.Void Proyecto26.RequestHelper::set_UploadHandler(UnityEngine.Networking.UploadHandler)
extern void RequestHelper_set_UploadHandler_mDA3C9BCAE240C2A5185B88FE2FE7BF198B6467ED (void);
// 0x00000780 UnityEngine.Networking.DownloadHandler Proyecto26.RequestHelper::get_DownloadHandler()
extern void RequestHelper_get_DownloadHandler_m3CC95DFC6C81ABD8508947C1808BC8FB7095BDA3 (void);
// 0x00000781 System.Void Proyecto26.RequestHelper::set_DownloadHandler(UnityEngine.Networking.DownloadHandler)
extern void RequestHelper_set_DownloadHandler_mE7FC2C6C38C5418A915D4007DCFF2A9CF5C6DC74 (void);
// 0x00000782 System.Collections.Generic.Dictionary`2<System.String,System.String> Proyecto26.RequestHelper::get_Headers()
extern void RequestHelper_get_Headers_m09842B9228E6B35E7254DFAC41A5D2C98DBF0BE9 (void);
// 0x00000783 System.Void Proyecto26.RequestHelper::set_Headers(System.Collections.Generic.Dictionary`2<System.String,System.String>)
extern void RequestHelper_set_Headers_mE4B41FA1E24D02AD057068E2D0A697C129921A29 (void);
// 0x00000784 System.Collections.Generic.Dictionary`2<System.String,System.String> Proyecto26.RequestHelper::get_Params()
extern void RequestHelper_get_Params_mDB495A9CB92B43BB1BD3526C99880EFBDC40E033 (void);
// 0x00000785 System.Void Proyecto26.RequestHelper::set_Params(System.Collections.Generic.Dictionary`2<System.String,System.String>)
extern void RequestHelper_set_Params_m887AAEE89701BE57B2875DD482A584C58EF7C400 (void);
// 0x00000786 System.Boolean Proyecto26.RequestHelper::get_ParseResponseBody()
extern void RequestHelper_get_ParseResponseBody_m8352D39E0EBACC6FB532FBE05B5D3EC709EA43BD (void);
// 0x00000787 System.Void Proyecto26.RequestHelper::set_ParseResponseBody(System.Boolean)
extern void RequestHelper_set_ParseResponseBody_m1149B5FC0628F6BCDAFA95D21516A9CAE3F4031D (void);
// 0x00000788 UnityEngine.Networking.UnityWebRequest Proyecto26.RequestHelper::get_Request()
extern void RequestHelper_get_Request_mB3DD0BDA5D264948DE92BA5E060545D8D3CCBD63 (void);
// 0x00000789 System.Void Proyecto26.RequestHelper::set_Request(UnityEngine.Networking.UnityWebRequest)
extern void RequestHelper_set_Request_m4C49567052095FC092A24AD821EF7957CCB6F4BF (void);
// 0x0000078A System.Single Proyecto26.RequestHelper::get_UploadProgress()
extern void RequestHelper_get_UploadProgress_m4F7D8BCA66AE1D13F1C59D91EE10E656D48DE91B (void);
// 0x0000078B System.UInt64 Proyecto26.RequestHelper::get_UploadedBytes()
extern void RequestHelper_get_UploadedBytes_m7D3445EE67E794E91D82A28171224EED03460533 (void);
// 0x0000078C System.Single Proyecto26.RequestHelper::get_DownloadProgress()
extern void RequestHelper_get_DownloadProgress_m0ADF466E7C45A9D043506B099C6B3E7A08C5BE33 (void);
// 0x0000078D System.UInt64 Proyecto26.RequestHelper::get_DownloadedBytes()
extern void RequestHelper_get_DownloadedBytes_m5201AEFCAF699F4C054CBA3C63004950C1E32ED7 (void);
// 0x0000078E System.String Proyecto26.RequestHelper::GetHeader(System.String)
extern void RequestHelper_GetHeader_m0EAA6AEED4B885553B4CD97B2C4CBF81A32EF468 (void);
// 0x0000078F System.Boolean Proyecto26.RequestHelper::get_IsAborted()
extern void RequestHelper_get_IsAborted_mED2B0C2E2FA7DA65574ED4141818116D5407825B (void);
// 0x00000790 System.Void Proyecto26.RequestHelper::set_IsAborted(System.Boolean)
extern void RequestHelper_set_IsAborted_mF37D6A6B93B70D7B4AC2B579FDA59345D0D51DDF (void);
// 0x00000791 System.Boolean Proyecto26.RequestHelper::get_DefaultContentType()
extern void RequestHelper_get_DefaultContentType_mF52E8AD322DC2F3A0A0444FA1A60D2C62B42113F (void);
// 0x00000792 System.Void Proyecto26.RequestHelper::set_DefaultContentType(System.Boolean)
extern void RequestHelper_set_DefaultContentType_m0098EE480F8E776673979AFB751C76CD61AB5365 (void);
// 0x00000793 System.Void Proyecto26.RequestHelper::Abort()
extern void RequestHelper_Abort_m7CE069D799170B7D0336E7DB358C78B1B00DA8F3 (void);
// 0x00000794 System.Void Proyecto26.RequestHelper::.ctor()
extern void RequestHelper__ctor_m76D5CE4B07BAD0EFE344E9A8C4D1BFE895922C13 (void);
// 0x00000795 UnityEngine.Networking.UnityWebRequest Proyecto26.ResponseHelper::get_Request()
extern void ResponseHelper_get_Request_mE2DF5B066E83E4216167EEE1A28A79A09A372843 (void);
// 0x00000796 System.Void Proyecto26.ResponseHelper::set_Request(UnityEngine.Networking.UnityWebRequest)
extern void ResponseHelper_set_Request_m9743967B9D789F748E8205E910F339114CB7D522 (void);
// 0x00000797 System.Void Proyecto26.ResponseHelper::.ctor(UnityEngine.Networking.UnityWebRequest)
extern void ResponseHelper__ctor_m0F45F428ADF231F62EC7C6E5621058DA806A1850 (void);
// 0x00000798 System.Int64 Proyecto26.ResponseHelper::get_StatusCode()
extern void ResponseHelper_get_StatusCode_mAC67167172472DC6804430CA8FDEDDBF1FED2D4F (void);
// 0x00000799 System.Byte[] Proyecto26.ResponseHelper::get_Data()
extern void ResponseHelper_get_Data_mDFB63D2691666575C126F59C928580F359646E44 (void);
// 0x0000079A System.String Proyecto26.ResponseHelper::get_Text()
extern void ResponseHelper_get_Text_m4C6CE813DC4FA5B0659BF20EC4B5EE2504C618AF (void);
// 0x0000079B System.String Proyecto26.ResponseHelper::get_Error()
extern void ResponseHelper_get_Error_mAF99FD6F00AF225E4D2EF620266C6A428199F6E7 (void);
// 0x0000079C System.Collections.Generic.Dictionary`2<System.String,System.String> Proyecto26.ResponseHelper::get_Headers()
extern void ResponseHelper_get_Headers_m2990A8F894F71B2503103388796B0717EEF9D9CB (void);
// 0x0000079D System.String Proyecto26.ResponseHelper::GetHeader(System.String)
extern void ResponseHelper_GetHeader_m9E39EF8000022EA04DF66B43FCCB31AA01AB0D52 (void);
// 0x0000079E System.String Proyecto26.ResponseHelper::ToString()
extern void ResponseHelper_ToString_m3DD8554605B5BDE7A6412218369E317C804B0572 (void);
// 0x0000079F Proyecto26.StaticCoroutine/CoroutineHolder Proyecto26.StaticCoroutine::get_Runner()
extern void StaticCoroutine_get_Runner_mC13BCB3C4C6F548812E35E5555D3455AEB2700DF (void);
// 0x000007A0 UnityEngine.Coroutine Proyecto26.StaticCoroutine::StartCoroutine(System.Collections.IEnumerator)
extern void StaticCoroutine_StartCoroutine_m46187D9942A6387EAD6EEE4771C5C54027547C58 (void);
// 0x000007A1 System.Void Proyecto26.StaticCoroutine/CoroutineHolder::.ctor()
extern void CoroutineHolder__ctor_mC1424305AB5B1F2E631B7121BBB184B1B8BBBCF6 (void);
// 0x000007A2 System.Version Proyecto26.RestClient::get_Version()
extern void RestClient_get_Version_m5701043CF8E7F883ACF65A25CADCE6AB4AFC8EB0 (void);
// 0x000007A3 System.Collections.Generic.Dictionary`2<System.String,System.String> Proyecto26.RestClient::get_DefaultRequestParams()
extern void RestClient_get_DefaultRequestParams_mE23F55E6B8003CFEE7FF60C7483232FE7E247A3D (void);
// 0x000007A4 System.Void Proyecto26.RestClient::set_DefaultRequestParams(System.Collections.Generic.Dictionary`2<System.String,System.String>)
extern void RestClient_set_DefaultRequestParams_mA826F5ADE53A384196CC73D37D26E72E5B7CAA9B (void);
// 0x000007A5 System.Void Proyecto26.RestClient::ClearDefaultParams()
extern void RestClient_ClearDefaultParams_mCBFF0C9C7DA720E8F0F9D54D0BDDE3D1C421CA71 (void);
// 0x000007A6 System.Collections.Generic.Dictionary`2<System.String,System.String> Proyecto26.RestClient::get_DefaultRequestHeaders()
extern void RestClient_get_DefaultRequestHeaders_m7CFC695866D2FFFAE6C2C79B38564917EFFE37AA (void);
// 0x000007A7 System.Void Proyecto26.RestClient::set_DefaultRequestHeaders(System.Collections.Generic.Dictionary`2<System.String,System.String>)
extern void RestClient_set_DefaultRequestHeaders_m8ED47C4D90B6F05DB8ED523A5A49C2B0B1B7C2B5 (void);
// 0x000007A8 System.Void Proyecto26.RestClient::ClearDefaultHeaders()
extern void RestClient_ClearDefaultHeaders_mFADDD96FA3D43A3CFC44B9A6A641B666F000C09F (void);
// 0x000007A9 System.Void Proyecto26.RestClient::Request(Proyecto26.RequestHelper,System.Action`2<Proyecto26.RequestException,Proyecto26.ResponseHelper>)
extern void RestClient_Request_m6D0EEC74B2073F8006977A737FEB5A26FF8B2BE4 (void);
// 0x000007AA System.Void Proyecto26.RestClient::Request(Proyecto26.RequestHelper,System.Action`3<Proyecto26.RequestException,Proyecto26.ResponseHelper,T>)
// 0x000007AB System.Void Proyecto26.RestClient::Get(System.String,System.Action`2<Proyecto26.RequestException,Proyecto26.ResponseHelper>)
extern void RestClient_Get_mD3CD57C7B7809173BB0841F05883D4EAB667C4E4 (void);
// 0x000007AC System.Void Proyecto26.RestClient::Get(Proyecto26.RequestHelper,System.Action`2<Proyecto26.RequestException,Proyecto26.ResponseHelper>)
extern void RestClient_Get_m62DDD6AA1F3E2D93DC5AF9378090C97D67EE7D54 (void);
// 0x000007AD System.Void Proyecto26.RestClient::Get(System.String,System.Action`3<Proyecto26.RequestException,Proyecto26.ResponseHelper,T>)
// 0x000007AE System.Void Proyecto26.RestClient::Get(Proyecto26.RequestHelper,System.Action`3<Proyecto26.RequestException,Proyecto26.ResponseHelper,T>)
// 0x000007AF System.Void Proyecto26.RestClient::GetArray(System.String,System.Action`3<Proyecto26.RequestException,Proyecto26.ResponseHelper,T[]>)
// 0x000007B0 System.Void Proyecto26.RestClient::GetArray(Proyecto26.RequestHelper,System.Action`3<Proyecto26.RequestException,Proyecto26.ResponseHelper,T[]>)
// 0x000007B1 System.Void Proyecto26.RestClient::Post(System.String,System.Object,System.Action`2<Proyecto26.RequestException,Proyecto26.ResponseHelper>)
extern void RestClient_Post_m4B07264A4B85D23B2F3013C227AB599B04F18E7A (void);
// 0x000007B2 System.Void Proyecto26.RestClient::Post(System.String,System.String,System.Action`2<Proyecto26.RequestException,Proyecto26.ResponseHelper>)
extern void RestClient_Post_mAD4114F33CCE7E913400D3AA42D2494C1C71D27A (void);
// 0x000007B3 System.Void Proyecto26.RestClient::Post(Proyecto26.RequestHelper,System.Action`2<Proyecto26.RequestException,Proyecto26.ResponseHelper>)
extern void RestClient_Post_mB43880A95D1AD7E76C1878CB288DC4E6B3453651 (void);
// 0x000007B4 System.Void Proyecto26.RestClient::Post(System.String,System.Object,System.Action`3<Proyecto26.RequestException,Proyecto26.ResponseHelper,T>)
// 0x000007B5 System.Void Proyecto26.RestClient::Post(System.String,System.String,System.Action`3<Proyecto26.RequestException,Proyecto26.ResponseHelper,T>)
// 0x000007B6 System.Void Proyecto26.RestClient::Post(Proyecto26.RequestHelper,System.Action`3<Proyecto26.RequestException,Proyecto26.ResponseHelper,T>)
// 0x000007B7 System.Void Proyecto26.RestClient::PostArray(System.String,System.Object,System.Action`3<Proyecto26.RequestException,Proyecto26.ResponseHelper,T[]>)
// 0x000007B8 System.Void Proyecto26.RestClient::PostArray(System.String,System.String,System.Action`3<Proyecto26.RequestException,Proyecto26.ResponseHelper,T[]>)
// 0x000007B9 System.Void Proyecto26.RestClient::PostArray(Proyecto26.RequestHelper,System.Action`3<Proyecto26.RequestException,Proyecto26.ResponseHelper,T[]>)
// 0x000007BA System.Void Proyecto26.RestClient::Put(System.String,System.Object,System.Action`2<Proyecto26.RequestException,Proyecto26.ResponseHelper>)
extern void RestClient_Put_m955E298F1437E5DDB4391BE3BF2A58B02232AA4E (void);
// 0x000007BB System.Void Proyecto26.RestClient::Put(System.String,System.String,System.Action`2<Proyecto26.RequestException,Proyecto26.ResponseHelper>)
extern void RestClient_Put_m8AF7486C0FD795F8D4CCEBE663A56980E9397A02 (void);
// 0x000007BC System.Void Proyecto26.RestClient::Put(Proyecto26.RequestHelper,System.Action`2<Proyecto26.RequestException,Proyecto26.ResponseHelper>)
extern void RestClient_Put_m08D2C2C9C8B6F73A7531BF8BD2AC15C9C5E13680 (void);
// 0x000007BD System.Void Proyecto26.RestClient::Put(System.String,System.Object,System.Action`3<Proyecto26.RequestException,Proyecto26.ResponseHelper,T>)
// 0x000007BE System.Void Proyecto26.RestClient::Put(System.String,System.String,System.Action`3<Proyecto26.RequestException,Proyecto26.ResponseHelper,T>)
// 0x000007BF System.Void Proyecto26.RestClient::Put(Proyecto26.RequestHelper,System.Action`3<Proyecto26.RequestException,Proyecto26.ResponseHelper,T>)
// 0x000007C0 System.Void Proyecto26.RestClient::Delete(System.String,System.Action`2<Proyecto26.RequestException,Proyecto26.ResponseHelper>)
extern void RestClient_Delete_m7701A6040B30FF21656087D1FFBE7BCAFA782E16 (void);
// 0x000007C1 System.Void Proyecto26.RestClient::Delete(Proyecto26.RequestHelper,System.Action`2<Proyecto26.RequestException,Proyecto26.ResponseHelper>)
extern void RestClient_Delete_m1C6AA27C4494A101CED7B1B6BFE9A3B38247FB60 (void);
// 0x000007C2 System.Void Proyecto26.RestClient::Head(System.String,System.Action`2<Proyecto26.RequestException,Proyecto26.ResponseHelper>)
extern void RestClient_Head_m01F7D5AEF77C54D5AE2948C1F579C1FD856AB0A8 (void);
// 0x000007C3 System.Void Proyecto26.RestClient::Head(Proyecto26.RequestHelper,System.Action`2<Proyecto26.RequestException,Proyecto26.ResponseHelper>)
extern void RestClient_Head_mFEFB7B87CFC87642E95B09D1A4AB76F63ABA3E14 (void);
// 0x000007C4 RSG.IPromise`1<Proyecto26.ResponseHelper> Proyecto26.RestClient::Request(Proyecto26.RequestHelper)
extern void RestClient_Request_mA130CF629F30CB634AA89655F61C5BD37F729409 (void);
// 0x000007C5 RSG.IPromise`1<T> Proyecto26.RestClient::Request(Proyecto26.RequestHelper)
// 0x000007C6 RSG.IPromise`1<Proyecto26.ResponseHelper> Proyecto26.RestClient::Get(System.String)
extern void RestClient_Get_m7E5DEB235C456E5FAFCA828BF688ECFD630E86F7 (void);
// 0x000007C7 RSG.IPromise`1<Proyecto26.ResponseHelper> Proyecto26.RestClient::Get(Proyecto26.RequestHelper)
extern void RestClient_Get_mDEA2483857BD9E64941E12CB42063D74EEF50FE6 (void);
// 0x000007C8 RSG.IPromise`1<T> Proyecto26.RestClient::Get(System.String)
// 0x000007C9 System.Object Proyecto26.RestClient::Post(System.String)
// 0x000007CA RSG.IPromise`1<T> Proyecto26.RestClient::Get(Proyecto26.RequestHelper)
// 0x000007CB RSG.IPromise`1<T[]> Proyecto26.RestClient::GetArray(System.String)
// 0x000007CC RSG.IPromise`1<T[]> Proyecto26.RestClient::GetArray(Proyecto26.RequestHelper)
// 0x000007CD RSG.IPromise`1<Proyecto26.ResponseHelper> Proyecto26.RestClient::Post(System.String,System.Object)
extern void RestClient_Post_m4763A3817990302191F3A5B069A64B02419523BC (void);
// 0x000007CE RSG.IPromise`1<Proyecto26.ResponseHelper> Proyecto26.RestClient::Post(System.String,System.String)
extern void RestClient_Post_mD59FE057DB71BF372315AB100BD5E4339FFFFE07 (void);
// 0x000007CF RSG.IPromise`1<Proyecto26.ResponseHelper> Proyecto26.RestClient::Post(Proyecto26.RequestHelper)
extern void RestClient_Post_m27A12B1AAD73F990A53E62B578D01BF4EA933509 (void);
// 0x000007D0 RSG.IPromise`1<T> Proyecto26.RestClient::Post(System.String,System.Object)
// 0x000007D1 RSG.IPromise`1<T> Proyecto26.RestClient::Post(System.String,System.String)
// 0x000007D2 RSG.IPromise`1<T> Proyecto26.RestClient::Post(Proyecto26.RequestHelper)
// 0x000007D3 RSG.IPromise`1<T[]> Proyecto26.RestClient::PostArray(System.String,System.Object)
// 0x000007D4 RSG.IPromise`1<T[]> Proyecto26.RestClient::PostArray(System.String,System.String)
// 0x000007D5 RSG.IPromise`1<T[]> Proyecto26.RestClient::PostArray(Proyecto26.RequestHelper)
// 0x000007D6 RSG.IPromise`1<Proyecto26.ResponseHelper> Proyecto26.RestClient::Put(System.String,System.Object)
extern void RestClient_Put_mB84F593B9E21B27814D3EBAA2920B67D1699FD26 (void);
// 0x000007D7 RSG.IPromise`1<Proyecto26.ResponseHelper> Proyecto26.RestClient::Put(System.String,System.String)
extern void RestClient_Put_m3E06B35E9CFAE4AF81DCF12A8F30A178E1CC5ABA (void);
// 0x000007D8 RSG.IPromise`1<Proyecto26.ResponseHelper> Proyecto26.RestClient::Put(Proyecto26.RequestHelper)
extern void RestClient_Put_m5E7F007136807CD127CA4FD4D6A986B3979391F4 (void);
// 0x000007D9 RSG.IPromise`1<T> Proyecto26.RestClient::Put(System.String,System.Object)
// 0x000007DA RSG.IPromise`1<T> Proyecto26.RestClient::Put(System.String,System.String)
// 0x000007DB RSG.IPromise`1<T> Proyecto26.RestClient::Put(Proyecto26.RequestHelper)
// 0x000007DC RSG.IPromise`1<Proyecto26.ResponseHelper> Proyecto26.RestClient::Delete(System.String)
extern void RestClient_Delete_m7CACD86724C07540430FDA0E5D759F6E57BBF75C (void);
// 0x000007DD RSG.IPromise`1<Proyecto26.ResponseHelper> Proyecto26.RestClient::Delete(Proyecto26.RequestHelper)
extern void RestClient_Delete_m33B84121232197C62DB7D3BAFB4E5F39CEF4C474 (void);
// 0x000007DE RSG.IPromise`1<Proyecto26.ResponseHelper> Proyecto26.RestClient::Head(System.String)
extern void RestClient_Head_m6CA11D53CD3CD3724C345EA2DCEBCF665E31D101 (void);
// 0x000007DF RSG.IPromise`1<Proyecto26.ResponseHelper> Proyecto26.RestClient::Head(Proyecto26.RequestHelper)
extern void RestClient_Head_mFE1399A7241D6D868C0AB0B65C774812167D9D43 (void);
// 0x000007E0 System.Void Proyecto26.RestClient::Promisify(RSG.Promise`1<T>,Proyecto26.RequestException,T)
// 0x000007E1 System.Void Proyecto26.RestClient::Promisify(RSG.Promise`1<T>,Proyecto26.RequestException,Proyecto26.ResponseHelper,T)
// 0x000007E2 System.String Proyecto26.Common.Common::GetFormSectionsContentType(System.Byte[]&,Proyecto26.RequestHelper)
extern void Common_GetFormSectionsContentType_m19E34A4A4613B617EB59187AE573639031CD6B93 (void);
// 0x000007E3 System.Void Proyecto26.Common.Common::ConfigureWebRequestWithOptions(UnityEngine.Networking.UnityWebRequest,System.Byte[],System.String,Proyecto26.RequestHelper)
extern void Common_ConfigureWebRequestWithOptions_mF5668E71E221566BFD09C871A2B0667206901F78 (void);
// 0x000007E4 System.Collections.IEnumerator Proyecto26.Common.Common::SendWebRequestWithOptions(UnityEngine.Networking.UnityWebRequest,Proyecto26.RequestHelper)
extern void Common_SendWebRequestWithOptions_mA6DA8F8469D1F337E3CB043E57F2191D8D95EE1B (void);
// 0x000007E5 System.Void Proyecto26.Common.Common/<SendWebRequestWithOptions>d__4::.ctor(System.Int32)
extern void U3CSendWebRequestWithOptionsU3Ed__4__ctor_m47FB1CC913CF34CE244286B13D6600F97F2C7006 (void);
// 0x000007E6 System.Void Proyecto26.Common.Common/<SendWebRequestWithOptions>d__4::System.IDisposable.Dispose()
extern void U3CSendWebRequestWithOptionsU3Ed__4_System_IDisposable_Dispose_m5F26553216BF51BE2953AFD9118938F28D511A34 (void);
// 0x000007E7 System.Boolean Proyecto26.Common.Common/<SendWebRequestWithOptions>d__4::MoveNext()
extern void U3CSendWebRequestWithOptionsU3Ed__4_MoveNext_mB5E4CFE2AAE6A9BCE0D76C72C378282AFCB56C68 (void);
// 0x000007E8 System.Object Proyecto26.Common.Common/<SendWebRequestWithOptions>d__4::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CSendWebRequestWithOptionsU3Ed__4_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mF49002192678DCE4CE3D8D78E1E922C730FED67C (void);
// 0x000007E9 System.Void Proyecto26.Common.Common/<SendWebRequestWithOptions>d__4::System.Collections.IEnumerator.Reset()
extern void U3CSendWebRequestWithOptionsU3Ed__4_System_Collections_IEnumerator_Reset_m82497AA2724FEA6918E2C0900364DA889677B45E (void);
// 0x000007EA System.Object Proyecto26.Common.Common/<SendWebRequestWithOptions>d__4::System.Collections.IEnumerator.get_Current()
extern void U3CSendWebRequestWithOptionsU3Ed__4_System_Collections_IEnumerator_get_Current_mD15322133CA601A9884E3FCBDE8BD76FFFB1558D (void);
// 0x000007EB Proyecto26.ResponseHelper Proyecto26.Common.Extensions::CreateWebResponse(UnityEngine.Networking.UnityWebRequest)
extern void Extensions_CreateWebResponse_m85A8BFBB1916DE1247943658A0758B372807A7F1 (void);
// 0x000007EC System.Boolean Proyecto26.Common.Extensions::IsValidRequest(UnityEngine.Networking.UnityWebRequest,Proyecto26.RequestHelper)
extern void Extensions_IsValidRequest_m95295E8977C94FB722EE97C5047C3975DE5EA65A (void);
// 0x000007ED System.String Proyecto26.Common.Extensions::EscapeURL(System.String)
extern void Extensions_EscapeURL_mAAD51E7CBC83CB8CE79715D8A6086CB153919CF7 (void);
// 0x000007EE System.String Proyecto26.Common.Extensions::BuildUrl(System.String,System.Collections.Generic.Dictionary`2<System.String,System.String>)
extern void Extensions_BuildUrl_mE2D5E2B005BCD677EED820AE36D5F28F01E57CA0 (void);
// 0x000007EF System.Void Proyecto26.Common.Extensions/<>c__DisplayClass3_0::.ctor()
extern void U3CU3Ec__DisplayClass3_0__ctor_mDEE443CFFB4553720586AE36F8109D07F7E08B87 (void);
// 0x000007F0 System.Boolean Proyecto26.Common.Extensions/<>c__DisplayClass3_0::<BuildUrl>b__0(System.Collections.Generic.KeyValuePair`2<System.String,System.String>)
extern void U3CU3Ec__DisplayClass3_0_U3CBuildUrlU3Eb__0_mDFB7EBC5B80A07C2899A8B10EE18130CC2AE4157 (void);
// 0x000007F1 System.Void Proyecto26.Common.Extensions/<>c::.cctor()
extern void U3CU3Ec__cctor_m51C966458A7B400E929B365351E9FA572508174F (void);
// 0x000007F2 System.Void Proyecto26.Common.Extensions/<>c::.ctor()
extern void U3CU3Ec__ctor_m3AF7EC6F7B8017905D17E0078A8E8219711B52B7 (void);
// 0x000007F3 System.String Proyecto26.Common.Extensions/<>c::<BuildUrl>b__3_1(System.Collections.Generic.KeyValuePair`2<System.String,System.String>)
extern void U3CU3Ec_U3CBuildUrlU3Eb__3_1_m3F16B42B55E86F33694E97ABD64E3ADB7B6EDD03 (void);
// 0x000007F4 System.String Models.Photo::ToString()
extern void Photo_ToString_mC7549402494D8D3A4192988B7A75C3F43BB81C7D (void);
// 0x000007F5 System.Void Models.Photo::.ctor()
extern void Photo__ctor_mFB7B355DA04B8883B308CC03347EF777898F6846 (void);
// 0x000007F6 System.String Models.Post::ToString()
extern void Post_ToString_m4AD8D8586CF507C7C435254089148F3AD32A855A (void);
// 0x000007F7 System.Void Models.Post::.ctor()
extern void Post__ctor_mAD9D421E1716FEB8D8B297565CE8F10FFB42CC79 (void);
// 0x000007F8 System.String Models.Todo::ToString()
extern void Todo_ToString_mAD1AD0938A238807A2D0EA2888893C11CBE105CF (void);
// 0x000007F9 System.Void Models.Todo::.ctor()
extern void Todo__ctor_m3DBCDF59C636313C811C93C83ED5ABC292A19AA4 (void);
// 0x000007FA System.String Models.User::ToString()
extern void User_ToString_mD57533E879F6F6B77BD1A38883A0DAED53D2FAF2 (void);
// 0x000007FB System.Void Models.User::.ctor()
extern void User__ctor_m179E4458F95BA48DD9D0F7E1E4DF8AC26DB05C0F (void);
// 0x000007FC System.Void UnityEngine.PostProcessing.GetSetAttribute::.ctor(System.String)
extern void GetSetAttribute__ctor_mAF21003A3420F0175EE49705C2FE162E4768DCF6 (void);
// 0x000007FD System.Void UnityEngine.PostProcessing.MinAttribute::.ctor(System.Single)
extern void MinAttribute__ctor_m0704195F6CAA3E54C6070CFAF5CD4B4F6F59E2C0 (void);
// 0x000007FE System.Void UnityEngine.PostProcessing.TrackballAttribute::.ctor(System.String)
extern void TrackballAttribute__ctor_mF93A32F3F5A18D303CFF8FF6D3B3B4639F950228 (void);
// 0x000007FF System.Void UnityEngine.PostProcessing.TrackballGroupAttribute::.ctor()
extern void TrackballGroupAttribute__ctor_mF3ADA8F2E6E316B188CC9394CC3405388C8C51C8 (void);
// 0x00000800 UnityEngine.PostProcessing.AmbientOcclusionComponent/OcclusionSource UnityEngine.PostProcessing.AmbientOcclusionComponent::get_occlusionSource()
extern void AmbientOcclusionComponent_get_occlusionSource_m539EF11FF7DEAF5A2AF8DA4D62BCB7E2306B3087 (void);
// 0x00000801 System.Boolean UnityEngine.PostProcessing.AmbientOcclusionComponent::get_ambientOnlySupported()
extern void AmbientOcclusionComponent_get_ambientOnlySupported_m51F05150E7C98A0D83EE87D72077E1173AA6D76B (void);
// 0x00000802 System.Boolean UnityEngine.PostProcessing.AmbientOcclusionComponent::get_active()
extern void AmbientOcclusionComponent_get_active_mBB714D81DE71CDD3225A84D76E5BA62F93AFFB77 (void);
// 0x00000803 UnityEngine.DepthTextureMode UnityEngine.PostProcessing.AmbientOcclusionComponent::GetCameraFlags()
extern void AmbientOcclusionComponent_GetCameraFlags_m7A30280E0AD156B00DBC2EAB46DDAECFD9293B4C (void);
// 0x00000804 System.String UnityEngine.PostProcessing.AmbientOcclusionComponent::GetName()
extern void AmbientOcclusionComponent_GetName_m1B868250FEF54023814DAB66FF4AC65485C65C50 (void);
// 0x00000805 UnityEngine.Rendering.CameraEvent UnityEngine.PostProcessing.AmbientOcclusionComponent::GetCameraEvent()
extern void AmbientOcclusionComponent_GetCameraEvent_m160D92A8753CAA68437A1D84D2062D9CAB866925 (void);
// 0x00000806 System.Void UnityEngine.PostProcessing.AmbientOcclusionComponent::PopulateCommandBuffer(UnityEngine.Rendering.CommandBuffer)
extern void AmbientOcclusionComponent_PopulateCommandBuffer_m5FCF85B0AB81F6CF61C733CA8DB23F9A7B5C4684 (void);
// 0x00000807 System.Void UnityEngine.PostProcessing.AmbientOcclusionComponent::.ctor()
extern void AmbientOcclusionComponent__ctor_mF0CCE56C8B25A75C4E0E59927C53142871DEC786 (void);
// 0x00000808 System.Void UnityEngine.PostProcessing.AmbientOcclusionComponent/Uniforms::.cctor()
extern void Uniforms__cctor_m38063CB99FC6BA1488ACFE086DDCEB8D9DC80138 (void);
// 0x00000809 System.Boolean UnityEngine.PostProcessing.BloomComponent::get_active()
extern void BloomComponent_get_active_mA00BB534E2DA1DA4B64C1E61A457DB63D837F8A2 (void);
// 0x0000080A System.Void UnityEngine.PostProcessing.BloomComponent::Prepare(UnityEngine.RenderTexture,UnityEngine.Material,UnityEngine.Texture)
extern void BloomComponent_Prepare_m97F648F4A3A407FB316504546B6DB8B5A4451328 (void);
// 0x0000080B System.Void UnityEngine.PostProcessing.BloomComponent::.ctor()
extern void BloomComponent__ctor_m55F038989C9314A1E26B4982F0F6BBDED033B736 (void);
// 0x0000080C System.Void UnityEngine.PostProcessing.BloomComponent/Uniforms::.cctor()
extern void Uniforms__cctor_m215A49821B9DBA3B81F09CEE54AF47791B5C8BAC (void);
// 0x0000080D System.Boolean UnityEngine.PostProcessing.BuiltinDebugViewsComponent::get_active()
extern void BuiltinDebugViewsComponent_get_active_mA2483382ECAAA5FC057939A60CF19F68498A573F (void);
// 0x0000080E UnityEngine.DepthTextureMode UnityEngine.PostProcessing.BuiltinDebugViewsComponent::GetCameraFlags()
extern void BuiltinDebugViewsComponent_GetCameraFlags_m8B43D922F78B0ACB29DB8D5830E526021E5C2D79 (void);
// 0x0000080F UnityEngine.Rendering.CameraEvent UnityEngine.PostProcessing.BuiltinDebugViewsComponent::GetCameraEvent()
extern void BuiltinDebugViewsComponent_GetCameraEvent_mEDCAE8A6EA1E9AA91B0829F5149489F5BC705E41 (void);
// 0x00000810 System.String UnityEngine.PostProcessing.BuiltinDebugViewsComponent::GetName()
extern void BuiltinDebugViewsComponent_GetName_m8DBB4AB637C5D0E59FC84F349FBB7EB8788D902F (void);
// 0x00000811 System.Void UnityEngine.PostProcessing.BuiltinDebugViewsComponent::PopulateCommandBuffer(UnityEngine.Rendering.CommandBuffer)
extern void BuiltinDebugViewsComponent_PopulateCommandBuffer_m22521778E45D4D9A6259572BD36E1480C6E1E611 (void);
// 0x00000812 System.Void UnityEngine.PostProcessing.BuiltinDebugViewsComponent::DepthPass(UnityEngine.Rendering.CommandBuffer)
extern void BuiltinDebugViewsComponent_DepthPass_mAB73CB7F0F7A29010CE83932B544E60484BD7694 (void);
// 0x00000813 System.Void UnityEngine.PostProcessing.BuiltinDebugViewsComponent::DepthNormalsPass(UnityEngine.Rendering.CommandBuffer)
extern void BuiltinDebugViewsComponent_DepthNormalsPass_mED9874A8E542D02042C9147EEC6A8A60E3AB69FB (void);
// 0x00000814 System.Void UnityEngine.PostProcessing.BuiltinDebugViewsComponent::MotionVectorsPass(UnityEngine.Rendering.CommandBuffer)
extern void BuiltinDebugViewsComponent_MotionVectorsPass_m6FF4B48F071D636346B28D31A072E0575245EC0F (void);
// 0x00000815 System.Void UnityEngine.PostProcessing.BuiltinDebugViewsComponent::PrepareArrows()
extern void BuiltinDebugViewsComponent_PrepareArrows_m282F4F365B391877ABF777654D7C039765C384AD (void);
// 0x00000816 System.Void UnityEngine.PostProcessing.BuiltinDebugViewsComponent::OnDisable()
extern void BuiltinDebugViewsComponent_OnDisable_m0DEEA40C98B18032BC08E77D104CABD2393C3F7A (void);
// 0x00000817 System.Void UnityEngine.PostProcessing.BuiltinDebugViewsComponent::.ctor()
extern void BuiltinDebugViewsComponent__ctor_m9F0C4752290D80BC9BE4816EF3E0547A38690F64 (void);
// 0x00000818 System.Void UnityEngine.PostProcessing.BuiltinDebugViewsComponent/Uniforms::.cctor()
extern void Uniforms__cctor_m9BBD20D226C673614BA222D689A0F7231D06E54D (void);
// 0x00000819 UnityEngine.Mesh UnityEngine.PostProcessing.BuiltinDebugViewsComponent/ArrowArray::get_mesh()
extern void ArrowArray_get_mesh_m159E0A6065671A721BF4F38F67796DFBF6A39A87 (void);
// 0x0000081A System.Void UnityEngine.PostProcessing.BuiltinDebugViewsComponent/ArrowArray::set_mesh(UnityEngine.Mesh)
extern void ArrowArray_set_mesh_mC3E00DD3EC2EBB9ABC145AB25262CE34FB47EA53 (void);
// 0x0000081B System.Int32 UnityEngine.PostProcessing.BuiltinDebugViewsComponent/ArrowArray::get_columnCount()
extern void ArrowArray_get_columnCount_mB2CE9D04ED4F52E6EB3270467EB6E9E7EC99DDF7 (void);
// 0x0000081C System.Void UnityEngine.PostProcessing.BuiltinDebugViewsComponent/ArrowArray::set_columnCount(System.Int32)
extern void ArrowArray_set_columnCount_m93B0297CF16BB02FCBB8FD401A27A5C9C05771E3 (void);
// 0x0000081D System.Int32 UnityEngine.PostProcessing.BuiltinDebugViewsComponent/ArrowArray::get_rowCount()
extern void ArrowArray_get_rowCount_m880F3F390ECA0958C345F986ED41AF4118B77FBD (void);
// 0x0000081E System.Void UnityEngine.PostProcessing.BuiltinDebugViewsComponent/ArrowArray::set_rowCount(System.Int32)
extern void ArrowArray_set_rowCount_m2B86B6790CEA32DE1C0B8A740764506E085C009C (void);
// 0x0000081F System.Void UnityEngine.PostProcessing.BuiltinDebugViewsComponent/ArrowArray::BuildMesh(System.Int32,System.Int32)
extern void ArrowArray_BuildMesh_mD099690ED8AAB42CBFF3AEB8A2BF30C02789D31A (void);
// 0x00000820 System.Void UnityEngine.PostProcessing.BuiltinDebugViewsComponent/ArrowArray::Release()
extern void ArrowArray_Release_m9093FA5C7964D9CFD460953693E4493F9DEC8412 (void);
// 0x00000821 System.Void UnityEngine.PostProcessing.BuiltinDebugViewsComponent/ArrowArray::.ctor()
extern void ArrowArray__ctor_mD7C99DB45967E3591400E99E778D38AB6DD1AA00 (void);
// 0x00000822 System.Boolean UnityEngine.PostProcessing.ChromaticAberrationComponent::get_active()
extern void ChromaticAberrationComponent_get_active_mE1E4B9B6240668C2B348386058E424E513815684 (void);
// 0x00000823 System.Void UnityEngine.PostProcessing.ChromaticAberrationComponent::OnDisable()
extern void ChromaticAberrationComponent_OnDisable_mD56F80D490E0BFE49CC996DB57F90E4F90555D93 (void);
// 0x00000824 System.Void UnityEngine.PostProcessing.ChromaticAberrationComponent::Prepare(UnityEngine.Material)
extern void ChromaticAberrationComponent_Prepare_mFEE25577FD9C583283667A05ED351A23BD963512 (void);
// 0x00000825 System.Void UnityEngine.PostProcessing.ChromaticAberrationComponent::.ctor()
extern void ChromaticAberrationComponent__ctor_m83C6BF5444D4B1E1EB50D0F87DF969F0972B55CA (void);
// 0x00000826 System.Void UnityEngine.PostProcessing.ChromaticAberrationComponent/Uniforms::.cctor()
extern void Uniforms__cctor_mECD80BABCE4BF5D9439FCFFC0DA6292F8AD02F1A (void);
// 0x00000827 System.Boolean UnityEngine.PostProcessing.ColorGradingComponent::get_active()
extern void ColorGradingComponent_get_active_m06FF224203204A0C9C1E9D85083787AAB549C3C6 (void);
// 0x00000828 System.Single UnityEngine.PostProcessing.ColorGradingComponent::StandardIlluminantY(System.Single)
extern void ColorGradingComponent_StandardIlluminantY_m581464CCCCD31BCE758529AB2E743634BCDD2CF7 (void);
// 0x00000829 UnityEngine.Vector3 UnityEngine.PostProcessing.ColorGradingComponent::CIExyToLMS(System.Single,System.Single)
extern void ColorGradingComponent_CIExyToLMS_m2BA32E63A2E455084AE2B039067ECD9A5810F898 (void);
// 0x0000082A UnityEngine.Vector3 UnityEngine.PostProcessing.ColorGradingComponent::CalculateColorBalance(System.Single,System.Single)
extern void ColorGradingComponent_CalculateColorBalance_m7A0D32191B6A190B555F36BE084D5C70F36AA1B3 (void);
// 0x0000082B UnityEngine.Color UnityEngine.PostProcessing.ColorGradingComponent::NormalizeColor(UnityEngine.Color)
extern void ColorGradingComponent_NormalizeColor_m5150AA8F3014FFB130253EC44DDEFE52A6562AC6 (void);
// 0x0000082C UnityEngine.Vector3 UnityEngine.PostProcessing.ColorGradingComponent::ClampVector(UnityEngine.Vector3,System.Single,System.Single)
extern void ColorGradingComponent_ClampVector_m267C996BD2AC79C64AEFF9B0D385E4BAB01BCB54 (void);
// 0x0000082D UnityEngine.Vector3 UnityEngine.PostProcessing.ColorGradingComponent::GetLiftValue(UnityEngine.Color)
extern void ColorGradingComponent_GetLiftValue_m1F32FEB2B18FB34B9DD00B4B9A69288324A3D368 (void);
// 0x0000082E UnityEngine.Vector3 UnityEngine.PostProcessing.ColorGradingComponent::GetGammaValue(UnityEngine.Color)
extern void ColorGradingComponent_GetGammaValue_mCDE9CB91EC493EFC3F9A2B4F56634D6AB11C2C31 (void);
// 0x0000082F UnityEngine.Vector3 UnityEngine.PostProcessing.ColorGradingComponent::GetGainValue(UnityEngine.Color)
extern void ColorGradingComponent_GetGainValue_mC6F4AE2D1ED3731FD7490FFC9F58E24A32E0F209 (void);
// 0x00000830 System.Void UnityEngine.PostProcessing.ColorGradingComponent::CalculateLiftGammaGain(UnityEngine.Color,UnityEngine.Color,UnityEngine.Color,UnityEngine.Vector3&,UnityEngine.Vector3&,UnityEngine.Vector3&)
extern void ColorGradingComponent_CalculateLiftGammaGain_m0ADD9FBA6B77C265FEECD43D65B7824CD7AD4B5C (void);
// 0x00000831 UnityEngine.Vector3 UnityEngine.PostProcessing.ColorGradingComponent::GetSlopeValue(UnityEngine.Color)
extern void ColorGradingComponent_GetSlopeValue_m6A182C21CD54B6820A437F232688BEF746B89468 (void);
// 0x00000832 UnityEngine.Vector3 UnityEngine.PostProcessing.ColorGradingComponent::GetPowerValue(UnityEngine.Color)
extern void ColorGradingComponent_GetPowerValue_m4C3D95344E89A38105B7E1B1A20673B4DDC4CD69 (void);
// 0x00000833 UnityEngine.Vector3 UnityEngine.PostProcessing.ColorGradingComponent::GetOffsetValue(UnityEngine.Color)
extern void ColorGradingComponent_GetOffsetValue_m45983DC54587B5F797E190B5DBCEC7DAA9745C7E (void);
// 0x00000834 System.Void UnityEngine.PostProcessing.ColorGradingComponent::CalculateSlopePowerOffset(UnityEngine.Color,UnityEngine.Color,UnityEngine.Color,UnityEngine.Vector3&,UnityEngine.Vector3&,UnityEngine.Vector3&)
extern void ColorGradingComponent_CalculateSlopePowerOffset_m7FB780E9E8246D5CD53FC32A903DB2FD53365B59 (void);
// 0x00000835 UnityEngine.TextureFormat UnityEngine.PostProcessing.ColorGradingComponent::GetCurveFormat()
extern void ColorGradingComponent_GetCurveFormat_m4424EEFD351F7D82677A27E032372B949100EEAF (void);
// 0x00000836 UnityEngine.Texture2D UnityEngine.PostProcessing.ColorGradingComponent::GetCurveTexture()
extern void ColorGradingComponent_GetCurveTexture_m99F8CF5E3F40CE250556AD73806938A414EB6179 (void);
// 0x00000837 System.Boolean UnityEngine.PostProcessing.ColorGradingComponent::IsLogLutValid(UnityEngine.RenderTexture)
extern void ColorGradingComponent_IsLogLutValid_m056EB20B397402DD34083FFC90483ABA8DCEE9C1 (void);
// 0x00000838 UnityEngine.RenderTextureFormat UnityEngine.PostProcessing.ColorGradingComponent::GetLutFormat()
extern void ColorGradingComponent_GetLutFormat_m832EFFCC1E7B679943326016B997010DB018EE13 (void);
// 0x00000839 System.Void UnityEngine.PostProcessing.ColorGradingComponent::GenerateLut()
extern void ColorGradingComponent_GenerateLut_mA250C15593E782D404EF571D06F115C0D9B8BDD2 (void);
// 0x0000083A System.Void UnityEngine.PostProcessing.ColorGradingComponent::Prepare(UnityEngine.Material)
extern void ColorGradingComponent_Prepare_m57655813E453F4CBD43034CBA52B0A00FE34FE77 (void);
// 0x0000083B System.Void UnityEngine.PostProcessing.ColorGradingComponent::OnGUI()
extern void ColorGradingComponent_OnGUI_m0FB84319DD49AD0796B54E8D8DC9664C5BDCAF11 (void);
// 0x0000083C System.Void UnityEngine.PostProcessing.ColorGradingComponent::OnDisable()
extern void ColorGradingComponent_OnDisable_m5B1D1BB75DDA375E92E412CA5CD447A6C3FDBBBE (void);
// 0x0000083D System.Void UnityEngine.PostProcessing.ColorGradingComponent::.ctor()
extern void ColorGradingComponent__ctor_m7D5E294E388470CA7833B9A0F28B7FBF3423B0AC (void);
// 0x0000083E System.Void UnityEngine.PostProcessing.ColorGradingComponent/Uniforms::.cctor()
extern void Uniforms__cctor_mE12551BB740B4F63B39B02596ADBD4F2CC6CC678 (void);
// 0x0000083F System.Boolean UnityEngine.PostProcessing.DepthOfFieldComponent::get_active()
extern void DepthOfFieldComponent_get_active_m993080D9AD09D645A61D4E0E8D748ABD6D224F71 (void);
// 0x00000840 UnityEngine.DepthTextureMode UnityEngine.PostProcessing.DepthOfFieldComponent::GetCameraFlags()
extern void DepthOfFieldComponent_GetCameraFlags_m73FE5E723AF6B14352BF3BC688D2CA6FFEAF4AD1 (void);
// 0x00000841 System.Single UnityEngine.PostProcessing.DepthOfFieldComponent::CalculateFocalLength()
extern void DepthOfFieldComponent_CalculateFocalLength_m4EFED65E4182D4569FE7C6D765F7D6C0B037342D (void);
// 0x00000842 System.Single UnityEngine.PostProcessing.DepthOfFieldComponent::CalculateMaxCoCRadius(System.Int32)
extern void DepthOfFieldComponent_CalculateMaxCoCRadius_m6ABFEDD08B6D26CEDCE048F457E41F7BD8430426 (void);
// 0x00000843 System.Boolean UnityEngine.PostProcessing.DepthOfFieldComponent::CheckHistory(System.Int32,System.Int32)
extern void DepthOfFieldComponent_CheckHistory_mD1A7CAA19DB8156E0D31DCAEC232BCEEDBF769BE (void);
// 0x00000844 UnityEngine.RenderTextureFormat UnityEngine.PostProcessing.DepthOfFieldComponent::SelectFormat(UnityEngine.RenderTextureFormat,UnityEngine.RenderTextureFormat)
extern void DepthOfFieldComponent_SelectFormat_mA1CF45D90B17B592B09F2D0FB108D3C58EF65068 (void);
// 0x00000845 System.Void UnityEngine.PostProcessing.DepthOfFieldComponent::Prepare(UnityEngine.RenderTexture,UnityEngine.Material,System.Boolean,UnityEngine.Vector2,System.Single)
extern void DepthOfFieldComponent_Prepare_m0DBA4F66A1A6DBA7C5E3353F75CE5DA101E5339F (void);
// 0x00000846 System.Void UnityEngine.PostProcessing.DepthOfFieldComponent::OnDisable()
extern void DepthOfFieldComponent_OnDisable_mAECFFE4FD3BD34B9B8CE20E64E1E8FB5F4A322E3 (void);
// 0x00000847 System.Void UnityEngine.PostProcessing.DepthOfFieldComponent::.ctor()
extern void DepthOfFieldComponent__ctor_mD6E5A73309A102E57430FD026794AA47699EA8DD (void);
// 0x00000848 System.Void UnityEngine.PostProcessing.DepthOfFieldComponent/Uniforms::.cctor()
extern void Uniforms__cctor_m87771E6C48E0FBFF2BA6EDCB6CB158700214A7D0 (void);
// 0x00000849 System.Boolean UnityEngine.PostProcessing.DitheringComponent::get_active()
extern void DitheringComponent_get_active_m816D22E56F17DECCE0E81FD52DE2AC8F7B1D75CA (void);
// 0x0000084A System.Void UnityEngine.PostProcessing.DitheringComponent::OnDisable()
extern void DitheringComponent_OnDisable_m173236CDDAF599D5FA51C7F4F8637C5310961512 (void);
// 0x0000084B System.Void UnityEngine.PostProcessing.DitheringComponent::LoadNoiseTextures()
extern void DitheringComponent_LoadNoiseTextures_m0029809F2A7586A885CD81FA44C89C97437C1EDA (void);
// 0x0000084C System.Void UnityEngine.PostProcessing.DitheringComponent::Prepare(UnityEngine.Material)
extern void DitheringComponent_Prepare_mA7C5E092DB8593AEE727910EABB776304C8C57FD (void);
// 0x0000084D System.Void UnityEngine.PostProcessing.DitheringComponent::.ctor()
extern void DitheringComponent__ctor_mD28F2C16C6F0DDBBD496FB1352C7E5AC8AFCBA84 (void);
// 0x0000084E System.Void UnityEngine.PostProcessing.DitheringComponent/Uniforms::.cctor()
extern void Uniforms__cctor_mF9C28E0D67C284374071F66F847932B8ADEFE184 (void);
// 0x0000084F System.Boolean UnityEngine.PostProcessing.EyeAdaptationComponent::get_active()
extern void EyeAdaptationComponent_get_active_mFCE81CACA6990FBE1CCF8AE09AB85145AFA41206 (void);
// 0x00000850 System.Void UnityEngine.PostProcessing.EyeAdaptationComponent::ResetHistory()
extern void EyeAdaptationComponent_ResetHistory_mAB55785616A0B69AC5078CC3A6C43A30806FB105 (void);
// 0x00000851 System.Void UnityEngine.PostProcessing.EyeAdaptationComponent::OnEnable()
extern void EyeAdaptationComponent_OnEnable_mE2253875624E2CB2552BB5F860B8D229D088AD00 (void);
// 0x00000852 System.Void UnityEngine.PostProcessing.EyeAdaptationComponent::OnDisable()
extern void EyeAdaptationComponent_OnDisable_mF2E5DA2DF3E03944A82090763C54BA71C36A5BD8 (void);
// 0x00000853 UnityEngine.Vector4 UnityEngine.PostProcessing.EyeAdaptationComponent::GetHistogramScaleOffsetRes()
extern void EyeAdaptationComponent_GetHistogramScaleOffsetRes_m3C6B2F85CFFE17B2297D3D9C21BAE4757DAD7282 (void);
// 0x00000854 UnityEngine.Texture UnityEngine.PostProcessing.EyeAdaptationComponent::Prepare(UnityEngine.RenderTexture,UnityEngine.Material)
extern void EyeAdaptationComponent_Prepare_mAE6FADB01F55216B9D4567C267F7D8B3B21845C9 (void);
// 0x00000855 System.Void UnityEngine.PostProcessing.EyeAdaptationComponent::OnGUI()
extern void EyeAdaptationComponent_OnGUI_mC4F29D66DB0AE8041F3D0D47AC73C3AD4E53A155 (void);
// 0x00000856 System.Void UnityEngine.PostProcessing.EyeAdaptationComponent::.ctor()
extern void EyeAdaptationComponent__ctor_m6A76F04CBFE8625723A4AAF099DA883F9197DD51 (void);
// 0x00000857 System.Void UnityEngine.PostProcessing.EyeAdaptationComponent/Uniforms::.cctor()
extern void Uniforms__cctor_mCB841BE0F9AF1A2E32E94B3B7E5424BC37B0DA0F (void);
// 0x00000858 System.Boolean UnityEngine.PostProcessing.FogComponent::get_active()
extern void FogComponent_get_active_m26A7ECF24D484516C89DCC8FDF7213A58065FBE2 (void);
// 0x00000859 System.String UnityEngine.PostProcessing.FogComponent::GetName()
extern void FogComponent_GetName_mF73166C2D760FD5316166C7BF34AA8D19A406CA5 (void);
// 0x0000085A UnityEngine.DepthTextureMode UnityEngine.PostProcessing.FogComponent::GetCameraFlags()
extern void FogComponent_GetCameraFlags_m2E3A15A63E1CC2D539647107618B8DF54938E039 (void);
// 0x0000085B UnityEngine.Rendering.CameraEvent UnityEngine.PostProcessing.FogComponent::GetCameraEvent()
extern void FogComponent_GetCameraEvent_m2A950B3F52828FD84199F9764CB9D06A6CA5F69D (void);
// 0x0000085C System.Void UnityEngine.PostProcessing.FogComponent::PopulateCommandBuffer(UnityEngine.Rendering.CommandBuffer)
extern void FogComponent_PopulateCommandBuffer_m5FCD2F4D55805CDCDFB87790E90658695144ECA9 (void);
// 0x0000085D System.Void UnityEngine.PostProcessing.FogComponent::.ctor()
extern void FogComponent__ctor_m6A272084C05BFB3E36EEF74CDD2CD0CF16C1F423 (void);
// 0x0000085E System.Void UnityEngine.PostProcessing.FogComponent/Uniforms::.cctor()
extern void Uniforms__cctor_mFF3DEA013808C7E86DCEA731A1040FB75B3718AD (void);
// 0x0000085F System.Boolean UnityEngine.PostProcessing.FxaaComponent::get_active()
extern void FxaaComponent_get_active_m780EA1897EEF779271A578CE97F4826C066C5DC3 (void);
// 0x00000860 System.Void UnityEngine.PostProcessing.FxaaComponent::Render(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern void FxaaComponent_Render_m5FCD55022139AA2AD08D6DC1A47549D76CD7A6FB (void);
// 0x00000861 System.Void UnityEngine.PostProcessing.FxaaComponent::.ctor()
extern void FxaaComponent__ctor_m9505105E1AF33D0CEC66259AA03835E2D5F513B3 (void);
// 0x00000862 System.Void UnityEngine.PostProcessing.FxaaComponent/Uniforms::.cctor()
extern void Uniforms__cctor_m9BBFBBF482F4984B56BA50885F227F07ED63ADCC (void);
// 0x00000863 System.Boolean UnityEngine.PostProcessing.GrainComponent::get_active()
extern void GrainComponent_get_active_mC8B4EA5150B86CADA8134F1F0B3A7AD7F413FB08 (void);
// 0x00000864 System.Void UnityEngine.PostProcessing.GrainComponent::OnDisable()
extern void GrainComponent_OnDisable_m57ADB3E0620BF47720A3B780992574F4B571FE6E (void);
// 0x00000865 System.Void UnityEngine.PostProcessing.GrainComponent::Prepare(UnityEngine.Material)
extern void GrainComponent_Prepare_m6E00891FF46600D26B740F07D98575D02A9A0B68 (void);
// 0x00000866 System.Void UnityEngine.PostProcessing.GrainComponent::.ctor()
extern void GrainComponent__ctor_mA1AC4A955638A99C429FB77C77141DD743542EFF (void);
// 0x00000867 System.Void UnityEngine.PostProcessing.GrainComponent/Uniforms::.cctor()
extern void Uniforms__cctor_mC7ABD16006C393A6C02C2E638E251B28E97CBC22 (void);
// 0x00000868 UnityEngine.PostProcessing.MotionBlurComponent/ReconstructionFilter UnityEngine.PostProcessing.MotionBlurComponent::get_reconstructionFilter()
extern void MotionBlurComponent_get_reconstructionFilter_mC7F9F8BAD03D07FA3404C9315F40CBB3509F199A (void);
// 0x00000869 UnityEngine.PostProcessing.MotionBlurComponent/FrameBlendingFilter UnityEngine.PostProcessing.MotionBlurComponent::get_frameBlendingFilter()
extern void MotionBlurComponent_get_frameBlendingFilter_mB0E43D1A8D75BE1609822D4579DCB1CDD7661189 (void);
// 0x0000086A System.Boolean UnityEngine.PostProcessing.MotionBlurComponent::get_active()
extern void MotionBlurComponent_get_active_mC0C49847057158AD983D269D79F8718B3860B2D2 (void);
// 0x0000086B System.String UnityEngine.PostProcessing.MotionBlurComponent::GetName()
extern void MotionBlurComponent_GetName_mBD539B9E35CC023B87FDE1E5F2BF1F25D922BC3B (void);
// 0x0000086C System.Void UnityEngine.PostProcessing.MotionBlurComponent::ResetHistory()
extern void MotionBlurComponent_ResetHistory_mFEC7FA4E6E68B0D8A57FD23E8B2308187012D5E3 (void);
// 0x0000086D UnityEngine.DepthTextureMode UnityEngine.PostProcessing.MotionBlurComponent::GetCameraFlags()
extern void MotionBlurComponent_GetCameraFlags_m257C15E2D8054287D819E0F5D6AEC3E08EA1178B (void);
// 0x0000086E UnityEngine.Rendering.CameraEvent UnityEngine.PostProcessing.MotionBlurComponent::GetCameraEvent()
extern void MotionBlurComponent_GetCameraEvent_m22E76B13B7FD0EF024546E0BABB0D7F09BA47197 (void);
// 0x0000086F System.Void UnityEngine.PostProcessing.MotionBlurComponent::OnEnable()
extern void MotionBlurComponent_OnEnable_m6282F206DEBD1540BD1D513DFDB0BAEB81AD33B6 (void);
// 0x00000870 System.Void UnityEngine.PostProcessing.MotionBlurComponent::PopulateCommandBuffer(UnityEngine.Rendering.CommandBuffer)
extern void MotionBlurComponent_PopulateCommandBuffer_m011F5A2E9E9C49AAE17CF6B65BB4B5EA5FED6DCD (void);
// 0x00000871 System.Void UnityEngine.PostProcessing.MotionBlurComponent::OnDisable()
extern void MotionBlurComponent_OnDisable_mC7F8B944F6A0169608AE671987149713A02AB1A1 (void);
// 0x00000872 System.Void UnityEngine.PostProcessing.MotionBlurComponent::.ctor()
extern void MotionBlurComponent__ctor_mA39D0CE3C49A4C6CB7E2392BD3D22B7A311A74CD (void);
// 0x00000873 System.Void UnityEngine.PostProcessing.MotionBlurComponent/Uniforms::.cctor()
extern void Uniforms__cctor_m3EE474D5E17F24D24155C55C631FB6D8FFFA6DDF (void);
// 0x00000874 System.Void UnityEngine.PostProcessing.MotionBlurComponent/ReconstructionFilter::.ctor()
extern void ReconstructionFilter__ctor_m3079740618840383340D431A8059BCF18A18EE7A (void);
// 0x00000875 System.Void UnityEngine.PostProcessing.MotionBlurComponent/ReconstructionFilter::CheckTextureFormatSupport()
extern void ReconstructionFilter_CheckTextureFormatSupport_m1825CA06223703C26B85AF769A244A466FAB9D99 (void);
// 0x00000876 System.Boolean UnityEngine.PostProcessing.MotionBlurComponent/ReconstructionFilter::IsSupported()
extern void ReconstructionFilter_IsSupported_mB6E6FCF099F4EF11910C0F20CF7F9912B747F595 (void);
// 0x00000877 System.Void UnityEngine.PostProcessing.MotionBlurComponent/ReconstructionFilter::ProcessImage(UnityEngine.PostProcessing.PostProcessingContext,UnityEngine.Rendering.CommandBuffer,UnityEngine.PostProcessing.MotionBlurModel/Settings&,UnityEngine.Rendering.RenderTargetIdentifier,UnityEngine.Rendering.RenderTargetIdentifier,UnityEngine.Material)
extern void ReconstructionFilter_ProcessImage_mDB83D5078ED2A115849DC99D52A425BA2DF8EAA5 (void);
// 0x00000878 System.Void UnityEngine.PostProcessing.MotionBlurComponent/FrameBlendingFilter::.ctor()
extern void FrameBlendingFilter__ctor_m3364D401CAFA1CBF2CF6640FAA055BA2F668A5FF (void);
// 0x00000879 System.Void UnityEngine.PostProcessing.MotionBlurComponent/FrameBlendingFilter::Dispose()
extern void FrameBlendingFilter_Dispose_mF4FE203027B3F40753941C07564936DEC7339D7D (void);
// 0x0000087A System.Void UnityEngine.PostProcessing.MotionBlurComponent/FrameBlendingFilter::PushFrame(UnityEngine.Rendering.CommandBuffer,UnityEngine.Rendering.RenderTargetIdentifier,System.Int32,System.Int32,UnityEngine.Material)
extern void FrameBlendingFilter_PushFrame_m58435E2ADBE0ED73A3667FC7587594D00ED7BD8F (void);
// 0x0000087B System.Void UnityEngine.PostProcessing.MotionBlurComponent/FrameBlendingFilter::BlendFrames(UnityEngine.Rendering.CommandBuffer,System.Single,UnityEngine.Rendering.RenderTargetIdentifier,UnityEngine.Rendering.RenderTargetIdentifier,UnityEngine.Material)
extern void FrameBlendingFilter_BlendFrames_m04F1245F52C29DE197C961C1B92ED33A23DA752E (void);
// 0x0000087C System.Boolean UnityEngine.PostProcessing.MotionBlurComponent/FrameBlendingFilter::CheckSupportCompression()
extern void FrameBlendingFilter_CheckSupportCompression_m8B4682F8535091B8CFE59E98D0637F801FDC8958 (void);
// 0x0000087D UnityEngine.RenderTextureFormat UnityEngine.PostProcessing.MotionBlurComponent/FrameBlendingFilter::GetPreferredRenderTextureFormat()
extern void FrameBlendingFilter_GetPreferredRenderTextureFormat_mC6E3869191968D568B48B17F4D6D20ED50364B88 (void);
// 0x0000087E UnityEngine.PostProcessing.MotionBlurComponent/FrameBlendingFilter/Frame UnityEngine.PostProcessing.MotionBlurComponent/FrameBlendingFilter::GetFrameRelative(System.Int32)
extern void FrameBlendingFilter_GetFrameRelative_m41534F912EDEF6872D5976D4152C62AA07CAA4D4 (void);
// 0x0000087F System.Single UnityEngine.PostProcessing.MotionBlurComponent/FrameBlendingFilter/Frame::CalculateWeight(System.Single,System.Single)
extern void Frame_CalculateWeight_mB09982C3C7BCBBB4235ADE144EF1BB1AB31DE077 (void);
// 0x00000880 System.Void UnityEngine.PostProcessing.MotionBlurComponent/FrameBlendingFilter/Frame::Release()
extern void Frame_Release_mC179B110B1D26F0A5B7EEED6AE18661F88695A74 (void);
// 0x00000881 System.Void UnityEngine.PostProcessing.MotionBlurComponent/FrameBlendingFilter/Frame::MakeRecord(UnityEngine.Rendering.CommandBuffer,UnityEngine.Rendering.RenderTargetIdentifier,System.Int32,System.Int32,UnityEngine.Material)
extern void Frame_MakeRecord_m76C2721EA40FE50536656F9CDCEB46FA9ED9FEAE (void);
// 0x00000882 System.Void UnityEngine.PostProcessing.MotionBlurComponent/FrameBlendingFilter/Frame::MakeRecordRaw(UnityEngine.Rendering.CommandBuffer,UnityEngine.Rendering.RenderTargetIdentifier,System.Int32,System.Int32,UnityEngine.RenderTextureFormat)
extern void Frame_MakeRecordRaw_m99E90CFFDF04BA32505153D2A005A5AF23ADD9AB (void);
// 0x00000883 UnityEngine.DepthTextureMode UnityEngine.PostProcessing.ScreenSpaceReflectionComponent::GetCameraFlags()
extern void ScreenSpaceReflectionComponent_GetCameraFlags_m0711FC9BF9AB4ABE9B35C45B57BDD6E76E25E56C (void);
// 0x00000884 System.Boolean UnityEngine.PostProcessing.ScreenSpaceReflectionComponent::get_active()
extern void ScreenSpaceReflectionComponent_get_active_m331F0E77DD6652C0A1854D56C80C1573427B266D (void);
// 0x00000885 System.Void UnityEngine.PostProcessing.ScreenSpaceReflectionComponent::OnEnable()
extern void ScreenSpaceReflectionComponent_OnEnable_mC1B38421A706ADCAB25BD942ACB6E270B6FDEF85 (void);
// 0x00000886 System.String UnityEngine.PostProcessing.ScreenSpaceReflectionComponent::GetName()
extern void ScreenSpaceReflectionComponent_GetName_m0DD750812DE714C1BCEFD128FE33C39E1BF1719A (void);
// 0x00000887 UnityEngine.Rendering.CameraEvent UnityEngine.PostProcessing.ScreenSpaceReflectionComponent::GetCameraEvent()
extern void ScreenSpaceReflectionComponent_GetCameraEvent_m72344E18F41C83F2553E1E29F968032FF97E7A75 (void);
// 0x00000888 System.Void UnityEngine.PostProcessing.ScreenSpaceReflectionComponent::PopulateCommandBuffer(UnityEngine.Rendering.CommandBuffer)
extern void ScreenSpaceReflectionComponent_PopulateCommandBuffer_mB47CEFFC0F11542B1374735D7F38F45C1B2062F5 (void);
// 0x00000889 System.Void UnityEngine.PostProcessing.ScreenSpaceReflectionComponent::.ctor()
extern void ScreenSpaceReflectionComponent__ctor_m251EC3222765FB6E4AF6C7D8DC95CED53731ADE4 (void);
// 0x0000088A System.Void UnityEngine.PostProcessing.ScreenSpaceReflectionComponent/Uniforms::.cctor()
extern void Uniforms__cctor_m0392FF620FE602B3A304DAC3C489D30D16F81323 (void);
// 0x0000088B System.Boolean UnityEngine.PostProcessing.TaaComponent::get_active()
extern void TaaComponent_get_active_m683D5A91BF3C0413C1AF45B2567E809FB132C944 (void);
// 0x0000088C UnityEngine.DepthTextureMode UnityEngine.PostProcessing.TaaComponent::GetCameraFlags()
extern void TaaComponent_GetCameraFlags_mED6480C0D60FBEA16CDF524DF0224E3BC5E60586 (void);
// 0x0000088D UnityEngine.Vector2 UnityEngine.PostProcessing.TaaComponent::get_jitterVector()
extern void TaaComponent_get_jitterVector_mC23A20437241F427CD0A37942FAC57B0180FD9CE (void);
// 0x0000088E System.Void UnityEngine.PostProcessing.TaaComponent::set_jitterVector(UnityEngine.Vector2)
extern void TaaComponent_set_jitterVector_m208276722CBEFEF46309B75A354A4A2CD04895A4 (void);
// 0x0000088F System.Void UnityEngine.PostProcessing.TaaComponent::ResetHistory()
extern void TaaComponent_ResetHistory_m714EB9AB2A2C116E2FCA79BD4C61ED44CCAD9B55 (void);
// 0x00000890 System.Void UnityEngine.PostProcessing.TaaComponent::SetProjectionMatrix(System.Func`2<UnityEngine.Vector2,UnityEngine.Matrix4x4>)
extern void TaaComponent_SetProjectionMatrix_m9207B34FA45A961CAC1E340869772E7720931A24 (void);
// 0x00000891 System.Void UnityEngine.PostProcessing.TaaComponent::Render(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern void TaaComponent_Render_mC6E5E9DBE5AADC291AE9733EE2547DB4CF6B1DF4 (void);
// 0x00000892 System.Single UnityEngine.PostProcessing.TaaComponent::GetHaltonValue(System.Int32,System.Int32)
extern void TaaComponent_GetHaltonValue_mCC40F798F5C62666E81D47AA38AACB10D046077A (void);
// 0x00000893 UnityEngine.Vector2 UnityEngine.PostProcessing.TaaComponent::GenerateRandomOffset()
extern void TaaComponent_GenerateRandomOffset_m6FC76A774D49521886A7E6E21C5D336CDCFA1683 (void);
// 0x00000894 UnityEngine.Matrix4x4 UnityEngine.PostProcessing.TaaComponent::GetPerspectiveProjectionMatrix(UnityEngine.Vector2)
extern void TaaComponent_GetPerspectiveProjectionMatrix_m97C63BC6280C46E84020F0BD84137153AC40C68C (void);
// 0x00000895 UnityEngine.Matrix4x4 UnityEngine.PostProcessing.TaaComponent::GetOrthographicProjectionMatrix(UnityEngine.Vector2)
extern void TaaComponent_GetOrthographicProjectionMatrix_m0323DE161A5183F1043F4BD85AB88B794186E770 (void);
// 0x00000896 System.Void UnityEngine.PostProcessing.TaaComponent::OnDisable()
extern void TaaComponent_OnDisable_mBD36E96001415D8D154C85B44AE5D8AC9168C098 (void);
// 0x00000897 System.Void UnityEngine.PostProcessing.TaaComponent::.ctor()
extern void TaaComponent__ctor_mF0777147EE098DA4C0289923CECAB38FBB8EF8AF (void);
// 0x00000898 System.Void UnityEngine.PostProcessing.TaaComponent/Uniforms::.cctor()
extern void Uniforms__cctor_m5F2E7221A556E2585D1E296269A93552CA10ABBD (void);
// 0x00000899 System.Boolean UnityEngine.PostProcessing.UserLutComponent::get_active()
extern void UserLutComponent_get_active_m708F304C0BD287F6657D19FA5C33D8EE83559061 (void);
// 0x0000089A System.Void UnityEngine.PostProcessing.UserLutComponent::Prepare(UnityEngine.Material)
extern void UserLutComponent_Prepare_mA899F06F7766DA8F8F6D2E8244AA664333E0857A (void);
// 0x0000089B System.Void UnityEngine.PostProcessing.UserLutComponent::OnGUI()
extern void UserLutComponent_OnGUI_mAF78EF9EE48CD319F5891AE865E03BE218FEF2F3 (void);
// 0x0000089C System.Void UnityEngine.PostProcessing.UserLutComponent::.ctor()
extern void UserLutComponent__ctor_mE5E7694AAB287B8B19131A0EDCB668650B098F85 (void);
// 0x0000089D System.Void UnityEngine.PostProcessing.UserLutComponent/Uniforms::.cctor()
extern void Uniforms__cctor_mE574B2AF3734357100C3A22818DE21E309D427F0 (void);
// 0x0000089E System.Boolean UnityEngine.PostProcessing.VignetteComponent::get_active()
extern void VignetteComponent_get_active_m898C5FC9A65D8EB081DE5AA9AEC80BC3B67A0DB8 (void);
// 0x0000089F System.Void UnityEngine.PostProcessing.VignetteComponent::Prepare(UnityEngine.Material)
extern void VignetteComponent_Prepare_mE406B9EFC58A96AAD7EF40A2CC8FEF86970CC814 (void);
// 0x000008A0 System.Void UnityEngine.PostProcessing.VignetteComponent::.ctor()
extern void VignetteComponent__ctor_mFF5B974DD88A3B8670E34CD6CE4F255BF6CC9331 (void);
// 0x000008A1 System.Void UnityEngine.PostProcessing.VignetteComponent/Uniforms::.cctor()
extern void Uniforms__cctor_m240E30AFE4A52ECDF8B50B89C787F6AEE08BF923 (void);
// 0x000008A2 UnityEngine.PostProcessing.AmbientOcclusionModel/Settings UnityEngine.PostProcessing.AmbientOcclusionModel::get_settings()
extern void AmbientOcclusionModel_get_settings_mD3A1F236E5E0BB83A4E17683BE3832B2F466FD96 (void);
// 0x000008A3 System.Void UnityEngine.PostProcessing.AmbientOcclusionModel::set_settings(UnityEngine.PostProcessing.AmbientOcclusionModel/Settings)
extern void AmbientOcclusionModel_set_settings_mCFAC76BB653A14E77889310220A26F12B65A52A9 (void);
// 0x000008A4 System.Void UnityEngine.PostProcessing.AmbientOcclusionModel::Reset()
extern void AmbientOcclusionModel_Reset_mD5669B61B25C9FBEE3EF377DD0AD772FFC863D11 (void);
// 0x000008A5 System.Void UnityEngine.PostProcessing.AmbientOcclusionModel::.ctor()
extern void AmbientOcclusionModel__ctor_m536DD8852E6BD73319678C4DEBEC547C43D27FC5 (void);
// 0x000008A6 UnityEngine.PostProcessing.AmbientOcclusionModel/Settings UnityEngine.PostProcessing.AmbientOcclusionModel/Settings::get_defaultSettings()
extern void Settings_get_defaultSettings_mE0F1D8A6811845D21B569AC011D5135920F9191A (void);
// 0x000008A7 UnityEngine.PostProcessing.AntialiasingModel/Settings UnityEngine.PostProcessing.AntialiasingModel::get_settings()
extern void AntialiasingModel_get_settings_m6369C917355D6197F56EFB2C918D620475E45829 (void);
// 0x000008A8 System.Void UnityEngine.PostProcessing.AntialiasingModel::set_settings(UnityEngine.PostProcessing.AntialiasingModel/Settings)
extern void AntialiasingModel_set_settings_m8C819CDB77BF9ADF26A5436380BE95F700699CC3 (void);
// 0x000008A9 System.Void UnityEngine.PostProcessing.AntialiasingModel::Reset()
extern void AntialiasingModel_Reset_m141981BD07C00463EF34B84CCD6096B52796A1D9 (void);
// 0x000008AA System.Void UnityEngine.PostProcessing.AntialiasingModel::.ctor()
extern void AntialiasingModel__ctor_mC32E11D67807F75D4D96335C7053700AA2078234 (void);
// 0x000008AB System.Void UnityEngine.PostProcessing.AntialiasingModel/FxaaQualitySettings::.cctor()
extern void FxaaQualitySettings__cctor_mAAC1546B619A2307C84E1A1F861A52BA5BFEA4C9 (void);
// 0x000008AC System.Void UnityEngine.PostProcessing.AntialiasingModel/FxaaConsoleSettings::.cctor()
extern void FxaaConsoleSettings__cctor_m36CCF558A8C4D525C3C52A5D2F553FA7793C389F (void);
// 0x000008AD UnityEngine.PostProcessing.AntialiasingModel/FxaaSettings UnityEngine.PostProcessing.AntialiasingModel/FxaaSettings::get_defaultSettings()
extern void FxaaSettings_get_defaultSettings_mCC499F9BC608EB8943C846C838A5DA4811B74852 (void);
// 0x000008AE UnityEngine.PostProcessing.AntialiasingModel/TaaSettings UnityEngine.PostProcessing.AntialiasingModel/TaaSettings::get_defaultSettings()
extern void TaaSettings_get_defaultSettings_m69113DF076C4FD27988E02EF1FD1C87EA1AB60D6 (void);
// 0x000008AF UnityEngine.PostProcessing.AntialiasingModel/Settings UnityEngine.PostProcessing.AntialiasingModel/Settings::get_defaultSettings()
extern void Settings_get_defaultSettings_m187BEA1D08AA74B7F2AAC6AC0000F6B19DADE248 (void);
// 0x000008B0 UnityEngine.PostProcessing.BloomModel/Settings UnityEngine.PostProcessing.BloomModel::get_settings()
extern void BloomModel_get_settings_m759A052A18CD2922A57BC5AB1DC8AED62F7DE57E (void);
// 0x000008B1 System.Void UnityEngine.PostProcessing.BloomModel::set_settings(UnityEngine.PostProcessing.BloomModel/Settings)
extern void BloomModel_set_settings_mC161C80C080FC5623E101E2DD374417DB615798B (void);
// 0x000008B2 System.Void UnityEngine.PostProcessing.BloomModel::Reset()
extern void BloomModel_Reset_m2D58211CB6F21456D058093E30B3D5D44D01A85E (void);
// 0x000008B3 System.Void UnityEngine.PostProcessing.BloomModel::.ctor()
extern void BloomModel__ctor_mF166C6353784112A973A26EB88542B3143225E01 (void);
// 0x000008B4 System.Void UnityEngine.PostProcessing.BloomModel/BloomSettings::set_thresholdLinear(System.Single)
extern void BloomSettings_set_thresholdLinear_m5D681343998DD1370FF502B96D0CD7990A8843BA (void);
// 0x000008B5 System.Single UnityEngine.PostProcessing.BloomModel/BloomSettings::get_thresholdLinear()
extern void BloomSettings_get_thresholdLinear_mAF02ADBEE0AA8D764116FFCA6E02317C44191C55 (void);
// 0x000008B6 UnityEngine.PostProcessing.BloomModel/BloomSettings UnityEngine.PostProcessing.BloomModel/BloomSettings::get_defaultSettings()
extern void BloomSettings_get_defaultSettings_m85CFAD9DD7D5157D8F4BB6DD441C2B28A8D55004 (void);
// 0x000008B7 UnityEngine.PostProcessing.BloomModel/LensDirtSettings UnityEngine.PostProcessing.BloomModel/LensDirtSettings::get_defaultSettings()
extern void LensDirtSettings_get_defaultSettings_m3417152CB106AD18C874F023EB672FBECD9EF475 (void);
// 0x000008B8 UnityEngine.PostProcessing.BloomModel/Settings UnityEngine.PostProcessing.BloomModel/Settings::get_defaultSettings()
extern void Settings_get_defaultSettings_mF7B09DA4EC2CFA21A7CE5519F7D147A3154F2DC3 (void);
// 0x000008B9 UnityEngine.PostProcessing.BuiltinDebugViewsModel/Settings UnityEngine.PostProcessing.BuiltinDebugViewsModel::get_settings()
extern void BuiltinDebugViewsModel_get_settings_mC783DAF1FDA65C85D33EA53AB8478328C20FAA76 (void);
// 0x000008BA System.Void UnityEngine.PostProcessing.BuiltinDebugViewsModel::set_settings(UnityEngine.PostProcessing.BuiltinDebugViewsModel/Settings)
extern void BuiltinDebugViewsModel_set_settings_mE4D84909F9F20E90E3F993544C271A965E508551 (void);
// 0x000008BB System.Boolean UnityEngine.PostProcessing.BuiltinDebugViewsModel::get_willInterrupt()
extern void BuiltinDebugViewsModel_get_willInterrupt_mC3263DF49F2D505A38B3CE0911FEFBF208BCF8B3 (void);
// 0x000008BC System.Void UnityEngine.PostProcessing.BuiltinDebugViewsModel::Reset()
extern void BuiltinDebugViewsModel_Reset_m8CCE4D760D74800EDF3F5D4F91F435502ABE5875 (void);
// 0x000008BD System.Boolean UnityEngine.PostProcessing.BuiltinDebugViewsModel::IsModeActive(UnityEngine.PostProcessing.BuiltinDebugViewsModel/Mode)
extern void BuiltinDebugViewsModel_IsModeActive_m7B201C061869970F7E6A4695D2E24E901EE62CD3 (void);
// 0x000008BE System.Void UnityEngine.PostProcessing.BuiltinDebugViewsModel::.ctor()
extern void BuiltinDebugViewsModel__ctor_mF105B8AE590F6631E06C79CC347B09C87A2B7CF4 (void);
// 0x000008BF UnityEngine.PostProcessing.BuiltinDebugViewsModel/DepthSettings UnityEngine.PostProcessing.BuiltinDebugViewsModel/DepthSettings::get_defaultSettings()
extern void DepthSettings_get_defaultSettings_mBE9E2BBBF99432BED7B8E3A11C6B0DB891B438FC (void);
// 0x000008C0 UnityEngine.PostProcessing.BuiltinDebugViewsModel/MotionVectorsSettings UnityEngine.PostProcessing.BuiltinDebugViewsModel/MotionVectorsSettings::get_defaultSettings()
extern void MotionVectorsSettings_get_defaultSettings_mCF968C1FFA44375D6B6D7049096F59F3F3E4D2B6 (void);
// 0x000008C1 UnityEngine.PostProcessing.BuiltinDebugViewsModel/Settings UnityEngine.PostProcessing.BuiltinDebugViewsModel/Settings::get_defaultSettings()
extern void Settings_get_defaultSettings_m0B1672C6368B880D4817C6AEDCD3EE009E6EAB14 (void);
// 0x000008C2 UnityEngine.PostProcessing.ChromaticAberrationModel/Settings UnityEngine.PostProcessing.ChromaticAberrationModel::get_settings()
extern void ChromaticAberrationModel_get_settings_mC53C47B383BD1B7EE8235EDD9379FABD0CBA05D7 (void);
// 0x000008C3 System.Void UnityEngine.PostProcessing.ChromaticAberrationModel::set_settings(UnityEngine.PostProcessing.ChromaticAberrationModel/Settings)
extern void ChromaticAberrationModel_set_settings_m41E200B5977BA43ABA907E89E9B1042537DEDC00 (void);
// 0x000008C4 System.Void UnityEngine.PostProcessing.ChromaticAberrationModel::Reset()
extern void ChromaticAberrationModel_Reset_m10EA27E5D4B6F5042210AA397310C079D51D93C4 (void);
// 0x000008C5 System.Void UnityEngine.PostProcessing.ChromaticAberrationModel::.ctor()
extern void ChromaticAberrationModel__ctor_mC064E504E1F0307E5D189EB9A26E80C04737A077 (void);
// 0x000008C6 UnityEngine.PostProcessing.ChromaticAberrationModel/Settings UnityEngine.PostProcessing.ChromaticAberrationModel/Settings::get_defaultSettings()
extern void Settings_get_defaultSettings_m791BDC0A3BA6328ADB04192E41FE897BBA646B37 (void);
// 0x000008C7 UnityEngine.PostProcessing.ColorGradingModel/Settings UnityEngine.PostProcessing.ColorGradingModel::get_settings()
extern void ColorGradingModel_get_settings_m2EA973DC7AF3BB333EF377C2E565DDF2AFBD0A45 (void);
// 0x000008C8 System.Void UnityEngine.PostProcessing.ColorGradingModel::set_settings(UnityEngine.PostProcessing.ColorGradingModel/Settings)
extern void ColorGradingModel_set_settings_m77858CD8DA8C0CDA7001AA27D37B080037F8C478 (void);
// 0x000008C9 System.Boolean UnityEngine.PostProcessing.ColorGradingModel::get_isDirty()
extern void ColorGradingModel_get_isDirty_mCE08360C3DF82F85F7461B05FE080CFBD1852421 (void);
// 0x000008CA System.Void UnityEngine.PostProcessing.ColorGradingModel::set_isDirty(System.Boolean)
extern void ColorGradingModel_set_isDirty_mB2BEBC2495596F8A7E45B5A91216443983DCAD3E (void);
// 0x000008CB UnityEngine.RenderTexture UnityEngine.PostProcessing.ColorGradingModel::get_bakedLut()
extern void ColorGradingModel_get_bakedLut_mA36059B954731D5EC292927A6CEE8D8BAC2CC510 (void);
// 0x000008CC System.Void UnityEngine.PostProcessing.ColorGradingModel::set_bakedLut(UnityEngine.RenderTexture)
extern void ColorGradingModel_set_bakedLut_mBA683C6FE3F64CB3D93BEF215B6C998FF4FDA5BE (void);
// 0x000008CD System.Void UnityEngine.PostProcessing.ColorGradingModel::Reset()
extern void ColorGradingModel_Reset_m015E0C3E0A1DFCB0F98F6C1AA2D1DAABA9CA7F2E (void);
// 0x000008CE System.Void UnityEngine.PostProcessing.ColorGradingModel::OnValidate()
extern void ColorGradingModel_OnValidate_mE7C9C32AB361E110AA7C2A34B22DFAB46D9A4AE5 (void);
// 0x000008CF System.Void UnityEngine.PostProcessing.ColorGradingModel::.ctor()
extern void ColorGradingModel__ctor_m68130B91EE52D2A11F0BA40092D93EDB4F3A5996 (void);
// 0x000008D0 UnityEngine.PostProcessing.ColorGradingModel/TonemappingSettings UnityEngine.PostProcessing.ColorGradingModel/TonemappingSettings::get_defaultSettings()
extern void TonemappingSettings_get_defaultSettings_m020E6A42A1677C1396D70795E85C1A69E47BA6B6 (void);
// 0x000008D1 UnityEngine.PostProcessing.ColorGradingModel/BasicSettings UnityEngine.PostProcessing.ColorGradingModel/BasicSettings::get_defaultSettings()
extern void BasicSettings_get_defaultSettings_m24138DA6AD05E42A0F4FFD778F19162240398986 (void);
// 0x000008D2 UnityEngine.PostProcessing.ColorGradingModel/ChannelMixerSettings UnityEngine.PostProcessing.ColorGradingModel/ChannelMixerSettings::get_defaultSettings()
extern void ChannelMixerSettings_get_defaultSettings_m7A8E8EB1A356EF330B897AC1FDD12554484C0A61 (void);
// 0x000008D3 UnityEngine.PostProcessing.ColorGradingModel/LogWheelsSettings UnityEngine.PostProcessing.ColorGradingModel/LogWheelsSettings::get_defaultSettings()
extern void LogWheelsSettings_get_defaultSettings_m145CBBD7D33AD3D97304172C23B418BE37393C2E (void);
// 0x000008D4 UnityEngine.PostProcessing.ColorGradingModel/LinearWheelsSettings UnityEngine.PostProcessing.ColorGradingModel/LinearWheelsSettings::get_defaultSettings()
extern void LinearWheelsSettings_get_defaultSettings_mD917AC276172EFD4831502D72B8825CAA4F698C4 (void);
// 0x000008D5 UnityEngine.PostProcessing.ColorGradingModel/ColorWheelsSettings UnityEngine.PostProcessing.ColorGradingModel/ColorWheelsSettings::get_defaultSettings()
extern void ColorWheelsSettings_get_defaultSettings_m826EE33BD2BFD73210D8950D645031157013FE31 (void);
// 0x000008D6 UnityEngine.PostProcessing.ColorGradingModel/CurvesSettings UnityEngine.PostProcessing.ColorGradingModel/CurvesSettings::get_defaultSettings()
extern void CurvesSettings_get_defaultSettings_mE69800F815D69BA509C8FF03ADEAB1C3CFF582A4 (void);
// 0x000008D7 UnityEngine.PostProcessing.ColorGradingModel/Settings UnityEngine.PostProcessing.ColorGradingModel/Settings::get_defaultSettings()
extern void Settings_get_defaultSettings_m8A9FD147177C9F669E8149E0BEF03201C449FEF1 (void);
// 0x000008D8 UnityEngine.PostProcessing.DepthOfFieldModel/Settings UnityEngine.PostProcessing.DepthOfFieldModel::get_settings()
extern void DepthOfFieldModel_get_settings_mAF2B9D62F10BFB6A49DACE1A855CB35B50D468E1 (void);
// 0x000008D9 System.Void UnityEngine.PostProcessing.DepthOfFieldModel::set_settings(UnityEngine.PostProcessing.DepthOfFieldModel/Settings)
extern void DepthOfFieldModel_set_settings_m9650C0CC437FE4F02040C18C66749A333515EEE3 (void);
// 0x000008DA System.Void UnityEngine.PostProcessing.DepthOfFieldModel::Reset()
extern void DepthOfFieldModel_Reset_m8B50D23681715ADBA273D8A4B9AB4372438EB522 (void);
// 0x000008DB System.Void UnityEngine.PostProcessing.DepthOfFieldModel::.ctor()
extern void DepthOfFieldModel__ctor_mCAC4870977FE18649F8AF0D79F3B24569A6026A9 (void);
// 0x000008DC UnityEngine.PostProcessing.DepthOfFieldModel/Settings UnityEngine.PostProcessing.DepthOfFieldModel/Settings::get_defaultSettings()
extern void Settings_get_defaultSettings_mABFD370ECDB30AF177A0D98B27D76BCFD38E9B47 (void);
// 0x000008DD UnityEngine.PostProcessing.DitheringModel/Settings UnityEngine.PostProcessing.DitheringModel::get_settings()
extern void DitheringModel_get_settings_m8D23BF6769B067C7ED743CD644C3B4AD3109226D (void);
// 0x000008DE System.Void UnityEngine.PostProcessing.DitheringModel::set_settings(UnityEngine.PostProcessing.DitheringModel/Settings)
extern void DitheringModel_set_settings_mFE8DA99430E47BA302EDBC94DD59B3899FD4FB14 (void);
// 0x000008DF System.Void UnityEngine.PostProcessing.DitheringModel::Reset()
extern void DitheringModel_Reset_mA803DE0E46B1D9B6E844E38D1B7F9D4E8D1DF74F (void);
// 0x000008E0 System.Void UnityEngine.PostProcessing.DitheringModel::.ctor()
extern void DitheringModel__ctor_m7F55D82228AA4AAA6FE23D275B38FC0F841F1BE2 (void);
// 0x000008E1 UnityEngine.PostProcessing.DitheringModel/Settings UnityEngine.PostProcessing.DitheringModel/Settings::get_defaultSettings()
extern void Settings_get_defaultSettings_mBDFB1447E0EDB6E476EB51D2A5EC758749A4D4C8 (void);
// 0x000008E2 UnityEngine.PostProcessing.EyeAdaptationModel/Settings UnityEngine.PostProcessing.EyeAdaptationModel::get_settings()
extern void EyeAdaptationModel_get_settings_mC49DE55E13916C1808242082918EAF36A63AF4E9 (void);
// 0x000008E3 System.Void UnityEngine.PostProcessing.EyeAdaptationModel::set_settings(UnityEngine.PostProcessing.EyeAdaptationModel/Settings)
extern void EyeAdaptationModel_set_settings_mC1533BB0C826E05964214ADA15BA7D2E655FD449 (void);
// 0x000008E4 System.Void UnityEngine.PostProcessing.EyeAdaptationModel::Reset()
extern void EyeAdaptationModel_Reset_m1EAAE64EA5B66F0C3BC9EB2662005B23D213B13C (void);
// 0x000008E5 System.Void UnityEngine.PostProcessing.EyeAdaptationModel::.ctor()
extern void EyeAdaptationModel__ctor_m33CEFCB575539035C72CA3A766BADFBB819E1D18 (void);
// 0x000008E6 UnityEngine.PostProcessing.EyeAdaptationModel/Settings UnityEngine.PostProcessing.EyeAdaptationModel/Settings::get_defaultSettings()
extern void Settings_get_defaultSettings_m2344CFBEAD562CEBC2691DF2F31544D2D1AECBAB (void);
// 0x000008E7 UnityEngine.PostProcessing.FogModel/Settings UnityEngine.PostProcessing.FogModel::get_settings()
extern void FogModel_get_settings_m1329E6494ED6C23AA93CB2CE59CACCD7AC440BE7 (void);
// 0x000008E8 System.Void UnityEngine.PostProcessing.FogModel::set_settings(UnityEngine.PostProcessing.FogModel/Settings)
extern void FogModel_set_settings_m65364F86D54F14BD081C39D39AEA32845FAC973D (void);
// 0x000008E9 System.Void UnityEngine.PostProcessing.FogModel::Reset()
extern void FogModel_Reset_m5CA0A31F82E608D4F0F724D2EA102B0E271725EC (void);
// 0x000008EA System.Void UnityEngine.PostProcessing.FogModel::.ctor()
extern void FogModel__ctor_mFC079C0889935D1BCABA42D2C3ECAB80CC86AB37 (void);
// 0x000008EB UnityEngine.PostProcessing.FogModel/Settings UnityEngine.PostProcessing.FogModel/Settings::get_defaultSettings()
extern void Settings_get_defaultSettings_m3F80C1208F2B493034B966175DA19D8BCE3B9001 (void);
// 0x000008EC UnityEngine.PostProcessing.GrainModel/Settings UnityEngine.PostProcessing.GrainModel::get_settings()
extern void GrainModel_get_settings_m0F0B56E8699AFB526BDF0C294934CB3A9F686C38 (void);
// 0x000008ED System.Void UnityEngine.PostProcessing.GrainModel::set_settings(UnityEngine.PostProcessing.GrainModel/Settings)
extern void GrainModel_set_settings_m5B93A60B5D5103E5E2F60C09C1725EC5FB0415B6 (void);
// 0x000008EE System.Void UnityEngine.PostProcessing.GrainModel::Reset()
extern void GrainModel_Reset_m9F0E7707467F681C523B694B7139B65BC8E19199 (void);
// 0x000008EF System.Void UnityEngine.PostProcessing.GrainModel::.ctor()
extern void GrainModel__ctor_m674882DF0D2FA7158846587A100E268CBB666DDB (void);
// 0x000008F0 UnityEngine.PostProcessing.GrainModel/Settings UnityEngine.PostProcessing.GrainModel/Settings::get_defaultSettings()
extern void Settings_get_defaultSettings_mE53750093B8A4A26FF705D456758ACC6AD7EEA56 (void);
// 0x000008F1 UnityEngine.PostProcessing.MotionBlurModel/Settings UnityEngine.PostProcessing.MotionBlurModel::get_settings()
extern void MotionBlurModel_get_settings_mD483F1C4CBE78CA212AF73A8334F79510090BC5F (void);
// 0x000008F2 System.Void UnityEngine.PostProcessing.MotionBlurModel::set_settings(UnityEngine.PostProcessing.MotionBlurModel/Settings)
extern void MotionBlurModel_set_settings_m3C8D69A114CF2FAAB9E05ED9CC8497BD163528CB (void);
// 0x000008F3 System.Void UnityEngine.PostProcessing.MotionBlurModel::Reset()
extern void MotionBlurModel_Reset_m09AAD87FC9997C56E9497099DE044BAF88A65421 (void);
// 0x000008F4 System.Void UnityEngine.PostProcessing.MotionBlurModel::.ctor()
extern void MotionBlurModel__ctor_mCD82263955130CB0C9070C29CA23F22810FAA5DE (void);
// 0x000008F5 UnityEngine.PostProcessing.MotionBlurModel/Settings UnityEngine.PostProcessing.MotionBlurModel/Settings::get_defaultSettings()
extern void Settings_get_defaultSettings_mD3A95AF0C716D2864CE8EB106FE41F08289841C1 (void);
// 0x000008F6 UnityEngine.PostProcessing.ScreenSpaceReflectionModel/Settings UnityEngine.PostProcessing.ScreenSpaceReflectionModel::get_settings()
extern void ScreenSpaceReflectionModel_get_settings_m18F1FEFCFE91B7C24F1B883A1F4A8A793E6F2207 (void);
// 0x000008F7 System.Void UnityEngine.PostProcessing.ScreenSpaceReflectionModel::set_settings(UnityEngine.PostProcessing.ScreenSpaceReflectionModel/Settings)
extern void ScreenSpaceReflectionModel_set_settings_mCC78EA8D34FF6CD9E1970A84054406630BDE560D (void);
// 0x000008F8 System.Void UnityEngine.PostProcessing.ScreenSpaceReflectionModel::Reset()
extern void ScreenSpaceReflectionModel_Reset_m31181E25C93827A9213B31D7F5E8FD1450F4CBD7 (void);
// 0x000008F9 System.Void UnityEngine.PostProcessing.ScreenSpaceReflectionModel::.ctor()
extern void ScreenSpaceReflectionModel__ctor_m638D25056004BAB4C3D8AE46CAD0FFE861F48F15 (void);
// 0x000008FA UnityEngine.PostProcessing.ScreenSpaceReflectionModel/Settings UnityEngine.PostProcessing.ScreenSpaceReflectionModel/Settings::get_defaultSettings()
extern void Settings_get_defaultSettings_m13216005F7A1ACB0998EBED272CA93D400CB3BCC (void);
// 0x000008FB UnityEngine.PostProcessing.UserLutModel/Settings UnityEngine.PostProcessing.UserLutModel::get_settings()
extern void UserLutModel_get_settings_mE109CC3AC743DB5B068CFF1088592D8A4936935D (void);
// 0x000008FC System.Void UnityEngine.PostProcessing.UserLutModel::set_settings(UnityEngine.PostProcessing.UserLutModel/Settings)
extern void UserLutModel_set_settings_m9CF084A35B9A5D3FA56E79FFAB6273F477BE3010 (void);
// 0x000008FD System.Void UnityEngine.PostProcessing.UserLutModel::Reset()
extern void UserLutModel_Reset_mA83F913DE67FBED1483AEF3A44683678DE87731D (void);
// 0x000008FE System.Void UnityEngine.PostProcessing.UserLutModel::.ctor()
extern void UserLutModel__ctor_m3E0004D52613BFF6ECA0E03DC79D922F3A6213B5 (void);
// 0x000008FF UnityEngine.PostProcessing.UserLutModel/Settings UnityEngine.PostProcessing.UserLutModel/Settings::get_defaultSettings()
extern void Settings_get_defaultSettings_m49DC2107075E1ECC4561EC906DBF69B5FA87980A (void);
// 0x00000900 UnityEngine.PostProcessing.VignetteModel/Settings UnityEngine.PostProcessing.VignetteModel::get_settings()
extern void VignetteModel_get_settings_mA0B3B522622D2FC247E9A2DF11A1AF78A3BEF8F6 (void);
// 0x00000901 System.Void UnityEngine.PostProcessing.VignetteModel::set_settings(UnityEngine.PostProcessing.VignetteModel/Settings)
extern void VignetteModel_set_settings_m6E295AF7A082C2A092AA29E14F1DD02A3D941DD4 (void);
// 0x00000902 System.Void UnityEngine.PostProcessing.VignetteModel::Reset()
extern void VignetteModel_Reset_m17F63609C24EEDA2F1A4477A3629CC2E6523A92F (void);
// 0x00000903 System.Void UnityEngine.PostProcessing.VignetteModel::.ctor()
extern void VignetteModel__ctor_m86430F872F45C41B1241D3F281DD90FEA93484D1 (void);
// 0x00000904 UnityEngine.PostProcessing.VignetteModel/Settings UnityEngine.PostProcessing.VignetteModel/Settings::get_defaultSettings()
extern void Settings_get_defaultSettings_mA0F09C1FA146892325FF02BE1859A7E117BDA9C7 (void);
// 0x00000905 System.Void UnityEngine.PostProcessing.PostProcessingBehaviour::OnEnable()
extern void PostProcessingBehaviour_OnEnable_mCC96A9D7BB473E3F8C11D9280204B95748C06467 (void);
// 0x00000906 System.Void UnityEngine.PostProcessing.PostProcessingBehaviour::OnPreCull()
extern void PostProcessingBehaviour_OnPreCull_m24D068B04C5DECDA0FFFB08E929C61942AF91C5F (void);
// 0x00000907 System.Void UnityEngine.PostProcessing.PostProcessingBehaviour::OnPreRender()
extern void PostProcessingBehaviour_OnPreRender_m946BE947393B10A7A6A8BA12481D35142078861E (void);
// 0x00000908 System.Void UnityEngine.PostProcessing.PostProcessingBehaviour::OnPostRender()
extern void PostProcessingBehaviour_OnPostRender_m497329C185DA9EB9B382618733FFC70631262459 (void);
// 0x00000909 System.Void UnityEngine.PostProcessing.PostProcessingBehaviour::OnRenderImage(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern void PostProcessingBehaviour_OnRenderImage_m0C0A56A6818A443903940D0557FEBFB29590A4B3 (void);
// 0x0000090A System.Void UnityEngine.PostProcessing.PostProcessingBehaviour::OnGUI()
extern void PostProcessingBehaviour_OnGUI_mA5A9BA4F5503BF6F81C1C8E45C5D0CBAA32117DA (void);
// 0x0000090B System.Void UnityEngine.PostProcessing.PostProcessingBehaviour::OnDisable()
extern void PostProcessingBehaviour_OnDisable_mD592853CC86C4434AC5552016DFE248278BF0F3A (void);
// 0x0000090C System.Void UnityEngine.PostProcessing.PostProcessingBehaviour::ResetTemporalEffects()
extern void PostProcessingBehaviour_ResetTemporalEffects_mA355B277D80B827CF5A7063EC126664D41F6AC28 (void);
// 0x0000090D System.Void UnityEngine.PostProcessing.PostProcessingBehaviour::CheckObservers()
extern void PostProcessingBehaviour_CheckObservers_mC7D9C361FBCB6057735CEF27775D54E708EDB3DA (void);
// 0x0000090E System.Void UnityEngine.PostProcessing.PostProcessingBehaviour::DisableComponents()
extern void PostProcessingBehaviour_DisableComponents_mCA4304C5D91742B94E55E1C141C59426294C36F1 (void);
// 0x0000090F UnityEngine.Rendering.CommandBuffer UnityEngine.PostProcessing.PostProcessingBehaviour::AddCommandBuffer(UnityEngine.Rendering.CameraEvent,System.String)
// 0x00000910 System.Void UnityEngine.PostProcessing.PostProcessingBehaviour::RemoveCommandBuffer()
// 0x00000911 UnityEngine.Rendering.CommandBuffer UnityEngine.PostProcessing.PostProcessingBehaviour::GetCommandBuffer(UnityEngine.Rendering.CameraEvent,System.String)
// 0x00000912 System.Void UnityEngine.PostProcessing.PostProcessingBehaviour::TryExecuteCommandBuffer(UnityEngine.PostProcessing.PostProcessingComponentCommandBuffer`1<T>)
// 0x00000913 System.Boolean UnityEngine.PostProcessing.PostProcessingBehaviour::TryPrepareUberImageEffect(UnityEngine.PostProcessing.PostProcessingComponentRenderTexture`1<T>,UnityEngine.Material)
// 0x00000914 T UnityEngine.PostProcessing.PostProcessingBehaviour::AddComponent(T)
// 0x00000915 System.Void UnityEngine.PostProcessing.PostProcessingBehaviour::.ctor()
extern void PostProcessingBehaviour__ctor_mFCFE406D007166C2D90597D7B21546FE57C4A619 (void);
// 0x00000916 UnityEngine.DepthTextureMode UnityEngine.PostProcessing.PostProcessingComponentBase::GetCameraFlags()
extern void PostProcessingComponentBase_GetCameraFlags_m0965E0C24C420F1883F838C468743696BC242175 (void);
// 0x00000917 System.Boolean UnityEngine.PostProcessing.PostProcessingComponentBase::get_active()
// 0x00000918 System.Void UnityEngine.PostProcessing.PostProcessingComponentBase::OnEnable()
extern void PostProcessingComponentBase_OnEnable_mDA6132C51C900B51AA1E9F5EEB9BA791DCD0A380 (void);
// 0x00000919 System.Void UnityEngine.PostProcessing.PostProcessingComponentBase::OnDisable()
extern void PostProcessingComponentBase_OnDisable_m339B13F2569EFECDB14DA03FBE4553D35EC28EC7 (void);
// 0x0000091A UnityEngine.PostProcessing.PostProcessingModel UnityEngine.PostProcessing.PostProcessingComponentBase::GetModel()
// 0x0000091B System.Void UnityEngine.PostProcessing.PostProcessingComponentBase::.ctor()
extern void PostProcessingComponentBase__ctor_m6973C9B7B13DF7B63B6E24F22A1465C4058AAA1C (void);
// 0x0000091C T UnityEngine.PostProcessing.PostProcessingComponent`1::get_model()
// 0x0000091D System.Void UnityEngine.PostProcessing.PostProcessingComponent`1::set_model(T)
// 0x0000091E System.Void UnityEngine.PostProcessing.PostProcessingComponent`1::Init(UnityEngine.PostProcessing.PostProcessingContext,T)
// 0x0000091F UnityEngine.PostProcessing.PostProcessingModel UnityEngine.PostProcessing.PostProcessingComponent`1::GetModel()
// 0x00000920 System.Void UnityEngine.PostProcessing.PostProcessingComponent`1::.ctor()
// 0x00000921 UnityEngine.Rendering.CameraEvent UnityEngine.PostProcessing.PostProcessingComponentCommandBuffer`1::GetCameraEvent()
// 0x00000922 System.String UnityEngine.PostProcessing.PostProcessingComponentCommandBuffer`1::GetName()
// 0x00000923 System.Void UnityEngine.PostProcessing.PostProcessingComponentCommandBuffer`1::PopulateCommandBuffer(UnityEngine.Rendering.CommandBuffer)
// 0x00000924 System.Void UnityEngine.PostProcessing.PostProcessingComponentCommandBuffer`1::.ctor()
// 0x00000925 System.Void UnityEngine.PostProcessing.PostProcessingComponentRenderTexture`1::Prepare(UnityEngine.Material)
// 0x00000926 System.Void UnityEngine.PostProcessing.PostProcessingComponentRenderTexture`1::.ctor()
// 0x00000927 System.Boolean UnityEngine.PostProcessing.PostProcessingContext::get_interrupted()
extern void PostProcessingContext_get_interrupted_mD4B924B1F05E49F1A9A6827163E371A45C9720A5 (void);
// 0x00000928 System.Void UnityEngine.PostProcessing.PostProcessingContext::set_interrupted(System.Boolean)
extern void PostProcessingContext_set_interrupted_m3DBDCA425B59F1582D272385FA4C3799FD5DA2A0 (void);
// 0x00000929 System.Void UnityEngine.PostProcessing.PostProcessingContext::Interrupt()
extern void PostProcessingContext_Interrupt_m224A982174C1C6108E2A2A037088B429B92D633D (void);
// 0x0000092A UnityEngine.PostProcessing.PostProcessingContext UnityEngine.PostProcessing.PostProcessingContext::Reset()
extern void PostProcessingContext_Reset_m91E248448B7DE654B5C9017AD9411392D408B274 (void);
// 0x0000092B System.Boolean UnityEngine.PostProcessing.PostProcessingContext::get_isGBufferAvailable()
extern void PostProcessingContext_get_isGBufferAvailable_mB06B5DC61A22CDECB02F28FDDF082A4D613F73B3 (void);
// 0x0000092C System.Boolean UnityEngine.PostProcessing.PostProcessingContext::get_isHdr()
extern void PostProcessingContext_get_isHdr_m3DEA2A02460B55B5E49938CAD7AB55EC4588BE3D (void);
// 0x0000092D System.Int32 UnityEngine.PostProcessing.PostProcessingContext::get_width()
extern void PostProcessingContext_get_width_m47E8EC1F6D88E4DB9713328878FAD8B315E60E4F (void);
// 0x0000092E System.Int32 UnityEngine.PostProcessing.PostProcessingContext::get_height()
extern void PostProcessingContext_get_height_m9D76FBA46F89929C331939910D5EFA5C11720CEC (void);
// 0x0000092F UnityEngine.Rect UnityEngine.PostProcessing.PostProcessingContext::get_viewport()
extern void PostProcessingContext_get_viewport_m4462CF303823BDD387DFBE73A7482AC19B09E88B (void);
// 0x00000930 System.Void UnityEngine.PostProcessing.PostProcessingContext::.ctor()
extern void PostProcessingContext__ctor_m4D3C87FA5CA436C62B4703150E619F0B3C3BC84D (void);
// 0x00000931 System.Boolean UnityEngine.PostProcessing.PostProcessingModel::get_enabled()
extern void PostProcessingModel_get_enabled_m3C190F4C9B03449BAC487ECE0A03E3F524BF5E95 (void);
// 0x00000932 System.Void UnityEngine.PostProcessing.PostProcessingModel::set_enabled(System.Boolean)
extern void PostProcessingModel_set_enabled_m8446050CDF2021DAAA7E785989CDF0274E2CDF91 (void);
// 0x00000933 System.Void UnityEngine.PostProcessing.PostProcessingModel::Reset()
// 0x00000934 System.Void UnityEngine.PostProcessing.PostProcessingModel::OnValidate()
extern void PostProcessingModel_OnValidate_m05B5A334ABE183A6E543A675A12458E44DDAB398 (void);
// 0x00000935 System.Void UnityEngine.PostProcessing.PostProcessingModel::.ctor()
extern void PostProcessingModel__ctor_mD66941A8E843AEC57572946AB3F9A66B7B17351D (void);
// 0x00000936 System.Void UnityEngine.PostProcessing.PostProcessingProfile::.ctor()
extern void PostProcessingProfile__ctor_mDDEFF2C43547D23A3DF77EDD787F52F087DE8BC3 (void);
// 0x00000937 System.Void UnityEngine.PostProcessing.ColorGradingCurve::.ctor(UnityEngine.AnimationCurve,System.Single,System.Boolean,UnityEngine.Vector2)
extern void ColorGradingCurve__ctor_m1F50481353D7421A25F249B584E0901FD7E92383 (void);
// 0x00000938 System.Void UnityEngine.PostProcessing.ColorGradingCurve::Cache()
extern void ColorGradingCurve_Cache_mDF20D10D83B9051AF369C4C06F9B8F51B1ABC2F8 (void);
// 0x00000939 System.Single UnityEngine.PostProcessing.ColorGradingCurve::Evaluate(System.Single)
extern void ColorGradingCurve_Evaluate_mC8FA46F66E495AA80618BEFCFAF026D06DD93F5B (void);
// 0x0000093A System.Boolean UnityEngine.PostProcessing.GraphicsUtils::get_isLinearColorSpace()
extern void GraphicsUtils_get_isLinearColorSpace_m7ABDDB13E42678E446B40014756BA3676C3DCEC4 (void);
// 0x0000093B System.Boolean UnityEngine.PostProcessing.GraphicsUtils::get_supportsDX11()
extern void GraphicsUtils_get_supportsDX11_mD23FAE3A45F359483A5EC8FB0BE0DBDDCFE18BA5 (void);
// 0x0000093C UnityEngine.Texture2D UnityEngine.PostProcessing.GraphicsUtils::get_whiteTexture()
extern void GraphicsUtils_get_whiteTexture_mED3B6448597490C8D9DA690D10A14731C496A82F (void);
// 0x0000093D UnityEngine.Mesh UnityEngine.PostProcessing.GraphicsUtils::get_quad()
extern void GraphicsUtils_get_quad_m759AD9D70DDD5FEFB815926855C749FF1D954377 (void);
// 0x0000093E System.Void UnityEngine.PostProcessing.GraphicsUtils::Blit(UnityEngine.Material,System.Int32)
extern void GraphicsUtils_Blit_mD74B897D1A76BB3A707FEC4734D21C174CA7A227 (void);
// 0x0000093F System.Void UnityEngine.PostProcessing.GraphicsUtils::ClearAndBlit(UnityEngine.Texture,UnityEngine.RenderTexture,UnityEngine.Material,System.Int32,System.Boolean,System.Boolean)
extern void GraphicsUtils_ClearAndBlit_mBBCC2133ADCA1C7799E79261F35A025B8737804C (void);
// 0x00000940 System.Void UnityEngine.PostProcessing.GraphicsUtils::Destroy(UnityEngine.Object)
extern void GraphicsUtils_Destroy_mC81DD13284816A2F5F2D10CE9D6850E197DAC550 (void);
// 0x00000941 System.Void UnityEngine.PostProcessing.GraphicsUtils::Dispose()
extern void GraphicsUtils_Dispose_m3669123C53863B048A91CA8CEDEEB054233108F6 (void);
// 0x00000942 System.Void UnityEngine.PostProcessing.MaterialFactory::.ctor()
extern void MaterialFactory__ctor_m7395EAC727BBC78DF1013999E763FF810EEFB0FE (void);
// 0x00000943 UnityEngine.Material UnityEngine.PostProcessing.MaterialFactory::Get(System.String)
extern void MaterialFactory_Get_m2C8DEF060705F34246D4BFA2A758647B7005C9FF (void);
// 0x00000944 System.Void UnityEngine.PostProcessing.MaterialFactory::Dispose()
extern void MaterialFactory_Dispose_mEDD1D01A1B06F228AAC07B915CC7EEBC7CC52668 (void);
// 0x00000945 System.Void UnityEngine.PostProcessing.RenderTextureFactory::.ctor()
extern void RenderTextureFactory__ctor_mF119EA0BBEDC89E81B7862174702BDBAC360B6A4 (void);
// 0x00000946 UnityEngine.RenderTexture UnityEngine.PostProcessing.RenderTextureFactory::Get(UnityEngine.RenderTexture)
extern void RenderTextureFactory_Get_m30A83EBD7B33051FEF9F47A1ED9965C0C0E3D8D7 (void);
// 0x00000947 UnityEngine.RenderTexture UnityEngine.PostProcessing.RenderTextureFactory::Get(System.Int32,System.Int32,System.Int32,UnityEngine.RenderTextureFormat,UnityEngine.RenderTextureReadWrite,UnityEngine.FilterMode,UnityEngine.TextureWrapMode,System.String)
extern void RenderTextureFactory_Get_m5E8066AD53F5D57E2DBF503198A7AA93EE86BA91 (void);
// 0x00000948 System.Void UnityEngine.PostProcessing.RenderTextureFactory::Release(UnityEngine.RenderTexture)
extern void RenderTextureFactory_Release_mE496D1BC8CCB04A6E0A385BCF39614FE06D9B541 (void);
// 0x00000949 System.Void UnityEngine.PostProcessing.RenderTextureFactory::ReleaseAll()
extern void RenderTextureFactory_ReleaseAll_m89CA9981D734B168A7E127EE68CC88C45635A268 (void);
// 0x0000094A System.Void UnityEngine.PostProcessing.RenderTextureFactory::Dispose()
extern void RenderTextureFactory_Dispose_m72D61911A920C073AE20175D140AC20CA786B424 (void);
// 0x0000094B System.String[] FrostweepGames.Plugins.Native.CustomMicrophone::get_devices()
extern void CustomMicrophone_get_devices_mEEAA2FC926FE7BADE2B6FA8FB614BCCAC006739E (void);
// 0x0000094C UnityEngine.AudioClip FrostweepGames.Plugins.Native.CustomMicrophone::Start(System.String,System.Boolean,System.Int32,System.Int32)
extern void CustomMicrophone_Start_m26B4828589CE729AB1E0FDC1B114593A239436F9 (void);
// 0x0000094D System.Boolean FrostweepGames.Plugins.Native.CustomMicrophone::IsRecording(System.String)
extern void CustomMicrophone_IsRecording_mDF34F52A65BBFE0567B5ED6860DBA51DB159C4F6 (void);
// 0x0000094E System.Void FrostweepGames.Plugins.Native.CustomMicrophone::GetDeviceCaps(System.String,System.Int32&,System.Int32&)
extern void CustomMicrophone_GetDeviceCaps_m37AF24EA8FA2D21D2230FBFCCA555EA3F4956593 (void);
// 0x0000094F System.Int32 FrostweepGames.Plugins.Native.CustomMicrophone::GetPosition(System.String)
extern void CustomMicrophone_GetPosition_mE7FFB4D8D1FF14228C6B32933A37A4ACEF3878AF (void);
// 0x00000950 System.Void FrostweepGames.Plugins.Native.CustomMicrophone::End(System.String)
extern void CustomMicrophone_End_mFA27CCFF1645B191EFB2EDAF1899134BCECB5396 (void);
// 0x00000951 System.Boolean FrostweepGames.Plugins.Native.CustomMicrophone::HasConnectedMicrophoneDevices()
extern void CustomMicrophone_HasConnectedMicrophoneDevices_m6FEE22CF63BC91DF3B34B25BA74A0D14AB5CA533 (void);
// 0x00000952 System.Void FrostweepGames.Plugins.Native.CustomMicrophone::RequestMicrophonePermission()
extern void CustomMicrophone_RequestMicrophonePermission_m394A9E7B25D95481A5F4E094A9BCFA328062E291 (void);
// 0x00000953 System.Boolean FrostweepGames.Plugins.Native.CustomMicrophone::HasMicrophonePermission()
extern void CustomMicrophone_HasMicrophonePermission_m7D44CAC92F1764F2216410F07A2EBB197AAEE59B (void);
// 0x00000954 System.Boolean FrostweepGames.Plugins.Native.CustomMicrophone::GetRawData(System.Single[]&,UnityEngine.AudioClip)
extern void CustomMicrophone_GetRawData_mC54002A61193D6E82917AB7992884B0065A9ACCF (void);
// 0x00000955 System.Void FrostweepGames.Plugins.Native.CustomMicrophone::.ctor()
extern void CustomMicrophone__ctor_mC17688DF04BC0D1476995174C3196EB1459753FE (void);
// 0x00000956 System.Void FrostweepGames.Plugins.Networking.NetworkConstants::.ctor()
extern void NetworkConstants__ctor_m3A7FCEF20B7D00B006EF4CD2F800D7D1F69FD7A6 (void);
// 0x00000957 System.Void FrostweepGames.Plugins.Networking.NetworkEnumerators::.ctor()
extern void NetworkEnumerators__ctor_mE94B83B891FDC10B06282634BE5E5DFD2786CEA4 (void);
// 0x00000958 System.Int64 FrostweepGames.Plugins.Networking.NetworkRequest::get_RequestId()
extern void NetworkRequest_get_RequestId_m2969362499CE21CFFDAD1A95EB5760423AA017EA (void);
// 0x00000959 System.Void FrostweepGames.Plugins.Networking.NetworkRequest::set_RequestId(System.Int64)
extern void NetworkRequest_set_RequestId_m0D8B8DD657B1B0F6A132E745291D4895DAF14F95 (void);
// 0x0000095A FrostweepGames.Plugins.Networking.NetworkEnumerators/RequestType FrostweepGames.Plugins.Networking.NetworkRequest::get_RequestType()
extern void NetworkRequest_get_RequestType_m92B34AB77AA059D5D61B90B14AC3BA6F7662E8EC (void);
// 0x0000095B System.Void FrostweepGames.Plugins.Networking.NetworkRequest::set_RequestType(FrostweepGames.Plugins.Networking.NetworkEnumerators/RequestType)
extern void NetworkRequest_set_RequestType_m1BCC9B85EFC4565332BC73747E28F6B5359E353A (void);
// 0x0000095C System.Object[] FrostweepGames.Plugins.Networking.NetworkRequest::get_Parameters()
extern void NetworkRequest_get_Parameters_m827091DF1E505089A4A6DF0F064588615AC8A0F3 (void);
// 0x0000095D System.Void FrostweepGames.Plugins.Networking.NetworkRequest::set_Parameters(System.Object[])
extern void NetworkRequest_set_Parameters_m2A85FE6E7E3D36EEDB37CCB233A4031D77B76A14 (void);
// 0x0000095E FrostweepGames.Plugins.Networking.NetworkMethod FrostweepGames.Plugins.Networking.NetworkRequest::get_Request()
extern void NetworkRequest_get_Request_m9260D4CC16EE4DB4DC104312B9E45164C8889C23 (void);
// 0x0000095F System.Void FrostweepGames.Plugins.Networking.NetworkRequest::set_Request(FrostweepGames.Plugins.Networking.NetworkMethod)
extern void NetworkRequest_set_Request_m0E3B27CE341C732AC8EC44C0780722BBD3FFA441 (void);
// 0x00000960 System.Void FrostweepGames.Plugins.Networking.NetworkRequest::.ctor(System.String,System.String,System.Int64,FrostweepGames.Plugins.Networking.NetworkEnumerators/RequestType,System.Object[],System.Boolean)
extern void NetworkRequest__ctor_m31C7215744A319183D9E46A55E161BC66AA0D752 (void);
// 0x00000961 System.Void FrostweepGames.Plugins.Networking.NetworkRequest::Send()
extern void NetworkRequest_Send_m4CCF7EADB0F112305647A5DB7E3E415A6AC18308 (void);
// 0x00000962 System.Boolean FrostweepGames.Plugins.Networking.NetworkMethod::get_isDone()
extern void NetworkMethod_get_isDone_mFBE895E18DA704AB8291D3638FF2A836D7B9E5E9 (void);
// 0x00000963 System.String FrostweepGames.Plugins.Networking.NetworkMethod::get_text()
extern void NetworkMethod_get_text_m336C07DA720B811A67EE18976EE1C97130FE81FC (void);
// 0x00000964 System.String FrostweepGames.Plugins.Networking.NetworkMethod::get_error()
extern void NetworkMethod_get_error_mCDBAD6808249F97CBC182CA2517D2573F23C2B2F (void);
// 0x00000965 System.Int64 FrostweepGames.Plugins.Networking.NetworkMethod::get_responseCode()
extern void NetworkMethod_get_responseCode_m9F34244FBA767D456084E7D9C69D8B138B53E1AC (void);
// 0x00000966 System.Void FrostweepGames.Plugins.Networking.NetworkMethod::.ctor(System.String,System.String,System.Boolean,FrostweepGames.Plugins.Networking.NetworkEnumerators/RequestType,FrostweepGames.Plugins.Networking.NetworkEnumerators/NetworkMethod)
extern void NetworkMethod__ctor_m7307703483824B5C3AB7BA5761AC062568BC5D41 (void);
// 0x00000967 System.Void FrostweepGames.Plugins.Networking.NetworkMethod::Send()
extern void NetworkMethod_Send_m1B217BC61892875766A1CF896C63A8809FFA8ACF (void);
// 0x00000968 System.Void FrostweepGames.Plugins.Networking.NetworkMethod::Cancel()
extern void NetworkMethod_Cancel_mA98BA7CDA4CF51BD22670FAB6BF4EAC581B9058A (void);
// 0x00000969 System.Int64 FrostweepGames.Plugins.Networking.NetworkResponse::get_RequestId()
extern void NetworkResponse_get_RequestId_mEEE1C0BEA1DADBDBD2614BFD5B34A45B2234975C (void);
// 0x0000096A System.Void FrostweepGames.Plugins.Networking.NetworkResponse::set_RequestId(System.Int64)
extern void NetworkResponse_set_RequestId_m6304592B381F6B58FA74C633535E1B037F0B29A1 (void);
// 0x0000096B FrostweepGames.Plugins.Networking.NetworkEnumerators/RequestType FrostweepGames.Plugins.Networking.NetworkResponse::get_RequestType()
extern void NetworkResponse_get_RequestType_m23F0A001C16B1FD8DBF1A48A3983174575AF2154 (void);
// 0x0000096C System.Void FrostweepGames.Plugins.Networking.NetworkResponse::set_RequestType(FrostweepGames.Plugins.Networking.NetworkEnumerators/RequestType)
extern void NetworkResponse_set_RequestType_m3D1122F5BC9F7B325D6BD545D92A1F42561D4C8B (void);
// 0x0000096D System.Object[] FrostweepGames.Plugins.Networking.NetworkResponse::get_Parameters()
extern void NetworkResponse_get_Parameters_m44A620E07F69E9496303C4F094B4B6299CB1C8DD (void);
// 0x0000096E System.Void FrostweepGames.Plugins.Networking.NetworkResponse::set_Parameters(System.Object[])
extern void NetworkResponse_set_Parameters_mF259D0ED405C34361BD71B63160FC6CEEC04D03C (void);
// 0x0000096F System.String FrostweepGames.Plugins.Networking.NetworkResponse::get_Response()
extern void NetworkResponse_get_Response_m24C3DF5E8E670A234FD0B20B2C9008C32FCC463F (void);
// 0x00000970 System.Void FrostweepGames.Plugins.Networking.NetworkResponse::set_Response(System.String)
extern void NetworkResponse_set_Response_m9F4EA0AD57BCB9F3DB8EB323190C2ADB2D82CF5E (void);
// 0x00000971 System.String FrostweepGames.Plugins.Networking.NetworkResponse::get_Error()
extern void NetworkResponse_get_Error_m3AADCC997DE7BEDD7B75FBF59861495C8CB4C75A (void);
// 0x00000972 System.Void FrostweepGames.Plugins.Networking.NetworkResponse::set_Error(System.String)
extern void NetworkResponse_set_Error_mEE96D159000568C6D5FC60448C3CC1CFAD689A9D (void);
// 0x00000973 System.Int64 FrostweepGames.Plugins.Networking.NetworkResponse::get_ResponseCode()
extern void NetworkResponse_get_ResponseCode_m666C1ED7EAFB0B40628367FD479D16988D126472 (void);
// 0x00000974 System.Void FrostweepGames.Plugins.Networking.NetworkResponse::set_ResponseCode(System.Int64)
extern void NetworkResponse_set_ResponseCode_m69921D1A553F087E40900D6F8BA086D0BE017DBE (void);
// 0x00000975 System.Void FrostweepGames.Plugins.Networking.NetworkResponse::.ctor(System.String,System.String,System.Int64,FrostweepGames.Plugins.Networking.NetworkEnumerators/RequestType,System.Object[])
extern void NetworkResponse__ctor_m3D04F1D38ACEDEE4E5C7FEAD4002B814CE1FDB38 (void);
// 0x00000976 System.Void FrostweepGames.Plugins.Networking.NetworkResponse::.ctor(FrostweepGames.Plugins.Networking.NetworkRequest)
extern void NetworkResponse__ctor_mA8A5ACAF75FBAA1F49D9A6D631B9B181B4EF6265 (void);
// 0x00000977 System.String FrostweepGames.Plugins.Networking.NetworkResponse::GetFullLog()
extern void NetworkResponse_GetFullLog_m8494E25A7B2BA6C3D9F1EB1B02871634B5CA4602 (void);
// 0x00000978 System.Boolean FrostweepGames.Plugins.Networking.NetworkResponse::HasError()
extern void NetworkResponse_HasError_m96B9A9BD9DBF44E6DEA071DABA48CF4E5FD5E38E (void);
// 0x00000979 System.Void FrostweepGames.Plugins.Networking.NetworkingService::add_NetworkResponseEvent(System.Action`1<FrostweepGames.Plugins.Networking.NetworkResponse>)
extern void NetworkingService_add_NetworkResponseEvent_m3B657FD26DA5F4F9EF1FFE1EC2CFED9A3A9FC0FF (void);
// 0x0000097A System.Void FrostweepGames.Plugins.Networking.NetworkingService::remove_NetworkResponseEvent(System.Action`1<FrostweepGames.Plugins.Networking.NetworkResponse>)
extern void NetworkingService_remove_NetworkResponseEvent_m7A7ED58D0632D2331F0092373A4B5C3F40B380F6 (void);
// 0x0000097B System.Void FrostweepGames.Plugins.Networking.NetworkingService::.ctor()
extern void NetworkingService__ctor_mB64F800056EC196EBD2F28D4FED8D1BD2C178B01 (void);
// 0x0000097C System.Void FrostweepGames.Plugins.Networking.NetworkingService::Update()
extern void NetworkingService_Update_mC443AB669DB96364456D0EA8175DAF8F076C7592 (void);
// 0x0000097D System.Void FrostweepGames.Plugins.Networking.NetworkingService::Dispose()
extern void NetworkingService_Dispose_m92DD5A5604CDCA99E3B304A33480CCBAC9B00518 (void);
// 0x0000097E System.Int64 FrostweepGames.Plugins.Networking.NetworkingService::SendRequest(System.String,System.String,FrostweepGames.Plugins.Networking.NetworkEnumerators/RequestType,System.Object[],System.Boolean)
extern void NetworkingService_SendRequest_m3B683689102827A62E7871DC5B1D93823FAF377F (void);
// 0x0000097F System.Boolean FrostweepGames.Plugins.Networking.NetworkingService::CancelRequest(System.Int64)
extern void NetworkingService_CancelRequest_mE14B7E1FBE41CA8BD3482DBA6B28FA4BBFCAE15E (void);
// 0x00000980 System.Int32 FrostweepGames.Plugins.Networking.NetworkingService::CancelAllRequests()
extern void NetworkingService_CancelAllRequests_m0400E62D6A8CACEEADAA9613B3DEC1A24E5105F0 (void);
// 0x00000981 System.Void FrostweepGames.Plugins.Networking.NetworkingService/<>c__DisplayClass10_0::.ctor()
extern void U3CU3Ec__DisplayClass10_0__ctor_mB13B700EE7045FC692BC7A44EDCCCB144FC76693 (void);
// 0x00000982 System.Boolean FrostweepGames.Plugins.Networking.NetworkingService/<>c__DisplayClass10_0::<CancelRequest>b__0(FrostweepGames.Plugins.Networking.NetworkRequest)
extern void U3CU3Ec__DisplayClass10_0_U3CCancelRequestU3Eb__0_mE0E28899EC6168A226351180E0DBCC3B2B1C6997 (void);
// 0x00000983 System.Void FrostweepGames.Plugins.Core.IService::Init()
// 0x00000984 System.Void FrostweepGames.Plugins.Core.IService::Update()
// 0x00000985 System.Void FrostweepGames.Plugins.Core.IService::Dispose()
// 0x00000986 FrostweepGames.Plugins.Core.ServiceLocator FrostweepGames.Plugins.Core.ServiceLocator::get_Instance()
extern void ServiceLocator_get_Instance_m4DA7D0F5488286A697049681E7AD236D65697F26 (void);
// 0x00000987 System.Void FrostweepGames.Plugins.Core.ServiceLocator::.ctor()
extern void ServiceLocator__ctor_m9096B1CB79055683681F21515B5B0BB26CC993EE (void);
// 0x00000988 System.Void FrostweepGames.Plugins.Core.ServiceLocator::InitServices()
extern void ServiceLocator_InitServices_m1F91E17DAD36EE14FEBF9683378F5BA7269FC183 (void);
// 0x00000989 System.Void FrostweepGames.Plugins.Core.ServiceLocator::Update()
extern void ServiceLocator_Update_m9180FF047442FD8551CE4ED4E49E8CB1CB68DC6E (void);
// 0x0000098A System.Void FrostweepGames.Plugins.Core.ServiceLocator::Dispose()
extern void ServiceLocator_Dispose_m9BA5A4816EE5318F7FC085A6EFC71ECFC8ACD45B (void);
// 0x0000098B T FrostweepGames.Plugins.Core.ServiceLocator::Get()
// 0x0000098C System.Void FrostweepGames.Plugins.Core.ServiceLocator::Register(FrostweepGames.Plugins.Core.IService)
// 0x0000098D System.String FrostweepGames.Plugins.GoogleCloud.SpeechRecognition.AudioConvert::Convert(UnityEngine.AudioClip,FrostweepGames.Plugins.GoogleCloud.SpeechRecognition.Enumerators/AudioEncoding,System.Boolean,System.Single)
extern void AudioConvert_Convert_m0BE689ACE77D444106FA6A8AAD1DCBE28CDC84C9 (void);
// 0x0000098E System.String FrostweepGames.Plugins.GoogleCloud.SpeechRecognition.AudioConvert::Convert(System.Single[],FrostweepGames.Plugins.GoogleCloud.SpeechRecognition.Enumerators/AudioEncoding,System.Boolean,System.Single)
extern void AudioConvert_Convert_m9E01A731D306D5B60B209A6CF68E56BFEC653231 (void);
// 0x0000098F UnityEngine.AudioClip FrostweepGames.Plugins.GoogleCloud.SpeechRecognition.AudioConvert::Convert(System.Single[],System.Int32,System.Int32)
extern void AudioConvert_Convert_m61A418186A735DD389B87AF58947CB13394EDE0F (void);
// 0x00000990 System.String FrostweepGames.Plugins.GoogleCloud.SpeechRecognition.AudioConvert::ToBase64(UnityEngine.AudioClip,FrostweepGames.Plugins.GoogleCloud.SpeechRecognition.Enumerators/AudioEncoding,System.Boolean,System.Single)
extern void AudioConvert_ToBase64_m38DE17AB682990B87286305304D89ACABCC9B43D (void);
// 0x00000991 System.String FrostweepGames.Plugins.GoogleCloud.SpeechRecognition.AudioConvert::ToBase64(System.Single[],FrostweepGames.Plugins.GoogleCloud.SpeechRecognition.Enumerators/AudioEncoding,System.Boolean,System.Single)
extern void AudioConvert_ToBase64_m1EFA2D1903D3310E98AA150093FCD6B434EC178A (void);
// 0x00000992 System.Byte[] FrostweepGames.Plugins.GoogleCloud.SpeechRecognition.AudioClip2ByteConverter::AudioClipToByte(UnityEngine.AudioClip,System.Boolean,System.Single)
extern void AudioClip2ByteConverter_AudioClipToByte_mF0E3FB11179718B0FCD8462437EBF4EC27A7F3DE (void);
// 0x00000993 System.Byte[] FrostweepGames.Plugins.GoogleCloud.SpeechRecognition.AudioClip2ByteConverter::FloatToByte(System.Single[])
extern void AudioClip2ByteConverter_FloatToByte_m883AE0C1BC06AC5F0CF011DAA6043F8F3C0E58B1 (void);
// 0x00000994 System.Single[] FrostweepGames.Plugins.GoogleCloud.SpeechRecognition.AudioClip2ByteConverter::ByteToFloat(System.Byte[])
extern void AudioClip2ByteConverter_ByteToFloat_mF9BE5138C719334C055ADF0339FA1C5C2571B232 (void);
// 0x00000995 System.Byte[] FrostweepGames.Plugins.GoogleCloud.SpeechRecognition.AudioClip2PCMConverter::AudioClip2PCM(UnityEngine.AudioClip)
extern void AudioClip2PCMConverter_AudioClip2PCM_mE4199111918BB7A3A857C02882906312C7629E94 (void);
// 0x00000996 UnityEngine.AudioClip FrostweepGames.Plugins.GoogleCloud.SpeechRecognition.AudioClip2PCMConverter::TrimSilence(UnityEngine.AudioClip,System.Single)
extern void AudioClip2PCMConverter_TrimSilence_mDE33833B305C3A199EFA2880F2DD9EE5809551BA (void);
// 0x00000997 UnityEngine.AudioClip FrostweepGames.Plugins.GoogleCloud.SpeechRecognition.AudioClip2PCMConverter::TrimSilence(System.Collections.Generic.List`1<System.Single>,System.Single,System.Int32,System.Int32)
extern void AudioClip2PCMConverter_TrimSilence_m8F22B63A57886111D474CCE709674131F17C9BB7 (void);
// 0x00000998 UnityEngine.AudioClip FrostweepGames.Plugins.GoogleCloud.SpeechRecognition.AudioClip2PCMConverter::TrimSilence(System.Collections.Generic.List`1<System.Single>,System.Single,System.Int32,System.Int32,System.Boolean)
extern void AudioClip2PCMConverter_TrimSilence_mA2C9FA82304F790ED8DF2BAC5FF2A2795CB179F2 (void);
// 0x00000999 System.IO.MemoryStream FrostweepGames.Plugins.GoogleCloud.SpeechRecognition.AudioClip2PCMConverter::CreateEmpty(System.IO.MemoryStream)
extern void AudioClip2PCMConverter_CreateEmpty_mD0D69BC5DF6C7FF7311E4C5842E28FC1DA54637D (void);
// 0x0000099A System.Void FrostweepGames.Plugins.GoogleCloud.SpeechRecognition.AudioClip2PCMConverter::ConvertAndWrite(System.IO.MemoryStream,UnityEngine.AudioClip)
extern void AudioClip2PCMConverter_ConvertAndWrite_mFEB9E5466D92517793273EFC23272EE3BAD53AC8 (void);
// 0x0000099B System.Void FrostweepGames.Plugins.GoogleCloud.SpeechRecognition.AudioClip2PCMConverter::WriteHeader(System.IO.MemoryStream,UnityEngine.AudioClip)
extern void AudioClip2PCMConverter_WriteHeader_m584BE7D22917ED5A27406EB49A6C9BF4185AA94C (void);
// 0x0000099C System.Byte[] FrostweepGames.Plugins.GoogleCloud.SpeechRecognition.AudioClipRaw2ByteConverter::AudioClipRawToByte(System.Single[],System.Boolean,System.Single)
extern void AudioClipRaw2ByteConverter_AudioClipRawToByte_mE8D083E7C2F15791C70052801BCE7BF2891D8C24 (void);
// 0x0000099D System.Byte[] FrostweepGames.Plugins.GoogleCloud.SpeechRecognition.AudioClipRaw2ByteConverter::FloatToByte(System.Single[])
extern void AudioClipRaw2ByteConverter_FloatToByte_m965DAA7503CAECF341854F5223DA8382FA637BB7 (void);
// 0x0000099E System.Single[] FrostweepGames.Plugins.GoogleCloud.SpeechRecognition.AudioClipRaw2ByteConverter::ByteToFloat(System.Byte[])
extern void AudioClipRaw2ByteConverter_ByteToFloat_m402A149029C53E2BC6D682604DC1CF0E6944F5AF (void);
// 0x0000099F System.Byte[] FrostweepGames.Plugins.GoogleCloud.SpeechRecognition.AudioClipRaw2PCMConverter::AudioClipRaw2PCM(System.Single[])
extern void AudioClipRaw2PCMConverter_AudioClipRaw2PCM_m1B0E998EF42828434DAA8F39C06BBBC791789078 (void);
// 0x000009A0 System.Single[] FrostweepGames.Plugins.GoogleCloud.SpeechRecognition.AudioClipRaw2PCMConverter::TrimSilence(System.Single[],System.Single)
extern void AudioClipRaw2PCMConverter_TrimSilence_mBE10D2BB3F13929BD071516470A905573B76D5CD (void);
// 0x000009A1 System.Single[] FrostweepGames.Plugins.GoogleCloud.SpeechRecognition.AudioClipRaw2PCMConverter::TrimSilence(System.Collections.Generic.List`1<System.Single>,System.Single,System.Int32,System.Int32)
extern void AudioClipRaw2PCMConverter_TrimSilence_m67FF8C2425084AA326CBFE58482595ED61869A68 (void);
// 0x000009A2 System.Single[] FrostweepGames.Plugins.GoogleCloud.SpeechRecognition.AudioClipRaw2PCMConverter::TrimSilence(System.Collections.Generic.List`1<System.Single>,System.Single,System.Int32,System.Int32,System.Boolean)
extern void AudioClipRaw2PCMConverter_TrimSilence_mFDD584F00CC92E7EFB874C8A8D15582E672FDFEB (void);
// 0x000009A3 System.IO.MemoryStream FrostweepGames.Plugins.GoogleCloud.SpeechRecognition.AudioClipRaw2PCMConverter::CreateEmpty(System.IO.MemoryStream)
extern void AudioClipRaw2PCMConverter_CreateEmpty_m50E48DCD192C8499D10F1A8F228922BC582CBE19 (void);
// 0x000009A4 System.Void FrostweepGames.Plugins.GoogleCloud.SpeechRecognition.AudioClipRaw2PCMConverter::ConvertAndWrite(System.IO.MemoryStream,System.Single[])
extern void AudioClipRaw2PCMConverter_ConvertAndWrite_m68E63BC075964FD909862BD55720E469711E6DF8 (void);
// 0x000009A5 System.Void FrostweepGames.Plugins.GoogleCloud.SpeechRecognition.AudioClipRaw2PCMConverter::WriteHeader(System.IO.MemoryStream)
extern void AudioClipRaw2PCMConverter_WriteHeader_m0AC76C27DDD7ADFFF7A8E268E75D2C0424A41278 (void);
// 0x000009A6 System.Void FrostweepGames.Plugins.GoogleCloud.SpeechRecognition.IMediaManager::add_MicrophoneDeviceSelectedEvent(System.Action)
// 0x000009A7 System.Void FrostweepGames.Plugins.GoogleCloud.SpeechRecognition.IMediaManager::remove_MicrophoneDeviceSelectedEvent(System.Action)
// 0x000009A8 System.Void FrostweepGames.Plugins.GoogleCloud.SpeechRecognition.IMediaManager::add_RecordStartedEvent(System.Action)
// 0x000009A9 System.Void FrostweepGames.Plugins.GoogleCloud.SpeechRecognition.IMediaManager::remove_RecordStartedEvent(System.Action)
// 0x000009AA System.Void FrostweepGames.Plugins.GoogleCloud.SpeechRecognition.IMediaManager::add_RecordFailedEvent(System.Action)
// 0x000009AB System.Void FrostweepGames.Plugins.GoogleCloud.SpeechRecognition.IMediaManager::remove_RecordFailedEvent(System.Action)
// 0x000009AC System.Void FrostweepGames.Plugins.GoogleCloud.SpeechRecognition.IMediaManager::add_RecordEndedEvent(System.Action`2<UnityEngine.AudioClip,System.Single[]>)
// 0x000009AD System.Void FrostweepGames.Plugins.GoogleCloud.SpeechRecognition.IMediaManager::remove_RecordEndedEvent(System.Action`2<UnityEngine.AudioClip,System.Single[]>)
// 0x000009AE System.Void FrostweepGames.Plugins.GoogleCloud.SpeechRecognition.IMediaManager::add_TalkBeganEvent(System.Action)
// 0x000009AF System.Void FrostweepGames.Plugins.GoogleCloud.SpeechRecognition.IMediaManager::remove_TalkBeganEvent(System.Action)
// 0x000009B0 System.Void FrostweepGames.Plugins.GoogleCloud.SpeechRecognition.IMediaManager::add_TalkEndedEvent(System.Action`2<UnityEngine.AudioClip,System.Single[]>)
// 0x000009B1 System.Void FrostweepGames.Plugins.GoogleCloud.SpeechRecognition.IMediaManager::remove_TalkEndedEvent(System.Action`2<UnityEngine.AudioClip,System.Single[]>)
// 0x000009B2 System.Boolean FrostweepGames.Plugins.GoogleCloud.SpeechRecognition.IMediaManager::get_IsRecording()
// 0x000009B3 System.String FrostweepGames.Plugins.GoogleCloud.SpeechRecognition.IMediaManager::get_MicrophoneDevice()
// 0x000009B4 UnityEngine.AudioClip FrostweepGames.Plugins.GoogleCloud.SpeechRecognition.IMediaManager::get_LastRecordedClip()
// 0x000009B5 System.Single[] FrostweepGames.Plugins.GoogleCloud.SpeechRecognition.IMediaManager::get_LastRecordedRaw()
// 0x000009B6 System.Boolean FrostweepGames.Plugins.GoogleCloud.SpeechRecognition.IMediaManager::get_DetectVoice()
// 0x000009B7 System.Single FrostweepGames.Plugins.GoogleCloud.SpeechRecognition.IMediaManager::GetLastFrame()
// 0x000009B8 System.Single FrostweepGames.Plugins.GoogleCloud.SpeechRecognition.IMediaManager::GetMaxFrame()
// 0x000009B9 System.Void FrostweepGames.Plugins.GoogleCloud.SpeechRecognition.IMediaManager::StartRecord(System.Boolean)
// 0x000009BA System.Void FrostweepGames.Plugins.GoogleCloud.SpeechRecognition.IMediaManager::StopRecord()
// 0x000009BB System.Boolean FrostweepGames.Plugins.GoogleCloud.SpeechRecognition.IMediaManager::ReadyToRecord()
// 0x000009BC System.Boolean FrostweepGames.Plugins.GoogleCloud.SpeechRecognition.IMediaManager::HasConnectedMicrophoneDevices()
// 0x000009BD System.Void FrostweepGames.Plugins.GoogleCloud.SpeechRecognition.IMediaManager::SetMicrophoneDevice(System.String)
// 0x000009BE System.String[] FrostweepGames.Plugins.GoogleCloud.SpeechRecognition.IMediaManager::GetMicrophoneDevices()
// 0x000009BF System.Void FrostweepGames.Plugins.GoogleCloud.SpeechRecognition.IMediaManager::SaveLastRecordedAudioClip(System.String)
// 0x000009C0 System.Collections.IEnumerator FrostweepGames.Plugins.GoogleCloud.SpeechRecognition.IMediaManager::OneTimeRecord(System.Int32,System.Action`1<System.Single[]>,System.Int32)
// 0x000009C1 System.Boolean FrostweepGames.Plugins.GoogleCloud.SpeechRecognition.IMediaManager::HasMicrophonePermission()
// 0x000009C2 System.Void FrostweepGames.Plugins.GoogleCloud.SpeechRecognition.IMediaManager::RequestMicrophonePermission(System.Action`1<System.Boolean>)
// 0x000009C3 System.Void FrostweepGames.Plugins.GoogleCloud.SpeechRecognition.ISpeechRecognitionManager::add_RecognizeSuccessEvent(System.Action`1<FrostweepGames.Plugins.GoogleCloud.SpeechRecognition.RecognitionResponse>)
// 0x000009C4 System.Void FrostweepGames.Plugins.GoogleCloud.SpeechRecognition.ISpeechRecognitionManager::remove_RecognizeSuccessEvent(System.Action`1<FrostweepGames.Plugins.GoogleCloud.SpeechRecognition.RecognitionResponse>)
// 0x000009C5 System.Void FrostweepGames.Plugins.GoogleCloud.SpeechRecognition.ISpeechRecognitionManager::add_RecognizeFailedEvent(System.Action`1<System.String>)
// 0x000009C6 System.Void FrostweepGames.Plugins.GoogleCloud.SpeechRecognition.ISpeechRecognitionManager::remove_RecognizeFailedEvent(System.Action`1<System.String>)
// 0x000009C7 System.Void FrostweepGames.Plugins.GoogleCloud.SpeechRecognition.ISpeechRecognitionManager::add_LongRunningRecognizeSuccessEvent(System.Action`1<FrostweepGames.Plugins.GoogleCloud.SpeechRecognition.Operation>)
// 0x000009C8 System.Void FrostweepGames.Plugins.GoogleCloud.SpeechRecognition.ISpeechRecognitionManager::remove_LongRunningRecognizeSuccessEvent(System.Action`1<FrostweepGames.Plugins.GoogleCloud.SpeechRecognition.Operation>)
// 0x000009C9 System.Void FrostweepGames.Plugins.GoogleCloud.SpeechRecognition.ISpeechRecognitionManager::add_LongRunningRecognizeFailedEvent(System.Action`1<System.String>)
// 0x000009CA System.Void FrostweepGames.Plugins.GoogleCloud.SpeechRecognition.ISpeechRecognitionManager::remove_LongRunningRecognizeFailedEvent(System.Action`1<System.String>)
// 0x000009CB System.Void FrostweepGames.Plugins.GoogleCloud.SpeechRecognition.ISpeechRecognitionManager::add_GetOperationSuccessEvent(System.Action`1<FrostweepGames.Plugins.GoogleCloud.SpeechRecognition.Operation>)
// 0x000009CC System.Void FrostweepGames.Plugins.GoogleCloud.SpeechRecognition.ISpeechRecognitionManager::remove_GetOperationSuccessEvent(System.Action`1<FrostweepGames.Plugins.GoogleCloud.SpeechRecognition.Operation>)
// 0x000009CD System.Void FrostweepGames.Plugins.GoogleCloud.SpeechRecognition.ISpeechRecognitionManager::add_GetOperationFailedEvent(System.Action`1<System.String>)
// 0x000009CE System.Void FrostweepGames.Plugins.GoogleCloud.SpeechRecognition.ISpeechRecognitionManager::remove_GetOperationFailedEvent(System.Action`1<System.String>)
// 0x000009CF System.Void FrostweepGames.Plugins.GoogleCloud.SpeechRecognition.ISpeechRecognitionManager::add_ListOperationsSuccessEvent(System.Action`1<FrostweepGames.Plugins.GoogleCloud.SpeechRecognition.ListOperationsResponse>)
// 0x000009D0 System.Void FrostweepGames.Plugins.GoogleCloud.SpeechRecognition.ISpeechRecognitionManager::remove_ListOperationsSuccessEvent(System.Action`1<FrostweepGames.Plugins.GoogleCloud.SpeechRecognition.ListOperationsResponse>)
// 0x000009D1 System.Void FrostweepGames.Plugins.GoogleCloud.SpeechRecognition.ISpeechRecognitionManager::add_ListOperationsFailedEvent(System.Action`1<System.String>)
// 0x000009D2 System.Void FrostweepGames.Plugins.GoogleCloud.SpeechRecognition.ISpeechRecognitionManager::remove_ListOperationsFailedEvent(System.Action`1<System.String>)
// 0x000009D3 FrostweepGames.Plugins.GoogleCloud.SpeechRecognition.Config FrostweepGames.Plugins.GoogleCloud.SpeechRecognition.ISpeechRecognitionManager::get_CurrentConfig()
// 0x000009D4 System.Void FrostweepGames.Plugins.GoogleCloud.SpeechRecognition.ISpeechRecognitionManager::SetConfig(FrostweepGames.Plugins.GoogleCloud.SpeechRecognition.Config)
// 0x000009D5 System.Boolean FrostweepGames.Plugins.GoogleCloud.SpeechRecognition.ISpeechRecognitionManager::CancelRequest(System.Int64)
// 0x000009D6 System.Int32 FrostweepGames.Plugins.GoogleCloud.SpeechRecognition.ISpeechRecognitionManager::CancelAllRequests()
// 0x000009D7 System.Int64 FrostweepGames.Plugins.GoogleCloud.SpeechRecognition.ISpeechRecognitionManager::Recognize(FrostweepGames.Plugins.GoogleCloud.SpeechRecognition.GeneralRecognitionRequest)
// 0x000009D8 System.Int64 FrostweepGames.Plugins.GoogleCloud.SpeechRecognition.ISpeechRecognitionManager::LongRunningRecognize(FrostweepGames.Plugins.GoogleCloud.SpeechRecognition.GeneralRecognitionRequest)
// 0x000009D9 System.Int64 FrostweepGames.Plugins.GoogleCloud.SpeechRecognition.ISpeechRecognitionManager::GetOperation(System.String)
// 0x000009DA System.Int64 FrostweepGames.Plugins.GoogleCloud.SpeechRecognition.ISpeechRecognitionManager::GetListOperations(System.String,System.String,System.Int32,System.String)
// 0x000009DB System.Boolean FrostweepGames.Plugins.GoogleCloud.SpeechRecognition.IVoiceDetectionManager::HasDetectedVoice(System.Byte[])
// 0x000009DC System.Void FrostweepGames.Plugins.GoogleCloud.SpeechRecognition.IVoiceDetectionManager::DetectThreshold(System.Int32)
// 0x000009DD System.Void FrostweepGames.Plugins.GoogleCloud.SpeechRecognition.MediaManager::add_MicrophoneDeviceSelectedEvent(System.Action)
extern void MediaManager_add_MicrophoneDeviceSelectedEvent_mBDCA782D22DAC4CE129645DDE109898042128810 (void);
// 0x000009DE System.Void FrostweepGames.Plugins.GoogleCloud.SpeechRecognition.MediaManager::remove_MicrophoneDeviceSelectedEvent(System.Action)
extern void MediaManager_remove_MicrophoneDeviceSelectedEvent_mBFA0FE5113681EDDBD7DB69D66245A4A273B8C79 (void);
// 0x000009DF System.Void FrostweepGames.Plugins.GoogleCloud.SpeechRecognition.MediaManager::add_RecordStartedEvent(System.Action)
extern void MediaManager_add_RecordStartedEvent_mC72A2FE6FA96D71CFF84E114B09ADB73F8EFC8CD (void);
// 0x000009E0 System.Void FrostweepGames.Plugins.GoogleCloud.SpeechRecognition.MediaManager::remove_RecordStartedEvent(System.Action)
extern void MediaManager_remove_RecordStartedEvent_mBE7A3971DF8F0597F4F1835C566B1B907A404BCD (void);
// 0x000009E1 System.Void FrostweepGames.Plugins.GoogleCloud.SpeechRecognition.MediaManager::add_RecordFailedEvent(System.Action)
extern void MediaManager_add_RecordFailedEvent_m05DC2F6508C62813228E2CC2B91BA55F988922B3 (void);
// 0x000009E2 System.Void FrostweepGames.Plugins.GoogleCloud.SpeechRecognition.MediaManager::remove_RecordFailedEvent(System.Action)
extern void MediaManager_remove_RecordFailedEvent_mDFB5AB7CA2920CBE578F8C62498A571776E42FEE (void);
// 0x000009E3 System.Void FrostweepGames.Plugins.GoogleCloud.SpeechRecognition.MediaManager::add_RecordEndedEvent(System.Action`2<UnityEngine.AudioClip,System.Single[]>)
extern void MediaManager_add_RecordEndedEvent_m7A609FA404581B7FCAF691E965049568AE747433 (void);
// 0x000009E4 System.Void FrostweepGames.Plugins.GoogleCloud.SpeechRecognition.MediaManager::remove_RecordEndedEvent(System.Action`2<UnityEngine.AudioClip,System.Single[]>)
extern void MediaManager_remove_RecordEndedEvent_m922036246B3FCFBD2A00F3A9EDC9440607568B89 (void);
// 0x000009E5 System.Void FrostweepGames.Plugins.GoogleCloud.SpeechRecognition.MediaManager::add_TalkBeganEvent(System.Action)
extern void MediaManager_add_TalkBeganEvent_mC5EB58C994A4DAC47016563A0556D74E1A882F85 (void);
// 0x000009E6 System.Void FrostweepGames.Plugins.GoogleCloud.SpeechRecognition.MediaManager::remove_TalkBeganEvent(System.Action)
extern void MediaManager_remove_TalkBeganEvent_m48A188214C58E555D4DC6064B69EB747E4BF6580 (void);
// 0x000009E7 System.Void FrostweepGames.Plugins.GoogleCloud.SpeechRecognition.MediaManager::add_TalkEndedEvent(System.Action`2<UnityEngine.AudioClip,System.Single[]>)
extern void MediaManager_add_TalkEndedEvent_mC171C644C12332B02FB29CA9A6B48C9A6567C1A0 (void);
// 0x000009E8 System.Void FrostweepGames.Plugins.GoogleCloud.SpeechRecognition.MediaManager::remove_TalkEndedEvent(System.Action`2<UnityEngine.AudioClip,System.Single[]>)
extern void MediaManager_remove_TalkEndedEvent_mD8AD5E14CE594C03C68B177F335309BF7B5CB18A (void);
// 0x000009E9 System.Boolean FrostweepGames.Plugins.GoogleCloud.SpeechRecognition.MediaManager::get_IsRecording()
extern void MediaManager_get_IsRecording_mD4DB51FEF2296BAA2BF34401B77B247E94F11459 (void);
// 0x000009EA System.Void FrostweepGames.Plugins.GoogleCloud.SpeechRecognition.MediaManager::set_IsRecording(System.Boolean)
extern void MediaManager_set_IsRecording_m12853A0BB27B8D46F10ECEB8DD27D4B8A01D7A4F (void);
// 0x000009EB System.String FrostweepGames.Plugins.GoogleCloud.SpeechRecognition.MediaManager::get_MicrophoneDevice()
extern void MediaManager_get_MicrophoneDevice_m3EC3EAE913F75D5EB030BC401172D0817EB45166 (void);
// 0x000009EC System.Void FrostweepGames.Plugins.GoogleCloud.SpeechRecognition.MediaManager::set_MicrophoneDevice(System.String)
extern void MediaManager_set_MicrophoneDevice_mEECD1C79A964BEFEEEA0F363161FA1A1CCDD9B4D (void);
// 0x000009ED UnityEngine.AudioClip FrostweepGames.Plugins.GoogleCloud.SpeechRecognition.MediaManager::get_LastRecordedClip()
extern void MediaManager_get_LastRecordedClip_m0C9C5C85BAA1C538D3B109BDC99CAFECB338BC3A (void);
// 0x000009EE System.Void FrostweepGames.Plugins.GoogleCloud.SpeechRecognition.MediaManager::set_LastRecordedClip(UnityEngine.AudioClip)
extern void MediaManager_set_LastRecordedClip_m722C21F3BF8D4486B3083006AA58EB5E782CCF5C (void);
// 0x000009EF System.Single[] FrostweepGames.Plugins.GoogleCloud.SpeechRecognition.MediaManager::get_LastRecordedRaw()
extern void MediaManager_get_LastRecordedRaw_m2DB631C2A3682C25A127267F0620493A56965C1F (void);
// 0x000009F0 System.Void FrostweepGames.Plugins.GoogleCloud.SpeechRecognition.MediaManager::set_LastRecordedRaw(System.Single[])
extern void MediaManager_set_LastRecordedRaw_m36378B318B55AD4FBF9417B872ED2A8E9401AAC8 (void);
// 0x000009F1 System.Boolean FrostweepGames.Plugins.GoogleCloud.SpeechRecognition.MediaManager::get_DetectVoice()
extern void MediaManager_get_DetectVoice_m2DCBA429CBEBB92FE40C4A6215692D2AF9F6A6F2 (void);
// 0x000009F2 System.Void FrostweepGames.Plugins.GoogleCloud.SpeechRecognition.MediaManager::set_DetectVoice(System.Boolean)
extern void MediaManager_set_DetectVoice_m42358364B88649FE4B5771829823E231172BFED7 (void);
// 0x000009F3 System.Void FrostweepGames.Plugins.GoogleCloud.SpeechRecognition.MediaManager::Init()
extern void MediaManager_Init_m022FF4A330C69E356562E38073B29B7B77F75089 (void);
// 0x000009F4 System.Void FrostweepGames.Plugins.GoogleCloud.SpeechRecognition.MediaManager::Update()
extern void MediaManager_Update_m7AF8A5417187EEF51FE74901A4F856C415F8DDCE (void);
// 0x000009F5 System.Void FrostweepGames.Plugins.GoogleCloud.SpeechRecognition.MediaManager::Dispose()
extern void MediaManager_Dispose_m5EEC11DF681495D599AC09E1818AD63229E8A123 (void);
// 0x000009F6 System.Single FrostweepGames.Plugins.GoogleCloud.SpeechRecognition.MediaManager::GetLastFrame()
extern void MediaManager_GetLastFrame_mC4958E5AA601302570BA2773B0C92EA11C02F04F (void);
// 0x000009F7 System.Single FrostweepGames.Plugins.GoogleCloud.SpeechRecognition.MediaManager::GetMaxFrame()
extern void MediaManager_GetMaxFrame_mC1E2B2D0DAA71A2C181253C646D59B52BBECD22C (void);
// 0x000009F8 System.Void FrostweepGames.Plugins.GoogleCloud.SpeechRecognition.MediaManager::StartRecord(System.Boolean)
extern void MediaManager_StartRecord_m9AC386FF952FB68199E67FABE32BA3862F25BDA1 (void);
// 0x000009F9 System.Void FrostweepGames.Plugins.GoogleCloud.SpeechRecognition.MediaManager::StopRecord()
extern void MediaManager_StopRecord_m5497CBF6598749697303F9C84F57AD33B9F786C9 (void);
// 0x000009FA System.Boolean FrostweepGames.Plugins.GoogleCloud.SpeechRecognition.MediaManager::ReadyToRecord()
extern void MediaManager_ReadyToRecord_m6333FA7219570892BC7FA32A15673732B6680D8D (void);
// 0x000009FB System.Boolean FrostweepGames.Plugins.GoogleCloud.SpeechRecognition.MediaManager::HasConnectedMicrophoneDevices()
extern void MediaManager_HasConnectedMicrophoneDevices_mAE636DFBCB48938C43E7A63852BB2CFD32B6287D (void);
// 0x000009FC System.Void FrostweepGames.Plugins.GoogleCloud.SpeechRecognition.MediaManager::SetMicrophoneDevice(System.String)
extern void MediaManager_SetMicrophoneDevice_m17655C2DA8B3C7576F3D1AEE925B995324C23ECE (void);
// 0x000009FD System.String[] FrostweepGames.Plugins.GoogleCloud.SpeechRecognition.MediaManager::GetMicrophoneDevices()
extern void MediaManager_GetMicrophoneDevices_mE7B5FEB23C6717F3837CAD06C02E3609C2108B8D (void);
// 0x000009FE System.Void FrostweepGames.Plugins.GoogleCloud.SpeechRecognition.MediaManager::SaveLastRecordedAudioClip(System.String)
extern void MediaManager_SaveLastRecordedAudioClip_m3DBD62B88E76E0F1E987E1106281BCE46CB43130 (void);
// 0x000009FF System.Collections.IEnumerator FrostweepGames.Plugins.GoogleCloud.SpeechRecognition.MediaManager::OneTimeRecord(System.Int32,System.Action`1<System.Single[]>,System.Int32)
extern void MediaManager_OneTimeRecord_m03ED788E4E450077CB431904429F1BF282FE5CBB (void);
// 0x00000A00 System.Boolean FrostweepGames.Plugins.GoogleCloud.SpeechRecognition.MediaManager::HasMicrophonePermission()
extern void MediaManager_HasMicrophonePermission_mA5378EA715F72133A85F693AE1F6D8120D7AD86D (void);
// 0x00000A01 System.Void FrostweepGames.Plugins.GoogleCloud.SpeechRecognition.MediaManager::RequestMicrophonePermission(System.Action`1<System.Boolean>)
extern void MediaManager_RequestMicrophonePermission_m8BBA9468404C1EEA8CACF326CB3A41E93E7AF12A (void);
// 0x00000A02 System.Void FrostweepGames.Plugins.GoogleCloud.SpeechRecognition.MediaManager::AddAudioSamplesIntoBuffer()
extern void MediaManager_AddAudioSamplesIntoBuffer_m59879B93EA5C0F8F922157CB83DE86513DE74CC6 (void);
// 0x00000A03 System.Void FrostweepGames.Plugins.GoogleCloud.SpeechRecognition.MediaManager::.ctor()
extern void MediaManager__ctor_m923D56FD2923AA74F9090974B66C7B1E7E35A08B (void);
// 0x00000A04 System.Void FrostweepGames.Plugins.GoogleCloud.SpeechRecognition.MediaManager/<OneTimeRecord>d__60::.ctor(System.Int32)
extern void U3COneTimeRecordU3Ed__60__ctor_m767730738666BEEF59C9947847EE22EC3A7BA61B (void);
// 0x00000A05 System.Void FrostweepGames.Plugins.GoogleCloud.SpeechRecognition.MediaManager/<OneTimeRecord>d__60::System.IDisposable.Dispose()
extern void U3COneTimeRecordU3Ed__60_System_IDisposable_Dispose_m10962C45219BFCCD43762918AF56AFC6BA1254E0 (void);
// 0x00000A06 System.Boolean FrostweepGames.Plugins.GoogleCloud.SpeechRecognition.MediaManager/<OneTimeRecord>d__60::MoveNext()
extern void U3COneTimeRecordU3Ed__60_MoveNext_mDBC85F3695D5AE2B81BB234A85BDFF08F4CC8BC3 (void);
// 0x00000A07 System.Object FrostweepGames.Plugins.GoogleCloud.SpeechRecognition.MediaManager/<OneTimeRecord>d__60::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3COneTimeRecordU3Ed__60_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m1C168B46157F5A49E172A6FC4530784B90F2421C (void);
// 0x00000A08 System.Void FrostweepGames.Plugins.GoogleCloud.SpeechRecognition.MediaManager/<OneTimeRecord>d__60::System.Collections.IEnumerator.Reset()
extern void U3COneTimeRecordU3Ed__60_System_Collections_IEnumerator_Reset_m612582A550C3B59B7362328BB04C2EADADA40038 (void);
// 0x00000A09 System.Object FrostweepGames.Plugins.GoogleCloud.SpeechRecognition.MediaManager/<OneTimeRecord>d__60::System.Collections.IEnumerator.get_Current()
extern void U3COneTimeRecordU3Ed__60_System_Collections_IEnumerator_get_Current_mBC123C49AAC9ADA5217ED8AAB1D8BBCD9C30C93D (void);
// 0x00000A0A System.Void FrostweepGames.Plugins.GoogleCloud.SpeechRecognition.SpeechRecognitionManager::add_RecognizeSuccessEvent(System.Action`1<FrostweepGames.Plugins.GoogleCloud.SpeechRecognition.RecognitionResponse>)
extern void SpeechRecognitionManager_add_RecognizeSuccessEvent_mBB07C04FB6E048E4EBB07678912D5E9309D461E6 (void);
// 0x00000A0B System.Void FrostweepGames.Plugins.GoogleCloud.SpeechRecognition.SpeechRecognitionManager::remove_RecognizeSuccessEvent(System.Action`1<FrostweepGames.Plugins.GoogleCloud.SpeechRecognition.RecognitionResponse>)
extern void SpeechRecognitionManager_remove_RecognizeSuccessEvent_mC541018CDA519C608F43F5A104BB3CE48B3AC6CE (void);
// 0x00000A0C System.Void FrostweepGames.Plugins.GoogleCloud.SpeechRecognition.SpeechRecognitionManager::add_RecognizeFailedEvent(System.Action`1<System.String>)
extern void SpeechRecognitionManager_add_RecognizeFailedEvent_mAA5AA910551FBA11921D5D3BAA7B397A0CDDC872 (void);
// 0x00000A0D System.Void FrostweepGames.Plugins.GoogleCloud.SpeechRecognition.SpeechRecognitionManager::remove_RecognizeFailedEvent(System.Action`1<System.String>)
extern void SpeechRecognitionManager_remove_RecognizeFailedEvent_m95FAB18CC327307098909FE5642982A3C7E79E01 (void);
// 0x00000A0E System.Void FrostweepGames.Plugins.GoogleCloud.SpeechRecognition.SpeechRecognitionManager::add_LongRunningRecognizeSuccessEvent(System.Action`1<FrostweepGames.Plugins.GoogleCloud.SpeechRecognition.Operation>)
extern void SpeechRecognitionManager_add_LongRunningRecognizeSuccessEvent_mF6315C0537F004A93CF3F653CAFCE996C0A63BE7 (void);
// 0x00000A0F System.Void FrostweepGames.Plugins.GoogleCloud.SpeechRecognition.SpeechRecognitionManager::remove_LongRunningRecognizeSuccessEvent(System.Action`1<FrostweepGames.Plugins.GoogleCloud.SpeechRecognition.Operation>)
extern void SpeechRecognitionManager_remove_LongRunningRecognizeSuccessEvent_m0F4B4D498593548A1426D0FFC1D165EF8982995F (void);
// 0x00000A10 System.Void FrostweepGames.Plugins.GoogleCloud.SpeechRecognition.SpeechRecognitionManager::add_LongRunningRecognizeFailedEvent(System.Action`1<System.String>)
extern void SpeechRecognitionManager_add_LongRunningRecognizeFailedEvent_mD23ECBD99E23BB0C5C6476F165C424FC5F6283AC (void);
// 0x00000A11 System.Void FrostweepGames.Plugins.GoogleCloud.SpeechRecognition.SpeechRecognitionManager::remove_LongRunningRecognizeFailedEvent(System.Action`1<System.String>)
extern void SpeechRecognitionManager_remove_LongRunningRecognizeFailedEvent_m6A917296094580EC8ED6CE85EC92F8E0D63D2AC8 (void);
// 0x00000A12 System.Void FrostweepGames.Plugins.GoogleCloud.SpeechRecognition.SpeechRecognitionManager::add_GetOperationSuccessEvent(System.Action`1<FrostweepGames.Plugins.GoogleCloud.SpeechRecognition.Operation>)
extern void SpeechRecognitionManager_add_GetOperationSuccessEvent_m9F7EE30DD1204967982044D6789F59993245AC06 (void);
// 0x00000A13 System.Void FrostweepGames.Plugins.GoogleCloud.SpeechRecognition.SpeechRecognitionManager::remove_GetOperationSuccessEvent(System.Action`1<FrostweepGames.Plugins.GoogleCloud.SpeechRecognition.Operation>)
extern void SpeechRecognitionManager_remove_GetOperationSuccessEvent_m590D09EB10FD81B7B6F865CDE0C529364AF88DAD (void);
// 0x00000A14 System.Void FrostweepGames.Plugins.GoogleCloud.SpeechRecognition.SpeechRecognitionManager::add_GetOperationFailedEvent(System.Action`1<System.String>)
extern void SpeechRecognitionManager_add_GetOperationFailedEvent_mA6B184F8F5C6D50423854FC7EC60236D2A29319C (void);
// 0x00000A15 System.Void FrostweepGames.Plugins.GoogleCloud.SpeechRecognition.SpeechRecognitionManager::remove_GetOperationFailedEvent(System.Action`1<System.String>)
extern void SpeechRecognitionManager_remove_GetOperationFailedEvent_m32CD1833E92C0308ED5FD5E3A5FD8700A9937A55 (void);
// 0x00000A16 System.Void FrostweepGames.Plugins.GoogleCloud.SpeechRecognition.SpeechRecognitionManager::add_ListOperationsSuccessEvent(System.Action`1<FrostweepGames.Plugins.GoogleCloud.SpeechRecognition.ListOperationsResponse>)
extern void SpeechRecognitionManager_add_ListOperationsSuccessEvent_mC03B5AD32C7C6BDDEB7817D4F5C8C567B3EAC1E7 (void);
// 0x00000A17 System.Void FrostweepGames.Plugins.GoogleCloud.SpeechRecognition.SpeechRecognitionManager::remove_ListOperationsSuccessEvent(System.Action`1<FrostweepGames.Plugins.GoogleCloud.SpeechRecognition.ListOperationsResponse>)
extern void SpeechRecognitionManager_remove_ListOperationsSuccessEvent_m29594F33733F1206750E470DD81BCAA24FE7F7A7 (void);
// 0x00000A18 System.Void FrostweepGames.Plugins.GoogleCloud.SpeechRecognition.SpeechRecognitionManager::add_ListOperationsFailedEvent(System.Action`1<System.String>)
extern void SpeechRecognitionManager_add_ListOperationsFailedEvent_m3E9F14455C47B476A3CCEF6B45D3656B7B7DA122 (void);
// 0x00000A19 System.Void FrostweepGames.Plugins.GoogleCloud.SpeechRecognition.SpeechRecognitionManager::remove_ListOperationsFailedEvent(System.Action`1<System.String>)
extern void SpeechRecognitionManager_remove_ListOperationsFailedEvent_m0EECC71F4ED57EC61AB758012C1F8D987858C984 (void);
// 0x00000A1A FrostweepGames.Plugins.GoogleCloud.SpeechRecognition.Config FrostweepGames.Plugins.GoogleCloud.SpeechRecognition.SpeechRecognitionManager::get_CurrentConfig()
extern void SpeechRecognitionManager_get_CurrentConfig_m114549A51F09DFB5F0B5F79531B2027E11ECAB93 (void);
// 0x00000A1B System.Void FrostweepGames.Plugins.GoogleCloud.SpeechRecognition.SpeechRecognitionManager::set_CurrentConfig(FrostweepGames.Plugins.GoogleCloud.SpeechRecognition.Config)
extern void SpeechRecognitionManager_set_CurrentConfig_m6330B2C55F296734ED7176892C31ABF98754B5CB (void);
// 0x00000A1C System.Void FrostweepGames.Plugins.GoogleCloud.SpeechRecognition.SpeechRecognitionManager::Init()
extern void SpeechRecognitionManager_Init_mF63AE77D5AF0D4002E71B309D4C4E82F8A134198 (void);
// 0x00000A1D System.Void FrostweepGames.Plugins.GoogleCloud.SpeechRecognition.SpeechRecognitionManager::Update()
extern void SpeechRecognitionManager_Update_mB5AC5ADD766E6ACBA5CD0DAD9BCC6E70D3D7624E (void);
// 0x00000A1E System.Void FrostweepGames.Plugins.GoogleCloud.SpeechRecognition.SpeechRecognitionManager::Dispose()
extern void SpeechRecognitionManager_Dispose_mEA667B1B9DC62BC648E5F86616FF5272A1FDDAD1 (void);
// 0x00000A1F System.Void FrostweepGames.Plugins.GoogleCloud.SpeechRecognition.SpeechRecognitionManager::SetConfig(FrostweepGames.Plugins.GoogleCloud.SpeechRecognition.Config)
extern void SpeechRecognitionManager_SetConfig_m7F5208473F3B97420BE64257DC8A1F4674B909A7 (void);
// 0x00000A20 System.Boolean FrostweepGames.Plugins.GoogleCloud.SpeechRecognition.SpeechRecognitionManager::CancelRequest(System.Int64)
extern void SpeechRecognitionManager_CancelRequest_m2766F4A2EF2E837229B1461DE5DEF60CDD51D56C (void);
// 0x00000A21 System.Int32 FrostweepGames.Plugins.GoogleCloud.SpeechRecognition.SpeechRecognitionManager::CancelAllRequests()
extern void SpeechRecognitionManager_CancelAllRequests_mFA10F11C015E91FF90071BF7EABC19DF5A749EC8 (void);
// 0x00000A22 System.Int64 FrostweepGames.Plugins.GoogleCloud.SpeechRecognition.SpeechRecognitionManager::Recognize(FrostweepGames.Plugins.GoogleCloud.SpeechRecognition.GeneralRecognitionRequest)
extern void SpeechRecognitionManager_Recognize_m9576C4138DBD259AA8B9F137D06819477BC73251 (void);
// 0x00000A23 System.Int64 FrostweepGames.Plugins.GoogleCloud.SpeechRecognition.SpeechRecognitionManager::LongRunningRecognize(FrostweepGames.Plugins.GoogleCloud.SpeechRecognition.GeneralRecognitionRequest)
extern void SpeechRecognitionManager_LongRunningRecognize_m2D679C310C2B79A19C4F3EAC468DFC75585A99C1 (void);
// 0x00000A24 System.Int64 FrostweepGames.Plugins.GoogleCloud.SpeechRecognition.SpeechRecognitionManager::GetOperation(System.String)
extern void SpeechRecognitionManager_GetOperation_m3B6650BD52A2707477EE17CF14E18FB23EBFBF2C (void);
// 0x00000A25 System.Int64 FrostweepGames.Plugins.GoogleCloud.SpeechRecognition.SpeechRecognitionManager::GetListOperations(System.String,System.String,System.Int32,System.String)
extern void SpeechRecognitionManager_GetListOperations_m7C94087AF5BD417EC07C6317C18932A477DFA51A (void);
// 0x00000A26 System.String FrostweepGames.Plugins.GoogleCloud.SpeechRecognition.SpeechRecognitionManager::GetAPiRouteEnd(System.String)
extern void SpeechRecognitionManager_GetAPiRouteEnd_mDB855BF6FD6353655EA83E41D5888939EF26F0C7 (void);
// 0x00000A27 System.Void FrostweepGames.Plugins.GoogleCloud.SpeechRecognition.SpeechRecognitionManager::NetworkResponseEventHandler(FrostweepGames.Plugins.Networking.NetworkResponse)
extern void SpeechRecognitionManager_NetworkResponseEventHandler_mCCBD6405806A8F3B64DF4B2D587642559D144F3A (void);
// 0x00000A28 System.Void FrostweepGames.Plugins.GoogleCloud.SpeechRecognition.SpeechRecognitionManager::.ctor()
extern void SpeechRecognitionManager__ctor_m46D0F0008C3E55D48D1C22942F44F150B22DB978 (void);
// 0x00000A29 System.Void FrostweepGames.Plugins.GoogleCloud.SpeechRecognition.VoiceDetectionManager::Init()
extern void VoiceDetectionManager_Init_m900E6115BF1FA778E1CCF22307B28EF8A31C4C12 (void);
// 0x00000A2A System.Void FrostweepGames.Plugins.GoogleCloud.SpeechRecognition.VoiceDetectionManager::Dispose()
extern void VoiceDetectionManager_Dispose_mA367FDEC22572223DF92A2CA9BE9A89BD2CC0DDD (void);
// 0x00000A2B System.Void FrostweepGames.Plugins.GoogleCloud.SpeechRecognition.VoiceDetectionManager::Update()
extern void VoiceDetectionManager_Update_mACF28F72EB49A9D1DC68C9D56CB867AAA09CAB5F (void);
// 0x00000A2C System.Boolean FrostweepGames.Plugins.GoogleCloud.SpeechRecognition.VoiceDetectionManager::HasDetectedVoice(System.Byte[])
extern void VoiceDetectionManager_HasDetectedVoice_mD792A747B758C76391CFA94415E7012DB67AAD2F (void);
// 0x00000A2D System.Void FrostweepGames.Plugins.GoogleCloud.SpeechRecognition.VoiceDetectionManager::DetectThreshold(System.Int32)
extern void VoiceDetectionManager_DetectThreshold_m894274D88B184B39198F0D521B083EE85C4A81B8 (void);
// 0x00000A2E System.Boolean FrostweepGames.Plugins.GoogleCloud.SpeechRecognition.VoiceDetectionManager::ProcessData(System.Byte[])
extern void VoiceDetectionManager_ProcessData_m38F1C2C592D09EA264C4884AA08821668923069D (void);
// 0x00000A2F System.Void FrostweepGames.Plugins.GoogleCloud.SpeechRecognition.VoiceDetectionManager::.ctor()
extern void VoiceDetectionManager__ctor_m7B71327982A1DF9AC89C9CFA4836B6B1CF98F49A (void);
// 0x00000A30 System.Void FrostweepGames.Plugins.GoogleCloud.SpeechRecognition.VoiceDetectionManager::<DetectThreshold>b__8_0(System.Single[])
extern void VoiceDetectionManager_U3CDetectThresholdU3Eb__8_0_m95A05494CEBE71D34B5899F4A1BDA4F8681F8A1A (void);
// 0x00000A31 System.Void FrostweepGames.Plugins.GoogleCloud.SpeechRecognition.RecognitionAudio::.ctor()
extern void RecognitionAudio__ctor_m300275B0F2EC826B2A44A2985A60555F829F8683 (void);
// 0x00000A32 System.Void FrostweepGames.Plugins.GoogleCloud.SpeechRecognition.RecognitionAudioContent::.ctor()
extern void RecognitionAudioContent__ctor_m178C23D282AAF0412F17D6BE96F425685EE480AD (void);
// 0x00000A33 System.Void FrostweepGames.Plugins.GoogleCloud.SpeechRecognition.RecognitionAudioUri::.ctor()
extern void RecognitionAudioUri__ctor_m5243CCEACF66C41EAFDBD5ED3095FC1BBF23DEE6 (void);
// 0x00000A34 FrostweepGames.Plugins.GoogleCloud.SpeechRecognition.RecognitionConfig FrostweepGames.Plugins.GoogleCloud.SpeechRecognition.RecognitionConfig::GetDefault()
extern void RecognitionConfig_GetDefault_mEAD6AF2ED364A2692F9376465D2BEBBA20A24952 (void);
// 0x00000A35 System.Void FrostweepGames.Plugins.GoogleCloud.SpeechRecognition.RecognitionConfig::.ctor()
extern void RecognitionConfig__ctor_m6FC78B524C5F9AEE5166E1E1E89DFD6763D76F8C (void);
// 0x00000A36 System.Void FrostweepGames.Plugins.GoogleCloud.SpeechRecognition.SpeakerDiarizationConfig::.ctor()
extern void SpeakerDiarizationConfig__ctor_m972794F1A1B9E96CECB972E1E6EC4EBD9E40AE98 (void);
// 0x00000A37 System.Void FrostweepGames.Plugins.GoogleCloud.SpeechRecognition.RecognitionMetadata::.ctor()
extern void RecognitionMetadata__ctor_m935DC66A2E53BD77C842E2606359003C5C544BF5 (void);
// 0x00000A38 System.Void FrostweepGames.Plugins.GoogleCloud.SpeechRecognition.SpeechContext::.ctor()
extern void SpeechContext__ctor_m42EB93FD08B24EBA0E264DA2EC803DF6854C1161 (void);
// 0x00000A39 System.Void FrostweepGames.Plugins.GoogleCloud.SpeechRecognition.WordInfo::.ctor()
extern void WordInfo__ctor_m8587CC5EBB8401D0CBC36CA5907988B68241C433 (void);
// 0x00000A3A System.Void FrostweepGames.Plugins.GoogleCloud.SpeechRecognition.Operation::.ctor()
extern void Operation__ctor_m4F5254EB459339F7E0FC69684DADCE200E8FC874 (void);
// 0x00000A3B System.Void FrostweepGames.Plugins.GoogleCloud.SpeechRecognition.SpeechRecognitionAlternative::.ctor()
extern void SpeechRecognitionAlternative__ctor_mB63CED0286743B98B848FB211C7924B3C932742D (void);
// 0x00000A3C System.Void FrostweepGames.Plugins.GoogleCloud.SpeechRecognition.Status::.ctor()
extern void Status__ctor_mF7C4F5CFA15C88BADCE5BCAD2DFF3E3E08CA64BA (void);
// 0x00000A3D System.Void FrostweepGames.Plugins.GoogleCloud.SpeechRecognition.LongRunningRecognizeMetadata::.ctor()
extern void LongRunningRecognizeMetadata__ctor_m7A5D0B5DC07BD88C2458D0254C8835B4B2BBA94C (void);
// 0x00000A3E System.Void FrostweepGames.Plugins.GoogleCloud.SpeechRecognition.SpeechRecognitionResult::.ctor()
extern void SpeechRecognitionResult__ctor_mD1330B8865C543F36F66F82FA6E5B0CE8740ADF7 (void);
// 0x00000A3F System.Void FrostweepGames.Plugins.GoogleCloud.SpeechRecognition.RecognitionResponse::.ctor()
extern void RecognitionResponse__ctor_m0CCF1C2BBF328272E9A1E6F191F56A6B448895A8 (void);
// 0x00000A40 System.Void FrostweepGames.Plugins.GoogleCloud.SpeechRecognition.ListOperationsResponse::.ctor()
extern void ListOperationsResponse__ctor_mDC4160855C26FB648A33FF91DECE6604323C44AA (void);
// 0x00000A41 System.Void FrostweepGames.Plugins.GoogleCloud.SpeechRecognition.GeneralRecognitionRequest::.ctor()
extern void GeneralRecognitionRequest__ctor_m7DE9899BE1807545C07C2D79DE40FD210B0B5E28 (void);
// 0x00000A42 System.Void FrostweepGames.Plugins.GoogleCloud.SpeechRecognition.Config::.ctor()
extern void Config__ctor_m7E54D5E7C4DBFAF085C859C876DDCE440054689A (void);
// 0x00000A43 System.Void FrostweepGames.Plugins.GoogleCloud.SpeechRecognition.Constants::.ctor()
extern void Constants__ctor_m3E42F34691D5E1DCB6D69821252998F0157633EA (void);
// 0x00000A44 System.String FrostweepGames.Plugins.GoogleCloud.SpeechRecognition.Enumerators::Parse(System.Enum,System.Char,System.Char)
extern void Enumerators_Parse_m78E1729FB02BFA264E7DEF16868D50926EAD9E58 (void);
// 0x00000A45 FrostweepGames.Plugins.GoogleCloud.SpeechRecognition.GCSpeechRecognition FrostweepGames.Plugins.GoogleCloud.SpeechRecognition.GCSpeechRecognition::get_Instance()
extern void GCSpeechRecognition_get_Instance_m992BC8241EBB7B35B149B2A3F18B5F9ECBEE64B1 (void);
// 0x00000A46 System.Void FrostweepGames.Plugins.GoogleCloud.SpeechRecognition.GCSpeechRecognition::add_RecognizeSuccessEvent(System.Action`1<FrostweepGames.Plugins.GoogleCloud.SpeechRecognition.RecognitionResponse>)
extern void GCSpeechRecognition_add_RecognizeSuccessEvent_m176E51F4DEF849A46FBA13C55080741DDD71B1EF (void);
// 0x00000A47 System.Void FrostweepGames.Plugins.GoogleCloud.SpeechRecognition.GCSpeechRecognition::remove_RecognizeSuccessEvent(System.Action`1<FrostweepGames.Plugins.GoogleCloud.SpeechRecognition.RecognitionResponse>)
extern void GCSpeechRecognition_remove_RecognizeSuccessEvent_mB65C27268776BB3A214C28F1AF356A22DD4F5704 (void);
// 0x00000A48 System.Void FrostweepGames.Plugins.GoogleCloud.SpeechRecognition.GCSpeechRecognition::add_RecognizeFailedEvent(System.Action`1<System.String>)
extern void GCSpeechRecognition_add_RecognizeFailedEvent_m9D16C98DE295EE5C7525DCFBC0858923E26EDBED (void);
// 0x00000A49 System.Void FrostweepGames.Plugins.GoogleCloud.SpeechRecognition.GCSpeechRecognition::remove_RecognizeFailedEvent(System.Action`1<System.String>)
extern void GCSpeechRecognition_remove_RecognizeFailedEvent_mB1A386C34CD6EBB13C0E0E8D6B23A3C5F69727B8 (void);
// 0x00000A4A System.Void FrostweepGames.Plugins.GoogleCloud.SpeechRecognition.GCSpeechRecognition::add_LongRunningRecognizeSuccessEvent(System.Action`1<FrostweepGames.Plugins.GoogleCloud.SpeechRecognition.Operation>)
extern void GCSpeechRecognition_add_LongRunningRecognizeSuccessEvent_m8600A85EB72F51C6437ED641C6E96EB3F3B1002F (void);
// 0x00000A4B System.Void FrostweepGames.Plugins.GoogleCloud.SpeechRecognition.GCSpeechRecognition::remove_LongRunningRecognizeSuccessEvent(System.Action`1<FrostweepGames.Plugins.GoogleCloud.SpeechRecognition.Operation>)
extern void GCSpeechRecognition_remove_LongRunningRecognizeSuccessEvent_m27182C58E5E962B4EF3EFE115DCAC9BB6849AB59 (void);
// 0x00000A4C System.Void FrostweepGames.Plugins.GoogleCloud.SpeechRecognition.GCSpeechRecognition::add_LongRunningRecognizeFailedEvent(System.Action`1<System.String>)
extern void GCSpeechRecognition_add_LongRunningRecognizeFailedEvent_m3FC5A7B37115C02C310601915A6985CD66E1007B (void);
// 0x00000A4D System.Void FrostweepGames.Plugins.GoogleCloud.SpeechRecognition.GCSpeechRecognition::remove_LongRunningRecognizeFailedEvent(System.Action`1<System.String>)
extern void GCSpeechRecognition_remove_LongRunningRecognizeFailedEvent_mF487C6389C824DB71F77394259911FA93B6F6441 (void);
// 0x00000A4E System.Void FrostweepGames.Plugins.GoogleCloud.SpeechRecognition.GCSpeechRecognition::add_GetOperationSuccessEvent(System.Action`1<FrostweepGames.Plugins.GoogleCloud.SpeechRecognition.Operation>)
extern void GCSpeechRecognition_add_GetOperationSuccessEvent_m14BD1A06A76F112D4845E133D13F9EB84D2B9E3B (void);
// 0x00000A4F System.Void FrostweepGames.Plugins.GoogleCloud.SpeechRecognition.GCSpeechRecognition::remove_GetOperationSuccessEvent(System.Action`1<FrostweepGames.Plugins.GoogleCloud.SpeechRecognition.Operation>)
extern void GCSpeechRecognition_remove_GetOperationSuccessEvent_mAF98F2D84B4D44CADFFCFAF55546A166F76B937B (void);
// 0x00000A50 System.Void FrostweepGames.Plugins.GoogleCloud.SpeechRecognition.GCSpeechRecognition::add_GetOperationFailedEvent(System.Action`1<System.String>)
extern void GCSpeechRecognition_add_GetOperationFailedEvent_mDCC71F0C76903666A814311D53BB80EE5B7EA04B (void);
// 0x00000A51 System.Void FrostweepGames.Plugins.GoogleCloud.SpeechRecognition.GCSpeechRecognition::remove_GetOperationFailedEvent(System.Action`1<System.String>)
extern void GCSpeechRecognition_remove_GetOperationFailedEvent_mBF3C6EDE9FDA16EA677CFB351F9B8F89A64C8615 (void);
// 0x00000A52 System.Void FrostweepGames.Plugins.GoogleCloud.SpeechRecognition.GCSpeechRecognition::add_ListOperationsSuccessEvent(System.Action`1<FrostweepGames.Plugins.GoogleCloud.SpeechRecognition.ListOperationsResponse>)
extern void GCSpeechRecognition_add_ListOperationsSuccessEvent_m56D1CBB30E0A16969C94919CFBAC0D11FED45713 (void);
// 0x00000A53 System.Void FrostweepGames.Plugins.GoogleCloud.SpeechRecognition.GCSpeechRecognition::remove_ListOperationsSuccessEvent(System.Action`1<FrostweepGames.Plugins.GoogleCloud.SpeechRecognition.ListOperationsResponse>)
extern void GCSpeechRecognition_remove_ListOperationsSuccessEvent_m422EA8BAD459BC74A18BF5DC7722A129D9DD46B9 (void);
// 0x00000A54 System.Void FrostweepGames.Plugins.GoogleCloud.SpeechRecognition.GCSpeechRecognition::add_ListOperationsFailedEvent(System.Action`1<System.String>)
extern void GCSpeechRecognition_add_ListOperationsFailedEvent_m2626560FCC9830C0D1138124EDAF995B84A49296 (void);
// 0x00000A55 System.Void FrostweepGames.Plugins.GoogleCloud.SpeechRecognition.GCSpeechRecognition::remove_ListOperationsFailedEvent(System.Action`1<System.String>)
extern void GCSpeechRecognition_remove_ListOperationsFailedEvent_mFCB729EAAD791931C2334249FFF0F693AEC3A3EF (void);
// 0x00000A56 System.Void FrostweepGames.Plugins.GoogleCloud.SpeechRecognition.GCSpeechRecognition::add_StartedRecordEvent(System.Action)
extern void GCSpeechRecognition_add_StartedRecordEvent_m9B107FAA56BB6401706279C11D9798ABA5B0AB96 (void);
// 0x00000A57 System.Void FrostweepGames.Plugins.GoogleCloud.SpeechRecognition.GCSpeechRecognition::remove_StartedRecordEvent(System.Action)
extern void GCSpeechRecognition_remove_StartedRecordEvent_m02579D1CC27D76539B15185A1ED2C735F53D40A0 (void);
// 0x00000A58 System.Void FrostweepGames.Plugins.GoogleCloud.SpeechRecognition.GCSpeechRecognition::add_FinishedRecordEvent(System.Action`2<UnityEngine.AudioClip,System.Single[]>)
extern void GCSpeechRecognition_add_FinishedRecordEvent_m479FDEEAFDA462934F06EE0B0697B9AF795795FA (void);
// 0x00000A59 System.Void FrostweepGames.Plugins.GoogleCloud.SpeechRecognition.GCSpeechRecognition::remove_FinishedRecordEvent(System.Action`2<UnityEngine.AudioClip,System.Single[]>)
extern void GCSpeechRecognition_remove_FinishedRecordEvent_m6C8DF51361C45C2C811C684B05C17FFFC20CB159 (void);
// 0x00000A5A System.Void FrostweepGames.Plugins.GoogleCloud.SpeechRecognition.GCSpeechRecognition::add_RecordFailedEvent(System.Action)
extern void GCSpeechRecognition_add_RecordFailedEvent_mE8ED5B3FFAAB782B50453D5F42D1B590B5CF6352 (void);
// 0x00000A5B System.Void FrostweepGames.Plugins.GoogleCloud.SpeechRecognition.GCSpeechRecognition::remove_RecordFailedEvent(System.Action)
extern void GCSpeechRecognition_remove_RecordFailedEvent_mA9C7E5052D47E93DF3C60ABFFFAC5D629D936932 (void);
// 0x00000A5C System.Void FrostweepGames.Plugins.GoogleCloud.SpeechRecognition.GCSpeechRecognition::add_BeginTalkigEvent(System.Action)
extern void GCSpeechRecognition_add_BeginTalkigEvent_mD40B76A1DB24B94CD62AE71934B6516F15D3B780 (void);
// 0x00000A5D System.Void FrostweepGames.Plugins.GoogleCloud.SpeechRecognition.GCSpeechRecognition::remove_BeginTalkigEvent(System.Action)
extern void GCSpeechRecognition_remove_BeginTalkigEvent_mD2456188394CD9A04DB429E35951A5DAD5319C52 (void);
// 0x00000A5E System.Void FrostweepGames.Plugins.GoogleCloud.SpeechRecognition.GCSpeechRecognition::add_EndTalkigEvent(System.Action`2<UnityEngine.AudioClip,System.Single[]>)
extern void GCSpeechRecognition_add_EndTalkigEvent_m7CE4BA15142DB0FB0BA351A5C9FD12A5F1DB7A29 (void);
// 0x00000A5F System.Void FrostweepGames.Plugins.GoogleCloud.SpeechRecognition.GCSpeechRecognition::remove_EndTalkigEvent(System.Action`2<UnityEngine.AudioClip,System.Single[]>)
extern void GCSpeechRecognition_remove_EndTalkigEvent_mBD1462A1303BF03C098BF477B4F71A5750F33C23 (void);
// 0x00000A60 System.Boolean FrostweepGames.Plugins.GoogleCloud.SpeechRecognition.GCSpeechRecognition::get__IsCurrentInstance()
extern void GCSpeechRecognition_get__IsCurrentInstance_m8D27479DE87A48A6E5981FE42BF83749D2BE6118 (void);
// 0x00000A61 UnityEngine.AudioClip FrostweepGames.Plugins.GoogleCloud.SpeechRecognition.GCSpeechRecognition::get_LastRecordedClip()
extern void GCSpeechRecognition_get_LastRecordedClip_mA447B0E2C3A18DD515F3E54A325A6374DE7BFDE8 (void);
// 0x00000A62 System.Boolean FrostweepGames.Plugins.GoogleCloud.SpeechRecognition.GCSpeechRecognition::get_IsRecording()
extern void GCSpeechRecognition_get_IsRecording_mDFB40CFA9B9B605B5C943E0A88A9C509E1BF7CB0 (void);
// 0x00000A63 System.Single[] FrostweepGames.Plugins.GoogleCloud.SpeechRecognition.GCSpeechRecognition::get_LastRecordedRaw()
extern void GCSpeechRecognition_get_LastRecordedRaw_m3547ACBCF52A2DD93ED1114AB8CE0221A438D2C6 (void);
// 0x00000A64 System.Void FrostweepGames.Plugins.GoogleCloud.SpeechRecognition.GCSpeechRecognition::Awake()
extern void GCSpeechRecognition_Awake_mA2FA7B84FF5BA9257B09C02BDE3A8D85358EB8DF (void);
// 0x00000A65 System.Void FrostweepGames.Plugins.GoogleCloud.SpeechRecognition.GCSpeechRecognition::Update()
extern void GCSpeechRecognition_Update_m9EF09B13D7B11BD88E124B98FC55BBB7FA1974B1 (void);
// 0x00000A66 System.Void FrostweepGames.Plugins.GoogleCloud.SpeechRecognition.GCSpeechRecognition::OnDestroy()
extern void GCSpeechRecognition_OnDestroy_mA6BBA4D8E9DC3650C6F9A6B88AF7A2995A05B3C8 (void);
// 0x00000A67 System.Single FrostweepGames.Plugins.GoogleCloud.SpeechRecognition.GCSpeechRecognition::GetLastFrame()
extern void GCSpeechRecognition_GetLastFrame_m7F37448BC9DFFFF791B0B9A152BC7E837200122D (void);
// 0x00000A68 System.Single FrostweepGames.Plugins.GoogleCloud.SpeechRecognition.GCSpeechRecognition::GetMaxFrame()
extern void GCSpeechRecognition_GetMaxFrame_m30A8F98EF970E22469F292003EDCF3AB8D05ACB4 (void);
// 0x00000A69 System.Void FrostweepGames.Plugins.GoogleCloud.SpeechRecognition.GCSpeechRecognition::StartRecord(System.Boolean)
extern void GCSpeechRecognition_StartRecord_mF029CEF763BEA4AF6BD23EA541EDB642AA9282DA (void);
// 0x00000A6A System.Void FrostweepGames.Plugins.GoogleCloud.SpeechRecognition.GCSpeechRecognition::StopRecord()
extern void GCSpeechRecognition_StopRecord_m6215A032241C8367A02C90F70081CE5B32D91515 (void);
// 0x00000A6B System.Void FrostweepGames.Plugins.GoogleCloud.SpeechRecognition.GCSpeechRecognition::DetectThreshold()
extern void GCSpeechRecognition_DetectThreshold_m88ED82DD0FFEBF47C0C204A07AC1D3C47E6DEA56 (void);
// 0x00000A6C System.Boolean FrostweepGames.Plugins.GoogleCloud.SpeechRecognition.GCSpeechRecognition::ReadyToRecord()
extern void GCSpeechRecognition_ReadyToRecord_mF97690BF95A8C988F685510296868D3C4730A1F4 (void);
// 0x00000A6D System.String[] FrostweepGames.Plugins.GoogleCloud.SpeechRecognition.GCSpeechRecognition::GetMicrophoneDevices()
extern void GCSpeechRecognition_GetMicrophoneDevices_mBBC180D5872F990DA3B9BD487D14C48C8CF40DDD (void);
// 0x00000A6E System.Boolean FrostweepGames.Plugins.GoogleCloud.SpeechRecognition.GCSpeechRecognition::HasConnectedMicrophoneDevices()
extern void GCSpeechRecognition_HasConnectedMicrophoneDevices_m2A67F0E291995C0E4A1C1BFF1F0B3B1F9D9BFA3F (void);
// 0x00000A6F System.Void FrostweepGames.Plugins.GoogleCloud.SpeechRecognition.GCSpeechRecognition::SetMicrophoneDevice(System.String)
extern void GCSpeechRecognition_SetMicrophoneDevice_mD27AD241DEBDF575029D5FCEDA7F8386EB76920C (void);
// 0x00000A70 System.Void FrostweepGames.Plugins.GoogleCloud.SpeechRecognition.GCSpeechRecognition::SaveLastRecordedAudioClip(System.String)
extern void GCSpeechRecognition_SaveLastRecordedAudioClip_mC3D7668FD6CDF43AA9CFE27E356A33BDE282A5BB (void);
// 0x00000A71 System.Int64 FrostweepGames.Plugins.GoogleCloud.SpeechRecognition.GCSpeechRecognition::Recognize(FrostweepGames.Plugins.GoogleCloud.SpeechRecognition.GeneralRecognitionRequest)
extern void GCSpeechRecognition_Recognize_mB96763A5D5F871ECCE79610767299E2B78DD76AB (void);
// 0x00000A72 System.Int64 FrostweepGames.Plugins.GoogleCloud.SpeechRecognition.GCSpeechRecognition::LongRunningRecognize(FrostweepGames.Plugins.GoogleCloud.SpeechRecognition.GeneralRecognitionRequest)
extern void GCSpeechRecognition_LongRunningRecognize_m0FBC506DA48C621008299E105FE2F02129C64F33 (void);
// 0x00000A73 System.Int64 FrostweepGames.Plugins.GoogleCloud.SpeechRecognition.GCSpeechRecognition::GetOperation(System.String)
extern void GCSpeechRecognition_GetOperation_m1D5CB11AEDBFA515E566B9AA84E271B082A477BA (void);
// 0x00000A74 System.Int64 FrostweepGames.Plugins.GoogleCloud.SpeechRecognition.GCSpeechRecognition::GetListOperations(System.String,System.String,System.Int32,System.String)
extern void GCSpeechRecognition_GetListOperations_mED6D64C149BA835EC5A57ED63849E3B5C14DD489 (void);
// 0x00000A75 System.Boolean FrostweepGames.Plugins.GoogleCloud.SpeechRecognition.GCSpeechRecognition::CancelRequest(System.Int64)
extern void GCSpeechRecognition_CancelRequest_m6B8DBDBC3F7ED8041C560E8678D523383B1393B4 (void);
// 0x00000A76 System.Int32 FrostweepGames.Plugins.GoogleCloud.SpeechRecognition.GCSpeechRecognition::CancelAllRequests()
extern void GCSpeechRecognition_CancelAllRequests_mC2869270BAE693AA13B7A7A67CBE26C666F42A8A (void);
// 0x00000A77 System.Boolean FrostweepGames.Plugins.GoogleCloud.SpeechRecognition.GCSpeechRecognition::HasMicrophonePermission()
extern void GCSpeechRecognition_HasMicrophonePermission_m85D2BEB942C003EE1D5B81467D56A4DDB820879B (void);
// 0x00000A78 System.Void FrostweepGames.Plugins.GoogleCloud.SpeechRecognition.GCSpeechRecognition::RequestMicrophonePermission(System.Action`1<System.Boolean>)
extern void GCSpeechRecognition_RequestMicrophonePermission_m464E84B5F84151E5D289E42F65F0158034EA61A0 (void);
// 0x00000A79 System.Void FrostweepGames.Plugins.GoogleCloud.SpeechRecognition.GCSpeechRecognition::RecognizeSuccessEventHandler(FrostweepGames.Plugins.GoogleCloud.SpeechRecognition.RecognitionResponse)
extern void GCSpeechRecognition_RecognizeSuccessEventHandler_m29C63E569FFB2990E01742F8712047E3FA7A4600 (void);
// 0x00000A7A System.Void FrostweepGames.Plugins.GoogleCloud.SpeechRecognition.GCSpeechRecognition::LongRunningRecognizeSuccessEventHandler(FrostweepGames.Plugins.GoogleCloud.SpeechRecognition.Operation)
extern void GCSpeechRecognition_LongRunningRecognizeSuccessEventHandler_m68598F3FFA7AEEC573F302C74300B53C1BB24B2F (void);
// 0x00000A7B System.Void FrostweepGames.Plugins.GoogleCloud.SpeechRecognition.GCSpeechRecognition::RecognizeFailedEventHandler(System.String)
extern void GCSpeechRecognition_RecognizeFailedEventHandler_m97A593907EA7530EF493EBC572D40569E06AC4EA (void);
// 0x00000A7C System.Void FrostweepGames.Plugins.GoogleCloud.SpeechRecognition.GCSpeechRecognition::LongRunningRecognizeFailedEventHandler(System.String)
extern void GCSpeechRecognition_LongRunningRecognizeFailedEventHandler_m41681E1765CB02CA5837213C9485CE3F880C2476 (void);
// 0x00000A7D System.Void FrostweepGames.Plugins.GoogleCloud.SpeechRecognition.GCSpeechRecognition::RecordFailedEventHandler()
extern void GCSpeechRecognition_RecordFailedEventHandler_m04CB1705B84A26D04E012D8A39331777CCC42C09 (void);
// 0x00000A7E System.Void FrostweepGames.Plugins.GoogleCloud.SpeechRecognition.GCSpeechRecognition::TalkBeganEventHandler()
extern void GCSpeechRecognition_TalkBeganEventHandler_m079CAA9F2E13EA5CF70F8D3F4317CDE47F145B30 (void);
// 0x00000A7F System.Void FrostweepGames.Plugins.GoogleCloud.SpeechRecognition.GCSpeechRecognition::TalkEndedEventHandler(UnityEngine.AudioClip,System.Single[])
extern void GCSpeechRecognition_TalkEndedEventHandler_m7013C4A5DAD516A2CBD9A6E0975DCA53C5880155 (void);
// 0x00000A80 System.Void FrostweepGames.Plugins.GoogleCloud.SpeechRecognition.GCSpeechRecognition::RecordStartedEventHandler()
extern void GCSpeechRecognition_RecordStartedEventHandler_mEE0189401FDCDB1910712B87D045E2BF6B7E9F4C (void);
// 0x00000A81 System.Void FrostweepGames.Plugins.GoogleCloud.SpeechRecognition.GCSpeechRecognition::RecordEndedEventHandler(UnityEngine.AudioClip,System.Single[])
extern void GCSpeechRecognition_RecordEndedEventHandler_mA5ABD995B2A506689B38AF5A8EAF6D12A813349E (void);
// 0x00000A82 System.Void FrostweepGames.Plugins.GoogleCloud.SpeechRecognition.GCSpeechRecognition::GetOperationSuccessEventHandler(FrostweepGames.Plugins.GoogleCloud.SpeechRecognition.Operation)
extern void GCSpeechRecognition_GetOperationSuccessEventHandler_mE78894DDDF5507CF52EBCFDA06A60A20FF041F2B (void);
// 0x00000A83 System.Void FrostweepGames.Plugins.GoogleCloud.SpeechRecognition.GCSpeechRecognition::GetOperationFailedEventHandler(System.String)
extern void GCSpeechRecognition_GetOperationFailedEventHandler_mC855B586556981A7BE65256F5F69948528DA6052 (void);
// 0x00000A84 System.Void FrostweepGames.Plugins.GoogleCloud.SpeechRecognition.GCSpeechRecognition::ListOperationsSuccessEventHandler(FrostweepGames.Plugins.GoogleCloud.SpeechRecognition.ListOperationsResponse)
extern void GCSpeechRecognition_ListOperationsSuccessEventHandler_m3ECC62B298038B40BD5AB645B72AA36F47E1D0D0 (void);
// 0x00000A85 System.Void FrostweepGames.Plugins.GoogleCloud.SpeechRecognition.GCSpeechRecognition::ListOperationsFailedEventHandler(System.String)
extern void GCSpeechRecognition_ListOperationsFailedEventHandler_mD6ADE8C5C5AF26896EB65A1B2153F3701C2D37FB (void);
// 0x00000A86 System.Void FrostweepGames.Plugins.GoogleCloud.SpeechRecognition.GCSpeechRecognition::.ctor()
extern void GCSpeechRecognition__ctor_m01ED16574E7F7772516D1EEE43D32F152E676AA1 (void);
// 0x00000A87 System.Void FrostweepGames.Plugins.GoogleCloud.SpeechRecognition.Examples.GCSR_DoCommandsExample::Start()
extern void GCSR_DoCommandsExample_Start_mEF99F5979B93C36E57D8C16754C116066B30F488 (void);
// 0x00000A88 System.Void FrostweepGames.Plugins.GoogleCloud.SpeechRecognition.Examples.GCSR_DoCommandsExample::OnDestroy()
extern void GCSR_DoCommandsExample_OnDestroy_m22497CA4F5ACE24ED7B60F82D33E87944318D55A (void);
// 0x00000A89 System.Void FrostweepGames.Plugins.GoogleCloud.SpeechRecognition.Examples.GCSR_DoCommandsExample::StartRecordButtonOnClickHandler()
extern void GCSR_DoCommandsExample_StartRecordButtonOnClickHandler_m07EC4CFF3D3BB94B6121A811CAEC24993871B3CD (void);
// 0x00000A8A System.Void FrostweepGames.Plugins.GoogleCloud.SpeechRecognition.Examples.GCSR_DoCommandsExample::StopRecordButtonOnClickHandler()
extern void GCSR_DoCommandsExample_StopRecordButtonOnClickHandler_mF63B77FDC42FD9254466CB566884870923F2A5B1 (void);
// 0x00000A8B System.Void FrostweepGames.Plugins.GoogleCloud.SpeechRecognition.Examples.GCSR_DoCommandsExample::StartedRecordEventHandler()
extern void GCSR_DoCommandsExample_StartedRecordEventHandler_m69A58B6FD4B08381D7F855F9835518CE656DFFB4 (void);
// 0x00000A8C System.Void FrostweepGames.Plugins.GoogleCloud.SpeechRecognition.Examples.GCSR_DoCommandsExample::RecordFailedEventHandler()
extern void GCSR_DoCommandsExample_RecordFailedEventHandler_mF50A567DCBCD696DD26E6557AFCE95294A27BBCB (void);
// 0x00000A8D System.Void FrostweepGames.Plugins.GoogleCloud.SpeechRecognition.Examples.GCSR_DoCommandsExample::EndTalkigEventHandler(UnityEngine.AudioClip,System.Single[])
extern void GCSR_DoCommandsExample_EndTalkigEventHandler_mF0FD9B99FC7ADEF99BA48B89591CED974D98301B (void);
// 0x00000A8E System.Void FrostweepGames.Plugins.GoogleCloud.SpeechRecognition.Examples.GCSR_DoCommandsExample::FinishedRecordEventHandler(UnityEngine.AudioClip,System.Single[])
extern void GCSR_DoCommandsExample_FinishedRecordEventHandler_m42B221984ADA64D5DDB8AA56C10A1783C2F14BD1 (void);
// 0x00000A8F System.Void FrostweepGames.Plugins.GoogleCloud.SpeechRecognition.Examples.GCSR_DoCommandsExample::RecognizeFailedEventHandler(System.String)
extern void GCSR_DoCommandsExample_RecognizeFailedEventHandler_m66F14488665CB5AA61393AC875243776953DD542 (void);
// 0x00000A90 System.Void FrostweepGames.Plugins.GoogleCloud.SpeechRecognition.Examples.GCSR_DoCommandsExample::RecognizeSuccessEventHandler(FrostweepGames.Plugins.GoogleCloud.SpeechRecognition.RecognitionResponse)
extern void GCSR_DoCommandsExample_RecognizeSuccessEventHandler_m53FB08218DA3FB812ED9612DAE38633FE184405A (void);
// 0x00000A91 System.Void FrostweepGames.Plugins.GoogleCloud.SpeechRecognition.Examples.GCSR_DoCommandsExample::DoCommand(System.String)
extern void GCSR_DoCommandsExample_DoCommand_mFB101E8B6B45139A11F2785D221AC93152DEFDF2 (void);
// 0x00000A92 System.Void FrostweepGames.Plugins.GoogleCloud.SpeechRecognition.Examples.GCSR_DoCommandsExample::.ctor()
extern void GCSR_DoCommandsExample__ctor_m1E36A47FA6CDF3C04D773D872EDCE34481806F4B (void);
// 0x00000A93 System.Void FrostweepGames.Plugins.GoogleCloud.SpeechRecognition.Examples.GCSR_DoCommandsExample/<>c::.cctor()
extern void U3CU3Ec__cctor_m6D2F1607739D08699B23954974D0F48826F81547 (void);
// 0x00000A94 System.Void FrostweepGames.Plugins.GoogleCloud.SpeechRecognition.Examples.GCSR_DoCommandsExample/<>c::.ctor()
extern void U3CU3Ec__ctor_mFBFF875A820F3D1F46EE151EF28BA66A665C9ED6 (void);
// 0x00000A95 System.Boolean FrostweepGames.Plugins.GoogleCloud.SpeechRecognition.Examples.GCSR_DoCommandsExample/<>c::<Start>b__8_0(UnityEngine.UI.Dropdown/OptionData)
extern void U3CU3Ec_U3CStartU3Eb__8_0_m2F9C61C32FA7857ED901DE1481A483C5C909D0B8 (void);
// 0x00000A96 System.Void FrostweepGames.Plugins.GoogleCloud.SpeechRecognition.Examples.GCSR_Example::Start()
extern void GCSR_Example_Start_mEDEBA7796B580FD5367845E69A54916F9CE24357 (void);
// 0x00000A97 System.Void FrostweepGames.Plugins.GoogleCloud.SpeechRecognition.Examples.GCSR_Example::OnDestroy()
extern void GCSR_Example_OnDestroy_mE775CF3285F05F5596AC7D3C81948BDCB8402ECC (void);
// 0x00000A98 System.Void FrostweepGames.Plugins.GoogleCloud.SpeechRecognition.Examples.GCSR_Example::Update()
extern void GCSR_Example_Update_mA625EC43C8A0CB70FB9B419EAFB3F1698CA9817C (void);
// 0x00000A99 System.Void FrostweepGames.Plugins.GoogleCloud.SpeechRecognition.Examples.GCSR_Example::RefreshMicsButtonOnClickHandler()
extern void GCSR_Example_RefreshMicsButtonOnClickHandler_mF50F2AF38212E054D838F4857C9A180BDA65F052 (void);
// 0x00000A9A System.Void FrostweepGames.Plugins.GoogleCloud.SpeechRecognition.Examples.GCSR_Example::MicrophoneDevicesDropdownOnValueChangedEventHandler(System.Int32)
extern void GCSR_Example_MicrophoneDevicesDropdownOnValueChangedEventHandler_mE2652754013DA6BCE475E7CB0DCF9B2F5F2CD7D3 (void);
// 0x00000A9B System.Void FrostweepGames.Plugins.GoogleCloud.SpeechRecognition.Examples.GCSR_Example::StartRecordButtonOnClickHandler()
extern void GCSR_Example_StartRecordButtonOnClickHandler_mB1BC04E647DB04429371C87C5AE298AEFB21DD07 (void);
// 0x00000A9C System.Void FrostweepGames.Plugins.GoogleCloud.SpeechRecognition.Examples.GCSR_Example::StopRecordButtonOnClickHandler()
extern void GCSR_Example_StopRecordButtonOnClickHandler_mBAAF04EFFA76887CA3010CE732AB9321FE4710E1 (void);
// 0x00000A9D System.Void FrostweepGames.Plugins.GoogleCloud.SpeechRecognition.Examples.GCSR_Example::GetOperationButtonOnClickHandler()
extern void GCSR_Example_GetOperationButtonOnClickHandler_m8605782AFB0EFF4FA765878610943CB3565A89D0 (void);
// 0x00000A9E System.Void FrostweepGames.Plugins.GoogleCloud.SpeechRecognition.Examples.GCSR_Example::GetListOperationsButtonOnClickHandler()
extern void GCSR_Example_GetListOperationsButtonOnClickHandler_m6C4C525F70DBB1DB9726DF158ABDFEDC96673D3C (void);
// 0x00000A9F System.Void FrostweepGames.Plugins.GoogleCloud.SpeechRecognition.Examples.GCSR_Example::DetectThresholdButtonOnClickHandler()
extern void GCSR_Example_DetectThresholdButtonOnClickHandler_mCAB1E74C61133F0FE3DC0FC2FA2DD16D6EB1B4D9 (void);
// 0x00000AA0 System.Void FrostweepGames.Plugins.GoogleCloud.SpeechRecognition.Examples.GCSR_Example::CancelAllRequetsButtonOnClickHandler()
extern void GCSR_Example_CancelAllRequetsButtonOnClickHandler_m90281CFE8EF97A04D8409EE5D454102B38BC2799 (void);
// 0x00000AA1 System.Void FrostweepGames.Plugins.GoogleCloud.SpeechRecognition.Examples.GCSR_Example::RecognizeButtonOnClickHandler()
extern void GCSR_Example_RecognizeButtonOnClickHandler_mFC51ED6576155B37F5DA60FAB0485C67ED92FD34 (void);
// 0x00000AA2 System.Void FrostweepGames.Plugins.GoogleCloud.SpeechRecognition.Examples.GCSR_Example::StartedRecordEventHandler()
extern void GCSR_Example_StartedRecordEventHandler_m5C55EF3D56D782819FD347ECA90A5455149A345F (void);
// 0x00000AA3 System.Void FrostweepGames.Plugins.GoogleCloud.SpeechRecognition.Examples.GCSR_Example::RecordFailedEventHandler()
extern void GCSR_Example_RecordFailedEventHandler_m2833F4C7AC8ADEEE087987D3A00546FDF241991E (void);
// 0x00000AA4 System.Void FrostweepGames.Plugins.GoogleCloud.SpeechRecognition.Examples.GCSR_Example::BeginTalkigEventHandler()
extern void GCSR_Example_BeginTalkigEventHandler_mB6A04D31BAF8D4487AA7597D6839EFA734860005 (void);
// 0x00000AA5 System.Void FrostweepGames.Plugins.GoogleCloud.SpeechRecognition.Examples.GCSR_Example::EndTalkigEventHandler(UnityEngine.AudioClip,System.Single[])
extern void GCSR_Example_EndTalkigEventHandler_mDD7CAE3865BA7D6F9E037EA67B4A65F2963000FD (void);
// 0x00000AA6 System.Void FrostweepGames.Plugins.GoogleCloud.SpeechRecognition.Examples.GCSR_Example::FinishedRecordEventHandler(UnityEngine.AudioClip,System.Single[])
extern void GCSR_Example_FinishedRecordEventHandler_m5A69C3A40D2EF5854C8CF534B36A1A794CD6F2E6 (void);
// 0x00000AA7 System.Void FrostweepGames.Plugins.GoogleCloud.SpeechRecognition.Examples.GCSR_Example::GetOperationFailedEventHandler(System.String)
extern void GCSR_Example_GetOperationFailedEventHandler_m8FAC950815B398DF903B6C8A5CC0AEC7B57EDE9F (void);
// 0x00000AA8 System.Void FrostweepGames.Plugins.GoogleCloud.SpeechRecognition.Examples.GCSR_Example::ListOperationsFailedEventHandler(System.String)
extern void GCSR_Example_ListOperationsFailedEventHandler_m8974DCB135999461C6654210AC1E8F0DB8A38F22 (void);
// 0x00000AA9 System.Void FrostweepGames.Plugins.GoogleCloud.SpeechRecognition.Examples.GCSR_Example::RecognizeFailedEventHandler(System.String)
extern void GCSR_Example_RecognizeFailedEventHandler_m752650BBFFB077D4584F46EB379105A2CCA26184 (void);
// 0x00000AAA System.Void FrostweepGames.Plugins.GoogleCloud.SpeechRecognition.Examples.GCSR_Example::LongRunningRecognizeFailedEventHandler(System.String)
extern void GCSR_Example_LongRunningRecognizeFailedEventHandler_mC62B8B4F3AB7F3498918670AF8E2B6E39CD69BA0 (void);
// 0x00000AAB System.Void FrostweepGames.Plugins.GoogleCloud.SpeechRecognition.Examples.GCSR_Example::ListOperationsSuccessEventHandler(FrostweepGames.Plugins.GoogleCloud.SpeechRecognition.ListOperationsResponse)
extern void GCSR_Example_ListOperationsSuccessEventHandler_mBAC38BAB2107547FD7DA6E0887B7774465770A8A (void);
// 0x00000AAC System.Void FrostweepGames.Plugins.GoogleCloud.SpeechRecognition.Examples.GCSR_Example::GetOperationSuccessEventHandler(FrostweepGames.Plugins.GoogleCloud.SpeechRecognition.Operation)
extern void GCSR_Example_GetOperationSuccessEventHandler_mB7279125A9F67E5893AE5D711993BDE3BC91A5D1 (void);
// 0x00000AAD System.Void FrostweepGames.Plugins.GoogleCloud.SpeechRecognition.Examples.GCSR_Example::RecognizeSuccessEventHandler(FrostweepGames.Plugins.GoogleCloud.SpeechRecognition.RecognitionResponse)
extern void GCSR_Example_RecognizeSuccessEventHandler_m9459DCF0F5D11D6AC9C6112D8B6E20F7BCCB2FBE (void);
// 0x00000AAE System.Void FrostweepGames.Plugins.GoogleCloud.SpeechRecognition.Examples.GCSR_Example::LongRunningRecognizeSuccessEventHandler(FrostweepGames.Plugins.GoogleCloud.SpeechRecognition.Operation)
extern void GCSR_Example_LongRunningRecognizeSuccessEventHandler_mC0ABDB4D23637AAB1C8445D452A69E4F0CF29029 (void);
// 0x00000AAF System.Void FrostweepGames.Plugins.GoogleCloud.SpeechRecognition.Examples.GCSR_Example::InsertRecognitionResponseInfo(FrostweepGames.Plugins.GoogleCloud.SpeechRecognition.RecognitionResponse)
extern void GCSR_Example_InsertRecognitionResponseInfo_m850861B87AB1D6EF68D1696A2D501592F0B75127 (void);
// 0x00000AB0 System.Void FrostweepGames.Plugins.GoogleCloud.SpeechRecognition.Examples.GCSR_Example::.ctor()
extern void GCSR_Example__ctor_m19DB5197F3D08CDD8FF39B36390718C820D8B862 (void);
// 0x00000AB1 System.Void FrostweepGames.Plugins.GoogleCloud.SpeechRecognition.Examples.GCSR_Example/<>c::.cctor()
extern void U3CU3Ec__cctor_mAD576C5A8D13A72D32EB0DE805F9EC506D895364 (void);
// 0x00000AB2 System.Void FrostweepGames.Plugins.GoogleCloud.SpeechRecognition.Examples.GCSR_Example/<>c::.ctor()
extern void U3CU3Ec__ctor_m1B50BD72179E3FFB0E6B6EAB44DE1458E87B1FD9 (void);
// 0x00000AB3 System.Boolean FrostweepGames.Plugins.GoogleCloud.SpeechRecognition.Examples.GCSR_Example/<>c::<FinishedRecordEventHandler>b__37_0(UnityEngine.UI.Dropdown/OptionData)
extern void U3CU3Ec_U3CFinishedRecordEventHandlerU3Eb__37_0_m9508E0E1467380DC7ED850098C8F28E5562B01BA (void);
// 0x00000AB4 System.Boolean FrostweepGames.Plugins.GoogleCloud.SpeechRecognition.Examples.GCSR_Example/<>c::<FinishedRecordEventHandler>b__37_1(UnityEngine.UI.Dropdown/OptionData)
extern void U3CU3Ec_U3CFinishedRecordEventHandlerU3Eb__37_1_mA974CA3E054FC59897BABC9F63D1F714147E710E (void);
// 0x00000AB5 System.Void FrostweepGames.Plugins.GoogleCloud.SpeechRecognition.Examples.speech2text::Start()
extern void speech2text_Start_m305E2C184126B3C5BD425BD274C9D3F233F0991B (void);
// 0x00000AB6 System.Void FrostweepGames.Plugins.GoogleCloud.SpeechRecognition.Examples.speech2text::OnDestroy()
extern void speech2text_OnDestroy_m6866FA09D49B3FB91C51D4BCD5D35E9B81E90791 (void);
// 0x00000AB7 System.Void FrostweepGames.Plugins.GoogleCloud.SpeechRecognition.Examples.speech2text::Update()
extern void speech2text_Update_m1C1FDA9180B0741C57A67B3FE4376AB8B6ACB8E4 (void);
// 0x00000AB8 System.Void FrostweepGames.Plugins.GoogleCloud.SpeechRecognition.Examples.speech2text::RefreshMicsButtonOnClickHandler()
extern void speech2text_RefreshMicsButtonOnClickHandler_m39B3CCEB45F267B0CB6EC2E3FD5C2E7AF62343BD (void);
// 0x00000AB9 System.Void FrostweepGames.Plugins.GoogleCloud.SpeechRecognition.Examples.speech2text::MicrophoneDevicesDropdownOnValueChangedEventHandler(System.Int32)
extern void speech2text_MicrophoneDevicesDropdownOnValueChangedEventHandler_m791CDD231CB6C238959E6EC87FAF7B462142B141 (void);
// 0x00000ABA System.Void FrostweepGames.Plugins.GoogleCloud.SpeechRecognition.Examples.speech2text::init_lang_var()
extern void speech2text_init_lang_var_m7D6C98430B2313BB98104AE9187C094EF1A8A56F (void);
// 0x00000ABB System.Void FrostweepGames.Plugins.GoogleCloud.SpeechRecognition.Examples.speech2text::StartRecordButtonOnClickHandler()
extern void speech2text_StartRecordButtonOnClickHandler_mA4F82021C073CE8C21D662B5231C09C005E34205 (void);
// 0x00000ABC System.Void FrostweepGames.Plugins.GoogleCloud.SpeechRecognition.Examples.speech2text::StopRecordButtonOnClickHandler()
extern void speech2text_StopRecordButtonOnClickHandler_m077AB3ECF0E761B8C2DD5A5D569CDCE5FCB0570A (void);
// 0x00000ABD System.Void FrostweepGames.Plugins.GoogleCloud.SpeechRecognition.Examples.speech2text::GetOperationButtonOnClickHandler()
extern void speech2text_GetOperationButtonOnClickHandler_mC02EF58A32DB88EC1AC444BE2A54D7D57D2B40E7 (void);
// 0x00000ABE System.Void FrostweepGames.Plugins.GoogleCloud.SpeechRecognition.Examples.speech2text::GetListOperationsButtonOnClickHandler()
extern void speech2text_GetListOperationsButtonOnClickHandler_mAE6862E2CDCC1F4BD7911B114CE1C5B341CF7A56 (void);
// 0x00000ABF System.Void FrostweepGames.Plugins.GoogleCloud.SpeechRecognition.Examples.speech2text::DetectThresholdButtonOnClickHandler()
extern void speech2text_DetectThresholdButtonOnClickHandler_mEB3B13C7EB0846F037374F4317119390A0BB2DF2 (void);
// 0x00000AC0 System.Void FrostweepGames.Plugins.GoogleCloud.SpeechRecognition.Examples.speech2text::CancelAllRequetsButtonOnClickHandler()
extern void speech2text_CancelAllRequetsButtonOnClickHandler_mA4F0A72A2F208CA1B14AB34681929FC13025995E (void);
// 0x00000AC1 System.Void FrostweepGames.Plugins.GoogleCloud.SpeechRecognition.Examples.speech2text::RecognizeButtonOnClickHandler()
extern void speech2text_RecognizeButtonOnClickHandler_mE5D569B06C80203793CFC3DF1064DB414104E7EA (void);
// 0x00000AC2 System.Void FrostweepGames.Plugins.GoogleCloud.SpeechRecognition.Examples.speech2text::StartedRecordEventHandler()
extern void speech2text_StartedRecordEventHandler_m718A34BD01296D9EDBD41BE3BC0160215BF155CE (void);
// 0x00000AC3 System.Void FrostweepGames.Plugins.GoogleCloud.SpeechRecognition.Examples.speech2text::RecordFailedEventHandler()
extern void speech2text_RecordFailedEventHandler_m4EDD874CF8D2F212EAF219F7A3A7D9F2D5397294 (void);
// 0x00000AC4 System.Void FrostweepGames.Plugins.GoogleCloud.SpeechRecognition.Examples.speech2text::BeginTalkigEventHandler()
extern void speech2text_BeginTalkigEventHandler_mDBEBC3E9F1469BC1A7B4B903342CAB1CF91E0298 (void);
// 0x00000AC5 System.Void FrostweepGames.Plugins.GoogleCloud.SpeechRecognition.Examples.speech2text::EndTalkigEventHandler(UnityEngine.AudioClip,System.Single[])
extern void speech2text_EndTalkigEventHandler_mF78CB283E46C7209A1B7D89CF29826B573679065 (void);
// 0x00000AC6 System.Void FrostweepGames.Plugins.GoogleCloud.SpeechRecognition.Examples.speech2text::linguaDetectAlternativa(FrostweepGames.Plugins.GoogleCloud.SpeechRecognition.RecognitionConfig,UnityEngine.AudioClip,System.Single[],System.Int32)
extern void speech2text_linguaDetectAlternativa_m9A2EB053E0C95FE62E720E6D3E79C4AD00D7F4EC (void);
// 0x00000AC7 System.Void FrostweepGames.Plugins.GoogleCloud.SpeechRecognition.Examples.speech2text::FinishedRecordEventHandler(UnityEngine.AudioClip,System.Single[])
extern void speech2text_FinishedRecordEventHandler_m3E3F1E240AB2D7B0439120F2724AB586373B79DE (void);
// 0x00000AC8 System.Void FrostweepGames.Plugins.GoogleCloud.SpeechRecognition.Examples.speech2text::GetOperationFailedEventHandler(System.String)
extern void speech2text_GetOperationFailedEventHandler_mCF483561D26A195595F88160BDD52E97D4A25CF8 (void);
// 0x00000AC9 System.Void FrostweepGames.Plugins.GoogleCloud.SpeechRecognition.Examples.speech2text::ListOperationsFailedEventHandler(System.String)
extern void speech2text_ListOperationsFailedEventHandler_m97666BDDE0971767D413C4EB7BEBC57F10488C5C (void);
// 0x00000ACA System.Void FrostweepGames.Plugins.GoogleCloud.SpeechRecognition.Examples.speech2text::RecognizeFailedEventHandler(System.String)
extern void speech2text_RecognizeFailedEventHandler_m973BBEF6BCA7CE43A3F0008954D5C15EC1154172 (void);
// 0x00000ACB System.Void FrostweepGames.Plugins.GoogleCloud.SpeechRecognition.Examples.speech2text::LongRunningRecognizeFailedEventHandler(System.String)
extern void speech2text_LongRunningRecognizeFailedEventHandler_m630148726F91F2BAE7847BF6BC2291AD28F35581 (void);
// 0x00000ACC System.Void FrostweepGames.Plugins.GoogleCloud.SpeechRecognition.Examples.speech2text::RecognizeSuccessEventHandler(FrostweepGames.Plugins.GoogleCloud.SpeechRecognition.RecognitionResponse)
extern void speech2text_RecognizeSuccessEventHandler_m8E8A950A7E824BF22180EF42FF06CE771211B782 (void);
// 0x00000ACD System.Void FrostweepGames.Plugins.GoogleCloud.SpeechRecognition.Examples.speech2text::InsertRecognitionResponseInfo(FrostweepGames.Plugins.GoogleCloud.SpeechRecognition.RecognitionResponse)
extern void speech2text_InsertRecognitionResponseInfo_mDA70B697E5C8594379EA3D51B8BEFA1C749D38E8 (void);
// 0x00000ACE System.Void FrostweepGames.Plugins.GoogleCloud.SpeechRecognition.Examples.speech2text::.ctor()
extern void speech2text__ctor_m46C0063CBC8EC4F04A4E00484F8880F8732C98F2 (void);
// 0x00000ACF System.Void FrostweepGames.Plugins.GoogleCloud.SpeechRecognition.Examples.speech2text/<>c::.cctor()
extern void U3CU3Ec__cctor_mDC9676760A0627A7FBEE93AC3CE1B8F5862AB7AA (void);
// 0x00000AD0 System.Void FrostweepGames.Plugins.GoogleCloud.SpeechRecognition.Examples.speech2text/<>c::.ctor()
extern void U3CU3Ec__ctor_mC56D25B1229BFD984C33925EEE9C69A1CC446F66 (void);
// 0x00000AD1 System.Boolean FrostweepGames.Plugins.GoogleCloud.SpeechRecognition.Examples.speech2text/<>c::<Start>b__17_0(UnityEngine.UI.Dropdown/OptionData)
extern void U3CU3Ec_U3CStartU3Eb__17_0_mA3C18A9832F127C06E196D5ED1E73D9E96EEEC96 (void);
// 0x00000AD2 System.Boolean FrostweepGames.Plugins.GoogleCloud.SpeechRecognition.Examples.speech2text/<>c::<FinishedRecordEventHandler>b__37_0(UnityEngine.UI.Dropdown/OptionData)
extern void U3CU3Ec_U3CFinishedRecordEventHandlerU3Eb__37_0_mE43B03956368519211E7D231CFE107E13F9087DB (void);
// 0x00000AD3 System.Void CrazyMinnow.AmplitudeWebGL.AmplitudeSamplesUI::Start()
extern void AmplitudeSamplesUI_Start_mC862317E34DC04867122696C5CF25A93A281F7DC (void);
// 0x00000AD4 System.Void CrazyMinnow.AmplitudeWebGL.AmplitudeSamplesUI::Update()
extern void AmplitudeSamplesUI_Update_m1799C215CFC0F4A9EB1D3F21E4A40B918D4E90A3 (void);
// 0x00000AD5 System.Void CrazyMinnow.AmplitudeWebGL.AmplitudeSamplesUI::SetBoost(System.Single)
extern void AmplitudeSamplesUI_SetBoost_m578252BFB16EE3439ED5E6DF4E100D5C6CFEE095 (void);
// 0x00000AD6 System.Void CrazyMinnow.AmplitudeWebGL.AmplitudeSamplesUI::OnValueChangedSampleSize(System.Int32)
extern void AmplitudeSamplesUI_OnValueChangedSampleSize_m951431B498A13F5AC49413E2B6568467221FE598 (void);
// 0x00000AD7 System.Void CrazyMinnow.AmplitudeWebGL.AmplitudeSamplesUI::OnValueChangedDataType(System.Int32)
extern void AmplitudeSamplesUI_OnValueChangedDataType_mE7CB21979BEA8508E6B78F3782C3BAE16D20FA4C (void);
// 0x00000AD8 System.Void CrazyMinnow.AmplitudeWebGL.AmplitudeSamplesUI::Play()
extern void AmplitudeSamplesUI_Play_m27F7EEEAA517929CBCDD59B1B6D169B392D4F0BB (void);
// 0x00000AD9 System.Void CrazyMinnow.AmplitudeWebGL.AmplitudeSamplesUI::Stop()
extern void AmplitudeSamplesUI_Stop_m8D795BC40191E30F1E4EDF38644D3B1F6C027EA2 (void);
// 0x00000ADA System.Void CrazyMinnow.AmplitudeWebGL.AmplitudeSamplesUI::.ctor()
extern void AmplitudeSamplesUI__ctor_mA89EA8A1180D77FA05FAE16868FB42DF6D9784C1 (void);
// 0x00000ADB System.Void CrazyMinnow.AmplitudeWebGL.AmplitudeTester::Update()
extern void AmplitudeTester_Update_m432D6C77B9DE0C10DBBF500EDA0CA0A6F980D8F2 (void);
// 0x00000ADC System.Void CrazyMinnow.AmplitudeWebGL.AmplitudeTester::Play()
extern void AmplitudeTester_Play_m961D5DADACCCA6A67220B0CA40C21A7B847FECAC (void);
// 0x00000ADD System.Void CrazyMinnow.AmplitudeWebGL.AmplitudeTester::Stop()
extern void AmplitudeTester_Stop_mCD33463014E2FEA12F14C828B5F722D3169FF9B7 (void);
// 0x00000ADE System.Void CrazyMinnow.AmplitudeWebGL.AmplitudeTester::.ctor()
extern void AmplitudeTester__ctor_mE5A18E5295CAD2881EFFC0522A25B2BA929EE3F6 (void);
// 0x00000ADF System.Void CrazyMinnow.AmplitudeWebGL.Amplitude::Awake()
extern void Amplitude_Awake_mF3B009C75395EE567E8B42D875017C4ADF0F1C5C (void);
// 0x00000AE0 System.Void CrazyMinnow.AmplitudeWebGL.Amplitude::Start()
extern void Amplitude_Start_m1C3C118F695BAA19D6C457134AC99AB8AAF70F21 (void);
// 0x00000AE1 System.Collections.IEnumerator CrazyMinnow.AmplitudeWebGL.Amplitude::PlayOnAwake(System.Single)
extern void Amplitude_PlayOnAwake_mEB70D141E7117F804F74F47BFF9410EA598FB68A (void);
// 0x00000AE2 System.Void CrazyMinnow.AmplitudeWebGL.Amplitude::LateUpdate()
extern void Amplitude_LateUpdate_mB2703DB0C4D5C5182573CCCF1201CC605ECA22F4 (void);
// 0x00000AE3 System.Single CrazyMinnow.AmplitudeWebGL.Amplitude::ScaleRange(System.Single,System.Single,System.Single,System.Single,System.Single)
extern void Amplitude_ScaleRange_m919F01A62EDBE80BB47730125E5B94C11D0589D1 (void);
// 0x00000AE4 System.Void CrazyMinnow.AmplitudeWebGL.Amplitude::.ctor()
extern void Amplitude__ctor_mC71496FD5D5DF312EE79E6943D46873BC4F75B9F (void);
// 0x00000AE5 System.Void CrazyMinnow.AmplitudeWebGL.Amplitude/<PlayOnAwake>d__24::.ctor(System.Int32)
extern void U3CPlayOnAwakeU3Ed__24__ctor_mB806F712BEE562BFD80BA8ED3FAE86EDC8587DA7 (void);
// 0x00000AE6 System.Void CrazyMinnow.AmplitudeWebGL.Amplitude/<PlayOnAwake>d__24::System.IDisposable.Dispose()
extern void U3CPlayOnAwakeU3Ed__24_System_IDisposable_Dispose_mABEE00ECE6ED6CDBAE345C4086415CFDCB46C136 (void);
// 0x00000AE7 System.Boolean CrazyMinnow.AmplitudeWebGL.Amplitude/<PlayOnAwake>d__24::MoveNext()
extern void U3CPlayOnAwakeU3Ed__24_MoveNext_mC764E91B7C9060F8219AF58521FDB020D05E244F (void);
// 0x00000AE8 System.Object CrazyMinnow.AmplitudeWebGL.Amplitude/<PlayOnAwake>d__24::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CPlayOnAwakeU3Ed__24_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mDB4BB88B57CE4320AB30E332C426C69F2CED93D9 (void);
// 0x00000AE9 System.Void CrazyMinnow.AmplitudeWebGL.Amplitude/<PlayOnAwake>d__24::System.Collections.IEnumerator.Reset()
extern void U3CPlayOnAwakeU3Ed__24_System_Collections_IEnumerator_Reset_mDFC37A3B54A0DACB1CB04B8AEBF3CC1840C04982 (void);
// 0x00000AEA System.Object CrazyMinnow.AmplitudeWebGL.Amplitude/<PlayOnAwake>d__24::System.Collections.IEnumerator.get_Current()
extern void U3CPlayOnAwakeU3Ed__24_System_Collections_IEnumerator_get_Current_mD759248DB373EF917B115C5727F2045C005DECC3 (void);
// 0x00000AEB System.Void CrazyMinnow.AmplitudeWebGL.WebAudioUrls.AudioUrl::.ctor(System.String,UnityEngine.AudioType)
extern void AudioUrl__ctor_m161DA67A8471A56C088D42472EB640CA99D6554C (void);
// 0x00000AEC System.Void CrazyMinnow.AmplitudeWebGL.WebAudioUrls.Notification::.ctor(CrazyMinnow.AmplitudeWebGL.WebAudioUrls.Notification/NotificationType,System.String,System.String)
extern void Notification__ctor_mD2703FBF62656074E25AFB11B5532DA4FA0CA27D (void);
// 0x00000AED System.Int32 CrazyMinnow.AmplitudeWebGL.WebAudioUrls.WebAudioUrlProcessor::get_QueueCount()
extern void WebAudioUrlProcessor_get_QueueCount_m2FB8E7965E822A319CBC91C1539DFB745EEBF136 (void);
// 0x00000AEE System.Boolean CrazyMinnow.AmplitudeWebGL.WebAudioUrls.WebAudioUrlProcessor::get_IsQueueProcessing()
extern void WebAudioUrlProcessor_get_IsQueueProcessing_m43EEB4787FF12D1CF18B6207A8AA09277346F559 (void);
// 0x00000AEF System.Collections.IEnumerator CrazyMinnow.AmplitudeWebGL.WebAudioUrls.WebAudioUrlProcessor::WaitForAudioUrlProcessing(CrazyMinnow.AmplitudeWebGL.WebAudioUrls.AudioUrl)
extern void WebAudioUrlProcessor_WaitForAudioUrlProcessing_m073F718578D30B2711F8DD9F11F749878BF3E7C3 (void);
// 0x00000AF0 System.Collections.IEnumerator CrazyMinnow.AmplitudeWebGL.WebAudioUrls.WebAudioUrlProcessor::ProcessUrlQueue()
extern void WebAudioUrlProcessor_ProcessUrlQueue_mE993AE7B9F4FCD6836C593C459F1A6C723DF6248 (void);
// 0x00000AF1 System.Collections.IEnumerator CrazyMinnow.AmplitudeWebGL.WebAudioUrls.WebAudioUrlProcessor::ProcessUrl(CrazyMinnow.AmplitudeWebGL.WebAudioUrls.AudioUrl)
extern void WebAudioUrlProcessor_ProcessUrl_m4C26E983564FE9DBA3B29902B888F8977E97F9DE (void);
// 0x00000AF2 System.Collections.IEnumerator CrazyMinnow.AmplitudeWebGL.WebAudioUrls.WebAudioUrlProcessor::WaitForQueue()
extern void WebAudioUrlProcessor_WaitForQueue_m18F65E2F2F75E7E2703AD065747FCAA26D7ABDF1 (void);
// 0x00000AF3 System.Void CrazyMinnow.AmplitudeWebGL.WebAudioUrls.WebAudioUrlProcessor::SetCallbacks(CrazyMinnow.AmplitudeWebGL.WebAudioUrls.WebAudioUrlProcessor/FailCallback)
extern void WebAudioUrlProcessor_SetCallbacks_m579F620DCB5756D94417E5E02A1E6EF404DD81FC (void);
// 0x00000AF4 System.Void CrazyMinnow.AmplitudeWebGL.WebAudioUrls.WebAudioUrlProcessor::SetCallbacks(CrazyMinnow.AmplitudeWebGL.WebAudioUrls.WebAudioUrlProcessor/SuccessCallback`1<UnityEngine.AudioClip>,CrazyMinnow.AmplitudeWebGL.WebAudioUrls.WebAudioUrlProcessor/FailCallback)
extern void WebAudioUrlProcessor_SetCallbacks_m9129ADD1276098C985614D7EEDD5EC3762CF255D (void);
// 0x00000AF5 System.Void CrazyMinnow.AmplitudeWebGL.WebAudioUrls.WebAudioUrlProcessor::GetAudioClip(CrazyMinnow.AmplitudeWebGL.WebAudioUrls.AudioUrl,CrazyMinnow.AmplitudeWebGL.WebAudioUrls.WebAudioUrlProcessor/SuccessCallback`1<UnityEngine.AudioClip>,CrazyMinnow.AmplitudeWebGL.WebAudioUrls.WebAudioUrlProcessor/FailCallback)
extern void WebAudioUrlProcessor_GetAudioClip_m3F3CD668F5F657DD8389DCA6C671BBD069645E0C (void);
// 0x00000AF6 System.Void CrazyMinnow.AmplitudeWebGL.WebAudioUrls.WebAudioUrlProcessor::EnqueueAudioUrl(CrazyMinnow.AmplitudeWebGL.WebAudioUrls.AudioUrl,CrazyMinnow.AmplitudeWebGL.WebAudioUrls.WebAudioUrlProcessor/FailCallback)
extern void WebAudioUrlProcessor_EnqueueAudioUrl_mCE09971129175BD3A8F3519B972AE28B2EA55BD1 (void);
// 0x00000AF7 System.Void CrazyMinnow.AmplitudeWebGL.WebAudioUrls.WebAudioUrlProcessor::GetBatchedAudioClip(CrazyMinnow.AmplitudeWebGL.WebAudioUrls.WebAudioUrlProcessor/SuccessCallback`1<UnityEngine.AudioClip>,CrazyMinnow.AmplitudeWebGL.WebAudioUrls.WebAudioUrlProcessor/FailCallback)
extern void WebAudioUrlProcessor_GetBatchedAudioClip_m1744859FF8C9E1B1CE2D504C6CEFE178C21CD692 (void);
// 0x00000AF8 System.Void CrazyMinnow.AmplitudeWebGL.WebAudioUrls.WebAudioUrlProcessor::ClearAudioClipQueue()
extern void WebAudioUrlProcessor_ClearAudioClipQueue_mA41490BD646DFC8113CF3FA0F347258206D329EF (void);
// 0x00000AF9 System.Void CrazyMinnow.AmplitudeWebGL.WebAudioUrls.WebAudioUrlProcessor::.ctor()
extern void WebAudioUrlProcessor__ctor_m6205A500669C64D40DC5E78AE8444225435A8D24 (void);
// 0x00000AFA System.Void CrazyMinnow.AmplitudeWebGL.WebAudioUrls.WebAudioUrlProcessor/SuccessCallback`1::.ctor(System.Object,System.IntPtr)
// 0x00000AFB System.Void CrazyMinnow.AmplitudeWebGL.WebAudioUrls.WebAudioUrlProcessor/SuccessCallback`1::Invoke(T)
// 0x00000AFC System.IAsyncResult CrazyMinnow.AmplitudeWebGL.WebAudioUrls.WebAudioUrlProcessor/SuccessCallback`1::BeginInvoke(T,System.AsyncCallback,System.Object)
// 0x00000AFD System.Void CrazyMinnow.AmplitudeWebGL.WebAudioUrls.WebAudioUrlProcessor/SuccessCallback`1::EndInvoke(System.IAsyncResult)
// 0x00000AFE System.Void CrazyMinnow.AmplitudeWebGL.WebAudioUrls.WebAudioUrlProcessor/FailCallback::.ctor(System.Object,System.IntPtr)
extern void FailCallback__ctor_mD98D404B6FEB882AAC01FE5FAD049450F252D1A0 (void);
// 0x00000AFF System.Void CrazyMinnow.AmplitudeWebGL.WebAudioUrls.WebAudioUrlProcessor/FailCallback::Invoke(CrazyMinnow.AmplitudeWebGL.WebAudioUrls.Notification)
extern void FailCallback_Invoke_m9BAF2CFFDD8BA208A09922A5FA72CF448B49DC74 (void);
// 0x00000B00 System.IAsyncResult CrazyMinnow.AmplitudeWebGL.WebAudioUrls.WebAudioUrlProcessor/FailCallback::BeginInvoke(CrazyMinnow.AmplitudeWebGL.WebAudioUrls.Notification,System.AsyncCallback,System.Object)
extern void FailCallback_BeginInvoke_m57CF2D596F36A37FD4ADBCA654303C1ADCA7798F (void);
// 0x00000B01 System.Void CrazyMinnow.AmplitudeWebGL.WebAudioUrls.WebAudioUrlProcessor/FailCallback::EndInvoke(System.IAsyncResult)
extern void FailCallback_EndInvoke_mEE3CA1FFBF5A3F63CE953B45184A9013BD268A71 (void);
// 0x00000B02 System.Void CrazyMinnow.AmplitudeWebGL.WebAudioUrls.WebAudioUrlProcessor/<WaitForAudioUrlProcessing>d__14::.ctor(System.Int32)
extern void U3CWaitForAudioUrlProcessingU3Ed__14__ctor_mE73D373F007FF26769B7A39CC9EE43D7D5DC5999 (void);
// 0x00000B03 System.Void CrazyMinnow.AmplitudeWebGL.WebAudioUrls.WebAudioUrlProcessor/<WaitForAudioUrlProcessing>d__14::System.IDisposable.Dispose()
extern void U3CWaitForAudioUrlProcessingU3Ed__14_System_IDisposable_Dispose_m40DB25CB4B363057ACCFE6175DA0B812E0EB4916 (void);
// 0x00000B04 System.Boolean CrazyMinnow.AmplitudeWebGL.WebAudioUrls.WebAudioUrlProcessor/<WaitForAudioUrlProcessing>d__14::MoveNext()
extern void U3CWaitForAudioUrlProcessingU3Ed__14_MoveNext_m81C7D54B7258F8680F2720F3869DD338C8AD9050 (void);
// 0x00000B05 System.Object CrazyMinnow.AmplitudeWebGL.WebAudioUrls.WebAudioUrlProcessor/<WaitForAudioUrlProcessing>d__14::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CWaitForAudioUrlProcessingU3Ed__14_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mE83403F038DBF77EF1BA1EFACC71F5C883AA7307 (void);
// 0x00000B06 System.Void CrazyMinnow.AmplitudeWebGL.WebAudioUrls.WebAudioUrlProcessor/<WaitForAudioUrlProcessing>d__14::System.Collections.IEnumerator.Reset()
extern void U3CWaitForAudioUrlProcessingU3Ed__14_System_Collections_IEnumerator_Reset_m5FECAEC4E160C94E9B6C6C061C28C4A96833D54A (void);
// 0x00000B07 System.Object CrazyMinnow.AmplitudeWebGL.WebAudioUrls.WebAudioUrlProcessor/<WaitForAudioUrlProcessing>d__14::System.Collections.IEnumerator.get_Current()
extern void U3CWaitForAudioUrlProcessingU3Ed__14_System_Collections_IEnumerator_get_Current_m49BC9B4608EA5B7EF36A7285009F53DBB3A1FEDA (void);
// 0x00000B08 System.Void CrazyMinnow.AmplitudeWebGL.WebAudioUrls.WebAudioUrlProcessor/<ProcessUrlQueue>d__15::.ctor(System.Int32)
extern void U3CProcessUrlQueueU3Ed__15__ctor_mB11C95B045C496FE1AF45815A9D9A80353392238 (void);
// 0x00000B09 System.Void CrazyMinnow.AmplitudeWebGL.WebAudioUrls.WebAudioUrlProcessor/<ProcessUrlQueue>d__15::System.IDisposable.Dispose()
extern void U3CProcessUrlQueueU3Ed__15_System_IDisposable_Dispose_m0DF615CB9BCF331920E179594A292C1FDE41509B (void);
// 0x00000B0A System.Boolean CrazyMinnow.AmplitudeWebGL.WebAudioUrls.WebAudioUrlProcessor/<ProcessUrlQueue>d__15::MoveNext()
extern void U3CProcessUrlQueueU3Ed__15_MoveNext_mE7B803EDC5171E62A16C3C694DBBD36DBE428FEE (void);
// 0x00000B0B System.Object CrazyMinnow.AmplitudeWebGL.WebAudioUrls.WebAudioUrlProcessor/<ProcessUrlQueue>d__15::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CProcessUrlQueueU3Ed__15_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m54532538CDB1A82F3C31B4EE096D5620FF2C6453 (void);
// 0x00000B0C System.Void CrazyMinnow.AmplitudeWebGL.WebAudioUrls.WebAudioUrlProcessor/<ProcessUrlQueue>d__15::System.Collections.IEnumerator.Reset()
extern void U3CProcessUrlQueueU3Ed__15_System_Collections_IEnumerator_Reset_mF4DDEF2FB2CB3CBCF0BCB1F89A98A0A1E7DEC751 (void);
// 0x00000B0D System.Object CrazyMinnow.AmplitudeWebGL.WebAudioUrls.WebAudioUrlProcessor/<ProcessUrlQueue>d__15::System.Collections.IEnumerator.get_Current()
extern void U3CProcessUrlQueueU3Ed__15_System_Collections_IEnumerator_get_Current_mE14194A18DF0D6CB9B52409B67705B06FA7F8A1B (void);
// 0x00000B0E System.Void CrazyMinnow.AmplitudeWebGL.WebAudioUrls.WebAudioUrlProcessor/<ProcessUrl>d__16::.ctor(System.Int32)
extern void U3CProcessUrlU3Ed__16__ctor_mC66072579BAE78073CC8652C1DEFCEFC97B0FE57 (void);
// 0x00000B0F System.Void CrazyMinnow.AmplitudeWebGL.WebAudioUrls.WebAudioUrlProcessor/<ProcessUrl>d__16::System.IDisposable.Dispose()
extern void U3CProcessUrlU3Ed__16_System_IDisposable_Dispose_m9C2BF4D674C2B05CBFDC4BCE9F6AEB8C20739404 (void);
// 0x00000B10 System.Boolean CrazyMinnow.AmplitudeWebGL.WebAudioUrls.WebAudioUrlProcessor/<ProcessUrl>d__16::MoveNext()
extern void U3CProcessUrlU3Ed__16_MoveNext_mBCA167BF9C8FF9CE6008DBE9FBDC7221EC741D1C (void);
// 0x00000B11 System.Void CrazyMinnow.AmplitudeWebGL.WebAudioUrls.WebAudioUrlProcessor/<ProcessUrl>d__16::<>m__Finally1()
extern void U3CProcessUrlU3Ed__16_U3CU3Em__Finally1_mA3BC5B43C2088568020CDA05664D497225A2C4DD (void);
// 0x00000B12 System.Object CrazyMinnow.AmplitudeWebGL.WebAudioUrls.WebAudioUrlProcessor/<ProcessUrl>d__16::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CProcessUrlU3Ed__16_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m39C984F612B617A8FCAAC0FE6FE97F00B92B96D1 (void);
// 0x00000B13 System.Void CrazyMinnow.AmplitudeWebGL.WebAudioUrls.WebAudioUrlProcessor/<ProcessUrl>d__16::System.Collections.IEnumerator.Reset()
extern void U3CProcessUrlU3Ed__16_System_Collections_IEnumerator_Reset_m651076750200783DE8226D093E59153136C451CF (void);
// 0x00000B14 System.Object CrazyMinnow.AmplitudeWebGL.WebAudioUrls.WebAudioUrlProcessor/<ProcessUrl>d__16::System.Collections.IEnumerator.get_Current()
extern void U3CProcessUrlU3Ed__16_System_Collections_IEnumerator_get_Current_mE3AD2930731866168C8D52B66CA858A92F1D8269 (void);
// 0x00000B15 System.Void CrazyMinnow.AmplitudeWebGL.WebAudioUrls.WebAudioUrlProcessor/<WaitForQueue>d__17::.ctor(System.Int32)
extern void U3CWaitForQueueU3Ed__17__ctor_m49D6149B0BA3130D79B3F07B8B3AC1E57B055E3D (void);
// 0x00000B16 System.Void CrazyMinnow.AmplitudeWebGL.WebAudioUrls.WebAudioUrlProcessor/<WaitForQueue>d__17::System.IDisposable.Dispose()
extern void U3CWaitForQueueU3Ed__17_System_IDisposable_Dispose_mA4440F9EDBC6370461BE37951BD4AD585090B9EB (void);
// 0x00000B17 System.Boolean CrazyMinnow.AmplitudeWebGL.WebAudioUrls.WebAudioUrlProcessor/<WaitForQueue>d__17::MoveNext()
extern void U3CWaitForQueueU3Ed__17_MoveNext_mFC776F253693AF51E9074AB4CF957349C3CFB17E (void);
// 0x00000B18 System.Object CrazyMinnow.AmplitudeWebGL.WebAudioUrls.WebAudioUrlProcessor/<WaitForQueue>d__17::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CWaitForQueueU3Ed__17_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mCBACF6139B2171180E6F861447113808FAC82593 (void);
// 0x00000B19 System.Void CrazyMinnow.AmplitudeWebGL.WebAudioUrls.WebAudioUrlProcessor/<WaitForQueue>d__17::System.Collections.IEnumerator.Reset()
extern void U3CWaitForQueueU3Ed__17_System_Collections_IEnumerator_Reset_mAA8202C833D9070E6609298C674302399A42E4C1 (void);
// 0x00000B1A System.Object CrazyMinnow.AmplitudeWebGL.WebAudioUrls.WebAudioUrlProcessor/<WaitForQueue>d__17::System.Collections.IEnumerator.get_Current()
extern void U3CWaitForQueueU3Ed__17_System_Collections_IEnumerator_get_Current_m6F01B944116F7AF35AE73BCDB13FC76E82935199 (void);
// 0x00000B1B System.Void CrazyMinnow.SALSA.CM_RuntimeSetupExample::Start()
extern void CM_RuntimeSetupExample_Start_mBFE59A62D3907030FFF20005D3BAD42E9252EFD7 (void);
// 0x00000B1C System.Void CrazyMinnow.SALSA.CM_RuntimeSetupExample::Update()
extern void CM_RuntimeSetupExample_Update_mC2784B90109EA40F40E5C26DD591DA5F7545428E (void);
// 0x00000B1D System.Collections.IEnumerator CrazyMinnow.SALSA.CM_RuntimeSetupExample::RemoveComponents()
extern void CM_RuntimeSetupExample_RemoveComponents_mA7C611D5F3ADDC8D02B2C6F4AB12E27CF75EED9F (void);
// 0x00000B1E System.Void CrazyMinnow.SALSA.CM_RuntimeSetupExample::.ctor()
extern void CM_RuntimeSetupExample__ctor_m4957371CAF8014C3FEBAAEB9647CCE2739EAF826 (void);
// 0x00000B1F System.Void CrazyMinnow.SALSA.CM_RuntimeSetupExample/<RemoveComponents>d__7::.ctor(System.Int32)
extern void U3CRemoveComponentsU3Ed__7__ctor_m41B7A3E15A422129EC240B326D8ADF94028894B6 (void);
// 0x00000B20 System.Void CrazyMinnow.SALSA.CM_RuntimeSetupExample/<RemoveComponents>d__7::System.IDisposable.Dispose()
extern void U3CRemoveComponentsU3Ed__7_System_IDisposable_Dispose_mAA6D6AA38ECADC34204CE8590DAA1A3E32ACA1DB (void);
// 0x00000B21 System.Boolean CrazyMinnow.SALSA.CM_RuntimeSetupExample/<RemoveComponents>d__7::MoveNext()
extern void U3CRemoveComponentsU3Ed__7_MoveNext_m57FB9C035701B9BBB5E5DBE01972F14794083491 (void);
// 0x00000B22 System.Object CrazyMinnow.SALSA.CM_RuntimeSetupExample/<RemoveComponents>d__7::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CRemoveComponentsU3Ed__7_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m70297559CB7CD4DDE0C2B6ADDCCC096F3D294798 (void);
// 0x00000B23 System.Void CrazyMinnow.SALSA.CM_RuntimeSetupExample/<RemoveComponents>d__7::System.Collections.IEnumerator.Reset()
extern void U3CRemoveComponentsU3Ed__7_System_Collections_IEnumerator_Reset_m71C6F10A218DAD07591A5901F8437E5C7C70DCCD (void);
// 0x00000B24 System.Object CrazyMinnow.SALSA.CM_RuntimeSetupExample/<RemoveComponents>d__7::System.Collections.IEnumerator.get_Current()
extern void U3CRemoveComponentsU3Ed__7_System_Collections_IEnumerator_get_Current_m99A5DB34E9D032ADFAA13A471094209D90BF84B2 (void);
// 0x00000B25 System.Void CrazyMinnow.SALSA.iClone.CM_iCloneSetup::Awake()
extern void CM_iCloneSetup_Awake_m060150ADE0FB0D048C426899FB2904280967CDDE (void);
// 0x00000B26 System.Void CrazyMinnow.SALSA.iClone.CM_iCloneSetup::Setup()
extern void CM_iCloneSetup_Setup_mCC5F12F4C44D60A5A34F775EA37B093EDAF410D5 (void);
// 0x00000B27 System.Void CrazyMinnow.SALSA.iClone.CM_iCloneSetup::.ctor()
extern void CM_iCloneSetup__ctor_m5A54790F3FFD18F240DFB9B461BA47D1DB7AAB5F (void);
// 0x00000B28 System.Void CrazyMinnow.SALSA.iClone.CM_iCloneSync::Reset()
extern void CM_iCloneSync_Reset_m30051B265E1529B2D0272AE6D7BF04B3B9846247 (void);
// 0x00000B29 System.Void CrazyMinnow.SALSA.iClone.CM_iCloneSync::Start()
extern void CM_iCloneSync_Start_m7886AFC63CB5BF0065F0CAD3063803453C974EBE (void);
// 0x00000B2A System.Void CrazyMinnow.SALSA.iClone.CM_iCloneSync::LateUpdate()
extern void CM_iCloneSync_LateUpdate_m3C2100B73171CF2992FD3D41489E88F6DF4C912D (void);
// 0x00000B2B System.Void CrazyMinnow.SALSA.iClone.CM_iCloneSync::Initialize()
extern void CM_iCloneSync_Initialize_mE0F9FFC931B8A93D94F7C915AE7EF840CE36675E (void);
// 0x00000B2C System.Void CrazyMinnow.SALSA.iClone.CM_iCloneSync::GetSalsa3D()
extern void CM_iCloneSync_GetSalsa3D_m00A165A7B89DAAD00F6F14C4BD9837A73B73763B (void);
// 0x00000B2D System.Void CrazyMinnow.SALSA.iClone.CM_iCloneSync::GetRandomEyes3D()
extern void CM_iCloneSync_GetRandomEyes3D_m29881426D8636B87FAB436890DDC6387F8B8E095 (void);
// 0x00000B2E System.Void CrazyMinnow.SALSA.iClone.CM_iCloneSync::GetBody()
extern void CM_iCloneSync_GetBody_mE7678B3FD3ABB33C75D048128A65D8834963B03A (void);
// 0x00000B2F System.Void CrazyMinnow.SALSA.iClone.CM_iCloneSync::GetEyeBones()
extern void CM_iCloneSync_GetEyeBones_mDB35EFE8B591110B44E9B8D3B12B248CA3EB48A5 (void);
// 0x00000B30 System.Void CrazyMinnow.SALSA.iClone.CM_iCloneSync::GetJawBone()
extern void CM_iCloneSync_GetJawBone_mC413D10590F8ADFBA570ED6DF79DACE0CE459CCD (void);
// 0x00000B31 System.Void CrazyMinnow.SALSA.iClone.CM_iCloneSync::GetFacialHair()
extern void CM_iCloneSync_GetFacialHair_m6B2F1B418A39C766457738D30318B316B9D5AD90 (void);
// 0x00000B32 UnityEngine.Transform CrazyMinnow.SALSA.iClone.CM_iCloneSync::ChildSearch(System.String)
extern void CM_iCloneSync_ChildSearch_mD818EDB16815CD3E297C6B3C5C0BFA2D454A21E2 (void);
// 0x00000B33 System.Int32 CrazyMinnow.SALSA.iClone.CM_iCloneSync::ShapeSearch(UnityEngine.SkinnedMeshRenderer,System.String)
extern void CM_iCloneSync_ShapeSearch_m4F228DC2332D424477105137095E83C45727C48A (void);
// 0x00000B34 System.Int32 CrazyMinnow.SALSA.iClone.CM_iCloneSync::GetShapeNames()
extern void CM_iCloneSync_GetShapeNames_mE985429B07EF10CE88D4B9A8F935398392E60BF9 (void);
// 0x00000B35 System.Void CrazyMinnow.SALSA.iClone.CM_iCloneSync::SetDefaultSmall()
extern void CM_iCloneSync_SetDefaultSmall_m5B57A882E6414DDADF1D5C788CEE6EFE9C93D2D4 (void);
// 0x00000B36 System.Void CrazyMinnow.SALSA.iClone.CM_iCloneSync::SetDefaultMedium()
extern void CM_iCloneSync_SetDefaultMedium_mC07E375BD4067AB8AB87CB70D9C74EE4127DA6AB (void);
// 0x00000B37 System.Void CrazyMinnow.SALSA.iClone.CM_iCloneSync::SetDefaultLarge()
extern void CM_iCloneSync_SetDefaultLarge_mEB6409D3DA2D4C553C6C0369A1C9C47D25DEEF92 (void);
// 0x00000B38 System.Void CrazyMinnow.SALSA.iClone.CM_iCloneSync::.ctor()
extern void CM_iCloneSync__ctor_mAF27CFCAAB7822AFDD6485449A7C888C44F0D18A (void);
// 0x00000B39 System.Void CrazyMinnow.SALSA.iClone.CM_ShapeGroup::.ctor()
extern void CM_ShapeGroup__ctor_mE18DAC01C2CDAD39D31D272A8BFF76F855E43C38 (void);
// 0x00000B3A System.Void CrazyMinnow.SALSA.iClone.CM_ShapeGroup::.ctor(System.Int32,System.String,System.Single)
extern void CM_ShapeGroup__ctor_m751029B78E0AD02184BD2FDBDDBE394FF7292B67 (void);
// 0x00000B3B System.Void CrazyMinnow.SALSA.Daz.CM_DazSetup::Awake()
extern void CM_DazSetup_Awake_m4489069F702A529781A5D47BF44D6C2097E3D34E (void);
// 0x00000B3C System.Void CrazyMinnow.SALSA.Daz.CM_DazSetup::Setup()
extern void CM_DazSetup_Setup_mF2AB321B96FD24CA6043CF4FD72B13DFEF3A8E32 (void);
// 0x00000B3D System.Void CrazyMinnow.SALSA.Daz.CM_DazSetup::.ctor()
extern void CM_DazSetup__ctor_m73F760C2F7B8F6444C6A3264AF20EF65FDD29511 (void);
// 0x00000B3E System.Void CrazyMinnow.SALSA.Daz.CM_DazSync::Reset()
extern void CM_DazSync_Reset_m6860AA97431722F776D99118A8EC11BE22584889 (void);
// 0x00000B3F System.Void CrazyMinnow.SALSA.Daz.CM_DazSync::Start()
extern void CM_DazSync_Start_mCA5A30D8697139286E5F27AF8C0AF23406762CFC (void);
// 0x00000B40 System.Void CrazyMinnow.SALSA.Daz.CM_DazSync::LateUpdate()
extern void CM_DazSync_LateUpdate_m929CC296EBC20AF377166AB18A6A4B57DFD558DB (void);
// 0x00000B41 System.Void CrazyMinnow.SALSA.Daz.CM_DazSync::Initialize()
extern void CM_DazSync_Initialize_m7494DBF98A2BD6B882BF6F6C9B180B345FE467CA (void);
// 0x00000B42 System.Void CrazyMinnow.SALSA.Daz.CM_DazSync::SetCharacterType(CrazyMinnow.SALSA.Daz.CM_DazSync/DazType)
extern void CM_DazSync_SetCharacterType_mE260BD9574BB6203F38E19D225C3EFBF0C47E366 (void);
// 0x00000B43 System.Void CrazyMinnow.SALSA.Daz.CM_DazSync::GetSalsa3D()
extern void CM_DazSync_GetSalsa3D_m2E0ED23B56034A6BEA4AC4C4E6ACB4BA13DC25D8 (void);
// 0x00000B44 System.Void CrazyMinnow.SALSA.Daz.CM_DazSync::GetRandomEyes3D()
extern void CM_DazSync_GetRandomEyes3D_mCCE5B222DB98DF166920CD1705A31562FBD25C14 (void);
// 0x00000B45 System.Void CrazyMinnow.SALSA.Daz.CM_DazSync::GetSmr()
extern void CM_DazSync_GetSmr_m4706A33C61EF2F25D13263BF14BB159F1A4E00D1 (void);
// 0x00000B46 System.Void CrazyMinnow.SALSA.Daz.CM_DazSync::GetEyeBones()
extern void CM_DazSync_GetEyeBones_m45D4E34882FA25F276D1A9F8B641CF3039E41D18 (void);
// 0x00000B47 System.Void CrazyMinnow.SALSA.Daz.CM_DazSync::GetBlinkIndexes()
extern void CM_DazSync_GetBlinkIndexes_mD83B2DD25CD62A5A7D150374DC3CFE59A864FDF9 (void);
// 0x00000B48 UnityEngine.Transform CrazyMinnow.SALSA.Daz.CM_DazSync::ChildSearch(System.String)
extern void CM_DazSync_ChildSearch_mE5350B07551410C943056F378BA33D05FE3EB1F8 (void);
// 0x00000B49 System.Int32 CrazyMinnow.SALSA.Daz.CM_DazSync::ShapeSearch(UnityEngine.SkinnedMeshRenderer,System.String)
extern void CM_DazSync_ShapeSearch_m4CFDE5964658AA85CBC8E7EE8D2884462F0E5747 (void);
// 0x00000B4A System.Int32 CrazyMinnow.SALSA.Daz.CM_DazSync::GetShapeNames()
extern void CM_DazSync_GetShapeNames_m1BF29B0E6E6088E87F8748B1C4270B2AAE151F45 (void);
// 0x00000B4B System.Void CrazyMinnow.SALSA.Daz.CM_DazSync::SetDragonSmall()
extern void CM_DazSync_SetDragonSmall_m3A95E0F512AEBEAB0AECF61B91EC3380D4D0094C (void);
// 0x00000B4C System.Void CrazyMinnow.SALSA.Daz.CM_DazSync::SetDragonMedium()
extern void CM_DazSync_SetDragonMedium_mB4672796360DB513F76B9A4AF9E771E07E8E6DB8 (void);
// 0x00000B4D System.Void CrazyMinnow.SALSA.Daz.CM_DazSync::SetDragonLarge()
extern void CM_DazSync_SetDragonLarge_m1C471B55E2AAB0EC5526671EF0AFCB8D0E133448 (void);
// 0x00000B4E System.Void CrazyMinnow.SALSA.Daz.CM_DazSync::SetEmotiguySmall()
extern void CM_DazSync_SetEmotiguySmall_mC8648A6DBD945C9C86F9E7770F0CEFC545334EE4 (void);
// 0x00000B4F System.Void CrazyMinnow.SALSA.Daz.CM_DazSync::SetEmotiguyMedium()
extern void CM_DazSync_SetEmotiguyMedium_mDC70BCE9DEE3B1A8CF28C68B5007199CD258B5D8 (void);
// 0x00000B50 System.Void CrazyMinnow.SALSA.Daz.CM_DazSync::SetEmotiguyLarge()
extern void CM_DazSync_SetEmotiguyLarge_mF11D617804AB4F87AFC5B8365CCC5C4BA7E25DEB (void);
// 0x00000B51 System.Void CrazyMinnow.SALSA.Daz.CM_DazSync::SetGenesis1_2Small()
extern void CM_DazSync_SetGenesis1_2Small_m96772DFF69A39727E812DE39ABF7C1169883E099 (void);
// 0x00000B52 System.Void CrazyMinnow.SALSA.Daz.CM_DazSync::SetGenesis1_2Medium()
extern void CM_DazSync_SetGenesis1_2Medium_m193A30935A62AB735DD3D8A4E5AE330C28864644 (void);
// 0x00000B53 System.Void CrazyMinnow.SALSA.Daz.CM_DazSync::SetGenesis1_2Large()
extern void CM_DazSync_SetGenesis1_2Large_m81FF8D00CCA832523A7611550BD0DB8D23BF04A9 (void);
// 0x00000B54 System.Void CrazyMinnow.SALSA.Daz.CM_DazSync::SetGenesis3Small()
extern void CM_DazSync_SetGenesis3Small_m04E2981FFDEBCFC5ABFBAB6933DD95220B9BFC98 (void);
// 0x00000B55 System.Void CrazyMinnow.SALSA.Daz.CM_DazSync::SetGenesis3Medium()
extern void CM_DazSync_SetGenesis3Medium_m2344088B8722B43F86B9DC0E17AF00D5884B45D2 (void);
// 0x00000B56 System.Void CrazyMinnow.SALSA.Daz.CM_DazSync::SetGenesis3Large()
extern void CM_DazSync_SetGenesis3Large_m438E566EEDA201E57D5EE5C0B309188DDBD7ABAE (void);
// 0x00000B57 System.Void CrazyMinnow.SALSA.Daz.CM_DazSync::.ctor()
extern void CM_DazSync__ctor_mF0149BF5001C4688AA8D7A5AD9D98E84C554AAE4 (void);
// 0x00000B58 System.Void CrazyMinnow.SALSA.Daz.CM_ShapeGroup::.ctor()
extern void CM_ShapeGroup__ctor_m85CF1EA71D635B5EEC906ACD8030B9C8613EDA4A (void);
// 0x00000B59 System.Void CrazyMinnow.SALSA.Daz.CM_ShapeGroup::.ctor(System.Int32,System.String,System.Single)
extern void CM_ShapeGroup__ctor_mD2A4D78A12B0C527D8C7C31F08E142A4C86E2649 (void);
// 0x00000B5A System.Void CrazyMinnow.SALSA.Examples.CM_SalsaTypeAndObject::.ctor()
extern void CM_SalsaTypeAndObject__ctor_m38E49FDEFE4F1F6554C246B39EB358D27EADFFDE (void);
// 0x00000B5B System.Void CrazyMinnow.SALSA.Examples.CM_PlayerResponse::.ctor()
extern void CM_PlayerResponse__ctor_m5955D733568D75EBEB84CE280CA869120A2145E1 (void);
// 0x00000B5C System.Void CrazyMinnow.SALSA.Examples.CM_NPCDialog::.ctor()
extern void CM_NPCDialog__ctor_mA4A8363BCBA9A6D05696E8F05D0E6AAF1219792B (void);
// 0x00000B5D System.Void CrazyMinnow.SALSA.Examples.CM_DialogSystem::Start()
extern void CM_DialogSystem_Start_m31EB6326D13B2966F01076D96873FC15FE250FD1 (void);
// 0x00000B5E System.Void CrazyMinnow.SALSA.Examples.CM_DialogSystem::Salsa_OnTalkStatusChanged(CrazyMinnow.SALSA.SalsaStatus)
extern void CM_DialogSystem_Salsa_OnTalkStatusChanged_mA370E66FA39F18F91B2AF53BFD88F905ED745E1F (void);
// 0x00000B5F System.Void CrazyMinnow.SALSA.Examples.CM_DialogSystem::OnGUI()
extern void CM_DialogSystem_OnGUI_m6A8ACDB63FA544F2381CE3D6C38836F70AC76E5A (void);
// 0x00000B60 CrazyMinnow.SALSA.Examples.CM_SalsaTypeAndObject CrazyMinnow.SALSA.Examples.CM_DialogSystem::GetSalsaType(UnityEngine.GameObject)
extern void CM_DialogSystem_GetSalsaType_m01A03E2A5CD97B46F2500D4D2345A3C27F5361C0 (void);
// 0x00000B61 System.Void CrazyMinnow.SALSA.Examples.CM_DialogSystem::EndDialog()
extern void CM_DialogSystem_EndDialog_m6ED7C481237180BEBA202E8862A5799F6C3ECFAE (void);
// 0x00000B62 System.Void CrazyMinnow.SALSA.Examples.CM_DialogSystem::ResetDialog()
extern void CM_DialogSystem_ResetDialog_mB2CAF989803B41E4BC09611C7CB7897B3E6051F9 (void);
// 0x00000B63 System.Void CrazyMinnow.SALSA.Examples.CM_DialogSystem::.ctor()
extern void CM_DialogSystem__ctor_mCDE140BEF61EF907A420705A1D330918BB5A26FE (void);
// 0x00000B64 System.Void CrazyMinnow.SALSA.Examples.CM_Ethan_Demo::Start()
extern void CM_Ethan_Demo_Start_m2E0F849F7A61FE235F6C6035B5C73AE4C7F73639 (void);
// 0x00000B65 System.Void CrazyMinnow.SALSA.Examples.CM_Ethan_Demo::Salsa_OnTalkStatusChanged(CrazyMinnow.SALSA.SalsaStatus)
extern void CM_Ethan_Demo_Salsa_OnTalkStatusChanged_mA301C2A72432F1BAF086677EA853AEA3517C674F (void);
// 0x00000B66 System.Void CrazyMinnow.SALSA.Examples.CM_Ethan_Demo::RandomEyes_OnLookStatusChanged(CrazyMinnow.SALSA.RandomEyesLookStatus)
extern void CM_Ethan_Demo_RandomEyes_OnLookStatusChanged_m92C2BD557973EA3A296D2F1ACB6E659EF19196F4 (void);
// 0x00000B67 System.Void CrazyMinnow.SALSA.Examples.CM_Ethan_Demo::RandomEyes_OnCustomShapeChanged(CrazyMinnow.SALSA.RandomEyesCustomShapeStatus)
extern void CM_Ethan_Demo_RandomEyes_OnCustomShapeChanged_m51E6EFB22DD272DF752C22B5CA09A1A574CE928C (void);
// 0x00000B68 System.Collections.IEnumerator CrazyMinnow.SALSA.Examples.CM_Ethan_Demo::Look(System.Single,System.Single,UnityEngine.GameObject)
extern void CM_Ethan_Demo_Look_m1894D23E2740CBCCD2156CE1B6292FAF526C4B86 (void);
// 0x00000B69 System.Collections.IEnumerator CrazyMinnow.SALSA.Examples.CM_Ethan_Demo::WaitStart(System.Single)
extern void CM_Ethan_Demo_WaitStart_m245B82125413DCD972E3C46AC88B470159EE2FB8 (void);
// 0x00000B6A System.Void CrazyMinnow.SALSA.Examples.CM_Ethan_Demo::.ctor()
extern void CM_Ethan_Demo__ctor_mE9CC699AAC92CCECC362D8D756B584FC3B1D9FD6 (void);
// 0x00000B6B System.Void CrazyMinnow.SALSA.Examples.CM_Ethan_Demo/<Look>d__11::.ctor(System.Int32)
extern void U3CLookU3Ed__11__ctor_m0C0602C6D13FD98A1C3E9861D60728A2A878067E (void);
// 0x00000B6C System.Void CrazyMinnow.SALSA.Examples.CM_Ethan_Demo/<Look>d__11::System.IDisposable.Dispose()
extern void U3CLookU3Ed__11_System_IDisposable_Dispose_mB065D40394D3306D926295AA04E857E4E893AEA6 (void);
// 0x00000B6D System.Boolean CrazyMinnow.SALSA.Examples.CM_Ethan_Demo/<Look>d__11::MoveNext()
extern void U3CLookU3Ed__11_MoveNext_mD8401698B9543AAD5E237E8FBAEE01930B468C8A (void);
// 0x00000B6E System.Object CrazyMinnow.SALSA.Examples.CM_Ethan_Demo/<Look>d__11::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CLookU3Ed__11_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mEAB61CB937BA80B253726650D1A70446F99DC62A (void);
// 0x00000B6F System.Void CrazyMinnow.SALSA.Examples.CM_Ethan_Demo/<Look>d__11::System.Collections.IEnumerator.Reset()
extern void U3CLookU3Ed__11_System_Collections_IEnumerator_Reset_mCD70572400F9FAA327104E951FEA60B7D9B27CC3 (void);
// 0x00000B70 System.Object CrazyMinnow.SALSA.Examples.CM_Ethan_Demo/<Look>d__11::System.Collections.IEnumerator.get_Current()
extern void U3CLookU3Ed__11_System_Collections_IEnumerator_get_Current_m2AB41E6148A13C6D813F3A3645D3805BB866D473 (void);
// 0x00000B71 System.Void CrazyMinnow.SALSA.Examples.CM_Ethan_Demo/<WaitStart>d__12::.ctor(System.Int32)
extern void U3CWaitStartU3Ed__12__ctor_mAD648999ABEAFD059CB1F0FB307CF014C265E18E (void);
// 0x00000B72 System.Void CrazyMinnow.SALSA.Examples.CM_Ethan_Demo/<WaitStart>d__12::System.IDisposable.Dispose()
extern void U3CWaitStartU3Ed__12_System_IDisposable_Dispose_m6283E332BA2CCB8F440B18136D5D9DD864B21288 (void);
// 0x00000B73 System.Boolean CrazyMinnow.SALSA.Examples.CM_Ethan_Demo/<WaitStart>d__12::MoveNext()
extern void U3CWaitStartU3Ed__12_MoveNext_m61697352AFE8FD2EFAE783B65E98CAEFF9118E79 (void);
// 0x00000B74 System.Object CrazyMinnow.SALSA.Examples.CM_Ethan_Demo/<WaitStart>d__12::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CWaitStartU3Ed__12_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m55777F556FD9449F18A3B85AD0A98F1FCFA16C55 (void);
// 0x00000B75 System.Void CrazyMinnow.SALSA.Examples.CM_Ethan_Demo/<WaitStart>d__12::System.Collections.IEnumerator.Reset()
extern void U3CWaitStartU3Ed__12_System_Collections_IEnumerator_Reset_m7499B509460A4F30948CFBAAEB6BCEA36FBE6C16 (void);
// 0x00000B76 System.Object CrazyMinnow.SALSA.Examples.CM_Ethan_Demo/<WaitStart>d__12::System.Collections.IEnumerator.get_Current()
extern void U3CWaitStartU3Ed__12_System_Collections_IEnumerator_get_Current_m177A5FEE4EC60BE618906AA734331936C5AF9D8D (void);
// 0x00000B77 System.Void CrazyMinnow.SALSA.Examples.CM_GameManager::OnGUI()
extern void CM_GameManager_OnGUI_m8E4DB48B486264D193B6E2C96D820E779BC496D5 (void);
// 0x00000B78 System.Void CrazyMinnow.SALSA.Examples.CM_GameManager::.ctor()
extern void CM_GameManager__ctor_m6A806AC95F833BBDFFB1FBFB641ECFC5EB14737F (void);
// 0x00000B79 System.Void CrazyMinnow.SALSA.Examples.CM_RandomEyes2D_Functions::Start()
extern void CM_RandomEyes2D_Functions_Start_m5FF21EDFB2D31BB4FD401320FEC8180733CBD22C (void);
// 0x00000B7A System.Void CrazyMinnow.SALSA.Examples.CM_RandomEyes2D_Functions::OnGUI()
extern void CM_RandomEyes2D_Functions_OnGUI_m2A34E0A12FEA0F287B2635BE13BC3C9A76010E32 (void);
// 0x00000B7B System.Void CrazyMinnow.SALSA.Examples.CM_RandomEyes2D_Functions::.ctor()
extern void CM_RandomEyes2D_Functions__ctor_m8F6326C30BC3124C0486C64049EAFF5F51E918D2 (void);
// 0x00000B7C System.Void CrazyMinnow.SALSA.Examples.CM_RandomEyes3D_Functions::Start()
extern void CM_RandomEyes3D_Functions_Start_m2F4DC0D3F6FA2B3B191ECEFE4CC260E735181538 (void);
// 0x00000B7D System.Void CrazyMinnow.SALSA.Examples.CM_RandomEyes3D_Functions::OnGUI()
extern void CM_RandomEyes3D_Functions_OnGUI_m58ED8F8F282B2EC582C12146488B77C7AB8D9745 (void);
// 0x00000B7E System.Void CrazyMinnow.SALSA.Examples.CM_RandomEyes3D_Functions::.ctor()
extern void CM_RandomEyes3D_Functions__ctor_m0A5D5308C1C812957E2E165D4216DDD7295DF7C6 (void);
// 0x00000B7F System.Void CrazyMinnow.SALSA.Examples.CM_RandomEyesBroadcastEventTester::RandomEyes_OnLookStatusChanged(CrazyMinnow.SALSA.RandomEyesLookStatus)
extern void CM_RandomEyesBroadcastEventTester_RandomEyes_OnLookStatusChanged_mEC096F1AC75E7D3B0383C67F431BA257C0521759 (void);
// 0x00000B80 System.Void CrazyMinnow.SALSA.Examples.CM_RandomEyesBroadcastEventTester::RandomEyes_OnCustomShapeChanged(CrazyMinnow.SALSA.RandomEyesCustomShapeStatus)
extern void CM_RandomEyesBroadcastEventTester_RandomEyes_OnCustomShapeChanged_m365512A792F3BAE7E7C1DC233B905721132D7F62 (void);
// 0x00000B81 System.Void CrazyMinnow.SALSA.Examples.CM_RandomEyesBroadcastEventTester::.ctor()
extern void CM_RandomEyesBroadcastEventTester__ctor_m5B156E4D9D7CF6BBBBF66558F97BC8D8E24269BB (void);
// 0x00000B82 System.Void CrazyMinnow.SALSA.Examples.CM_RandomEyesCustomShapeIterator::Start()
extern void CM_RandomEyesCustomShapeIterator_Start_m6D54A0CF79E951046C07D03E5289323D159209E9 (void);
// 0x00000B83 System.Void CrazyMinnow.SALSA.Examples.CM_RandomEyesCustomShapeIterator::RandomEyes_OnCustomShapeChanged(CrazyMinnow.SALSA.RandomEyesCustomShapeStatus)
extern void CM_RandomEyesCustomShapeIterator_RandomEyes_OnCustomShapeChanged_m3411D524807635764DFCA3DB2F2EC2D7AE3683A7 (void);
// 0x00000B84 System.Void CrazyMinnow.SALSA.Examples.CM_RandomEyesCustomShapeIterator::.ctor()
extern void CM_RandomEyesCustomShapeIterator__ctor_m91F3D95FD998AFBE3B6036AECDDABC9A948CEDC3 (void);
// 0x00000B85 System.Void CrazyMinnow.SALSA.Examples.CM_RandomEyesTriggerTracking::Start()
extern void CM_RandomEyesTriggerTracking_Start_mCF41787D78B878D78C9F306A8404676D5ED9EE37 (void);
// 0x00000B86 System.Void CrazyMinnow.SALSA.Examples.CM_RandomEyesTriggerTracking::OnTriggerEnter(UnityEngine.Collider)
extern void CM_RandomEyesTriggerTracking_OnTriggerEnter_mE3CB4C2C83686EBBF79B29E097A706EB70E30909 (void);
// 0x00000B87 System.Void CrazyMinnow.SALSA.Examples.CM_RandomEyesTriggerTracking::OnTriggerExit(UnityEngine.Collider)
extern void CM_RandomEyesTriggerTracking_OnTriggerExit_mE0B932AFB67E07DE3942D9CE3AE8C743662F5054 (void);
// 0x00000B88 System.Void CrazyMinnow.SALSA.Examples.CM_RandomEyesTriggerTracking::.ctor()
extern void CM_RandomEyesTriggerTracking__ctor_m462BE0E2E1C46B2F3DCF9D9B5086938B200F0138 (void);
// 0x00000B89 System.Void CrazyMinnow.SALSA.Examples.CM_Salsa2D_Functions::Start()
extern void CM_Salsa2D_Functions_Start_m7E9E4BC3706BE503279BBC1405750122AF6DA777 (void);
// 0x00000B8A System.Void CrazyMinnow.SALSA.Examples.CM_Salsa2D_Functions::OnGUI()
extern void CM_Salsa2D_Functions_OnGUI_mAEF661961367C6B8D6144B281169D86A0E139F4D (void);
// 0x00000B8B System.Void CrazyMinnow.SALSA.Examples.CM_Salsa2D_Functions::.ctor()
extern void CM_Salsa2D_Functions__ctor_mC0F866956C211BD7A60A740BB2309274661DBFA5 (void);
// 0x00000B8C System.Void CrazyMinnow.SALSA.Examples.CM_Salsa3D_Functions::Start()
extern void CM_Salsa3D_Functions_Start_m2D8E3DD15E10EFA97874692ED7AB8FC352297197 (void);
// 0x00000B8D System.Void CrazyMinnow.SALSA.Examples.CM_Salsa3D_Functions::OnGUI()
extern void CM_Salsa3D_Functions_OnGUI_m44DB6B566F81633B2A47C0349653A9A2C34BF3CA (void);
// 0x00000B8E System.Void CrazyMinnow.SALSA.Examples.CM_Salsa3D_Functions::.ctor()
extern void CM_Salsa3D_Functions__ctor_m1EE368AB5883C5179B6BBFCC69CB286506AC370E (void);
// 0x00000B8F System.Void CrazyMinnow.SALSA.Examples.CM_SalsaBroadcastEventTester::Salsa_OnTalkStatusChanged(CrazyMinnow.SALSA.SalsaStatus)
extern void CM_SalsaBroadcastEventTester_Salsa_OnTalkStatusChanged_mF50CAE7CB454B6051DE779173A8F94981D9C237A (void);
// 0x00000B90 System.Void CrazyMinnow.SALSA.Examples.CM_SalsaBroadcastEventTester::.ctor()
extern void CM_SalsaBroadcastEventTester__ctor_mF12D465360B6D0FD1F5C7F47ECCAEAFC3E77EF6C (void);
// 0x00000B91 System.Void CrazyMinnow.SALSA.Examples.CM_SalsaWaypointTriggers::.ctor()
extern void CM_SalsaWaypointTriggers__ctor_m0E09DA7A3FEE0BD10650089CC2B95D1B87443294 (void);
// 0x00000B92 System.Void CrazyMinnow.SALSA.Examples.CM_SalsaWaypoints::Start()
extern void CM_SalsaWaypoints_Start_mD084D3CFDFF746D0F8616EF6AE7E2C30E8EDD3D4 (void);
// 0x00000B93 System.Void CrazyMinnow.SALSA.Examples.CM_SalsaWaypoints::Update()
extern void CM_SalsaWaypoints_Update_m70B4AE34E82DB01A0156FE281BD5795B74AC32F3 (void);
// 0x00000B94 System.Void CrazyMinnow.SALSA.Examples.CM_SalsaWaypoints::Salsa_OnTalkStatusChanged(CrazyMinnow.SALSA.SalsaStatus)
extern void CM_SalsaWaypoints_Salsa_OnTalkStatusChanged_m3BB90E7D6192836D6057E57B5DAB6B9AD85A818F (void);
// 0x00000B95 System.Void CrazyMinnow.SALSA.Examples.CM_SalsaWaypoints::SetWaypoint(System.Int32)
extern void CM_SalsaWaypoints_SetWaypoint_m964E61F4B5A8F145A047D0400164B3F40C2B9350 (void);
// 0x00000B96 System.Void CrazyMinnow.SALSA.Examples.CM_SalsaWaypoints::SetSpeed(System.Single)
extern void CM_SalsaWaypoints_SetSpeed_mEDEFC56B21AA7457B0A62BE4AAAF36763D475101 (void);
// 0x00000B97 System.Void CrazyMinnow.SALSA.Examples.CM_SalsaWaypoints::ResetSalsaWaypoints()
extern void CM_SalsaWaypoints_ResetSalsaWaypoints_m00492355368B60AD1D5B981C1DA26532473B2D21 (void);
// 0x00000B98 System.Void CrazyMinnow.SALSA.Examples.CM_SalsaWaypoints::.ctor()
extern void CM_SalsaWaypoints__ctor_mBE46E4BAC07AA3C6CC75EF190CD4B7C6DAE98D6E (void);
// 0x00000B99 System.Void CrazyMinnow.SALSA.Examples.CM_WaypointItems::.ctor()
extern void CM_WaypointItems__ctor_m0B308F6D34E3D36C6C691A74AA5262F18E497EE3 (void);
// 0x00000B9A System.Void CrazyMinnow.SALSA.Examples.CM_Waypoints::Start()
extern void CM_Waypoints_Start_mEA3A1FF43D43B79A0CC6C6F15A25FEF7E22A3482 (void);
// 0x00000B9B System.Void CrazyMinnow.SALSA.Examples.CM_Waypoints::Update()
extern void CM_Waypoints_Update_m59C02ECEFC66282ECAAFD38DE94E1EFD49DAA203 (void);
// 0x00000B9C System.Void CrazyMinnow.SALSA.Examples.CM_Waypoints::WaypointCheck()
extern void CM_Waypoints_WaypointCheck_m3103A033B72AD0FEF95EB6854CE2A3EB6D514A64 (void);
// 0x00000B9D System.Void CrazyMinnow.SALSA.Examples.CM_Waypoints::SetAnimationType(CrazyMinnow.SALSA.Examples.CM_Waypoints/AnimationType)
extern void CM_Waypoints_SetAnimationType_mC96C82F12FF08A385BE3E471F987CD9132E479AA (void);
// 0x00000B9E System.Void CrazyMinnow.SALSA.Examples.CM_Waypoints::.ctor()
extern void CM_Waypoints__ctor_mE92D295A4B0D8CB69817CF60037B57FEDAA3E9FD (void);
// 0x00000B9F System.Void CrazyMinnow.SALSA.RTVoice.Salsa_RTVoice::Awake()
extern void Salsa_RTVoice_Awake_m33BF6931483E0A99E751810DD198135F9087594D (void);
// 0x00000BA0 System.Void CrazyMinnow.SALSA.RTVoice.Salsa_RTVoice::LateUpdate()
extern void Salsa_RTVoice_LateUpdate_m499E26FA766E02BC9A20A1F2836A9B11B191A0A4 (void);
// 0x00000BA1 System.Void CrazyMinnow.SALSA.RTVoice.Salsa_RTVoice::.ctor()
extern void Salsa_RTVoice__ctor_mE3A2BEF591FE93DD763DE5B4927DB8478F05076B (void);
// 0x00000BA2 System.Void CrazyMinnow.SALSA.RTVoice.Salsa_RTVoice_Native::OnEnable()
extern void Salsa_RTVoice_Native_OnEnable_m191C846DE66FD3FC91B6F35FE468169CE647B0F1 (void);
// 0x00000BA3 System.Void CrazyMinnow.SALSA.RTVoice.Salsa_RTVoice_Native::OnDisable()
extern void Salsa_RTVoice_Native_OnDisable_mA6BC7E9A718F67939F9EAB522145E93B40ED1179 (void);
// 0x00000BA4 System.Void CrazyMinnow.SALSA.RTVoice.Salsa_RTVoice_Native::OnDestroy()
extern void Salsa_RTVoice_Native_OnDestroy_mC616E5F1F74316186E039DF52BBA562E2B625518 (void);
// 0x00000BA5 System.Void CrazyMinnow.SALSA.RTVoice.Salsa_RTVoice_Native::Speaker_OnSpeakCurrentViseme(Crosstales.RTVoice.Model.Wrapper,System.String)
extern void Salsa_RTVoice_Native_Speaker_OnSpeakCurrentViseme_m4836938D966FA3CDB69AEE630A68EDFF40A87F93 (void);
// 0x00000BA6 System.Void CrazyMinnow.SALSA.RTVoice.Salsa_RTVoice_Native::Awake()
extern void Salsa_RTVoice_Native_Awake_m76AB606F34ECE2F59333F1EC0D11FAC70FADFAC5 (void);
// 0x00000BA7 System.Void CrazyMinnow.SALSA.RTVoice.Salsa_RTVoice_Native::LateUpdate()
extern void Salsa_RTVoice_Native_LateUpdate_m95C72053DD1FFE0D3F2A1840FEAC92125ACD8DAB (void);
// 0x00000BA8 System.Void CrazyMinnow.SALSA.RTVoice.Salsa_RTVoice_Native::.ctor()
extern void Salsa_RTVoice_Native__ctor_m61170D69EE4DA84AF50E3CAF0D782C1EB6BB143B (void);
// 0x00000BA9 System.Void CrazyMinnow.SALSA.AmplitudeWebGL.AmplitudeSALSA::Reset()
extern void AmplitudeSALSA_Reset_m25E64A5391C665AE5C914221B02A64E0EC6A7AE7 (void);
// 0x00000BAA System.Void CrazyMinnow.SALSA.AmplitudeWebGL.AmplitudeSALSA::Start()
extern void AmplitudeSALSA_Start_m812CFF867CA78BB09F672824EB1BF587486927BF (void);
// 0x00000BAB System.Collections.IEnumerator CrazyMinnow.SALSA.AmplitudeWebGL.AmplitudeSALSA::UpdateSalsa()
extern void AmplitudeSALSA_UpdateSalsa_m30EF64E1901B9844290E461FCDBD5F3F5C10229B (void);
// 0x00000BAC System.Void CrazyMinnow.SALSA.AmplitudeWebGL.AmplitudeSALSA::SetSalsaType(CrazyMinnow.SALSA.AmplitudeWebGL.AmplitudeSALSA/SalsaType)
extern void AmplitudeSALSA_SetSalsaType_m79323879203598B46148C3C464629AC09138DF5C (void);
// 0x00000BAD System.Void CrazyMinnow.SALSA.AmplitudeWebGL.AmplitudeSALSA::FindAmplitude()
extern void AmplitudeSALSA_FindAmplitude_m36E43773DDD23EEBAF550C534737F7069A76E954 (void);
// 0x00000BAE System.Void CrazyMinnow.SALSA.AmplitudeWebGL.AmplitudeSALSA::.ctor()
extern void AmplitudeSALSA__ctor_mCFCDD70B0AD3F39E464E2026A7107D719191B99E (void);
// 0x00000BAF System.Void CrazyMinnow.SALSA.AmplitudeWebGL.AmplitudeSALSA/<UpdateSalsa>d__9::.ctor(System.Int32)
extern void U3CUpdateSalsaU3Ed__9__ctor_m9E79A0DBE4A266CD6F3EFDD9915425F97D9E4691 (void);
// 0x00000BB0 System.Void CrazyMinnow.SALSA.AmplitudeWebGL.AmplitudeSALSA/<UpdateSalsa>d__9::System.IDisposable.Dispose()
extern void U3CUpdateSalsaU3Ed__9_System_IDisposable_Dispose_m75FE562BE04E0C9573F9F48D24AAF8365EE167DC (void);
// 0x00000BB1 System.Boolean CrazyMinnow.SALSA.AmplitudeWebGL.AmplitudeSALSA/<UpdateSalsa>d__9::MoveNext()
extern void U3CUpdateSalsaU3Ed__9_MoveNext_m1C84433A452B1F41CEAC17E439DFC20C3B18A766 (void);
// 0x00000BB2 System.Object CrazyMinnow.SALSA.AmplitudeWebGL.AmplitudeSALSA/<UpdateSalsa>d__9::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CUpdateSalsaU3Ed__9_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m3C11D377A669748A16C41925BE8009627AD31B44 (void);
// 0x00000BB3 System.Void CrazyMinnow.SALSA.AmplitudeWebGL.AmplitudeSALSA/<UpdateSalsa>d__9::System.Collections.IEnumerator.Reset()
extern void U3CUpdateSalsaU3Ed__9_System_Collections_IEnumerator_Reset_m5E33580B54E8D416AD732AE2CB200958FD929F97 (void);
// 0x00000BB4 System.Object CrazyMinnow.SALSA.AmplitudeWebGL.AmplitudeSALSA/<UpdateSalsa>d__9::System.Collections.IEnumerator.get_Current()
extern void U3CUpdateSalsaU3Ed__9_System_Collections_IEnumerator_get_Current_m957517B4EE9E38BEA9FAA9671E7D504AF61A9439 (void);
// 0x00000BB5 System.UInt32 <PrivateImplementationDetails>::ComputeStringHash(System.String)
extern void U3CPrivateImplementationDetailsU3E_ComputeStringHash_mD94B0E22EF32AD3DFD277ED8E911B5DFA4CDB91E (void);
static Il2CppMethodPointer s_methodPointers[2997] = 
{
	WebAudioActionExample_Start_m520A7BF8B9F6C1C6722AE202F18DE074A5D22A53,
	WebAudioActionExample_Update_mA50491CC6D1CC38B73BBD40773DE96592A818D31,
	WebAudioActionExample_delayAudioWeb_m1E19E01C2B6F78F3A6DACA39E36E0B0E2A9833A2,
	WebAudioActionExample_playMyAudio_m5AA4ABB566730A4C6DC41415D0B0C2631535A359,
	WebAudioActionExample_ToggleFetch_m7902D8850B518B6422F52226A1C6F387B86E2825,
	WebAudioActionExample_GetAudioClip_m75452A554F88957E8EF4A1E68A81A0A5ECCB4595,
	WebAudioActionExample_OnSuccess_m2F9BAD7CF8313944778889FF36E30066F4FD304C,
	WebAudioActionExample_OnFailed_mC5BA7C0AE6097ADF28A4ED86217A437FD0619D3E,
	WebAudioActionExample__ctor_m2E0C9889A785EA88BB2E814A39C2F749B22CCBA4,
	U3CdelayAudioWebU3Ed__14__ctor_m3AA6C070450EF01955EF5D793E777B5552E71000,
	U3CdelayAudioWebU3Ed__14_System_IDisposable_Dispose_m57DE3ECAF415939BA57B92E8314593062F3D2A78,
	U3CdelayAudioWebU3Ed__14_MoveNext_mAD2B0B36E401C812B966270B37C6786FCC5E4F0D,
	U3CdelayAudioWebU3Ed__14_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m433CB2171CF6E1957477168B3CC8AC3701B90158,
	U3CdelayAudioWebU3Ed__14_System_Collections_IEnumerator_Reset_m72A7F23479569D7C4F4F1A9C8F77BF8470D66B89,
	U3CdelayAudioWebU3Ed__14_System_Collections_IEnumerator_get_Current_m024134DB8FAE671E8FA571CA0748217221FA4FC5,
	WebAudioBatchExample_Start_m1CEC8884AE94B9C92EB9C4A01F7A0CB69044F7EF,
	WebAudioBatchExample_Update_mF3AB99FCE85A61839726DE55781CE4049330B057,
	WebAudioBatchExample_PushAudioUrlToQueue_m64F0984821D402B4EF538DDFA624FB13754D8B4C,
	WebAudioBatchExample_GetAudioClip_mE1FAFEA41FEF737F6C8DBA1D653AC0E7051C013F,
	WebAudioBatchExample_OnSuccess_mD019682A9C1CAB60CEF1079E71CD887391761520,
	WebAudioBatchExample_OnFailed_m1D9B4B9B9146284B54A07FFBC7749D4E47B08F55,
	WebAudioBatchExample__ctor_m37516A360B4E15CBE3712D183E84C91407A177CA,
	SALSA_Template_EventControllerSubscriber_OnEnable_mB6649D7D3E3A026EEA456196939D6A4B64FC921E,
	SALSA_Template_EventControllerSubscriber_OnDisable_m416FA6A8104B86BF45BCDBF391C5233C8033AE9E,
	SALSA_Template_EventControllerSubscriber_OnAnimationStarting_mB08B03CF07F2639CC08354B819EFF7A7EFCDF660,
	SALSA_Template_EventControllerSubscriber_OnAnimationON_m52E405BC8FA4F0032C9778ECA538884B5D6A380E,
	SALSA_Template_EventControllerSubscriber_OnAnimationEnding_m97D0D9EEC53BB13FB3A84A8A2DB9834FA902A4C0,
	SALSA_Template_EventControllerSubscriber_OnAnimationOFF_m10571050F35F15FCD9DA77050325BDD983342478,
	SALSA_Template_EventControllerSubscriber__ctor_m53D942182B15797AE2684742632DF341377082F4,
	SALSA_Template_SalsaEventSubscriber_OnEnable_m94A019D5443ED1B1AD2592265517A745F71AD09F,
	SALSA_Template_SalsaEventSubscriber_OnDisable_mA29752E8EF1E71DA7A4F41C316F487FA050DEE80,
	SALSA_Template_SalsaEventSubscriber_OnStoppedSalsaing_mFE4FE659CB7BE04B16A6D951990B69D8135365FF,
	SALSA_Template_SalsaEventSubscriber_OnStartedSalsaing_m99DED93FD77CF6D8A72D84DEACAD0B903B1DBFD9,
	SALSA_Template_SalsaEventSubscriber__ctor_mD8ACCD47157F8DBD752790095821DE39A7FB9C34,
	SALSA_Template_SalsaVisemeTriggerEventSubscriber_OnEnable_m00DC9D18B5E991800A35552CECC62B8C4902F9B2,
	SALSA_Template_SalsaVisemeTriggerEventSubscriber_OnDisable_mC4549D9BFAB298009D7990E173CC5DA8AABAEAA6,
	SALSA_Template_SalsaVisemeTriggerEventSubscriber_SalsaOnVisemeTriggered_m2A6D575DE39D79FA7B2676F54712CEF650A97201,
	SALSA_Template_SalsaVisemeTriggerEventSubscriber__ctor_m75629EB0AFBC47F4E066FD0F4F570E3283947DF7,
	Breathe_Start_m8D4C7456E8CFD5AE95F03929963046E0FB88C29F,
	Breathe_Update_m95AAD266506234658D72A172EA47AF4A42496A81,
	Breathe__ctor_mC71EAC917BF49532042E70D3EA8446E39D51E130,
	LightMover_Start_mD72AE2058B16ECC907FA2F94428F45E9911C1B7A,
	LightMover_Update_m80CD35B5C65D116E8B977382A5843C549449816B,
	LightMover_Rando_m2335CCCA2B020B0478A0FDC8E93A7A1C091FCACC,
	LightMover__ctor_m8ACFB59609F815667BCC20468A4A4BAC0B61320F,
	U3CRandoU3Ed__5__ctor_m1403ED7FD2349C2CB16CEA7D4C00520A38CA3EEC,
	U3CRandoU3Ed__5_System_IDisposable_Dispose_m73AE8C4BF7C348A094C55ADD9634534E5B081270,
	U3CRandoU3Ed__5_MoveNext_m1B3060CE052AED0659E1A3715106527C2FC30E0A,
	U3CRandoU3Ed__5_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m1AE9A7A8229C4C92D44B2DBDA606E5D5DECE43A5,
	U3CRandoU3Ed__5_System_Collections_IEnumerator_Reset_m1A73670DB4DF628509B735CBEA3A26E070FF0DA6,
	U3CRandoU3Ed__5_System_Collections_IEnumerator_get_Current_mC4E9F4D960D2E0EA3109670068E88A32274C2EA6,
	LookAt_Start_m17791867B3D5AC7D09956B53884C99059A44C69E,
	LookAt_Update_mF3460A983189AE9E63FAEE1C2E719D80BBFD4499,
	LookAt__ctor_m794401EFA0AB058C33C45BA86E49541B3B0F39DC,
	MapChanger_Start_m4DF9217889BEE970359C440CE49D689D29D66500,
	MapChanger_Update_m0EF2ACD3BCCD4051E5343CD7BAFBCA0B8FF10904,
	MapChanger_OnGUI_m068D591CA8FD71CDDA062DA8BFD41AD6EFA94219,
	MapChanger__ctor_m0C7D09A7F04868AC6DFB47E3C27EF9F84764D152,
	Probe_Awake_m89C4BBC848164D7B93EA1ED97936369DCBB5B676,
	Probe_Update_m70C8F77A4CE9A3B50E4B9CA6897291B09CD7F094,
	Probe__ctor_m2CE9B908BD06F078539259E005220E7AB1C60572,
	AudioTools_IncreaseVolume_m688AC39B7537437623CC2764353363A4F5D05952,
	mYmENU_Start_m8443DA7EB9B0BA77D4989139BDFE338E95DD0C57,
	mYmENU_Update_mD9465CD472EDB105002B8E40A8021E66D2DCF5DA,
	mYmENU_LoadScene_mB44D21237A663F784033BD74F1AC0BE6CAD34DAE,
	mYmENU__ctor_mC2C1DC644C5EF3A881E92A6F4BB58B0CE904551D,
	ExampleWheelController_Start_mAD76D68B46BA3AD276D093C1DAB4D8C781C9419A,
	ExampleWheelController_Update_mBFE66DA1650BB9834C338777827D9E93E5165DFA,
	ExampleWheelController__ctor_mE2773CAA6EF9FFDDC7312C1F287DB441F791695C,
	Uniforms__cctor_m718B59E42C2DEAF29E07EC5506DACBE52FCB5746,
	MainScript_LogMessage_m7953CB447ABFC4D0E286219425CF26684DB6F6B0,
	MainScript_Get_m3BD5249433D12EE1562CB113FF95FD302207B2A0,
	MainScript_Post_mF7C21819A39C80CBEF1E5471FF7A505A6CF9B687,
	MainScript_Put_m2E077AE7D135014788A9F219D4FA32B618BED552,
	MainScript_Delete_m3CEEF47B740A70E766D3CF74E6C1D37E13C0A8C8,
	MainScript_AbortRequest_mDCEC8008FD946FB21C66D8DE04AA652FD091AC5E,
	MainScript_DownloadFile_mF54C07D06969AD450838E27A885FB450BDA6971C,
	MainScript__ctor_mDE68DE7EC111F10F50287CE15AC2100BAC26DF23,
	MainScript_U3CPostU3Eb__4_0_m8B0202AA78218D3787871313668C76F260A52D42,
	MainScript_U3CPostU3Eb__4_1_m2C8C662A8A4EE9A219C715C38E88E9D156CC0777,
	MainScript_U3CPutU3Eb__5_0_m556845DFA7BB10FCFF4A3A04B17EF6BC8AA77CAE,
	MainScript_U3CDeleteU3Eb__6_0_m3092FBC0D31C4B83C0C7E537BFD404FD28F0BB8E,
	MainScript_U3CDownloadFileU3Eb__8_0_m8D61A6C39FFBB97884CF0935DA9BF297B5DE8393,
	MainScript_U3CDownloadFileU3Eb__8_1_m99B4D340A42D8D8B784669EDB99B721CAD853FB6,
	U3CU3Ec__DisplayClass3_0__ctor_m46C50E0A5C12FA151CC55E0B928E27838925B4FC,
	U3CU3Ec__DisplayClass3_0_U3CGetU3Eb__0_m1E95C1F0B2D8E5BC302A5ADCA1ABAE0293AEA82D,
	U3CU3Ec__DisplayClass3_0_U3CGetU3Eb__1_mB5A9CA352F06FB80FDC47E6FE114495BA4687AC1,
	U3CU3Ec__DisplayClass3_0_U3CGetU3Eb__2_mB93ACE707409C85F0542FD7665B0BF499473D98D,
	U3CU3Ec__DisplayClass3_0_U3CGetU3Eb__3_m381C9E851E8FB988B02A1076B749138ABC954BDB,
	U3CU3Ec__DisplayClass3_0_U3CGetU3Eb__4_m44FBEB10E4BAEB006FC92154DB40AB3CB7BC4DE0,
	U3CU3Ec__cctor_m4FA3DF2408BEA55F36632D2A36DDEA9E1475BBA3,
	U3CU3Ec__ctor_m6B5FCC705564205B21810FFBA043CB8DA659F8E1,
	U3CU3Ec_U3CPutU3Eb__5_1_mC12F48C710EDD364D74E0411BCE9CB0F55326D2F,
	ArrayLenghtExample_Update_m439A2360ABB7F38336FC1763B3251D37764119C1,
	ArrayLenghtExample__ctor_m8559D0D49A2BB7EFDFDD61E2CC8D54DB71840176,
	Fashionmanager_Start_m7840AD390EC9CC1481D8912CAE6FAC17C8C8536B,
	Fashionmanager_Update_m6E657E7ED7E231D179A3C112BBFE2333117A7E80,
	Fashionmanager__ctor_m3B067E91A0E4C80C06482F0F0BE43522768F89D1,
	MenuManager_Start_m4E4A5EF33C27448D7F34FD29B93589635F1B2EE2,
	MenuManager_GoBack_mED70223B46A8AF47FFD83ADA5978C1335BA581B3,
	MenuManager_GoToMenu_mD3F22584A78A4D0FFE0D024660B4ABC6130795D4,
	MenuManager_Animate_m28495AA50A493ABA5454D5766FC9C548EA3C2C54,
	MenuManager_LoadScene_m7DBD3691722FA968ABA290C6C16FACA0650B74EE,
	MenuManager_LoadChroma_mA9A328B7D4020C6519D663B46F21F186F0A57F8A,
	MenuManager_LoadVideo_mDAF29DD24C4E6FF581942CED89CC35B834D907C6,
	MenuManager_Awake_mEFBAF3F8CBDEF5A033B3BAD9CA897801135B6463,
	MenuManager__ctor_m8F61CC885B72291B54C1C6EC368AE303EA856529,
	Pause_Start_mAF3951FCCD204C9094502892B23E1BB368C938BC,
	Pause_Update_m2BEBA55A60F1B469B29DD631505236F1B5F21FEE,
	Pause__ctor_m0A16764376F8C9A6D19DE0BB24A41FA81F587928,
	animator_clickController_Start_mBE1F9E31C6AD21F147AD13D1DA5F95E1E3390862,
	animator_clickController_Update_m91995767D34A551CC334C5A5600C09BD60573E34,
	animator_clickController__ctor_mE64FB9AD14ED760CC29EF32E209349C2DBF431A8,
	backgrounManager_Awake_m4EEF27EBDEC805826663EE644346F79E62236E30,
	backgrounManager_Update_m4FB45C070AD0F22ADB799B3B94DBD0F856D62546,
	backgrounManager_greenBg_mC0E17D486AEABF43CC5F408F5BC2D1A84875655D,
	backgrounManager__ctor_mD003C8D4654C9CB7687FE2244FD38A1B3D16AAEC,
	cam_flyaway_Start_mC7C9351D51B0780AABAE6A2DBAB14A55EB349FBD,
	cam_flyaway_Update_mE15153479D2889C0C61340C5D39EE25B3CDC6765,
	cam_flyaway__ctor_m2E0B19657E7E75BC6B6AD6860DEBD85DC4A49C4C,
	clickAnimatorParametr_waitclick_m084799C820BA08D53EB503E16D16C2C2DEDA7689,
	clickAnimatorParametr_Start_mFDC1CD2712202AA920C38E2B5109B437CE9F0F69,
	clickAnimatorParametr_Update_mD8FB42A7250BAE9F99D163364742A1F155A7FF21,
	clickAnimatorParametr__ctor_m85C40615274AE3798F0B3F8705BD8C06CEC4DA64,
	U3CwaitclickU3Ed__3__ctor_mA830488CFBA1E55D7FFAFC22226E78380257A5B0,
	U3CwaitclickU3Ed__3_System_IDisposable_Dispose_m3A1B2C90D476875637B69557F82A9BD37A0AE36B,
	U3CwaitclickU3Ed__3_MoveNext_mFD3158638EE4D965FEC903E8D1A5A54D98ABD5DB,
	U3CwaitclickU3Ed__3_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mB511A71DDCEBDB0D88E2C392EA1FA088C3D9B402,
	U3CwaitclickU3Ed__3_System_Collections_IEnumerator_Reset_m8311FF5D6F36EE0D15CA64DEF8A7CECCDDFC4605,
	U3CwaitclickU3Ed__3_System_Collections_IEnumerator_get_Current_mABF464AD1627A8BD8D5857C3B67C3CFB9297FA27,
	listTimelineController_Start_mE9426B939F4884E11A9138A4FF4AAE8DA6139545,
	listTimelineController_Play_m6BC4FB0B461B5C7BB1FBF56F8AE0F3A810E32492,
	listTimelineController_Update_mB4BAEF8477C68DE0A70E2F80DFA7FACE712C1943,
	listTimelineController__ctor_mE67DDFB8C79953E33276252565CED06EDA365879,
	quitpresentazione_Start_mD1BC003BD2D4D9F40A2DC56A3827D0F86A169FF4,
	quitpresentazione_Update_mCF4C9EDA4132BEDDEF978C0FE2B5E2C607677C22,
	quitpresentazione__ctor_mC1DCA6B5834D4A7AE95217C9BCB86C3B36EE1331,
	switchDress_Start_m34F362F0E40E35D4EF75F321D8210CF0FEBE1C1E,
	switchDress_Update_m65A656821DE4BEDF1C20CE71A0FDF1C10C8109B6,
	switchDress__ctor_mFD6EA473E3822BE5261918B65B69A5E8764F7ACF,
	talknorma_Start_m6EE2E56716E2FB1DAAF1D91C54ADBF0B1F6F3877,
	talknorma_Update_m7919C3B5B861CCCB27F912BB4BC4E4ACCBEADFF3,
	talknorma__ctor_m597E37CC82955980B90DA2A88C894DFE6A31225B,
	timelineController_Play_m61EBE6F06FF37BB2A3DE17E8521DFC4701A90174,
	timelineController_Start_m165B9B4F66B5F011CA8FBF45D1B1ACFAF0B0C128,
	timelineController_Update_m70BADEAF07E27255C34919253C0FC5B684EC2AF9,
	timelineController__ctor_m2CF666BF13E36839D7982412435FB8C221EEA738,
	ActiveStateToggler_ToggleActive_mE5AFDC4E770D3AB465A4EFEC92CA40F962BCE9FA,
	ActiveStateToggler__ctor_m8085169E76AB400B373EFC0E5E4028E189A25028,
	ApplicationManager_Quit_m403368607C71AACA27341CA2B11F90B15D9C2275,
	ApplicationManager__ctor_m4D923E22E12C36CD444B73E88487BB4E77A83E62,
	AssetManager_Start_m8FEBC0A4B22F47E6DB32C56C17A3105FB2B83D47,
	AssetManager_Update_m2B32C31905FD36C44721D7DBDDF6FB199E5FA0B5,
	AssetManager__ctor_mC11699C1F3965E8487903A31B3AE1089213B541A,
	ChangeColor_OnEnable_m7FE31A95B4478A63871D6365942BC37DE75B985D,
	ChangeColor_SetRed_mB0090838752887384E425FEAA937ABBEE13E4EB8,
	ChangeColor_SetGreen_m05B35D3F91BCA56B345321414A034A7D92D0E45B,
	ChangeColor_SetBlue_m57D6E496618765E4DDE654275D7589AB1C409630,
	ChangeColor_OnValueChanged_mFA920A494A5F9749FCDBD6BFB7EC8E3D9B6172DB,
	ChangeColor_OnPointerClick_m7AB5B0F618C688037FB77A27B05BBCB8AA957A1F,
	ChangeColor__ctor_m41B10590AF4632D7F8E7ABDD4130C5B1067561A6,
	DragMe_OnBeginDrag_mFC5EDC3D150A4B114381C991228B71BFCC9760C9,
	DragMe_OnDrag_m7988B9DAA4376D14957C051110A049215892025D,
	DragMe_SetDraggedPosition_m7C8DB86B92268E42387E0E6479521095429EABDE,
	DragMe_OnEndDrag_m24576A6489ED4643BE5BAB5FE4246EF5857AFC3E,
	NULL,
	DragMe__ctor_mB0FCFD436CFBA38B6A39024D5BB21AE21D0CF978,
	DragPanel_Awake_m7730D945BF0A144028A6F53F10306D326CEE9980,
	DragPanel_OnPointerDown_mFDF80CB62D238558FC69270E3F05C9BADE87DAF3,
	DragPanel_OnDrag_m11327A95544774C1FA3EFC7E5B9B2D685C6DA6A7,
	DragPanel_ClampToWindow_m4F9FC1B0D2B94CA8060853823AAEE496AAE38AE7,
	DragPanel__ctor_mBDA15B1E481A3D5538BD1B319AE4B3B4ABF64F7F,
	DropMe_OnEnable_mE82969854A6362AF87DE93F1A9CBFE6B83F504E5,
	DropMe_OnDrop_m3AE8F24C2C9D91DFF973EEF324CC9CB5FAD9401D,
	DropMe_OnPointerEnter_m2F6C117AF60413EBC4FB03EEB4D09EB40B3317B4,
	DropMe_OnPointerExit_m31E7FEB688CA54F16B81A3CA8A4B21D676EDB651,
	DropMe_GetDropSprite_m2C3723D7AB5974D991F47C67F8BFB465D85DE166,
	DropMe__ctor_mA62B4643146CD6841C0D898D5686FEF75A7E5DF9,
	PanelManager_OnEnable_m09547ED27D87F696B41F1FAF5EFC3826F3204E2D,
	PanelManager_OpenPanel_m55C75D9E37F2CDE8B1670FBF979329E5D6D13E0F,
	PanelManager_FindFirstEnabledSelectable_m59C12281C9EAED20F4FBB9C820D4EAD2C2DA117B,
	PanelManager_CloseCurrent_m20DE00E5BAE15FA1D4A052E555A3CC5C58A6A695,
	PanelManager_DisablePanelDeleyed_mB120A21FD25F2168724F35B9FAD134487EF0507A,
	PanelManager_SetSelected_m33A82F0C75C5439BE6C1FEE4B93BF4E3949FE882,
	PanelManager__ctor_m945C0470B90313D67EDCED0D97AFADB78A190AFD,
	U3CDisablePanelDeleyedU3Ed__10__ctor_m3595FCFEB5B1694DEAE36303E82C43199E309861,
	U3CDisablePanelDeleyedU3Ed__10_System_IDisposable_Dispose_m37D3EA493F4986A2E0A6263B72E96EF00126946B,
	U3CDisablePanelDeleyedU3Ed__10_MoveNext_m7BFBF535CE4EBEABFFB4B592D5BA81B83152B2AD,
	U3CDisablePanelDeleyedU3Ed__10_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m82D3317E9507246F91AABBA087291CE3DFAD9B4C,
	U3CDisablePanelDeleyedU3Ed__10_System_Collections_IEnumerator_Reset_mCCE2A629729D15A72499C8B3EBE2380829AA7291,
	U3CDisablePanelDeleyedU3Ed__10_System_Collections_IEnumerator_get_Current_m271F5CC65D4EAF362EEEB37323D10ABBE73D0554,
	ResizePanel_Awake_m02B9AC5CDBA67698A54F038360EBBE1A5358C034,
	ResizePanel_OnPointerDown_m37657BDB0494DD539375450934A44FDAAF3B9249,
	ResizePanel_OnDrag_mE54AEB640A9F9432EB5E64251BC10D2C20667A9E,
	ResizePanel__ctor_m12B4ECE6819037FC06C27BE7CBEF5B3112870FDB,
	ScrollDetailTexture_OnEnable_m8B3390F8C092EA8409C0FA2617A5B130D6943DE1,
	ScrollDetailTexture_OnDisable_mC96EA86291FB8B4919FE11018EB8E7849311059F,
	ScrollDetailTexture_Update_m8B1F71303E9E2692FFED74C314E3183C40BDC2FF,
	ScrollDetailTexture__ctor_m82FA53B10261E10F9FC5A42CEBB20E3B4EA79EBE,
	ShowSliderValue_UpdateLabel_m6BF2232BF66791803BDFF15885544AA28F9DE766,
	ShowSliderValue__ctor_m92B3943A578957A0E0036A16DC9024C213FAF38D,
	StreamVideo_Start_m856EBA64997835A1514D498BA1F81F90E0D0C865,
	StreamVideo_playVideo_m665494A62132E352E3D9992154A5BA5A6263E0F1,
	StreamVideo__ctor_mE3613E1CD24FBE07CD46C88F6E03E29C7C796C18,
	U3CplayVideoU3Ed__6__ctor_mFDB672CF9D2AE25511238488FDCA4FCA83005F5A,
	U3CplayVideoU3Ed__6_System_IDisposable_Dispose_mEA5585FE473EAA0C3887FDC55F7FFF74D648C338,
	U3CplayVideoU3Ed__6_MoveNext_mAFE58DDB33CCAC21758665ED5EE7B601DED94262,
	U3CplayVideoU3Ed__6_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mC32C8B8FE648421EC1D29C2DE18689FBCFEC4BD1,
	U3CplayVideoU3Ed__6_System_Collections_IEnumerator_Reset_m1E6B4CB9B2DA5A4F5AF0E8B55458CF92037AEE9D,
	U3CplayVideoU3Ed__6_System_Collections_IEnumerator_get_Current_m1ABB3BCE3C8DDA8C375F9A7B2CFE414851BF7D23,
	TiltWindow_Start_mCDDE54F1278FA28BDEAB725B8283FF494D6D0ACC,
	TiltWindow_Update_mD571F534039FB9C6D19D024371F16CCDA3F2EAFA,
	TiltWindow__ctor_mEA9ECC1BB8D20A2225044D6F394A7AEBDFDBD47A,
	IconPreview_Awake_mD330471AA6E0259A3EE92FADE843189B6463D21E,
	IconPreview_Update_mE7394435FCE9210899203B0724F7E2717868CF04,
	IconPreview__ctor_m6739A07D0628A74DB7748300B21999D113B0FDBE,
	ChatController_OnEnable_mBC3BFC05A1D47069DFA58A4F0E39CFF8FAD44FF2,
	ChatController_OnDisable_m268A7488A3FDEB850FC3BC91A0BBDA767D42876B,
	ChatController_AddToChatOutput_m43856EEA133E04C24701A2616E7F10D7FFA68371,
	ChatController__ctor_m3B66A5F749B457D865E8BDA1DE481C8CF1158026,
	EnvMapAnimator_Awake_mFFC04BC5320F83CA8C45FF36A11BF43AC17C6B93,
	EnvMapAnimator_Start_mC0D348CAB0F0DC920EF3D3A008688B533F66D1BE,
	EnvMapAnimator__ctor_mC151D673A394E2E7CEA8774C4004985028C5C3EC,
	U3CStartU3Ed__4__ctor_m4D65F4FC2207AE4B6BE963AF9B5EDC55C7E29B23,
	U3CStartU3Ed__4_System_IDisposable_Dispose_mFEE2ACED70A3D825988E28CC61FEF8DCD7660A5B,
	U3CStartU3Ed__4_MoveNext_m2F1A8053A32AD86DA80F86391EC32EDC1C396AEE,
	U3CStartU3Ed__4_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mF03E2A57659B8F598AC450183F55D43F903C0A1E,
	U3CStartU3Ed__4_System_Collections_IEnumerator_Reset_m268BA538CF6812C56EB281C0CE29D5AA2E9A2CAB,
	U3CStartU3Ed__4_System_Collections_IEnumerator_get_Current_mFE9CA9F52F9AFA1E4C9DBADA2064F854D6931CFF,
	API_Webgl_CustomScript_Start_m992496291BD9EE4C71D6558ED0B5FD133650F37C,
	API_Webgl_CustomScript_Update_m2BA9B158877F6CBF505439F210F95A1D01A47808,
	API_Webgl_CustomScript_HandleDetectionResult_m00CA3C68F85F757E38B2E51F8C0FC89A022B174D,
	API_Webgl_CustomScript__ctor_mFE18591EAB468A9FC08AAE8D51AA5BD0C63FE240,
	U3CStartU3Ed__1__ctor_m5E90A5E31AFFDA9A00027F23432E83AD003F8F06,
	U3CStartU3Ed__1_System_IDisposable_Dispose_m0CCCE8D014E1F539B5CE96B2FA024CD9DAE4E52A,
	U3CStartU3Ed__1_MoveNext_m508623E249221F0110E563DA4222AD1013B6F755,
	U3CStartU3Ed__1_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mE32C41EA69F6F619888FF1E1C03D8BC2639C1D30,
	U3CStartU3Ed__1_System_Collections_IEnumerator_Reset_m790784997CB2EEB06544021FE7691779CB59D373,
	U3CStartU3Ed__1_System_Collections_IEnumerator_get_Current_mFE4B07ABE524624161A7BAFFE8B6C2B3B2A9B8F3,
	AlphaDictation_CreateText_m46C4F0FFDAA30AC7E8B92D500EB5FFE5170F81E7,
	AlphaDictation_CreateNameInputField_m7AC3A3048D731EC81C9E6EDB7A937701691E4BB0,
	AlphaDictation_CreateTalkInputField_m212356FCFBE4942C8C91B9085FFC3DD655B1700A,
	AlphaDictation_CreateTextAndSpeak_m523F7F9250A8652685FB2DDA7BC985A9873B48C3,
	AlphaDictation_Start_mE73F0C092900182FAEAB2154C77FA9B969076BD4,
	AlphaDictation_HandleDetectionResult_mC6E2E77A757423D220C9358275F6B2E6ED4D8252,
	AlphaDictation_HandleSynthesisOnEnd_mA834E1DC2E59E56945999805AD56E94366BF21DB,
	AlphaDictation_GetVoices_mABA61640ADF4F82FCDC628FD7BEEAD5DDF980132,
	AlphaDictation_FixedUpdate_mB8B21C223460615C2107C4C5200540AA2739710D,
	AlphaDictation_Speak_mC4290CA03C4DC651005EB265A22545C5AE0D4B51,
	AlphaDictation__ctor_m713E7930D50FDF1FA2ABEA40B76693C867465EDA,
	AlphaDictation_U3CStartU3Eb__21_0_mA7498778E41A70AC06A961C5F9C1F899DBE77578,
	AlphaDictation_U3CStartU3Eb__21_1_m64B2BD3408CA0A3B2952E02DE572BC79487C16CB,
	AlphaDictation_U3CStartU3Eb__21_2_mC4886CB5A7ABF8351AA5EAA455A504AD4E8A7C2B,
	AlphaDictation_U3CGetVoicesU3Eb__24_0_mB54B6275ADFCC20BD44B8B91DB44B766BA086B92,
	U3CCreateNameInputFieldU3Ed__18__ctor_m16DD53F38890499364303711C09C1C5214EEDD70,
	U3CCreateNameInputFieldU3Ed__18_System_IDisposable_Dispose_m0361E7C9EDBF31EB17C87B8B7D2D5D8372BF4EF7,
	U3CCreateNameInputFieldU3Ed__18_MoveNext_mEF5B0F14C068D7406691FDE9FD44A352D5263906,
	U3CCreateNameInputFieldU3Ed__18_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mD961FEBA5850489423C74E8A9ECF65799EDABA19,
	U3CCreateNameInputFieldU3Ed__18_System_Collections_IEnumerator_Reset_mF531741BA7DF17D9D80B7D368C54765DC2DDD36A,
	U3CCreateNameInputFieldU3Ed__18_System_Collections_IEnumerator_get_Current_mC900B40596EAC5FB8A8E6D8F97C7DE06C32769EF,
	U3CCreateTalkInputFieldU3Ed__19__ctor_m7EF2DFD1A550B11488B93EAF9CCBDEDDBBD5070F,
	U3CCreateTalkInputFieldU3Ed__19_System_IDisposable_Dispose_mA785F12D160C5605B61C8E05F42FF2DEE32FD5D3,
	U3CCreateTalkInputFieldU3Ed__19_MoveNext_m99F33A207FEBB6E605F2CABA6E1F0EAE35F722F2,
	U3CCreateTalkInputFieldU3Ed__19_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m3D9B40DD836926B602CA3956E729D17831707F2E,
	U3CCreateTalkInputFieldU3Ed__19_System_Collections_IEnumerator_Reset_m6030C1BC4160045FECCE7404FB1258B3326A4EFE,
	U3CCreateTalkInputFieldU3Ed__19_System_Collections_IEnumerator_get_Current_m8E8A6F01565CDD65153C8335CC73EBA5F83050E9,
	U3CStartU3Ed__21__ctor_m26D519F01F14E6D96913DE072F29AB761206406A,
	U3CStartU3Ed__21_System_IDisposable_Dispose_m83494FD180C6AB29D8ADD11E43F3BFF286086BB6,
	U3CStartU3Ed__21_MoveNext_m7F80D98E4B5DED0B33D0A9C1750DBAA690325D5F,
	U3CStartU3Ed__21_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mB9FD60E1B238D04F2053FBB9F7D909F7F0407903,
	U3CStartU3Ed__21_System_Collections_IEnumerator_Reset_mB8E6F4D18EBDD4308E0EC58B5857A9E2DD150002,
	U3CStartU3Ed__21_System_Collections_IEnumerator_get_Current_mAA0AC8FE81AE618E6E533FFFB3A3EB512247C5D7,
	U3CGetVoicesU3Ed__24__ctor_m6EC67E3F6B6F579CFBFA5D72A871657B6E041DF7,
	U3CGetVoicesU3Ed__24_System_IDisposable_Dispose_m2D86E52EE2167BF97EBFD117EB53F27F9B14FA7D,
	U3CGetVoicesU3Ed__24_MoveNext_mC85ABFF0235E61E1014895917343284664C70818,
	U3CGetVoicesU3Ed__24_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m2ADDAD12E3D1D614D1896E530F3D1AB0822FF876,
	U3CGetVoicesU3Ed__24_System_Collections_IEnumerator_Reset_mD951BF2ADA17869A6F1798A98F277F1254A1AFD7,
	U3CGetVoicesU3Ed__24_System_Collections_IEnumerator_get_Current_m534DD093F2D41F07001B5D87DC34CA74D7446D81,
	AsyncTest_Start_m2907A9617EA115AB97EE9D38B5A418C255D9E244,
	AsyncTest_OnRequestFinished_m71140E44A0FB04D9B8134425ABA355EAED483AB9,
	AsyncTest_chiamata_mD2A738D13AF489CDA32EAC0FE95F1DCFA3A557F9,
	AsyncTest__ctor_mF169445CD9FF6C4DFEFB8F0916CC70249577D1CB,
	AsyncTest_U3CchiamataU3Eb__2_0_m08D9EA2FBFB5A5593F9C6CF3615B51497563B2AF,
	AsyncTest_U3CchiamataU3Eb__2_1_m82F6E12D3D29F080A8F6417994BB79A23E428EFA,
	U3CStartU3Ed__0_MoveNext_mBDA40DCEF83D7125F6056B9FEDBFAC0514D18D79,
	U3CStartU3Ed__0_SetStateMachine_m13EE83714ABD4D5D9AB518554FDFA48D6E3D5B3D,
	AudioWebDownload_Start_m5C68F3BB98AC3F3DC750CCD1782FC55699B1FE00,
	AudioWebDownload_playAudio_mF23FEFDEBF16708B9308AAE47EAC88CA49440978,
	AudioWebDownload_Play_m1F8AA0EC4D598F7EE4D9306655A9344DF8998D15,
	AudioWebDownload__ctor_m021D371A9D02D77FFFB0D510C5DAFEACB446594B,
	U3CStartU3Ed__4__ctor_m91EA2977411F2D5F1F38F22A96463D6922ADC6A9,
	U3CStartU3Ed__4_System_IDisposable_Dispose_m6068A7F795E708511DCA473A8774621B114C08BB,
	U3CStartU3Ed__4_MoveNext_m5347AD2C086F6401688826A7E66C111BDD0365F6,
	U3CStartU3Ed__4_U3CU3Em__Finally1_mA5B1FCF05C223A6056AAEDC45D59ADD6BD1B8AA8,
	U3CStartU3Ed__4_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mE29825A2EEC44D0C234AA720FF5AF59EEC4CCEFD,
	U3CStartU3Ed__4_System_Collections_IEnumerator_Reset_m8D91B10117F29DBCE66FB0AB79F45716B5B3A852,
	U3CStartU3Ed__4_System_Collections_IEnumerator_get_Current_m31F531ABC8E70793483ADE592ADFCA9BC7B32F6A,
	DataContainer_Load_m0884A3D313193B52821F89E2030774753E15A52F,
	DataContainer__ctor_m26CBEA5E0F69B8BB6AB9132B38BB8C0EC088985C,
	Emotification_Start_m467B40A90243A74151BFA1F1A8B9E6ADA35BA4B8,
	Emotification_Update_mBF64972B9C1FFEAF8676EB4E2D4058DD90BE8B90,
	Emotification__ctor_mEE7AE1D9B8031C2446B78BE5A1FDD317E216C287,
	ExecuteCommandTool_Start_mD24F530E0F93CDEC1E6D0DAAA1FAD80BD5BB381F,
	ExecuteCommandTool_ExecuteCommand_m9C048A35B986A440672EBDF2EF469AFE7545184F,
	ExecuteCommandTool_Command_m4FB509EDB60381862DAA3FD0C4703C0B12A11809,
	ExecuteCommandTool_calcola_m7BA14E1784C884230AB17B40C9150335012C560E,
	ExecuteCommandTool_browser_m6253EC25E1572198449CA967ED3793C862684DEB,
	ExecuteCommandTool_miometod_mCAA2C1E31E7138D465C3265780ED7636A2F777DF,
	ExecuteCommandTool_batch_m3FE965B114A3F6305270B4BE27AE8F40F735C52A,
	ExecuteCommandTool_RunFile_m221F4A9EBE910ADBAE3700ECADB8EC32B43814C6,
	ExecuteCommandTool_batchWav2lip_m179062873C8FE7CCA191B410322EFB276EFEE0D6,
	ExecuteCommandTool_trasnfer2server_m6ED2EA83F57B22FCDB268DDF44A5DDB786356DB0,
	ExecuteCommandTool_server2unity_m31A0F987DCD427DE6B13EB4F2C5DC612A0379C80,
	ExecuteCommandTool__ctor_mD6318799D6E4877E4460870BFCA1122144361547,
	U3CU3Ec__DisplayClass8_0__ctor_mEC30FB793086E401C4330816CB76F33E3CEEFD96,
	U3CU3Ec__DisplayClass8_0_U3CExecuteCommandU3Eb__0_m70E0A94262DF0551A399396D9B9E946453F40E51,
	Loader_Start_m31D4A12C74578B3A6BDC94F52BAEAF3639A71D1C,
	Loader__ctor_m15D78DFE09D5A9C562B7E58B0D4C392427D1E674,
	ProductConfig_get_Instance_mEBB02C79BF1291144EC952F122635CDDC2EAE0AA,
	ProductConfig_add_onAppConfigured_m225A66D36308969E5527B66D8A87BA1F478448CF,
	ProductConfig_remove_onAppConfigured_m5ED96BA9E7C9F8E0F0642F5E1C8272D65B7EEF74,
	ProductConfig_Awake_mAD7E981E1889AF56A52539476628DB74C200C537,
	ProductConfig_ProductConfig_onAppConfigured_mBD8B08EB8CE5B3BD8CFA4753C484D23472D3E656,
	ProductConfig_OnDestroy_m6CECCF21C6DC81F5E8718EF3E53C08F2C653CE6B,
	ProductConfig_Start_mEB3078D001663BF597FE8628917800D4F631B3DE,
	ProductConfig_ReadPrefsJson_m058F9FDB22B8999DE2D1FA247770F5E3AE58DF06,
	ProductConfig_ReadEngineJson_m0387D0BF66D8059E62D46F51E8650EF00E202C55,
	ProductConfig__ctor_m7E47D39B606AA615DA54A0A677969E052CDBAC08,
	AppConfigured__ctor_m4AA784B7528682370CC3B861F796B7F4C7E1FE88,
	AppConfigured_Invoke_m401451E66161F97C89123DEB8CEBC6D4759F5F12,
	AppConfigured_BeginInvoke_m14583E82793A869D64AA503CB66CAF33981DD6E8,
	AppConfigured_EndInvoke_m99CB9889EABA1D324E3646BE642EF15302E65DC2,
	ConfigDataClass__ctor_m1C9E04789516FEEF3BD42A5E22F9C58179CA9202,
	VampJson_get_data_m8144B4629CB7511325960930B6FC1E1704B34279,
	VampJson_set_data_m324D78FD2D84110350689B0D420658A842FD37E3,
	VampJson__ctor_m63821C612E5F5FB5707EF789D7C8882E6A84755F,
	ResponseMessage__ctor_m323FBDA168075443A9A5D23022FA7BD8D2C415BD,
	VampJson_get_data_mC83BA77A1EDE4B5D73280878433071E94FB4E8DF,
	VampJson_set_data_mD9931EC77A8E455CFD4431703C74E962F2B4CDB8,
	VampJson__ctor_m100A27BC9E36685BBCC44BC4C294A70716579FCC,
	ServerGET_Start_m214FD6BA943E096C665081C97E3A35F0176EAF27,
	ServerGET_mandaTellme_m7CFA9D14C649AC6EC533CBE87DC14A802567F65C,
	ServerGET_mandaStart_mDB49AFB0A02556E80B024A685096389FFA73FB98,
	ServerGET_sendit_m1EA29AAF88B44756F00A3B7F35EFE305996ACDB2,
	ServerGET_sendStart_mB210B583791D836A00986A0A8C3F6725D854B9E6,
	ServerGET_OnRequestFinished_mBE5B5E12989EB93DDCE356EFEC9F9103867E874C,
	ServerGET__ctor_m64078CD815F5B8E107D4AE2CDE13FB3D8B2FBEB9,
	ServerGET_U3CmandaTellmeU3Eb__2_0_mA36FA1317E057FFF14B768B4780362CBF2833EF3,
	ServerGET_U3CmandaStartU3Eb__3_0_m1C16D722E0FDD4F011F761BC580A4CC0C0EF7D6A,
	U3CsenditU3Ed__4__ctor_mE07C479F2D6D08CEE765BC0A16075B269195DE04,
	U3CsenditU3Ed__4_System_IDisposable_Dispose_mEAB91ACF97C985B4818468F0570A6BB1B2D2CAEE,
	U3CsenditU3Ed__4_MoveNext_mF74C75DD4E4185EDBA7BEBEE0C13C1924F55DFEF,
	U3CsenditU3Ed__4_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m0075B1BB7202B48EAF8DF68BD1E75E7479DF09DE,
	U3CsenditU3Ed__4_System_Collections_IEnumerator_Reset_m3C35FCF6F7379FB5FE4D93030DFAA521BCF53796,
	U3CsenditU3Ed__4_System_Collections_IEnumerator_get_Current_m2418856C8CBC200779218B14301B6585C7E81BB8,
	U3CsendStartU3Ed__5__ctor_mD6894369E0CB939F1F4BF9E740CD24EDC91BAA97,
	U3CsendStartU3Ed__5_System_IDisposable_Dispose_m12FED3311D45913F73A5B237F6A82857641407A9,
	U3CsendStartU3Ed__5_MoveNext_m3A2EE043DCDEFEBAE3ED357E8C1A91352AC0C774,
	U3CsendStartU3Ed__5_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m98D1A599E6148119D2BEC23D1B565C3684A94E98,
	U3CsendStartU3Ed__5_System_Collections_IEnumerator_Reset_mD881B26362ABCA5EADB3A324451DF16766FF345A,
	U3CsendStartU3Ed__5_System_Collections_IEnumerator_get_Current_m8E257796E3411938AA72D419B4B6965E8AA3A78D,
	SessionLog_WriteToLogFile_m625DCAD01A9E75512ABDB79895F03EB48E008ACE,
	SessionLog__ctor_m8427FF253053ACA1AF3519E7E73C929335F8E292,
	SpeechDetection_Awake_m0A80DC2A0BEAD54DB64F1834C7D41EF1798087DC,
	SpeechDetection_Start_m964CB86ED3FB5503009540A9FCAF2C1BC58EC52D,
	SpeechDetection_WebSocket_m1196E409C1FECF0EC616386C809AEEC3DEB00640,
	SpeechDetection_EnableDetectionHandle_m49950EB1CE75DF37E2CA956C5A6321C867A2D154,
	SpeechDetection_Interrompi_m8B8B8998BAA77C96F91DC39A22CD61DF9D0E597A,
	SpeechDetection_Update_mDDD932189D9A2D28CDFC3DEA2A7A5BEC67B2CAA6,
	SpeechDetection_WaitDetecte_mD06C349890C0CA117F3B5EC2E65DE5F8081BF0C5,
	SpeechDetection_delay_m183FFB8CEE5DDD89082F2CE0C2A4D2DB1DC65368,
	SpeechDetection_delayAnswer_m843ACAC286411C73DB04E624B52DF75E524E5376,
	SpeechDetection_HandleDetectionResult_m8F9D847517E729BE94E47CC28D2D988C89603B98,
	SpeechDetection_settaLingua_mD6794B022337169B42AEE0C7C9206AF425E3D8CA,
	SpeechDetection__ctor_m6A5EDF54B97BF4FEFA63A4CDD84D38520291E979,
	SpeechDetection_U3CStartU3Eb__15_0_m85EB3162E958E0310FFD0E9793D32D5EA273A9CF,
	SpeechDetection_U3CStartU3Eb__15_1_m8B33085CD6325B0D6AAB6D98724D7695B2737933,
	SpeechDetection_U3CStartU3Eb__15_2_mDB40EC83F38DDEEEA4EA54ABEC0D7C5C859DBF98,
	U3CStartU3Ed__15__ctor_mC9AE0236D7B588698073D4B89CB654641A6DB0AE,
	U3CStartU3Ed__15_System_IDisposable_Dispose_m6ABD141C75095FBC2065E255D6C4448AC3060984,
	U3CStartU3Ed__15_MoveNext_m9D44B4F04AD99F4842E24B2A0812078B1898FF9D,
	U3CStartU3Ed__15_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mCE9755FDED93AE39AAA993068440542D54FFA2C7,
	U3CStartU3Ed__15_System_Collections_IEnumerator_Reset_m065FA6FC5BA16F1A873BF95F9F9A535B2E997D5E,
	U3CStartU3Ed__15_System_Collections_IEnumerator_get_Current_m9079006723B5335C044B979CBEB474EF6BD19C84,
	U3CInterrompiU3Ed__18__ctor_mC4BF20D4B4F0924F4316A332EA064658ECDF3B8F,
	U3CInterrompiU3Ed__18_System_IDisposable_Dispose_m1E8E4915DAD9852F47258A33747876E6D9363E5A,
	U3CInterrompiU3Ed__18_MoveNext_m5D306ED87C95B41F64362772D1058825769C2776,
	U3CInterrompiU3Ed__18_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m3CB514C370F4075CED65FAA6F8713C5881C501A7,
	U3CInterrompiU3Ed__18_System_Collections_IEnumerator_Reset_mB1124F4AFA3CBBE56267B4AE3B3E6FE1E17B8903,
	U3CInterrompiU3Ed__18_System_Collections_IEnumerator_get_Current_m583EF39D69E9F1241AE2B86CECA06D88FF507673,
	U3CWaitDetecteU3Ed__20__ctor_m1A67F7878DA418826DFF5D95A0BFBB2574D910C4,
	U3CWaitDetecteU3Ed__20_System_IDisposable_Dispose_m43C5ACD018DA8ABBA17B6FE01CEA7F2EF50358E5,
	U3CWaitDetecteU3Ed__20_MoveNext_m2B1BE065562C52649D6794675B21335F65BED889,
	U3CWaitDetecteU3Ed__20_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m6ACF176E8490275E029219E9FD34C0E7E840023C,
	U3CWaitDetecteU3Ed__20_System_Collections_IEnumerator_Reset_m19BCEE584D5A53FE393B353AD316C26394B15AA3,
	U3CWaitDetecteU3Ed__20_System_Collections_IEnumerator_get_Current_m0257432B6E2C13570BA9D06616B0047DF5427830,
	U3CdelayU3Ed__21__ctor_m4972F51E4B93A2627E1963D38EBB5678F80CC498,
	U3CdelayU3Ed__21_System_IDisposable_Dispose_m887E96F8864511CBDF1F1F49312EB454D89CFBF4,
	U3CdelayU3Ed__21_MoveNext_m07CA5215F58141DB89E06467826DDEE03A5EC1AC,
	U3CdelayU3Ed__21_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m886749BC4D4F2A41AE65E71E9C803BC67CFE1F9C,
	U3CdelayU3Ed__21_System_Collections_IEnumerator_Reset_m3255BDF07E7B37EB9F97B3DA74FF5E650FA89F2B,
	U3CdelayU3Ed__21_System_Collections_IEnumerator_get_Current_m16CB6970AAF99BF95E44CBF1B101CC1BFF577454,
	U3CdelayAnswerU3Ed__22__ctor_mA746C78FB7F20405F54BE83796272BA0F8C1CEF9,
	U3CdelayAnswerU3Ed__22_System_IDisposable_Dispose_m21DC4C5108F00FFF3D5678EB7E31A5172FDD35F9,
	U3CdelayAnswerU3Ed__22_MoveNext_mC4D22297822A1D3508D82F9BDE2642792A8774C0,
	U3CdelayAnswerU3Ed__22_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m72CB275E58D203B0A5C803951F7D4EE7422FCDD8,
	U3CdelayAnswerU3Ed__22_System_Collections_IEnumerator_Reset_m93850936565CD71FF2F17D4BEF4321C1A05C449A,
	U3CdelayAnswerU3Ed__22_System_Collections_IEnumerator_get_Current_m3A0860DD62ED37606BAF36C70E4659B3E18340A4,
	TestScript_Start_m5EADB03D1F6C909A74ABA654FA9F2162CD2DA750,
	TestScript__ctor_m58A026B07FA116838E76CF62F4630A99FB96365C,
	Tg_Loader_Awake_mF5AD0A3AC72F3B7BD341C74012E3194AA0C08B68,
	Tg_Loader_Start_mB0602917F605650FF8C59A90098A2A5A35143C19,
	Tg_Loader_processVideo_mD605E0E448B6A69D8726C1BA1D65A3DAB17AB5DF,
	Tg_Loader_quit_m9119A499A217F4D589F1C03F50EF1BD84337E54D,
	Tg_Loader_execute_m55D106AD2047D2ECC981B04076BE3A2719ED3489,
	Tg_Loader_CopyVideo_mA1C006CAB312C57E5DDC19B5B8182AEAD490DD98,
	Tg_Loader_Update_mD2D821AB2E3D937E7259C1F49F1D9E19FCC8AF8F,
	Tg_Loader_mediaFolders_mB6BDAFFA0EA1D843770DDC9D978659E51FB566BC,
	Tg_Loader_show_m313D3E3554FC596FBA175EB79C04DFA481965ED9,
	Tg_Loader_genera_m1902CBDE3685FBD4BD6652FD178C86CB4BBCB071,
	Tg_Loader_jukebox_m016BB104F339C981ED8B6ED2D2D633E23132657F,
	Tg_Loader_videoRunnerS_m282046B2D95397FEF9608E076865ADE28D78A6E3,
	Tg_Loader_renderVideo_m25A0FD4111D75349E8731C8E34D44D89596E6F2A,
	Tg_Loader_GetMyAudioClip_m35188C6DF6958545FB1B1BA4435558BD15629A37,
	Tg_Loader_EndReached_m2C703B71FCFF3CF9EA2F557CB4CAAC0AF32DC756,
	Tg_Loader_LoadUrlImage_m44DC8C20A059F5D2235ED1FCF301161E9AA73DE4,
	Tg_Loader_ParseFile_mDE501F1209DD61E886C5C0CBFF9CE0DBD89C91F6,
	Tg_Loader_OnRequestFinished_m110A8D30271106ADA81425D0B4A7ED117EB01C87,
	Tg_Loader_OnRequestFinished2_m53712D6E15AE43D475930401AEE59024CC80ACBB,
	Tg_Loader_callVag_m17EF8B802D02053FACE4BF714FA90AA24A72CDF6,
	Tg_Loader__ctor_m4EE6E4524974A5ACCAD1B1BA45DD53709F78911D,
	U3CprocessVideoU3Ed__53__ctor_m7B70B7D8BF77E0FE33C6537F49E42DECC9E332B1,
	U3CprocessVideoU3Ed__53_System_IDisposable_Dispose_m37FE66AB29FCB306DCAF376FB882E412E22AFE8D,
	U3CprocessVideoU3Ed__53_MoveNext_mC5A0860E3885710853B3FBFDE2F95C906D5C5B22,
	U3CprocessVideoU3Ed__53_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m1C6D4906559004D0AECF5EE7A7C57988BA7F1088,
	U3CprocessVideoU3Ed__53_System_Collections_IEnumerator_Reset_mD22DA273ADD91480522C7DA18F33AC51D6B385C9,
	U3CprocessVideoU3Ed__53_System_Collections_IEnumerator_get_Current_m4F1A2B4117A7C2EC833EF6AE1A5850D342ACFE07,
	U3CexecuteU3Ed__55__ctor_m885AE33F260C60F219FD2B844EB98EBAF611BFF2,
	U3CexecuteU3Ed__55_System_IDisposable_Dispose_m34E1554BD78DCEF5CB43E68D4C8CB2F780E823B1,
	U3CexecuteU3Ed__55_MoveNext_m7F309D410CA598066B927D55993C4053C461FAA1,
	U3CexecuteU3Ed__55_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m288B19DEE73CCD61964017D6100AFCF20CB78E7D,
	U3CexecuteU3Ed__55_System_Collections_IEnumerator_Reset_m6A9EA4A1FB29D28B11DA5790C9B37518FA34AC81,
	U3CexecuteU3Ed__55_System_Collections_IEnumerator_get_Current_m6273F0AEF4942EFD70F45599A1B9C4D6EEBE3EAA,
	U3CshowU3Ed__59__ctor_mED8B04C32888BA300218A44889A2FA6E78DBD710,
	U3CshowU3Ed__59_System_IDisposable_Dispose_m6D7FBE31991420375ADB065BE2501AB8E107F00A,
	U3CshowU3Ed__59_MoveNext_m8FD67620C4BDDF6C236E0E78293296A394196C79,
	U3CshowU3Ed__59_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mF5565A2CDFD058A3644B0C1BB91FEC826D12F9C6,
	U3CshowU3Ed__59_System_Collections_IEnumerator_Reset_mCA2AAA628E2B90C96B6F0FC5D97ED1BEF9BA294C,
	U3CshowU3Ed__59_System_Collections_IEnumerator_get_Current_m17809F1B25774F439939ABB1FFA63DDDEFD7A790,
	U3CjukeboxU3Ed__63__ctor_m788936E37197E6C962E65ABD255191A13B7CACDF,
	U3CjukeboxU3Ed__63_System_IDisposable_Dispose_mDD459F3537DD1B15C788A209BC7D1767883D0849,
	U3CjukeboxU3Ed__63_MoveNext_m6FD3167752F55BD9AB3675D98AC63547413D836C,
	U3CjukeboxU3Ed__63_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m72BD4D30CE93F47422A50C9D496F82AF021B171B,
	U3CjukeboxU3Ed__63_System_Collections_IEnumerator_Reset_m702048AD1849520971A30FF9A525EC9E0654CD9F,
	U3CjukeboxU3Ed__63_System_Collections_IEnumerator_get_Current_m02B826D98C5608960FA65922030CA48AB17C0B18,
	U3CvideoRunnerSU3Ed__64__ctor_m67B95AC50CE8C446A7C3D63DDF3C80EF4A4FBD80,
	U3CvideoRunnerSU3Ed__64_System_IDisposable_Dispose_m0CD164B57208BFAC22F5B760A75552826800D423,
	U3CvideoRunnerSU3Ed__64_MoveNext_m690D04967ABA68549A936070189AF5CCB5A5427A,
	U3CvideoRunnerSU3Ed__64_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mBA9B20D22D57AF509774B5EB5E49A7760E1BD4CE,
	U3CvideoRunnerSU3Ed__64_System_Collections_IEnumerator_Reset_mF71A27C1DD781C6B04C4B72FBF148E550681745C,
	U3CvideoRunnerSU3Ed__64_System_Collections_IEnumerator_get_Current_m38928416452416CAF750A970B477DD913AB62DA3,
	U3CrenderVideoU3Ed__65__ctor_mBE99CFA8D2EDC000725338649BA61259B214AB5E,
	U3CrenderVideoU3Ed__65_System_IDisposable_Dispose_m8465EBA8E2A7CE6985EABB44296BC6859A0EBAA6,
	U3CrenderVideoU3Ed__65_MoveNext_m7E93D9F87DB90414865F56BE30B82E474DBAB39C,
	U3CrenderVideoU3Ed__65_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mB9AE8F7F4219F73097D0AF7F3AC907724C3FCD61,
	U3CrenderVideoU3Ed__65_System_Collections_IEnumerator_Reset_m6F4C4ABE6EAE1125927F81BE3F98F537ED0B569F,
	U3CrenderVideoU3Ed__65_System_Collections_IEnumerator_get_Current_mCA747C01DE5A1D63D3C3B02780016299904DF0FB,
	U3CGetMyAudioClipU3Ed__66__ctor_m57CD51B53186CD7850110F3E359E1B4CF083CFAE,
	U3CGetMyAudioClipU3Ed__66_System_IDisposable_Dispose_mC22B8F7AEC1FCB124388C1B7AAC4ABD270BABA6F,
	U3CGetMyAudioClipU3Ed__66_MoveNext_m5C0D21B299F6581FC59397447A73F6E8D4AF5343,
	U3CGetMyAudioClipU3Ed__66_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mF6043BA370A11DD4D0A7333309082073A8457E4E,
	U3CGetMyAudioClipU3Ed__66_System_Collections_IEnumerator_Reset_mE97DFD9B9BFA341BC9FC781A2BF96D49D6CDFD98,
	U3CGetMyAudioClipU3Ed__66_System_Collections_IEnumerator_get_Current_m8CB1CC81A5E3F3D1E865F39AEDF615C1DD2816F2,
	U3CLoadUrlImageU3Ed__68__ctor_m403B159AE1C276523A88D3CA0FEA0DF2F55064C1,
	U3CLoadUrlImageU3Ed__68_System_IDisposable_Dispose_mE4DEE79133EEB29626B08AD2F105582D10307573,
	U3CLoadUrlImageU3Ed__68_MoveNext_mDC991CABC2CACC0931FB1D2937A472EE86D8F3B5,
	U3CLoadUrlImageU3Ed__68_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mD7961709D007A131EBCFB1D2FDAC358B2A414373,
	U3CLoadUrlImageU3Ed__68_System_Collections_IEnumerator_Reset_m055D45D65A5024E94C9D251BA1798D6753FF1FF9,
	U3CLoadUrlImageU3Ed__68_System_Collections_IEnumerator_get_Current_mE2FA24BB2ED24E0FEAA6D8C3BA07B34969CACD75,
	U3CU3Ec__cctor_m24AC40C463D0D71908BE3C91E3274D64D0A42D5E,
	U3CU3Ec__ctor_mA32FC2881122187E8A4CC946785E489F46DC8454,
	U3CU3Ec_U3CParseFileU3Eb__69_0_mDCEC96DADDB5E4F2A6170B58F30FC011182424C8,
	U3CU3Ec_U3CParseFileU3Eb__69_1_m998C4700B033561B487A648DDD94210AFC59540C,
	U3CU3Ec_U3CParseFileU3Eb__69_2_m8ECDFAE6B47D9AEB5455407B05438ED4F57EBE66,
	U3CU3Ec_U3CParseFileU3Eb__69_3_m8E680D83C4790B2075934788B651AE8DBFC59C0C,
	U3CU3Ec_U3CParseFileU3Eb__69_4_m657B0A9C0A21682E166DD8E8F5D9BC2E24C3797C,
	U3CU3Ec_U3CParseFileU3Eb__69_5_m4D7C76EDDE00FE2030D2E057019224DD2DA16AC9,
	U3CU3Ec_U3CParseFileU3Eb__69_6_m6BEE3D7A47BE8DCACDBAD7E9F0CD4B2AD4768E29,
	U3CU3Ec_U3CParseFileU3Eb__69_7_m3517A4DF12AD7FB872B4F1A9BBC7A0CA36CD7F03,
	U3CcallVagU3Ed__72__ctor_m15E218A419C37BA70C856A045D1411D20A138D62,
	U3CcallVagU3Ed__72_System_IDisposable_Dispose_mE652410C5E070E38174A8796B5BF07B1079570EF,
	U3CcallVagU3Ed__72_MoveNext_mC0C1FCFD1ED4F3B395C1522E6A95ECEBD99C3B7F,
	U3CcallVagU3Ed__72_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mB62BC9086B174EA111632FADC2884C460F1A6460,
	U3CcallVagU3Ed__72_System_Collections_IEnumerator_Reset_mC64D65BC96BE24DF9A18FE5673CFE248F4DE54A0,
	U3CcallVagU3Ed__72_System_Collections_IEnumerator_get_Current_m9994C3DC29325A4E463DC31B715136A91257FEA5,
	VideoDownloader_Start_m981B7942A953FF6074D6EE3EB3720311918B6945,
	VideoDownloader_go_m1327A11969E9C5E6C844765D09CCCB9DC467C72C,
	VideoDownloader_playVideo_m464C5DC3E3CBF21319197C6E8925A69011937C5C,
	VideoDownloader_DownloadVideo_m719763A83CB4BBEA82DF740B54CE0AA7D21D2C2B,
	VideoDownloader_wwwDown_m88F7A3D54F0CC998DB5854A764E1B6FDD11F6D04,
	VideoDownloader__ctor_m2DE017284C80E89EC8D9DDB9800B99027E7540DF,
	U3CplayVideoU3Ed__10__ctor_m48975DF9569E6A0218A13559373B12BC4D27170B,
	U3CplayVideoU3Ed__10_System_IDisposable_Dispose_mA1CD8004810E0FE8E5191C673EFF8C069FEE47B7,
	U3CplayVideoU3Ed__10_MoveNext_mC664DC7BC632818612305FB2F7A42C8F4E1CE65A,
	U3CplayVideoU3Ed__10_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m2058D973233485B77635A9630795C8319CF63170,
	U3CplayVideoU3Ed__10_System_Collections_IEnumerator_Reset_mA64E97F2F124BABECF11154E0E411F6364940F88,
	U3CplayVideoU3Ed__10_System_Collections_IEnumerator_get_Current_m4B13895F46BA6B8932236B660F9C52C4D74D772B,
	U3CDownloadVideoU3Ed__11__ctor_mA5D00A3222F41CB502644E4F5C1AA011838AE73E,
	U3CDownloadVideoU3Ed__11_System_IDisposable_Dispose_mB55FB69115EC712D31633C5E75C6A4A42C0EB767,
	U3CDownloadVideoU3Ed__11_MoveNext_m5D848DB95441751CF5ABBDA6F7AA73217F2CF6B7,
	U3CDownloadVideoU3Ed__11_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mDE7DB4F243F436C386E8764D2043BE462F5756A2,
	U3CDownloadVideoU3Ed__11_System_Collections_IEnumerator_Reset_m8139EF4BAA0F855AD1DA43CC07DAD62C7E88BE79,
	U3CDownloadVideoU3Ed__11_System_Collections_IEnumerator_get_Current_m4270645782C032FF5BCAFDD9F3FCFEA1D8BCFE49,
	U3CwwwDownU3Ed__12__ctor_mB02E1A9B8595456FDDA6BCC21A45844C806CE592,
	U3CwwwDownU3Ed__12_System_IDisposable_Dispose_m9F170D89BBB1EDDD61A5B103A61DAE081B6D1324,
	U3CwwwDownU3Ed__12_MoveNext_mAB551D8FACEF500ABAEE46E502D3165076C5E29E,
	U3CwwwDownU3Ed__12_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m9D49DDACCDA9C860FE8957F4B402CE0BCA608859,
	U3CwwwDownU3Ed__12_System_Collections_IEnumerator_Reset_mC73066870C1B2956AC57AC9FAFD000EC970BAC09,
	U3CwwwDownU3Ed__12_System_Collections_IEnumerator_get_Current_m773C343AAC269EECC3E25A1FA999D78EE6479A94,
	Web_PUT_Uploader_Start_mD07675BE02DB53BFDE382EC2F7C987E173338AF6,
	Web_PUT_Uploader_Upload_mBF754827DE2B7976A8E46A639DCD938E44833AA6,
	Web_PUT_Uploader__ctor_mF88145246EAD5E7C054D6A9BEAC2DFA4052053B6,
	U3CUploadU3Ed__1__ctor_mB0FECAF97BE4A068EDAAC30E6324B3E31F8164E2,
	U3CUploadU3Ed__1_System_IDisposable_Dispose_mC480BEB8A2C0442E587E792F4090A02A78DC4734,
	U3CUploadU3Ed__1_MoveNext_m27B122245D63433BFC88CB9C2CE370A6841B82B9,
	U3CUploadU3Ed__1_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m792CB3C128BEDEB3DD57FD5AF92D25550125389C,
	U3CUploadU3Ed__1_System_Collections_IEnumerator_Reset_m5C31F98908F060ECF2EE769D3BBDAC0A8741EBBA,
	U3CUploadU3Ed__1_System_Collections_IEnumerator_get_Current_m28E0F47DB35842744912550EC682E295694E5222,
	WriteToFile_Start_mC9EF0CEDE2F185996B2712E971D985CE0BB35293,
	WriteToFile_Update_mCECB51DDCB8A8F806CB38C3CB2A83D8FDB15B842,
	WriteToFile_UpdateSpeechDetected_m895F73FEB26EF77DD61AEA9DC1005FBCE4B4799C,
	WriteToFile_SendToServerFile_m977EF05F9079844F1F05EC65D085F84CE4D2E4D0,
	WriteToFile_GetFromServerFile_mF25678AD251EEF3E4AE248CA88F9ED08435D136D,
	WriteToFile_WriteToLocalFile_m1C36142BEC937E9093EF53D00F49D2F6AA351DF6,
	WriteToFile_SendSocket_Webgl_Rest_mC18A0269B6A431B2017AB66534DE2BA44F0B17AA,
	WriteToFile_sendTextToFile_m3F01F72F56C5AC7DE226C0F09C04F6243751D6D9,
	WriteToFile__sendTextToFile_m32AD1B51D54993753117D111C89CD17AA36E6CFC,
	WriteToFile_getTextFromFile_mE25957A8B76F6D530B69C7CAB54E82B14CEB0D8B,
	WriteToFile_LoadUrlImage_m927ADBC753815ACBC4F44A95707C265149E5A41E,
	WriteToFile_GetMessage_m73CBDFFD048DE6334EDD6C19C373854089777295,
	WriteToFile__ctor_m366A4DB41C8CBAF4ABC8F030C48C4224A6568F1A,
	U3CU3Ec__cctor_m8DD5F7B81D82EBF4219819D8D2A7071BAD3C8DCB,
	U3CU3Ec__ctor_m5DC2214FBEBEBCD4EEAC781FAE943B05974F9BB9,
	U3CU3Ec_U3CSendSocket_Webgl_RestU3Eb__24_0_mABF1EEA8CCC68ACE6328DC07F7A0A7BF76149A9B,
	U3CU3Ec_U3C_sendTextToFileU3Eb__26_0_mCC834519DD98659AD0D448117EFFDACA74476F0B,
	U3CsendTextToFileU3Ed__25__ctor_m5CA84019D801CE1AE60A7736EE43041D3482BB55,
	U3CsendTextToFileU3Ed__25_System_IDisposable_Dispose_m5B6067FFCDF7ED61689C9DA39056B8C5AB5A3768,
	U3CsendTextToFileU3Ed__25_MoveNext_mFA1CC8ACB450A11F354BDD5CFAB7587CE8AA9C8F,
	U3CsendTextToFileU3Ed__25_U3CU3Em__Finally1_m8015E290EEE91E319D12D9C8E4AACF8060860F1A,
	U3CsendTextToFileU3Ed__25_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mF579AE8FE6888EA8DA2F4E76C8D56CCB80641E39,
	U3CsendTextToFileU3Ed__25_System_Collections_IEnumerator_Reset_mA35A79D89500DBDE609032742D0A20C9C07DCCEE,
	U3CsendTextToFileU3Ed__25_System_Collections_IEnumerator_get_Current_mE96BCA88012F1F105791A2008A849CA51B5B7365,
	U3C_sendTextToFileU3Ed__26__ctor_mAB45EDBB9B379E41B188DBD26506F62F6E07D71B,
	U3C_sendTextToFileU3Ed__26_System_IDisposable_Dispose_m8C9F2E27544C986C5DEC10AAA70C01DF34D9098B,
	U3C_sendTextToFileU3Ed__26_MoveNext_m0FCF5889984BCF28A1F4C59FFA6823E0FB69CFC7,
	U3C_sendTextToFileU3Ed__26_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m36F7678FAE257FAED4BF7E351E5F0AAAE6E18B42,
	U3C_sendTextToFileU3Ed__26_System_Collections_IEnumerator_Reset_m5ABBE0B9D68C0EAE2C2B2DFC1A0C3B754A98BA1E,
	U3C_sendTextToFileU3Ed__26_System_Collections_IEnumerator_get_Current_mAA6EB7EEEB6EB8EB2D26B6AE55707DFFEC36096A,
	U3CgetTextFromFileU3Ed__27__ctor_m1996F982FF8DE37EC6A3311041031D1CFF978160,
	U3CgetTextFromFileU3Ed__27_System_IDisposable_Dispose_m5BEE6B4F978B9012A2145393E3E28DBB721629F7,
	U3CgetTextFromFileU3Ed__27_MoveNext_mC2E33995CD095700C10FAFA885A18D0E80DB42CE,
	U3CgetTextFromFileU3Ed__27_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m2F5564C3E17D6DE1641D6D438A16A9671D6DF424,
	U3CgetTextFromFileU3Ed__27_System_Collections_IEnumerator_Reset_m90DEE2DEA6F4D21D4634806909720CBEF37AC4FC,
	U3CgetTextFromFileU3Ed__27_System_Collections_IEnumerator_get_Current_m3CAE87CA87CE43722580119C4FC7D3DC30C33676,
	U3CLoadUrlImageU3Ed__28__ctor_m4D0E48BFFBC2FC935E4C4346DAAD2486048F4D61,
	U3CLoadUrlImageU3Ed__28_System_IDisposable_Dispose_m8AE6CAE8E82011B361B6035FA48D11B2653E052D,
	U3CLoadUrlImageU3Ed__28_MoveNext_m0F77B36D4F4770D1958EC3D057349B3C0A8F9167,
	U3CLoadUrlImageU3Ed__28_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m1F8F91750191D9B7E7DAF80EEFD98A76C58D47C1,
	U3CLoadUrlImageU3Ed__28_System_Collections_IEnumerator_Reset_m53BE2A5492898B0852E5A7C936591F207CA17B87,
	U3CLoadUrlImageU3Ed__28_System_Collections_IEnumerator_get_Current_mA4391FEBC347FD18BCABF41300102777467F0AC1,
	XMLProtocol__ctor_m51C46F8787202A3C17925D7111B81A0EF3B6B569,
	animatorEngine_Start_mC80CB028703CB0C21E5B31B2B3A66B852F642B80,
	animatorEngine_triggerPersonaggio_m48817FBE94D9CA2C05A4DA17B2509C939926EABA,
	animatorEngine_setTriggerAnimator_m3AA43F1B83785553DA3D8007A583767AC314F338,
	animatorEngine_animaPersonaggio_m5F8CF655F4FB45507D097E3B5F551E3EDDD6A9FC,
	animatorEngine_cambia_espressione_m1AB373B0E36FB31CFDAB7F504E7B49828129662C,
	animatorEngine_espressioniFacciali_m67CEBD66750D5D065E939273619E5E2BD4D0BFF3,
	animatorEngine_setEmotionStates_m133520155E108B929AD62EA0A6BFBFD41A724330,
	animatorEngine__ctor_m9DD455CC225EE8E019ED7B045E74A1F98D639FDD,
	U3Ccambia_espressioneU3Ed__6__ctor_m37941ACC1E2EC38754327F3A7615973A5F429219,
	U3Ccambia_espressioneU3Ed__6_System_IDisposable_Dispose_mB674941E9C97E1E818F8C1277B9748F353A484C6,
	U3Ccambia_espressioneU3Ed__6_MoveNext_m2176A187DF33EDA4290358A1B8319132F9CE7C10,
	U3Ccambia_espressioneU3Ed__6_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mA88DEFCB74CBE4F92B568E027D6616395650CC6D,
	U3Ccambia_espressioneU3Ed__6_System_Collections_IEnumerator_Reset_m2DFC0C7C71CDAF37DF1A65B7425E90D5F175D7FC,
	U3Ccambia_espressioneU3Ed__6_System_Collections_IEnumerator_get_Current_mC47E1B6CE7ADEEFD114DF06155249C36008C0024,
	U3CsetEmotionStatesU3Ed__8__ctor_mF84C7F02F7D8A0CA096DA9D4F146B519BF400AF0,
	U3CsetEmotionStatesU3Ed__8_System_IDisposable_Dispose_m873CA8C25018771442F848D5343D093AD67CF013,
	U3CsetEmotionStatesU3Ed__8_MoveNext_m2F0E73AE5FD286FA52791AB374FE795768A990F2,
	U3CsetEmotionStatesU3Ed__8_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mA2F305461E014333419F46D70F68050F92CC8B9C,
	U3CsetEmotionStatesU3Ed__8_System_Collections_IEnumerator_Reset_m02D991E812CFAAAC73A3DA52F299AA23525A3BB7,
	U3CsetEmotionStatesU3Ed__8_System_Collections_IEnumerator_get_Current_m49E11D945FBE50359C677211178C7096F0F494E4,
	audioConvert_Start_mA98AD88ADDB98CCF1C72656977E25E11D99007EC,
	audioConvert_Update_mCD8F3433C50EEEA8EA7B718D1C1CDE85F96F6C86,
	audioConvert__ctor_m0A93DA43F6D54357C8AEA2738A240A46BEECE9A9,
	blendShapeExample_Awake_mCF676437EF9D90EC93782490892AF482675DDC0C,
	blendShapeExample_Start_m2663D7C178438BD6D3BD23F22C57EEE1EB3B11FC,
	blendShapeExample_Update_mD15B5A3FB15D8AAD0235E423C3C74631A9D790DA,
	blendShapeExample__ctor_mF7FF586DA9C64F78DA39C24749C53BF137732509,
	clipSwitcher_Start_m38AC67C2A9CB737EA1DC0619A5C72F50EFB2E0CA,
	clipSwitcher_Update_m764606566967531974A5F7CB035535479DE38FC2,
	clipSwitcher__ctor_m6DA74610F0F498224F64166C8BFA7EE899F1E7E6,
	constraints_Start_m092A655C3E727266A67CC25FAE62197820866F5C,
	constraints_Update_mDABF1459EF8A25239FB5438B6CABC90CBD51D503,
	constraints__ctor_m7CCA24C67313F5790DE35D3DA28045FEEFE220EA,
	doMainThread_Update_m34C2C2817775E05F992FDDBD43EE5D4051822D6D,
	doMainThread__ctor_m6FD6BB2BCE7DA34000C33FA669D7C3CA4B80682C,
	doMainThread__cctor_m23374A28634144E63EAF541B730854B80B2E37CD,
	esempioAudioPlay_Start_m139818446A75AE7A76B05082F27F0729B0956E0B,
	esempioAudioPlay_OnGUI_m86A951562F40E0B55DEA581AFFE6A8E3E929F0C3,
	esempioAudioPlay__ctor_m4CFA94F70B93AFF4D9BCF6B08FD312A078201642,
	exampleClass_Update_m0DFB12E8AAEFDDEF904AD10A9040F9F18C21B5A5,
	exampleClass_Start_mE068BC0C37959E7A262BA1B27A86804A513EACD1,
	exampleClass_WaitAndPrint_m30E47268495281CB0951B0E1F2F248B6928237C7,
	exampleClass__ctor_mE212F3C2CD04FC56A13000F8DED71092FEBD6A5B,
	U3CWaitAndPrintU3Ed__4__ctor_mB34E8AC7D21DC592BF53C2929D36790E60DD9086,
	U3CWaitAndPrintU3Ed__4_System_IDisposable_Dispose_m9E29FFB94187C72312B70ACA4CF9F7532FF9D0BA,
	U3CWaitAndPrintU3Ed__4_MoveNext_m8999ECD7672AF76D103D6F0F32F99ADBE8E7CB80,
	U3CWaitAndPrintU3Ed__4_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m1143F5D55709B0DEF0F3A95E5EE814371120E30A,
	U3CWaitAndPrintU3Ed__4_System_Collections_IEnumerator_Reset_m29D707B34A5DB5F8C69BB6586FD29B6069C46B85,
	U3CWaitAndPrintU3Ed__4_System_Collections_IEnumerator_get_Current_m484CB267B62893FBD4BD76BA337B8CAD696F0EF0,
	exeLauncher_Start_mF47C931BF21E799EB06E2767AC8E0318209354CD,
	exeLauncher_calcola_mC340B56D339C55B7C6B85F2723E170281ED61214,
	exeLauncher_AvviaServizi_mA3FA8E5938A675E555EE7002CA4B8D64B54576AE,
	exeLauncher_launchChrome_mBCA6C8195269C8B0F28EB1293334D8302ACB7A4F,
	exeLauncher_tastiera_mD0E0222AE231EA2F72155DA39EDB87D2A049BF21,
	exeLauncher_mandaWhatsup_m58BDE69EF4EE453C13290CA37A6693FB17D00149,
	exeLauncher_preferenze_m6380D683A77C8DBC19010759AB895D1371727AC0,
	exeLauncher_operatore_m36AC70D7A78A0DB1B4D7417E5ED78ED0E462EA5F,
	exeLauncher__sendTextToFile_mE36165DEE6D627D7AA689A4064CBD37A9A9DE715,
	exeLauncher__ctor_mF9ADCAA5310C435045EE16F529C175651A0A13EF,
	U3ClaunchChromeU3Ed__14__ctor_mA94C1B58D26CD87E44002F155E0BEB916DC5906C,
	U3ClaunchChromeU3Ed__14_System_IDisposable_Dispose_mCE3086F48436531A2829EEB9FD4F534563422E3E,
	U3ClaunchChromeU3Ed__14_MoveNext_m9CF0CC710BE88340A215A8968BA59768D28B7168,
	U3ClaunchChromeU3Ed__14_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mE96CC10804F0AAD4E6547A009D03A5381E6F2F98,
	U3ClaunchChromeU3Ed__14_System_Collections_IEnumerator_Reset_m3EAFEB2C8B9B725C3C67C225B265339539DC7AF9,
	U3ClaunchChromeU3Ed__14_System_Collections_IEnumerator_get_Current_mC524A488EC7A15ED76D300C708933BD753E99DAE,
	U3C_sendTextToFileU3Ed__19__ctor_mE9FFABEB76B5B9CCBA6EC682C48E72A9DCB697D0,
	U3C_sendTextToFileU3Ed__19_System_IDisposable_Dispose_mD837E0E5FAE686F346B65EB74CFEC09737C6D85A,
	U3C_sendTextToFileU3Ed__19_MoveNext_m5EB1EA190AB3ABC98B1A2B6CEB50A055BB475B32,
	U3C_sendTextToFileU3Ed__19_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mF2C1F447744A21075C51143C24705339639EBA5D,
	U3C_sendTextToFileU3Ed__19_System_Collections_IEnumerator_Reset_m25ABAE019024D27530C31311990CF4CA72CDD346,
	U3C_sendTextToFileU3Ed__19_System_Collections_IEnumerator_get_Current_m9E64F4BA0D1979835A44686AF23109DDDD13B788,
	globalStatus__cctor_m4681F2EF2427E4CB68EB708F1F59FBEF31280A6A,
	globalStatusEmozioni__ctor_mD8FB012EF4E15B5E08E6557BBA718E736869751C,
	globalStatusEmozioni__cctor_m2ADB64A9441211DDFD58DA325832340188ACCB87,
	Keyboard_scriviKey_mAB3406F91ACDBC113D8167B3B3196460FB29C10A,
	Keyboard_Update_m3D5A2385BA1DFC226C5A0976A74DD5F378488E82,
	Keyboard__ctor_mA16C7D9533AB869EDEF4DB1DBACD90819A2B4F2E,
	keyboard_Start_m7795BC139A4BD0C3FE61A57ABF9AC16F90256BEE,
	keyboard_Update_m3AE9F04A8442AF29FE1E21CCA891FAEB0BB4124D,
	keyboard__ctor_m8EF72015AC926D09FBBF6119D3E4DBB54269BF25,
	mouthController_Start_m26059457DE0285D18F7ECDA5D2BB319FED2C6269,
	mouthController_Update_m5D7BBFD8E556D24273F963D885E2C7B257A04C37,
	mouthController__ctor_m3E457B683F82AC63A4A522BFFF63B8AEB44CC10C,
	openLink_OnPointerClick_m8C5AADC0651CDAFB701716398718CBCF3E4C1398,
	openLink_SetLinkToColor_m84F23CF13681BD5DB1A7E25962D9111F73087262,
	openLink__ctor_mEC3671EE29544BA4C194039ED0B20652084C638D,
	overrideText_Start_m6150B2946CF045C31BE0876A5BC2F3B56877E8FC,
	overrideText_Update_m385DC9642358D59B4DD55A9C380AD8B835E979F7,
	overrideText__ctor_m4EED847BD01D416F69D6B8DBAC6F71091CFF637E,
	sequencer_Start_m44B2EA4667EA89B1F179075E5BE1C7595A6F7394,
	sequencer_Update_m535C42335A7B2DC1D6D66F5F135EA62CD89670F7,
	sequencer_VideoAnswer_mC501069F7DF027136AF3AC222AC0D91E74C6B89E,
	sequencer_VideoAnswerSad_mBE7CAAB9A1E188A2D7E9CD46A662CEB318EB10AE,
	sequencer_delayPlay_m2C27D9DEE2EF80430BD8E79D65DC69BDF36BDB56,
	sequencer_videoFakeSync_m6344060F4F752FEBD2AFAF3187469D5DD3F746F4,
	sequencer_videoBottoni_m45758BE12A0380B723C159515B742ED5A7019E01,
	sequencer_intro_m1B99E6801F07FBE2BA9B462242F959A666DF17D8,
	sequencer_EndIntroReached_mE5D5F901B3DDB9D7DDF9BFDD0EB5650F65AE529E,
	sequencer_EndFinalIntroReached_m0A7678C38FE63487B0C4073CDFEE75C88AD28952,
	sequencer_introRoutine_mF694342DA868D09838A3FA0E48663B0239FEC192,
	sequencer_introRoutinePart_2_mE423BE36BD46E168F951569F11E464DBCAA8D2AA,
	sequencer_pausaVideo_m95D64B2D60518300F1975020A6934E4477B28253,
	sequencer_StopAll_m7FD2057845EE6C17D77F97D84EE0AF1C6097F25F,
	sequencer_clipBlendRoutine_m5777CB407F60EA559971DC0D5A0780DB9CA6F21A,
	sequencer_playVideoTemp_mB6DF86BDB360FCBBD7D1548EFAC4201029283510,
	sequencer_playVideoWeb_mF5AD6092008B0A53439AFCBC40E54E94D4845C40,
	sequencer_loopsmileCall_m35A836088CAA74A08DAAA049E4C312F81CB8EE46,
	sequencer_saluti_mFF9C0F8E08F9D5F333411EFC5F0D26680124EFCE,
	sequencer_playVideoAfterOgg_mF2B0432AD6E93B47B7A6061BA0408756605D217A,
	sequencer_VideoPlayer_errorReceived_m5FD7B2D21B4784009E999730DC5AB34BF8DFDB2B,
	sequencer_Attacco_seekCompleted_m3D78B8C0129A3CA2B5F85E0EAA05E3B4B019064E,
	sequencer_EndServerAnswer_m1CBE8E9EFA06CCF2DF16715C0A69832F5C1D34B7,
	sequencer_serverAnswer_m733203EB6644DCE032EA6DD2F9C62B21906C5DD7,
	sequencer_Prepared_m2279CFD5C1AAA16B41961279EBA6B7BAA23E4387,
	sequencer_EndReached_m2BBF7350C4D32232764DA0A498CF130D6E93DB0E,
	sequencer_EndSmileReach_m7245202758A4FBECBC5B8E51282B2CF84D14A7C6,
	sequencer_EndLooping_mA8A174E81B6B0C65B7DB372E1000D43DC23CEB63,
	sequencer_loopVideoPlay_m93CD8FA5AA4518DDFE6CA097569BD60EBB7C7E91,
	sequencer_disableMic_m8B1850F9C05C5C7DEDE1833D262C9749EC7C9E57,
	sequencer_openAndroidApp_m0708DDA16AFD5F04205EA2F65C7E0C91C1B6B1B1,
	sequencer__ctor_mF866D4A45110DB69B67995C303CB5121B7431BF5,
	U3CdelayPlayU3Ed__29__ctor_m023458AEFE269E6C89507FB7218EB590E87E955C,
	U3CdelayPlayU3Ed__29_System_IDisposable_Dispose_m8E5CB4A389E9B5BDCD052E072E4B28EA900D9A96,
	U3CdelayPlayU3Ed__29_MoveNext_m73D0FDE4170A15C42C87A1CFE4474E1A7A859C58,
	U3CdelayPlayU3Ed__29_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mCF01DD925FF5A2BC33C4536771F7BB737197835D,
	U3CdelayPlayU3Ed__29_System_Collections_IEnumerator_Reset_mE83F5461ACB3605475EBB779B0DDF046AF4F9C82,
	U3CdelayPlayU3Ed__29_System_Collections_IEnumerator_get_Current_mF094F9F11945B9C641F4022CC73F84B3FB0EF9D4,
	U3CintroRoutineU3Ed__35__ctor_mD4511881F41425ED17013437A26238EFC5F557D9,
	U3CintroRoutineU3Ed__35_System_IDisposable_Dispose_m75C57FD288A1193B11EBD5282EF84E447C7D75A3,
	U3CintroRoutineU3Ed__35_MoveNext_mCC9A692709EF57A1B637336A5F2862CE4810DD97,
	U3CintroRoutineU3Ed__35_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mE567ED81FED53E5DC22ACD282AD8908AE93F4707,
	U3CintroRoutineU3Ed__35_System_Collections_IEnumerator_Reset_mD6490C3D42B4A674F63C339FA17FA374FD95A288,
	U3CintroRoutineU3Ed__35_System_Collections_IEnumerator_get_Current_m07BB886C11DD160FE11410B0855CD58D0E43A595,
	U3CintroRoutinePart_2U3Ed__36__ctor_m1CE24248CAD65B75F5AC3B9C57CCF4C972976CB3,
	U3CintroRoutinePart_2U3Ed__36_System_IDisposable_Dispose_mD9E5A332FF0A47F6511EFC435A6C07919CB157E6,
	U3CintroRoutinePart_2U3Ed__36_MoveNext_m14EE9FD537E85B879F85056A879C4C5D0D42749B,
	U3CintroRoutinePart_2U3Ed__36_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m3D544CA3FEEF56A4D99DC4621BC765425545A8CC,
	U3CintroRoutinePart_2U3Ed__36_System_Collections_IEnumerator_Reset_mB12C6AC3D5C6545E4364B05824A5BCA7E283BD04,
	U3CintroRoutinePart_2U3Ed__36_System_Collections_IEnumerator_get_Current_mEFEC7F42E9F209C8EA4A6A3D5491076CF5F19394,
	U3CpausaVideoU3Ed__37__ctor_m6A237DD1D0C2BA335BAD8B14547A2DE735E707FE,
	U3CpausaVideoU3Ed__37_System_IDisposable_Dispose_m7DE30BAF2006A568D628AB0B8128BDE5254FDA44,
	U3CpausaVideoU3Ed__37_MoveNext_m0B37DA99178FD7B669AE44C5E17D3F5E605D04DD,
	U3CpausaVideoU3Ed__37_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m4BB781283CF74A13FB5C3D1BC74DE1C392CD41FD,
	U3CpausaVideoU3Ed__37_System_Collections_IEnumerator_Reset_m2ACE47CA8797EE478BF541C7A45C908AE8E30409,
	U3CpausaVideoU3Ed__37_System_Collections_IEnumerator_get_Current_m8A9C24E784FB6EFCBC6F7764270FD1C4EB898264,
	U3CclipBlendRoutineU3Ed__39__ctor_m0513167C124D47880554B065F732E32E60B72EFA,
	U3CclipBlendRoutineU3Ed__39_System_IDisposable_Dispose_mA8DCD77584821A23A580B8C361B5782460484AAC,
	U3CclipBlendRoutineU3Ed__39_MoveNext_mBF9F452148A4653632034BAE3DA4DFAED8147157,
	U3CclipBlendRoutineU3Ed__39_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m9212C3113F1D91F3EF359FFAF2ADEA4083CA4FAE,
	U3CclipBlendRoutineU3Ed__39_System_Collections_IEnumerator_Reset_m1491D7E4C12382C1EDB32D776FFBD7854DECC066,
	U3CclipBlendRoutineU3Ed__39_System_Collections_IEnumerator_get_Current_mAE6D78CDCB120494C5D87AC011688A836D0DBF87,
	U3CplayVideoTempU3Ed__40__ctor_m5DB1DE4D9F1096B68A80D91596E1C6FE65192F43,
	U3CplayVideoTempU3Ed__40_System_IDisposable_Dispose_m200C5562175D9711226E368E8A347B65BDBF3048,
	U3CplayVideoTempU3Ed__40_MoveNext_m372EC5CF1247FB1CE529B8BAB44179BD6FDD8DD7,
	U3CplayVideoTempU3Ed__40_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mDE4662AD9BB3CF9931EF82ADAD99444B9F6C053C,
	U3CplayVideoTempU3Ed__40_System_Collections_IEnumerator_Reset_m9E57D1B6D239620707494DB871C165AA69C4CAA6,
	U3CplayVideoTempU3Ed__40_System_Collections_IEnumerator_get_Current_m856C864380F20D4EDE78DDC337EE71984A2990E5,
	U3CplayVideoWebU3Ed__41__ctor_m03DBC883D90CB73D90E1CF808348B6F42AA51BC0,
	U3CplayVideoWebU3Ed__41_System_IDisposable_Dispose_m6A1C0908A37CE8395C4B07E71F078AA871E7BE43,
	U3CplayVideoWebU3Ed__41_MoveNext_m16F20FCD6F2854AA18B66C9D62DFD2C7E67C0EE3,
	U3CplayVideoWebU3Ed__41_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m8FA3A494572BB7E37C49A591A12DCE9BE0124858,
	U3CplayVideoWebU3Ed__41_System_Collections_IEnumerator_Reset_m23DC531DD6A1FC5FDED8BBCD9F2241DF6C0FDAB3,
	U3CplayVideoWebU3Ed__41_System_Collections_IEnumerator_get_Current_m85A6DD711F0C01831828EBFAE741995EB4253E34,
	U3CplayVideoAfterOggU3Ed__44__ctor_m53D6EC5B96A0E3C345DABD4F4D0E8340B32AFA26,
	U3CplayVideoAfterOggU3Ed__44_System_IDisposable_Dispose_m308BB69BA17323764DC46B437B2088610322EDC4,
	U3CplayVideoAfterOggU3Ed__44_MoveNext_m00D63ADECC606F0D21102B92BCE5CCAA6C6A75F2,
	U3CplayVideoAfterOggU3Ed__44_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mB34F583ADD2FE9A78CF25A847EE032A8F24D5F44,
	U3CplayVideoAfterOggU3Ed__44_System_Collections_IEnumerator_Reset_m4144611BFCEDEFDF31045F7F4363B4A210EB36F2,
	U3CplayVideoAfterOggU3Ed__44_System_Collections_IEnumerator_get_Current_mEC07399EB4789093884702EDB15B6ADF6AC7028A,
	U3CserverAnswerU3Ed__48__ctor_m6A7A92FEEA87A2E8E92DDB303E5D3785CC7942E9,
	U3CserverAnswerU3Ed__48_System_IDisposable_Dispose_mC484F81B2511141889656C56B2D9808DBA35256A,
	U3CserverAnswerU3Ed__48_MoveNext_m2136857CB980AD4C5E8A2EAFC21C05C51C026B92,
	U3CserverAnswerU3Ed__48_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m014F771A4989B9F2938634B712BB4B50699FAFDD,
	U3CserverAnswerU3Ed__48_System_Collections_IEnumerator_Reset_mCDAD61CD7BB37FE52EFE010336E45DF69DB5E25F,
	U3CserverAnswerU3Ed__48_System_Collections_IEnumerator_get_Current_m1548162E65DA70F46F48C11113719A77EBBA6555,
	U3CloopVideoPlayU3Ed__53__ctor_m70D40300314FFD6B16AED50E3542946D07197535,
	U3CloopVideoPlayU3Ed__53_System_IDisposable_Dispose_m26A043E63DD995EA9575BA5BE1185B876211B4D1,
	U3CloopVideoPlayU3Ed__53_MoveNext_m6BB8D9A55CB2C93DCFAC6AF282F9D16B8E22B6A5,
	U3CloopVideoPlayU3Ed__53_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mEFD5BCB35D349C5A91E6B2C02F9BD3AC09A71AFC,
	U3CloopVideoPlayU3Ed__53_System_Collections_IEnumerator_Reset_m1D4755D5A33141216DB874577F07BC1B5FAB65BB,
	U3CloopVideoPlayU3Ed__53_System_Collections_IEnumerator_get_Current_m128DB3233632224FEAC6FF021B83A9EA6D6C84FB,
	U3CdisableMicU3Ed__54__ctor_m29BFBD22091AF6A22313F28A619FB7F1089779C0,
	U3CdisableMicU3Ed__54_System_IDisposable_Dispose_m52DDC7F3C2823F6D01F9379E295935587AE5D284,
	U3CdisableMicU3Ed__54_MoveNext_mDF20A351C2D4A49B6A0EEFE73280EE445C5C0070,
	U3CdisableMicU3Ed__54_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mAC3CF3E21B98C243CD7C30E5923D066452A190AF,
	U3CdisableMicU3Ed__54_System_Collections_IEnumerator_Reset_m9F909940FD093B5EF92539B5F150A693AD1B90EA,
	U3CdisableMicU3Ed__54_System_Collections_IEnumerator_get_Current_m3E4DFB35BF5BF11128B24968423BBD9BF2F24AFF,
	stateMachine_stoppaDetection_mB7D04DEED6218242C96FFB1CE248FBF45B4C0A1D,
	stateMachine__ctor_m0915C2A8F1B05569C3557CB569D43E973D606A3A,
	stringaUnivoca_ComputeHash_mDE4C8E9C4EF20A3FF1412E422A37184C7AED8DB4,
	stringaUnivoca_Start_mAD21C041B109D6FBA0356E6C674077B24D03FD67,
	stringaUnivoca__ctor_m0387A6028500A77743195875A93057773E8DE859,
	tastieraAndroid_Start_mEF6EE8B0BB3C11AF51F3B27791D87457409A0748,
	tastieraAndroid_scriviKey_m3EF72BD3069B33D1F7D1A9971B58CDE945860C91,
	tastieraAndroid_sendKeyboard_m9B980AA9F15BE0CB35A881A78DB1E38DA8EBDE0C,
	tastieraAndroid_Update_mFEEAA7DCA0BA0653C6DB2ADA328B3700E44D7E45,
	tastieraAndroid_mandaEngineKeyboard_m8B94726EBA2064621D43EC48324DD9DC485E8CB0,
	tastieraAndroid__ctor_mFB3D68AB0542B5974A0B51F814A65603C19FF649,
	U3CmandaEngineKeyboardU3Ed__7__ctor_mA34BC55FF6FE43098072C9FAB99A4278F901B48E,
	U3CmandaEngineKeyboardU3Ed__7_System_IDisposable_Dispose_mDBB540F6CD0AACAC51C9BFAA1551C810F5726E20,
	U3CmandaEngineKeyboardU3Ed__7_MoveNext_m4A5FB063C3BEC62AD02268EC4B7DFDE38BDCD412,
	U3CmandaEngineKeyboardU3Ed__7_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m33E0B67C389877142FB78A7CDD956ACA9F1CFE6C,
	U3CmandaEngineKeyboardU3Ed__7_System_Collections_IEnumerator_Reset_m665DDB67539ACABA1230337C5962CE3D1508CCDE,
	U3CmandaEngineKeyboardU3Ed__7_System_Collections_IEnumerator_get_Current_m9BF8AB2847D11FE6587683203B39421E2EA71A01,
	textFadein_Start_mFE97649FB0528D95C75DDD86338C532E0B2FB042,
	textFadein_animaTesti_mD2B040FD63DB912F6F9AE13BDFBF813BA1034593,
	textFadein__ctor_mD803BE7CA3EB995768AC395C1F6ABB2F15C271C6,
	tgMachine_playVideoWeb_m854F5591CFB54AB7DB63163307980E95509E2A1D,
	tgMachine_EndReached_m77B3A1C86FF6CB6E388DB2E28F28CD91D5E99F33,
	tgMachine_Start_m43F13A903C986E4874258A2E7FB32E376DD131AA,
	tgMachine__ctor_m06D8F6E445AF3C7BB374A8A96679A1CB29F5CD8C,
	U3CplayVideoWebU3Ed__4__ctor_mFDDFCC811DD203AB80E56BCE5919B3F6499A0D0F,
	U3CplayVideoWebU3Ed__4_System_IDisposable_Dispose_mA98BBBCA44DEC675DDF92323FF15401827DFAF2D,
	U3CplayVideoWebU3Ed__4_MoveNext_m7A62325F7B8583323C13B2CBFA286FBEDBC0B9FD,
	U3CplayVideoWebU3Ed__4_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mBE034BE6A3F16D48E59ACFB54593D2CAEBCE4EC5,
	U3CplayVideoWebU3Ed__4_System_Collections_IEnumerator_Reset_mB77D255826B74B07B73795021853D04BF5562382,
	U3CplayVideoWebU3Ed__4_System_Collections_IEnumerator_get_Current_mC3EE2120157846EBF1EBD006C5DE866B5A38019E,
	touchKeyboard_OnGUI_mB6C97AFDFE7ECDCFC55A614DA1788A13097C1ED9,
	touchKeyboard__ctor_m2B1BAE54BD4EF12A33F109772F673D4DD89CE317,
	unity2engine_Start_m5175819313CE50497C155746A23470ACC7D13BBB,
	unity2engine_startSend_m321911D47EBFCE1E23A32EFAC18185CC2506D90A,
	unity2engine_startButton_m2CB7D7427131CADC8CFBC7FA292CBC1859134ECA,
	unity2engine_sendManually_m5B339B0C9D831A5761D054E79E2EB9053A93C1AE,
	unity2engine_sendTellme_m55CD37C6B59AB9C1246FB2D5C25C8E5C583D75C8,
	unity2engine_Update_m4DE0DC87A8FCBED30BFDD5BC12CDCCA38BF3999F,
	unity2engine_rispostaProxy_m4DF4A1C0E37B49871650116FBC37A9B8BDF11613,
	unity2engine_OnRequestFinished_mEBC636E25FB27E4FCB7B2C821F1A9648E43FC0E2,
	unity2engine_Send_m434835E81BE8D4F5E4E4BBB9D1A8AF2C6B4FBAA7,
	unity2engine_LoadUrlImage_mE1F0A9052142EB6EB137801B6930C6EF81C0DD95,
	unity2engine_stoppami_m5B833D6E8705B0C8537FE075EEB3EA7638B4146D,
	unity2engine_GetMyAudioClip_mBC12DA73CBC3E9893E1F2A0DD81A9359A63AE040,
	unity2engine_resetSpeec_m1CAE9638961AFB8832AAB68A63D9062A0DC05151,
	unity2engine_stopVoice_mFC27B1C4ADECEBC1504CDB18C11695E183BC4917,
	unity2engine_quit_mCDDF0869EB661CC8337DC8A882ECCC4DF2C081E9,
	unity2engine_disabilitaMic_m74084AB89F41943C2A6E7BDE61D8899DB6B8CDEE,
	unity2engine_changeServer_m14D344C93610422269CBDC2A12D549CB2E7CC8BB,
	unity2engine_sendNextCall_m302AA3682E0C8BE49DE5D9AC06011ACD228D5D8F,
	unity2engine__ctor_m9E79A3E125FFF7D49B24F36DE071F211893165B6,
	unity2engine_U3CUpdateU3Eb__47_0_m2B41AAD71A3507D1F5AAD9C0F316AD37367501BB,
	unity2engine_U3CrispostaProxyU3Eb__48_0_m0197B58F268D4B94BF5852F2B92F7B53697F6E6D,
	unity2engine_U3CrispostaProxyU3Eg__delayAnswerDialogU7C48_1_m49B440A7640DFB04DA419AD1D84F4F6C8F3EE065,
	unity2engine_U3CrispostaProxyU3Eb__48_2_m04AE9DD66D2DC7ADEC3BBAA14735C2CA5ED85632,
	unity2engine_U3CrispostaProxyU3Eb__48_3_m8720BDF3113659B89A9D05195A78444B6171DC7D,
	unity2engine_U3CGetMyAudioClipU3Eb__53_0_m300B40256A4AC105E60EB63A9353390C0A0548EE,
	U3CstartSendU3Ed__43__ctor_mABE45B7A5D4DFE8CB7CC21A67A059CC4DC81F28F,
	U3CstartSendU3Ed__43_System_IDisposable_Dispose_m1A65B71059DD4A8036AD70192B26CEC5AC2B1C3F,
	U3CstartSendU3Ed__43_MoveNext_m457A6142454ADD744C073C91ACCEB146BD4E0082,
	U3CstartSendU3Ed__43_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m41BA86D16BB674D64E8E2D6B8EE8471FB91F7CF0,
	U3CstartSendU3Ed__43_System_Collections_IEnumerator_Reset_m478B1F7CFBEA44DE9BA70326A9FEE9A0DE5175A0,
	U3CstartSendU3Ed__43_System_Collections_IEnumerator_get_Current_m82AA44A6D44A1821CDD808C30707B3DE522AEA49,
	U3CU3Ec__DisplayClass45_0__ctor_m8B28D1040E90986EF352BE4A86A67A4E9E5C1A3C,
	U3CU3Ec__DisplayClass45_0_U3CsendManuallyU3Eb__0_m14679AC3D5C767ADA07474605922D8C90632718F,
	U3CU3Ec__DisplayClass46_0__ctor_m4EFBF0AB5674AD2B4313883762DF58FAB283E8D0,
	U3CU3Ec__DisplayClass46_0_U3CsendTellmeU3Eb__0_m12A29FB81F91368978DA25A88F8334710C5735E4,
	U3CSendU3Ed__50__ctor_m5511BA409171BB1D2955B7EC06615B79F9A95641,
	U3CSendU3Ed__50_System_IDisposable_Dispose_mB2CEBC254E504CE801A1DE9BCF462A2FFBFF768F,
	U3CSendU3Ed__50_MoveNext_m2AA60459AC5E2DCF1CF85A1D57D72CC4EEF97583,
	U3CSendU3Ed__50_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m26BF695B36BCE2579D017BF81731AF3913FE2BE7,
	U3CSendU3Ed__50_System_Collections_IEnumerator_Reset_m9DB1F403F265F482CF5A80E898D67FE1C10D32E5,
	U3CSendU3Ed__50_System_Collections_IEnumerator_get_Current_mFAE7C5F81C2E395631303EDA9D43CD079EB5A32B,
	U3CLoadUrlImageU3Ed__51__ctor_mA86D936CAEE70220FCC1A4D38B8128815DF58E67,
	U3CLoadUrlImageU3Ed__51_System_IDisposable_Dispose_m386E166BE1079315E128C8E00B7E29BC4EE1544B,
	U3CLoadUrlImageU3Ed__51_MoveNext_m4A91E41809E754F6285D48178543E98EAB24CC58,
	U3CLoadUrlImageU3Ed__51_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mFA84CE1307DB3465668DEB293FCB28AE1651859E,
	U3CLoadUrlImageU3Ed__51_System_Collections_IEnumerator_Reset_m2EC283C3C6365FA41B51DB5EE873AC6DA0EE2F9C,
	U3CLoadUrlImageU3Ed__51_System_Collections_IEnumerator_get_Current_m617F5B8C0D6243FB933E392A56D92CB6F2D019EB,
	U3CGetMyAudioClipU3Ed__53__ctor_mA2CDB381213F68AC219C244C04003E5525E7B79E,
	U3CGetMyAudioClipU3Ed__53_System_IDisposable_Dispose_mAA34BD019AB2C8BCD5978E2A4DFA46F77C708BEB,
	U3CGetMyAudioClipU3Ed__53_MoveNext_m18E9475D7C5CC4FA4D3C60A3AD1A2953AD870C43,
	U3CGetMyAudioClipU3Ed__53_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mF79557ECC35B7E252751E6A790658A9DE32CC143,
	U3CGetMyAudioClipU3Ed__53_System_Collections_IEnumerator_Reset_mAA5114FD75953F9B3EE32CA740ACB812ED056EBB,
	U3CGetMyAudioClipU3Ed__53_System_Collections_IEnumerator_get_Current_mB52588418975771332C78D37CC81672F01571C3F,
	U3CresetSpeecU3Ed__54__ctor_m80FD103D8EDCD4188C1146F2B5B9883B59113040,
	U3CresetSpeecU3Ed__54_System_IDisposable_Dispose_m959473E11085E6F96D6B7C0D750CE50576526417,
	U3CresetSpeecU3Ed__54_MoveNext_m0B0D5E146545ED26E4DBB6F4412A7D6E316A63CB,
	U3CresetSpeecU3Ed__54_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m6A40EC4795918DDC43840B6F2FE1646876EB98D7,
	U3CresetSpeecU3Ed__54_System_Collections_IEnumerator_Reset_m6E55BAF67AFC3B3A7BE5109493C5DF4EE38EAE1A,
	U3CresetSpeecU3Ed__54_System_Collections_IEnumerator_get_Current_mEA9BEC7C98C1517E059C529A76CA9E01462FAE21,
	U3CdisabilitaMicU3Ed__57__ctor_m879FB5BD8ED3F548FCD93504C71B04D19D395303,
	U3CdisabilitaMicU3Ed__57_System_IDisposable_Dispose_mA12069290B5D0B7D598A670277EFBCC5047AE453,
	U3CdisabilitaMicU3Ed__57_MoveNext_m5F2549822F11220E0F60345BC0BC72553B659A13,
	U3CdisabilitaMicU3Ed__57_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m7AAD39ACF4E129473817CDEA08D87E66632AD2CA,
	U3CdisabilitaMicU3Ed__57_System_Collections_IEnumerator_Reset_m8D57CF9DC2383B7023F1D35C3DA9F85B5EC56BC8,
	U3CdisabilitaMicU3Ed__57_System_Collections_IEnumerator_get_Current_m85EA32C68D8EAD2C1AD3F525BD0D6C62B35FC8D5,
	U3CsendNextCallU3Ed__59__ctor_m5F8A96508471B1B1520CED4AD07D62A467E6C2CD,
	U3CsendNextCallU3Ed__59_System_IDisposable_Dispose_m1A30D808BE5628F58CE2C8C00621980D3A7FE361,
	U3CsendNextCallU3Ed__59_MoveNext_m35D68003DF2A346167FC126B2514F5C6FBF57C88,
	U3CsendNextCallU3Ed__59_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mFB110A759F853D0834BFF8F662E0E99027E814BE,
	U3CsendNextCallU3Ed__59_System_Collections_IEnumerator_Reset_mD7E10B923091F7B297B6D2A5445E081A286E8A12,
	U3CsendNextCallU3Ed__59_System_Collections_IEnumerator_get_Current_m1017D6432A8000B5E78AD544A327DE8AF8E40AC0,
	U3CU3CrispostaProxyU3Eg__delayAnswerDialogU7C48_1U3Ed__ctor_m7DEC9C3073DCE991F97956BFD402FAABFEC283ED,
	U3CU3CrispostaProxyU3Eg__delayAnswerDialogU7C48_1U3Ed_System_IDisposable_Dispose_m9158747AF96044DCCA0E22DB9E362C155692DF77,
	U3CU3CrispostaProxyU3Eg__delayAnswerDialogU7C48_1U3Ed_MoveNext_m2848B0B330C614DC9533BE19FD4013275F46BBEF,
	U3CU3CrispostaProxyU3Eg__delayAnswerDialogU7C48_1U3Ed_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m6B39C334166543EB98E9258AD17C3B5866C3079D,
	U3CU3CrispostaProxyU3Eg__delayAnswerDialogU7C48_1U3Ed_System_Collections_IEnumerator_Reset_mBDFBAA86A871C69C49CC7A8AAA16748DB8965441,
	U3CU3CrispostaProxyU3Eg__delayAnswerDialogU7C48_1U3Ed_System_Collections_IEnumerator_get_Current_m0D8F77607022AA470B75662B49795A061AC9640C,
	videoPlayManager_Update_mD229529B877AC71064CD4CD084950BE3779D2E49,
	videoPlayManager_playVideoTemp_mA1AEB0A158F357CEB7B403C1B52F40B20F726A2A,
	videoPlayManager_playVideoTempDebug_mA2C1D0EA019F115C7B1B251D6E3D86AA664BD6FE,
	videoPlayManager_playHalf_m259FCC0B88F9B9E2DEDE448142FC7E7AC08ADAE1,
	videoPlayManager_EndReached_mFFA64140AEB73FF22F2422B136DFE7AFE0F69255,
	videoPlayManager_EndLoopReached_mF2C1045EFF0F9676278DE837BC177558F0488A04,
	videoPlayManager_closeVideo_mEB6BB921CD5893C54BBFB02F8A0A3A3B0D624FB1,
	videoPlayManager_prepareVideo_mC2D50395D9848930F076DA5A1A265FFED084ED73,
	videoPlayManager_playClippa_m6D6DAB4D013CC0D3B02A9214B53E70A7E45D318E,
	videoPlayManager_Start_mA17355AE81D3EE8F6BE8BD81B59647DA8187BB9C,
	videoPlayManager__ctor_m1C03C9D18EDB8D36F62477CC11255634A240E01A,
	U3CplayVideoTempU3Ed__9__ctor_mA25E1BFDEE14F31D96EDCC1379AD59595406D0BB,
	U3CplayVideoTempU3Ed__9_System_IDisposable_Dispose_mB7FC958AB60F24DA21E0F6D5CBA7533167027B74,
	U3CplayVideoTempU3Ed__9_MoveNext_m87B4218212236BBE8400D83973D9EB6C54AC5196,
	U3CplayVideoTempU3Ed__9_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m566974F7912B2FB6C4CE6184A4855E89B44B2EC3,
	U3CplayVideoTempU3Ed__9_System_Collections_IEnumerator_Reset_m1848685EF5E4C0A786ACA426C217B2E6F773D8F2,
	U3CplayVideoTempU3Ed__9_System_Collections_IEnumerator_get_Current_m4B98D4B0178E6BCAE109F9C0FC8917C2BE380105,
	U3CplayVideoTempDebugU3Ed__10__ctor_m205A0380F7F90DDD5093ADC36D692E2902B590EE,
	U3CplayVideoTempDebugU3Ed__10_System_IDisposable_Dispose_m10AD654020361853C3AF65FF7AB6D4109FA23D0A,
	U3CplayVideoTempDebugU3Ed__10_MoveNext_m4E9A647F4BC8510B2860D42B61B0F9071BD17340,
	U3CplayVideoTempDebugU3Ed__10_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m2B184855AE0236F734B376CA1D14B684A9F14460,
	U3CplayVideoTempDebugU3Ed__10_System_Collections_IEnumerator_Reset_m13BAB237EEC71EA8243CB68B8D461DF5B2587A02,
	U3CplayVideoTempDebugU3Ed__10_System_Collections_IEnumerator_get_Current_m0EF65A6FF3B7555B8FD2808295E4F9731026CB74,
	U3CplayHalfU3Ed__11__ctor_m14E8757DE092AC0FA256267397586BF238953DC2,
	U3CplayHalfU3Ed__11_System_IDisposable_Dispose_mC0F3DCA5D04B2A172FAB282BD950A0FEA8B2604C,
	U3CplayHalfU3Ed__11_MoveNext_mD8ADEA9220F35125D2A959F6650F5B33F66CE837,
	U3CplayHalfU3Ed__11_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mD1E8DDB8029F3516686393A264D808A54E631900,
	U3CplayHalfU3Ed__11_System_Collections_IEnumerator_Reset_m941B7C51362BE7ED32FAD78A03A5E55A8B57E265,
	U3CplayHalfU3Ed__11_System_Collections_IEnumerator_get_Current_m222A406BEE6B957CC04213404CDE0A2F065DD915,
	U3CcloseVideoU3Ed__14__ctor_m62F71233100BFB92436AF800319981A1843A8E70,
	U3CcloseVideoU3Ed__14_System_IDisposable_Dispose_m7BA2FE06A34AA7D25B44A0530E4B0C492AA08967,
	U3CcloseVideoU3Ed__14_MoveNext_m598CF056ABECD6D327A0BA80F68B1D268CC4CBA2,
	U3CcloseVideoU3Ed__14_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m1345F35C619FDD292BDDFAD401DD9D52FDCB35FA,
	U3CcloseVideoU3Ed__14_System_Collections_IEnumerator_Reset_mE69FD69FC79D5DA8478575CFD234DADF20D20D01,
	U3CcloseVideoU3Ed__14_System_Collections_IEnumerator_get_Current_mE3B6D4FC9881998E91B65CBC12274A10640A0ECB,
	U3CprepareVideoU3Ed__15__ctor_mE3D473058FCF64ECD2F96BDED323E8B5F036AD74,
	U3CprepareVideoU3Ed__15_System_IDisposable_Dispose_m8401F0BEA8363C3CEBFC094D8AF91DF3381D20D9,
	U3CprepareVideoU3Ed__15_MoveNext_mEED81159BCA037BACDE4D6A6B20DD0DBA02FD88D,
	U3CprepareVideoU3Ed__15_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m816179CAC29DD055098B694C6EDF902E5900A72F,
	U3CprepareVideoU3Ed__15_System_Collections_IEnumerator_Reset_m5EA75C0A274DF34BED9F22608285CD2A841D6EBF,
	U3CprepareVideoU3Ed__15_System_Collections_IEnumerator_get_Current_mC5F7D452BD004CE0BA47C1CC711DF200A247864F,
	videoSequencer_Start_mB23D407476876245637EF3E6D8FD9C3D055558D9,
	videoSequencer_playVideo_mE4FA3DD412FE13A1DBE9EC1169C1460CD5C33374,
	videoSequencer__ctor_mB3F2CE16043374E87D94476758E6AC926051D1E7,
	U3CplayVideoU3Ed__5__ctor_m00B3388F912439CE2FF9DFA734F37005794806A3,
	U3CplayVideoU3Ed__5_System_IDisposable_Dispose_m65C9226619D8F46157CDB526B292D0217D17EF3C,
	U3CplayVideoU3Ed__5_MoveNext_m32197296869AF17D942C7528CE1FDBAFBFD8C222,
	U3CplayVideoU3Ed__5_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m9E4C3CD352511F808CD8537F9A86AF7F863077EF,
	U3CplayVideoU3Ed__5_System_Collections_IEnumerator_Reset_m228B5CCDF6DA29AB5C0BB04D9DC08161A8419BED,
	U3CplayVideoU3Ed__5_System_Collections_IEnumerator_get_Current_mCED4A54324DF9D93832EDF01F77605FAC2464F3B,
	aliasing_Start_m32F9CB9275F87F217C9A116E3DE800B7044A4A63,
	aliasing_Update_mC8C290FCB0D6FB3D208EF8069780534A086A84B7,
	aliasing__ctor_mEE63D43E883AD51E2E8780F1B38B684AE036E4A5,
	disableEnableAudio_Start_mF24740D7E7C304BAA1ADAED68FA462642C8F1357,
	disableEnableAudio_disableAudioRec_m517C35AB19D4C5062D8319EF4D0ADBB5EB6EE20C,
	disableEnableAudio_delay_mC5461D5A21F48828D010C6ABD533A8FCAD022BF8,
	disableEnableAudio__ctor_m1A75E575058F9E9069695D5FC2FF42A32C23A59A,
	U3CdelayU3Ed__4__ctor_mD6DB4C2A2D072FAC408B455DA1D2BD1C0D5863E8,
	U3CdelayU3Ed__4_System_IDisposable_Dispose_mDD9B74D48087F66A9C041A64746BA6C46C19F72F,
	U3CdelayU3Ed__4_MoveNext_m0AA5743CF7E88594BE59643BE0101D7905E73936,
	U3CdelayU3Ed__4_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m02A170592C68E149E49D7461108F904716CAE1F1,
	U3CdelayU3Ed__4_System_Collections_IEnumerator_Reset_mAF189F8F46DF5BADE6C2ADDBD64EE0EE7DB00CB6,
	U3CdelayU3Ed__4_System_Collections_IEnumerator_get_Current_m4781475F251F2C885549C56096A2E15E2876F648,
	SceneController_awake_mA47004C4963E3D21FC298E078522923CCE8CA56A,
	SceneController_Update_m4E482D951B939F4031265B2193BC129EAC917AD7,
	SceneController__ctor_mD43BE19257E8699CFE380AD920050CE78721E604,
	load_Start_m67E642A2D27E744C2DD1553EC699B37B3217F6BB,
	load_Update_mBA00AA2B3558B5E3759A76B4E4D5EFC302A0B7FF,
	load__ctor_mACD0C604AEFEB63F31D7749644CD519BE1007957,
	load4principle_Start_mEDF4CFCF73F250FC8F8076FE15605C3EDBE57AA5,
	load4principle_Update_m704E9C74957BB53AC4059D9469BC87C005628BB7,
	load4principle__ctor_m3A9D435000D9ED330ACEFCB3EC9B0E31BFCAB549,
	soundtravel_Start_m7B9DE1B6B90B1B199E03A33E41715CB8D49F2700,
	soundtravel_Update_mD90ACBE57D58A63F5E955FD20779B573A9A34CD1,
	soundtravel__ctor_mEB87DCEDC95D08CAA9CB3FDB2E41BF9F58175020,
	variazione_Start_m31019B4E4C9FDBF1F1B0B33E2E78974351D3001E,
	variazione_Update_m1297AE0151C0BD7F9B53EEDF160CF8216F39E815,
	variazione_WaitAndPrint_m38FFAF59698F6009BD14FE7C2145E6DB2941FB8A,
	variazione__ctor_m6A7F8043D77D1692A1F53EE2DDF88B81CB52B1AF,
	U3CStartU3Ed__4__ctor_m27EE29CF5AAC580BB19C67E82250A6788A54A94A,
	U3CStartU3Ed__4_System_IDisposable_Dispose_mF7394E30CF998B7B304A715FE81B82AC8E427F6C,
	U3CStartU3Ed__4_MoveNext_m75ED99DAFCFA53F1A9956306C21E1D3BBDF3149E,
	U3CStartU3Ed__4_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mE5845366DA537A53692735DD7D5F183C4F4B5D8F,
	U3CStartU3Ed__4_System_Collections_IEnumerator_Reset_mB9A0A536A6FC4775A24355A5FFD7FC991664C303,
	U3CStartU3Ed__4_System_Collections_IEnumerator_get_Current_mB5CA2F0BEBC84005DACE725143D53ECE31D69E43,
	U3CWaitAndPrintU3Ed__6__ctor_m4D4390329168BAB6B32E48982CA150FF4684685C,
	U3CWaitAndPrintU3Ed__6_System_IDisposable_Dispose_m3CF5BBB5785F05DA969896B0BF6A27B79EEDE871,
	U3CWaitAndPrintU3Ed__6_MoveNext_mB6A99D0EB0E9A57F191FCC2EA489B62F19FD8790,
	U3CWaitAndPrintU3Ed__6_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mF153EF72BD1C389E5DC76BFE11518647EA1C4C30,
	U3CWaitAndPrintU3Ed__6_System_Collections_IEnumerator_Reset_m5A5A9D921C78F9F9412DFD0D3BE532FD43B4F0E2,
	U3CWaitAndPrintU3Ed__6_System_Collections_IEnumerator_get_Current_m97E9AD5E843D0727F9EBD43BE5D9FBA34410E8F2,
	micController_Awake_m950344DD270F1A375AA6D7BA4DF376B80BB6EED2,
	micController_Start_m2CE7D573AF10FFF50F4F6C6662EE2615AD8FFB39,
	micController_HandleDetectionResult_mA99A0C098823BD612C634FBCBAAA87D38F2DD49C,
	micController_interrompiDisab_m80F76EAC94E0DED1DED94ED560CB93B377431C6A,
	micController_terzoNomatchAzioni_m9618D6733A2018AC74AE126D6B04AC23D8A9754E,
	micController_EndReached_m443717AEF48134F6235CC2EA1DD36EB75E8CCE6B,
	micController_nomeAccettato_m3774E0DA7707673CEFB14B1C4584F38F98ABEFF0,
	micController_numeroAccettato_mA05190B597F03CA8FF9C48A1FEE416D976158FA9,
	micController_whatsapp_m763EDEF0E10A41CA57C1EB000BA2E10CF2D88691,
	micController_stoppati_mF38A912EAFC291C7A60A48DC363A99D1E5124185,
	micController_interrompimi_m141A3F49E2A373B092EAD1C8A3ADC003695CECBF,
	micController_messaggioInvioBrochure_mED877F47A372C5690CE275746C4E52C959597AC9,
	micController_mandaConferma_m17DE2BD73491E05A90BCDBF72FFE57D263049425,
	micController_Update_mF1B1B13660055773C447F1B8F28A3845F6AF4764,
	micController__ctor_m72348C6DDA4F766D0E00AFD5F377336BC0C6EAC2,
	micController_U3CStartU3Eb__34_0_m9EBD4700C0EC351DE682F41C974B1CA5E4E8ECCD,
	micController_U3CStartU3Eb__34_1_mF5D4D8DA66B276C856F1E202AB39C117096B0933,
	micController_U3CStartU3Eb__34_2_mA3981D138FDA1E614EF78710C40BB59E7F8C1BAE,
	U3CStartU3Ed__34__ctor_m61C18E778D4C5AE15B75FE3CB2065D568347C96A,
	U3CStartU3Ed__34_System_IDisposable_Dispose_m4A033915BC64DDC54D96C772CEA1D33117F24A32,
	U3CStartU3Ed__34_MoveNext_mBE4FE80B5B57EB56468CAC97325E2D3425AE8E5F,
	U3CStartU3Ed__34_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m8D2B6B0CC17948635BBECA686EED7A5AB1FEC3BD,
	U3CStartU3Ed__34_System_Collections_IEnumerator_Reset_m60C60E66D9BADC265905FD9E1ED67FDB7D8515E5,
	U3CStartU3Ed__34_System_Collections_IEnumerator_get_Current_m9094F8D72E04247CBBD44D8ED8E65C6822239DB5,
	U3CwhatsappU3Ed__41__ctor_m74E09730CEAC086E195EE169F691DDA9322A6509,
	U3CwhatsappU3Ed__41_System_IDisposable_Dispose_mA9CC498E1D3222F749413F2DF5D0322A991889E4,
	U3CwhatsappU3Ed__41_MoveNext_m5AB9FB600D15C3FB1606B2ECEAA74E457DCED52D,
	U3CwhatsappU3Ed__41_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mB3B9C47DC7615DA62187DA4AAB81AA408305D3BD,
	U3CwhatsappU3Ed__41_System_Collections_IEnumerator_Reset_mF91F61D50FAE3E2E65CDD503C934FD619B8F776A,
	U3CwhatsappU3Ed__41_System_Collections_IEnumerator_get_Current_mAC018A9AFFC5463B889E9D52EBDA33EDBA750B4F,
	U3CstoppatiU3Ed__42__ctor_m07127D75FB4BF5EC244B49BF6737D8A825EDB0F7,
	U3CstoppatiU3Ed__42_System_IDisposable_Dispose_m0AF950F187C2DA1723400DAFEE05B7CE8B8662C7,
	U3CstoppatiU3Ed__42_MoveNext_m56C536FF453E44A853A5696609FCA9D12EF42C08,
	U3CstoppatiU3Ed__42_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m2A2D2B0FE64C2B951EBDD45465CE3F116B6C6E80,
	U3CstoppatiU3Ed__42_System_Collections_IEnumerator_Reset_m4FC7CEF78B2D035040B648FB29D70AE7009FF229,
	U3CstoppatiU3Ed__42_System_Collections_IEnumerator_get_Current_m7626B063192E312E3E18EDFD5675318E986E1489,
	U3CmessaggioInvioBrochureU3Ed__44__ctor_mC5C861AFFF08FF6A66E0B1604E6E22CA2339087B,
	U3CmessaggioInvioBrochureU3Ed__44_System_IDisposable_Dispose_m9E32FE4DC0A5965CF51554B03F040579016CEAB7,
	U3CmessaggioInvioBrochureU3Ed__44_MoveNext_mF4D497B117A5C302A9DDCAA34A9C1D3183BE8C51,
	U3CmessaggioInvioBrochureU3Ed__44_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m76E0EB7C41673B7DB03154497385715070C721D8,
	U3CmessaggioInvioBrochureU3Ed__44_System_Collections_IEnumerator_Reset_m307936C90A499E64E03A97625D8157EF4F0C1E77,
	U3CmessaggioInvioBrochureU3Ed__44_System_Collections_IEnumerator_get_Current_m2549A86672E78591D2132D73869CC13CBB022B26,
	TMP_DigitValidator_Validate_m5D303EB8CD6E9E7D526D633BA1021884051FE226,
	TMP_DigitValidator__ctor_m1E838567EF38170662F1BAF52A847CC2C258333E,
	TMP_PhoneNumberValidator_Validate_mF0E90A277E9E91BC213DD02DC60088D03C9436B1,
	TMP_PhoneNumberValidator__ctor_mB3C36CAAE3B52554C44A1D19194F0176B5A8EED3,
	TMP_TextEventHandler_get_onCharacterSelection_m90C39320C726E8E542D91F4BBD690697349F5385,
	TMP_TextEventHandler_set_onCharacterSelection_m5ED7658EB101C6740A921FA150DE18C443BDA0C4,
	TMP_TextEventHandler_get_onSpriteSelection_m0E645AE1DFE19B011A3319474D0CF0DA612C8B7B,
	TMP_TextEventHandler_set_onSpriteSelection_m4AFC6772C3357218956A5D33B3CD19F3AAF39788,
	TMP_TextEventHandler_get_onWordSelection_mA42B89A37810FB659FCFA8539339A3BB8037203A,
	TMP_TextEventHandler_set_onWordSelection_m4A839FAFFABAFECD073B82BA8826E1CD033C0076,
	TMP_TextEventHandler_get_onLineSelection_mB701B6C713AD4EFC61E1B30A564EE54ADE31F58D,
	TMP_TextEventHandler_set_onLineSelection_m0B2337598350E51D0A17B8FCB3AAA533F312F3DA,
	TMP_TextEventHandler_get_onLinkSelection_mF5C3875D661F5B1E3712566FE16D332EA37D15BB,
	TMP_TextEventHandler_set_onLinkSelection_m200566EDEB2C9299647F3EEAC588B51818D360A5,
	TMP_TextEventHandler_Awake_m43EB03A4A6776A624F79457EC49E78E7B5BA1C70,
	TMP_TextEventHandler_LateUpdate_mE1D989C40DA8E54E116E3C60217DFCAADD6FDE11,
	TMP_TextEventHandler_OnPointerEnter_m8FC88F25858B24CE68BE80C727A3F0227A8EE5AC,
	TMP_TextEventHandler_OnPointerExit_mBB2A74D55741F631A678A5D6997D40247FE75D44,
	TMP_TextEventHandler_SendOnCharacterSelection_m78983D3590F1B0C242BEAB0A11FDBDABDD1814EC,
	TMP_TextEventHandler_SendOnSpriteSelection_m10C257A74F121B95E7077F7E488FBC52380A6C53,
	TMP_TextEventHandler_SendOnWordSelection_m95A4E5A00E339E5B8BA9AA63B98DB3E81C8B8F66,
	TMP_TextEventHandler_SendOnLineSelection_mE85BA6ECE1188B666FD36839B8970C3E0130BC43,
	TMP_TextEventHandler_SendOnLinkSelection_m299E6620DFD835C8258A3F48B4EA076C304E9B77,
	TMP_TextEventHandler__ctor_m6345DC1CEA2E4209E928AD1E61C3ACA4227DD9B8,
	CharacterSelectionEvent__ctor_m06BC183AF31BA4A2055A44514BC3FF0539DD04C7,
	SpriteSelectionEvent__ctor_m0F760052E9A5AF44A7AF7AC006CB4B24809590F2,
	WordSelectionEvent__ctor_m106CDEB17C520C9D20CE7120DE6BBBDEDB48886C,
	LineSelectionEvent__ctor_m5D735FDA9B71B9147C6F791B331498F145D75018,
	LinkSelectionEvent__ctor_m7AB7977D0D0F8883C5A98DD6BB2D390BC3CAB8E0,
	Benchmark01_Start_mE7E5146B0D8D926CC410CAA48F3B617A0A7D955C,
	Benchmark01__ctor_mB92568AA7A9E13B92315B6270FCA23584A7D0F7F,
	U3CStartU3Ed__10__ctor_mBE5D8B4B98C372BD6DC3936437999DF3048DE3AB,
	U3CStartU3Ed__10_System_IDisposable_Dispose_m1F4180A7FDAE9AC5F11F30B67F813B3D7CD56D73,
	U3CStartU3Ed__10_MoveNext_mF3B1D38CD37FD5187C3192141DB380747C66AD3C,
	U3CStartU3Ed__10_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m22B2975D42326E3DE437174BBE4F0E8790CB6591,
	U3CStartU3Ed__10_System_Collections_IEnumerator_Reset_m514DA545ECA10AA62BFC239BB582FF0017B91D2B,
	U3CStartU3Ed__10_System_Collections_IEnumerator_get_Current_m6904A738F04118BA24E473F1809B3D69E711627E,
	Benchmark01_UGUI_Start_m2F8F9E1798943DA8086A7C7E73BA9D7C67482BD4,
	Benchmark01_UGUI__ctor_mACFE8D997EAB50FDBD7F671C1A25A892D9F78376,
	U3CStartU3Ed__10__ctor_m653B757B49A674E239C804FFF4EDEF325B5DB651,
	U3CStartU3Ed__10_System_IDisposable_Dispose_mAD0BC985E1E01C8369A7A955A60D3E545B0B3561,
	U3CStartU3Ed__10_MoveNext_mF9B2C8D290035BC9A79C4347772B5B7B9D404DE1,
	U3CStartU3Ed__10_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mF438D42D7C5A811FF9A283F8780F015FF55826CD,
	U3CStartU3Ed__10_System_Collections_IEnumerator_Reset_m65545EB5B6DDDDAB5ACF92600137C1C754B322BA,
	U3CStartU3Ed__10_System_Collections_IEnumerator_get_Current_mD4523BE1D1014AAF1C0709EB767C0B0F47D739D2,
	Benchmark02_Start_m315C0DF3A9AC66B4F5802FDAED056E7E5F3D2046,
	Benchmark02__ctor_m526B38D3B7E75905208E4E57B168D8AC1900F4E5,
	Benchmark03_Awake_mD23429C1EE428EEF4ED9A753385BF194BAA41A77,
	Benchmark03_Start_m19E638DED29CB7F96362F3C346DC47217C4AF516,
	Benchmark03__ctor_m89F9102259BE1694EC418E9023DC563F3999B0BE,
	Benchmark04_Start_mEABD467C12547066E33359FDC433E8B5BAD43DB9,
	Benchmark04__ctor_m5015375E0C8D19060CB5DE14CBF4AC02DDD70E7C,
	CameraController_Awake_m420393892377B9703EC97764B34E32890FC5283E,
	CameraController_Start_m32D90EE7232BE6DE08D224F824F8EB9571655610,
	CameraController_LateUpdate_m6D81DEBA4E8B443CF2AD8288F0E76E3A6B3B5373,
	CameraController_GetPlayerInput_m6695FE20CFC691585A6AC279EDB338EC9DD13FE3,
	CameraController__ctor_m09187FB27B590118043D4DC7B89B93164124C124,
	ObjectSpin_Awake_mC1EB9630B6D3BAE645D3DD79C264F71F7B18A1AA,
	ObjectSpin_Update_mD39DCBA0789DC0116037C442F0BA1EE6752E36D3,
	ObjectSpin__ctor_m1827B9648659746252026432DFED907AEC6007FC,
	ShaderPropAnimator_Awake_mE04C66A41CA53AB73733E7D2CCD21B30A360C5A8,
	ShaderPropAnimator_Start_mC5AC59C59AF2F71E0CF65F11ACD787488140EDD2,
	ShaderPropAnimator_AnimateProperties_mAF49CD157AD41377CE00AA10F0C06C8BF5AA0469,
	ShaderPropAnimator__ctor_mAA626BC8AEEB00C5AE362FE8690D3F2200CE6E64,
	U3CAnimatePropertiesU3Ed__6__ctor_mFAC0F8A7368D9D35AD2780C118E13414DA79B56A,
	U3CAnimatePropertiesU3Ed__6_System_IDisposable_Dispose_m628EECBFCBC49087298185F17BC2AE7A73FC8A7B,
	U3CAnimatePropertiesU3Ed__6_MoveNext_m8A58CCFDAE59F55AB1BB2C103800886E62C807CF,
	U3CAnimatePropertiesU3Ed__6_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m0742A0FA62C518EBD119ED5FEBF849F918E12390,
	U3CAnimatePropertiesU3Ed__6_System_Collections_IEnumerator_Reset_mC076F281DD17668D6CE42EB04EA974DE1FFB3F6B,
	U3CAnimatePropertiesU3Ed__6_System_Collections_IEnumerator_get_Current_mA585E786DA86D2589EBCA029AD9E64E8B5135362,
	SimpleScript_Start_m75CD9CEDCAFA9A991753478D7C28407C7329FB8F,
	SimpleScript_Update_mE8C3930DCC1767C2DF59B622FABD188E6FB91BE5,
	SimpleScript__ctor_mEB370A69544227EF04C48C604A28502ADAA73697,
	SkewTextExample_Awake_m6FBA7E7DC9AFDD3099F0D0B9CDE91574551105DB,
	SkewTextExample_Start_m536F82F3A5D229332694688C59F396E07F88153F,
	SkewTextExample_CopyAnimationCurve_m555177255F5828DBC7E677ED06F7EFFC052886B3,
	SkewTextExample_WarpText_m0E0C46988600673F0E5DFA3133534DC6CA5950D3,
	SkewTextExample__ctor_m945059906734DD38CF860CD32B3E07635D0E1F86,
	U3CWarpTextU3Ed__7__ctor_mEAD3C39209B75514446A44B6C2FA76F8097EBD6F,
	U3CWarpTextU3Ed__7_System_IDisposable_Dispose_m32AA7120BE15547799BDC515FA3486488952BDCF,
	U3CWarpTextU3Ed__7_MoveNext_mB832E4A3DFDDFECC460A510BBC664F218B3D7FCF,
	U3CWarpTextU3Ed__7_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m3CF506B740C16FEDE58B6E1BE4556CAC2C6AEAD1,
	U3CWarpTextU3Ed__7_System_Collections_IEnumerator_Reset_mDDCDBB794F21DF70178A75747D8D8398F38A9C2F,
	U3CWarpTextU3Ed__7_System_Collections_IEnumerator_get_Current_m6D89CF1910F76577E23C924B535E93FD1B01CF0A,
	TMP_ExampleScript_01_Awake_m381EF5B50E1D012B8CA1883DEFF604EEAE6D95F4,
	TMP_ExampleScript_01_Update_m31611F22A608784F164A24444195B33B714567FB,
	TMP_ExampleScript_01__ctor_m3641F2E0D25B6666CE77773A4A493C4C4D3D3A12,
	TMP_FrameRateCounter_Awake_m958B668086DB4A38D9C56E3F8C2DCCB6FF11FAA3,
	TMP_FrameRateCounter_Start_mC642CA714D0FB8482D2AC6192D976DA179EE3720,
	TMP_FrameRateCounter_Update_m5E41B55573D86C3D345BFE11C489B00229BCEE21,
	TMP_FrameRateCounter_Set_FrameCounter_Position_mA72ECDE464E3290E4F533491FC8113B8D4068BE1,
	TMP_FrameRateCounter__ctor_mC79D5BF3FAE2BB7D22D9955A75E3483725BA619C,
	TMP_TextEventCheck_OnEnable_mE6D5125EEE0720E6F414978A496066E7CF1592DF,
	TMP_TextEventCheck_OnDisable_m94F087C259890C48D4F5464ABA4CD1D0E5863A9A,
	TMP_TextEventCheck_OnCharacterSelection_mDA044F0808D61A99CDA374075943CEB1C92C253D,
	TMP_TextEventCheck_OnSpriteSelection_mE8FFA550F38F5447CA37205698A30A41ADE901E0,
	TMP_TextEventCheck_OnWordSelection_m6037166D18A938678A2B06F28A5DCB3E09FAC61B,
	TMP_TextEventCheck_OnLineSelection_mD2BAA6C8ABD61F0442A40C5448FA7774D782C363,
	TMP_TextEventCheck_OnLinkSelection_m184B11D5E35AC4EA7CDCE3340AB9FEE3C14BF41C,
	TMP_TextEventCheck__ctor_mA5E82EE7CE8F8836FC6CF3823CBC7356005B898B,
	TMP_TextInfoDebugTool__ctor_mF6AA30660FBD4CE708B6147833853498993CB9EE,
	TMP_TextSelector_A_Awake_mD7252A5075E30E3BF9BEF4353F7AA2A9203FC943,
	TMP_TextSelector_A_LateUpdate_mDBBC09726332EDDEAF7C30AB6C08FB33261F79FD,
	TMP_TextSelector_A_OnPointerEnter_mC19B85FB5B4E6EB47832C39F0115A513B85060D0,
	TMP_TextSelector_A_OnPointerExit_mE51C850F54B18A5C96E3BE015DFBB0F079E5AE66,
	TMP_TextSelector_A__ctor_mEB83D3952B32CEE9A871EC60E3AE9B79048302BF,
	TMP_TextSelector_B_Awake_m7E413A43C54DF9E0FE70C4E69FC682391B47205A,
	TMP_TextSelector_B_OnEnable_m0828D13E2D407B90038442228D54FB0D7B3D29FB,
	TMP_TextSelector_B_OnDisable_m2B559A85B52C8CAFC7350CC7B4F8E5BC773EF781,
	TMP_TextSelector_B_ON_TEXT_CHANGED_m1597DBE7C7EBE7CFA4DC395897A4779387B59910,
	TMP_TextSelector_B_LateUpdate_m8BA10E368C7F3483E9EC05589BBE45D7EFC75691,
	TMP_TextSelector_B_OnPointerEnter_m0A0064632E4C0E0ADCD4358AD5BC168BFB74AC4D,
	TMP_TextSelector_B_OnPointerExit_m667659102B5B17FBAF56593B7034E5EC1C48D43F,
	TMP_TextSelector_B_OnPointerClick_m8DB2D22EE2F4D3965115791C81F985D82021469F,
	TMP_TextSelector_B_OnPointerUp_m303500BE309B56F3ADCE8B461CEC50C8D5ED70BC,
	TMP_TextSelector_B_RestoreCachedVertexAttributes_m3D637CF2C6CA663922EBE56FFD672BE582E9F1F2,
	TMP_TextSelector_B__ctor_mE654F9F1570570C4BACDA79640B8DB3033D91C33,
	TMP_UiFrameRateCounter_Awake_m83DBE22B6CC551BE5E385048B92067679716179E,
	TMP_UiFrameRateCounter_Start_m59DA342A492C9EF8AD5C4512753211BF3796C944,
	TMP_UiFrameRateCounter_Update_m28FC233C475AA15A3BE399BF9345977089997749,
	TMP_UiFrameRateCounter_Set_FrameCounter_Position_m2025933902F312C0EC4B6A864196A8BA8D545647,
	TMP_UiFrameRateCounter__ctor_m5774BF4B9389770FC34991095557B24417600F84,
	TMPro_InstructionOverlay_Awake_m2444EA8749D72BCEA4DC30A328E3745AB19062EC,
	TMPro_InstructionOverlay_Set_FrameCounter_Position_m62B897E4DB2D6B1936833EC54D9C246D68D50EEB,
	TMPro_InstructionOverlay__ctor_m7AB5B851B4BFB07547E460D6B7B9D969DEB4A7CF,
	TeleType_Awake_m099FCB202F2A8299B133DC56ECB9D01A16C08DFE,
	TeleType_Start_m72DE9DE597F4FA0B1CA02115CBC1E74EB53F63F7,
	TeleType__ctor_m12A79B34F66CBFDCA8F582F0689D1731586B90AF,
	U3CStartU3Ed__4__ctor_mF3575DBEBF4F153F6A899AF3362940298C11B629,
	U3CStartU3Ed__4_System_IDisposable_Dispose_m6DABFDBC2A313BF6DD90AA1C43B4EF9D6249CBE9,
	U3CStartU3Ed__4_MoveNext_mEF7A3215376BDFB52C3DA9D26FF0459074E89715,
	U3CStartU3Ed__4_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m66DC06D52865D30DAA78DBD70B6554D13E2EA70B,
	U3CStartU3Ed__4_System_Collections_IEnumerator_Reset_mE854A2CECA0E01F3C2D90DA4F720CF0F387994F8,
	U3CStartU3Ed__4_System_Collections_IEnumerator_get_Current_m77D3936B94684C817EE9F6D2C903238A457D5261,
	TextConsoleSimulator_Awake_mA51EE182A528CCA5CAEA8DBE8AD30E43FDFAE7B0,
	TextConsoleSimulator_Start_m429701BE4C9A52AA6B257172A4D85D58E091E7DA,
	TextConsoleSimulator_OnEnable_m3397FBCDA8D9DA264D2149288BE4DCF63368AB50,
	TextConsoleSimulator_OnDisable_m43DF869E864789E215873192ECFCDC51ABA79712,
	TextConsoleSimulator_ON_TEXT_CHANGED_m51F06EE5DD9B32FEFB529743F209C6E6C41BE3B8,
	TextConsoleSimulator_RevealCharacters_mE8E415644F7BD2056D0809F7BB0FA9A2F9FE8534,
	TextConsoleSimulator_RevealWords_mFDBC863D30BC63ADCF2860F303AF252E27D0F4F4,
	TextConsoleSimulator__ctor_m4719FB9D4D89F37234757D93876DF0193E8E2848,
	U3CRevealCharactersU3Ed__7__ctor_mD45A85F5F50909F70C80AC8CE460F4FD261CDE9D,
	U3CRevealCharactersU3Ed__7_System_IDisposable_Dispose_m85F270FDC11A4D79E9CF47AADC9FA1FBF032F86C,
	U3CRevealCharactersU3Ed__7_MoveNext_mA9A6555E50889A7AA73F20749F25989165023DE3,
	U3CRevealCharactersU3Ed__7_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m5BBAE6868EB7F1C11BE5DF001641E18C9D625F83,
	U3CRevealCharactersU3Ed__7_System_Collections_IEnumerator_Reset_mA9F4893381AB24E01EAEBFC38A88AF363A9A0691,
	U3CRevealCharactersU3Ed__7_System_Collections_IEnumerator_get_Current_mFD0B7538B1A650FB389FFE9296B0E51AEA5B6B6F,
	U3CRevealWordsU3Ed__8__ctor_m22FF9E770988107A928C5D1EA639F60239BFFEF0,
	U3CRevealWordsU3Ed__8_System_IDisposable_Dispose_m3EAF6EF4A8C99A71FEED251BF3F28A26BF6AD7F8,
	U3CRevealWordsU3Ed__8_MoveNext_m3811753E2384D4CBBAD6BD712EBA4FAF00D73210,
	U3CRevealWordsU3Ed__8_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mA3F93E4AA532F15D52D68B7121805C014AB2D7FB,
	U3CRevealWordsU3Ed__8_System_Collections_IEnumerator_Reset_mCDC8982948ED5F7743567569CA1D4A354218714F,
	U3CRevealWordsU3Ed__8_System_Collections_IEnumerator_get_Current_mF39E492D576810F58DB9031556CFD6806FD32E27,
	TextMeshProFloatingText_Awake_m679E597FF18E192C1FBD263D0E5ECEA392412046,
	TextMeshProFloatingText_Start_m68E511DEEDA883FE0F0999B329451F3A8A7269B1,
	TextMeshProFloatingText_DisplayTextMeshProFloatingText_m3C381B8A53C58CF001D7A9212B8BDA6F368CC2C7,
	TextMeshProFloatingText_DisplayTextMeshFloatingText_m22691E6EA41B7FCF782B03954865D8E3B890E4DF,
	TextMeshProFloatingText__ctor_m968E2691E21A93C010CF8205BC3666ADF712457E,
	U3CDisplayTextMeshProFloatingTextU3Ed__12__ctor_mA27BBC06A409A9D9FB03E0D2C74677486B80D168,
	U3CDisplayTextMeshProFloatingTextU3Ed__12_System_IDisposable_Dispose_m510FB73335FCBCBEC6AC61A4F2A0217722BF27FC,
	U3CDisplayTextMeshProFloatingTextU3Ed__12_MoveNext_m8DFB6850F5F9B8DF05173D4CB9C76B997EC87B57,
	U3CDisplayTextMeshProFloatingTextU3Ed__12_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mCD7B5218C81C0872AB501CD54626F1284A4F73BF,
	U3CDisplayTextMeshProFloatingTextU3Ed__12_System_Collections_IEnumerator_Reset_mD2D32B94CA8D5A6D502BA38BDE1969C856368F73,
	U3CDisplayTextMeshProFloatingTextU3Ed__12_System_Collections_IEnumerator_get_Current_m59B2488B0E3C72CE97659E8D19BB13C6E0A44E90,
	U3CDisplayTextMeshFloatingTextU3Ed__13__ctor_m359EAC129649F98C759B341846A6DC07F95230D2,
	U3CDisplayTextMeshFloatingTextU3Ed__13_System_IDisposable_Dispose_m36DE8722B670A7DDD9E70107024590B20A4E0B18,
	U3CDisplayTextMeshFloatingTextU3Ed__13_MoveNext_mB7D320C272AAA835590B8460DA89DEBC0A243815,
	U3CDisplayTextMeshFloatingTextU3Ed__13_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m891591B80D28B1DF2CF9EB78C19DA672A81231A2,
	U3CDisplayTextMeshFloatingTextU3Ed__13_System_Collections_IEnumerator_Reset_m5EF21CA1E0C64E67D18D9355B5E064C93452F5B2,
	U3CDisplayTextMeshFloatingTextU3Ed__13_System_Collections_IEnumerator_get_Current_m4850A72D887C4DA3F53CA1A1A2BEAD5C5C6605A1,
	TextMeshSpawner_Awake_mDF8CCC9C6B7380D6FA027263CDC3BC00EF75AEFB,
	TextMeshSpawner_Start_m7CC21883CA786A846A44921D2E37C201C25439BA,
	TextMeshSpawner__ctor_m1255777673BE591F62B4FC55EB999DBD7A7CB5A1,
	VertexColorCycler_Awake_m84B4548078500DA811F3ADFF66186373BE8EDABC,
	VertexColorCycler_Start_mC3FF90808EDD6A02F08375E909254778D5268B66,
	VertexColorCycler_AnimateVertexColors_m6960F777E4876DFCA726BCAE7A8163850D58FA42,
	VertexColorCycler__ctor_m09990A8066C8A957A96CDBEBDA980399283B45E9,
	U3CAnimateVertexColorsU3Ed__3__ctor_m0038B1054BCC928D35F8C0021ED7D2E1C533E35F,
	U3CAnimateVertexColorsU3Ed__3_System_IDisposable_Dispose_m5BAD394A0B09B3E0FF19E91521E02C2B3ADD6007,
	U3CAnimateVertexColorsU3Ed__3_MoveNext_m2842AF5B12AFC17112D1AE75E46AB1B12776D2A6,
	U3CAnimateVertexColorsU3Ed__3_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mAE810D7968957A09C88E61C29DAAEC68E4AF1E51,
	U3CAnimateVertexColorsU3Ed__3_System_Collections_IEnumerator_Reset_m72ABAC50E9E4D972FB44CAFF387F3E23FEC5D932,
	U3CAnimateVertexColorsU3Ed__3_System_Collections_IEnumerator_get_Current_m8DE595A1D01F3A507A356F8BCE020D0851412B52,
	VertexJitter_Awake_m9C5586A35BD9C928455D6479C93C1CE095447D8F,
	VertexJitter_OnEnable_m97ED60DBD350C72D1436ADFB8009A6F33A78C825,
	VertexJitter_OnDisable_mF8D32D6E02E41A73C3E958FB6EE5D1D659D2A846,
	VertexJitter_Start_m8A0FED7ED16F90DBF287E09BFCBD7B26E07DBF97,
	VertexJitter_ON_TEXT_CHANGED_mC7AB0113F6823D4FF415A096314B55D9C1BE9549,
	VertexJitter_AnimateVertexColors_mECAE037FC0CBA52CAC71C0B61E88829FF18BCC16,
	VertexJitter__ctor_m550C9169D6FCD6F60D6AABCB8B4955DF58A12DCE,
	U3CAnimateVertexColorsU3Ed__11__ctor_m0222C3457F5ACA497FE3A8EC829DE4AD11A169F8,
	U3CAnimateVertexColorsU3Ed__11_System_IDisposable_Dispose_m717E79A39A8161ADDA9E62F7CDFB67B8F2D65099,
	U3CAnimateVertexColorsU3Ed__11_MoveNext_m169A75C7E2147372CB933520B670AF77907C1C6B,
	U3CAnimateVertexColorsU3Ed__11_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m920EBA43D59A1A88E27FED92CF0AC0DF90179479,
	U3CAnimateVertexColorsU3Ed__11_System_Collections_IEnumerator_Reset_m6C045A6DD0B60F1512457448E615877EAB86D75D,
	U3CAnimateVertexColorsU3Ed__11_System_Collections_IEnumerator_get_Current_m017345FE58B497EAFC9D0CB1FB733F76EB3449AF,
	VertexShakeA_Awake_m005C6F9EE8A8FD816CD3A736D6AF199CD2B615A2,
	VertexShakeA_OnEnable_m9F60F3951A7D0DF4201A0409F0ADC05243D324EA,
	VertexShakeA_OnDisable_m59B86895C03B278B2E634A7C68CC942344A8D2D2,
	VertexShakeA_Start_m3E623C28F873F54F4AC7579433C7C50B2A3319DE,
	VertexShakeA_ON_TEXT_CHANGED_m7AD31F3239351E0916E4D02148F46A80DC148D7E,
	VertexShakeA_AnimateVertexColors_mC451E2732A4E3E90E2553811AD75903EEF974599,
	VertexShakeA__ctor_m7E6FD1700BD08616532AF22B3B489A18B88DFB62,
	U3CAnimateVertexColorsU3Ed__11__ctor_m92612416BEC0EBF5E9849FB603629C0F2F95FEF2,
	U3CAnimateVertexColorsU3Ed__11_System_IDisposable_Dispose_m77D966994D4717EAFD8EFE169F3E8A4EE8B05B81,
	U3CAnimateVertexColorsU3Ed__11_MoveNext_mCEEDE03667D329386BB4AE7B8252B7A9B54F443F,
	U3CAnimateVertexColorsU3Ed__11_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mDCCD3645ACF9B18D760B341C863F853996FA9BCE,
	U3CAnimateVertexColorsU3Ed__11_System_Collections_IEnumerator_Reset_mB9444B5E58B4E97105C447F547C0F74C51BCFBFA,
	U3CAnimateVertexColorsU3Ed__11_System_Collections_IEnumerator_get_Current_m5CC464185D5C0251C6206E20AFFA681BA0525A7E,
	VertexShakeB_Awake_m99C9A0474DBFE462DC88C898F958C1805376F9D5,
	VertexShakeB_OnEnable_m64299258797D745CDFE7F3CBEC4708BDBC7C3971,
	VertexShakeB_OnDisable_m07D520A8D7BCD8D188CE9F5CC7845F47D5AD6EF4,
	VertexShakeB_Start_m9642281210FA5F701A324B78850331E4638B2DD1,
	VertexShakeB_ON_TEXT_CHANGED_m5650D69D258D0EEF35AF390D6D5086B327B308A5,
	VertexShakeB_AnimateVertexColors_mDF7A7E7028361D288DF588D9301541E4FA1EFA87,
	VertexShakeB__ctor_m605A778C36B506B5763A1AE17B01F8DCCBCD51EC,
	U3CAnimateVertexColorsU3Ed__10__ctor_m3E3B1D286DEBED2BC028AD490308568B930C3760,
	U3CAnimateVertexColorsU3Ed__10_System_IDisposable_Dispose_m0F9D4B2A6ED0500C2120DBA29932CC279E8908DC,
	U3CAnimateVertexColorsU3Ed__10_MoveNext_m0534E436514E663BF024364E524EE5716FF15C8E,
	U3CAnimateVertexColorsU3Ed__10_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mF2A8BAFC0261ACBBE8EEA74DE4B498D30C68AE3D,
	U3CAnimateVertexColorsU3Ed__10_System_Collections_IEnumerator_Reset_m5F12D7C6F2523BEB6326FE49AE972116D6157CBB,
	U3CAnimateVertexColorsU3Ed__10_System_Collections_IEnumerator_get_Current_mB9C12786011A3B78517AEFAE8D5A78B95A4219AD,
	VertexZoom_Awake_m1B5D386B98CF2EB05A8155B238D6F6E8275D181C,
	VertexZoom_OnEnable_m7F980FC038FC2534C428A5FD33E3E13AEAEB4EEC,
	VertexZoom_OnDisable_m880C38B62B4BB05AF49F12F75B83FFA2F0519884,
	VertexZoom_Start_m10A182DCEF8D430DADAFBFFA3F04171F8E60C84C,
	VertexZoom_ON_TEXT_CHANGED_mB696E443C61D2212AC93A7615AA58106DD25250F,
	VertexZoom_AnimateVertexColors_mEFBA6C940DFECB485236C247358A32011A7963ED,
	VertexZoom__ctor_mE7B36F2D3EC8EF397AB0AD7A19E988C06927A3B2,
	U3CU3Ec__DisplayClass10_0__ctor_m4EC5F8042B5AC645EA7D0913E50FD22144DBD348,
	U3CU3Ec__DisplayClass10_0_U3CAnimateVertexColorsU3Eb__0_m959A7E1D6162B5AAF28B953AFB04D7943BCAB107,
	U3CAnimateVertexColorsU3Ed__10__ctor_m537CF0A5ADF5BBC2DF784BF526E3F32EB528E1B2,
	U3CAnimateVertexColorsU3Ed__10_System_IDisposable_Dispose_mF0CD944C393771101D2718A660E1DFF22385819F,
	U3CAnimateVertexColorsU3Ed__10_MoveNext_m8340EFEB2B2CF6EA1545CFB6967968CA171FEE66,
	U3CAnimateVertexColorsU3Ed__10_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m093E922A5A046910B2A7EE826D804CA3064A7BD9,
	U3CAnimateVertexColorsU3Ed__10_System_Collections_IEnumerator_Reset_mDED63FD52741D58D8D5A3E53415F91B6560F680C,
	U3CAnimateVertexColorsU3Ed__10_System_Collections_IEnumerator_get_Current_m933EBF7C4300C7C1E3EE60E4741110025877CA5F,
	WarpTextExample_Awake_mEDB072A485F3F37270FC736E1A201624D696B4B0,
	WarpTextExample_Start_m36448AEA494658D7C129C3B71BF3A64CC4DFEDC4,
	WarpTextExample_CopyAnimationCurve_m744026E638662DA48AC81ED21E0589E3E4E05FAB,
	WarpTextExample_WarpText_mA29C98CF3B92F253C2DD2BADFC76618F8324BEF2,
	WarpTextExample__ctor_m62299DFDB704027267321007E16DB87D93D0F099,
	U3CWarpTextU3Ed__8__ctor_m9FADE04C27A0034C5A276232FCA187AECDC6BF49,
	U3CWarpTextU3Ed__8_System_IDisposable_Dispose_m5AB0DCF4B3DE6C290487F3DBBC7BAE931130DE60,
	U3CWarpTextU3Ed__8_MoveNext_m9C83C8455FF8ADFBAA8D05958C3048612A96EB84,
	U3CWarpTextU3Ed__8_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m9D5776AF80C4227401ADD7E13D98F2530CB9E7A1,
	U3CWarpTextU3Ed__8_System_Collections_IEnumerator_Reset_m4F2FBBE7B375E6FC837736B4275A7B601A01F535,
	U3CWarpTextU3Ed__8_System_Collections_IEnumerator_get_Current_m01FA935D93D737C9DDD88A051F7DFC393DD7BD25,
	AudioCapture_get_status_m56E2B766A9CA188CD8EEEF520EC058584D874E3E,
	AudioCapture_set_status_m1E39D8508A0944F2C764CFDF6DA72F40968C3146,
	AudioCapture_get_filePath_m6C2DE6B6A65A63FAA8847535E5569932F33DACBB,
	AudioCapture_set_filePath_m3C77C213075EF3E5DB4BD5A308A20F39131349D1,
	AudioCapture_Cleanup_mF14AD3F474ADC5C4D1339819EC3BA0D0BCE42A0B,
	AudioCapture_StartCapture_m5188B27DFE105A71AF588263A321D4CCB0E30E61,
	AudioCapture_StopCapture_mB341C1E4C3421ACEF0631D1B557B45AFDEB2A00A,
	AudioCapture_PauseCapture_mA4EE944AB2C907AD36A99DC79817A612E6983834,
	AudioCapture_Awake_m3AE496DBED033831A4A4A9903DE29A932C2A9A2D,
	AudioCapture_OnAudioFilterRead_m76DD79CB5D3AAAD7E176709E7DED0A7F5EE3E70F,
	AudioCapture_AudioCaptureLib_Get_mC6416E539ADEA8AB5BC892E63693E6CA041BB6F3,
	AudioCapture_AudioCaptureLib_WriteFrame_m85D72E9D49593C492A636C8F0EF13869295D8AC4,
	AudioCapture_AudioCaptureLib_Close_mAF6668A52955C0CA54417614820ED61D295774BB,
	AudioCapture_AudioCaptureLib_Clean_mE2FE8BF6BB2904D7288F8193ED1C4EF26C02FE02,
	AudioCapture__ctor_mBE1317E3F3A8AEA7D03034F1636032DCBC05500F,
	VideoCaptureBase_get_frameWidth_m30887D181D279743F9B485B95E85A2B4E4752BF4,
	VideoCaptureBase_get_frameHeight_m10D46EDB9C07F1A5B9DB992C10C0CCB91550E0E9,
	VideoCaptureBase_get_cubemapSize_m2C45427F1F8C5A27632BFA644D4C62DADBC2BA32,
	VideoCaptureBase_get_antiAliasing_m83B4F7A5CA63AD77B1363B1726A7C9B94E5C0701,
	VideoCaptureBase_get_bitrate_mF6142599C5560AF81A078B6D621D8FB5C5258600,
	VideoCaptureBase_get_targetFramerate_m752C9DA32FD5639809165B961DDB111A78C42CD1,
	VideoCaptureBase_get_filePath_m47CAA68BE4F7B187DDDB38BDE19486F7817420FA,
	VideoCaptureBase_set_filePath_m1B925667D6E4D151D80A876FBE350DB8B2AD6E23,
	VideoCaptureBase_Awake_m26142FBDEF8B003D2C38743F5A8AFD350C2F451D,
	VideoCaptureBase_StartCapture_m3013969305F830E5642868367E385E7717B23E41,
	VideoCaptureBase_StopCapture_m00700F62005DE97952D4C6BBDF1B892D68E8D480,
	VideoCaptureBase_ToggleCapture_mC1E80E2AF344349F4AE1CA86A76F98E9DCB92790,
	VideoCaptureBase_get_isPanorama_m67127DF463C86EE71AF549BE7768DBE87EF45A0F,
	VideoCaptureBase_get_isLiveStreaming_m66A77B20D419128C73442F613C560AF83B0BB8F9,
	VideoCaptureBase_RenderTextureToPNG_m56673E7AAB44BCA00C726529BA64218DB72EC493,
	VideoCaptureBase_TextureToPNG_mE45B601DF15672A295267B3147C201CA338B5693,
	VideoCaptureBase_SetStereoVideoFormat_mFD413EEBA2351A1EC822E8C5D731D9DC05A26C6F,
	VideoCaptureBase_RenderToCubemapWithGUI_m736B83E8E174A7F7CFA0A2FB44A4F9582396FB59,
	VideoCaptureBase_RenderCameraToRenderTexture_m6D3A6B9553445C19CEEAFD6AA3FD87E8392F8871,
	VideoCaptureBase__ctor_mCE9E51D0F97FC2934841D9C918AFFF791D5DF2B4,
	VideoCaptureCtrlBase_get_status_m190249C80EBE548327CCBFCC9912C84E798E2F4A,
	VideoCaptureCtrlBase_set_status_m8F4B13A5B5A945FDE87A2D04FF6EFD0B8C6D4D88,
	VideoCaptureCtrlBase_get_videoCaptures_m425D14FFED1EC140FB1898456BE5FA4EF1AC2762,
	VideoCaptureCtrlBase_set_videoCaptures_mCECF5016B63BC308926B1B659F743E7D890982DE,
	VideoCaptureCtrlBase_StartCapture_mAB78E3CED54F6F05A3F90FFA59735C3139139A90,
	VideoCaptureCtrlBase_StopCapture_m52AC4978D064A7EC4119B68CCBCE5A413F220608,
	VideoCaptureCtrlBase_ToggleCapture_m11A8ADBDAF5FCCB738363A801008A5A41658EF9C,
	VideoCaptureCtrlBase_Start_m79329B3C3B3D91EB18B7E16CA95B65A53BADEB0F,
	VideoCaptureCtrlBase_Update_mCF94B098F3F5966FE3D1BAF4BF8983AED0D9E045,
	VideoCaptureCtrlBase__ctor_m266CCCCFB5986CE05F5CA1D2CECF96A588C67441,
	PathConfig_get_SaveFolder_m0CF2F311D40D2E5FE2E62722DAB322D1569D4FED,
	PathConfig_set_SaveFolder_m58DE48455B2BB95CBD45E811B6222FD9E89DBEF9,
	PathConfig_get_ffmpegPath_mA4D263DF1E51B9D85F5C79D5FD06334DE1D7668E,
	PathConfig_get_injectorPath_m1F02FB0B75BFA7DD82A2024CB38450C1BD3107AF,
	PathConfig__ctor_m9978E41B8C7D808D5609185295AFE1560B4CB54C,
	PathConfig__cctor_m5FE032CEC5C7BADEA5A6918E6687E0119D2F61BB,
	Screenshot_get_Camera_m8AA0892EBC1793EF667760BDC715C677224BF76D,
	Screenshot_Start_m27E1947C3576649271CF2527C84D964B7FA6C98F,
	Screenshot_AutoTakeScreenshot_m20BE31EE8B6C8C332BC70B394395A2B196149930,
	Screenshot_TakeScreenshot_mA3C2AF43612E83C3B4D6FE9A3A0B79C7BECD8316,
	Screenshot__ctor_mE1CA0DE1BF8D9913273E4B9CC0A8F23CD731AB82,
	U3CAutoTakeScreenshotU3Ed__8__ctor_m071D1E2C42EDE895DC697003F8C091490440B15C,
	U3CAutoTakeScreenshotU3Ed__8_System_IDisposable_Dispose_mC3F85BEB0DC8C76723BCBEFB55DCA5D5E3EACEF6,
	U3CAutoTakeScreenshotU3Ed__8_MoveNext_mA180AF291ED09DA0DF95FFB9BC63CCB9A1B7B428,
	U3CAutoTakeScreenshotU3Ed__8_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mBF203EDCD3A1CC0A2EE0A54492365E267841918C,
	U3CAutoTakeScreenshotU3Ed__8_System_Collections_IEnumerator_Reset_mBC7EFA60581C7596B406B799B67D488D2B917F92,
	U3CAutoTakeScreenshotU3Ed__8_System_Collections_IEnumerator_get_Current_mA5BA749D7AD517F4B917E12CB106FB3757E9A266,
	StringUtils_GetTimeString_m8323FDF230F189CEAD9194244B40DE4641AA52BC,
	StringUtils_GetRandomString_m809A91C40CA40734027F2C44F2DFE2F82EBF98B5,
	StringUtils_GetMp4FileName_m53C1E9EAE2871481860D5F9CD1D5DC8D0271DA0A,
	StringUtils_GetH264FileName_m75D0F7B3F82A35E5026CA241D2885C68B32DE221,
	StringUtils_GetWavFileName_m04F1694AFB96F0E9A9FD4C3FDD38FFF7ED42C4A6,
	StringUtils_GetPngFileName_m09B8E114051FD1F22BC37D6CF34537D558ECA2ED,
	StringUtils_GetJpgFileName_m1ECB4D352EAB7AFEA6BD1EB4ADF452E1280D81C7,
	StringUtils_IsRtmpAddress_mFF0E958604290D307A63D39274D0C0E09ECDA210,
	StringUtils__ctor_mA5F77CB798471C593A60F0C1D23A04F69FB0C207,
	U3CU3Ec__DisplayClass1_0__ctor_m1A5723050A445DAAB02C6F44A677EB843AF07AC1,
	U3CU3Ec__DisplayClass1_0_U3CGetRandomStringU3Eb__0_mEF0E5699C07EC858EEE2EBADF93720F70D5F3075,
	MathUtils_CheckPowerOfTwo_m69CA923FD548C300A6347B16335BC79BDB2DBAE6,
	MathUtils__ctor_mDA4F39AAABFCBCC9E717EBFB91476DB42BCC8890,
	VideoCapture_get_status_m418342C3D1B1C60BC7E9E46783F045B5E5826BBB,
	VideoCapture_set_status_m26E046134DC8C0161A319480FABAB776E9FDDCAF,
	VideoCapture_Cleanup_mAA4078713D9DC15EBDE3B69E05DF59AD1F0DBB7B,
	VideoCapture_StartCapture_mFA8B0914AE97F23EF05EF56C3427E5F894157D2B,
	VideoCapture_StopCapture_m7D077BB0470389C711A58AD88210196174EB26AD,
	VideoCapture_ToggleCapture_mADD2E3677B84C769B464C5F818EF6CCC3AEAC823,
	VideoCapture_Awake_m573170C5B7D125834549905CA9B89FC9D2F77493,
	VideoCapture_OnRenderImage_m1683B212DC56A4B929B01DA16CBDE0744308A926,
	VideoCapture_LateUpdate_m690B612A2F3EFD01ACB99C369DE37733612BC08D,
	VideoCapture_OnDestroy_m0949F83B32C88FE70F7B8F92DC3B06FA34A5F00B,
	VideoCapture_OnApplicationQuit_mEBEADA3C27D07F2E33C89D1A4ABC4342C726DC8C,
	VideoCapture_CaptureFrameAsync_m5B486B78D00834A6FE1F63AD950F212171A3F1C5,
	VideoCapture_CaptureCubemapFrameSync_m43D14136A6CA0BD5A144594B4BE27B5C688FB047,
	VideoCapture_CopyFrameTexture_m5DD2F546239F24A875A7419C4C8F445115A66D85,
	VideoCapture_EnqueueFrameTexture_m0A708D1AEACAF83A5DD7FC24E89C39E5B24EF9B0,
	VideoCapture_FrameEncodeThreadFunction_m45C399B289D66F66D6231E03197C68862577CA6B,
	VideoCapture_VideoCaptureLib_Get_m7C957A27D8F125D85970AA8A9A03EB261443FD9F,
	VideoCapture_VideoCaptureLib_WriteFrames_mB47A170A22C613403FE0C3088A21EC903C3E9576,
	VideoCapture_VideoCaptureLib_Close_m1DB9B37045C3AE8D0D94F4DB3B0C93929D7FD93B,
	VideoCapture_VideoCaptureLib_Clean_m6B4F7316E874E3AD4C06C0A0222EED9624F04F3A,
	VideoCapture_VideoStreamingLib_Get_mF8CA45471D2BED0520BBC731F2DE7C744B896149,
	VideoCapture_VideoStreamingLib_WriteFrames_m50F8E681EE1FB01E1A202A4C8B65E0178172F02E,
	VideoCapture_VideoStreamingLib_Close_m7EC2D8677E013017E5308266C5CDDE308A460A08,
	VideoCapture_VideoStreamingLib_Clean_m446C144B2BF2CAACD9069B885B40DF330FF0972E,
	VideoCapture__ctor_mA2EBE79FC8C3F61757616A24BE04917249932172,
	FrameData__ctor_m998B6E32258EF155CA31AFB8D53307247807765F,
	U3CCaptureFrameAsyncU3Ed__26__ctor_m8DB8A61D8D83A2B2BFFF4F5E4AEB7E5CA9865AA9,
	U3CCaptureFrameAsyncU3Ed__26_System_IDisposable_Dispose_m16FB3151DF9C1A0FFFC4B5B4E520EFDA57CD8C21,
	U3CCaptureFrameAsyncU3Ed__26_MoveNext_m7976231B758894E9A8204F2DCD2962B8A7ED7FAB,
	U3CCaptureFrameAsyncU3Ed__26_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mA5975049F28F465987C255BEAB8EBB096909FA2A,
	U3CCaptureFrameAsyncU3Ed__26_System_Collections_IEnumerator_Reset_m06A761BB7C018F2FD9999B313FB3C97CE008BFD8,
	U3CCaptureFrameAsyncU3Ed__26_System_Collections_IEnumerator_get_Current_mA5CCE329EB1C82067D0B5B4B71633F9928B59CB6,
	VideoMuxing__ctor_mC6C234BCFADE86E96280CBC9128E41D8A6EDC591,
	VideoMuxing_Muxing_m76DF75EA615A331EF71647A056319EC0A1C6233A,
	VideoMuxing_MuxingLib_Get_mDEF72C2AD83F3282B529B7DD2DF92C621D09E698,
	VideoMuxing_MuxingLib_Muxing_m14184A0F6A3D39460F25F294CCBA7E4621AEDE1F,
	VideoMuxing_MuxingLib_Clean_m9A58C8796563F662B722FC07816F861DFC70D8CD,
	VideoCaptureCtrl_get_audioCapture_m4A232276C577E7264F2EAFEA2F95753FA9DC98E7,
	VideoCaptureCtrl_set_audioCapture_m46849A247E5EEBC041724FA2D6F6303C78444ADA,
	VideoCaptureCtrl_StartCapture_m0A9F0D1E820CA25530CAE0049D559A914B0A2A45,
	VideoCaptureCtrl_StopCapture_m81A2FEB4B393092B0AB397893E45CF32282616ED,
	VideoCaptureCtrl_ToggleCapture_mDFD84FF882F3E76F7921D2C355E5FA63647F4EFE,
	VideoCaptureCtrl_OnVideoCaptureComplete_mDDFA7A70FBA21CDFB92D86CFFB017A41A1D4FDCB,
	VideoCaptureCtrl_OnAudioCaptureComplete_m4E4CD80551FD5289B5C076E897719BCC4384EBBE,
	VideoCaptureCtrl_VideoMergeThreadFunction_mF4424620F3EE51C9ED0D59391844A2816D9D354F,
	VideoCaptureCtrl_GarbageCollectionThreadFunction_mEA0F32FD0D20B0E9BF68B4495AD46F5D6E7AEEFC,
	VideoCaptureCtrl_Cleanup_m0849D4B6C027CDBC7ECCC719D66F5428F76C4135,
	VideoCaptureCtrl_IsCaptureAudio_m6BE853F2364BA9E96FC8071A59A93A6F1B110548,
	VideoCaptureCtrl_Awake_mCBC3C6675D44D08C27C71693506C00EAD0C722A9,
	VideoCaptureCtrl_OnApplicationQuit_m5B2DC8C4BA56FAC028DFAC997780F2F421AEB1B1,
	VideoCaptureCtrl__ctor_m84BBF5B8A47B2DD9BB1BEAC87FF551E0AC7B17C4,
	VideoCapturePro_get_audioCapture_m70A99B46ACE208E98BE2F502278A473D49A556C9,
	VideoCapturePro_set_audioCapture_mE1236BB3108F81FEF011245319FD14136F9DF6D9,
	VideoCapturePro_StartCapture_mA8972C8249C32D126C12D9DE0E8301F4C404E8A5,
	VideoCapturePro_StopCapture_mB08D850A7B29215F943724D2E06779B2E1F045A3,
	VideoCapturePro_ToggleCapture_m2E45E17E9E4D3C58E52AD39A0720AB77DAB5ACF3,
	VideoCapturePro_Awake_mBF4C62C65E393B906EF119FE7F00088D1C234F7E,
	VideoCapturePro_Update_mDAA5DBA319EF1FFCA9C8785F4F35408E502FB128,
	VideoCapturePro_OnRenderImage_m9413EA436097B53B7852BBB703A3C6098DAED94A,
	VideoCapturePro_OnDestroy_m45E97C6F035AFF20DB8E7ADF68FDD5C4D5F3AF20,
	VideoCapturePro_OnApplicationQuit_m9A2D6D4B2BB05E39580B5130A70895F53DAF9E04,
	VideoCapturePro_LiveThreadFunction_m3CBC70316522EDA7EF26989EEE66535EE38BF2EC,
	VideoCapturePro_MuxingThreadFunction_m07CD6F4713982381EA84DEEFF5A98C5E4CC56B23,
	VideoCapturePro_AudioThreadFunction_m1B3C74EC8DC859262E1A39A5C25A5F642102EE80,
	VideoCapturePro_SetOutputSize_m35E201238671B0FE561EE7978504A7B40C531426,
	VideoCapturePro_Screenshot_m58EB28374775084138D420520DCC1EE187170DB3,
	VideoCapturePro_CaptureScreenshot_m5D753812398655B257B81EB830E8D7529733301E,
	VideoCapturePro_Cleanup_mA14DE6156B54032B923C39148BCAD5E34B0FB9BA,
	VideoCapturePro_RenderCubeFace_m3EEA8DDC4FCD4830D6365A4360C5E8258E208B9D,
	VideoCapturePro_SetMaterialParameters_m34DCBCE12E5AB63D2AEA5264490CBFA9E6F1D1C6,
	VideoCapturePro_DisplayCubeMap_m9E99754F5CF85E1322306FF1A0B6DDF16E6790C6,
	VideoCapturePro_DisplayEquirect_m08495B1C95BCF7F6051FB582B01BA00B0C393BA5,
	VideoCapturePro_GPUCaptureLib_StartEncoding_m1F74D2EB8A75B949CA5AFE40B577249EDC70C30B,
	VideoCapturePro_GPUCaptureLib_AudioEncoding_mF915DDF8D770A23C05BECACBD80761A0B6108274,
	VideoCapturePro_GPUCaptureLib_StopEncoding_m8AA2D90DA016ACEDC44945B0A81385D6BF1CA070,
	VideoCapturePro_GPUCaptureLib_MuxingData_m72642DE5FEE516E4085D2A9EE3736627BB875D82,
	VideoCapturePro_GPUCaptureLib_MuxingClean_m0F722160AA1567E4C3195CB85FA726B2CB87AC07,
	VideoCapturePro_GPUCaptureLib_StartLiveStream_m2678180E0CC80C7A0BE5863572C126867244DF8C,
	VideoCapturePro_GPUCaptureLib_StopLiveStream_mFCF82720A11FB6F92AB5C78BB9B1097DAE152689,
	VideoCapturePro_GPUCaptureLib_SaveScreenShot_m290B5EBD2F645CD34E94CE375BF429D05D807E13,
	VideoCapturePro__ctor_m422DD25D5F24C7F16D0E8CD6D64D251C89E635CD,
	U3CCaptureScreenshotU3Ed__44__ctor_m5440C36152A5DFFC0AD853BB1035B90027D895BD,
	U3CCaptureScreenshotU3Ed__44_System_IDisposable_Dispose_m358F68199E88F6738A424253CB009B5375E784E9,
	U3CCaptureScreenshotU3Ed__44_MoveNext_m07826777E6F5C4060A590BA884832AF0FFC563B3,
	U3CCaptureScreenshotU3Ed__44_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m8387B032366EAA7E7B95E28FC8B0522201E747CC,
	U3CCaptureScreenshotU3Ed__44_System_Collections_IEnumerator_Reset_mD8684A9E7E36362EC57DA9D67DC4108DB49B44A4,
	U3CCaptureScreenshotU3Ed__44_System_Collections_IEnumerator_get_Current_m0833A58D437ECE623F0F58A11D42A2180F16C44F,
	VideoCaptureProCtrl_Awake_mE8A2AE48EC6A91D7A5A8C8B55586C0F451534977,
	VideoCaptureProCtrl_StartCapture_mFCD460982A34F6F3E89EA4CE9CCD770D0F594703,
	VideoCaptureProCtrl_StopCapture_m601F492C7676D4819F234A29F766167BB6DD32C7,
	VideoCaptureProCtrl_ToggleCapture_mB0A42003B41193A40FA4D1D4E0F7D7C981925AB4,
	VideoCaptureProCtrl_CheckCapturingFinish_m998A3B5259F0FC34E5F099D82B2345BD299327E4,
	VideoCaptureProCtrl_SendUploadRequest_m2DC30E61C69030A926BCBA105BB42168AC0FBBC6,
	VideoCaptureProCtrl__ctor_mB195B1D23B924AF20A1B0F50525CA0DE130F0A7F,
	U3CCheckCapturingFinishU3Ed__4__ctor_mC2626C87443EE0FDD5331037FF0E0F2B2F3445D6,
	U3CCheckCapturingFinishU3Ed__4_System_IDisposable_Dispose_m8A648104C06258C26D1C34B22EF1346077DBA08F,
	U3CCheckCapturingFinishU3Ed__4_MoveNext_mD3E37D1AFE0A0DD95B5BDE2315DDB257D829A872,
	U3CCheckCapturingFinishU3Ed__4_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mA5A291D14C23284FEB8ABAE89D6FC5E9CF6C4B5F,
	U3CCheckCapturingFinishU3Ed__4_System_Collections_IEnumerator_Reset_mF613813F0DAA0E1178BF7A8BCC7675A5A35F05AB,
	U3CCheckCapturingFinishU3Ed__4_System_Collections_IEnumerator_get_Current_m3C4EEFC55C1050F47D7AD9AB56E5754DD5C971C8,
	U3CSendUploadRequestU3Ed__5__ctor_m86D838FC6D0B76A2698AF726CAA88A2364144C48,
	U3CSendUploadRequestU3Ed__5_System_IDisposable_Dispose_m75EAA0B82B0D3724100C0F0EDA07A8D060F116A0,
	U3CSendUploadRequestU3Ed__5_MoveNext_m9257F53A88E2355A27E8F6A5F87FC01E779BDD7C,
	U3CSendUploadRequestU3Ed__5_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m716AD8D50AD356974FD83DC3D0C1BCDB2966B275,
	U3CSendUploadRequestU3Ed__5_System_Collections_IEnumerator_Reset_m3EBCCC7DCCC6E2A46F6BD2A7070CCBCBFEA4EBCB,
	U3CSendUploadRequestU3Ed__5_System_Collections_IEnumerator_get_Current_mC068709B92FDBD47434B495D4B13A050AEB156F0,
	VideoPlayer_Awake_m37E1582665642F406AB2AEBDE0E392EA1BE6AE7F,
	VideoPlayer_SetRootFolder_mDC38604AFAB4B5C74B73D70CCF676E0E04394ADB,
	VideoPlayer_PlayVideo_m8C30CA0222E263520E0FA290C5CD219E2C19CF0E,
	VideoPlayer_NextVideo_mECC9B185712F59750FDC16AD1F0290126C20D2A7,
	VideoPlayer__ctor_m2381501972737BEF5FF684325047C3228526408C,
	AutoRotate_Start_m7A38F11117A8B47DBFC9C385120D90414B2C35BE,
	AutoRotate_Update_m32C70E5891369AC4A54E1D21286043986535B642,
	AutoRotate__ctor_mE2D6A62ADE0168480EDB9388683F7E2B3DBEF24E,
	VideoCaptureNormaUI_Awake_mCEEAD79DA9680E4009CAED0FB4852F96C761E89D,
	VideoCaptureNormaUI_OnGUI_m44C9DDCD617F3E6D3E543A12F10CC47911082C17,
	VideoCaptureNormaUI__ctor_mF0776E817775669D8A9575AE9E1F311DB0705BA0,
	VideoCaptureProUI_Awake_mA3B070462E1EA63AD829BC780C9EFB5752FF0A46,
	VideoCaptureProUI_OnGUI_m10995E86AC9F19952E96AD7D89776445927CDF81,
	VideoCaptureProUI__ctor_m30B95DA5A2979F8C36FCF40EC05B7D00D7F81E4C,
	VideoCaptureUI_Awake_m279C3293583F40EC8BBBBBC23AE06F08FD61F015,
	VideoCaptureUI_OnGUI_mB85C7F8CE04B62563D0AB98A67CF42E9EA5E0949,
	VideoCaptureUI__ctor_m5C434FFE0B4F469A426F5F2A41CCC9964BDF25B5,
	EventDelegate__ctor_m06965DDFFC2F7A9853A94C36CFB170615EC0FC8F,
	ErrorDelegate__ctor_m7FCF08BB7D614FAA1CCD31147E0CB6BEC95BF438,
	ErrorDelegate_Invoke_m18868889100082913D01DC5A8DFDF5AC5369B1B5,
	ErrorDelegate_BeginInvoke_mF75AD985DE85036FC9CD15CB61E843658B5639DC,
	ErrorDelegate_EndInvoke_mF3EBBAAEF551D523CDF11810C96BC95C79B5939F,
	CompleteDelegate__ctor_m4281FD3CD64A394E5DD908C9E96EFEE87E2BEDAE,
	CompleteDelegate_Invoke_m2B967011A5EC153DD0E4388C109848D7433445AA,
	CompleteDelegate_BeginInvoke_mAB776D7C091DC78D22D91E0A3CF14E68289EACFC,
	CompleteDelegate_EndInvoke_mA9EE3DD1D8CBC08B0A40B5350BFC4FFBA6827824,
	FPSDisplay_Start_mF5790ACD1FF8D265B1C687E29E3FD234036582D8,
	FPSDisplay_Update_mB24EA6A5FCAA6A46FF0E2085CCCB623C46AD88E9,
	FPSDisplay__ctor_m33EE5DFBCC1A3E63DB8796646BB5641FFDBD2727,
	Platform_IsSupported_mA0FC8553677FFD47C335863EC9E6C5DE288B4754,
	Platform__ctor_m23F1463A7D76B01DF03466F64F545C90265850D5,
	CmdProcess_Run_mC30E3303B51DDEA442A57E761468C900E901BA3C,
	CmdProcess__ctor_m58402B7736247188A60799F5EE17DA178C9B6AA4,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	PromiseCancelledException__ctor_mDA55F2F9C6DF87917C0A40BC149A437C750C66FD,
	PromiseCancelledException__ctor_m0733E2CEAEE71A6A1A22E96FFF62D85627437BF1,
	PredicateWait__ctor_mCD6942E554ED82AF36B3F07828C4E5BCA7F0D318,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	PromiseTimer_WaitFor_m99B243125D5686DC36844FA69D796651E12C9B76,
	PromiseTimer_WaitWhile_m8EF7C405289D3A2AE7F7185AE8E4E901BF504EC9,
	PromiseTimer_WaitUntil_m7AC7AE2AFC6CB5E43A789DE5A6E683D62D23FBFC,
	PromiseTimer_Cancel_m87F8524626DDC28FB1ECA1968F73A2D18540704E,
	PromiseTimer_FindInWaiting_mA1660E092E5065B151FB000488B2C87FF4C6C8F1,
	PromiseTimer_Update_m9C84E7E7B30B14A7C13510CC22AB6032C7C0874F,
	PromiseTimer_RemoveNode_mF32CC3AE04AE1AFBCEF4B8DA7DFD9E2AF02A6E89,
	PromiseTimer__ctor_m3F1298B21CD5644C3AE646911AD13B6FCCD19BE6,
	U3CU3Ec__DisplayClass3_0__ctor_mC51BCEA4436030F27897F20585D501ABF744C78E,
	U3CU3Ec__DisplayClass3_0_U3CWaitForU3Eb__0_m7EEE8458FA739B7F87A8EF705E05039C9EFB483D,
	U3CU3Ec__DisplayClass4_0__ctor_m8884F549B84F64B7E7D4B61E036A398416091313,
	U3CU3Ec__DisplayClass4_0_U3CWaitWhileU3Eb__0_m8DF4AC7205D6CD8FF4E8AE640F92F6AB4A9B59F7,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	ExceptionEventArgs__ctor_m7023DA6E99C0B23D96D0AAB0500F16B19345A30C,
	ExceptionEventArgs_get_Exception_m79958EAB2CAA0E6C91E10902A3ABCC242012745A,
	ExceptionEventArgs_set_Exception_mBEFA8B8A6990A708691413D5E188FD73D6036A88,
	Promise_add_UnhandledException_m501ED8710D326733D0A4B095516E79AD93771A17,
	Promise_remove_UnhandledException_m0A7255398B93C672C3F4AD9AE8CC1260E6BDC493,
	Promise_GetPendingPromises_mCD78AA3FD69C8809D95E0FBBC6DC24DB648CD1E4,
	Promise_get_Id_m2A159B9C83B813983859B9F8497B92A636D26198,
	Promise_get_Name_m2681487A6A6188C86D6B1FEE6F4E42F0817F0AF1,
	Promise_set_Name_m04BD79E80089945F37642A501B2E0C621CFAADFF,
	Promise_get_CurState_mBE60E772D62CB2C8449BF67F3A2106C0E0FA720D,
	Promise_set_CurState_mC9014B9835DB4BDFE7B3E1AFC0B61A249DB0B148,
	Promise__ctor_m72891E8C449A8F0285F2BBEE6596EEE91D1B69D0,
	Promise__ctor_m0F170C315BB66058A0B8DB59D2FB2E269460DB4A,
	Promise_NextId_m91D574BF971140F5BCA824858A1F23785E625936,
	Promise_AddRejectHandler_m8A5C912F1C8D8FC752EAAFCC3397992F84DE2C83,
	Promise_AddResolveHandler_m39C28AC656C57C1865250657901C41569B49B87A,
	Promise_AddProgressHandler_mC3147E564182B45C04B4AD18F55200ADE09494A9,
	Promise_InvokeRejectHandler_mD002929AA239DA2F2D3CBA6B828BCC190E2FA010,
	Promise_InvokeResolveHandler_m8253411D16B16952735214A8D4840B69CBC19367,
	Promise_InvokeProgressHandler_m2767B570D7BE8E457C295823CFD48F4DF079AD1F,
	Promise_ClearHandlers_m77595F54867BFFB97E19A6354940D3C574E8AF30,
	Promise_InvokeRejectHandlers_m78C4A77BB7F1728BCF63373111A992940FD73FBA,
	Promise_InvokeResolveHandlers_m071F1B5884447CEE1F7D19154AB430522BE6C208,
	Promise_InvokeProgressHandlers_m33BBE047F8B32BDC15B943203CF4D653C10F5F3D,
	Promise_Reject_mA334C811DFA609A9294A15B8C4FE3D06CFE59E4B,
	Promise_Resolve_mB395CBF764AD56F64C953F240F5525C4F24AA176,
	Promise_ReportProgress_m7496AD24A305E551C317FAF83A339D50555EC2F0,
	Promise_Done_mBF4D40ECA5734EFE1213BD2AD11124841BCA6C63,
	Promise_Done_mA20B58D299DFC458330AB673679F52760076ED6D,
	Promise_Done_mB94F090BD0EDC421C98291DF2437788619416322,
	Promise_WithName_m9A8610E5066D79F98133FBDE0C427032A9EF04DB,
	Promise_Catch_m34A1A6F483209E344CEE20CAECA131319E25A35F,
	NULL,
	Promise_Then_m660D492D7CB977D2658FDC71CA16C5F1C7C80BFB,
	Promise_Then_mE35145837C7EBB840D64C24A92FFEC013AA4E52F,
	NULL,
	Promise_Then_m9C5987E6126BE35855402986BE5878DDDA5DDD2F,
	Promise_Then_m1F2DA120D93DB9D1801A4410BACE242992BFDE5D,
	NULL,
	Promise_Then_mE03C61503CE58EA2981EF96852E6F252B6F29736,
	Promise_Then_m6693CA7F4467B53237CE92F7F44C36CD44FABE0D,
	Promise_ActionHandlers_m226D1CB18140648C8EC32E3E2FA7DF0730478F26,
	Promise_ProgressHandlers_m21C8B94D38E0FBF1F14B24FCFFFEC214D9D80912,
	Promise_ThenAll_m444DBB4B6489921A44CDCF98AB8D3F2708372D66,
	NULL,
	Promise_All_m4021CBAF737AB75139BE836A21E02B28B238AD21,
	Promise_All_mD80850322247A01D346893F8962617690B9D2606,
	Promise_ThenSequence_mE42D6CD848060EB59C10345B5A14B16724D35F9B,
	Promise_Sequence_m9F9C078AA9DB0404DAB83831A3EF769379E58906,
	Promise_Sequence_m646E63C1583234FE0B9EC2F642D5B1EB7A830E52,
	Promise_ThenRace_mF04FB2EDFB9FF1589EF714F409994945E123EE7D,
	NULL,
	Promise_Race_m7A61FE78FC907244940AE23BE119699E657860BB,
	Promise_Race_m422B8D03C310B70EC818A1A5F7A8E12C836FD877,
	Promise_Resolved_m27D7349EFBB518D89093ADB573BD7D7F836B24DE,
	Promise_Rejected_mF6CA3689BE449406F16A5D145B1DD4FD13C41890,
	Promise_Finally_m762D2CD8F1F48788A1B94FB46CB338D69083230B,
	Promise_ContinueWith_m1A53BD142DC421621617AE8E0BE6DA2053D96D8A,
	NULL,
	Promise_Progress_m53E64BDA8764C5E88057601FAB54BF75FD0B368A,
	Promise_PropagateUnhandledException_m30B4923F2598FA6B4F7CD5964E00760B5D9F10B7,
	Promise__cctor_mB03C9B64D5DF3D552E96C4FCFA942B128173413D,
	Promise_U3CInvokeResolveHandlersU3Eb__35_0_mE4D9BB0CB11B33C2E7F12BFE44103CC546FD2E2A,
	Promise_U3CDoneU3Eb__40_0_m03D038B6097EEEF2B736CCE4A1D9154518BE3E01,
	Promise_U3CDoneU3Eb__41_0_mDE5A60CCBCC7F17ADD74A395C92F553EAAA52817,
	Promise_U3CDoneU3Eb__42_0_m10D5F4FC16A8A05494ED431EA28AA288DB13AE3B,
	U3CU3Ec__DisplayClass34_0__ctor_mF81EF54B39A864EB5E463DA04D72B5F8A0CF4DFC,
	U3CU3Ec__DisplayClass34_0_U3CInvokeRejectHandlersU3Eb__0_mC2CB7940F03A8D527C6C9BA7793BA6C6A07E13FC,
	U3CU3Ec__DisplayClass36_0__ctor_m18834E008AB1CCA3258C2388F1158B37AB6AFAC1,
	U3CU3Ec__DisplayClass36_0_U3CInvokeProgressHandlersU3Eb__0_mB466D8A8500579A62E0EF36D3766DB1A59E89F41,
	U3CU3Ec__DisplayClass44_0__ctor_mCB6B49A87238E9E939A3FB884973D5B8085A1805,
	U3CU3Ec__DisplayClass44_0_U3CCatchU3Eb__0_m59D9BB020E9BAA19BF3A55E1A66B4BBF0115A4F3,
	U3CU3Ec__DisplayClass44_0_U3CCatchU3Eb__1_mC2F86C6B2CC1463D07838F8B6E71CF95A00D6B1C,
	U3CU3Ec__DisplayClass44_0_U3CCatchU3Eb__2_m1C82A7D09BAFC515977C21BBAD3E8EE6E6C8B665,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	U3CU3Ec__DisplayClass52_0__ctor_mB894C314363743106885723C9A014E58D79394DE,
	U3CU3Ec__DisplayClass52_0_U3CThenU3Eb__0_mFAB16B8D05F5B6D1B0820FEAECBB318B28D6F95F,
	U3CU3Ec__DisplayClass52_0_U3CThenU3Eb__2_m68317E06CF2A6CB4E45914984E5E7B1C8E250B3A,
	U3CU3Ec__DisplayClass52_0_U3CThenU3Eb__3_m1DD830DC1CA6AEAB53B32028949E3349E37146EE,
	U3CU3Ec__DisplayClass52_0_U3CThenU3Eb__4_m6F52D5A374717AC944B68D544EF57498A6CF0C12,
	U3CU3Ec__DisplayClass52_0_U3CThenU3Eb__1_mE79481476F323A2E8C64CF6853F48919B454B238,
	U3CU3Ec__DisplayClass53_0__ctor_m8AD8D6610D93664501D710EB55F2D75DE59813DC,
	U3CU3Ec__DisplayClass53_0_U3CThenU3Eb__0_m089AF19F35A75DF89A101BC920FA8F3CBF856F03,
	U3CU3Ec__DisplayClass53_0_U3CThenU3Eb__1_m6BF2C7A1D0A0D3699E5339E92DFF00CA7B60BBE2,
	U3CU3Ec__DisplayClass56_0__ctor_mA16A09395360FDB88DAEB794DAF2A6BAF4CAC8AE,
	U3CU3Ec__DisplayClass56_0_U3CThenAllU3Eb__0_m76809680DE80AD5F5596FA06F41BA163AC6D1C30,
	NULL,
	NULL,
	U3CU3Ec__DisplayClass59_0__ctor_mBA66AC86289688F1C468B96C50BE5358F31A082F,
	U3CU3Ec__DisplayClass59_0_U3CAllU3Eb__0_mA4CF65F285BDA1A6D803D917E9FD265051D6BEB9,
	U3CU3Ec__DisplayClass59_0_U3CAllU3Eb__3_m8253EC136EA78D646B69616B15FA235271A992AA,
	U3CU3Ec__DisplayClass59_1__ctor_m526DF9DF8BBEBFA28914FB4F54E3A1AF03B414BB,
	U3CU3Ec__DisplayClass59_1_U3CAllU3Eb__1_m593233FA219F817C7E2E48E4B57F1967B608EDAE,
	U3CU3Ec__DisplayClass59_1_U3CAllU3Eb__2_m5AC61534D5AE0A52E1F555A299AD9A673E2DFD45,
	U3CU3Ec__DisplayClass60_0__ctor_m99C72D8D0D1E5F3744F901AF2CE1D01B10074CD3,
	U3CU3Ec__DisplayClass60_0_U3CThenSequenceU3Eb__0_m6FC48A06C04839828FBCC417B074F35B1849CBF1,
	U3CU3Ec__DisplayClass62_0__ctor_m4E600EF746E7384F9FF81A106144CCC5CA295EB6,
	U3CU3Ec__DisplayClass62_0_U3CSequenceU3Eb__0_m3BD1BDD0E7EEF8F00E0C7DE8CD8AA502555164A8,
	U3CU3Ec__DisplayClass62_0_U3CSequenceU3Eb__1_m030831A862679BA67E0AFFF2A793684303FDC197,
	U3CU3Ec__DisplayClass62_1__ctor_m4E3A09413376A37B8429009826FB5E58FB5918D7,
	U3CU3Ec__DisplayClass62_1_U3CSequenceU3Eb__2_m38F09C7AB72B5AAAEE2B1F47D8B8E46A8B244D76,
	U3CU3Ec__DisplayClass62_1_U3CSequenceU3Eb__3_m45E00529CA4F236640E37AD28227E06C03260F75,
	U3CU3Ec__DisplayClass63_0__ctor_mA3AE9325599E7C269CF28464DFD338E2FB36D87B,
	U3CU3Ec__DisplayClass63_0_U3CThenRaceU3Eb__0_m5B8C751B1FAD053AE58DE02DF3FEEA12AE43EC44,
	NULL,
	NULL,
	U3CU3Ec__DisplayClass66_0__ctor_m20042739F9915BDA4DCECD38A6FAEC9A32FC9FD4,
	U3CU3Ec__DisplayClass66_0_U3CRaceU3Eb__0_m3C1812F10C134DFFDE20266483FB8CEE43E00D28,
	U3CU3Ec__DisplayClass66_0_U3CRaceU3Eb__2_mD8A7C646663CD58D24E0B9E5AD99A223042BCC7B,
	U3CU3Ec__DisplayClass66_0_U3CRaceU3Eb__3_m5925431199B068785646FFD433C27ACF0DA6C7D2,
	U3CU3Ec__DisplayClass66_1__ctor_m864691E480DAEE7AEEAD46F8FB882828DB249E77,
	U3CU3Ec__DisplayClass66_1_U3CRaceU3Eb__1_mAA09AC32AAC0FB42ED3E102028E9F62480131D48,
	U3CU3Ec__DisplayClass69_0__ctor_m1076FE9DB847E9C246A4B77B4A9AD68DA316DAFC,
	U3CU3Ec__DisplayClass69_0_U3CFinallyU3Eb__0_m05B68401734C79A8332FD12FBF32C5C1DB2FCF61,
	U3CU3Ec__DisplayClass69_0_U3CFinallyU3Eb__1_m805874267E687B76A8017DEFD8A11A96DA9FA8AD,
	U3CU3Ec__DisplayClass70_0__ctor_mD191E17C3CB17FB74E6636B739DFFE3332CF62FD,
	U3CU3Ec__DisplayClass70_0_U3CContinueWithU3Eb__0_m251D75318366BEDFA9CC05A71A3B108CBEBDE4D6,
	U3CU3Ec__DisplayClass70_0_U3CContinueWithU3Eb__1_m8EF480C7A95C97BAE71BB581C32C4B1A14B98178,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	Tuple__ctor_mB90BC7455CD38661E7B174497948846C3717B84A,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	PromiseException__ctor_mC1C353F7C09B485B8EFCF3B176412385745A37B2,
	PromiseException__ctor_m25FEF871F4EADD0813603091A36151F62E072F34,
	PromiseException__ctor_m378D7284D56C1662B14AFD057C60ACBBABCF8895,
	PromiseStateException__ctor_m0841D9ED95CB98A821651EB934050C7F99325539,
	PromiseStateException__ctor_mB10D86B4B0D696B724F6A344C9DA8BA582785E8A,
	PromiseStateException__ctor_m09D7E625AC5B60FBAA33F53C52F2C5B9784C3AA9,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	HttpBase_CreateRequestAndRetry_m6DC921EB7E83F0FB058D9C9889B15D489092D059,
	HttpBase_CreateRequest_m395D82BBC11CA3C9D8FA5DCA55BC4E5AFE6AC451,
	HttpBase_CreateException_m4F00611C1B083E175A53008F96FCDAEED39E094E,
	HttpBase_DebugLog_mB403310A43B2C0F73923BE8135DBA43E5FECF8C3,
	HttpBase_DefaultUnityWebRequest_mA2CA028D057F027E7D8283E63643F0AF2974B069,
	NULL,
	NULL,
	U3CCreateRequestAndRetryU3Ed__0__ctor_mA038A5FC9BE11F7D821C2EB89C903BD81DD6673F,
	U3CCreateRequestAndRetryU3Ed__0_System_IDisposable_Dispose_m0F0654EB7C2275B8DDFC1B070E8B145B2C5FBCAC,
	U3CCreateRequestAndRetryU3Ed__0_MoveNext_mC05783571C265F2A6348BF44253BDF379AD516DD,
	U3CCreateRequestAndRetryU3Ed__0_U3CU3Em__Finally1_m65EAC36497FC4D9964288E99CD0E428ABC806C46,
	U3CCreateRequestAndRetryU3Ed__0_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m585D7434321501EABFADD21A314DCBB231EC3252,
	U3CCreateRequestAndRetryU3Ed__0_System_Collections_IEnumerator_Reset_m09C357E4FCCD4430591B46B1960B5DC7D4BFE376,
	U3CCreateRequestAndRetryU3Ed__0_System_Collections_IEnumerator_get_Current_m4D5CD62D3F82FDDE9C67A9FD2E2894857D7746A6,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	RequestException_get_IsHttpError_mF1D87818843F7F4BFA1BB1B1F1EAD6925497CF30,
	RequestException_set_IsHttpError_mF311DBD704CCC61C23A270F8BCD1E7E8E5F17469,
	RequestException_get_IsNetworkError_mF6C31F24DCF7B5F649A4CABF455EBB03CB53434C,
	RequestException_set_IsNetworkError_mB4D9148437DD8A59B2969764E067E35E085ABB15,
	RequestException_get_StatusCode_m786C170A63F36EF40CF6F4347F5EACC85925E1F1,
	RequestException_set_StatusCode_mFD7034224DB7D4C27984CE0DA365D02173432309,
	RequestException_get_ServerMessage_m14FEBDED7437643B057376759428572897C03597,
	RequestException_set_ServerMessage_m3DE596FD331A4F694B415FA3C8886FDC5F861DE9,
	RequestException_get_Response_m4FE8480DACD2F2347FDE3BC8F0843885E10912E5,
	RequestException_set_Response_mEF4EB83001EB93F345CD9FB6214237F3888EDA25,
	RequestException__ctor_m4255A573E554424A1B5C9F6E2D58590C50877C85,
	RequestException__ctor_m2A12EC5D24756FA04C4D080FFD4DB9264B01753B,
	RequestException__ctor_m230AC0532C60E770A87DB74A57630F5464269D23,
	RequestException__ctor_mD2293F4AA83F9A3A3EA0E48219894E91DF485FCC,
	RequestHelper_get_Uri_m579870F497E54040498F01E0F03D8E385B75682B,
	RequestHelper_set_Uri_mED832E7E6B02422B23573A3BE154D41312A32BE3,
	RequestHelper_get_Method_m3CAF08A8E99D378C7693940ED9BBC576130C5558,
	RequestHelper_set_Method_m93E8A5F66E37E3459E8F1689E272C9EA2303EEA2,
	RequestHelper_get_Body_mE96291B55EA0C31805FDF261E1A6E0E0A04B9B4D,
	RequestHelper_set_Body_m6C34BB52EE69E8A7D5B2FC14CE8478A4CCCB223A,
	RequestHelper_get_BodyString_m583C6B16055049B23992E90B9BE9240D2494C872,
	RequestHelper_set_BodyString_m264B491596B1E5D59DEBEDFA6D2C07EC37F6AF25,
	RequestHelper_get_BodyRaw_m0567B940B2FD769D548A63A3937EDAB8AF8BEB07,
	RequestHelper_set_BodyRaw_m4BB1B19C3109ED8297827657F5855DCAF7EF315E,
	RequestHelper_get_Timeout_m5EB987EDD3D72C0AFC56600A506DED4393B31D03,
	RequestHelper_set_Timeout_m8769C12B49481D0B8BC11B3227F802FDF049707F,
	RequestHelper_get_ContentType_mF1D15920E2C975B8788B61B57B6A0EC713C5EB97,
	RequestHelper_set_ContentType_m53EBFC2111ABCD757D3B613E943FED0F2A916B1F,
	RequestHelper_get_Retries_mFE6F51F6EE991FEE0982128A8305F013668B7D5B,
	RequestHelper_set_Retries_m8E91AB08F2D651BE88B57973D472A87CFE403AE1,
	RequestHelper_get_RetrySecondsDelay_mF461806AF11B3F89D745861D3EE818A9548705F5,
	RequestHelper_set_RetrySecondsDelay_mF554D9A11BAD76DF31F07A5FA92B67E34FF7D6F8,
	RequestHelper_get_RetryCallback_m8487D96D1889FAC85CC99D59CE9AAD81AF975858,
	RequestHelper_set_RetryCallback_m8B02AFF7AB55609093C48182D79501B34C2F2646,
	RequestHelper_get_EnableDebug_m031DA45E2494D29141E793FBFE2BEF047DCF7472,
	RequestHelper_set_EnableDebug_m7DEAE27887D92A686E341EB14010DE5A4B3E4DF9,
	RequestHelper_get_UseHttpContinue_m64713A0BA2260B82D1587517ADF0671DB60B8D66,
	RequestHelper_set_UseHttpContinue_m454427FB941A608ABD370EC1E64F0C5166183E48,
	RequestHelper_get_RedirectLimit_m7628CFD271A1A06DBF264C289268CE52DA66DB82,
	RequestHelper_set_RedirectLimit_m32E15A1AE611D405855B008D58576C8839929DC3,
	RequestHelper_get_IgnoreHttpException_m0BA6B17AACCF9C74FEFD730806A64B67E4505FEA,
	RequestHelper_set_IgnoreHttpException_m6653B079C6C70E478895D1EF4BFAC9FE5D5D2209,
	RequestHelper_get_FormData_m1CE699C5BC879C7AEFBB1084CF0A1928CD02B931,
	RequestHelper_set_FormData_mD33B2EEF5749F7898DB81BD2ACEB0F7EA9E97EC4,
	RequestHelper_get_SimpleForm_mE158581E8CAC46C434A072324A6290CBE4878C78,
	RequestHelper_set_SimpleForm_m885368EFE7D8FF37C0E9CBC36A8B551562042165,
	RequestHelper_get_FormSections_m4157D4255B827B864F882669136AB30BA93316F0,
	RequestHelper_set_FormSections_m955F901BFE0A3D2FFE42F223BB2005BA2028337E,
	RequestHelper_get_CertificateHandler_mBF95069255782425F21F491C5A51F3BBCEE956B1,
	RequestHelper_set_CertificateHandler_m1B5296D19D58F16EF358706D8AC3817F0ED0B1C5,
	RequestHelper_get_UploadHandler_m5C72A7EF49F678C8CB02FA0620C0B41D1CF8AE78,
	RequestHelper_set_UploadHandler_mDA3C9BCAE240C2A5185B88FE2FE7BF198B6467ED,
	RequestHelper_get_DownloadHandler_m3CC95DFC6C81ABD8508947C1808BC8FB7095BDA3,
	RequestHelper_set_DownloadHandler_mE7FC2C6C38C5418A915D4007DCFF2A9CF5C6DC74,
	RequestHelper_get_Headers_m09842B9228E6B35E7254DFAC41A5D2C98DBF0BE9,
	RequestHelper_set_Headers_mE4B41FA1E24D02AD057068E2D0A697C129921A29,
	RequestHelper_get_Params_mDB495A9CB92B43BB1BD3526C99880EFBDC40E033,
	RequestHelper_set_Params_m887AAEE89701BE57B2875DD482A584C58EF7C400,
	RequestHelper_get_ParseResponseBody_m8352D39E0EBACC6FB532FBE05B5D3EC709EA43BD,
	RequestHelper_set_ParseResponseBody_m1149B5FC0628F6BCDAFA95D21516A9CAE3F4031D,
	RequestHelper_get_Request_mB3DD0BDA5D264948DE92BA5E060545D8D3CCBD63,
	RequestHelper_set_Request_m4C49567052095FC092A24AD821EF7957CCB6F4BF,
	RequestHelper_get_UploadProgress_m4F7D8BCA66AE1D13F1C59D91EE10E656D48DE91B,
	RequestHelper_get_UploadedBytes_m7D3445EE67E794E91D82A28171224EED03460533,
	RequestHelper_get_DownloadProgress_m0ADF466E7C45A9D043506B099C6B3E7A08C5BE33,
	RequestHelper_get_DownloadedBytes_m5201AEFCAF699F4C054CBA3C63004950C1E32ED7,
	RequestHelper_GetHeader_m0EAA6AEED4B885553B4CD97B2C4CBF81A32EF468,
	RequestHelper_get_IsAborted_mED2B0C2E2FA7DA65574ED4141818116D5407825B,
	RequestHelper_set_IsAborted_mF37D6A6B93B70D7B4AC2B579FDA59345D0D51DDF,
	RequestHelper_get_DefaultContentType_mF52E8AD322DC2F3A0A0444FA1A60D2C62B42113F,
	RequestHelper_set_DefaultContentType_m0098EE480F8E776673979AFB751C76CD61AB5365,
	RequestHelper_Abort_m7CE069D799170B7D0336E7DB358C78B1B00DA8F3,
	RequestHelper__ctor_m76D5CE4B07BAD0EFE344E9A8C4D1BFE895922C13,
	ResponseHelper_get_Request_mE2DF5B066E83E4216167EEE1A28A79A09A372843,
	ResponseHelper_set_Request_m9743967B9D789F748E8205E910F339114CB7D522,
	ResponseHelper__ctor_m0F45F428ADF231F62EC7C6E5621058DA806A1850,
	ResponseHelper_get_StatusCode_mAC67167172472DC6804430CA8FDEDDBF1FED2D4F,
	ResponseHelper_get_Data_mDFB63D2691666575C126F59C928580F359646E44,
	ResponseHelper_get_Text_m4C6CE813DC4FA5B0659BF20EC4B5EE2504C618AF,
	ResponseHelper_get_Error_mAF99FD6F00AF225E4D2EF620266C6A428199F6E7,
	ResponseHelper_get_Headers_m2990A8F894F71B2503103388796B0717EEF9D9CB,
	ResponseHelper_GetHeader_m9E39EF8000022EA04DF66B43FCCB31AA01AB0D52,
	ResponseHelper_ToString_m3DD8554605B5BDE7A6412218369E317C804B0572,
	StaticCoroutine_get_Runner_mC13BCB3C4C6F548812E35E5555D3455AEB2700DF,
	StaticCoroutine_StartCoroutine_m46187D9942A6387EAD6EEE4771C5C54027547C58,
	CoroutineHolder__ctor_mC1424305AB5B1F2E631B7121BBB184B1B8BBBCF6,
	RestClient_get_Version_m5701043CF8E7F883ACF65A25CADCE6AB4AFC8EB0,
	RestClient_get_DefaultRequestParams_mE23F55E6B8003CFEE7FF60C7483232FE7E247A3D,
	RestClient_set_DefaultRequestParams_mA826F5ADE53A384196CC73D37D26E72E5B7CAA9B,
	RestClient_ClearDefaultParams_mCBFF0C9C7DA720E8F0F9D54D0BDDE3D1C421CA71,
	RestClient_get_DefaultRequestHeaders_m7CFC695866D2FFFAE6C2C79B38564917EFFE37AA,
	RestClient_set_DefaultRequestHeaders_m8ED47C4D90B6F05DB8ED523A5A49C2B0B1B7C2B5,
	RestClient_ClearDefaultHeaders_mFADDD96FA3D43A3CFC44B9A6A641B666F000C09F,
	RestClient_Request_m6D0EEC74B2073F8006977A737FEB5A26FF8B2BE4,
	NULL,
	RestClient_Get_mD3CD57C7B7809173BB0841F05883D4EAB667C4E4,
	RestClient_Get_m62DDD6AA1F3E2D93DC5AF9378090C97D67EE7D54,
	NULL,
	NULL,
	NULL,
	NULL,
	RestClient_Post_m4B07264A4B85D23B2F3013C227AB599B04F18E7A,
	RestClient_Post_mAD4114F33CCE7E913400D3AA42D2494C1C71D27A,
	RestClient_Post_mB43880A95D1AD7E76C1878CB288DC4E6B3453651,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	RestClient_Put_m955E298F1437E5DDB4391BE3BF2A58B02232AA4E,
	RestClient_Put_m8AF7486C0FD795F8D4CCEBE663A56980E9397A02,
	RestClient_Put_m08D2C2C9C8B6F73A7531BF8BD2AC15C9C5E13680,
	NULL,
	NULL,
	NULL,
	RestClient_Delete_m7701A6040B30FF21656087D1FFBE7BCAFA782E16,
	RestClient_Delete_m1C6AA27C4494A101CED7B1B6BFE9A3B38247FB60,
	RestClient_Head_m01F7D5AEF77C54D5AE2948C1F579C1FD856AB0A8,
	RestClient_Head_mFEFB7B87CFC87642E95B09D1A4AB76F63ABA3E14,
	RestClient_Request_mA130CF629F30CB634AA89655F61C5BD37F729409,
	NULL,
	RestClient_Get_m7E5DEB235C456E5FAFCA828BF688ECFD630E86F7,
	RestClient_Get_mDEA2483857BD9E64941E12CB42063D74EEF50FE6,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	RestClient_Post_m4763A3817990302191F3A5B069A64B02419523BC,
	RestClient_Post_mD59FE057DB71BF372315AB100BD5E4339FFFFE07,
	RestClient_Post_m27A12B1AAD73F990A53E62B578D01BF4EA933509,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	RestClient_Put_mB84F593B9E21B27814D3EBAA2920B67D1699FD26,
	RestClient_Put_m3E06B35E9CFAE4AF81DCF12A8F30A178E1CC5ABA,
	RestClient_Put_m5E7F007136807CD127CA4FD4D6A986B3979391F4,
	NULL,
	NULL,
	NULL,
	RestClient_Delete_m7CACD86724C07540430FDA0E5D759F6E57BBF75C,
	RestClient_Delete_m33B84121232197C62DB7D3BAFB4E5F39CEF4C474,
	RestClient_Head_m6CA11D53CD3CD3724C345EA2DCEBCF665E31D101,
	RestClient_Head_mFE1399A7241D6D868C0AB0B65C774812167D9D43,
	NULL,
	NULL,
	Common_GetFormSectionsContentType_m19E34A4A4613B617EB59187AE573639031CD6B93,
	Common_ConfigureWebRequestWithOptions_mF5668E71E221566BFD09C871A2B0667206901F78,
	Common_SendWebRequestWithOptions_mA6DA8F8469D1F337E3CB043E57F2191D8D95EE1B,
	U3CSendWebRequestWithOptionsU3Ed__4__ctor_m47FB1CC913CF34CE244286B13D6600F97F2C7006,
	U3CSendWebRequestWithOptionsU3Ed__4_System_IDisposable_Dispose_m5F26553216BF51BE2953AFD9118938F28D511A34,
	U3CSendWebRequestWithOptionsU3Ed__4_MoveNext_mB5E4CFE2AAE6A9BCE0D76C72C378282AFCB56C68,
	U3CSendWebRequestWithOptionsU3Ed__4_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mF49002192678DCE4CE3D8D78E1E922C730FED67C,
	U3CSendWebRequestWithOptionsU3Ed__4_System_Collections_IEnumerator_Reset_m82497AA2724FEA6918E2C0900364DA889677B45E,
	U3CSendWebRequestWithOptionsU3Ed__4_System_Collections_IEnumerator_get_Current_mD15322133CA601A9884E3FCBDE8BD76FFFB1558D,
	Extensions_CreateWebResponse_m85A8BFBB1916DE1247943658A0758B372807A7F1,
	Extensions_IsValidRequest_m95295E8977C94FB722EE97C5047C3975DE5EA65A,
	Extensions_EscapeURL_mAAD51E7CBC83CB8CE79715D8A6086CB153919CF7,
	Extensions_BuildUrl_mE2D5E2B005BCD677EED820AE36D5F28F01E57CA0,
	U3CU3Ec__DisplayClass3_0__ctor_mDEE443CFFB4553720586AE36F8109D07F7E08B87,
	U3CU3Ec__DisplayClass3_0_U3CBuildUrlU3Eb__0_mDFB7EBC5B80A07C2899A8B10EE18130CC2AE4157,
	U3CU3Ec__cctor_m51C966458A7B400E929B365351E9FA572508174F,
	U3CU3Ec__ctor_m3AF7EC6F7B8017905D17E0078A8E8219711B52B7,
	U3CU3Ec_U3CBuildUrlU3Eb__3_1_m3F16B42B55E86F33694E97ABD64E3ADB7B6EDD03,
	Photo_ToString_mC7549402494D8D3A4192988B7A75C3F43BB81C7D,
	Photo__ctor_mFB7B355DA04B8883B308CC03347EF777898F6846,
	Post_ToString_m4AD8D8586CF507C7C435254089148F3AD32A855A,
	Post__ctor_mAD9D421E1716FEB8D8B297565CE8F10FFB42CC79,
	Todo_ToString_mAD1AD0938A238807A2D0EA2888893C11CBE105CF,
	Todo__ctor_m3DBCDF59C636313C811C93C83ED5ABC292A19AA4,
	User_ToString_mD57533E879F6F6B77BD1A38883A0DAED53D2FAF2,
	User__ctor_m179E4458F95BA48DD9D0F7E1E4DF8AC26DB05C0F,
	GetSetAttribute__ctor_mAF21003A3420F0175EE49705C2FE162E4768DCF6,
	MinAttribute__ctor_m0704195F6CAA3E54C6070CFAF5CD4B4F6F59E2C0,
	TrackballAttribute__ctor_mF93A32F3F5A18D303CFF8FF6D3B3B4639F950228,
	TrackballGroupAttribute__ctor_mF3ADA8F2E6E316B188CC9394CC3405388C8C51C8,
	AmbientOcclusionComponent_get_occlusionSource_m539EF11FF7DEAF5A2AF8DA4D62BCB7E2306B3087,
	AmbientOcclusionComponent_get_ambientOnlySupported_m51F05150E7C98A0D83EE87D72077E1173AA6D76B,
	AmbientOcclusionComponent_get_active_mBB714D81DE71CDD3225A84D76E5BA62F93AFFB77,
	AmbientOcclusionComponent_GetCameraFlags_m7A30280E0AD156B00DBC2EAB46DDAECFD9293B4C,
	AmbientOcclusionComponent_GetName_m1B868250FEF54023814DAB66FF4AC65485C65C50,
	AmbientOcclusionComponent_GetCameraEvent_m160D92A8753CAA68437A1D84D2062D9CAB866925,
	AmbientOcclusionComponent_PopulateCommandBuffer_m5FCF85B0AB81F6CF61C733CA8DB23F9A7B5C4684,
	AmbientOcclusionComponent__ctor_mF0CCE56C8B25A75C4E0E59927C53142871DEC786,
	Uniforms__cctor_m38063CB99FC6BA1488ACFE086DDCEB8D9DC80138,
	BloomComponent_get_active_mA00BB534E2DA1DA4B64C1E61A457DB63D837F8A2,
	BloomComponent_Prepare_m97F648F4A3A407FB316504546B6DB8B5A4451328,
	BloomComponent__ctor_m55F038989C9314A1E26B4982F0F6BBDED033B736,
	Uniforms__cctor_m215A49821B9DBA3B81F09CEE54AF47791B5C8BAC,
	BuiltinDebugViewsComponent_get_active_mA2483382ECAAA5FC057939A60CF19F68498A573F,
	BuiltinDebugViewsComponent_GetCameraFlags_m8B43D922F78B0ACB29DB8D5830E526021E5C2D79,
	BuiltinDebugViewsComponent_GetCameraEvent_mEDCAE8A6EA1E9AA91B0829F5149489F5BC705E41,
	BuiltinDebugViewsComponent_GetName_m8DBB4AB637C5D0E59FC84F349FBB7EB8788D902F,
	BuiltinDebugViewsComponent_PopulateCommandBuffer_m22521778E45D4D9A6259572BD36E1480C6E1E611,
	BuiltinDebugViewsComponent_DepthPass_mAB73CB7F0F7A29010CE83932B544E60484BD7694,
	BuiltinDebugViewsComponent_DepthNormalsPass_mED9874A8E542D02042C9147EEC6A8A60E3AB69FB,
	BuiltinDebugViewsComponent_MotionVectorsPass_m6FF4B48F071D636346B28D31A072E0575245EC0F,
	BuiltinDebugViewsComponent_PrepareArrows_m282F4F365B391877ABF777654D7C039765C384AD,
	BuiltinDebugViewsComponent_OnDisable_m0DEEA40C98B18032BC08E77D104CABD2393C3F7A,
	BuiltinDebugViewsComponent__ctor_m9F0C4752290D80BC9BE4816EF3E0547A38690F64,
	Uniforms__cctor_m9BBD20D226C673614BA222D689A0F7231D06E54D,
	ArrowArray_get_mesh_m159E0A6065671A721BF4F38F67796DFBF6A39A87,
	ArrowArray_set_mesh_mC3E00DD3EC2EBB9ABC145AB25262CE34FB47EA53,
	ArrowArray_get_columnCount_mB2CE9D04ED4F52E6EB3270467EB6E9E7EC99DDF7,
	ArrowArray_set_columnCount_m93B0297CF16BB02FCBB8FD401A27A5C9C05771E3,
	ArrowArray_get_rowCount_m880F3F390ECA0958C345F986ED41AF4118B77FBD,
	ArrowArray_set_rowCount_m2B86B6790CEA32DE1C0B8A740764506E085C009C,
	ArrowArray_BuildMesh_mD099690ED8AAB42CBFF3AEB8A2BF30C02789D31A,
	ArrowArray_Release_m9093FA5C7964D9CFD460953693E4493F9DEC8412,
	ArrowArray__ctor_mD7C99DB45967E3591400E99E778D38AB6DD1AA00,
	ChromaticAberrationComponent_get_active_mE1E4B9B6240668C2B348386058E424E513815684,
	ChromaticAberrationComponent_OnDisable_mD56F80D490E0BFE49CC996DB57F90E4F90555D93,
	ChromaticAberrationComponent_Prepare_mFEE25577FD9C583283667A05ED351A23BD963512,
	ChromaticAberrationComponent__ctor_m83C6BF5444D4B1E1EB50D0F87DF969F0972B55CA,
	Uniforms__cctor_mECD80BABCE4BF5D9439FCFFC0DA6292F8AD02F1A,
	ColorGradingComponent_get_active_m06FF224203204A0C9C1E9D85083787AAB549C3C6,
	ColorGradingComponent_StandardIlluminantY_m581464CCCCD31BCE758529AB2E743634BCDD2CF7,
	ColorGradingComponent_CIExyToLMS_m2BA32E63A2E455084AE2B039067ECD9A5810F898,
	ColorGradingComponent_CalculateColorBalance_m7A0D32191B6A190B555F36BE084D5C70F36AA1B3,
	ColorGradingComponent_NormalizeColor_m5150AA8F3014FFB130253EC44DDEFE52A6562AC6,
	ColorGradingComponent_ClampVector_m267C996BD2AC79C64AEFF9B0D385E4BAB01BCB54,
	ColorGradingComponent_GetLiftValue_m1F32FEB2B18FB34B9DD00B4B9A69288324A3D368,
	ColorGradingComponent_GetGammaValue_mCDE9CB91EC493EFC3F9A2B4F56634D6AB11C2C31,
	ColorGradingComponent_GetGainValue_mC6F4AE2D1ED3731FD7490FFC9F58E24A32E0F209,
	ColorGradingComponent_CalculateLiftGammaGain_m0ADD9FBA6B77C265FEECD43D65B7824CD7AD4B5C,
	ColorGradingComponent_GetSlopeValue_m6A182C21CD54B6820A437F232688BEF746B89468,
	ColorGradingComponent_GetPowerValue_m4C3D95344E89A38105B7E1B1A20673B4DDC4CD69,
	ColorGradingComponent_GetOffsetValue_m45983DC54587B5F797E190B5DBCEC7DAA9745C7E,
	ColorGradingComponent_CalculateSlopePowerOffset_m7FB780E9E8246D5CD53FC32A903DB2FD53365B59,
	ColorGradingComponent_GetCurveFormat_m4424EEFD351F7D82677A27E032372B949100EEAF,
	ColorGradingComponent_GetCurveTexture_m99F8CF5E3F40CE250556AD73806938A414EB6179,
	ColorGradingComponent_IsLogLutValid_m056EB20B397402DD34083FFC90483ABA8DCEE9C1,
	ColorGradingComponent_GetLutFormat_m832EFFCC1E7B679943326016B997010DB018EE13,
	ColorGradingComponent_GenerateLut_mA250C15593E782D404EF571D06F115C0D9B8BDD2,
	ColorGradingComponent_Prepare_m57655813E453F4CBD43034CBA52B0A00FE34FE77,
	ColorGradingComponent_OnGUI_m0FB84319DD49AD0796B54E8D8DC9664C5BDCAF11,
	ColorGradingComponent_OnDisable_m5B1D1BB75DDA375E92E412CA5CD447A6C3FDBBBE,
	ColorGradingComponent__ctor_m7D5E294E388470CA7833B9A0F28B7FBF3423B0AC,
	Uniforms__cctor_mE12551BB740B4F63B39B02596ADBD4F2CC6CC678,
	DepthOfFieldComponent_get_active_m993080D9AD09D645A61D4E0E8D748ABD6D224F71,
	DepthOfFieldComponent_GetCameraFlags_m73FE5E723AF6B14352BF3BC688D2CA6FFEAF4AD1,
	DepthOfFieldComponent_CalculateFocalLength_m4EFED65E4182D4569FE7C6D765F7D6C0B037342D,
	DepthOfFieldComponent_CalculateMaxCoCRadius_m6ABFEDD08B6D26CEDCE048F457E41F7BD8430426,
	DepthOfFieldComponent_CheckHistory_mD1A7CAA19DB8156E0D31DCAEC232BCEEDBF769BE,
	DepthOfFieldComponent_SelectFormat_mA1CF45D90B17B592B09F2D0FB108D3C58EF65068,
	DepthOfFieldComponent_Prepare_m0DBA4F66A1A6DBA7C5E3353F75CE5DA101E5339F,
	DepthOfFieldComponent_OnDisable_mAECFFE4FD3BD34B9B8CE20E64E1E8FB5F4A322E3,
	DepthOfFieldComponent__ctor_mD6E5A73309A102E57430FD026794AA47699EA8DD,
	Uniforms__cctor_m87771E6C48E0FBFF2BA6EDCB6CB158700214A7D0,
	DitheringComponent_get_active_m816D22E56F17DECCE0E81FD52DE2AC8F7B1D75CA,
	DitheringComponent_OnDisable_m173236CDDAF599D5FA51C7F4F8637C5310961512,
	DitheringComponent_LoadNoiseTextures_m0029809F2A7586A885CD81FA44C89C97437C1EDA,
	DitheringComponent_Prepare_mA7C5E092DB8593AEE727910EABB776304C8C57FD,
	DitheringComponent__ctor_mD28F2C16C6F0DDBBD496FB1352C7E5AC8AFCBA84,
	Uniforms__cctor_mF9C28E0D67C284374071F66F847932B8ADEFE184,
	EyeAdaptationComponent_get_active_mFCE81CACA6990FBE1CCF8AE09AB85145AFA41206,
	EyeAdaptationComponent_ResetHistory_mAB55785616A0B69AC5078CC3A6C43A30806FB105,
	EyeAdaptationComponent_OnEnable_mE2253875624E2CB2552BB5F860B8D229D088AD00,
	EyeAdaptationComponent_OnDisable_mF2E5DA2DF3E03944A82090763C54BA71C36A5BD8,
	EyeAdaptationComponent_GetHistogramScaleOffsetRes_m3C6B2F85CFFE17B2297D3D9C21BAE4757DAD7282,
	EyeAdaptationComponent_Prepare_mAE6FADB01F55216B9D4567C267F7D8B3B21845C9,
	EyeAdaptationComponent_OnGUI_mC4F29D66DB0AE8041F3D0D47AC73C3AD4E53A155,
	EyeAdaptationComponent__ctor_m6A76F04CBFE8625723A4AAF099DA883F9197DD51,
	Uniforms__cctor_mCB841BE0F9AF1A2E32E94B3B7E5424BC37B0DA0F,
	FogComponent_get_active_m26A7ECF24D484516C89DCC8FDF7213A58065FBE2,
	FogComponent_GetName_mF73166C2D760FD5316166C7BF34AA8D19A406CA5,
	FogComponent_GetCameraFlags_m2E3A15A63E1CC2D539647107618B8DF54938E039,
	FogComponent_GetCameraEvent_m2A950B3F52828FD84199F9764CB9D06A6CA5F69D,
	FogComponent_PopulateCommandBuffer_m5FCD2F4D55805CDCDFB87790E90658695144ECA9,
	FogComponent__ctor_m6A272084C05BFB3E36EEF74CDD2CD0CF16C1F423,
	Uniforms__cctor_mFF3DEA013808C7E86DCEA731A1040FB75B3718AD,
	FxaaComponent_get_active_m780EA1897EEF779271A578CE97F4826C066C5DC3,
	FxaaComponent_Render_m5FCD55022139AA2AD08D6DC1A47549D76CD7A6FB,
	FxaaComponent__ctor_m9505105E1AF33D0CEC66259AA03835E2D5F513B3,
	Uniforms__cctor_m9BBFBBF482F4984B56BA50885F227F07ED63ADCC,
	GrainComponent_get_active_mC8B4EA5150B86CADA8134F1F0B3A7AD7F413FB08,
	GrainComponent_OnDisable_m57ADB3E0620BF47720A3B780992574F4B571FE6E,
	GrainComponent_Prepare_m6E00891FF46600D26B740F07D98575D02A9A0B68,
	GrainComponent__ctor_mA1AC4A955638A99C429FB77C77141DD743542EFF,
	Uniforms__cctor_mC7ABD16006C393A6C02C2E638E251B28E97CBC22,
	MotionBlurComponent_get_reconstructionFilter_mC7F9F8BAD03D07FA3404C9315F40CBB3509F199A,
	MotionBlurComponent_get_frameBlendingFilter_mB0E43D1A8D75BE1609822D4579DCB1CDD7661189,
	MotionBlurComponent_get_active_mC0C49847057158AD983D269D79F8718B3860B2D2,
	MotionBlurComponent_GetName_mBD539B9E35CC023B87FDE1E5F2BF1F25D922BC3B,
	MotionBlurComponent_ResetHistory_mFEC7FA4E6E68B0D8A57FD23E8B2308187012D5E3,
	MotionBlurComponent_GetCameraFlags_m257C15E2D8054287D819E0F5D6AEC3E08EA1178B,
	MotionBlurComponent_GetCameraEvent_m22E76B13B7FD0EF024546E0BABB0D7F09BA47197,
	MotionBlurComponent_OnEnable_m6282F206DEBD1540BD1D513DFDB0BAEB81AD33B6,
	MotionBlurComponent_PopulateCommandBuffer_m011F5A2E9E9C49AAE17CF6B65BB4B5EA5FED6DCD,
	MotionBlurComponent_OnDisable_mC7F8B944F6A0169608AE671987149713A02AB1A1,
	MotionBlurComponent__ctor_mA39D0CE3C49A4C6CB7E2392BD3D22B7A311A74CD,
	Uniforms__cctor_m3EE474D5E17F24D24155C55C631FB6D8FFFA6DDF,
	ReconstructionFilter__ctor_m3079740618840383340D431A8059BCF18A18EE7A,
	ReconstructionFilter_CheckTextureFormatSupport_m1825CA06223703C26B85AF769A244A466FAB9D99,
	ReconstructionFilter_IsSupported_mB6E6FCF099F4EF11910C0F20CF7F9912B747F595,
	ReconstructionFilter_ProcessImage_mDB83D5078ED2A115849DC99D52A425BA2DF8EAA5,
	FrameBlendingFilter__ctor_m3364D401CAFA1CBF2CF6640FAA055BA2F668A5FF,
	FrameBlendingFilter_Dispose_mF4FE203027B3F40753941C07564936DEC7339D7D,
	FrameBlendingFilter_PushFrame_m58435E2ADBE0ED73A3667FC7587594D00ED7BD8F,
	FrameBlendingFilter_BlendFrames_m04F1245F52C29DE197C961C1B92ED33A23DA752E,
	FrameBlendingFilter_CheckSupportCompression_m8B4682F8535091B8CFE59E98D0637F801FDC8958,
	FrameBlendingFilter_GetPreferredRenderTextureFormat_mC6E3869191968D568B48B17F4D6D20ED50364B88,
	FrameBlendingFilter_GetFrameRelative_m41534F912EDEF6872D5976D4152C62AA07CAA4D4,
	Frame_CalculateWeight_mB09982C3C7BCBBB4235ADE144EF1BB1AB31DE077,
	Frame_Release_mC179B110B1D26F0A5B7EEED6AE18661F88695A74,
	Frame_MakeRecord_m76C2721EA40FE50536656F9CDCEB46FA9ED9FEAE,
	Frame_MakeRecordRaw_m99E90CFFDF04BA32505153D2A005A5AF23ADD9AB,
	ScreenSpaceReflectionComponent_GetCameraFlags_m0711FC9BF9AB4ABE9B35C45B57BDD6E76E25E56C,
	ScreenSpaceReflectionComponent_get_active_m331F0E77DD6652C0A1854D56C80C1573427B266D,
	ScreenSpaceReflectionComponent_OnEnable_mC1B38421A706ADCAB25BD942ACB6E270B6FDEF85,
	ScreenSpaceReflectionComponent_GetName_m0DD750812DE714C1BCEFD128FE33C39E1BF1719A,
	ScreenSpaceReflectionComponent_GetCameraEvent_m72344E18F41C83F2553E1E29F968032FF97E7A75,
	ScreenSpaceReflectionComponent_PopulateCommandBuffer_mB47CEFFC0F11542B1374735D7F38F45C1B2062F5,
	ScreenSpaceReflectionComponent__ctor_m251EC3222765FB6E4AF6C7D8DC95CED53731ADE4,
	Uniforms__cctor_m0392FF620FE602B3A304DAC3C489D30D16F81323,
	TaaComponent_get_active_m683D5A91BF3C0413C1AF45B2567E809FB132C944,
	TaaComponent_GetCameraFlags_mED6480C0D60FBEA16CDF524DF0224E3BC5E60586,
	TaaComponent_get_jitterVector_mC23A20437241F427CD0A37942FAC57B0180FD9CE,
	TaaComponent_set_jitterVector_m208276722CBEFEF46309B75A354A4A2CD04895A4,
	TaaComponent_ResetHistory_m714EB9AB2A2C116E2FCA79BD4C61ED44CCAD9B55,
	TaaComponent_SetProjectionMatrix_m9207B34FA45A961CAC1E340869772E7720931A24,
	TaaComponent_Render_mC6E5E9DBE5AADC291AE9733EE2547DB4CF6B1DF4,
	TaaComponent_GetHaltonValue_mCC40F798F5C62666E81D47AA38AACB10D046077A,
	TaaComponent_GenerateRandomOffset_m6FC76A774D49521886A7E6E21C5D336CDCFA1683,
	TaaComponent_GetPerspectiveProjectionMatrix_m97C63BC6280C46E84020F0BD84137153AC40C68C,
	TaaComponent_GetOrthographicProjectionMatrix_m0323DE161A5183F1043F4BD85AB88B794186E770,
	TaaComponent_OnDisable_mBD36E96001415D8D154C85B44AE5D8AC9168C098,
	TaaComponent__ctor_mF0777147EE098DA4C0289923CECAB38FBB8EF8AF,
	Uniforms__cctor_m5F2E7221A556E2585D1E296269A93552CA10ABBD,
	UserLutComponent_get_active_m708F304C0BD287F6657D19FA5C33D8EE83559061,
	UserLutComponent_Prepare_mA899F06F7766DA8F8F6D2E8244AA664333E0857A,
	UserLutComponent_OnGUI_mAF78EF9EE48CD319F5891AE865E03BE218FEF2F3,
	UserLutComponent__ctor_mE5E7694AAB287B8B19131A0EDCB668650B098F85,
	Uniforms__cctor_mE574B2AF3734357100C3A22818DE21E309D427F0,
	VignetteComponent_get_active_m898C5FC9A65D8EB081DE5AA9AEC80BC3B67A0DB8,
	VignetteComponent_Prepare_mE406B9EFC58A96AAD7EF40A2CC8FEF86970CC814,
	VignetteComponent__ctor_mFF5B974DD88A3B8670E34CD6CE4F255BF6CC9331,
	Uniforms__cctor_m240E30AFE4A52ECDF8B50B89C787F6AEE08BF923,
	AmbientOcclusionModel_get_settings_mD3A1F236E5E0BB83A4E17683BE3832B2F466FD96,
	AmbientOcclusionModel_set_settings_mCFAC76BB653A14E77889310220A26F12B65A52A9,
	AmbientOcclusionModel_Reset_mD5669B61B25C9FBEE3EF377DD0AD772FFC863D11,
	AmbientOcclusionModel__ctor_m536DD8852E6BD73319678C4DEBEC547C43D27FC5,
	Settings_get_defaultSettings_mE0F1D8A6811845D21B569AC011D5135920F9191A,
	AntialiasingModel_get_settings_m6369C917355D6197F56EFB2C918D620475E45829,
	AntialiasingModel_set_settings_m8C819CDB77BF9ADF26A5436380BE95F700699CC3,
	AntialiasingModel_Reset_m141981BD07C00463EF34B84CCD6096B52796A1D9,
	AntialiasingModel__ctor_mC32E11D67807F75D4D96335C7053700AA2078234,
	FxaaQualitySettings__cctor_mAAC1546B619A2307C84E1A1F861A52BA5BFEA4C9,
	FxaaConsoleSettings__cctor_m36CCF558A8C4D525C3C52A5D2F553FA7793C389F,
	FxaaSettings_get_defaultSettings_mCC499F9BC608EB8943C846C838A5DA4811B74852,
	TaaSettings_get_defaultSettings_m69113DF076C4FD27988E02EF1FD1C87EA1AB60D6,
	Settings_get_defaultSettings_m187BEA1D08AA74B7F2AAC6AC0000F6B19DADE248,
	BloomModel_get_settings_m759A052A18CD2922A57BC5AB1DC8AED62F7DE57E,
	BloomModel_set_settings_mC161C80C080FC5623E101E2DD374417DB615798B,
	BloomModel_Reset_m2D58211CB6F21456D058093E30B3D5D44D01A85E,
	BloomModel__ctor_mF166C6353784112A973A26EB88542B3143225E01,
	BloomSettings_set_thresholdLinear_m5D681343998DD1370FF502B96D0CD7990A8843BA,
	BloomSettings_get_thresholdLinear_mAF02ADBEE0AA8D764116FFCA6E02317C44191C55,
	BloomSettings_get_defaultSettings_m85CFAD9DD7D5157D8F4BB6DD441C2B28A8D55004,
	LensDirtSettings_get_defaultSettings_m3417152CB106AD18C874F023EB672FBECD9EF475,
	Settings_get_defaultSettings_mF7B09DA4EC2CFA21A7CE5519F7D147A3154F2DC3,
	BuiltinDebugViewsModel_get_settings_mC783DAF1FDA65C85D33EA53AB8478328C20FAA76,
	BuiltinDebugViewsModel_set_settings_mE4D84909F9F20E90E3F993544C271A965E508551,
	BuiltinDebugViewsModel_get_willInterrupt_mC3263DF49F2D505A38B3CE0911FEFBF208BCF8B3,
	BuiltinDebugViewsModel_Reset_m8CCE4D760D74800EDF3F5D4F91F435502ABE5875,
	BuiltinDebugViewsModel_IsModeActive_m7B201C061869970F7E6A4695D2E24E901EE62CD3,
	BuiltinDebugViewsModel__ctor_mF105B8AE590F6631E06C79CC347B09C87A2B7CF4,
	DepthSettings_get_defaultSettings_mBE9E2BBBF99432BED7B8E3A11C6B0DB891B438FC,
	MotionVectorsSettings_get_defaultSettings_mCF968C1FFA44375D6B6D7049096F59F3F3E4D2B6,
	Settings_get_defaultSettings_m0B1672C6368B880D4817C6AEDCD3EE009E6EAB14,
	ChromaticAberrationModel_get_settings_mC53C47B383BD1B7EE8235EDD9379FABD0CBA05D7,
	ChromaticAberrationModel_set_settings_m41E200B5977BA43ABA907E89E9B1042537DEDC00,
	ChromaticAberrationModel_Reset_m10EA27E5D4B6F5042210AA397310C079D51D93C4,
	ChromaticAberrationModel__ctor_mC064E504E1F0307E5D189EB9A26E80C04737A077,
	Settings_get_defaultSettings_m791BDC0A3BA6328ADB04192E41FE897BBA646B37,
	ColorGradingModel_get_settings_m2EA973DC7AF3BB333EF377C2E565DDF2AFBD0A45,
	ColorGradingModel_set_settings_m77858CD8DA8C0CDA7001AA27D37B080037F8C478,
	ColorGradingModel_get_isDirty_mCE08360C3DF82F85F7461B05FE080CFBD1852421,
	ColorGradingModel_set_isDirty_mB2BEBC2495596F8A7E45B5A91216443983DCAD3E,
	ColorGradingModel_get_bakedLut_mA36059B954731D5EC292927A6CEE8D8BAC2CC510,
	ColorGradingModel_set_bakedLut_mBA683C6FE3F64CB3D93BEF215B6C998FF4FDA5BE,
	ColorGradingModel_Reset_m015E0C3E0A1DFCB0F98F6C1AA2D1DAABA9CA7F2E,
	ColorGradingModel_OnValidate_mE7C9C32AB361E110AA7C2A34B22DFAB46D9A4AE5,
	ColorGradingModel__ctor_m68130B91EE52D2A11F0BA40092D93EDB4F3A5996,
	TonemappingSettings_get_defaultSettings_m020E6A42A1677C1396D70795E85C1A69E47BA6B6,
	BasicSettings_get_defaultSettings_m24138DA6AD05E42A0F4FFD778F19162240398986,
	ChannelMixerSettings_get_defaultSettings_m7A8E8EB1A356EF330B897AC1FDD12554484C0A61,
	LogWheelsSettings_get_defaultSettings_m145CBBD7D33AD3D97304172C23B418BE37393C2E,
	LinearWheelsSettings_get_defaultSettings_mD917AC276172EFD4831502D72B8825CAA4F698C4,
	ColorWheelsSettings_get_defaultSettings_m826EE33BD2BFD73210D8950D645031157013FE31,
	CurvesSettings_get_defaultSettings_mE69800F815D69BA509C8FF03ADEAB1C3CFF582A4,
	Settings_get_defaultSettings_m8A9FD147177C9F669E8149E0BEF03201C449FEF1,
	DepthOfFieldModel_get_settings_mAF2B9D62F10BFB6A49DACE1A855CB35B50D468E1,
	DepthOfFieldModel_set_settings_m9650C0CC437FE4F02040C18C66749A333515EEE3,
	DepthOfFieldModel_Reset_m8B50D23681715ADBA273D8A4B9AB4372438EB522,
	DepthOfFieldModel__ctor_mCAC4870977FE18649F8AF0D79F3B24569A6026A9,
	Settings_get_defaultSettings_mABFD370ECDB30AF177A0D98B27D76BCFD38E9B47,
	DitheringModel_get_settings_m8D23BF6769B067C7ED743CD644C3B4AD3109226D,
	DitheringModel_set_settings_mFE8DA99430E47BA302EDBC94DD59B3899FD4FB14,
	DitheringModel_Reset_mA803DE0E46B1D9B6E844E38D1B7F9D4E8D1DF74F,
	DitheringModel__ctor_m7F55D82228AA4AAA6FE23D275B38FC0F841F1BE2,
	Settings_get_defaultSettings_mBDFB1447E0EDB6E476EB51D2A5EC758749A4D4C8,
	EyeAdaptationModel_get_settings_mC49DE55E13916C1808242082918EAF36A63AF4E9,
	EyeAdaptationModel_set_settings_mC1533BB0C826E05964214ADA15BA7D2E655FD449,
	EyeAdaptationModel_Reset_m1EAAE64EA5B66F0C3BC9EB2662005B23D213B13C,
	EyeAdaptationModel__ctor_m33CEFCB575539035C72CA3A766BADFBB819E1D18,
	Settings_get_defaultSettings_m2344CFBEAD562CEBC2691DF2F31544D2D1AECBAB,
	FogModel_get_settings_m1329E6494ED6C23AA93CB2CE59CACCD7AC440BE7,
	FogModel_set_settings_m65364F86D54F14BD081C39D39AEA32845FAC973D,
	FogModel_Reset_m5CA0A31F82E608D4F0F724D2EA102B0E271725EC,
	FogModel__ctor_mFC079C0889935D1BCABA42D2C3ECAB80CC86AB37,
	Settings_get_defaultSettings_m3F80C1208F2B493034B966175DA19D8BCE3B9001,
	GrainModel_get_settings_m0F0B56E8699AFB526BDF0C294934CB3A9F686C38,
	GrainModel_set_settings_m5B93A60B5D5103E5E2F60C09C1725EC5FB0415B6,
	GrainModel_Reset_m9F0E7707467F681C523B694B7139B65BC8E19199,
	GrainModel__ctor_m674882DF0D2FA7158846587A100E268CBB666DDB,
	Settings_get_defaultSettings_mE53750093B8A4A26FF705D456758ACC6AD7EEA56,
	MotionBlurModel_get_settings_mD483F1C4CBE78CA212AF73A8334F79510090BC5F,
	MotionBlurModel_set_settings_m3C8D69A114CF2FAAB9E05ED9CC8497BD163528CB,
	MotionBlurModel_Reset_m09AAD87FC9997C56E9497099DE044BAF88A65421,
	MotionBlurModel__ctor_mCD82263955130CB0C9070C29CA23F22810FAA5DE,
	Settings_get_defaultSettings_mD3A95AF0C716D2864CE8EB106FE41F08289841C1,
	ScreenSpaceReflectionModel_get_settings_m18F1FEFCFE91B7C24F1B883A1F4A8A793E6F2207,
	ScreenSpaceReflectionModel_set_settings_mCC78EA8D34FF6CD9E1970A84054406630BDE560D,
	ScreenSpaceReflectionModel_Reset_m31181E25C93827A9213B31D7F5E8FD1450F4CBD7,
	ScreenSpaceReflectionModel__ctor_m638D25056004BAB4C3D8AE46CAD0FFE861F48F15,
	Settings_get_defaultSettings_m13216005F7A1ACB0998EBED272CA93D400CB3BCC,
	UserLutModel_get_settings_mE109CC3AC743DB5B068CFF1088592D8A4936935D,
	UserLutModel_set_settings_m9CF084A35B9A5D3FA56E79FFAB6273F477BE3010,
	UserLutModel_Reset_mA83F913DE67FBED1483AEF3A44683678DE87731D,
	UserLutModel__ctor_m3E0004D52613BFF6ECA0E03DC79D922F3A6213B5,
	Settings_get_defaultSettings_m49DC2107075E1ECC4561EC906DBF69B5FA87980A,
	VignetteModel_get_settings_mA0B3B522622D2FC247E9A2DF11A1AF78A3BEF8F6,
	VignetteModel_set_settings_m6E295AF7A082C2A092AA29E14F1DD02A3D941DD4,
	VignetteModel_Reset_m17F63609C24EEDA2F1A4477A3629CC2E6523A92F,
	VignetteModel__ctor_m86430F872F45C41B1241D3F281DD90FEA93484D1,
	Settings_get_defaultSettings_mA0F09C1FA146892325FF02BE1859A7E117BDA9C7,
	PostProcessingBehaviour_OnEnable_mCC96A9D7BB473E3F8C11D9280204B95748C06467,
	PostProcessingBehaviour_OnPreCull_m24D068B04C5DECDA0FFFB08E929C61942AF91C5F,
	PostProcessingBehaviour_OnPreRender_m946BE947393B10A7A6A8BA12481D35142078861E,
	PostProcessingBehaviour_OnPostRender_m497329C185DA9EB9B382618733FFC70631262459,
	PostProcessingBehaviour_OnRenderImage_m0C0A56A6818A443903940D0557FEBFB29590A4B3,
	PostProcessingBehaviour_OnGUI_mA5A9BA4F5503BF6F81C1C8E45C5D0CBAA32117DA,
	PostProcessingBehaviour_OnDisable_mD592853CC86C4434AC5552016DFE248278BF0F3A,
	PostProcessingBehaviour_ResetTemporalEffects_mA355B277D80B827CF5A7063EC126664D41F6AC28,
	PostProcessingBehaviour_CheckObservers_mC7D9C361FBCB6057735CEF27775D54E708EDB3DA,
	PostProcessingBehaviour_DisableComponents_mCA4304C5D91742B94E55E1C141C59426294C36F1,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	PostProcessingBehaviour__ctor_mFCFE406D007166C2D90597D7B21546FE57C4A619,
	PostProcessingComponentBase_GetCameraFlags_m0965E0C24C420F1883F838C468743696BC242175,
	NULL,
	PostProcessingComponentBase_OnEnable_mDA6132C51C900B51AA1E9F5EEB9BA791DCD0A380,
	PostProcessingComponentBase_OnDisable_m339B13F2569EFECDB14DA03FBE4553D35EC28EC7,
	NULL,
	PostProcessingComponentBase__ctor_m6973C9B7B13DF7B63B6E24F22A1465C4058AAA1C,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	PostProcessingContext_get_interrupted_mD4B924B1F05E49F1A9A6827163E371A45C9720A5,
	PostProcessingContext_set_interrupted_m3DBDCA425B59F1582D272385FA4C3799FD5DA2A0,
	PostProcessingContext_Interrupt_m224A982174C1C6108E2A2A037088B429B92D633D,
	PostProcessingContext_Reset_m91E248448B7DE654B5C9017AD9411392D408B274,
	PostProcessingContext_get_isGBufferAvailable_mB06B5DC61A22CDECB02F28FDDF082A4D613F73B3,
	PostProcessingContext_get_isHdr_m3DEA2A02460B55B5E49938CAD7AB55EC4588BE3D,
	PostProcessingContext_get_width_m47E8EC1F6D88E4DB9713328878FAD8B315E60E4F,
	PostProcessingContext_get_height_m9D76FBA46F89929C331939910D5EFA5C11720CEC,
	PostProcessingContext_get_viewport_m4462CF303823BDD387DFBE73A7482AC19B09E88B,
	PostProcessingContext__ctor_m4D3C87FA5CA436C62B4703150E619F0B3C3BC84D,
	PostProcessingModel_get_enabled_m3C190F4C9B03449BAC487ECE0A03E3F524BF5E95,
	PostProcessingModel_set_enabled_m8446050CDF2021DAAA7E785989CDF0274E2CDF91,
	NULL,
	PostProcessingModel_OnValidate_m05B5A334ABE183A6E543A675A12458E44DDAB398,
	PostProcessingModel__ctor_mD66941A8E843AEC57572946AB3F9A66B7B17351D,
	PostProcessingProfile__ctor_mDDEFF2C43547D23A3DF77EDD787F52F087DE8BC3,
	ColorGradingCurve__ctor_m1F50481353D7421A25F249B584E0901FD7E92383,
	ColorGradingCurve_Cache_mDF20D10D83B9051AF369C4C06F9B8F51B1ABC2F8,
	ColorGradingCurve_Evaluate_mC8FA46F66E495AA80618BEFCFAF026D06DD93F5B,
	GraphicsUtils_get_isLinearColorSpace_m7ABDDB13E42678E446B40014756BA3676C3DCEC4,
	GraphicsUtils_get_supportsDX11_mD23FAE3A45F359483A5EC8FB0BE0DBDDCFE18BA5,
	GraphicsUtils_get_whiteTexture_mED3B6448597490C8D9DA690D10A14731C496A82F,
	GraphicsUtils_get_quad_m759AD9D70DDD5FEFB815926855C749FF1D954377,
	GraphicsUtils_Blit_mD74B897D1A76BB3A707FEC4734D21C174CA7A227,
	GraphicsUtils_ClearAndBlit_mBBCC2133ADCA1C7799E79261F35A025B8737804C,
	GraphicsUtils_Destroy_mC81DD13284816A2F5F2D10CE9D6850E197DAC550,
	GraphicsUtils_Dispose_m3669123C53863B048A91CA8CEDEEB054233108F6,
	MaterialFactory__ctor_m7395EAC727BBC78DF1013999E763FF810EEFB0FE,
	MaterialFactory_Get_m2C8DEF060705F34246D4BFA2A758647B7005C9FF,
	MaterialFactory_Dispose_mEDD1D01A1B06F228AAC07B915CC7EEBC7CC52668,
	RenderTextureFactory__ctor_mF119EA0BBEDC89E81B7862174702BDBAC360B6A4,
	RenderTextureFactory_Get_m30A83EBD7B33051FEF9F47A1ED9965C0C0E3D8D7,
	RenderTextureFactory_Get_m5E8066AD53F5D57E2DBF503198A7AA93EE86BA91,
	RenderTextureFactory_Release_mE496D1BC8CCB04A6E0A385BCF39614FE06D9B541,
	RenderTextureFactory_ReleaseAll_m89CA9981D734B168A7E127EE68CC88C45635A268,
	RenderTextureFactory_Dispose_m72D61911A920C073AE20175D140AC20CA786B424,
	CustomMicrophone_get_devices_mEEAA2FC926FE7BADE2B6FA8FB614BCCAC006739E,
	CustomMicrophone_Start_m26B4828589CE729AB1E0FDC1B114593A239436F9,
	CustomMicrophone_IsRecording_mDF34F52A65BBFE0567B5ED6860DBA51DB159C4F6,
	CustomMicrophone_GetDeviceCaps_m37AF24EA8FA2D21D2230FBFCCA555EA3F4956593,
	CustomMicrophone_GetPosition_mE7FFB4D8D1FF14228C6B32933A37A4ACEF3878AF,
	CustomMicrophone_End_mFA27CCFF1645B191EFB2EDAF1899134BCECB5396,
	CustomMicrophone_HasConnectedMicrophoneDevices_m6FEE22CF63BC91DF3B34B25BA74A0D14AB5CA533,
	CustomMicrophone_RequestMicrophonePermission_m394A9E7B25D95481A5F4E094A9BCFA328062E291,
	CustomMicrophone_HasMicrophonePermission_m7D44CAC92F1764F2216410F07A2EBB197AAEE59B,
	CustomMicrophone_GetRawData_mC54002A61193D6E82917AB7992884B0065A9ACCF,
	CustomMicrophone__ctor_mC17688DF04BC0D1476995174C3196EB1459753FE,
	NetworkConstants__ctor_m3A7FCEF20B7D00B006EF4CD2F800D7D1F69FD7A6,
	NetworkEnumerators__ctor_mE94B83B891FDC10B06282634BE5E5DFD2786CEA4,
	NetworkRequest_get_RequestId_m2969362499CE21CFFDAD1A95EB5760423AA017EA,
	NetworkRequest_set_RequestId_m0D8B8DD657B1B0F6A132E745291D4895DAF14F95,
	NetworkRequest_get_RequestType_m92B34AB77AA059D5D61B90B14AC3BA6F7662E8EC,
	NetworkRequest_set_RequestType_m1BCC9B85EFC4565332BC73747E28F6B5359E353A,
	NetworkRequest_get_Parameters_m827091DF1E505089A4A6DF0F064588615AC8A0F3,
	NetworkRequest_set_Parameters_m2A85FE6E7E3D36EEDB37CCB233A4031D77B76A14,
	NetworkRequest_get_Request_m9260D4CC16EE4DB4DC104312B9E45164C8889C23,
	NetworkRequest_set_Request_m0E3B27CE341C732AC8EC44C0780722BBD3FFA441,
	NetworkRequest__ctor_m31C7215744A319183D9E46A55E161BC66AA0D752,
	NetworkRequest_Send_m4CCF7EADB0F112305647A5DB7E3E415A6AC18308,
	NetworkMethod_get_isDone_mFBE895E18DA704AB8291D3638FF2A836D7B9E5E9,
	NetworkMethod_get_text_m336C07DA720B811A67EE18976EE1C97130FE81FC,
	NetworkMethod_get_error_mCDBAD6808249F97CBC182CA2517D2573F23C2B2F,
	NetworkMethod_get_responseCode_m9F34244FBA767D456084E7D9C69D8B138B53E1AC,
	NetworkMethod__ctor_m7307703483824B5C3AB7BA5761AC062568BC5D41,
	NetworkMethod_Send_m1B217BC61892875766A1CF896C63A8809FFA8ACF,
	NetworkMethod_Cancel_mA98BA7CDA4CF51BD22670FAB6BF4EAC581B9058A,
	NetworkResponse_get_RequestId_mEEE1C0BEA1DADBDBD2614BFD5B34A45B2234975C,
	NetworkResponse_set_RequestId_m6304592B381F6B58FA74C633535E1B037F0B29A1,
	NetworkResponse_get_RequestType_m23F0A001C16B1FD8DBF1A48A3983174575AF2154,
	NetworkResponse_set_RequestType_m3D1122F5BC9F7B325D6BD545D92A1F42561D4C8B,
	NetworkResponse_get_Parameters_m44A620E07F69E9496303C4F094B4B6299CB1C8DD,
	NetworkResponse_set_Parameters_mF259D0ED405C34361BD71B63160FC6CEEC04D03C,
	NetworkResponse_get_Response_m24C3DF5E8E670A234FD0B20B2C9008C32FCC463F,
	NetworkResponse_set_Response_m9F4EA0AD57BCB9F3DB8EB323190C2ADB2D82CF5E,
	NetworkResponse_get_Error_m3AADCC997DE7BEDD7B75FBF59861495C8CB4C75A,
	NetworkResponse_set_Error_mEE96D159000568C6D5FC60448C3CC1CFAD689A9D,
	NetworkResponse_get_ResponseCode_m666C1ED7EAFB0B40628367FD479D16988D126472,
	NetworkResponse_set_ResponseCode_m69921D1A553F087E40900D6F8BA086D0BE017DBE,
	NetworkResponse__ctor_m3D04F1D38ACEDEE4E5C7FEAD4002B814CE1FDB38,
	NetworkResponse__ctor_mA8A5ACAF75FBAA1F49D9A6D631B9B181B4EF6265,
	NetworkResponse_GetFullLog_m8494E25A7B2BA6C3D9F1EB1B02871634B5CA4602,
	NetworkResponse_HasError_m96B9A9BD9DBF44E6DEA071DABA48CF4E5FD5E38E,
	NetworkingService_add_NetworkResponseEvent_m3B657FD26DA5F4F9EF1FFE1EC2CFED9A3A9FC0FF,
	NetworkingService_remove_NetworkResponseEvent_m7A7ED58D0632D2331F0092373A4B5C3F40B380F6,
	NetworkingService__ctor_mB64F800056EC196EBD2F28D4FED8D1BD2C178B01,
	NetworkingService_Update_mC443AB669DB96364456D0EA8175DAF8F076C7592,
	NetworkingService_Dispose_m92DD5A5604CDCA99E3B304A33480CCBAC9B00518,
	NetworkingService_SendRequest_m3B683689102827A62E7871DC5B1D93823FAF377F,
	NetworkingService_CancelRequest_mE14B7E1FBE41CA8BD3482DBA6B28FA4BBFCAE15E,
	NetworkingService_CancelAllRequests_m0400E62D6A8CACEEADAA9613B3DEC1A24E5105F0,
	U3CU3Ec__DisplayClass10_0__ctor_mB13B700EE7045FC692BC7A44EDCCCB144FC76693,
	U3CU3Ec__DisplayClass10_0_U3CCancelRequestU3Eb__0_mE0E28899EC6168A226351180E0DBCC3B2B1C6997,
	NULL,
	NULL,
	NULL,
	ServiceLocator_get_Instance_m4DA7D0F5488286A697049681E7AD236D65697F26,
	ServiceLocator__ctor_m9096B1CB79055683681F21515B5B0BB26CC993EE,
	ServiceLocator_InitServices_m1F91E17DAD36EE14FEBF9683378F5BA7269FC183,
	ServiceLocator_Update_m9180FF047442FD8551CE4ED4E49E8CB1CB68DC6E,
	ServiceLocator_Dispose_m9BA5A4816EE5318F7FC085A6EFC71ECFC8ACD45B,
	NULL,
	NULL,
	AudioConvert_Convert_m0BE689ACE77D444106FA6A8AAD1DCBE28CDC84C9,
	AudioConvert_Convert_m9E01A731D306D5B60B209A6CF68E56BFEC653231,
	AudioConvert_Convert_m61A418186A735DD389B87AF58947CB13394EDE0F,
	AudioConvert_ToBase64_m38DE17AB682990B87286305304D89ACABCC9B43D,
	AudioConvert_ToBase64_m1EFA2D1903D3310E98AA150093FCD6B434EC178A,
	AudioClip2ByteConverter_AudioClipToByte_mF0E3FB11179718B0FCD8462437EBF4EC27A7F3DE,
	AudioClip2ByteConverter_FloatToByte_m883AE0C1BC06AC5F0CF011DAA6043F8F3C0E58B1,
	AudioClip2ByteConverter_ByteToFloat_mF9BE5138C719334C055ADF0339FA1C5C2571B232,
	AudioClip2PCMConverter_AudioClip2PCM_mE4199111918BB7A3A857C02882906312C7629E94,
	AudioClip2PCMConverter_TrimSilence_mDE33833B305C3A199EFA2880F2DD9EE5809551BA,
	AudioClip2PCMConverter_TrimSilence_m8F22B63A57886111D474CCE709674131F17C9BB7,
	AudioClip2PCMConverter_TrimSilence_mA2C9FA82304F790ED8DF2BAC5FF2A2795CB179F2,
	AudioClip2PCMConverter_CreateEmpty_mD0D69BC5DF6C7FF7311E4C5842E28FC1DA54637D,
	AudioClip2PCMConverter_ConvertAndWrite_mFEB9E5466D92517793273EFC23272EE3BAD53AC8,
	AudioClip2PCMConverter_WriteHeader_m584BE7D22917ED5A27406EB49A6C9BF4185AA94C,
	AudioClipRaw2ByteConverter_AudioClipRawToByte_mE8D083E7C2F15791C70052801BCE7BF2891D8C24,
	AudioClipRaw2ByteConverter_FloatToByte_m965DAA7503CAECF341854F5223DA8382FA637BB7,
	AudioClipRaw2ByteConverter_ByteToFloat_m402A149029C53E2BC6D682604DC1CF0E6944F5AF,
	AudioClipRaw2PCMConverter_AudioClipRaw2PCM_m1B0E998EF42828434DAA8F39C06BBBC791789078,
	AudioClipRaw2PCMConverter_TrimSilence_mBE10D2BB3F13929BD071516470A905573B76D5CD,
	AudioClipRaw2PCMConverter_TrimSilence_m67FF8C2425084AA326CBFE58482595ED61869A68,
	AudioClipRaw2PCMConverter_TrimSilence_mFDD584F00CC92E7EFB874C8A8D15582E672FDFEB,
	AudioClipRaw2PCMConverter_CreateEmpty_m50E48DCD192C8499D10F1A8F228922BC582CBE19,
	AudioClipRaw2PCMConverter_ConvertAndWrite_m68E63BC075964FD909862BD55720E469711E6DF8,
	AudioClipRaw2PCMConverter_WriteHeader_m0AC76C27DDD7ADFFF7A8E268E75D2C0424A41278,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	MediaManager_add_MicrophoneDeviceSelectedEvent_mBDCA782D22DAC4CE129645DDE109898042128810,
	MediaManager_remove_MicrophoneDeviceSelectedEvent_mBFA0FE5113681EDDBD7DB69D66245A4A273B8C79,
	MediaManager_add_RecordStartedEvent_mC72A2FE6FA96D71CFF84E114B09ADB73F8EFC8CD,
	MediaManager_remove_RecordStartedEvent_mBE7A3971DF8F0597F4F1835C566B1B907A404BCD,
	MediaManager_add_RecordFailedEvent_m05DC2F6508C62813228E2CC2B91BA55F988922B3,
	MediaManager_remove_RecordFailedEvent_mDFB5AB7CA2920CBE578F8C62498A571776E42FEE,
	MediaManager_add_RecordEndedEvent_m7A609FA404581B7FCAF691E965049568AE747433,
	MediaManager_remove_RecordEndedEvent_m922036246B3FCFBD2A00F3A9EDC9440607568B89,
	MediaManager_add_TalkBeganEvent_mC5EB58C994A4DAC47016563A0556D74E1A882F85,
	MediaManager_remove_TalkBeganEvent_m48A188214C58E555D4DC6064B69EB747E4BF6580,
	MediaManager_add_TalkEndedEvent_mC171C644C12332B02FB29CA9A6B48C9A6567C1A0,
	MediaManager_remove_TalkEndedEvent_mD8AD5E14CE594C03C68B177F335309BF7B5CB18A,
	MediaManager_get_IsRecording_mD4DB51FEF2296BAA2BF34401B77B247E94F11459,
	MediaManager_set_IsRecording_m12853A0BB27B8D46F10ECEB8DD27D4B8A01D7A4F,
	MediaManager_get_MicrophoneDevice_m3EC3EAE913F75D5EB030BC401172D0817EB45166,
	MediaManager_set_MicrophoneDevice_mEECD1C79A964BEFEEEA0F363161FA1A1CCDD9B4D,
	MediaManager_get_LastRecordedClip_m0C9C5C85BAA1C538D3B109BDC99CAFECB338BC3A,
	MediaManager_set_LastRecordedClip_m722C21F3BF8D4486B3083006AA58EB5E782CCF5C,
	MediaManager_get_LastRecordedRaw_m2DB631C2A3682C25A127267F0620493A56965C1F,
	MediaManager_set_LastRecordedRaw_m36378B318B55AD4FBF9417B872ED2A8E9401AAC8,
	MediaManager_get_DetectVoice_m2DCBA429CBEBB92FE40C4A6215692D2AF9F6A6F2,
	MediaManager_set_DetectVoice_m42358364B88649FE4B5771829823E231172BFED7,
	MediaManager_Init_m022FF4A330C69E356562E38073B29B7B77F75089,
	MediaManager_Update_m7AF8A5417187EEF51FE74901A4F856C415F8DDCE,
	MediaManager_Dispose_m5EEC11DF681495D599AC09E1818AD63229E8A123,
	MediaManager_GetLastFrame_mC4958E5AA601302570BA2773B0C92EA11C02F04F,
	MediaManager_GetMaxFrame_mC1E2B2D0DAA71A2C181253C646D59B52BBECD22C,
	MediaManager_StartRecord_m9AC386FF952FB68199E67FABE32BA3862F25BDA1,
	MediaManager_StopRecord_m5497CBF6598749697303F9C84F57AD33B9F786C9,
	MediaManager_ReadyToRecord_m6333FA7219570892BC7FA32A15673732B6680D8D,
	MediaManager_HasConnectedMicrophoneDevices_mAE636DFBCB48938C43E7A63852BB2CFD32B6287D,
	MediaManager_SetMicrophoneDevice_m17655C2DA8B3C7576F3D1AEE925B995324C23ECE,
	MediaManager_GetMicrophoneDevices_mE7B5FEB23C6717F3837CAD06C02E3609C2108B8D,
	MediaManager_SaveLastRecordedAudioClip_m3DBD62B88E76E0F1E987E1106281BCE46CB43130,
	MediaManager_OneTimeRecord_m03ED788E4E450077CB431904429F1BF282FE5CBB,
	MediaManager_HasMicrophonePermission_mA5378EA715F72133A85F693AE1F6D8120D7AD86D,
	MediaManager_RequestMicrophonePermission_m8BBA9468404C1EEA8CACF326CB3A41E93E7AF12A,
	MediaManager_AddAudioSamplesIntoBuffer_m59879B93EA5C0F8F922157CB83DE86513DE74CC6,
	MediaManager__ctor_m923D56FD2923AA74F9090974B66C7B1E7E35A08B,
	U3COneTimeRecordU3Ed__60__ctor_m767730738666BEEF59C9947847EE22EC3A7BA61B,
	U3COneTimeRecordU3Ed__60_System_IDisposable_Dispose_m10962C45219BFCCD43762918AF56AFC6BA1254E0,
	U3COneTimeRecordU3Ed__60_MoveNext_mDBC85F3695D5AE2B81BB234A85BDFF08F4CC8BC3,
	U3COneTimeRecordU3Ed__60_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m1C168B46157F5A49E172A6FC4530784B90F2421C,
	U3COneTimeRecordU3Ed__60_System_Collections_IEnumerator_Reset_m612582A550C3B59B7362328BB04C2EADADA40038,
	U3COneTimeRecordU3Ed__60_System_Collections_IEnumerator_get_Current_mBC123C49AAC9ADA5217ED8AAB1D8BBCD9C30C93D,
	SpeechRecognitionManager_add_RecognizeSuccessEvent_mBB07C04FB6E048E4EBB07678912D5E9309D461E6,
	SpeechRecognitionManager_remove_RecognizeSuccessEvent_mC541018CDA519C608F43F5A104BB3CE48B3AC6CE,
	SpeechRecognitionManager_add_RecognizeFailedEvent_mAA5AA910551FBA11921D5D3BAA7B397A0CDDC872,
	SpeechRecognitionManager_remove_RecognizeFailedEvent_m95FAB18CC327307098909FE5642982A3C7E79E01,
	SpeechRecognitionManager_add_LongRunningRecognizeSuccessEvent_mF6315C0537F004A93CF3F653CAFCE996C0A63BE7,
	SpeechRecognitionManager_remove_LongRunningRecognizeSuccessEvent_m0F4B4D498593548A1426D0FFC1D165EF8982995F,
	SpeechRecognitionManager_add_LongRunningRecognizeFailedEvent_mD23ECBD99E23BB0C5C6476F165C424FC5F6283AC,
	SpeechRecognitionManager_remove_LongRunningRecognizeFailedEvent_m6A917296094580EC8ED6CE85EC92F8E0D63D2AC8,
	SpeechRecognitionManager_add_GetOperationSuccessEvent_m9F7EE30DD1204967982044D6789F59993245AC06,
	SpeechRecognitionManager_remove_GetOperationSuccessEvent_m590D09EB10FD81B7B6F865CDE0C529364AF88DAD,
	SpeechRecognitionManager_add_GetOperationFailedEvent_mA6B184F8F5C6D50423854FC7EC60236D2A29319C,
	SpeechRecognitionManager_remove_GetOperationFailedEvent_m32CD1833E92C0308ED5FD5E3A5FD8700A9937A55,
	SpeechRecognitionManager_add_ListOperationsSuccessEvent_mC03B5AD32C7C6BDDEB7817D4F5C8C567B3EAC1E7,
	SpeechRecognitionManager_remove_ListOperationsSuccessEvent_m29594F33733F1206750E470DD81BCAA24FE7F7A7,
	SpeechRecognitionManager_add_ListOperationsFailedEvent_m3E9F14455C47B476A3CCEF6B45D3656B7B7DA122,
	SpeechRecognitionManager_remove_ListOperationsFailedEvent_m0EECC71F4ED57EC61AB758012C1F8D987858C984,
	SpeechRecognitionManager_get_CurrentConfig_m114549A51F09DFB5F0B5F79531B2027E11ECAB93,
	SpeechRecognitionManager_set_CurrentConfig_m6330B2C55F296734ED7176892C31ABF98754B5CB,
	SpeechRecognitionManager_Init_mF63AE77D5AF0D4002E71B309D4C4E82F8A134198,
	SpeechRecognitionManager_Update_mB5AC5ADD766E6ACBA5CD0DAD9BCC6E70D3D7624E,
	SpeechRecognitionManager_Dispose_mEA667B1B9DC62BC648E5F86616FF5272A1FDDAD1,
	SpeechRecognitionManager_SetConfig_m7F5208473F3B97420BE64257DC8A1F4674B909A7,
	SpeechRecognitionManager_CancelRequest_m2766F4A2EF2E837229B1461DE5DEF60CDD51D56C,
	SpeechRecognitionManager_CancelAllRequests_mFA10F11C015E91FF90071BF7EABC19DF5A749EC8,
	SpeechRecognitionManager_Recognize_m9576C4138DBD259AA8B9F137D06819477BC73251,
	SpeechRecognitionManager_LongRunningRecognize_m2D679C310C2B79A19C4F3EAC468DFC75585A99C1,
	SpeechRecognitionManager_GetOperation_m3B6650BD52A2707477EE17CF14E18FB23EBFBF2C,
	SpeechRecognitionManager_GetListOperations_m7C94087AF5BD417EC07C6317C18932A477DFA51A,
	SpeechRecognitionManager_GetAPiRouteEnd_mDB855BF6FD6353655EA83E41D5888939EF26F0C7,
	SpeechRecognitionManager_NetworkResponseEventHandler_mCCBD6405806A8F3B64DF4B2D587642559D144F3A,
	SpeechRecognitionManager__ctor_m46D0F0008C3E55D48D1C22942F44F150B22DB978,
	VoiceDetectionManager_Init_m900E6115BF1FA778E1CCF22307B28EF8A31C4C12,
	VoiceDetectionManager_Dispose_mA367FDEC22572223DF92A2CA9BE9A89BD2CC0DDD,
	VoiceDetectionManager_Update_mACF28F72EB49A9D1DC68C9D56CB867AAA09CAB5F,
	VoiceDetectionManager_HasDetectedVoice_mD792A747B758C76391CFA94415E7012DB67AAD2F,
	VoiceDetectionManager_DetectThreshold_m894274D88B184B39198F0D521B083EE85C4A81B8,
	VoiceDetectionManager_ProcessData_m38F1C2C592D09EA264C4884AA08821668923069D,
	VoiceDetectionManager__ctor_m7B71327982A1DF9AC89C9CFA4836B6B1CF98F49A,
	VoiceDetectionManager_U3CDetectThresholdU3Eb__8_0_m95A05494CEBE71D34B5899F4A1BDA4F8681F8A1A,
	RecognitionAudio__ctor_m300275B0F2EC826B2A44A2985A60555F829F8683,
	RecognitionAudioContent__ctor_m178C23D282AAF0412F17D6BE96F425685EE480AD,
	RecognitionAudioUri__ctor_m5243CCEACF66C41EAFDBD5ED3095FC1BBF23DEE6,
	RecognitionConfig_GetDefault_mEAD6AF2ED364A2692F9376465D2BEBBA20A24952,
	RecognitionConfig__ctor_m6FC78B524C5F9AEE5166E1E1E89DFD6763D76F8C,
	SpeakerDiarizationConfig__ctor_m972794F1A1B9E96CECB972E1E6EC4EBD9E40AE98,
	RecognitionMetadata__ctor_m935DC66A2E53BD77C842E2606359003C5C544BF5,
	SpeechContext__ctor_m42EB93FD08B24EBA0E264DA2EC803DF6854C1161,
	WordInfo__ctor_m8587CC5EBB8401D0CBC36CA5907988B68241C433,
	Operation__ctor_m4F5254EB459339F7E0FC69684DADCE200E8FC874,
	SpeechRecognitionAlternative__ctor_mB63CED0286743B98B848FB211C7924B3C932742D,
	Status__ctor_mF7C4F5CFA15C88BADCE5BCAD2DFF3E3E08CA64BA,
	LongRunningRecognizeMetadata__ctor_m7A5D0B5DC07BD88C2458D0254C8835B4B2BBA94C,
	SpeechRecognitionResult__ctor_mD1330B8865C543F36F66F82FA6E5B0CE8740ADF7,
	RecognitionResponse__ctor_m0CCF1C2BBF328272E9A1E6F191F56A6B448895A8,
	ListOperationsResponse__ctor_mDC4160855C26FB648A33FF91DECE6604323C44AA,
	GeneralRecognitionRequest__ctor_m7DE9899BE1807545C07C2D79DE40FD210B0B5E28,
	Config__ctor_m7E54D5E7C4DBFAF085C859C876DDCE440054689A,
	Constants__ctor_m3E42F34691D5E1DCB6D69821252998F0157633EA,
	Enumerators_Parse_m78E1729FB02BFA264E7DEF16868D50926EAD9E58,
	GCSpeechRecognition_get_Instance_m992BC8241EBB7B35B149B2A3F18B5F9ECBEE64B1,
	GCSpeechRecognition_add_RecognizeSuccessEvent_m176E51F4DEF849A46FBA13C55080741DDD71B1EF,
	GCSpeechRecognition_remove_RecognizeSuccessEvent_mB65C27268776BB3A214C28F1AF356A22DD4F5704,
	GCSpeechRecognition_add_RecognizeFailedEvent_m9D16C98DE295EE5C7525DCFBC0858923E26EDBED,
	GCSpeechRecognition_remove_RecognizeFailedEvent_mB1A386C34CD6EBB13C0E0E8D6B23A3C5F69727B8,
	GCSpeechRecognition_add_LongRunningRecognizeSuccessEvent_m8600A85EB72F51C6437ED641C6E96EB3F3B1002F,
	GCSpeechRecognition_remove_LongRunningRecognizeSuccessEvent_m27182C58E5E962B4EF3EFE115DCAC9BB6849AB59,
	GCSpeechRecognition_add_LongRunningRecognizeFailedEvent_m3FC5A7B37115C02C310601915A6985CD66E1007B,
	GCSpeechRecognition_remove_LongRunningRecognizeFailedEvent_mF487C6389C824DB71F77394259911FA93B6F6441,
	GCSpeechRecognition_add_GetOperationSuccessEvent_m14BD1A06A76F112D4845E133D13F9EB84D2B9E3B,
	GCSpeechRecognition_remove_GetOperationSuccessEvent_mAF98F2D84B4D44CADFFCFAF55546A166F76B937B,
	GCSpeechRecognition_add_GetOperationFailedEvent_mDCC71F0C76903666A814311D53BB80EE5B7EA04B,
	GCSpeechRecognition_remove_GetOperationFailedEvent_mBF3C6EDE9FDA16EA677CFB351F9B8F89A64C8615,
	GCSpeechRecognition_add_ListOperationsSuccessEvent_m56D1CBB30E0A16969C94919CFBAC0D11FED45713,
	GCSpeechRecognition_remove_ListOperationsSuccessEvent_m422EA8BAD459BC74A18BF5DC7722A129D9DD46B9,
	GCSpeechRecognition_add_ListOperationsFailedEvent_m2626560FCC9830C0D1138124EDAF995B84A49296,
	GCSpeechRecognition_remove_ListOperationsFailedEvent_mFCB729EAAD791931C2334249FFF0F693AEC3A3EF,
	GCSpeechRecognition_add_StartedRecordEvent_m9B107FAA56BB6401706279C11D9798ABA5B0AB96,
	GCSpeechRecognition_remove_StartedRecordEvent_m02579D1CC27D76539B15185A1ED2C735F53D40A0,
	GCSpeechRecognition_add_FinishedRecordEvent_m479FDEEAFDA462934F06EE0B0697B9AF795795FA,
	GCSpeechRecognition_remove_FinishedRecordEvent_m6C8DF51361C45C2C811C684B05C17FFFC20CB159,
	GCSpeechRecognition_add_RecordFailedEvent_mE8ED5B3FFAAB782B50453D5F42D1B590B5CF6352,
	GCSpeechRecognition_remove_RecordFailedEvent_mA9C7E5052D47E93DF3C60ABFFFAC5D629D936932,
	GCSpeechRecognition_add_BeginTalkigEvent_mD40B76A1DB24B94CD62AE71934B6516F15D3B780,
	GCSpeechRecognition_remove_BeginTalkigEvent_mD2456188394CD9A04DB429E35951A5DAD5319C52,
	GCSpeechRecognition_add_EndTalkigEvent_m7CE4BA15142DB0FB0BA351A5C9FD12A5F1DB7A29,
	GCSpeechRecognition_remove_EndTalkigEvent_mBD1462A1303BF03C098BF477B4F71A5750F33C23,
	GCSpeechRecognition_get__IsCurrentInstance_m8D27479DE87A48A6E5981FE42BF83749D2BE6118,
	GCSpeechRecognition_get_LastRecordedClip_mA447B0E2C3A18DD515F3E54A325A6374DE7BFDE8,
	GCSpeechRecognition_get_IsRecording_mDFB40CFA9B9B605B5C943E0A88A9C509E1BF7CB0,
	GCSpeechRecognition_get_LastRecordedRaw_m3547ACBCF52A2DD93ED1114AB8CE0221A438D2C6,
	GCSpeechRecognition_Awake_mA2FA7B84FF5BA9257B09C02BDE3A8D85358EB8DF,
	GCSpeechRecognition_Update_m9EF09B13D7B11BD88E124B98FC55BBB7FA1974B1,
	GCSpeechRecognition_OnDestroy_mA6BBA4D8E9DC3650C6F9A6B88AF7A2995A05B3C8,
	GCSpeechRecognition_GetLastFrame_m7F37448BC9DFFFF791B0B9A152BC7E837200122D,
	GCSpeechRecognition_GetMaxFrame_m30A8F98EF970E22469F292003EDCF3AB8D05ACB4,
	GCSpeechRecognition_StartRecord_mF029CEF763BEA4AF6BD23EA541EDB642AA9282DA,
	GCSpeechRecognition_StopRecord_m6215A032241C8367A02C90F70081CE5B32D91515,
	GCSpeechRecognition_DetectThreshold_m88ED82DD0FFEBF47C0C204A07AC1D3C47E6DEA56,
	GCSpeechRecognition_ReadyToRecord_mF97690BF95A8C988F685510296868D3C4730A1F4,
	GCSpeechRecognition_GetMicrophoneDevices_mBBC180D5872F990DA3B9BD487D14C48C8CF40DDD,
	GCSpeechRecognition_HasConnectedMicrophoneDevices_m2A67F0E291995C0E4A1C1BFF1F0B3B1F9D9BFA3F,
	GCSpeechRecognition_SetMicrophoneDevice_mD27AD241DEBDF575029D5FCEDA7F8386EB76920C,
	GCSpeechRecognition_SaveLastRecordedAudioClip_mC3D7668FD6CDF43AA9CFE27E356A33BDE282A5BB,
	GCSpeechRecognition_Recognize_mB96763A5D5F871ECCE79610767299E2B78DD76AB,
	GCSpeechRecognition_LongRunningRecognize_m0FBC506DA48C621008299E105FE2F02129C64F33,
	GCSpeechRecognition_GetOperation_m1D5CB11AEDBFA515E566B9AA84E271B082A477BA,
	GCSpeechRecognition_GetListOperations_mED6D64C149BA835EC5A57ED63849E3B5C14DD489,
	GCSpeechRecognition_CancelRequest_m6B8DBDBC3F7ED8041C560E8678D523383B1393B4,
	GCSpeechRecognition_CancelAllRequests_mC2869270BAE693AA13B7A7A67CBE26C666F42A8A,
	GCSpeechRecognition_HasMicrophonePermission_m85D2BEB942C003EE1D5B81467D56A4DDB820879B,
	GCSpeechRecognition_RequestMicrophonePermission_m464E84B5F84151E5D289E42F65F0158034EA61A0,
	GCSpeechRecognition_RecognizeSuccessEventHandler_m29C63E569FFB2990E01742F8712047E3FA7A4600,
	GCSpeechRecognition_LongRunningRecognizeSuccessEventHandler_m68598F3FFA7AEEC573F302C74300B53C1BB24B2F,
	GCSpeechRecognition_RecognizeFailedEventHandler_m97A593907EA7530EF493EBC572D40569E06AC4EA,
	GCSpeechRecognition_LongRunningRecognizeFailedEventHandler_m41681E1765CB02CA5837213C9485CE3F880C2476,
	GCSpeechRecognition_RecordFailedEventHandler_m04CB1705B84A26D04E012D8A39331777CCC42C09,
	GCSpeechRecognition_TalkBeganEventHandler_m079CAA9F2E13EA5CF70F8D3F4317CDE47F145B30,
	GCSpeechRecognition_TalkEndedEventHandler_m7013C4A5DAD516A2CBD9A6E0975DCA53C5880155,
	GCSpeechRecognition_RecordStartedEventHandler_mEE0189401FDCDB1910712B87D045E2BF6B7E9F4C,
	GCSpeechRecognition_RecordEndedEventHandler_mA5ABD995B2A506689B38AF5A8EAF6D12A813349E,
	GCSpeechRecognition_GetOperationSuccessEventHandler_mE78894DDDF5507CF52EBCFDA06A60A20FF041F2B,
	GCSpeechRecognition_GetOperationFailedEventHandler_mC855B586556981A7BE65256F5F69948528DA6052,
	GCSpeechRecognition_ListOperationsSuccessEventHandler_m3ECC62B298038B40BD5AB645B72AA36F47E1D0D0,
	GCSpeechRecognition_ListOperationsFailedEventHandler_mD6ADE8C5C5AF26896EB65A1B2153F3701C2D37FB,
	GCSpeechRecognition__ctor_m01ED16574E7F7772516D1EEE43D32F152E676AA1,
	GCSR_DoCommandsExample_Start_mEF99F5979B93C36E57D8C16754C116066B30F488,
	GCSR_DoCommandsExample_OnDestroy_m22497CA4F5ACE24ED7B60F82D33E87944318D55A,
	GCSR_DoCommandsExample_StartRecordButtonOnClickHandler_m07EC4CFF3D3BB94B6121A811CAEC24993871B3CD,
	GCSR_DoCommandsExample_StopRecordButtonOnClickHandler_mF63B77FDC42FD9254466CB566884870923F2A5B1,
	GCSR_DoCommandsExample_StartedRecordEventHandler_m69A58B6FD4B08381D7F855F9835518CE656DFFB4,
	GCSR_DoCommandsExample_RecordFailedEventHandler_mF50A567DCBCD696DD26E6557AFCE95294A27BBCB,
	GCSR_DoCommandsExample_EndTalkigEventHandler_mF0FD9B99FC7ADEF99BA48B89591CED974D98301B,
	GCSR_DoCommandsExample_FinishedRecordEventHandler_m42B221984ADA64D5DDB8AA56C10A1783C2F14BD1,
	GCSR_DoCommandsExample_RecognizeFailedEventHandler_m66F14488665CB5AA61393AC875243776953DD542,
	GCSR_DoCommandsExample_RecognizeSuccessEventHandler_m53FB08218DA3FB812ED9612DAE38633FE184405A,
	GCSR_DoCommandsExample_DoCommand_mFB101E8B6B45139A11F2785D221AC93152DEFDF2,
	GCSR_DoCommandsExample__ctor_m1E36A47FA6CDF3C04D773D872EDCE34481806F4B,
	U3CU3Ec__cctor_m6D2F1607739D08699B23954974D0F48826F81547,
	U3CU3Ec__ctor_mFBFF875A820F3D1F46EE151EF28BA66A665C9ED6,
	U3CU3Ec_U3CStartU3Eb__8_0_m2F9C61C32FA7857ED901DE1481A483C5C909D0B8,
	GCSR_Example_Start_mEDEBA7796B580FD5367845E69A54916F9CE24357,
	GCSR_Example_OnDestroy_mE775CF3285F05F5596AC7D3C81948BDCB8402ECC,
	GCSR_Example_Update_mA625EC43C8A0CB70FB9B419EAFB3F1698CA9817C,
	GCSR_Example_RefreshMicsButtonOnClickHandler_mF50F2AF38212E054D838F4857C9A180BDA65F052,
	GCSR_Example_MicrophoneDevicesDropdownOnValueChangedEventHandler_mE2652754013DA6BCE475E7CB0DCF9B2F5F2CD7D3,
	GCSR_Example_StartRecordButtonOnClickHandler_mB1BC04E647DB04429371C87C5AE298AEFB21DD07,
	GCSR_Example_StopRecordButtonOnClickHandler_mBAAF04EFFA76887CA3010CE732AB9321FE4710E1,
	GCSR_Example_GetOperationButtonOnClickHandler_m8605782AFB0EFF4FA765878610943CB3565A89D0,
	GCSR_Example_GetListOperationsButtonOnClickHandler_m6C4C525F70DBB1DB9726DF158ABDFEDC96673D3C,
	GCSR_Example_DetectThresholdButtonOnClickHandler_mCAB1E74C61133F0FE3DC0FC2FA2DD16D6EB1B4D9,
	GCSR_Example_CancelAllRequetsButtonOnClickHandler_m90281CFE8EF97A04D8409EE5D454102B38BC2799,
	GCSR_Example_RecognizeButtonOnClickHandler_mFC51ED6576155B37F5DA60FAB0485C67ED92FD34,
	GCSR_Example_StartedRecordEventHandler_m5C55EF3D56D782819FD347ECA90A5455149A345F,
	GCSR_Example_RecordFailedEventHandler_m2833F4C7AC8ADEEE087987D3A00546FDF241991E,
	GCSR_Example_BeginTalkigEventHandler_mB6A04D31BAF8D4487AA7597D6839EFA734860005,
	GCSR_Example_EndTalkigEventHandler_mDD7CAE3865BA7D6F9E037EA67B4A65F2963000FD,
	GCSR_Example_FinishedRecordEventHandler_m5A69C3A40D2EF5854C8CF534B36A1A794CD6F2E6,
	GCSR_Example_GetOperationFailedEventHandler_m8FAC950815B398DF903B6C8A5CC0AEC7B57EDE9F,
	GCSR_Example_ListOperationsFailedEventHandler_m8974DCB135999461C6654210AC1E8F0DB8A38F22,
	GCSR_Example_RecognizeFailedEventHandler_m752650BBFFB077D4584F46EB379105A2CCA26184,
	GCSR_Example_LongRunningRecognizeFailedEventHandler_mC62B8B4F3AB7F3498918670AF8E2B6E39CD69BA0,
	GCSR_Example_ListOperationsSuccessEventHandler_mBAC38BAB2107547FD7DA6E0887B7774465770A8A,
	GCSR_Example_GetOperationSuccessEventHandler_mB7279125A9F67E5893AE5D711993BDE3BC91A5D1,
	GCSR_Example_RecognizeSuccessEventHandler_m9459DCF0F5D11D6AC9C6112D8B6E20F7BCCB2FBE,
	GCSR_Example_LongRunningRecognizeSuccessEventHandler_mC0ABDB4D23637AAB1C8445D452A69E4F0CF29029,
	GCSR_Example_InsertRecognitionResponseInfo_m850861B87AB1D6EF68D1696A2D501592F0B75127,
	GCSR_Example__ctor_m19DB5197F3D08CDD8FF39B36390718C820D8B862,
	U3CU3Ec__cctor_mAD576C5A8D13A72D32EB0DE805F9EC506D895364,
	U3CU3Ec__ctor_m1B50BD72179E3FFB0E6B6EAB44DE1458E87B1FD9,
	U3CU3Ec_U3CFinishedRecordEventHandlerU3Eb__37_0_m9508E0E1467380DC7ED850098C8F28E5562B01BA,
	U3CU3Ec_U3CFinishedRecordEventHandlerU3Eb__37_1_mA974CA3E054FC59897BABC9F63D1F714147E710E,
	speech2text_Start_m305E2C184126B3C5BD425BD274C9D3F233F0991B,
	speech2text_OnDestroy_m6866FA09D49B3FB91C51D4BCD5D35E9B81E90791,
	speech2text_Update_m1C1FDA9180B0741C57A67B3FE4376AB8B6ACB8E4,
	speech2text_RefreshMicsButtonOnClickHandler_m39B3CCEB45F267B0CB6EC2E3FD5C2E7AF62343BD,
	speech2text_MicrophoneDevicesDropdownOnValueChangedEventHandler_m791CDD231CB6C238959E6EC87FAF7B462142B141,
	speech2text_init_lang_var_m7D6C98430B2313BB98104AE9187C094EF1A8A56F,
	speech2text_StartRecordButtonOnClickHandler_mA4F82021C073CE8C21D662B5231C09C005E34205,
	speech2text_StopRecordButtonOnClickHandler_m077AB3ECF0E761B8C2DD5A5D569CDCE5FCB0570A,
	speech2text_GetOperationButtonOnClickHandler_mC02EF58A32DB88EC1AC444BE2A54D7D57D2B40E7,
	speech2text_GetListOperationsButtonOnClickHandler_mAE6862E2CDCC1F4BD7911B114CE1C5B341CF7A56,
	speech2text_DetectThresholdButtonOnClickHandler_mEB3B13C7EB0846F037374F4317119390A0BB2DF2,
	speech2text_CancelAllRequetsButtonOnClickHandler_mA4F0A72A2F208CA1B14AB34681929FC13025995E,
	speech2text_RecognizeButtonOnClickHandler_mE5D569B06C80203793CFC3DF1064DB414104E7EA,
	speech2text_StartedRecordEventHandler_m718A34BD01296D9EDBD41BE3BC0160215BF155CE,
	speech2text_RecordFailedEventHandler_m4EDD874CF8D2F212EAF219F7A3A7D9F2D5397294,
	speech2text_BeginTalkigEventHandler_mDBEBC3E9F1469BC1A7B4B903342CAB1CF91E0298,
	speech2text_EndTalkigEventHandler_mF78CB283E46C7209A1B7D89CF29826B573679065,
	speech2text_linguaDetectAlternativa_m9A2EB053E0C95FE62E720E6D3E79C4AD00D7F4EC,
	speech2text_FinishedRecordEventHandler_m3E3F1E240AB2D7B0439120F2724AB586373B79DE,
	speech2text_GetOperationFailedEventHandler_mCF483561D26A195595F88160BDD52E97D4A25CF8,
	speech2text_ListOperationsFailedEventHandler_m97666BDDE0971767D413C4EB7BEBC57F10488C5C,
	speech2text_RecognizeFailedEventHandler_m973BBEF6BCA7CE43A3F0008954D5C15EC1154172,
	speech2text_LongRunningRecognizeFailedEventHandler_m630148726F91F2BAE7847BF6BC2291AD28F35581,
	speech2text_RecognizeSuccessEventHandler_m8E8A950A7E824BF22180EF42FF06CE771211B782,
	speech2text_InsertRecognitionResponseInfo_mDA70B697E5C8594379EA3D51B8BEFA1C749D38E8,
	speech2text__ctor_m46C0063CBC8EC4F04A4E00484F8880F8732C98F2,
	U3CU3Ec__cctor_mDC9676760A0627A7FBEE93AC3CE1B8F5862AB7AA,
	U3CU3Ec__ctor_mC56D25B1229BFD984C33925EEE9C69A1CC446F66,
	U3CU3Ec_U3CStartU3Eb__17_0_mA3C18A9832F127C06E196D5ED1E73D9E96EEEC96,
	U3CU3Ec_U3CFinishedRecordEventHandlerU3Eb__37_0_mE43B03956368519211E7D231CFE107E13F9087DB,
	AmplitudeSamplesUI_Start_mC862317E34DC04867122696C5CF25A93A281F7DC,
	AmplitudeSamplesUI_Update_m1799C215CFC0F4A9EB1D3F21E4A40B918D4E90A3,
	AmplitudeSamplesUI_SetBoost_m578252BFB16EE3439ED5E6DF4E100D5C6CFEE095,
	AmplitudeSamplesUI_OnValueChangedSampleSize_m951431B498A13F5AC49413E2B6568467221FE598,
	AmplitudeSamplesUI_OnValueChangedDataType_mE7CB21979BEA8508E6B78F3782C3BAE16D20FA4C,
	AmplitudeSamplesUI_Play_m27F7EEEAA517929CBCDD59B1B6D169B392D4F0BB,
	AmplitudeSamplesUI_Stop_m8D795BC40191E30F1E4EDF38644D3B1F6C027EA2,
	AmplitudeSamplesUI__ctor_mA89EA8A1180D77FA05FAE16868FB42DF6D9784C1,
	AmplitudeTester_Update_m432D6C77B9DE0C10DBBF500EDA0CA0A6F980D8F2,
	AmplitudeTester_Play_m961D5DADACCCA6A67220B0CA40C21A7B847FECAC,
	AmplitudeTester_Stop_mCD33463014E2FEA12F14C828B5F722D3169FF9B7,
	AmplitudeTester__ctor_mE5A18E5295CAD2881EFFC0522A25B2BA929EE3F6,
	Amplitude_Awake_mF3B009C75395EE567E8B42D875017C4ADF0F1C5C,
	Amplitude_Start_m1C3C118F695BAA19D6C457134AC99AB8AAF70F21,
	Amplitude_PlayOnAwake_mEB70D141E7117F804F74F47BFF9410EA598FB68A,
	Amplitude_LateUpdate_mB2703DB0C4D5C5182573CCCF1201CC605ECA22F4,
	Amplitude_ScaleRange_m919F01A62EDBE80BB47730125E5B94C11D0589D1,
	Amplitude__ctor_mC71496FD5D5DF312EE79E6943D46873BC4F75B9F,
	U3CPlayOnAwakeU3Ed__24__ctor_mB806F712BEE562BFD80BA8ED3FAE86EDC8587DA7,
	U3CPlayOnAwakeU3Ed__24_System_IDisposable_Dispose_mABEE00ECE6ED6CDBAE345C4086415CFDCB46C136,
	U3CPlayOnAwakeU3Ed__24_MoveNext_mC764E91B7C9060F8219AF58521FDB020D05E244F,
	U3CPlayOnAwakeU3Ed__24_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mDB4BB88B57CE4320AB30E332C426C69F2CED93D9,
	U3CPlayOnAwakeU3Ed__24_System_Collections_IEnumerator_Reset_mDFC37A3B54A0DACB1CB04B8AEBF3CC1840C04982,
	U3CPlayOnAwakeU3Ed__24_System_Collections_IEnumerator_get_Current_mD759248DB373EF917B115C5727F2045C005DECC3,
	AudioUrl__ctor_m161DA67A8471A56C088D42472EB640CA99D6554C,
	Notification__ctor_mD2703FBF62656074E25AFB11B5532DA4FA0CA27D,
	WebAudioUrlProcessor_get_QueueCount_m2FB8E7965E822A319CBC91C1539DFB745EEBF136,
	WebAudioUrlProcessor_get_IsQueueProcessing_m43EEB4787FF12D1CF18B6207A8AA09277346F559,
	WebAudioUrlProcessor_WaitForAudioUrlProcessing_m073F718578D30B2711F8DD9F11F749878BF3E7C3,
	WebAudioUrlProcessor_ProcessUrlQueue_mE993AE7B9F4FCD6836C593C459F1A6C723DF6248,
	WebAudioUrlProcessor_ProcessUrl_m4C26E983564FE9DBA3B29902B888F8977E97F9DE,
	WebAudioUrlProcessor_WaitForQueue_m18F65E2F2F75E7E2703AD065747FCAA26D7ABDF1,
	WebAudioUrlProcessor_SetCallbacks_m579F620DCB5756D94417E5E02A1E6EF404DD81FC,
	WebAudioUrlProcessor_SetCallbacks_m9129ADD1276098C985614D7EEDD5EC3762CF255D,
	WebAudioUrlProcessor_GetAudioClip_m3F3CD668F5F657DD8389DCA6C671BBD069645E0C,
	WebAudioUrlProcessor_EnqueueAudioUrl_mCE09971129175BD3A8F3519B972AE28B2EA55BD1,
	WebAudioUrlProcessor_GetBatchedAudioClip_m1744859FF8C9E1B1CE2D504C6CEFE178C21CD692,
	WebAudioUrlProcessor_ClearAudioClipQueue_mA41490BD646DFC8113CF3FA0F347258206D329EF,
	WebAudioUrlProcessor__ctor_m6205A500669C64D40DC5E78AE8444225435A8D24,
	NULL,
	NULL,
	NULL,
	NULL,
	FailCallback__ctor_mD98D404B6FEB882AAC01FE5FAD049450F252D1A0,
	FailCallback_Invoke_m9BAF2CFFDD8BA208A09922A5FA72CF448B49DC74,
	FailCallback_BeginInvoke_m57CF2D596F36A37FD4ADBCA654303C1ADCA7798F,
	FailCallback_EndInvoke_mEE3CA1FFBF5A3F63CE953B45184A9013BD268A71,
	U3CWaitForAudioUrlProcessingU3Ed__14__ctor_mE73D373F007FF26769B7A39CC9EE43D7D5DC5999,
	U3CWaitForAudioUrlProcessingU3Ed__14_System_IDisposable_Dispose_m40DB25CB4B363057ACCFE6175DA0B812E0EB4916,
	U3CWaitForAudioUrlProcessingU3Ed__14_MoveNext_m81C7D54B7258F8680F2720F3869DD338C8AD9050,
	U3CWaitForAudioUrlProcessingU3Ed__14_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mE83403F038DBF77EF1BA1EFACC71F5C883AA7307,
	U3CWaitForAudioUrlProcessingU3Ed__14_System_Collections_IEnumerator_Reset_m5FECAEC4E160C94E9B6C6C061C28C4A96833D54A,
	U3CWaitForAudioUrlProcessingU3Ed__14_System_Collections_IEnumerator_get_Current_m49BC9B4608EA5B7EF36A7285009F53DBB3A1FEDA,
	U3CProcessUrlQueueU3Ed__15__ctor_mB11C95B045C496FE1AF45815A9D9A80353392238,
	U3CProcessUrlQueueU3Ed__15_System_IDisposable_Dispose_m0DF615CB9BCF331920E179594A292C1FDE41509B,
	U3CProcessUrlQueueU3Ed__15_MoveNext_mE7B803EDC5171E62A16C3C694DBBD36DBE428FEE,
	U3CProcessUrlQueueU3Ed__15_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m54532538CDB1A82F3C31B4EE096D5620FF2C6453,
	U3CProcessUrlQueueU3Ed__15_System_Collections_IEnumerator_Reset_mF4DDEF2FB2CB3CBCF0BCB1F89A98A0A1E7DEC751,
	U3CProcessUrlQueueU3Ed__15_System_Collections_IEnumerator_get_Current_mE14194A18DF0D6CB9B52409B67705B06FA7F8A1B,
	U3CProcessUrlU3Ed__16__ctor_mC66072579BAE78073CC8652C1DEFCEFC97B0FE57,
	U3CProcessUrlU3Ed__16_System_IDisposable_Dispose_m9C2BF4D674C2B05CBFDC4BCE9F6AEB8C20739404,
	U3CProcessUrlU3Ed__16_MoveNext_mBCA167BF9C8FF9CE6008DBE9FBDC7221EC741D1C,
	U3CProcessUrlU3Ed__16_U3CU3Em__Finally1_mA3BC5B43C2088568020CDA05664D497225A2C4DD,
	U3CProcessUrlU3Ed__16_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m39C984F612B617A8FCAAC0FE6FE97F00B92B96D1,
	U3CProcessUrlU3Ed__16_System_Collections_IEnumerator_Reset_m651076750200783DE8226D093E59153136C451CF,
	U3CProcessUrlU3Ed__16_System_Collections_IEnumerator_get_Current_mE3AD2930731866168C8D52B66CA858A92F1D8269,
	U3CWaitForQueueU3Ed__17__ctor_m49D6149B0BA3130D79B3F07B8B3AC1E57B055E3D,
	U3CWaitForQueueU3Ed__17_System_IDisposable_Dispose_mA4440F9EDBC6370461BE37951BD4AD585090B9EB,
	U3CWaitForQueueU3Ed__17_MoveNext_mFC776F253693AF51E9074AB4CF957349C3CFB17E,
	U3CWaitForQueueU3Ed__17_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mCBACF6139B2171180E6F861447113808FAC82593,
	U3CWaitForQueueU3Ed__17_System_Collections_IEnumerator_Reset_mAA8202C833D9070E6609298C674302399A42E4C1,
	U3CWaitForQueueU3Ed__17_System_Collections_IEnumerator_get_Current_m6F01B944116F7AF35AE73BCDB13FC76E82935199,
	CM_RuntimeSetupExample_Start_mBFE59A62D3907030FFF20005D3BAD42E9252EFD7,
	CM_RuntimeSetupExample_Update_mC2784B90109EA40F40E5C26DD591DA5F7545428E,
	CM_RuntimeSetupExample_RemoveComponents_mA7C611D5F3ADDC8D02B2C6F4AB12E27CF75EED9F,
	CM_RuntimeSetupExample__ctor_m4957371CAF8014C3FEBAAEB9647CCE2739EAF826,
	U3CRemoveComponentsU3Ed__7__ctor_m41B7A3E15A422129EC240B326D8ADF94028894B6,
	U3CRemoveComponentsU3Ed__7_System_IDisposable_Dispose_mAA6D6AA38ECADC34204CE8590DAA1A3E32ACA1DB,
	U3CRemoveComponentsU3Ed__7_MoveNext_m57FB9C035701B9BBB5E5DBE01972F14794083491,
	U3CRemoveComponentsU3Ed__7_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m70297559CB7CD4DDE0C2B6ADDCCC096F3D294798,
	U3CRemoveComponentsU3Ed__7_System_Collections_IEnumerator_Reset_m71C6F10A218DAD07591A5901F8437E5C7C70DCCD,
	U3CRemoveComponentsU3Ed__7_System_Collections_IEnumerator_get_Current_m99A5DB34E9D032ADFAA13A471094209D90BF84B2,
	CM_iCloneSetup_Awake_m060150ADE0FB0D048C426899FB2904280967CDDE,
	CM_iCloneSetup_Setup_mCC5F12F4C44D60A5A34F775EA37B093EDAF410D5,
	CM_iCloneSetup__ctor_m5A54790F3FFD18F240DFB9B461BA47D1DB7AAB5F,
	CM_iCloneSync_Reset_m30051B265E1529B2D0272AE6D7BF04B3B9846247,
	CM_iCloneSync_Start_m7886AFC63CB5BF0065F0CAD3063803453C974EBE,
	CM_iCloneSync_LateUpdate_m3C2100B73171CF2992FD3D41489E88F6DF4C912D,
	CM_iCloneSync_Initialize_mE0F9FFC931B8A93D94F7C915AE7EF840CE36675E,
	CM_iCloneSync_GetSalsa3D_m00A165A7B89DAAD00F6F14C4BD9837A73B73763B,
	CM_iCloneSync_GetRandomEyes3D_m29881426D8636B87FAB436890DDC6387F8B8E095,
	CM_iCloneSync_GetBody_mE7678B3FD3ABB33C75D048128A65D8834963B03A,
	CM_iCloneSync_GetEyeBones_mDB35EFE8B591110B44E9B8D3B12B248CA3EB48A5,
	CM_iCloneSync_GetJawBone_mC413D10590F8ADFBA570ED6DF79DACE0CE459CCD,
	CM_iCloneSync_GetFacialHair_m6B2F1B418A39C766457738D30318B316B9D5AD90,
	CM_iCloneSync_ChildSearch_mD818EDB16815CD3E297C6B3C5C0BFA2D454A21E2,
	CM_iCloneSync_ShapeSearch_m4F228DC2332D424477105137095E83C45727C48A,
	CM_iCloneSync_GetShapeNames_mE985429B07EF10CE88D4B9A8F935398392E60BF9,
	CM_iCloneSync_SetDefaultSmall_m5B57A882E6414DDADF1D5C788CEE6EFE9C93D2D4,
	CM_iCloneSync_SetDefaultMedium_mC07E375BD4067AB8AB87CB70D9C74EE4127DA6AB,
	CM_iCloneSync_SetDefaultLarge_mEB6409D3DA2D4C553C6C0369A1C9C47D25DEEF92,
	CM_iCloneSync__ctor_mAF27CFCAAB7822AFDD6485449A7C888C44F0D18A,
	CM_ShapeGroup__ctor_mE18DAC01C2CDAD39D31D272A8BFF76F855E43C38,
	CM_ShapeGroup__ctor_m751029B78E0AD02184BD2FDBDDBE394FF7292B67,
	CM_DazSetup_Awake_m4489069F702A529781A5D47BF44D6C2097E3D34E,
	CM_DazSetup_Setup_mF2AB321B96FD24CA6043CF4FD72B13DFEF3A8E32,
	CM_DazSetup__ctor_m73F760C2F7B8F6444C6A3264AF20EF65FDD29511,
	CM_DazSync_Reset_m6860AA97431722F776D99118A8EC11BE22584889,
	CM_DazSync_Start_mCA5A30D8697139286E5F27AF8C0AF23406762CFC,
	CM_DazSync_LateUpdate_m929CC296EBC20AF377166AB18A6A4B57DFD558DB,
	CM_DazSync_Initialize_m7494DBF98A2BD6B882BF6F6C9B180B345FE467CA,
	CM_DazSync_SetCharacterType_mE260BD9574BB6203F38E19D225C3EFBF0C47E366,
	CM_DazSync_GetSalsa3D_m2E0ED23B56034A6BEA4AC4C4E6ACB4BA13DC25D8,
	CM_DazSync_GetRandomEyes3D_mCCE5B222DB98DF166920CD1705A31562FBD25C14,
	CM_DazSync_GetSmr_m4706A33C61EF2F25D13263BF14BB159F1A4E00D1,
	CM_DazSync_GetEyeBones_m45D4E34882FA25F276D1A9F8B641CF3039E41D18,
	CM_DazSync_GetBlinkIndexes_mD83B2DD25CD62A5A7D150374DC3CFE59A864FDF9,
	CM_DazSync_ChildSearch_mE5350B07551410C943056F378BA33D05FE3EB1F8,
	CM_DazSync_ShapeSearch_m4CFDE5964658AA85CBC8E7EE8D2884462F0E5747,
	CM_DazSync_GetShapeNames_m1BF29B0E6E6088E87F8748B1C4270B2AAE151F45,
	CM_DazSync_SetDragonSmall_m3A95E0F512AEBEAB0AECF61B91EC3380D4D0094C,
	CM_DazSync_SetDragonMedium_mB4672796360DB513F76B9A4AF9E771E07E8E6DB8,
	CM_DazSync_SetDragonLarge_m1C471B55E2AAB0EC5526671EF0AFCB8D0E133448,
	CM_DazSync_SetEmotiguySmall_mC8648A6DBD945C9C86F9E7770F0CEFC545334EE4,
	CM_DazSync_SetEmotiguyMedium_mDC70BCE9DEE3B1A8CF28C68B5007199CD258B5D8,
	CM_DazSync_SetEmotiguyLarge_mF11D617804AB4F87AFC5B8365CCC5C4BA7E25DEB,
	CM_DazSync_SetGenesis1_2Small_m96772DFF69A39727E812DE39ABF7C1169883E099,
	CM_DazSync_SetGenesis1_2Medium_m193A30935A62AB735DD3D8A4E5AE330C28864644,
	CM_DazSync_SetGenesis1_2Large_m81FF8D00CCA832523A7611550BD0DB8D23BF04A9,
	CM_DazSync_SetGenesis3Small_m04E2981FFDEBCFC5ABFBAB6933DD95220B9BFC98,
	CM_DazSync_SetGenesis3Medium_m2344088B8722B43F86B9DC0E17AF00D5884B45D2,
	CM_DazSync_SetGenesis3Large_m438E566EEDA201E57D5EE5C0B309188DDBD7ABAE,
	CM_DazSync__ctor_mF0149BF5001C4688AA8D7A5AD9D98E84C554AAE4,
	CM_ShapeGroup__ctor_m85CF1EA71D635B5EEC906ACD8030B9C8613EDA4A,
	CM_ShapeGroup__ctor_mD2A4D78A12B0C527D8C7C31F08E142A4C86E2649,
	CM_SalsaTypeAndObject__ctor_m38E49FDEFE4F1F6554C246B39EB358D27EADFFDE,
	CM_PlayerResponse__ctor_m5955D733568D75EBEB84CE280CA869120A2145E1,
	CM_NPCDialog__ctor_mA4A8363BCBA9A6D05696E8F05D0E6AAF1219792B,
	CM_DialogSystem_Start_m31EB6326D13B2966F01076D96873FC15FE250FD1,
	CM_DialogSystem_Salsa_OnTalkStatusChanged_mA370E66FA39F18F91B2AF53BFD88F905ED745E1F,
	CM_DialogSystem_OnGUI_m6A8ACDB63FA544F2381CE3D6C38836F70AC76E5A,
	CM_DialogSystem_GetSalsaType_m01A03E2A5CD97B46F2500D4D2345A3C27F5361C0,
	CM_DialogSystem_EndDialog_m6ED7C481237180BEBA202E8862A5799F6C3ECFAE,
	CM_DialogSystem_ResetDialog_mB2CAF989803B41E4BC09611C7CB7897B3E6051F9,
	CM_DialogSystem__ctor_mCDE140BEF61EF907A420705A1D330918BB5A26FE,
	CM_Ethan_Demo_Start_m2E0F849F7A61FE235F6C6035B5C73AE4C7F73639,
	CM_Ethan_Demo_Salsa_OnTalkStatusChanged_mA301C2A72432F1BAF086677EA853AEA3517C674F,
	CM_Ethan_Demo_RandomEyes_OnLookStatusChanged_m92C2BD557973EA3A296D2F1ACB6E659EF19196F4,
	CM_Ethan_Demo_RandomEyes_OnCustomShapeChanged_m51E6EFB22DD272DF752C22B5CA09A1A574CE928C,
	CM_Ethan_Demo_Look_m1894D23E2740CBCCD2156CE1B6292FAF526C4B86,
	CM_Ethan_Demo_WaitStart_m245B82125413DCD972E3C46AC88B470159EE2FB8,
	CM_Ethan_Demo__ctor_mE9CC699AAC92CCECC362D8D756B584FC3B1D9FD6,
	U3CLookU3Ed__11__ctor_m0C0602C6D13FD98A1C3E9861D60728A2A878067E,
	U3CLookU3Ed__11_System_IDisposable_Dispose_mB065D40394D3306D926295AA04E857E4E893AEA6,
	U3CLookU3Ed__11_MoveNext_mD8401698B9543AAD5E237E8FBAEE01930B468C8A,
	U3CLookU3Ed__11_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mEAB61CB937BA80B253726650D1A70446F99DC62A,
	U3CLookU3Ed__11_System_Collections_IEnumerator_Reset_mCD70572400F9FAA327104E951FEA60B7D9B27CC3,
	U3CLookU3Ed__11_System_Collections_IEnumerator_get_Current_m2AB41E6148A13C6D813F3A3645D3805BB866D473,
	U3CWaitStartU3Ed__12__ctor_mAD648999ABEAFD059CB1F0FB307CF014C265E18E,
	U3CWaitStartU3Ed__12_System_IDisposable_Dispose_m6283E332BA2CCB8F440B18136D5D9DD864B21288,
	U3CWaitStartU3Ed__12_MoveNext_m61697352AFE8FD2EFAE783B65E98CAEFF9118E79,
	U3CWaitStartU3Ed__12_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m55777F556FD9449F18A3B85AD0A98F1FCFA16C55,
	U3CWaitStartU3Ed__12_System_Collections_IEnumerator_Reset_m7499B509460A4F30948CFBAAEB6BCEA36FBE6C16,
	U3CWaitStartU3Ed__12_System_Collections_IEnumerator_get_Current_m177A5FEE4EC60BE618906AA734331936C5AF9D8D,
	CM_GameManager_OnGUI_m8E4DB48B486264D193B6E2C96D820E779BC496D5,
	CM_GameManager__ctor_m6A806AC95F833BBDFFB1FBFB641ECFC5EB14737F,
	CM_RandomEyes2D_Functions_Start_m5FF21EDFB2D31BB4FD401320FEC8180733CBD22C,
	CM_RandomEyes2D_Functions_OnGUI_m2A34E0A12FEA0F287B2635BE13BC3C9A76010E32,
	CM_RandomEyes2D_Functions__ctor_m8F6326C30BC3124C0486C64049EAFF5F51E918D2,
	CM_RandomEyes3D_Functions_Start_m2F4DC0D3F6FA2B3B191ECEFE4CC260E735181538,
	CM_RandomEyes3D_Functions_OnGUI_m58ED8F8F282B2EC582C12146488B77C7AB8D9745,
	CM_RandomEyes3D_Functions__ctor_m0A5D5308C1C812957E2E165D4216DDD7295DF7C6,
	CM_RandomEyesBroadcastEventTester_RandomEyes_OnLookStatusChanged_mEC096F1AC75E7D3B0383C67F431BA257C0521759,
	CM_RandomEyesBroadcastEventTester_RandomEyes_OnCustomShapeChanged_m365512A792F3BAE7E7C1DC233B905721132D7F62,
	CM_RandomEyesBroadcastEventTester__ctor_m5B156E4D9D7CF6BBBBF66558F97BC8D8E24269BB,
	CM_RandomEyesCustomShapeIterator_Start_m6D54A0CF79E951046C07D03E5289323D159209E9,
	CM_RandomEyesCustomShapeIterator_RandomEyes_OnCustomShapeChanged_m3411D524807635764DFCA3DB2F2EC2D7AE3683A7,
	CM_RandomEyesCustomShapeIterator__ctor_m91F3D95FD998AFBE3B6036AECDDABC9A948CEDC3,
	CM_RandomEyesTriggerTracking_Start_mCF41787D78B878D78C9F306A8404676D5ED9EE37,
	CM_RandomEyesTriggerTracking_OnTriggerEnter_mE3CB4C2C83686EBBF79B29E097A706EB70E30909,
	CM_RandomEyesTriggerTracking_OnTriggerExit_mE0B932AFB67E07DE3942D9CE3AE8C743662F5054,
	CM_RandomEyesTriggerTracking__ctor_m462BE0E2E1C46B2F3DCF9D9B5086938B200F0138,
	CM_Salsa2D_Functions_Start_m7E9E4BC3706BE503279BBC1405750122AF6DA777,
	CM_Salsa2D_Functions_OnGUI_mAEF661961367C6B8D6144B281169D86A0E139F4D,
	CM_Salsa2D_Functions__ctor_mC0F866956C211BD7A60A740BB2309274661DBFA5,
	CM_Salsa3D_Functions_Start_m2D8E3DD15E10EFA97874692ED7AB8FC352297197,
	CM_Salsa3D_Functions_OnGUI_m44DB6B566F81633B2A47C0349653A9A2C34BF3CA,
	CM_Salsa3D_Functions__ctor_m1EE368AB5883C5179B6BBFCC69CB286506AC370E,
	CM_SalsaBroadcastEventTester_Salsa_OnTalkStatusChanged_mF50CAE7CB454B6051DE779173A8F94981D9C237A,
	CM_SalsaBroadcastEventTester__ctor_mF12D465360B6D0FD1F5C7F47ECCAEAFC3E77EF6C,
	CM_SalsaWaypointTriggers__ctor_m0E09DA7A3FEE0BD10650089CC2B95D1B87443294,
	CM_SalsaWaypoints_Start_mD084D3CFDFF746D0F8616EF6AE7E2C30E8EDD3D4,
	CM_SalsaWaypoints_Update_m70B4AE34E82DB01A0156FE281BD5795B74AC32F3,
	CM_SalsaWaypoints_Salsa_OnTalkStatusChanged_m3BB90E7D6192836D6057E57B5DAB6B9AD85A818F,
	CM_SalsaWaypoints_SetWaypoint_m964E61F4B5A8F145A047D0400164B3F40C2B9350,
	CM_SalsaWaypoints_SetSpeed_mEDEFC56B21AA7457B0A62BE4AAAF36763D475101,
	CM_SalsaWaypoints_ResetSalsaWaypoints_m00492355368B60AD1D5B981C1DA26532473B2D21,
	CM_SalsaWaypoints__ctor_mBE46E4BAC07AA3C6CC75EF190CD4B7C6DAE98D6E,
	CM_WaypointItems__ctor_m0B308F6D34E3D36C6C691A74AA5262F18E497EE3,
	CM_Waypoints_Start_mEA3A1FF43D43B79A0CC6C6F15A25FEF7E22A3482,
	CM_Waypoints_Update_m59C02ECEFC66282ECAAFD38DE94E1EFD49DAA203,
	CM_Waypoints_WaypointCheck_m3103A033B72AD0FEF95EB6854CE2A3EB6D514A64,
	CM_Waypoints_SetAnimationType_mC96C82F12FF08A385BE3E471F987CD9132E479AA,
	CM_Waypoints__ctor_mE92D295A4B0D8CB69817CF60037B57FEDAA3E9FD,
	Salsa_RTVoice_Awake_m33BF6931483E0A99E751810DD198135F9087594D,
	Salsa_RTVoice_LateUpdate_m499E26FA766E02BC9A20A1F2836A9B11B191A0A4,
	Salsa_RTVoice__ctor_mE3A2BEF591FE93DD763DE5B4927DB8478F05076B,
	Salsa_RTVoice_Native_OnEnable_m191C846DE66FD3FC91B6F35FE468169CE647B0F1,
	Salsa_RTVoice_Native_OnDisable_mA6BC7E9A718F67939F9EAB522145E93B40ED1179,
	Salsa_RTVoice_Native_OnDestroy_mC616E5F1F74316186E039DF52BBA562E2B625518,
	Salsa_RTVoice_Native_Speaker_OnSpeakCurrentViseme_m4836938D966FA3CDB69AEE630A68EDFF40A87F93,
	Salsa_RTVoice_Native_Awake_m76AB606F34ECE2F59333F1EC0D11FAC70FADFAC5,
	Salsa_RTVoice_Native_LateUpdate_m95C72053DD1FFE0D3F2A1840FEAC92125ACD8DAB,
	Salsa_RTVoice_Native__ctor_m61170D69EE4DA84AF50E3CAF0D782C1EB6BB143B,
	AmplitudeSALSA_Reset_m25E64A5391C665AE5C914221B02A64E0EC6A7AE7,
	AmplitudeSALSA_Start_m812CFF867CA78BB09F672824EB1BF587486927BF,
	AmplitudeSALSA_UpdateSalsa_m30EF64E1901B9844290E461FCDBD5F3F5C10229B,
	AmplitudeSALSA_SetSalsaType_m79323879203598B46148C3C464629AC09138DF5C,
	AmplitudeSALSA_FindAmplitude_m36E43773DDD23EEBAF550C534737F7069A76E954,
	AmplitudeSALSA__ctor_mCFCDD70B0AD3F39E464E2026A7107D719191B99E,
	U3CUpdateSalsaU3Ed__9__ctor_m9E79A0DBE4A266CD6F3EFDD9915425F97D9E4691,
	U3CUpdateSalsaU3Ed__9_System_IDisposable_Dispose_m75FE562BE04E0C9573F9F48D24AAF8365EE167DC,
	U3CUpdateSalsaU3Ed__9_MoveNext_m1C84433A452B1F41CEAC17E439DFC20C3B18A766,
	U3CUpdateSalsaU3Ed__9_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m3C11D377A669748A16C41925BE8009627AD31B44,
	U3CUpdateSalsaU3Ed__9_System_Collections_IEnumerator_Reset_m5E33580B54E8D416AD732AE2CB200958FD929F97,
	U3CUpdateSalsaU3Ed__9_System_Collections_IEnumerator_get_Current_m957517B4EE9E38BEA9FAA9671E7D504AF61A9439,
	U3CPrivateImplementationDetailsU3E_ComputeStringHash_mD94B0E22EF32AD3DFD277ED8E911B5DFA4CDB91E,
};
extern void U3CStartU3Ed__0_MoveNext_mBDA40DCEF83D7125F6056B9FEDBFAC0514D18D79_AdjustorThunk (void);
extern void U3CStartU3Ed__0_SetStateMachine_m13EE83714ABD4D5D9AB518554FDFA48D6E3D5B3D_AdjustorThunk (void);
extern void FrameData__ctor_m998B6E32258EF155CA31AFB8D53307247807765F_AdjustorThunk (void);
extern void Frame_CalculateWeight_mB09982C3C7BCBBB4235ADE144EF1BB1AB31DE077_AdjustorThunk (void);
extern void Frame_Release_mC179B110B1D26F0A5B7EEED6AE18661F88695A74_AdjustorThunk (void);
extern void Frame_MakeRecord_m76C2721EA40FE50536656F9CDCEB46FA9ED9FEAE_AdjustorThunk (void);
extern void Frame_MakeRecordRaw_m99E90CFFDF04BA32505153D2A005A5AF23ADD9AB_AdjustorThunk (void);
extern void BloomSettings_set_thresholdLinear_m5D681343998DD1370FF502B96D0CD7990A8843BA_AdjustorThunk (void);
extern void BloomSettings_get_thresholdLinear_mAF02ADBEE0AA8D764116FFCA6E02317C44191C55_AdjustorThunk (void);
extern void AudioUrl__ctor_m161DA67A8471A56C088D42472EB640CA99D6554C_AdjustorThunk (void);
extern void Notification__ctor_mD2703FBF62656074E25AFB11B5532DA4FA0CA27D_AdjustorThunk (void);
static Il2CppTokenAdjustorThunkPair s_adjustorThunks[11] = 
{
	{ 0x0600011D, U3CStartU3Ed__0_MoveNext_mBDA40DCEF83D7125F6056B9FEDBFAC0514D18D79_AdjustorThunk },
	{ 0x0600011E, U3CStartU3Ed__0_SetStateMachine_m13EE83714ABD4D5D9AB518554FDFA48D6E3D5B3D_AdjustorThunk },
	{ 0x06000546, FrameData__ctor_m998B6E32258EF155CA31AFB8D53307247807765F_AdjustorThunk },
	{ 0x0600087F, Frame_CalculateWeight_mB09982C3C7BCBBB4235ADE144EF1BB1AB31DE077_AdjustorThunk },
	{ 0x06000880, Frame_Release_mC179B110B1D26F0A5B7EEED6AE18661F88695A74_AdjustorThunk },
	{ 0x06000881, Frame_MakeRecord_m76C2721EA40FE50536656F9CDCEB46FA9ED9FEAE_AdjustorThunk },
	{ 0x06000882, Frame_MakeRecordRaw_m99E90CFFDF04BA32505153D2A005A5AF23ADD9AB_AdjustorThunk },
	{ 0x060008B4, BloomSettings_set_thresholdLinear_m5D681343998DD1370FF502B96D0CD7990A8843BA_AdjustorThunk },
	{ 0x060008B5, BloomSettings_get_thresholdLinear_mAF02ADBEE0AA8D764116FFCA6E02317C44191C55_AdjustorThunk },
	{ 0x06000AEB, AudioUrl__ctor_m161DA67A8471A56C088D42472EB640CA99D6554C_AdjustorThunk },
	{ 0x06000AEC, Notification__ctor_mD2703FBF62656074E25AFB11B5532DA4FA0CA27D_AdjustorThunk },
};
static const int32_t s_InvokerIndices[2997] = 
{
	3479,
	3479,
	2303,
	2887,
	2887,
	1743,
	2887,
	2886,
	3479,
	2870,
	3479,
	3451,
	3414,
	3479,
	3414,
	3479,
	3479,
	2817,
	3479,
	2887,
	2886,
	3479,
	3479,
	3479,
	1747,
	1747,
	1747,
	1747,
	3479,
	3479,
	3479,
	1747,
	1747,
	3479,
	3479,
	3479,
	1747,
	3479,
	3479,
	3479,
	3479,
	3479,
	3479,
	3414,
	3479,
	2870,
	3479,
	3451,
	3414,
	3479,
	3414,
	3479,
	3479,
	3479,
	3479,
	3479,
	3479,
	3479,
	3479,
	3479,
	3479,
	4681,
	3479,
	3479,
	3479,
	3479,
	3479,
	3479,
	3479,
	5226,
	1747,
	3479,
	3479,
	3479,
	3479,
	3479,
	3479,
	3479,
	2887,
	2887,
	1142,
	1747,
	2887,
	2887,
	3479,
	2300,
	2300,
	2300,
	2887,
	2887,
	5226,
	3479,
	1743,
	3479,
	3479,
	3479,
	3479,
	3479,
	3479,
	3479,
	2887,
	1753,
	3479,
	3479,
	3479,
	3479,
	3479,
	3479,
	3479,
	3479,
	3479,
	3479,
	3479,
	3479,
	3479,
	3479,
	3479,
	3479,
	3479,
	3479,
	3414,
	3479,
	3479,
	3479,
	2870,
	3479,
	3451,
	3414,
	3479,
	3414,
	3479,
	3479,
	3479,
	3479,
	3479,
	3479,
	3479,
	3479,
	3479,
	3479,
	3479,
	3479,
	3479,
	3479,
	3479,
	3479,
	3479,
	3479,
	3479,
	3479,
	3479,
	3479,
	3479,
	3479,
	3479,
	2922,
	2922,
	2922,
	1774,
	2887,
	3479,
	2887,
	2887,
	2887,
	2887,
	-1,
	3479,
	3479,
	2887,
	2887,
	3479,
	3479,
	3479,
	2887,
	2887,
	2887,
	2300,
	3479,
	3479,
	2887,
	5066,
	3479,
	2300,
	2887,
	3479,
	2870,
	3479,
	3451,
	3414,
	3479,
	3414,
	3479,
	2887,
	2887,
	3479,
	3479,
	3479,
	3479,
	3479,
	2922,
	3479,
	3479,
	3414,
	3479,
	2870,
	3479,
	3451,
	3414,
	3479,
	3414,
	3479,
	3479,
	3479,
	3479,
	3479,
	3479,
	3479,
	3479,
	2887,
	3479,
	3479,
	3414,
	3479,
	2870,
	3479,
	3451,
	3414,
	3479,
	3414,
	3414,
	3479,
	2545,
	3479,
	2870,
	3479,
	3451,
	3414,
	3479,
	3414,
	1738,
	3414,
	3414,
	2887,
	3414,
	2545,
	2887,
	3414,
	3479,
	2887,
	3479,
	3479,
	3479,
	2887,
	2887,
	2870,
	3479,
	3451,
	3414,
	3479,
	3414,
	2870,
	3479,
	3451,
	3414,
	3479,
	3414,
	2870,
	3479,
	3451,
	3414,
	3479,
	3414,
	2870,
	3479,
	3451,
	3414,
	3479,
	3414,
	3414,
	1747,
	3479,
	3479,
	3414,
	3414,
	3479,
	2887,
	3414,
	3479,
	3479,
	3479,
	2870,
	3479,
	3451,
	3479,
	3414,
	3479,
	3414,
	5066,
	3479,
	3479,
	3479,
	3479,
	3479,
	5159,
	5159,
	3479,
	3479,
	3479,
	3479,
	3479,
	3479,
	3479,
	3479,
	3479,
	3479,
	3479,
	3479,
	3479,
	5208,
	2887,
	2887,
	3479,
	2887,
	3479,
	3479,
	3414,
	2297,
	3479,
	1745,
	2887,
	942,
	2887,
	3479,
	3414,
	2887,
	3479,
	2887,
	3414,
	2887,
	3479,
	3479,
	3479,
	3479,
	3414,
	3414,
	1747,
	3479,
	3479,
	3479,
	2870,
	3479,
	3451,
	3414,
	3479,
	3414,
	2870,
	3479,
	3451,
	3414,
	3479,
	3414,
	2887,
	3479,
	3479,
	3414,
	2300,
	2920,
	3414,
	3479,
	3414,
	2297,
	2297,
	2545,
	2870,
	3479,
	2887,
	2870,
	2870,
	2870,
	3479,
	3451,
	3414,
	3479,
	3414,
	2870,
	3479,
	3451,
	3414,
	3479,
	3414,
	2870,
	3479,
	3451,
	3414,
	3479,
	3414,
	2870,
	3479,
	3451,
	3414,
	3479,
	3414,
	2870,
	3479,
	3451,
	3414,
	3479,
	3414,
	3479,
	3479,
	3479,
	3479,
	3414,
	3479,
	3414,
	5226,
	3479,
	3479,
	3414,
	3479,
	3414,
	2297,
	3414,
	2300,
	2887,
	3414,
	2887,
	1747,
	1747,
	3414,
	3479,
	2870,
	3479,
	3451,
	3414,
	3479,
	3414,
	2870,
	3479,
	3451,
	3414,
	3479,
	3414,
	2870,
	3479,
	3451,
	3414,
	3479,
	3414,
	2870,
	3479,
	3451,
	3414,
	3479,
	3414,
	2870,
	3479,
	3451,
	3414,
	3479,
	3414,
	2870,
	3479,
	3451,
	3414,
	3479,
	3414,
	2870,
	3479,
	3451,
	3414,
	3479,
	3414,
	2870,
	3479,
	3451,
	3414,
	3479,
	3414,
	5226,
	3479,
	2300,
	2545,
	2300,
	2300,
	2300,
	2545,
	2300,
	2300,
	2870,
	3479,
	3451,
	3414,
	3479,
	3414,
	3479,
	3479,
	2302,
	3414,
	3414,
	3479,
	2870,
	3479,
	3451,
	3414,
	3479,
	3414,
	2870,
	3479,
	3451,
	3414,
	3479,
	3414,
	2870,
	3479,
	3451,
	3414,
	3479,
	3414,
	3479,
	3414,
	3479,
	2870,
	3479,
	3451,
	3414,
	3479,
	3414,
	3479,
	3479,
	2887,
	3479,
	3479,
	3479,
	2300,
	3414,
	3414,
	3414,
	3414,
	2300,
	3479,
	5226,
	3479,
	685,
	685,
	2870,
	3479,
	3451,
	3479,
	3414,
	3479,
	3414,
	2870,
	3479,
	3451,
	3414,
	3479,
	3414,
	2870,
	3479,
	3451,
	3414,
	3479,
	3414,
	2870,
	3479,
	3451,
	3414,
	3479,
	3414,
	3479,
	3479,
	3479,
	2887,
	2887,
	3414,
	3479,
	3414,
	3479,
	2870,
	3479,
	3451,
	3414,
	3479,
	3414,
	2870,
	3479,
	3451,
	3414,
	3479,
	3414,
	3479,
	3479,
	3479,
	3479,
	3479,
	3479,
	3479,
	3479,
	3479,
	3479,
	3479,
	3479,
	3479,
	3479,
	3479,
	5226,
	3479,
	3479,
	3479,
	3479,
	3479,
	2303,
	3479,
	2870,
	3479,
	3451,
	3414,
	3479,
	3414,
	3479,
	3479,
	3479,
	3414,
	3479,
	3479,
	3479,
	3479,
	3414,
	3479,
	2870,
	3479,
	3451,
	3414,
	3479,
	3414,
	2870,
	3479,
	3451,
	3414,
	3479,
	3414,
	5226,
	3479,
	5226,
	3479,
	3479,
	3479,
	3479,
	3479,
	3479,
	3479,
	3479,
	3479,
	2887,
	1301,
	3479,
	3479,
	3479,
	3479,
	3479,
	3479,
	3479,
	3479,
	3414,
	3479,
	3479,
	3479,
	2887,
	2887,
	2294,
	2294,
	3414,
	3479,
	2297,
	2300,
	2300,
	3479,
	3479,
	2300,
	1747,
	2887,
	2887,
	3414,
	2887,
	2887,
	2887,
	2887,
	3414,
	3414,
	3479,
	3479,
	2870,
	3479,
	3451,
	3414,
	3479,
	3414,
	2870,
	3479,
	3451,
	3414,
	3479,
	3414,
	2870,
	3479,
	3451,
	3414,
	3479,
	3414,
	2870,
	3479,
	3451,
	3414,
	3479,
	3414,
	2870,
	3479,
	3451,
	3414,
	3479,
	3414,
	2870,
	3479,
	3451,
	3414,
	3479,
	3414,
	2870,
	3479,
	3451,
	3414,
	3479,
	3414,
	2870,
	3479,
	3451,
	3414,
	3479,
	3414,
	2870,
	3479,
	3451,
	3414,
	3479,
	3414,
	2870,
	3479,
	3451,
	3414,
	3479,
	3414,
	2870,
	3479,
	3451,
	3414,
	3479,
	3414,
	3479,
	3479,
	2300,
	3479,
	3479,
	3479,
	3479,
	3479,
	3479,
	3414,
	3479,
	2870,
	3479,
	3451,
	3414,
	3479,
	3414,
	3479,
	3479,
	3479,
	2300,
	2887,
	3479,
	3479,
	2870,
	3479,
	3451,
	3414,
	3479,
	3414,
	3479,
	3479,
	3479,
	3414,
	3479,
	3479,
	3479,
	3479,
	3479,
	1747,
	2300,
	3414,
	3479,
	3414,
	3414,
	3479,
	3479,
	3414,
	3479,
	3414,
	3479,
	3479,
	3479,
	3414,
	3479,
	3479,
	3479,
	2870,
	3479,
	3451,
	3414,
	3479,
	3414,
	3479,
	3479,
	3479,
	3479,
	2870,
	3479,
	3451,
	3414,
	3479,
	3414,
	2870,
	3479,
	3451,
	3414,
	3479,
	3414,
	2870,
	3479,
	3451,
	3414,
	3479,
	3414,
	2870,
	3479,
	3451,
	3414,
	3479,
	3414,
	2870,
	3479,
	3451,
	3414,
	3479,
	3414,
	2870,
	3479,
	3451,
	3414,
	3479,
	3414,
	2870,
	3479,
	3451,
	3414,
	3479,
	3414,
	3479,
	2300,
	3414,
	3414,
	2887,
	2887,
	3414,
	3414,
	3479,
	3479,
	3479,
	2870,
	3479,
	3451,
	3414,
	3479,
	3414,
	2870,
	3479,
	3451,
	3414,
	3479,
	3414,
	2870,
	3479,
	3451,
	3414,
	3479,
	3414,
	2870,
	3479,
	3451,
	3414,
	3479,
	3414,
	2870,
	3479,
	3451,
	3414,
	3479,
	3414,
	3479,
	2302,
	3479,
	2870,
	3479,
	3451,
	3414,
	3479,
	3414,
	3479,
	3479,
	3479,
	3479,
	3479,
	2297,
	3479,
	2870,
	3479,
	3451,
	3414,
	3479,
	3414,
	3479,
	3479,
	3479,
	3479,
	3479,
	3479,
	3479,
	3479,
	3479,
	3479,
	3479,
	3479,
	3414,
	3479,
	2303,
	3479,
	2870,
	3479,
	3451,
	3414,
	3479,
	3414,
	2870,
	3479,
	3451,
	3414,
	3479,
	3414,
	3479,
	3414,
	2545,
	3479,
	3479,
	2887,
	3479,
	3479,
	3414,
	3414,
	3479,
	3414,
	3479,
	3479,
	3479,
	2887,
	2870,
	2870,
	2870,
	3479,
	3451,
	3414,
	3479,
	3414,
	2870,
	3479,
	3451,
	3414,
	3479,
	3414,
	2870,
	3479,
	3451,
	3414,
	3479,
	3414,
	2870,
	3479,
	3451,
	3414,
	3479,
	3414,
	845,
	3479,
	845,
	3479,
	3414,
	2887,
	3414,
	2887,
	3414,
	2887,
	3414,
	2887,
	3414,
	2887,
	3479,
	3479,
	2887,
	2887,
	1488,
	1488,
	1130,
	1130,
	1140,
	3479,
	3479,
	3479,
	3479,
	3479,
	3479,
	3414,
	3479,
	2870,
	3479,
	3451,
	3414,
	3479,
	3414,
	3414,
	3479,
	2870,
	3479,
	3451,
	3414,
	3479,
	3414,
	3479,
	3479,
	3479,
	3479,
	3479,
	3479,
	3479,
	3479,
	3479,
	3479,
	3479,
	3479,
	3479,
	3479,
	3479,
	3479,
	3479,
	3414,
	3479,
	2870,
	3479,
	3451,
	3414,
	3479,
	3414,
	3479,
	3479,
	3479,
	3479,
	3479,
	2300,
	3414,
	3479,
	2870,
	3479,
	3451,
	3414,
	3479,
	3414,
	3479,
	3479,
	3479,
	3479,
	3479,
	3479,
	2870,
	3479,
	3479,
	3479,
	1488,
	1488,
	1130,
	1130,
	1140,
	3479,
	3479,
	3479,
	3479,
	2887,
	2887,
	3479,
	3479,
	3479,
	3479,
	2887,
	3479,
	2887,
	2887,
	2887,
	2887,
	2870,
	3479,
	3479,
	3479,
	3479,
	2870,
	3479,
	3479,
	2870,
	3479,
	3479,
	3414,
	3479,
	2870,
	3479,
	3451,
	3414,
	3479,
	3414,
	3479,
	3479,
	3479,
	3479,
	2887,
	2300,
	2300,
	3479,
	2870,
	3479,
	3451,
	3414,
	3479,
	3414,
	2870,
	3479,
	3451,
	3414,
	3479,
	3414,
	3479,
	3479,
	3414,
	3414,
	3479,
	2870,
	3479,
	3451,
	3414,
	3479,
	3414,
	2870,
	3479,
	3451,
	3414,
	3479,
	3414,
	3479,
	3479,
	3479,
	3479,
	3479,
	3414,
	3479,
	2870,
	3479,
	3451,
	3414,
	3479,
	3414,
	3479,
	3479,
	3479,
	3479,
	2887,
	3414,
	3479,
	2870,
	3479,
	3451,
	3414,
	3479,
	3414,
	3479,
	3479,
	3479,
	3479,
	2887,
	3414,
	3479,
	2870,
	3479,
	3451,
	3414,
	3479,
	3414,
	3479,
	3479,
	3479,
	3479,
	2887,
	3414,
	3479,
	2870,
	3479,
	3451,
	3414,
	3479,
	3414,
	3479,
	3479,
	3479,
	3479,
	2887,
	3414,
	3479,
	3479,
	1227,
	2870,
	3479,
	3451,
	3414,
	3479,
	3414,
	3479,
	3479,
	2300,
	3414,
	3479,
	2870,
	3479,
	3451,
	3414,
	3479,
	3414,
	3397,
	2870,
	3414,
	2887,
	3479,
	3479,
	3479,
	3479,
	3479,
	1743,
	4326,
	4849,
	5158,
	5158,
	3479,
	3397,
	3397,
	3397,
	3397,
	3397,
	3397,
	3414,
	2887,
	3479,
	3479,
	3479,
	3479,
	3451,
	3451,
	1747,
	1747,
	2887,
	1747,
	1142,
	3479,
	3397,
	2870,
	3414,
	2887,
	3479,
	3479,
	3479,
	3479,
	3479,
	3479,
	5208,
	5159,
	5208,
	5208,
	3479,
	5226,
	3414,
	3479,
	2297,
	3479,
	3479,
	2870,
	3479,
	3451,
	3414,
	3479,
	3414,
	5208,
	5063,
	5066,
	5066,
	5066,
	5066,
	5066,
	5111,
	3479,
	3479,
	2009,
	5108,
	3479,
	3397,
	2870,
	3479,
	3479,
	3479,
	3479,
	3479,
	1747,
	3479,
	3479,
	3479,
	3414,
	3479,
	3479,
	3479,
	3479,
	3683,
	4494,
	5158,
	5158,
	3683,
	4494,
	5158,
	5158,
	3479,
	1743,
	2870,
	3479,
	3451,
	3414,
	3479,
	3414,
	1747,
	3451,
	3838,
	5158,
	5158,
	3414,
	2887,
	3479,
	3479,
	3479,
	3479,
	3479,
	3479,
	3479,
	3479,
	3451,
	3479,
	3479,
	3479,
	3414,
	2887,
	3479,
	3479,
	3479,
	3479,
	3479,
	1747,
	3479,
	3479,
	3479,
	3479,
	3479,
	3451,
	3479,
	3414,
	3479,
	379,
	2887,
	2887,
	2887,
	3734,
	5226,
	5226,
	4513,
	5226,
	4511,
	5226,
	4495,
	3479,
	2870,
	3479,
	3451,
	3414,
	3479,
	3414,
	3479,
	3479,
	3479,
	3479,
	3414,
	3414,
	3479,
	2870,
	3479,
	3451,
	3414,
	3479,
	3414,
	2870,
	3479,
	3451,
	3414,
	3479,
	3414,
	3479,
	3479,
	3479,
	3479,
	3479,
	3479,
	3479,
	3479,
	3479,
	3479,
	3479,
	3479,
	3479,
	3479,
	3479,
	3479,
	3479,
	3479,
	1745,
	2870,
	919,
	2887,
	1745,
	3479,
	1315,
	2887,
	3479,
	3479,
	3479,
	5108,
	3479,
	4862,
	3479,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	2887,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	3479,
	2887,
	3479,
	2303,
	2300,
	2300,
	2922,
	2545,
	2303,
	2300,
	2300,
	2545,
	2300,
	2922,
	2300,
	3479,
	3479,
	2592,
	3479,
	2592,
	3397,
	2300,
	1747,
	2887,
	3479,
	2300,
	-1,
	2300,
	2300,
	-1,
	1315,
	1315,
	-1,
	942,
	942,
	2300,
	-1,
	2300,
	2300,
	-1,
	2300,
	2300,
	-1,
	2300,
	3397,
	3479,
	2922,
	3397,
	3414,
	2887,
	3414,
	2887,
	5159,
	5159,
	5208,
	3397,
	3414,
	2887,
	3397,
	2870,
	3479,
	2887,
	5202,
	1747,
	1747,
	1747,
	1142,
	1747,
	1145,
	3479,
	2887,
	3479,
	2922,
	2887,
	3479,
	2922,
	1747,
	2887,
	3479,
	2300,
	2300,
	-1,
	2300,
	2300,
	-1,
	1315,
	1315,
	-1,
	942,
	942,
	1142,
	1747,
	2300,
	-1,
	5066,
	5066,
	2300,
	5066,
	5066,
	2300,
	-1,
	5066,
	5066,
	5208,
	5066,
	2300,
	2300,
	-1,
	2300,
	4862,
	5226,
	2981,
	2887,
	2887,
	2887,
	3479,
	2912,
	3479,
	2898,
	3479,
	3479,
	2887,
	2922,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	3479,
	3479,
	2922,
	3479,
	2887,
	2887,
	3479,
	3479,
	2887,
	3479,
	3414,
	-1,
	-1,
	3479,
	1743,
	2887,
	3479,
	2922,
	3479,
	3479,
	3414,
	3479,
	1315,
	3479,
	3479,
	3414,
	2922,
	3479,
	3414,
	-1,
	-1,
	3479,
	1743,
	2887,
	3479,
	3479,
	2922,
	3479,
	3479,
	2887,
	3479,
	3479,
	2887,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	3479,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	3479,
	2887,
	1747,
	3479,
	2887,
	1747,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	4679,
	5066,
	4679,
	4529,
	4679,
	-1,
	-1,
	2870,
	3479,
	3451,
	3479,
	3414,
	3479,
	3414,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	3451,
	2920,
	3451,
	2920,
	3398,
	2871,
	3414,
	2887,
	3414,
	2887,
	3479,
	2887,
	1747,
	427,
	3414,
	2887,
	3414,
	2887,
	3414,
	2887,
	3414,
	2887,
	3414,
	2887,
	3300,
	2772,
	3414,
	2887,
	3397,
	2870,
	3453,
	2922,
	3414,
	2887,
	3451,
	2920,
	3294,
	2762,
	3300,
	2772,
	3451,
	2920,
	3414,
	2887,
	3414,
	2887,
	3414,
	2887,
	3414,
	2887,
	3414,
	2887,
	3414,
	2887,
	3414,
	2887,
	3414,
	2887,
	3451,
	2920,
	3414,
	2887,
	3453,
	3398,
	3453,
	3398,
	2300,
	3451,
	2920,
	3451,
	2920,
	3479,
	3479,
	3414,
	2887,
	2887,
	3398,
	3414,
	3414,
	3414,
	3414,
	2300,
	3414,
	5208,
	5066,
	3479,
	5208,
	5208,
	5159,
	5226,
	5208,
	5159,
	5226,
	4862,
	-1,
	4862,
	4862,
	-1,
	-1,
	-1,
	-1,
	4513,
	4513,
	4862,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	4513,
	4513,
	4862,
	-1,
	-1,
	-1,
	4862,
	4862,
	4862,
	4862,
	5066,
	-1,
	5066,
	5066,
	-1,
	-1,
	-1,
	-1,
	-1,
	4679,
	4679,
	5066,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	4679,
	4679,
	5066,
	-1,
	-1,
	-1,
	5066,
	5066,
	5066,
	5066,
	-1,
	-1,
	4649,
	4234,
	4679,
	2870,
	3479,
	3451,
	3414,
	3479,
	3414,
	5066,
	4759,
	5066,
	4679,
	3479,
	2404,
	5226,
	3479,
	2278,
	3414,
	3479,
	3414,
	3479,
	3414,
	3479,
	3414,
	3479,
	2887,
	2922,
	2887,
	3479,
	3397,
	3451,
	3451,
	3397,
	3414,
	3397,
	2887,
	3479,
	5226,
	3451,
	1142,
	3479,
	5226,
	3451,
	3397,
	3397,
	3414,
	2887,
	2887,
	2887,
	2887,
	3479,
	3479,
	3479,
	5226,
	3414,
	2887,
	3397,
	2870,
	3397,
	2870,
	1612,
	3479,
	3479,
	3451,
	3479,
	2887,
	3479,
	5226,
	3451,
	2657,
	1461,
	1461,
	4922,
	4450,
	5142,
	5142,
	5142,
	3730,
	5142,
	5142,
	5142,
	3730,
	3397,
	3414,
	2545,
	3397,
	3479,
	2887,
	3479,
	3479,
	3479,
	5226,
	3451,
	3397,
	3453,
	2654,
	1373,
	1227,
	419,
	3479,
	3479,
	5226,
	3451,
	3479,
	3479,
	2887,
	3479,
	5226,
	3451,
	3479,
	3479,
	3479,
	3477,
	1315,
	3479,
	3479,
	5226,
	3451,
	3414,
	3397,
	3397,
	2887,
	3479,
	5226,
	3451,
	1747,
	3479,
	5226,
	3451,
	3479,
	2887,
	3479,
	5226,
	3414,
	3414,
	3451,
	3414,
	3479,
	3397,
	3397,
	3479,
	2887,
	3479,
	3479,
	5226,
	3479,
	3479,
	3451,
	247,
	3479,
	3479,
	422,
	431,
	5216,
	5202,
	3070,
	1454,
	3479,
	422,
	421,
	3397,
	3451,
	3479,
	3414,
	3397,
	2887,
	3479,
	5226,
	3451,
	3397,
	3474,
	2942,
	3479,
	2887,
	1747,
	1445,
	3474,
	2271,
	2271,
	3479,
	3479,
	5226,
	3451,
	2887,
	3479,
	3479,
	5226,
	3451,
	2887,
	3479,
	5226,
	3487,
	2954,
	3479,
	3479,
	5229,
	3491,
	2958,
	3479,
	3479,
	5226,
	5226,
	5230,
	5232,
	5231,
	3494,
	2961,
	3479,
	3479,
	2922,
	3453,
	5233,
	5234,
	5235,
	3495,
	2962,
	3451,
	3479,
	2528,
	3479,
	5236,
	5237,
	5238,
	3497,
	2964,
	3479,
	3479,
	5239,
	3499,
	2966,
	3451,
	2920,
	3414,
	2887,
	3479,
	3479,
	3479,
	5247,
	5240,
	5241,
	5245,
	5244,
	5242,
	5243,
	5246,
	3506,
	2970,
	3479,
	3479,
	5248,
	3507,
	2971,
	3479,
	3479,
	5249,
	3509,
	2972,
	3479,
	3479,
	5250,
	3510,
	2973,
	3479,
	3479,
	5251,
	3511,
	2974,
	3479,
	3479,
	5252,
	3518,
	2979,
	3479,
	3479,
	5254,
	3522,
	2983,
	3479,
	3479,
	5255,
	3535,
	2993,
	3479,
	3479,
	5257,
	3538,
	2996,
	3479,
	3479,
	5258,
	3479,
	3479,
	3479,
	3479,
	1747,
	3479,
	3479,
	3479,
	3479,
	3479,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	3479,
	3397,
	3451,
	3479,
	3479,
	3414,
	3479,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	3451,
	2920,
	3479,
	3414,
	3451,
	3451,
	3397,
	3397,
	3438,
	3479,
	3451,
	2920,
	3479,
	3479,
	3479,
	3479,
	800,
	3479,
	2657,
	5216,
	5216,
	5208,
	5208,
	4857,
	3748,
	5159,
	5226,
	3479,
	2300,
	3479,
	3479,
	2300,
	55,
	2887,
	3479,
	3479,
	5208,
	4117,
	5111,
	4497,
	4998,
	5159,
	5216,
	5226,
	5216,
	4722,
	3479,
	3479,
	3479,
	3398,
	2871,
	3397,
	2870,
	3414,
	2887,
	3414,
	2887,
	250,
	3479,
	3451,
	3414,
	3414,
	3398,
	415,
	3479,
	3479,
	3398,
	2871,
	3397,
	2870,
	3414,
	2887,
	3414,
	2887,
	3414,
	2887,
	3398,
	2871,
	403,
	2887,
	3414,
	3451,
	2887,
	2887,
	3479,
	3479,
	3479,
	298,
	2529,
	3397,
	3479,
	2545,
	3479,
	3479,
	3479,
	5208,
	3479,
	5226,
	3479,
	3479,
	-1,
	-1,
	4104,
	4104,
	4365,
	4104,
	4104,
	4376,
	5066,
	5066,
	5066,
	4681,
	4122,
	3875,
	5066,
	4862,
	4862,
	4376,
	5066,
	5066,
	5066,
	4681,
	4122,
	3875,
	5066,
	4862,
	5159,
	2887,
	2887,
	2887,
	2887,
	2887,
	2887,
	2887,
	2887,
	2887,
	2887,
	2887,
	2887,
	3451,
	3414,
	3414,
	3414,
	3451,
	3453,
	3453,
	2920,
	3479,
	3451,
	3451,
	2887,
	3414,
	2887,
	918,
	3451,
	2887,
	2887,
	2887,
	2887,
	2887,
	2887,
	2887,
	2887,
	2887,
	2887,
	2887,
	2887,
	2887,
	2887,
	2887,
	2887,
	2887,
	3414,
	2887,
	2529,
	3397,
	2251,
	2251,
	2251,
	540,
	2545,
	2870,
	2887,
	2887,
	2887,
	2887,
	2887,
	2887,
	2887,
	2887,
	2887,
	2887,
	2887,
	2887,
	3451,
	2920,
	3414,
	2887,
	3414,
	2887,
	3414,
	2887,
	3451,
	2920,
	3479,
	3479,
	3479,
	3453,
	3453,
	2920,
	3479,
	3451,
	3451,
	2887,
	3414,
	2887,
	918,
	3451,
	2887,
	3479,
	3479,
	2870,
	3479,
	3451,
	3414,
	3479,
	3414,
	2887,
	2887,
	2887,
	2887,
	2887,
	2887,
	2887,
	2887,
	2887,
	2887,
	2887,
	2887,
	2887,
	2887,
	2887,
	2887,
	3414,
	2887,
	3479,
	3479,
	3479,
	2887,
	2529,
	3397,
	2251,
	2251,
	2251,
	540,
	2300,
	2887,
	3479,
	3479,
	3479,
	3479,
	2545,
	2870,
	2545,
	3479,
	2887,
	3479,
	3479,
	3479,
	5208,
	3479,
	3479,
	3479,
	3479,
	3479,
	3479,
	3479,
	3479,
	3479,
	3479,
	3479,
	3479,
	3479,
	3479,
	3479,
	4362,
	5208,
	2887,
	2887,
	2887,
	2887,
	2887,
	2887,
	2887,
	2887,
	2887,
	2887,
	2887,
	2887,
	2887,
	2887,
	2887,
	2887,
	2887,
	2887,
	2887,
	2887,
	2887,
	2887,
	2887,
	2887,
	2887,
	2887,
	3451,
	3414,
	3451,
	3414,
	3479,
	3479,
	3479,
	3453,
	3453,
	2920,
	3479,
	3479,
	3451,
	3414,
	3451,
	2887,
	2887,
	2251,
	2251,
	2251,
	540,
	2529,
	3397,
	3451,
	2887,
	2887,
	2887,
	2887,
	2887,
	3479,
	3479,
	1747,
	3479,
	1747,
	2887,
	2887,
	2887,
	2887,
	3479,
	3479,
	3479,
	3479,
	3479,
	3479,
	3479,
	1747,
	1747,
	2887,
	2887,
	2887,
	3479,
	5226,
	3479,
	2545,
	3479,
	3479,
	3479,
	3479,
	2870,
	3479,
	3479,
	3479,
	3479,
	3479,
	3479,
	3479,
	3479,
	3479,
	3479,
	1747,
	1747,
	2887,
	2887,
	2887,
	2887,
	2887,
	2887,
	2887,
	2887,
	2887,
	3479,
	5226,
	3479,
	2545,
	2545,
	3479,
	3479,
	3479,
	3479,
	2870,
	3479,
	3479,
	3479,
	3479,
	3479,
	3479,
	3479,
	3479,
	3479,
	3479,
	3479,
	1747,
	780,
	1747,
	2887,
	2887,
	2887,
	2887,
	2887,
	2887,
	3479,
	5226,
	3479,
	2545,
	2545,
	3479,
	3479,
	2922,
	2870,
	2870,
	3479,
	3479,
	3479,
	3479,
	3479,
	3479,
	3479,
	3479,
	3479,
	2303,
	3479,
	3913,
	3479,
	2870,
	3479,
	3451,
	3414,
	3479,
	3414,
	1743,
	1100,
	3397,
	3451,
	2287,
	3414,
	2287,
	3414,
	2887,
	1747,
	1077,
	1473,
	1747,
	3479,
	3479,
	-1,
	-1,
	-1,
	-1,
	1745,
	2886,
	928,
	2887,
	2870,
	3479,
	3451,
	3414,
	3479,
	3414,
	2870,
	3479,
	3451,
	3414,
	3479,
	3414,
	2870,
	3479,
	3451,
	3479,
	3414,
	3479,
	3414,
	2870,
	3479,
	3451,
	3414,
	3479,
	3414,
	3479,
	3479,
	3414,
	3479,
	2870,
	3479,
	3451,
	3414,
	3479,
	3414,
	3479,
	3479,
	3479,
	3479,
	3479,
	3479,
	3479,
	3479,
	3479,
	3479,
	3479,
	3479,
	3479,
	2300,
	1238,
	3397,
	3479,
	3479,
	3479,
	3479,
	3479,
	1102,
	3479,
	3479,
	3479,
	3479,
	3479,
	3479,
	3479,
	2870,
	3479,
	3479,
	3479,
	3479,
	3479,
	2300,
	1238,
	3397,
	3479,
	3479,
	3479,
	3479,
	3479,
	3479,
	3479,
	3479,
	3479,
	3479,
	3479,
	3479,
	3479,
	3479,
	1102,
	3479,
	3479,
	3479,
	3479,
	2887,
	3479,
	2300,
	3479,
	3479,
	3479,
	3479,
	2887,
	2887,
	2887,
	970,
	2303,
	3479,
	2870,
	3479,
	3451,
	3414,
	3479,
	3414,
	2870,
	3479,
	3451,
	3414,
	3479,
	3414,
	3479,
	3479,
	3479,
	3479,
	3479,
	3479,
	3479,
	3479,
	2887,
	2887,
	3479,
	3479,
	2887,
	3479,
	3479,
	2887,
	2887,
	3479,
	3479,
	3479,
	3479,
	3479,
	3479,
	3479,
	2887,
	3479,
	3479,
	3479,
	3479,
	2887,
	2870,
	2922,
	3479,
	3479,
	3479,
	3479,
	3479,
	3479,
	2870,
	3479,
	3479,
	3479,
	3479,
	3479,
	3479,
	3479,
	1747,
	3479,
	3479,
	3479,
	3479,
	3479,
	3414,
	2870,
	3479,
	3479,
	2870,
	3479,
	3451,
	3414,
	3479,
	3414,
	4998,
};
static const Il2CppTokenRangePair s_rgctxIndices[101] = 
{
	{ 0x02000123, { 2, 4 } },
	{ 0x02000128, { 6, 101 } },
	{ 0x02000129, { 140, 1 } },
	{ 0x0200012A, { 141, 1 } },
	{ 0x0200012C, { 142, 4 } },
	{ 0x0200012D, { 146, 13 } },
	{ 0x0200012E, { 159, 4 } },
	{ 0x0200012F, { 163, 1 } },
	{ 0x02000130, { 164, 3 } },
	{ 0x02000131, { 167, 3 } },
	{ 0x02000132, { 170, 1 } },
	{ 0x02000133, { 171, 10 } },
	{ 0x02000134, { 181, 3 } },
	{ 0x02000135, { 184, 3 } },
	{ 0x02000136, { 187, 1 } },
	{ 0x02000137, { 188, 11 } },
	{ 0x02000138, { 199, 2 } },
	{ 0x02000139, { 201, 2 } },
	{ 0x0200013D, { 232, 3 } },
	{ 0x0200013E, { 235, 8 } },
	{ 0x0200013F, { 243, 10 } },
	{ 0x02000152, { 279, 13 } },
	{ 0x02000156, { 292, 3 } },
	{ 0x0200015D, { 295, 3 } },
	{ 0x02000164, { 304, 2 } },
	{ 0x02000165, { 306, 3 } },
	{ 0x02000166, { 309, 4 } },
	{ 0x0200016A, { 321, 4 } },
	{ 0x0200016D, { 331, 2 } },
	{ 0x0200016E, { 333, 2 } },
	{ 0x020001E9, { 414, 3 } },
	{ 0x020001EA, { 417, 2 } },
	{ 0x020001EB, { 419, 2 } },
	{ 0x060000A6, { 0, 2 } },
	{ 0x060005E4, { 107, 1 } },
	{ 0x060005F2, { 108, 1 } },
	{ 0x060005F5, { 109, 1 } },
	{ 0x060005F8, { 110, 7 } },
	{ 0x060005FB, { 117, 6 } },
	{ 0x060005FE, { 123, 6 } },
	{ 0x06000602, { 129, 6 } },
	{ 0x0600060A, { 135, 5 } },
	{ 0x0600064C, { 203, 14 } },
	{ 0x0600064D, { 217, 7 } },
	{ 0x0600064E, { 224, 8 } },
	{ 0x060006AB, { 253, 1 } },
	{ 0x060006AE, { 254, 1 } },
	{ 0x060006B1, { 255, 7 } },
	{ 0x060006B7, { 262, 6 } },
	{ 0x060006BE, { 268, 6 } },
	{ 0x060006C5, { 274, 5 } },
	{ 0x0600070B, { 298, 2 } },
	{ 0x0600070C, { 300, 2 } },
	{ 0x0600070D, { 302, 2 } },
	{ 0x0600072A, { 313, 3 } },
	{ 0x0600072B, { 316, 3 } },
	{ 0x0600072C, { 319, 2 } },
	{ 0x0600073A, { 325, 3 } },
	{ 0x0600073B, { 328, 3 } },
	{ 0x06000747, { 335, 1 } },
	{ 0x06000748, { 336, 1 } },
	{ 0x06000749, { 337, 2 } },
	{ 0x0600074A, { 339, 2 } },
	{ 0x060007AA, { 341, 1 } },
	{ 0x060007AD, { 342, 1 } },
	{ 0x060007AE, { 343, 1 } },
	{ 0x060007AF, { 344, 1 } },
	{ 0x060007B0, { 345, 1 } },
	{ 0x060007B4, { 346, 1 } },
	{ 0x060007B5, { 347, 1 } },
	{ 0x060007B6, { 348, 1 } },
	{ 0x060007B7, { 349, 1 } },
	{ 0x060007B8, { 350, 1 } },
	{ 0x060007B9, { 351, 1 } },
	{ 0x060007BD, { 352, 1 } },
	{ 0x060007BE, { 353, 1 } },
	{ 0x060007BF, { 354, 1 } },
	{ 0x060007C5, { 355, 6 } },
	{ 0x060007C8, { 361, 1 } },
	{ 0x060007CA, { 362, 6 } },
	{ 0x060007CB, { 368, 1 } },
	{ 0x060007CC, { 369, 6 } },
	{ 0x060007D0, { 375, 1 } },
	{ 0x060007D1, { 376, 1 } },
	{ 0x060007D2, { 377, 6 } },
	{ 0x060007D3, { 383, 1 } },
	{ 0x060007D4, { 384, 1 } },
	{ 0x060007D5, { 385, 6 } },
	{ 0x060007D9, { 391, 1 } },
	{ 0x060007DA, { 392, 1 } },
	{ 0x060007DB, { 393, 6 } },
	{ 0x060007E0, { 399, 2 } },
	{ 0x060007E1, { 401, 1 } },
	{ 0x0600090F, { 402, 1 } },
	{ 0x06000910, { 403, 1 } },
	{ 0x06000911, { 404, 3 } },
	{ 0x06000912, { 407, 5 } },
	{ 0x06000913, { 412, 1 } },
	{ 0x06000914, { 413, 1 } },
	{ 0x0600098B, { 421, 2 } },
	{ 0x0600098C, { 423, 1 } },
};
static const Il2CppRGCTXDefinition s_rgctxValues[424] = 
{
	{ (Il2CppRGCTXDataType)3, 25399 },
	{ (Il2CppRGCTXDataType)2, 90 },
	{ (Il2CppRGCTXDataType)2, 4404 },
	{ (Il2CppRGCTXDataType)1, 604 },
	{ (Il2CppRGCTXDataType)2, 604 },
	{ (Il2CppRGCTXDataType)3, 25351 },
	{ (Il2CppRGCTXDataType)3, 19040 },
	{ (Il2CppRGCTXDataType)3, 19031 },
	{ (Il2CppRGCTXDataType)2, 1306 },
	{ (Il2CppRGCTXDataType)3, 791 },
	{ (Il2CppRGCTXDataType)3, 19030 },
	{ (Il2CppRGCTXDataType)3, 920 },
	{ (Il2CppRGCTXDataType)2, 3670 },
	{ (Il2CppRGCTXDataType)3, 12970 },
	{ (Il2CppRGCTXDataType)3, 12971 },
	{ (Il2CppRGCTXDataType)2, 1192 },
	{ (Il2CppRGCTXDataType)3, 124 },
	{ (Il2CppRGCTXDataType)3, 125 },
	{ (Il2CppRGCTXDataType)3, 19024 },
	{ (Il2CppRGCTXDataType)3, 12972 },
	{ (Il2CppRGCTXDataType)3, 12973 },
	{ (Il2CppRGCTXDataType)3, 19006 },
	{ (Il2CppRGCTXDataType)2, 1193 },
	{ (Il2CppRGCTXDataType)3, 128 },
	{ (Il2CppRGCTXDataType)3, 129 },
	{ (Il2CppRGCTXDataType)3, 19038 },
	{ (Il2CppRGCTXDataType)3, 19026 },
	{ (Il2CppRGCTXDataType)3, 19027 },
	{ (Il2CppRGCTXDataType)3, 19025 },
	{ (Il2CppRGCTXDataType)3, 19034 },
	{ (Il2CppRGCTXDataType)3, 19015 },
	{ (Il2CppRGCTXDataType)3, 19032 },
	{ (Il2CppRGCTXDataType)3, 19016 },
	{ (Il2CppRGCTXDataType)3, 19017 },
	{ (Il2CppRGCTXDataType)3, 19023 },
	{ (Il2CppRGCTXDataType)3, 19041 },
	{ (Il2CppRGCTXDataType)2, 1196 },
	{ (Il2CppRGCTXDataType)3, 140 },
	{ (Il2CppRGCTXDataType)3, 19039 },
	{ (Il2CppRGCTXDataType)3, 141 },
	{ (Il2CppRGCTXDataType)3, 142 },
	{ (Il2CppRGCTXDataType)3, 19018 },
	{ (Il2CppRGCTXDataType)3, 143 },
	{ (Il2CppRGCTXDataType)3, 19028 },
	{ (Il2CppRGCTXDataType)2, 1200 },
	{ (Il2CppRGCTXDataType)3, 160 },
	{ (Il2CppRGCTXDataType)2, 4335 },
	{ (Il2CppRGCTXDataType)3, 19014 },
	{ (Il2CppRGCTXDataType)3, 19037 },
	{ (Il2CppRGCTXDataType)3, 161 },
	{ (Il2CppRGCTXDataType)3, 162 },
	{ (Il2CppRGCTXDataType)3, 163 },
	{ (Il2CppRGCTXDataType)3, 19036 },
	{ (Il2CppRGCTXDataType)3, 19035 },
	{ (Il2CppRGCTXDataType)2, 1217 },
	{ (Il2CppRGCTXDataType)3, 274 },
	{ (Il2CppRGCTXDataType)3, 275 },
	{ (Il2CppRGCTXDataType)3, 276 },
	{ (Il2CppRGCTXDataType)2, 1220 },
	{ (Il2CppRGCTXDataType)3, 304 },
	{ (Il2CppRGCTXDataType)3, 305 },
	{ (Il2CppRGCTXDataType)3, 306 },
	{ (Il2CppRGCTXDataType)3, 19007 },
	{ (Il2CppRGCTXDataType)3, 19021 },
	{ (Il2CppRGCTXDataType)3, 19020 },
	{ (Il2CppRGCTXDataType)3, 19019 },
	{ (Il2CppRGCTXDataType)2, 1225 },
	{ (Il2CppRGCTXDataType)3, 326 },
	{ (Il2CppRGCTXDataType)3, 327 },
	{ (Il2CppRGCTXDataType)2, 2303 },
	{ (Il2CppRGCTXDataType)3, 9392 },
	{ (Il2CppRGCTXDataType)3, 19033 },
	{ (Il2CppRGCTXDataType)3, 19022 },
	{ (Il2CppRGCTXDataType)2, 4335 },
	{ (Il2CppRGCTXDataType)2, 1228 },
	{ (Il2CppRGCTXDataType)3, 340 },
	{ (Il2CppRGCTXDataType)3, 25237 },
	{ (Il2CppRGCTXDataType)3, 25168 },
	{ (Il2CppRGCTXDataType)3, 19066 },
	{ (Il2CppRGCTXDataType)2, 4348 },
	{ (Il2CppRGCTXDataType)2, 5364 },
	{ (Il2CppRGCTXDataType)2, 4348 },
	{ (Il2CppRGCTXDataType)3, 19065 },
	{ (Il2CppRGCTXDataType)3, 19067 },
	{ (Il2CppRGCTXDataType)3, 341 },
	{ (Il2CppRGCTXDataType)2, 1424 },
	{ (Il2CppRGCTXDataType)3, 921 },
	{ (Il2CppRGCTXDataType)3, 25286 },
	{ (Il2CppRGCTXDataType)2, 1234 },
	{ (Il2CppRGCTXDataType)3, 380 },
	{ (Il2CppRGCTXDataType)3, 381 },
	{ (Il2CppRGCTXDataType)3, 19029 },
	{ (Il2CppRGCTXDataType)2, 1238 },
	{ (Il2CppRGCTXDataType)3, 396 },
	{ (Il2CppRGCTXDataType)3, 397 },
	{ (Il2CppRGCTXDataType)2, 1242 },
	{ (Il2CppRGCTXDataType)3, 412 },
	{ (Il2CppRGCTXDataType)3, 413 },
	{ (Il2CppRGCTXDataType)3, 414 },
	{ (Il2CppRGCTXDataType)3, 415 },
	{ (Il2CppRGCTXDataType)2, 2293 },
	{ (Il2CppRGCTXDataType)3, 9388 },
	{ (Il2CppRGCTXDataType)3, 19010 },
	{ (Il2CppRGCTXDataType)2, 1243 },
	{ (Il2CppRGCTXDataType)3, 420 },
	{ (Il2CppRGCTXDataType)3, 421 },
	{ (Il2CppRGCTXDataType)3, 422 },
	{ (Il2CppRGCTXDataType)3, 788 },
	{ (Il2CppRGCTXDataType)3, 19012 },
	{ (Il2CppRGCTXDataType)3, 19013 },
	{ (Il2CppRGCTXDataType)2, 1215 },
	{ (Il2CppRGCTXDataType)3, 254 },
	{ (Il2CppRGCTXDataType)2, 4328 },
	{ (Il2CppRGCTXDataType)3, 18998 },
	{ (Il2CppRGCTXDataType)3, 18999 },
	{ (Il2CppRGCTXDataType)3, 255 },
	{ (Il2CppRGCTXDataType)3, 256 },
	{ (Il2CppRGCTXDataType)2, 1222 },
	{ (Il2CppRGCTXDataType)3, 314 },
	{ (Il2CppRGCTXDataType)3, 315 },
	{ (Il2CppRGCTXDataType)2, 2297 },
	{ (Il2CppRGCTXDataType)3, 9389 },
	{ (Il2CppRGCTXDataType)3, 19008 },
	{ (Il2CppRGCTXDataType)2, 1224 },
	{ (Il2CppRGCTXDataType)3, 322 },
	{ (Il2CppRGCTXDataType)3, 323 },
	{ (Il2CppRGCTXDataType)2, 2302 },
	{ (Il2CppRGCTXDataType)3, 9391 },
	{ (Il2CppRGCTXDataType)3, 19011 },
	{ (Il2CppRGCTXDataType)2, 1233 },
	{ (Il2CppRGCTXDataType)3, 376 },
	{ (Il2CppRGCTXDataType)3, 377 },
	{ (Il2CppRGCTXDataType)2, 2301 },
	{ (Il2CppRGCTXDataType)3, 9390 },
	{ (Il2CppRGCTXDataType)3, 19009 },
	{ (Il2CppRGCTXDataType)2, 1249 },
	{ (Il2CppRGCTXDataType)3, 452 },
	{ (Il2CppRGCTXDataType)3, 453 },
	{ (Il2CppRGCTXDataType)3, 454 },
	{ (Il2CppRGCTXDataType)3, 25910 },
	{ (Il2CppRGCTXDataType)3, 19047 },
	{ (Il2CppRGCTXDataType)3, 19048 },
	{ (Il2CppRGCTXDataType)3, 19051 },
	{ (Il2CppRGCTXDataType)3, 9467 },
	{ (Il2CppRGCTXDataType)3, 19049 },
	{ (Il2CppRGCTXDataType)3, 19050 },
	{ (Il2CppRGCTXDataType)3, 9406 },
	{ (Il2CppRGCTXDataType)3, 257 },
	{ (Il2CppRGCTXDataType)2, 3387 },
	{ (Il2CppRGCTXDataType)3, 258 },
	{ (Il2CppRGCTXDataType)2, 1320 },
	{ (Il2CppRGCTXDataType)3, 803 },
	{ (Il2CppRGCTXDataType)3, 259 },
	{ (Il2CppRGCTXDataType)3, 19060 },
	{ (Il2CppRGCTXDataType)3, 19061 },
	{ (Il2CppRGCTXDataType)3, 19059 },
	{ (Il2CppRGCTXDataType)3, 9469 },
	{ (Il2CppRGCTXDataType)3, 260 },
	{ (Il2CppRGCTXDataType)3, 261 },
	{ (Il2CppRGCTXDataType)3, 9407 },
	{ (Il2CppRGCTXDataType)3, 277 },
	{ (Il2CppRGCTXDataType)3, 278 },
	{ (Il2CppRGCTXDataType)3, 279 },
	{ (Il2CppRGCTXDataType)3, 795 },
	{ (Il2CppRGCTXDataType)3, 9408 },
	{ (Il2CppRGCTXDataType)3, 19062 },
	{ (Il2CppRGCTXDataType)2, 4345 },
	{ (Il2CppRGCTXDataType)3, 9409 },
	{ (Il2CppRGCTXDataType)3, 19063 },
	{ (Il2CppRGCTXDataType)2, 4346 },
	{ (Il2CppRGCTXDataType)3, 9410 },
	{ (Il2CppRGCTXDataType)2, 1231 },
	{ (Il2CppRGCTXDataType)3, 362 },
	{ (Il2CppRGCTXDataType)3, 363 },
	{ (Il2CppRGCTXDataType)2, 3382 },
	{ (Il2CppRGCTXDataType)3, 364 },
	{ (Il2CppRGCTXDataType)2, 1312 },
	{ (Il2CppRGCTXDataType)3, 796 },
	{ (Il2CppRGCTXDataType)3, 342 },
	{ (Il2CppRGCTXDataType)3, 19069 },
	{ (Il2CppRGCTXDataType)3, 19068 },
	{ (Il2CppRGCTXDataType)3, 19072 },
	{ (Il2CppRGCTXDataType)3, 19070 },
	{ (Il2CppRGCTXDataType)3, 19071 },
	{ (Il2CppRGCTXDataType)3, 9411 },
	{ (Il2CppRGCTXDataType)3, 19064 },
	{ (Il2CppRGCTXDataType)2, 4347 },
	{ (Il2CppRGCTXDataType)3, 9412 },
	{ (Il2CppRGCTXDataType)2, 1240 },
	{ (Il2CppRGCTXDataType)3, 404 },
	{ (Il2CppRGCTXDataType)3, 405 },
	{ (Il2CppRGCTXDataType)2, 3383 },
	{ (Il2CppRGCTXDataType)3, 398 },
	{ (Il2CppRGCTXDataType)2, 1313 },
	{ (Il2CppRGCTXDataType)3, 797 },
	{ (Il2CppRGCTXDataType)3, 399 },
	{ (Il2CppRGCTXDataType)3, 19054 },
	{ (Il2CppRGCTXDataType)3, 19053 },
	{ (Il2CppRGCTXDataType)3, 19052 },
	{ (Il2CppRGCTXDataType)3, 19056 },
	{ (Il2CppRGCTXDataType)3, 19055 },
	{ (Il2CppRGCTXDataType)3, 19058 },
	{ (Il2CppRGCTXDataType)3, 19057 },
	{ (Il2CppRGCTXDataType)2, 1183 },
	{ (Il2CppRGCTXDataType)3, 78 },
	{ (Il2CppRGCTXDataType)2, 4350 },
	{ (Il2CppRGCTXDataType)3, 19073 },
	{ (Il2CppRGCTXDataType)3, 79 },
	{ (Il2CppRGCTXDataType)2, 1303 },
	{ (Il2CppRGCTXDataType)3, 789 },
	{ (Il2CppRGCTXDataType)2, 3374 },
	{ (Il2CppRGCTXDataType)3, 80 },
	{ (Il2CppRGCTXDataType)3, 81 },
	{ (Il2CppRGCTXDataType)2, 1319 },
	{ (Il2CppRGCTXDataType)3, 802 },
	{ (Il2CppRGCTXDataType)2, 3384 },
	{ (Il2CppRGCTXDataType)3, 82 },
	{ (Il2CppRGCTXDataType)3, 25919 },
	{ (Il2CppRGCTXDataType)3, 25922 },
	{ (Il2CppRGCTXDataType)2, 1169 },
	{ (Il2CppRGCTXDataType)3, 47 },
	{ (Il2CppRGCTXDataType)2, 2362 },
	{ (Il2CppRGCTXDataType)3, 9437 },
	{ (Il2CppRGCTXDataType)3, 10263 },
	{ (Il2CppRGCTXDataType)3, 25920 },
	{ (Il2CppRGCTXDataType)3, 25921 },
	{ (Il2CppRGCTXDataType)3, 25923 },
	{ (Il2CppRGCTXDataType)2, 1175 },
	{ (Il2CppRGCTXDataType)3, 60 },
	{ (Il2CppRGCTXDataType)2, 2363 },
	{ (Il2CppRGCTXDataType)3, 9438 },
	{ (Il2CppRGCTXDataType)3, 10264 },
	{ (Il2CppRGCTXDataType)3, 26265 },
	{ (Il2CppRGCTXDataType)3, 19075 },
	{ (Il2CppRGCTXDataType)3, 19074 },
	{ (Il2CppRGCTXDataType)2, 1170 },
	{ (Il2CppRGCTXDataType)3, 48 },
	{ (Il2CppRGCTXDataType)2, 1170 },
	{ (Il2CppRGCTXDataType)3, 20705 },
	{ (Il2CppRGCTXDataType)3, 20699 },
	{ (Il2CppRGCTXDataType)3, 20700 },
	{ (Il2CppRGCTXDataType)3, 20706 },
	{ (Il2CppRGCTXDataType)3, 26267 },
	{ (Il2CppRGCTXDataType)2, 1176 },
	{ (Il2CppRGCTXDataType)3, 61 },
	{ (Il2CppRGCTXDataType)2, 1176 },
	{ (Il2CppRGCTXDataType)3, 20707 },
	{ (Il2CppRGCTXDataType)3, 20701 },
	{ (Il2CppRGCTXDataType)3, 20702 },
	{ (Il2CppRGCTXDataType)3, 20708 },
	{ (Il2CppRGCTXDataType)3, 20703 },
	{ (Il2CppRGCTXDataType)3, 20704 },
	{ (Il2CppRGCTXDataType)3, 26269 },
	{ (Il2CppRGCTXDataType)3, 25914 },
	{ (Il2CppRGCTXDataType)3, 25915 },
	{ (Il2CppRGCTXDataType)2, 1230 },
	{ (Il2CppRGCTXDataType)3, 346 },
	{ (Il2CppRGCTXDataType)2, 4327 },
	{ (Il2CppRGCTXDataType)3, 18996 },
	{ (Il2CppRGCTXDataType)3, 18997 },
	{ (Il2CppRGCTXDataType)3, 347 },
	{ (Il2CppRGCTXDataType)3, 348 },
	{ (Il2CppRGCTXDataType)2, 1241 },
	{ (Il2CppRGCTXDataType)3, 408 },
	{ (Il2CppRGCTXDataType)3, 409 },
	{ (Il2CppRGCTXDataType)2, 2211 },
	{ (Il2CppRGCTXDataType)3, 9318 },
	{ (Il2CppRGCTXDataType)3, 25911 },
	{ (Il2CppRGCTXDataType)2, 1250 },
	{ (Il2CppRGCTXDataType)3, 458 },
	{ (Il2CppRGCTXDataType)3, 459 },
	{ (Il2CppRGCTXDataType)2, 2208 },
	{ (Il2CppRGCTXDataType)3, 9316 },
	{ (Il2CppRGCTXDataType)3, 25909 },
	{ (Il2CppRGCTXDataType)2, 1260 },
	{ (Il2CppRGCTXDataType)3, 498 },
	{ (Il2CppRGCTXDataType)3, 499 },
	{ (Il2CppRGCTXDataType)3, 500 },
	{ (Il2CppRGCTXDataType)3, 25908 },
	{ (Il2CppRGCTXDataType)3, 9317 },
	{ (Il2CppRGCTXDataType)3, 349 },
	{ (Il2CppRGCTXDataType)2, 3379 },
	{ (Il2CppRGCTXDataType)3, 350 },
	{ (Il2CppRGCTXDataType)2, 1309 },
	{ (Il2CppRGCTXDataType)3, 794 },
	{ (Il2CppRGCTXDataType)3, 351 },
	{ (Il2CppRGCTXDataType)3, 19043 },
	{ (Il2CppRGCTXDataType)3, 19044 },
	{ (Il2CppRGCTXDataType)3, 19042 },
	{ (Il2CppRGCTXDataType)3, 9468 },
	{ (Il2CppRGCTXDataType)3, 352 },
	{ (Il2CppRGCTXDataType)3, 353 },
	{ (Il2CppRGCTXDataType)3, 9313 },
	{ (Il2CppRGCTXDataType)3, 19045 },
	{ (Il2CppRGCTXDataType)2, 4337 },
	{ (Il2CppRGCTXDataType)3, 9314 },
	{ (Il2CppRGCTXDataType)3, 19046 },
	{ (Il2CppRGCTXDataType)2, 4338 },
	{ (Il2CppRGCTXDataType)2, 4666 },
	{ (Il2CppRGCTXDataType)3, 20696 },
	{ (Il2CppRGCTXDataType)2, 4686 },
	{ (Il2CppRGCTXDataType)3, 20759 },
	{ (Il2CppRGCTXDataType)2, 4691 },
	{ (Il2CppRGCTXDataType)3, 20786 },
	{ (Il2CppRGCTXDataType)3, 20697 },
	{ (Il2CppRGCTXDataType)3, 20698 },
	{ (Il2CppRGCTXDataType)3, 20760 },
	{ (Il2CppRGCTXDataType)3, 20761 },
	{ (Il2CppRGCTXDataType)3, 20762 },
	{ (Il2CppRGCTXDataType)3, 20787 },
	{ (Il2CppRGCTXDataType)3, 20788 },
	{ (Il2CppRGCTXDataType)3, 20789 },
	{ (Il2CppRGCTXDataType)3, 20790 },
	{ (Il2CppRGCTXDataType)2, 2802 },
	{ (Il2CppRGCTXDataType)2, 2986 },
	{ (Il2CppRGCTXDataType)3, 787 },
	{ (Il2CppRGCTXDataType)2, 2803 },
	{ (Il2CppRGCTXDataType)2, 2987 },
	{ (Il2CppRGCTXDataType)3, 912 },
	{ (Il2CppRGCTXDataType)2, 1274 },
	{ (Il2CppRGCTXDataType)3, 660 },
	{ (Il2CppRGCTXDataType)2, 706 },
	{ (Il2CppRGCTXDataType)2, 1275 },
	{ (Il2CppRGCTXDataType)3, 661 },
	{ (Il2CppRGCTXDataType)3, 662 },
	{ (Il2CppRGCTXDataType)2, 1247 },
	{ (Il2CppRGCTXDataType)3, 440 },
	{ (Il2CppRGCTXDataType)3, 441 },
	{ (Il2CppRGCTXDataType)2, 1256 },
	{ (Il2CppRGCTXDataType)3, 470 },
	{ (Il2CppRGCTXDataType)3, 471 },
	{ (Il2CppRGCTXDataType)3, 25670 },
	{ (Il2CppRGCTXDataType)3, 1026 },
	{ (Il2CppRGCTXDataType)3, 25619 },
	{ (Il2CppRGCTXDataType)3, 1029 },
	{ (Il2CppRGCTXDataType)3, 25671 },
	{ (Il2CppRGCTXDataType)3, 25672 },
	{ (Il2CppRGCTXDataType)2, 4838 },
	{ (Il2CppRGCTXDataType)3, 21909 },
	{ (Il2CppRGCTXDataType)2, 4839 },
	{ (Il2CppRGCTXDataType)3, 21910 },
	{ (Il2CppRGCTXDataType)3, 25496 },
	{ (Il2CppRGCTXDataType)3, 25968 },
	{ (Il2CppRGCTXDataType)3, 26030 },
	{ (Il2CppRGCTXDataType)3, 25979 },
	{ (Il2CppRGCTXDataType)3, 25494 },
	{ (Il2CppRGCTXDataType)3, 25988 },
	{ (Il2CppRGCTXDataType)3, 25989 },
	{ (Il2CppRGCTXDataType)3, 26031 },
	{ (Il2CppRGCTXDataType)3, 25999 },
	{ (Il2CppRGCTXDataType)3, 26000 },
	{ (Il2CppRGCTXDataType)3, 25495 },
	{ (Il2CppRGCTXDataType)3, 26020 },
	{ (Il2CppRGCTXDataType)3, 26021 },
	{ (Il2CppRGCTXDataType)3, 26032 },
	{ (Il2CppRGCTXDataType)2, 4332 },
	{ (Il2CppRGCTXDataType)3, 19003 },
	{ (Il2CppRGCTXDataType)3, 26012 },
	{ (Il2CppRGCTXDataType)2, 1486 },
	{ (Il2CppRGCTXDataType)3, 1025 },
	{ (Il2CppRGCTXDataType)3, 26029 },
	{ (Il2CppRGCTXDataType)3, 25964 },
	{ (Il2CppRGCTXDataType)2, 4329 },
	{ (Il2CppRGCTXDataType)3, 19000 },
	{ (Il2CppRGCTXDataType)3, 26009 },
	{ (Il2CppRGCTXDataType)2, 1483 },
	{ (Il2CppRGCTXDataType)3, 1022 },
	{ (Il2CppRGCTXDataType)3, 25967 },
	{ (Il2CppRGCTXDataType)3, 25971 },
	{ (Il2CppRGCTXDataType)2, 4352 },
	{ (Il2CppRGCTXDataType)3, 19076 },
	{ (Il2CppRGCTXDataType)3, 26013 },
	{ (Il2CppRGCTXDataType)2, 1498 },
	{ (Il2CppRGCTXDataType)3, 1027 },
	{ (Il2CppRGCTXDataType)3, 25978 },
	{ (Il2CppRGCTXDataType)3, 25982 },
	{ (Il2CppRGCTXDataType)3, 25983 },
	{ (Il2CppRGCTXDataType)2, 4330 },
	{ (Il2CppRGCTXDataType)3, 19001 },
	{ (Il2CppRGCTXDataType)3, 26010 },
	{ (Il2CppRGCTXDataType)2, 1484 },
	{ (Il2CppRGCTXDataType)3, 1023 },
	{ (Il2CppRGCTXDataType)3, 25987 },
	{ (Il2CppRGCTXDataType)3, 25995 },
	{ (Il2CppRGCTXDataType)3, 25996 },
	{ (Il2CppRGCTXDataType)2, 4353 },
	{ (Il2CppRGCTXDataType)3, 19077 },
	{ (Il2CppRGCTXDataType)3, 26014 },
	{ (Il2CppRGCTXDataType)2, 1499 },
	{ (Il2CppRGCTXDataType)3, 1028 },
	{ (Il2CppRGCTXDataType)3, 25998 },
	{ (Il2CppRGCTXDataType)3, 26016 },
	{ (Il2CppRGCTXDataType)3, 26017 },
	{ (Il2CppRGCTXDataType)2, 4331 },
	{ (Il2CppRGCTXDataType)3, 19002 },
	{ (Il2CppRGCTXDataType)3, 26011 },
	{ (Il2CppRGCTXDataType)2, 1485 },
	{ (Il2CppRGCTXDataType)3, 1024 },
	{ (Il2CppRGCTXDataType)3, 26019 },
	{ (Il2CppRGCTXDataType)3, 19004 },
	{ (Il2CppRGCTXDataType)3, 19005 },
	{ (Il2CppRGCTXDataType)3, 26006 },
	{ (Il2CppRGCTXDataType)1, 272 },
	{ (Il2CppRGCTXDataType)1, 269 },
	{ (Il2CppRGCTXDataType)1, 273 },
	{ (Il2CppRGCTXDataType)3, 25870 },
	{ (Il2CppRGCTXDataType)3, 25892 },
	{ (Il2CppRGCTXDataType)3, 18638 },
	{ (Il2CppRGCTXDataType)3, 18639 },
	{ (Il2CppRGCTXDataType)3, 25889 },
	{ (Il2CppRGCTXDataType)3, 18640 },
	{ (Il2CppRGCTXDataType)3, 25891 },
	{ (Il2CppRGCTXDataType)3, 18650 },
	{ (Il2CppRGCTXDataType)2, 271 },
	{ (Il2CppRGCTXDataType)3, 18674 },
	{ (Il2CppRGCTXDataType)3, 18673 },
	{ (Il2CppRGCTXDataType)2, 589 },
	{ (Il2CppRGCTXDataType)3, 18667 },
	{ (Il2CppRGCTXDataType)2, 4285 },
	{ (Il2CppRGCTXDataType)3, 18670 },
	{ (Il2CppRGCTXDataType)2, 4286 },
	{ (Il2CppRGCTXDataType)1, 339 },
	{ (Il2CppRGCTXDataType)2, 339 },
	{ (Il2CppRGCTXDataType)1, 338 },
};
extern const CustomAttributesCacheGenerator g_AssemblyU2DCSharp_AttributeGenerators[];
IL2CPP_EXTERN_C const Il2CppCodeGenModule g_AssemblyU2DCSharp_CodeGenModule;
const Il2CppCodeGenModule g_AssemblyU2DCSharp_CodeGenModule = 
{
	"Assembly-CSharp.dll",
	2997,
	s_methodPointers,
	11,
	s_adjustorThunks,
	s_InvokerIndices,
	0,
	NULL,
	101,
	s_rgctxIndices,
	424,
	s_rgctxValues,
	NULL,
	g_AssemblyU2DCSharp_AttributeGenerators,
	NULL, // module initializer,
	NULL,
	NULL,
	NULL,
};
