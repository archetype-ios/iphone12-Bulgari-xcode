﻿#include "pch-cpp.hpp"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include <limits>
#include <stdint.h>


template <typename R>
struct VirtFuncInvoker0
{
	typedef R (*Func)(void*, const RuntimeMethod*);

	static inline R Invoke (Il2CppMethodSlot slot, RuntimeObject* obj)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_virtual_invoke_data(slot, obj);
		return ((Func)invokeData.methodPtr)(obj, invokeData.method);
	}
};
template <typename R>
struct InterfaceFuncInvoker0
{
	typedef R (*Func)(void*, const RuntimeMethod*);

	static inline R Invoke (Il2CppMethodSlot slot, RuntimeClass* declaringInterface, RuntimeObject* obj)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_interface_invoke_data(slot, obj, declaringInterface);
		return ((Func)invokeData.methodPtr)(obj, invokeData.method);
	}
};
template <typename R, typename T1>
struct InterfaceFuncInvoker1
{
	typedef R (*Func)(void*, T1, const RuntimeMethod*);

	static inline R Invoke (Il2CppMethodSlot slot, RuntimeClass* declaringInterface, RuntimeObject* obj, T1 p1)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_interface_invoke_data(slot, obj, declaringInterface);
		return ((Func)invokeData.methodPtr)(obj, p1, invokeData.method);
	}
};
template <typename T1>
struct InterfaceActionInvoker1
{
	typedef void (*Action)(void*, T1, const RuntimeMethod*);

	static inline void Invoke (Il2CppMethodSlot slot, RuntimeClass* declaringInterface, RuntimeObject* obj, T1 p1)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_interface_invoke_data(slot, obj, declaringInterface);
		((Action)invokeData.methodPtr)(obj, p1, invokeData.method);
	}
};

// System.Action`1<BestHTTP.SignalRCore.HubConnection>
struct Action_1_tD3410B67BFA3C1FCF5D3D8345C933F0ACA7A5426;
// System.Action`1<System.Object>
struct Action_1_tD9663D9715FAA4E62035CFCF1AD4D094EE7872DC;
// System.Action`2<BestHTTP.SignalRCore.HubConnection,System.String>
struct Action_2_t36E73E35B8C2732CDF2877AFC5E932F8910CD2A5;
// System.Action`2<System.Object,System.Object>
struct Action_2_t4FB8E5660AE634E13BF340904C61FEA9DCE9D52D;
// System.Action`3<BestHTTP.SignalRCore.HubConnection,BestHTTP.SignalRCore.ITransport,BestHTTP.SignalRCore.TransportEvents>
struct Action_3_t2A25F65DF18A1F5038436F84891B59501F1E97B6;
// System.Action`3<BestHTTP.SignalRCore.HubConnection,System.Uri,System.Uri>
struct Action_3_t5271F369265AB717807700BB149273B6EE0B1A4E;
// System.Action`3<System.Object,System.Object,System.Int32Enum>
struct Action_3_tDDADDA1F8499D3255F24355EFF7A9FA4E2B5823B;
// System.Action`3<System.Object,System.Object,System.Object>
struct Action_3_t40CAA9C4849DA1712B1B6ECA55C18E0C0DFEBE4C;
// System.Collections.Generic.Dictionary`2<System.Int64,BestHTTP.SignalRCore.InvocationDefinition>
struct Dictionary_2_tB49E454438088BDA98F73A4D9F23684ABCD53CCA;
// System.Collections.Generic.Dictionary`2<System.String,BestHTTP.SignalRCore.Subscription>
struct Dictionary_2_t6F4431355404A55604094B7002C21EE985AD3090;
// System.Func`3<BestHTTP.SignalRCore.HubConnection,BestHTTP.SignalRCore.Messages.Message,System.Boolean>
struct Func_3_tA9872E728CF5CEA9BE21146D76352E8C6293C2C6;
// BestHTTP.Futures.FutureValueCallback`1<System.Object>
struct FutureValueCallback_1_t7F9F1097135E654A08A590D5E1DD9A30730308D2;
// BestHTTP.Futures.FutureValueCallback`1<System.String>
struct FutureValueCallback_1_t31AE88E9DCB927E5F16747DB54C827FE97F91BBF;
// BestHTTP.Futures.IFuture`1<System.Object>
struct IFuture_1_tA448683556ED28BC3A3ECBB944647E9285433894;
// BestHTTP.Futures.IFuture`1<System.String>
struct IFuture_1_tEBD463543458CE8C841D19C91EAC2683130EC31D;
// System.Collections.Generic.List`1<UnityEngine.CanvasGroup>
struct List_1_t34AA4AF4E7352129CA58045901530E41445AC16D;
// System.Collections.Generic.List`1<BestHTTP.Examples.Helpers.SampleBase>
struct List_1_tED79C3AFE45C4FEB86845EA70A708CADDAA78135;
// System.Collections.Generic.List`1<BestHTTP.SignalRCore.Messages.SupportedTransport>
struct List_1_tCA38AB358E3D5B04B4A3CC166F4256C250654469;
// System.Collections.Generic.List`1<BestHTTP.SignalRCore.TransportTypes>
struct List_1_tA942B432640F6BFD6DDD187F45FF27BDB1719805;
// System.Threading.Tasks.TaskCompletionSource`1<BestHTTP.SignalRCore.HubConnection>
struct TaskCompletionSource_1_t59017925BDC6678DB216933E2762064022417A3F;
// System.Char[]
struct CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34;
// System.Delegate[]
struct DelegateU5BU5D_t677D8FE08A5F99E8EE49150B73966CD6E9BF7DB8;
// System.Int32[]
struct Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32;
// System.IntPtr[]
struct IntPtrU5BU5D_t27FC72B0409D75AAF33EC42498E8094E95FEE9A6;
// System.Object[]
struct ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE;
// UnityEngine.RuntimePlatform[]
struct RuntimePlatformU5BU5D_tA221FE8D5CE756108CBC39E15F0CB99A0787AD52;
// UnityEngine.UI.Selectable[]
struct SelectableU5BU5D_tECF9F5BDBF0652A937D18F10C883EFDAE2E62535;
// System.Diagnostics.StackTrace[]
struct StackTraceU5BU5D_t4AD999C288CB6D1F38A299D12B1598D606588971;
// UnityEngine.Vector3[]
struct Vector3U5BU5D_t5FB88EAA33E46838BDC2ABDAEA3E8727491CB9E4;
// BestHTTP.Connections.HTTP2.HuffmanEncoder/TableEntry[]
struct TableEntryU5BU5D_tB3F4A3142B2B8D2FFFA48D2697C81F352A3D4A94;
// BestHTTP.Connections.HTTP2.HuffmanEncoder/TreeNode[]
struct TreeNodeU5BU5D_t5032BD830284F31E0A94DEB0072DB2B07126BABD;
// UnityEngine.UI.AnimationTriggers
struct AnimationTriggers_tF38CA7FA631709E096B57D732668D86081F44C11;
// System.AsyncCallback
struct AsyncCallback_tA7921BEF974919C46FF8F9D9867C567B200BB0EA;
// UnityEngine.UI.Button
struct Button_tA893FC15AB26E1439AC25BDCA7079530587BB65D;
// System.DelegateData
struct DelegateData_t17DD30660E330C49381DAA99F934BE75CB11F288;
// UnityEngine.UI.Dropdown
struct Dropdown_t099F5232BB75810BC79EED6E27DDCED46C3BCD96;
// System.Exception
struct Exception_t;
// UnityEngine.UI.Graphic
struct Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24;
// BestHTTP.HTTPRequest
struct HTTPRequest_t583F05C0A0E42F47AD66FA8AAE73244C29833844;
// BestHTTP.HTTPResponse
struct HTTPResponse_t025FD1144BF8C8102E0E5F59D4447B218E77C600;
// BestHTTP.SignalRCore.HubConnection
struct HubConnection_t797D4973C9BBE6895D85BF743512ADA7D419736D;
// BestHTTP.SignalRCore.HubOptions
struct HubOptions_t53B108AE0F01D633E571D739D96B573C5C68A6F7;
// BestHTTP.Examples.HubWithAuthorizationSample
struct HubWithAuthorizationSample_t19979B3628DD848910FD70011959F7CB115D5968;
// BestHTTP.Examples.HubWithPreAuthorizationSample
struct HubWithPreAuthorizationSample_tDF799D602DC996BFF26C8E97175B4E64BF3807D8;
// System.IAsyncResult
struct IAsyncResult_tC9F97BF36FCF122D29D3101D80642278297BF370;
// BestHTTP.SignalRCore.IAuthenticationProvider
struct IAuthenticationProvider_tE57312D246360017364A3ED93CF4AE0BCC123517;
// System.Collections.IDictionary
struct IDictionary_t99871C56B8EC2452AC5C4CF3831695E617B89D3A;
// BestHTTP.SignalRCore.IEncoder
struct IEncoder_t7C0A30CEBD642FBAFB2444D006FE6575D8914FE1;
// BestHTTP.SignalRCore.IProtocol
struct IProtocol_tFE0A86EE25C0427642F1B825BD8ADA84E8CD20CD;
// BestHTTP.SignalRCore.IRetryPolicy
struct IRetryPolicy_tCA627B759446200581578A24C647C463D5603A98;
// BestHTTP.SignalRCore.ITransport
struct ITransport_t832D0F246CABAD20CB7410CC70ED972EF084053B;
// UnityEngine.UI.InputField
struct InputField_tB41A2814F31A3E9373D443EDEBBB2856006324D0;
// BestHTTP.SignalRCore.JsonProtocol
struct JsonProtocol_t0DA0EB80CD537920D655CE3D790C87A7035A7635;
// BestHTTP.SignalRCore.Encoders.LitJsonEncoder
struct LitJsonEncoder_t93D5E7144163028B776CFC2472958FD241FDAEC7;
// BestHTTP.Logger.LoggingContext
struct LoggingContext_tBC479DCBB94ED3A18F737DE8435E866B1E67178C;
// System.Reflection.MethodInfo
struct MethodInfo_t;
// BestHTTP.SignalRCore.Messages.NegotiationResult
struct NegotiationResult_tB56E4CA54D44A10740AEDCBAC199D03849AEE84C;
// UnityEngine.Object
struct Object_tF2F3778131EFF286AF62B7B013A170F95A91571A;
// BestHTTP.SignalRCore.OnAuthenticationFailedDelegate
struct OnAuthenticationFailedDelegate_tB415D1478EDFF7309A953E24BD13797DEFAAF9B2;
// BestHTTP.SignalRCore.OnAuthenticationSuccededDelegate
struct OnAuthenticationSuccededDelegate_t7A96699C45F6100206650225F8875DCAC1A8DE46;
// BestHTTP.Examples.PreAuthAccessTokenAuthenticator
struct PreAuthAccessTokenAuthenticator_t424A1F277DA9B16ADCB16A5B9A7659E4B5DC6162;
// UnityEngine.RectTransform
struct RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072;
// System.Runtime.Serialization.SafeSerializationManager
struct SafeSerializationManager_tDE44F029589A028F8A3053C5C06153FAB4AAE29F;
// BestHTTP.Examples.Helpers.SampleBase
struct SampleBase_tC30950FFCCCE8696B341569EEE364BF4C4DB7157;
// BestHTTP.Examples.SampleRoot
struct SampleRoot_tB11D3D3067248A62081D6D1A6F5936A1A3D856E1;
// UnityEngine.UI.ScrollRect
struct ScrollRect_tB16156010F89FFDAAB2127CA878608FD91B9FBEA;
// UnityEngine.UI.Scrollbar
struct Scrollbar_tECAC7FD315210FC856A3EC60AE1847A66AAF6C28;
// UnityEngine.UI.Selectable
struct Selectable_t34088A3677CC9D344F81B0D91999D8C5963D7DBD;
// UnityEngine.Sprite
struct Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9;
// System.String
struct String_t;
// UnityEngine.UI.Text
struct Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1;
// BestHTTP.Examples.Helpers.TextListItem
struct TextListItem_tC8FEA392C6AF65CFF22D8089B9104B79C84A6D8B;
// System.Uri
struct Uri_t4A915E1CC15B2C650F478099AD448E9466CBF612;
// System.UriParser
struct UriParser_t6DEBE5C6CDC3C29C9019CD951C7ECEBD6A5D3E3A;
// System.Void
struct Void_t700C6383A2A510C2CF4DD86DABD5CA9FF70ADAC5;
// UnityEngine.UI.Button/ButtonClickedEvent
struct ButtonClickedEvent_tE6D6D94ED8100451CF00D2BED1FB2253F37BB14F;
// UnityEngine.RectTransform/ReapplyDrivenProperties
struct ReapplyDrivenProperties_t1441259DADA8FE33A95334AC24C017DFA3DEB4CE;
// UnityEngine.UI.ScrollRect/ScrollRectEvent
struct ScrollRectEvent_tA2F08EF8BB0B0B0F72DB8242DC5AB17BB0D1731E;
// System.Uri/UriInfo
struct UriInfo_tCB2302A896132D1F70E47C3895FAB9A0F2A6EE45;

IL2CPP_EXTERN_C RuntimeClass* Action_1_tD3410B67BFA3C1FCF5D3D8345C933F0ACA7A5426_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Action_2_t36E73E35B8C2732CDF2877AFC5E932F8910CD2A5_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Action_3_t2A25F65DF18A1F5038436F84891B59501F1E97B6_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Action_3_t5271F369265AB717807700BB149273B6EE0B1A4E_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Exception_t_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* FutureValueCallback_1_t31AE88E9DCB927E5F16747DB54C827FE97F91BBF_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* GUIHelper_t1BA8F4DF68223FC31CA8DDE367DF168C2ADAE16D_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* HubConnection_t797D4973C9BBE6895D85BF743512ADA7D419736D_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* HuffmanEncoder_tE53CF82173488443CE9781D476D0E545B4485E6C_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* IAuthenticationProvider_tE57312D246360017364A3ED93CF4AE0BCC123517_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* IFuture_1_tEBD463543458CE8C841D19C91EAC2683130EC31D_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* IProtocol_tFE0A86EE25C0427642F1B825BD8ADA84E8CD20CD_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* ITransport_t832D0F246CABAD20CB7410CC70ED972EF084053B_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* JsonProtocol_t0DA0EB80CD537920D655CE3D790C87A7035A7635_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* LitJsonEncoder_t93D5E7144163028B776CFC2472958FD241FDAEC7_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* OnAuthenticationFailedDelegate_tB415D1478EDFF7309A953E24BD13797DEFAAF9B2_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* OnAuthenticationSuccededDelegate_t7A96699C45F6100206650225F8875DCAC1A8DE46_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* PreAuthAccessTokenAuthenticator_t424A1F277DA9B16ADCB16A5B9A7659E4B5DC6162_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* TableEntryU5BU5D_tB3F4A3142B2B8D2FFFA48D2697C81F352A3D4A94_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* TimeSpan_t4F6A0E13E703B65365CFCAB58E05EE0AF3EE6203_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* TransportEvents_tE38D2B53053334BE1B4419061B7701FF8E099D19_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* TransportTypes_t8387D0BDDE41AD7796AEFFC585944D24F9389FB0_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* TreeNodeU5BU5D_t5032BD830284F31E0A94DEB0072DB2B07126BABD_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Uri_t4A915E1CC15B2C650F478099AD448E9466CBF612_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C String_t* _stringLiteral0916CABAB7E3E45364FFC1B36C761FA2ABC7FF8E;
IL2CPP_EXTERN_C String_t* _stringLiteral0D092D548D913C794FEC0976BBA5DAF67E0F36F2;
IL2CPP_EXTERN_C String_t* _stringLiteral17DFF9E84B78EDF31E28D673A72044E0DC1CA6AE;
IL2CPP_EXTERN_C String_t* _stringLiteral3177C3549DF7103EBF6FAD9F320123D3A8D97BE7;
IL2CPP_EXTERN_C String_t* _stringLiteral450C5E3FC3E837EBBE92288DB9DE76195D7CF985;
IL2CPP_EXTERN_C String_t* _stringLiteral4B9EA1F270BF9EE6BCF2EFE20495E4564CFEE369;
IL2CPP_EXTERN_C String_t* _stringLiteral519A627D101D9C5AE7C965506A4856184ACCD31F;
IL2CPP_EXTERN_C String_t* _stringLiteral5904D6D202BF91354FCA994B64879EAF9DEA8268;
IL2CPP_EXTERN_C String_t* _stringLiteral6BAB476667C71E550B8BB632BE04ADDC98348F2D;
IL2CPP_EXTERN_C String_t* _stringLiteral7DB56CFD558B13D95B19D64BDAE0AAD5F3AECBCB;
IL2CPP_EXTERN_C String_t* _stringLiteral94A3AB9B36FB187F4215E62357DFFBDC6B930029;
IL2CPP_EXTERN_C String_t* _stringLiteralA4529359CF24AF6F4533508E56794F9CAD34F426;
IL2CPP_EXTERN_C String_t* _stringLiteralA91BC6B7438D9FA67272371E212F4796D0F610FA;
IL2CPP_EXTERN_C String_t* _stringLiteralABF1DBB99693496E8838B727F5C5E3D5D5ED72C8;
IL2CPP_EXTERN_C String_t* _stringLiteralB82244460F050E4F7E423A7000AE2646F10EF257;
IL2CPP_EXTERN_C String_t* _stringLiteralCB58925B56DD017047A25D2F36CB1BCA26F68FC3;
IL2CPP_EXTERN_C String_t* _stringLiteralE7C5667AC424C541B18739402C56AA560254BB74;
IL2CPP_EXTERN_C const RuntimeMethod* Action_1__ctor_mF3C29B87B57748F26952F077111724B7F8728AD8_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Action_2__ctor_m9E9D0A4856BE79EC7CCD28D4A14DC05BB0BB1CBE_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Action_3__ctor_m4CBDE8196ACBD5567E04A96C3714BF3D6C29F818_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Action_3__ctor_m9F958EE4C81855D891333C72B908AA2E183BCA4A_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* FutureValueCallback_1__ctor_mC0AAA3F5D464DBA95A0F1604DDD4376A568D8E75_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* HubConnection_Invoke_TisString_t_mC092761B744F2CFDEBB042CD081359F0E51B8F96_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* HubWithAuthorizationSample_Hub_OnClosed_mCFB8D0CCE1D0FB7B4E8971854C017D4D42AC1C5B_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* HubWithAuthorizationSample_Hub_OnConnected_m338A40A3F89743396F7D043FA490EACAB06E2498_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* HubWithAuthorizationSample_Hub_OnError_mFFD5C8D2A4A52374687962623A0E30E1B0165482_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* HubWithAuthorizationSample_Hub_Redirected_m8556E4CBAD4E69E1191E3C0CB892779A9F80AA6D_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* HubWithAuthorizationSample_U3CHub_OnConnectedU3Eb__13_0_m65C934096BC045F396D14A1CE2E283380F3E801A_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* HubWithAuthorizationSample_U3COnConnectButtonU3Eb__10_0_m6D430BDDCAA15325F032C3A4B13E411D21DE5EBD_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* HubWithPreAuthorizationSample_AuthenticationProvider_OnAuthenticationFailed_m10A16F3C48FF2D02BF7203E2B70415F6FF185BE3_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* HubWithPreAuthorizationSample_AuthenticationProvider_OnAuthenticationSucceded_m4112BE89F0E1497A6C2E8CEFEDFF45DA075DDDFD_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* HubWithPreAuthorizationSample_Hub_OnClosed_m70E27F329CD5D47268228528DEC9B130B77997AF_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* HubWithPreAuthorizationSample_Hub_OnConnected_m70A6CC492CCC83DCF659FDB088D2881C276A7A3A_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* HubWithPreAuthorizationSample_Hub_OnError_m16BE097913A88F543907D908F64B234B15F9F87A_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* HubWithPreAuthorizationSample_U3CHub_OnConnectedU3Eb__15_0_m11819123816AA7FA753584C7A0B140A4CB7FA660_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* HubWithPreAuthorizationSample_U3COnConnectButtonU3Eb__11_0_m6CC61349D8E7D060103AD3D5AA813DDD28A871C4_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* HuffmanEncoder_GetNext_m3E9434FE1E755349C7127E960E54123755C95C20_RuntimeMethod_var;
struct Delegate_t_marshaled_com;
struct Delegate_t_marshaled_pinvoke;
struct Exception_t_marshaled_com;
struct Exception_t_marshaled_pinvoke;

struct ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE;
struct TableEntryU5BU5D_tB3F4A3142B2B8D2FFFA48D2697C81F352A3D4A94;
struct TreeNodeU5BU5D_t5032BD830284F31E0A94DEB0072DB2B07126BABD;

IL2CPP_EXTERN_C_BEGIN
IL2CPP_EXTERN_C_END

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Object

struct Il2CppArrayBounds;

// System.Array


// BestHTTP.SignalRCore.HubConnectionExtensions
struct HubConnectionExtensions_t47CA793E3E11CB5CCA09CEF9976504F14D2C3FE7  : public RuntimeObject
{
public:

public:
};


// BestHTTP.Connections.HTTP2.HuffmanEncoder
struct HuffmanEncoder_tE53CF82173488443CE9781D476D0E545B4485E6C  : public RuntimeObject
{
public:

public:
};

struct HuffmanEncoder_tE53CF82173488443CE9781D476D0E545B4485E6C_StaticFields
{
public:
	// BestHTTP.Connections.HTTP2.HuffmanEncoder/TableEntry[] BestHTTP.Connections.HTTP2.HuffmanEncoder::StaticTable
	TableEntryU5BU5D_tB3F4A3142B2B8D2FFFA48D2697C81F352A3D4A94* ___StaticTable_1;
	// BestHTTP.Connections.HTTP2.HuffmanEncoder/TreeNode[] BestHTTP.Connections.HTTP2.HuffmanEncoder::HuffmanTree
	TreeNodeU5BU5D_t5032BD830284F31E0A94DEB0072DB2B07126BABD* ___HuffmanTree_2;

public:
	inline static int32_t get_offset_of_StaticTable_1() { return static_cast<int32_t>(offsetof(HuffmanEncoder_tE53CF82173488443CE9781D476D0E545B4485E6C_StaticFields, ___StaticTable_1)); }
	inline TableEntryU5BU5D_tB3F4A3142B2B8D2FFFA48D2697C81F352A3D4A94* get_StaticTable_1() const { return ___StaticTable_1; }
	inline TableEntryU5BU5D_tB3F4A3142B2B8D2FFFA48D2697C81F352A3D4A94** get_address_of_StaticTable_1() { return &___StaticTable_1; }
	inline void set_StaticTable_1(TableEntryU5BU5D_tB3F4A3142B2B8D2FFFA48D2697C81F352A3D4A94* value)
	{
		___StaticTable_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___StaticTable_1), (void*)value);
	}

	inline static int32_t get_offset_of_HuffmanTree_2() { return static_cast<int32_t>(offsetof(HuffmanEncoder_tE53CF82173488443CE9781D476D0E545B4485E6C_StaticFields, ___HuffmanTree_2)); }
	inline TreeNodeU5BU5D_t5032BD830284F31E0A94DEB0072DB2B07126BABD* get_HuffmanTree_2() const { return ___HuffmanTree_2; }
	inline TreeNodeU5BU5D_t5032BD830284F31E0A94DEB0072DB2B07126BABD** get_address_of_HuffmanTree_2() { return &___HuffmanTree_2; }
	inline void set_HuffmanTree_2(TreeNodeU5BU5D_t5032BD830284F31E0A94DEB0072DB2B07126BABD* value)
	{
		___HuffmanTree_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___HuffmanTree_2), (void*)value);
	}
};


// BestHTTP.SignalRCore.JsonProtocol
struct JsonProtocol_t0DA0EB80CD537920D655CE3D790C87A7035A7635  : public RuntimeObject
{
public:
	// BestHTTP.SignalRCore.IEncoder BestHTTP.SignalRCore.JsonProtocol::<Encoder>k__BackingField
	RuntimeObject* ___U3CEncoderU3Ek__BackingField_1;
	// BestHTTP.SignalRCore.HubConnection BestHTTP.SignalRCore.JsonProtocol::<Connection>k__BackingField
	HubConnection_t797D4973C9BBE6895D85BF743512ADA7D419736D * ___U3CConnectionU3Ek__BackingField_2;

public:
	inline static int32_t get_offset_of_U3CEncoderU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(JsonProtocol_t0DA0EB80CD537920D655CE3D790C87A7035A7635, ___U3CEncoderU3Ek__BackingField_1)); }
	inline RuntimeObject* get_U3CEncoderU3Ek__BackingField_1() const { return ___U3CEncoderU3Ek__BackingField_1; }
	inline RuntimeObject** get_address_of_U3CEncoderU3Ek__BackingField_1() { return &___U3CEncoderU3Ek__BackingField_1; }
	inline void set_U3CEncoderU3Ek__BackingField_1(RuntimeObject* value)
	{
		___U3CEncoderU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CEncoderU3Ek__BackingField_1), (void*)value);
	}

	inline static int32_t get_offset_of_U3CConnectionU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(JsonProtocol_t0DA0EB80CD537920D655CE3D790C87A7035A7635, ___U3CConnectionU3Ek__BackingField_2)); }
	inline HubConnection_t797D4973C9BBE6895D85BF743512ADA7D419736D * get_U3CConnectionU3Ek__BackingField_2() const { return ___U3CConnectionU3Ek__BackingField_2; }
	inline HubConnection_t797D4973C9BBE6895D85BF743512ADA7D419736D ** get_address_of_U3CConnectionU3Ek__BackingField_2() { return &___U3CConnectionU3Ek__BackingField_2; }
	inline void set_U3CConnectionU3Ek__BackingField_2(HubConnection_t797D4973C9BBE6895D85BF743512ADA7D419736D * value)
	{
		___U3CConnectionU3Ek__BackingField_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CConnectionU3Ek__BackingField_2), (void*)value);
	}
};


// BestHTTP.SignalRCore.Encoders.LitJsonEncoder
struct LitJsonEncoder_t93D5E7144163028B776CFC2472958FD241FDAEC7  : public RuntimeObject
{
public:

public:
};


// BestHTTP.SignalRCore.Messages.NegotiationResult
struct NegotiationResult_tB56E4CA54D44A10740AEDCBAC199D03849AEE84C  : public RuntimeObject
{
public:
	// System.Int32 BestHTTP.SignalRCore.Messages.NegotiationResult::<NegotiateVersion>k__BackingField
	int32_t ___U3CNegotiateVersionU3Ek__BackingField_0;
	// System.String BestHTTP.SignalRCore.Messages.NegotiationResult::<ConnectionToken>k__BackingField
	String_t* ___U3CConnectionTokenU3Ek__BackingField_1;
	// System.String BestHTTP.SignalRCore.Messages.NegotiationResult::<ConnectionId>k__BackingField
	String_t* ___U3CConnectionIdU3Ek__BackingField_2;
	// System.Collections.Generic.List`1<BestHTTP.SignalRCore.Messages.SupportedTransport> BestHTTP.SignalRCore.Messages.NegotiationResult::<SupportedTransports>k__BackingField
	List_1_tCA38AB358E3D5B04B4A3CC166F4256C250654469 * ___U3CSupportedTransportsU3Ek__BackingField_3;
	// System.Uri BestHTTP.SignalRCore.Messages.NegotiationResult::<Url>k__BackingField
	Uri_t4A915E1CC15B2C650F478099AD448E9466CBF612 * ___U3CUrlU3Ek__BackingField_4;
	// System.String BestHTTP.SignalRCore.Messages.NegotiationResult::<AccessToken>k__BackingField
	String_t* ___U3CAccessTokenU3Ek__BackingField_5;
	// BestHTTP.HTTPResponse BestHTTP.SignalRCore.Messages.NegotiationResult::<NegotiationResponse>k__BackingField
	HTTPResponse_t025FD1144BF8C8102E0E5F59D4447B218E77C600 * ___U3CNegotiationResponseU3Ek__BackingField_6;

public:
	inline static int32_t get_offset_of_U3CNegotiateVersionU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(NegotiationResult_tB56E4CA54D44A10740AEDCBAC199D03849AEE84C, ___U3CNegotiateVersionU3Ek__BackingField_0)); }
	inline int32_t get_U3CNegotiateVersionU3Ek__BackingField_0() const { return ___U3CNegotiateVersionU3Ek__BackingField_0; }
	inline int32_t* get_address_of_U3CNegotiateVersionU3Ek__BackingField_0() { return &___U3CNegotiateVersionU3Ek__BackingField_0; }
	inline void set_U3CNegotiateVersionU3Ek__BackingField_0(int32_t value)
	{
		___U3CNegotiateVersionU3Ek__BackingField_0 = value;
	}

	inline static int32_t get_offset_of_U3CConnectionTokenU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(NegotiationResult_tB56E4CA54D44A10740AEDCBAC199D03849AEE84C, ___U3CConnectionTokenU3Ek__BackingField_1)); }
	inline String_t* get_U3CConnectionTokenU3Ek__BackingField_1() const { return ___U3CConnectionTokenU3Ek__BackingField_1; }
	inline String_t** get_address_of_U3CConnectionTokenU3Ek__BackingField_1() { return &___U3CConnectionTokenU3Ek__BackingField_1; }
	inline void set_U3CConnectionTokenU3Ek__BackingField_1(String_t* value)
	{
		___U3CConnectionTokenU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CConnectionTokenU3Ek__BackingField_1), (void*)value);
	}

	inline static int32_t get_offset_of_U3CConnectionIdU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(NegotiationResult_tB56E4CA54D44A10740AEDCBAC199D03849AEE84C, ___U3CConnectionIdU3Ek__BackingField_2)); }
	inline String_t* get_U3CConnectionIdU3Ek__BackingField_2() const { return ___U3CConnectionIdU3Ek__BackingField_2; }
	inline String_t** get_address_of_U3CConnectionIdU3Ek__BackingField_2() { return &___U3CConnectionIdU3Ek__BackingField_2; }
	inline void set_U3CConnectionIdU3Ek__BackingField_2(String_t* value)
	{
		___U3CConnectionIdU3Ek__BackingField_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CConnectionIdU3Ek__BackingField_2), (void*)value);
	}

	inline static int32_t get_offset_of_U3CSupportedTransportsU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(NegotiationResult_tB56E4CA54D44A10740AEDCBAC199D03849AEE84C, ___U3CSupportedTransportsU3Ek__BackingField_3)); }
	inline List_1_tCA38AB358E3D5B04B4A3CC166F4256C250654469 * get_U3CSupportedTransportsU3Ek__BackingField_3() const { return ___U3CSupportedTransportsU3Ek__BackingField_3; }
	inline List_1_tCA38AB358E3D5B04B4A3CC166F4256C250654469 ** get_address_of_U3CSupportedTransportsU3Ek__BackingField_3() { return &___U3CSupportedTransportsU3Ek__BackingField_3; }
	inline void set_U3CSupportedTransportsU3Ek__BackingField_3(List_1_tCA38AB358E3D5B04B4A3CC166F4256C250654469 * value)
	{
		___U3CSupportedTransportsU3Ek__BackingField_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CSupportedTransportsU3Ek__BackingField_3), (void*)value);
	}

	inline static int32_t get_offset_of_U3CUrlU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(NegotiationResult_tB56E4CA54D44A10740AEDCBAC199D03849AEE84C, ___U3CUrlU3Ek__BackingField_4)); }
	inline Uri_t4A915E1CC15B2C650F478099AD448E9466CBF612 * get_U3CUrlU3Ek__BackingField_4() const { return ___U3CUrlU3Ek__BackingField_4; }
	inline Uri_t4A915E1CC15B2C650F478099AD448E9466CBF612 ** get_address_of_U3CUrlU3Ek__BackingField_4() { return &___U3CUrlU3Ek__BackingField_4; }
	inline void set_U3CUrlU3Ek__BackingField_4(Uri_t4A915E1CC15B2C650F478099AD448E9466CBF612 * value)
	{
		___U3CUrlU3Ek__BackingField_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CUrlU3Ek__BackingField_4), (void*)value);
	}

	inline static int32_t get_offset_of_U3CAccessTokenU3Ek__BackingField_5() { return static_cast<int32_t>(offsetof(NegotiationResult_tB56E4CA54D44A10740AEDCBAC199D03849AEE84C, ___U3CAccessTokenU3Ek__BackingField_5)); }
	inline String_t* get_U3CAccessTokenU3Ek__BackingField_5() const { return ___U3CAccessTokenU3Ek__BackingField_5; }
	inline String_t** get_address_of_U3CAccessTokenU3Ek__BackingField_5() { return &___U3CAccessTokenU3Ek__BackingField_5; }
	inline void set_U3CAccessTokenU3Ek__BackingField_5(String_t* value)
	{
		___U3CAccessTokenU3Ek__BackingField_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CAccessTokenU3Ek__BackingField_5), (void*)value);
	}

	inline static int32_t get_offset_of_U3CNegotiationResponseU3Ek__BackingField_6() { return static_cast<int32_t>(offsetof(NegotiationResult_tB56E4CA54D44A10740AEDCBAC199D03849AEE84C, ___U3CNegotiationResponseU3Ek__BackingField_6)); }
	inline HTTPResponse_t025FD1144BF8C8102E0E5F59D4447B218E77C600 * get_U3CNegotiationResponseU3Ek__BackingField_6() const { return ___U3CNegotiationResponseU3Ek__BackingField_6; }
	inline HTTPResponse_t025FD1144BF8C8102E0E5F59D4447B218E77C600 ** get_address_of_U3CNegotiationResponseU3Ek__BackingField_6() { return &___U3CNegotiationResponseU3Ek__BackingField_6; }
	inline void set_U3CNegotiationResponseU3Ek__BackingField_6(HTTPResponse_t025FD1144BF8C8102E0E5F59D4447B218E77C600 * value)
	{
		___U3CNegotiationResponseU3Ek__BackingField_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CNegotiationResponseU3Ek__BackingField_6), (void*)value);
	}
};


// BestHTTP.Examples.PreAuthAccessTokenAuthenticator
struct PreAuthAccessTokenAuthenticator_t424A1F277DA9B16ADCB16A5B9A7659E4B5DC6162  : public RuntimeObject
{
public:
	// BestHTTP.SignalRCore.OnAuthenticationSuccededDelegate BestHTTP.Examples.PreAuthAccessTokenAuthenticator::OnAuthenticationSucceded
	OnAuthenticationSuccededDelegate_t7A96699C45F6100206650225F8875DCAC1A8DE46 * ___OnAuthenticationSucceded_0;
	// BestHTTP.SignalRCore.OnAuthenticationFailedDelegate BestHTTP.Examples.PreAuthAccessTokenAuthenticator::OnAuthenticationFailed
	OnAuthenticationFailedDelegate_tB415D1478EDFF7309A953E24BD13797DEFAAF9B2 * ___OnAuthenticationFailed_1;
	// System.String BestHTTP.Examples.PreAuthAccessTokenAuthenticator::<Token>k__BackingField
	String_t* ___U3CTokenU3Ek__BackingField_2;
	// System.Uri BestHTTP.Examples.PreAuthAccessTokenAuthenticator::authenticationUri
	Uri_t4A915E1CC15B2C650F478099AD448E9466CBF612 * ___authenticationUri_3;
	// BestHTTP.HTTPRequest BestHTTP.Examples.PreAuthAccessTokenAuthenticator::authenticationRequest
	HTTPRequest_t583F05C0A0E42F47AD66FA8AAE73244C29833844 * ___authenticationRequest_4;
	// System.Boolean BestHTTP.Examples.PreAuthAccessTokenAuthenticator::isCancellationRequested
	bool ___isCancellationRequested_5;

public:
	inline static int32_t get_offset_of_OnAuthenticationSucceded_0() { return static_cast<int32_t>(offsetof(PreAuthAccessTokenAuthenticator_t424A1F277DA9B16ADCB16A5B9A7659E4B5DC6162, ___OnAuthenticationSucceded_0)); }
	inline OnAuthenticationSuccededDelegate_t7A96699C45F6100206650225F8875DCAC1A8DE46 * get_OnAuthenticationSucceded_0() const { return ___OnAuthenticationSucceded_0; }
	inline OnAuthenticationSuccededDelegate_t7A96699C45F6100206650225F8875DCAC1A8DE46 ** get_address_of_OnAuthenticationSucceded_0() { return &___OnAuthenticationSucceded_0; }
	inline void set_OnAuthenticationSucceded_0(OnAuthenticationSuccededDelegate_t7A96699C45F6100206650225F8875DCAC1A8DE46 * value)
	{
		___OnAuthenticationSucceded_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___OnAuthenticationSucceded_0), (void*)value);
	}

	inline static int32_t get_offset_of_OnAuthenticationFailed_1() { return static_cast<int32_t>(offsetof(PreAuthAccessTokenAuthenticator_t424A1F277DA9B16ADCB16A5B9A7659E4B5DC6162, ___OnAuthenticationFailed_1)); }
	inline OnAuthenticationFailedDelegate_tB415D1478EDFF7309A953E24BD13797DEFAAF9B2 * get_OnAuthenticationFailed_1() const { return ___OnAuthenticationFailed_1; }
	inline OnAuthenticationFailedDelegate_tB415D1478EDFF7309A953E24BD13797DEFAAF9B2 ** get_address_of_OnAuthenticationFailed_1() { return &___OnAuthenticationFailed_1; }
	inline void set_OnAuthenticationFailed_1(OnAuthenticationFailedDelegate_tB415D1478EDFF7309A953E24BD13797DEFAAF9B2 * value)
	{
		___OnAuthenticationFailed_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___OnAuthenticationFailed_1), (void*)value);
	}

	inline static int32_t get_offset_of_U3CTokenU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(PreAuthAccessTokenAuthenticator_t424A1F277DA9B16ADCB16A5B9A7659E4B5DC6162, ___U3CTokenU3Ek__BackingField_2)); }
	inline String_t* get_U3CTokenU3Ek__BackingField_2() const { return ___U3CTokenU3Ek__BackingField_2; }
	inline String_t** get_address_of_U3CTokenU3Ek__BackingField_2() { return &___U3CTokenU3Ek__BackingField_2; }
	inline void set_U3CTokenU3Ek__BackingField_2(String_t* value)
	{
		___U3CTokenU3Ek__BackingField_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CTokenU3Ek__BackingField_2), (void*)value);
	}

	inline static int32_t get_offset_of_authenticationUri_3() { return static_cast<int32_t>(offsetof(PreAuthAccessTokenAuthenticator_t424A1F277DA9B16ADCB16A5B9A7659E4B5DC6162, ___authenticationUri_3)); }
	inline Uri_t4A915E1CC15B2C650F478099AD448E9466CBF612 * get_authenticationUri_3() const { return ___authenticationUri_3; }
	inline Uri_t4A915E1CC15B2C650F478099AD448E9466CBF612 ** get_address_of_authenticationUri_3() { return &___authenticationUri_3; }
	inline void set_authenticationUri_3(Uri_t4A915E1CC15B2C650F478099AD448E9466CBF612 * value)
	{
		___authenticationUri_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___authenticationUri_3), (void*)value);
	}

	inline static int32_t get_offset_of_authenticationRequest_4() { return static_cast<int32_t>(offsetof(PreAuthAccessTokenAuthenticator_t424A1F277DA9B16ADCB16A5B9A7659E4B5DC6162, ___authenticationRequest_4)); }
	inline HTTPRequest_t583F05C0A0E42F47AD66FA8AAE73244C29833844 * get_authenticationRequest_4() const { return ___authenticationRequest_4; }
	inline HTTPRequest_t583F05C0A0E42F47AD66FA8AAE73244C29833844 ** get_address_of_authenticationRequest_4() { return &___authenticationRequest_4; }
	inline void set_authenticationRequest_4(HTTPRequest_t583F05C0A0E42F47AD66FA8AAE73244C29833844 * value)
	{
		___authenticationRequest_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___authenticationRequest_4), (void*)value);
	}

	inline static int32_t get_offset_of_isCancellationRequested_5() { return static_cast<int32_t>(offsetof(PreAuthAccessTokenAuthenticator_t424A1F277DA9B16ADCB16A5B9A7659E4B5DC6162, ___isCancellationRequested_5)); }
	inline bool get_isCancellationRequested_5() const { return ___isCancellationRequested_5; }
	inline bool* get_address_of_isCancellationRequested_5() { return &___isCancellationRequested_5; }
	inline void set_isCancellationRequested_5(bool value)
	{
		___isCancellationRequested_5 = value;
	}
};


// System.String
struct String_t  : public RuntimeObject
{
public:
	// System.Int32 System.String::m_stringLength
	int32_t ___m_stringLength_0;
	// System.Char System.String::m_firstChar
	Il2CppChar ___m_firstChar_1;

public:
	inline static int32_t get_offset_of_m_stringLength_0() { return static_cast<int32_t>(offsetof(String_t, ___m_stringLength_0)); }
	inline int32_t get_m_stringLength_0() const { return ___m_stringLength_0; }
	inline int32_t* get_address_of_m_stringLength_0() { return &___m_stringLength_0; }
	inline void set_m_stringLength_0(int32_t value)
	{
		___m_stringLength_0 = value;
	}

	inline static int32_t get_offset_of_m_firstChar_1() { return static_cast<int32_t>(offsetof(String_t, ___m_firstChar_1)); }
	inline Il2CppChar get_m_firstChar_1() const { return ___m_firstChar_1; }
	inline Il2CppChar* get_address_of_m_firstChar_1() { return &___m_firstChar_1; }
	inline void set_m_firstChar_1(Il2CppChar value)
	{
		___m_firstChar_1 = value;
	}
};

struct String_t_StaticFields
{
public:
	// System.String System.String::Empty
	String_t* ___Empty_5;

public:
	inline static int32_t get_offset_of_Empty_5() { return static_cast<int32_t>(offsetof(String_t_StaticFields, ___Empty_5)); }
	inline String_t* get_Empty_5() const { return ___Empty_5; }
	inline String_t** get_address_of_Empty_5() { return &___Empty_5; }
	inline void set_Empty_5(String_t* value)
	{
		___Empty_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___Empty_5), (void*)value);
	}
};


// System.ValueType
struct ValueType_tDBF999C1B75C48C68621878250DBF6CDBCF51E52  : public RuntimeObject
{
public:

public:
};

// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_tDBF999C1B75C48C68621878250DBF6CDBCF51E52_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_tDBF999C1B75C48C68621878250DBF6CDBCF51E52_marshaled_com
{
};

// System.Boolean
struct Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37 
{
public:
	// System.Boolean System.Boolean::m_value
	bool ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37, ___m_value_0)); }
	inline bool get_m_value_0() const { return ___m_value_0; }
	inline bool* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(bool value)
	{
		___m_value_0 = value;
	}
};

struct Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37_StaticFields
{
public:
	// System.String System.Boolean::TrueString
	String_t* ___TrueString_5;
	// System.String System.Boolean::FalseString
	String_t* ___FalseString_6;

public:
	inline static int32_t get_offset_of_TrueString_5() { return static_cast<int32_t>(offsetof(Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37_StaticFields, ___TrueString_5)); }
	inline String_t* get_TrueString_5() const { return ___TrueString_5; }
	inline String_t** get_address_of_TrueString_5() { return &___TrueString_5; }
	inline void set_TrueString_5(String_t* value)
	{
		___TrueString_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___TrueString_5), (void*)value);
	}

	inline static int32_t get_offset_of_FalseString_6() { return static_cast<int32_t>(offsetof(Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37_StaticFields, ___FalseString_6)); }
	inline String_t* get_FalseString_6() const { return ___FalseString_6; }
	inline String_t** get_address_of_FalseString_6() { return &___FalseString_6; }
	inline void set_FalseString_6(String_t* value)
	{
		___FalseString_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___FalseString_6), (void*)value);
	}
};


// System.Byte
struct Byte_t0111FAB8B8685667EDDAF77683F0D8F86B659056 
{
public:
	// System.Byte System.Byte::m_value
	uint8_t ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Byte_t0111FAB8B8685667EDDAF77683F0D8F86B659056, ___m_value_0)); }
	inline uint8_t get_m_value_0() const { return ___m_value_0; }
	inline uint8_t* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(uint8_t value)
	{
		___m_value_0 = value;
	}
};


// UnityEngine.Color
struct Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659 
{
public:
	// System.Single UnityEngine.Color::r
	float ___r_0;
	// System.Single UnityEngine.Color::g
	float ___g_1;
	// System.Single UnityEngine.Color::b
	float ___b_2;
	// System.Single UnityEngine.Color::a
	float ___a_3;

public:
	inline static int32_t get_offset_of_r_0() { return static_cast<int32_t>(offsetof(Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659, ___r_0)); }
	inline float get_r_0() const { return ___r_0; }
	inline float* get_address_of_r_0() { return &___r_0; }
	inline void set_r_0(float value)
	{
		___r_0 = value;
	}

	inline static int32_t get_offset_of_g_1() { return static_cast<int32_t>(offsetof(Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659, ___g_1)); }
	inline float get_g_1() const { return ___g_1; }
	inline float* get_address_of_g_1() { return &___g_1; }
	inline void set_g_1(float value)
	{
		___g_1 = value;
	}

	inline static int32_t get_offset_of_b_2() { return static_cast<int32_t>(offsetof(Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659, ___b_2)); }
	inline float get_b_2() const { return ___b_2; }
	inline float* get_address_of_b_2() { return &___b_2; }
	inline void set_b_2(float value)
	{
		___b_2 = value;
	}

	inline static int32_t get_offset_of_a_3() { return static_cast<int32_t>(offsetof(Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659, ___a_3)); }
	inline float get_a_3() const { return ___a_3; }
	inline float* get_address_of_a_3() { return &___a_3; }
	inline void set_a_3(float value)
	{
		___a_3 = value;
	}
};


// System.DateTime
struct DateTime_tEAF2CD16E071DF5441F40822E4CFE880E5245405 
{
public:
	// System.UInt64 System.DateTime::dateData
	uint64_t ___dateData_44;

public:
	inline static int32_t get_offset_of_dateData_44() { return static_cast<int32_t>(offsetof(DateTime_tEAF2CD16E071DF5441F40822E4CFE880E5245405, ___dateData_44)); }
	inline uint64_t get_dateData_44() const { return ___dateData_44; }
	inline uint64_t* get_address_of_dateData_44() { return &___dateData_44; }
	inline void set_dateData_44(uint64_t value)
	{
		___dateData_44 = value;
	}
};

struct DateTime_tEAF2CD16E071DF5441F40822E4CFE880E5245405_StaticFields
{
public:
	// System.Int32[] System.DateTime::DaysToMonth365
	Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* ___DaysToMonth365_29;
	// System.Int32[] System.DateTime::DaysToMonth366
	Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* ___DaysToMonth366_30;
	// System.DateTime System.DateTime::MinValue
	DateTime_tEAF2CD16E071DF5441F40822E4CFE880E5245405  ___MinValue_31;
	// System.DateTime System.DateTime::MaxValue
	DateTime_tEAF2CD16E071DF5441F40822E4CFE880E5245405  ___MaxValue_32;

public:
	inline static int32_t get_offset_of_DaysToMonth365_29() { return static_cast<int32_t>(offsetof(DateTime_tEAF2CD16E071DF5441F40822E4CFE880E5245405_StaticFields, ___DaysToMonth365_29)); }
	inline Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* get_DaysToMonth365_29() const { return ___DaysToMonth365_29; }
	inline Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32** get_address_of_DaysToMonth365_29() { return &___DaysToMonth365_29; }
	inline void set_DaysToMonth365_29(Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* value)
	{
		___DaysToMonth365_29 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___DaysToMonth365_29), (void*)value);
	}

	inline static int32_t get_offset_of_DaysToMonth366_30() { return static_cast<int32_t>(offsetof(DateTime_tEAF2CD16E071DF5441F40822E4CFE880E5245405_StaticFields, ___DaysToMonth366_30)); }
	inline Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* get_DaysToMonth366_30() const { return ___DaysToMonth366_30; }
	inline Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32** get_address_of_DaysToMonth366_30() { return &___DaysToMonth366_30; }
	inline void set_DaysToMonth366_30(Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* value)
	{
		___DaysToMonth366_30 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___DaysToMonth366_30), (void*)value);
	}

	inline static int32_t get_offset_of_MinValue_31() { return static_cast<int32_t>(offsetof(DateTime_tEAF2CD16E071DF5441F40822E4CFE880E5245405_StaticFields, ___MinValue_31)); }
	inline DateTime_tEAF2CD16E071DF5441F40822E4CFE880E5245405  get_MinValue_31() const { return ___MinValue_31; }
	inline DateTime_tEAF2CD16E071DF5441F40822E4CFE880E5245405 * get_address_of_MinValue_31() { return &___MinValue_31; }
	inline void set_MinValue_31(DateTime_tEAF2CD16E071DF5441F40822E4CFE880E5245405  value)
	{
		___MinValue_31 = value;
	}

	inline static int32_t get_offset_of_MaxValue_32() { return static_cast<int32_t>(offsetof(DateTime_tEAF2CD16E071DF5441F40822E4CFE880E5245405_StaticFields, ___MaxValue_32)); }
	inline DateTime_tEAF2CD16E071DF5441F40822E4CFE880E5245405  get_MaxValue_32() const { return ___MaxValue_32; }
	inline DateTime_tEAF2CD16E071DF5441F40822E4CFE880E5245405 * get_address_of_MaxValue_32() { return &___MaxValue_32; }
	inline void set_MaxValue_32(DateTime_tEAF2CD16E071DF5441F40822E4CFE880E5245405  value)
	{
		___MaxValue_32 = value;
	}
};


// System.Double
struct Double_t42821932CB52DE2057E685D0E1AF3DE5033D2181 
{
public:
	// System.Double System.Double::m_value
	double ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Double_t42821932CB52DE2057E685D0E1AF3DE5033D2181, ___m_value_0)); }
	inline double get_m_value_0() const { return ___m_value_0; }
	inline double* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(double value)
	{
		___m_value_0 = value;
	}
};

struct Double_t42821932CB52DE2057E685D0E1AF3DE5033D2181_StaticFields
{
public:
	// System.Double System.Double::NegativeZero
	double ___NegativeZero_7;

public:
	inline static int32_t get_offset_of_NegativeZero_7() { return static_cast<int32_t>(offsetof(Double_t42821932CB52DE2057E685D0E1AF3DE5033D2181_StaticFields, ___NegativeZero_7)); }
	inline double get_NegativeZero_7() const { return ___NegativeZero_7; }
	inline double* get_address_of_NegativeZero_7() { return &___NegativeZero_7; }
	inline void set_NegativeZero_7(double value)
	{
		___NegativeZero_7 = value;
	}
};


// UnityEngine.DrivenRectTransformTracker
struct DrivenRectTransformTracker_t7DAF937E47C63B899C7BA0E9B0F206AAB4D85AC2 
{
public:
	union
	{
		struct
		{
		};
		uint8_t DrivenRectTransformTracker_t7DAF937E47C63B899C7BA0E9B0F206AAB4D85AC2__padding[1];
	};

public:
};


// System.Enum
struct Enum_t23B90B40F60E677A8025267341651C94AE079CDA  : public ValueType_tDBF999C1B75C48C68621878250DBF6CDBCF51E52
{
public:

public:
};

struct Enum_t23B90B40F60E677A8025267341651C94AE079CDA_StaticFields
{
public:
	// System.Char[] System.Enum::enumSeperatorCharArray
	CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34* ___enumSeperatorCharArray_0;

public:
	inline static int32_t get_offset_of_enumSeperatorCharArray_0() { return static_cast<int32_t>(offsetof(Enum_t23B90B40F60E677A8025267341651C94AE079CDA_StaticFields, ___enumSeperatorCharArray_0)); }
	inline CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34* get_enumSeperatorCharArray_0() const { return ___enumSeperatorCharArray_0; }
	inline CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34** get_address_of_enumSeperatorCharArray_0() { return &___enumSeperatorCharArray_0; }
	inline void set_enumSeperatorCharArray_0(CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34* value)
	{
		___enumSeperatorCharArray_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___enumSeperatorCharArray_0), (void*)value);
	}
};

// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t23B90B40F60E677A8025267341651C94AE079CDA_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t23B90B40F60E677A8025267341651C94AE079CDA_marshaled_com
{
};

// System.Int32
struct Int32_tFDE5F8CD43D10453F6A2E0C77FE48C6CC7009046 
{
public:
	// System.Int32 System.Int32::m_value
	int32_t ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Int32_tFDE5F8CD43D10453F6A2E0C77FE48C6CC7009046, ___m_value_0)); }
	inline int32_t get_m_value_0() const { return ___m_value_0; }
	inline int32_t* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(int32_t value)
	{
		___m_value_0 = value;
	}
};


// System.IntPtr
struct IntPtr_t 
{
public:
	// System.Void* System.IntPtr::m_value
	void* ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(IntPtr_t, ___m_value_0)); }
	inline void* get_m_value_0() const { return ___m_value_0; }
	inline void** get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(void* value)
	{
		___m_value_0 = value;
	}
};

struct IntPtr_t_StaticFields
{
public:
	// System.IntPtr System.IntPtr::Zero
	intptr_t ___Zero_1;

public:
	inline static int32_t get_offset_of_Zero_1() { return static_cast<int32_t>(offsetof(IntPtr_t_StaticFields, ___Zero_1)); }
	inline intptr_t get_Zero_1() const { return ___Zero_1; }
	inline intptr_t* get_address_of_Zero_1() { return &___Zero_1; }
	inline void set_Zero_1(intptr_t value)
	{
		___Zero_1 = value;
	}
};


// UnityEngine.UI.SpriteState
struct SpriteState_t9024961148433175CE2F3D9E8E9239A8B1CAB15E 
{
public:
	// UnityEngine.Sprite UnityEngine.UI.SpriteState::m_HighlightedSprite
	Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 * ___m_HighlightedSprite_0;
	// UnityEngine.Sprite UnityEngine.UI.SpriteState::m_PressedSprite
	Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 * ___m_PressedSprite_1;
	// UnityEngine.Sprite UnityEngine.UI.SpriteState::m_SelectedSprite
	Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 * ___m_SelectedSprite_2;
	// UnityEngine.Sprite UnityEngine.UI.SpriteState::m_DisabledSprite
	Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 * ___m_DisabledSprite_3;

public:
	inline static int32_t get_offset_of_m_HighlightedSprite_0() { return static_cast<int32_t>(offsetof(SpriteState_t9024961148433175CE2F3D9E8E9239A8B1CAB15E, ___m_HighlightedSprite_0)); }
	inline Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 * get_m_HighlightedSprite_0() const { return ___m_HighlightedSprite_0; }
	inline Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 ** get_address_of_m_HighlightedSprite_0() { return &___m_HighlightedSprite_0; }
	inline void set_m_HighlightedSprite_0(Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 * value)
	{
		___m_HighlightedSprite_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_HighlightedSprite_0), (void*)value);
	}

	inline static int32_t get_offset_of_m_PressedSprite_1() { return static_cast<int32_t>(offsetof(SpriteState_t9024961148433175CE2F3D9E8E9239A8B1CAB15E, ___m_PressedSprite_1)); }
	inline Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 * get_m_PressedSprite_1() const { return ___m_PressedSprite_1; }
	inline Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 ** get_address_of_m_PressedSprite_1() { return &___m_PressedSprite_1; }
	inline void set_m_PressedSprite_1(Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 * value)
	{
		___m_PressedSprite_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_PressedSprite_1), (void*)value);
	}

	inline static int32_t get_offset_of_m_SelectedSprite_2() { return static_cast<int32_t>(offsetof(SpriteState_t9024961148433175CE2F3D9E8E9239A8B1CAB15E, ___m_SelectedSprite_2)); }
	inline Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 * get_m_SelectedSprite_2() const { return ___m_SelectedSprite_2; }
	inline Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 ** get_address_of_m_SelectedSprite_2() { return &___m_SelectedSprite_2; }
	inline void set_m_SelectedSprite_2(Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 * value)
	{
		___m_SelectedSprite_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_SelectedSprite_2), (void*)value);
	}

	inline static int32_t get_offset_of_m_DisabledSprite_3() { return static_cast<int32_t>(offsetof(SpriteState_t9024961148433175CE2F3D9E8E9239A8B1CAB15E, ___m_DisabledSprite_3)); }
	inline Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 * get_m_DisabledSprite_3() const { return ___m_DisabledSprite_3; }
	inline Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 ** get_address_of_m_DisabledSprite_3() { return &___m_DisabledSprite_3; }
	inline void set_m_DisabledSprite_3(Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 * value)
	{
		___m_DisabledSprite_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_DisabledSprite_3), (void*)value);
	}
};

// Native definition for P/Invoke marshalling of UnityEngine.UI.SpriteState
struct SpriteState_t9024961148433175CE2F3D9E8E9239A8B1CAB15E_marshaled_pinvoke
{
	Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 * ___m_HighlightedSprite_0;
	Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 * ___m_PressedSprite_1;
	Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 * ___m_SelectedSprite_2;
	Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 * ___m_DisabledSprite_3;
};
// Native definition for COM marshalling of UnityEngine.UI.SpriteState
struct SpriteState_t9024961148433175CE2F3D9E8E9239A8B1CAB15E_marshaled_com
{
	Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 * ___m_HighlightedSprite_0;
	Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 * ___m_PressedSprite_1;
	Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 * ___m_SelectedSprite_2;
	Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 * ___m_DisabledSprite_3;
};

// System.UInt16
struct UInt16_t894EA9D4FB7C799B244E7BBF2DF0EEEDBC77A8BD 
{
public:
	// System.UInt16 System.UInt16::m_value
	uint16_t ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(UInt16_t894EA9D4FB7C799B244E7BBF2DF0EEEDBC77A8BD, ___m_value_0)); }
	inline uint16_t get_m_value_0() const { return ___m_value_0; }
	inline uint16_t* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(uint16_t value)
	{
		___m_value_0 = value;
	}
};


// System.UInt32
struct UInt32_tE60352A06233E4E69DD198BCC67142159F686B15 
{
public:
	// System.UInt32 System.UInt32::m_value
	uint32_t ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(UInt32_tE60352A06233E4E69DD198BCC67142159F686B15, ___m_value_0)); }
	inline uint32_t get_m_value_0() const { return ___m_value_0; }
	inline uint32_t* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(uint32_t value)
	{
		___m_value_0 = value;
	}
};


// UnityEngine.Vector2
struct Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 
{
public:
	// System.Single UnityEngine.Vector2::x
	float ___x_0;
	// System.Single UnityEngine.Vector2::y
	float ___y_1;

public:
	inline static int32_t get_offset_of_x_0() { return static_cast<int32_t>(offsetof(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9, ___x_0)); }
	inline float get_x_0() const { return ___x_0; }
	inline float* get_address_of_x_0() { return &___x_0; }
	inline void set_x_0(float value)
	{
		___x_0 = value;
	}

	inline static int32_t get_offset_of_y_1() { return static_cast<int32_t>(offsetof(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9, ___y_1)); }
	inline float get_y_1() const { return ___y_1; }
	inline float* get_address_of_y_1() { return &___y_1; }
	inline void set_y_1(float value)
	{
		___y_1 = value;
	}
};

struct Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9_StaticFields
{
public:
	// UnityEngine.Vector2 UnityEngine.Vector2::zeroVector
	Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ___zeroVector_2;
	// UnityEngine.Vector2 UnityEngine.Vector2::oneVector
	Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ___oneVector_3;
	// UnityEngine.Vector2 UnityEngine.Vector2::upVector
	Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ___upVector_4;
	// UnityEngine.Vector2 UnityEngine.Vector2::downVector
	Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ___downVector_5;
	// UnityEngine.Vector2 UnityEngine.Vector2::leftVector
	Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ___leftVector_6;
	// UnityEngine.Vector2 UnityEngine.Vector2::rightVector
	Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ___rightVector_7;
	// UnityEngine.Vector2 UnityEngine.Vector2::positiveInfinityVector
	Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ___positiveInfinityVector_8;
	// UnityEngine.Vector2 UnityEngine.Vector2::negativeInfinityVector
	Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ___negativeInfinityVector_9;

public:
	inline static int32_t get_offset_of_zeroVector_2() { return static_cast<int32_t>(offsetof(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9_StaticFields, ___zeroVector_2)); }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  get_zeroVector_2() const { return ___zeroVector_2; }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 * get_address_of_zeroVector_2() { return &___zeroVector_2; }
	inline void set_zeroVector_2(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  value)
	{
		___zeroVector_2 = value;
	}

	inline static int32_t get_offset_of_oneVector_3() { return static_cast<int32_t>(offsetof(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9_StaticFields, ___oneVector_3)); }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  get_oneVector_3() const { return ___oneVector_3; }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 * get_address_of_oneVector_3() { return &___oneVector_3; }
	inline void set_oneVector_3(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  value)
	{
		___oneVector_3 = value;
	}

	inline static int32_t get_offset_of_upVector_4() { return static_cast<int32_t>(offsetof(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9_StaticFields, ___upVector_4)); }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  get_upVector_4() const { return ___upVector_4; }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 * get_address_of_upVector_4() { return &___upVector_4; }
	inline void set_upVector_4(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  value)
	{
		___upVector_4 = value;
	}

	inline static int32_t get_offset_of_downVector_5() { return static_cast<int32_t>(offsetof(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9_StaticFields, ___downVector_5)); }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  get_downVector_5() const { return ___downVector_5; }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 * get_address_of_downVector_5() { return &___downVector_5; }
	inline void set_downVector_5(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  value)
	{
		___downVector_5 = value;
	}

	inline static int32_t get_offset_of_leftVector_6() { return static_cast<int32_t>(offsetof(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9_StaticFields, ___leftVector_6)); }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  get_leftVector_6() const { return ___leftVector_6; }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 * get_address_of_leftVector_6() { return &___leftVector_6; }
	inline void set_leftVector_6(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  value)
	{
		___leftVector_6 = value;
	}

	inline static int32_t get_offset_of_rightVector_7() { return static_cast<int32_t>(offsetof(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9_StaticFields, ___rightVector_7)); }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  get_rightVector_7() const { return ___rightVector_7; }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 * get_address_of_rightVector_7() { return &___rightVector_7; }
	inline void set_rightVector_7(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  value)
	{
		___rightVector_7 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_8() { return static_cast<int32_t>(offsetof(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9_StaticFields, ___positiveInfinityVector_8)); }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  get_positiveInfinityVector_8() const { return ___positiveInfinityVector_8; }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 * get_address_of_positiveInfinityVector_8() { return &___positiveInfinityVector_8; }
	inline void set_positiveInfinityVector_8(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  value)
	{
		___positiveInfinityVector_8 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_9() { return static_cast<int32_t>(offsetof(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9_StaticFields, ___negativeInfinityVector_9)); }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  get_negativeInfinityVector_9() const { return ___negativeInfinityVector_9; }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 * get_address_of_negativeInfinityVector_9() { return &___negativeInfinityVector_9; }
	inline void set_negativeInfinityVector_9(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  value)
	{
		___negativeInfinityVector_9 = value;
	}
};


// UnityEngine.Vector3
struct Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E 
{
public:
	// System.Single UnityEngine.Vector3::x
	float ___x_2;
	// System.Single UnityEngine.Vector3::y
	float ___y_3;
	// System.Single UnityEngine.Vector3::z
	float ___z_4;

public:
	inline static int32_t get_offset_of_x_2() { return static_cast<int32_t>(offsetof(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E, ___x_2)); }
	inline float get_x_2() const { return ___x_2; }
	inline float* get_address_of_x_2() { return &___x_2; }
	inline void set_x_2(float value)
	{
		___x_2 = value;
	}

	inline static int32_t get_offset_of_y_3() { return static_cast<int32_t>(offsetof(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E, ___y_3)); }
	inline float get_y_3() const { return ___y_3; }
	inline float* get_address_of_y_3() { return &___y_3; }
	inline void set_y_3(float value)
	{
		___y_3 = value;
	}

	inline static int32_t get_offset_of_z_4() { return static_cast<int32_t>(offsetof(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E, ___z_4)); }
	inline float get_z_4() const { return ___z_4; }
	inline float* get_address_of_z_4() { return &___z_4; }
	inline void set_z_4(float value)
	{
		___z_4 = value;
	}
};

struct Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E_StaticFields
{
public:
	// UnityEngine.Vector3 UnityEngine.Vector3::zeroVector
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___zeroVector_5;
	// UnityEngine.Vector3 UnityEngine.Vector3::oneVector
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___oneVector_6;
	// UnityEngine.Vector3 UnityEngine.Vector3::upVector
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___upVector_7;
	// UnityEngine.Vector3 UnityEngine.Vector3::downVector
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___downVector_8;
	// UnityEngine.Vector3 UnityEngine.Vector3::leftVector
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___leftVector_9;
	// UnityEngine.Vector3 UnityEngine.Vector3::rightVector
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___rightVector_10;
	// UnityEngine.Vector3 UnityEngine.Vector3::forwardVector
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___forwardVector_11;
	// UnityEngine.Vector3 UnityEngine.Vector3::backVector
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___backVector_12;
	// UnityEngine.Vector3 UnityEngine.Vector3::positiveInfinityVector
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___positiveInfinityVector_13;
	// UnityEngine.Vector3 UnityEngine.Vector3::negativeInfinityVector
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___negativeInfinityVector_14;

public:
	inline static int32_t get_offset_of_zeroVector_5() { return static_cast<int32_t>(offsetof(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E_StaticFields, ___zeroVector_5)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_zeroVector_5() const { return ___zeroVector_5; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_zeroVector_5() { return &___zeroVector_5; }
	inline void set_zeroVector_5(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___zeroVector_5 = value;
	}

	inline static int32_t get_offset_of_oneVector_6() { return static_cast<int32_t>(offsetof(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E_StaticFields, ___oneVector_6)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_oneVector_6() const { return ___oneVector_6; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_oneVector_6() { return &___oneVector_6; }
	inline void set_oneVector_6(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___oneVector_6 = value;
	}

	inline static int32_t get_offset_of_upVector_7() { return static_cast<int32_t>(offsetof(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E_StaticFields, ___upVector_7)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_upVector_7() const { return ___upVector_7; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_upVector_7() { return &___upVector_7; }
	inline void set_upVector_7(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___upVector_7 = value;
	}

	inline static int32_t get_offset_of_downVector_8() { return static_cast<int32_t>(offsetof(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E_StaticFields, ___downVector_8)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_downVector_8() const { return ___downVector_8; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_downVector_8() { return &___downVector_8; }
	inline void set_downVector_8(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___downVector_8 = value;
	}

	inline static int32_t get_offset_of_leftVector_9() { return static_cast<int32_t>(offsetof(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E_StaticFields, ___leftVector_9)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_leftVector_9() const { return ___leftVector_9; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_leftVector_9() { return &___leftVector_9; }
	inline void set_leftVector_9(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___leftVector_9 = value;
	}

	inline static int32_t get_offset_of_rightVector_10() { return static_cast<int32_t>(offsetof(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E_StaticFields, ___rightVector_10)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_rightVector_10() const { return ___rightVector_10; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_rightVector_10() { return &___rightVector_10; }
	inline void set_rightVector_10(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___rightVector_10 = value;
	}

	inline static int32_t get_offset_of_forwardVector_11() { return static_cast<int32_t>(offsetof(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E_StaticFields, ___forwardVector_11)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_forwardVector_11() const { return ___forwardVector_11; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_forwardVector_11() { return &___forwardVector_11; }
	inline void set_forwardVector_11(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___forwardVector_11 = value;
	}

	inline static int32_t get_offset_of_backVector_12() { return static_cast<int32_t>(offsetof(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E_StaticFields, ___backVector_12)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_backVector_12() const { return ___backVector_12; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_backVector_12() { return &___backVector_12; }
	inline void set_backVector_12(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___backVector_12 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_13() { return static_cast<int32_t>(offsetof(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E_StaticFields, ___positiveInfinityVector_13)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_positiveInfinityVector_13() const { return ___positiveInfinityVector_13; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_positiveInfinityVector_13() { return &___positiveInfinityVector_13; }
	inline void set_positiveInfinityVector_13(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___positiveInfinityVector_13 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_14() { return static_cast<int32_t>(offsetof(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E_StaticFields, ___negativeInfinityVector_14)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_negativeInfinityVector_14() const { return ___negativeInfinityVector_14; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_negativeInfinityVector_14() { return &___negativeInfinityVector_14; }
	inline void set_negativeInfinityVector_14(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___negativeInfinityVector_14 = value;
	}
};


// System.Void
struct Void_t700C6383A2A510C2CF4DD86DABD5CA9FF70ADAC5 
{
public:
	union
	{
		struct
		{
		};
		uint8_t Void_t700C6383A2A510C2CF4DD86DABD5CA9FF70ADAC5__padding[1];
	};

public:
};


// BestHTTP.Connections.HTTP2.HuffmanEncoder/TableEntry
struct TableEntry_tDFCA5A567802D72C4E23BFC6CEA14C32F52229FF 
{
public:
	// System.UInt32 BestHTTP.Connections.HTTP2.HuffmanEncoder/TableEntry::Code
	uint32_t ___Code_0;
	// System.Byte BestHTTP.Connections.HTTP2.HuffmanEncoder/TableEntry::Bits
	uint8_t ___Bits_1;

public:
	inline static int32_t get_offset_of_Code_0() { return static_cast<int32_t>(offsetof(TableEntry_tDFCA5A567802D72C4E23BFC6CEA14C32F52229FF, ___Code_0)); }
	inline uint32_t get_Code_0() const { return ___Code_0; }
	inline uint32_t* get_address_of_Code_0() { return &___Code_0; }
	inline void set_Code_0(uint32_t value)
	{
		___Code_0 = value;
	}

	inline static int32_t get_offset_of_Bits_1() { return static_cast<int32_t>(offsetof(TableEntry_tDFCA5A567802D72C4E23BFC6CEA14C32F52229FF, ___Bits_1)); }
	inline uint8_t get_Bits_1() const { return ___Bits_1; }
	inline uint8_t* get_address_of_Bits_1() { return &___Bits_1; }
	inline void set_Bits_1(uint8_t value)
	{
		___Bits_1 = value;
	}
};


// BestHTTP.Connections.HTTP2.HuffmanEncoder/TreeNode
struct TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F 
{
public:
	// System.UInt16 BestHTTP.Connections.HTTP2.HuffmanEncoder/TreeNode::NextZeroIdx
	uint16_t ___NextZeroIdx_0;
	// System.UInt16 BestHTTP.Connections.HTTP2.HuffmanEncoder/TreeNode::NextOneIdx
	uint16_t ___NextOneIdx_1;
	// System.UInt16 BestHTTP.Connections.HTTP2.HuffmanEncoder/TreeNode::Value
	uint16_t ___Value_2;

public:
	inline static int32_t get_offset_of_NextZeroIdx_0() { return static_cast<int32_t>(offsetof(TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F, ___NextZeroIdx_0)); }
	inline uint16_t get_NextZeroIdx_0() const { return ___NextZeroIdx_0; }
	inline uint16_t* get_address_of_NextZeroIdx_0() { return &___NextZeroIdx_0; }
	inline void set_NextZeroIdx_0(uint16_t value)
	{
		___NextZeroIdx_0 = value;
	}

	inline static int32_t get_offset_of_NextOneIdx_1() { return static_cast<int32_t>(offsetof(TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F, ___NextOneIdx_1)); }
	inline uint16_t get_NextOneIdx_1() const { return ___NextOneIdx_1; }
	inline uint16_t* get_address_of_NextOneIdx_1() { return &___NextOneIdx_1; }
	inline void set_NextOneIdx_1(uint16_t value)
	{
		___NextOneIdx_1 = value;
	}

	inline static int32_t get_offset_of_Value_2() { return static_cast<int32_t>(offsetof(TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F, ___Value_2)); }
	inline uint16_t get_Value_2() const { return ___Value_2; }
	inline uint16_t* get_address_of_Value_2() { return &___Value_2; }
	inline void set_Value_2(uint16_t value)
	{
		___Value_2 = value;
	}
};


// UnityEngine.Bounds
struct Bounds_t0F1F36D4F7AF49524B3C2A2259594412A3D3AE37 
{
public:
	// UnityEngine.Vector3 UnityEngine.Bounds::m_Center
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___m_Center_0;
	// UnityEngine.Vector3 UnityEngine.Bounds::m_Extents
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___m_Extents_1;

public:
	inline static int32_t get_offset_of_m_Center_0() { return static_cast<int32_t>(offsetof(Bounds_t0F1F36D4F7AF49524B3C2A2259594412A3D3AE37, ___m_Center_0)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_m_Center_0() const { return ___m_Center_0; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_m_Center_0() { return &___m_Center_0; }
	inline void set_m_Center_0(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___m_Center_0 = value;
	}

	inline static int32_t get_offset_of_m_Extents_1() { return static_cast<int32_t>(offsetof(Bounds_t0F1F36D4F7AF49524B3C2A2259594412A3D3AE37, ___m_Extents_1)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_m_Extents_1() const { return ___m_Extents_1; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_m_Extents_1() { return &___m_Extents_1; }
	inline void set_m_Extents_1(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___m_Extents_1 = value;
	}
};


// UnityEngine.UI.ColorBlock
struct ColorBlock_t04DFBB97B4772D2E00FD17ED2E3E6590E6916955 
{
public:
	// UnityEngine.Color UnityEngine.UI.ColorBlock::m_NormalColor
	Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  ___m_NormalColor_0;
	// UnityEngine.Color UnityEngine.UI.ColorBlock::m_HighlightedColor
	Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  ___m_HighlightedColor_1;
	// UnityEngine.Color UnityEngine.UI.ColorBlock::m_PressedColor
	Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  ___m_PressedColor_2;
	// UnityEngine.Color UnityEngine.UI.ColorBlock::m_SelectedColor
	Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  ___m_SelectedColor_3;
	// UnityEngine.Color UnityEngine.UI.ColorBlock::m_DisabledColor
	Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  ___m_DisabledColor_4;
	// System.Single UnityEngine.UI.ColorBlock::m_ColorMultiplier
	float ___m_ColorMultiplier_5;
	// System.Single UnityEngine.UI.ColorBlock::m_FadeDuration
	float ___m_FadeDuration_6;

public:
	inline static int32_t get_offset_of_m_NormalColor_0() { return static_cast<int32_t>(offsetof(ColorBlock_t04DFBB97B4772D2E00FD17ED2E3E6590E6916955, ___m_NormalColor_0)); }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  get_m_NormalColor_0() const { return ___m_NormalColor_0; }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659 * get_address_of_m_NormalColor_0() { return &___m_NormalColor_0; }
	inline void set_m_NormalColor_0(Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  value)
	{
		___m_NormalColor_0 = value;
	}

	inline static int32_t get_offset_of_m_HighlightedColor_1() { return static_cast<int32_t>(offsetof(ColorBlock_t04DFBB97B4772D2E00FD17ED2E3E6590E6916955, ___m_HighlightedColor_1)); }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  get_m_HighlightedColor_1() const { return ___m_HighlightedColor_1; }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659 * get_address_of_m_HighlightedColor_1() { return &___m_HighlightedColor_1; }
	inline void set_m_HighlightedColor_1(Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  value)
	{
		___m_HighlightedColor_1 = value;
	}

	inline static int32_t get_offset_of_m_PressedColor_2() { return static_cast<int32_t>(offsetof(ColorBlock_t04DFBB97B4772D2E00FD17ED2E3E6590E6916955, ___m_PressedColor_2)); }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  get_m_PressedColor_2() const { return ___m_PressedColor_2; }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659 * get_address_of_m_PressedColor_2() { return &___m_PressedColor_2; }
	inline void set_m_PressedColor_2(Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  value)
	{
		___m_PressedColor_2 = value;
	}

	inline static int32_t get_offset_of_m_SelectedColor_3() { return static_cast<int32_t>(offsetof(ColorBlock_t04DFBB97B4772D2E00FD17ED2E3E6590E6916955, ___m_SelectedColor_3)); }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  get_m_SelectedColor_3() const { return ___m_SelectedColor_3; }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659 * get_address_of_m_SelectedColor_3() { return &___m_SelectedColor_3; }
	inline void set_m_SelectedColor_3(Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  value)
	{
		___m_SelectedColor_3 = value;
	}

	inline static int32_t get_offset_of_m_DisabledColor_4() { return static_cast<int32_t>(offsetof(ColorBlock_t04DFBB97B4772D2E00FD17ED2E3E6590E6916955, ___m_DisabledColor_4)); }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  get_m_DisabledColor_4() const { return ___m_DisabledColor_4; }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659 * get_address_of_m_DisabledColor_4() { return &___m_DisabledColor_4; }
	inline void set_m_DisabledColor_4(Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  value)
	{
		___m_DisabledColor_4 = value;
	}

	inline static int32_t get_offset_of_m_ColorMultiplier_5() { return static_cast<int32_t>(offsetof(ColorBlock_t04DFBB97B4772D2E00FD17ED2E3E6590E6916955, ___m_ColorMultiplier_5)); }
	inline float get_m_ColorMultiplier_5() const { return ___m_ColorMultiplier_5; }
	inline float* get_address_of_m_ColorMultiplier_5() { return &___m_ColorMultiplier_5; }
	inline void set_m_ColorMultiplier_5(float value)
	{
		___m_ColorMultiplier_5 = value;
	}

	inline static int32_t get_offset_of_m_FadeDuration_6() { return static_cast<int32_t>(offsetof(ColorBlock_t04DFBB97B4772D2E00FD17ED2E3E6590E6916955, ___m_FadeDuration_6)); }
	inline float get_m_FadeDuration_6() const { return ___m_FadeDuration_6; }
	inline float* get_address_of_m_FadeDuration_6() { return &___m_FadeDuration_6; }
	inline void set_m_FadeDuration_6(float value)
	{
		___m_FadeDuration_6 = value;
	}
};

struct ColorBlock_t04DFBB97B4772D2E00FD17ED2E3E6590E6916955_StaticFields
{
public:
	// UnityEngine.UI.ColorBlock UnityEngine.UI.ColorBlock::defaultColorBlock
	ColorBlock_t04DFBB97B4772D2E00FD17ED2E3E6590E6916955  ___defaultColorBlock_7;

public:
	inline static int32_t get_offset_of_defaultColorBlock_7() { return static_cast<int32_t>(offsetof(ColorBlock_t04DFBB97B4772D2E00FD17ED2E3E6590E6916955_StaticFields, ___defaultColorBlock_7)); }
	inline ColorBlock_t04DFBB97B4772D2E00FD17ED2E3E6590E6916955  get_defaultColorBlock_7() const { return ___defaultColorBlock_7; }
	inline ColorBlock_t04DFBB97B4772D2E00FD17ED2E3E6590E6916955 * get_address_of_defaultColorBlock_7() { return &___defaultColorBlock_7; }
	inline void set_defaultColorBlock_7(ColorBlock_t04DFBB97B4772D2E00FD17ED2E3E6590E6916955  value)
	{
		___defaultColorBlock_7 = value;
	}
};


// BestHTTP.SignalRCore.ConnectionStates
struct ConnectionStates_t415B499776148FFF9D274DCA1F5E46F6501F8D34 
{
public:
	// System.Int32 BestHTTP.SignalRCore.ConnectionStates::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(ConnectionStates_t415B499776148FFF9D274DCA1F5E46F6501F8D34, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// System.Delegate
struct Delegate_t  : public RuntimeObject
{
public:
	// System.IntPtr System.Delegate::method_ptr
	Il2CppMethodPointer ___method_ptr_0;
	// System.IntPtr System.Delegate::invoke_impl
	intptr_t ___invoke_impl_1;
	// System.Object System.Delegate::m_target
	RuntimeObject * ___m_target_2;
	// System.IntPtr System.Delegate::method
	intptr_t ___method_3;
	// System.IntPtr System.Delegate::delegate_trampoline
	intptr_t ___delegate_trampoline_4;
	// System.IntPtr System.Delegate::extra_arg
	intptr_t ___extra_arg_5;
	// System.IntPtr System.Delegate::method_code
	intptr_t ___method_code_6;
	// System.Reflection.MethodInfo System.Delegate::method_info
	MethodInfo_t * ___method_info_7;
	// System.Reflection.MethodInfo System.Delegate::original_method_info
	MethodInfo_t * ___original_method_info_8;
	// System.DelegateData System.Delegate::data
	DelegateData_t17DD30660E330C49381DAA99F934BE75CB11F288 * ___data_9;
	// System.Boolean System.Delegate::method_is_virtual
	bool ___method_is_virtual_10;

public:
	inline static int32_t get_offset_of_method_ptr_0() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_ptr_0)); }
	inline Il2CppMethodPointer get_method_ptr_0() const { return ___method_ptr_0; }
	inline Il2CppMethodPointer* get_address_of_method_ptr_0() { return &___method_ptr_0; }
	inline void set_method_ptr_0(Il2CppMethodPointer value)
	{
		___method_ptr_0 = value;
	}

	inline static int32_t get_offset_of_invoke_impl_1() { return static_cast<int32_t>(offsetof(Delegate_t, ___invoke_impl_1)); }
	inline intptr_t get_invoke_impl_1() const { return ___invoke_impl_1; }
	inline intptr_t* get_address_of_invoke_impl_1() { return &___invoke_impl_1; }
	inline void set_invoke_impl_1(intptr_t value)
	{
		___invoke_impl_1 = value;
	}

	inline static int32_t get_offset_of_m_target_2() { return static_cast<int32_t>(offsetof(Delegate_t, ___m_target_2)); }
	inline RuntimeObject * get_m_target_2() const { return ___m_target_2; }
	inline RuntimeObject ** get_address_of_m_target_2() { return &___m_target_2; }
	inline void set_m_target_2(RuntimeObject * value)
	{
		___m_target_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_target_2), (void*)value);
	}

	inline static int32_t get_offset_of_method_3() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_3)); }
	inline intptr_t get_method_3() const { return ___method_3; }
	inline intptr_t* get_address_of_method_3() { return &___method_3; }
	inline void set_method_3(intptr_t value)
	{
		___method_3 = value;
	}

	inline static int32_t get_offset_of_delegate_trampoline_4() { return static_cast<int32_t>(offsetof(Delegate_t, ___delegate_trampoline_4)); }
	inline intptr_t get_delegate_trampoline_4() const { return ___delegate_trampoline_4; }
	inline intptr_t* get_address_of_delegate_trampoline_4() { return &___delegate_trampoline_4; }
	inline void set_delegate_trampoline_4(intptr_t value)
	{
		___delegate_trampoline_4 = value;
	}

	inline static int32_t get_offset_of_extra_arg_5() { return static_cast<int32_t>(offsetof(Delegate_t, ___extra_arg_5)); }
	inline intptr_t get_extra_arg_5() const { return ___extra_arg_5; }
	inline intptr_t* get_address_of_extra_arg_5() { return &___extra_arg_5; }
	inline void set_extra_arg_5(intptr_t value)
	{
		___extra_arg_5 = value;
	}

	inline static int32_t get_offset_of_method_code_6() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_code_6)); }
	inline intptr_t get_method_code_6() const { return ___method_code_6; }
	inline intptr_t* get_address_of_method_code_6() { return &___method_code_6; }
	inline void set_method_code_6(intptr_t value)
	{
		___method_code_6 = value;
	}

	inline static int32_t get_offset_of_method_info_7() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_info_7)); }
	inline MethodInfo_t * get_method_info_7() const { return ___method_info_7; }
	inline MethodInfo_t ** get_address_of_method_info_7() { return &___method_info_7; }
	inline void set_method_info_7(MethodInfo_t * value)
	{
		___method_info_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___method_info_7), (void*)value);
	}

	inline static int32_t get_offset_of_original_method_info_8() { return static_cast<int32_t>(offsetof(Delegate_t, ___original_method_info_8)); }
	inline MethodInfo_t * get_original_method_info_8() const { return ___original_method_info_8; }
	inline MethodInfo_t ** get_address_of_original_method_info_8() { return &___original_method_info_8; }
	inline void set_original_method_info_8(MethodInfo_t * value)
	{
		___original_method_info_8 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___original_method_info_8), (void*)value);
	}

	inline static int32_t get_offset_of_data_9() { return static_cast<int32_t>(offsetof(Delegate_t, ___data_9)); }
	inline DelegateData_t17DD30660E330C49381DAA99F934BE75CB11F288 * get_data_9() const { return ___data_9; }
	inline DelegateData_t17DD30660E330C49381DAA99F934BE75CB11F288 ** get_address_of_data_9() { return &___data_9; }
	inline void set_data_9(DelegateData_t17DD30660E330C49381DAA99F934BE75CB11F288 * value)
	{
		___data_9 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___data_9), (void*)value);
	}

	inline static int32_t get_offset_of_method_is_virtual_10() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_is_virtual_10)); }
	inline bool get_method_is_virtual_10() const { return ___method_is_virtual_10; }
	inline bool* get_address_of_method_is_virtual_10() { return &___method_is_virtual_10; }
	inline void set_method_is_virtual_10(bool value)
	{
		___method_is_virtual_10 = value;
	}
};

// Native definition for P/Invoke marshalling of System.Delegate
struct Delegate_t_marshaled_pinvoke
{
	intptr_t ___method_ptr_0;
	intptr_t ___invoke_impl_1;
	Il2CppIUnknown* ___m_target_2;
	intptr_t ___method_3;
	intptr_t ___delegate_trampoline_4;
	intptr_t ___extra_arg_5;
	intptr_t ___method_code_6;
	MethodInfo_t * ___method_info_7;
	MethodInfo_t * ___original_method_info_8;
	DelegateData_t17DD30660E330C49381DAA99F934BE75CB11F288 * ___data_9;
	int32_t ___method_is_virtual_10;
};
// Native definition for COM marshalling of System.Delegate
struct Delegate_t_marshaled_com
{
	intptr_t ___method_ptr_0;
	intptr_t ___invoke_impl_1;
	Il2CppIUnknown* ___m_target_2;
	intptr_t ___method_3;
	intptr_t ___delegate_trampoline_4;
	intptr_t ___extra_arg_5;
	intptr_t ___method_code_6;
	MethodInfo_t * ___method_info_7;
	MethodInfo_t * ___original_method_info_8;
	DelegateData_t17DD30660E330C49381DAA99F934BE75CB11F288 * ___data_9;
	int32_t ___method_is_virtual_10;
};

// System.Exception
struct Exception_t  : public RuntimeObject
{
public:
	// System.String System.Exception::_className
	String_t* ____className_1;
	// System.String System.Exception::_message
	String_t* ____message_2;
	// System.Collections.IDictionary System.Exception::_data
	RuntimeObject* ____data_3;
	// System.Exception System.Exception::_innerException
	Exception_t * ____innerException_4;
	// System.String System.Exception::_helpURL
	String_t* ____helpURL_5;
	// System.Object System.Exception::_stackTrace
	RuntimeObject * ____stackTrace_6;
	// System.String System.Exception::_stackTraceString
	String_t* ____stackTraceString_7;
	// System.String System.Exception::_remoteStackTraceString
	String_t* ____remoteStackTraceString_8;
	// System.Int32 System.Exception::_remoteStackIndex
	int32_t ____remoteStackIndex_9;
	// System.Object System.Exception::_dynamicMethods
	RuntimeObject * ____dynamicMethods_10;
	// System.Int32 System.Exception::_HResult
	int32_t ____HResult_11;
	// System.String System.Exception::_source
	String_t* ____source_12;
	// System.Runtime.Serialization.SafeSerializationManager System.Exception::_safeSerializationManager
	SafeSerializationManager_tDE44F029589A028F8A3053C5C06153FAB4AAE29F * ____safeSerializationManager_13;
	// System.Diagnostics.StackTrace[] System.Exception::captured_traces
	StackTraceU5BU5D_t4AD999C288CB6D1F38A299D12B1598D606588971* ___captured_traces_14;
	// System.IntPtr[] System.Exception::native_trace_ips
	IntPtrU5BU5D_t27FC72B0409D75AAF33EC42498E8094E95FEE9A6* ___native_trace_ips_15;

public:
	inline static int32_t get_offset_of__className_1() { return static_cast<int32_t>(offsetof(Exception_t, ____className_1)); }
	inline String_t* get__className_1() const { return ____className_1; }
	inline String_t** get_address_of__className_1() { return &____className_1; }
	inline void set__className_1(String_t* value)
	{
		____className_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____className_1), (void*)value);
	}

	inline static int32_t get_offset_of__message_2() { return static_cast<int32_t>(offsetof(Exception_t, ____message_2)); }
	inline String_t* get__message_2() const { return ____message_2; }
	inline String_t** get_address_of__message_2() { return &____message_2; }
	inline void set__message_2(String_t* value)
	{
		____message_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____message_2), (void*)value);
	}

	inline static int32_t get_offset_of__data_3() { return static_cast<int32_t>(offsetof(Exception_t, ____data_3)); }
	inline RuntimeObject* get__data_3() const { return ____data_3; }
	inline RuntimeObject** get_address_of__data_3() { return &____data_3; }
	inline void set__data_3(RuntimeObject* value)
	{
		____data_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____data_3), (void*)value);
	}

	inline static int32_t get_offset_of__innerException_4() { return static_cast<int32_t>(offsetof(Exception_t, ____innerException_4)); }
	inline Exception_t * get__innerException_4() const { return ____innerException_4; }
	inline Exception_t ** get_address_of__innerException_4() { return &____innerException_4; }
	inline void set__innerException_4(Exception_t * value)
	{
		____innerException_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____innerException_4), (void*)value);
	}

	inline static int32_t get_offset_of__helpURL_5() { return static_cast<int32_t>(offsetof(Exception_t, ____helpURL_5)); }
	inline String_t* get__helpURL_5() const { return ____helpURL_5; }
	inline String_t** get_address_of__helpURL_5() { return &____helpURL_5; }
	inline void set__helpURL_5(String_t* value)
	{
		____helpURL_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____helpURL_5), (void*)value);
	}

	inline static int32_t get_offset_of__stackTrace_6() { return static_cast<int32_t>(offsetof(Exception_t, ____stackTrace_6)); }
	inline RuntimeObject * get__stackTrace_6() const { return ____stackTrace_6; }
	inline RuntimeObject ** get_address_of__stackTrace_6() { return &____stackTrace_6; }
	inline void set__stackTrace_6(RuntimeObject * value)
	{
		____stackTrace_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____stackTrace_6), (void*)value);
	}

	inline static int32_t get_offset_of__stackTraceString_7() { return static_cast<int32_t>(offsetof(Exception_t, ____stackTraceString_7)); }
	inline String_t* get__stackTraceString_7() const { return ____stackTraceString_7; }
	inline String_t** get_address_of__stackTraceString_7() { return &____stackTraceString_7; }
	inline void set__stackTraceString_7(String_t* value)
	{
		____stackTraceString_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____stackTraceString_7), (void*)value);
	}

	inline static int32_t get_offset_of__remoteStackTraceString_8() { return static_cast<int32_t>(offsetof(Exception_t, ____remoteStackTraceString_8)); }
	inline String_t* get__remoteStackTraceString_8() const { return ____remoteStackTraceString_8; }
	inline String_t** get_address_of__remoteStackTraceString_8() { return &____remoteStackTraceString_8; }
	inline void set__remoteStackTraceString_8(String_t* value)
	{
		____remoteStackTraceString_8 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____remoteStackTraceString_8), (void*)value);
	}

	inline static int32_t get_offset_of__remoteStackIndex_9() { return static_cast<int32_t>(offsetof(Exception_t, ____remoteStackIndex_9)); }
	inline int32_t get__remoteStackIndex_9() const { return ____remoteStackIndex_9; }
	inline int32_t* get_address_of__remoteStackIndex_9() { return &____remoteStackIndex_9; }
	inline void set__remoteStackIndex_9(int32_t value)
	{
		____remoteStackIndex_9 = value;
	}

	inline static int32_t get_offset_of__dynamicMethods_10() { return static_cast<int32_t>(offsetof(Exception_t, ____dynamicMethods_10)); }
	inline RuntimeObject * get__dynamicMethods_10() const { return ____dynamicMethods_10; }
	inline RuntimeObject ** get_address_of__dynamicMethods_10() { return &____dynamicMethods_10; }
	inline void set__dynamicMethods_10(RuntimeObject * value)
	{
		____dynamicMethods_10 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____dynamicMethods_10), (void*)value);
	}

	inline static int32_t get_offset_of__HResult_11() { return static_cast<int32_t>(offsetof(Exception_t, ____HResult_11)); }
	inline int32_t get__HResult_11() const { return ____HResult_11; }
	inline int32_t* get_address_of__HResult_11() { return &____HResult_11; }
	inline void set__HResult_11(int32_t value)
	{
		____HResult_11 = value;
	}

	inline static int32_t get_offset_of__source_12() { return static_cast<int32_t>(offsetof(Exception_t, ____source_12)); }
	inline String_t* get__source_12() const { return ____source_12; }
	inline String_t** get_address_of__source_12() { return &____source_12; }
	inline void set__source_12(String_t* value)
	{
		____source_12 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____source_12), (void*)value);
	}

	inline static int32_t get_offset_of__safeSerializationManager_13() { return static_cast<int32_t>(offsetof(Exception_t, ____safeSerializationManager_13)); }
	inline SafeSerializationManager_tDE44F029589A028F8A3053C5C06153FAB4AAE29F * get__safeSerializationManager_13() const { return ____safeSerializationManager_13; }
	inline SafeSerializationManager_tDE44F029589A028F8A3053C5C06153FAB4AAE29F ** get_address_of__safeSerializationManager_13() { return &____safeSerializationManager_13; }
	inline void set__safeSerializationManager_13(SafeSerializationManager_tDE44F029589A028F8A3053C5C06153FAB4AAE29F * value)
	{
		____safeSerializationManager_13 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____safeSerializationManager_13), (void*)value);
	}

	inline static int32_t get_offset_of_captured_traces_14() { return static_cast<int32_t>(offsetof(Exception_t, ___captured_traces_14)); }
	inline StackTraceU5BU5D_t4AD999C288CB6D1F38A299D12B1598D606588971* get_captured_traces_14() const { return ___captured_traces_14; }
	inline StackTraceU5BU5D_t4AD999C288CB6D1F38A299D12B1598D606588971** get_address_of_captured_traces_14() { return &___captured_traces_14; }
	inline void set_captured_traces_14(StackTraceU5BU5D_t4AD999C288CB6D1F38A299D12B1598D606588971* value)
	{
		___captured_traces_14 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___captured_traces_14), (void*)value);
	}

	inline static int32_t get_offset_of_native_trace_ips_15() { return static_cast<int32_t>(offsetof(Exception_t, ___native_trace_ips_15)); }
	inline IntPtrU5BU5D_t27FC72B0409D75AAF33EC42498E8094E95FEE9A6* get_native_trace_ips_15() const { return ___native_trace_ips_15; }
	inline IntPtrU5BU5D_t27FC72B0409D75AAF33EC42498E8094E95FEE9A6** get_address_of_native_trace_ips_15() { return &___native_trace_ips_15; }
	inline void set_native_trace_ips_15(IntPtrU5BU5D_t27FC72B0409D75AAF33EC42498E8094E95FEE9A6* value)
	{
		___native_trace_ips_15 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___native_trace_ips_15), (void*)value);
	}
};

struct Exception_t_StaticFields
{
public:
	// System.Object System.Exception::s_EDILock
	RuntimeObject * ___s_EDILock_0;

public:
	inline static int32_t get_offset_of_s_EDILock_0() { return static_cast<int32_t>(offsetof(Exception_t_StaticFields, ___s_EDILock_0)); }
	inline RuntimeObject * get_s_EDILock_0() const { return ___s_EDILock_0; }
	inline RuntimeObject ** get_address_of_s_EDILock_0() { return &___s_EDILock_0; }
	inline void set_s_EDILock_0(RuntimeObject * value)
	{
		___s_EDILock_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___s_EDILock_0), (void*)value);
	}
};

// Native definition for P/Invoke marshalling of System.Exception
struct Exception_t_marshaled_pinvoke
{
	char* ____className_1;
	char* ____message_2;
	RuntimeObject* ____data_3;
	Exception_t_marshaled_pinvoke* ____innerException_4;
	char* ____helpURL_5;
	Il2CppIUnknown* ____stackTrace_6;
	char* ____stackTraceString_7;
	char* ____remoteStackTraceString_8;
	int32_t ____remoteStackIndex_9;
	Il2CppIUnknown* ____dynamicMethods_10;
	int32_t ____HResult_11;
	char* ____source_12;
	SafeSerializationManager_tDE44F029589A028F8A3053C5C06153FAB4AAE29F * ____safeSerializationManager_13;
	StackTraceU5BU5D_t4AD999C288CB6D1F38A299D12B1598D606588971* ___captured_traces_14;
	Il2CppSafeArray/*NONE*/* ___native_trace_ips_15;
};
// Native definition for COM marshalling of System.Exception
struct Exception_t_marshaled_com
{
	Il2CppChar* ____className_1;
	Il2CppChar* ____message_2;
	RuntimeObject* ____data_3;
	Exception_t_marshaled_com* ____innerException_4;
	Il2CppChar* ____helpURL_5;
	Il2CppIUnknown* ____stackTrace_6;
	Il2CppChar* ____stackTraceString_7;
	Il2CppChar* ____remoteStackTraceString_8;
	int32_t ____remoteStackIndex_9;
	Il2CppIUnknown* ____dynamicMethods_10;
	int32_t ____HResult_11;
	Il2CppChar* ____source_12;
	SafeSerializationManager_tDE44F029589A028F8A3053C5C06153FAB4AAE29F * ____safeSerializationManager_13;
	StackTraceU5BU5D_t4AD999C288CB6D1F38A299D12B1598D606588971* ___captured_traces_14;
	Il2CppSafeArray/*NONE*/* ___native_trace_ips_15;
};

// UnityEngine.Object
struct Object_tF2F3778131EFF286AF62B7B013A170F95A91571A  : public RuntimeObject
{
public:
	// System.IntPtr UnityEngine.Object::m_CachedPtr
	intptr_t ___m_CachedPtr_0;

public:
	inline static int32_t get_offset_of_m_CachedPtr_0() { return static_cast<int32_t>(offsetof(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A, ___m_CachedPtr_0)); }
	inline intptr_t get_m_CachedPtr_0() const { return ___m_CachedPtr_0; }
	inline intptr_t* get_address_of_m_CachedPtr_0() { return &___m_CachedPtr_0; }
	inline void set_m_CachedPtr_0(intptr_t value)
	{
		___m_CachedPtr_0 = value;
	}
};

struct Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_StaticFields
{
public:
	// System.Int32 UnityEngine.Object::OffsetOfInstanceIDInCPlusPlusObject
	int32_t ___OffsetOfInstanceIDInCPlusPlusObject_1;

public:
	inline static int32_t get_offset_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return static_cast<int32_t>(offsetof(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_StaticFields, ___OffsetOfInstanceIDInCPlusPlusObject_1)); }
	inline int32_t get_OffsetOfInstanceIDInCPlusPlusObject_1() const { return ___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline int32_t* get_address_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return &___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline void set_OffsetOfInstanceIDInCPlusPlusObject_1(int32_t value)
	{
		___OffsetOfInstanceIDInCPlusPlusObject_1 = value;
	}
};

// Native definition for P/Invoke marshalling of UnityEngine.Object
struct Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_marshaled_pinvoke
{
	intptr_t ___m_CachedPtr_0;
};
// Native definition for COM marshalling of UnityEngine.Object
struct Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_marshaled_com
{
	intptr_t ___m_CachedPtr_0;
};

// System.TimeSpan
struct TimeSpan_t4F6A0E13E703B65365CFCAB58E05EE0AF3EE6203 
{
public:
	// System.Int64 System.TimeSpan::_ticks
	int64_t ____ticks_22;

public:
	inline static int32_t get_offset_of__ticks_22() { return static_cast<int32_t>(offsetof(TimeSpan_t4F6A0E13E703B65365CFCAB58E05EE0AF3EE6203, ____ticks_22)); }
	inline int64_t get__ticks_22() const { return ____ticks_22; }
	inline int64_t* get_address_of__ticks_22() { return &____ticks_22; }
	inline void set__ticks_22(int64_t value)
	{
		____ticks_22 = value;
	}
};

struct TimeSpan_t4F6A0E13E703B65365CFCAB58E05EE0AF3EE6203_StaticFields
{
public:
	// System.TimeSpan System.TimeSpan::Zero
	TimeSpan_t4F6A0E13E703B65365CFCAB58E05EE0AF3EE6203  ___Zero_19;
	// System.TimeSpan System.TimeSpan::MaxValue
	TimeSpan_t4F6A0E13E703B65365CFCAB58E05EE0AF3EE6203  ___MaxValue_20;
	// System.TimeSpan System.TimeSpan::MinValue
	TimeSpan_t4F6A0E13E703B65365CFCAB58E05EE0AF3EE6203  ___MinValue_21;
	// System.Boolean modreq(System.Runtime.CompilerServices.IsVolatile) System.TimeSpan::_legacyConfigChecked
	bool ____legacyConfigChecked_23;
	// System.Boolean modreq(System.Runtime.CompilerServices.IsVolatile) System.TimeSpan::_legacyMode
	bool ____legacyMode_24;

public:
	inline static int32_t get_offset_of_Zero_19() { return static_cast<int32_t>(offsetof(TimeSpan_t4F6A0E13E703B65365CFCAB58E05EE0AF3EE6203_StaticFields, ___Zero_19)); }
	inline TimeSpan_t4F6A0E13E703B65365CFCAB58E05EE0AF3EE6203  get_Zero_19() const { return ___Zero_19; }
	inline TimeSpan_t4F6A0E13E703B65365CFCAB58E05EE0AF3EE6203 * get_address_of_Zero_19() { return &___Zero_19; }
	inline void set_Zero_19(TimeSpan_t4F6A0E13E703B65365CFCAB58E05EE0AF3EE6203  value)
	{
		___Zero_19 = value;
	}

	inline static int32_t get_offset_of_MaxValue_20() { return static_cast<int32_t>(offsetof(TimeSpan_t4F6A0E13E703B65365CFCAB58E05EE0AF3EE6203_StaticFields, ___MaxValue_20)); }
	inline TimeSpan_t4F6A0E13E703B65365CFCAB58E05EE0AF3EE6203  get_MaxValue_20() const { return ___MaxValue_20; }
	inline TimeSpan_t4F6A0E13E703B65365CFCAB58E05EE0AF3EE6203 * get_address_of_MaxValue_20() { return &___MaxValue_20; }
	inline void set_MaxValue_20(TimeSpan_t4F6A0E13E703B65365CFCAB58E05EE0AF3EE6203  value)
	{
		___MaxValue_20 = value;
	}

	inline static int32_t get_offset_of_MinValue_21() { return static_cast<int32_t>(offsetof(TimeSpan_t4F6A0E13E703B65365CFCAB58E05EE0AF3EE6203_StaticFields, ___MinValue_21)); }
	inline TimeSpan_t4F6A0E13E703B65365CFCAB58E05EE0AF3EE6203  get_MinValue_21() const { return ___MinValue_21; }
	inline TimeSpan_t4F6A0E13E703B65365CFCAB58E05EE0AF3EE6203 * get_address_of_MinValue_21() { return &___MinValue_21; }
	inline void set_MinValue_21(TimeSpan_t4F6A0E13E703B65365CFCAB58E05EE0AF3EE6203  value)
	{
		___MinValue_21 = value;
	}

	inline static int32_t get_offset_of__legacyConfigChecked_23() { return static_cast<int32_t>(offsetof(TimeSpan_t4F6A0E13E703B65365CFCAB58E05EE0AF3EE6203_StaticFields, ____legacyConfigChecked_23)); }
	inline bool get__legacyConfigChecked_23() const { return ____legacyConfigChecked_23; }
	inline bool* get_address_of__legacyConfigChecked_23() { return &____legacyConfigChecked_23; }
	inline void set__legacyConfigChecked_23(bool value)
	{
		____legacyConfigChecked_23 = value;
	}

	inline static int32_t get_offset_of__legacyMode_24() { return static_cast<int32_t>(offsetof(TimeSpan_t4F6A0E13E703B65365CFCAB58E05EE0AF3EE6203_StaticFields, ____legacyMode_24)); }
	inline bool get__legacyMode_24() const { return ____legacyMode_24; }
	inline bool* get_address_of__legacyMode_24() { return &____legacyMode_24; }
	inline void set__legacyMode_24(bool value)
	{
		____legacyMode_24 = value;
	}
};


// BestHTTP.SignalRCore.TransportEvents
struct TransportEvents_tE38D2B53053334BE1B4419061B7701FF8E099D19 
{
public:
	// System.Int32 BestHTTP.SignalRCore.TransportEvents::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(TransportEvents_tE38D2B53053334BE1B4419061B7701FF8E099D19, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// BestHTTP.SignalRCore.TransportTypes
struct TransportTypes_t8387D0BDDE41AD7796AEFFC585944D24F9389FB0 
{
public:
	// System.Int32 BestHTTP.SignalRCore.TransportTypes::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(TransportTypes_t8387D0BDDE41AD7796AEFFC585944D24F9389FB0, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// System.UriFormat
struct UriFormat_t25C936463BDE737B16A8EC3DA05091FC31F1A71F 
{
public:
	// System.Int32 System.UriFormat::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(UriFormat_t25C936463BDE737B16A8EC3DA05091FC31F1A71F, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// System.UriIdnScope
struct UriIdnScope_tBA22B992BA582F68F2B98CDEBCB24299F249DE4D 
{
public:
	// System.Int32 System.UriIdnScope::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(UriIdnScope_tBA22B992BA582F68F2B98CDEBCB24299F249DE4D, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// System.UriKind
struct UriKind_tFC16ACC1842283AAE2C7F50C9C70EFBF6550B3FC 
{
public:
	// System.Int32 System.UriKind::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(UriKind_tFC16ACC1842283AAE2C7F50C9C70EFBF6550B3FC, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// UnityEngine.UI.Navigation/Mode
struct Mode_t3113FDF05158BBA1DFC78D7F69E4C1D25135CB0F 
{
public:
	// System.Int32 UnityEngine.UI.Navigation/Mode::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(Mode_t3113FDF05158BBA1DFC78D7F69E4C1D25135CB0F, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// UnityEngine.UI.ScrollRect/MovementType
struct MovementType_tAC9293D74600C5C0F8769961576D21C7107BB258 
{
public:
	// System.Int32 UnityEngine.UI.ScrollRect/MovementType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(MovementType_tAC9293D74600C5C0F8769961576D21C7107BB258, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// UnityEngine.UI.ScrollRect/ScrollbarVisibility
struct ScrollbarVisibility_t8223EB8BD4F3CB01D1A246265D1563AAB5F89F2E 
{
public:
	// System.Int32 UnityEngine.UI.ScrollRect/ScrollbarVisibility::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(ScrollbarVisibility_t8223EB8BD4F3CB01D1A246265D1563AAB5F89F2E, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// UnityEngine.UI.Selectable/Transition
struct Transition_t1FC449676815A798E758D32E8BE6DC0A2511DF14 
{
public:
	// System.Int32 UnityEngine.UI.Selectable/Transition::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(Transition_t1FC449676815A798E758D32E8BE6DC0A2511DF14, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// System.Uri/Flags
struct Flags_t72C622DF5C3ED762F55AB36EC2CCDDF3AF56B8D4 
{
public:
	// System.UInt64 System.Uri/Flags::value__
	uint64_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(Flags_t72C622DF5C3ED762F55AB36EC2CCDDF3AF56B8D4, ___value___2)); }
	inline uint64_t get_value___2() const { return ___value___2; }
	inline uint64_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(uint64_t value)
	{
		___value___2 = value;
	}
};


// UnityEngine.Component
struct Component_t62FBC8D2420DA4BE9037AFE430740F6B3EECA684  : public Object_tF2F3778131EFF286AF62B7B013A170F95A91571A
{
public:

public:
};


// BestHTTP.SignalRCore.HubOptions
struct HubOptions_t53B108AE0F01D633E571D739D96B573C5C68A6F7  : public RuntimeObject
{
public:
	// System.Boolean BestHTTP.SignalRCore.HubOptions::<SkipNegotiation>k__BackingField
	bool ___U3CSkipNegotiationU3Ek__BackingField_0;
	// BestHTTP.SignalRCore.TransportTypes BestHTTP.SignalRCore.HubOptions::<PreferedTransport>k__BackingField
	int32_t ___U3CPreferedTransportU3Ek__BackingField_1;
	// System.TimeSpan BestHTTP.SignalRCore.HubOptions::<PingInterval>k__BackingField
	TimeSpan_t4F6A0E13E703B65365CFCAB58E05EE0AF3EE6203  ___U3CPingIntervalU3Ek__BackingField_2;
	// System.TimeSpan BestHTTP.SignalRCore.HubOptions::<PingTimeoutInterval>k__BackingField
	TimeSpan_t4F6A0E13E703B65365CFCAB58E05EE0AF3EE6203  ___U3CPingTimeoutIntervalU3Ek__BackingField_3;
	// System.Int32 BestHTTP.SignalRCore.HubOptions::<MaxRedirects>k__BackingField
	int32_t ___U3CMaxRedirectsU3Ek__BackingField_4;
	// System.TimeSpan BestHTTP.SignalRCore.HubOptions::<ConnectTimeout>k__BackingField
	TimeSpan_t4F6A0E13E703B65365CFCAB58E05EE0AF3EE6203  ___U3CConnectTimeoutU3Ek__BackingField_5;

public:
	inline static int32_t get_offset_of_U3CSkipNegotiationU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(HubOptions_t53B108AE0F01D633E571D739D96B573C5C68A6F7, ___U3CSkipNegotiationU3Ek__BackingField_0)); }
	inline bool get_U3CSkipNegotiationU3Ek__BackingField_0() const { return ___U3CSkipNegotiationU3Ek__BackingField_0; }
	inline bool* get_address_of_U3CSkipNegotiationU3Ek__BackingField_0() { return &___U3CSkipNegotiationU3Ek__BackingField_0; }
	inline void set_U3CSkipNegotiationU3Ek__BackingField_0(bool value)
	{
		___U3CSkipNegotiationU3Ek__BackingField_0 = value;
	}

	inline static int32_t get_offset_of_U3CPreferedTransportU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(HubOptions_t53B108AE0F01D633E571D739D96B573C5C68A6F7, ___U3CPreferedTransportU3Ek__BackingField_1)); }
	inline int32_t get_U3CPreferedTransportU3Ek__BackingField_1() const { return ___U3CPreferedTransportU3Ek__BackingField_1; }
	inline int32_t* get_address_of_U3CPreferedTransportU3Ek__BackingField_1() { return &___U3CPreferedTransportU3Ek__BackingField_1; }
	inline void set_U3CPreferedTransportU3Ek__BackingField_1(int32_t value)
	{
		___U3CPreferedTransportU3Ek__BackingField_1 = value;
	}

	inline static int32_t get_offset_of_U3CPingIntervalU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(HubOptions_t53B108AE0F01D633E571D739D96B573C5C68A6F7, ___U3CPingIntervalU3Ek__BackingField_2)); }
	inline TimeSpan_t4F6A0E13E703B65365CFCAB58E05EE0AF3EE6203  get_U3CPingIntervalU3Ek__BackingField_2() const { return ___U3CPingIntervalU3Ek__BackingField_2; }
	inline TimeSpan_t4F6A0E13E703B65365CFCAB58E05EE0AF3EE6203 * get_address_of_U3CPingIntervalU3Ek__BackingField_2() { return &___U3CPingIntervalU3Ek__BackingField_2; }
	inline void set_U3CPingIntervalU3Ek__BackingField_2(TimeSpan_t4F6A0E13E703B65365CFCAB58E05EE0AF3EE6203  value)
	{
		___U3CPingIntervalU3Ek__BackingField_2 = value;
	}

	inline static int32_t get_offset_of_U3CPingTimeoutIntervalU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(HubOptions_t53B108AE0F01D633E571D739D96B573C5C68A6F7, ___U3CPingTimeoutIntervalU3Ek__BackingField_3)); }
	inline TimeSpan_t4F6A0E13E703B65365CFCAB58E05EE0AF3EE6203  get_U3CPingTimeoutIntervalU3Ek__BackingField_3() const { return ___U3CPingTimeoutIntervalU3Ek__BackingField_3; }
	inline TimeSpan_t4F6A0E13E703B65365CFCAB58E05EE0AF3EE6203 * get_address_of_U3CPingTimeoutIntervalU3Ek__BackingField_3() { return &___U3CPingTimeoutIntervalU3Ek__BackingField_3; }
	inline void set_U3CPingTimeoutIntervalU3Ek__BackingField_3(TimeSpan_t4F6A0E13E703B65365CFCAB58E05EE0AF3EE6203  value)
	{
		___U3CPingTimeoutIntervalU3Ek__BackingField_3 = value;
	}

	inline static int32_t get_offset_of_U3CMaxRedirectsU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(HubOptions_t53B108AE0F01D633E571D739D96B573C5C68A6F7, ___U3CMaxRedirectsU3Ek__BackingField_4)); }
	inline int32_t get_U3CMaxRedirectsU3Ek__BackingField_4() const { return ___U3CMaxRedirectsU3Ek__BackingField_4; }
	inline int32_t* get_address_of_U3CMaxRedirectsU3Ek__BackingField_4() { return &___U3CMaxRedirectsU3Ek__BackingField_4; }
	inline void set_U3CMaxRedirectsU3Ek__BackingField_4(int32_t value)
	{
		___U3CMaxRedirectsU3Ek__BackingField_4 = value;
	}

	inline static int32_t get_offset_of_U3CConnectTimeoutU3Ek__BackingField_5() { return static_cast<int32_t>(offsetof(HubOptions_t53B108AE0F01D633E571D739D96B573C5C68A6F7, ___U3CConnectTimeoutU3Ek__BackingField_5)); }
	inline TimeSpan_t4F6A0E13E703B65365CFCAB58E05EE0AF3EE6203  get_U3CConnectTimeoutU3Ek__BackingField_5() const { return ___U3CConnectTimeoutU3Ek__BackingField_5; }
	inline TimeSpan_t4F6A0E13E703B65365CFCAB58E05EE0AF3EE6203 * get_address_of_U3CConnectTimeoutU3Ek__BackingField_5() { return &___U3CConnectTimeoutU3Ek__BackingField_5; }
	inline void set_U3CConnectTimeoutU3Ek__BackingField_5(TimeSpan_t4F6A0E13E703B65365CFCAB58E05EE0AF3EE6203  value)
	{
		___U3CConnectTimeoutU3Ek__BackingField_5 = value;
	}
};


// System.MulticastDelegate
struct MulticastDelegate_t  : public Delegate_t
{
public:
	// System.Delegate[] System.MulticastDelegate::delegates
	DelegateU5BU5D_t677D8FE08A5F99E8EE49150B73966CD6E9BF7DB8* ___delegates_11;

public:
	inline static int32_t get_offset_of_delegates_11() { return static_cast<int32_t>(offsetof(MulticastDelegate_t, ___delegates_11)); }
	inline DelegateU5BU5D_t677D8FE08A5F99E8EE49150B73966CD6E9BF7DB8* get_delegates_11() const { return ___delegates_11; }
	inline DelegateU5BU5D_t677D8FE08A5F99E8EE49150B73966CD6E9BF7DB8** get_address_of_delegates_11() { return &___delegates_11; }
	inline void set_delegates_11(DelegateU5BU5D_t677D8FE08A5F99E8EE49150B73966CD6E9BF7DB8* value)
	{
		___delegates_11 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___delegates_11), (void*)value);
	}
};

// Native definition for P/Invoke marshalling of System.MulticastDelegate
struct MulticastDelegate_t_marshaled_pinvoke : public Delegate_t_marshaled_pinvoke
{
	Delegate_t_marshaled_pinvoke** ___delegates_11;
};
// Native definition for COM marshalling of System.MulticastDelegate
struct MulticastDelegate_t_marshaled_com : public Delegate_t_marshaled_com
{
	Delegate_t_marshaled_com** ___delegates_11;
};

// UnityEngine.UI.Navigation
struct Navigation_t1CF0FFB22C0357CD64714FB7A40A275F899D363A 
{
public:
	// UnityEngine.UI.Navigation/Mode UnityEngine.UI.Navigation::m_Mode
	int32_t ___m_Mode_0;
	// System.Boolean UnityEngine.UI.Navigation::m_WrapAround
	bool ___m_WrapAround_1;
	// UnityEngine.UI.Selectable UnityEngine.UI.Navigation::m_SelectOnUp
	Selectable_t34088A3677CC9D344F81B0D91999D8C5963D7DBD * ___m_SelectOnUp_2;
	// UnityEngine.UI.Selectable UnityEngine.UI.Navigation::m_SelectOnDown
	Selectable_t34088A3677CC9D344F81B0D91999D8C5963D7DBD * ___m_SelectOnDown_3;
	// UnityEngine.UI.Selectable UnityEngine.UI.Navigation::m_SelectOnLeft
	Selectable_t34088A3677CC9D344F81B0D91999D8C5963D7DBD * ___m_SelectOnLeft_4;
	// UnityEngine.UI.Selectable UnityEngine.UI.Navigation::m_SelectOnRight
	Selectable_t34088A3677CC9D344F81B0D91999D8C5963D7DBD * ___m_SelectOnRight_5;

public:
	inline static int32_t get_offset_of_m_Mode_0() { return static_cast<int32_t>(offsetof(Navigation_t1CF0FFB22C0357CD64714FB7A40A275F899D363A, ___m_Mode_0)); }
	inline int32_t get_m_Mode_0() const { return ___m_Mode_0; }
	inline int32_t* get_address_of_m_Mode_0() { return &___m_Mode_0; }
	inline void set_m_Mode_0(int32_t value)
	{
		___m_Mode_0 = value;
	}

	inline static int32_t get_offset_of_m_WrapAround_1() { return static_cast<int32_t>(offsetof(Navigation_t1CF0FFB22C0357CD64714FB7A40A275F899D363A, ___m_WrapAround_1)); }
	inline bool get_m_WrapAround_1() const { return ___m_WrapAround_1; }
	inline bool* get_address_of_m_WrapAround_1() { return &___m_WrapAround_1; }
	inline void set_m_WrapAround_1(bool value)
	{
		___m_WrapAround_1 = value;
	}

	inline static int32_t get_offset_of_m_SelectOnUp_2() { return static_cast<int32_t>(offsetof(Navigation_t1CF0FFB22C0357CD64714FB7A40A275F899D363A, ___m_SelectOnUp_2)); }
	inline Selectable_t34088A3677CC9D344F81B0D91999D8C5963D7DBD * get_m_SelectOnUp_2() const { return ___m_SelectOnUp_2; }
	inline Selectable_t34088A3677CC9D344F81B0D91999D8C5963D7DBD ** get_address_of_m_SelectOnUp_2() { return &___m_SelectOnUp_2; }
	inline void set_m_SelectOnUp_2(Selectable_t34088A3677CC9D344F81B0D91999D8C5963D7DBD * value)
	{
		___m_SelectOnUp_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_SelectOnUp_2), (void*)value);
	}

	inline static int32_t get_offset_of_m_SelectOnDown_3() { return static_cast<int32_t>(offsetof(Navigation_t1CF0FFB22C0357CD64714FB7A40A275F899D363A, ___m_SelectOnDown_3)); }
	inline Selectable_t34088A3677CC9D344F81B0D91999D8C5963D7DBD * get_m_SelectOnDown_3() const { return ___m_SelectOnDown_3; }
	inline Selectable_t34088A3677CC9D344F81B0D91999D8C5963D7DBD ** get_address_of_m_SelectOnDown_3() { return &___m_SelectOnDown_3; }
	inline void set_m_SelectOnDown_3(Selectable_t34088A3677CC9D344F81B0D91999D8C5963D7DBD * value)
	{
		___m_SelectOnDown_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_SelectOnDown_3), (void*)value);
	}

	inline static int32_t get_offset_of_m_SelectOnLeft_4() { return static_cast<int32_t>(offsetof(Navigation_t1CF0FFB22C0357CD64714FB7A40A275F899D363A, ___m_SelectOnLeft_4)); }
	inline Selectable_t34088A3677CC9D344F81B0D91999D8C5963D7DBD * get_m_SelectOnLeft_4() const { return ___m_SelectOnLeft_4; }
	inline Selectable_t34088A3677CC9D344F81B0D91999D8C5963D7DBD ** get_address_of_m_SelectOnLeft_4() { return &___m_SelectOnLeft_4; }
	inline void set_m_SelectOnLeft_4(Selectable_t34088A3677CC9D344F81B0D91999D8C5963D7DBD * value)
	{
		___m_SelectOnLeft_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_SelectOnLeft_4), (void*)value);
	}

	inline static int32_t get_offset_of_m_SelectOnRight_5() { return static_cast<int32_t>(offsetof(Navigation_t1CF0FFB22C0357CD64714FB7A40A275F899D363A, ___m_SelectOnRight_5)); }
	inline Selectable_t34088A3677CC9D344F81B0D91999D8C5963D7DBD * get_m_SelectOnRight_5() const { return ___m_SelectOnRight_5; }
	inline Selectable_t34088A3677CC9D344F81B0D91999D8C5963D7DBD ** get_address_of_m_SelectOnRight_5() { return &___m_SelectOnRight_5; }
	inline void set_m_SelectOnRight_5(Selectable_t34088A3677CC9D344F81B0D91999D8C5963D7DBD * value)
	{
		___m_SelectOnRight_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_SelectOnRight_5), (void*)value);
	}
};

// Native definition for P/Invoke marshalling of UnityEngine.UI.Navigation
struct Navigation_t1CF0FFB22C0357CD64714FB7A40A275F899D363A_marshaled_pinvoke
{
	int32_t ___m_Mode_0;
	int32_t ___m_WrapAround_1;
	Selectable_t34088A3677CC9D344F81B0D91999D8C5963D7DBD * ___m_SelectOnUp_2;
	Selectable_t34088A3677CC9D344F81B0D91999D8C5963D7DBD * ___m_SelectOnDown_3;
	Selectable_t34088A3677CC9D344F81B0D91999D8C5963D7DBD * ___m_SelectOnLeft_4;
	Selectable_t34088A3677CC9D344F81B0D91999D8C5963D7DBD * ___m_SelectOnRight_5;
};
// Native definition for COM marshalling of UnityEngine.UI.Navigation
struct Navigation_t1CF0FFB22C0357CD64714FB7A40A275F899D363A_marshaled_com
{
	int32_t ___m_Mode_0;
	int32_t ___m_WrapAround_1;
	Selectable_t34088A3677CC9D344F81B0D91999D8C5963D7DBD * ___m_SelectOnUp_2;
	Selectable_t34088A3677CC9D344F81B0D91999D8C5963D7DBD * ___m_SelectOnDown_3;
	Selectable_t34088A3677CC9D344F81B0D91999D8C5963D7DBD * ___m_SelectOnLeft_4;
	Selectable_t34088A3677CC9D344F81B0D91999D8C5963D7DBD * ___m_SelectOnRight_5;
};

// BestHTTP.SignalRCore.RetryContext
struct RetryContext_t9CF398FFC95B89BFF0C1FFEE078A1C08F880A8F0 
{
public:
	// System.UInt32 BestHTTP.SignalRCore.RetryContext::PreviousRetryCount
	uint32_t ___PreviousRetryCount_0;
	// System.TimeSpan BestHTTP.SignalRCore.RetryContext::ElapsedTime
	TimeSpan_t4F6A0E13E703B65365CFCAB58E05EE0AF3EE6203  ___ElapsedTime_1;
	// System.String BestHTTP.SignalRCore.RetryContext::RetryReason
	String_t* ___RetryReason_2;

public:
	inline static int32_t get_offset_of_PreviousRetryCount_0() { return static_cast<int32_t>(offsetof(RetryContext_t9CF398FFC95B89BFF0C1FFEE078A1C08F880A8F0, ___PreviousRetryCount_0)); }
	inline uint32_t get_PreviousRetryCount_0() const { return ___PreviousRetryCount_0; }
	inline uint32_t* get_address_of_PreviousRetryCount_0() { return &___PreviousRetryCount_0; }
	inline void set_PreviousRetryCount_0(uint32_t value)
	{
		___PreviousRetryCount_0 = value;
	}

	inline static int32_t get_offset_of_ElapsedTime_1() { return static_cast<int32_t>(offsetof(RetryContext_t9CF398FFC95B89BFF0C1FFEE078A1C08F880A8F0, ___ElapsedTime_1)); }
	inline TimeSpan_t4F6A0E13E703B65365CFCAB58E05EE0AF3EE6203  get_ElapsedTime_1() const { return ___ElapsedTime_1; }
	inline TimeSpan_t4F6A0E13E703B65365CFCAB58E05EE0AF3EE6203 * get_address_of_ElapsedTime_1() { return &___ElapsedTime_1; }
	inline void set_ElapsedTime_1(TimeSpan_t4F6A0E13E703B65365CFCAB58E05EE0AF3EE6203  value)
	{
		___ElapsedTime_1 = value;
	}

	inline static int32_t get_offset_of_RetryReason_2() { return static_cast<int32_t>(offsetof(RetryContext_t9CF398FFC95B89BFF0C1FFEE078A1C08F880A8F0, ___RetryReason_2)); }
	inline String_t* get_RetryReason_2() const { return ___RetryReason_2; }
	inline String_t** get_address_of_RetryReason_2() { return &___RetryReason_2; }
	inline void set_RetryReason_2(String_t* value)
	{
		___RetryReason_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___RetryReason_2), (void*)value);
	}
};

// Native definition for P/Invoke marshalling of BestHTTP.SignalRCore.RetryContext
struct RetryContext_t9CF398FFC95B89BFF0C1FFEE078A1C08F880A8F0_marshaled_pinvoke
{
	uint32_t ___PreviousRetryCount_0;
	TimeSpan_t4F6A0E13E703B65365CFCAB58E05EE0AF3EE6203  ___ElapsedTime_1;
	char* ___RetryReason_2;
};
// Native definition for COM marshalling of BestHTTP.SignalRCore.RetryContext
struct RetryContext_t9CF398FFC95B89BFF0C1FFEE078A1C08F880A8F0_marshaled_com
{
	uint32_t ___PreviousRetryCount_0;
	TimeSpan_t4F6A0E13E703B65365CFCAB58E05EE0AF3EE6203  ___ElapsedTime_1;
	Il2CppChar* ___RetryReason_2;
};

// System.Uri
struct Uri_t4A915E1CC15B2C650F478099AD448E9466CBF612  : public RuntimeObject
{
public:
	// System.String System.Uri::m_String
	String_t* ___m_String_16;
	// System.String System.Uri::m_originalUnicodeString
	String_t* ___m_originalUnicodeString_17;
	// System.UriParser System.Uri::m_Syntax
	UriParser_t6DEBE5C6CDC3C29C9019CD951C7ECEBD6A5D3E3A * ___m_Syntax_18;
	// System.String System.Uri::m_DnsSafeHost
	String_t* ___m_DnsSafeHost_19;
	// System.Uri/Flags System.Uri::m_Flags
	uint64_t ___m_Flags_20;
	// System.Uri/UriInfo System.Uri::m_Info
	UriInfo_tCB2302A896132D1F70E47C3895FAB9A0F2A6EE45 * ___m_Info_21;
	// System.Boolean System.Uri::m_iriParsing
	bool ___m_iriParsing_22;

public:
	inline static int32_t get_offset_of_m_String_16() { return static_cast<int32_t>(offsetof(Uri_t4A915E1CC15B2C650F478099AD448E9466CBF612, ___m_String_16)); }
	inline String_t* get_m_String_16() const { return ___m_String_16; }
	inline String_t** get_address_of_m_String_16() { return &___m_String_16; }
	inline void set_m_String_16(String_t* value)
	{
		___m_String_16 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_String_16), (void*)value);
	}

	inline static int32_t get_offset_of_m_originalUnicodeString_17() { return static_cast<int32_t>(offsetof(Uri_t4A915E1CC15B2C650F478099AD448E9466CBF612, ___m_originalUnicodeString_17)); }
	inline String_t* get_m_originalUnicodeString_17() const { return ___m_originalUnicodeString_17; }
	inline String_t** get_address_of_m_originalUnicodeString_17() { return &___m_originalUnicodeString_17; }
	inline void set_m_originalUnicodeString_17(String_t* value)
	{
		___m_originalUnicodeString_17 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_originalUnicodeString_17), (void*)value);
	}

	inline static int32_t get_offset_of_m_Syntax_18() { return static_cast<int32_t>(offsetof(Uri_t4A915E1CC15B2C650F478099AD448E9466CBF612, ___m_Syntax_18)); }
	inline UriParser_t6DEBE5C6CDC3C29C9019CD951C7ECEBD6A5D3E3A * get_m_Syntax_18() const { return ___m_Syntax_18; }
	inline UriParser_t6DEBE5C6CDC3C29C9019CD951C7ECEBD6A5D3E3A ** get_address_of_m_Syntax_18() { return &___m_Syntax_18; }
	inline void set_m_Syntax_18(UriParser_t6DEBE5C6CDC3C29C9019CD951C7ECEBD6A5D3E3A * value)
	{
		___m_Syntax_18 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Syntax_18), (void*)value);
	}

	inline static int32_t get_offset_of_m_DnsSafeHost_19() { return static_cast<int32_t>(offsetof(Uri_t4A915E1CC15B2C650F478099AD448E9466CBF612, ___m_DnsSafeHost_19)); }
	inline String_t* get_m_DnsSafeHost_19() const { return ___m_DnsSafeHost_19; }
	inline String_t** get_address_of_m_DnsSafeHost_19() { return &___m_DnsSafeHost_19; }
	inline void set_m_DnsSafeHost_19(String_t* value)
	{
		___m_DnsSafeHost_19 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_DnsSafeHost_19), (void*)value);
	}

	inline static int32_t get_offset_of_m_Flags_20() { return static_cast<int32_t>(offsetof(Uri_t4A915E1CC15B2C650F478099AD448E9466CBF612, ___m_Flags_20)); }
	inline uint64_t get_m_Flags_20() const { return ___m_Flags_20; }
	inline uint64_t* get_address_of_m_Flags_20() { return &___m_Flags_20; }
	inline void set_m_Flags_20(uint64_t value)
	{
		___m_Flags_20 = value;
	}

	inline static int32_t get_offset_of_m_Info_21() { return static_cast<int32_t>(offsetof(Uri_t4A915E1CC15B2C650F478099AD448E9466CBF612, ___m_Info_21)); }
	inline UriInfo_tCB2302A896132D1F70E47C3895FAB9A0F2A6EE45 * get_m_Info_21() const { return ___m_Info_21; }
	inline UriInfo_tCB2302A896132D1F70E47C3895FAB9A0F2A6EE45 ** get_address_of_m_Info_21() { return &___m_Info_21; }
	inline void set_m_Info_21(UriInfo_tCB2302A896132D1F70E47C3895FAB9A0F2A6EE45 * value)
	{
		___m_Info_21 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Info_21), (void*)value);
	}

	inline static int32_t get_offset_of_m_iriParsing_22() { return static_cast<int32_t>(offsetof(Uri_t4A915E1CC15B2C650F478099AD448E9466CBF612, ___m_iriParsing_22)); }
	inline bool get_m_iriParsing_22() const { return ___m_iriParsing_22; }
	inline bool* get_address_of_m_iriParsing_22() { return &___m_iriParsing_22; }
	inline void set_m_iriParsing_22(bool value)
	{
		___m_iriParsing_22 = value;
	}
};

struct Uri_t4A915E1CC15B2C650F478099AD448E9466CBF612_StaticFields
{
public:
	// System.String System.Uri::UriSchemeFile
	String_t* ___UriSchemeFile_0;
	// System.String System.Uri::UriSchemeFtp
	String_t* ___UriSchemeFtp_1;
	// System.String System.Uri::UriSchemeGopher
	String_t* ___UriSchemeGopher_2;
	// System.String System.Uri::UriSchemeHttp
	String_t* ___UriSchemeHttp_3;
	// System.String System.Uri::UriSchemeHttps
	String_t* ___UriSchemeHttps_4;
	// System.String System.Uri::UriSchemeWs
	String_t* ___UriSchemeWs_5;
	// System.String System.Uri::UriSchemeWss
	String_t* ___UriSchemeWss_6;
	// System.String System.Uri::UriSchemeMailto
	String_t* ___UriSchemeMailto_7;
	// System.String System.Uri::UriSchemeNews
	String_t* ___UriSchemeNews_8;
	// System.String System.Uri::UriSchemeNntp
	String_t* ___UriSchemeNntp_9;
	// System.String System.Uri::UriSchemeNetTcp
	String_t* ___UriSchemeNetTcp_10;
	// System.String System.Uri::UriSchemeNetPipe
	String_t* ___UriSchemeNetPipe_11;
	// System.String System.Uri::SchemeDelimiter
	String_t* ___SchemeDelimiter_12;
	// System.Boolean modreq(System.Runtime.CompilerServices.IsVolatile) System.Uri::s_ConfigInitialized
	bool ___s_ConfigInitialized_23;
	// System.Boolean modreq(System.Runtime.CompilerServices.IsVolatile) System.Uri::s_ConfigInitializing
	bool ___s_ConfigInitializing_24;
	// System.UriIdnScope modreq(System.Runtime.CompilerServices.IsVolatile) System.Uri::s_IdnScope
	int32_t ___s_IdnScope_25;
	// System.Boolean modreq(System.Runtime.CompilerServices.IsVolatile) System.Uri::s_IriParsing
	bool ___s_IriParsing_26;
	// System.Boolean System.Uri::useDotNetRelativeOrAbsolute
	bool ___useDotNetRelativeOrAbsolute_27;
	// System.Boolean System.Uri::IsWindowsFileSystem
	bool ___IsWindowsFileSystem_29;
	// System.Object System.Uri::s_initLock
	RuntimeObject * ___s_initLock_30;
	// System.Char[] System.Uri::HexLowerChars
	CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34* ___HexLowerChars_34;
	// System.Char[] System.Uri::_WSchars
	CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34* ____WSchars_35;

public:
	inline static int32_t get_offset_of_UriSchemeFile_0() { return static_cast<int32_t>(offsetof(Uri_t4A915E1CC15B2C650F478099AD448E9466CBF612_StaticFields, ___UriSchemeFile_0)); }
	inline String_t* get_UriSchemeFile_0() const { return ___UriSchemeFile_0; }
	inline String_t** get_address_of_UriSchemeFile_0() { return &___UriSchemeFile_0; }
	inline void set_UriSchemeFile_0(String_t* value)
	{
		___UriSchemeFile_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___UriSchemeFile_0), (void*)value);
	}

	inline static int32_t get_offset_of_UriSchemeFtp_1() { return static_cast<int32_t>(offsetof(Uri_t4A915E1CC15B2C650F478099AD448E9466CBF612_StaticFields, ___UriSchemeFtp_1)); }
	inline String_t* get_UriSchemeFtp_1() const { return ___UriSchemeFtp_1; }
	inline String_t** get_address_of_UriSchemeFtp_1() { return &___UriSchemeFtp_1; }
	inline void set_UriSchemeFtp_1(String_t* value)
	{
		___UriSchemeFtp_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___UriSchemeFtp_1), (void*)value);
	}

	inline static int32_t get_offset_of_UriSchemeGopher_2() { return static_cast<int32_t>(offsetof(Uri_t4A915E1CC15B2C650F478099AD448E9466CBF612_StaticFields, ___UriSchemeGopher_2)); }
	inline String_t* get_UriSchemeGopher_2() const { return ___UriSchemeGopher_2; }
	inline String_t** get_address_of_UriSchemeGopher_2() { return &___UriSchemeGopher_2; }
	inline void set_UriSchemeGopher_2(String_t* value)
	{
		___UriSchemeGopher_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___UriSchemeGopher_2), (void*)value);
	}

	inline static int32_t get_offset_of_UriSchemeHttp_3() { return static_cast<int32_t>(offsetof(Uri_t4A915E1CC15B2C650F478099AD448E9466CBF612_StaticFields, ___UriSchemeHttp_3)); }
	inline String_t* get_UriSchemeHttp_3() const { return ___UriSchemeHttp_3; }
	inline String_t** get_address_of_UriSchemeHttp_3() { return &___UriSchemeHttp_3; }
	inline void set_UriSchemeHttp_3(String_t* value)
	{
		___UriSchemeHttp_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___UriSchemeHttp_3), (void*)value);
	}

	inline static int32_t get_offset_of_UriSchemeHttps_4() { return static_cast<int32_t>(offsetof(Uri_t4A915E1CC15B2C650F478099AD448E9466CBF612_StaticFields, ___UriSchemeHttps_4)); }
	inline String_t* get_UriSchemeHttps_4() const { return ___UriSchemeHttps_4; }
	inline String_t** get_address_of_UriSchemeHttps_4() { return &___UriSchemeHttps_4; }
	inline void set_UriSchemeHttps_4(String_t* value)
	{
		___UriSchemeHttps_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___UriSchemeHttps_4), (void*)value);
	}

	inline static int32_t get_offset_of_UriSchemeWs_5() { return static_cast<int32_t>(offsetof(Uri_t4A915E1CC15B2C650F478099AD448E9466CBF612_StaticFields, ___UriSchemeWs_5)); }
	inline String_t* get_UriSchemeWs_5() const { return ___UriSchemeWs_5; }
	inline String_t** get_address_of_UriSchemeWs_5() { return &___UriSchemeWs_5; }
	inline void set_UriSchemeWs_5(String_t* value)
	{
		___UriSchemeWs_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___UriSchemeWs_5), (void*)value);
	}

	inline static int32_t get_offset_of_UriSchemeWss_6() { return static_cast<int32_t>(offsetof(Uri_t4A915E1CC15B2C650F478099AD448E9466CBF612_StaticFields, ___UriSchemeWss_6)); }
	inline String_t* get_UriSchemeWss_6() const { return ___UriSchemeWss_6; }
	inline String_t** get_address_of_UriSchemeWss_6() { return &___UriSchemeWss_6; }
	inline void set_UriSchemeWss_6(String_t* value)
	{
		___UriSchemeWss_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___UriSchemeWss_6), (void*)value);
	}

	inline static int32_t get_offset_of_UriSchemeMailto_7() { return static_cast<int32_t>(offsetof(Uri_t4A915E1CC15B2C650F478099AD448E9466CBF612_StaticFields, ___UriSchemeMailto_7)); }
	inline String_t* get_UriSchemeMailto_7() const { return ___UriSchemeMailto_7; }
	inline String_t** get_address_of_UriSchemeMailto_7() { return &___UriSchemeMailto_7; }
	inline void set_UriSchemeMailto_7(String_t* value)
	{
		___UriSchemeMailto_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___UriSchemeMailto_7), (void*)value);
	}

	inline static int32_t get_offset_of_UriSchemeNews_8() { return static_cast<int32_t>(offsetof(Uri_t4A915E1CC15B2C650F478099AD448E9466CBF612_StaticFields, ___UriSchemeNews_8)); }
	inline String_t* get_UriSchemeNews_8() const { return ___UriSchemeNews_8; }
	inline String_t** get_address_of_UriSchemeNews_8() { return &___UriSchemeNews_8; }
	inline void set_UriSchemeNews_8(String_t* value)
	{
		___UriSchemeNews_8 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___UriSchemeNews_8), (void*)value);
	}

	inline static int32_t get_offset_of_UriSchemeNntp_9() { return static_cast<int32_t>(offsetof(Uri_t4A915E1CC15B2C650F478099AD448E9466CBF612_StaticFields, ___UriSchemeNntp_9)); }
	inline String_t* get_UriSchemeNntp_9() const { return ___UriSchemeNntp_9; }
	inline String_t** get_address_of_UriSchemeNntp_9() { return &___UriSchemeNntp_9; }
	inline void set_UriSchemeNntp_9(String_t* value)
	{
		___UriSchemeNntp_9 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___UriSchemeNntp_9), (void*)value);
	}

	inline static int32_t get_offset_of_UriSchemeNetTcp_10() { return static_cast<int32_t>(offsetof(Uri_t4A915E1CC15B2C650F478099AD448E9466CBF612_StaticFields, ___UriSchemeNetTcp_10)); }
	inline String_t* get_UriSchemeNetTcp_10() const { return ___UriSchemeNetTcp_10; }
	inline String_t** get_address_of_UriSchemeNetTcp_10() { return &___UriSchemeNetTcp_10; }
	inline void set_UriSchemeNetTcp_10(String_t* value)
	{
		___UriSchemeNetTcp_10 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___UriSchemeNetTcp_10), (void*)value);
	}

	inline static int32_t get_offset_of_UriSchemeNetPipe_11() { return static_cast<int32_t>(offsetof(Uri_t4A915E1CC15B2C650F478099AD448E9466CBF612_StaticFields, ___UriSchemeNetPipe_11)); }
	inline String_t* get_UriSchemeNetPipe_11() const { return ___UriSchemeNetPipe_11; }
	inline String_t** get_address_of_UriSchemeNetPipe_11() { return &___UriSchemeNetPipe_11; }
	inline void set_UriSchemeNetPipe_11(String_t* value)
	{
		___UriSchemeNetPipe_11 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___UriSchemeNetPipe_11), (void*)value);
	}

	inline static int32_t get_offset_of_SchemeDelimiter_12() { return static_cast<int32_t>(offsetof(Uri_t4A915E1CC15B2C650F478099AD448E9466CBF612_StaticFields, ___SchemeDelimiter_12)); }
	inline String_t* get_SchemeDelimiter_12() const { return ___SchemeDelimiter_12; }
	inline String_t** get_address_of_SchemeDelimiter_12() { return &___SchemeDelimiter_12; }
	inline void set_SchemeDelimiter_12(String_t* value)
	{
		___SchemeDelimiter_12 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___SchemeDelimiter_12), (void*)value);
	}

	inline static int32_t get_offset_of_s_ConfigInitialized_23() { return static_cast<int32_t>(offsetof(Uri_t4A915E1CC15B2C650F478099AD448E9466CBF612_StaticFields, ___s_ConfigInitialized_23)); }
	inline bool get_s_ConfigInitialized_23() const { return ___s_ConfigInitialized_23; }
	inline bool* get_address_of_s_ConfigInitialized_23() { return &___s_ConfigInitialized_23; }
	inline void set_s_ConfigInitialized_23(bool value)
	{
		___s_ConfigInitialized_23 = value;
	}

	inline static int32_t get_offset_of_s_ConfigInitializing_24() { return static_cast<int32_t>(offsetof(Uri_t4A915E1CC15B2C650F478099AD448E9466CBF612_StaticFields, ___s_ConfigInitializing_24)); }
	inline bool get_s_ConfigInitializing_24() const { return ___s_ConfigInitializing_24; }
	inline bool* get_address_of_s_ConfigInitializing_24() { return &___s_ConfigInitializing_24; }
	inline void set_s_ConfigInitializing_24(bool value)
	{
		___s_ConfigInitializing_24 = value;
	}

	inline static int32_t get_offset_of_s_IdnScope_25() { return static_cast<int32_t>(offsetof(Uri_t4A915E1CC15B2C650F478099AD448E9466CBF612_StaticFields, ___s_IdnScope_25)); }
	inline int32_t get_s_IdnScope_25() const { return ___s_IdnScope_25; }
	inline int32_t* get_address_of_s_IdnScope_25() { return &___s_IdnScope_25; }
	inline void set_s_IdnScope_25(int32_t value)
	{
		___s_IdnScope_25 = value;
	}

	inline static int32_t get_offset_of_s_IriParsing_26() { return static_cast<int32_t>(offsetof(Uri_t4A915E1CC15B2C650F478099AD448E9466CBF612_StaticFields, ___s_IriParsing_26)); }
	inline bool get_s_IriParsing_26() const { return ___s_IriParsing_26; }
	inline bool* get_address_of_s_IriParsing_26() { return &___s_IriParsing_26; }
	inline void set_s_IriParsing_26(bool value)
	{
		___s_IriParsing_26 = value;
	}

	inline static int32_t get_offset_of_useDotNetRelativeOrAbsolute_27() { return static_cast<int32_t>(offsetof(Uri_t4A915E1CC15B2C650F478099AD448E9466CBF612_StaticFields, ___useDotNetRelativeOrAbsolute_27)); }
	inline bool get_useDotNetRelativeOrAbsolute_27() const { return ___useDotNetRelativeOrAbsolute_27; }
	inline bool* get_address_of_useDotNetRelativeOrAbsolute_27() { return &___useDotNetRelativeOrAbsolute_27; }
	inline void set_useDotNetRelativeOrAbsolute_27(bool value)
	{
		___useDotNetRelativeOrAbsolute_27 = value;
	}

	inline static int32_t get_offset_of_IsWindowsFileSystem_29() { return static_cast<int32_t>(offsetof(Uri_t4A915E1CC15B2C650F478099AD448E9466CBF612_StaticFields, ___IsWindowsFileSystem_29)); }
	inline bool get_IsWindowsFileSystem_29() const { return ___IsWindowsFileSystem_29; }
	inline bool* get_address_of_IsWindowsFileSystem_29() { return &___IsWindowsFileSystem_29; }
	inline void set_IsWindowsFileSystem_29(bool value)
	{
		___IsWindowsFileSystem_29 = value;
	}

	inline static int32_t get_offset_of_s_initLock_30() { return static_cast<int32_t>(offsetof(Uri_t4A915E1CC15B2C650F478099AD448E9466CBF612_StaticFields, ___s_initLock_30)); }
	inline RuntimeObject * get_s_initLock_30() const { return ___s_initLock_30; }
	inline RuntimeObject ** get_address_of_s_initLock_30() { return &___s_initLock_30; }
	inline void set_s_initLock_30(RuntimeObject * value)
	{
		___s_initLock_30 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___s_initLock_30), (void*)value);
	}

	inline static int32_t get_offset_of_HexLowerChars_34() { return static_cast<int32_t>(offsetof(Uri_t4A915E1CC15B2C650F478099AD448E9466CBF612_StaticFields, ___HexLowerChars_34)); }
	inline CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34* get_HexLowerChars_34() const { return ___HexLowerChars_34; }
	inline CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34** get_address_of_HexLowerChars_34() { return &___HexLowerChars_34; }
	inline void set_HexLowerChars_34(CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34* value)
	{
		___HexLowerChars_34 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___HexLowerChars_34), (void*)value);
	}

	inline static int32_t get_offset_of__WSchars_35() { return static_cast<int32_t>(offsetof(Uri_t4A915E1CC15B2C650F478099AD448E9466CBF612_StaticFields, ____WSchars_35)); }
	inline CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34* get__WSchars_35() const { return ____WSchars_35; }
	inline CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34** get_address_of__WSchars_35() { return &____WSchars_35; }
	inline void set__WSchars_35(CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34* value)
	{
		____WSchars_35 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____WSchars_35), (void*)value);
	}
};


// System.Action`1<BestHTTP.SignalRCore.HubConnection>
struct Action_1_tD3410B67BFA3C1FCF5D3D8345C933F0ACA7A5426  : public MulticastDelegate_t
{
public:

public:
};


// System.Action`2<BestHTTP.SignalRCore.HubConnection,System.String>
struct Action_2_t36E73E35B8C2732CDF2877AFC5E932F8910CD2A5  : public MulticastDelegate_t
{
public:

public:
};


// System.Action`3<BestHTTP.SignalRCore.HubConnection,BestHTTP.SignalRCore.ITransport,BestHTTP.SignalRCore.TransportEvents>
struct Action_3_t2A25F65DF18A1F5038436F84891B59501F1E97B6  : public MulticastDelegate_t
{
public:

public:
};


// System.Action`3<BestHTTP.SignalRCore.HubConnection,System.Uri,System.Uri>
struct Action_3_t5271F369265AB717807700BB149273B6EE0B1A4E  : public MulticastDelegate_t
{
public:

public:
};


// BestHTTP.Futures.FutureValueCallback`1<System.String>
struct FutureValueCallback_1_t31AE88E9DCB927E5F16747DB54C827FE97F91BBF  : public MulticastDelegate_t
{
public:

public:
};


// UnityEngine.Behaviour
struct Behaviour_t1A3DDDCF73B4627928FBFE02ED52B7251777DBD9  : public Component_t62FBC8D2420DA4BE9037AFE430740F6B3EECA684
{
public:

public:
};


// BestHTTP.SignalRCore.HubConnection
struct HubConnection_t797D4973C9BBE6895D85BF743512ADA7D419736D  : public RuntimeObject
{
public:
	// System.Uri BestHTTP.SignalRCore.HubConnection::<Uri>k__BackingField
	Uri_t4A915E1CC15B2C650F478099AD448E9466CBF612 * ___U3CUriU3Ek__BackingField_1;
	// BestHTTP.SignalRCore.ConnectionStates BestHTTP.SignalRCore.HubConnection::<State>k__BackingField
	int32_t ___U3CStateU3Ek__BackingField_2;
	// BestHTTP.SignalRCore.ITransport BestHTTP.SignalRCore.HubConnection::<Transport>k__BackingField
	RuntimeObject* ___U3CTransportU3Ek__BackingField_3;
	// BestHTTP.SignalRCore.IProtocol BestHTTP.SignalRCore.HubConnection::<Protocol>k__BackingField
	RuntimeObject* ___U3CProtocolU3Ek__BackingField_4;
	// System.Action`3<BestHTTP.SignalRCore.HubConnection,System.Uri,System.Uri> BestHTTP.SignalRCore.HubConnection::OnRedirected
	Action_3_t5271F369265AB717807700BB149273B6EE0B1A4E * ___OnRedirected_5;
	// System.Action`1<BestHTTP.SignalRCore.HubConnection> BestHTTP.SignalRCore.HubConnection::OnConnected
	Action_1_tD3410B67BFA3C1FCF5D3D8345C933F0ACA7A5426 * ___OnConnected_6;
	// System.Action`2<BestHTTP.SignalRCore.HubConnection,System.String> BestHTTP.SignalRCore.HubConnection::OnError
	Action_2_t36E73E35B8C2732CDF2877AFC5E932F8910CD2A5 * ___OnError_7;
	// System.Action`1<BestHTTP.SignalRCore.HubConnection> BestHTTP.SignalRCore.HubConnection::OnClosed
	Action_1_tD3410B67BFA3C1FCF5D3D8345C933F0ACA7A5426 * ___OnClosed_8;
	// System.Func`3<BestHTTP.SignalRCore.HubConnection,BestHTTP.SignalRCore.Messages.Message,System.Boolean> BestHTTP.SignalRCore.HubConnection::OnMessage
	Func_3_tA9872E728CF5CEA9BE21146D76352E8C6293C2C6 * ___OnMessage_9;
	// System.Action`2<BestHTTP.SignalRCore.HubConnection,System.String> BestHTTP.SignalRCore.HubConnection::OnReconnecting
	Action_2_t36E73E35B8C2732CDF2877AFC5E932F8910CD2A5 * ___OnReconnecting_10;
	// System.Action`1<BestHTTP.SignalRCore.HubConnection> BestHTTP.SignalRCore.HubConnection::OnReconnected
	Action_1_tD3410B67BFA3C1FCF5D3D8345C933F0ACA7A5426 * ___OnReconnected_11;
	// System.Action`3<BestHTTP.SignalRCore.HubConnection,BestHTTP.SignalRCore.ITransport,BestHTTP.SignalRCore.TransportEvents> BestHTTP.SignalRCore.HubConnection::OnTransportEvent
	Action_3_t2A25F65DF18A1F5038436F84891B59501F1E97B6 * ___OnTransportEvent_12;
	// BestHTTP.SignalRCore.IAuthenticationProvider BestHTTP.SignalRCore.HubConnection::<AuthenticationProvider>k__BackingField
	RuntimeObject* ___U3CAuthenticationProviderU3Ek__BackingField_13;
	// BestHTTP.SignalRCore.Messages.NegotiationResult BestHTTP.SignalRCore.HubConnection::<NegotiationResult>k__BackingField
	NegotiationResult_tB56E4CA54D44A10740AEDCBAC199D03849AEE84C * ___U3CNegotiationResultU3Ek__BackingField_14;
	// BestHTTP.SignalRCore.HubOptions BestHTTP.SignalRCore.HubConnection::<Options>k__BackingField
	HubOptions_t53B108AE0F01D633E571D739D96B573C5C68A6F7 * ___U3COptionsU3Ek__BackingField_15;
	// System.Int32 BestHTTP.SignalRCore.HubConnection::<RedirectCount>k__BackingField
	int32_t ___U3CRedirectCountU3Ek__BackingField_16;
	// BestHTTP.SignalRCore.IRetryPolicy BestHTTP.SignalRCore.HubConnection::<ReconnectPolicy>k__BackingField
	RuntimeObject* ___U3CReconnectPolicyU3Ek__BackingField_17;
	// BestHTTP.Logger.LoggingContext BestHTTP.SignalRCore.HubConnection::<Context>k__BackingField
	LoggingContext_tBC479DCBB94ED3A18F737DE8435E866B1E67178C * ___U3CContextU3Ek__BackingField_18;
	// System.Int64 BestHTTP.SignalRCore.HubConnection::lastInvocationId
	int64_t ___lastInvocationId_19;
	// System.Int32 BestHTTP.SignalRCore.HubConnection::lastStreamId
	int32_t ___lastStreamId_20;
	// System.Collections.Generic.Dictionary`2<System.Int64,BestHTTP.SignalRCore.InvocationDefinition> BestHTTP.SignalRCore.HubConnection::invocations
	Dictionary_2_tB49E454438088BDA98F73A4D9F23684ABCD53CCA * ___invocations_21;
	// System.Collections.Generic.Dictionary`2<System.String,BestHTTP.SignalRCore.Subscription> BestHTTP.SignalRCore.HubConnection::subscriptions
	Dictionary_2_t6F4431355404A55604094B7002C21EE985AD3090 * ___subscriptions_22;
	// System.DateTime BestHTTP.SignalRCore.HubConnection::lastMessageSentAt
	DateTime_tEAF2CD16E071DF5441F40822E4CFE880E5245405  ___lastMessageSentAt_23;
	// System.DateTime BestHTTP.SignalRCore.HubConnection::lastMessageReceivedAt
	DateTime_tEAF2CD16E071DF5441F40822E4CFE880E5245405  ___lastMessageReceivedAt_24;
	// System.DateTime BestHTTP.SignalRCore.HubConnection::connectionStartedAt
	DateTime_tEAF2CD16E071DF5441F40822E4CFE880E5245405  ___connectionStartedAt_25;
	// BestHTTP.SignalRCore.RetryContext BestHTTP.SignalRCore.HubConnection::currentContext
	RetryContext_t9CF398FFC95B89BFF0C1FFEE078A1C08F880A8F0  ___currentContext_26;
	// System.DateTime BestHTTP.SignalRCore.HubConnection::reconnectStartTime
	DateTime_tEAF2CD16E071DF5441F40822E4CFE880E5245405  ___reconnectStartTime_27;
	// System.DateTime BestHTTP.SignalRCore.HubConnection::reconnectAt
	DateTime_tEAF2CD16E071DF5441F40822E4CFE880E5245405  ___reconnectAt_28;
	// System.Collections.Generic.List`1<BestHTTP.SignalRCore.TransportTypes> BestHTTP.SignalRCore.HubConnection::triedoutTransports
	List_1_tA942B432640F6BFD6DDD187F45FF27BDB1719805 * ___triedoutTransports_29;
	// System.Threading.Tasks.TaskCompletionSource`1<BestHTTP.SignalRCore.HubConnection> BestHTTP.SignalRCore.HubConnection::connectAsyncTaskCompletionSource
	TaskCompletionSource_1_t59017925BDC6678DB216933E2762064022417A3F * ___connectAsyncTaskCompletionSource_30;
	// System.Threading.Tasks.TaskCompletionSource`1<BestHTTP.SignalRCore.HubConnection> BestHTTP.SignalRCore.HubConnection::closeAsyncTaskCompletionSource
	TaskCompletionSource_1_t59017925BDC6678DB216933E2762064022417A3F * ___closeAsyncTaskCompletionSource_31;

public:
	inline static int32_t get_offset_of_U3CUriU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(HubConnection_t797D4973C9BBE6895D85BF743512ADA7D419736D, ___U3CUriU3Ek__BackingField_1)); }
	inline Uri_t4A915E1CC15B2C650F478099AD448E9466CBF612 * get_U3CUriU3Ek__BackingField_1() const { return ___U3CUriU3Ek__BackingField_1; }
	inline Uri_t4A915E1CC15B2C650F478099AD448E9466CBF612 ** get_address_of_U3CUriU3Ek__BackingField_1() { return &___U3CUriU3Ek__BackingField_1; }
	inline void set_U3CUriU3Ek__BackingField_1(Uri_t4A915E1CC15B2C650F478099AD448E9466CBF612 * value)
	{
		___U3CUriU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CUriU3Ek__BackingField_1), (void*)value);
	}

	inline static int32_t get_offset_of_U3CStateU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(HubConnection_t797D4973C9BBE6895D85BF743512ADA7D419736D, ___U3CStateU3Ek__BackingField_2)); }
	inline int32_t get_U3CStateU3Ek__BackingField_2() const { return ___U3CStateU3Ek__BackingField_2; }
	inline int32_t* get_address_of_U3CStateU3Ek__BackingField_2() { return &___U3CStateU3Ek__BackingField_2; }
	inline void set_U3CStateU3Ek__BackingField_2(int32_t value)
	{
		___U3CStateU3Ek__BackingField_2 = value;
	}

	inline static int32_t get_offset_of_U3CTransportU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(HubConnection_t797D4973C9BBE6895D85BF743512ADA7D419736D, ___U3CTransportU3Ek__BackingField_3)); }
	inline RuntimeObject* get_U3CTransportU3Ek__BackingField_3() const { return ___U3CTransportU3Ek__BackingField_3; }
	inline RuntimeObject** get_address_of_U3CTransportU3Ek__BackingField_3() { return &___U3CTransportU3Ek__BackingField_3; }
	inline void set_U3CTransportU3Ek__BackingField_3(RuntimeObject* value)
	{
		___U3CTransportU3Ek__BackingField_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CTransportU3Ek__BackingField_3), (void*)value);
	}

	inline static int32_t get_offset_of_U3CProtocolU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(HubConnection_t797D4973C9BBE6895D85BF743512ADA7D419736D, ___U3CProtocolU3Ek__BackingField_4)); }
	inline RuntimeObject* get_U3CProtocolU3Ek__BackingField_4() const { return ___U3CProtocolU3Ek__BackingField_4; }
	inline RuntimeObject** get_address_of_U3CProtocolU3Ek__BackingField_4() { return &___U3CProtocolU3Ek__BackingField_4; }
	inline void set_U3CProtocolU3Ek__BackingField_4(RuntimeObject* value)
	{
		___U3CProtocolU3Ek__BackingField_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CProtocolU3Ek__BackingField_4), (void*)value);
	}

	inline static int32_t get_offset_of_OnRedirected_5() { return static_cast<int32_t>(offsetof(HubConnection_t797D4973C9BBE6895D85BF743512ADA7D419736D, ___OnRedirected_5)); }
	inline Action_3_t5271F369265AB717807700BB149273B6EE0B1A4E * get_OnRedirected_5() const { return ___OnRedirected_5; }
	inline Action_3_t5271F369265AB717807700BB149273B6EE0B1A4E ** get_address_of_OnRedirected_5() { return &___OnRedirected_5; }
	inline void set_OnRedirected_5(Action_3_t5271F369265AB717807700BB149273B6EE0B1A4E * value)
	{
		___OnRedirected_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___OnRedirected_5), (void*)value);
	}

	inline static int32_t get_offset_of_OnConnected_6() { return static_cast<int32_t>(offsetof(HubConnection_t797D4973C9BBE6895D85BF743512ADA7D419736D, ___OnConnected_6)); }
	inline Action_1_tD3410B67BFA3C1FCF5D3D8345C933F0ACA7A5426 * get_OnConnected_6() const { return ___OnConnected_6; }
	inline Action_1_tD3410B67BFA3C1FCF5D3D8345C933F0ACA7A5426 ** get_address_of_OnConnected_6() { return &___OnConnected_6; }
	inline void set_OnConnected_6(Action_1_tD3410B67BFA3C1FCF5D3D8345C933F0ACA7A5426 * value)
	{
		___OnConnected_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___OnConnected_6), (void*)value);
	}

	inline static int32_t get_offset_of_OnError_7() { return static_cast<int32_t>(offsetof(HubConnection_t797D4973C9BBE6895D85BF743512ADA7D419736D, ___OnError_7)); }
	inline Action_2_t36E73E35B8C2732CDF2877AFC5E932F8910CD2A5 * get_OnError_7() const { return ___OnError_7; }
	inline Action_2_t36E73E35B8C2732CDF2877AFC5E932F8910CD2A5 ** get_address_of_OnError_7() { return &___OnError_7; }
	inline void set_OnError_7(Action_2_t36E73E35B8C2732CDF2877AFC5E932F8910CD2A5 * value)
	{
		___OnError_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___OnError_7), (void*)value);
	}

	inline static int32_t get_offset_of_OnClosed_8() { return static_cast<int32_t>(offsetof(HubConnection_t797D4973C9BBE6895D85BF743512ADA7D419736D, ___OnClosed_8)); }
	inline Action_1_tD3410B67BFA3C1FCF5D3D8345C933F0ACA7A5426 * get_OnClosed_8() const { return ___OnClosed_8; }
	inline Action_1_tD3410B67BFA3C1FCF5D3D8345C933F0ACA7A5426 ** get_address_of_OnClosed_8() { return &___OnClosed_8; }
	inline void set_OnClosed_8(Action_1_tD3410B67BFA3C1FCF5D3D8345C933F0ACA7A5426 * value)
	{
		___OnClosed_8 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___OnClosed_8), (void*)value);
	}

	inline static int32_t get_offset_of_OnMessage_9() { return static_cast<int32_t>(offsetof(HubConnection_t797D4973C9BBE6895D85BF743512ADA7D419736D, ___OnMessage_9)); }
	inline Func_3_tA9872E728CF5CEA9BE21146D76352E8C6293C2C6 * get_OnMessage_9() const { return ___OnMessage_9; }
	inline Func_3_tA9872E728CF5CEA9BE21146D76352E8C6293C2C6 ** get_address_of_OnMessage_9() { return &___OnMessage_9; }
	inline void set_OnMessage_9(Func_3_tA9872E728CF5CEA9BE21146D76352E8C6293C2C6 * value)
	{
		___OnMessage_9 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___OnMessage_9), (void*)value);
	}

	inline static int32_t get_offset_of_OnReconnecting_10() { return static_cast<int32_t>(offsetof(HubConnection_t797D4973C9BBE6895D85BF743512ADA7D419736D, ___OnReconnecting_10)); }
	inline Action_2_t36E73E35B8C2732CDF2877AFC5E932F8910CD2A5 * get_OnReconnecting_10() const { return ___OnReconnecting_10; }
	inline Action_2_t36E73E35B8C2732CDF2877AFC5E932F8910CD2A5 ** get_address_of_OnReconnecting_10() { return &___OnReconnecting_10; }
	inline void set_OnReconnecting_10(Action_2_t36E73E35B8C2732CDF2877AFC5E932F8910CD2A5 * value)
	{
		___OnReconnecting_10 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___OnReconnecting_10), (void*)value);
	}

	inline static int32_t get_offset_of_OnReconnected_11() { return static_cast<int32_t>(offsetof(HubConnection_t797D4973C9BBE6895D85BF743512ADA7D419736D, ___OnReconnected_11)); }
	inline Action_1_tD3410B67BFA3C1FCF5D3D8345C933F0ACA7A5426 * get_OnReconnected_11() const { return ___OnReconnected_11; }
	inline Action_1_tD3410B67BFA3C1FCF5D3D8345C933F0ACA7A5426 ** get_address_of_OnReconnected_11() { return &___OnReconnected_11; }
	inline void set_OnReconnected_11(Action_1_tD3410B67BFA3C1FCF5D3D8345C933F0ACA7A5426 * value)
	{
		___OnReconnected_11 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___OnReconnected_11), (void*)value);
	}

	inline static int32_t get_offset_of_OnTransportEvent_12() { return static_cast<int32_t>(offsetof(HubConnection_t797D4973C9BBE6895D85BF743512ADA7D419736D, ___OnTransportEvent_12)); }
	inline Action_3_t2A25F65DF18A1F5038436F84891B59501F1E97B6 * get_OnTransportEvent_12() const { return ___OnTransportEvent_12; }
	inline Action_3_t2A25F65DF18A1F5038436F84891B59501F1E97B6 ** get_address_of_OnTransportEvent_12() { return &___OnTransportEvent_12; }
	inline void set_OnTransportEvent_12(Action_3_t2A25F65DF18A1F5038436F84891B59501F1E97B6 * value)
	{
		___OnTransportEvent_12 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___OnTransportEvent_12), (void*)value);
	}

	inline static int32_t get_offset_of_U3CAuthenticationProviderU3Ek__BackingField_13() { return static_cast<int32_t>(offsetof(HubConnection_t797D4973C9BBE6895D85BF743512ADA7D419736D, ___U3CAuthenticationProviderU3Ek__BackingField_13)); }
	inline RuntimeObject* get_U3CAuthenticationProviderU3Ek__BackingField_13() const { return ___U3CAuthenticationProviderU3Ek__BackingField_13; }
	inline RuntimeObject** get_address_of_U3CAuthenticationProviderU3Ek__BackingField_13() { return &___U3CAuthenticationProviderU3Ek__BackingField_13; }
	inline void set_U3CAuthenticationProviderU3Ek__BackingField_13(RuntimeObject* value)
	{
		___U3CAuthenticationProviderU3Ek__BackingField_13 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CAuthenticationProviderU3Ek__BackingField_13), (void*)value);
	}

	inline static int32_t get_offset_of_U3CNegotiationResultU3Ek__BackingField_14() { return static_cast<int32_t>(offsetof(HubConnection_t797D4973C9BBE6895D85BF743512ADA7D419736D, ___U3CNegotiationResultU3Ek__BackingField_14)); }
	inline NegotiationResult_tB56E4CA54D44A10740AEDCBAC199D03849AEE84C * get_U3CNegotiationResultU3Ek__BackingField_14() const { return ___U3CNegotiationResultU3Ek__BackingField_14; }
	inline NegotiationResult_tB56E4CA54D44A10740AEDCBAC199D03849AEE84C ** get_address_of_U3CNegotiationResultU3Ek__BackingField_14() { return &___U3CNegotiationResultU3Ek__BackingField_14; }
	inline void set_U3CNegotiationResultU3Ek__BackingField_14(NegotiationResult_tB56E4CA54D44A10740AEDCBAC199D03849AEE84C * value)
	{
		___U3CNegotiationResultU3Ek__BackingField_14 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CNegotiationResultU3Ek__BackingField_14), (void*)value);
	}

	inline static int32_t get_offset_of_U3COptionsU3Ek__BackingField_15() { return static_cast<int32_t>(offsetof(HubConnection_t797D4973C9BBE6895D85BF743512ADA7D419736D, ___U3COptionsU3Ek__BackingField_15)); }
	inline HubOptions_t53B108AE0F01D633E571D739D96B573C5C68A6F7 * get_U3COptionsU3Ek__BackingField_15() const { return ___U3COptionsU3Ek__BackingField_15; }
	inline HubOptions_t53B108AE0F01D633E571D739D96B573C5C68A6F7 ** get_address_of_U3COptionsU3Ek__BackingField_15() { return &___U3COptionsU3Ek__BackingField_15; }
	inline void set_U3COptionsU3Ek__BackingField_15(HubOptions_t53B108AE0F01D633E571D739D96B573C5C68A6F7 * value)
	{
		___U3COptionsU3Ek__BackingField_15 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3COptionsU3Ek__BackingField_15), (void*)value);
	}

	inline static int32_t get_offset_of_U3CRedirectCountU3Ek__BackingField_16() { return static_cast<int32_t>(offsetof(HubConnection_t797D4973C9BBE6895D85BF743512ADA7D419736D, ___U3CRedirectCountU3Ek__BackingField_16)); }
	inline int32_t get_U3CRedirectCountU3Ek__BackingField_16() const { return ___U3CRedirectCountU3Ek__BackingField_16; }
	inline int32_t* get_address_of_U3CRedirectCountU3Ek__BackingField_16() { return &___U3CRedirectCountU3Ek__BackingField_16; }
	inline void set_U3CRedirectCountU3Ek__BackingField_16(int32_t value)
	{
		___U3CRedirectCountU3Ek__BackingField_16 = value;
	}

	inline static int32_t get_offset_of_U3CReconnectPolicyU3Ek__BackingField_17() { return static_cast<int32_t>(offsetof(HubConnection_t797D4973C9BBE6895D85BF743512ADA7D419736D, ___U3CReconnectPolicyU3Ek__BackingField_17)); }
	inline RuntimeObject* get_U3CReconnectPolicyU3Ek__BackingField_17() const { return ___U3CReconnectPolicyU3Ek__BackingField_17; }
	inline RuntimeObject** get_address_of_U3CReconnectPolicyU3Ek__BackingField_17() { return &___U3CReconnectPolicyU3Ek__BackingField_17; }
	inline void set_U3CReconnectPolicyU3Ek__BackingField_17(RuntimeObject* value)
	{
		___U3CReconnectPolicyU3Ek__BackingField_17 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CReconnectPolicyU3Ek__BackingField_17), (void*)value);
	}

	inline static int32_t get_offset_of_U3CContextU3Ek__BackingField_18() { return static_cast<int32_t>(offsetof(HubConnection_t797D4973C9BBE6895D85BF743512ADA7D419736D, ___U3CContextU3Ek__BackingField_18)); }
	inline LoggingContext_tBC479DCBB94ED3A18F737DE8435E866B1E67178C * get_U3CContextU3Ek__BackingField_18() const { return ___U3CContextU3Ek__BackingField_18; }
	inline LoggingContext_tBC479DCBB94ED3A18F737DE8435E866B1E67178C ** get_address_of_U3CContextU3Ek__BackingField_18() { return &___U3CContextU3Ek__BackingField_18; }
	inline void set_U3CContextU3Ek__BackingField_18(LoggingContext_tBC479DCBB94ED3A18F737DE8435E866B1E67178C * value)
	{
		___U3CContextU3Ek__BackingField_18 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CContextU3Ek__BackingField_18), (void*)value);
	}

	inline static int32_t get_offset_of_lastInvocationId_19() { return static_cast<int32_t>(offsetof(HubConnection_t797D4973C9BBE6895D85BF743512ADA7D419736D, ___lastInvocationId_19)); }
	inline int64_t get_lastInvocationId_19() const { return ___lastInvocationId_19; }
	inline int64_t* get_address_of_lastInvocationId_19() { return &___lastInvocationId_19; }
	inline void set_lastInvocationId_19(int64_t value)
	{
		___lastInvocationId_19 = value;
	}

	inline static int32_t get_offset_of_lastStreamId_20() { return static_cast<int32_t>(offsetof(HubConnection_t797D4973C9BBE6895D85BF743512ADA7D419736D, ___lastStreamId_20)); }
	inline int32_t get_lastStreamId_20() const { return ___lastStreamId_20; }
	inline int32_t* get_address_of_lastStreamId_20() { return &___lastStreamId_20; }
	inline void set_lastStreamId_20(int32_t value)
	{
		___lastStreamId_20 = value;
	}

	inline static int32_t get_offset_of_invocations_21() { return static_cast<int32_t>(offsetof(HubConnection_t797D4973C9BBE6895D85BF743512ADA7D419736D, ___invocations_21)); }
	inline Dictionary_2_tB49E454438088BDA98F73A4D9F23684ABCD53CCA * get_invocations_21() const { return ___invocations_21; }
	inline Dictionary_2_tB49E454438088BDA98F73A4D9F23684ABCD53CCA ** get_address_of_invocations_21() { return &___invocations_21; }
	inline void set_invocations_21(Dictionary_2_tB49E454438088BDA98F73A4D9F23684ABCD53CCA * value)
	{
		___invocations_21 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___invocations_21), (void*)value);
	}

	inline static int32_t get_offset_of_subscriptions_22() { return static_cast<int32_t>(offsetof(HubConnection_t797D4973C9BBE6895D85BF743512ADA7D419736D, ___subscriptions_22)); }
	inline Dictionary_2_t6F4431355404A55604094B7002C21EE985AD3090 * get_subscriptions_22() const { return ___subscriptions_22; }
	inline Dictionary_2_t6F4431355404A55604094B7002C21EE985AD3090 ** get_address_of_subscriptions_22() { return &___subscriptions_22; }
	inline void set_subscriptions_22(Dictionary_2_t6F4431355404A55604094B7002C21EE985AD3090 * value)
	{
		___subscriptions_22 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___subscriptions_22), (void*)value);
	}

	inline static int32_t get_offset_of_lastMessageSentAt_23() { return static_cast<int32_t>(offsetof(HubConnection_t797D4973C9BBE6895D85BF743512ADA7D419736D, ___lastMessageSentAt_23)); }
	inline DateTime_tEAF2CD16E071DF5441F40822E4CFE880E5245405  get_lastMessageSentAt_23() const { return ___lastMessageSentAt_23; }
	inline DateTime_tEAF2CD16E071DF5441F40822E4CFE880E5245405 * get_address_of_lastMessageSentAt_23() { return &___lastMessageSentAt_23; }
	inline void set_lastMessageSentAt_23(DateTime_tEAF2CD16E071DF5441F40822E4CFE880E5245405  value)
	{
		___lastMessageSentAt_23 = value;
	}

	inline static int32_t get_offset_of_lastMessageReceivedAt_24() { return static_cast<int32_t>(offsetof(HubConnection_t797D4973C9BBE6895D85BF743512ADA7D419736D, ___lastMessageReceivedAt_24)); }
	inline DateTime_tEAF2CD16E071DF5441F40822E4CFE880E5245405  get_lastMessageReceivedAt_24() const { return ___lastMessageReceivedAt_24; }
	inline DateTime_tEAF2CD16E071DF5441F40822E4CFE880E5245405 * get_address_of_lastMessageReceivedAt_24() { return &___lastMessageReceivedAt_24; }
	inline void set_lastMessageReceivedAt_24(DateTime_tEAF2CD16E071DF5441F40822E4CFE880E5245405  value)
	{
		___lastMessageReceivedAt_24 = value;
	}

	inline static int32_t get_offset_of_connectionStartedAt_25() { return static_cast<int32_t>(offsetof(HubConnection_t797D4973C9BBE6895D85BF743512ADA7D419736D, ___connectionStartedAt_25)); }
	inline DateTime_tEAF2CD16E071DF5441F40822E4CFE880E5245405  get_connectionStartedAt_25() const { return ___connectionStartedAt_25; }
	inline DateTime_tEAF2CD16E071DF5441F40822E4CFE880E5245405 * get_address_of_connectionStartedAt_25() { return &___connectionStartedAt_25; }
	inline void set_connectionStartedAt_25(DateTime_tEAF2CD16E071DF5441F40822E4CFE880E5245405  value)
	{
		___connectionStartedAt_25 = value;
	}

	inline static int32_t get_offset_of_currentContext_26() { return static_cast<int32_t>(offsetof(HubConnection_t797D4973C9BBE6895D85BF743512ADA7D419736D, ___currentContext_26)); }
	inline RetryContext_t9CF398FFC95B89BFF0C1FFEE078A1C08F880A8F0  get_currentContext_26() const { return ___currentContext_26; }
	inline RetryContext_t9CF398FFC95B89BFF0C1FFEE078A1C08F880A8F0 * get_address_of_currentContext_26() { return &___currentContext_26; }
	inline void set_currentContext_26(RetryContext_t9CF398FFC95B89BFF0C1FFEE078A1C08F880A8F0  value)
	{
		___currentContext_26 = value;
		Il2CppCodeGenWriteBarrier((void**)&(((&___currentContext_26))->___RetryReason_2), (void*)NULL);
	}

	inline static int32_t get_offset_of_reconnectStartTime_27() { return static_cast<int32_t>(offsetof(HubConnection_t797D4973C9BBE6895D85BF743512ADA7D419736D, ___reconnectStartTime_27)); }
	inline DateTime_tEAF2CD16E071DF5441F40822E4CFE880E5245405  get_reconnectStartTime_27() const { return ___reconnectStartTime_27; }
	inline DateTime_tEAF2CD16E071DF5441F40822E4CFE880E5245405 * get_address_of_reconnectStartTime_27() { return &___reconnectStartTime_27; }
	inline void set_reconnectStartTime_27(DateTime_tEAF2CD16E071DF5441F40822E4CFE880E5245405  value)
	{
		___reconnectStartTime_27 = value;
	}

	inline static int32_t get_offset_of_reconnectAt_28() { return static_cast<int32_t>(offsetof(HubConnection_t797D4973C9BBE6895D85BF743512ADA7D419736D, ___reconnectAt_28)); }
	inline DateTime_tEAF2CD16E071DF5441F40822E4CFE880E5245405  get_reconnectAt_28() const { return ___reconnectAt_28; }
	inline DateTime_tEAF2CD16E071DF5441F40822E4CFE880E5245405 * get_address_of_reconnectAt_28() { return &___reconnectAt_28; }
	inline void set_reconnectAt_28(DateTime_tEAF2CD16E071DF5441F40822E4CFE880E5245405  value)
	{
		___reconnectAt_28 = value;
	}

	inline static int32_t get_offset_of_triedoutTransports_29() { return static_cast<int32_t>(offsetof(HubConnection_t797D4973C9BBE6895D85BF743512ADA7D419736D, ___triedoutTransports_29)); }
	inline List_1_tA942B432640F6BFD6DDD187F45FF27BDB1719805 * get_triedoutTransports_29() const { return ___triedoutTransports_29; }
	inline List_1_tA942B432640F6BFD6DDD187F45FF27BDB1719805 ** get_address_of_triedoutTransports_29() { return &___triedoutTransports_29; }
	inline void set_triedoutTransports_29(List_1_tA942B432640F6BFD6DDD187F45FF27BDB1719805 * value)
	{
		___triedoutTransports_29 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___triedoutTransports_29), (void*)value);
	}

	inline static int32_t get_offset_of_connectAsyncTaskCompletionSource_30() { return static_cast<int32_t>(offsetof(HubConnection_t797D4973C9BBE6895D85BF743512ADA7D419736D, ___connectAsyncTaskCompletionSource_30)); }
	inline TaskCompletionSource_1_t59017925BDC6678DB216933E2762064022417A3F * get_connectAsyncTaskCompletionSource_30() const { return ___connectAsyncTaskCompletionSource_30; }
	inline TaskCompletionSource_1_t59017925BDC6678DB216933E2762064022417A3F ** get_address_of_connectAsyncTaskCompletionSource_30() { return &___connectAsyncTaskCompletionSource_30; }
	inline void set_connectAsyncTaskCompletionSource_30(TaskCompletionSource_1_t59017925BDC6678DB216933E2762064022417A3F * value)
	{
		___connectAsyncTaskCompletionSource_30 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___connectAsyncTaskCompletionSource_30), (void*)value);
	}

	inline static int32_t get_offset_of_closeAsyncTaskCompletionSource_31() { return static_cast<int32_t>(offsetof(HubConnection_t797D4973C9BBE6895D85BF743512ADA7D419736D, ___closeAsyncTaskCompletionSource_31)); }
	inline TaskCompletionSource_1_t59017925BDC6678DB216933E2762064022417A3F * get_closeAsyncTaskCompletionSource_31() const { return ___closeAsyncTaskCompletionSource_31; }
	inline TaskCompletionSource_1_t59017925BDC6678DB216933E2762064022417A3F ** get_address_of_closeAsyncTaskCompletionSource_31() { return &___closeAsyncTaskCompletionSource_31; }
	inline void set_closeAsyncTaskCompletionSource_31(TaskCompletionSource_1_t59017925BDC6678DB216933E2762064022417A3F * value)
	{
		___closeAsyncTaskCompletionSource_31 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___closeAsyncTaskCompletionSource_31), (void*)value);
	}
};

struct HubConnection_t797D4973C9BBE6895D85BF743512ADA7D419736D_StaticFields
{
public:
	// System.Object[] BestHTTP.SignalRCore.HubConnection::EmptyArgs
	ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE* ___EmptyArgs_0;

public:
	inline static int32_t get_offset_of_EmptyArgs_0() { return static_cast<int32_t>(offsetof(HubConnection_t797D4973C9BBE6895D85BF743512ADA7D419736D_StaticFields, ___EmptyArgs_0)); }
	inline ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE* get_EmptyArgs_0() const { return ___EmptyArgs_0; }
	inline ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE** get_address_of_EmptyArgs_0() { return &___EmptyArgs_0; }
	inline void set_EmptyArgs_0(ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE* value)
	{
		___EmptyArgs_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___EmptyArgs_0), (void*)value);
	}
};


// BestHTTP.SignalRCore.OnAuthenticationFailedDelegate
struct OnAuthenticationFailedDelegate_tB415D1478EDFF7309A953E24BD13797DEFAAF9B2  : public MulticastDelegate_t
{
public:

public:
};


// BestHTTP.SignalRCore.OnAuthenticationSuccededDelegate
struct OnAuthenticationSuccededDelegate_t7A96699C45F6100206650225F8875DCAC1A8DE46  : public MulticastDelegate_t
{
public:

public:
};


// UnityEngine.Transform
struct Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1  : public Component_t62FBC8D2420DA4BE9037AFE430740F6B3EECA684
{
public:

public:
};


// UnityEngine.MonoBehaviour
struct MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A  : public Behaviour_t1A3DDDCF73B4627928FBFE02ED52B7251777DBD9
{
public:

public:
};


// UnityEngine.RectTransform
struct RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072  : public Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1
{
public:

public:
};

struct RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072_StaticFields
{
public:
	// UnityEngine.RectTransform/ReapplyDrivenProperties UnityEngine.RectTransform::reapplyDrivenProperties
	ReapplyDrivenProperties_t1441259DADA8FE33A95334AC24C017DFA3DEB4CE * ___reapplyDrivenProperties_4;

public:
	inline static int32_t get_offset_of_reapplyDrivenProperties_4() { return static_cast<int32_t>(offsetof(RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072_StaticFields, ___reapplyDrivenProperties_4)); }
	inline ReapplyDrivenProperties_t1441259DADA8FE33A95334AC24C017DFA3DEB4CE * get_reapplyDrivenProperties_4() const { return ___reapplyDrivenProperties_4; }
	inline ReapplyDrivenProperties_t1441259DADA8FE33A95334AC24C017DFA3DEB4CE ** get_address_of_reapplyDrivenProperties_4() { return &___reapplyDrivenProperties_4; }
	inline void set_reapplyDrivenProperties_4(ReapplyDrivenProperties_t1441259DADA8FE33A95334AC24C017DFA3DEB4CE * value)
	{
		___reapplyDrivenProperties_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___reapplyDrivenProperties_4), (void*)value);
	}
};


// BestHTTP.Examples.Helpers.SampleBase
struct SampleBase_tC30950FFCCCE8696B341569EEE364BF4C4DB7157  : public MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A
{
public:
	// System.String BestHTTP.Examples.Helpers.SampleBase::Category
	String_t* ___Category_4;
	// System.String BestHTTP.Examples.Helpers.SampleBase::DisplayName
	String_t* ___DisplayName_5;
	// System.String BestHTTP.Examples.Helpers.SampleBase::Description
	String_t* ___Description_6;
	// UnityEngine.RuntimePlatform[] BestHTTP.Examples.Helpers.SampleBase::BannedPlatforms
	RuntimePlatformU5BU5D_tA221FE8D5CE756108CBC39E15F0CB99A0787AD52* ___BannedPlatforms_7;
	// BestHTTP.Examples.SampleRoot BestHTTP.Examples.Helpers.SampleBase::sampleSelector
	SampleRoot_tB11D3D3067248A62081D6D1A6F5936A1A3D856E1 * ___sampleSelector_8;

public:
	inline static int32_t get_offset_of_Category_4() { return static_cast<int32_t>(offsetof(SampleBase_tC30950FFCCCE8696B341569EEE364BF4C4DB7157, ___Category_4)); }
	inline String_t* get_Category_4() const { return ___Category_4; }
	inline String_t** get_address_of_Category_4() { return &___Category_4; }
	inline void set_Category_4(String_t* value)
	{
		___Category_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___Category_4), (void*)value);
	}

	inline static int32_t get_offset_of_DisplayName_5() { return static_cast<int32_t>(offsetof(SampleBase_tC30950FFCCCE8696B341569EEE364BF4C4DB7157, ___DisplayName_5)); }
	inline String_t* get_DisplayName_5() const { return ___DisplayName_5; }
	inline String_t** get_address_of_DisplayName_5() { return &___DisplayName_5; }
	inline void set_DisplayName_5(String_t* value)
	{
		___DisplayName_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___DisplayName_5), (void*)value);
	}

	inline static int32_t get_offset_of_Description_6() { return static_cast<int32_t>(offsetof(SampleBase_tC30950FFCCCE8696B341569EEE364BF4C4DB7157, ___Description_6)); }
	inline String_t* get_Description_6() const { return ___Description_6; }
	inline String_t** get_address_of_Description_6() { return &___Description_6; }
	inline void set_Description_6(String_t* value)
	{
		___Description_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___Description_6), (void*)value);
	}

	inline static int32_t get_offset_of_BannedPlatforms_7() { return static_cast<int32_t>(offsetof(SampleBase_tC30950FFCCCE8696B341569EEE364BF4C4DB7157, ___BannedPlatforms_7)); }
	inline RuntimePlatformU5BU5D_tA221FE8D5CE756108CBC39E15F0CB99A0787AD52* get_BannedPlatforms_7() const { return ___BannedPlatforms_7; }
	inline RuntimePlatformU5BU5D_tA221FE8D5CE756108CBC39E15F0CB99A0787AD52** get_address_of_BannedPlatforms_7() { return &___BannedPlatforms_7; }
	inline void set_BannedPlatforms_7(RuntimePlatformU5BU5D_tA221FE8D5CE756108CBC39E15F0CB99A0787AD52* value)
	{
		___BannedPlatforms_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___BannedPlatforms_7), (void*)value);
	}

	inline static int32_t get_offset_of_sampleSelector_8() { return static_cast<int32_t>(offsetof(SampleBase_tC30950FFCCCE8696B341569EEE364BF4C4DB7157, ___sampleSelector_8)); }
	inline SampleRoot_tB11D3D3067248A62081D6D1A6F5936A1A3D856E1 * get_sampleSelector_8() const { return ___sampleSelector_8; }
	inline SampleRoot_tB11D3D3067248A62081D6D1A6F5936A1A3D856E1 ** get_address_of_sampleSelector_8() { return &___sampleSelector_8; }
	inline void set_sampleSelector_8(SampleRoot_tB11D3D3067248A62081D6D1A6F5936A1A3D856E1 * value)
	{
		___sampleSelector_8 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___sampleSelector_8), (void*)value);
	}
};


// BestHTTP.Examples.SampleRoot
struct SampleRoot_tB11D3D3067248A62081D6D1A6F5936A1A3D856E1  : public MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A
{
public:
	// System.String BestHTTP.Examples.SampleRoot::BaseURL
	String_t* ___BaseURL_4;
	// System.String BestHTTP.Examples.SampleRoot::CDNUrl
	String_t* ___CDNUrl_5;
	// UnityEngine.UI.Text BestHTTP.Examples.SampleRoot::_pluginVersion
	Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 * ____pluginVersion_6;
	// UnityEngine.UI.Dropdown BestHTTP.Examples.SampleRoot::_logLevelDropdown
	Dropdown_t099F5232BB75810BC79EED6E27DDCED46C3BCD96 * ____logLevelDropdown_7;
	// UnityEngine.UI.Text BestHTTP.Examples.SampleRoot::_proxyLabel
	Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 * ____proxyLabel_8;
	// UnityEngine.UI.InputField BestHTTP.Examples.SampleRoot::_proxyInputField
	InputField_tB41A2814F31A3E9373D443EDEBBB2856006324D0 * ____proxyInputField_9;
	// System.Collections.Generic.List`1<BestHTTP.Examples.Helpers.SampleBase> BestHTTP.Examples.SampleRoot::samples
	List_1_tED79C3AFE45C4FEB86845EA70A708CADDAA78135 * ___samples_10;
	// BestHTTP.Examples.Helpers.SampleBase BestHTTP.Examples.SampleRoot::selectedExamplePrefab
	SampleBase_tC30950FFCCCE8696B341569EEE364BF4C4DB7157 * ___selectedExamplePrefab_11;

public:
	inline static int32_t get_offset_of_BaseURL_4() { return static_cast<int32_t>(offsetof(SampleRoot_tB11D3D3067248A62081D6D1A6F5936A1A3D856E1, ___BaseURL_4)); }
	inline String_t* get_BaseURL_4() const { return ___BaseURL_4; }
	inline String_t** get_address_of_BaseURL_4() { return &___BaseURL_4; }
	inline void set_BaseURL_4(String_t* value)
	{
		___BaseURL_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___BaseURL_4), (void*)value);
	}

	inline static int32_t get_offset_of_CDNUrl_5() { return static_cast<int32_t>(offsetof(SampleRoot_tB11D3D3067248A62081D6D1A6F5936A1A3D856E1, ___CDNUrl_5)); }
	inline String_t* get_CDNUrl_5() const { return ___CDNUrl_5; }
	inline String_t** get_address_of_CDNUrl_5() { return &___CDNUrl_5; }
	inline void set_CDNUrl_5(String_t* value)
	{
		___CDNUrl_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___CDNUrl_5), (void*)value);
	}

	inline static int32_t get_offset_of__pluginVersion_6() { return static_cast<int32_t>(offsetof(SampleRoot_tB11D3D3067248A62081D6D1A6F5936A1A3D856E1, ____pluginVersion_6)); }
	inline Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 * get__pluginVersion_6() const { return ____pluginVersion_6; }
	inline Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 ** get_address_of__pluginVersion_6() { return &____pluginVersion_6; }
	inline void set__pluginVersion_6(Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 * value)
	{
		____pluginVersion_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____pluginVersion_6), (void*)value);
	}

	inline static int32_t get_offset_of__logLevelDropdown_7() { return static_cast<int32_t>(offsetof(SampleRoot_tB11D3D3067248A62081D6D1A6F5936A1A3D856E1, ____logLevelDropdown_7)); }
	inline Dropdown_t099F5232BB75810BC79EED6E27DDCED46C3BCD96 * get__logLevelDropdown_7() const { return ____logLevelDropdown_7; }
	inline Dropdown_t099F5232BB75810BC79EED6E27DDCED46C3BCD96 ** get_address_of__logLevelDropdown_7() { return &____logLevelDropdown_7; }
	inline void set__logLevelDropdown_7(Dropdown_t099F5232BB75810BC79EED6E27DDCED46C3BCD96 * value)
	{
		____logLevelDropdown_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____logLevelDropdown_7), (void*)value);
	}

	inline static int32_t get_offset_of__proxyLabel_8() { return static_cast<int32_t>(offsetof(SampleRoot_tB11D3D3067248A62081D6D1A6F5936A1A3D856E1, ____proxyLabel_8)); }
	inline Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 * get__proxyLabel_8() const { return ____proxyLabel_8; }
	inline Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 ** get_address_of__proxyLabel_8() { return &____proxyLabel_8; }
	inline void set__proxyLabel_8(Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 * value)
	{
		____proxyLabel_8 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____proxyLabel_8), (void*)value);
	}

	inline static int32_t get_offset_of__proxyInputField_9() { return static_cast<int32_t>(offsetof(SampleRoot_tB11D3D3067248A62081D6D1A6F5936A1A3D856E1, ____proxyInputField_9)); }
	inline InputField_tB41A2814F31A3E9373D443EDEBBB2856006324D0 * get__proxyInputField_9() const { return ____proxyInputField_9; }
	inline InputField_tB41A2814F31A3E9373D443EDEBBB2856006324D0 ** get_address_of__proxyInputField_9() { return &____proxyInputField_9; }
	inline void set__proxyInputField_9(InputField_tB41A2814F31A3E9373D443EDEBBB2856006324D0 * value)
	{
		____proxyInputField_9 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____proxyInputField_9), (void*)value);
	}

	inline static int32_t get_offset_of_samples_10() { return static_cast<int32_t>(offsetof(SampleRoot_tB11D3D3067248A62081D6D1A6F5936A1A3D856E1, ___samples_10)); }
	inline List_1_tED79C3AFE45C4FEB86845EA70A708CADDAA78135 * get_samples_10() const { return ___samples_10; }
	inline List_1_tED79C3AFE45C4FEB86845EA70A708CADDAA78135 ** get_address_of_samples_10() { return &___samples_10; }
	inline void set_samples_10(List_1_tED79C3AFE45C4FEB86845EA70A708CADDAA78135 * value)
	{
		___samples_10 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___samples_10), (void*)value);
	}

	inline static int32_t get_offset_of_selectedExamplePrefab_11() { return static_cast<int32_t>(offsetof(SampleRoot_tB11D3D3067248A62081D6D1A6F5936A1A3D856E1, ___selectedExamplePrefab_11)); }
	inline SampleBase_tC30950FFCCCE8696B341569EEE364BF4C4DB7157 * get_selectedExamplePrefab_11() const { return ___selectedExamplePrefab_11; }
	inline SampleBase_tC30950FFCCCE8696B341569EEE364BF4C4DB7157 ** get_address_of_selectedExamplePrefab_11() { return &___selectedExamplePrefab_11; }
	inline void set_selectedExamplePrefab_11(SampleBase_tC30950FFCCCE8696B341569EEE364BF4C4DB7157 * value)
	{
		___selectedExamplePrefab_11 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___selectedExamplePrefab_11), (void*)value);
	}
};


// BestHTTP.Examples.Helpers.TextListItem
struct TextListItem_tC8FEA392C6AF65CFF22D8089B9104B79C84A6D8B  : public MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A
{
public:
	// UnityEngine.UI.Text BestHTTP.Examples.Helpers.TextListItem::_text
	Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 * ____text_4;

public:
	inline static int32_t get_offset_of__text_4() { return static_cast<int32_t>(offsetof(TextListItem_tC8FEA392C6AF65CFF22D8089B9104B79C84A6D8B, ____text_4)); }
	inline Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 * get__text_4() const { return ____text_4; }
	inline Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 ** get_address_of__text_4() { return &____text_4; }
	inline void set__text_4(Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 * value)
	{
		____text_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____text_4), (void*)value);
	}
};


// UnityEngine.EventSystems.UIBehaviour
struct UIBehaviour_tD1C6E2D542222546D68510ECE74036EFBC3C3B0E  : public MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A
{
public:

public:
};


// BestHTTP.Examples.HubWithAuthorizationSample
struct HubWithAuthorizationSample_t19979B3628DD848910FD70011959F7CB115D5968  : public SampleBase_tC30950FFCCCE8696B341569EEE364BF4C4DB7157
{
public:
	// System.String BestHTTP.Examples.HubWithAuthorizationSample::_path
	String_t* ____path_9;
	// UnityEngine.UI.ScrollRect BestHTTP.Examples.HubWithAuthorizationSample::_scrollRect
	ScrollRect_tB16156010F89FFDAAB2127CA878608FD91B9FBEA * ____scrollRect_10;
	// UnityEngine.RectTransform BestHTTP.Examples.HubWithAuthorizationSample::_contentRoot
	RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 * ____contentRoot_11;
	// BestHTTP.Examples.Helpers.TextListItem BestHTTP.Examples.HubWithAuthorizationSample::_listItemPrefab
	TextListItem_tC8FEA392C6AF65CFF22D8089B9104B79C84A6D8B * ____listItemPrefab_12;
	// System.Int32 BestHTTP.Examples.HubWithAuthorizationSample::_maxListItemEntries
	int32_t ____maxListItemEntries_13;
	// UnityEngine.UI.Button BestHTTP.Examples.HubWithAuthorizationSample::_connectButton
	Button_tA893FC15AB26E1439AC25BDCA7079530587BB65D * ____connectButton_14;
	// UnityEngine.UI.Button BestHTTP.Examples.HubWithAuthorizationSample::_closeButton
	Button_tA893FC15AB26E1439AC25BDCA7079530587BB65D * ____closeButton_15;
	// BestHTTP.SignalRCore.HubConnection BestHTTP.Examples.HubWithAuthorizationSample::hub
	HubConnection_t797D4973C9BBE6895D85BF743512ADA7D419736D * ___hub_16;

public:
	inline static int32_t get_offset_of__path_9() { return static_cast<int32_t>(offsetof(HubWithAuthorizationSample_t19979B3628DD848910FD70011959F7CB115D5968, ____path_9)); }
	inline String_t* get__path_9() const { return ____path_9; }
	inline String_t** get_address_of__path_9() { return &____path_9; }
	inline void set__path_9(String_t* value)
	{
		____path_9 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____path_9), (void*)value);
	}

	inline static int32_t get_offset_of__scrollRect_10() { return static_cast<int32_t>(offsetof(HubWithAuthorizationSample_t19979B3628DD848910FD70011959F7CB115D5968, ____scrollRect_10)); }
	inline ScrollRect_tB16156010F89FFDAAB2127CA878608FD91B9FBEA * get__scrollRect_10() const { return ____scrollRect_10; }
	inline ScrollRect_tB16156010F89FFDAAB2127CA878608FD91B9FBEA ** get_address_of__scrollRect_10() { return &____scrollRect_10; }
	inline void set__scrollRect_10(ScrollRect_tB16156010F89FFDAAB2127CA878608FD91B9FBEA * value)
	{
		____scrollRect_10 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____scrollRect_10), (void*)value);
	}

	inline static int32_t get_offset_of__contentRoot_11() { return static_cast<int32_t>(offsetof(HubWithAuthorizationSample_t19979B3628DD848910FD70011959F7CB115D5968, ____contentRoot_11)); }
	inline RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 * get__contentRoot_11() const { return ____contentRoot_11; }
	inline RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 ** get_address_of__contentRoot_11() { return &____contentRoot_11; }
	inline void set__contentRoot_11(RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 * value)
	{
		____contentRoot_11 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____contentRoot_11), (void*)value);
	}

	inline static int32_t get_offset_of__listItemPrefab_12() { return static_cast<int32_t>(offsetof(HubWithAuthorizationSample_t19979B3628DD848910FD70011959F7CB115D5968, ____listItemPrefab_12)); }
	inline TextListItem_tC8FEA392C6AF65CFF22D8089B9104B79C84A6D8B * get__listItemPrefab_12() const { return ____listItemPrefab_12; }
	inline TextListItem_tC8FEA392C6AF65CFF22D8089B9104B79C84A6D8B ** get_address_of__listItemPrefab_12() { return &____listItemPrefab_12; }
	inline void set__listItemPrefab_12(TextListItem_tC8FEA392C6AF65CFF22D8089B9104B79C84A6D8B * value)
	{
		____listItemPrefab_12 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____listItemPrefab_12), (void*)value);
	}

	inline static int32_t get_offset_of__maxListItemEntries_13() { return static_cast<int32_t>(offsetof(HubWithAuthorizationSample_t19979B3628DD848910FD70011959F7CB115D5968, ____maxListItemEntries_13)); }
	inline int32_t get__maxListItemEntries_13() const { return ____maxListItemEntries_13; }
	inline int32_t* get_address_of__maxListItemEntries_13() { return &____maxListItemEntries_13; }
	inline void set__maxListItemEntries_13(int32_t value)
	{
		____maxListItemEntries_13 = value;
	}

	inline static int32_t get_offset_of__connectButton_14() { return static_cast<int32_t>(offsetof(HubWithAuthorizationSample_t19979B3628DD848910FD70011959F7CB115D5968, ____connectButton_14)); }
	inline Button_tA893FC15AB26E1439AC25BDCA7079530587BB65D * get__connectButton_14() const { return ____connectButton_14; }
	inline Button_tA893FC15AB26E1439AC25BDCA7079530587BB65D ** get_address_of__connectButton_14() { return &____connectButton_14; }
	inline void set__connectButton_14(Button_tA893FC15AB26E1439AC25BDCA7079530587BB65D * value)
	{
		____connectButton_14 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____connectButton_14), (void*)value);
	}

	inline static int32_t get_offset_of__closeButton_15() { return static_cast<int32_t>(offsetof(HubWithAuthorizationSample_t19979B3628DD848910FD70011959F7CB115D5968, ____closeButton_15)); }
	inline Button_tA893FC15AB26E1439AC25BDCA7079530587BB65D * get__closeButton_15() const { return ____closeButton_15; }
	inline Button_tA893FC15AB26E1439AC25BDCA7079530587BB65D ** get_address_of__closeButton_15() { return &____closeButton_15; }
	inline void set__closeButton_15(Button_tA893FC15AB26E1439AC25BDCA7079530587BB65D * value)
	{
		____closeButton_15 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____closeButton_15), (void*)value);
	}

	inline static int32_t get_offset_of_hub_16() { return static_cast<int32_t>(offsetof(HubWithAuthorizationSample_t19979B3628DD848910FD70011959F7CB115D5968, ___hub_16)); }
	inline HubConnection_t797D4973C9BBE6895D85BF743512ADA7D419736D * get_hub_16() const { return ___hub_16; }
	inline HubConnection_t797D4973C9BBE6895D85BF743512ADA7D419736D ** get_address_of_hub_16() { return &___hub_16; }
	inline void set_hub_16(HubConnection_t797D4973C9BBE6895D85BF743512ADA7D419736D * value)
	{
		___hub_16 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___hub_16), (void*)value);
	}
};


// BestHTTP.Examples.HubWithPreAuthorizationSample
struct HubWithPreAuthorizationSample_tDF799D602DC996BFF26C8E97175B4E64BF3807D8  : public SampleBase_tC30950FFCCCE8696B341569EEE364BF4C4DB7157
{
public:
	// System.String BestHTTP.Examples.HubWithPreAuthorizationSample::_hubPath
	String_t* ____hubPath_9;
	// System.String BestHTTP.Examples.HubWithPreAuthorizationSample::_jwtTokenPath
	String_t* ____jwtTokenPath_10;
	// UnityEngine.UI.ScrollRect BestHTTP.Examples.HubWithPreAuthorizationSample::_scrollRect
	ScrollRect_tB16156010F89FFDAAB2127CA878608FD91B9FBEA * ____scrollRect_11;
	// UnityEngine.RectTransform BestHTTP.Examples.HubWithPreAuthorizationSample::_contentRoot
	RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 * ____contentRoot_12;
	// BestHTTP.Examples.Helpers.TextListItem BestHTTP.Examples.HubWithPreAuthorizationSample::_listItemPrefab
	TextListItem_tC8FEA392C6AF65CFF22D8089B9104B79C84A6D8B * ____listItemPrefab_13;
	// System.Int32 BestHTTP.Examples.HubWithPreAuthorizationSample::_maxListItemEntries
	int32_t ____maxListItemEntries_14;
	// UnityEngine.UI.Button BestHTTP.Examples.HubWithPreAuthorizationSample::_connectButton
	Button_tA893FC15AB26E1439AC25BDCA7079530587BB65D * ____connectButton_15;
	// UnityEngine.UI.Button BestHTTP.Examples.HubWithPreAuthorizationSample::_closeButton
	Button_tA893FC15AB26E1439AC25BDCA7079530587BB65D * ____closeButton_16;
	// BestHTTP.SignalRCore.HubConnection BestHTTP.Examples.HubWithPreAuthorizationSample::hub
	HubConnection_t797D4973C9BBE6895D85BF743512ADA7D419736D * ___hub_17;

public:
	inline static int32_t get_offset_of__hubPath_9() { return static_cast<int32_t>(offsetof(HubWithPreAuthorizationSample_tDF799D602DC996BFF26C8E97175B4E64BF3807D8, ____hubPath_9)); }
	inline String_t* get__hubPath_9() const { return ____hubPath_9; }
	inline String_t** get_address_of__hubPath_9() { return &____hubPath_9; }
	inline void set__hubPath_9(String_t* value)
	{
		____hubPath_9 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____hubPath_9), (void*)value);
	}

	inline static int32_t get_offset_of__jwtTokenPath_10() { return static_cast<int32_t>(offsetof(HubWithPreAuthorizationSample_tDF799D602DC996BFF26C8E97175B4E64BF3807D8, ____jwtTokenPath_10)); }
	inline String_t* get__jwtTokenPath_10() const { return ____jwtTokenPath_10; }
	inline String_t** get_address_of__jwtTokenPath_10() { return &____jwtTokenPath_10; }
	inline void set__jwtTokenPath_10(String_t* value)
	{
		____jwtTokenPath_10 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____jwtTokenPath_10), (void*)value);
	}

	inline static int32_t get_offset_of__scrollRect_11() { return static_cast<int32_t>(offsetof(HubWithPreAuthorizationSample_tDF799D602DC996BFF26C8E97175B4E64BF3807D8, ____scrollRect_11)); }
	inline ScrollRect_tB16156010F89FFDAAB2127CA878608FD91B9FBEA * get__scrollRect_11() const { return ____scrollRect_11; }
	inline ScrollRect_tB16156010F89FFDAAB2127CA878608FD91B9FBEA ** get_address_of__scrollRect_11() { return &____scrollRect_11; }
	inline void set__scrollRect_11(ScrollRect_tB16156010F89FFDAAB2127CA878608FD91B9FBEA * value)
	{
		____scrollRect_11 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____scrollRect_11), (void*)value);
	}

	inline static int32_t get_offset_of__contentRoot_12() { return static_cast<int32_t>(offsetof(HubWithPreAuthorizationSample_tDF799D602DC996BFF26C8E97175B4E64BF3807D8, ____contentRoot_12)); }
	inline RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 * get__contentRoot_12() const { return ____contentRoot_12; }
	inline RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 ** get_address_of__contentRoot_12() { return &____contentRoot_12; }
	inline void set__contentRoot_12(RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 * value)
	{
		____contentRoot_12 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____contentRoot_12), (void*)value);
	}

	inline static int32_t get_offset_of__listItemPrefab_13() { return static_cast<int32_t>(offsetof(HubWithPreAuthorizationSample_tDF799D602DC996BFF26C8E97175B4E64BF3807D8, ____listItemPrefab_13)); }
	inline TextListItem_tC8FEA392C6AF65CFF22D8089B9104B79C84A6D8B * get__listItemPrefab_13() const { return ____listItemPrefab_13; }
	inline TextListItem_tC8FEA392C6AF65CFF22D8089B9104B79C84A6D8B ** get_address_of__listItemPrefab_13() { return &____listItemPrefab_13; }
	inline void set__listItemPrefab_13(TextListItem_tC8FEA392C6AF65CFF22D8089B9104B79C84A6D8B * value)
	{
		____listItemPrefab_13 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____listItemPrefab_13), (void*)value);
	}

	inline static int32_t get_offset_of__maxListItemEntries_14() { return static_cast<int32_t>(offsetof(HubWithPreAuthorizationSample_tDF799D602DC996BFF26C8E97175B4E64BF3807D8, ____maxListItemEntries_14)); }
	inline int32_t get__maxListItemEntries_14() const { return ____maxListItemEntries_14; }
	inline int32_t* get_address_of__maxListItemEntries_14() { return &____maxListItemEntries_14; }
	inline void set__maxListItemEntries_14(int32_t value)
	{
		____maxListItemEntries_14 = value;
	}

	inline static int32_t get_offset_of__connectButton_15() { return static_cast<int32_t>(offsetof(HubWithPreAuthorizationSample_tDF799D602DC996BFF26C8E97175B4E64BF3807D8, ____connectButton_15)); }
	inline Button_tA893FC15AB26E1439AC25BDCA7079530587BB65D * get__connectButton_15() const { return ____connectButton_15; }
	inline Button_tA893FC15AB26E1439AC25BDCA7079530587BB65D ** get_address_of__connectButton_15() { return &____connectButton_15; }
	inline void set__connectButton_15(Button_tA893FC15AB26E1439AC25BDCA7079530587BB65D * value)
	{
		____connectButton_15 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____connectButton_15), (void*)value);
	}

	inline static int32_t get_offset_of__closeButton_16() { return static_cast<int32_t>(offsetof(HubWithPreAuthorizationSample_tDF799D602DC996BFF26C8E97175B4E64BF3807D8, ____closeButton_16)); }
	inline Button_tA893FC15AB26E1439AC25BDCA7079530587BB65D * get__closeButton_16() const { return ____closeButton_16; }
	inline Button_tA893FC15AB26E1439AC25BDCA7079530587BB65D ** get_address_of__closeButton_16() { return &____closeButton_16; }
	inline void set__closeButton_16(Button_tA893FC15AB26E1439AC25BDCA7079530587BB65D * value)
	{
		____closeButton_16 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____closeButton_16), (void*)value);
	}

	inline static int32_t get_offset_of_hub_17() { return static_cast<int32_t>(offsetof(HubWithPreAuthorizationSample_tDF799D602DC996BFF26C8E97175B4E64BF3807D8, ___hub_17)); }
	inline HubConnection_t797D4973C9BBE6895D85BF743512ADA7D419736D * get_hub_17() const { return ___hub_17; }
	inline HubConnection_t797D4973C9BBE6895D85BF743512ADA7D419736D ** get_address_of_hub_17() { return &___hub_17; }
	inline void set_hub_17(HubConnection_t797D4973C9BBE6895D85BF743512ADA7D419736D * value)
	{
		___hub_17 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___hub_17), (void*)value);
	}
};


// UnityEngine.UI.ScrollRect
struct ScrollRect_tB16156010F89FFDAAB2127CA878608FD91B9FBEA  : public UIBehaviour_tD1C6E2D542222546D68510ECE74036EFBC3C3B0E
{
public:
	// UnityEngine.RectTransform UnityEngine.UI.ScrollRect::m_Content
	RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 * ___m_Content_4;
	// System.Boolean UnityEngine.UI.ScrollRect::m_Horizontal
	bool ___m_Horizontal_5;
	// System.Boolean UnityEngine.UI.ScrollRect::m_Vertical
	bool ___m_Vertical_6;
	// UnityEngine.UI.ScrollRect/MovementType UnityEngine.UI.ScrollRect::m_MovementType
	int32_t ___m_MovementType_7;
	// System.Single UnityEngine.UI.ScrollRect::m_Elasticity
	float ___m_Elasticity_8;
	// System.Boolean UnityEngine.UI.ScrollRect::m_Inertia
	bool ___m_Inertia_9;
	// System.Single UnityEngine.UI.ScrollRect::m_DecelerationRate
	float ___m_DecelerationRate_10;
	// System.Single UnityEngine.UI.ScrollRect::m_ScrollSensitivity
	float ___m_ScrollSensitivity_11;
	// UnityEngine.RectTransform UnityEngine.UI.ScrollRect::m_Viewport
	RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 * ___m_Viewport_12;
	// UnityEngine.UI.Scrollbar UnityEngine.UI.ScrollRect::m_HorizontalScrollbar
	Scrollbar_tECAC7FD315210FC856A3EC60AE1847A66AAF6C28 * ___m_HorizontalScrollbar_13;
	// UnityEngine.UI.Scrollbar UnityEngine.UI.ScrollRect::m_VerticalScrollbar
	Scrollbar_tECAC7FD315210FC856A3EC60AE1847A66AAF6C28 * ___m_VerticalScrollbar_14;
	// UnityEngine.UI.ScrollRect/ScrollbarVisibility UnityEngine.UI.ScrollRect::m_HorizontalScrollbarVisibility
	int32_t ___m_HorizontalScrollbarVisibility_15;
	// UnityEngine.UI.ScrollRect/ScrollbarVisibility UnityEngine.UI.ScrollRect::m_VerticalScrollbarVisibility
	int32_t ___m_VerticalScrollbarVisibility_16;
	// System.Single UnityEngine.UI.ScrollRect::m_HorizontalScrollbarSpacing
	float ___m_HorizontalScrollbarSpacing_17;
	// System.Single UnityEngine.UI.ScrollRect::m_VerticalScrollbarSpacing
	float ___m_VerticalScrollbarSpacing_18;
	// UnityEngine.UI.ScrollRect/ScrollRectEvent UnityEngine.UI.ScrollRect::m_OnValueChanged
	ScrollRectEvent_tA2F08EF8BB0B0B0F72DB8242DC5AB17BB0D1731E * ___m_OnValueChanged_19;
	// UnityEngine.Vector2 UnityEngine.UI.ScrollRect::m_PointerStartLocalCursor
	Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ___m_PointerStartLocalCursor_20;
	// UnityEngine.Vector2 UnityEngine.UI.ScrollRect::m_ContentStartPosition
	Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ___m_ContentStartPosition_21;
	// UnityEngine.RectTransform UnityEngine.UI.ScrollRect::m_ViewRect
	RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 * ___m_ViewRect_22;
	// UnityEngine.Bounds UnityEngine.UI.ScrollRect::m_ContentBounds
	Bounds_t0F1F36D4F7AF49524B3C2A2259594412A3D3AE37  ___m_ContentBounds_23;
	// UnityEngine.Bounds UnityEngine.UI.ScrollRect::m_ViewBounds
	Bounds_t0F1F36D4F7AF49524B3C2A2259594412A3D3AE37  ___m_ViewBounds_24;
	// UnityEngine.Vector2 UnityEngine.UI.ScrollRect::m_Velocity
	Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ___m_Velocity_25;
	// System.Boolean UnityEngine.UI.ScrollRect::m_Dragging
	bool ___m_Dragging_26;
	// System.Boolean UnityEngine.UI.ScrollRect::m_Scrolling
	bool ___m_Scrolling_27;
	// UnityEngine.Vector2 UnityEngine.UI.ScrollRect::m_PrevPosition
	Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ___m_PrevPosition_28;
	// UnityEngine.Bounds UnityEngine.UI.ScrollRect::m_PrevContentBounds
	Bounds_t0F1F36D4F7AF49524B3C2A2259594412A3D3AE37  ___m_PrevContentBounds_29;
	// UnityEngine.Bounds UnityEngine.UI.ScrollRect::m_PrevViewBounds
	Bounds_t0F1F36D4F7AF49524B3C2A2259594412A3D3AE37  ___m_PrevViewBounds_30;
	// System.Boolean UnityEngine.UI.ScrollRect::m_HasRebuiltLayout
	bool ___m_HasRebuiltLayout_31;
	// System.Boolean UnityEngine.UI.ScrollRect::m_HSliderExpand
	bool ___m_HSliderExpand_32;
	// System.Boolean UnityEngine.UI.ScrollRect::m_VSliderExpand
	bool ___m_VSliderExpand_33;
	// System.Single UnityEngine.UI.ScrollRect::m_HSliderHeight
	float ___m_HSliderHeight_34;
	// System.Single UnityEngine.UI.ScrollRect::m_VSliderWidth
	float ___m_VSliderWidth_35;
	// UnityEngine.RectTransform UnityEngine.UI.ScrollRect::m_Rect
	RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 * ___m_Rect_36;
	// UnityEngine.RectTransform UnityEngine.UI.ScrollRect::m_HorizontalScrollbarRect
	RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 * ___m_HorizontalScrollbarRect_37;
	// UnityEngine.RectTransform UnityEngine.UI.ScrollRect::m_VerticalScrollbarRect
	RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 * ___m_VerticalScrollbarRect_38;
	// UnityEngine.DrivenRectTransformTracker UnityEngine.UI.ScrollRect::m_Tracker
	DrivenRectTransformTracker_t7DAF937E47C63B899C7BA0E9B0F206AAB4D85AC2  ___m_Tracker_39;
	// UnityEngine.Vector3[] UnityEngine.UI.ScrollRect::m_Corners
	Vector3U5BU5D_t5FB88EAA33E46838BDC2ABDAEA3E8727491CB9E4* ___m_Corners_40;

public:
	inline static int32_t get_offset_of_m_Content_4() { return static_cast<int32_t>(offsetof(ScrollRect_tB16156010F89FFDAAB2127CA878608FD91B9FBEA, ___m_Content_4)); }
	inline RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 * get_m_Content_4() const { return ___m_Content_4; }
	inline RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 ** get_address_of_m_Content_4() { return &___m_Content_4; }
	inline void set_m_Content_4(RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 * value)
	{
		___m_Content_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Content_4), (void*)value);
	}

	inline static int32_t get_offset_of_m_Horizontal_5() { return static_cast<int32_t>(offsetof(ScrollRect_tB16156010F89FFDAAB2127CA878608FD91B9FBEA, ___m_Horizontal_5)); }
	inline bool get_m_Horizontal_5() const { return ___m_Horizontal_5; }
	inline bool* get_address_of_m_Horizontal_5() { return &___m_Horizontal_5; }
	inline void set_m_Horizontal_5(bool value)
	{
		___m_Horizontal_5 = value;
	}

	inline static int32_t get_offset_of_m_Vertical_6() { return static_cast<int32_t>(offsetof(ScrollRect_tB16156010F89FFDAAB2127CA878608FD91B9FBEA, ___m_Vertical_6)); }
	inline bool get_m_Vertical_6() const { return ___m_Vertical_6; }
	inline bool* get_address_of_m_Vertical_6() { return &___m_Vertical_6; }
	inline void set_m_Vertical_6(bool value)
	{
		___m_Vertical_6 = value;
	}

	inline static int32_t get_offset_of_m_MovementType_7() { return static_cast<int32_t>(offsetof(ScrollRect_tB16156010F89FFDAAB2127CA878608FD91B9FBEA, ___m_MovementType_7)); }
	inline int32_t get_m_MovementType_7() const { return ___m_MovementType_7; }
	inline int32_t* get_address_of_m_MovementType_7() { return &___m_MovementType_7; }
	inline void set_m_MovementType_7(int32_t value)
	{
		___m_MovementType_7 = value;
	}

	inline static int32_t get_offset_of_m_Elasticity_8() { return static_cast<int32_t>(offsetof(ScrollRect_tB16156010F89FFDAAB2127CA878608FD91B9FBEA, ___m_Elasticity_8)); }
	inline float get_m_Elasticity_8() const { return ___m_Elasticity_8; }
	inline float* get_address_of_m_Elasticity_8() { return &___m_Elasticity_8; }
	inline void set_m_Elasticity_8(float value)
	{
		___m_Elasticity_8 = value;
	}

	inline static int32_t get_offset_of_m_Inertia_9() { return static_cast<int32_t>(offsetof(ScrollRect_tB16156010F89FFDAAB2127CA878608FD91B9FBEA, ___m_Inertia_9)); }
	inline bool get_m_Inertia_9() const { return ___m_Inertia_9; }
	inline bool* get_address_of_m_Inertia_9() { return &___m_Inertia_9; }
	inline void set_m_Inertia_9(bool value)
	{
		___m_Inertia_9 = value;
	}

	inline static int32_t get_offset_of_m_DecelerationRate_10() { return static_cast<int32_t>(offsetof(ScrollRect_tB16156010F89FFDAAB2127CA878608FD91B9FBEA, ___m_DecelerationRate_10)); }
	inline float get_m_DecelerationRate_10() const { return ___m_DecelerationRate_10; }
	inline float* get_address_of_m_DecelerationRate_10() { return &___m_DecelerationRate_10; }
	inline void set_m_DecelerationRate_10(float value)
	{
		___m_DecelerationRate_10 = value;
	}

	inline static int32_t get_offset_of_m_ScrollSensitivity_11() { return static_cast<int32_t>(offsetof(ScrollRect_tB16156010F89FFDAAB2127CA878608FD91B9FBEA, ___m_ScrollSensitivity_11)); }
	inline float get_m_ScrollSensitivity_11() const { return ___m_ScrollSensitivity_11; }
	inline float* get_address_of_m_ScrollSensitivity_11() { return &___m_ScrollSensitivity_11; }
	inline void set_m_ScrollSensitivity_11(float value)
	{
		___m_ScrollSensitivity_11 = value;
	}

	inline static int32_t get_offset_of_m_Viewport_12() { return static_cast<int32_t>(offsetof(ScrollRect_tB16156010F89FFDAAB2127CA878608FD91B9FBEA, ___m_Viewport_12)); }
	inline RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 * get_m_Viewport_12() const { return ___m_Viewport_12; }
	inline RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 ** get_address_of_m_Viewport_12() { return &___m_Viewport_12; }
	inline void set_m_Viewport_12(RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 * value)
	{
		___m_Viewport_12 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Viewport_12), (void*)value);
	}

	inline static int32_t get_offset_of_m_HorizontalScrollbar_13() { return static_cast<int32_t>(offsetof(ScrollRect_tB16156010F89FFDAAB2127CA878608FD91B9FBEA, ___m_HorizontalScrollbar_13)); }
	inline Scrollbar_tECAC7FD315210FC856A3EC60AE1847A66AAF6C28 * get_m_HorizontalScrollbar_13() const { return ___m_HorizontalScrollbar_13; }
	inline Scrollbar_tECAC7FD315210FC856A3EC60AE1847A66AAF6C28 ** get_address_of_m_HorizontalScrollbar_13() { return &___m_HorizontalScrollbar_13; }
	inline void set_m_HorizontalScrollbar_13(Scrollbar_tECAC7FD315210FC856A3EC60AE1847A66AAF6C28 * value)
	{
		___m_HorizontalScrollbar_13 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_HorizontalScrollbar_13), (void*)value);
	}

	inline static int32_t get_offset_of_m_VerticalScrollbar_14() { return static_cast<int32_t>(offsetof(ScrollRect_tB16156010F89FFDAAB2127CA878608FD91B9FBEA, ___m_VerticalScrollbar_14)); }
	inline Scrollbar_tECAC7FD315210FC856A3EC60AE1847A66AAF6C28 * get_m_VerticalScrollbar_14() const { return ___m_VerticalScrollbar_14; }
	inline Scrollbar_tECAC7FD315210FC856A3EC60AE1847A66AAF6C28 ** get_address_of_m_VerticalScrollbar_14() { return &___m_VerticalScrollbar_14; }
	inline void set_m_VerticalScrollbar_14(Scrollbar_tECAC7FD315210FC856A3EC60AE1847A66AAF6C28 * value)
	{
		___m_VerticalScrollbar_14 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_VerticalScrollbar_14), (void*)value);
	}

	inline static int32_t get_offset_of_m_HorizontalScrollbarVisibility_15() { return static_cast<int32_t>(offsetof(ScrollRect_tB16156010F89FFDAAB2127CA878608FD91B9FBEA, ___m_HorizontalScrollbarVisibility_15)); }
	inline int32_t get_m_HorizontalScrollbarVisibility_15() const { return ___m_HorizontalScrollbarVisibility_15; }
	inline int32_t* get_address_of_m_HorizontalScrollbarVisibility_15() { return &___m_HorizontalScrollbarVisibility_15; }
	inline void set_m_HorizontalScrollbarVisibility_15(int32_t value)
	{
		___m_HorizontalScrollbarVisibility_15 = value;
	}

	inline static int32_t get_offset_of_m_VerticalScrollbarVisibility_16() { return static_cast<int32_t>(offsetof(ScrollRect_tB16156010F89FFDAAB2127CA878608FD91B9FBEA, ___m_VerticalScrollbarVisibility_16)); }
	inline int32_t get_m_VerticalScrollbarVisibility_16() const { return ___m_VerticalScrollbarVisibility_16; }
	inline int32_t* get_address_of_m_VerticalScrollbarVisibility_16() { return &___m_VerticalScrollbarVisibility_16; }
	inline void set_m_VerticalScrollbarVisibility_16(int32_t value)
	{
		___m_VerticalScrollbarVisibility_16 = value;
	}

	inline static int32_t get_offset_of_m_HorizontalScrollbarSpacing_17() { return static_cast<int32_t>(offsetof(ScrollRect_tB16156010F89FFDAAB2127CA878608FD91B9FBEA, ___m_HorizontalScrollbarSpacing_17)); }
	inline float get_m_HorizontalScrollbarSpacing_17() const { return ___m_HorizontalScrollbarSpacing_17; }
	inline float* get_address_of_m_HorizontalScrollbarSpacing_17() { return &___m_HorizontalScrollbarSpacing_17; }
	inline void set_m_HorizontalScrollbarSpacing_17(float value)
	{
		___m_HorizontalScrollbarSpacing_17 = value;
	}

	inline static int32_t get_offset_of_m_VerticalScrollbarSpacing_18() { return static_cast<int32_t>(offsetof(ScrollRect_tB16156010F89FFDAAB2127CA878608FD91B9FBEA, ___m_VerticalScrollbarSpacing_18)); }
	inline float get_m_VerticalScrollbarSpacing_18() const { return ___m_VerticalScrollbarSpacing_18; }
	inline float* get_address_of_m_VerticalScrollbarSpacing_18() { return &___m_VerticalScrollbarSpacing_18; }
	inline void set_m_VerticalScrollbarSpacing_18(float value)
	{
		___m_VerticalScrollbarSpacing_18 = value;
	}

	inline static int32_t get_offset_of_m_OnValueChanged_19() { return static_cast<int32_t>(offsetof(ScrollRect_tB16156010F89FFDAAB2127CA878608FD91B9FBEA, ___m_OnValueChanged_19)); }
	inline ScrollRectEvent_tA2F08EF8BB0B0B0F72DB8242DC5AB17BB0D1731E * get_m_OnValueChanged_19() const { return ___m_OnValueChanged_19; }
	inline ScrollRectEvent_tA2F08EF8BB0B0B0F72DB8242DC5AB17BB0D1731E ** get_address_of_m_OnValueChanged_19() { return &___m_OnValueChanged_19; }
	inline void set_m_OnValueChanged_19(ScrollRectEvent_tA2F08EF8BB0B0B0F72DB8242DC5AB17BB0D1731E * value)
	{
		___m_OnValueChanged_19 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_OnValueChanged_19), (void*)value);
	}

	inline static int32_t get_offset_of_m_PointerStartLocalCursor_20() { return static_cast<int32_t>(offsetof(ScrollRect_tB16156010F89FFDAAB2127CA878608FD91B9FBEA, ___m_PointerStartLocalCursor_20)); }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  get_m_PointerStartLocalCursor_20() const { return ___m_PointerStartLocalCursor_20; }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 * get_address_of_m_PointerStartLocalCursor_20() { return &___m_PointerStartLocalCursor_20; }
	inline void set_m_PointerStartLocalCursor_20(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  value)
	{
		___m_PointerStartLocalCursor_20 = value;
	}

	inline static int32_t get_offset_of_m_ContentStartPosition_21() { return static_cast<int32_t>(offsetof(ScrollRect_tB16156010F89FFDAAB2127CA878608FD91B9FBEA, ___m_ContentStartPosition_21)); }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  get_m_ContentStartPosition_21() const { return ___m_ContentStartPosition_21; }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 * get_address_of_m_ContentStartPosition_21() { return &___m_ContentStartPosition_21; }
	inline void set_m_ContentStartPosition_21(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  value)
	{
		___m_ContentStartPosition_21 = value;
	}

	inline static int32_t get_offset_of_m_ViewRect_22() { return static_cast<int32_t>(offsetof(ScrollRect_tB16156010F89FFDAAB2127CA878608FD91B9FBEA, ___m_ViewRect_22)); }
	inline RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 * get_m_ViewRect_22() const { return ___m_ViewRect_22; }
	inline RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 ** get_address_of_m_ViewRect_22() { return &___m_ViewRect_22; }
	inline void set_m_ViewRect_22(RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 * value)
	{
		___m_ViewRect_22 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_ViewRect_22), (void*)value);
	}

	inline static int32_t get_offset_of_m_ContentBounds_23() { return static_cast<int32_t>(offsetof(ScrollRect_tB16156010F89FFDAAB2127CA878608FD91B9FBEA, ___m_ContentBounds_23)); }
	inline Bounds_t0F1F36D4F7AF49524B3C2A2259594412A3D3AE37  get_m_ContentBounds_23() const { return ___m_ContentBounds_23; }
	inline Bounds_t0F1F36D4F7AF49524B3C2A2259594412A3D3AE37 * get_address_of_m_ContentBounds_23() { return &___m_ContentBounds_23; }
	inline void set_m_ContentBounds_23(Bounds_t0F1F36D4F7AF49524B3C2A2259594412A3D3AE37  value)
	{
		___m_ContentBounds_23 = value;
	}

	inline static int32_t get_offset_of_m_ViewBounds_24() { return static_cast<int32_t>(offsetof(ScrollRect_tB16156010F89FFDAAB2127CA878608FD91B9FBEA, ___m_ViewBounds_24)); }
	inline Bounds_t0F1F36D4F7AF49524B3C2A2259594412A3D3AE37  get_m_ViewBounds_24() const { return ___m_ViewBounds_24; }
	inline Bounds_t0F1F36D4F7AF49524B3C2A2259594412A3D3AE37 * get_address_of_m_ViewBounds_24() { return &___m_ViewBounds_24; }
	inline void set_m_ViewBounds_24(Bounds_t0F1F36D4F7AF49524B3C2A2259594412A3D3AE37  value)
	{
		___m_ViewBounds_24 = value;
	}

	inline static int32_t get_offset_of_m_Velocity_25() { return static_cast<int32_t>(offsetof(ScrollRect_tB16156010F89FFDAAB2127CA878608FD91B9FBEA, ___m_Velocity_25)); }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  get_m_Velocity_25() const { return ___m_Velocity_25; }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 * get_address_of_m_Velocity_25() { return &___m_Velocity_25; }
	inline void set_m_Velocity_25(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  value)
	{
		___m_Velocity_25 = value;
	}

	inline static int32_t get_offset_of_m_Dragging_26() { return static_cast<int32_t>(offsetof(ScrollRect_tB16156010F89FFDAAB2127CA878608FD91B9FBEA, ___m_Dragging_26)); }
	inline bool get_m_Dragging_26() const { return ___m_Dragging_26; }
	inline bool* get_address_of_m_Dragging_26() { return &___m_Dragging_26; }
	inline void set_m_Dragging_26(bool value)
	{
		___m_Dragging_26 = value;
	}

	inline static int32_t get_offset_of_m_Scrolling_27() { return static_cast<int32_t>(offsetof(ScrollRect_tB16156010F89FFDAAB2127CA878608FD91B9FBEA, ___m_Scrolling_27)); }
	inline bool get_m_Scrolling_27() const { return ___m_Scrolling_27; }
	inline bool* get_address_of_m_Scrolling_27() { return &___m_Scrolling_27; }
	inline void set_m_Scrolling_27(bool value)
	{
		___m_Scrolling_27 = value;
	}

	inline static int32_t get_offset_of_m_PrevPosition_28() { return static_cast<int32_t>(offsetof(ScrollRect_tB16156010F89FFDAAB2127CA878608FD91B9FBEA, ___m_PrevPosition_28)); }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  get_m_PrevPosition_28() const { return ___m_PrevPosition_28; }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 * get_address_of_m_PrevPosition_28() { return &___m_PrevPosition_28; }
	inline void set_m_PrevPosition_28(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  value)
	{
		___m_PrevPosition_28 = value;
	}

	inline static int32_t get_offset_of_m_PrevContentBounds_29() { return static_cast<int32_t>(offsetof(ScrollRect_tB16156010F89FFDAAB2127CA878608FD91B9FBEA, ___m_PrevContentBounds_29)); }
	inline Bounds_t0F1F36D4F7AF49524B3C2A2259594412A3D3AE37  get_m_PrevContentBounds_29() const { return ___m_PrevContentBounds_29; }
	inline Bounds_t0F1F36D4F7AF49524B3C2A2259594412A3D3AE37 * get_address_of_m_PrevContentBounds_29() { return &___m_PrevContentBounds_29; }
	inline void set_m_PrevContentBounds_29(Bounds_t0F1F36D4F7AF49524B3C2A2259594412A3D3AE37  value)
	{
		___m_PrevContentBounds_29 = value;
	}

	inline static int32_t get_offset_of_m_PrevViewBounds_30() { return static_cast<int32_t>(offsetof(ScrollRect_tB16156010F89FFDAAB2127CA878608FD91B9FBEA, ___m_PrevViewBounds_30)); }
	inline Bounds_t0F1F36D4F7AF49524B3C2A2259594412A3D3AE37  get_m_PrevViewBounds_30() const { return ___m_PrevViewBounds_30; }
	inline Bounds_t0F1F36D4F7AF49524B3C2A2259594412A3D3AE37 * get_address_of_m_PrevViewBounds_30() { return &___m_PrevViewBounds_30; }
	inline void set_m_PrevViewBounds_30(Bounds_t0F1F36D4F7AF49524B3C2A2259594412A3D3AE37  value)
	{
		___m_PrevViewBounds_30 = value;
	}

	inline static int32_t get_offset_of_m_HasRebuiltLayout_31() { return static_cast<int32_t>(offsetof(ScrollRect_tB16156010F89FFDAAB2127CA878608FD91B9FBEA, ___m_HasRebuiltLayout_31)); }
	inline bool get_m_HasRebuiltLayout_31() const { return ___m_HasRebuiltLayout_31; }
	inline bool* get_address_of_m_HasRebuiltLayout_31() { return &___m_HasRebuiltLayout_31; }
	inline void set_m_HasRebuiltLayout_31(bool value)
	{
		___m_HasRebuiltLayout_31 = value;
	}

	inline static int32_t get_offset_of_m_HSliderExpand_32() { return static_cast<int32_t>(offsetof(ScrollRect_tB16156010F89FFDAAB2127CA878608FD91B9FBEA, ___m_HSliderExpand_32)); }
	inline bool get_m_HSliderExpand_32() const { return ___m_HSliderExpand_32; }
	inline bool* get_address_of_m_HSliderExpand_32() { return &___m_HSliderExpand_32; }
	inline void set_m_HSliderExpand_32(bool value)
	{
		___m_HSliderExpand_32 = value;
	}

	inline static int32_t get_offset_of_m_VSliderExpand_33() { return static_cast<int32_t>(offsetof(ScrollRect_tB16156010F89FFDAAB2127CA878608FD91B9FBEA, ___m_VSliderExpand_33)); }
	inline bool get_m_VSliderExpand_33() const { return ___m_VSliderExpand_33; }
	inline bool* get_address_of_m_VSliderExpand_33() { return &___m_VSliderExpand_33; }
	inline void set_m_VSliderExpand_33(bool value)
	{
		___m_VSliderExpand_33 = value;
	}

	inline static int32_t get_offset_of_m_HSliderHeight_34() { return static_cast<int32_t>(offsetof(ScrollRect_tB16156010F89FFDAAB2127CA878608FD91B9FBEA, ___m_HSliderHeight_34)); }
	inline float get_m_HSliderHeight_34() const { return ___m_HSliderHeight_34; }
	inline float* get_address_of_m_HSliderHeight_34() { return &___m_HSliderHeight_34; }
	inline void set_m_HSliderHeight_34(float value)
	{
		___m_HSliderHeight_34 = value;
	}

	inline static int32_t get_offset_of_m_VSliderWidth_35() { return static_cast<int32_t>(offsetof(ScrollRect_tB16156010F89FFDAAB2127CA878608FD91B9FBEA, ___m_VSliderWidth_35)); }
	inline float get_m_VSliderWidth_35() const { return ___m_VSliderWidth_35; }
	inline float* get_address_of_m_VSliderWidth_35() { return &___m_VSliderWidth_35; }
	inline void set_m_VSliderWidth_35(float value)
	{
		___m_VSliderWidth_35 = value;
	}

	inline static int32_t get_offset_of_m_Rect_36() { return static_cast<int32_t>(offsetof(ScrollRect_tB16156010F89FFDAAB2127CA878608FD91B9FBEA, ___m_Rect_36)); }
	inline RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 * get_m_Rect_36() const { return ___m_Rect_36; }
	inline RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 ** get_address_of_m_Rect_36() { return &___m_Rect_36; }
	inline void set_m_Rect_36(RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 * value)
	{
		___m_Rect_36 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Rect_36), (void*)value);
	}

	inline static int32_t get_offset_of_m_HorizontalScrollbarRect_37() { return static_cast<int32_t>(offsetof(ScrollRect_tB16156010F89FFDAAB2127CA878608FD91B9FBEA, ___m_HorizontalScrollbarRect_37)); }
	inline RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 * get_m_HorizontalScrollbarRect_37() const { return ___m_HorizontalScrollbarRect_37; }
	inline RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 ** get_address_of_m_HorizontalScrollbarRect_37() { return &___m_HorizontalScrollbarRect_37; }
	inline void set_m_HorizontalScrollbarRect_37(RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 * value)
	{
		___m_HorizontalScrollbarRect_37 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_HorizontalScrollbarRect_37), (void*)value);
	}

	inline static int32_t get_offset_of_m_VerticalScrollbarRect_38() { return static_cast<int32_t>(offsetof(ScrollRect_tB16156010F89FFDAAB2127CA878608FD91B9FBEA, ___m_VerticalScrollbarRect_38)); }
	inline RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 * get_m_VerticalScrollbarRect_38() const { return ___m_VerticalScrollbarRect_38; }
	inline RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 ** get_address_of_m_VerticalScrollbarRect_38() { return &___m_VerticalScrollbarRect_38; }
	inline void set_m_VerticalScrollbarRect_38(RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 * value)
	{
		___m_VerticalScrollbarRect_38 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_VerticalScrollbarRect_38), (void*)value);
	}

	inline static int32_t get_offset_of_m_Tracker_39() { return static_cast<int32_t>(offsetof(ScrollRect_tB16156010F89FFDAAB2127CA878608FD91B9FBEA, ___m_Tracker_39)); }
	inline DrivenRectTransformTracker_t7DAF937E47C63B899C7BA0E9B0F206AAB4D85AC2  get_m_Tracker_39() const { return ___m_Tracker_39; }
	inline DrivenRectTransformTracker_t7DAF937E47C63B899C7BA0E9B0F206AAB4D85AC2 * get_address_of_m_Tracker_39() { return &___m_Tracker_39; }
	inline void set_m_Tracker_39(DrivenRectTransformTracker_t7DAF937E47C63B899C7BA0E9B0F206AAB4D85AC2  value)
	{
		___m_Tracker_39 = value;
	}

	inline static int32_t get_offset_of_m_Corners_40() { return static_cast<int32_t>(offsetof(ScrollRect_tB16156010F89FFDAAB2127CA878608FD91B9FBEA, ___m_Corners_40)); }
	inline Vector3U5BU5D_t5FB88EAA33E46838BDC2ABDAEA3E8727491CB9E4* get_m_Corners_40() const { return ___m_Corners_40; }
	inline Vector3U5BU5D_t5FB88EAA33E46838BDC2ABDAEA3E8727491CB9E4** get_address_of_m_Corners_40() { return &___m_Corners_40; }
	inline void set_m_Corners_40(Vector3U5BU5D_t5FB88EAA33E46838BDC2ABDAEA3E8727491CB9E4* value)
	{
		___m_Corners_40 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Corners_40), (void*)value);
	}
};


// UnityEngine.UI.Selectable
struct Selectable_t34088A3677CC9D344F81B0D91999D8C5963D7DBD  : public UIBehaviour_tD1C6E2D542222546D68510ECE74036EFBC3C3B0E
{
public:
	// System.Boolean UnityEngine.UI.Selectable::m_EnableCalled
	bool ___m_EnableCalled_6;
	// UnityEngine.UI.Navigation UnityEngine.UI.Selectable::m_Navigation
	Navigation_t1CF0FFB22C0357CD64714FB7A40A275F899D363A  ___m_Navigation_7;
	// UnityEngine.UI.Selectable/Transition UnityEngine.UI.Selectable::m_Transition
	int32_t ___m_Transition_8;
	// UnityEngine.UI.ColorBlock UnityEngine.UI.Selectable::m_Colors
	ColorBlock_t04DFBB97B4772D2E00FD17ED2E3E6590E6916955  ___m_Colors_9;
	// UnityEngine.UI.SpriteState UnityEngine.UI.Selectable::m_SpriteState
	SpriteState_t9024961148433175CE2F3D9E8E9239A8B1CAB15E  ___m_SpriteState_10;
	// UnityEngine.UI.AnimationTriggers UnityEngine.UI.Selectable::m_AnimationTriggers
	AnimationTriggers_tF38CA7FA631709E096B57D732668D86081F44C11 * ___m_AnimationTriggers_11;
	// System.Boolean UnityEngine.UI.Selectable::m_Interactable
	bool ___m_Interactable_12;
	// UnityEngine.UI.Graphic UnityEngine.UI.Selectable::m_TargetGraphic
	Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24 * ___m_TargetGraphic_13;
	// System.Boolean UnityEngine.UI.Selectable::m_GroupsAllowInteraction
	bool ___m_GroupsAllowInteraction_14;
	// System.Int32 UnityEngine.UI.Selectable::m_CurrentIndex
	int32_t ___m_CurrentIndex_15;
	// System.Boolean UnityEngine.UI.Selectable::<isPointerInside>k__BackingField
	bool ___U3CisPointerInsideU3Ek__BackingField_16;
	// System.Boolean UnityEngine.UI.Selectable::<isPointerDown>k__BackingField
	bool ___U3CisPointerDownU3Ek__BackingField_17;
	// System.Boolean UnityEngine.UI.Selectable::<hasSelection>k__BackingField
	bool ___U3ChasSelectionU3Ek__BackingField_18;
	// System.Collections.Generic.List`1<UnityEngine.CanvasGroup> UnityEngine.UI.Selectable::m_CanvasGroupCache
	List_1_t34AA4AF4E7352129CA58045901530E41445AC16D * ___m_CanvasGroupCache_19;

public:
	inline static int32_t get_offset_of_m_EnableCalled_6() { return static_cast<int32_t>(offsetof(Selectable_t34088A3677CC9D344F81B0D91999D8C5963D7DBD, ___m_EnableCalled_6)); }
	inline bool get_m_EnableCalled_6() const { return ___m_EnableCalled_6; }
	inline bool* get_address_of_m_EnableCalled_6() { return &___m_EnableCalled_6; }
	inline void set_m_EnableCalled_6(bool value)
	{
		___m_EnableCalled_6 = value;
	}

	inline static int32_t get_offset_of_m_Navigation_7() { return static_cast<int32_t>(offsetof(Selectable_t34088A3677CC9D344F81B0D91999D8C5963D7DBD, ___m_Navigation_7)); }
	inline Navigation_t1CF0FFB22C0357CD64714FB7A40A275F899D363A  get_m_Navigation_7() const { return ___m_Navigation_7; }
	inline Navigation_t1CF0FFB22C0357CD64714FB7A40A275F899D363A * get_address_of_m_Navigation_7() { return &___m_Navigation_7; }
	inline void set_m_Navigation_7(Navigation_t1CF0FFB22C0357CD64714FB7A40A275F899D363A  value)
	{
		___m_Navigation_7 = value;
		Il2CppCodeGenWriteBarrier((void**)&(((&___m_Navigation_7))->___m_SelectOnUp_2), (void*)NULL);
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&(((&___m_Navigation_7))->___m_SelectOnDown_3), (void*)NULL);
		#endif
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&(((&___m_Navigation_7))->___m_SelectOnLeft_4), (void*)NULL);
		#endif
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&(((&___m_Navigation_7))->___m_SelectOnRight_5), (void*)NULL);
		#endif
	}

	inline static int32_t get_offset_of_m_Transition_8() { return static_cast<int32_t>(offsetof(Selectable_t34088A3677CC9D344F81B0D91999D8C5963D7DBD, ___m_Transition_8)); }
	inline int32_t get_m_Transition_8() const { return ___m_Transition_8; }
	inline int32_t* get_address_of_m_Transition_8() { return &___m_Transition_8; }
	inline void set_m_Transition_8(int32_t value)
	{
		___m_Transition_8 = value;
	}

	inline static int32_t get_offset_of_m_Colors_9() { return static_cast<int32_t>(offsetof(Selectable_t34088A3677CC9D344F81B0D91999D8C5963D7DBD, ___m_Colors_9)); }
	inline ColorBlock_t04DFBB97B4772D2E00FD17ED2E3E6590E6916955  get_m_Colors_9() const { return ___m_Colors_9; }
	inline ColorBlock_t04DFBB97B4772D2E00FD17ED2E3E6590E6916955 * get_address_of_m_Colors_9() { return &___m_Colors_9; }
	inline void set_m_Colors_9(ColorBlock_t04DFBB97B4772D2E00FD17ED2E3E6590E6916955  value)
	{
		___m_Colors_9 = value;
	}

	inline static int32_t get_offset_of_m_SpriteState_10() { return static_cast<int32_t>(offsetof(Selectable_t34088A3677CC9D344F81B0D91999D8C5963D7DBD, ___m_SpriteState_10)); }
	inline SpriteState_t9024961148433175CE2F3D9E8E9239A8B1CAB15E  get_m_SpriteState_10() const { return ___m_SpriteState_10; }
	inline SpriteState_t9024961148433175CE2F3D9E8E9239A8B1CAB15E * get_address_of_m_SpriteState_10() { return &___m_SpriteState_10; }
	inline void set_m_SpriteState_10(SpriteState_t9024961148433175CE2F3D9E8E9239A8B1CAB15E  value)
	{
		___m_SpriteState_10 = value;
		Il2CppCodeGenWriteBarrier((void**)&(((&___m_SpriteState_10))->___m_HighlightedSprite_0), (void*)NULL);
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&(((&___m_SpriteState_10))->___m_PressedSprite_1), (void*)NULL);
		#endif
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&(((&___m_SpriteState_10))->___m_SelectedSprite_2), (void*)NULL);
		#endif
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&(((&___m_SpriteState_10))->___m_DisabledSprite_3), (void*)NULL);
		#endif
	}

	inline static int32_t get_offset_of_m_AnimationTriggers_11() { return static_cast<int32_t>(offsetof(Selectable_t34088A3677CC9D344F81B0D91999D8C5963D7DBD, ___m_AnimationTriggers_11)); }
	inline AnimationTriggers_tF38CA7FA631709E096B57D732668D86081F44C11 * get_m_AnimationTriggers_11() const { return ___m_AnimationTriggers_11; }
	inline AnimationTriggers_tF38CA7FA631709E096B57D732668D86081F44C11 ** get_address_of_m_AnimationTriggers_11() { return &___m_AnimationTriggers_11; }
	inline void set_m_AnimationTriggers_11(AnimationTriggers_tF38CA7FA631709E096B57D732668D86081F44C11 * value)
	{
		___m_AnimationTriggers_11 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_AnimationTriggers_11), (void*)value);
	}

	inline static int32_t get_offset_of_m_Interactable_12() { return static_cast<int32_t>(offsetof(Selectable_t34088A3677CC9D344F81B0D91999D8C5963D7DBD, ___m_Interactable_12)); }
	inline bool get_m_Interactable_12() const { return ___m_Interactable_12; }
	inline bool* get_address_of_m_Interactable_12() { return &___m_Interactable_12; }
	inline void set_m_Interactable_12(bool value)
	{
		___m_Interactable_12 = value;
	}

	inline static int32_t get_offset_of_m_TargetGraphic_13() { return static_cast<int32_t>(offsetof(Selectable_t34088A3677CC9D344F81B0D91999D8C5963D7DBD, ___m_TargetGraphic_13)); }
	inline Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24 * get_m_TargetGraphic_13() const { return ___m_TargetGraphic_13; }
	inline Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24 ** get_address_of_m_TargetGraphic_13() { return &___m_TargetGraphic_13; }
	inline void set_m_TargetGraphic_13(Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24 * value)
	{
		___m_TargetGraphic_13 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_TargetGraphic_13), (void*)value);
	}

	inline static int32_t get_offset_of_m_GroupsAllowInteraction_14() { return static_cast<int32_t>(offsetof(Selectable_t34088A3677CC9D344F81B0D91999D8C5963D7DBD, ___m_GroupsAllowInteraction_14)); }
	inline bool get_m_GroupsAllowInteraction_14() const { return ___m_GroupsAllowInteraction_14; }
	inline bool* get_address_of_m_GroupsAllowInteraction_14() { return &___m_GroupsAllowInteraction_14; }
	inline void set_m_GroupsAllowInteraction_14(bool value)
	{
		___m_GroupsAllowInteraction_14 = value;
	}

	inline static int32_t get_offset_of_m_CurrentIndex_15() { return static_cast<int32_t>(offsetof(Selectable_t34088A3677CC9D344F81B0D91999D8C5963D7DBD, ___m_CurrentIndex_15)); }
	inline int32_t get_m_CurrentIndex_15() const { return ___m_CurrentIndex_15; }
	inline int32_t* get_address_of_m_CurrentIndex_15() { return &___m_CurrentIndex_15; }
	inline void set_m_CurrentIndex_15(int32_t value)
	{
		___m_CurrentIndex_15 = value;
	}

	inline static int32_t get_offset_of_U3CisPointerInsideU3Ek__BackingField_16() { return static_cast<int32_t>(offsetof(Selectable_t34088A3677CC9D344F81B0D91999D8C5963D7DBD, ___U3CisPointerInsideU3Ek__BackingField_16)); }
	inline bool get_U3CisPointerInsideU3Ek__BackingField_16() const { return ___U3CisPointerInsideU3Ek__BackingField_16; }
	inline bool* get_address_of_U3CisPointerInsideU3Ek__BackingField_16() { return &___U3CisPointerInsideU3Ek__BackingField_16; }
	inline void set_U3CisPointerInsideU3Ek__BackingField_16(bool value)
	{
		___U3CisPointerInsideU3Ek__BackingField_16 = value;
	}

	inline static int32_t get_offset_of_U3CisPointerDownU3Ek__BackingField_17() { return static_cast<int32_t>(offsetof(Selectable_t34088A3677CC9D344F81B0D91999D8C5963D7DBD, ___U3CisPointerDownU3Ek__BackingField_17)); }
	inline bool get_U3CisPointerDownU3Ek__BackingField_17() const { return ___U3CisPointerDownU3Ek__BackingField_17; }
	inline bool* get_address_of_U3CisPointerDownU3Ek__BackingField_17() { return &___U3CisPointerDownU3Ek__BackingField_17; }
	inline void set_U3CisPointerDownU3Ek__BackingField_17(bool value)
	{
		___U3CisPointerDownU3Ek__BackingField_17 = value;
	}

	inline static int32_t get_offset_of_U3ChasSelectionU3Ek__BackingField_18() { return static_cast<int32_t>(offsetof(Selectable_t34088A3677CC9D344F81B0D91999D8C5963D7DBD, ___U3ChasSelectionU3Ek__BackingField_18)); }
	inline bool get_U3ChasSelectionU3Ek__BackingField_18() const { return ___U3ChasSelectionU3Ek__BackingField_18; }
	inline bool* get_address_of_U3ChasSelectionU3Ek__BackingField_18() { return &___U3ChasSelectionU3Ek__BackingField_18; }
	inline void set_U3ChasSelectionU3Ek__BackingField_18(bool value)
	{
		___U3ChasSelectionU3Ek__BackingField_18 = value;
	}

	inline static int32_t get_offset_of_m_CanvasGroupCache_19() { return static_cast<int32_t>(offsetof(Selectable_t34088A3677CC9D344F81B0D91999D8C5963D7DBD, ___m_CanvasGroupCache_19)); }
	inline List_1_t34AA4AF4E7352129CA58045901530E41445AC16D * get_m_CanvasGroupCache_19() const { return ___m_CanvasGroupCache_19; }
	inline List_1_t34AA4AF4E7352129CA58045901530E41445AC16D ** get_address_of_m_CanvasGroupCache_19() { return &___m_CanvasGroupCache_19; }
	inline void set_m_CanvasGroupCache_19(List_1_t34AA4AF4E7352129CA58045901530E41445AC16D * value)
	{
		___m_CanvasGroupCache_19 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_CanvasGroupCache_19), (void*)value);
	}
};

struct Selectable_t34088A3677CC9D344F81B0D91999D8C5963D7DBD_StaticFields
{
public:
	// UnityEngine.UI.Selectable[] UnityEngine.UI.Selectable::s_Selectables
	SelectableU5BU5D_tECF9F5BDBF0652A937D18F10C883EFDAE2E62535* ___s_Selectables_4;
	// System.Int32 UnityEngine.UI.Selectable::s_SelectableCount
	int32_t ___s_SelectableCount_5;

public:
	inline static int32_t get_offset_of_s_Selectables_4() { return static_cast<int32_t>(offsetof(Selectable_t34088A3677CC9D344F81B0D91999D8C5963D7DBD_StaticFields, ___s_Selectables_4)); }
	inline SelectableU5BU5D_tECF9F5BDBF0652A937D18F10C883EFDAE2E62535* get_s_Selectables_4() const { return ___s_Selectables_4; }
	inline SelectableU5BU5D_tECF9F5BDBF0652A937D18F10C883EFDAE2E62535** get_address_of_s_Selectables_4() { return &___s_Selectables_4; }
	inline void set_s_Selectables_4(SelectableU5BU5D_tECF9F5BDBF0652A937D18F10C883EFDAE2E62535* value)
	{
		___s_Selectables_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___s_Selectables_4), (void*)value);
	}

	inline static int32_t get_offset_of_s_SelectableCount_5() { return static_cast<int32_t>(offsetof(Selectable_t34088A3677CC9D344F81B0D91999D8C5963D7DBD_StaticFields, ___s_SelectableCount_5)); }
	inline int32_t get_s_SelectableCount_5() const { return ___s_SelectableCount_5; }
	inline int32_t* get_address_of_s_SelectableCount_5() { return &___s_SelectableCount_5; }
	inline void set_s_SelectableCount_5(int32_t value)
	{
		___s_SelectableCount_5 = value;
	}
};


// UnityEngine.UI.Button
struct Button_tA893FC15AB26E1439AC25BDCA7079530587BB65D  : public Selectable_t34088A3677CC9D344F81B0D91999D8C5963D7DBD
{
public:
	// UnityEngine.UI.Button/ButtonClickedEvent UnityEngine.UI.Button::m_OnClick
	ButtonClickedEvent_tE6D6D94ED8100451CF00D2BED1FB2253F37BB14F * ___m_OnClick_20;

public:
	inline static int32_t get_offset_of_m_OnClick_20() { return static_cast<int32_t>(offsetof(Button_tA893FC15AB26E1439AC25BDCA7079530587BB65D, ___m_OnClick_20)); }
	inline ButtonClickedEvent_tE6D6D94ED8100451CF00D2BED1FB2253F37BB14F * get_m_OnClick_20() const { return ___m_OnClick_20; }
	inline ButtonClickedEvent_tE6D6D94ED8100451CF00D2BED1FB2253F37BB14F ** get_address_of_m_OnClick_20() { return &___m_OnClick_20; }
	inline void set_m_OnClick_20(ButtonClickedEvent_tE6D6D94ED8100451CF00D2BED1FB2253F37BB14F * value)
	{
		___m_OnClick_20 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_OnClick_20), (void*)value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// System.Object[]
struct ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) RuntimeObject * m_Items[1];

public:
	inline RuntimeObject * GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline RuntimeObject ** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, RuntimeObject * value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier((void**)m_Items + index, (void*)value);
	}
	inline RuntimeObject * GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline RuntimeObject ** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, RuntimeObject * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier((void**)m_Items + index, (void*)value);
	}
};
// BestHTTP.Connections.HTTP2.HuffmanEncoder/TreeNode[]
struct TreeNodeU5BU5D_t5032BD830284F31E0A94DEB0072DB2B07126BABD  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F  m_Items[1];

public:
	inline TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F  GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F * GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F  value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
	}
	inline TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F  GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F * GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F  value)
	{
		m_Items[index] = value;
	}
};
// BestHTTP.Connections.HTTP2.HuffmanEncoder/TableEntry[]
struct TableEntryU5BU5D_tB3F4A3142B2B8D2FFFA48D2697C81F352A3D4A94  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) TableEntry_tDFCA5A567802D72C4E23BFC6CEA14C32F52229FF  m_Items[1];

public:
	inline TableEntry_tDFCA5A567802D72C4E23BFC6CEA14C32F52229FF  GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline TableEntry_tDFCA5A567802D72C4E23BFC6CEA14C32F52229FF * GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, TableEntry_tDFCA5A567802D72C4E23BFC6CEA14C32F52229FF  value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
	}
	inline TableEntry_tDFCA5A567802D72C4E23BFC6CEA14C32F52229FF  GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline TableEntry_tDFCA5A567802D72C4E23BFC6CEA14C32F52229FF * GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, TableEntry_tDFCA5A567802D72C4E23BFC6CEA14C32F52229FF  value)
	{
		m_Items[index] = value;
	}
};


// System.Void System.Action`1<System.Object>::.ctor(System.Object,System.IntPtr)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Action_1__ctor_mA671E933C9D3DAE4E3F71D34FDDA971739618158_gshared (Action_1_tD9663D9715FAA4E62035CFCF1AD4D094EE7872DC * __this, RuntimeObject * ___object0, intptr_t ___method1, const RuntimeMethod* method);
// System.Void System.Action`2<System.Object,System.Object>::.ctor(System.Object,System.IntPtr)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Action_2__ctor_mE1761BE81335B68DA4E0F742344DA72F092A29C1_gshared (Action_2_t4FB8E5660AE634E13BF340904C61FEA9DCE9D52D * __this, RuntimeObject * ___object0, intptr_t ___method1, const RuntimeMethod* method);
// System.Void System.Action`3<System.Object,System.Object,System.Object>::.ctor(System.Object,System.IntPtr)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Action_3__ctor_m2C9082E7979FF084723BC9078175D8F984DCA7BD_gshared (Action_3_t40CAA9C4849DA1712B1B6ECA55C18E0C0DFEBE4C * __this, RuntimeObject * ___object0, intptr_t ___method1, const RuntimeMethod* method);
// System.Void System.Action`3<System.Object,System.Object,System.Int32Enum>::.ctor(System.Object,System.IntPtr)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Action_3__ctor_m71AE1C02A577DEC742DECB6C9A2C3D2540DB81F0_gshared (Action_3_tDDADDA1F8499D3255F24355EFF7A9FA4E2B5823B * __this, RuntimeObject * ___object0, intptr_t ___method1, const RuntimeMethod* method);
// BestHTTP.Futures.IFuture`1<TResult> BestHTTP.SignalRCore.HubConnection::Invoke<System.Object>(System.String,System.Object[])
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* HubConnection_Invoke_TisRuntimeObject_m745BFBF46AEFFDF5DEB78BC011581184E0D746BB_gshared (HubConnection_t797D4973C9BBE6895D85BF743512ADA7D419736D * __this, String_t* ___target0, ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE* ___args1, const RuntimeMethod* method);
// System.Void BestHTTP.Futures.FutureValueCallback`1<System.Object>::.ctor(System.Object,System.IntPtr)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void FutureValueCallback_1__ctor_mCB9297BD84400C68AB7A3F4C319EB2D0D6E08704_gshared (FutureValueCallback_1_t7F9F1097135E654A08A590D5E1DD9A30730308D2 * __this, RuntimeObject * ___object0, intptr_t ___method1, const RuntimeMethod* method);

// System.Void System.Object::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Object__ctor_m88880E0413421D13FD95325EDCE231707CE1F405 (RuntimeObject * __this, const RuntimeMethod* method);
// System.Void BestHTTP.SignalRCore.HubOptions::set_SkipNegotiation(System.Boolean)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void HubOptions_set_SkipNegotiation_m59BB857442CD42D47F5642B8DD8040EFF158E406_inline (HubOptions_t53B108AE0F01D633E571D739D96B573C5C68A6F7 * __this, bool ___value0, const RuntimeMethod* method);
// System.Void BestHTTP.SignalRCore.HubOptions::set_PreferedTransport(BestHTTP.SignalRCore.TransportTypes)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void HubOptions_set_PreferedTransport_mA281AA3D280D43C4F104055FA9B31A6328504260_inline (HubOptions_t53B108AE0F01D633E571D739D96B573C5C68A6F7 * __this, int32_t ___value0, const RuntimeMethod* method);
// System.TimeSpan System.TimeSpan::FromSeconds(System.Double)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR TimeSpan_t4F6A0E13E703B65365CFCAB58E05EE0AF3EE6203  TimeSpan_FromSeconds_m4644EABECA69BC6C07AD649C5898A8E53F4FE7B0 (double ___value0, const RuntimeMethod* method);
// System.Void BestHTTP.SignalRCore.HubOptions::set_PingInterval(System.TimeSpan)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void HubOptions_set_PingInterval_mA0F505D0A849192BF594DDD9968139AE2621FFC4_inline (HubOptions_t53B108AE0F01D633E571D739D96B573C5C68A6F7 * __this, TimeSpan_t4F6A0E13E703B65365CFCAB58E05EE0AF3EE6203  ___value0, const RuntimeMethod* method);
// System.Void BestHTTP.SignalRCore.HubOptions::set_PingTimeoutInterval(System.TimeSpan)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void HubOptions_set_PingTimeoutInterval_mE19EF89589B500776CA57FDDB5D4FC14EB5966DF_inline (HubOptions_t53B108AE0F01D633E571D739D96B573C5C68A6F7 * __this, TimeSpan_t4F6A0E13E703B65365CFCAB58E05EE0AF3EE6203  ___value0, const RuntimeMethod* method);
// System.Void BestHTTP.SignalRCore.HubOptions::set_MaxRedirects(System.Int32)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void HubOptions_set_MaxRedirects_m044A7BFC4105C2B38A2DF088B9A779275A08F994_inline (HubOptions_t53B108AE0F01D633E571D739D96B573C5C68A6F7 * __this, int32_t ___value0, const RuntimeMethod* method);
// System.Void BestHTTP.SignalRCore.HubOptions::set_ConnectTimeout(System.TimeSpan)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void HubOptions_set_ConnectTimeout_m4B2039D9EDEAD322404EDAE96A6D575924625E71_inline (HubOptions_t53B108AE0F01D633E571D739D96B573C5C68A6F7 * __this, TimeSpan_t4F6A0E13E703B65365CFCAB58E05EE0AF3EE6203  ___value0, const RuntimeMethod* method);
// System.Void BestHTTP.Examples.Helpers.SampleBase::Start()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void SampleBase_Start_m00468C0449F4FC1F2D15592407F5B41DF8E094AE (SampleBase_tC30950FFCCCE8696B341569EEE364BF4C4DB7157 * __this, const RuntimeMethod* method);
// System.Void BestHTTP.Examples.HubWithAuthorizationSample::SetButtons(System.Boolean,System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void HubWithAuthorizationSample_SetButtons_m84ECF94EFAD5BB948496C706675EB578DEA1573A (HubWithAuthorizationSample_t19979B3628DD848910FD70011959F7CB115D5968 * __this, bool ___connect0, bool ___close1, const RuntimeMethod* method);
// System.Void BestHTTP.SignalRCore.HubConnection::StartClose()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void HubConnection_StartClose_mD0F78FCA41866D4E763C83A4E2280B84B3A1082D (HubConnection_t797D4973C9BBE6895D85BF743512ADA7D419736D * __this, const RuntimeMethod* method);
// System.Void BestHTTP.SignalRCore.Encoders.LitJsonEncoder::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void LitJsonEncoder__ctor_m4832CB769DD2810EAF328862A46B448D2F170FA2 (LitJsonEncoder_t93D5E7144163028B776CFC2472958FD241FDAEC7 * __this, const RuntimeMethod* method);
// System.Void BestHTTP.SignalRCore.JsonProtocol::.ctor(BestHTTP.SignalRCore.IEncoder)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void JsonProtocol__ctor_m8227721C2A25FA8E7749C10D2152C1701EB5876B (JsonProtocol_t0DA0EB80CD537920D655CE3D790C87A7035A7635 * __this, RuntimeObject* ___encoder0, const RuntimeMethod* method);
// System.String System.String::Concat(System.String,System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* String_Concat_m4B4AB72618348C5DFBFBA8DED84B9E2EBDB55E1B (String_t* ___str00, String_t* ___str11, const RuntimeMethod* method);
// System.Void System.Uri::.ctor(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Uri__ctor_m7724F43B1525624FFF97A774B6B909B075714D5C (Uri_t4A915E1CC15B2C650F478099AD448E9466CBF612 * __this, String_t* ___uriString0, const RuntimeMethod* method);
// System.Void BestHTTP.SignalRCore.HubConnection::.ctor(System.Uri,BestHTTP.SignalRCore.IProtocol)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void HubConnection__ctor_m488859A04D0B53DFC278E28AEE4AADFF879F401D (HubConnection_t797D4973C9BBE6895D85BF743512ADA7D419736D * __this, Uri_t4A915E1CC15B2C650F478099AD448E9466CBF612 * ___hubUri0, RuntimeObject* ___protocol1, const RuntimeMethod* method);
// System.Void System.Action`1<BestHTTP.SignalRCore.HubConnection>::.ctor(System.Object,System.IntPtr)
inline void Action_1__ctor_mF3C29B87B57748F26952F077111724B7F8728AD8 (Action_1_tD3410B67BFA3C1FCF5D3D8345C933F0ACA7A5426 * __this, RuntimeObject * ___object0, intptr_t ___method1, const RuntimeMethod* method)
{
	((  void (*) (Action_1_tD3410B67BFA3C1FCF5D3D8345C933F0ACA7A5426 *, RuntimeObject *, intptr_t, const RuntimeMethod*))Action_1__ctor_mA671E933C9D3DAE4E3F71D34FDDA971739618158_gshared)(__this, ___object0, ___method1, method);
}
// System.Void BestHTTP.SignalRCore.HubConnection::add_OnConnected(System.Action`1<BestHTTP.SignalRCore.HubConnection>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void HubConnection_add_OnConnected_mF853CCA78B6716068BD0F6328605DE4956F852FF (HubConnection_t797D4973C9BBE6895D85BF743512ADA7D419736D * __this, Action_1_tD3410B67BFA3C1FCF5D3D8345C933F0ACA7A5426 * ___value0, const RuntimeMethod* method);
// System.Void System.Action`2<BestHTTP.SignalRCore.HubConnection,System.String>::.ctor(System.Object,System.IntPtr)
inline void Action_2__ctor_m9E9D0A4856BE79EC7CCD28D4A14DC05BB0BB1CBE (Action_2_t36E73E35B8C2732CDF2877AFC5E932F8910CD2A5 * __this, RuntimeObject * ___object0, intptr_t ___method1, const RuntimeMethod* method)
{
	((  void (*) (Action_2_t36E73E35B8C2732CDF2877AFC5E932F8910CD2A5 *, RuntimeObject *, intptr_t, const RuntimeMethod*))Action_2__ctor_mE1761BE81335B68DA4E0F742344DA72F092A29C1_gshared)(__this, ___object0, ___method1, method);
}
// System.Void BestHTTP.SignalRCore.HubConnection::add_OnError(System.Action`2<BestHTTP.SignalRCore.HubConnection,System.String>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void HubConnection_add_OnError_m118C02C801BDA2C9E869B658BA1A023E68EF0871 (HubConnection_t797D4973C9BBE6895D85BF743512ADA7D419736D * __this, Action_2_t36E73E35B8C2732CDF2877AFC5E932F8910CD2A5 * ___value0, const RuntimeMethod* method);
// System.Void BestHTTP.SignalRCore.HubConnection::add_OnClosed(System.Action`1<BestHTTP.SignalRCore.HubConnection>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void HubConnection_add_OnClosed_m2C9F0587D49C73063523EFB7DC18D45847C311C5 (HubConnection_t797D4973C9BBE6895D85BF743512ADA7D419736D * __this, Action_1_tD3410B67BFA3C1FCF5D3D8345C933F0ACA7A5426 * ___value0, const RuntimeMethod* method);
// System.Void System.Action`3<BestHTTP.SignalRCore.HubConnection,System.Uri,System.Uri>::.ctor(System.Object,System.IntPtr)
inline void Action_3__ctor_m9F958EE4C81855D891333C72B908AA2E183BCA4A (Action_3_t5271F369265AB717807700BB149273B6EE0B1A4E * __this, RuntimeObject * ___object0, intptr_t ___method1, const RuntimeMethod* method)
{
	((  void (*) (Action_3_t5271F369265AB717807700BB149273B6EE0B1A4E *, RuntimeObject *, intptr_t, const RuntimeMethod*))Action_3__ctor_m2C9082E7979FF084723BC9078175D8F984DCA7BD_gshared)(__this, ___object0, ___method1, method);
}
// System.Void BestHTTP.SignalRCore.HubConnection::add_OnRedirected(System.Action`3<BestHTTP.SignalRCore.HubConnection,System.Uri,System.Uri>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void HubConnection_add_OnRedirected_mE1F89384FCB1EAF4BB7B6776D4A3C5E2AFA132BF (HubConnection_t797D4973C9BBE6895D85BF743512ADA7D419736D * __this, Action_3_t5271F369265AB717807700BB149273B6EE0B1A4E * ___value0, const RuntimeMethod* method);
// System.Void System.Action`3<BestHTTP.SignalRCore.HubConnection,BestHTTP.SignalRCore.ITransport,BestHTTP.SignalRCore.TransportEvents>::.ctor(System.Object,System.IntPtr)
inline void Action_3__ctor_m4CBDE8196ACBD5567E04A96C3714BF3D6C29F818 (Action_3_t2A25F65DF18A1F5038436F84891B59501F1E97B6 * __this, RuntimeObject * ___object0, intptr_t ___method1, const RuntimeMethod* method)
{
	((  void (*) (Action_3_t2A25F65DF18A1F5038436F84891B59501F1E97B6 *, RuntimeObject *, intptr_t, const RuntimeMethod*))Action_3__ctor_m71AE1C02A577DEC742DECB6C9A2C3D2540DB81F0_gshared)(__this, ___object0, ___method1, method);
}
// System.Void BestHTTP.SignalRCore.HubConnection::add_OnTransportEvent(System.Action`3<BestHTTP.SignalRCore.HubConnection,BestHTTP.SignalRCore.ITransport,BestHTTP.SignalRCore.TransportEvents>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void HubConnection_add_OnTransportEvent_mBB4C43B11D8BB500C39F28370A602638E1ADB4F4 (HubConnection_t797D4973C9BBE6895D85BF743512ADA7D419736D * __this, Action_3_t2A25F65DF18A1F5038436F84891B59501F1E97B6 * ___value0, const RuntimeMethod* method);
// System.Void BestHTTP.SignalRCore.HubConnection::StartConnect()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void HubConnection_StartConnect_m63E5C170518ECC28C2E038BAF8407CD13FE30219 (HubConnection_t797D4973C9BBE6895D85BF743512ADA7D419736D * __this, const RuntimeMethod* method);
// BestHTTP.Examples.Helpers.TextListItem BestHTTP.Examples.HubWithAuthorizationSample::AddText(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR TextListItem_tC8FEA392C6AF65CFF22D8089B9104B79C84A6D8B * HubWithAuthorizationSample_AddText_m0E07BF05AEEB07DEFCBB4D7072BD1BDC73FCD8EE (HubWithAuthorizationSample_t19979B3628DD848910FD70011959F7CB115D5968 * __this, String_t* ___text0, const RuntimeMethod* method);
// System.Uri BestHTTP.SignalRCore.HubConnection::get_Uri()
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR Uri_t4A915E1CC15B2C650F478099AD448E9466CBF612 * HubConnection_get_Uri_mBA6812461AA0DDC4D50DB39047299F2EE8EE59DA_inline (HubConnection_t797D4973C9BBE6895D85BF743512ADA7D419736D * __this, const RuntimeMethod* method);
// BestHTTP.SignalRCore.Messages.NegotiationResult BestHTTP.SignalRCore.HubConnection::get_NegotiationResult()
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR NegotiationResult_tB56E4CA54D44A10740AEDCBAC199D03849AEE84C * HubConnection_get_NegotiationResult_mB9C9B17462369CA6EB10279D142F7639E63E5322_inline (HubConnection_t797D4973C9BBE6895D85BF743512ADA7D419736D * __this, const RuntimeMethod* method);
// System.String BestHTTP.SignalRCore.Messages.NegotiationResult::get_AccessToken()
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR String_t* NegotiationResult_get_AccessToken_m2A2669209F7FCD3D9E1799E9CCAEC0D95B49D1A4_inline (NegotiationResult_tB56E4CA54D44A10740AEDCBAC199D03849AEE84C * __this, const RuntimeMethod* method);
// System.String System.String::Format(System.String,System.Object,System.Object)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* String_Format_m8D1CB0410C35E052A53AE957C914C841E54BAB66 (String_t* ___format0, RuntimeObject * ___arg01, RuntimeObject * ___arg12, const RuntimeMethod* method);
// BestHTTP.SignalRCore.ITransport BestHTTP.SignalRCore.HubConnection::get_Transport()
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR RuntimeObject* HubConnection_get_Transport_m1C10051EF86E5ED6DF6672BC9366D0C3DFDD166D_inline (HubConnection_t797D4973C9BBE6895D85BF743512ADA7D419736D * __this, const RuntimeMethod* method);
// BestHTTP.SignalRCore.IProtocol BestHTTP.SignalRCore.HubConnection::get_Protocol()
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR RuntimeObject* HubConnection_get_Protocol_mD9367B3CDFA166D9C0F7EE4671B02E6A61F839D9_inline (HubConnection_t797D4973C9BBE6895D85BF743512ADA7D419736D * __this, const RuntimeMethod* method);
// BestHTTP.Futures.IFuture`1<TResult> BestHTTP.SignalRCore.HubConnection::Invoke<System.String>(System.String,System.Object[])
inline RuntimeObject* HubConnection_Invoke_TisString_t_mC092761B744F2CFDEBB042CD081359F0E51B8F96 (HubConnection_t797D4973C9BBE6895D85BF743512ADA7D419736D * __this, String_t* ___target0, ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE* ___args1, const RuntimeMethod* method)
{
	return ((  RuntimeObject* (*) (HubConnection_t797D4973C9BBE6895D85BF743512ADA7D419736D *, String_t*, ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE*, const RuntimeMethod*))HubConnection_Invoke_TisRuntimeObject_m745BFBF46AEFFDF5DEB78BC011581184E0D746BB_gshared)(__this, ___target0, ___args1, method);
}
// System.Void BestHTTP.Futures.FutureValueCallback`1<System.String>::.ctor(System.Object,System.IntPtr)
inline void FutureValueCallback_1__ctor_mC0AAA3F5D464DBA95A0F1604DDD4376A568D8E75 (FutureValueCallback_1_t31AE88E9DCB927E5F16747DB54C827FE97F91BBF * __this, RuntimeObject * ___object0, intptr_t ___method1, const RuntimeMethod* method)
{
	((  void (*) (FutureValueCallback_1_t31AE88E9DCB927E5F16747DB54C827FE97F91BBF *, RuntimeObject *, intptr_t, const RuntimeMethod*))FutureValueCallback_1__ctor_mCB9297BD84400C68AB7A3F4C319EB2D0D6E08704_gshared)(__this, ___object0, ___method1, method);
}
// System.Void BestHTTP.Examples.Helpers.TextListItem::AddLeftPadding(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void TextListItem_AddLeftPadding_m41C860ECCEF8C63672FB82BCB45198F313528C5F (TextListItem_tC8FEA392C6AF65CFF22D8089B9104B79C84A6D8B * __this, int32_t ___padding0, const RuntimeMethod* method);
// System.String System.String::Format(System.String,System.Object)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* String_Format_mB3D38E5238C3164DB4D7D29339D9E225A4496D17 (String_t* ___format0, RuntimeObject * ___arg01, const RuntimeMethod* method);
// System.Boolean UnityEngine.Object::op_Inequality(UnityEngine.Object,UnityEngine.Object)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool Object_op_Inequality_mE1F187520BD83FB7D86A6D850710C4D42B864E90 (Object_tF2F3778131EFF286AF62B7B013A170F95A91571A * ___x0, Object_tF2F3778131EFF286AF62B7B013A170F95A91571A * ___y1, const RuntimeMethod* method);
// System.Void UnityEngine.UI.Selectable::set_interactable(System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Selectable_set_interactable_mE6F57D33A9E0484377174D0F490C4372BF7F0D40 (Selectable_t34088A3677CC9D344F81B0D91999D8C5963D7DBD * __this, bool ___value0, const RuntimeMethod* method);
// BestHTTP.Examples.Helpers.TextListItem BestHTTP.Examples.GUIHelper::AddText(BestHTTP.Examples.Helpers.TextListItem,UnityEngine.RectTransform,System.String,System.Int32,UnityEngine.UI.ScrollRect)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR TextListItem_tC8FEA392C6AF65CFF22D8089B9104B79C84A6D8B * GUIHelper_AddText_mAC779A2E1D44664A6F92A3CAD4AB26F8BFE3E56E (TextListItem_tC8FEA392C6AF65CFF22D8089B9104B79C84A6D8B * ___prefab0, RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 * ___contentRoot1, String_t* ___text2, int32_t ___maxEntries3, ScrollRect_tB16156010F89FFDAAB2127CA878608FD91B9FBEA * ___scrollRect4, const RuntimeMethod* method);
// System.Void BestHTTP.Examples.Helpers.SampleBase::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void SampleBase__ctor_mBF0888BC18DA99E126CDA8D6221DF4CC54D93E22 (SampleBase_tC30950FFCCCE8696B341569EEE364BF4C4DB7157 * __this, const RuntimeMethod* method);
// System.Void BestHTTP.Examples.HubWithPreAuthorizationSample::SetButtons(System.Boolean,System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void HubWithPreAuthorizationSample_SetButtons_mF8733AB0AA545452F48E29183D53D0D3CDDD4035 (HubWithPreAuthorizationSample_tDF799D602DC996BFF26C8E97175B4E64BF3807D8 * __this, bool ___connect0, bool ___close1, const RuntimeMethod* method);
// System.Void BestHTTP.Examples.PreAuthAccessTokenAuthenticator::.ctor(System.Uri)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PreAuthAccessTokenAuthenticator__ctor_mAAF19DED5CA60BF7BC0C5B13BDA11DA4B8784F17 (PreAuthAccessTokenAuthenticator_t424A1F277DA9B16ADCB16A5B9A7659E4B5DC6162 * __this, Uri_t4A915E1CC15B2C650F478099AD448E9466CBF612 * ___authUri0, const RuntimeMethod* method);
// System.Void BestHTTP.SignalRCore.HubConnection::set_AuthenticationProvider(BestHTTP.SignalRCore.IAuthenticationProvider)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void HubConnection_set_AuthenticationProvider_mB2FE128F4FC37EDDA68714B81AE5A861B3681C68_inline (HubConnection_t797D4973C9BBE6895D85BF743512ADA7D419736D * __this, RuntimeObject* ___value0, const RuntimeMethod* method);
// BestHTTP.SignalRCore.IAuthenticationProvider BestHTTP.SignalRCore.HubConnection::get_AuthenticationProvider()
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR RuntimeObject* HubConnection_get_AuthenticationProvider_m136DD5105ABD5D51D8068992CD2FB5D0BD9F2BE3_inline (HubConnection_t797D4973C9BBE6895D85BF743512ADA7D419736D * __this, const RuntimeMethod* method);
// System.Void BestHTTP.SignalRCore.OnAuthenticationSuccededDelegate::.ctor(System.Object,System.IntPtr)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void OnAuthenticationSuccededDelegate__ctor_mC383B649F658A751643D66BB98847A914F53C8CA (OnAuthenticationSuccededDelegate_t7A96699C45F6100206650225F8875DCAC1A8DE46 * __this, RuntimeObject * ___object0, intptr_t ___method1, const RuntimeMethod* method);
// System.Void BestHTTP.SignalRCore.OnAuthenticationFailedDelegate::.ctor(System.Object,System.IntPtr)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void OnAuthenticationFailedDelegate__ctor_m99752AE3FAFEE6DB1CCD7F0666AEEF16FF6D62F4 (OnAuthenticationFailedDelegate_tB415D1478EDFF7309A953E24BD13797DEFAAF9B2 * __this, RuntimeObject * ___object0, intptr_t ___method1, const RuntimeMethod* method);
// BestHTTP.Examples.Helpers.TextListItem BestHTTP.Examples.HubWithPreAuthorizationSample::AddText(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR TextListItem_tC8FEA392C6AF65CFF22D8089B9104B79C84A6D8B * HubWithPreAuthorizationSample_AddText_m23B3D8EE9963A7D7ED1F6FD1655F0B0619542EF3 (HubWithPreAuthorizationSample_tDF799D602DC996BFF26C8E97175B4E64BF3807D8 * __this, String_t* ___text0, const RuntimeMethod* method);
// System.String BestHTTP.Examples.PreAuthAccessTokenAuthenticator::get_Token()
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR String_t* PreAuthAccessTokenAuthenticator_get_Token_m168ED3A5300106BB0EF7FC10D9DE2D666FAFCF82_inline (PreAuthAccessTokenAuthenticator_t424A1F277DA9B16ADCB16A5B9A7659E4B5DC6162 * __this, const RuntimeMethod* method);
// System.String System.Byte::ToString()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* Byte_ToString_m6A11C71EB9B8031596645EA1A4C2430721B282B5 (uint8_t* __this, const RuntimeMethod* method);
// System.Void System.Exception::.ctor(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Exception__ctor_m8ECDE8ACA7F2E0EF1144BD1200FB5DB2870B5F11 (Exception_t * __this, String_t* ___message0, const RuntimeMethod* method);
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Boolean BestHTTP.SignalRCore.HubOptions::get_SkipNegotiation()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool HubOptions_get_SkipNegotiation_mEF675402F98108D39C81DEB4BF32F9EF221CF3A3 (HubOptions_t53B108AE0F01D633E571D739D96B573C5C68A6F7 * __this, const RuntimeMethod* method)
{
	{
		// public bool SkipNegotiation { get; set; }
		bool L_0 = __this->get_U3CSkipNegotiationU3Ek__BackingField_0();
		return L_0;
	}
}
// System.Void BestHTTP.SignalRCore.HubOptions::set_SkipNegotiation(System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void HubOptions_set_SkipNegotiation_m59BB857442CD42D47F5642B8DD8040EFF158E406 (HubOptions_t53B108AE0F01D633E571D739D96B573C5C68A6F7 * __this, bool ___value0, const RuntimeMethod* method)
{
	{
		// public bool SkipNegotiation { get; set; }
		bool L_0 = ___value0;
		__this->set_U3CSkipNegotiationU3Ek__BackingField_0(L_0);
		return;
	}
}
// BestHTTP.SignalRCore.TransportTypes BestHTTP.SignalRCore.HubOptions::get_PreferedTransport()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t HubOptions_get_PreferedTransport_m0B3F3ADB8D9B0A02484128D0D46201F9E3FFA638 (HubOptions_t53B108AE0F01D633E571D739D96B573C5C68A6F7 * __this, const RuntimeMethod* method)
{
	{
		// public TransportTypes PreferedTransport { get; set; }
		int32_t L_0 = __this->get_U3CPreferedTransportU3Ek__BackingField_1();
		return L_0;
	}
}
// System.Void BestHTTP.SignalRCore.HubOptions::set_PreferedTransport(BestHTTP.SignalRCore.TransportTypes)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void HubOptions_set_PreferedTransport_mA281AA3D280D43C4F104055FA9B31A6328504260 (HubOptions_t53B108AE0F01D633E571D739D96B573C5C68A6F7 * __this, int32_t ___value0, const RuntimeMethod* method)
{
	{
		// public TransportTypes PreferedTransport { get; set; }
		int32_t L_0 = ___value0;
		__this->set_U3CPreferedTransportU3Ek__BackingField_1(L_0);
		return;
	}
}
// System.TimeSpan BestHTTP.SignalRCore.HubOptions::get_PingInterval()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR TimeSpan_t4F6A0E13E703B65365CFCAB58E05EE0AF3EE6203  HubOptions_get_PingInterval_m58EA1733847C42158188138E01FD9C0CBB595BFA (HubOptions_t53B108AE0F01D633E571D739D96B573C5C68A6F7 * __this, const RuntimeMethod* method)
{
	{
		// public TimeSpan PingInterval { get; set; }
		TimeSpan_t4F6A0E13E703B65365CFCAB58E05EE0AF3EE6203  L_0 = __this->get_U3CPingIntervalU3Ek__BackingField_2();
		return L_0;
	}
}
// System.Void BestHTTP.SignalRCore.HubOptions::set_PingInterval(System.TimeSpan)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void HubOptions_set_PingInterval_mA0F505D0A849192BF594DDD9968139AE2621FFC4 (HubOptions_t53B108AE0F01D633E571D739D96B573C5C68A6F7 * __this, TimeSpan_t4F6A0E13E703B65365CFCAB58E05EE0AF3EE6203  ___value0, const RuntimeMethod* method)
{
	{
		// public TimeSpan PingInterval { get; set; }
		TimeSpan_t4F6A0E13E703B65365CFCAB58E05EE0AF3EE6203  L_0 = ___value0;
		__this->set_U3CPingIntervalU3Ek__BackingField_2(L_0);
		return;
	}
}
// System.TimeSpan BestHTTP.SignalRCore.HubOptions::get_PingTimeoutInterval()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR TimeSpan_t4F6A0E13E703B65365CFCAB58E05EE0AF3EE6203  HubOptions_get_PingTimeoutInterval_m9FFCFEF27577B06279625C79142DA56FC231F338 (HubOptions_t53B108AE0F01D633E571D739D96B573C5C68A6F7 * __this, const RuntimeMethod* method)
{
	{
		// public TimeSpan PingTimeoutInterval { get; set; }
		TimeSpan_t4F6A0E13E703B65365CFCAB58E05EE0AF3EE6203  L_0 = __this->get_U3CPingTimeoutIntervalU3Ek__BackingField_3();
		return L_0;
	}
}
// System.Void BestHTTP.SignalRCore.HubOptions::set_PingTimeoutInterval(System.TimeSpan)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void HubOptions_set_PingTimeoutInterval_mE19EF89589B500776CA57FDDB5D4FC14EB5966DF (HubOptions_t53B108AE0F01D633E571D739D96B573C5C68A6F7 * __this, TimeSpan_t4F6A0E13E703B65365CFCAB58E05EE0AF3EE6203  ___value0, const RuntimeMethod* method)
{
	{
		// public TimeSpan PingTimeoutInterval { get; set; }
		TimeSpan_t4F6A0E13E703B65365CFCAB58E05EE0AF3EE6203  L_0 = ___value0;
		__this->set_U3CPingTimeoutIntervalU3Ek__BackingField_3(L_0);
		return;
	}
}
// System.Int32 BestHTTP.SignalRCore.HubOptions::get_MaxRedirects()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t HubOptions_get_MaxRedirects_mD4A94B9A3411F169F7AF057F526761BFF0351757 (HubOptions_t53B108AE0F01D633E571D739D96B573C5C68A6F7 * __this, const RuntimeMethod* method)
{
	{
		// public int MaxRedirects { get; set; }
		int32_t L_0 = __this->get_U3CMaxRedirectsU3Ek__BackingField_4();
		return L_0;
	}
}
// System.Void BestHTTP.SignalRCore.HubOptions::set_MaxRedirects(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void HubOptions_set_MaxRedirects_m044A7BFC4105C2B38A2DF088B9A779275A08F994 (HubOptions_t53B108AE0F01D633E571D739D96B573C5C68A6F7 * __this, int32_t ___value0, const RuntimeMethod* method)
{
	{
		// public int MaxRedirects { get; set; }
		int32_t L_0 = ___value0;
		__this->set_U3CMaxRedirectsU3Ek__BackingField_4(L_0);
		return;
	}
}
// System.TimeSpan BestHTTP.SignalRCore.HubOptions::get_ConnectTimeout()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR TimeSpan_t4F6A0E13E703B65365CFCAB58E05EE0AF3EE6203  HubOptions_get_ConnectTimeout_mFD026FEA79C6A4992D9DEA433B95CE775E58B854 (HubOptions_t53B108AE0F01D633E571D739D96B573C5C68A6F7 * __this, const RuntimeMethod* method)
{
	{
		// public TimeSpan ConnectTimeout { get; set; }
		TimeSpan_t4F6A0E13E703B65365CFCAB58E05EE0AF3EE6203  L_0 = __this->get_U3CConnectTimeoutU3Ek__BackingField_5();
		return L_0;
	}
}
// System.Void BestHTTP.SignalRCore.HubOptions::set_ConnectTimeout(System.TimeSpan)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void HubOptions_set_ConnectTimeout_m4B2039D9EDEAD322404EDAE96A6D575924625E71 (HubOptions_t53B108AE0F01D633E571D739D96B573C5C68A6F7 * __this, TimeSpan_t4F6A0E13E703B65365CFCAB58E05EE0AF3EE6203  ___value0, const RuntimeMethod* method)
{
	{
		// public TimeSpan ConnectTimeout { get; set; }
		TimeSpan_t4F6A0E13E703B65365CFCAB58E05EE0AF3EE6203  L_0 = ___value0;
		__this->set_U3CConnectTimeoutU3Ek__BackingField_5(L_0);
		return;
	}
}
// System.Void BestHTTP.SignalRCore.HubOptions::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void HubOptions__ctor_m45B0DD49E456E068617B58A4C1F8F0E560933DF8 (HubOptions_t53B108AE0F01D633E571D739D96B573C5C68A6F7 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&TimeSpan_t4F6A0E13E703B65365CFCAB58E05EE0AF3EE6203_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// public HubOptions()
		Object__ctor_m88880E0413421D13FD95325EDCE231707CE1F405(__this, /*hidden argument*/NULL);
		// this.SkipNegotiation = false;
		HubOptions_set_SkipNegotiation_m59BB857442CD42D47F5642B8DD8040EFF158E406_inline(__this, (bool)0, /*hidden argument*/NULL);
		// this.PreferedTransport = TransportTypes.WebSocket;
		HubOptions_set_PreferedTransport_mA281AA3D280D43C4F104055FA9B31A6328504260_inline(__this, 0, /*hidden argument*/NULL);
		// this.PingInterval = TimeSpan.FromSeconds(15);
		IL2CPP_RUNTIME_CLASS_INIT(TimeSpan_t4F6A0E13E703B65365CFCAB58E05EE0AF3EE6203_il2cpp_TypeInfo_var);
		TimeSpan_t4F6A0E13E703B65365CFCAB58E05EE0AF3EE6203  L_0;
		L_0 = TimeSpan_FromSeconds_m4644EABECA69BC6C07AD649C5898A8E53F4FE7B0((15.0), /*hidden argument*/NULL);
		HubOptions_set_PingInterval_mA0F505D0A849192BF594DDD9968139AE2621FFC4_inline(__this, L_0, /*hidden argument*/NULL);
		// this.PingTimeoutInterval = TimeSpan.FromSeconds(30);
		TimeSpan_t4F6A0E13E703B65365CFCAB58E05EE0AF3EE6203  L_1;
		L_1 = TimeSpan_FromSeconds_m4644EABECA69BC6C07AD649C5898A8E53F4FE7B0((30.0), /*hidden argument*/NULL);
		HubOptions_set_PingTimeoutInterval_mE19EF89589B500776CA57FDDB5D4FC14EB5966DF_inline(__this, L_1, /*hidden argument*/NULL);
		// this.MaxRedirects = 100;
		HubOptions_set_MaxRedirects_m044A7BFC4105C2B38A2DF088B9A779275A08F994_inline(__this, ((int32_t)100), /*hidden argument*/NULL);
		// this.ConnectTimeout = TimeSpan.FromSeconds(60);
		TimeSpan_t4F6A0E13E703B65365CFCAB58E05EE0AF3EE6203  L_2;
		L_2 = TimeSpan_FromSeconds_m4644EABECA69BC6C07AD649C5898A8E53F4FE7B0((60.0), /*hidden argument*/NULL);
		HubOptions_set_ConnectTimeout_m4B2039D9EDEAD322404EDAE96A6D575924625E71_inline(__this, L_2, /*hidden argument*/NULL);
		// }
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void BestHTTP.Examples.HubWithAuthorizationSample::Start()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void HubWithAuthorizationSample_Start_mF1B9119831942811CD268F15C428C760061AC0F8 (HubWithAuthorizationSample_t19979B3628DD848910FD70011959F7CB115D5968 * __this, const RuntimeMethod* method)
{
	{
		// base.Start();
		SampleBase_Start_m00468C0449F4FC1F2D15592407F5B41DF8E094AE(__this, /*hidden argument*/NULL);
		// SetButtons(true, false);
		HubWithAuthorizationSample_SetButtons_m84ECF94EFAD5BB948496C706675EB578DEA1573A(__this, (bool)1, (bool)0, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void BestHTTP.Examples.HubWithAuthorizationSample::OnDestroy()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void HubWithAuthorizationSample_OnDestroy_m65D7784203A9326793AFDAB5288391CC04E1AB85 (HubWithAuthorizationSample_t19979B3628DD848910FD70011959F7CB115D5968 * __this, const RuntimeMethod* method)
{
	{
		// if (hub != null)
		HubConnection_t797D4973C9BBE6895D85BF743512ADA7D419736D * L_0 = __this->get_hub_16();
		if (!L_0)
		{
			goto IL_0013;
		}
	}
	{
		// hub.StartClose();
		HubConnection_t797D4973C9BBE6895D85BF743512ADA7D419736D * L_1 = __this->get_hub_16();
		NullCheck(L_1);
		HubConnection_StartClose_mD0F78FCA41866D4E763C83A4E2280B84B3A1082D(L_1, /*hidden argument*/NULL);
	}

IL_0013:
	{
		// }
		return;
	}
}
// System.Void BestHTTP.Examples.HubWithAuthorizationSample::OnConnectButton()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void HubWithAuthorizationSample_OnConnectButton_mC0D5DB1EBB13F2C75602C884349E4928393E52CA (HubWithAuthorizationSample_t19979B3628DD848910FD70011959F7CB115D5968 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Action_1__ctor_mF3C29B87B57748F26952F077111724B7F8728AD8_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Action_1_tD3410B67BFA3C1FCF5D3D8345C933F0ACA7A5426_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Action_2__ctor_m9E9D0A4856BE79EC7CCD28D4A14DC05BB0BB1CBE_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Action_2_t36E73E35B8C2732CDF2877AFC5E932F8910CD2A5_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Action_3__ctor_m4CBDE8196ACBD5567E04A96C3714BF3D6C29F818_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Action_3__ctor_m9F958EE4C81855D891333C72B908AA2E183BCA4A_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Action_3_t2A25F65DF18A1F5038436F84891B59501F1E97B6_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Action_3_t5271F369265AB717807700BB149273B6EE0B1A4E_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&HubConnection_t797D4973C9BBE6895D85BF743512ADA7D419736D_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&HubWithAuthorizationSample_Hub_OnClosed_mCFB8D0CCE1D0FB7B4E8971854C017D4D42AC1C5B_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&HubWithAuthorizationSample_Hub_OnConnected_m338A40A3F89743396F7D043FA490EACAB06E2498_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&HubWithAuthorizationSample_Hub_OnError_mFFD5C8D2A4A52374687962623A0E30E1B0165482_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&HubWithAuthorizationSample_Hub_Redirected_m8556E4CBAD4E69E1191E3C0CB892779A9F80AA6D_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&HubWithAuthorizationSample_U3COnConnectButtonU3Eb__10_0_m6D430BDDCAA15325F032C3A4B13E411D21DE5EBD_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&JsonProtocol_t0DA0EB80CD537920D655CE3D790C87A7035A7635_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&LitJsonEncoder_t93D5E7144163028B776CFC2472958FD241FDAEC7_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Uri_t4A915E1CC15B2C650F478099AD448E9466CBF612_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral94A3AB9B36FB187F4215E62357DFFBDC6B930029);
		s_Il2CppMethodInitialized = true;
	}
	RuntimeObject* V_0 = NULL;
	{
		// IProtocol protocol = null;
		V_0 = (RuntimeObject*)NULL;
		// protocol = new JsonProtocol(new LitJsonEncoder());
		LitJsonEncoder_t93D5E7144163028B776CFC2472958FD241FDAEC7 * L_0 = (LitJsonEncoder_t93D5E7144163028B776CFC2472958FD241FDAEC7 *)il2cpp_codegen_object_new(LitJsonEncoder_t93D5E7144163028B776CFC2472958FD241FDAEC7_il2cpp_TypeInfo_var);
		LitJsonEncoder__ctor_m4832CB769DD2810EAF328862A46B448D2F170FA2(L_0, /*hidden argument*/NULL);
		JsonProtocol_t0DA0EB80CD537920D655CE3D790C87A7035A7635 * L_1 = (JsonProtocol_t0DA0EB80CD537920D655CE3D790C87A7035A7635 *)il2cpp_codegen_object_new(JsonProtocol_t0DA0EB80CD537920D655CE3D790C87A7035A7635_il2cpp_TypeInfo_var);
		JsonProtocol__ctor_m8227721C2A25FA8E7749C10D2152C1701EB5876B(L_1, L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		// hub = new HubConnection(new Uri(base.sampleSelector.BaseURL + this._path), protocol);
		SampleRoot_tB11D3D3067248A62081D6D1A6F5936A1A3D856E1 * L_2 = ((SampleBase_tC30950FFCCCE8696B341569EEE364BF4C4DB7157 *)__this)->get_sampleSelector_8();
		NullCheck(L_2);
		String_t* L_3 = L_2->get_BaseURL_4();
		String_t* L_4 = __this->get__path_9();
		String_t* L_5;
		L_5 = String_Concat_m4B4AB72618348C5DFBFBA8DED84B9E2EBDB55E1B(L_3, L_4, /*hidden argument*/NULL);
		Uri_t4A915E1CC15B2C650F478099AD448E9466CBF612 * L_6 = (Uri_t4A915E1CC15B2C650F478099AD448E9466CBF612 *)il2cpp_codegen_object_new(Uri_t4A915E1CC15B2C650F478099AD448E9466CBF612_il2cpp_TypeInfo_var);
		Uri__ctor_m7724F43B1525624FFF97A774B6B909B075714D5C(L_6, L_5, /*hidden argument*/NULL);
		RuntimeObject* L_7 = V_0;
		HubConnection_t797D4973C9BBE6895D85BF743512ADA7D419736D * L_8 = (HubConnection_t797D4973C9BBE6895D85BF743512ADA7D419736D *)il2cpp_codegen_object_new(HubConnection_t797D4973C9BBE6895D85BF743512ADA7D419736D_il2cpp_TypeInfo_var);
		HubConnection__ctor_m488859A04D0B53DFC278E28AEE4AADFF879F401D(L_8, L_6, L_7, /*hidden argument*/NULL);
		__this->set_hub_16(L_8);
		// hub.OnConnected += Hub_OnConnected;
		HubConnection_t797D4973C9BBE6895D85BF743512ADA7D419736D * L_9 = __this->get_hub_16();
		Action_1_tD3410B67BFA3C1FCF5D3D8345C933F0ACA7A5426 * L_10 = (Action_1_tD3410B67BFA3C1FCF5D3D8345C933F0ACA7A5426 *)il2cpp_codegen_object_new(Action_1_tD3410B67BFA3C1FCF5D3D8345C933F0ACA7A5426_il2cpp_TypeInfo_var);
		Action_1__ctor_mF3C29B87B57748F26952F077111724B7F8728AD8(L_10, __this, (intptr_t)((intptr_t)HubWithAuthorizationSample_Hub_OnConnected_m338A40A3F89743396F7D043FA490EACAB06E2498_RuntimeMethod_var), /*hidden argument*/Action_1__ctor_mF3C29B87B57748F26952F077111724B7F8728AD8_RuntimeMethod_var);
		NullCheck(L_9);
		HubConnection_add_OnConnected_mF853CCA78B6716068BD0F6328605DE4956F852FF(L_9, L_10, /*hidden argument*/NULL);
		// hub.OnError += Hub_OnError;
		HubConnection_t797D4973C9BBE6895D85BF743512ADA7D419736D * L_11 = __this->get_hub_16();
		Action_2_t36E73E35B8C2732CDF2877AFC5E932F8910CD2A5 * L_12 = (Action_2_t36E73E35B8C2732CDF2877AFC5E932F8910CD2A5 *)il2cpp_codegen_object_new(Action_2_t36E73E35B8C2732CDF2877AFC5E932F8910CD2A5_il2cpp_TypeInfo_var);
		Action_2__ctor_m9E9D0A4856BE79EC7CCD28D4A14DC05BB0BB1CBE(L_12, __this, (intptr_t)((intptr_t)HubWithAuthorizationSample_Hub_OnError_mFFD5C8D2A4A52374687962623A0E30E1B0165482_RuntimeMethod_var), /*hidden argument*/Action_2__ctor_m9E9D0A4856BE79EC7CCD28D4A14DC05BB0BB1CBE_RuntimeMethod_var);
		NullCheck(L_11);
		HubConnection_add_OnError_m118C02C801BDA2C9E869B658BA1A023E68EF0871(L_11, L_12, /*hidden argument*/NULL);
		// hub.OnClosed += Hub_OnClosed;
		HubConnection_t797D4973C9BBE6895D85BF743512ADA7D419736D * L_13 = __this->get_hub_16();
		Action_1_tD3410B67BFA3C1FCF5D3D8345C933F0ACA7A5426 * L_14 = (Action_1_tD3410B67BFA3C1FCF5D3D8345C933F0ACA7A5426 *)il2cpp_codegen_object_new(Action_1_tD3410B67BFA3C1FCF5D3D8345C933F0ACA7A5426_il2cpp_TypeInfo_var);
		Action_1__ctor_mF3C29B87B57748F26952F077111724B7F8728AD8(L_14, __this, (intptr_t)((intptr_t)HubWithAuthorizationSample_Hub_OnClosed_mCFB8D0CCE1D0FB7B4E8971854C017D4D42AC1C5B_RuntimeMethod_var), /*hidden argument*/Action_1__ctor_mF3C29B87B57748F26952F077111724B7F8728AD8_RuntimeMethod_var);
		NullCheck(L_13);
		HubConnection_add_OnClosed_m2C9F0587D49C73063523EFB7DC18D45847C311C5(L_13, L_14, /*hidden argument*/NULL);
		// hub.OnRedirected += Hub_Redirected;
		HubConnection_t797D4973C9BBE6895D85BF743512ADA7D419736D * L_15 = __this->get_hub_16();
		Action_3_t5271F369265AB717807700BB149273B6EE0B1A4E * L_16 = (Action_3_t5271F369265AB717807700BB149273B6EE0B1A4E *)il2cpp_codegen_object_new(Action_3_t5271F369265AB717807700BB149273B6EE0B1A4E_il2cpp_TypeInfo_var);
		Action_3__ctor_m9F958EE4C81855D891333C72B908AA2E183BCA4A(L_16, __this, (intptr_t)((intptr_t)HubWithAuthorizationSample_Hub_Redirected_m8556E4CBAD4E69E1191E3C0CB892779A9F80AA6D_RuntimeMethod_var), /*hidden argument*/Action_3__ctor_m9F958EE4C81855D891333C72B908AA2E183BCA4A_RuntimeMethod_var);
		NullCheck(L_15);
		HubConnection_add_OnRedirected_mE1F89384FCB1EAF4BB7B6776D4A3C5E2AFA132BF(L_15, L_16, /*hidden argument*/NULL);
		// hub.OnTransportEvent += (hub, transport, ev) => AddText(string.Format("Transport(<color=green>{0}</color>) event: <color=green>{1}</color>", transport.TransportType, ev));
		HubConnection_t797D4973C9BBE6895D85BF743512ADA7D419736D * L_17 = __this->get_hub_16();
		Action_3_t2A25F65DF18A1F5038436F84891B59501F1E97B6 * L_18 = (Action_3_t2A25F65DF18A1F5038436F84891B59501F1E97B6 *)il2cpp_codegen_object_new(Action_3_t2A25F65DF18A1F5038436F84891B59501F1E97B6_il2cpp_TypeInfo_var);
		Action_3__ctor_m4CBDE8196ACBD5567E04A96C3714BF3D6C29F818(L_18, __this, (intptr_t)((intptr_t)HubWithAuthorizationSample_U3COnConnectButtonU3Eb__10_0_m6D430BDDCAA15325F032C3A4B13E411D21DE5EBD_RuntimeMethod_var), /*hidden argument*/Action_3__ctor_m4CBDE8196ACBD5567E04A96C3714BF3D6C29F818_RuntimeMethod_var);
		NullCheck(L_17);
		HubConnection_add_OnTransportEvent_mBB4C43B11D8BB500C39F28370A602638E1ADB4F4(L_17, L_18, /*hidden argument*/NULL);
		// hub.StartConnect();
		HubConnection_t797D4973C9BBE6895D85BF743512ADA7D419736D * L_19 = __this->get_hub_16();
		NullCheck(L_19);
		HubConnection_StartConnect_m63E5C170518ECC28C2E038BAF8407CD13FE30219(L_19, /*hidden argument*/NULL);
		// AddText("StartConnect called");
		TextListItem_tC8FEA392C6AF65CFF22D8089B9104B79C84A6D8B * L_20;
		L_20 = HubWithAuthorizationSample_AddText_m0E07BF05AEEB07DEFCBB4D7072BD1BDC73FCD8EE(__this, _stringLiteral94A3AB9B36FB187F4215E62357DFFBDC6B930029, /*hidden argument*/NULL);
		// SetButtons(false, false);
		HubWithAuthorizationSample_SetButtons_m84ECF94EFAD5BB948496C706675EB578DEA1573A(__this, (bool)0, (bool)0, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void BestHTTP.Examples.HubWithAuthorizationSample::OnCloseButton()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void HubWithAuthorizationSample_OnCloseButton_m6774295B9EAFEDEC2D9799D888DE6DC360FBBD5F (HubWithAuthorizationSample_t19979B3628DD848910FD70011959F7CB115D5968 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralCB58925B56DD017047A25D2F36CB1BCA26F68FC3);
		s_Il2CppMethodInitialized = true;
	}
	{
		// if (hub != null)
		HubConnection_t797D4973C9BBE6895D85BF743512ADA7D419736D * L_0 = __this->get_hub_16();
		if (!L_0)
		{
			goto IL_0027;
		}
	}
	{
		// hub.StartClose();
		HubConnection_t797D4973C9BBE6895D85BF743512ADA7D419736D * L_1 = __this->get_hub_16();
		NullCheck(L_1);
		HubConnection_StartClose_mD0F78FCA41866D4E763C83A4E2280B84B3A1082D(L_1, /*hidden argument*/NULL);
		// AddText("StartClose called");
		TextListItem_tC8FEA392C6AF65CFF22D8089B9104B79C84A6D8B * L_2;
		L_2 = HubWithAuthorizationSample_AddText_m0E07BF05AEEB07DEFCBB4D7072BD1BDC73FCD8EE(__this, _stringLiteralCB58925B56DD017047A25D2F36CB1BCA26F68FC3, /*hidden argument*/NULL);
		// SetButtons(false, false);
		HubWithAuthorizationSample_SetButtons_m84ECF94EFAD5BB948496C706675EB578DEA1573A(__this, (bool)0, (bool)0, /*hidden argument*/NULL);
	}

IL_0027:
	{
		// }
		return;
	}
}
// System.Void BestHTTP.Examples.HubWithAuthorizationSample::Hub_Redirected(BestHTTP.SignalRCore.HubConnection,System.Uri,System.Uri)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void HubWithAuthorizationSample_Hub_Redirected_m8556E4CBAD4E69E1191E3C0CB892779A9F80AA6D (HubWithAuthorizationSample_t19979B3628DD848910FD70011959F7CB115D5968 * __this, HubConnection_t797D4973C9BBE6895D85BF743512ADA7D419736D * ___hub0, Uri_t4A915E1CC15B2C650F478099AD448E9466CBF612 * ___oldUri1, Uri_t4A915E1CC15B2C650F478099AD448E9466CBF612 * ___newUri2, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralA4529359CF24AF6F4533508E56794F9CAD34F426);
		s_Il2CppMethodInitialized = true;
	}
	{
		// AddText(string.Format("Hub connection redirected to '<color=green>{0}</color>' with Access Token: '<color=green>{1}</color>'", hub.Uri, hub.NegotiationResult.AccessToken));
		HubConnection_t797D4973C9BBE6895D85BF743512ADA7D419736D * L_0 = ___hub0;
		NullCheck(L_0);
		Uri_t4A915E1CC15B2C650F478099AD448E9466CBF612 * L_1;
		L_1 = HubConnection_get_Uri_mBA6812461AA0DDC4D50DB39047299F2EE8EE59DA_inline(L_0, /*hidden argument*/NULL);
		HubConnection_t797D4973C9BBE6895D85BF743512ADA7D419736D * L_2 = ___hub0;
		NullCheck(L_2);
		NegotiationResult_tB56E4CA54D44A10740AEDCBAC199D03849AEE84C * L_3;
		L_3 = HubConnection_get_NegotiationResult_mB9C9B17462369CA6EB10279D142F7639E63E5322_inline(L_2, /*hidden argument*/NULL);
		NullCheck(L_3);
		String_t* L_4;
		L_4 = NegotiationResult_get_AccessToken_m2A2669209F7FCD3D9E1799E9CCAEC0D95B49D1A4_inline(L_3, /*hidden argument*/NULL);
		String_t* L_5;
		L_5 = String_Format_m8D1CB0410C35E052A53AE957C914C841E54BAB66(_stringLiteralA4529359CF24AF6F4533508E56794F9CAD34F426, L_1, L_4, /*hidden argument*/NULL);
		TextListItem_tC8FEA392C6AF65CFF22D8089B9104B79C84A6D8B * L_6;
		L_6 = HubWithAuthorizationSample_AddText_m0E07BF05AEEB07DEFCBB4D7072BD1BDC73FCD8EE(__this, L_5, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void BestHTTP.Examples.HubWithAuthorizationSample::Hub_OnConnected(BestHTTP.SignalRCore.HubConnection)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void HubWithAuthorizationSample_Hub_OnConnected_m338A40A3F89743396F7D043FA490EACAB06E2498 (HubWithAuthorizationSample_t19979B3628DD848910FD70011959F7CB115D5968 * __this, HubConnection_t797D4973C9BBE6895D85BF743512ADA7D419736D * ___hub0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&FutureValueCallback_1__ctor_mC0AAA3F5D464DBA95A0F1604DDD4376A568D8E75_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&FutureValueCallback_1_t31AE88E9DCB927E5F16747DB54C827FE97F91BBF_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&HubConnection_Invoke_TisString_t_mC092761B744F2CFDEBB042CD081359F0E51B8F96_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&HubWithAuthorizationSample_U3CHub_OnConnectedU3Eb__13_0_m65C934096BC045F396D14A1CE2E283380F3E801A_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&IFuture_1_tEBD463543458CE8C841D19C91EAC2683130EC31D_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&IProtocol_tFE0A86EE25C0427642F1B825BD8ADA84E8CD20CD_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&ITransport_t832D0F246CABAD20CB7410CC70ED972EF084053B_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&TransportTypes_t8387D0BDDE41AD7796AEFFC585944D24F9389FB0_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral3177C3549DF7103EBF6FAD9F320123D3A8D97BE7);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral450C5E3FC3E837EBBE92288DB9DE76195D7CF985);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral519A627D101D9C5AE7C965506A4856184ACCD31F);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralB82244460F050E4F7E423A7000AE2646F10EF257);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	{
		// AddText(string.Format("Hub Connected with <color=green>{0}</color> transport using the <color=green>{1}</color> encoder.", hub.Transport.TransportType.ToString(), hub.Protocol.Name));
		HubConnection_t797D4973C9BBE6895D85BF743512ADA7D419736D * L_0 = ___hub0;
		NullCheck(L_0);
		RuntimeObject* L_1;
		L_1 = HubConnection_get_Transport_m1C10051EF86E5ED6DF6672BC9366D0C3DFDD166D_inline(L_0, /*hidden argument*/NULL);
		NullCheck(L_1);
		int32_t L_2;
		L_2 = InterfaceFuncInvoker0< int32_t >::Invoke(1 /* BestHTTP.SignalRCore.TransportTypes BestHTTP.SignalRCore.ITransport::get_TransportType() */, ITransport_t832D0F246CABAD20CB7410CC70ED972EF084053B_il2cpp_TypeInfo_var, L_1);
		V_0 = L_2;
		RuntimeObject * L_3 = Box(TransportTypes_t8387D0BDDE41AD7796AEFFC585944D24F9389FB0_il2cpp_TypeInfo_var, (&V_0));
		NullCheck(L_3);
		String_t* L_4;
		L_4 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_3);
		V_0 = *(int32_t*)UnBox(L_3);
		HubConnection_t797D4973C9BBE6895D85BF743512ADA7D419736D * L_5 = ___hub0;
		NullCheck(L_5);
		RuntimeObject* L_6;
		L_6 = HubConnection_get_Protocol_mD9367B3CDFA166D9C0F7EE4671B02E6A61F839D9_inline(L_5, /*hidden argument*/NULL);
		NullCheck(L_6);
		String_t* L_7;
		L_7 = InterfaceFuncInvoker0< String_t* >::Invoke(0 /* System.String BestHTTP.SignalRCore.IProtocol::get_Name() */, IProtocol_tFE0A86EE25C0427642F1B825BD8ADA84E8CD20CD_il2cpp_TypeInfo_var, L_6);
		String_t* L_8;
		L_8 = String_Format_m8D1CB0410C35E052A53AE957C914C841E54BAB66(_stringLiteral519A627D101D9C5AE7C965506A4856184ACCD31F, L_4, L_7, /*hidden argument*/NULL);
		TextListItem_tC8FEA392C6AF65CFF22D8089B9104B79C84A6D8B * L_9;
		L_9 = HubWithAuthorizationSample_AddText_m0E07BF05AEEB07DEFCBB4D7072BD1BDC73FCD8EE(__this, L_8, /*hidden argument*/NULL);
		// SetButtons(false, true);
		HubWithAuthorizationSample_SetButtons_m84ECF94EFAD5BB948496C706675EB578DEA1573A(__this, (bool)0, (bool)1, /*hidden argument*/NULL);
		// hub.Invoke<string>("Echo", "Message from the client")
		//     .OnSuccess(ret => AddText(string.Format("'<color=green>Echo</color>' returned: '<color=yellow>{0}</color>'", ret)).AddLeftPadding(20));
		HubConnection_t797D4973C9BBE6895D85BF743512ADA7D419736D * L_10 = ___hub0;
		ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE* L_11 = (ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE*)(ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE*)SZArrayNew(ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE_il2cpp_TypeInfo_var, (uint32_t)1);
		ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE* L_12 = L_11;
		NullCheck(L_12);
		ArrayElementTypeCheck (L_12, _stringLiteral3177C3549DF7103EBF6FAD9F320123D3A8D97BE7);
		(L_12)->SetAt(static_cast<il2cpp_array_size_t>(0), (RuntimeObject *)_stringLiteral3177C3549DF7103EBF6FAD9F320123D3A8D97BE7);
		NullCheck(L_10);
		RuntimeObject* L_13;
		L_13 = HubConnection_Invoke_TisString_t_mC092761B744F2CFDEBB042CD081359F0E51B8F96(L_10, _stringLiteralB82244460F050E4F7E423A7000AE2646F10EF257, L_12, /*hidden argument*/HubConnection_Invoke_TisString_t_mC092761B744F2CFDEBB042CD081359F0E51B8F96_RuntimeMethod_var);
		FutureValueCallback_1_t31AE88E9DCB927E5F16747DB54C827FE97F91BBF * L_14 = (FutureValueCallback_1_t31AE88E9DCB927E5F16747DB54C827FE97F91BBF *)il2cpp_codegen_object_new(FutureValueCallback_1_t31AE88E9DCB927E5F16747DB54C827FE97F91BBF_il2cpp_TypeInfo_var);
		FutureValueCallback_1__ctor_mC0AAA3F5D464DBA95A0F1604DDD4376A568D8E75(L_14, __this, (intptr_t)((intptr_t)HubWithAuthorizationSample_U3CHub_OnConnectedU3Eb__13_0_m65C934096BC045F396D14A1CE2E283380F3E801A_RuntimeMethod_var), /*hidden argument*/FutureValueCallback_1__ctor_mC0AAA3F5D464DBA95A0F1604DDD4376A568D8E75_RuntimeMethod_var);
		NullCheck(L_13);
		RuntimeObject* L_15;
		L_15 = InterfaceFuncInvoker1< RuntimeObject*, FutureValueCallback_1_t31AE88E9DCB927E5F16747DB54C827FE97F91BBF * >::Invoke(4 /* BestHTTP.Futures.IFuture`1<T> BestHTTP.Futures.IFuture`1<System.String>::OnSuccess(BestHTTP.Futures.FutureValueCallback`1<T>) */, IFuture_1_tEBD463543458CE8C841D19C91EAC2683130EC31D_il2cpp_TypeInfo_var, L_13, L_14);
		// AddText("'<color=green>Message from the client</color>' sent!")
		//     .AddLeftPadding(20);
		TextListItem_tC8FEA392C6AF65CFF22D8089B9104B79C84A6D8B * L_16;
		L_16 = HubWithAuthorizationSample_AddText_m0E07BF05AEEB07DEFCBB4D7072BD1BDC73FCD8EE(__this, _stringLiteral450C5E3FC3E837EBBE92288DB9DE76195D7CF985, /*hidden argument*/NULL);
		NullCheck(L_16);
		TextListItem_AddLeftPadding_m41C860ECCEF8C63672FB82BCB45198F313528C5F(L_16, ((int32_t)20), /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void BestHTTP.Examples.HubWithAuthorizationSample::Hub_OnClosed(BestHTTP.SignalRCore.HubConnection)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void HubWithAuthorizationSample_Hub_OnClosed_mCFB8D0CCE1D0FB7B4E8971854C017D4D42AC1C5B (HubWithAuthorizationSample_t19979B3628DD848910FD70011959F7CB115D5968 * __this, HubConnection_t797D4973C9BBE6895D85BF743512ADA7D419736D * ___hub0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral5904D6D202BF91354FCA994B64879EAF9DEA8268);
		s_Il2CppMethodInitialized = true;
	}
	{
		// AddText("Hub Closed");
		TextListItem_tC8FEA392C6AF65CFF22D8089B9104B79C84A6D8B * L_0;
		L_0 = HubWithAuthorizationSample_AddText_m0E07BF05AEEB07DEFCBB4D7072BD1BDC73FCD8EE(__this, _stringLiteral5904D6D202BF91354FCA994B64879EAF9DEA8268, /*hidden argument*/NULL);
		// SetButtons(true, false);
		HubWithAuthorizationSample_SetButtons_m84ECF94EFAD5BB948496C706675EB578DEA1573A(__this, (bool)1, (bool)0, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void BestHTTP.Examples.HubWithAuthorizationSample::Hub_OnError(BestHTTP.SignalRCore.HubConnection,System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void HubWithAuthorizationSample_Hub_OnError_mFFD5C8D2A4A52374687962623A0E30E1B0165482 (HubWithAuthorizationSample_t19979B3628DD848910FD70011959F7CB115D5968 * __this, HubConnection_t797D4973C9BBE6895D85BF743512ADA7D419736D * ___hub0, String_t* ___error1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral0916CABAB7E3E45364FFC1B36C761FA2ABC7FF8E);
		s_Il2CppMethodInitialized = true;
	}
	{
		// AddText(string.Format("Hub Error: <color=red>{0}</color>", error));
		String_t* L_0 = ___error1;
		String_t* L_1;
		L_1 = String_Format_mB3D38E5238C3164DB4D7D29339D9E225A4496D17(_stringLiteral0916CABAB7E3E45364FFC1B36C761FA2ABC7FF8E, L_0, /*hidden argument*/NULL);
		TextListItem_tC8FEA392C6AF65CFF22D8089B9104B79C84A6D8B * L_2;
		L_2 = HubWithAuthorizationSample_AddText_m0E07BF05AEEB07DEFCBB4D7072BD1BDC73FCD8EE(__this, L_1, /*hidden argument*/NULL);
		// SetButtons(true, false);
		HubWithAuthorizationSample_SetButtons_m84ECF94EFAD5BB948496C706675EB578DEA1573A(__this, (bool)1, (bool)0, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void BestHTTP.Examples.HubWithAuthorizationSample::SetButtons(System.Boolean,System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void HubWithAuthorizationSample_SetButtons_m84ECF94EFAD5BB948496C706675EB578DEA1573A (HubWithAuthorizationSample_t19979B3628DD848910FD70011959F7CB115D5968 * __this, bool ___connect0, bool ___close1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// if (this._connectButton != null)
		Button_tA893FC15AB26E1439AC25BDCA7079530587BB65D * L_0 = __this->get__connectButton_14();
		IL2CPP_RUNTIME_CLASS_INIT(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		bool L_1;
		L_1 = Object_op_Inequality_mE1F187520BD83FB7D86A6D850710C4D42B864E90(L_0, (Object_tF2F3778131EFF286AF62B7B013A170F95A91571A *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_001a;
		}
	}
	{
		// this._connectButton.interactable = connect;
		Button_tA893FC15AB26E1439AC25BDCA7079530587BB65D * L_2 = __this->get__connectButton_14();
		bool L_3 = ___connect0;
		NullCheck(L_2);
		Selectable_set_interactable_mE6F57D33A9E0484377174D0F490C4372BF7F0D40(L_2, L_3, /*hidden argument*/NULL);
	}

IL_001a:
	{
		// if (this._closeButton != null)
		Button_tA893FC15AB26E1439AC25BDCA7079530587BB65D * L_4 = __this->get__closeButton_15();
		IL2CPP_RUNTIME_CLASS_INIT(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		bool L_5;
		L_5 = Object_op_Inequality_mE1F187520BD83FB7D86A6D850710C4D42B864E90(L_4, (Object_tF2F3778131EFF286AF62B7B013A170F95A91571A *)NULL, /*hidden argument*/NULL);
		if (!L_5)
		{
			goto IL_0034;
		}
	}
	{
		// this._closeButton.interactable = close;
		Button_tA893FC15AB26E1439AC25BDCA7079530587BB65D * L_6 = __this->get__closeButton_15();
		bool L_7 = ___close1;
		NullCheck(L_6);
		Selectable_set_interactable_mE6F57D33A9E0484377174D0F490C4372BF7F0D40(L_6, L_7, /*hidden argument*/NULL);
	}

IL_0034:
	{
		// }
		return;
	}
}
// BestHTTP.Examples.Helpers.TextListItem BestHTTP.Examples.HubWithAuthorizationSample::AddText(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR TextListItem_tC8FEA392C6AF65CFF22D8089B9104B79C84A6D8B * HubWithAuthorizationSample_AddText_m0E07BF05AEEB07DEFCBB4D7072BD1BDC73FCD8EE (HubWithAuthorizationSample_t19979B3628DD848910FD70011959F7CB115D5968 * __this, String_t* ___text0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&GUIHelper_t1BA8F4DF68223FC31CA8DDE367DF168C2ADAE16D_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// return GUIHelper.AddText(this._listItemPrefab, this._contentRoot, text, this._maxListItemEntries, this._scrollRect);
		TextListItem_tC8FEA392C6AF65CFF22D8089B9104B79C84A6D8B * L_0 = __this->get__listItemPrefab_12();
		RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 * L_1 = __this->get__contentRoot_11();
		String_t* L_2 = ___text0;
		int32_t L_3 = __this->get__maxListItemEntries_13();
		ScrollRect_tB16156010F89FFDAAB2127CA878608FD91B9FBEA * L_4 = __this->get__scrollRect_10();
		IL2CPP_RUNTIME_CLASS_INIT(GUIHelper_t1BA8F4DF68223FC31CA8DDE367DF168C2ADAE16D_il2cpp_TypeInfo_var);
		TextListItem_tC8FEA392C6AF65CFF22D8089B9104B79C84A6D8B * L_5;
		L_5 = GUIHelper_AddText_mAC779A2E1D44664A6F92A3CAD4AB26F8BFE3E56E(L_0, L_1, L_2, L_3, L_4, /*hidden argument*/NULL);
		return L_5;
	}
}
// System.Void BestHTTP.Examples.HubWithAuthorizationSample::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void HubWithAuthorizationSample__ctor_m228DDE83FC13E311625D691B1C49AE56D2AEC9EE (HubWithAuthorizationSample_t19979B3628DD848910FD70011959F7CB115D5968 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralA91BC6B7438D9FA67272371E212F4796D0F610FA);
		s_Il2CppMethodInitialized = true;
	}
	{
		// private string _path = "/redirect";
		__this->set__path_9(_stringLiteralA91BC6B7438D9FA67272371E212F4796D0F610FA);
		// private int _maxListItemEntries = 100;
		__this->set__maxListItemEntries_13(((int32_t)100));
		SampleBase__ctor_mBF0888BC18DA99E126CDA8D6221DF4CC54D93E22(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void BestHTTP.Examples.HubWithAuthorizationSample::<OnConnectButton>b__10_0(BestHTTP.SignalRCore.HubConnection,BestHTTP.SignalRCore.ITransport,BestHTTP.SignalRCore.TransportEvents)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void HubWithAuthorizationSample_U3COnConnectButtonU3Eb__10_0_m6D430BDDCAA15325F032C3A4B13E411D21DE5EBD (HubWithAuthorizationSample_t19979B3628DD848910FD70011959F7CB115D5968 * __this, HubConnection_t797D4973C9BBE6895D85BF743512ADA7D419736D * ___hub0, RuntimeObject* ___transport1, int32_t ___ev2, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&ITransport_t832D0F246CABAD20CB7410CC70ED972EF084053B_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&TransportEvents_tE38D2B53053334BE1B4419061B7701FF8E099D19_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&TransportTypes_t8387D0BDDE41AD7796AEFFC585944D24F9389FB0_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralABF1DBB99693496E8838B727F5C5E3D5D5ED72C8);
		s_Il2CppMethodInitialized = true;
	}
	{
		// hub.OnTransportEvent += (hub, transport, ev) => AddText(string.Format("Transport(<color=green>{0}</color>) event: <color=green>{1}</color>", transport.TransportType, ev));
		RuntimeObject* L_0 = ___transport1;
		NullCheck(L_0);
		int32_t L_1;
		L_1 = InterfaceFuncInvoker0< int32_t >::Invoke(1 /* BestHTTP.SignalRCore.TransportTypes BestHTTP.SignalRCore.ITransport::get_TransportType() */, ITransport_t832D0F246CABAD20CB7410CC70ED972EF084053B_il2cpp_TypeInfo_var, L_0);
		int32_t L_2 = L_1;
		RuntimeObject * L_3 = Box(TransportTypes_t8387D0BDDE41AD7796AEFFC585944D24F9389FB0_il2cpp_TypeInfo_var, &L_2);
		int32_t L_4 = ___ev2;
		int32_t L_5 = L_4;
		RuntimeObject * L_6 = Box(TransportEvents_tE38D2B53053334BE1B4419061B7701FF8E099D19_il2cpp_TypeInfo_var, &L_5);
		String_t* L_7;
		L_7 = String_Format_m8D1CB0410C35E052A53AE957C914C841E54BAB66(_stringLiteralABF1DBB99693496E8838B727F5C5E3D5D5ED72C8, L_3, L_6, /*hidden argument*/NULL);
		TextListItem_tC8FEA392C6AF65CFF22D8089B9104B79C84A6D8B * L_8;
		L_8 = HubWithAuthorizationSample_AddText_m0E07BF05AEEB07DEFCBB4D7072BD1BDC73FCD8EE(__this, L_7, /*hidden argument*/NULL);
		return;
	}
}
// System.Void BestHTTP.Examples.HubWithAuthorizationSample::<Hub_OnConnected>b__13_0(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void HubWithAuthorizationSample_U3CHub_OnConnectedU3Eb__13_0_m65C934096BC045F396D14A1CE2E283380F3E801A (HubWithAuthorizationSample_t19979B3628DD848910FD70011959F7CB115D5968 * __this, String_t* ___ret0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral6BAB476667C71E550B8BB632BE04ADDC98348F2D);
		s_Il2CppMethodInitialized = true;
	}
	{
		// .OnSuccess(ret => AddText(string.Format("'<color=green>Echo</color>' returned: '<color=yellow>{0}</color>'", ret)).AddLeftPadding(20));
		String_t* L_0 = ___ret0;
		String_t* L_1;
		L_1 = String_Format_mB3D38E5238C3164DB4D7D29339D9E225A4496D17(_stringLiteral6BAB476667C71E550B8BB632BE04ADDC98348F2D, L_0, /*hidden argument*/NULL);
		TextListItem_tC8FEA392C6AF65CFF22D8089B9104B79C84A6D8B * L_2;
		L_2 = HubWithAuthorizationSample_AddText_m0E07BF05AEEB07DEFCBB4D7072BD1BDC73FCD8EE(__this, L_1, /*hidden argument*/NULL);
		NullCheck(L_2);
		TextListItem_AddLeftPadding_m41C860ECCEF8C63672FB82BCB45198F313528C5F(L_2, ((int32_t)20), /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void BestHTTP.Examples.HubWithPreAuthorizationSample::Start()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void HubWithPreAuthorizationSample_Start_mA7A76A6D66C3DEA91ADF5227892D04AC2EC7B2A8 (HubWithPreAuthorizationSample_tDF799D602DC996BFF26C8E97175B4E64BF3807D8 * __this, const RuntimeMethod* method)
{
	{
		// base.Start();
		SampleBase_Start_m00468C0449F4FC1F2D15592407F5B41DF8E094AE(__this, /*hidden argument*/NULL);
		// SetButtons(true, false);
		HubWithPreAuthorizationSample_SetButtons_mF8733AB0AA545452F48E29183D53D0D3CDDD4035(__this, (bool)1, (bool)0, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void BestHTTP.Examples.HubWithPreAuthorizationSample::OnDestroy()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void HubWithPreAuthorizationSample_OnDestroy_m5EECA3AA334D73F814C6E961E0BFD30410068537 (HubWithPreAuthorizationSample_tDF799D602DC996BFF26C8E97175B4E64BF3807D8 * __this, const RuntimeMethod* method)
{
	{
		// if (hub != null)
		HubConnection_t797D4973C9BBE6895D85BF743512ADA7D419736D * L_0 = __this->get_hub_17();
		if (!L_0)
		{
			goto IL_0013;
		}
	}
	{
		// hub.StartClose();
		HubConnection_t797D4973C9BBE6895D85BF743512ADA7D419736D * L_1 = __this->get_hub_17();
		NullCheck(L_1);
		HubConnection_StartClose_mD0F78FCA41866D4E763C83A4E2280B84B3A1082D(L_1, /*hidden argument*/NULL);
	}

IL_0013:
	{
		// }
		return;
	}
}
// System.Void BestHTTP.Examples.HubWithPreAuthorizationSample::OnConnectButton()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void HubWithPreAuthorizationSample_OnConnectButton_m412B29CE0E979BFC914D7D04274C71994CAE58AE (HubWithPreAuthorizationSample_tDF799D602DC996BFF26C8E97175B4E64BF3807D8 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Action_1__ctor_mF3C29B87B57748F26952F077111724B7F8728AD8_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Action_1_tD3410B67BFA3C1FCF5D3D8345C933F0ACA7A5426_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Action_2__ctor_m9E9D0A4856BE79EC7CCD28D4A14DC05BB0BB1CBE_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Action_2_t36E73E35B8C2732CDF2877AFC5E932F8910CD2A5_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Action_3__ctor_m4CBDE8196ACBD5567E04A96C3714BF3D6C29F818_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Action_3_t2A25F65DF18A1F5038436F84891B59501F1E97B6_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&HubConnection_t797D4973C9BBE6895D85BF743512ADA7D419736D_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&HubWithPreAuthorizationSample_AuthenticationProvider_OnAuthenticationFailed_m10A16F3C48FF2D02BF7203E2B70415F6FF185BE3_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&HubWithPreAuthorizationSample_AuthenticationProvider_OnAuthenticationSucceded_m4112BE89F0E1497A6C2E8CEFEDFF45DA075DDDFD_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&HubWithPreAuthorizationSample_Hub_OnClosed_m70E27F329CD5D47268228528DEC9B130B77997AF_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&HubWithPreAuthorizationSample_Hub_OnConnected_m70A6CC492CCC83DCF659FDB088D2881C276A7A3A_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&HubWithPreAuthorizationSample_Hub_OnError_m16BE097913A88F543907D908F64B234B15F9F87A_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&HubWithPreAuthorizationSample_U3COnConnectButtonU3Eb__11_0_m6CC61349D8E7D060103AD3D5AA813DDD28A871C4_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&IAuthenticationProvider_tE57312D246360017364A3ED93CF4AE0BCC123517_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&JsonProtocol_t0DA0EB80CD537920D655CE3D790C87A7035A7635_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&LitJsonEncoder_t93D5E7144163028B776CFC2472958FD241FDAEC7_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&OnAuthenticationFailedDelegate_tB415D1478EDFF7309A953E24BD13797DEFAAF9B2_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&OnAuthenticationSuccededDelegate_t7A96699C45F6100206650225F8875DCAC1A8DE46_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&PreAuthAccessTokenAuthenticator_t424A1F277DA9B16ADCB16A5B9A7659E4B5DC6162_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Uri_t4A915E1CC15B2C650F478099AD448E9466CBF612_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral94A3AB9B36FB187F4215E62357DFFBDC6B930029);
		s_Il2CppMethodInitialized = true;
	}
	RuntimeObject* V_0 = NULL;
	{
		// IProtocol protocol = null;
		V_0 = (RuntimeObject*)NULL;
		// protocol = new JsonProtocol(new LitJsonEncoder());
		LitJsonEncoder_t93D5E7144163028B776CFC2472958FD241FDAEC7 * L_0 = (LitJsonEncoder_t93D5E7144163028B776CFC2472958FD241FDAEC7 *)il2cpp_codegen_object_new(LitJsonEncoder_t93D5E7144163028B776CFC2472958FD241FDAEC7_il2cpp_TypeInfo_var);
		LitJsonEncoder__ctor_m4832CB769DD2810EAF328862A46B448D2F170FA2(L_0, /*hidden argument*/NULL);
		JsonProtocol_t0DA0EB80CD537920D655CE3D790C87A7035A7635 * L_1 = (JsonProtocol_t0DA0EB80CD537920D655CE3D790C87A7035A7635 *)il2cpp_codegen_object_new(JsonProtocol_t0DA0EB80CD537920D655CE3D790C87A7035A7635_il2cpp_TypeInfo_var);
		JsonProtocol__ctor_m8227721C2A25FA8E7749C10D2152C1701EB5876B(L_1, L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		// hub = new HubConnection(new Uri(base.sampleSelector.BaseURL + this._hubPath), protocol);
		SampleRoot_tB11D3D3067248A62081D6D1A6F5936A1A3D856E1 * L_2 = ((SampleBase_tC30950FFCCCE8696B341569EEE364BF4C4DB7157 *)__this)->get_sampleSelector_8();
		NullCheck(L_2);
		String_t* L_3 = L_2->get_BaseURL_4();
		String_t* L_4 = __this->get__hubPath_9();
		String_t* L_5;
		L_5 = String_Concat_m4B4AB72618348C5DFBFBA8DED84B9E2EBDB55E1B(L_3, L_4, /*hidden argument*/NULL);
		Uri_t4A915E1CC15B2C650F478099AD448E9466CBF612 * L_6 = (Uri_t4A915E1CC15B2C650F478099AD448E9466CBF612 *)il2cpp_codegen_object_new(Uri_t4A915E1CC15B2C650F478099AD448E9466CBF612_il2cpp_TypeInfo_var);
		Uri__ctor_m7724F43B1525624FFF97A774B6B909B075714D5C(L_6, L_5, /*hidden argument*/NULL);
		RuntimeObject* L_7 = V_0;
		HubConnection_t797D4973C9BBE6895D85BF743512ADA7D419736D * L_8 = (HubConnection_t797D4973C9BBE6895D85BF743512ADA7D419736D *)il2cpp_codegen_object_new(HubConnection_t797D4973C9BBE6895D85BF743512ADA7D419736D_il2cpp_TypeInfo_var);
		HubConnection__ctor_m488859A04D0B53DFC278E28AEE4AADFF879F401D(L_8, L_6, L_7, /*hidden argument*/NULL);
		__this->set_hub_17(L_8);
		// hub.AuthenticationProvider = new PreAuthAccessTokenAuthenticator(new Uri(base.sampleSelector.BaseURL + this._jwtTokenPath));
		HubConnection_t797D4973C9BBE6895D85BF743512ADA7D419736D * L_9 = __this->get_hub_17();
		SampleRoot_tB11D3D3067248A62081D6D1A6F5936A1A3D856E1 * L_10 = ((SampleBase_tC30950FFCCCE8696B341569EEE364BF4C4DB7157 *)__this)->get_sampleSelector_8();
		NullCheck(L_10);
		String_t* L_11 = L_10->get_BaseURL_4();
		String_t* L_12 = __this->get__jwtTokenPath_10();
		String_t* L_13;
		L_13 = String_Concat_m4B4AB72618348C5DFBFBA8DED84B9E2EBDB55E1B(L_11, L_12, /*hidden argument*/NULL);
		Uri_t4A915E1CC15B2C650F478099AD448E9466CBF612 * L_14 = (Uri_t4A915E1CC15B2C650F478099AD448E9466CBF612 *)il2cpp_codegen_object_new(Uri_t4A915E1CC15B2C650F478099AD448E9466CBF612_il2cpp_TypeInfo_var);
		Uri__ctor_m7724F43B1525624FFF97A774B6B909B075714D5C(L_14, L_13, /*hidden argument*/NULL);
		PreAuthAccessTokenAuthenticator_t424A1F277DA9B16ADCB16A5B9A7659E4B5DC6162 * L_15 = (PreAuthAccessTokenAuthenticator_t424A1F277DA9B16ADCB16A5B9A7659E4B5DC6162 *)il2cpp_codegen_object_new(PreAuthAccessTokenAuthenticator_t424A1F277DA9B16ADCB16A5B9A7659E4B5DC6162_il2cpp_TypeInfo_var);
		PreAuthAccessTokenAuthenticator__ctor_mAAF19DED5CA60BF7BC0C5B13BDA11DA4B8784F17(L_15, L_14, /*hidden argument*/NULL);
		NullCheck(L_9);
		HubConnection_set_AuthenticationProvider_mB2FE128F4FC37EDDA68714B81AE5A861B3681C68_inline(L_9, L_15, /*hidden argument*/NULL);
		// hub.AuthenticationProvider.OnAuthenticationSucceded += AuthenticationProvider_OnAuthenticationSucceded;
		HubConnection_t797D4973C9BBE6895D85BF743512ADA7D419736D * L_16 = __this->get_hub_17();
		NullCheck(L_16);
		RuntimeObject* L_17;
		L_17 = HubConnection_get_AuthenticationProvider_m136DD5105ABD5D51D8068992CD2FB5D0BD9F2BE3_inline(L_16, /*hidden argument*/NULL);
		OnAuthenticationSuccededDelegate_t7A96699C45F6100206650225F8875DCAC1A8DE46 * L_18 = (OnAuthenticationSuccededDelegate_t7A96699C45F6100206650225F8875DCAC1A8DE46 *)il2cpp_codegen_object_new(OnAuthenticationSuccededDelegate_t7A96699C45F6100206650225F8875DCAC1A8DE46_il2cpp_TypeInfo_var);
		OnAuthenticationSuccededDelegate__ctor_mC383B649F658A751643D66BB98847A914F53C8CA(L_18, __this, (intptr_t)((intptr_t)HubWithPreAuthorizationSample_AuthenticationProvider_OnAuthenticationSucceded_m4112BE89F0E1497A6C2E8CEFEDFF45DA075DDDFD_RuntimeMethod_var), /*hidden argument*/NULL);
		NullCheck(L_17);
		InterfaceActionInvoker1< OnAuthenticationSuccededDelegate_t7A96699C45F6100206650225F8875DCAC1A8DE46 * >::Invoke(1 /* System.Void BestHTTP.SignalRCore.IAuthenticationProvider::add_OnAuthenticationSucceded(BestHTTP.SignalRCore.OnAuthenticationSuccededDelegate) */, IAuthenticationProvider_tE57312D246360017364A3ED93CF4AE0BCC123517_il2cpp_TypeInfo_var, L_17, L_18);
		// hub.AuthenticationProvider.OnAuthenticationFailed += AuthenticationProvider_OnAuthenticationFailed;
		HubConnection_t797D4973C9BBE6895D85BF743512ADA7D419736D * L_19 = __this->get_hub_17();
		NullCheck(L_19);
		RuntimeObject* L_20;
		L_20 = HubConnection_get_AuthenticationProvider_m136DD5105ABD5D51D8068992CD2FB5D0BD9F2BE3_inline(L_19, /*hidden argument*/NULL);
		OnAuthenticationFailedDelegate_tB415D1478EDFF7309A953E24BD13797DEFAAF9B2 * L_21 = (OnAuthenticationFailedDelegate_tB415D1478EDFF7309A953E24BD13797DEFAAF9B2 *)il2cpp_codegen_object_new(OnAuthenticationFailedDelegate_tB415D1478EDFF7309A953E24BD13797DEFAAF9B2_il2cpp_TypeInfo_var);
		OnAuthenticationFailedDelegate__ctor_m99752AE3FAFEE6DB1CCD7F0666AEEF16FF6D62F4(L_21, __this, (intptr_t)((intptr_t)HubWithPreAuthorizationSample_AuthenticationProvider_OnAuthenticationFailed_m10A16F3C48FF2D02BF7203E2B70415F6FF185BE3_RuntimeMethod_var), /*hidden argument*/NULL);
		NullCheck(L_20);
		InterfaceActionInvoker1< OnAuthenticationFailedDelegate_tB415D1478EDFF7309A953E24BD13797DEFAAF9B2 * >::Invoke(3 /* System.Void BestHTTP.SignalRCore.IAuthenticationProvider::add_OnAuthenticationFailed(BestHTTP.SignalRCore.OnAuthenticationFailedDelegate) */, IAuthenticationProvider_tE57312D246360017364A3ED93CF4AE0BCC123517_il2cpp_TypeInfo_var, L_20, L_21);
		// hub.OnConnected += Hub_OnConnected;
		HubConnection_t797D4973C9BBE6895D85BF743512ADA7D419736D * L_22 = __this->get_hub_17();
		Action_1_tD3410B67BFA3C1FCF5D3D8345C933F0ACA7A5426 * L_23 = (Action_1_tD3410B67BFA3C1FCF5D3D8345C933F0ACA7A5426 *)il2cpp_codegen_object_new(Action_1_tD3410B67BFA3C1FCF5D3D8345C933F0ACA7A5426_il2cpp_TypeInfo_var);
		Action_1__ctor_mF3C29B87B57748F26952F077111724B7F8728AD8(L_23, __this, (intptr_t)((intptr_t)HubWithPreAuthorizationSample_Hub_OnConnected_m70A6CC492CCC83DCF659FDB088D2881C276A7A3A_RuntimeMethod_var), /*hidden argument*/Action_1__ctor_mF3C29B87B57748F26952F077111724B7F8728AD8_RuntimeMethod_var);
		NullCheck(L_22);
		HubConnection_add_OnConnected_mF853CCA78B6716068BD0F6328605DE4956F852FF(L_22, L_23, /*hidden argument*/NULL);
		// hub.OnError += Hub_OnError;
		HubConnection_t797D4973C9BBE6895D85BF743512ADA7D419736D * L_24 = __this->get_hub_17();
		Action_2_t36E73E35B8C2732CDF2877AFC5E932F8910CD2A5 * L_25 = (Action_2_t36E73E35B8C2732CDF2877AFC5E932F8910CD2A5 *)il2cpp_codegen_object_new(Action_2_t36E73E35B8C2732CDF2877AFC5E932F8910CD2A5_il2cpp_TypeInfo_var);
		Action_2__ctor_m9E9D0A4856BE79EC7CCD28D4A14DC05BB0BB1CBE(L_25, __this, (intptr_t)((intptr_t)HubWithPreAuthorizationSample_Hub_OnError_m16BE097913A88F543907D908F64B234B15F9F87A_RuntimeMethod_var), /*hidden argument*/Action_2__ctor_m9E9D0A4856BE79EC7CCD28D4A14DC05BB0BB1CBE_RuntimeMethod_var);
		NullCheck(L_24);
		HubConnection_add_OnError_m118C02C801BDA2C9E869B658BA1A023E68EF0871(L_24, L_25, /*hidden argument*/NULL);
		// hub.OnClosed += Hub_OnClosed;
		HubConnection_t797D4973C9BBE6895D85BF743512ADA7D419736D * L_26 = __this->get_hub_17();
		Action_1_tD3410B67BFA3C1FCF5D3D8345C933F0ACA7A5426 * L_27 = (Action_1_tD3410B67BFA3C1FCF5D3D8345C933F0ACA7A5426 *)il2cpp_codegen_object_new(Action_1_tD3410B67BFA3C1FCF5D3D8345C933F0ACA7A5426_il2cpp_TypeInfo_var);
		Action_1__ctor_mF3C29B87B57748F26952F077111724B7F8728AD8(L_27, __this, (intptr_t)((intptr_t)HubWithPreAuthorizationSample_Hub_OnClosed_m70E27F329CD5D47268228528DEC9B130B77997AF_RuntimeMethod_var), /*hidden argument*/Action_1__ctor_mF3C29B87B57748F26952F077111724B7F8728AD8_RuntimeMethod_var);
		NullCheck(L_26);
		HubConnection_add_OnClosed_m2C9F0587D49C73063523EFB7DC18D45847C311C5(L_26, L_27, /*hidden argument*/NULL);
		// hub.OnTransportEvent += (hub, transport, ev) => AddText(string.Format("Transport(<color=green>{0}</color>) event: <color=green>{1}</color>", transport.TransportType, ev));
		HubConnection_t797D4973C9BBE6895D85BF743512ADA7D419736D * L_28 = __this->get_hub_17();
		Action_3_t2A25F65DF18A1F5038436F84891B59501F1E97B6 * L_29 = (Action_3_t2A25F65DF18A1F5038436F84891B59501F1E97B6 *)il2cpp_codegen_object_new(Action_3_t2A25F65DF18A1F5038436F84891B59501F1E97B6_il2cpp_TypeInfo_var);
		Action_3__ctor_m4CBDE8196ACBD5567E04A96C3714BF3D6C29F818(L_29, __this, (intptr_t)((intptr_t)HubWithPreAuthorizationSample_U3COnConnectButtonU3Eb__11_0_m6CC61349D8E7D060103AD3D5AA813DDD28A871C4_RuntimeMethod_var), /*hidden argument*/Action_3__ctor_m4CBDE8196ACBD5567E04A96C3714BF3D6C29F818_RuntimeMethod_var);
		NullCheck(L_28);
		HubConnection_add_OnTransportEvent_mBB4C43B11D8BB500C39F28370A602638E1ADB4F4(L_28, L_29, /*hidden argument*/NULL);
		// hub.StartConnect();
		HubConnection_t797D4973C9BBE6895D85BF743512ADA7D419736D * L_30 = __this->get_hub_17();
		NullCheck(L_30);
		HubConnection_StartConnect_m63E5C170518ECC28C2E038BAF8407CD13FE30219(L_30, /*hidden argument*/NULL);
		// AddText("StartConnect called");
		TextListItem_tC8FEA392C6AF65CFF22D8089B9104B79C84A6D8B * L_31;
		L_31 = HubWithPreAuthorizationSample_AddText_m23B3D8EE9963A7D7ED1F6FD1655F0B0619542EF3(__this, _stringLiteral94A3AB9B36FB187F4215E62357DFFBDC6B930029, /*hidden argument*/NULL);
		// SetButtons(false, false);
		HubWithPreAuthorizationSample_SetButtons_mF8733AB0AA545452F48E29183D53D0D3CDDD4035(__this, (bool)0, (bool)0, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void BestHTTP.Examples.HubWithPreAuthorizationSample::OnCloseButton()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void HubWithPreAuthorizationSample_OnCloseButton_m5894D41DB295C931BB156122761DD6BB9D4A55B7 (HubWithPreAuthorizationSample_tDF799D602DC996BFF26C8E97175B4E64BF3807D8 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralCB58925B56DD017047A25D2F36CB1BCA26F68FC3);
		s_Il2CppMethodInitialized = true;
	}
	{
		// if (hub != null)
		HubConnection_t797D4973C9BBE6895D85BF743512ADA7D419736D * L_0 = __this->get_hub_17();
		if (!L_0)
		{
			goto IL_0027;
		}
	}
	{
		// hub.StartClose();
		HubConnection_t797D4973C9BBE6895D85BF743512ADA7D419736D * L_1 = __this->get_hub_17();
		NullCheck(L_1);
		HubConnection_StartClose_mD0F78FCA41866D4E763C83A4E2280B84B3A1082D(L_1, /*hidden argument*/NULL);
		// AddText("StartClose called");
		TextListItem_tC8FEA392C6AF65CFF22D8089B9104B79C84A6D8B * L_2;
		L_2 = HubWithPreAuthorizationSample_AddText_m23B3D8EE9963A7D7ED1F6FD1655F0B0619542EF3(__this, _stringLiteralCB58925B56DD017047A25D2F36CB1BCA26F68FC3, /*hidden argument*/NULL);
		// SetButtons(false, false);
		HubWithPreAuthorizationSample_SetButtons_mF8733AB0AA545452F48E29183D53D0D3CDDD4035(__this, (bool)0, (bool)0, /*hidden argument*/NULL);
	}

IL_0027:
	{
		// }
		return;
	}
}
// System.Void BestHTTP.Examples.HubWithPreAuthorizationSample::AuthenticationProvider_OnAuthenticationSucceded(BestHTTP.SignalRCore.IAuthenticationProvider)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void HubWithPreAuthorizationSample_AuthenticationProvider_OnAuthenticationSucceded_m4112BE89F0E1497A6C2E8CEFEDFF45DA075DDDFD (HubWithPreAuthorizationSample_tDF799D602DC996BFF26C8E97175B4E64BF3807D8 * __this, RuntimeObject* ___provider0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&PreAuthAccessTokenAuthenticator_t424A1F277DA9B16ADCB16A5B9A7659E4B5DC6162_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral0D092D548D913C794FEC0976BBA5DAF67E0F36F2);
		s_Il2CppMethodInitialized = true;
	}
	String_t* V_0 = NULL;
	{
		// string str = string.Format("Pre-Authentication Succeded! Token: '<color=green>{0}</color>' ", (hub.AuthenticationProvider as PreAuthAccessTokenAuthenticator).Token);
		HubConnection_t797D4973C9BBE6895D85BF743512ADA7D419736D * L_0 = __this->get_hub_17();
		NullCheck(L_0);
		RuntimeObject* L_1;
		L_1 = HubConnection_get_AuthenticationProvider_m136DD5105ABD5D51D8068992CD2FB5D0BD9F2BE3_inline(L_0, /*hidden argument*/NULL);
		NullCheck(((PreAuthAccessTokenAuthenticator_t424A1F277DA9B16ADCB16A5B9A7659E4B5DC6162 *)IsInstSealed((RuntimeObject*)L_1, PreAuthAccessTokenAuthenticator_t424A1F277DA9B16ADCB16A5B9A7659E4B5DC6162_il2cpp_TypeInfo_var)));
		String_t* L_2;
		L_2 = PreAuthAccessTokenAuthenticator_get_Token_m168ED3A5300106BB0EF7FC10D9DE2D666FAFCF82_inline(((PreAuthAccessTokenAuthenticator_t424A1F277DA9B16ADCB16A5B9A7659E4B5DC6162 *)IsInstSealed((RuntimeObject*)L_1, PreAuthAccessTokenAuthenticator_t424A1F277DA9B16ADCB16A5B9A7659E4B5DC6162_il2cpp_TypeInfo_var)), /*hidden argument*/NULL);
		String_t* L_3;
		L_3 = String_Format_mB3D38E5238C3164DB4D7D29339D9E225A4496D17(_stringLiteral0D092D548D913C794FEC0976BBA5DAF67E0F36F2, L_2, /*hidden argument*/NULL);
		V_0 = L_3;
		// AddText(str);
		String_t* L_4 = V_0;
		TextListItem_tC8FEA392C6AF65CFF22D8089B9104B79C84A6D8B * L_5;
		L_5 = HubWithPreAuthorizationSample_AddText_m23B3D8EE9963A7D7ED1F6FD1655F0B0619542EF3(__this, L_4, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void BestHTTP.Examples.HubWithPreAuthorizationSample::AuthenticationProvider_OnAuthenticationFailed(BestHTTP.SignalRCore.IAuthenticationProvider,System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void HubWithPreAuthorizationSample_AuthenticationProvider_OnAuthenticationFailed_m10A16F3C48FF2D02BF7203E2B70415F6FF185BE3 (HubWithPreAuthorizationSample_tDF799D602DC996BFF26C8E97175B4E64BF3807D8 * __this, RuntimeObject* ___provider0, String_t* ___reason1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralE7C5667AC424C541B18739402C56AA560254BB74);
		s_Il2CppMethodInitialized = true;
	}
	{
		// AddText(string.Format("Authentication Failed! Reason: '{0}'", reason));
		String_t* L_0 = ___reason1;
		String_t* L_1;
		L_1 = String_Format_mB3D38E5238C3164DB4D7D29339D9E225A4496D17(_stringLiteralE7C5667AC424C541B18739402C56AA560254BB74, L_0, /*hidden argument*/NULL);
		TextListItem_tC8FEA392C6AF65CFF22D8089B9104B79C84A6D8B * L_2;
		L_2 = HubWithPreAuthorizationSample_AddText_m23B3D8EE9963A7D7ED1F6FD1655F0B0619542EF3(__this, L_1, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void BestHTTP.Examples.HubWithPreAuthorizationSample::Hub_OnConnected(BestHTTP.SignalRCore.HubConnection)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void HubWithPreAuthorizationSample_Hub_OnConnected_m70A6CC492CCC83DCF659FDB088D2881C276A7A3A (HubWithPreAuthorizationSample_tDF799D602DC996BFF26C8E97175B4E64BF3807D8 * __this, HubConnection_t797D4973C9BBE6895D85BF743512ADA7D419736D * ___hub0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&FutureValueCallback_1__ctor_mC0AAA3F5D464DBA95A0F1604DDD4376A568D8E75_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&FutureValueCallback_1_t31AE88E9DCB927E5F16747DB54C827FE97F91BBF_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&HubConnection_Invoke_TisString_t_mC092761B744F2CFDEBB042CD081359F0E51B8F96_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&HubWithPreAuthorizationSample_U3CHub_OnConnectedU3Eb__15_0_m11819123816AA7FA753584C7A0B140A4CB7FA660_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&IFuture_1_tEBD463543458CE8C841D19C91EAC2683130EC31D_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&IProtocol_tFE0A86EE25C0427642F1B825BD8ADA84E8CD20CD_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&ITransport_t832D0F246CABAD20CB7410CC70ED972EF084053B_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&TransportTypes_t8387D0BDDE41AD7796AEFFC585944D24F9389FB0_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral3177C3549DF7103EBF6FAD9F320123D3A8D97BE7);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral450C5E3FC3E837EBBE92288DB9DE76195D7CF985);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral519A627D101D9C5AE7C965506A4856184ACCD31F);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralB82244460F050E4F7E423A7000AE2646F10EF257);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	{
		// AddText(string.Format("Hub Connected with <color=green>{0}</color> transport using the <color=green>{1}</color> encoder.", hub.Transport.TransportType.ToString(), hub.Protocol.Name));
		HubConnection_t797D4973C9BBE6895D85BF743512ADA7D419736D * L_0 = ___hub0;
		NullCheck(L_0);
		RuntimeObject* L_1;
		L_1 = HubConnection_get_Transport_m1C10051EF86E5ED6DF6672BC9366D0C3DFDD166D_inline(L_0, /*hidden argument*/NULL);
		NullCheck(L_1);
		int32_t L_2;
		L_2 = InterfaceFuncInvoker0< int32_t >::Invoke(1 /* BestHTTP.SignalRCore.TransportTypes BestHTTP.SignalRCore.ITransport::get_TransportType() */, ITransport_t832D0F246CABAD20CB7410CC70ED972EF084053B_il2cpp_TypeInfo_var, L_1);
		V_0 = L_2;
		RuntimeObject * L_3 = Box(TransportTypes_t8387D0BDDE41AD7796AEFFC585944D24F9389FB0_il2cpp_TypeInfo_var, (&V_0));
		NullCheck(L_3);
		String_t* L_4;
		L_4 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_3);
		V_0 = *(int32_t*)UnBox(L_3);
		HubConnection_t797D4973C9BBE6895D85BF743512ADA7D419736D * L_5 = ___hub0;
		NullCheck(L_5);
		RuntimeObject* L_6;
		L_6 = HubConnection_get_Protocol_mD9367B3CDFA166D9C0F7EE4671B02E6A61F839D9_inline(L_5, /*hidden argument*/NULL);
		NullCheck(L_6);
		String_t* L_7;
		L_7 = InterfaceFuncInvoker0< String_t* >::Invoke(0 /* System.String BestHTTP.SignalRCore.IProtocol::get_Name() */, IProtocol_tFE0A86EE25C0427642F1B825BD8ADA84E8CD20CD_il2cpp_TypeInfo_var, L_6);
		String_t* L_8;
		L_8 = String_Format_m8D1CB0410C35E052A53AE957C914C841E54BAB66(_stringLiteral519A627D101D9C5AE7C965506A4856184ACCD31F, L_4, L_7, /*hidden argument*/NULL);
		TextListItem_tC8FEA392C6AF65CFF22D8089B9104B79C84A6D8B * L_9;
		L_9 = HubWithPreAuthorizationSample_AddText_m23B3D8EE9963A7D7ED1F6FD1655F0B0619542EF3(__this, L_8, /*hidden argument*/NULL);
		// SetButtons(false, true);
		HubWithPreAuthorizationSample_SetButtons_mF8733AB0AA545452F48E29183D53D0D3CDDD4035(__this, (bool)0, (bool)1, /*hidden argument*/NULL);
		// hub.Invoke<string>("Echo", "Message from the client")
		//     .OnSuccess(ret => AddText(string.Format("'<color=green>Echo</color>' returned: '<color=yellow>{0}</color>'", ret)).AddLeftPadding(20));
		HubConnection_t797D4973C9BBE6895D85BF743512ADA7D419736D * L_10 = ___hub0;
		ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE* L_11 = (ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE*)(ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE*)SZArrayNew(ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE_il2cpp_TypeInfo_var, (uint32_t)1);
		ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE* L_12 = L_11;
		NullCheck(L_12);
		ArrayElementTypeCheck (L_12, _stringLiteral3177C3549DF7103EBF6FAD9F320123D3A8D97BE7);
		(L_12)->SetAt(static_cast<il2cpp_array_size_t>(0), (RuntimeObject *)_stringLiteral3177C3549DF7103EBF6FAD9F320123D3A8D97BE7);
		NullCheck(L_10);
		RuntimeObject* L_13;
		L_13 = HubConnection_Invoke_TisString_t_mC092761B744F2CFDEBB042CD081359F0E51B8F96(L_10, _stringLiteralB82244460F050E4F7E423A7000AE2646F10EF257, L_12, /*hidden argument*/HubConnection_Invoke_TisString_t_mC092761B744F2CFDEBB042CD081359F0E51B8F96_RuntimeMethod_var);
		FutureValueCallback_1_t31AE88E9DCB927E5F16747DB54C827FE97F91BBF * L_14 = (FutureValueCallback_1_t31AE88E9DCB927E5F16747DB54C827FE97F91BBF *)il2cpp_codegen_object_new(FutureValueCallback_1_t31AE88E9DCB927E5F16747DB54C827FE97F91BBF_il2cpp_TypeInfo_var);
		FutureValueCallback_1__ctor_mC0AAA3F5D464DBA95A0F1604DDD4376A568D8E75(L_14, __this, (intptr_t)((intptr_t)HubWithPreAuthorizationSample_U3CHub_OnConnectedU3Eb__15_0_m11819123816AA7FA753584C7A0B140A4CB7FA660_RuntimeMethod_var), /*hidden argument*/FutureValueCallback_1__ctor_mC0AAA3F5D464DBA95A0F1604DDD4376A568D8E75_RuntimeMethod_var);
		NullCheck(L_13);
		RuntimeObject* L_15;
		L_15 = InterfaceFuncInvoker1< RuntimeObject*, FutureValueCallback_1_t31AE88E9DCB927E5F16747DB54C827FE97F91BBF * >::Invoke(4 /* BestHTTP.Futures.IFuture`1<T> BestHTTP.Futures.IFuture`1<System.String>::OnSuccess(BestHTTP.Futures.FutureValueCallback`1<T>) */, IFuture_1_tEBD463543458CE8C841D19C91EAC2683130EC31D_il2cpp_TypeInfo_var, L_13, L_14);
		// AddText("'<color=green>Message from the client</color>' sent!")
		//     .AddLeftPadding(20);
		TextListItem_tC8FEA392C6AF65CFF22D8089B9104B79C84A6D8B * L_16;
		L_16 = HubWithPreAuthorizationSample_AddText_m23B3D8EE9963A7D7ED1F6FD1655F0B0619542EF3(__this, _stringLiteral450C5E3FC3E837EBBE92288DB9DE76195D7CF985, /*hidden argument*/NULL);
		NullCheck(L_16);
		TextListItem_AddLeftPadding_m41C860ECCEF8C63672FB82BCB45198F313528C5F(L_16, ((int32_t)20), /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void BestHTTP.Examples.HubWithPreAuthorizationSample::Hub_OnClosed(BestHTTP.SignalRCore.HubConnection)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void HubWithPreAuthorizationSample_Hub_OnClosed_m70E27F329CD5D47268228528DEC9B130B77997AF (HubWithPreAuthorizationSample_tDF799D602DC996BFF26C8E97175B4E64BF3807D8 * __this, HubConnection_t797D4973C9BBE6895D85BF743512ADA7D419736D * ___hub0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral5904D6D202BF91354FCA994B64879EAF9DEA8268);
		s_Il2CppMethodInitialized = true;
	}
	{
		// AddText("Hub Closed");
		TextListItem_tC8FEA392C6AF65CFF22D8089B9104B79C84A6D8B * L_0;
		L_0 = HubWithPreAuthorizationSample_AddText_m23B3D8EE9963A7D7ED1F6FD1655F0B0619542EF3(__this, _stringLiteral5904D6D202BF91354FCA994B64879EAF9DEA8268, /*hidden argument*/NULL);
		// SetButtons(true, false);
		HubWithPreAuthorizationSample_SetButtons_mF8733AB0AA545452F48E29183D53D0D3CDDD4035(__this, (bool)1, (bool)0, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void BestHTTP.Examples.HubWithPreAuthorizationSample::Hub_OnError(BestHTTP.SignalRCore.HubConnection,System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void HubWithPreAuthorizationSample_Hub_OnError_m16BE097913A88F543907D908F64B234B15F9F87A (HubWithPreAuthorizationSample_tDF799D602DC996BFF26C8E97175B4E64BF3807D8 * __this, HubConnection_t797D4973C9BBE6895D85BF743512ADA7D419736D * ___hub0, String_t* ___error1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral0916CABAB7E3E45364FFC1B36C761FA2ABC7FF8E);
		s_Il2CppMethodInitialized = true;
	}
	{
		// AddText(string.Format("Hub Error: <color=red>{0}</color>", error));
		String_t* L_0 = ___error1;
		String_t* L_1;
		L_1 = String_Format_mB3D38E5238C3164DB4D7D29339D9E225A4496D17(_stringLiteral0916CABAB7E3E45364FFC1B36C761FA2ABC7FF8E, L_0, /*hidden argument*/NULL);
		TextListItem_tC8FEA392C6AF65CFF22D8089B9104B79C84A6D8B * L_2;
		L_2 = HubWithPreAuthorizationSample_AddText_m23B3D8EE9963A7D7ED1F6FD1655F0B0619542EF3(__this, L_1, /*hidden argument*/NULL);
		// SetButtons(true, false);
		HubWithPreAuthorizationSample_SetButtons_mF8733AB0AA545452F48E29183D53D0D3CDDD4035(__this, (bool)1, (bool)0, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void BestHTTP.Examples.HubWithPreAuthorizationSample::SetButtons(System.Boolean,System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void HubWithPreAuthorizationSample_SetButtons_mF8733AB0AA545452F48E29183D53D0D3CDDD4035 (HubWithPreAuthorizationSample_tDF799D602DC996BFF26C8E97175B4E64BF3807D8 * __this, bool ___connect0, bool ___close1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// if (this._connectButton != null)
		Button_tA893FC15AB26E1439AC25BDCA7079530587BB65D * L_0 = __this->get__connectButton_15();
		IL2CPP_RUNTIME_CLASS_INIT(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		bool L_1;
		L_1 = Object_op_Inequality_mE1F187520BD83FB7D86A6D850710C4D42B864E90(L_0, (Object_tF2F3778131EFF286AF62B7B013A170F95A91571A *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_001a;
		}
	}
	{
		// this._connectButton.interactable = connect;
		Button_tA893FC15AB26E1439AC25BDCA7079530587BB65D * L_2 = __this->get__connectButton_15();
		bool L_3 = ___connect0;
		NullCheck(L_2);
		Selectable_set_interactable_mE6F57D33A9E0484377174D0F490C4372BF7F0D40(L_2, L_3, /*hidden argument*/NULL);
	}

IL_001a:
	{
		// if (this._closeButton != null)
		Button_tA893FC15AB26E1439AC25BDCA7079530587BB65D * L_4 = __this->get__closeButton_16();
		IL2CPP_RUNTIME_CLASS_INIT(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		bool L_5;
		L_5 = Object_op_Inequality_mE1F187520BD83FB7D86A6D850710C4D42B864E90(L_4, (Object_tF2F3778131EFF286AF62B7B013A170F95A91571A *)NULL, /*hidden argument*/NULL);
		if (!L_5)
		{
			goto IL_0034;
		}
	}
	{
		// this._closeButton.interactable = close;
		Button_tA893FC15AB26E1439AC25BDCA7079530587BB65D * L_6 = __this->get__closeButton_16();
		bool L_7 = ___close1;
		NullCheck(L_6);
		Selectable_set_interactable_mE6F57D33A9E0484377174D0F490C4372BF7F0D40(L_6, L_7, /*hidden argument*/NULL);
	}

IL_0034:
	{
		// }
		return;
	}
}
// BestHTTP.Examples.Helpers.TextListItem BestHTTP.Examples.HubWithPreAuthorizationSample::AddText(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR TextListItem_tC8FEA392C6AF65CFF22D8089B9104B79C84A6D8B * HubWithPreAuthorizationSample_AddText_m23B3D8EE9963A7D7ED1F6FD1655F0B0619542EF3 (HubWithPreAuthorizationSample_tDF799D602DC996BFF26C8E97175B4E64BF3807D8 * __this, String_t* ___text0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&GUIHelper_t1BA8F4DF68223FC31CA8DDE367DF168C2ADAE16D_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// return GUIHelper.AddText(this._listItemPrefab, this._contentRoot, text, this._maxListItemEntries, this._scrollRect);
		TextListItem_tC8FEA392C6AF65CFF22D8089B9104B79C84A6D8B * L_0 = __this->get__listItemPrefab_13();
		RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 * L_1 = __this->get__contentRoot_12();
		String_t* L_2 = ___text0;
		int32_t L_3 = __this->get__maxListItemEntries_14();
		ScrollRect_tB16156010F89FFDAAB2127CA878608FD91B9FBEA * L_4 = __this->get__scrollRect_11();
		IL2CPP_RUNTIME_CLASS_INIT(GUIHelper_t1BA8F4DF68223FC31CA8DDE367DF168C2ADAE16D_il2cpp_TypeInfo_var);
		TextListItem_tC8FEA392C6AF65CFF22D8089B9104B79C84A6D8B * L_5;
		L_5 = GUIHelper_AddText_mAC779A2E1D44664A6F92A3CAD4AB26F8BFE3E56E(L_0, L_1, L_2, L_3, L_4, /*hidden argument*/NULL);
		return L_5;
	}
}
// System.Void BestHTTP.Examples.HubWithPreAuthorizationSample::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void HubWithPreAuthorizationSample__ctor_mD67505220BB65DB77A4A823146570AB6F063B54E (HubWithPreAuthorizationSample_tDF799D602DC996BFF26C8E97175B4E64BF3807D8 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral4B9EA1F270BF9EE6BCF2EFE20495E4564CFEE369);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral7DB56CFD558B13D95B19D64BDAE0AAD5F3AECBCB);
		s_Il2CppMethodInitialized = true;
	}
	{
		// private string _hubPath = "/HubWithAuthorization";
		__this->set__hubPath_9(_stringLiteral7DB56CFD558B13D95B19D64BDAE0AAD5F3AECBCB);
		// private string _jwtTokenPath = "/generateJwtToken";
		__this->set__jwtTokenPath_10(_stringLiteral4B9EA1F270BF9EE6BCF2EFE20495E4564CFEE369);
		// private int _maxListItemEntries = 100;
		__this->set__maxListItemEntries_14(((int32_t)100));
		SampleBase__ctor_mBF0888BC18DA99E126CDA8D6221DF4CC54D93E22(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void BestHTTP.Examples.HubWithPreAuthorizationSample::<OnConnectButton>b__11_0(BestHTTP.SignalRCore.HubConnection,BestHTTP.SignalRCore.ITransport,BestHTTP.SignalRCore.TransportEvents)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void HubWithPreAuthorizationSample_U3COnConnectButtonU3Eb__11_0_m6CC61349D8E7D060103AD3D5AA813DDD28A871C4 (HubWithPreAuthorizationSample_tDF799D602DC996BFF26C8E97175B4E64BF3807D8 * __this, HubConnection_t797D4973C9BBE6895D85BF743512ADA7D419736D * ___hub0, RuntimeObject* ___transport1, int32_t ___ev2, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&ITransport_t832D0F246CABAD20CB7410CC70ED972EF084053B_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&TransportEvents_tE38D2B53053334BE1B4419061B7701FF8E099D19_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&TransportTypes_t8387D0BDDE41AD7796AEFFC585944D24F9389FB0_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralABF1DBB99693496E8838B727F5C5E3D5D5ED72C8);
		s_Il2CppMethodInitialized = true;
	}
	{
		// hub.OnTransportEvent += (hub, transport, ev) => AddText(string.Format("Transport(<color=green>{0}</color>) event: <color=green>{1}</color>", transport.TransportType, ev));
		RuntimeObject* L_0 = ___transport1;
		NullCheck(L_0);
		int32_t L_1;
		L_1 = InterfaceFuncInvoker0< int32_t >::Invoke(1 /* BestHTTP.SignalRCore.TransportTypes BestHTTP.SignalRCore.ITransport::get_TransportType() */, ITransport_t832D0F246CABAD20CB7410CC70ED972EF084053B_il2cpp_TypeInfo_var, L_0);
		int32_t L_2 = L_1;
		RuntimeObject * L_3 = Box(TransportTypes_t8387D0BDDE41AD7796AEFFC585944D24F9389FB0_il2cpp_TypeInfo_var, &L_2);
		int32_t L_4 = ___ev2;
		int32_t L_5 = L_4;
		RuntimeObject * L_6 = Box(TransportEvents_tE38D2B53053334BE1B4419061B7701FF8E099D19_il2cpp_TypeInfo_var, &L_5);
		String_t* L_7;
		L_7 = String_Format_m8D1CB0410C35E052A53AE957C914C841E54BAB66(_stringLiteralABF1DBB99693496E8838B727F5C5E3D5D5ED72C8, L_3, L_6, /*hidden argument*/NULL);
		TextListItem_tC8FEA392C6AF65CFF22D8089B9104B79C84A6D8B * L_8;
		L_8 = HubWithPreAuthorizationSample_AddText_m23B3D8EE9963A7D7ED1F6FD1655F0B0619542EF3(__this, L_7, /*hidden argument*/NULL);
		return;
	}
}
// System.Void BestHTTP.Examples.HubWithPreAuthorizationSample::<Hub_OnConnected>b__15_0(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void HubWithPreAuthorizationSample_U3CHub_OnConnectedU3Eb__15_0_m11819123816AA7FA753584C7A0B140A4CB7FA660 (HubWithPreAuthorizationSample_tDF799D602DC996BFF26C8E97175B4E64BF3807D8 * __this, String_t* ___ret0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral6BAB476667C71E550B8BB632BE04ADDC98348F2D);
		s_Il2CppMethodInitialized = true;
	}
	{
		// .OnSuccess(ret => AddText(string.Format("'<color=green>Echo</color>' returned: '<color=yellow>{0}</color>'", ret)).AddLeftPadding(20));
		String_t* L_0 = ___ret0;
		String_t* L_1;
		L_1 = String_Format_mB3D38E5238C3164DB4D7D29339D9E225A4496D17(_stringLiteral6BAB476667C71E550B8BB632BE04ADDC98348F2D, L_0, /*hidden argument*/NULL);
		TextListItem_tC8FEA392C6AF65CFF22D8089B9104B79C84A6D8B * L_2;
		L_2 = HubWithPreAuthorizationSample_AddText_m23B3D8EE9963A7D7ED1F6FD1655F0B0619542EF3(__this, L_1, /*hidden argument*/NULL);
		NullCheck(L_2);
		TextListItem_AddLeftPadding_m41C860ECCEF8C63672FB82BCB45198F313528C5F(L_2, ((int32_t)20), /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// BestHTTP.Connections.HTTP2.HuffmanEncoder/TreeNode BestHTTP.Connections.HTTP2.HuffmanEncoder::GetRoot()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F  HuffmanEncoder_GetRoot_m06840269B12D96197F2C24E996424FB001FF0C88 (const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&HuffmanEncoder_tE53CF82173488443CE9781D476D0E545B4485E6C_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// return HuffmanTree[0];
		IL2CPP_RUNTIME_CLASS_INIT(HuffmanEncoder_tE53CF82173488443CE9781D476D0E545B4485E6C_il2cpp_TypeInfo_var);
		TreeNodeU5BU5D_t5032BD830284F31E0A94DEB0072DB2B07126BABD* L_0 = ((HuffmanEncoder_tE53CF82173488443CE9781D476D0E545B4485E6C_StaticFields*)il2cpp_codegen_static_fields_for(HuffmanEncoder_tE53CF82173488443CE9781D476D0E545B4485E6C_il2cpp_TypeInfo_var))->get_HuffmanTree_2();
		NullCheck(L_0);
		int32_t L_1 = 0;
		TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F  L_2 = (L_0)->GetAt(static_cast<il2cpp_array_size_t>(L_1));
		return L_2;
	}
}
// BestHTTP.Connections.HTTP2.HuffmanEncoder/TreeNode BestHTTP.Connections.HTTP2.HuffmanEncoder::GetNext(BestHTTP.Connections.HTTP2.HuffmanEncoder/TreeNode,System.Byte)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F  HuffmanEncoder_GetNext_m3E9434FE1E755349C7127E960E54123755C95C20 (TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F  ___current0, uint8_t ___bit1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&HuffmanEncoder_tE53CF82173488443CE9781D476D0E545B4485E6C_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		uint8_t L_0 = ___bit1;
		if (!L_0)
		{
			goto IL_0009;
		}
	}
	{
		uint8_t L_1 = ___bit1;
		if ((((int32_t)L_1) == ((int32_t)1)))
		{
			goto IL_001a;
		}
	}
	{
		goto IL_002b;
	}

IL_0009:
	{
		// return HuffmanTree[current.NextZeroIdx];
		IL2CPP_RUNTIME_CLASS_INIT(HuffmanEncoder_tE53CF82173488443CE9781D476D0E545B4485E6C_il2cpp_TypeInfo_var);
		TreeNodeU5BU5D_t5032BD830284F31E0A94DEB0072DB2B07126BABD* L_2 = ((HuffmanEncoder_tE53CF82173488443CE9781D476D0E545B4485E6C_StaticFields*)il2cpp_codegen_static_fields_for(HuffmanEncoder_tE53CF82173488443CE9781D476D0E545B4485E6C_il2cpp_TypeInfo_var))->get_HuffmanTree_2();
		TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F  L_3 = ___current0;
		uint16_t L_4 = L_3.get_NextZeroIdx_0();
		NullCheck(L_2);
		uint16_t L_5 = L_4;
		TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F  L_6 = (L_2)->GetAt(static_cast<il2cpp_array_size_t>(L_5));
		return L_6;
	}

IL_001a:
	{
		// return HuffmanTree[current.NextOneIdx];
		IL2CPP_RUNTIME_CLASS_INIT(HuffmanEncoder_tE53CF82173488443CE9781D476D0E545B4485E6C_il2cpp_TypeInfo_var);
		TreeNodeU5BU5D_t5032BD830284F31E0A94DEB0072DB2B07126BABD* L_7 = ((HuffmanEncoder_tE53CF82173488443CE9781D476D0E545B4485E6C_StaticFields*)il2cpp_codegen_static_fields_for(HuffmanEncoder_tE53CF82173488443CE9781D476D0E545B4485E6C_il2cpp_TypeInfo_var))->get_HuffmanTree_2();
		TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F  L_8 = ___current0;
		uint16_t L_9 = L_8.get_NextOneIdx_1();
		NullCheck(L_7);
		uint16_t L_10 = L_9;
		TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F  L_11 = (L_7)->GetAt(static_cast<il2cpp_array_size_t>(L_10));
		return L_11;
	}

IL_002b:
	{
		// throw new Exception("HuffmanEncoder - GetNext - unsupported bit: " + bit);
		String_t* L_12;
		L_12 = Byte_ToString_m6A11C71EB9B8031596645EA1A4C2430721B282B5((uint8_t*)(&___bit1), /*hidden argument*/NULL);
		String_t* L_13;
		L_13 = String_Concat_m4B4AB72618348C5DFBFBA8DED84B9E2EBDB55E1B(((String_t*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&_stringLiteral17DFF9E84B78EDF31E28D673A72044E0DC1CA6AE)), L_12, /*hidden argument*/NULL);
		Exception_t * L_14 = (Exception_t *)il2cpp_codegen_object_new(((RuntimeClass*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&Exception_t_il2cpp_TypeInfo_var)));
		Exception__ctor_m8ECDE8ACA7F2E0EF1144BD1200FB5DB2870B5F11(L_14, L_13, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_14, ((RuntimeMethod*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&HuffmanEncoder_GetNext_m3E9434FE1E755349C7127E960E54123755C95C20_RuntimeMethod_var)));
	}
}
// BestHTTP.Connections.HTTP2.HuffmanEncoder/TableEntry BestHTTP.Connections.HTTP2.HuffmanEncoder::GetEntryForCodePoint(System.UInt16)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR TableEntry_tDFCA5A567802D72C4E23BFC6CEA14C32F52229FF  HuffmanEncoder_GetEntryForCodePoint_m577DC52BAC0749FEB2726021ADD1F074CE5F3F85 (uint16_t ___codePoint0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&HuffmanEncoder_tE53CF82173488443CE9781D476D0E545B4485E6C_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// return StaticTable[codePoint];
		IL2CPP_RUNTIME_CLASS_INIT(HuffmanEncoder_tE53CF82173488443CE9781D476D0E545B4485E6C_il2cpp_TypeInfo_var);
		TableEntryU5BU5D_tB3F4A3142B2B8D2FFFA48D2697C81F352A3D4A94* L_0 = ((HuffmanEncoder_tE53CF82173488443CE9781D476D0E545B4485E6C_StaticFields*)il2cpp_codegen_static_fields_for(HuffmanEncoder_tE53CF82173488443CE9781D476D0E545B4485E6C_il2cpp_TypeInfo_var))->get_StaticTable_1();
		uint16_t L_1 = ___codePoint0;
		NullCheck(L_0);
		uint16_t L_2 = L_1;
		TableEntry_tDFCA5A567802D72C4E23BFC6CEA14C32F52229FF  L_3 = (L_0)->GetAt(static_cast<il2cpp_array_size_t>(L_2));
		return L_3;
	}
}
// System.Void BestHTTP.Connections.HTTP2.HuffmanEncoder::.cctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void HuffmanEncoder__cctor_m87F5D9992185884D4E3CF87FB97D033A35CB89E4 (const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&HuffmanEncoder_tE53CF82173488443CE9781D476D0E545B4485E6C_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&TableEntryU5BU5D_tB3F4A3142B2B8D2FFFA48D2697C81F352A3D4A94_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&TreeNodeU5BU5D_t5032BD830284F31E0A94DEB0072DB2B07126BABD_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	TableEntry_tDFCA5A567802D72C4E23BFC6CEA14C32F52229FF  V_0;
	memset((&V_0), 0, sizeof(V_0));
	TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F  V_1;
	memset((&V_1), 0, sizeof(V_1));
	{
		// static TableEntry[] StaticTable = new TableEntry[257]
		// {
		//     new TableEntry{ Code = 0x1ff8   , Bits = 13 },
		//     new TableEntry{ Code = 0x7fffd8 , Bits = 23 },
		//     new TableEntry{ Code = 0xfffffe2, Bits = 28 },
		//     new TableEntry{ Code = 0xfffffe3, Bits = 28 },
		//     new TableEntry{ Code = 0xfffffe4, Bits = 28 },
		//     new TableEntry{ Code = 0xfffffe5, Bits = 28 },
		//     new TableEntry{ Code = 0xfffffe6, Bits = 28 },
		//     new TableEntry{ Code = 0xfffffe7, Bits = 28 },
		//     new TableEntry{ Code = 0xfffffe8, Bits = 28 },
		//     new TableEntry{ Code = 0xffffea, Bits = 24 },
		//     new TableEntry{ Code = 0x3ffffffc , Bits = 30 },
		//     new TableEntry{ Code = 0xfffffe9 , Bits = 28 },
		//     new TableEntry{ Code = 0xfffffea , Bits = 28 },
		//     new TableEntry{ Code = 0x3ffffffd , Bits = 30 },
		//     new TableEntry{ Code = 0xfffffeb , Bits = 28 },
		//     new TableEntry{ Code = 0xfffffec , Bits = 28 },
		//     new TableEntry{ Code = 0xfffffed , Bits = 28 },
		//     new TableEntry{ Code = 0xfffffee , Bits = 28 },
		//     new TableEntry{ Code = 0xfffffef , Bits = 28 },
		//     new TableEntry{ Code = 0xffffff0 , Bits = 28 },
		//     new TableEntry{ Code = 0xffffff1 , Bits = 28 },
		//     new TableEntry{ Code = 0xffffff2 , Bits = 28 },
		//     new TableEntry{ Code = 0x3ffffffe, Bits = 30  },
		//     new TableEntry{ Code = 0xffffff3 , Bits = 28 },
		//     new TableEntry{ Code = 0xffffff4 , Bits = 28 },
		//     new TableEntry{ Code = 0xffffff5 , Bits = 28 },
		//     new TableEntry{ Code = 0xffffff6 , Bits = 28 },
		//     new TableEntry{ Code = 0xffffff7 , Bits = 28 },
		//     new TableEntry{ Code = 0xffffff8 , Bits = 28 },
		//     new TableEntry{ Code = 0xffffff9 , Bits = 28 },
		//     new TableEntry{ Code = 0xffffffa , Bits = 28 },
		//     new TableEntry{ Code = 0xffffffb , Bits = 28 },
		//     new TableEntry{ Code = 0x14 , Bits = 6 },
		//     new TableEntry{ Code = 0x3f8, Bits = 10  },
		//     new TableEntry{ Code = 0x3f9, Bits = 10  },
		//     new TableEntry{ Code = 0xffa, Bits = 12  },
		//     new TableEntry{ Code = 0x1ff9 , Bits = 13 },
		//     new TableEntry{ Code = 0x15 , Bits = 6 },
		//     new TableEntry{ Code = 0xf8 , Bits = 8 },
		//     new TableEntry{ Code = 0x7fa, Bits = 11  },
		//     new TableEntry{ Code = 0x3fa, Bits = 10  },
		//     new TableEntry{ Code = 0x3fb, Bits = 10  },
		//     new TableEntry{ Code = 0xf9 , Bits = 8 },
		//     new TableEntry{ Code = 0x7fb, Bits = 11  },
		//     new TableEntry{ Code = 0xfa , Bits = 8 },
		//     new TableEntry{ Code = 0x16 , Bits = 6 },
		//     new TableEntry{ Code = 0x17 , Bits = 6 },
		//     new TableEntry{ Code = 0x18 , Bits = 6 },
		//     new TableEntry{ Code = 0x0 , Bits = 5 },
		//     new TableEntry{ Code = 0x1 , Bits = 5 },
		//     new TableEntry{ Code = 0x2 , Bits = 5 },
		//     new TableEntry{ Code = 0x19 , Bits = 6 },
		//     new TableEntry{ Code = 0x1a , Bits = 6 },
		//     new TableEntry{ Code = 0x1b , Bits = 6 },
		//     new TableEntry{ Code = 0x1c , Bits = 6 },
		//     new TableEntry{ Code = 0x1d , Bits = 6 },
		//     new TableEntry{ Code = 0x1e , Bits = 6 },
		//     new TableEntry{ Code = 0x1f , Bits = 6 },
		//     new TableEntry{ Code = 0x5c , Bits = 7 },
		//     new TableEntry{ Code = 0xfb , Bits = 8 },
		//     new TableEntry{ Code = 0x7ffc , Bits = 15 },
		//     new TableEntry{ Code = 0x20 , Bits = 6 },
		//     new TableEntry{ Code = 0xffb, Bits = 12  },
		//     new TableEntry{ Code = 0x3fc, Bits = 10  },
		//     new TableEntry{ Code = 0x1ffa , Bits = 13 },
		//     new TableEntry{ Code = 0x21, Bits = 6  },
		//     new TableEntry{ Code = 0x5d, Bits = 7  },
		//     new TableEntry{ Code = 0x5e, Bits = 7  },
		//     new TableEntry{ Code = 0x5f, Bits = 7  },
		//     new TableEntry{ Code = 0x60, Bits = 7  },
		//     new TableEntry{ Code = 0x61, Bits = 7  },
		//     new TableEntry{ Code = 0x62, Bits = 7  },
		//     new TableEntry{ Code = 0x63, Bits = 7  },
		//     new TableEntry{ Code = 0x64, Bits = 7  },
		//     new TableEntry{ Code = 0x65, Bits = 7  },
		//     new TableEntry{ Code = 0x66, Bits = 7  },
		//     new TableEntry{ Code = 0x67, Bits = 7  },
		//     new TableEntry{ Code = 0x68, Bits = 7  },
		//     new TableEntry{ Code = 0x69, Bits = 7  },
		//     new TableEntry{ Code = 0x6a, Bits = 7  },
		//     new TableEntry{ Code = 0x6b, Bits = 7  },
		//     new TableEntry{ Code = 0x6c, Bits = 7  },
		//     new TableEntry{ Code = 0x6d, Bits = 7  },
		//     new TableEntry{ Code = 0x6e, Bits = 7  },
		//     new TableEntry{ Code = 0x6f, Bits = 7  },
		//     new TableEntry{ Code = 0x70, Bits = 7  },
		//     new TableEntry{ Code = 0x71, Bits = 7  },
		//     new TableEntry{ Code = 0x72, Bits = 7  },
		//     new TableEntry{ Code = 0xfc, Bits = 8  },
		//     new TableEntry{ Code = 0x73, Bits = 7  },
		//     new TableEntry{ Code = 0xfd, Bits = 8  },
		//     new TableEntry{ Code = 0x1ffb, Bits = 13  },
		//     new TableEntry{ Code = 0x7fff0, Bits = 19  },
		//     new TableEntry{ Code = 0x1ffc, Bits = 13  },
		//     new TableEntry{ Code = 0x3ffc, Bits = 14  },
		//     new TableEntry{ Code = 0x22, Bits = 6  },
		//     new TableEntry{ Code = 0x7ffd, Bits = 15  },
		//     new TableEntry{ Code = 0x3, Bits = 5  },
		//     new TableEntry{ Code = 0x23, Bits = 6  },
		//     new TableEntry{ Code = 0x4, Bits = 5  },
		//     new TableEntry{ Code = 0x24, Bits = 6  },
		//     new TableEntry{ Code = 0x5, Bits = 5  },
		//     new TableEntry{ Code = 0x25, Bits = 6  },
		//     new TableEntry{ Code = 0x26, Bits = 6  },
		//     new TableEntry{ Code = 0x27, Bits = 6  },
		//     new TableEntry{ Code = 0x6 , Bits = 5 },
		//     new TableEntry{ Code = 0x74, Bits = 7  },
		//     new TableEntry{ Code = 0x75, Bits = 7  },
		//     new TableEntry{ Code = 0x28, Bits = 6  },
		//     new TableEntry{ Code = 0x29, Bits = 6  },
		//     new TableEntry{ Code = 0x2a, Bits = 6  },
		//     new TableEntry{ Code = 0x7 , Bits = 5 },
		//     new TableEntry{ Code = 0x2b, Bits = 6  },
		//     new TableEntry{ Code = 0x76, Bits = 7  },
		//     new TableEntry{ Code = 0x2c, Bits = 6  },
		//     new TableEntry{ Code = 0x8 , Bits = 5 },
		//     new TableEntry{ Code = 0x9 , Bits = 5 },
		//     new TableEntry{ Code = 0x2d, Bits = 6  },
		//     new TableEntry{ Code = 0x77, Bits = 7  },
		//     new TableEntry{ Code = 0x78, Bits = 7  },
		//     new TableEntry{ Code = 0x79, Bits = 7  },
		//     new TableEntry{ Code = 0x7a, Bits = 7  },
		//     new TableEntry{ Code = 0x7b, Bits = 7  },
		//     new TableEntry{ Code = 0x7ffe, Bits = 15  },
		//     new TableEntry{ Code = 0x7fc, Bits = 11  },
		//     new TableEntry{ Code = 0x3ffd, Bits = 14  },
		//     new TableEntry{ Code = 0x1ffd, Bits = 13  },
		//     new TableEntry{ Code = 0xffffffc, Bits = 28  },
		//     new TableEntry{ Code = 0xfffe6 , Bits = 20 },
		//     new TableEntry{ Code = 0x3fffd2, Bits = 22  },
		//     new TableEntry{ Code = 0xfffe7 , Bits = 20 },
		//     new TableEntry{ Code = 0xfffe8 , Bits = 20 },
		//     new TableEntry{ Code = 0x3fffd3, Bits = 22  },
		//     new TableEntry{ Code = 0x3fffd4, Bits = 22  },
		//     new TableEntry{ Code = 0x3fffd5, Bits = 22  },
		//     new TableEntry{ Code = 0x7fffd9, Bits = 23  },
		//     new TableEntry{ Code = 0x3fffd6, Bits = 22  },
		//     new TableEntry{ Code = 0x7fffda, Bits = 23  },
		//     new TableEntry{ Code = 0x7fffdb, Bits = 23  },
		//     new TableEntry{ Code = 0x7fffdc, Bits = 23  },
		//     new TableEntry{ Code = 0x7fffdd, Bits = 23  },
		//     new TableEntry{ Code = 0x7fffde, Bits = 23  },
		//     new TableEntry{ Code = 0xffffeb, Bits = 24  },
		//     new TableEntry{ Code = 0x7fffdf, Bits = 23  },
		//     new TableEntry{ Code = 0xffffec, Bits = 24  },
		//     new TableEntry{ Code = 0xffffed, Bits = 24  },
		//     new TableEntry{ Code = 0x3fffd7, Bits = 22  },
		//     new TableEntry{ Code = 0x7fffe0, Bits = 23  },
		//     new TableEntry{ Code = 0xffffee, Bits = 24  },
		//     new TableEntry{ Code = 0x7fffe1, Bits = 23  },
		//     new TableEntry{ Code = 0x7fffe2, Bits = 23  },
		//     new TableEntry{ Code = 0x7fffe3, Bits = 23  },
		//     new TableEntry{ Code = 0x7fffe4, Bits = 23  },
		//     new TableEntry{ Code = 0x1fffdc, Bits = 21  },
		//     new TableEntry{ Code = 0x3fffd8, Bits = 22  },
		//     new TableEntry{ Code = 0x7fffe5, Bits = 23  },
		//     new TableEntry{ Code = 0x3fffd9, Bits = 22  },
		//     new TableEntry{ Code = 0x7fffe6, Bits = 23  },
		//     new TableEntry{ Code = 0x7fffe7, Bits = 23  },
		//     new TableEntry{ Code = 0xffffef, Bits = 24  },
		//     new TableEntry{ Code = 0x3fffda, Bits = 22  },
		//     new TableEntry{ Code = 0x1fffdd, Bits = 21  },
		//     new TableEntry{ Code = 0xfffe9 , Bits = 20 },
		//     new TableEntry{ Code = 0x3fffdb, Bits = 22  },
		//     new TableEntry{ Code = 0x3fffdc, Bits = 22  },
		//     new TableEntry{ Code = 0x7fffe8, Bits = 23  },
		//     new TableEntry{ Code = 0x7fffe9, Bits = 23  },
		//     new TableEntry{ Code = 0x1fffde, Bits = 21  },
		//     new TableEntry{ Code = 0x7fffea, Bits = 23  },
		//     new TableEntry{ Code = 0x3fffdd, Bits = 22  },
		//     new TableEntry{ Code = 0x3fffde, Bits = 22  },
		//     new TableEntry{ Code = 0xfffff0, Bits = 24  },
		//     new TableEntry{ Code = 0x1fffdf, Bits = 21  },
		//     new TableEntry{ Code = 0x3fffdf, Bits = 22  },
		//     new TableEntry{ Code = 0x7fffeb, Bits = 23  },
		//     new TableEntry{ Code = 0x7fffec, Bits = 23  },
		//     new TableEntry{ Code = 0x1fffe0, Bits = 21  },
		//     new TableEntry{ Code = 0x1fffe1, Bits = 21  },
		//     new TableEntry{ Code = 0x3fffe0, Bits = 22  },
		//     new TableEntry{ Code = 0x1fffe2, Bits = 21  },
		//     new TableEntry{ Code = 0x7fffed, Bits = 23  },
		//     new TableEntry{ Code = 0x3fffe1, Bits = 22  },
		//     new TableEntry{ Code = 0x7fffee, Bits = 23  },
		//     new TableEntry{ Code = 0x7fffef, Bits = 23  },
		//     new TableEntry{ Code = 0xfffea , Bits = 20 },
		//     new TableEntry{ Code = 0x3fffe2, Bits = 22  },
		//     new TableEntry{ Code = 0x3fffe3, Bits = 22  },
		//     new TableEntry{ Code = 0x3fffe4, Bits = 22  },
		//     new TableEntry{ Code = 0x7ffff0, Bits = 23  },
		//     new TableEntry{ Code = 0x3fffe5, Bits = 22  },
		//     new TableEntry{ Code = 0x3fffe6, Bits = 22  },
		//     new TableEntry{ Code = 0x7ffff1, Bits = 23  },
		//     new TableEntry{ Code = 0x3ffffe0, Bits = 26  },
		//     new TableEntry{ Code = 0x3ffffe1, Bits = 26  },
		//     new TableEntry{ Code = 0xfffeb , Bits = 20 },
		//     new TableEntry{ Code = 0x7fff1 , Bits = 19 },
		//     new TableEntry{ Code = 0x3fffe7, Bits = 22  },
		//     new TableEntry{ Code = 0x7ffff2, Bits = 23  },
		//     new TableEntry{ Code = 0x3fffe8, Bits = 22 },
		//     new TableEntry{ Code = 0x1ffffec, Bits = 25  },
		//     new TableEntry{ Code = 0x3ffffe2, Bits = 26  },
		//     new TableEntry{ Code = 0x3ffffe3, Bits = 26  },
		//     new TableEntry{ Code = 0x3ffffe4, Bits = 26  },
		//     new TableEntry{ Code = 0x7ffffde, Bits = 27  },
		//     new TableEntry{ Code = 0x7ffffdf, Bits = 27  },
		//     new TableEntry{ Code = 0x3ffffe5, Bits = 26  },
		//     new TableEntry{ Code = 0xfffff1 , Bits = 24 },
		//     new TableEntry{ Code = 0x1ffffed, Bits = 25  },
		//     new TableEntry{ Code = 0x7fff2 , Bits = 19 },
		//     new TableEntry{ Code = 0x1fffe3 , Bits = 21 },
		//     new TableEntry{ Code = 0x3ffffe6, Bits = 26  },
		//     new TableEntry{ Code = 0x7ffffe0, Bits = 27  },
		//     new TableEntry{ Code = 0x7ffffe1, Bits = 27  },
		//     new TableEntry{ Code = 0x3ffffe7, Bits = 26  },
		//     new TableEntry{ Code = 0x7ffffe2, Bits = 27  },
		//     new TableEntry{ Code = 0xfffff2 , Bits = 24 },
		//     new TableEntry{ Code = 0x1fffe4 , Bits = 21 },
		//     new TableEntry{ Code = 0x1fffe5 , Bits = 21 },
		//     new TableEntry{ Code = 0x3ffffe8, Bits = 26  },
		//     new TableEntry{ Code = 0x3ffffe9, Bits = 26  },
		//     new TableEntry{ Code = 0xffffffd, Bits = 28  },
		//     new TableEntry{ Code = 0x7ffffe3, Bits = 27  },
		//     new TableEntry{ Code = 0x7ffffe4, Bits = 27  },
		//     new TableEntry{ Code = 0x7ffffe5, Bits = 27  },
		//     new TableEntry{ Code = 0xfffec , Bits = 20 },
		//     new TableEntry{ Code = 0xfffff3, Bits = 24  },
		//     new TableEntry{ Code = 0xfffed , Bits = 20 },
		//     new TableEntry{ Code = 0x1fffe6, Bits = 21  },
		//     new TableEntry{ Code = 0x3fffe9, Bits = 22  },
		//     new TableEntry{ Code = 0x1fffe7, Bits = 21  },
		//     new TableEntry{ Code = 0x1fffe8, Bits = 21  },
		//     new TableEntry{ Code = 0x7ffff3, Bits = 23  },
		//     new TableEntry{ Code = 0x3fffea, Bits = 22  },
		//     new TableEntry{ Code = 0x3fffeb, Bits = 22  },
		//     new TableEntry{ Code = 0x1ffffee, Bits = 25  },
		//     new TableEntry{ Code = 0x1ffffef, Bits = 25  },
		//     new TableEntry{ Code = 0xfffff4 , Bits = 24 },
		//     new TableEntry{ Code = 0xfffff5 , Bits = 24 },
		//     new TableEntry{ Code = 0x3ffffea, Bits = 26  },
		//     new TableEntry{ Code = 0x7ffff4 , Bits = 23 },
		//     new TableEntry{ Code = 0x3ffffeb, Bits = 26  },
		//     new TableEntry{ Code = 0x7ffffe6, Bits = 27  },
		//     new TableEntry{ Code = 0x3ffffec, Bits = 26  },
		//     new TableEntry{ Code = 0x3ffffed, Bits = 26  },
		//     new TableEntry{ Code = 0x7ffffe7, Bits = 27  },
		//     new TableEntry{ Code = 0x7ffffe8, Bits = 27  },
		//     new TableEntry{ Code = 0x7ffffe9, Bits = 27  },
		//     new TableEntry{ Code = 0x7ffffea, Bits = 27  },
		//     new TableEntry{ Code = 0x7ffffeb, Bits = 27  },
		//     new TableEntry{ Code = 0xffffffe, Bits = 28  },
		//     new TableEntry{ Code = 0x7ffffec, Bits = 27  },
		//     new TableEntry{ Code = 0x7ffffed, Bits = 27  },
		//     new TableEntry{ Code = 0x7ffffee, Bits = 27  },
		//     new TableEntry{ Code = 0x7ffffef, Bits = 27  },
		//     new TableEntry{ Code = 0x7fffff0, Bits = 27  },
		//     new TableEntry{ Code = 0x3ffffee, Bits = 26  },
		//     new TableEntry{ Code = 0x3fffffff, Bits = 30  }
		// };
		TableEntryU5BU5D_tB3F4A3142B2B8D2FFFA48D2697C81F352A3D4A94* L_0 = (TableEntryU5BU5D_tB3F4A3142B2B8D2FFFA48D2697C81F352A3D4A94*)(TableEntryU5BU5D_tB3F4A3142B2B8D2FFFA48D2697C81F352A3D4A94*)SZArrayNew(TableEntryU5BU5D_tB3F4A3142B2B8D2FFFA48D2697C81F352A3D4A94_il2cpp_TypeInfo_var, (uint32_t)((int32_t)257));
		TableEntryU5BU5D_tB3F4A3142B2B8D2FFFA48D2697C81F352A3D4A94* L_1 = L_0;
		il2cpp_codegen_initobj((&V_0), sizeof(TableEntry_tDFCA5A567802D72C4E23BFC6CEA14C32F52229FF ));
		(&V_0)->set_Code_0(((int32_t)8184));
		(&V_0)->set_Bits_1((uint8_t)((int32_t)13));
		TableEntry_tDFCA5A567802D72C4E23BFC6CEA14C32F52229FF  L_2 = V_0;
		NullCheck(L_1);
		(L_1)->SetAt(static_cast<il2cpp_array_size_t>(0), (TableEntry_tDFCA5A567802D72C4E23BFC6CEA14C32F52229FF )L_2);
		TableEntryU5BU5D_tB3F4A3142B2B8D2FFFA48D2697C81F352A3D4A94* L_3 = L_1;
		il2cpp_codegen_initobj((&V_0), sizeof(TableEntry_tDFCA5A567802D72C4E23BFC6CEA14C32F52229FF ));
		(&V_0)->set_Code_0(((int32_t)8388568));
		(&V_0)->set_Bits_1((uint8_t)((int32_t)23));
		TableEntry_tDFCA5A567802D72C4E23BFC6CEA14C32F52229FF  L_4 = V_0;
		NullCheck(L_3);
		(L_3)->SetAt(static_cast<il2cpp_array_size_t>(1), (TableEntry_tDFCA5A567802D72C4E23BFC6CEA14C32F52229FF )L_4);
		TableEntryU5BU5D_tB3F4A3142B2B8D2FFFA48D2697C81F352A3D4A94* L_5 = L_3;
		il2cpp_codegen_initobj((&V_0), sizeof(TableEntry_tDFCA5A567802D72C4E23BFC6CEA14C32F52229FF ));
		(&V_0)->set_Code_0(((int32_t)268435426));
		(&V_0)->set_Bits_1((uint8_t)((int32_t)28));
		TableEntry_tDFCA5A567802D72C4E23BFC6CEA14C32F52229FF  L_6 = V_0;
		NullCheck(L_5);
		(L_5)->SetAt(static_cast<il2cpp_array_size_t>(2), (TableEntry_tDFCA5A567802D72C4E23BFC6CEA14C32F52229FF )L_6);
		TableEntryU5BU5D_tB3F4A3142B2B8D2FFFA48D2697C81F352A3D4A94* L_7 = L_5;
		il2cpp_codegen_initobj((&V_0), sizeof(TableEntry_tDFCA5A567802D72C4E23BFC6CEA14C32F52229FF ));
		(&V_0)->set_Code_0(((int32_t)268435427));
		(&V_0)->set_Bits_1((uint8_t)((int32_t)28));
		TableEntry_tDFCA5A567802D72C4E23BFC6CEA14C32F52229FF  L_8 = V_0;
		NullCheck(L_7);
		(L_7)->SetAt(static_cast<il2cpp_array_size_t>(3), (TableEntry_tDFCA5A567802D72C4E23BFC6CEA14C32F52229FF )L_8);
		TableEntryU5BU5D_tB3F4A3142B2B8D2FFFA48D2697C81F352A3D4A94* L_9 = L_7;
		il2cpp_codegen_initobj((&V_0), sizeof(TableEntry_tDFCA5A567802D72C4E23BFC6CEA14C32F52229FF ));
		(&V_0)->set_Code_0(((int32_t)268435428));
		(&V_0)->set_Bits_1((uint8_t)((int32_t)28));
		TableEntry_tDFCA5A567802D72C4E23BFC6CEA14C32F52229FF  L_10 = V_0;
		NullCheck(L_9);
		(L_9)->SetAt(static_cast<il2cpp_array_size_t>(4), (TableEntry_tDFCA5A567802D72C4E23BFC6CEA14C32F52229FF )L_10);
		TableEntryU5BU5D_tB3F4A3142B2B8D2FFFA48D2697C81F352A3D4A94* L_11 = L_9;
		il2cpp_codegen_initobj((&V_0), sizeof(TableEntry_tDFCA5A567802D72C4E23BFC6CEA14C32F52229FF ));
		(&V_0)->set_Code_0(((int32_t)268435429));
		(&V_0)->set_Bits_1((uint8_t)((int32_t)28));
		TableEntry_tDFCA5A567802D72C4E23BFC6CEA14C32F52229FF  L_12 = V_0;
		NullCheck(L_11);
		(L_11)->SetAt(static_cast<il2cpp_array_size_t>(5), (TableEntry_tDFCA5A567802D72C4E23BFC6CEA14C32F52229FF )L_12);
		TableEntryU5BU5D_tB3F4A3142B2B8D2FFFA48D2697C81F352A3D4A94* L_13 = L_11;
		il2cpp_codegen_initobj((&V_0), sizeof(TableEntry_tDFCA5A567802D72C4E23BFC6CEA14C32F52229FF ));
		(&V_0)->set_Code_0(((int32_t)268435430));
		(&V_0)->set_Bits_1((uint8_t)((int32_t)28));
		TableEntry_tDFCA5A567802D72C4E23BFC6CEA14C32F52229FF  L_14 = V_0;
		NullCheck(L_13);
		(L_13)->SetAt(static_cast<il2cpp_array_size_t>(6), (TableEntry_tDFCA5A567802D72C4E23BFC6CEA14C32F52229FF )L_14);
		TableEntryU5BU5D_tB3F4A3142B2B8D2FFFA48D2697C81F352A3D4A94* L_15 = L_13;
		il2cpp_codegen_initobj((&V_0), sizeof(TableEntry_tDFCA5A567802D72C4E23BFC6CEA14C32F52229FF ));
		(&V_0)->set_Code_0(((int32_t)268435431));
		(&V_0)->set_Bits_1((uint8_t)((int32_t)28));
		TableEntry_tDFCA5A567802D72C4E23BFC6CEA14C32F52229FF  L_16 = V_0;
		NullCheck(L_15);
		(L_15)->SetAt(static_cast<il2cpp_array_size_t>(7), (TableEntry_tDFCA5A567802D72C4E23BFC6CEA14C32F52229FF )L_16);
		TableEntryU5BU5D_tB3F4A3142B2B8D2FFFA48D2697C81F352A3D4A94* L_17 = L_15;
		il2cpp_codegen_initobj((&V_0), sizeof(TableEntry_tDFCA5A567802D72C4E23BFC6CEA14C32F52229FF ));
		(&V_0)->set_Code_0(((int32_t)268435432));
		(&V_0)->set_Bits_1((uint8_t)((int32_t)28));
		TableEntry_tDFCA5A567802D72C4E23BFC6CEA14C32F52229FF  L_18 = V_0;
		NullCheck(L_17);
		(L_17)->SetAt(static_cast<il2cpp_array_size_t>(8), (TableEntry_tDFCA5A567802D72C4E23BFC6CEA14C32F52229FF )L_18);
		TableEntryU5BU5D_tB3F4A3142B2B8D2FFFA48D2697C81F352A3D4A94* L_19 = L_17;
		il2cpp_codegen_initobj((&V_0), sizeof(TableEntry_tDFCA5A567802D72C4E23BFC6CEA14C32F52229FF ));
		(&V_0)->set_Code_0(((int32_t)16777194));
		(&V_0)->set_Bits_1((uint8_t)((int32_t)24));
		TableEntry_tDFCA5A567802D72C4E23BFC6CEA14C32F52229FF  L_20 = V_0;
		NullCheck(L_19);
		(L_19)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)9)), (TableEntry_tDFCA5A567802D72C4E23BFC6CEA14C32F52229FF )L_20);
		TableEntryU5BU5D_tB3F4A3142B2B8D2FFFA48D2697C81F352A3D4A94* L_21 = L_19;
		il2cpp_codegen_initobj((&V_0), sizeof(TableEntry_tDFCA5A567802D72C4E23BFC6CEA14C32F52229FF ));
		(&V_0)->set_Code_0(((int32_t)1073741820));
		(&V_0)->set_Bits_1((uint8_t)((int32_t)30));
		TableEntry_tDFCA5A567802D72C4E23BFC6CEA14C32F52229FF  L_22 = V_0;
		NullCheck(L_21);
		(L_21)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)10)), (TableEntry_tDFCA5A567802D72C4E23BFC6CEA14C32F52229FF )L_22);
		TableEntryU5BU5D_tB3F4A3142B2B8D2FFFA48D2697C81F352A3D4A94* L_23 = L_21;
		il2cpp_codegen_initobj((&V_0), sizeof(TableEntry_tDFCA5A567802D72C4E23BFC6CEA14C32F52229FF ));
		(&V_0)->set_Code_0(((int32_t)268435433));
		(&V_0)->set_Bits_1((uint8_t)((int32_t)28));
		TableEntry_tDFCA5A567802D72C4E23BFC6CEA14C32F52229FF  L_24 = V_0;
		NullCheck(L_23);
		(L_23)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)11)), (TableEntry_tDFCA5A567802D72C4E23BFC6CEA14C32F52229FF )L_24);
		TableEntryU5BU5D_tB3F4A3142B2B8D2FFFA48D2697C81F352A3D4A94* L_25 = L_23;
		il2cpp_codegen_initobj((&V_0), sizeof(TableEntry_tDFCA5A567802D72C4E23BFC6CEA14C32F52229FF ));
		(&V_0)->set_Code_0(((int32_t)268435434));
		(&V_0)->set_Bits_1((uint8_t)((int32_t)28));
		TableEntry_tDFCA5A567802D72C4E23BFC6CEA14C32F52229FF  L_26 = V_0;
		NullCheck(L_25);
		(L_25)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)12)), (TableEntry_tDFCA5A567802D72C4E23BFC6CEA14C32F52229FF )L_26);
		TableEntryU5BU5D_tB3F4A3142B2B8D2FFFA48D2697C81F352A3D4A94* L_27 = L_25;
		il2cpp_codegen_initobj((&V_0), sizeof(TableEntry_tDFCA5A567802D72C4E23BFC6CEA14C32F52229FF ));
		(&V_0)->set_Code_0(((int32_t)1073741821));
		(&V_0)->set_Bits_1((uint8_t)((int32_t)30));
		TableEntry_tDFCA5A567802D72C4E23BFC6CEA14C32F52229FF  L_28 = V_0;
		NullCheck(L_27);
		(L_27)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)13)), (TableEntry_tDFCA5A567802D72C4E23BFC6CEA14C32F52229FF )L_28);
		TableEntryU5BU5D_tB3F4A3142B2B8D2FFFA48D2697C81F352A3D4A94* L_29 = L_27;
		il2cpp_codegen_initobj((&V_0), sizeof(TableEntry_tDFCA5A567802D72C4E23BFC6CEA14C32F52229FF ));
		(&V_0)->set_Code_0(((int32_t)268435435));
		(&V_0)->set_Bits_1((uint8_t)((int32_t)28));
		TableEntry_tDFCA5A567802D72C4E23BFC6CEA14C32F52229FF  L_30 = V_0;
		NullCheck(L_29);
		(L_29)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)14)), (TableEntry_tDFCA5A567802D72C4E23BFC6CEA14C32F52229FF )L_30);
		TableEntryU5BU5D_tB3F4A3142B2B8D2FFFA48D2697C81F352A3D4A94* L_31 = L_29;
		il2cpp_codegen_initobj((&V_0), sizeof(TableEntry_tDFCA5A567802D72C4E23BFC6CEA14C32F52229FF ));
		(&V_0)->set_Code_0(((int32_t)268435436));
		(&V_0)->set_Bits_1((uint8_t)((int32_t)28));
		TableEntry_tDFCA5A567802D72C4E23BFC6CEA14C32F52229FF  L_32 = V_0;
		NullCheck(L_31);
		(L_31)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)15)), (TableEntry_tDFCA5A567802D72C4E23BFC6CEA14C32F52229FF )L_32);
		TableEntryU5BU5D_tB3F4A3142B2B8D2FFFA48D2697C81F352A3D4A94* L_33 = L_31;
		il2cpp_codegen_initobj((&V_0), sizeof(TableEntry_tDFCA5A567802D72C4E23BFC6CEA14C32F52229FF ));
		(&V_0)->set_Code_0(((int32_t)268435437));
		(&V_0)->set_Bits_1((uint8_t)((int32_t)28));
		TableEntry_tDFCA5A567802D72C4E23BFC6CEA14C32F52229FF  L_34 = V_0;
		NullCheck(L_33);
		(L_33)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)16)), (TableEntry_tDFCA5A567802D72C4E23BFC6CEA14C32F52229FF )L_34);
		TableEntryU5BU5D_tB3F4A3142B2B8D2FFFA48D2697C81F352A3D4A94* L_35 = L_33;
		il2cpp_codegen_initobj((&V_0), sizeof(TableEntry_tDFCA5A567802D72C4E23BFC6CEA14C32F52229FF ));
		(&V_0)->set_Code_0(((int32_t)268435438));
		(&V_0)->set_Bits_1((uint8_t)((int32_t)28));
		TableEntry_tDFCA5A567802D72C4E23BFC6CEA14C32F52229FF  L_36 = V_0;
		NullCheck(L_35);
		(L_35)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)17)), (TableEntry_tDFCA5A567802D72C4E23BFC6CEA14C32F52229FF )L_36);
		TableEntryU5BU5D_tB3F4A3142B2B8D2FFFA48D2697C81F352A3D4A94* L_37 = L_35;
		il2cpp_codegen_initobj((&V_0), sizeof(TableEntry_tDFCA5A567802D72C4E23BFC6CEA14C32F52229FF ));
		(&V_0)->set_Code_0(((int32_t)268435439));
		(&V_0)->set_Bits_1((uint8_t)((int32_t)28));
		TableEntry_tDFCA5A567802D72C4E23BFC6CEA14C32F52229FF  L_38 = V_0;
		NullCheck(L_37);
		(L_37)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)18)), (TableEntry_tDFCA5A567802D72C4E23BFC6CEA14C32F52229FF )L_38);
		TableEntryU5BU5D_tB3F4A3142B2B8D2FFFA48D2697C81F352A3D4A94* L_39 = L_37;
		il2cpp_codegen_initobj((&V_0), sizeof(TableEntry_tDFCA5A567802D72C4E23BFC6CEA14C32F52229FF ));
		(&V_0)->set_Code_0(((int32_t)268435440));
		(&V_0)->set_Bits_1((uint8_t)((int32_t)28));
		TableEntry_tDFCA5A567802D72C4E23BFC6CEA14C32F52229FF  L_40 = V_0;
		NullCheck(L_39);
		(L_39)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)19)), (TableEntry_tDFCA5A567802D72C4E23BFC6CEA14C32F52229FF )L_40);
		TableEntryU5BU5D_tB3F4A3142B2B8D2FFFA48D2697C81F352A3D4A94* L_41 = L_39;
		il2cpp_codegen_initobj((&V_0), sizeof(TableEntry_tDFCA5A567802D72C4E23BFC6CEA14C32F52229FF ));
		(&V_0)->set_Code_0(((int32_t)268435441));
		(&V_0)->set_Bits_1((uint8_t)((int32_t)28));
		TableEntry_tDFCA5A567802D72C4E23BFC6CEA14C32F52229FF  L_42 = V_0;
		NullCheck(L_41);
		(L_41)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)20)), (TableEntry_tDFCA5A567802D72C4E23BFC6CEA14C32F52229FF )L_42);
		TableEntryU5BU5D_tB3F4A3142B2B8D2FFFA48D2697C81F352A3D4A94* L_43 = L_41;
		il2cpp_codegen_initobj((&V_0), sizeof(TableEntry_tDFCA5A567802D72C4E23BFC6CEA14C32F52229FF ));
		(&V_0)->set_Code_0(((int32_t)268435442));
		(&V_0)->set_Bits_1((uint8_t)((int32_t)28));
		TableEntry_tDFCA5A567802D72C4E23BFC6CEA14C32F52229FF  L_44 = V_0;
		NullCheck(L_43);
		(L_43)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)21)), (TableEntry_tDFCA5A567802D72C4E23BFC6CEA14C32F52229FF )L_44);
		TableEntryU5BU5D_tB3F4A3142B2B8D2FFFA48D2697C81F352A3D4A94* L_45 = L_43;
		il2cpp_codegen_initobj((&V_0), sizeof(TableEntry_tDFCA5A567802D72C4E23BFC6CEA14C32F52229FF ));
		(&V_0)->set_Code_0(((int32_t)1073741822));
		(&V_0)->set_Bits_1((uint8_t)((int32_t)30));
		TableEntry_tDFCA5A567802D72C4E23BFC6CEA14C32F52229FF  L_46 = V_0;
		NullCheck(L_45);
		(L_45)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)22)), (TableEntry_tDFCA5A567802D72C4E23BFC6CEA14C32F52229FF )L_46);
		TableEntryU5BU5D_tB3F4A3142B2B8D2FFFA48D2697C81F352A3D4A94* L_47 = L_45;
		il2cpp_codegen_initobj((&V_0), sizeof(TableEntry_tDFCA5A567802D72C4E23BFC6CEA14C32F52229FF ));
		(&V_0)->set_Code_0(((int32_t)268435443));
		(&V_0)->set_Bits_1((uint8_t)((int32_t)28));
		TableEntry_tDFCA5A567802D72C4E23BFC6CEA14C32F52229FF  L_48 = V_0;
		NullCheck(L_47);
		(L_47)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)23)), (TableEntry_tDFCA5A567802D72C4E23BFC6CEA14C32F52229FF )L_48);
		TableEntryU5BU5D_tB3F4A3142B2B8D2FFFA48D2697C81F352A3D4A94* L_49 = L_47;
		il2cpp_codegen_initobj((&V_0), sizeof(TableEntry_tDFCA5A567802D72C4E23BFC6CEA14C32F52229FF ));
		(&V_0)->set_Code_0(((int32_t)268435444));
		(&V_0)->set_Bits_1((uint8_t)((int32_t)28));
		TableEntry_tDFCA5A567802D72C4E23BFC6CEA14C32F52229FF  L_50 = V_0;
		NullCheck(L_49);
		(L_49)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)24)), (TableEntry_tDFCA5A567802D72C4E23BFC6CEA14C32F52229FF )L_50);
		TableEntryU5BU5D_tB3F4A3142B2B8D2FFFA48D2697C81F352A3D4A94* L_51 = L_49;
		il2cpp_codegen_initobj((&V_0), sizeof(TableEntry_tDFCA5A567802D72C4E23BFC6CEA14C32F52229FF ));
		(&V_0)->set_Code_0(((int32_t)268435445));
		(&V_0)->set_Bits_1((uint8_t)((int32_t)28));
		TableEntry_tDFCA5A567802D72C4E23BFC6CEA14C32F52229FF  L_52 = V_0;
		NullCheck(L_51);
		(L_51)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)25)), (TableEntry_tDFCA5A567802D72C4E23BFC6CEA14C32F52229FF )L_52);
		TableEntryU5BU5D_tB3F4A3142B2B8D2FFFA48D2697C81F352A3D4A94* L_53 = L_51;
		il2cpp_codegen_initobj((&V_0), sizeof(TableEntry_tDFCA5A567802D72C4E23BFC6CEA14C32F52229FF ));
		(&V_0)->set_Code_0(((int32_t)268435446));
		(&V_0)->set_Bits_1((uint8_t)((int32_t)28));
		TableEntry_tDFCA5A567802D72C4E23BFC6CEA14C32F52229FF  L_54 = V_0;
		NullCheck(L_53);
		(L_53)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)26)), (TableEntry_tDFCA5A567802D72C4E23BFC6CEA14C32F52229FF )L_54);
		TableEntryU5BU5D_tB3F4A3142B2B8D2FFFA48D2697C81F352A3D4A94* L_55 = L_53;
		il2cpp_codegen_initobj((&V_0), sizeof(TableEntry_tDFCA5A567802D72C4E23BFC6CEA14C32F52229FF ));
		(&V_0)->set_Code_0(((int32_t)268435447));
		(&V_0)->set_Bits_1((uint8_t)((int32_t)28));
		TableEntry_tDFCA5A567802D72C4E23BFC6CEA14C32F52229FF  L_56 = V_0;
		NullCheck(L_55);
		(L_55)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)27)), (TableEntry_tDFCA5A567802D72C4E23BFC6CEA14C32F52229FF )L_56);
		TableEntryU5BU5D_tB3F4A3142B2B8D2FFFA48D2697C81F352A3D4A94* L_57 = L_55;
		il2cpp_codegen_initobj((&V_0), sizeof(TableEntry_tDFCA5A567802D72C4E23BFC6CEA14C32F52229FF ));
		(&V_0)->set_Code_0(((int32_t)268435448));
		(&V_0)->set_Bits_1((uint8_t)((int32_t)28));
		TableEntry_tDFCA5A567802D72C4E23BFC6CEA14C32F52229FF  L_58 = V_0;
		NullCheck(L_57);
		(L_57)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)28)), (TableEntry_tDFCA5A567802D72C4E23BFC6CEA14C32F52229FF )L_58);
		TableEntryU5BU5D_tB3F4A3142B2B8D2FFFA48D2697C81F352A3D4A94* L_59 = L_57;
		il2cpp_codegen_initobj((&V_0), sizeof(TableEntry_tDFCA5A567802D72C4E23BFC6CEA14C32F52229FF ));
		(&V_0)->set_Code_0(((int32_t)268435449));
		(&V_0)->set_Bits_1((uint8_t)((int32_t)28));
		TableEntry_tDFCA5A567802D72C4E23BFC6CEA14C32F52229FF  L_60 = V_0;
		NullCheck(L_59);
		(L_59)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)29)), (TableEntry_tDFCA5A567802D72C4E23BFC6CEA14C32F52229FF )L_60);
		TableEntryU5BU5D_tB3F4A3142B2B8D2FFFA48D2697C81F352A3D4A94* L_61 = L_59;
		il2cpp_codegen_initobj((&V_0), sizeof(TableEntry_tDFCA5A567802D72C4E23BFC6CEA14C32F52229FF ));
		(&V_0)->set_Code_0(((int32_t)268435450));
		(&V_0)->set_Bits_1((uint8_t)((int32_t)28));
		TableEntry_tDFCA5A567802D72C4E23BFC6CEA14C32F52229FF  L_62 = V_0;
		NullCheck(L_61);
		(L_61)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)30)), (TableEntry_tDFCA5A567802D72C4E23BFC6CEA14C32F52229FF )L_62);
		TableEntryU5BU5D_tB3F4A3142B2B8D2FFFA48D2697C81F352A3D4A94* L_63 = L_61;
		il2cpp_codegen_initobj((&V_0), sizeof(TableEntry_tDFCA5A567802D72C4E23BFC6CEA14C32F52229FF ));
		(&V_0)->set_Code_0(((int32_t)268435451));
		(&V_0)->set_Bits_1((uint8_t)((int32_t)28));
		TableEntry_tDFCA5A567802D72C4E23BFC6CEA14C32F52229FF  L_64 = V_0;
		NullCheck(L_63);
		(L_63)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)31)), (TableEntry_tDFCA5A567802D72C4E23BFC6CEA14C32F52229FF )L_64);
		TableEntryU5BU5D_tB3F4A3142B2B8D2FFFA48D2697C81F352A3D4A94* L_65 = L_63;
		il2cpp_codegen_initobj((&V_0), sizeof(TableEntry_tDFCA5A567802D72C4E23BFC6CEA14C32F52229FF ));
		(&V_0)->set_Code_0(((int32_t)20));
		(&V_0)->set_Bits_1((uint8_t)6);
		TableEntry_tDFCA5A567802D72C4E23BFC6CEA14C32F52229FF  L_66 = V_0;
		NullCheck(L_65);
		(L_65)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)32)), (TableEntry_tDFCA5A567802D72C4E23BFC6CEA14C32F52229FF )L_66);
		TableEntryU5BU5D_tB3F4A3142B2B8D2FFFA48D2697C81F352A3D4A94* L_67 = L_65;
		il2cpp_codegen_initobj((&V_0), sizeof(TableEntry_tDFCA5A567802D72C4E23BFC6CEA14C32F52229FF ));
		(&V_0)->set_Code_0(((int32_t)1016));
		(&V_0)->set_Bits_1((uint8_t)((int32_t)10));
		TableEntry_tDFCA5A567802D72C4E23BFC6CEA14C32F52229FF  L_68 = V_0;
		NullCheck(L_67);
		(L_67)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)33)), (TableEntry_tDFCA5A567802D72C4E23BFC6CEA14C32F52229FF )L_68);
		TableEntryU5BU5D_tB3F4A3142B2B8D2FFFA48D2697C81F352A3D4A94* L_69 = L_67;
		il2cpp_codegen_initobj((&V_0), sizeof(TableEntry_tDFCA5A567802D72C4E23BFC6CEA14C32F52229FF ));
		(&V_0)->set_Code_0(((int32_t)1017));
		(&V_0)->set_Bits_1((uint8_t)((int32_t)10));
		TableEntry_tDFCA5A567802D72C4E23BFC6CEA14C32F52229FF  L_70 = V_0;
		NullCheck(L_69);
		(L_69)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)34)), (TableEntry_tDFCA5A567802D72C4E23BFC6CEA14C32F52229FF )L_70);
		TableEntryU5BU5D_tB3F4A3142B2B8D2FFFA48D2697C81F352A3D4A94* L_71 = L_69;
		il2cpp_codegen_initobj((&V_0), sizeof(TableEntry_tDFCA5A567802D72C4E23BFC6CEA14C32F52229FF ));
		(&V_0)->set_Code_0(((int32_t)4090));
		(&V_0)->set_Bits_1((uint8_t)((int32_t)12));
		TableEntry_tDFCA5A567802D72C4E23BFC6CEA14C32F52229FF  L_72 = V_0;
		NullCheck(L_71);
		(L_71)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)35)), (TableEntry_tDFCA5A567802D72C4E23BFC6CEA14C32F52229FF )L_72);
		TableEntryU5BU5D_tB3F4A3142B2B8D2FFFA48D2697C81F352A3D4A94* L_73 = L_71;
		il2cpp_codegen_initobj((&V_0), sizeof(TableEntry_tDFCA5A567802D72C4E23BFC6CEA14C32F52229FF ));
		(&V_0)->set_Code_0(((int32_t)8185));
		(&V_0)->set_Bits_1((uint8_t)((int32_t)13));
		TableEntry_tDFCA5A567802D72C4E23BFC6CEA14C32F52229FF  L_74 = V_0;
		NullCheck(L_73);
		(L_73)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)36)), (TableEntry_tDFCA5A567802D72C4E23BFC6CEA14C32F52229FF )L_74);
		TableEntryU5BU5D_tB3F4A3142B2B8D2FFFA48D2697C81F352A3D4A94* L_75 = L_73;
		il2cpp_codegen_initobj((&V_0), sizeof(TableEntry_tDFCA5A567802D72C4E23BFC6CEA14C32F52229FF ));
		(&V_0)->set_Code_0(((int32_t)21));
		(&V_0)->set_Bits_1((uint8_t)6);
		TableEntry_tDFCA5A567802D72C4E23BFC6CEA14C32F52229FF  L_76 = V_0;
		NullCheck(L_75);
		(L_75)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)37)), (TableEntry_tDFCA5A567802D72C4E23BFC6CEA14C32F52229FF )L_76);
		TableEntryU5BU5D_tB3F4A3142B2B8D2FFFA48D2697C81F352A3D4A94* L_77 = L_75;
		il2cpp_codegen_initobj((&V_0), sizeof(TableEntry_tDFCA5A567802D72C4E23BFC6CEA14C32F52229FF ));
		(&V_0)->set_Code_0(((int32_t)248));
		(&V_0)->set_Bits_1((uint8_t)8);
		TableEntry_tDFCA5A567802D72C4E23BFC6CEA14C32F52229FF  L_78 = V_0;
		NullCheck(L_77);
		(L_77)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)38)), (TableEntry_tDFCA5A567802D72C4E23BFC6CEA14C32F52229FF )L_78);
		TableEntryU5BU5D_tB3F4A3142B2B8D2FFFA48D2697C81F352A3D4A94* L_79 = L_77;
		il2cpp_codegen_initobj((&V_0), sizeof(TableEntry_tDFCA5A567802D72C4E23BFC6CEA14C32F52229FF ));
		(&V_0)->set_Code_0(((int32_t)2042));
		(&V_0)->set_Bits_1((uint8_t)((int32_t)11));
		TableEntry_tDFCA5A567802D72C4E23BFC6CEA14C32F52229FF  L_80 = V_0;
		NullCheck(L_79);
		(L_79)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)39)), (TableEntry_tDFCA5A567802D72C4E23BFC6CEA14C32F52229FF )L_80);
		TableEntryU5BU5D_tB3F4A3142B2B8D2FFFA48D2697C81F352A3D4A94* L_81 = L_79;
		il2cpp_codegen_initobj((&V_0), sizeof(TableEntry_tDFCA5A567802D72C4E23BFC6CEA14C32F52229FF ));
		(&V_0)->set_Code_0(((int32_t)1018));
		(&V_0)->set_Bits_1((uint8_t)((int32_t)10));
		TableEntry_tDFCA5A567802D72C4E23BFC6CEA14C32F52229FF  L_82 = V_0;
		NullCheck(L_81);
		(L_81)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)40)), (TableEntry_tDFCA5A567802D72C4E23BFC6CEA14C32F52229FF )L_82);
		TableEntryU5BU5D_tB3F4A3142B2B8D2FFFA48D2697C81F352A3D4A94* L_83 = L_81;
		il2cpp_codegen_initobj((&V_0), sizeof(TableEntry_tDFCA5A567802D72C4E23BFC6CEA14C32F52229FF ));
		(&V_0)->set_Code_0(((int32_t)1019));
		(&V_0)->set_Bits_1((uint8_t)((int32_t)10));
		TableEntry_tDFCA5A567802D72C4E23BFC6CEA14C32F52229FF  L_84 = V_0;
		NullCheck(L_83);
		(L_83)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)41)), (TableEntry_tDFCA5A567802D72C4E23BFC6CEA14C32F52229FF )L_84);
		TableEntryU5BU5D_tB3F4A3142B2B8D2FFFA48D2697C81F352A3D4A94* L_85 = L_83;
		il2cpp_codegen_initobj((&V_0), sizeof(TableEntry_tDFCA5A567802D72C4E23BFC6CEA14C32F52229FF ));
		(&V_0)->set_Code_0(((int32_t)249));
		(&V_0)->set_Bits_1((uint8_t)8);
		TableEntry_tDFCA5A567802D72C4E23BFC6CEA14C32F52229FF  L_86 = V_0;
		NullCheck(L_85);
		(L_85)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)42)), (TableEntry_tDFCA5A567802D72C4E23BFC6CEA14C32F52229FF )L_86);
		TableEntryU5BU5D_tB3F4A3142B2B8D2FFFA48D2697C81F352A3D4A94* L_87 = L_85;
		il2cpp_codegen_initobj((&V_0), sizeof(TableEntry_tDFCA5A567802D72C4E23BFC6CEA14C32F52229FF ));
		(&V_0)->set_Code_0(((int32_t)2043));
		(&V_0)->set_Bits_1((uint8_t)((int32_t)11));
		TableEntry_tDFCA5A567802D72C4E23BFC6CEA14C32F52229FF  L_88 = V_0;
		NullCheck(L_87);
		(L_87)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)43)), (TableEntry_tDFCA5A567802D72C4E23BFC6CEA14C32F52229FF )L_88);
		TableEntryU5BU5D_tB3F4A3142B2B8D2FFFA48D2697C81F352A3D4A94* L_89 = L_87;
		il2cpp_codegen_initobj((&V_0), sizeof(TableEntry_tDFCA5A567802D72C4E23BFC6CEA14C32F52229FF ));
		(&V_0)->set_Code_0(((int32_t)250));
		(&V_0)->set_Bits_1((uint8_t)8);
		TableEntry_tDFCA5A567802D72C4E23BFC6CEA14C32F52229FF  L_90 = V_0;
		NullCheck(L_89);
		(L_89)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)44)), (TableEntry_tDFCA5A567802D72C4E23BFC6CEA14C32F52229FF )L_90);
		TableEntryU5BU5D_tB3F4A3142B2B8D2FFFA48D2697C81F352A3D4A94* L_91 = L_89;
		il2cpp_codegen_initobj((&V_0), sizeof(TableEntry_tDFCA5A567802D72C4E23BFC6CEA14C32F52229FF ));
		(&V_0)->set_Code_0(((int32_t)22));
		(&V_0)->set_Bits_1((uint8_t)6);
		TableEntry_tDFCA5A567802D72C4E23BFC6CEA14C32F52229FF  L_92 = V_0;
		NullCheck(L_91);
		(L_91)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)45)), (TableEntry_tDFCA5A567802D72C4E23BFC6CEA14C32F52229FF )L_92);
		TableEntryU5BU5D_tB3F4A3142B2B8D2FFFA48D2697C81F352A3D4A94* L_93 = L_91;
		il2cpp_codegen_initobj((&V_0), sizeof(TableEntry_tDFCA5A567802D72C4E23BFC6CEA14C32F52229FF ));
		(&V_0)->set_Code_0(((int32_t)23));
		(&V_0)->set_Bits_1((uint8_t)6);
		TableEntry_tDFCA5A567802D72C4E23BFC6CEA14C32F52229FF  L_94 = V_0;
		NullCheck(L_93);
		(L_93)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)46)), (TableEntry_tDFCA5A567802D72C4E23BFC6CEA14C32F52229FF )L_94);
		TableEntryU5BU5D_tB3F4A3142B2B8D2FFFA48D2697C81F352A3D4A94* L_95 = L_93;
		il2cpp_codegen_initobj((&V_0), sizeof(TableEntry_tDFCA5A567802D72C4E23BFC6CEA14C32F52229FF ));
		(&V_0)->set_Code_0(((int32_t)24));
		(&V_0)->set_Bits_1((uint8_t)6);
		TableEntry_tDFCA5A567802D72C4E23BFC6CEA14C32F52229FF  L_96 = V_0;
		NullCheck(L_95);
		(L_95)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)47)), (TableEntry_tDFCA5A567802D72C4E23BFC6CEA14C32F52229FF )L_96);
		TableEntryU5BU5D_tB3F4A3142B2B8D2FFFA48D2697C81F352A3D4A94* L_97 = L_95;
		il2cpp_codegen_initobj((&V_0), sizeof(TableEntry_tDFCA5A567802D72C4E23BFC6CEA14C32F52229FF ));
		(&V_0)->set_Code_0(0);
		(&V_0)->set_Bits_1((uint8_t)5);
		TableEntry_tDFCA5A567802D72C4E23BFC6CEA14C32F52229FF  L_98 = V_0;
		NullCheck(L_97);
		(L_97)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)48)), (TableEntry_tDFCA5A567802D72C4E23BFC6CEA14C32F52229FF )L_98);
		TableEntryU5BU5D_tB3F4A3142B2B8D2FFFA48D2697C81F352A3D4A94* L_99 = L_97;
		il2cpp_codegen_initobj((&V_0), sizeof(TableEntry_tDFCA5A567802D72C4E23BFC6CEA14C32F52229FF ));
		(&V_0)->set_Code_0(1);
		(&V_0)->set_Bits_1((uint8_t)5);
		TableEntry_tDFCA5A567802D72C4E23BFC6CEA14C32F52229FF  L_100 = V_0;
		NullCheck(L_99);
		(L_99)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)49)), (TableEntry_tDFCA5A567802D72C4E23BFC6CEA14C32F52229FF )L_100);
		TableEntryU5BU5D_tB3F4A3142B2B8D2FFFA48D2697C81F352A3D4A94* L_101 = L_99;
		il2cpp_codegen_initobj((&V_0), sizeof(TableEntry_tDFCA5A567802D72C4E23BFC6CEA14C32F52229FF ));
		(&V_0)->set_Code_0(2);
		(&V_0)->set_Bits_1((uint8_t)5);
		TableEntry_tDFCA5A567802D72C4E23BFC6CEA14C32F52229FF  L_102 = V_0;
		NullCheck(L_101);
		(L_101)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)50)), (TableEntry_tDFCA5A567802D72C4E23BFC6CEA14C32F52229FF )L_102);
		TableEntryU5BU5D_tB3F4A3142B2B8D2FFFA48D2697C81F352A3D4A94* L_103 = L_101;
		il2cpp_codegen_initobj((&V_0), sizeof(TableEntry_tDFCA5A567802D72C4E23BFC6CEA14C32F52229FF ));
		(&V_0)->set_Code_0(((int32_t)25));
		(&V_0)->set_Bits_1((uint8_t)6);
		TableEntry_tDFCA5A567802D72C4E23BFC6CEA14C32F52229FF  L_104 = V_0;
		NullCheck(L_103);
		(L_103)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)51)), (TableEntry_tDFCA5A567802D72C4E23BFC6CEA14C32F52229FF )L_104);
		TableEntryU5BU5D_tB3F4A3142B2B8D2FFFA48D2697C81F352A3D4A94* L_105 = L_103;
		il2cpp_codegen_initobj((&V_0), sizeof(TableEntry_tDFCA5A567802D72C4E23BFC6CEA14C32F52229FF ));
		(&V_0)->set_Code_0(((int32_t)26));
		(&V_0)->set_Bits_1((uint8_t)6);
		TableEntry_tDFCA5A567802D72C4E23BFC6CEA14C32F52229FF  L_106 = V_0;
		NullCheck(L_105);
		(L_105)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)52)), (TableEntry_tDFCA5A567802D72C4E23BFC6CEA14C32F52229FF )L_106);
		TableEntryU5BU5D_tB3F4A3142B2B8D2FFFA48D2697C81F352A3D4A94* L_107 = L_105;
		il2cpp_codegen_initobj((&V_0), sizeof(TableEntry_tDFCA5A567802D72C4E23BFC6CEA14C32F52229FF ));
		(&V_0)->set_Code_0(((int32_t)27));
		(&V_0)->set_Bits_1((uint8_t)6);
		TableEntry_tDFCA5A567802D72C4E23BFC6CEA14C32F52229FF  L_108 = V_0;
		NullCheck(L_107);
		(L_107)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)53)), (TableEntry_tDFCA5A567802D72C4E23BFC6CEA14C32F52229FF )L_108);
		TableEntryU5BU5D_tB3F4A3142B2B8D2FFFA48D2697C81F352A3D4A94* L_109 = L_107;
		il2cpp_codegen_initobj((&V_0), sizeof(TableEntry_tDFCA5A567802D72C4E23BFC6CEA14C32F52229FF ));
		(&V_0)->set_Code_0(((int32_t)28));
		(&V_0)->set_Bits_1((uint8_t)6);
		TableEntry_tDFCA5A567802D72C4E23BFC6CEA14C32F52229FF  L_110 = V_0;
		NullCheck(L_109);
		(L_109)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)54)), (TableEntry_tDFCA5A567802D72C4E23BFC6CEA14C32F52229FF )L_110);
		TableEntryU5BU5D_tB3F4A3142B2B8D2FFFA48D2697C81F352A3D4A94* L_111 = L_109;
		il2cpp_codegen_initobj((&V_0), sizeof(TableEntry_tDFCA5A567802D72C4E23BFC6CEA14C32F52229FF ));
		(&V_0)->set_Code_0(((int32_t)29));
		(&V_0)->set_Bits_1((uint8_t)6);
		TableEntry_tDFCA5A567802D72C4E23BFC6CEA14C32F52229FF  L_112 = V_0;
		NullCheck(L_111);
		(L_111)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)55)), (TableEntry_tDFCA5A567802D72C4E23BFC6CEA14C32F52229FF )L_112);
		TableEntryU5BU5D_tB3F4A3142B2B8D2FFFA48D2697C81F352A3D4A94* L_113 = L_111;
		il2cpp_codegen_initobj((&V_0), sizeof(TableEntry_tDFCA5A567802D72C4E23BFC6CEA14C32F52229FF ));
		(&V_0)->set_Code_0(((int32_t)30));
		(&V_0)->set_Bits_1((uint8_t)6);
		TableEntry_tDFCA5A567802D72C4E23BFC6CEA14C32F52229FF  L_114 = V_0;
		NullCheck(L_113);
		(L_113)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)56)), (TableEntry_tDFCA5A567802D72C4E23BFC6CEA14C32F52229FF )L_114);
		TableEntryU5BU5D_tB3F4A3142B2B8D2FFFA48D2697C81F352A3D4A94* L_115 = L_113;
		il2cpp_codegen_initobj((&V_0), sizeof(TableEntry_tDFCA5A567802D72C4E23BFC6CEA14C32F52229FF ));
		(&V_0)->set_Code_0(((int32_t)31));
		(&V_0)->set_Bits_1((uint8_t)6);
		TableEntry_tDFCA5A567802D72C4E23BFC6CEA14C32F52229FF  L_116 = V_0;
		NullCheck(L_115);
		(L_115)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)57)), (TableEntry_tDFCA5A567802D72C4E23BFC6CEA14C32F52229FF )L_116);
		TableEntryU5BU5D_tB3F4A3142B2B8D2FFFA48D2697C81F352A3D4A94* L_117 = L_115;
		il2cpp_codegen_initobj((&V_0), sizeof(TableEntry_tDFCA5A567802D72C4E23BFC6CEA14C32F52229FF ));
		(&V_0)->set_Code_0(((int32_t)92));
		(&V_0)->set_Bits_1((uint8_t)7);
		TableEntry_tDFCA5A567802D72C4E23BFC6CEA14C32F52229FF  L_118 = V_0;
		NullCheck(L_117);
		(L_117)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)58)), (TableEntry_tDFCA5A567802D72C4E23BFC6CEA14C32F52229FF )L_118);
		TableEntryU5BU5D_tB3F4A3142B2B8D2FFFA48D2697C81F352A3D4A94* L_119 = L_117;
		il2cpp_codegen_initobj((&V_0), sizeof(TableEntry_tDFCA5A567802D72C4E23BFC6CEA14C32F52229FF ));
		(&V_0)->set_Code_0(((int32_t)251));
		(&V_0)->set_Bits_1((uint8_t)8);
		TableEntry_tDFCA5A567802D72C4E23BFC6CEA14C32F52229FF  L_120 = V_0;
		NullCheck(L_119);
		(L_119)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)59)), (TableEntry_tDFCA5A567802D72C4E23BFC6CEA14C32F52229FF )L_120);
		TableEntryU5BU5D_tB3F4A3142B2B8D2FFFA48D2697C81F352A3D4A94* L_121 = L_119;
		il2cpp_codegen_initobj((&V_0), sizeof(TableEntry_tDFCA5A567802D72C4E23BFC6CEA14C32F52229FF ));
		(&V_0)->set_Code_0(((int32_t)32764));
		(&V_0)->set_Bits_1((uint8_t)((int32_t)15));
		TableEntry_tDFCA5A567802D72C4E23BFC6CEA14C32F52229FF  L_122 = V_0;
		NullCheck(L_121);
		(L_121)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)60)), (TableEntry_tDFCA5A567802D72C4E23BFC6CEA14C32F52229FF )L_122);
		TableEntryU5BU5D_tB3F4A3142B2B8D2FFFA48D2697C81F352A3D4A94* L_123 = L_121;
		il2cpp_codegen_initobj((&V_0), sizeof(TableEntry_tDFCA5A567802D72C4E23BFC6CEA14C32F52229FF ));
		(&V_0)->set_Code_0(((int32_t)32));
		(&V_0)->set_Bits_1((uint8_t)6);
		TableEntry_tDFCA5A567802D72C4E23BFC6CEA14C32F52229FF  L_124 = V_0;
		NullCheck(L_123);
		(L_123)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)61)), (TableEntry_tDFCA5A567802D72C4E23BFC6CEA14C32F52229FF )L_124);
		TableEntryU5BU5D_tB3F4A3142B2B8D2FFFA48D2697C81F352A3D4A94* L_125 = L_123;
		il2cpp_codegen_initobj((&V_0), sizeof(TableEntry_tDFCA5A567802D72C4E23BFC6CEA14C32F52229FF ));
		(&V_0)->set_Code_0(((int32_t)4091));
		(&V_0)->set_Bits_1((uint8_t)((int32_t)12));
		TableEntry_tDFCA5A567802D72C4E23BFC6CEA14C32F52229FF  L_126 = V_0;
		NullCheck(L_125);
		(L_125)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)62)), (TableEntry_tDFCA5A567802D72C4E23BFC6CEA14C32F52229FF )L_126);
		TableEntryU5BU5D_tB3F4A3142B2B8D2FFFA48D2697C81F352A3D4A94* L_127 = L_125;
		il2cpp_codegen_initobj((&V_0), sizeof(TableEntry_tDFCA5A567802D72C4E23BFC6CEA14C32F52229FF ));
		(&V_0)->set_Code_0(((int32_t)1020));
		(&V_0)->set_Bits_1((uint8_t)((int32_t)10));
		TableEntry_tDFCA5A567802D72C4E23BFC6CEA14C32F52229FF  L_128 = V_0;
		NullCheck(L_127);
		(L_127)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)63)), (TableEntry_tDFCA5A567802D72C4E23BFC6CEA14C32F52229FF )L_128);
		TableEntryU5BU5D_tB3F4A3142B2B8D2FFFA48D2697C81F352A3D4A94* L_129 = L_127;
		il2cpp_codegen_initobj((&V_0), sizeof(TableEntry_tDFCA5A567802D72C4E23BFC6CEA14C32F52229FF ));
		(&V_0)->set_Code_0(((int32_t)8186));
		(&V_0)->set_Bits_1((uint8_t)((int32_t)13));
		TableEntry_tDFCA5A567802D72C4E23BFC6CEA14C32F52229FF  L_130 = V_0;
		NullCheck(L_129);
		(L_129)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)64)), (TableEntry_tDFCA5A567802D72C4E23BFC6CEA14C32F52229FF )L_130);
		TableEntryU5BU5D_tB3F4A3142B2B8D2FFFA48D2697C81F352A3D4A94* L_131 = L_129;
		il2cpp_codegen_initobj((&V_0), sizeof(TableEntry_tDFCA5A567802D72C4E23BFC6CEA14C32F52229FF ));
		(&V_0)->set_Code_0(((int32_t)33));
		(&V_0)->set_Bits_1((uint8_t)6);
		TableEntry_tDFCA5A567802D72C4E23BFC6CEA14C32F52229FF  L_132 = V_0;
		NullCheck(L_131);
		(L_131)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)65)), (TableEntry_tDFCA5A567802D72C4E23BFC6CEA14C32F52229FF )L_132);
		TableEntryU5BU5D_tB3F4A3142B2B8D2FFFA48D2697C81F352A3D4A94* L_133 = L_131;
		il2cpp_codegen_initobj((&V_0), sizeof(TableEntry_tDFCA5A567802D72C4E23BFC6CEA14C32F52229FF ));
		(&V_0)->set_Code_0(((int32_t)93));
		(&V_0)->set_Bits_1((uint8_t)7);
		TableEntry_tDFCA5A567802D72C4E23BFC6CEA14C32F52229FF  L_134 = V_0;
		NullCheck(L_133);
		(L_133)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)66)), (TableEntry_tDFCA5A567802D72C4E23BFC6CEA14C32F52229FF )L_134);
		TableEntryU5BU5D_tB3F4A3142B2B8D2FFFA48D2697C81F352A3D4A94* L_135 = L_133;
		il2cpp_codegen_initobj((&V_0), sizeof(TableEntry_tDFCA5A567802D72C4E23BFC6CEA14C32F52229FF ));
		(&V_0)->set_Code_0(((int32_t)94));
		(&V_0)->set_Bits_1((uint8_t)7);
		TableEntry_tDFCA5A567802D72C4E23BFC6CEA14C32F52229FF  L_136 = V_0;
		NullCheck(L_135);
		(L_135)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)67)), (TableEntry_tDFCA5A567802D72C4E23BFC6CEA14C32F52229FF )L_136);
		TableEntryU5BU5D_tB3F4A3142B2B8D2FFFA48D2697C81F352A3D4A94* L_137 = L_135;
		il2cpp_codegen_initobj((&V_0), sizeof(TableEntry_tDFCA5A567802D72C4E23BFC6CEA14C32F52229FF ));
		(&V_0)->set_Code_0(((int32_t)95));
		(&V_0)->set_Bits_1((uint8_t)7);
		TableEntry_tDFCA5A567802D72C4E23BFC6CEA14C32F52229FF  L_138 = V_0;
		NullCheck(L_137);
		(L_137)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)68)), (TableEntry_tDFCA5A567802D72C4E23BFC6CEA14C32F52229FF )L_138);
		TableEntryU5BU5D_tB3F4A3142B2B8D2FFFA48D2697C81F352A3D4A94* L_139 = L_137;
		il2cpp_codegen_initobj((&V_0), sizeof(TableEntry_tDFCA5A567802D72C4E23BFC6CEA14C32F52229FF ));
		(&V_0)->set_Code_0(((int32_t)96));
		(&V_0)->set_Bits_1((uint8_t)7);
		TableEntry_tDFCA5A567802D72C4E23BFC6CEA14C32F52229FF  L_140 = V_0;
		NullCheck(L_139);
		(L_139)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)69)), (TableEntry_tDFCA5A567802D72C4E23BFC6CEA14C32F52229FF )L_140);
		TableEntryU5BU5D_tB3F4A3142B2B8D2FFFA48D2697C81F352A3D4A94* L_141 = L_139;
		il2cpp_codegen_initobj((&V_0), sizeof(TableEntry_tDFCA5A567802D72C4E23BFC6CEA14C32F52229FF ));
		(&V_0)->set_Code_0(((int32_t)97));
		(&V_0)->set_Bits_1((uint8_t)7);
		TableEntry_tDFCA5A567802D72C4E23BFC6CEA14C32F52229FF  L_142 = V_0;
		NullCheck(L_141);
		(L_141)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)70)), (TableEntry_tDFCA5A567802D72C4E23BFC6CEA14C32F52229FF )L_142);
		TableEntryU5BU5D_tB3F4A3142B2B8D2FFFA48D2697C81F352A3D4A94* L_143 = L_141;
		il2cpp_codegen_initobj((&V_0), sizeof(TableEntry_tDFCA5A567802D72C4E23BFC6CEA14C32F52229FF ));
		(&V_0)->set_Code_0(((int32_t)98));
		(&V_0)->set_Bits_1((uint8_t)7);
		TableEntry_tDFCA5A567802D72C4E23BFC6CEA14C32F52229FF  L_144 = V_0;
		NullCheck(L_143);
		(L_143)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)71)), (TableEntry_tDFCA5A567802D72C4E23BFC6CEA14C32F52229FF )L_144);
		TableEntryU5BU5D_tB3F4A3142B2B8D2FFFA48D2697C81F352A3D4A94* L_145 = L_143;
		il2cpp_codegen_initobj((&V_0), sizeof(TableEntry_tDFCA5A567802D72C4E23BFC6CEA14C32F52229FF ));
		(&V_0)->set_Code_0(((int32_t)99));
		(&V_0)->set_Bits_1((uint8_t)7);
		TableEntry_tDFCA5A567802D72C4E23BFC6CEA14C32F52229FF  L_146 = V_0;
		NullCheck(L_145);
		(L_145)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)72)), (TableEntry_tDFCA5A567802D72C4E23BFC6CEA14C32F52229FF )L_146);
		TableEntryU5BU5D_tB3F4A3142B2B8D2FFFA48D2697C81F352A3D4A94* L_147 = L_145;
		il2cpp_codegen_initobj((&V_0), sizeof(TableEntry_tDFCA5A567802D72C4E23BFC6CEA14C32F52229FF ));
		(&V_0)->set_Code_0(((int32_t)100));
		(&V_0)->set_Bits_1((uint8_t)7);
		TableEntry_tDFCA5A567802D72C4E23BFC6CEA14C32F52229FF  L_148 = V_0;
		NullCheck(L_147);
		(L_147)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)73)), (TableEntry_tDFCA5A567802D72C4E23BFC6CEA14C32F52229FF )L_148);
		TableEntryU5BU5D_tB3F4A3142B2B8D2FFFA48D2697C81F352A3D4A94* L_149 = L_147;
		il2cpp_codegen_initobj((&V_0), sizeof(TableEntry_tDFCA5A567802D72C4E23BFC6CEA14C32F52229FF ));
		(&V_0)->set_Code_0(((int32_t)101));
		(&V_0)->set_Bits_1((uint8_t)7);
		TableEntry_tDFCA5A567802D72C4E23BFC6CEA14C32F52229FF  L_150 = V_0;
		NullCheck(L_149);
		(L_149)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)74)), (TableEntry_tDFCA5A567802D72C4E23BFC6CEA14C32F52229FF )L_150);
		TableEntryU5BU5D_tB3F4A3142B2B8D2FFFA48D2697C81F352A3D4A94* L_151 = L_149;
		il2cpp_codegen_initobj((&V_0), sizeof(TableEntry_tDFCA5A567802D72C4E23BFC6CEA14C32F52229FF ));
		(&V_0)->set_Code_0(((int32_t)102));
		(&V_0)->set_Bits_1((uint8_t)7);
		TableEntry_tDFCA5A567802D72C4E23BFC6CEA14C32F52229FF  L_152 = V_0;
		NullCheck(L_151);
		(L_151)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)75)), (TableEntry_tDFCA5A567802D72C4E23BFC6CEA14C32F52229FF )L_152);
		TableEntryU5BU5D_tB3F4A3142B2B8D2FFFA48D2697C81F352A3D4A94* L_153 = L_151;
		il2cpp_codegen_initobj((&V_0), sizeof(TableEntry_tDFCA5A567802D72C4E23BFC6CEA14C32F52229FF ));
		(&V_0)->set_Code_0(((int32_t)103));
		(&V_0)->set_Bits_1((uint8_t)7);
		TableEntry_tDFCA5A567802D72C4E23BFC6CEA14C32F52229FF  L_154 = V_0;
		NullCheck(L_153);
		(L_153)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)76)), (TableEntry_tDFCA5A567802D72C4E23BFC6CEA14C32F52229FF )L_154);
		TableEntryU5BU5D_tB3F4A3142B2B8D2FFFA48D2697C81F352A3D4A94* L_155 = L_153;
		il2cpp_codegen_initobj((&V_0), sizeof(TableEntry_tDFCA5A567802D72C4E23BFC6CEA14C32F52229FF ));
		(&V_0)->set_Code_0(((int32_t)104));
		(&V_0)->set_Bits_1((uint8_t)7);
		TableEntry_tDFCA5A567802D72C4E23BFC6CEA14C32F52229FF  L_156 = V_0;
		NullCheck(L_155);
		(L_155)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)77)), (TableEntry_tDFCA5A567802D72C4E23BFC6CEA14C32F52229FF )L_156);
		TableEntryU5BU5D_tB3F4A3142B2B8D2FFFA48D2697C81F352A3D4A94* L_157 = L_155;
		il2cpp_codegen_initobj((&V_0), sizeof(TableEntry_tDFCA5A567802D72C4E23BFC6CEA14C32F52229FF ));
		(&V_0)->set_Code_0(((int32_t)105));
		(&V_0)->set_Bits_1((uint8_t)7);
		TableEntry_tDFCA5A567802D72C4E23BFC6CEA14C32F52229FF  L_158 = V_0;
		NullCheck(L_157);
		(L_157)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)78)), (TableEntry_tDFCA5A567802D72C4E23BFC6CEA14C32F52229FF )L_158);
		TableEntryU5BU5D_tB3F4A3142B2B8D2FFFA48D2697C81F352A3D4A94* L_159 = L_157;
		il2cpp_codegen_initobj((&V_0), sizeof(TableEntry_tDFCA5A567802D72C4E23BFC6CEA14C32F52229FF ));
		(&V_0)->set_Code_0(((int32_t)106));
		(&V_0)->set_Bits_1((uint8_t)7);
		TableEntry_tDFCA5A567802D72C4E23BFC6CEA14C32F52229FF  L_160 = V_0;
		NullCheck(L_159);
		(L_159)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)79)), (TableEntry_tDFCA5A567802D72C4E23BFC6CEA14C32F52229FF )L_160);
		TableEntryU5BU5D_tB3F4A3142B2B8D2FFFA48D2697C81F352A3D4A94* L_161 = L_159;
		il2cpp_codegen_initobj((&V_0), sizeof(TableEntry_tDFCA5A567802D72C4E23BFC6CEA14C32F52229FF ));
		(&V_0)->set_Code_0(((int32_t)107));
		(&V_0)->set_Bits_1((uint8_t)7);
		TableEntry_tDFCA5A567802D72C4E23BFC6CEA14C32F52229FF  L_162 = V_0;
		NullCheck(L_161);
		(L_161)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)80)), (TableEntry_tDFCA5A567802D72C4E23BFC6CEA14C32F52229FF )L_162);
		TableEntryU5BU5D_tB3F4A3142B2B8D2FFFA48D2697C81F352A3D4A94* L_163 = L_161;
		il2cpp_codegen_initobj((&V_0), sizeof(TableEntry_tDFCA5A567802D72C4E23BFC6CEA14C32F52229FF ));
		(&V_0)->set_Code_0(((int32_t)108));
		(&V_0)->set_Bits_1((uint8_t)7);
		TableEntry_tDFCA5A567802D72C4E23BFC6CEA14C32F52229FF  L_164 = V_0;
		NullCheck(L_163);
		(L_163)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)81)), (TableEntry_tDFCA5A567802D72C4E23BFC6CEA14C32F52229FF )L_164);
		TableEntryU5BU5D_tB3F4A3142B2B8D2FFFA48D2697C81F352A3D4A94* L_165 = L_163;
		il2cpp_codegen_initobj((&V_0), sizeof(TableEntry_tDFCA5A567802D72C4E23BFC6CEA14C32F52229FF ));
		(&V_0)->set_Code_0(((int32_t)109));
		(&V_0)->set_Bits_1((uint8_t)7);
		TableEntry_tDFCA5A567802D72C4E23BFC6CEA14C32F52229FF  L_166 = V_0;
		NullCheck(L_165);
		(L_165)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)82)), (TableEntry_tDFCA5A567802D72C4E23BFC6CEA14C32F52229FF )L_166);
		TableEntryU5BU5D_tB3F4A3142B2B8D2FFFA48D2697C81F352A3D4A94* L_167 = L_165;
		il2cpp_codegen_initobj((&V_0), sizeof(TableEntry_tDFCA5A567802D72C4E23BFC6CEA14C32F52229FF ));
		(&V_0)->set_Code_0(((int32_t)110));
		(&V_0)->set_Bits_1((uint8_t)7);
		TableEntry_tDFCA5A567802D72C4E23BFC6CEA14C32F52229FF  L_168 = V_0;
		NullCheck(L_167);
		(L_167)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)83)), (TableEntry_tDFCA5A567802D72C4E23BFC6CEA14C32F52229FF )L_168);
		TableEntryU5BU5D_tB3F4A3142B2B8D2FFFA48D2697C81F352A3D4A94* L_169 = L_167;
		il2cpp_codegen_initobj((&V_0), sizeof(TableEntry_tDFCA5A567802D72C4E23BFC6CEA14C32F52229FF ));
		(&V_0)->set_Code_0(((int32_t)111));
		(&V_0)->set_Bits_1((uint8_t)7);
		TableEntry_tDFCA5A567802D72C4E23BFC6CEA14C32F52229FF  L_170 = V_0;
		NullCheck(L_169);
		(L_169)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)84)), (TableEntry_tDFCA5A567802D72C4E23BFC6CEA14C32F52229FF )L_170);
		TableEntryU5BU5D_tB3F4A3142B2B8D2FFFA48D2697C81F352A3D4A94* L_171 = L_169;
		il2cpp_codegen_initobj((&V_0), sizeof(TableEntry_tDFCA5A567802D72C4E23BFC6CEA14C32F52229FF ));
		(&V_0)->set_Code_0(((int32_t)112));
		(&V_0)->set_Bits_1((uint8_t)7);
		TableEntry_tDFCA5A567802D72C4E23BFC6CEA14C32F52229FF  L_172 = V_0;
		NullCheck(L_171);
		(L_171)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)85)), (TableEntry_tDFCA5A567802D72C4E23BFC6CEA14C32F52229FF )L_172);
		TableEntryU5BU5D_tB3F4A3142B2B8D2FFFA48D2697C81F352A3D4A94* L_173 = L_171;
		il2cpp_codegen_initobj((&V_0), sizeof(TableEntry_tDFCA5A567802D72C4E23BFC6CEA14C32F52229FF ));
		(&V_0)->set_Code_0(((int32_t)113));
		(&V_0)->set_Bits_1((uint8_t)7);
		TableEntry_tDFCA5A567802D72C4E23BFC6CEA14C32F52229FF  L_174 = V_0;
		NullCheck(L_173);
		(L_173)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)86)), (TableEntry_tDFCA5A567802D72C4E23BFC6CEA14C32F52229FF )L_174);
		TableEntryU5BU5D_tB3F4A3142B2B8D2FFFA48D2697C81F352A3D4A94* L_175 = L_173;
		il2cpp_codegen_initobj((&V_0), sizeof(TableEntry_tDFCA5A567802D72C4E23BFC6CEA14C32F52229FF ));
		(&V_0)->set_Code_0(((int32_t)114));
		(&V_0)->set_Bits_1((uint8_t)7);
		TableEntry_tDFCA5A567802D72C4E23BFC6CEA14C32F52229FF  L_176 = V_0;
		NullCheck(L_175);
		(L_175)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)87)), (TableEntry_tDFCA5A567802D72C4E23BFC6CEA14C32F52229FF )L_176);
		TableEntryU5BU5D_tB3F4A3142B2B8D2FFFA48D2697C81F352A3D4A94* L_177 = L_175;
		il2cpp_codegen_initobj((&V_0), sizeof(TableEntry_tDFCA5A567802D72C4E23BFC6CEA14C32F52229FF ));
		(&V_0)->set_Code_0(((int32_t)252));
		(&V_0)->set_Bits_1((uint8_t)8);
		TableEntry_tDFCA5A567802D72C4E23BFC6CEA14C32F52229FF  L_178 = V_0;
		NullCheck(L_177);
		(L_177)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)88)), (TableEntry_tDFCA5A567802D72C4E23BFC6CEA14C32F52229FF )L_178);
		TableEntryU5BU5D_tB3F4A3142B2B8D2FFFA48D2697C81F352A3D4A94* L_179 = L_177;
		il2cpp_codegen_initobj((&V_0), sizeof(TableEntry_tDFCA5A567802D72C4E23BFC6CEA14C32F52229FF ));
		(&V_0)->set_Code_0(((int32_t)115));
		(&V_0)->set_Bits_1((uint8_t)7);
		TableEntry_tDFCA5A567802D72C4E23BFC6CEA14C32F52229FF  L_180 = V_0;
		NullCheck(L_179);
		(L_179)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)89)), (TableEntry_tDFCA5A567802D72C4E23BFC6CEA14C32F52229FF )L_180);
		TableEntryU5BU5D_tB3F4A3142B2B8D2FFFA48D2697C81F352A3D4A94* L_181 = L_179;
		il2cpp_codegen_initobj((&V_0), sizeof(TableEntry_tDFCA5A567802D72C4E23BFC6CEA14C32F52229FF ));
		(&V_0)->set_Code_0(((int32_t)253));
		(&V_0)->set_Bits_1((uint8_t)8);
		TableEntry_tDFCA5A567802D72C4E23BFC6CEA14C32F52229FF  L_182 = V_0;
		NullCheck(L_181);
		(L_181)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)90)), (TableEntry_tDFCA5A567802D72C4E23BFC6CEA14C32F52229FF )L_182);
		TableEntryU5BU5D_tB3F4A3142B2B8D2FFFA48D2697C81F352A3D4A94* L_183 = L_181;
		il2cpp_codegen_initobj((&V_0), sizeof(TableEntry_tDFCA5A567802D72C4E23BFC6CEA14C32F52229FF ));
		(&V_0)->set_Code_0(((int32_t)8187));
		(&V_0)->set_Bits_1((uint8_t)((int32_t)13));
		TableEntry_tDFCA5A567802D72C4E23BFC6CEA14C32F52229FF  L_184 = V_0;
		NullCheck(L_183);
		(L_183)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)91)), (TableEntry_tDFCA5A567802D72C4E23BFC6CEA14C32F52229FF )L_184);
		TableEntryU5BU5D_tB3F4A3142B2B8D2FFFA48D2697C81F352A3D4A94* L_185 = L_183;
		il2cpp_codegen_initobj((&V_0), sizeof(TableEntry_tDFCA5A567802D72C4E23BFC6CEA14C32F52229FF ));
		(&V_0)->set_Code_0(((int32_t)524272));
		(&V_0)->set_Bits_1((uint8_t)((int32_t)19));
		TableEntry_tDFCA5A567802D72C4E23BFC6CEA14C32F52229FF  L_186 = V_0;
		NullCheck(L_185);
		(L_185)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)92)), (TableEntry_tDFCA5A567802D72C4E23BFC6CEA14C32F52229FF )L_186);
		TableEntryU5BU5D_tB3F4A3142B2B8D2FFFA48D2697C81F352A3D4A94* L_187 = L_185;
		il2cpp_codegen_initobj((&V_0), sizeof(TableEntry_tDFCA5A567802D72C4E23BFC6CEA14C32F52229FF ));
		(&V_0)->set_Code_0(((int32_t)8188));
		(&V_0)->set_Bits_1((uint8_t)((int32_t)13));
		TableEntry_tDFCA5A567802D72C4E23BFC6CEA14C32F52229FF  L_188 = V_0;
		NullCheck(L_187);
		(L_187)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)93)), (TableEntry_tDFCA5A567802D72C4E23BFC6CEA14C32F52229FF )L_188);
		TableEntryU5BU5D_tB3F4A3142B2B8D2FFFA48D2697C81F352A3D4A94* L_189 = L_187;
		il2cpp_codegen_initobj((&V_0), sizeof(TableEntry_tDFCA5A567802D72C4E23BFC6CEA14C32F52229FF ));
		(&V_0)->set_Code_0(((int32_t)16380));
		(&V_0)->set_Bits_1((uint8_t)((int32_t)14));
		TableEntry_tDFCA5A567802D72C4E23BFC6CEA14C32F52229FF  L_190 = V_0;
		NullCheck(L_189);
		(L_189)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)94)), (TableEntry_tDFCA5A567802D72C4E23BFC6CEA14C32F52229FF )L_190);
		TableEntryU5BU5D_tB3F4A3142B2B8D2FFFA48D2697C81F352A3D4A94* L_191 = L_189;
		il2cpp_codegen_initobj((&V_0), sizeof(TableEntry_tDFCA5A567802D72C4E23BFC6CEA14C32F52229FF ));
		(&V_0)->set_Code_0(((int32_t)34));
		(&V_0)->set_Bits_1((uint8_t)6);
		TableEntry_tDFCA5A567802D72C4E23BFC6CEA14C32F52229FF  L_192 = V_0;
		NullCheck(L_191);
		(L_191)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)95)), (TableEntry_tDFCA5A567802D72C4E23BFC6CEA14C32F52229FF )L_192);
		TableEntryU5BU5D_tB3F4A3142B2B8D2FFFA48D2697C81F352A3D4A94* L_193 = L_191;
		il2cpp_codegen_initobj((&V_0), sizeof(TableEntry_tDFCA5A567802D72C4E23BFC6CEA14C32F52229FF ));
		(&V_0)->set_Code_0(((int32_t)32765));
		(&V_0)->set_Bits_1((uint8_t)((int32_t)15));
		TableEntry_tDFCA5A567802D72C4E23BFC6CEA14C32F52229FF  L_194 = V_0;
		NullCheck(L_193);
		(L_193)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)96)), (TableEntry_tDFCA5A567802D72C4E23BFC6CEA14C32F52229FF )L_194);
		TableEntryU5BU5D_tB3F4A3142B2B8D2FFFA48D2697C81F352A3D4A94* L_195 = L_193;
		il2cpp_codegen_initobj((&V_0), sizeof(TableEntry_tDFCA5A567802D72C4E23BFC6CEA14C32F52229FF ));
		(&V_0)->set_Code_0(3);
		(&V_0)->set_Bits_1((uint8_t)5);
		TableEntry_tDFCA5A567802D72C4E23BFC6CEA14C32F52229FF  L_196 = V_0;
		NullCheck(L_195);
		(L_195)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)97)), (TableEntry_tDFCA5A567802D72C4E23BFC6CEA14C32F52229FF )L_196);
		TableEntryU5BU5D_tB3F4A3142B2B8D2FFFA48D2697C81F352A3D4A94* L_197 = L_195;
		il2cpp_codegen_initobj((&V_0), sizeof(TableEntry_tDFCA5A567802D72C4E23BFC6CEA14C32F52229FF ));
		(&V_0)->set_Code_0(((int32_t)35));
		(&V_0)->set_Bits_1((uint8_t)6);
		TableEntry_tDFCA5A567802D72C4E23BFC6CEA14C32F52229FF  L_198 = V_0;
		NullCheck(L_197);
		(L_197)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)98)), (TableEntry_tDFCA5A567802D72C4E23BFC6CEA14C32F52229FF )L_198);
		TableEntryU5BU5D_tB3F4A3142B2B8D2FFFA48D2697C81F352A3D4A94* L_199 = L_197;
		il2cpp_codegen_initobj((&V_0), sizeof(TableEntry_tDFCA5A567802D72C4E23BFC6CEA14C32F52229FF ));
		(&V_0)->set_Code_0(4);
		(&V_0)->set_Bits_1((uint8_t)5);
		TableEntry_tDFCA5A567802D72C4E23BFC6CEA14C32F52229FF  L_200 = V_0;
		NullCheck(L_199);
		(L_199)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)99)), (TableEntry_tDFCA5A567802D72C4E23BFC6CEA14C32F52229FF )L_200);
		TableEntryU5BU5D_tB3F4A3142B2B8D2FFFA48D2697C81F352A3D4A94* L_201 = L_199;
		il2cpp_codegen_initobj((&V_0), sizeof(TableEntry_tDFCA5A567802D72C4E23BFC6CEA14C32F52229FF ));
		(&V_0)->set_Code_0(((int32_t)36));
		(&V_0)->set_Bits_1((uint8_t)6);
		TableEntry_tDFCA5A567802D72C4E23BFC6CEA14C32F52229FF  L_202 = V_0;
		NullCheck(L_201);
		(L_201)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)100)), (TableEntry_tDFCA5A567802D72C4E23BFC6CEA14C32F52229FF )L_202);
		TableEntryU5BU5D_tB3F4A3142B2B8D2FFFA48D2697C81F352A3D4A94* L_203 = L_201;
		il2cpp_codegen_initobj((&V_0), sizeof(TableEntry_tDFCA5A567802D72C4E23BFC6CEA14C32F52229FF ));
		(&V_0)->set_Code_0(5);
		(&V_0)->set_Bits_1((uint8_t)5);
		TableEntry_tDFCA5A567802D72C4E23BFC6CEA14C32F52229FF  L_204 = V_0;
		NullCheck(L_203);
		(L_203)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)101)), (TableEntry_tDFCA5A567802D72C4E23BFC6CEA14C32F52229FF )L_204);
		TableEntryU5BU5D_tB3F4A3142B2B8D2FFFA48D2697C81F352A3D4A94* L_205 = L_203;
		il2cpp_codegen_initobj((&V_0), sizeof(TableEntry_tDFCA5A567802D72C4E23BFC6CEA14C32F52229FF ));
		(&V_0)->set_Code_0(((int32_t)37));
		(&V_0)->set_Bits_1((uint8_t)6);
		TableEntry_tDFCA5A567802D72C4E23BFC6CEA14C32F52229FF  L_206 = V_0;
		NullCheck(L_205);
		(L_205)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)102)), (TableEntry_tDFCA5A567802D72C4E23BFC6CEA14C32F52229FF )L_206);
		TableEntryU5BU5D_tB3F4A3142B2B8D2FFFA48D2697C81F352A3D4A94* L_207 = L_205;
		il2cpp_codegen_initobj((&V_0), sizeof(TableEntry_tDFCA5A567802D72C4E23BFC6CEA14C32F52229FF ));
		(&V_0)->set_Code_0(((int32_t)38));
		(&V_0)->set_Bits_1((uint8_t)6);
		TableEntry_tDFCA5A567802D72C4E23BFC6CEA14C32F52229FF  L_208 = V_0;
		NullCheck(L_207);
		(L_207)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)103)), (TableEntry_tDFCA5A567802D72C4E23BFC6CEA14C32F52229FF )L_208);
		TableEntryU5BU5D_tB3F4A3142B2B8D2FFFA48D2697C81F352A3D4A94* L_209 = L_207;
		il2cpp_codegen_initobj((&V_0), sizeof(TableEntry_tDFCA5A567802D72C4E23BFC6CEA14C32F52229FF ));
		(&V_0)->set_Code_0(((int32_t)39));
		(&V_0)->set_Bits_1((uint8_t)6);
		TableEntry_tDFCA5A567802D72C4E23BFC6CEA14C32F52229FF  L_210 = V_0;
		NullCheck(L_209);
		(L_209)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)104)), (TableEntry_tDFCA5A567802D72C4E23BFC6CEA14C32F52229FF )L_210);
		TableEntryU5BU5D_tB3F4A3142B2B8D2FFFA48D2697C81F352A3D4A94* L_211 = L_209;
		il2cpp_codegen_initobj((&V_0), sizeof(TableEntry_tDFCA5A567802D72C4E23BFC6CEA14C32F52229FF ));
		(&V_0)->set_Code_0(6);
		(&V_0)->set_Bits_1((uint8_t)5);
		TableEntry_tDFCA5A567802D72C4E23BFC6CEA14C32F52229FF  L_212 = V_0;
		NullCheck(L_211);
		(L_211)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)105)), (TableEntry_tDFCA5A567802D72C4E23BFC6CEA14C32F52229FF )L_212);
		TableEntryU5BU5D_tB3F4A3142B2B8D2FFFA48D2697C81F352A3D4A94* L_213 = L_211;
		il2cpp_codegen_initobj((&V_0), sizeof(TableEntry_tDFCA5A567802D72C4E23BFC6CEA14C32F52229FF ));
		(&V_0)->set_Code_0(((int32_t)116));
		(&V_0)->set_Bits_1((uint8_t)7);
		TableEntry_tDFCA5A567802D72C4E23BFC6CEA14C32F52229FF  L_214 = V_0;
		NullCheck(L_213);
		(L_213)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)106)), (TableEntry_tDFCA5A567802D72C4E23BFC6CEA14C32F52229FF )L_214);
		TableEntryU5BU5D_tB3F4A3142B2B8D2FFFA48D2697C81F352A3D4A94* L_215 = L_213;
		il2cpp_codegen_initobj((&V_0), sizeof(TableEntry_tDFCA5A567802D72C4E23BFC6CEA14C32F52229FF ));
		(&V_0)->set_Code_0(((int32_t)117));
		(&V_0)->set_Bits_1((uint8_t)7);
		TableEntry_tDFCA5A567802D72C4E23BFC6CEA14C32F52229FF  L_216 = V_0;
		NullCheck(L_215);
		(L_215)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)107)), (TableEntry_tDFCA5A567802D72C4E23BFC6CEA14C32F52229FF )L_216);
		TableEntryU5BU5D_tB3F4A3142B2B8D2FFFA48D2697C81F352A3D4A94* L_217 = L_215;
		il2cpp_codegen_initobj((&V_0), sizeof(TableEntry_tDFCA5A567802D72C4E23BFC6CEA14C32F52229FF ));
		(&V_0)->set_Code_0(((int32_t)40));
		(&V_0)->set_Bits_1((uint8_t)6);
		TableEntry_tDFCA5A567802D72C4E23BFC6CEA14C32F52229FF  L_218 = V_0;
		NullCheck(L_217);
		(L_217)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)108)), (TableEntry_tDFCA5A567802D72C4E23BFC6CEA14C32F52229FF )L_218);
		TableEntryU5BU5D_tB3F4A3142B2B8D2FFFA48D2697C81F352A3D4A94* L_219 = L_217;
		il2cpp_codegen_initobj((&V_0), sizeof(TableEntry_tDFCA5A567802D72C4E23BFC6CEA14C32F52229FF ));
		(&V_0)->set_Code_0(((int32_t)41));
		(&V_0)->set_Bits_1((uint8_t)6);
		TableEntry_tDFCA5A567802D72C4E23BFC6CEA14C32F52229FF  L_220 = V_0;
		NullCheck(L_219);
		(L_219)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)109)), (TableEntry_tDFCA5A567802D72C4E23BFC6CEA14C32F52229FF )L_220);
		TableEntryU5BU5D_tB3F4A3142B2B8D2FFFA48D2697C81F352A3D4A94* L_221 = L_219;
		il2cpp_codegen_initobj((&V_0), sizeof(TableEntry_tDFCA5A567802D72C4E23BFC6CEA14C32F52229FF ));
		(&V_0)->set_Code_0(((int32_t)42));
		(&V_0)->set_Bits_1((uint8_t)6);
		TableEntry_tDFCA5A567802D72C4E23BFC6CEA14C32F52229FF  L_222 = V_0;
		NullCheck(L_221);
		(L_221)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)110)), (TableEntry_tDFCA5A567802D72C4E23BFC6CEA14C32F52229FF )L_222);
		TableEntryU5BU5D_tB3F4A3142B2B8D2FFFA48D2697C81F352A3D4A94* L_223 = L_221;
		il2cpp_codegen_initobj((&V_0), sizeof(TableEntry_tDFCA5A567802D72C4E23BFC6CEA14C32F52229FF ));
		(&V_0)->set_Code_0(7);
		(&V_0)->set_Bits_1((uint8_t)5);
		TableEntry_tDFCA5A567802D72C4E23BFC6CEA14C32F52229FF  L_224 = V_0;
		NullCheck(L_223);
		(L_223)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)111)), (TableEntry_tDFCA5A567802D72C4E23BFC6CEA14C32F52229FF )L_224);
		TableEntryU5BU5D_tB3F4A3142B2B8D2FFFA48D2697C81F352A3D4A94* L_225 = L_223;
		il2cpp_codegen_initobj((&V_0), sizeof(TableEntry_tDFCA5A567802D72C4E23BFC6CEA14C32F52229FF ));
		(&V_0)->set_Code_0(((int32_t)43));
		(&V_0)->set_Bits_1((uint8_t)6);
		TableEntry_tDFCA5A567802D72C4E23BFC6CEA14C32F52229FF  L_226 = V_0;
		NullCheck(L_225);
		(L_225)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)112)), (TableEntry_tDFCA5A567802D72C4E23BFC6CEA14C32F52229FF )L_226);
		TableEntryU5BU5D_tB3F4A3142B2B8D2FFFA48D2697C81F352A3D4A94* L_227 = L_225;
		il2cpp_codegen_initobj((&V_0), sizeof(TableEntry_tDFCA5A567802D72C4E23BFC6CEA14C32F52229FF ));
		(&V_0)->set_Code_0(((int32_t)118));
		(&V_0)->set_Bits_1((uint8_t)7);
		TableEntry_tDFCA5A567802D72C4E23BFC6CEA14C32F52229FF  L_228 = V_0;
		NullCheck(L_227);
		(L_227)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)113)), (TableEntry_tDFCA5A567802D72C4E23BFC6CEA14C32F52229FF )L_228);
		TableEntryU5BU5D_tB3F4A3142B2B8D2FFFA48D2697C81F352A3D4A94* L_229 = L_227;
		il2cpp_codegen_initobj((&V_0), sizeof(TableEntry_tDFCA5A567802D72C4E23BFC6CEA14C32F52229FF ));
		(&V_0)->set_Code_0(((int32_t)44));
		(&V_0)->set_Bits_1((uint8_t)6);
		TableEntry_tDFCA5A567802D72C4E23BFC6CEA14C32F52229FF  L_230 = V_0;
		NullCheck(L_229);
		(L_229)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)114)), (TableEntry_tDFCA5A567802D72C4E23BFC6CEA14C32F52229FF )L_230);
		TableEntryU5BU5D_tB3F4A3142B2B8D2FFFA48D2697C81F352A3D4A94* L_231 = L_229;
		il2cpp_codegen_initobj((&V_0), sizeof(TableEntry_tDFCA5A567802D72C4E23BFC6CEA14C32F52229FF ));
		(&V_0)->set_Code_0(8);
		(&V_0)->set_Bits_1((uint8_t)5);
		TableEntry_tDFCA5A567802D72C4E23BFC6CEA14C32F52229FF  L_232 = V_0;
		NullCheck(L_231);
		(L_231)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)115)), (TableEntry_tDFCA5A567802D72C4E23BFC6CEA14C32F52229FF )L_232);
		TableEntryU5BU5D_tB3F4A3142B2B8D2FFFA48D2697C81F352A3D4A94* L_233 = L_231;
		il2cpp_codegen_initobj((&V_0), sizeof(TableEntry_tDFCA5A567802D72C4E23BFC6CEA14C32F52229FF ));
		(&V_0)->set_Code_0(((int32_t)9));
		(&V_0)->set_Bits_1((uint8_t)5);
		TableEntry_tDFCA5A567802D72C4E23BFC6CEA14C32F52229FF  L_234 = V_0;
		NullCheck(L_233);
		(L_233)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)116)), (TableEntry_tDFCA5A567802D72C4E23BFC6CEA14C32F52229FF )L_234);
		TableEntryU5BU5D_tB3F4A3142B2B8D2FFFA48D2697C81F352A3D4A94* L_235 = L_233;
		il2cpp_codegen_initobj((&V_0), sizeof(TableEntry_tDFCA5A567802D72C4E23BFC6CEA14C32F52229FF ));
		(&V_0)->set_Code_0(((int32_t)45));
		(&V_0)->set_Bits_1((uint8_t)6);
		TableEntry_tDFCA5A567802D72C4E23BFC6CEA14C32F52229FF  L_236 = V_0;
		NullCheck(L_235);
		(L_235)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)117)), (TableEntry_tDFCA5A567802D72C4E23BFC6CEA14C32F52229FF )L_236);
		TableEntryU5BU5D_tB3F4A3142B2B8D2FFFA48D2697C81F352A3D4A94* L_237 = L_235;
		il2cpp_codegen_initobj((&V_0), sizeof(TableEntry_tDFCA5A567802D72C4E23BFC6CEA14C32F52229FF ));
		(&V_0)->set_Code_0(((int32_t)119));
		(&V_0)->set_Bits_1((uint8_t)7);
		TableEntry_tDFCA5A567802D72C4E23BFC6CEA14C32F52229FF  L_238 = V_0;
		NullCheck(L_237);
		(L_237)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)118)), (TableEntry_tDFCA5A567802D72C4E23BFC6CEA14C32F52229FF )L_238);
		TableEntryU5BU5D_tB3F4A3142B2B8D2FFFA48D2697C81F352A3D4A94* L_239 = L_237;
		il2cpp_codegen_initobj((&V_0), sizeof(TableEntry_tDFCA5A567802D72C4E23BFC6CEA14C32F52229FF ));
		(&V_0)->set_Code_0(((int32_t)120));
		(&V_0)->set_Bits_1((uint8_t)7);
		TableEntry_tDFCA5A567802D72C4E23BFC6CEA14C32F52229FF  L_240 = V_0;
		NullCheck(L_239);
		(L_239)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)119)), (TableEntry_tDFCA5A567802D72C4E23BFC6CEA14C32F52229FF )L_240);
		TableEntryU5BU5D_tB3F4A3142B2B8D2FFFA48D2697C81F352A3D4A94* L_241 = L_239;
		il2cpp_codegen_initobj((&V_0), sizeof(TableEntry_tDFCA5A567802D72C4E23BFC6CEA14C32F52229FF ));
		(&V_0)->set_Code_0(((int32_t)121));
		(&V_0)->set_Bits_1((uint8_t)7);
		TableEntry_tDFCA5A567802D72C4E23BFC6CEA14C32F52229FF  L_242 = V_0;
		NullCheck(L_241);
		(L_241)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)120)), (TableEntry_tDFCA5A567802D72C4E23BFC6CEA14C32F52229FF )L_242);
		TableEntryU5BU5D_tB3F4A3142B2B8D2FFFA48D2697C81F352A3D4A94* L_243 = L_241;
		il2cpp_codegen_initobj((&V_0), sizeof(TableEntry_tDFCA5A567802D72C4E23BFC6CEA14C32F52229FF ));
		(&V_0)->set_Code_0(((int32_t)122));
		(&V_0)->set_Bits_1((uint8_t)7);
		TableEntry_tDFCA5A567802D72C4E23BFC6CEA14C32F52229FF  L_244 = V_0;
		NullCheck(L_243);
		(L_243)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)121)), (TableEntry_tDFCA5A567802D72C4E23BFC6CEA14C32F52229FF )L_244);
		TableEntryU5BU5D_tB3F4A3142B2B8D2FFFA48D2697C81F352A3D4A94* L_245 = L_243;
		il2cpp_codegen_initobj((&V_0), sizeof(TableEntry_tDFCA5A567802D72C4E23BFC6CEA14C32F52229FF ));
		(&V_0)->set_Code_0(((int32_t)123));
		(&V_0)->set_Bits_1((uint8_t)7);
		TableEntry_tDFCA5A567802D72C4E23BFC6CEA14C32F52229FF  L_246 = V_0;
		NullCheck(L_245);
		(L_245)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)122)), (TableEntry_tDFCA5A567802D72C4E23BFC6CEA14C32F52229FF )L_246);
		TableEntryU5BU5D_tB3F4A3142B2B8D2FFFA48D2697C81F352A3D4A94* L_247 = L_245;
		il2cpp_codegen_initobj((&V_0), sizeof(TableEntry_tDFCA5A567802D72C4E23BFC6CEA14C32F52229FF ));
		(&V_0)->set_Code_0(((int32_t)32766));
		(&V_0)->set_Bits_1((uint8_t)((int32_t)15));
		TableEntry_tDFCA5A567802D72C4E23BFC6CEA14C32F52229FF  L_248 = V_0;
		NullCheck(L_247);
		(L_247)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)123)), (TableEntry_tDFCA5A567802D72C4E23BFC6CEA14C32F52229FF )L_248);
		TableEntryU5BU5D_tB3F4A3142B2B8D2FFFA48D2697C81F352A3D4A94* L_249 = L_247;
		il2cpp_codegen_initobj((&V_0), sizeof(TableEntry_tDFCA5A567802D72C4E23BFC6CEA14C32F52229FF ));
		(&V_0)->set_Code_0(((int32_t)2044));
		(&V_0)->set_Bits_1((uint8_t)((int32_t)11));
		TableEntry_tDFCA5A567802D72C4E23BFC6CEA14C32F52229FF  L_250 = V_0;
		NullCheck(L_249);
		(L_249)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)124)), (TableEntry_tDFCA5A567802D72C4E23BFC6CEA14C32F52229FF )L_250);
		TableEntryU5BU5D_tB3F4A3142B2B8D2FFFA48D2697C81F352A3D4A94* L_251 = L_249;
		il2cpp_codegen_initobj((&V_0), sizeof(TableEntry_tDFCA5A567802D72C4E23BFC6CEA14C32F52229FF ));
		(&V_0)->set_Code_0(((int32_t)16381));
		(&V_0)->set_Bits_1((uint8_t)((int32_t)14));
		TableEntry_tDFCA5A567802D72C4E23BFC6CEA14C32F52229FF  L_252 = V_0;
		NullCheck(L_251);
		(L_251)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)125)), (TableEntry_tDFCA5A567802D72C4E23BFC6CEA14C32F52229FF )L_252);
		TableEntryU5BU5D_tB3F4A3142B2B8D2FFFA48D2697C81F352A3D4A94* L_253 = L_251;
		il2cpp_codegen_initobj((&V_0), sizeof(TableEntry_tDFCA5A567802D72C4E23BFC6CEA14C32F52229FF ));
		(&V_0)->set_Code_0(((int32_t)8189));
		(&V_0)->set_Bits_1((uint8_t)((int32_t)13));
		TableEntry_tDFCA5A567802D72C4E23BFC6CEA14C32F52229FF  L_254 = V_0;
		NullCheck(L_253);
		(L_253)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)126)), (TableEntry_tDFCA5A567802D72C4E23BFC6CEA14C32F52229FF )L_254);
		TableEntryU5BU5D_tB3F4A3142B2B8D2FFFA48D2697C81F352A3D4A94* L_255 = L_253;
		il2cpp_codegen_initobj((&V_0), sizeof(TableEntry_tDFCA5A567802D72C4E23BFC6CEA14C32F52229FF ));
		(&V_0)->set_Code_0(((int32_t)268435452));
		(&V_0)->set_Bits_1((uint8_t)((int32_t)28));
		TableEntry_tDFCA5A567802D72C4E23BFC6CEA14C32F52229FF  L_256 = V_0;
		NullCheck(L_255);
		(L_255)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)127)), (TableEntry_tDFCA5A567802D72C4E23BFC6CEA14C32F52229FF )L_256);
		TableEntryU5BU5D_tB3F4A3142B2B8D2FFFA48D2697C81F352A3D4A94* L_257 = L_255;
		il2cpp_codegen_initobj((&V_0), sizeof(TableEntry_tDFCA5A567802D72C4E23BFC6CEA14C32F52229FF ));
		(&V_0)->set_Code_0(((int32_t)1048550));
		(&V_0)->set_Bits_1((uint8_t)((int32_t)20));
		TableEntry_tDFCA5A567802D72C4E23BFC6CEA14C32F52229FF  L_258 = V_0;
		NullCheck(L_257);
		(L_257)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)128)), (TableEntry_tDFCA5A567802D72C4E23BFC6CEA14C32F52229FF )L_258);
		TableEntryU5BU5D_tB3F4A3142B2B8D2FFFA48D2697C81F352A3D4A94* L_259 = L_257;
		il2cpp_codegen_initobj((&V_0), sizeof(TableEntry_tDFCA5A567802D72C4E23BFC6CEA14C32F52229FF ));
		(&V_0)->set_Code_0(((int32_t)4194258));
		(&V_0)->set_Bits_1((uint8_t)((int32_t)22));
		TableEntry_tDFCA5A567802D72C4E23BFC6CEA14C32F52229FF  L_260 = V_0;
		NullCheck(L_259);
		(L_259)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)129)), (TableEntry_tDFCA5A567802D72C4E23BFC6CEA14C32F52229FF )L_260);
		TableEntryU5BU5D_tB3F4A3142B2B8D2FFFA48D2697C81F352A3D4A94* L_261 = L_259;
		il2cpp_codegen_initobj((&V_0), sizeof(TableEntry_tDFCA5A567802D72C4E23BFC6CEA14C32F52229FF ));
		(&V_0)->set_Code_0(((int32_t)1048551));
		(&V_0)->set_Bits_1((uint8_t)((int32_t)20));
		TableEntry_tDFCA5A567802D72C4E23BFC6CEA14C32F52229FF  L_262 = V_0;
		NullCheck(L_261);
		(L_261)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)130)), (TableEntry_tDFCA5A567802D72C4E23BFC6CEA14C32F52229FF )L_262);
		TableEntryU5BU5D_tB3F4A3142B2B8D2FFFA48D2697C81F352A3D4A94* L_263 = L_261;
		il2cpp_codegen_initobj((&V_0), sizeof(TableEntry_tDFCA5A567802D72C4E23BFC6CEA14C32F52229FF ));
		(&V_0)->set_Code_0(((int32_t)1048552));
		(&V_0)->set_Bits_1((uint8_t)((int32_t)20));
		TableEntry_tDFCA5A567802D72C4E23BFC6CEA14C32F52229FF  L_264 = V_0;
		NullCheck(L_263);
		(L_263)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)131)), (TableEntry_tDFCA5A567802D72C4E23BFC6CEA14C32F52229FF )L_264);
		TableEntryU5BU5D_tB3F4A3142B2B8D2FFFA48D2697C81F352A3D4A94* L_265 = L_263;
		il2cpp_codegen_initobj((&V_0), sizeof(TableEntry_tDFCA5A567802D72C4E23BFC6CEA14C32F52229FF ));
		(&V_0)->set_Code_0(((int32_t)4194259));
		(&V_0)->set_Bits_1((uint8_t)((int32_t)22));
		TableEntry_tDFCA5A567802D72C4E23BFC6CEA14C32F52229FF  L_266 = V_0;
		NullCheck(L_265);
		(L_265)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)132)), (TableEntry_tDFCA5A567802D72C4E23BFC6CEA14C32F52229FF )L_266);
		TableEntryU5BU5D_tB3F4A3142B2B8D2FFFA48D2697C81F352A3D4A94* L_267 = L_265;
		il2cpp_codegen_initobj((&V_0), sizeof(TableEntry_tDFCA5A567802D72C4E23BFC6CEA14C32F52229FF ));
		(&V_0)->set_Code_0(((int32_t)4194260));
		(&V_0)->set_Bits_1((uint8_t)((int32_t)22));
		TableEntry_tDFCA5A567802D72C4E23BFC6CEA14C32F52229FF  L_268 = V_0;
		NullCheck(L_267);
		(L_267)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)133)), (TableEntry_tDFCA5A567802D72C4E23BFC6CEA14C32F52229FF )L_268);
		TableEntryU5BU5D_tB3F4A3142B2B8D2FFFA48D2697C81F352A3D4A94* L_269 = L_267;
		il2cpp_codegen_initobj((&V_0), sizeof(TableEntry_tDFCA5A567802D72C4E23BFC6CEA14C32F52229FF ));
		(&V_0)->set_Code_0(((int32_t)4194261));
		(&V_0)->set_Bits_1((uint8_t)((int32_t)22));
		TableEntry_tDFCA5A567802D72C4E23BFC6CEA14C32F52229FF  L_270 = V_0;
		NullCheck(L_269);
		(L_269)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)134)), (TableEntry_tDFCA5A567802D72C4E23BFC6CEA14C32F52229FF )L_270);
		TableEntryU5BU5D_tB3F4A3142B2B8D2FFFA48D2697C81F352A3D4A94* L_271 = L_269;
		il2cpp_codegen_initobj((&V_0), sizeof(TableEntry_tDFCA5A567802D72C4E23BFC6CEA14C32F52229FF ));
		(&V_0)->set_Code_0(((int32_t)8388569));
		(&V_0)->set_Bits_1((uint8_t)((int32_t)23));
		TableEntry_tDFCA5A567802D72C4E23BFC6CEA14C32F52229FF  L_272 = V_0;
		NullCheck(L_271);
		(L_271)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)135)), (TableEntry_tDFCA5A567802D72C4E23BFC6CEA14C32F52229FF )L_272);
		TableEntryU5BU5D_tB3F4A3142B2B8D2FFFA48D2697C81F352A3D4A94* L_273 = L_271;
		il2cpp_codegen_initobj((&V_0), sizeof(TableEntry_tDFCA5A567802D72C4E23BFC6CEA14C32F52229FF ));
		(&V_0)->set_Code_0(((int32_t)4194262));
		(&V_0)->set_Bits_1((uint8_t)((int32_t)22));
		TableEntry_tDFCA5A567802D72C4E23BFC6CEA14C32F52229FF  L_274 = V_0;
		NullCheck(L_273);
		(L_273)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)136)), (TableEntry_tDFCA5A567802D72C4E23BFC6CEA14C32F52229FF )L_274);
		TableEntryU5BU5D_tB3F4A3142B2B8D2FFFA48D2697C81F352A3D4A94* L_275 = L_273;
		il2cpp_codegen_initobj((&V_0), sizeof(TableEntry_tDFCA5A567802D72C4E23BFC6CEA14C32F52229FF ));
		(&V_0)->set_Code_0(((int32_t)8388570));
		(&V_0)->set_Bits_1((uint8_t)((int32_t)23));
		TableEntry_tDFCA5A567802D72C4E23BFC6CEA14C32F52229FF  L_276 = V_0;
		NullCheck(L_275);
		(L_275)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)137)), (TableEntry_tDFCA5A567802D72C4E23BFC6CEA14C32F52229FF )L_276);
		TableEntryU5BU5D_tB3F4A3142B2B8D2FFFA48D2697C81F352A3D4A94* L_277 = L_275;
		il2cpp_codegen_initobj((&V_0), sizeof(TableEntry_tDFCA5A567802D72C4E23BFC6CEA14C32F52229FF ));
		(&V_0)->set_Code_0(((int32_t)8388571));
		(&V_0)->set_Bits_1((uint8_t)((int32_t)23));
		TableEntry_tDFCA5A567802D72C4E23BFC6CEA14C32F52229FF  L_278 = V_0;
		NullCheck(L_277);
		(L_277)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)138)), (TableEntry_tDFCA5A567802D72C4E23BFC6CEA14C32F52229FF )L_278);
		TableEntryU5BU5D_tB3F4A3142B2B8D2FFFA48D2697C81F352A3D4A94* L_279 = L_277;
		il2cpp_codegen_initobj((&V_0), sizeof(TableEntry_tDFCA5A567802D72C4E23BFC6CEA14C32F52229FF ));
		(&V_0)->set_Code_0(((int32_t)8388572));
		(&V_0)->set_Bits_1((uint8_t)((int32_t)23));
		TableEntry_tDFCA5A567802D72C4E23BFC6CEA14C32F52229FF  L_280 = V_0;
		NullCheck(L_279);
		(L_279)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)139)), (TableEntry_tDFCA5A567802D72C4E23BFC6CEA14C32F52229FF )L_280);
		TableEntryU5BU5D_tB3F4A3142B2B8D2FFFA48D2697C81F352A3D4A94* L_281 = L_279;
		il2cpp_codegen_initobj((&V_0), sizeof(TableEntry_tDFCA5A567802D72C4E23BFC6CEA14C32F52229FF ));
		(&V_0)->set_Code_0(((int32_t)8388573));
		(&V_0)->set_Bits_1((uint8_t)((int32_t)23));
		TableEntry_tDFCA5A567802D72C4E23BFC6CEA14C32F52229FF  L_282 = V_0;
		NullCheck(L_281);
		(L_281)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)140)), (TableEntry_tDFCA5A567802D72C4E23BFC6CEA14C32F52229FF )L_282);
		TableEntryU5BU5D_tB3F4A3142B2B8D2FFFA48D2697C81F352A3D4A94* L_283 = L_281;
		il2cpp_codegen_initobj((&V_0), sizeof(TableEntry_tDFCA5A567802D72C4E23BFC6CEA14C32F52229FF ));
		(&V_0)->set_Code_0(((int32_t)8388574));
		(&V_0)->set_Bits_1((uint8_t)((int32_t)23));
		TableEntry_tDFCA5A567802D72C4E23BFC6CEA14C32F52229FF  L_284 = V_0;
		NullCheck(L_283);
		(L_283)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)141)), (TableEntry_tDFCA5A567802D72C4E23BFC6CEA14C32F52229FF )L_284);
		TableEntryU5BU5D_tB3F4A3142B2B8D2FFFA48D2697C81F352A3D4A94* L_285 = L_283;
		il2cpp_codegen_initobj((&V_0), sizeof(TableEntry_tDFCA5A567802D72C4E23BFC6CEA14C32F52229FF ));
		(&V_0)->set_Code_0(((int32_t)16777195));
		(&V_0)->set_Bits_1((uint8_t)((int32_t)24));
		TableEntry_tDFCA5A567802D72C4E23BFC6CEA14C32F52229FF  L_286 = V_0;
		NullCheck(L_285);
		(L_285)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)142)), (TableEntry_tDFCA5A567802D72C4E23BFC6CEA14C32F52229FF )L_286);
		TableEntryU5BU5D_tB3F4A3142B2B8D2FFFA48D2697C81F352A3D4A94* L_287 = L_285;
		il2cpp_codegen_initobj((&V_0), sizeof(TableEntry_tDFCA5A567802D72C4E23BFC6CEA14C32F52229FF ));
		(&V_0)->set_Code_0(((int32_t)8388575));
		(&V_0)->set_Bits_1((uint8_t)((int32_t)23));
		TableEntry_tDFCA5A567802D72C4E23BFC6CEA14C32F52229FF  L_288 = V_0;
		NullCheck(L_287);
		(L_287)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)143)), (TableEntry_tDFCA5A567802D72C4E23BFC6CEA14C32F52229FF )L_288);
		TableEntryU5BU5D_tB3F4A3142B2B8D2FFFA48D2697C81F352A3D4A94* L_289 = L_287;
		il2cpp_codegen_initobj((&V_0), sizeof(TableEntry_tDFCA5A567802D72C4E23BFC6CEA14C32F52229FF ));
		(&V_0)->set_Code_0(((int32_t)16777196));
		(&V_0)->set_Bits_1((uint8_t)((int32_t)24));
		TableEntry_tDFCA5A567802D72C4E23BFC6CEA14C32F52229FF  L_290 = V_0;
		NullCheck(L_289);
		(L_289)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)144)), (TableEntry_tDFCA5A567802D72C4E23BFC6CEA14C32F52229FF )L_290);
		TableEntryU5BU5D_tB3F4A3142B2B8D2FFFA48D2697C81F352A3D4A94* L_291 = L_289;
		il2cpp_codegen_initobj((&V_0), sizeof(TableEntry_tDFCA5A567802D72C4E23BFC6CEA14C32F52229FF ));
		(&V_0)->set_Code_0(((int32_t)16777197));
		(&V_0)->set_Bits_1((uint8_t)((int32_t)24));
		TableEntry_tDFCA5A567802D72C4E23BFC6CEA14C32F52229FF  L_292 = V_0;
		NullCheck(L_291);
		(L_291)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)145)), (TableEntry_tDFCA5A567802D72C4E23BFC6CEA14C32F52229FF )L_292);
		TableEntryU5BU5D_tB3F4A3142B2B8D2FFFA48D2697C81F352A3D4A94* L_293 = L_291;
		il2cpp_codegen_initobj((&V_0), sizeof(TableEntry_tDFCA5A567802D72C4E23BFC6CEA14C32F52229FF ));
		(&V_0)->set_Code_0(((int32_t)4194263));
		(&V_0)->set_Bits_1((uint8_t)((int32_t)22));
		TableEntry_tDFCA5A567802D72C4E23BFC6CEA14C32F52229FF  L_294 = V_0;
		NullCheck(L_293);
		(L_293)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)146)), (TableEntry_tDFCA5A567802D72C4E23BFC6CEA14C32F52229FF )L_294);
		TableEntryU5BU5D_tB3F4A3142B2B8D2FFFA48D2697C81F352A3D4A94* L_295 = L_293;
		il2cpp_codegen_initobj((&V_0), sizeof(TableEntry_tDFCA5A567802D72C4E23BFC6CEA14C32F52229FF ));
		(&V_0)->set_Code_0(((int32_t)8388576));
		(&V_0)->set_Bits_1((uint8_t)((int32_t)23));
		TableEntry_tDFCA5A567802D72C4E23BFC6CEA14C32F52229FF  L_296 = V_0;
		NullCheck(L_295);
		(L_295)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)147)), (TableEntry_tDFCA5A567802D72C4E23BFC6CEA14C32F52229FF )L_296);
		TableEntryU5BU5D_tB3F4A3142B2B8D2FFFA48D2697C81F352A3D4A94* L_297 = L_295;
		il2cpp_codegen_initobj((&V_0), sizeof(TableEntry_tDFCA5A567802D72C4E23BFC6CEA14C32F52229FF ));
		(&V_0)->set_Code_0(((int32_t)16777198));
		(&V_0)->set_Bits_1((uint8_t)((int32_t)24));
		TableEntry_tDFCA5A567802D72C4E23BFC6CEA14C32F52229FF  L_298 = V_0;
		NullCheck(L_297);
		(L_297)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)148)), (TableEntry_tDFCA5A567802D72C4E23BFC6CEA14C32F52229FF )L_298);
		TableEntryU5BU5D_tB3F4A3142B2B8D2FFFA48D2697C81F352A3D4A94* L_299 = L_297;
		il2cpp_codegen_initobj((&V_0), sizeof(TableEntry_tDFCA5A567802D72C4E23BFC6CEA14C32F52229FF ));
		(&V_0)->set_Code_0(((int32_t)8388577));
		(&V_0)->set_Bits_1((uint8_t)((int32_t)23));
		TableEntry_tDFCA5A567802D72C4E23BFC6CEA14C32F52229FF  L_300 = V_0;
		NullCheck(L_299);
		(L_299)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)149)), (TableEntry_tDFCA5A567802D72C4E23BFC6CEA14C32F52229FF )L_300);
		TableEntryU5BU5D_tB3F4A3142B2B8D2FFFA48D2697C81F352A3D4A94* L_301 = L_299;
		il2cpp_codegen_initobj((&V_0), sizeof(TableEntry_tDFCA5A567802D72C4E23BFC6CEA14C32F52229FF ));
		(&V_0)->set_Code_0(((int32_t)8388578));
		(&V_0)->set_Bits_1((uint8_t)((int32_t)23));
		TableEntry_tDFCA5A567802D72C4E23BFC6CEA14C32F52229FF  L_302 = V_0;
		NullCheck(L_301);
		(L_301)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)150)), (TableEntry_tDFCA5A567802D72C4E23BFC6CEA14C32F52229FF )L_302);
		TableEntryU5BU5D_tB3F4A3142B2B8D2FFFA48D2697C81F352A3D4A94* L_303 = L_301;
		il2cpp_codegen_initobj((&V_0), sizeof(TableEntry_tDFCA5A567802D72C4E23BFC6CEA14C32F52229FF ));
		(&V_0)->set_Code_0(((int32_t)8388579));
		(&V_0)->set_Bits_1((uint8_t)((int32_t)23));
		TableEntry_tDFCA5A567802D72C4E23BFC6CEA14C32F52229FF  L_304 = V_0;
		NullCheck(L_303);
		(L_303)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)151)), (TableEntry_tDFCA5A567802D72C4E23BFC6CEA14C32F52229FF )L_304);
		TableEntryU5BU5D_tB3F4A3142B2B8D2FFFA48D2697C81F352A3D4A94* L_305 = L_303;
		il2cpp_codegen_initobj((&V_0), sizeof(TableEntry_tDFCA5A567802D72C4E23BFC6CEA14C32F52229FF ));
		(&V_0)->set_Code_0(((int32_t)8388580));
		(&V_0)->set_Bits_1((uint8_t)((int32_t)23));
		TableEntry_tDFCA5A567802D72C4E23BFC6CEA14C32F52229FF  L_306 = V_0;
		NullCheck(L_305);
		(L_305)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)152)), (TableEntry_tDFCA5A567802D72C4E23BFC6CEA14C32F52229FF )L_306);
		TableEntryU5BU5D_tB3F4A3142B2B8D2FFFA48D2697C81F352A3D4A94* L_307 = L_305;
		il2cpp_codegen_initobj((&V_0), sizeof(TableEntry_tDFCA5A567802D72C4E23BFC6CEA14C32F52229FF ));
		(&V_0)->set_Code_0(((int32_t)2097116));
		(&V_0)->set_Bits_1((uint8_t)((int32_t)21));
		TableEntry_tDFCA5A567802D72C4E23BFC6CEA14C32F52229FF  L_308 = V_0;
		NullCheck(L_307);
		(L_307)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)153)), (TableEntry_tDFCA5A567802D72C4E23BFC6CEA14C32F52229FF )L_308);
		TableEntryU5BU5D_tB3F4A3142B2B8D2FFFA48D2697C81F352A3D4A94* L_309 = L_307;
		il2cpp_codegen_initobj((&V_0), sizeof(TableEntry_tDFCA5A567802D72C4E23BFC6CEA14C32F52229FF ));
		(&V_0)->set_Code_0(((int32_t)4194264));
		(&V_0)->set_Bits_1((uint8_t)((int32_t)22));
		TableEntry_tDFCA5A567802D72C4E23BFC6CEA14C32F52229FF  L_310 = V_0;
		NullCheck(L_309);
		(L_309)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)154)), (TableEntry_tDFCA5A567802D72C4E23BFC6CEA14C32F52229FF )L_310);
		TableEntryU5BU5D_tB3F4A3142B2B8D2FFFA48D2697C81F352A3D4A94* L_311 = L_309;
		il2cpp_codegen_initobj((&V_0), sizeof(TableEntry_tDFCA5A567802D72C4E23BFC6CEA14C32F52229FF ));
		(&V_0)->set_Code_0(((int32_t)8388581));
		(&V_0)->set_Bits_1((uint8_t)((int32_t)23));
		TableEntry_tDFCA5A567802D72C4E23BFC6CEA14C32F52229FF  L_312 = V_0;
		NullCheck(L_311);
		(L_311)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)155)), (TableEntry_tDFCA5A567802D72C4E23BFC6CEA14C32F52229FF )L_312);
		TableEntryU5BU5D_tB3F4A3142B2B8D2FFFA48D2697C81F352A3D4A94* L_313 = L_311;
		il2cpp_codegen_initobj((&V_0), sizeof(TableEntry_tDFCA5A567802D72C4E23BFC6CEA14C32F52229FF ));
		(&V_0)->set_Code_0(((int32_t)4194265));
		(&V_0)->set_Bits_1((uint8_t)((int32_t)22));
		TableEntry_tDFCA5A567802D72C4E23BFC6CEA14C32F52229FF  L_314 = V_0;
		NullCheck(L_313);
		(L_313)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)156)), (TableEntry_tDFCA5A567802D72C4E23BFC6CEA14C32F52229FF )L_314);
		TableEntryU5BU5D_tB3F4A3142B2B8D2FFFA48D2697C81F352A3D4A94* L_315 = L_313;
		il2cpp_codegen_initobj((&V_0), sizeof(TableEntry_tDFCA5A567802D72C4E23BFC6CEA14C32F52229FF ));
		(&V_0)->set_Code_0(((int32_t)8388582));
		(&V_0)->set_Bits_1((uint8_t)((int32_t)23));
		TableEntry_tDFCA5A567802D72C4E23BFC6CEA14C32F52229FF  L_316 = V_0;
		NullCheck(L_315);
		(L_315)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)157)), (TableEntry_tDFCA5A567802D72C4E23BFC6CEA14C32F52229FF )L_316);
		TableEntryU5BU5D_tB3F4A3142B2B8D2FFFA48D2697C81F352A3D4A94* L_317 = L_315;
		il2cpp_codegen_initobj((&V_0), sizeof(TableEntry_tDFCA5A567802D72C4E23BFC6CEA14C32F52229FF ));
		(&V_0)->set_Code_0(((int32_t)8388583));
		(&V_0)->set_Bits_1((uint8_t)((int32_t)23));
		TableEntry_tDFCA5A567802D72C4E23BFC6CEA14C32F52229FF  L_318 = V_0;
		NullCheck(L_317);
		(L_317)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)158)), (TableEntry_tDFCA5A567802D72C4E23BFC6CEA14C32F52229FF )L_318);
		TableEntryU5BU5D_tB3F4A3142B2B8D2FFFA48D2697C81F352A3D4A94* L_319 = L_317;
		il2cpp_codegen_initobj((&V_0), sizeof(TableEntry_tDFCA5A567802D72C4E23BFC6CEA14C32F52229FF ));
		(&V_0)->set_Code_0(((int32_t)16777199));
		(&V_0)->set_Bits_1((uint8_t)((int32_t)24));
		TableEntry_tDFCA5A567802D72C4E23BFC6CEA14C32F52229FF  L_320 = V_0;
		NullCheck(L_319);
		(L_319)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)159)), (TableEntry_tDFCA5A567802D72C4E23BFC6CEA14C32F52229FF )L_320);
		TableEntryU5BU5D_tB3F4A3142B2B8D2FFFA48D2697C81F352A3D4A94* L_321 = L_319;
		il2cpp_codegen_initobj((&V_0), sizeof(TableEntry_tDFCA5A567802D72C4E23BFC6CEA14C32F52229FF ));
		(&V_0)->set_Code_0(((int32_t)4194266));
		(&V_0)->set_Bits_1((uint8_t)((int32_t)22));
		TableEntry_tDFCA5A567802D72C4E23BFC6CEA14C32F52229FF  L_322 = V_0;
		NullCheck(L_321);
		(L_321)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)160)), (TableEntry_tDFCA5A567802D72C4E23BFC6CEA14C32F52229FF )L_322);
		TableEntryU5BU5D_tB3F4A3142B2B8D2FFFA48D2697C81F352A3D4A94* L_323 = L_321;
		il2cpp_codegen_initobj((&V_0), sizeof(TableEntry_tDFCA5A567802D72C4E23BFC6CEA14C32F52229FF ));
		(&V_0)->set_Code_0(((int32_t)2097117));
		(&V_0)->set_Bits_1((uint8_t)((int32_t)21));
		TableEntry_tDFCA5A567802D72C4E23BFC6CEA14C32F52229FF  L_324 = V_0;
		NullCheck(L_323);
		(L_323)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)161)), (TableEntry_tDFCA5A567802D72C4E23BFC6CEA14C32F52229FF )L_324);
		TableEntryU5BU5D_tB3F4A3142B2B8D2FFFA48D2697C81F352A3D4A94* L_325 = L_323;
		il2cpp_codegen_initobj((&V_0), sizeof(TableEntry_tDFCA5A567802D72C4E23BFC6CEA14C32F52229FF ));
		(&V_0)->set_Code_0(((int32_t)1048553));
		(&V_0)->set_Bits_1((uint8_t)((int32_t)20));
		TableEntry_tDFCA5A567802D72C4E23BFC6CEA14C32F52229FF  L_326 = V_0;
		NullCheck(L_325);
		(L_325)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)162)), (TableEntry_tDFCA5A567802D72C4E23BFC6CEA14C32F52229FF )L_326);
		TableEntryU5BU5D_tB3F4A3142B2B8D2FFFA48D2697C81F352A3D4A94* L_327 = L_325;
		il2cpp_codegen_initobj((&V_0), sizeof(TableEntry_tDFCA5A567802D72C4E23BFC6CEA14C32F52229FF ));
		(&V_0)->set_Code_0(((int32_t)4194267));
		(&V_0)->set_Bits_1((uint8_t)((int32_t)22));
		TableEntry_tDFCA5A567802D72C4E23BFC6CEA14C32F52229FF  L_328 = V_0;
		NullCheck(L_327);
		(L_327)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)163)), (TableEntry_tDFCA5A567802D72C4E23BFC6CEA14C32F52229FF )L_328);
		TableEntryU5BU5D_tB3F4A3142B2B8D2FFFA48D2697C81F352A3D4A94* L_329 = L_327;
		il2cpp_codegen_initobj((&V_0), sizeof(TableEntry_tDFCA5A567802D72C4E23BFC6CEA14C32F52229FF ));
		(&V_0)->set_Code_0(((int32_t)4194268));
		(&V_0)->set_Bits_1((uint8_t)((int32_t)22));
		TableEntry_tDFCA5A567802D72C4E23BFC6CEA14C32F52229FF  L_330 = V_0;
		NullCheck(L_329);
		(L_329)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)164)), (TableEntry_tDFCA5A567802D72C4E23BFC6CEA14C32F52229FF )L_330);
		TableEntryU5BU5D_tB3F4A3142B2B8D2FFFA48D2697C81F352A3D4A94* L_331 = L_329;
		il2cpp_codegen_initobj((&V_0), sizeof(TableEntry_tDFCA5A567802D72C4E23BFC6CEA14C32F52229FF ));
		(&V_0)->set_Code_0(((int32_t)8388584));
		(&V_0)->set_Bits_1((uint8_t)((int32_t)23));
		TableEntry_tDFCA5A567802D72C4E23BFC6CEA14C32F52229FF  L_332 = V_0;
		NullCheck(L_331);
		(L_331)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)165)), (TableEntry_tDFCA5A567802D72C4E23BFC6CEA14C32F52229FF )L_332);
		TableEntryU5BU5D_tB3F4A3142B2B8D2FFFA48D2697C81F352A3D4A94* L_333 = L_331;
		il2cpp_codegen_initobj((&V_0), sizeof(TableEntry_tDFCA5A567802D72C4E23BFC6CEA14C32F52229FF ));
		(&V_0)->set_Code_0(((int32_t)8388585));
		(&V_0)->set_Bits_1((uint8_t)((int32_t)23));
		TableEntry_tDFCA5A567802D72C4E23BFC6CEA14C32F52229FF  L_334 = V_0;
		NullCheck(L_333);
		(L_333)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)166)), (TableEntry_tDFCA5A567802D72C4E23BFC6CEA14C32F52229FF )L_334);
		TableEntryU5BU5D_tB3F4A3142B2B8D2FFFA48D2697C81F352A3D4A94* L_335 = L_333;
		il2cpp_codegen_initobj((&V_0), sizeof(TableEntry_tDFCA5A567802D72C4E23BFC6CEA14C32F52229FF ));
		(&V_0)->set_Code_0(((int32_t)2097118));
		(&V_0)->set_Bits_1((uint8_t)((int32_t)21));
		TableEntry_tDFCA5A567802D72C4E23BFC6CEA14C32F52229FF  L_336 = V_0;
		NullCheck(L_335);
		(L_335)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)167)), (TableEntry_tDFCA5A567802D72C4E23BFC6CEA14C32F52229FF )L_336);
		TableEntryU5BU5D_tB3F4A3142B2B8D2FFFA48D2697C81F352A3D4A94* L_337 = L_335;
		il2cpp_codegen_initobj((&V_0), sizeof(TableEntry_tDFCA5A567802D72C4E23BFC6CEA14C32F52229FF ));
		(&V_0)->set_Code_0(((int32_t)8388586));
		(&V_0)->set_Bits_1((uint8_t)((int32_t)23));
		TableEntry_tDFCA5A567802D72C4E23BFC6CEA14C32F52229FF  L_338 = V_0;
		NullCheck(L_337);
		(L_337)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)168)), (TableEntry_tDFCA5A567802D72C4E23BFC6CEA14C32F52229FF )L_338);
		TableEntryU5BU5D_tB3F4A3142B2B8D2FFFA48D2697C81F352A3D4A94* L_339 = L_337;
		il2cpp_codegen_initobj((&V_0), sizeof(TableEntry_tDFCA5A567802D72C4E23BFC6CEA14C32F52229FF ));
		(&V_0)->set_Code_0(((int32_t)4194269));
		(&V_0)->set_Bits_1((uint8_t)((int32_t)22));
		TableEntry_tDFCA5A567802D72C4E23BFC6CEA14C32F52229FF  L_340 = V_0;
		NullCheck(L_339);
		(L_339)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)169)), (TableEntry_tDFCA5A567802D72C4E23BFC6CEA14C32F52229FF )L_340);
		TableEntryU5BU5D_tB3F4A3142B2B8D2FFFA48D2697C81F352A3D4A94* L_341 = L_339;
		il2cpp_codegen_initobj((&V_0), sizeof(TableEntry_tDFCA5A567802D72C4E23BFC6CEA14C32F52229FF ));
		(&V_0)->set_Code_0(((int32_t)4194270));
		(&V_0)->set_Bits_1((uint8_t)((int32_t)22));
		TableEntry_tDFCA5A567802D72C4E23BFC6CEA14C32F52229FF  L_342 = V_0;
		NullCheck(L_341);
		(L_341)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)170)), (TableEntry_tDFCA5A567802D72C4E23BFC6CEA14C32F52229FF )L_342);
		TableEntryU5BU5D_tB3F4A3142B2B8D2FFFA48D2697C81F352A3D4A94* L_343 = L_341;
		il2cpp_codegen_initobj((&V_0), sizeof(TableEntry_tDFCA5A567802D72C4E23BFC6CEA14C32F52229FF ));
		(&V_0)->set_Code_0(((int32_t)16777200));
		(&V_0)->set_Bits_1((uint8_t)((int32_t)24));
		TableEntry_tDFCA5A567802D72C4E23BFC6CEA14C32F52229FF  L_344 = V_0;
		NullCheck(L_343);
		(L_343)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)171)), (TableEntry_tDFCA5A567802D72C4E23BFC6CEA14C32F52229FF )L_344);
		TableEntryU5BU5D_tB3F4A3142B2B8D2FFFA48D2697C81F352A3D4A94* L_345 = L_343;
		il2cpp_codegen_initobj((&V_0), sizeof(TableEntry_tDFCA5A567802D72C4E23BFC6CEA14C32F52229FF ));
		(&V_0)->set_Code_0(((int32_t)2097119));
		(&V_0)->set_Bits_1((uint8_t)((int32_t)21));
		TableEntry_tDFCA5A567802D72C4E23BFC6CEA14C32F52229FF  L_346 = V_0;
		NullCheck(L_345);
		(L_345)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)172)), (TableEntry_tDFCA5A567802D72C4E23BFC6CEA14C32F52229FF )L_346);
		TableEntryU5BU5D_tB3F4A3142B2B8D2FFFA48D2697C81F352A3D4A94* L_347 = L_345;
		il2cpp_codegen_initobj((&V_0), sizeof(TableEntry_tDFCA5A567802D72C4E23BFC6CEA14C32F52229FF ));
		(&V_0)->set_Code_0(((int32_t)4194271));
		(&V_0)->set_Bits_1((uint8_t)((int32_t)22));
		TableEntry_tDFCA5A567802D72C4E23BFC6CEA14C32F52229FF  L_348 = V_0;
		NullCheck(L_347);
		(L_347)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)173)), (TableEntry_tDFCA5A567802D72C4E23BFC6CEA14C32F52229FF )L_348);
		TableEntryU5BU5D_tB3F4A3142B2B8D2FFFA48D2697C81F352A3D4A94* L_349 = L_347;
		il2cpp_codegen_initobj((&V_0), sizeof(TableEntry_tDFCA5A567802D72C4E23BFC6CEA14C32F52229FF ));
		(&V_0)->set_Code_0(((int32_t)8388587));
		(&V_0)->set_Bits_1((uint8_t)((int32_t)23));
		TableEntry_tDFCA5A567802D72C4E23BFC6CEA14C32F52229FF  L_350 = V_0;
		NullCheck(L_349);
		(L_349)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)174)), (TableEntry_tDFCA5A567802D72C4E23BFC6CEA14C32F52229FF )L_350);
		TableEntryU5BU5D_tB3F4A3142B2B8D2FFFA48D2697C81F352A3D4A94* L_351 = L_349;
		il2cpp_codegen_initobj((&V_0), sizeof(TableEntry_tDFCA5A567802D72C4E23BFC6CEA14C32F52229FF ));
		(&V_0)->set_Code_0(((int32_t)8388588));
		(&V_0)->set_Bits_1((uint8_t)((int32_t)23));
		TableEntry_tDFCA5A567802D72C4E23BFC6CEA14C32F52229FF  L_352 = V_0;
		NullCheck(L_351);
		(L_351)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)175)), (TableEntry_tDFCA5A567802D72C4E23BFC6CEA14C32F52229FF )L_352);
		TableEntryU5BU5D_tB3F4A3142B2B8D2FFFA48D2697C81F352A3D4A94* L_353 = L_351;
		il2cpp_codegen_initobj((&V_0), sizeof(TableEntry_tDFCA5A567802D72C4E23BFC6CEA14C32F52229FF ));
		(&V_0)->set_Code_0(((int32_t)2097120));
		(&V_0)->set_Bits_1((uint8_t)((int32_t)21));
		TableEntry_tDFCA5A567802D72C4E23BFC6CEA14C32F52229FF  L_354 = V_0;
		NullCheck(L_353);
		(L_353)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)176)), (TableEntry_tDFCA5A567802D72C4E23BFC6CEA14C32F52229FF )L_354);
		TableEntryU5BU5D_tB3F4A3142B2B8D2FFFA48D2697C81F352A3D4A94* L_355 = L_353;
		il2cpp_codegen_initobj((&V_0), sizeof(TableEntry_tDFCA5A567802D72C4E23BFC6CEA14C32F52229FF ));
		(&V_0)->set_Code_0(((int32_t)2097121));
		(&V_0)->set_Bits_1((uint8_t)((int32_t)21));
		TableEntry_tDFCA5A567802D72C4E23BFC6CEA14C32F52229FF  L_356 = V_0;
		NullCheck(L_355);
		(L_355)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)177)), (TableEntry_tDFCA5A567802D72C4E23BFC6CEA14C32F52229FF )L_356);
		TableEntryU5BU5D_tB3F4A3142B2B8D2FFFA48D2697C81F352A3D4A94* L_357 = L_355;
		il2cpp_codegen_initobj((&V_0), sizeof(TableEntry_tDFCA5A567802D72C4E23BFC6CEA14C32F52229FF ));
		(&V_0)->set_Code_0(((int32_t)4194272));
		(&V_0)->set_Bits_1((uint8_t)((int32_t)22));
		TableEntry_tDFCA5A567802D72C4E23BFC6CEA14C32F52229FF  L_358 = V_0;
		NullCheck(L_357);
		(L_357)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)178)), (TableEntry_tDFCA5A567802D72C4E23BFC6CEA14C32F52229FF )L_358);
		TableEntryU5BU5D_tB3F4A3142B2B8D2FFFA48D2697C81F352A3D4A94* L_359 = L_357;
		il2cpp_codegen_initobj((&V_0), sizeof(TableEntry_tDFCA5A567802D72C4E23BFC6CEA14C32F52229FF ));
		(&V_0)->set_Code_0(((int32_t)2097122));
		(&V_0)->set_Bits_1((uint8_t)((int32_t)21));
		TableEntry_tDFCA5A567802D72C4E23BFC6CEA14C32F52229FF  L_360 = V_0;
		NullCheck(L_359);
		(L_359)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)179)), (TableEntry_tDFCA5A567802D72C4E23BFC6CEA14C32F52229FF )L_360);
		TableEntryU5BU5D_tB3F4A3142B2B8D2FFFA48D2697C81F352A3D4A94* L_361 = L_359;
		il2cpp_codegen_initobj((&V_0), sizeof(TableEntry_tDFCA5A567802D72C4E23BFC6CEA14C32F52229FF ));
		(&V_0)->set_Code_0(((int32_t)8388589));
		(&V_0)->set_Bits_1((uint8_t)((int32_t)23));
		TableEntry_tDFCA5A567802D72C4E23BFC6CEA14C32F52229FF  L_362 = V_0;
		NullCheck(L_361);
		(L_361)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)180)), (TableEntry_tDFCA5A567802D72C4E23BFC6CEA14C32F52229FF )L_362);
		TableEntryU5BU5D_tB3F4A3142B2B8D2FFFA48D2697C81F352A3D4A94* L_363 = L_361;
		il2cpp_codegen_initobj((&V_0), sizeof(TableEntry_tDFCA5A567802D72C4E23BFC6CEA14C32F52229FF ));
		(&V_0)->set_Code_0(((int32_t)4194273));
		(&V_0)->set_Bits_1((uint8_t)((int32_t)22));
		TableEntry_tDFCA5A567802D72C4E23BFC6CEA14C32F52229FF  L_364 = V_0;
		NullCheck(L_363);
		(L_363)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)181)), (TableEntry_tDFCA5A567802D72C4E23BFC6CEA14C32F52229FF )L_364);
		TableEntryU5BU5D_tB3F4A3142B2B8D2FFFA48D2697C81F352A3D4A94* L_365 = L_363;
		il2cpp_codegen_initobj((&V_0), sizeof(TableEntry_tDFCA5A567802D72C4E23BFC6CEA14C32F52229FF ));
		(&V_0)->set_Code_0(((int32_t)8388590));
		(&V_0)->set_Bits_1((uint8_t)((int32_t)23));
		TableEntry_tDFCA5A567802D72C4E23BFC6CEA14C32F52229FF  L_366 = V_0;
		NullCheck(L_365);
		(L_365)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)182)), (TableEntry_tDFCA5A567802D72C4E23BFC6CEA14C32F52229FF )L_366);
		TableEntryU5BU5D_tB3F4A3142B2B8D2FFFA48D2697C81F352A3D4A94* L_367 = L_365;
		il2cpp_codegen_initobj((&V_0), sizeof(TableEntry_tDFCA5A567802D72C4E23BFC6CEA14C32F52229FF ));
		(&V_0)->set_Code_0(((int32_t)8388591));
		(&V_0)->set_Bits_1((uint8_t)((int32_t)23));
		TableEntry_tDFCA5A567802D72C4E23BFC6CEA14C32F52229FF  L_368 = V_0;
		NullCheck(L_367);
		(L_367)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)183)), (TableEntry_tDFCA5A567802D72C4E23BFC6CEA14C32F52229FF )L_368);
		TableEntryU5BU5D_tB3F4A3142B2B8D2FFFA48D2697C81F352A3D4A94* L_369 = L_367;
		il2cpp_codegen_initobj((&V_0), sizeof(TableEntry_tDFCA5A567802D72C4E23BFC6CEA14C32F52229FF ));
		(&V_0)->set_Code_0(((int32_t)1048554));
		(&V_0)->set_Bits_1((uint8_t)((int32_t)20));
		TableEntry_tDFCA5A567802D72C4E23BFC6CEA14C32F52229FF  L_370 = V_0;
		NullCheck(L_369);
		(L_369)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)184)), (TableEntry_tDFCA5A567802D72C4E23BFC6CEA14C32F52229FF )L_370);
		TableEntryU5BU5D_tB3F4A3142B2B8D2FFFA48D2697C81F352A3D4A94* L_371 = L_369;
		il2cpp_codegen_initobj((&V_0), sizeof(TableEntry_tDFCA5A567802D72C4E23BFC6CEA14C32F52229FF ));
		(&V_0)->set_Code_0(((int32_t)4194274));
		(&V_0)->set_Bits_1((uint8_t)((int32_t)22));
		TableEntry_tDFCA5A567802D72C4E23BFC6CEA14C32F52229FF  L_372 = V_0;
		NullCheck(L_371);
		(L_371)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)185)), (TableEntry_tDFCA5A567802D72C4E23BFC6CEA14C32F52229FF )L_372);
		TableEntryU5BU5D_tB3F4A3142B2B8D2FFFA48D2697C81F352A3D4A94* L_373 = L_371;
		il2cpp_codegen_initobj((&V_0), sizeof(TableEntry_tDFCA5A567802D72C4E23BFC6CEA14C32F52229FF ));
		(&V_0)->set_Code_0(((int32_t)4194275));
		(&V_0)->set_Bits_1((uint8_t)((int32_t)22));
		TableEntry_tDFCA5A567802D72C4E23BFC6CEA14C32F52229FF  L_374 = V_0;
		NullCheck(L_373);
		(L_373)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)186)), (TableEntry_tDFCA5A567802D72C4E23BFC6CEA14C32F52229FF )L_374);
		TableEntryU5BU5D_tB3F4A3142B2B8D2FFFA48D2697C81F352A3D4A94* L_375 = L_373;
		il2cpp_codegen_initobj((&V_0), sizeof(TableEntry_tDFCA5A567802D72C4E23BFC6CEA14C32F52229FF ));
		(&V_0)->set_Code_0(((int32_t)4194276));
		(&V_0)->set_Bits_1((uint8_t)((int32_t)22));
		TableEntry_tDFCA5A567802D72C4E23BFC6CEA14C32F52229FF  L_376 = V_0;
		NullCheck(L_375);
		(L_375)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)187)), (TableEntry_tDFCA5A567802D72C4E23BFC6CEA14C32F52229FF )L_376);
		TableEntryU5BU5D_tB3F4A3142B2B8D2FFFA48D2697C81F352A3D4A94* L_377 = L_375;
		il2cpp_codegen_initobj((&V_0), sizeof(TableEntry_tDFCA5A567802D72C4E23BFC6CEA14C32F52229FF ));
		(&V_0)->set_Code_0(((int32_t)8388592));
		(&V_0)->set_Bits_1((uint8_t)((int32_t)23));
		TableEntry_tDFCA5A567802D72C4E23BFC6CEA14C32F52229FF  L_378 = V_0;
		NullCheck(L_377);
		(L_377)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)188)), (TableEntry_tDFCA5A567802D72C4E23BFC6CEA14C32F52229FF )L_378);
		TableEntryU5BU5D_tB3F4A3142B2B8D2FFFA48D2697C81F352A3D4A94* L_379 = L_377;
		il2cpp_codegen_initobj((&V_0), sizeof(TableEntry_tDFCA5A567802D72C4E23BFC6CEA14C32F52229FF ));
		(&V_0)->set_Code_0(((int32_t)4194277));
		(&V_0)->set_Bits_1((uint8_t)((int32_t)22));
		TableEntry_tDFCA5A567802D72C4E23BFC6CEA14C32F52229FF  L_380 = V_0;
		NullCheck(L_379);
		(L_379)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)189)), (TableEntry_tDFCA5A567802D72C4E23BFC6CEA14C32F52229FF )L_380);
		TableEntryU5BU5D_tB3F4A3142B2B8D2FFFA48D2697C81F352A3D4A94* L_381 = L_379;
		il2cpp_codegen_initobj((&V_0), sizeof(TableEntry_tDFCA5A567802D72C4E23BFC6CEA14C32F52229FF ));
		(&V_0)->set_Code_0(((int32_t)4194278));
		(&V_0)->set_Bits_1((uint8_t)((int32_t)22));
		TableEntry_tDFCA5A567802D72C4E23BFC6CEA14C32F52229FF  L_382 = V_0;
		NullCheck(L_381);
		(L_381)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)190)), (TableEntry_tDFCA5A567802D72C4E23BFC6CEA14C32F52229FF )L_382);
		TableEntryU5BU5D_tB3F4A3142B2B8D2FFFA48D2697C81F352A3D4A94* L_383 = L_381;
		il2cpp_codegen_initobj((&V_0), sizeof(TableEntry_tDFCA5A567802D72C4E23BFC6CEA14C32F52229FF ));
		(&V_0)->set_Code_0(((int32_t)8388593));
		(&V_0)->set_Bits_1((uint8_t)((int32_t)23));
		TableEntry_tDFCA5A567802D72C4E23BFC6CEA14C32F52229FF  L_384 = V_0;
		NullCheck(L_383);
		(L_383)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)191)), (TableEntry_tDFCA5A567802D72C4E23BFC6CEA14C32F52229FF )L_384);
		TableEntryU5BU5D_tB3F4A3142B2B8D2FFFA48D2697C81F352A3D4A94* L_385 = L_383;
		il2cpp_codegen_initobj((&V_0), sizeof(TableEntry_tDFCA5A567802D72C4E23BFC6CEA14C32F52229FF ));
		(&V_0)->set_Code_0(((int32_t)67108832));
		(&V_0)->set_Bits_1((uint8_t)((int32_t)26));
		TableEntry_tDFCA5A567802D72C4E23BFC6CEA14C32F52229FF  L_386 = V_0;
		NullCheck(L_385);
		(L_385)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)192)), (TableEntry_tDFCA5A567802D72C4E23BFC6CEA14C32F52229FF )L_386);
		TableEntryU5BU5D_tB3F4A3142B2B8D2FFFA48D2697C81F352A3D4A94* L_387 = L_385;
		il2cpp_codegen_initobj((&V_0), sizeof(TableEntry_tDFCA5A567802D72C4E23BFC6CEA14C32F52229FF ));
		(&V_0)->set_Code_0(((int32_t)67108833));
		(&V_0)->set_Bits_1((uint8_t)((int32_t)26));
		TableEntry_tDFCA5A567802D72C4E23BFC6CEA14C32F52229FF  L_388 = V_0;
		NullCheck(L_387);
		(L_387)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)193)), (TableEntry_tDFCA5A567802D72C4E23BFC6CEA14C32F52229FF )L_388);
		TableEntryU5BU5D_tB3F4A3142B2B8D2FFFA48D2697C81F352A3D4A94* L_389 = L_387;
		il2cpp_codegen_initobj((&V_0), sizeof(TableEntry_tDFCA5A567802D72C4E23BFC6CEA14C32F52229FF ));
		(&V_0)->set_Code_0(((int32_t)1048555));
		(&V_0)->set_Bits_1((uint8_t)((int32_t)20));
		TableEntry_tDFCA5A567802D72C4E23BFC6CEA14C32F52229FF  L_390 = V_0;
		NullCheck(L_389);
		(L_389)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)194)), (TableEntry_tDFCA5A567802D72C4E23BFC6CEA14C32F52229FF )L_390);
		TableEntryU5BU5D_tB3F4A3142B2B8D2FFFA48D2697C81F352A3D4A94* L_391 = L_389;
		il2cpp_codegen_initobj((&V_0), sizeof(TableEntry_tDFCA5A567802D72C4E23BFC6CEA14C32F52229FF ));
		(&V_0)->set_Code_0(((int32_t)524273));
		(&V_0)->set_Bits_1((uint8_t)((int32_t)19));
		TableEntry_tDFCA5A567802D72C4E23BFC6CEA14C32F52229FF  L_392 = V_0;
		NullCheck(L_391);
		(L_391)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)195)), (TableEntry_tDFCA5A567802D72C4E23BFC6CEA14C32F52229FF )L_392);
		TableEntryU5BU5D_tB3F4A3142B2B8D2FFFA48D2697C81F352A3D4A94* L_393 = L_391;
		il2cpp_codegen_initobj((&V_0), sizeof(TableEntry_tDFCA5A567802D72C4E23BFC6CEA14C32F52229FF ));
		(&V_0)->set_Code_0(((int32_t)4194279));
		(&V_0)->set_Bits_1((uint8_t)((int32_t)22));
		TableEntry_tDFCA5A567802D72C4E23BFC6CEA14C32F52229FF  L_394 = V_0;
		NullCheck(L_393);
		(L_393)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)196)), (TableEntry_tDFCA5A567802D72C4E23BFC6CEA14C32F52229FF )L_394);
		TableEntryU5BU5D_tB3F4A3142B2B8D2FFFA48D2697C81F352A3D4A94* L_395 = L_393;
		il2cpp_codegen_initobj((&V_0), sizeof(TableEntry_tDFCA5A567802D72C4E23BFC6CEA14C32F52229FF ));
		(&V_0)->set_Code_0(((int32_t)8388594));
		(&V_0)->set_Bits_1((uint8_t)((int32_t)23));
		TableEntry_tDFCA5A567802D72C4E23BFC6CEA14C32F52229FF  L_396 = V_0;
		NullCheck(L_395);
		(L_395)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)197)), (TableEntry_tDFCA5A567802D72C4E23BFC6CEA14C32F52229FF )L_396);
		TableEntryU5BU5D_tB3F4A3142B2B8D2FFFA48D2697C81F352A3D4A94* L_397 = L_395;
		il2cpp_codegen_initobj((&V_0), sizeof(TableEntry_tDFCA5A567802D72C4E23BFC6CEA14C32F52229FF ));
		(&V_0)->set_Code_0(((int32_t)4194280));
		(&V_0)->set_Bits_1((uint8_t)((int32_t)22));
		TableEntry_tDFCA5A567802D72C4E23BFC6CEA14C32F52229FF  L_398 = V_0;
		NullCheck(L_397);
		(L_397)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)198)), (TableEntry_tDFCA5A567802D72C4E23BFC6CEA14C32F52229FF )L_398);
		TableEntryU5BU5D_tB3F4A3142B2B8D2FFFA48D2697C81F352A3D4A94* L_399 = L_397;
		il2cpp_codegen_initobj((&V_0), sizeof(TableEntry_tDFCA5A567802D72C4E23BFC6CEA14C32F52229FF ));
		(&V_0)->set_Code_0(((int32_t)33554412));
		(&V_0)->set_Bits_1((uint8_t)((int32_t)25));
		TableEntry_tDFCA5A567802D72C4E23BFC6CEA14C32F52229FF  L_400 = V_0;
		NullCheck(L_399);
		(L_399)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)199)), (TableEntry_tDFCA5A567802D72C4E23BFC6CEA14C32F52229FF )L_400);
		TableEntryU5BU5D_tB3F4A3142B2B8D2FFFA48D2697C81F352A3D4A94* L_401 = L_399;
		il2cpp_codegen_initobj((&V_0), sizeof(TableEntry_tDFCA5A567802D72C4E23BFC6CEA14C32F52229FF ));
		(&V_0)->set_Code_0(((int32_t)67108834));
		(&V_0)->set_Bits_1((uint8_t)((int32_t)26));
		TableEntry_tDFCA5A567802D72C4E23BFC6CEA14C32F52229FF  L_402 = V_0;
		NullCheck(L_401);
		(L_401)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)200)), (TableEntry_tDFCA5A567802D72C4E23BFC6CEA14C32F52229FF )L_402);
		TableEntryU5BU5D_tB3F4A3142B2B8D2FFFA48D2697C81F352A3D4A94* L_403 = L_401;
		il2cpp_codegen_initobj((&V_0), sizeof(TableEntry_tDFCA5A567802D72C4E23BFC6CEA14C32F52229FF ));
		(&V_0)->set_Code_0(((int32_t)67108835));
		(&V_0)->set_Bits_1((uint8_t)((int32_t)26));
		TableEntry_tDFCA5A567802D72C4E23BFC6CEA14C32F52229FF  L_404 = V_0;
		NullCheck(L_403);
		(L_403)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)201)), (TableEntry_tDFCA5A567802D72C4E23BFC6CEA14C32F52229FF )L_404);
		TableEntryU5BU5D_tB3F4A3142B2B8D2FFFA48D2697C81F352A3D4A94* L_405 = L_403;
		il2cpp_codegen_initobj((&V_0), sizeof(TableEntry_tDFCA5A567802D72C4E23BFC6CEA14C32F52229FF ));
		(&V_0)->set_Code_0(((int32_t)67108836));
		(&V_0)->set_Bits_1((uint8_t)((int32_t)26));
		TableEntry_tDFCA5A567802D72C4E23BFC6CEA14C32F52229FF  L_406 = V_0;
		NullCheck(L_405);
		(L_405)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)202)), (TableEntry_tDFCA5A567802D72C4E23BFC6CEA14C32F52229FF )L_406);
		TableEntryU5BU5D_tB3F4A3142B2B8D2FFFA48D2697C81F352A3D4A94* L_407 = L_405;
		il2cpp_codegen_initobj((&V_0), sizeof(TableEntry_tDFCA5A567802D72C4E23BFC6CEA14C32F52229FF ));
		(&V_0)->set_Code_0(((int32_t)134217694));
		(&V_0)->set_Bits_1((uint8_t)((int32_t)27));
		TableEntry_tDFCA5A567802D72C4E23BFC6CEA14C32F52229FF  L_408 = V_0;
		NullCheck(L_407);
		(L_407)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)203)), (TableEntry_tDFCA5A567802D72C4E23BFC6CEA14C32F52229FF )L_408);
		TableEntryU5BU5D_tB3F4A3142B2B8D2FFFA48D2697C81F352A3D4A94* L_409 = L_407;
		il2cpp_codegen_initobj((&V_0), sizeof(TableEntry_tDFCA5A567802D72C4E23BFC6CEA14C32F52229FF ));
		(&V_0)->set_Code_0(((int32_t)134217695));
		(&V_0)->set_Bits_1((uint8_t)((int32_t)27));
		TableEntry_tDFCA5A567802D72C4E23BFC6CEA14C32F52229FF  L_410 = V_0;
		NullCheck(L_409);
		(L_409)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)204)), (TableEntry_tDFCA5A567802D72C4E23BFC6CEA14C32F52229FF )L_410);
		TableEntryU5BU5D_tB3F4A3142B2B8D2FFFA48D2697C81F352A3D4A94* L_411 = L_409;
		il2cpp_codegen_initobj((&V_0), sizeof(TableEntry_tDFCA5A567802D72C4E23BFC6CEA14C32F52229FF ));
		(&V_0)->set_Code_0(((int32_t)67108837));
		(&V_0)->set_Bits_1((uint8_t)((int32_t)26));
		TableEntry_tDFCA5A567802D72C4E23BFC6CEA14C32F52229FF  L_412 = V_0;
		NullCheck(L_411);
		(L_411)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)205)), (TableEntry_tDFCA5A567802D72C4E23BFC6CEA14C32F52229FF )L_412);
		TableEntryU5BU5D_tB3F4A3142B2B8D2FFFA48D2697C81F352A3D4A94* L_413 = L_411;
		il2cpp_codegen_initobj((&V_0), sizeof(TableEntry_tDFCA5A567802D72C4E23BFC6CEA14C32F52229FF ));
		(&V_0)->set_Code_0(((int32_t)16777201));
		(&V_0)->set_Bits_1((uint8_t)((int32_t)24));
		TableEntry_tDFCA5A567802D72C4E23BFC6CEA14C32F52229FF  L_414 = V_0;
		NullCheck(L_413);
		(L_413)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)206)), (TableEntry_tDFCA5A567802D72C4E23BFC6CEA14C32F52229FF )L_414);
		TableEntryU5BU5D_tB3F4A3142B2B8D2FFFA48D2697C81F352A3D4A94* L_415 = L_413;
		il2cpp_codegen_initobj((&V_0), sizeof(TableEntry_tDFCA5A567802D72C4E23BFC6CEA14C32F52229FF ));
		(&V_0)->set_Code_0(((int32_t)33554413));
		(&V_0)->set_Bits_1((uint8_t)((int32_t)25));
		TableEntry_tDFCA5A567802D72C4E23BFC6CEA14C32F52229FF  L_416 = V_0;
		NullCheck(L_415);
		(L_415)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)207)), (TableEntry_tDFCA5A567802D72C4E23BFC6CEA14C32F52229FF )L_416);
		TableEntryU5BU5D_tB3F4A3142B2B8D2FFFA48D2697C81F352A3D4A94* L_417 = L_415;
		il2cpp_codegen_initobj((&V_0), sizeof(TableEntry_tDFCA5A567802D72C4E23BFC6CEA14C32F52229FF ));
		(&V_0)->set_Code_0(((int32_t)524274));
		(&V_0)->set_Bits_1((uint8_t)((int32_t)19));
		TableEntry_tDFCA5A567802D72C4E23BFC6CEA14C32F52229FF  L_418 = V_0;
		NullCheck(L_417);
		(L_417)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)208)), (TableEntry_tDFCA5A567802D72C4E23BFC6CEA14C32F52229FF )L_418);
		TableEntryU5BU5D_tB3F4A3142B2B8D2FFFA48D2697C81F352A3D4A94* L_419 = L_417;
		il2cpp_codegen_initobj((&V_0), sizeof(TableEntry_tDFCA5A567802D72C4E23BFC6CEA14C32F52229FF ));
		(&V_0)->set_Code_0(((int32_t)2097123));
		(&V_0)->set_Bits_1((uint8_t)((int32_t)21));
		TableEntry_tDFCA5A567802D72C4E23BFC6CEA14C32F52229FF  L_420 = V_0;
		NullCheck(L_419);
		(L_419)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)209)), (TableEntry_tDFCA5A567802D72C4E23BFC6CEA14C32F52229FF )L_420);
		TableEntryU5BU5D_tB3F4A3142B2B8D2FFFA48D2697C81F352A3D4A94* L_421 = L_419;
		il2cpp_codegen_initobj((&V_0), sizeof(TableEntry_tDFCA5A567802D72C4E23BFC6CEA14C32F52229FF ));
		(&V_0)->set_Code_0(((int32_t)67108838));
		(&V_0)->set_Bits_1((uint8_t)((int32_t)26));
		TableEntry_tDFCA5A567802D72C4E23BFC6CEA14C32F52229FF  L_422 = V_0;
		NullCheck(L_421);
		(L_421)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)210)), (TableEntry_tDFCA5A567802D72C4E23BFC6CEA14C32F52229FF )L_422);
		TableEntryU5BU5D_tB3F4A3142B2B8D2FFFA48D2697C81F352A3D4A94* L_423 = L_421;
		il2cpp_codegen_initobj((&V_0), sizeof(TableEntry_tDFCA5A567802D72C4E23BFC6CEA14C32F52229FF ));
		(&V_0)->set_Code_0(((int32_t)134217696));
		(&V_0)->set_Bits_1((uint8_t)((int32_t)27));
		TableEntry_tDFCA5A567802D72C4E23BFC6CEA14C32F52229FF  L_424 = V_0;
		NullCheck(L_423);
		(L_423)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)211)), (TableEntry_tDFCA5A567802D72C4E23BFC6CEA14C32F52229FF )L_424);
		TableEntryU5BU5D_tB3F4A3142B2B8D2FFFA48D2697C81F352A3D4A94* L_425 = L_423;
		il2cpp_codegen_initobj((&V_0), sizeof(TableEntry_tDFCA5A567802D72C4E23BFC6CEA14C32F52229FF ));
		(&V_0)->set_Code_0(((int32_t)134217697));
		(&V_0)->set_Bits_1((uint8_t)((int32_t)27));
		TableEntry_tDFCA5A567802D72C4E23BFC6CEA14C32F52229FF  L_426 = V_0;
		NullCheck(L_425);
		(L_425)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)212)), (TableEntry_tDFCA5A567802D72C4E23BFC6CEA14C32F52229FF )L_426);
		TableEntryU5BU5D_tB3F4A3142B2B8D2FFFA48D2697C81F352A3D4A94* L_427 = L_425;
		il2cpp_codegen_initobj((&V_0), sizeof(TableEntry_tDFCA5A567802D72C4E23BFC6CEA14C32F52229FF ));
		(&V_0)->set_Code_0(((int32_t)67108839));
		(&V_0)->set_Bits_1((uint8_t)((int32_t)26));
		TableEntry_tDFCA5A567802D72C4E23BFC6CEA14C32F52229FF  L_428 = V_0;
		NullCheck(L_427);
		(L_427)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)213)), (TableEntry_tDFCA5A567802D72C4E23BFC6CEA14C32F52229FF )L_428);
		TableEntryU5BU5D_tB3F4A3142B2B8D2FFFA48D2697C81F352A3D4A94* L_429 = L_427;
		il2cpp_codegen_initobj((&V_0), sizeof(TableEntry_tDFCA5A567802D72C4E23BFC6CEA14C32F52229FF ));
		(&V_0)->set_Code_0(((int32_t)134217698));
		(&V_0)->set_Bits_1((uint8_t)((int32_t)27));
		TableEntry_tDFCA5A567802D72C4E23BFC6CEA14C32F52229FF  L_430 = V_0;
		NullCheck(L_429);
		(L_429)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)214)), (TableEntry_tDFCA5A567802D72C4E23BFC6CEA14C32F52229FF )L_430);
		TableEntryU5BU5D_tB3F4A3142B2B8D2FFFA48D2697C81F352A3D4A94* L_431 = L_429;
		il2cpp_codegen_initobj((&V_0), sizeof(TableEntry_tDFCA5A567802D72C4E23BFC6CEA14C32F52229FF ));
		(&V_0)->set_Code_0(((int32_t)16777202));
		(&V_0)->set_Bits_1((uint8_t)((int32_t)24));
		TableEntry_tDFCA5A567802D72C4E23BFC6CEA14C32F52229FF  L_432 = V_0;
		NullCheck(L_431);
		(L_431)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)215)), (TableEntry_tDFCA5A567802D72C4E23BFC6CEA14C32F52229FF )L_432);
		TableEntryU5BU5D_tB3F4A3142B2B8D2FFFA48D2697C81F352A3D4A94* L_433 = L_431;
		il2cpp_codegen_initobj((&V_0), sizeof(TableEntry_tDFCA5A567802D72C4E23BFC6CEA14C32F52229FF ));
		(&V_0)->set_Code_0(((int32_t)2097124));
		(&V_0)->set_Bits_1((uint8_t)((int32_t)21));
		TableEntry_tDFCA5A567802D72C4E23BFC6CEA14C32F52229FF  L_434 = V_0;
		NullCheck(L_433);
		(L_433)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)216)), (TableEntry_tDFCA5A567802D72C4E23BFC6CEA14C32F52229FF )L_434);
		TableEntryU5BU5D_tB3F4A3142B2B8D2FFFA48D2697C81F352A3D4A94* L_435 = L_433;
		il2cpp_codegen_initobj((&V_0), sizeof(TableEntry_tDFCA5A567802D72C4E23BFC6CEA14C32F52229FF ));
		(&V_0)->set_Code_0(((int32_t)2097125));
		(&V_0)->set_Bits_1((uint8_t)((int32_t)21));
		TableEntry_tDFCA5A567802D72C4E23BFC6CEA14C32F52229FF  L_436 = V_0;
		NullCheck(L_435);
		(L_435)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)217)), (TableEntry_tDFCA5A567802D72C4E23BFC6CEA14C32F52229FF )L_436);
		TableEntryU5BU5D_tB3F4A3142B2B8D2FFFA48D2697C81F352A3D4A94* L_437 = L_435;
		il2cpp_codegen_initobj((&V_0), sizeof(TableEntry_tDFCA5A567802D72C4E23BFC6CEA14C32F52229FF ));
		(&V_0)->set_Code_0(((int32_t)67108840));
		(&V_0)->set_Bits_1((uint8_t)((int32_t)26));
		TableEntry_tDFCA5A567802D72C4E23BFC6CEA14C32F52229FF  L_438 = V_0;
		NullCheck(L_437);
		(L_437)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)218)), (TableEntry_tDFCA5A567802D72C4E23BFC6CEA14C32F52229FF )L_438);
		TableEntryU5BU5D_tB3F4A3142B2B8D2FFFA48D2697C81F352A3D4A94* L_439 = L_437;
		il2cpp_codegen_initobj((&V_0), sizeof(TableEntry_tDFCA5A567802D72C4E23BFC6CEA14C32F52229FF ));
		(&V_0)->set_Code_0(((int32_t)67108841));
		(&V_0)->set_Bits_1((uint8_t)((int32_t)26));
		TableEntry_tDFCA5A567802D72C4E23BFC6CEA14C32F52229FF  L_440 = V_0;
		NullCheck(L_439);
		(L_439)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)219)), (TableEntry_tDFCA5A567802D72C4E23BFC6CEA14C32F52229FF )L_440);
		TableEntryU5BU5D_tB3F4A3142B2B8D2FFFA48D2697C81F352A3D4A94* L_441 = L_439;
		il2cpp_codegen_initobj((&V_0), sizeof(TableEntry_tDFCA5A567802D72C4E23BFC6CEA14C32F52229FF ));
		(&V_0)->set_Code_0(((int32_t)268435453));
		(&V_0)->set_Bits_1((uint8_t)((int32_t)28));
		TableEntry_tDFCA5A567802D72C4E23BFC6CEA14C32F52229FF  L_442 = V_0;
		NullCheck(L_441);
		(L_441)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)220)), (TableEntry_tDFCA5A567802D72C4E23BFC6CEA14C32F52229FF )L_442);
		TableEntryU5BU5D_tB3F4A3142B2B8D2FFFA48D2697C81F352A3D4A94* L_443 = L_441;
		il2cpp_codegen_initobj((&V_0), sizeof(TableEntry_tDFCA5A567802D72C4E23BFC6CEA14C32F52229FF ));
		(&V_0)->set_Code_0(((int32_t)134217699));
		(&V_0)->set_Bits_1((uint8_t)((int32_t)27));
		TableEntry_tDFCA5A567802D72C4E23BFC6CEA14C32F52229FF  L_444 = V_0;
		NullCheck(L_443);
		(L_443)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)221)), (TableEntry_tDFCA5A567802D72C4E23BFC6CEA14C32F52229FF )L_444);
		TableEntryU5BU5D_tB3F4A3142B2B8D2FFFA48D2697C81F352A3D4A94* L_445 = L_443;
		il2cpp_codegen_initobj((&V_0), sizeof(TableEntry_tDFCA5A567802D72C4E23BFC6CEA14C32F52229FF ));
		(&V_0)->set_Code_0(((int32_t)134217700));
		(&V_0)->set_Bits_1((uint8_t)((int32_t)27));
		TableEntry_tDFCA5A567802D72C4E23BFC6CEA14C32F52229FF  L_446 = V_0;
		NullCheck(L_445);
		(L_445)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)222)), (TableEntry_tDFCA5A567802D72C4E23BFC6CEA14C32F52229FF )L_446);
		TableEntryU5BU5D_tB3F4A3142B2B8D2FFFA48D2697C81F352A3D4A94* L_447 = L_445;
		il2cpp_codegen_initobj((&V_0), sizeof(TableEntry_tDFCA5A567802D72C4E23BFC6CEA14C32F52229FF ));
		(&V_0)->set_Code_0(((int32_t)134217701));
		(&V_0)->set_Bits_1((uint8_t)((int32_t)27));
		TableEntry_tDFCA5A567802D72C4E23BFC6CEA14C32F52229FF  L_448 = V_0;
		NullCheck(L_447);
		(L_447)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)223)), (TableEntry_tDFCA5A567802D72C4E23BFC6CEA14C32F52229FF )L_448);
		TableEntryU5BU5D_tB3F4A3142B2B8D2FFFA48D2697C81F352A3D4A94* L_449 = L_447;
		il2cpp_codegen_initobj((&V_0), sizeof(TableEntry_tDFCA5A567802D72C4E23BFC6CEA14C32F52229FF ));
		(&V_0)->set_Code_0(((int32_t)1048556));
		(&V_0)->set_Bits_1((uint8_t)((int32_t)20));
		TableEntry_tDFCA5A567802D72C4E23BFC6CEA14C32F52229FF  L_450 = V_0;
		NullCheck(L_449);
		(L_449)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)224)), (TableEntry_tDFCA5A567802D72C4E23BFC6CEA14C32F52229FF )L_450);
		TableEntryU5BU5D_tB3F4A3142B2B8D2FFFA48D2697C81F352A3D4A94* L_451 = L_449;
		il2cpp_codegen_initobj((&V_0), sizeof(TableEntry_tDFCA5A567802D72C4E23BFC6CEA14C32F52229FF ));
		(&V_0)->set_Code_0(((int32_t)16777203));
		(&V_0)->set_Bits_1((uint8_t)((int32_t)24));
		TableEntry_tDFCA5A567802D72C4E23BFC6CEA14C32F52229FF  L_452 = V_0;
		NullCheck(L_451);
		(L_451)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)225)), (TableEntry_tDFCA5A567802D72C4E23BFC6CEA14C32F52229FF )L_452);
		TableEntryU5BU5D_tB3F4A3142B2B8D2FFFA48D2697C81F352A3D4A94* L_453 = L_451;
		il2cpp_codegen_initobj((&V_0), sizeof(TableEntry_tDFCA5A567802D72C4E23BFC6CEA14C32F52229FF ));
		(&V_0)->set_Code_0(((int32_t)1048557));
		(&V_0)->set_Bits_1((uint8_t)((int32_t)20));
		TableEntry_tDFCA5A567802D72C4E23BFC6CEA14C32F52229FF  L_454 = V_0;
		NullCheck(L_453);
		(L_453)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)226)), (TableEntry_tDFCA5A567802D72C4E23BFC6CEA14C32F52229FF )L_454);
		TableEntryU5BU5D_tB3F4A3142B2B8D2FFFA48D2697C81F352A3D4A94* L_455 = L_453;
		il2cpp_codegen_initobj((&V_0), sizeof(TableEntry_tDFCA5A567802D72C4E23BFC6CEA14C32F52229FF ));
		(&V_0)->set_Code_0(((int32_t)2097126));
		(&V_0)->set_Bits_1((uint8_t)((int32_t)21));
		TableEntry_tDFCA5A567802D72C4E23BFC6CEA14C32F52229FF  L_456 = V_0;
		NullCheck(L_455);
		(L_455)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)227)), (TableEntry_tDFCA5A567802D72C4E23BFC6CEA14C32F52229FF )L_456);
		TableEntryU5BU5D_tB3F4A3142B2B8D2FFFA48D2697C81F352A3D4A94* L_457 = L_455;
		il2cpp_codegen_initobj((&V_0), sizeof(TableEntry_tDFCA5A567802D72C4E23BFC6CEA14C32F52229FF ));
		(&V_0)->set_Code_0(((int32_t)4194281));
		(&V_0)->set_Bits_1((uint8_t)((int32_t)22));
		TableEntry_tDFCA5A567802D72C4E23BFC6CEA14C32F52229FF  L_458 = V_0;
		NullCheck(L_457);
		(L_457)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)228)), (TableEntry_tDFCA5A567802D72C4E23BFC6CEA14C32F52229FF )L_458);
		TableEntryU5BU5D_tB3F4A3142B2B8D2FFFA48D2697C81F352A3D4A94* L_459 = L_457;
		il2cpp_codegen_initobj((&V_0), sizeof(TableEntry_tDFCA5A567802D72C4E23BFC6CEA14C32F52229FF ));
		(&V_0)->set_Code_0(((int32_t)2097127));
		(&V_0)->set_Bits_1((uint8_t)((int32_t)21));
		TableEntry_tDFCA5A567802D72C4E23BFC6CEA14C32F52229FF  L_460 = V_0;
		NullCheck(L_459);
		(L_459)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)229)), (TableEntry_tDFCA5A567802D72C4E23BFC6CEA14C32F52229FF )L_460);
		TableEntryU5BU5D_tB3F4A3142B2B8D2FFFA48D2697C81F352A3D4A94* L_461 = L_459;
		il2cpp_codegen_initobj((&V_0), sizeof(TableEntry_tDFCA5A567802D72C4E23BFC6CEA14C32F52229FF ));
		(&V_0)->set_Code_0(((int32_t)2097128));
		(&V_0)->set_Bits_1((uint8_t)((int32_t)21));
		TableEntry_tDFCA5A567802D72C4E23BFC6CEA14C32F52229FF  L_462 = V_0;
		NullCheck(L_461);
		(L_461)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)230)), (TableEntry_tDFCA5A567802D72C4E23BFC6CEA14C32F52229FF )L_462);
		TableEntryU5BU5D_tB3F4A3142B2B8D2FFFA48D2697C81F352A3D4A94* L_463 = L_461;
		il2cpp_codegen_initobj((&V_0), sizeof(TableEntry_tDFCA5A567802D72C4E23BFC6CEA14C32F52229FF ));
		(&V_0)->set_Code_0(((int32_t)8388595));
		(&V_0)->set_Bits_1((uint8_t)((int32_t)23));
		TableEntry_tDFCA5A567802D72C4E23BFC6CEA14C32F52229FF  L_464 = V_0;
		NullCheck(L_463);
		(L_463)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)231)), (TableEntry_tDFCA5A567802D72C4E23BFC6CEA14C32F52229FF )L_464);
		TableEntryU5BU5D_tB3F4A3142B2B8D2FFFA48D2697C81F352A3D4A94* L_465 = L_463;
		il2cpp_codegen_initobj((&V_0), sizeof(TableEntry_tDFCA5A567802D72C4E23BFC6CEA14C32F52229FF ));
		(&V_0)->set_Code_0(((int32_t)4194282));
		(&V_0)->set_Bits_1((uint8_t)((int32_t)22));
		TableEntry_tDFCA5A567802D72C4E23BFC6CEA14C32F52229FF  L_466 = V_0;
		NullCheck(L_465);
		(L_465)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)232)), (TableEntry_tDFCA5A567802D72C4E23BFC6CEA14C32F52229FF )L_466);
		TableEntryU5BU5D_tB3F4A3142B2B8D2FFFA48D2697C81F352A3D4A94* L_467 = L_465;
		il2cpp_codegen_initobj((&V_0), sizeof(TableEntry_tDFCA5A567802D72C4E23BFC6CEA14C32F52229FF ));
		(&V_0)->set_Code_0(((int32_t)4194283));
		(&V_0)->set_Bits_1((uint8_t)((int32_t)22));
		TableEntry_tDFCA5A567802D72C4E23BFC6CEA14C32F52229FF  L_468 = V_0;
		NullCheck(L_467);
		(L_467)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)233)), (TableEntry_tDFCA5A567802D72C4E23BFC6CEA14C32F52229FF )L_468);
		TableEntryU5BU5D_tB3F4A3142B2B8D2FFFA48D2697C81F352A3D4A94* L_469 = L_467;
		il2cpp_codegen_initobj((&V_0), sizeof(TableEntry_tDFCA5A567802D72C4E23BFC6CEA14C32F52229FF ));
		(&V_0)->set_Code_0(((int32_t)33554414));
		(&V_0)->set_Bits_1((uint8_t)((int32_t)25));
		TableEntry_tDFCA5A567802D72C4E23BFC6CEA14C32F52229FF  L_470 = V_0;
		NullCheck(L_469);
		(L_469)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)234)), (TableEntry_tDFCA5A567802D72C4E23BFC6CEA14C32F52229FF )L_470);
		TableEntryU5BU5D_tB3F4A3142B2B8D2FFFA48D2697C81F352A3D4A94* L_471 = L_469;
		il2cpp_codegen_initobj((&V_0), sizeof(TableEntry_tDFCA5A567802D72C4E23BFC6CEA14C32F52229FF ));
		(&V_0)->set_Code_0(((int32_t)33554415));
		(&V_0)->set_Bits_1((uint8_t)((int32_t)25));
		TableEntry_tDFCA5A567802D72C4E23BFC6CEA14C32F52229FF  L_472 = V_0;
		NullCheck(L_471);
		(L_471)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)235)), (TableEntry_tDFCA5A567802D72C4E23BFC6CEA14C32F52229FF )L_472);
		TableEntryU5BU5D_tB3F4A3142B2B8D2FFFA48D2697C81F352A3D4A94* L_473 = L_471;
		il2cpp_codegen_initobj((&V_0), sizeof(TableEntry_tDFCA5A567802D72C4E23BFC6CEA14C32F52229FF ));
		(&V_0)->set_Code_0(((int32_t)16777204));
		(&V_0)->set_Bits_1((uint8_t)((int32_t)24));
		TableEntry_tDFCA5A567802D72C4E23BFC6CEA14C32F52229FF  L_474 = V_0;
		NullCheck(L_473);
		(L_473)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)236)), (TableEntry_tDFCA5A567802D72C4E23BFC6CEA14C32F52229FF )L_474);
		TableEntryU5BU5D_tB3F4A3142B2B8D2FFFA48D2697C81F352A3D4A94* L_475 = L_473;
		il2cpp_codegen_initobj((&V_0), sizeof(TableEntry_tDFCA5A567802D72C4E23BFC6CEA14C32F52229FF ));
		(&V_0)->set_Code_0(((int32_t)16777205));
		(&V_0)->set_Bits_1((uint8_t)((int32_t)24));
		TableEntry_tDFCA5A567802D72C4E23BFC6CEA14C32F52229FF  L_476 = V_0;
		NullCheck(L_475);
		(L_475)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)237)), (TableEntry_tDFCA5A567802D72C4E23BFC6CEA14C32F52229FF )L_476);
		TableEntryU5BU5D_tB3F4A3142B2B8D2FFFA48D2697C81F352A3D4A94* L_477 = L_475;
		il2cpp_codegen_initobj((&V_0), sizeof(TableEntry_tDFCA5A567802D72C4E23BFC6CEA14C32F52229FF ));
		(&V_0)->set_Code_0(((int32_t)67108842));
		(&V_0)->set_Bits_1((uint8_t)((int32_t)26));
		TableEntry_tDFCA5A567802D72C4E23BFC6CEA14C32F52229FF  L_478 = V_0;
		NullCheck(L_477);
		(L_477)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)238)), (TableEntry_tDFCA5A567802D72C4E23BFC6CEA14C32F52229FF )L_478);
		TableEntryU5BU5D_tB3F4A3142B2B8D2FFFA48D2697C81F352A3D4A94* L_479 = L_477;
		il2cpp_codegen_initobj((&V_0), sizeof(TableEntry_tDFCA5A567802D72C4E23BFC6CEA14C32F52229FF ));
		(&V_0)->set_Code_0(((int32_t)8388596));
		(&V_0)->set_Bits_1((uint8_t)((int32_t)23));
		TableEntry_tDFCA5A567802D72C4E23BFC6CEA14C32F52229FF  L_480 = V_0;
		NullCheck(L_479);
		(L_479)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)239)), (TableEntry_tDFCA5A567802D72C4E23BFC6CEA14C32F52229FF )L_480);
		TableEntryU5BU5D_tB3F4A3142B2B8D2FFFA48D2697C81F352A3D4A94* L_481 = L_479;
		il2cpp_codegen_initobj((&V_0), sizeof(TableEntry_tDFCA5A567802D72C4E23BFC6CEA14C32F52229FF ));
		(&V_0)->set_Code_0(((int32_t)67108843));
		(&V_0)->set_Bits_1((uint8_t)((int32_t)26));
		TableEntry_tDFCA5A567802D72C4E23BFC6CEA14C32F52229FF  L_482 = V_0;
		NullCheck(L_481);
		(L_481)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)240)), (TableEntry_tDFCA5A567802D72C4E23BFC6CEA14C32F52229FF )L_482);
		TableEntryU5BU5D_tB3F4A3142B2B8D2FFFA48D2697C81F352A3D4A94* L_483 = L_481;
		il2cpp_codegen_initobj((&V_0), sizeof(TableEntry_tDFCA5A567802D72C4E23BFC6CEA14C32F52229FF ));
		(&V_0)->set_Code_0(((int32_t)134217702));
		(&V_0)->set_Bits_1((uint8_t)((int32_t)27));
		TableEntry_tDFCA5A567802D72C4E23BFC6CEA14C32F52229FF  L_484 = V_0;
		NullCheck(L_483);
		(L_483)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)241)), (TableEntry_tDFCA5A567802D72C4E23BFC6CEA14C32F52229FF )L_484);
		TableEntryU5BU5D_tB3F4A3142B2B8D2FFFA48D2697C81F352A3D4A94* L_485 = L_483;
		il2cpp_codegen_initobj((&V_0), sizeof(TableEntry_tDFCA5A567802D72C4E23BFC6CEA14C32F52229FF ));
		(&V_0)->set_Code_0(((int32_t)67108844));
		(&V_0)->set_Bits_1((uint8_t)((int32_t)26));
		TableEntry_tDFCA5A567802D72C4E23BFC6CEA14C32F52229FF  L_486 = V_0;
		NullCheck(L_485);
		(L_485)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)242)), (TableEntry_tDFCA5A567802D72C4E23BFC6CEA14C32F52229FF )L_486);
		TableEntryU5BU5D_tB3F4A3142B2B8D2FFFA48D2697C81F352A3D4A94* L_487 = L_485;
		il2cpp_codegen_initobj((&V_0), sizeof(TableEntry_tDFCA5A567802D72C4E23BFC6CEA14C32F52229FF ));
		(&V_0)->set_Code_0(((int32_t)67108845));
		(&V_0)->set_Bits_1((uint8_t)((int32_t)26));
		TableEntry_tDFCA5A567802D72C4E23BFC6CEA14C32F52229FF  L_488 = V_0;
		NullCheck(L_487);
		(L_487)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)243)), (TableEntry_tDFCA5A567802D72C4E23BFC6CEA14C32F52229FF )L_488);
		TableEntryU5BU5D_tB3F4A3142B2B8D2FFFA48D2697C81F352A3D4A94* L_489 = L_487;
		il2cpp_codegen_initobj((&V_0), sizeof(TableEntry_tDFCA5A567802D72C4E23BFC6CEA14C32F52229FF ));
		(&V_0)->set_Code_0(((int32_t)134217703));
		(&V_0)->set_Bits_1((uint8_t)((int32_t)27));
		TableEntry_tDFCA5A567802D72C4E23BFC6CEA14C32F52229FF  L_490 = V_0;
		NullCheck(L_489);
		(L_489)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)244)), (TableEntry_tDFCA5A567802D72C4E23BFC6CEA14C32F52229FF )L_490);
		TableEntryU5BU5D_tB3F4A3142B2B8D2FFFA48D2697C81F352A3D4A94* L_491 = L_489;
		il2cpp_codegen_initobj((&V_0), sizeof(TableEntry_tDFCA5A567802D72C4E23BFC6CEA14C32F52229FF ));
		(&V_0)->set_Code_0(((int32_t)134217704));
		(&V_0)->set_Bits_1((uint8_t)((int32_t)27));
		TableEntry_tDFCA5A567802D72C4E23BFC6CEA14C32F52229FF  L_492 = V_0;
		NullCheck(L_491);
		(L_491)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)245)), (TableEntry_tDFCA5A567802D72C4E23BFC6CEA14C32F52229FF )L_492);
		TableEntryU5BU5D_tB3F4A3142B2B8D2FFFA48D2697C81F352A3D4A94* L_493 = L_491;
		il2cpp_codegen_initobj((&V_0), sizeof(TableEntry_tDFCA5A567802D72C4E23BFC6CEA14C32F52229FF ));
		(&V_0)->set_Code_0(((int32_t)134217705));
		(&V_0)->set_Bits_1((uint8_t)((int32_t)27));
		TableEntry_tDFCA5A567802D72C4E23BFC6CEA14C32F52229FF  L_494 = V_0;
		NullCheck(L_493);
		(L_493)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)246)), (TableEntry_tDFCA5A567802D72C4E23BFC6CEA14C32F52229FF )L_494);
		TableEntryU5BU5D_tB3F4A3142B2B8D2FFFA48D2697C81F352A3D4A94* L_495 = L_493;
		il2cpp_codegen_initobj((&V_0), sizeof(TableEntry_tDFCA5A567802D72C4E23BFC6CEA14C32F52229FF ));
		(&V_0)->set_Code_0(((int32_t)134217706));
		(&V_0)->set_Bits_1((uint8_t)((int32_t)27));
		TableEntry_tDFCA5A567802D72C4E23BFC6CEA14C32F52229FF  L_496 = V_0;
		NullCheck(L_495);
		(L_495)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)247)), (TableEntry_tDFCA5A567802D72C4E23BFC6CEA14C32F52229FF )L_496);
		TableEntryU5BU5D_tB3F4A3142B2B8D2FFFA48D2697C81F352A3D4A94* L_497 = L_495;
		il2cpp_codegen_initobj((&V_0), sizeof(TableEntry_tDFCA5A567802D72C4E23BFC6CEA14C32F52229FF ));
		(&V_0)->set_Code_0(((int32_t)134217707));
		(&V_0)->set_Bits_1((uint8_t)((int32_t)27));
		TableEntry_tDFCA5A567802D72C4E23BFC6CEA14C32F52229FF  L_498 = V_0;
		NullCheck(L_497);
		(L_497)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)248)), (TableEntry_tDFCA5A567802D72C4E23BFC6CEA14C32F52229FF )L_498);
		TableEntryU5BU5D_tB3F4A3142B2B8D2FFFA48D2697C81F352A3D4A94* L_499 = L_497;
		il2cpp_codegen_initobj((&V_0), sizeof(TableEntry_tDFCA5A567802D72C4E23BFC6CEA14C32F52229FF ));
		(&V_0)->set_Code_0(((int32_t)268435454));
		(&V_0)->set_Bits_1((uint8_t)((int32_t)28));
		TableEntry_tDFCA5A567802D72C4E23BFC6CEA14C32F52229FF  L_500 = V_0;
		NullCheck(L_499);
		(L_499)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)249)), (TableEntry_tDFCA5A567802D72C4E23BFC6CEA14C32F52229FF )L_500);
		TableEntryU5BU5D_tB3F4A3142B2B8D2FFFA48D2697C81F352A3D4A94* L_501 = L_499;
		il2cpp_codegen_initobj((&V_0), sizeof(TableEntry_tDFCA5A567802D72C4E23BFC6CEA14C32F52229FF ));
		(&V_0)->set_Code_0(((int32_t)134217708));
		(&V_0)->set_Bits_1((uint8_t)((int32_t)27));
		TableEntry_tDFCA5A567802D72C4E23BFC6CEA14C32F52229FF  L_502 = V_0;
		NullCheck(L_501);
		(L_501)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)250)), (TableEntry_tDFCA5A567802D72C4E23BFC6CEA14C32F52229FF )L_502);
		TableEntryU5BU5D_tB3F4A3142B2B8D2FFFA48D2697C81F352A3D4A94* L_503 = L_501;
		il2cpp_codegen_initobj((&V_0), sizeof(TableEntry_tDFCA5A567802D72C4E23BFC6CEA14C32F52229FF ));
		(&V_0)->set_Code_0(((int32_t)134217709));
		(&V_0)->set_Bits_1((uint8_t)((int32_t)27));
		TableEntry_tDFCA5A567802D72C4E23BFC6CEA14C32F52229FF  L_504 = V_0;
		NullCheck(L_503);
		(L_503)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)251)), (TableEntry_tDFCA5A567802D72C4E23BFC6CEA14C32F52229FF )L_504);
		TableEntryU5BU5D_tB3F4A3142B2B8D2FFFA48D2697C81F352A3D4A94* L_505 = L_503;
		il2cpp_codegen_initobj((&V_0), sizeof(TableEntry_tDFCA5A567802D72C4E23BFC6CEA14C32F52229FF ));
		(&V_0)->set_Code_0(((int32_t)134217710));
		(&V_0)->set_Bits_1((uint8_t)((int32_t)27));
		TableEntry_tDFCA5A567802D72C4E23BFC6CEA14C32F52229FF  L_506 = V_0;
		NullCheck(L_505);
		(L_505)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)252)), (TableEntry_tDFCA5A567802D72C4E23BFC6CEA14C32F52229FF )L_506);
		TableEntryU5BU5D_tB3F4A3142B2B8D2FFFA48D2697C81F352A3D4A94* L_507 = L_505;
		il2cpp_codegen_initobj((&V_0), sizeof(TableEntry_tDFCA5A567802D72C4E23BFC6CEA14C32F52229FF ));
		(&V_0)->set_Code_0(((int32_t)134217711));
		(&V_0)->set_Bits_1((uint8_t)((int32_t)27));
		TableEntry_tDFCA5A567802D72C4E23BFC6CEA14C32F52229FF  L_508 = V_0;
		NullCheck(L_507);
		(L_507)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)253)), (TableEntry_tDFCA5A567802D72C4E23BFC6CEA14C32F52229FF )L_508);
		TableEntryU5BU5D_tB3F4A3142B2B8D2FFFA48D2697C81F352A3D4A94* L_509 = L_507;
		il2cpp_codegen_initobj((&V_0), sizeof(TableEntry_tDFCA5A567802D72C4E23BFC6CEA14C32F52229FF ));
		(&V_0)->set_Code_0(((int32_t)134217712));
		(&V_0)->set_Bits_1((uint8_t)((int32_t)27));
		TableEntry_tDFCA5A567802D72C4E23BFC6CEA14C32F52229FF  L_510 = V_0;
		NullCheck(L_509);
		(L_509)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)254)), (TableEntry_tDFCA5A567802D72C4E23BFC6CEA14C32F52229FF )L_510);
		TableEntryU5BU5D_tB3F4A3142B2B8D2FFFA48D2697C81F352A3D4A94* L_511 = L_509;
		il2cpp_codegen_initobj((&V_0), sizeof(TableEntry_tDFCA5A567802D72C4E23BFC6CEA14C32F52229FF ));
		(&V_0)->set_Code_0(((int32_t)67108846));
		(&V_0)->set_Bits_1((uint8_t)((int32_t)26));
		TableEntry_tDFCA5A567802D72C4E23BFC6CEA14C32F52229FF  L_512 = V_0;
		NullCheck(L_511);
		(L_511)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)255)), (TableEntry_tDFCA5A567802D72C4E23BFC6CEA14C32F52229FF )L_512);
		TableEntryU5BU5D_tB3F4A3142B2B8D2FFFA48D2697C81F352A3D4A94* L_513 = L_511;
		il2cpp_codegen_initobj((&V_0), sizeof(TableEntry_tDFCA5A567802D72C4E23BFC6CEA14C32F52229FF ));
		(&V_0)->set_Code_0(((int32_t)1073741823));
		(&V_0)->set_Bits_1((uint8_t)((int32_t)30));
		TableEntry_tDFCA5A567802D72C4E23BFC6CEA14C32F52229FF  L_514 = V_0;
		NullCheck(L_513);
		(L_513)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)256)), (TableEntry_tDFCA5A567802D72C4E23BFC6CEA14C32F52229FF )L_514);
		((HuffmanEncoder_tE53CF82173488443CE9781D476D0E545B4485E6C_StaticFields*)il2cpp_codegen_static_fields_for(HuffmanEncoder_tE53CF82173488443CE9781D476D0E545B4485E6C_il2cpp_TypeInfo_var))->set_StaticTable_1(L_513);
		// static TreeNode[] HuffmanTree = new TreeNode[]
		// {
		//     new TreeNode { Value = 0, NextZeroIdx = 98, NextOneIdx = 1 },
		//     new TreeNode { Value = 0, NextZeroIdx = 151, NextOneIdx = 2 },
		//     new TreeNode { Value = 0, NextZeroIdx = 173, NextOneIdx = 3 },
		//     new TreeNode { Value = 0, NextZeroIdx = 204, NextOneIdx = 4 },
		//     new TreeNode { Value = 0, NextZeroIdx = 263, NextOneIdx = 5 },
		//     new TreeNode { Value = 0, NextZeroIdx = 113, NextOneIdx = 6 },
		//     new TreeNode { Value = 0, NextZeroIdx = 211, NextOneIdx = 7 },
		//     new TreeNode { Value = 0, NextZeroIdx = 104, NextOneIdx = 8 },
		//     new TreeNode { Value = 0, NextZeroIdx = 116, NextOneIdx = 9 },
		//     new TreeNode { Value = 0, NextZeroIdx = 108, NextOneIdx = 10 },
		//     new TreeNode { Value = 0, NextZeroIdx = 11, NextOneIdx = 14 },
		//     new TreeNode { Value = 0, NextZeroIdx = 12, NextOneIdx = 166 },
		//     new TreeNode { Value = 0, NextZeroIdx = 13, NextOneIdx = 111 },
		//     new TreeNode { Value = 0, NextZeroIdx = 0, NextOneIdx = 0 },
		//     new TreeNode { Value = 0, NextZeroIdx = 220, NextOneIdx = 15 },
		//     new TreeNode { Value = 0, NextZeroIdx = 222, NextOneIdx = 16 },
		//     new TreeNode { Value = 0, NextZeroIdx = 158, NextOneIdx = 17 },
		//     new TreeNode { Value = 0, NextZeroIdx = 270, NextOneIdx = 18 },
		//     new TreeNode { Value = 0, NextZeroIdx = 216, NextOneIdx = 19 },
		//     new TreeNode { Value = 0, NextZeroIdx = 279, NextOneIdx = 20 },
		//     new TreeNode { Value = 0, NextZeroIdx = 21, NextOneIdx = 27 },
		//     new TreeNode { Value = 0, NextZeroIdx = 377, NextOneIdx = 22 },
		//     new TreeNode { Value = 0, NextZeroIdx = 414, NextOneIdx = 23 },
		//     new TreeNode { Value = 0, NextZeroIdx = 24, NextOneIdx = 301 },
		//     new TreeNode { Value = 0, NextZeroIdx = 25, NextOneIdx = 298 },
		//     new TreeNode { Value = 0, NextZeroIdx = 26, NextOneIdx = 295 },
		//     new TreeNode { Value = 1, NextZeroIdx = 0, NextOneIdx = 0 },
		//     new TreeNode { Value = 0, NextZeroIdx = 314, NextOneIdx = 28 },
		//     new TreeNode { Value = 0, NextZeroIdx = 50, NextOneIdx = 29 },
		//     new TreeNode { Value = 0, NextZeroIdx = 362, NextOneIdx = 30 },
		//     new TreeNode { Value = 0, NextZeroIdx = 403, NextOneIdx = 31 },
		//     new TreeNode { Value = 0, NextZeroIdx = 440, NextOneIdx = 32 },
		//     new TreeNode { Value = 0, NextZeroIdx = 33, NextOneIdx = 55 },
		//     new TreeNode { Value = 0, NextZeroIdx = 34, NextOneIdx = 46 },
		//     new TreeNode { Value = 0, NextZeroIdx = 35, NextOneIdx = 39 },
		//     new TreeNode { Value = 0, NextZeroIdx = 510, NextOneIdx = 36 },
		//     new TreeNode { Value = 0, NextZeroIdx = 37, NextOneIdx = 38 },
		//     new TreeNode { Value = 2, NextZeroIdx = 0, NextOneIdx = 0 },
		//     new TreeNode { Value = 3, NextZeroIdx = 0, NextOneIdx = 0 },
		//     new TreeNode { Value = 0, NextZeroIdx = 40, NextOneIdx = 43 },
		//     new TreeNode { Value = 0, NextZeroIdx = 41, NextOneIdx = 42 },
		//     new TreeNode { Value = 4, NextZeroIdx = 0, NextOneIdx = 0 },
		//     new TreeNode { Value = 5, NextZeroIdx = 0, NextOneIdx = 0 },
		//     new TreeNode { Value = 0, NextZeroIdx = 44, NextOneIdx = 45 },
		//     new TreeNode { Value = 6, NextZeroIdx = 0, NextOneIdx = 0 },
		//     new TreeNode { Value = 7, NextZeroIdx = 0, NextOneIdx = 0 },
		//     new TreeNode { Value = 0, NextZeroIdx = 47, NextOneIdx = 67 },
		//     new TreeNode { Value = 0, NextZeroIdx = 48, NextOneIdx = 63 },
		//     new TreeNode { Value = 0, NextZeroIdx = 49, NextOneIdx = 62 },
		//     new TreeNode { Value = 8, NextZeroIdx = 0, NextOneIdx = 0 },
		//     new TreeNode { Value = 0, NextZeroIdx = 396, NextOneIdx = 51 },
		//     new TreeNode { Value = 0, NextZeroIdx = 52, NextOneIdx = 309 },
		//     new TreeNode { Value = 0, NextZeroIdx = 486, NextOneIdx = 53 },
		//     new TreeNode { Value = 0, NextZeroIdx = 54, NextOneIdx = 307 },
		//     new TreeNode { Value = 9, NextZeroIdx = 0, NextOneIdx = 0 },
		//     new TreeNode { Value = 0, NextZeroIdx = 74, NextOneIdx = 56 },
		//     new TreeNode { Value = 0, NextZeroIdx = 91, NextOneIdx = 57 },
		//     new TreeNode { Value = 0, NextZeroIdx = 274, NextOneIdx = 58 },
		//     new TreeNode { Value = 0, NextZeroIdx = 502, NextOneIdx = 59 },
		//     new TreeNode { Value = 0, NextZeroIdx = 60, NextOneIdx = 81 },
		//     new TreeNode { Value = 0, NextZeroIdx = 61, NextOneIdx = 65 },
		//     new TreeNode { Value = 10, NextZeroIdx = 0, NextOneIdx = 0 },
		//     new TreeNode { Value = 11, NextZeroIdx = 0, NextOneIdx = 0 },
		//     new TreeNode { Value = 0, NextZeroIdx = 64, NextOneIdx = 66 },
		//     new TreeNode { Value = 12, NextZeroIdx = 0, NextOneIdx = 0 },
		//     new TreeNode { Value = 13, NextZeroIdx = 0, NextOneIdx = 0 },
		//     new TreeNode { Value = 14, NextZeroIdx = 0, NextOneIdx = 0 },
		//     new TreeNode { Value = 0, NextZeroIdx = 68, NextOneIdx = 71 },
		//     new TreeNode { Value = 0, NextZeroIdx = 69, NextOneIdx = 70 },
		//     new TreeNode { Value = 15, NextZeroIdx = 0, NextOneIdx = 0 },
		//     new TreeNode { Value = 16, NextZeroIdx = 0, NextOneIdx = 0 },
		//     new TreeNode { Value = 0, NextZeroIdx = 72, NextOneIdx = 73 },
		//     new TreeNode { Value = 17, NextZeroIdx = 0, NextOneIdx = 0 },
		//     new TreeNode { Value = 18, NextZeroIdx = 0, NextOneIdx = 0 },
		//     new TreeNode { Value = 0, NextZeroIdx = 75, NextOneIdx = 84 },
		//     new TreeNode { Value = 0, NextZeroIdx = 76, NextOneIdx = 79 },
		//     new TreeNode { Value = 0, NextZeroIdx = 77, NextOneIdx = 78 },
		//     new TreeNode { Value = 19, NextZeroIdx = 0, NextOneIdx = 0 },
		//     new TreeNode { Value = 20, NextZeroIdx = 0, NextOneIdx = 0 },
		//     new TreeNode { Value = 0, NextZeroIdx = 80, NextOneIdx = 83 },
		//     new TreeNode { Value = 21, NextZeroIdx = 0, NextOneIdx = 0 },
		//     new TreeNode { Value = 0, NextZeroIdx = 82, NextOneIdx = 512 },
		//     new TreeNode { Value = 22, NextZeroIdx = 0, NextOneIdx = 0 },
		//     new TreeNode { Value = 23, NextZeroIdx = 0, NextOneIdx = 0 },
		//     new TreeNode { Value = 0, NextZeroIdx = 85, NextOneIdx = 88 },
		//     new TreeNode { Value = 0, NextZeroIdx = 86, NextOneIdx = 87 },
		//     new TreeNode { Value = 24, NextZeroIdx = 0, NextOneIdx = 0 },
		//     new TreeNode { Value = 25, NextZeroIdx = 0, NextOneIdx = 0 },
		//     new TreeNode { Value = 0, NextZeroIdx = 89, NextOneIdx = 90 },
		//     new TreeNode { Value = 26, NextZeroIdx = 0, NextOneIdx = 0 },
		//     new TreeNode { Value = 27, NextZeroIdx = 0, NextOneIdx = 0 },
		//     new TreeNode { Value = 0, NextZeroIdx = 92, NextOneIdx = 95 },
		//     new TreeNode { Value = 0, NextZeroIdx = 93, NextOneIdx = 94 },
		//     new TreeNode { Value = 28, NextZeroIdx = 0, NextOneIdx = 0 },
		//     new TreeNode { Value = 29, NextZeroIdx = 0, NextOneIdx = 0 },
		//     new TreeNode { Value = 0, NextZeroIdx = 96, NextOneIdx = 97 },
		//     new TreeNode { Value = 30, NextZeroIdx = 0, NextOneIdx = 0 },
		//     new TreeNode { Value = 31, NextZeroIdx = 0, NextOneIdx = 0 },
		//     new TreeNode { Value = 0, NextZeroIdx = 133, NextOneIdx = 99 },
		//     new TreeNode { Value = 0, NextZeroIdx = 100, NextOneIdx = 129 },
		//     new TreeNode { Value = 0, NextZeroIdx = 258, NextOneIdx = 101 },
		//     new TreeNode { Value = 0, NextZeroIdx = 102, NextOneIdx = 126 },
		//     new TreeNode { Value = 0, NextZeroIdx = 103, NextOneIdx = 112 },
		//     new TreeNode { Value = 32, NextZeroIdx = 0, NextOneIdx = 0 },
		//     new TreeNode { Value = 0, NextZeroIdx = 105, NextOneIdx = 119 },
		//     new TreeNode { Value = 0, NextZeroIdx = 106, NextOneIdx = 107 },
		//     new TreeNode { Value = 33, NextZeroIdx = 0, NextOneIdx = 0 },
		//     new TreeNode { Value = 34, NextZeroIdx = 0, NextOneIdx = 0 },
		//     new TreeNode { Value = 0, NextZeroIdx = 271, NextOneIdx = 109 },
		//     new TreeNode { Value = 0, NextZeroIdx = 110, NextOneIdx = 164 },
		//     new TreeNode { Value = 35, NextZeroIdx = 0, NextOneIdx = 0 },
		//     new TreeNode { Value = 36, NextZeroIdx = 0, NextOneIdx = 0 },
		//     new TreeNode { Value = 37, NextZeroIdx = 0, NextOneIdx = 0 },
		//     new TreeNode { Value = 0, NextZeroIdx = 114, NextOneIdx = 124 },
		//     new TreeNode { Value = 0, NextZeroIdx = 115, NextOneIdx = 122 },
		//     new TreeNode { Value = 38, NextZeroIdx = 0, NextOneIdx = 0 },
		//     new TreeNode { Value = 0, NextZeroIdx = 165, NextOneIdx = 117 },
		//     new TreeNode { Value = 0, NextZeroIdx = 118, NextOneIdx = 123 },
		//     new TreeNode { Value = 39, NextZeroIdx = 0, NextOneIdx = 0 },
		//     new TreeNode { Value = 0, NextZeroIdx = 120, NextOneIdx = 121 },
		//     new TreeNode { Value = 40, NextZeroIdx = 0, NextOneIdx = 0 },
		//     new TreeNode { Value = 41, NextZeroIdx = 0, NextOneIdx = 0 },
		//     new TreeNode { Value = 42, NextZeroIdx = 0, NextOneIdx = 0 },
		//     new TreeNode { Value = 43, NextZeroIdx = 0, NextOneIdx = 0 },
		//     new TreeNode { Value = 0, NextZeroIdx = 125, NextOneIdx = 157 },
		//     new TreeNode { Value = 44, NextZeroIdx = 0, NextOneIdx = 0 },
		//     new TreeNode { Value = 0, NextZeroIdx = 127, NextOneIdx = 128 },
		//     new TreeNode { Value = 45, NextZeroIdx = 0, NextOneIdx = 0 },
		//     new TreeNode { Value = 46, NextZeroIdx = 0, NextOneIdx = 0 },
		//     new TreeNode { Value = 0, NextZeroIdx = 130, NextOneIdx = 144 },
		//     new TreeNode { Value = 0, NextZeroIdx = 131, NextOneIdx = 141 },
		//     new TreeNode { Value = 0, NextZeroIdx = 132, NextOneIdx = 140 },
		//     new TreeNode { Value = 47, NextZeroIdx = 0, NextOneIdx = 0 },
		//     new TreeNode { Value = 0, NextZeroIdx = 134, NextOneIdx = 229 },
		//     new TreeNode { Value = 0, NextZeroIdx = 135, NextOneIdx = 138 },
		//     new TreeNode { Value = 0, NextZeroIdx = 136, NextOneIdx = 137 },
		//     new TreeNode { Value = 48, NextZeroIdx = 0, NextOneIdx = 0 },
		//     new TreeNode { Value = 49, NextZeroIdx = 0, NextOneIdx = 0 },
		//     new TreeNode { Value = 0, NextZeroIdx = 139, NextOneIdx = 227 },
		//     new TreeNode { Value = 50, NextZeroIdx = 0, NextOneIdx = 0 },
		//     new TreeNode { Value = 51, NextZeroIdx = 0, NextOneIdx = 0 },
		//     new TreeNode { Value = 0, NextZeroIdx = 142, NextOneIdx = 143 },
		//     new TreeNode { Value = 52, NextZeroIdx = 0, NextOneIdx = 0 },
		//     new TreeNode { Value = 53, NextZeroIdx = 0, NextOneIdx = 0 },
		//     new TreeNode { Value = 0, NextZeroIdx = 145, NextOneIdx = 148 },
		//     new TreeNode { Value = 0, NextZeroIdx = 146, NextOneIdx = 147 },
		//     new TreeNode { Value = 54, NextZeroIdx = 0, NextOneIdx = 0 },
		//     new TreeNode { Value = 55, NextZeroIdx = 0, NextOneIdx = 0 },
		//     new TreeNode { Value = 0, NextZeroIdx = 149, NextOneIdx = 150 },
		//     new TreeNode { Value = 56, NextZeroIdx = 0, NextOneIdx = 0 },
		//     new TreeNode { Value = 57, NextZeroIdx = 0, NextOneIdx = 0 },
		//     new TreeNode { Value = 0, NextZeroIdx = 160, NextOneIdx = 152 },
		//     new TreeNode { Value = 0, NextZeroIdx = 246, NextOneIdx = 153 },
		//     new TreeNode { Value = 0, NextZeroIdx = 256, NextOneIdx = 154 },
		//     new TreeNode { Value = 0, NextZeroIdx = 155, NextOneIdx = 170 },
		//     new TreeNode { Value = 0, NextZeroIdx = 156, NextOneIdx = 169 },
		//     new TreeNode { Value = 58, NextZeroIdx = 0, NextOneIdx = 0 },
		//     new TreeNode { Value = 59, NextZeroIdx = 0, NextOneIdx = 0 },
		//     new TreeNode { Value = 0, NextZeroIdx = 159, NextOneIdx = 226 },
		//     new TreeNode { Value = 60, NextZeroIdx = 0, NextOneIdx = 0 },
		//     new TreeNode { Value = 0, NextZeroIdx = 161, NextOneIdx = 232 },
		//     new TreeNode { Value = 0, NextZeroIdx = 162, NextOneIdx = 224 },
		//     new TreeNode { Value = 0, NextZeroIdx = 163, NextOneIdx = 168 },
		//     new TreeNode { Value = 61, NextZeroIdx = 0, NextOneIdx = 0 },
		//     new TreeNode { Value = 62, NextZeroIdx = 0, NextOneIdx = 0 },
		//     new TreeNode { Value = 63, NextZeroIdx = 0, NextOneIdx = 0 },
		//     new TreeNode { Value = 0, NextZeroIdx = 167, NextOneIdx = 215 },
		//     new TreeNode { Value = 64, NextZeroIdx = 0, NextOneIdx = 0 },
		//     new TreeNode { Value = 65, NextZeroIdx = 0, NextOneIdx = 0 },
		//     new TreeNode { Value = 66, NextZeroIdx = 0, NextOneIdx = 0 },
		//     new TreeNode { Value = 0, NextZeroIdx = 171, NextOneIdx = 172 },
		//     new TreeNode { Value = 67, NextZeroIdx = 0, NextOneIdx = 0 },
		//     new TreeNode { Value = 68, NextZeroIdx = 0, NextOneIdx = 0 },
		//     new TreeNode { Value = 0, NextZeroIdx = 174, NextOneIdx = 189 },
		//     new TreeNode { Value = 0, NextZeroIdx = 175, NextOneIdx = 182 },
		//     new TreeNode { Value = 0, NextZeroIdx = 176, NextOneIdx = 179 },
		//     new TreeNode { Value = 0, NextZeroIdx = 177, NextOneIdx = 178 },
		//     new TreeNode { Value = 69, NextZeroIdx = 0, NextOneIdx = 0 },
		//     new TreeNode { Value = 70, NextZeroIdx = 0, NextOneIdx = 0 },
		//     new TreeNode { Value = 0, NextZeroIdx = 180, NextOneIdx = 181 },
		//     new TreeNode { Value = 71, NextZeroIdx = 0, NextOneIdx = 0 },
		//     new TreeNode { Value = 72, NextZeroIdx = 0, NextOneIdx = 0 },
		//     new TreeNode { Value = 0, NextZeroIdx = 183, NextOneIdx = 186 },
		//     new TreeNode { Value = 0, NextZeroIdx = 184, NextOneIdx = 185 },
		//     new TreeNode { Value = 73, NextZeroIdx = 0, NextOneIdx = 0 },
		//     new TreeNode { Value = 74, NextZeroIdx = 0, NextOneIdx = 0 },
		//     new TreeNode { Value = 0, NextZeroIdx = 187, NextOneIdx = 188 },
		//     new TreeNode { Value = 75, NextZeroIdx = 0, NextOneIdx = 0 },
		//     new TreeNode { Value = 76, NextZeroIdx = 0, NextOneIdx = 0 },
		//     new TreeNode { Value = 0, NextZeroIdx = 190, NextOneIdx = 197 },
		//     new TreeNode { Value = 0, NextZeroIdx = 191, NextOneIdx = 194 },
		//     new TreeNode { Value = 0, NextZeroIdx = 192, NextOneIdx = 193 },
		//     new TreeNode { Value = 77, NextZeroIdx = 0, NextOneIdx = 0 },
		//     new TreeNode { Value = 78, NextZeroIdx = 0, NextOneIdx = 0 },
		//     new TreeNode { Value = 0, NextZeroIdx = 195, NextOneIdx = 196 },
		//     new TreeNode { Value = 79, NextZeroIdx = 0, NextOneIdx = 0 },
		//     new TreeNode { Value = 80, NextZeroIdx = 0, NextOneIdx = 0 },
		//     new TreeNode { Value = 0, NextZeroIdx = 198, NextOneIdx = 201 },
		//     new TreeNode { Value = 0, NextZeroIdx = 199, NextOneIdx = 200 },
		//     new TreeNode { Value = 81, NextZeroIdx = 0, NextOneIdx = 0 },
		//     new TreeNode { Value = 82, NextZeroIdx = 0, NextOneIdx = 0 },
		//     new TreeNode { Value = 0, NextZeroIdx = 202, NextOneIdx = 203 },
		//     new TreeNode { Value = 83, NextZeroIdx = 0, NextOneIdx = 0 },
		//     new TreeNode { Value = 84, NextZeroIdx = 0, NextOneIdx = 0 },
		//     new TreeNode { Value = 0, NextZeroIdx = 205, NextOneIdx = 242 },
		//     new TreeNode { Value = 0, NextZeroIdx = 206, NextOneIdx = 209 },
		//     new TreeNode { Value = 0, NextZeroIdx = 207, NextOneIdx = 208 },
		//     new TreeNode { Value = 85, NextZeroIdx = 0, NextOneIdx = 0 },
		//     new TreeNode { Value = 86, NextZeroIdx = 0, NextOneIdx = 0 },
		//     new TreeNode { Value = 0, NextZeroIdx = 210, NextOneIdx = 213 },
		//     new TreeNode { Value = 87, NextZeroIdx = 0, NextOneIdx = 0 },
		//     new TreeNode { Value = 0, NextZeroIdx = 212, NextOneIdx = 214 },
		//     new TreeNode { Value = 88, NextZeroIdx = 0, NextOneIdx = 0 },
		//     new TreeNode { Value = 89, NextZeroIdx = 0, NextOneIdx = 0 },
		//     new TreeNode { Value = 90, NextZeroIdx = 0, NextOneIdx = 0 },
		//     new TreeNode { Value = 91, NextZeroIdx = 0, NextOneIdx = 0 },
		//     new TreeNode { Value = 0, NextZeroIdx = 217, NextOneIdx = 286 },
		//     new TreeNode { Value = 0, NextZeroIdx = 218, NextOneIdx = 276 },
		//     new TreeNode { Value = 0, NextZeroIdx = 219, NextOneIdx = 410 },
		//     new TreeNode { Value = 92, NextZeroIdx = 0, NextOneIdx = 0 },
		//     new TreeNode { Value = 0, NextZeroIdx = 221, NextOneIdx = 273 },
		//     new TreeNode { Value = 93, NextZeroIdx = 0, NextOneIdx = 0 },
		//     new TreeNode { Value = 0, NextZeroIdx = 223, NextOneIdx = 272 },
		//     new TreeNode { Value = 94, NextZeroIdx = 0, NextOneIdx = 0 },
		//     new TreeNode { Value = 0, NextZeroIdx = 225, NextOneIdx = 228 },
		//     new TreeNode { Value = 95, NextZeroIdx = 0, NextOneIdx = 0 },
		//     new TreeNode { Value = 96, NextZeroIdx = 0, NextOneIdx = 0 },
		//     new TreeNode { Value = 97, NextZeroIdx = 0, NextOneIdx = 0 },
		//     new TreeNode { Value = 98, NextZeroIdx = 0, NextOneIdx = 0 },
		//     new TreeNode { Value = 0, NextZeroIdx = 230, NextOneIdx = 240 },
		//     new TreeNode { Value = 0, NextZeroIdx = 231, NextOneIdx = 235 },
		//     new TreeNode { Value = 99, NextZeroIdx = 0, NextOneIdx = 0 },
		//     new TreeNode { Value = 0, NextZeroIdx = 233, NextOneIdx = 237 },
		//     new TreeNode { Value = 0, NextZeroIdx = 234, NextOneIdx = 236 },
		//     new TreeNode { Value = 100, NextZeroIdx = 0, NextOneIdx = 0 },
		//     new TreeNode { Value = 101, NextZeroIdx = 0, NextOneIdx = 0 },
		//     new TreeNode { Value = 102, NextZeroIdx = 0, NextOneIdx = 0 },
		//     new TreeNode { Value = 0, NextZeroIdx = 238, NextOneIdx = 239 },
		//     new TreeNode { Value = 103, NextZeroIdx = 0, NextOneIdx = 0 },
		//     new TreeNode { Value = 104, NextZeroIdx = 0, NextOneIdx = 0 },
		//     new TreeNode { Value = 0, NextZeroIdx = 241, NextOneIdx = 252 },
		//     new TreeNode { Value = 105, NextZeroIdx = 0, NextOneIdx = 0 },
		//     new TreeNode { Value = 0, NextZeroIdx = 243, NextOneIdx = 254 },
		//     new TreeNode { Value = 0, NextZeroIdx = 244, NextOneIdx = 245 },
		//     new TreeNode { Value = 106, NextZeroIdx = 0, NextOneIdx = 0 },
		//     new TreeNode { Value = 107, NextZeroIdx = 0, NextOneIdx = 0 },
		//     new TreeNode { Value = 0, NextZeroIdx = 247, NextOneIdx = 250 },
		//     new TreeNode { Value = 0, NextZeroIdx = 248, NextOneIdx = 249 },
		//     new TreeNode { Value = 108, NextZeroIdx = 0, NextOneIdx = 0 },
		//     new TreeNode { Value = 109, NextZeroIdx = 0, NextOneIdx = 0 },
		//     new TreeNode { Value = 0, NextZeroIdx = 251, NextOneIdx = 253 },
		//     new TreeNode { Value = 110, NextZeroIdx = 0, NextOneIdx = 0 },
		//     new TreeNode { Value = 111, NextZeroIdx = 0, NextOneIdx = 0 },
		//     new TreeNode { Value = 112, NextZeroIdx = 0, NextOneIdx = 0 },
		//     new TreeNode { Value = 0, NextZeroIdx = 255, NextOneIdx = 262 },
		//     new TreeNode { Value = 113, NextZeroIdx = 0, NextOneIdx = 0 },
		//     new TreeNode { Value = 0, NextZeroIdx = 257, NextOneIdx = 261 },
		//     new TreeNode { Value = 114, NextZeroIdx = 0, NextOneIdx = 0 },
		//     new TreeNode { Value = 0, NextZeroIdx = 259, NextOneIdx = 260 },
		//     new TreeNode { Value = 115, NextZeroIdx = 0, NextOneIdx = 0 },
		//     new TreeNode { Value = 116, NextZeroIdx = 0, NextOneIdx = 0 },
		//     new TreeNode { Value = 117, NextZeroIdx = 0, NextOneIdx = 0 },
		//     new TreeNode { Value = 118, NextZeroIdx = 0, NextOneIdx = 0 },
		//     new TreeNode { Value = 0, NextZeroIdx = 264, NextOneIdx = 267 },
		//     new TreeNode { Value = 0, NextZeroIdx = 265, NextOneIdx = 266 },
		//     new TreeNode { Value = 119, NextZeroIdx = 0, NextOneIdx = 0 },
		//     new TreeNode { Value = 120, NextZeroIdx = 0, NextOneIdx = 0 },
		//     new TreeNode { Value = 0, NextZeroIdx = 268, NextOneIdx = 269 },
		//     new TreeNode { Value = 121, NextZeroIdx = 0, NextOneIdx = 0 },
		//     new TreeNode { Value = 122, NextZeroIdx = 0, NextOneIdx = 0 },
		//     new TreeNode { Value = 123, NextZeroIdx = 0, NextOneIdx = 0 },
		//     new TreeNode { Value = 124, NextZeroIdx = 0, NextOneIdx = 0 },
		//     new TreeNode { Value = 125, NextZeroIdx = 0, NextOneIdx = 0 },
		//     new TreeNode { Value = 126, NextZeroIdx = 0, NextOneIdx = 0 },
		//     new TreeNode { Value = 0, NextZeroIdx = 275, NextOneIdx = 459 },
		//     new TreeNode { Value = 127, NextZeroIdx = 0, NextOneIdx = 0 },
		//     new TreeNode { Value = 0, NextZeroIdx = 436, NextOneIdx = 277 },
		//     new TreeNode { Value = 0, NextZeroIdx = 278, NextOneIdx = 285 },
		//     new TreeNode { Value = 128, NextZeroIdx = 0, NextOneIdx = 0 },
		//     new TreeNode { Value = 0, NextZeroIdx = 372, NextOneIdx = 280 },
		//     new TreeNode { Value = 0, NextZeroIdx = 281, NextOneIdx = 332 },
		//     new TreeNode { Value = 0, NextZeroIdx = 282, NextOneIdx = 291 },
		//     new TreeNode { Value = 0, NextZeroIdx = 473, NextOneIdx = 283 },
		//     new TreeNode { Value = 0, NextZeroIdx = 284, NextOneIdx = 290 },
		//     new TreeNode { Value = 129, NextZeroIdx = 0, NextOneIdx = 0 },
		//     new TreeNode { Value = 130, NextZeroIdx = 0, NextOneIdx = 0 },
		//     new TreeNode { Value = 0, NextZeroIdx = 287, NextOneIdx = 328 },
		//     new TreeNode { Value = 0, NextZeroIdx = 288, NextOneIdx = 388 },
		//     new TreeNode { Value = 0, NextZeroIdx = 289, NextOneIdx = 345 },
		//     new TreeNode { Value = 131, NextZeroIdx = 0, NextOneIdx = 0 },
		//     new TreeNode { Value = 132, NextZeroIdx = 0, NextOneIdx = 0 },
		//     new TreeNode { Value = 0, NextZeroIdx = 292, NextOneIdx = 296 },
		//     new TreeNode { Value = 0, NextZeroIdx = 293, NextOneIdx = 294 },
		//     new TreeNode { Value = 133, NextZeroIdx = 0, NextOneIdx = 0 },
		//     new TreeNode { Value = 134, NextZeroIdx = 0, NextOneIdx = 0 },
		//     new TreeNode { Value = 135, NextZeroIdx = 0, NextOneIdx = 0 },
		//     new TreeNode { Value = 0, NextZeroIdx = 297, NextOneIdx = 313 },
		//     new TreeNode { Value = 136, NextZeroIdx = 0, NextOneIdx = 0 },
		//     new TreeNode { Value = 0, NextZeroIdx = 299, NextOneIdx = 300 },
		//     new TreeNode { Value = 137, NextZeroIdx = 0, NextOneIdx = 0 },
		//     new TreeNode { Value = 138, NextZeroIdx = 0, NextOneIdx = 0 },
		//     new TreeNode { Value = 0, NextZeroIdx = 302, NextOneIdx = 305 },
		//     new TreeNode { Value = 0, NextZeroIdx = 303, NextOneIdx = 304 },
		//     new TreeNode { Value = 139, NextZeroIdx = 0, NextOneIdx = 0 },
		//     new TreeNode { Value = 140, NextZeroIdx = 0, NextOneIdx = 0 },
		//     new TreeNode { Value = 0, NextZeroIdx = 306, NextOneIdx = 308 },
		//     new TreeNode { Value = 141, NextZeroIdx = 0, NextOneIdx = 0 },
		//     new TreeNode { Value = 142, NextZeroIdx = 0, NextOneIdx = 0 },
		//     new TreeNode { Value = 143, NextZeroIdx = 0, NextOneIdx = 0 },
		//     new TreeNode { Value = 0, NextZeroIdx = 310, NextOneIdx = 319 },
		//     new TreeNode { Value = 0, NextZeroIdx = 311, NextOneIdx = 312 },
		//     new TreeNode { Value = 144, NextZeroIdx = 0, NextOneIdx = 0 },
		//     new TreeNode { Value = 145, NextZeroIdx = 0, NextOneIdx = 0 },
		//     new TreeNode { Value = 146, NextZeroIdx = 0, NextOneIdx = 0 },
		//     new TreeNode { Value = 0, NextZeroIdx = 315, NextOneIdx = 350 },
		//     new TreeNode { Value = 0, NextZeroIdx = 316, NextOneIdx = 325 },
		//     new TreeNode { Value = 0, NextZeroIdx = 317, NextOneIdx = 322 },
		//     new TreeNode { Value = 0, NextZeroIdx = 318, NextOneIdx = 321 },
		//     new TreeNode { Value = 147, NextZeroIdx = 0, NextOneIdx = 0 },
		//     new TreeNode { Value = 0, NextZeroIdx = 320, NextOneIdx = 341 },
		//     new TreeNode { Value = 148, NextZeroIdx = 0, NextOneIdx = 0 },
		//     new TreeNode { Value = 149, NextZeroIdx = 0, NextOneIdx = 0 },
		//     new TreeNode { Value = 0, NextZeroIdx = 323, NextOneIdx = 324 },
		//     new TreeNode { Value = 150, NextZeroIdx = 0, NextOneIdx = 0 },
		//     new TreeNode { Value = 151, NextZeroIdx = 0, NextOneIdx = 0 },
		//     new TreeNode { Value = 0, NextZeroIdx = 326, NextOneIdx = 338 },
		//     new TreeNode { Value = 0, NextZeroIdx = 327, NextOneIdx = 336 },
		//     new TreeNode { Value = 152, NextZeroIdx = 0, NextOneIdx = 0 },
		//     new TreeNode { Value = 0, NextZeroIdx = 465, NextOneIdx = 329 },
		//     new TreeNode { Value = 0, NextZeroIdx = 330, NextOneIdx = 355 },
		//     new TreeNode { Value = 0, NextZeroIdx = 331, NextOneIdx = 344 },
		//     new TreeNode { Value = 153, NextZeroIdx = 0, NextOneIdx = 0 },
		//     new TreeNode { Value = 0, NextZeroIdx = 333, NextOneIdx = 347 },
		//     new TreeNode { Value = 0, NextZeroIdx = 334, NextOneIdx = 342 },
		//     new TreeNode { Value = 0, NextZeroIdx = 335, NextOneIdx = 337 },
		//     new TreeNode { Value = 154, NextZeroIdx = 0, NextOneIdx = 0 },
		//     new TreeNode { Value = 155, NextZeroIdx = 0, NextOneIdx = 0 },
		//     new TreeNode { Value = 156, NextZeroIdx = 0, NextOneIdx = 0 },
		//     new TreeNode { Value = 0, NextZeroIdx = 339, NextOneIdx = 340 },
		//     new TreeNode { Value = 157, NextZeroIdx = 0, NextOneIdx = 0 },
		//     new TreeNode { Value = 158, NextZeroIdx = 0, NextOneIdx = 0 },
		//     new TreeNode { Value = 159, NextZeroIdx = 0, NextOneIdx = 0 },
		//     new TreeNode { Value = 0, NextZeroIdx = 343, NextOneIdx = 346 },
		//     new TreeNode { Value = 160, NextZeroIdx = 0, NextOneIdx = 0 },
		//     new TreeNode { Value = 161, NextZeroIdx = 0, NextOneIdx = 0 },
		//     new TreeNode { Value = 162, NextZeroIdx = 0, NextOneIdx = 0 },
		//     new TreeNode { Value = 163, NextZeroIdx = 0, NextOneIdx = 0 },
		//     new TreeNode { Value = 0, NextZeroIdx = 348, NextOneIdx = 360 },
		//     new TreeNode { Value = 0, NextZeroIdx = 349, NextOneIdx = 359 },
		//     new TreeNode { Value = 164, NextZeroIdx = 0, NextOneIdx = 0 },
		//     new TreeNode { Value = 0, NextZeroIdx = 351, NextOneIdx = 369 },
		//     new TreeNode { Value = 0, NextZeroIdx = 352, NextOneIdx = 357 },
		//     new TreeNode { Value = 0, NextZeroIdx = 353, NextOneIdx = 354 },
		//     new TreeNode { Value = 165, NextZeroIdx = 0, NextOneIdx = 0 },
		//     new TreeNode { Value = 166, NextZeroIdx = 0, NextOneIdx = 0 },
		//     new TreeNode { Value = 0, NextZeroIdx = 356, NextOneIdx = 366 },
		//     new TreeNode { Value = 167, NextZeroIdx = 0, NextOneIdx = 0 },
		//     new TreeNode { Value = 0, NextZeroIdx = 358, NextOneIdx = 368 },
		//     new TreeNode { Value = 168, NextZeroIdx = 0, NextOneIdx = 0 },
		//     new TreeNode { Value = 169, NextZeroIdx = 0, NextOneIdx = 0 },
		//     new TreeNode { Value = 0, NextZeroIdx = 361, NextOneIdx = 367 },
		//     new TreeNode { Value = 170, NextZeroIdx = 0, NextOneIdx = 0 },
		//     new TreeNode { Value = 0, NextZeroIdx = 363, NextOneIdx = 417 },
		//     new TreeNode { Value = 0, NextZeroIdx = 364, NextOneIdx = 449 },
		//     new TreeNode { Value = 0, NextZeroIdx = 365, NextOneIdx = 434 },
		//     new TreeNode { Value = 171, NextZeroIdx = 0, NextOneIdx = 0 },
		//     new TreeNode { Value = 172, NextZeroIdx = 0, NextOneIdx = 0 },
		//     new TreeNode { Value = 173, NextZeroIdx = 0, NextOneIdx = 0 },
		//     new TreeNode { Value = 174, NextZeroIdx = 0, NextOneIdx = 0 },
		//     new TreeNode { Value = 0, NextZeroIdx = 370, NextOneIdx = 385 },
		//     new TreeNode { Value = 0, NextZeroIdx = 371, NextOneIdx = 383 },
		//     new TreeNode { Value = 175, NextZeroIdx = 0, NextOneIdx = 0 },
		//     new TreeNode { Value = 0, NextZeroIdx = 373, NextOneIdx = 451 },
		//     new TreeNode { Value = 0, NextZeroIdx = 374, NextOneIdx = 381 },
		//     new TreeNode { Value = 0, NextZeroIdx = 375, NextOneIdx = 376 },
		//     new TreeNode { Value = 176, NextZeroIdx = 0, NextOneIdx = 0 },
		//     new TreeNode { Value = 177, NextZeroIdx = 0, NextOneIdx = 0 },
		//     new TreeNode { Value = 0, NextZeroIdx = 378, NextOneIdx = 393 },
		//     new TreeNode { Value = 0, NextZeroIdx = 379, NextOneIdx = 390 },
		//     new TreeNode { Value = 0, NextZeroIdx = 380, NextOneIdx = 384 },
		//     new TreeNode { Value = 178, NextZeroIdx = 0, NextOneIdx = 0 },
		//     new TreeNode { Value = 0, NextZeroIdx = 382, NextOneIdx = 437 },
		//     new TreeNode { Value = 179, NextZeroIdx = 0, NextOneIdx = 0 },
		//     new TreeNode { Value = 180, NextZeroIdx = 0, NextOneIdx = 0 },
		//     new TreeNode { Value = 181, NextZeroIdx = 0, NextOneIdx = 0 },
		//     new TreeNode { Value = 0, NextZeroIdx = 386, NextOneIdx = 387 },
		//     new TreeNode { Value = 182, NextZeroIdx = 0, NextOneIdx = 0 },
		//     new TreeNode { Value = 183, NextZeroIdx = 0, NextOneIdx = 0 },
		//     new TreeNode { Value = 0, NextZeroIdx = 389, NextOneIdx = 409 },
		//     new TreeNode { Value = 184, NextZeroIdx = 0, NextOneIdx = 0 },
		//     new TreeNode { Value = 0, NextZeroIdx = 391, NextOneIdx = 392 },
		//     new TreeNode { Value = 185, NextZeroIdx = 0, NextOneIdx = 0 },
		//     new TreeNode { Value = 186, NextZeroIdx = 0, NextOneIdx = 0 },
		//     new TreeNode { Value = 0, NextZeroIdx = 394, NextOneIdx = 400 },
		//     new TreeNode { Value = 0, NextZeroIdx = 395, NextOneIdx = 399 },
		//     new TreeNode { Value = 187, NextZeroIdx = 0, NextOneIdx = 0 },
		//     new TreeNode { Value = 0, NextZeroIdx = 397, NextOneIdx = 412 },
		//     new TreeNode { Value = 0, NextZeroIdx = 398, NextOneIdx = 402 },
		//     new TreeNode { Value = 188, NextZeroIdx = 0, NextOneIdx = 0 },
		//     new TreeNode { Value = 189, NextZeroIdx = 0, NextOneIdx = 0 },
		//     new TreeNode { Value = 0, NextZeroIdx = 401, NextOneIdx = 411 },
		//     new TreeNode { Value = 190, NextZeroIdx = 0, NextOneIdx = 0 },
		//     new TreeNode { Value = 191, NextZeroIdx = 0, NextOneIdx = 0 },
		//     new TreeNode { Value = 0, NextZeroIdx = 404, NextOneIdx = 427 },
		//     new TreeNode { Value = 0, NextZeroIdx = 405, NextOneIdx = 424 },
		//     new TreeNode { Value = 0, NextZeroIdx = 406, NextOneIdx = 421 },
		//     new TreeNode { Value = 0, NextZeroIdx = 407, NextOneIdx = 408 },
		//     new TreeNode { Value = 192, NextZeroIdx = 0, NextOneIdx = 0 },
		//     new TreeNode { Value = 193, NextZeroIdx = 0, NextOneIdx = 0 },
		//     new TreeNode { Value = 194, NextZeroIdx = 0, NextOneIdx = 0 },
		//     new TreeNode { Value = 195, NextZeroIdx = 0, NextOneIdx = 0 },
		//     new TreeNode { Value = 196, NextZeroIdx = 0, NextOneIdx = 0 },
		//     new TreeNode { Value = 0, NextZeroIdx = 413, NextOneIdx = 474 },
		//     new TreeNode { Value = 197, NextZeroIdx = 0, NextOneIdx = 0 },
		//     new TreeNode { Value = 0, NextZeroIdx = 415, NextOneIdx = 475 },
		//     new TreeNode { Value = 0, NextZeroIdx = 416, NextOneIdx = 471 },
		//     new TreeNode { Value = 198, NextZeroIdx = 0, NextOneIdx = 0 },
		//     new TreeNode { Value = 0, NextZeroIdx = 481, NextOneIdx = 418 },
		//     new TreeNode { Value = 0, NextZeroIdx = 419, NextOneIdx = 478 },
		//     new TreeNode { Value = 0, NextZeroIdx = 420, NextOneIdx = 435 },
		//     new TreeNode { Value = 199, NextZeroIdx = 0, NextOneIdx = 0 },
		//     new TreeNode { Value = 0, NextZeroIdx = 422, NextOneIdx = 423 },
		//     new TreeNode { Value = 200, NextZeroIdx = 0, NextOneIdx = 0 },
		//     new TreeNode { Value = 201, NextZeroIdx = 0, NextOneIdx = 0 },
		//     new TreeNode { Value = 0, NextZeroIdx = 425, NextOneIdx = 438 },
		//     new TreeNode { Value = 0, NextZeroIdx = 426, NextOneIdx = 433 },
		//     new TreeNode { Value = 202, NextZeroIdx = 0, NextOneIdx = 0 },
		//     new TreeNode { Value = 0, NextZeroIdx = 455, NextOneIdx = 428 },
		//     new TreeNode { Value = 0, NextZeroIdx = 490, NextOneIdx = 429 },
		//     new TreeNode { Value = 0, NextZeroIdx = 511, NextOneIdx = 430 },
		//     new TreeNode { Value = 0, NextZeroIdx = 431, NextOneIdx = 432 },
		//     new TreeNode { Value = 203, NextZeroIdx = 0, NextOneIdx = 0 },
		//     new TreeNode { Value = 204, NextZeroIdx = 0, NextOneIdx = 0 },
		//     new TreeNode { Value = 205, NextZeroIdx = 0, NextOneIdx = 0 },
		//     new TreeNode { Value = 206, NextZeroIdx = 0, NextOneIdx = 0 },
		//     new TreeNode { Value = 207, NextZeroIdx = 0, NextOneIdx = 0 },
		//     new TreeNode { Value = 208, NextZeroIdx = 0, NextOneIdx = 0 },
		//     new TreeNode { Value = 209, NextZeroIdx = 0, NextOneIdx = 0 },
		//     new TreeNode { Value = 0, NextZeroIdx = 439, NextOneIdx = 446 },
		//     new TreeNode { Value = 210, NextZeroIdx = 0, NextOneIdx = 0 },
		//     new TreeNode { Value = 0, NextZeroIdx = 441, NextOneIdx = 494 },
		//     new TreeNode { Value = 0, NextZeroIdx = 442, NextOneIdx = 461 },
		//     new TreeNode { Value = 0, NextZeroIdx = 443, NextOneIdx = 447 },
		//     new TreeNode { Value = 0, NextZeroIdx = 444, NextOneIdx = 445 },
		//     new TreeNode { Value = 211, NextZeroIdx = 0, NextOneIdx = 0 },
		//     new TreeNode { Value = 212, NextZeroIdx = 0, NextOneIdx = 0 },
		//     new TreeNode { Value = 213, NextZeroIdx = 0, NextOneIdx = 0 },
		//     new TreeNode { Value = 0, NextZeroIdx = 448, NextOneIdx = 460 },
		//     new TreeNode { Value = 214, NextZeroIdx = 0, NextOneIdx = 0 },
		//     new TreeNode { Value = 0, NextZeroIdx = 450, NextOneIdx = 467 },
		//     new TreeNode { Value = 215, NextZeroIdx = 0, NextOneIdx = 0 },
		//     new TreeNode { Value = 0, NextZeroIdx = 452, NextOneIdx = 469 },
		//     new TreeNode { Value = 0, NextZeroIdx = 453, NextOneIdx = 454 },
		//     new TreeNode { Value = 216, NextZeroIdx = 0, NextOneIdx = 0 },
		//     new TreeNode { Value = 217, NextZeroIdx = 0, NextOneIdx = 0 },
		//     new TreeNode { Value = 0, NextZeroIdx = 456, NextOneIdx = 484 },
		//     new TreeNode { Value = 0, NextZeroIdx = 457, NextOneIdx = 458 },
		//     new TreeNode { Value = 218, NextZeroIdx = 0, NextOneIdx = 0 },
		//     new TreeNode { Value = 219, NextZeroIdx = 0, NextOneIdx = 0 },
		//     new TreeNode { Value = 220, NextZeroIdx = 0, NextOneIdx = 0 },
		//     new TreeNode { Value = 221, NextZeroIdx = 0, NextOneIdx = 0 },
		//     new TreeNode { Value = 0, NextZeroIdx = 462, NextOneIdx = 488 },
		//     new TreeNode { Value = 0, NextZeroIdx = 463, NextOneIdx = 464 },
		//     new TreeNode { Value = 222, NextZeroIdx = 0, NextOneIdx = 0 },
		//     new TreeNode { Value = 223, NextZeroIdx = 0, NextOneIdx = 0 },
		//     new TreeNode { Value = 0, NextZeroIdx = 466, NextOneIdx = 468 },
		//     new TreeNode { Value = 224, NextZeroIdx = 0, NextOneIdx = 0 },
		//     new TreeNode { Value = 225, NextZeroIdx = 0, NextOneIdx = 0 },
		//     new TreeNode { Value = 226, NextZeroIdx = 0, NextOneIdx = 0 },
		//     new TreeNode { Value = 0, NextZeroIdx = 470, NextOneIdx = 472 },
		//     new TreeNode { Value = 227, NextZeroIdx = 0, NextOneIdx = 0 },
		//     new TreeNode { Value = 228, NextZeroIdx = 0, NextOneIdx = 0 },
		//     new TreeNode { Value = 229, NextZeroIdx = 0, NextOneIdx = 0 },
		//     new TreeNode { Value = 230, NextZeroIdx = 0, NextOneIdx = 0 },
		//     new TreeNode { Value = 231, NextZeroIdx = 0, NextOneIdx = 0 },
		//     new TreeNode { Value = 0, NextZeroIdx = 476, NextOneIdx = 477 },
		//     new TreeNode { Value = 232, NextZeroIdx = 0, NextOneIdx = 0 },
		//     new TreeNode { Value = 233, NextZeroIdx = 0, NextOneIdx = 0 },
		//     new TreeNode { Value = 0, NextZeroIdx = 479, NextOneIdx = 480 },
		//     new TreeNode { Value = 234, NextZeroIdx = 0, NextOneIdx = 0 },
		//     new TreeNode { Value = 235, NextZeroIdx = 0, NextOneIdx = 0 },
		//     new TreeNode { Value = 0, NextZeroIdx = 482, NextOneIdx = 483 },
		//     new TreeNode { Value = 236, NextZeroIdx = 0, NextOneIdx = 0 },
		//     new TreeNode { Value = 237, NextZeroIdx = 0, NextOneIdx = 0 },
		//     new TreeNode { Value = 0, NextZeroIdx = 485, NextOneIdx = 487 },
		//     new TreeNode { Value = 238, NextZeroIdx = 0, NextOneIdx = 0 },
		//     new TreeNode { Value = 239, NextZeroIdx = 0, NextOneIdx = 0 },
		//     new TreeNode { Value = 240, NextZeroIdx = 0, NextOneIdx = 0 },
		//     new TreeNode { Value = 0, NextZeroIdx = 489, NextOneIdx = 493 },
		//     new TreeNode { Value = 241, NextZeroIdx = 0, NextOneIdx = 0 },
		//     new TreeNode { Value = 0, NextZeroIdx = 491, NextOneIdx = 492 },
		//     new TreeNode { Value = 242, NextZeroIdx = 0, NextOneIdx = 0 },
		//     new TreeNode { Value = 243, NextZeroIdx = 0, NextOneIdx = 0 },
		//     new TreeNode { Value = 244, NextZeroIdx = 0, NextOneIdx = 0 },
		//     new TreeNode { Value = 0, NextZeroIdx = 495, NextOneIdx = 503 },
		//     new TreeNode { Value = 0, NextZeroIdx = 496, NextOneIdx = 499 },
		//     new TreeNode { Value = 0, NextZeroIdx = 497, NextOneIdx = 498 },
		//     new TreeNode { Value = 245, NextZeroIdx = 0, NextOneIdx = 0 },
		//     new TreeNode { Value = 246, NextZeroIdx = 0, NextOneIdx = 0 },
		//     new TreeNode { Value = 0, NextZeroIdx = 500, NextOneIdx = 501 },
		//     new TreeNode { Value = 247, NextZeroIdx = 0, NextOneIdx = 0 },
		//     new TreeNode { Value = 248, NextZeroIdx = 0, NextOneIdx = 0 },
		//     new TreeNode { Value = 249, NextZeroIdx = 0, NextOneIdx = 0 },
		//     new TreeNode { Value = 0, NextZeroIdx = 504, NextOneIdx = 507 },
		//     new TreeNode { Value = 0, NextZeroIdx = 505, NextOneIdx = 506 },
		//     new TreeNode { Value = 250, NextZeroIdx = 0, NextOneIdx = 0 },
		//     new TreeNode { Value = 251, NextZeroIdx = 0, NextOneIdx = 0 },
		//     new TreeNode { Value = 0, NextZeroIdx = 508, NextOneIdx = 509 },
		//     new TreeNode { Value = 252, NextZeroIdx = 0, NextOneIdx = 0 },
		//     new TreeNode { Value = 253, NextZeroIdx = 0, NextOneIdx = 0 },
		//     new TreeNode { Value = 254, NextZeroIdx = 0, NextOneIdx = 0 },
		//     new TreeNode { Value = 255, NextZeroIdx = 0, NextOneIdx = 0 },
		//     new TreeNode { Value = 256, NextZeroIdx = 0, NextOneIdx = 0 }
		// };
		TreeNodeU5BU5D_t5032BD830284F31E0A94DEB0072DB2B07126BABD* L_515 = (TreeNodeU5BU5D_t5032BD830284F31E0A94DEB0072DB2B07126BABD*)(TreeNodeU5BU5D_t5032BD830284F31E0A94DEB0072DB2B07126BABD*)SZArrayNew(TreeNodeU5BU5D_t5032BD830284F31E0A94DEB0072DB2B07126BABD_il2cpp_TypeInfo_var, (uint32_t)((int32_t)513));
		TreeNodeU5BU5D_t5032BD830284F31E0A94DEB0072DB2B07126BABD* L_516 = L_515;
		il2cpp_codegen_initobj((&V_1), sizeof(TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F ));
		(&V_1)->set_Value_2((uint16_t)0);
		(&V_1)->set_NextZeroIdx_0((uint16_t)((int32_t)98));
		(&V_1)->set_NextOneIdx_1((uint16_t)1);
		TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F  L_517 = V_1;
		NullCheck(L_516);
		(L_516)->SetAt(static_cast<il2cpp_array_size_t>(0), (TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F )L_517);
		TreeNodeU5BU5D_t5032BD830284F31E0A94DEB0072DB2B07126BABD* L_518 = L_516;
		il2cpp_codegen_initobj((&V_1), sizeof(TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F ));
		(&V_1)->set_Value_2((uint16_t)0);
		(&V_1)->set_NextZeroIdx_0((uint16_t)((int32_t)151));
		(&V_1)->set_NextOneIdx_1((uint16_t)2);
		TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F  L_519 = V_1;
		NullCheck(L_518);
		(L_518)->SetAt(static_cast<il2cpp_array_size_t>(1), (TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F )L_519);
		TreeNodeU5BU5D_t5032BD830284F31E0A94DEB0072DB2B07126BABD* L_520 = L_518;
		il2cpp_codegen_initobj((&V_1), sizeof(TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F ));
		(&V_1)->set_Value_2((uint16_t)0);
		(&V_1)->set_NextZeroIdx_0((uint16_t)((int32_t)173));
		(&V_1)->set_NextOneIdx_1((uint16_t)3);
		TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F  L_521 = V_1;
		NullCheck(L_520);
		(L_520)->SetAt(static_cast<il2cpp_array_size_t>(2), (TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F )L_521);
		TreeNodeU5BU5D_t5032BD830284F31E0A94DEB0072DB2B07126BABD* L_522 = L_520;
		il2cpp_codegen_initobj((&V_1), sizeof(TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F ));
		(&V_1)->set_Value_2((uint16_t)0);
		(&V_1)->set_NextZeroIdx_0((uint16_t)((int32_t)204));
		(&V_1)->set_NextOneIdx_1((uint16_t)4);
		TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F  L_523 = V_1;
		NullCheck(L_522);
		(L_522)->SetAt(static_cast<il2cpp_array_size_t>(3), (TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F )L_523);
		TreeNodeU5BU5D_t5032BD830284F31E0A94DEB0072DB2B07126BABD* L_524 = L_522;
		il2cpp_codegen_initobj((&V_1), sizeof(TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F ));
		(&V_1)->set_Value_2((uint16_t)0);
		(&V_1)->set_NextZeroIdx_0((uint16_t)((int32_t)263));
		(&V_1)->set_NextOneIdx_1((uint16_t)5);
		TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F  L_525 = V_1;
		NullCheck(L_524);
		(L_524)->SetAt(static_cast<il2cpp_array_size_t>(4), (TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F )L_525);
		TreeNodeU5BU5D_t5032BD830284F31E0A94DEB0072DB2B07126BABD* L_526 = L_524;
		il2cpp_codegen_initobj((&V_1), sizeof(TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F ));
		(&V_1)->set_Value_2((uint16_t)0);
		(&V_1)->set_NextZeroIdx_0((uint16_t)((int32_t)113));
		(&V_1)->set_NextOneIdx_1((uint16_t)6);
		TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F  L_527 = V_1;
		NullCheck(L_526);
		(L_526)->SetAt(static_cast<il2cpp_array_size_t>(5), (TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F )L_527);
		TreeNodeU5BU5D_t5032BD830284F31E0A94DEB0072DB2B07126BABD* L_528 = L_526;
		il2cpp_codegen_initobj((&V_1), sizeof(TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F ));
		(&V_1)->set_Value_2((uint16_t)0);
		(&V_1)->set_NextZeroIdx_0((uint16_t)((int32_t)211));
		(&V_1)->set_NextOneIdx_1((uint16_t)7);
		TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F  L_529 = V_1;
		NullCheck(L_528);
		(L_528)->SetAt(static_cast<il2cpp_array_size_t>(6), (TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F )L_529);
		TreeNodeU5BU5D_t5032BD830284F31E0A94DEB0072DB2B07126BABD* L_530 = L_528;
		il2cpp_codegen_initobj((&V_1), sizeof(TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F ));
		(&V_1)->set_Value_2((uint16_t)0);
		(&V_1)->set_NextZeroIdx_0((uint16_t)((int32_t)104));
		(&V_1)->set_NextOneIdx_1((uint16_t)8);
		TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F  L_531 = V_1;
		NullCheck(L_530);
		(L_530)->SetAt(static_cast<il2cpp_array_size_t>(7), (TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F )L_531);
		TreeNodeU5BU5D_t5032BD830284F31E0A94DEB0072DB2B07126BABD* L_532 = L_530;
		il2cpp_codegen_initobj((&V_1), sizeof(TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F ));
		(&V_1)->set_Value_2((uint16_t)0);
		(&V_1)->set_NextZeroIdx_0((uint16_t)((int32_t)116));
		(&V_1)->set_NextOneIdx_1((uint16_t)((int32_t)9));
		TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F  L_533 = V_1;
		NullCheck(L_532);
		(L_532)->SetAt(static_cast<il2cpp_array_size_t>(8), (TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F )L_533);
		TreeNodeU5BU5D_t5032BD830284F31E0A94DEB0072DB2B07126BABD* L_534 = L_532;
		il2cpp_codegen_initobj((&V_1), sizeof(TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F ));
		(&V_1)->set_Value_2((uint16_t)0);
		(&V_1)->set_NextZeroIdx_0((uint16_t)((int32_t)108));
		(&V_1)->set_NextOneIdx_1((uint16_t)((int32_t)10));
		TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F  L_535 = V_1;
		NullCheck(L_534);
		(L_534)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)9)), (TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F )L_535);
		TreeNodeU5BU5D_t5032BD830284F31E0A94DEB0072DB2B07126BABD* L_536 = L_534;
		il2cpp_codegen_initobj((&V_1), sizeof(TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F ));
		(&V_1)->set_Value_2((uint16_t)0);
		(&V_1)->set_NextZeroIdx_0((uint16_t)((int32_t)11));
		(&V_1)->set_NextOneIdx_1((uint16_t)((int32_t)14));
		TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F  L_537 = V_1;
		NullCheck(L_536);
		(L_536)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)10)), (TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F )L_537);
		TreeNodeU5BU5D_t5032BD830284F31E0A94DEB0072DB2B07126BABD* L_538 = L_536;
		il2cpp_codegen_initobj((&V_1), sizeof(TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F ));
		(&V_1)->set_Value_2((uint16_t)0);
		(&V_1)->set_NextZeroIdx_0((uint16_t)((int32_t)12));
		(&V_1)->set_NextOneIdx_1((uint16_t)((int32_t)166));
		TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F  L_539 = V_1;
		NullCheck(L_538);
		(L_538)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)11)), (TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F )L_539);
		TreeNodeU5BU5D_t5032BD830284F31E0A94DEB0072DB2B07126BABD* L_540 = L_538;
		il2cpp_codegen_initobj((&V_1), sizeof(TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F ));
		(&V_1)->set_Value_2((uint16_t)0);
		(&V_1)->set_NextZeroIdx_0((uint16_t)((int32_t)13));
		(&V_1)->set_NextOneIdx_1((uint16_t)((int32_t)111));
		TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F  L_541 = V_1;
		NullCheck(L_540);
		(L_540)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)12)), (TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F )L_541);
		TreeNodeU5BU5D_t5032BD830284F31E0A94DEB0072DB2B07126BABD* L_542 = L_540;
		il2cpp_codegen_initobj((&V_1), sizeof(TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F ));
		(&V_1)->set_Value_2((uint16_t)0);
		(&V_1)->set_NextZeroIdx_0((uint16_t)0);
		(&V_1)->set_NextOneIdx_1((uint16_t)0);
		TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F  L_543 = V_1;
		NullCheck(L_542);
		(L_542)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)13)), (TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F )L_543);
		TreeNodeU5BU5D_t5032BD830284F31E0A94DEB0072DB2B07126BABD* L_544 = L_542;
		il2cpp_codegen_initobj((&V_1), sizeof(TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F ));
		(&V_1)->set_Value_2((uint16_t)0);
		(&V_1)->set_NextZeroIdx_0((uint16_t)((int32_t)220));
		(&V_1)->set_NextOneIdx_1((uint16_t)((int32_t)15));
		TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F  L_545 = V_1;
		NullCheck(L_544);
		(L_544)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)14)), (TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F )L_545);
		TreeNodeU5BU5D_t5032BD830284F31E0A94DEB0072DB2B07126BABD* L_546 = L_544;
		il2cpp_codegen_initobj((&V_1), sizeof(TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F ));
		(&V_1)->set_Value_2((uint16_t)0);
		(&V_1)->set_NextZeroIdx_0((uint16_t)((int32_t)222));
		(&V_1)->set_NextOneIdx_1((uint16_t)((int32_t)16));
		TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F  L_547 = V_1;
		NullCheck(L_546);
		(L_546)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)15)), (TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F )L_547);
		TreeNodeU5BU5D_t5032BD830284F31E0A94DEB0072DB2B07126BABD* L_548 = L_546;
		il2cpp_codegen_initobj((&V_1), sizeof(TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F ));
		(&V_1)->set_Value_2((uint16_t)0);
		(&V_1)->set_NextZeroIdx_0((uint16_t)((int32_t)158));
		(&V_1)->set_NextOneIdx_1((uint16_t)((int32_t)17));
		TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F  L_549 = V_1;
		NullCheck(L_548);
		(L_548)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)16)), (TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F )L_549);
		TreeNodeU5BU5D_t5032BD830284F31E0A94DEB0072DB2B07126BABD* L_550 = L_548;
		il2cpp_codegen_initobj((&V_1), sizeof(TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F ));
		(&V_1)->set_Value_2((uint16_t)0);
		(&V_1)->set_NextZeroIdx_0((uint16_t)((int32_t)270));
		(&V_1)->set_NextOneIdx_1((uint16_t)((int32_t)18));
		TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F  L_551 = V_1;
		NullCheck(L_550);
		(L_550)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)17)), (TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F )L_551);
		TreeNodeU5BU5D_t5032BD830284F31E0A94DEB0072DB2B07126BABD* L_552 = L_550;
		il2cpp_codegen_initobj((&V_1), sizeof(TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F ));
		(&V_1)->set_Value_2((uint16_t)0);
		(&V_1)->set_NextZeroIdx_0((uint16_t)((int32_t)216));
		(&V_1)->set_NextOneIdx_1((uint16_t)((int32_t)19));
		TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F  L_553 = V_1;
		NullCheck(L_552);
		(L_552)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)18)), (TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F )L_553);
		TreeNodeU5BU5D_t5032BD830284F31E0A94DEB0072DB2B07126BABD* L_554 = L_552;
		il2cpp_codegen_initobj((&V_1), sizeof(TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F ));
		(&V_1)->set_Value_2((uint16_t)0);
		(&V_1)->set_NextZeroIdx_0((uint16_t)((int32_t)279));
		(&V_1)->set_NextOneIdx_1((uint16_t)((int32_t)20));
		TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F  L_555 = V_1;
		NullCheck(L_554);
		(L_554)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)19)), (TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F )L_555);
		TreeNodeU5BU5D_t5032BD830284F31E0A94DEB0072DB2B07126BABD* L_556 = L_554;
		il2cpp_codegen_initobj((&V_1), sizeof(TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F ));
		(&V_1)->set_Value_2((uint16_t)0);
		(&V_1)->set_NextZeroIdx_0((uint16_t)((int32_t)21));
		(&V_1)->set_NextOneIdx_1((uint16_t)((int32_t)27));
		TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F  L_557 = V_1;
		NullCheck(L_556);
		(L_556)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)20)), (TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F )L_557);
		TreeNodeU5BU5D_t5032BD830284F31E0A94DEB0072DB2B07126BABD* L_558 = L_556;
		il2cpp_codegen_initobj((&V_1), sizeof(TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F ));
		(&V_1)->set_Value_2((uint16_t)0);
		(&V_1)->set_NextZeroIdx_0((uint16_t)((int32_t)377));
		(&V_1)->set_NextOneIdx_1((uint16_t)((int32_t)22));
		TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F  L_559 = V_1;
		NullCheck(L_558);
		(L_558)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)21)), (TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F )L_559);
		TreeNodeU5BU5D_t5032BD830284F31E0A94DEB0072DB2B07126BABD* L_560 = L_558;
		il2cpp_codegen_initobj((&V_1), sizeof(TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F ));
		(&V_1)->set_Value_2((uint16_t)0);
		(&V_1)->set_NextZeroIdx_0((uint16_t)((int32_t)414));
		(&V_1)->set_NextOneIdx_1((uint16_t)((int32_t)23));
		TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F  L_561 = V_1;
		NullCheck(L_560);
		(L_560)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)22)), (TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F )L_561);
		TreeNodeU5BU5D_t5032BD830284F31E0A94DEB0072DB2B07126BABD* L_562 = L_560;
		il2cpp_codegen_initobj((&V_1), sizeof(TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F ));
		(&V_1)->set_Value_2((uint16_t)0);
		(&V_1)->set_NextZeroIdx_0((uint16_t)((int32_t)24));
		(&V_1)->set_NextOneIdx_1((uint16_t)((int32_t)301));
		TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F  L_563 = V_1;
		NullCheck(L_562);
		(L_562)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)23)), (TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F )L_563);
		TreeNodeU5BU5D_t5032BD830284F31E0A94DEB0072DB2B07126BABD* L_564 = L_562;
		il2cpp_codegen_initobj((&V_1), sizeof(TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F ));
		(&V_1)->set_Value_2((uint16_t)0);
		(&V_1)->set_NextZeroIdx_0((uint16_t)((int32_t)25));
		(&V_1)->set_NextOneIdx_1((uint16_t)((int32_t)298));
		TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F  L_565 = V_1;
		NullCheck(L_564);
		(L_564)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)24)), (TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F )L_565);
		TreeNodeU5BU5D_t5032BD830284F31E0A94DEB0072DB2B07126BABD* L_566 = L_564;
		il2cpp_codegen_initobj((&V_1), sizeof(TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F ));
		(&V_1)->set_Value_2((uint16_t)0);
		(&V_1)->set_NextZeroIdx_0((uint16_t)((int32_t)26));
		(&V_1)->set_NextOneIdx_1((uint16_t)((int32_t)295));
		TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F  L_567 = V_1;
		NullCheck(L_566);
		(L_566)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)25)), (TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F )L_567);
		TreeNodeU5BU5D_t5032BD830284F31E0A94DEB0072DB2B07126BABD* L_568 = L_566;
		il2cpp_codegen_initobj((&V_1), sizeof(TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F ));
		(&V_1)->set_Value_2((uint16_t)1);
		(&V_1)->set_NextZeroIdx_0((uint16_t)0);
		(&V_1)->set_NextOneIdx_1((uint16_t)0);
		TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F  L_569 = V_1;
		NullCheck(L_568);
		(L_568)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)26)), (TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F )L_569);
		TreeNodeU5BU5D_t5032BD830284F31E0A94DEB0072DB2B07126BABD* L_570 = L_568;
		il2cpp_codegen_initobj((&V_1), sizeof(TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F ));
		(&V_1)->set_Value_2((uint16_t)0);
		(&V_1)->set_NextZeroIdx_0((uint16_t)((int32_t)314));
		(&V_1)->set_NextOneIdx_1((uint16_t)((int32_t)28));
		TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F  L_571 = V_1;
		NullCheck(L_570);
		(L_570)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)27)), (TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F )L_571);
		TreeNodeU5BU5D_t5032BD830284F31E0A94DEB0072DB2B07126BABD* L_572 = L_570;
		il2cpp_codegen_initobj((&V_1), sizeof(TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F ));
		(&V_1)->set_Value_2((uint16_t)0);
		(&V_1)->set_NextZeroIdx_0((uint16_t)((int32_t)50));
		(&V_1)->set_NextOneIdx_1((uint16_t)((int32_t)29));
		TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F  L_573 = V_1;
		NullCheck(L_572);
		(L_572)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)28)), (TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F )L_573);
		TreeNodeU5BU5D_t5032BD830284F31E0A94DEB0072DB2B07126BABD* L_574 = L_572;
		il2cpp_codegen_initobj((&V_1), sizeof(TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F ));
		(&V_1)->set_Value_2((uint16_t)0);
		(&V_1)->set_NextZeroIdx_0((uint16_t)((int32_t)362));
		(&V_1)->set_NextOneIdx_1((uint16_t)((int32_t)30));
		TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F  L_575 = V_1;
		NullCheck(L_574);
		(L_574)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)29)), (TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F )L_575);
		TreeNodeU5BU5D_t5032BD830284F31E0A94DEB0072DB2B07126BABD* L_576 = L_574;
		il2cpp_codegen_initobj((&V_1), sizeof(TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F ));
		(&V_1)->set_Value_2((uint16_t)0);
		(&V_1)->set_NextZeroIdx_0((uint16_t)((int32_t)403));
		(&V_1)->set_NextOneIdx_1((uint16_t)((int32_t)31));
		TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F  L_577 = V_1;
		NullCheck(L_576);
		(L_576)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)30)), (TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F )L_577);
		TreeNodeU5BU5D_t5032BD830284F31E0A94DEB0072DB2B07126BABD* L_578 = L_576;
		il2cpp_codegen_initobj((&V_1), sizeof(TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F ));
		(&V_1)->set_Value_2((uint16_t)0);
		(&V_1)->set_NextZeroIdx_0((uint16_t)((int32_t)440));
		(&V_1)->set_NextOneIdx_1((uint16_t)((int32_t)32));
		TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F  L_579 = V_1;
		NullCheck(L_578);
		(L_578)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)31)), (TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F )L_579);
		TreeNodeU5BU5D_t5032BD830284F31E0A94DEB0072DB2B07126BABD* L_580 = L_578;
		il2cpp_codegen_initobj((&V_1), sizeof(TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F ));
		(&V_1)->set_Value_2((uint16_t)0);
		(&V_1)->set_NextZeroIdx_0((uint16_t)((int32_t)33));
		(&V_1)->set_NextOneIdx_1((uint16_t)((int32_t)55));
		TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F  L_581 = V_1;
		NullCheck(L_580);
		(L_580)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)32)), (TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F )L_581);
		TreeNodeU5BU5D_t5032BD830284F31E0A94DEB0072DB2B07126BABD* L_582 = L_580;
		il2cpp_codegen_initobj((&V_1), sizeof(TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F ));
		(&V_1)->set_Value_2((uint16_t)0);
		(&V_1)->set_NextZeroIdx_0((uint16_t)((int32_t)34));
		(&V_1)->set_NextOneIdx_1((uint16_t)((int32_t)46));
		TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F  L_583 = V_1;
		NullCheck(L_582);
		(L_582)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)33)), (TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F )L_583);
		TreeNodeU5BU5D_t5032BD830284F31E0A94DEB0072DB2B07126BABD* L_584 = L_582;
		il2cpp_codegen_initobj((&V_1), sizeof(TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F ));
		(&V_1)->set_Value_2((uint16_t)0);
		(&V_1)->set_NextZeroIdx_0((uint16_t)((int32_t)35));
		(&V_1)->set_NextOneIdx_1((uint16_t)((int32_t)39));
		TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F  L_585 = V_1;
		NullCheck(L_584);
		(L_584)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)34)), (TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F )L_585);
		TreeNodeU5BU5D_t5032BD830284F31E0A94DEB0072DB2B07126BABD* L_586 = L_584;
		il2cpp_codegen_initobj((&V_1), sizeof(TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F ));
		(&V_1)->set_Value_2((uint16_t)0);
		(&V_1)->set_NextZeroIdx_0((uint16_t)((int32_t)510));
		(&V_1)->set_NextOneIdx_1((uint16_t)((int32_t)36));
		TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F  L_587 = V_1;
		NullCheck(L_586);
		(L_586)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)35)), (TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F )L_587);
		TreeNodeU5BU5D_t5032BD830284F31E0A94DEB0072DB2B07126BABD* L_588 = L_586;
		il2cpp_codegen_initobj((&V_1), sizeof(TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F ));
		(&V_1)->set_Value_2((uint16_t)0);
		(&V_1)->set_NextZeroIdx_0((uint16_t)((int32_t)37));
		(&V_1)->set_NextOneIdx_1((uint16_t)((int32_t)38));
		TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F  L_589 = V_1;
		NullCheck(L_588);
		(L_588)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)36)), (TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F )L_589);
		TreeNodeU5BU5D_t5032BD830284F31E0A94DEB0072DB2B07126BABD* L_590 = L_588;
		il2cpp_codegen_initobj((&V_1), sizeof(TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F ));
		(&V_1)->set_Value_2((uint16_t)2);
		(&V_1)->set_NextZeroIdx_0((uint16_t)0);
		(&V_1)->set_NextOneIdx_1((uint16_t)0);
		TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F  L_591 = V_1;
		NullCheck(L_590);
		(L_590)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)37)), (TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F )L_591);
		TreeNodeU5BU5D_t5032BD830284F31E0A94DEB0072DB2B07126BABD* L_592 = L_590;
		il2cpp_codegen_initobj((&V_1), sizeof(TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F ));
		(&V_1)->set_Value_2((uint16_t)3);
		(&V_1)->set_NextZeroIdx_0((uint16_t)0);
		(&V_1)->set_NextOneIdx_1((uint16_t)0);
		TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F  L_593 = V_1;
		NullCheck(L_592);
		(L_592)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)38)), (TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F )L_593);
		TreeNodeU5BU5D_t5032BD830284F31E0A94DEB0072DB2B07126BABD* L_594 = L_592;
		il2cpp_codegen_initobj((&V_1), sizeof(TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F ));
		(&V_1)->set_Value_2((uint16_t)0);
		(&V_1)->set_NextZeroIdx_0((uint16_t)((int32_t)40));
		(&V_1)->set_NextOneIdx_1((uint16_t)((int32_t)43));
		TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F  L_595 = V_1;
		NullCheck(L_594);
		(L_594)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)39)), (TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F )L_595);
		TreeNodeU5BU5D_t5032BD830284F31E0A94DEB0072DB2B07126BABD* L_596 = L_594;
		il2cpp_codegen_initobj((&V_1), sizeof(TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F ));
		(&V_1)->set_Value_2((uint16_t)0);
		(&V_1)->set_NextZeroIdx_0((uint16_t)((int32_t)41));
		(&V_1)->set_NextOneIdx_1((uint16_t)((int32_t)42));
		TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F  L_597 = V_1;
		NullCheck(L_596);
		(L_596)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)40)), (TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F )L_597);
		TreeNodeU5BU5D_t5032BD830284F31E0A94DEB0072DB2B07126BABD* L_598 = L_596;
		il2cpp_codegen_initobj((&V_1), sizeof(TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F ));
		(&V_1)->set_Value_2((uint16_t)4);
		(&V_1)->set_NextZeroIdx_0((uint16_t)0);
		(&V_1)->set_NextOneIdx_1((uint16_t)0);
		TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F  L_599 = V_1;
		NullCheck(L_598);
		(L_598)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)41)), (TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F )L_599);
		TreeNodeU5BU5D_t5032BD830284F31E0A94DEB0072DB2B07126BABD* L_600 = L_598;
		il2cpp_codegen_initobj((&V_1), sizeof(TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F ));
		(&V_1)->set_Value_2((uint16_t)5);
		(&V_1)->set_NextZeroIdx_0((uint16_t)0);
		(&V_1)->set_NextOneIdx_1((uint16_t)0);
		TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F  L_601 = V_1;
		NullCheck(L_600);
		(L_600)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)42)), (TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F )L_601);
		TreeNodeU5BU5D_t5032BD830284F31E0A94DEB0072DB2B07126BABD* L_602 = L_600;
		il2cpp_codegen_initobj((&V_1), sizeof(TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F ));
		(&V_1)->set_Value_2((uint16_t)0);
		(&V_1)->set_NextZeroIdx_0((uint16_t)((int32_t)44));
		(&V_1)->set_NextOneIdx_1((uint16_t)((int32_t)45));
		TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F  L_603 = V_1;
		NullCheck(L_602);
		(L_602)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)43)), (TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F )L_603);
		TreeNodeU5BU5D_t5032BD830284F31E0A94DEB0072DB2B07126BABD* L_604 = L_602;
		il2cpp_codegen_initobj((&V_1), sizeof(TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F ));
		(&V_1)->set_Value_2((uint16_t)6);
		(&V_1)->set_NextZeroIdx_0((uint16_t)0);
		(&V_1)->set_NextOneIdx_1((uint16_t)0);
		TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F  L_605 = V_1;
		NullCheck(L_604);
		(L_604)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)44)), (TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F )L_605);
		TreeNodeU5BU5D_t5032BD830284F31E0A94DEB0072DB2B07126BABD* L_606 = L_604;
		il2cpp_codegen_initobj((&V_1), sizeof(TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F ));
		(&V_1)->set_Value_2((uint16_t)7);
		(&V_1)->set_NextZeroIdx_0((uint16_t)0);
		(&V_1)->set_NextOneIdx_1((uint16_t)0);
		TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F  L_607 = V_1;
		NullCheck(L_606);
		(L_606)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)45)), (TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F )L_607);
		TreeNodeU5BU5D_t5032BD830284F31E0A94DEB0072DB2B07126BABD* L_608 = L_606;
		il2cpp_codegen_initobj((&V_1), sizeof(TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F ));
		(&V_1)->set_Value_2((uint16_t)0);
		(&V_1)->set_NextZeroIdx_0((uint16_t)((int32_t)47));
		(&V_1)->set_NextOneIdx_1((uint16_t)((int32_t)67));
		TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F  L_609 = V_1;
		NullCheck(L_608);
		(L_608)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)46)), (TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F )L_609);
		TreeNodeU5BU5D_t5032BD830284F31E0A94DEB0072DB2B07126BABD* L_610 = L_608;
		il2cpp_codegen_initobj((&V_1), sizeof(TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F ));
		(&V_1)->set_Value_2((uint16_t)0);
		(&V_1)->set_NextZeroIdx_0((uint16_t)((int32_t)48));
		(&V_1)->set_NextOneIdx_1((uint16_t)((int32_t)63));
		TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F  L_611 = V_1;
		NullCheck(L_610);
		(L_610)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)47)), (TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F )L_611);
		TreeNodeU5BU5D_t5032BD830284F31E0A94DEB0072DB2B07126BABD* L_612 = L_610;
		il2cpp_codegen_initobj((&V_1), sizeof(TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F ));
		(&V_1)->set_Value_2((uint16_t)0);
		(&V_1)->set_NextZeroIdx_0((uint16_t)((int32_t)49));
		(&V_1)->set_NextOneIdx_1((uint16_t)((int32_t)62));
		TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F  L_613 = V_1;
		NullCheck(L_612);
		(L_612)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)48)), (TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F )L_613);
		TreeNodeU5BU5D_t5032BD830284F31E0A94DEB0072DB2B07126BABD* L_614 = L_612;
		il2cpp_codegen_initobj((&V_1), sizeof(TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F ));
		(&V_1)->set_Value_2((uint16_t)8);
		(&V_1)->set_NextZeroIdx_0((uint16_t)0);
		(&V_1)->set_NextOneIdx_1((uint16_t)0);
		TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F  L_615 = V_1;
		NullCheck(L_614);
		(L_614)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)49)), (TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F )L_615);
		TreeNodeU5BU5D_t5032BD830284F31E0A94DEB0072DB2B07126BABD* L_616 = L_614;
		il2cpp_codegen_initobj((&V_1), sizeof(TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F ));
		(&V_1)->set_Value_2((uint16_t)0);
		(&V_1)->set_NextZeroIdx_0((uint16_t)((int32_t)396));
		(&V_1)->set_NextOneIdx_1((uint16_t)((int32_t)51));
		TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F  L_617 = V_1;
		NullCheck(L_616);
		(L_616)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)50)), (TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F )L_617);
		TreeNodeU5BU5D_t5032BD830284F31E0A94DEB0072DB2B07126BABD* L_618 = L_616;
		il2cpp_codegen_initobj((&V_1), sizeof(TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F ));
		(&V_1)->set_Value_2((uint16_t)0);
		(&V_1)->set_NextZeroIdx_0((uint16_t)((int32_t)52));
		(&V_1)->set_NextOneIdx_1((uint16_t)((int32_t)309));
		TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F  L_619 = V_1;
		NullCheck(L_618);
		(L_618)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)51)), (TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F )L_619);
		TreeNodeU5BU5D_t5032BD830284F31E0A94DEB0072DB2B07126BABD* L_620 = L_618;
		il2cpp_codegen_initobj((&V_1), sizeof(TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F ));
		(&V_1)->set_Value_2((uint16_t)0);
		(&V_1)->set_NextZeroIdx_0((uint16_t)((int32_t)486));
		(&V_1)->set_NextOneIdx_1((uint16_t)((int32_t)53));
		TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F  L_621 = V_1;
		NullCheck(L_620);
		(L_620)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)52)), (TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F )L_621);
		TreeNodeU5BU5D_t5032BD830284F31E0A94DEB0072DB2B07126BABD* L_622 = L_620;
		il2cpp_codegen_initobj((&V_1), sizeof(TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F ));
		(&V_1)->set_Value_2((uint16_t)0);
		(&V_1)->set_NextZeroIdx_0((uint16_t)((int32_t)54));
		(&V_1)->set_NextOneIdx_1((uint16_t)((int32_t)307));
		TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F  L_623 = V_1;
		NullCheck(L_622);
		(L_622)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)53)), (TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F )L_623);
		TreeNodeU5BU5D_t5032BD830284F31E0A94DEB0072DB2B07126BABD* L_624 = L_622;
		il2cpp_codegen_initobj((&V_1), sizeof(TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F ));
		(&V_1)->set_Value_2((uint16_t)((int32_t)9));
		(&V_1)->set_NextZeroIdx_0((uint16_t)0);
		(&V_1)->set_NextOneIdx_1((uint16_t)0);
		TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F  L_625 = V_1;
		NullCheck(L_624);
		(L_624)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)54)), (TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F )L_625);
		TreeNodeU5BU5D_t5032BD830284F31E0A94DEB0072DB2B07126BABD* L_626 = L_624;
		il2cpp_codegen_initobj((&V_1), sizeof(TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F ));
		(&V_1)->set_Value_2((uint16_t)0);
		(&V_1)->set_NextZeroIdx_0((uint16_t)((int32_t)74));
		(&V_1)->set_NextOneIdx_1((uint16_t)((int32_t)56));
		TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F  L_627 = V_1;
		NullCheck(L_626);
		(L_626)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)55)), (TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F )L_627);
		TreeNodeU5BU5D_t5032BD830284F31E0A94DEB0072DB2B07126BABD* L_628 = L_626;
		il2cpp_codegen_initobj((&V_1), sizeof(TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F ));
		(&V_1)->set_Value_2((uint16_t)0);
		(&V_1)->set_NextZeroIdx_0((uint16_t)((int32_t)91));
		(&V_1)->set_NextOneIdx_1((uint16_t)((int32_t)57));
		TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F  L_629 = V_1;
		NullCheck(L_628);
		(L_628)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)56)), (TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F )L_629);
		TreeNodeU5BU5D_t5032BD830284F31E0A94DEB0072DB2B07126BABD* L_630 = L_628;
		il2cpp_codegen_initobj((&V_1), sizeof(TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F ));
		(&V_1)->set_Value_2((uint16_t)0);
		(&V_1)->set_NextZeroIdx_0((uint16_t)((int32_t)274));
		(&V_1)->set_NextOneIdx_1((uint16_t)((int32_t)58));
		TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F  L_631 = V_1;
		NullCheck(L_630);
		(L_630)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)57)), (TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F )L_631);
		TreeNodeU5BU5D_t5032BD830284F31E0A94DEB0072DB2B07126BABD* L_632 = L_630;
		il2cpp_codegen_initobj((&V_1), sizeof(TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F ));
		(&V_1)->set_Value_2((uint16_t)0);
		(&V_1)->set_NextZeroIdx_0((uint16_t)((int32_t)502));
		(&V_1)->set_NextOneIdx_1((uint16_t)((int32_t)59));
		TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F  L_633 = V_1;
		NullCheck(L_632);
		(L_632)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)58)), (TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F )L_633);
		TreeNodeU5BU5D_t5032BD830284F31E0A94DEB0072DB2B07126BABD* L_634 = L_632;
		il2cpp_codegen_initobj((&V_1), sizeof(TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F ));
		(&V_1)->set_Value_2((uint16_t)0);
		(&V_1)->set_NextZeroIdx_0((uint16_t)((int32_t)60));
		(&V_1)->set_NextOneIdx_1((uint16_t)((int32_t)81));
		TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F  L_635 = V_1;
		NullCheck(L_634);
		(L_634)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)59)), (TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F )L_635);
		TreeNodeU5BU5D_t5032BD830284F31E0A94DEB0072DB2B07126BABD* L_636 = L_634;
		il2cpp_codegen_initobj((&V_1), sizeof(TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F ));
		(&V_1)->set_Value_2((uint16_t)0);
		(&V_1)->set_NextZeroIdx_0((uint16_t)((int32_t)61));
		(&V_1)->set_NextOneIdx_1((uint16_t)((int32_t)65));
		TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F  L_637 = V_1;
		NullCheck(L_636);
		(L_636)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)60)), (TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F )L_637);
		TreeNodeU5BU5D_t5032BD830284F31E0A94DEB0072DB2B07126BABD* L_638 = L_636;
		il2cpp_codegen_initobj((&V_1), sizeof(TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F ));
		(&V_1)->set_Value_2((uint16_t)((int32_t)10));
		(&V_1)->set_NextZeroIdx_0((uint16_t)0);
		(&V_1)->set_NextOneIdx_1((uint16_t)0);
		TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F  L_639 = V_1;
		NullCheck(L_638);
		(L_638)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)61)), (TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F )L_639);
		TreeNodeU5BU5D_t5032BD830284F31E0A94DEB0072DB2B07126BABD* L_640 = L_638;
		il2cpp_codegen_initobj((&V_1), sizeof(TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F ));
		(&V_1)->set_Value_2((uint16_t)((int32_t)11));
		(&V_1)->set_NextZeroIdx_0((uint16_t)0);
		(&V_1)->set_NextOneIdx_1((uint16_t)0);
		TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F  L_641 = V_1;
		NullCheck(L_640);
		(L_640)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)62)), (TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F )L_641);
		TreeNodeU5BU5D_t5032BD830284F31E0A94DEB0072DB2B07126BABD* L_642 = L_640;
		il2cpp_codegen_initobj((&V_1), sizeof(TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F ));
		(&V_1)->set_Value_2((uint16_t)0);
		(&V_1)->set_NextZeroIdx_0((uint16_t)((int32_t)64));
		(&V_1)->set_NextOneIdx_1((uint16_t)((int32_t)66));
		TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F  L_643 = V_1;
		NullCheck(L_642);
		(L_642)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)63)), (TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F )L_643);
		TreeNodeU5BU5D_t5032BD830284F31E0A94DEB0072DB2B07126BABD* L_644 = L_642;
		il2cpp_codegen_initobj((&V_1), sizeof(TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F ));
		(&V_1)->set_Value_2((uint16_t)((int32_t)12));
		(&V_1)->set_NextZeroIdx_0((uint16_t)0);
		(&V_1)->set_NextOneIdx_1((uint16_t)0);
		TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F  L_645 = V_1;
		NullCheck(L_644);
		(L_644)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)64)), (TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F )L_645);
		TreeNodeU5BU5D_t5032BD830284F31E0A94DEB0072DB2B07126BABD* L_646 = L_644;
		il2cpp_codegen_initobj((&V_1), sizeof(TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F ));
		(&V_1)->set_Value_2((uint16_t)((int32_t)13));
		(&V_1)->set_NextZeroIdx_0((uint16_t)0);
		(&V_1)->set_NextOneIdx_1((uint16_t)0);
		TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F  L_647 = V_1;
		NullCheck(L_646);
		(L_646)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)65)), (TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F )L_647);
		TreeNodeU5BU5D_t5032BD830284F31E0A94DEB0072DB2B07126BABD* L_648 = L_646;
		il2cpp_codegen_initobj((&V_1), sizeof(TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F ));
		(&V_1)->set_Value_2((uint16_t)((int32_t)14));
		(&V_1)->set_NextZeroIdx_0((uint16_t)0);
		(&V_1)->set_NextOneIdx_1((uint16_t)0);
		TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F  L_649 = V_1;
		NullCheck(L_648);
		(L_648)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)66)), (TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F )L_649);
		TreeNodeU5BU5D_t5032BD830284F31E0A94DEB0072DB2B07126BABD* L_650 = L_648;
		il2cpp_codegen_initobj((&V_1), sizeof(TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F ));
		(&V_1)->set_Value_2((uint16_t)0);
		(&V_1)->set_NextZeroIdx_0((uint16_t)((int32_t)68));
		(&V_1)->set_NextOneIdx_1((uint16_t)((int32_t)71));
		TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F  L_651 = V_1;
		NullCheck(L_650);
		(L_650)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)67)), (TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F )L_651);
		TreeNodeU5BU5D_t5032BD830284F31E0A94DEB0072DB2B07126BABD* L_652 = L_650;
		il2cpp_codegen_initobj((&V_1), sizeof(TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F ));
		(&V_1)->set_Value_2((uint16_t)0);
		(&V_1)->set_NextZeroIdx_0((uint16_t)((int32_t)69));
		(&V_1)->set_NextOneIdx_1((uint16_t)((int32_t)70));
		TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F  L_653 = V_1;
		NullCheck(L_652);
		(L_652)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)68)), (TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F )L_653);
		TreeNodeU5BU5D_t5032BD830284F31E0A94DEB0072DB2B07126BABD* L_654 = L_652;
		il2cpp_codegen_initobj((&V_1), sizeof(TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F ));
		(&V_1)->set_Value_2((uint16_t)((int32_t)15));
		(&V_1)->set_NextZeroIdx_0((uint16_t)0);
		(&V_1)->set_NextOneIdx_1((uint16_t)0);
		TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F  L_655 = V_1;
		NullCheck(L_654);
		(L_654)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)69)), (TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F )L_655);
		TreeNodeU5BU5D_t5032BD830284F31E0A94DEB0072DB2B07126BABD* L_656 = L_654;
		il2cpp_codegen_initobj((&V_1), sizeof(TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F ));
		(&V_1)->set_Value_2((uint16_t)((int32_t)16));
		(&V_1)->set_NextZeroIdx_0((uint16_t)0);
		(&V_1)->set_NextOneIdx_1((uint16_t)0);
		TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F  L_657 = V_1;
		NullCheck(L_656);
		(L_656)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)70)), (TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F )L_657);
		TreeNodeU5BU5D_t5032BD830284F31E0A94DEB0072DB2B07126BABD* L_658 = L_656;
		il2cpp_codegen_initobj((&V_1), sizeof(TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F ));
		(&V_1)->set_Value_2((uint16_t)0);
		(&V_1)->set_NextZeroIdx_0((uint16_t)((int32_t)72));
		(&V_1)->set_NextOneIdx_1((uint16_t)((int32_t)73));
		TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F  L_659 = V_1;
		NullCheck(L_658);
		(L_658)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)71)), (TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F )L_659);
		TreeNodeU5BU5D_t5032BD830284F31E0A94DEB0072DB2B07126BABD* L_660 = L_658;
		il2cpp_codegen_initobj((&V_1), sizeof(TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F ));
		(&V_1)->set_Value_2((uint16_t)((int32_t)17));
		(&V_1)->set_NextZeroIdx_0((uint16_t)0);
		(&V_1)->set_NextOneIdx_1((uint16_t)0);
		TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F  L_661 = V_1;
		NullCheck(L_660);
		(L_660)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)72)), (TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F )L_661);
		TreeNodeU5BU5D_t5032BD830284F31E0A94DEB0072DB2B07126BABD* L_662 = L_660;
		il2cpp_codegen_initobj((&V_1), sizeof(TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F ));
		(&V_1)->set_Value_2((uint16_t)((int32_t)18));
		(&V_1)->set_NextZeroIdx_0((uint16_t)0);
		(&V_1)->set_NextOneIdx_1((uint16_t)0);
		TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F  L_663 = V_1;
		NullCheck(L_662);
		(L_662)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)73)), (TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F )L_663);
		TreeNodeU5BU5D_t5032BD830284F31E0A94DEB0072DB2B07126BABD* L_664 = L_662;
		il2cpp_codegen_initobj((&V_1), sizeof(TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F ));
		(&V_1)->set_Value_2((uint16_t)0);
		(&V_1)->set_NextZeroIdx_0((uint16_t)((int32_t)75));
		(&V_1)->set_NextOneIdx_1((uint16_t)((int32_t)84));
		TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F  L_665 = V_1;
		NullCheck(L_664);
		(L_664)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)74)), (TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F )L_665);
		TreeNodeU5BU5D_t5032BD830284F31E0A94DEB0072DB2B07126BABD* L_666 = L_664;
		il2cpp_codegen_initobj((&V_1), sizeof(TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F ));
		(&V_1)->set_Value_2((uint16_t)0);
		(&V_1)->set_NextZeroIdx_0((uint16_t)((int32_t)76));
		(&V_1)->set_NextOneIdx_1((uint16_t)((int32_t)79));
		TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F  L_667 = V_1;
		NullCheck(L_666);
		(L_666)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)75)), (TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F )L_667);
		TreeNodeU5BU5D_t5032BD830284F31E0A94DEB0072DB2B07126BABD* L_668 = L_666;
		il2cpp_codegen_initobj((&V_1), sizeof(TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F ));
		(&V_1)->set_Value_2((uint16_t)0);
		(&V_1)->set_NextZeroIdx_0((uint16_t)((int32_t)77));
		(&V_1)->set_NextOneIdx_1((uint16_t)((int32_t)78));
		TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F  L_669 = V_1;
		NullCheck(L_668);
		(L_668)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)76)), (TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F )L_669);
		TreeNodeU5BU5D_t5032BD830284F31E0A94DEB0072DB2B07126BABD* L_670 = L_668;
		il2cpp_codegen_initobj((&V_1), sizeof(TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F ));
		(&V_1)->set_Value_2((uint16_t)((int32_t)19));
		(&V_1)->set_NextZeroIdx_0((uint16_t)0);
		(&V_1)->set_NextOneIdx_1((uint16_t)0);
		TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F  L_671 = V_1;
		NullCheck(L_670);
		(L_670)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)77)), (TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F )L_671);
		TreeNodeU5BU5D_t5032BD830284F31E0A94DEB0072DB2B07126BABD* L_672 = L_670;
		il2cpp_codegen_initobj((&V_1), sizeof(TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F ));
		(&V_1)->set_Value_2((uint16_t)((int32_t)20));
		(&V_1)->set_NextZeroIdx_0((uint16_t)0);
		(&V_1)->set_NextOneIdx_1((uint16_t)0);
		TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F  L_673 = V_1;
		NullCheck(L_672);
		(L_672)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)78)), (TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F )L_673);
		TreeNodeU5BU5D_t5032BD830284F31E0A94DEB0072DB2B07126BABD* L_674 = L_672;
		il2cpp_codegen_initobj((&V_1), sizeof(TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F ));
		(&V_1)->set_Value_2((uint16_t)0);
		(&V_1)->set_NextZeroIdx_0((uint16_t)((int32_t)80));
		(&V_1)->set_NextOneIdx_1((uint16_t)((int32_t)83));
		TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F  L_675 = V_1;
		NullCheck(L_674);
		(L_674)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)79)), (TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F )L_675);
		TreeNodeU5BU5D_t5032BD830284F31E0A94DEB0072DB2B07126BABD* L_676 = L_674;
		il2cpp_codegen_initobj((&V_1), sizeof(TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F ));
		(&V_1)->set_Value_2((uint16_t)((int32_t)21));
		(&V_1)->set_NextZeroIdx_0((uint16_t)0);
		(&V_1)->set_NextOneIdx_1((uint16_t)0);
		TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F  L_677 = V_1;
		NullCheck(L_676);
		(L_676)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)80)), (TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F )L_677);
		TreeNodeU5BU5D_t5032BD830284F31E0A94DEB0072DB2B07126BABD* L_678 = L_676;
		il2cpp_codegen_initobj((&V_1), sizeof(TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F ));
		(&V_1)->set_Value_2((uint16_t)0);
		(&V_1)->set_NextZeroIdx_0((uint16_t)((int32_t)82));
		(&V_1)->set_NextOneIdx_1((uint16_t)((int32_t)512));
		TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F  L_679 = V_1;
		NullCheck(L_678);
		(L_678)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)81)), (TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F )L_679);
		TreeNodeU5BU5D_t5032BD830284F31E0A94DEB0072DB2B07126BABD* L_680 = L_678;
		il2cpp_codegen_initobj((&V_1), sizeof(TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F ));
		(&V_1)->set_Value_2((uint16_t)((int32_t)22));
		(&V_1)->set_NextZeroIdx_0((uint16_t)0);
		(&V_1)->set_NextOneIdx_1((uint16_t)0);
		TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F  L_681 = V_1;
		NullCheck(L_680);
		(L_680)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)82)), (TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F )L_681);
		TreeNodeU5BU5D_t5032BD830284F31E0A94DEB0072DB2B07126BABD* L_682 = L_680;
		il2cpp_codegen_initobj((&V_1), sizeof(TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F ));
		(&V_1)->set_Value_2((uint16_t)((int32_t)23));
		(&V_1)->set_NextZeroIdx_0((uint16_t)0);
		(&V_1)->set_NextOneIdx_1((uint16_t)0);
		TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F  L_683 = V_1;
		NullCheck(L_682);
		(L_682)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)83)), (TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F )L_683);
		TreeNodeU5BU5D_t5032BD830284F31E0A94DEB0072DB2B07126BABD* L_684 = L_682;
		il2cpp_codegen_initobj((&V_1), sizeof(TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F ));
		(&V_1)->set_Value_2((uint16_t)0);
		(&V_1)->set_NextZeroIdx_0((uint16_t)((int32_t)85));
		(&V_1)->set_NextOneIdx_1((uint16_t)((int32_t)88));
		TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F  L_685 = V_1;
		NullCheck(L_684);
		(L_684)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)84)), (TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F )L_685);
		TreeNodeU5BU5D_t5032BD830284F31E0A94DEB0072DB2B07126BABD* L_686 = L_684;
		il2cpp_codegen_initobj((&V_1), sizeof(TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F ));
		(&V_1)->set_Value_2((uint16_t)0);
		(&V_1)->set_NextZeroIdx_0((uint16_t)((int32_t)86));
		(&V_1)->set_NextOneIdx_1((uint16_t)((int32_t)87));
		TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F  L_687 = V_1;
		NullCheck(L_686);
		(L_686)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)85)), (TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F )L_687);
		TreeNodeU5BU5D_t5032BD830284F31E0A94DEB0072DB2B07126BABD* L_688 = L_686;
		il2cpp_codegen_initobj((&V_1), sizeof(TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F ));
		(&V_1)->set_Value_2((uint16_t)((int32_t)24));
		(&V_1)->set_NextZeroIdx_0((uint16_t)0);
		(&V_1)->set_NextOneIdx_1((uint16_t)0);
		TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F  L_689 = V_1;
		NullCheck(L_688);
		(L_688)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)86)), (TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F )L_689);
		TreeNodeU5BU5D_t5032BD830284F31E0A94DEB0072DB2B07126BABD* L_690 = L_688;
		il2cpp_codegen_initobj((&V_1), sizeof(TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F ));
		(&V_1)->set_Value_2((uint16_t)((int32_t)25));
		(&V_1)->set_NextZeroIdx_0((uint16_t)0);
		(&V_1)->set_NextOneIdx_1((uint16_t)0);
		TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F  L_691 = V_1;
		NullCheck(L_690);
		(L_690)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)87)), (TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F )L_691);
		TreeNodeU5BU5D_t5032BD830284F31E0A94DEB0072DB2B07126BABD* L_692 = L_690;
		il2cpp_codegen_initobj((&V_1), sizeof(TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F ));
		(&V_1)->set_Value_2((uint16_t)0);
		(&V_1)->set_NextZeroIdx_0((uint16_t)((int32_t)89));
		(&V_1)->set_NextOneIdx_1((uint16_t)((int32_t)90));
		TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F  L_693 = V_1;
		NullCheck(L_692);
		(L_692)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)88)), (TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F )L_693);
		TreeNodeU5BU5D_t5032BD830284F31E0A94DEB0072DB2B07126BABD* L_694 = L_692;
		il2cpp_codegen_initobj((&V_1), sizeof(TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F ));
		(&V_1)->set_Value_2((uint16_t)((int32_t)26));
		(&V_1)->set_NextZeroIdx_0((uint16_t)0);
		(&V_1)->set_NextOneIdx_1((uint16_t)0);
		TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F  L_695 = V_1;
		NullCheck(L_694);
		(L_694)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)89)), (TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F )L_695);
		TreeNodeU5BU5D_t5032BD830284F31E0A94DEB0072DB2B07126BABD* L_696 = L_694;
		il2cpp_codegen_initobj((&V_1), sizeof(TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F ));
		(&V_1)->set_Value_2((uint16_t)((int32_t)27));
		(&V_1)->set_NextZeroIdx_0((uint16_t)0);
		(&V_1)->set_NextOneIdx_1((uint16_t)0);
		TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F  L_697 = V_1;
		NullCheck(L_696);
		(L_696)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)90)), (TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F )L_697);
		TreeNodeU5BU5D_t5032BD830284F31E0A94DEB0072DB2B07126BABD* L_698 = L_696;
		il2cpp_codegen_initobj((&V_1), sizeof(TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F ));
		(&V_1)->set_Value_2((uint16_t)0);
		(&V_1)->set_NextZeroIdx_0((uint16_t)((int32_t)92));
		(&V_1)->set_NextOneIdx_1((uint16_t)((int32_t)95));
		TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F  L_699 = V_1;
		NullCheck(L_698);
		(L_698)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)91)), (TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F )L_699);
		TreeNodeU5BU5D_t5032BD830284F31E0A94DEB0072DB2B07126BABD* L_700 = L_698;
		il2cpp_codegen_initobj((&V_1), sizeof(TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F ));
		(&V_1)->set_Value_2((uint16_t)0);
		(&V_1)->set_NextZeroIdx_0((uint16_t)((int32_t)93));
		(&V_1)->set_NextOneIdx_1((uint16_t)((int32_t)94));
		TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F  L_701 = V_1;
		NullCheck(L_700);
		(L_700)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)92)), (TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F )L_701);
		TreeNodeU5BU5D_t5032BD830284F31E0A94DEB0072DB2B07126BABD* L_702 = L_700;
		il2cpp_codegen_initobj((&V_1), sizeof(TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F ));
		(&V_1)->set_Value_2((uint16_t)((int32_t)28));
		(&V_1)->set_NextZeroIdx_0((uint16_t)0);
		(&V_1)->set_NextOneIdx_1((uint16_t)0);
		TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F  L_703 = V_1;
		NullCheck(L_702);
		(L_702)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)93)), (TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F )L_703);
		TreeNodeU5BU5D_t5032BD830284F31E0A94DEB0072DB2B07126BABD* L_704 = L_702;
		il2cpp_codegen_initobj((&V_1), sizeof(TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F ));
		(&V_1)->set_Value_2((uint16_t)((int32_t)29));
		(&V_1)->set_NextZeroIdx_0((uint16_t)0);
		(&V_1)->set_NextOneIdx_1((uint16_t)0);
		TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F  L_705 = V_1;
		NullCheck(L_704);
		(L_704)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)94)), (TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F )L_705);
		TreeNodeU5BU5D_t5032BD830284F31E0A94DEB0072DB2B07126BABD* L_706 = L_704;
		il2cpp_codegen_initobj((&V_1), sizeof(TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F ));
		(&V_1)->set_Value_2((uint16_t)0);
		(&V_1)->set_NextZeroIdx_0((uint16_t)((int32_t)96));
		(&V_1)->set_NextOneIdx_1((uint16_t)((int32_t)97));
		TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F  L_707 = V_1;
		NullCheck(L_706);
		(L_706)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)95)), (TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F )L_707);
		TreeNodeU5BU5D_t5032BD830284F31E0A94DEB0072DB2B07126BABD* L_708 = L_706;
		il2cpp_codegen_initobj((&V_1), sizeof(TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F ));
		(&V_1)->set_Value_2((uint16_t)((int32_t)30));
		(&V_1)->set_NextZeroIdx_0((uint16_t)0);
		(&V_1)->set_NextOneIdx_1((uint16_t)0);
		TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F  L_709 = V_1;
		NullCheck(L_708);
		(L_708)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)96)), (TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F )L_709);
		TreeNodeU5BU5D_t5032BD830284F31E0A94DEB0072DB2B07126BABD* L_710 = L_708;
		il2cpp_codegen_initobj((&V_1), sizeof(TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F ));
		(&V_1)->set_Value_2((uint16_t)((int32_t)31));
		(&V_1)->set_NextZeroIdx_0((uint16_t)0);
		(&V_1)->set_NextOneIdx_1((uint16_t)0);
		TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F  L_711 = V_1;
		NullCheck(L_710);
		(L_710)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)97)), (TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F )L_711);
		TreeNodeU5BU5D_t5032BD830284F31E0A94DEB0072DB2B07126BABD* L_712 = L_710;
		il2cpp_codegen_initobj((&V_1), sizeof(TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F ));
		(&V_1)->set_Value_2((uint16_t)0);
		(&V_1)->set_NextZeroIdx_0((uint16_t)((int32_t)133));
		(&V_1)->set_NextOneIdx_1((uint16_t)((int32_t)99));
		TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F  L_713 = V_1;
		NullCheck(L_712);
		(L_712)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)98)), (TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F )L_713);
		TreeNodeU5BU5D_t5032BD830284F31E0A94DEB0072DB2B07126BABD* L_714 = L_712;
		il2cpp_codegen_initobj((&V_1), sizeof(TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F ));
		(&V_1)->set_Value_2((uint16_t)0);
		(&V_1)->set_NextZeroIdx_0((uint16_t)((int32_t)100));
		(&V_1)->set_NextOneIdx_1((uint16_t)((int32_t)129));
		TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F  L_715 = V_1;
		NullCheck(L_714);
		(L_714)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)99)), (TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F )L_715);
		TreeNodeU5BU5D_t5032BD830284F31E0A94DEB0072DB2B07126BABD* L_716 = L_714;
		il2cpp_codegen_initobj((&V_1), sizeof(TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F ));
		(&V_1)->set_Value_2((uint16_t)0);
		(&V_1)->set_NextZeroIdx_0((uint16_t)((int32_t)258));
		(&V_1)->set_NextOneIdx_1((uint16_t)((int32_t)101));
		TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F  L_717 = V_1;
		NullCheck(L_716);
		(L_716)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)100)), (TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F )L_717);
		TreeNodeU5BU5D_t5032BD830284F31E0A94DEB0072DB2B07126BABD* L_718 = L_716;
		il2cpp_codegen_initobj((&V_1), sizeof(TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F ));
		(&V_1)->set_Value_2((uint16_t)0);
		(&V_1)->set_NextZeroIdx_0((uint16_t)((int32_t)102));
		(&V_1)->set_NextOneIdx_1((uint16_t)((int32_t)126));
		TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F  L_719 = V_1;
		NullCheck(L_718);
		(L_718)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)101)), (TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F )L_719);
		TreeNodeU5BU5D_t5032BD830284F31E0A94DEB0072DB2B07126BABD* L_720 = L_718;
		il2cpp_codegen_initobj((&V_1), sizeof(TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F ));
		(&V_1)->set_Value_2((uint16_t)0);
		(&V_1)->set_NextZeroIdx_0((uint16_t)((int32_t)103));
		(&V_1)->set_NextOneIdx_1((uint16_t)((int32_t)112));
		TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F  L_721 = V_1;
		NullCheck(L_720);
		(L_720)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)102)), (TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F )L_721);
		TreeNodeU5BU5D_t5032BD830284F31E0A94DEB0072DB2B07126BABD* L_722 = L_720;
		il2cpp_codegen_initobj((&V_1), sizeof(TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F ));
		(&V_1)->set_Value_2((uint16_t)((int32_t)32));
		(&V_1)->set_NextZeroIdx_0((uint16_t)0);
		(&V_1)->set_NextOneIdx_1((uint16_t)0);
		TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F  L_723 = V_1;
		NullCheck(L_722);
		(L_722)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)103)), (TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F )L_723);
		TreeNodeU5BU5D_t5032BD830284F31E0A94DEB0072DB2B07126BABD* L_724 = L_722;
		il2cpp_codegen_initobj((&V_1), sizeof(TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F ));
		(&V_1)->set_Value_2((uint16_t)0);
		(&V_1)->set_NextZeroIdx_0((uint16_t)((int32_t)105));
		(&V_1)->set_NextOneIdx_1((uint16_t)((int32_t)119));
		TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F  L_725 = V_1;
		NullCheck(L_724);
		(L_724)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)104)), (TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F )L_725);
		TreeNodeU5BU5D_t5032BD830284F31E0A94DEB0072DB2B07126BABD* L_726 = L_724;
		il2cpp_codegen_initobj((&V_1), sizeof(TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F ));
		(&V_1)->set_Value_2((uint16_t)0);
		(&V_1)->set_NextZeroIdx_0((uint16_t)((int32_t)106));
		(&V_1)->set_NextOneIdx_1((uint16_t)((int32_t)107));
		TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F  L_727 = V_1;
		NullCheck(L_726);
		(L_726)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)105)), (TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F )L_727);
		TreeNodeU5BU5D_t5032BD830284F31E0A94DEB0072DB2B07126BABD* L_728 = L_726;
		il2cpp_codegen_initobj((&V_1), sizeof(TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F ));
		(&V_1)->set_Value_2((uint16_t)((int32_t)33));
		(&V_1)->set_NextZeroIdx_0((uint16_t)0);
		(&V_1)->set_NextOneIdx_1((uint16_t)0);
		TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F  L_729 = V_1;
		NullCheck(L_728);
		(L_728)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)106)), (TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F )L_729);
		TreeNodeU5BU5D_t5032BD830284F31E0A94DEB0072DB2B07126BABD* L_730 = L_728;
		il2cpp_codegen_initobj((&V_1), sizeof(TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F ));
		(&V_1)->set_Value_2((uint16_t)((int32_t)34));
		(&V_1)->set_NextZeroIdx_0((uint16_t)0);
		(&V_1)->set_NextOneIdx_1((uint16_t)0);
		TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F  L_731 = V_1;
		NullCheck(L_730);
		(L_730)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)107)), (TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F )L_731);
		TreeNodeU5BU5D_t5032BD830284F31E0A94DEB0072DB2B07126BABD* L_732 = L_730;
		il2cpp_codegen_initobj((&V_1), sizeof(TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F ));
		(&V_1)->set_Value_2((uint16_t)0);
		(&V_1)->set_NextZeroIdx_0((uint16_t)((int32_t)271));
		(&V_1)->set_NextOneIdx_1((uint16_t)((int32_t)109));
		TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F  L_733 = V_1;
		NullCheck(L_732);
		(L_732)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)108)), (TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F )L_733);
		TreeNodeU5BU5D_t5032BD830284F31E0A94DEB0072DB2B07126BABD* L_734 = L_732;
		il2cpp_codegen_initobj((&V_1), sizeof(TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F ));
		(&V_1)->set_Value_2((uint16_t)0);
		(&V_1)->set_NextZeroIdx_0((uint16_t)((int32_t)110));
		(&V_1)->set_NextOneIdx_1((uint16_t)((int32_t)164));
		TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F  L_735 = V_1;
		NullCheck(L_734);
		(L_734)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)109)), (TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F )L_735);
		TreeNodeU5BU5D_t5032BD830284F31E0A94DEB0072DB2B07126BABD* L_736 = L_734;
		il2cpp_codegen_initobj((&V_1), sizeof(TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F ));
		(&V_1)->set_Value_2((uint16_t)((int32_t)35));
		(&V_1)->set_NextZeroIdx_0((uint16_t)0);
		(&V_1)->set_NextOneIdx_1((uint16_t)0);
		TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F  L_737 = V_1;
		NullCheck(L_736);
		(L_736)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)110)), (TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F )L_737);
		TreeNodeU5BU5D_t5032BD830284F31E0A94DEB0072DB2B07126BABD* L_738 = L_736;
		il2cpp_codegen_initobj((&V_1), sizeof(TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F ));
		(&V_1)->set_Value_2((uint16_t)((int32_t)36));
		(&V_1)->set_NextZeroIdx_0((uint16_t)0);
		(&V_1)->set_NextOneIdx_1((uint16_t)0);
		TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F  L_739 = V_1;
		NullCheck(L_738);
		(L_738)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)111)), (TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F )L_739);
		TreeNodeU5BU5D_t5032BD830284F31E0A94DEB0072DB2B07126BABD* L_740 = L_738;
		il2cpp_codegen_initobj((&V_1), sizeof(TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F ));
		(&V_1)->set_Value_2((uint16_t)((int32_t)37));
		(&V_1)->set_NextZeroIdx_0((uint16_t)0);
		(&V_1)->set_NextOneIdx_1((uint16_t)0);
		TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F  L_741 = V_1;
		NullCheck(L_740);
		(L_740)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)112)), (TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F )L_741);
		TreeNodeU5BU5D_t5032BD830284F31E0A94DEB0072DB2B07126BABD* L_742 = L_740;
		il2cpp_codegen_initobj((&V_1), sizeof(TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F ));
		(&V_1)->set_Value_2((uint16_t)0);
		(&V_1)->set_NextZeroIdx_0((uint16_t)((int32_t)114));
		(&V_1)->set_NextOneIdx_1((uint16_t)((int32_t)124));
		TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F  L_743 = V_1;
		NullCheck(L_742);
		(L_742)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)113)), (TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F )L_743);
		TreeNodeU5BU5D_t5032BD830284F31E0A94DEB0072DB2B07126BABD* L_744 = L_742;
		il2cpp_codegen_initobj((&V_1), sizeof(TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F ));
		(&V_1)->set_Value_2((uint16_t)0);
		(&V_1)->set_NextZeroIdx_0((uint16_t)((int32_t)115));
		(&V_1)->set_NextOneIdx_1((uint16_t)((int32_t)122));
		TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F  L_745 = V_1;
		NullCheck(L_744);
		(L_744)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)114)), (TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F )L_745);
		TreeNodeU5BU5D_t5032BD830284F31E0A94DEB0072DB2B07126BABD* L_746 = L_744;
		il2cpp_codegen_initobj((&V_1), sizeof(TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F ));
		(&V_1)->set_Value_2((uint16_t)((int32_t)38));
		(&V_1)->set_NextZeroIdx_0((uint16_t)0);
		(&V_1)->set_NextOneIdx_1((uint16_t)0);
		TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F  L_747 = V_1;
		NullCheck(L_746);
		(L_746)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)115)), (TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F )L_747);
		TreeNodeU5BU5D_t5032BD830284F31E0A94DEB0072DB2B07126BABD* L_748 = L_746;
		il2cpp_codegen_initobj((&V_1), sizeof(TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F ));
		(&V_1)->set_Value_2((uint16_t)0);
		(&V_1)->set_NextZeroIdx_0((uint16_t)((int32_t)165));
		(&V_1)->set_NextOneIdx_1((uint16_t)((int32_t)117));
		TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F  L_749 = V_1;
		NullCheck(L_748);
		(L_748)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)116)), (TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F )L_749);
		TreeNodeU5BU5D_t5032BD830284F31E0A94DEB0072DB2B07126BABD* L_750 = L_748;
		il2cpp_codegen_initobj((&V_1), sizeof(TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F ));
		(&V_1)->set_Value_2((uint16_t)0);
		(&V_1)->set_NextZeroIdx_0((uint16_t)((int32_t)118));
		(&V_1)->set_NextOneIdx_1((uint16_t)((int32_t)123));
		TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F  L_751 = V_1;
		NullCheck(L_750);
		(L_750)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)117)), (TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F )L_751);
		TreeNodeU5BU5D_t5032BD830284F31E0A94DEB0072DB2B07126BABD* L_752 = L_750;
		il2cpp_codegen_initobj((&V_1), sizeof(TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F ));
		(&V_1)->set_Value_2((uint16_t)((int32_t)39));
		(&V_1)->set_NextZeroIdx_0((uint16_t)0);
		(&V_1)->set_NextOneIdx_1((uint16_t)0);
		TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F  L_753 = V_1;
		NullCheck(L_752);
		(L_752)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)118)), (TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F )L_753);
		TreeNodeU5BU5D_t5032BD830284F31E0A94DEB0072DB2B07126BABD* L_754 = L_752;
		il2cpp_codegen_initobj((&V_1), sizeof(TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F ));
		(&V_1)->set_Value_2((uint16_t)0);
		(&V_1)->set_NextZeroIdx_0((uint16_t)((int32_t)120));
		(&V_1)->set_NextOneIdx_1((uint16_t)((int32_t)121));
		TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F  L_755 = V_1;
		NullCheck(L_754);
		(L_754)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)119)), (TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F )L_755);
		TreeNodeU5BU5D_t5032BD830284F31E0A94DEB0072DB2B07126BABD* L_756 = L_754;
		il2cpp_codegen_initobj((&V_1), sizeof(TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F ));
		(&V_1)->set_Value_2((uint16_t)((int32_t)40));
		(&V_1)->set_NextZeroIdx_0((uint16_t)0);
		(&V_1)->set_NextOneIdx_1((uint16_t)0);
		TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F  L_757 = V_1;
		NullCheck(L_756);
		(L_756)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)120)), (TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F )L_757);
		TreeNodeU5BU5D_t5032BD830284F31E0A94DEB0072DB2B07126BABD* L_758 = L_756;
		il2cpp_codegen_initobj((&V_1), sizeof(TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F ));
		(&V_1)->set_Value_2((uint16_t)((int32_t)41));
		(&V_1)->set_NextZeroIdx_0((uint16_t)0);
		(&V_1)->set_NextOneIdx_1((uint16_t)0);
		TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F  L_759 = V_1;
		NullCheck(L_758);
		(L_758)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)121)), (TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F )L_759);
		TreeNodeU5BU5D_t5032BD830284F31E0A94DEB0072DB2B07126BABD* L_760 = L_758;
		il2cpp_codegen_initobj((&V_1), sizeof(TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F ));
		(&V_1)->set_Value_2((uint16_t)((int32_t)42));
		(&V_1)->set_NextZeroIdx_0((uint16_t)0);
		(&V_1)->set_NextOneIdx_1((uint16_t)0);
		TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F  L_761 = V_1;
		NullCheck(L_760);
		(L_760)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)122)), (TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F )L_761);
		TreeNodeU5BU5D_t5032BD830284F31E0A94DEB0072DB2B07126BABD* L_762 = L_760;
		il2cpp_codegen_initobj((&V_1), sizeof(TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F ));
		(&V_1)->set_Value_2((uint16_t)((int32_t)43));
		(&V_1)->set_NextZeroIdx_0((uint16_t)0);
		(&V_1)->set_NextOneIdx_1((uint16_t)0);
		TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F  L_763 = V_1;
		NullCheck(L_762);
		(L_762)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)123)), (TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F )L_763);
		TreeNodeU5BU5D_t5032BD830284F31E0A94DEB0072DB2B07126BABD* L_764 = L_762;
		il2cpp_codegen_initobj((&V_1), sizeof(TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F ));
		(&V_1)->set_Value_2((uint16_t)0);
		(&V_1)->set_NextZeroIdx_0((uint16_t)((int32_t)125));
		(&V_1)->set_NextOneIdx_1((uint16_t)((int32_t)157));
		TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F  L_765 = V_1;
		NullCheck(L_764);
		(L_764)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)124)), (TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F )L_765);
		TreeNodeU5BU5D_t5032BD830284F31E0A94DEB0072DB2B07126BABD* L_766 = L_764;
		il2cpp_codegen_initobj((&V_1), sizeof(TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F ));
		(&V_1)->set_Value_2((uint16_t)((int32_t)44));
		(&V_1)->set_NextZeroIdx_0((uint16_t)0);
		(&V_1)->set_NextOneIdx_1((uint16_t)0);
		TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F  L_767 = V_1;
		NullCheck(L_766);
		(L_766)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)125)), (TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F )L_767);
		TreeNodeU5BU5D_t5032BD830284F31E0A94DEB0072DB2B07126BABD* L_768 = L_766;
		il2cpp_codegen_initobj((&V_1), sizeof(TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F ));
		(&V_1)->set_Value_2((uint16_t)0);
		(&V_1)->set_NextZeroIdx_0((uint16_t)((int32_t)127));
		(&V_1)->set_NextOneIdx_1((uint16_t)((int32_t)128));
		TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F  L_769 = V_1;
		NullCheck(L_768);
		(L_768)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)126)), (TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F )L_769);
		TreeNodeU5BU5D_t5032BD830284F31E0A94DEB0072DB2B07126BABD* L_770 = L_768;
		il2cpp_codegen_initobj((&V_1), sizeof(TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F ));
		(&V_1)->set_Value_2((uint16_t)((int32_t)45));
		(&V_1)->set_NextZeroIdx_0((uint16_t)0);
		(&V_1)->set_NextOneIdx_1((uint16_t)0);
		TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F  L_771 = V_1;
		NullCheck(L_770);
		(L_770)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)127)), (TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F )L_771);
		TreeNodeU5BU5D_t5032BD830284F31E0A94DEB0072DB2B07126BABD* L_772 = L_770;
		il2cpp_codegen_initobj((&V_1), sizeof(TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F ));
		(&V_1)->set_Value_2((uint16_t)((int32_t)46));
		(&V_1)->set_NextZeroIdx_0((uint16_t)0);
		(&V_1)->set_NextOneIdx_1((uint16_t)0);
		TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F  L_773 = V_1;
		NullCheck(L_772);
		(L_772)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)128)), (TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F )L_773);
		TreeNodeU5BU5D_t5032BD830284F31E0A94DEB0072DB2B07126BABD* L_774 = L_772;
		il2cpp_codegen_initobj((&V_1), sizeof(TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F ));
		(&V_1)->set_Value_2((uint16_t)0);
		(&V_1)->set_NextZeroIdx_0((uint16_t)((int32_t)130));
		(&V_1)->set_NextOneIdx_1((uint16_t)((int32_t)144));
		TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F  L_775 = V_1;
		NullCheck(L_774);
		(L_774)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)129)), (TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F )L_775);
		TreeNodeU5BU5D_t5032BD830284F31E0A94DEB0072DB2B07126BABD* L_776 = L_774;
		il2cpp_codegen_initobj((&V_1), sizeof(TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F ));
		(&V_1)->set_Value_2((uint16_t)0);
		(&V_1)->set_NextZeroIdx_0((uint16_t)((int32_t)131));
		(&V_1)->set_NextOneIdx_1((uint16_t)((int32_t)141));
		TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F  L_777 = V_1;
		NullCheck(L_776);
		(L_776)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)130)), (TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F )L_777);
		TreeNodeU5BU5D_t5032BD830284F31E0A94DEB0072DB2B07126BABD* L_778 = L_776;
		il2cpp_codegen_initobj((&V_1), sizeof(TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F ));
		(&V_1)->set_Value_2((uint16_t)0);
		(&V_1)->set_NextZeroIdx_0((uint16_t)((int32_t)132));
		(&V_1)->set_NextOneIdx_1((uint16_t)((int32_t)140));
		TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F  L_779 = V_1;
		NullCheck(L_778);
		(L_778)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)131)), (TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F )L_779);
		TreeNodeU5BU5D_t5032BD830284F31E0A94DEB0072DB2B07126BABD* L_780 = L_778;
		il2cpp_codegen_initobj((&V_1), sizeof(TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F ));
		(&V_1)->set_Value_2((uint16_t)((int32_t)47));
		(&V_1)->set_NextZeroIdx_0((uint16_t)0);
		(&V_1)->set_NextOneIdx_1((uint16_t)0);
		TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F  L_781 = V_1;
		NullCheck(L_780);
		(L_780)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)132)), (TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F )L_781);
		TreeNodeU5BU5D_t5032BD830284F31E0A94DEB0072DB2B07126BABD* L_782 = L_780;
		il2cpp_codegen_initobj((&V_1), sizeof(TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F ));
		(&V_1)->set_Value_2((uint16_t)0);
		(&V_1)->set_NextZeroIdx_0((uint16_t)((int32_t)134));
		(&V_1)->set_NextOneIdx_1((uint16_t)((int32_t)229));
		TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F  L_783 = V_1;
		NullCheck(L_782);
		(L_782)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)133)), (TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F )L_783);
		TreeNodeU5BU5D_t5032BD830284F31E0A94DEB0072DB2B07126BABD* L_784 = L_782;
		il2cpp_codegen_initobj((&V_1), sizeof(TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F ));
		(&V_1)->set_Value_2((uint16_t)0);
		(&V_1)->set_NextZeroIdx_0((uint16_t)((int32_t)135));
		(&V_1)->set_NextOneIdx_1((uint16_t)((int32_t)138));
		TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F  L_785 = V_1;
		NullCheck(L_784);
		(L_784)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)134)), (TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F )L_785);
		TreeNodeU5BU5D_t5032BD830284F31E0A94DEB0072DB2B07126BABD* L_786 = L_784;
		il2cpp_codegen_initobj((&V_1), sizeof(TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F ));
		(&V_1)->set_Value_2((uint16_t)0);
		(&V_1)->set_NextZeroIdx_0((uint16_t)((int32_t)136));
		(&V_1)->set_NextOneIdx_1((uint16_t)((int32_t)137));
		TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F  L_787 = V_1;
		NullCheck(L_786);
		(L_786)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)135)), (TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F )L_787);
		TreeNodeU5BU5D_t5032BD830284F31E0A94DEB0072DB2B07126BABD* L_788 = L_786;
		il2cpp_codegen_initobj((&V_1), sizeof(TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F ));
		(&V_1)->set_Value_2((uint16_t)((int32_t)48));
		(&V_1)->set_NextZeroIdx_0((uint16_t)0);
		(&V_1)->set_NextOneIdx_1((uint16_t)0);
		TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F  L_789 = V_1;
		NullCheck(L_788);
		(L_788)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)136)), (TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F )L_789);
		TreeNodeU5BU5D_t5032BD830284F31E0A94DEB0072DB2B07126BABD* L_790 = L_788;
		il2cpp_codegen_initobj((&V_1), sizeof(TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F ));
		(&V_1)->set_Value_2((uint16_t)((int32_t)49));
		(&V_1)->set_NextZeroIdx_0((uint16_t)0);
		(&V_1)->set_NextOneIdx_1((uint16_t)0);
		TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F  L_791 = V_1;
		NullCheck(L_790);
		(L_790)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)137)), (TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F )L_791);
		TreeNodeU5BU5D_t5032BD830284F31E0A94DEB0072DB2B07126BABD* L_792 = L_790;
		il2cpp_codegen_initobj((&V_1), sizeof(TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F ));
		(&V_1)->set_Value_2((uint16_t)0);
		(&V_1)->set_NextZeroIdx_0((uint16_t)((int32_t)139));
		(&V_1)->set_NextOneIdx_1((uint16_t)((int32_t)227));
		TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F  L_793 = V_1;
		NullCheck(L_792);
		(L_792)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)138)), (TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F )L_793);
		TreeNodeU5BU5D_t5032BD830284F31E0A94DEB0072DB2B07126BABD* L_794 = L_792;
		il2cpp_codegen_initobj((&V_1), sizeof(TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F ));
		(&V_1)->set_Value_2((uint16_t)((int32_t)50));
		(&V_1)->set_NextZeroIdx_0((uint16_t)0);
		(&V_1)->set_NextOneIdx_1((uint16_t)0);
		TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F  L_795 = V_1;
		NullCheck(L_794);
		(L_794)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)139)), (TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F )L_795);
		TreeNodeU5BU5D_t5032BD830284F31E0A94DEB0072DB2B07126BABD* L_796 = L_794;
		il2cpp_codegen_initobj((&V_1), sizeof(TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F ));
		(&V_1)->set_Value_2((uint16_t)((int32_t)51));
		(&V_1)->set_NextZeroIdx_0((uint16_t)0);
		(&V_1)->set_NextOneIdx_1((uint16_t)0);
		TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F  L_797 = V_1;
		NullCheck(L_796);
		(L_796)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)140)), (TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F )L_797);
		TreeNodeU5BU5D_t5032BD830284F31E0A94DEB0072DB2B07126BABD* L_798 = L_796;
		il2cpp_codegen_initobj((&V_1), sizeof(TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F ));
		(&V_1)->set_Value_2((uint16_t)0);
		(&V_1)->set_NextZeroIdx_0((uint16_t)((int32_t)142));
		(&V_1)->set_NextOneIdx_1((uint16_t)((int32_t)143));
		TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F  L_799 = V_1;
		NullCheck(L_798);
		(L_798)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)141)), (TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F )L_799);
		TreeNodeU5BU5D_t5032BD830284F31E0A94DEB0072DB2B07126BABD* L_800 = L_798;
		il2cpp_codegen_initobj((&V_1), sizeof(TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F ));
		(&V_1)->set_Value_2((uint16_t)((int32_t)52));
		(&V_1)->set_NextZeroIdx_0((uint16_t)0);
		(&V_1)->set_NextOneIdx_1((uint16_t)0);
		TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F  L_801 = V_1;
		NullCheck(L_800);
		(L_800)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)142)), (TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F )L_801);
		TreeNodeU5BU5D_t5032BD830284F31E0A94DEB0072DB2B07126BABD* L_802 = L_800;
		il2cpp_codegen_initobj((&V_1), sizeof(TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F ));
		(&V_1)->set_Value_2((uint16_t)((int32_t)53));
		(&V_1)->set_NextZeroIdx_0((uint16_t)0);
		(&V_1)->set_NextOneIdx_1((uint16_t)0);
		TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F  L_803 = V_1;
		NullCheck(L_802);
		(L_802)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)143)), (TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F )L_803);
		TreeNodeU5BU5D_t5032BD830284F31E0A94DEB0072DB2B07126BABD* L_804 = L_802;
		il2cpp_codegen_initobj((&V_1), sizeof(TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F ));
		(&V_1)->set_Value_2((uint16_t)0);
		(&V_1)->set_NextZeroIdx_0((uint16_t)((int32_t)145));
		(&V_1)->set_NextOneIdx_1((uint16_t)((int32_t)148));
		TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F  L_805 = V_1;
		NullCheck(L_804);
		(L_804)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)144)), (TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F )L_805);
		TreeNodeU5BU5D_t5032BD830284F31E0A94DEB0072DB2B07126BABD* L_806 = L_804;
		il2cpp_codegen_initobj((&V_1), sizeof(TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F ));
		(&V_1)->set_Value_2((uint16_t)0);
		(&V_1)->set_NextZeroIdx_0((uint16_t)((int32_t)146));
		(&V_1)->set_NextOneIdx_1((uint16_t)((int32_t)147));
		TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F  L_807 = V_1;
		NullCheck(L_806);
		(L_806)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)145)), (TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F )L_807);
		TreeNodeU5BU5D_t5032BD830284F31E0A94DEB0072DB2B07126BABD* L_808 = L_806;
		il2cpp_codegen_initobj((&V_1), sizeof(TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F ));
		(&V_1)->set_Value_2((uint16_t)((int32_t)54));
		(&V_1)->set_NextZeroIdx_0((uint16_t)0);
		(&V_1)->set_NextOneIdx_1((uint16_t)0);
		TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F  L_809 = V_1;
		NullCheck(L_808);
		(L_808)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)146)), (TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F )L_809);
		TreeNodeU5BU5D_t5032BD830284F31E0A94DEB0072DB2B07126BABD* L_810 = L_808;
		il2cpp_codegen_initobj((&V_1), sizeof(TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F ));
		(&V_1)->set_Value_2((uint16_t)((int32_t)55));
		(&V_1)->set_NextZeroIdx_0((uint16_t)0);
		(&V_1)->set_NextOneIdx_1((uint16_t)0);
		TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F  L_811 = V_1;
		NullCheck(L_810);
		(L_810)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)147)), (TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F )L_811);
		TreeNodeU5BU5D_t5032BD830284F31E0A94DEB0072DB2B07126BABD* L_812 = L_810;
		il2cpp_codegen_initobj((&V_1), sizeof(TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F ));
		(&V_1)->set_Value_2((uint16_t)0);
		(&V_1)->set_NextZeroIdx_0((uint16_t)((int32_t)149));
		(&V_1)->set_NextOneIdx_1((uint16_t)((int32_t)150));
		TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F  L_813 = V_1;
		NullCheck(L_812);
		(L_812)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)148)), (TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F )L_813);
		TreeNodeU5BU5D_t5032BD830284F31E0A94DEB0072DB2B07126BABD* L_814 = L_812;
		il2cpp_codegen_initobj((&V_1), sizeof(TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F ));
		(&V_1)->set_Value_2((uint16_t)((int32_t)56));
		(&V_1)->set_NextZeroIdx_0((uint16_t)0);
		(&V_1)->set_NextOneIdx_1((uint16_t)0);
		TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F  L_815 = V_1;
		NullCheck(L_814);
		(L_814)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)149)), (TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F )L_815);
		TreeNodeU5BU5D_t5032BD830284F31E0A94DEB0072DB2B07126BABD* L_816 = L_814;
		il2cpp_codegen_initobj((&V_1), sizeof(TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F ));
		(&V_1)->set_Value_2((uint16_t)((int32_t)57));
		(&V_1)->set_NextZeroIdx_0((uint16_t)0);
		(&V_1)->set_NextOneIdx_1((uint16_t)0);
		TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F  L_817 = V_1;
		NullCheck(L_816);
		(L_816)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)150)), (TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F )L_817);
		TreeNodeU5BU5D_t5032BD830284F31E0A94DEB0072DB2B07126BABD* L_818 = L_816;
		il2cpp_codegen_initobj((&V_1), sizeof(TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F ));
		(&V_1)->set_Value_2((uint16_t)0);
		(&V_1)->set_NextZeroIdx_0((uint16_t)((int32_t)160));
		(&V_1)->set_NextOneIdx_1((uint16_t)((int32_t)152));
		TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F  L_819 = V_1;
		NullCheck(L_818);
		(L_818)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)151)), (TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F )L_819);
		TreeNodeU5BU5D_t5032BD830284F31E0A94DEB0072DB2B07126BABD* L_820 = L_818;
		il2cpp_codegen_initobj((&V_1), sizeof(TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F ));
		(&V_1)->set_Value_2((uint16_t)0);
		(&V_1)->set_NextZeroIdx_0((uint16_t)((int32_t)246));
		(&V_1)->set_NextOneIdx_1((uint16_t)((int32_t)153));
		TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F  L_821 = V_1;
		NullCheck(L_820);
		(L_820)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)152)), (TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F )L_821);
		TreeNodeU5BU5D_t5032BD830284F31E0A94DEB0072DB2B07126BABD* L_822 = L_820;
		il2cpp_codegen_initobj((&V_1), sizeof(TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F ));
		(&V_1)->set_Value_2((uint16_t)0);
		(&V_1)->set_NextZeroIdx_0((uint16_t)((int32_t)256));
		(&V_1)->set_NextOneIdx_1((uint16_t)((int32_t)154));
		TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F  L_823 = V_1;
		NullCheck(L_822);
		(L_822)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)153)), (TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F )L_823);
		TreeNodeU5BU5D_t5032BD830284F31E0A94DEB0072DB2B07126BABD* L_824 = L_822;
		il2cpp_codegen_initobj((&V_1), sizeof(TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F ));
		(&V_1)->set_Value_2((uint16_t)0);
		(&V_1)->set_NextZeroIdx_0((uint16_t)((int32_t)155));
		(&V_1)->set_NextOneIdx_1((uint16_t)((int32_t)170));
		TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F  L_825 = V_1;
		NullCheck(L_824);
		(L_824)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)154)), (TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F )L_825);
		TreeNodeU5BU5D_t5032BD830284F31E0A94DEB0072DB2B07126BABD* L_826 = L_824;
		il2cpp_codegen_initobj((&V_1), sizeof(TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F ));
		(&V_1)->set_Value_2((uint16_t)0);
		(&V_1)->set_NextZeroIdx_0((uint16_t)((int32_t)156));
		(&V_1)->set_NextOneIdx_1((uint16_t)((int32_t)169));
		TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F  L_827 = V_1;
		NullCheck(L_826);
		(L_826)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)155)), (TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F )L_827);
		TreeNodeU5BU5D_t5032BD830284F31E0A94DEB0072DB2B07126BABD* L_828 = L_826;
		il2cpp_codegen_initobj((&V_1), sizeof(TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F ));
		(&V_1)->set_Value_2((uint16_t)((int32_t)58));
		(&V_1)->set_NextZeroIdx_0((uint16_t)0);
		(&V_1)->set_NextOneIdx_1((uint16_t)0);
		TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F  L_829 = V_1;
		NullCheck(L_828);
		(L_828)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)156)), (TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F )L_829);
		TreeNodeU5BU5D_t5032BD830284F31E0A94DEB0072DB2B07126BABD* L_830 = L_828;
		il2cpp_codegen_initobj((&V_1), sizeof(TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F ));
		(&V_1)->set_Value_2((uint16_t)((int32_t)59));
		(&V_1)->set_NextZeroIdx_0((uint16_t)0);
		(&V_1)->set_NextOneIdx_1((uint16_t)0);
		TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F  L_831 = V_1;
		NullCheck(L_830);
		(L_830)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)157)), (TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F )L_831);
		TreeNodeU5BU5D_t5032BD830284F31E0A94DEB0072DB2B07126BABD* L_832 = L_830;
		il2cpp_codegen_initobj((&V_1), sizeof(TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F ));
		(&V_1)->set_Value_2((uint16_t)0);
		(&V_1)->set_NextZeroIdx_0((uint16_t)((int32_t)159));
		(&V_1)->set_NextOneIdx_1((uint16_t)((int32_t)226));
		TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F  L_833 = V_1;
		NullCheck(L_832);
		(L_832)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)158)), (TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F )L_833);
		TreeNodeU5BU5D_t5032BD830284F31E0A94DEB0072DB2B07126BABD* L_834 = L_832;
		il2cpp_codegen_initobj((&V_1), sizeof(TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F ));
		(&V_1)->set_Value_2((uint16_t)((int32_t)60));
		(&V_1)->set_NextZeroIdx_0((uint16_t)0);
		(&V_1)->set_NextOneIdx_1((uint16_t)0);
		TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F  L_835 = V_1;
		NullCheck(L_834);
		(L_834)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)159)), (TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F )L_835);
		TreeNodeU5BU5D_t5032BD830284F31E0A94DEB0072DB2B07126BABD* L_836 = L_834;
		il2cpp_codegen_initobj((&V_1), sizeof(TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F ));
		(&V_1)->set_Value_2((uint16_t)0);
		(&V_1)->set_NextZeroIdx_0((uint16_t)((int32_t)161));
		(&V_1)->set_NextOneIdx_1((uint16_t)((int32_t)232));
		TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F  L_837 = V_1;
		NullCheck(L_836);
		(L_836)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)160)), (TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F )L_837);
		TreeNodeU5BU5D_t5032BD830284F31E0A94DEB0072DB2B07126BABD* L_838 = L_836;
		il2cpp_codegen_initobj((&V_1), sizeof(TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F ));
		(&V_1)->set_Value_2((uint16_t)0);
		(&V_1)->set_NextZeroIdx_0((uint16_t)((int32_t)162));
		(&V_1)->set_NextOneIdx_1((uint16_t)((int32_t)224));
		TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F  L_839 = V_1;
		NullCheck(L_838);
		(L_838)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)161)), (TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F )L_839);
		TreeNodeU5BU5D_t5032BD830284F31E0A94DEB0072DB2B07126BABD* L_840 = L_838;
		il2cpp_codegen_initobj((&V_1), sizeof(TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F ));
		(&V_1)->set_Value_2((uint16_t)0);
		(&V_1)->set_NextZeroIdx_0((uint16_t)((int32_t)163));
		(&V_1)->set_NextOneIdx_1((uint16_t)((int32_t)168));
		TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F  L_841 = V_1;
		NullCheck(L_840);
		(L_840)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)162)), (TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F )L_841);
		TreeNodeU5BU5D_t5032BD830284F31E0A94DEB0072DB2B07126BABD* L_842 = L_840;
		il2cpp_codegen_initobj((&V_1), sizeof(TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F ));
		(&V_1)->set_Value_2((uint16_t)((int32_t)61));
		(&V_1)->set_NextZeroIdx_0((uint16_t)0);
		(&V_1)->set_NextOneIdx_1((uint16_t)0);
		TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F  L_843 = V_1;
		NullCheck(L_842);
		(L_842)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)163)), (TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F )L_843);
		TreeNodeU5BU5D_t5032BD830284F31E0A94DEB0072DB2B07126BABD* L_844 = L_842;
		il2cpp_codegen_initobj((&V_1), sizeof(TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F ));
		(&V_1)->set_Value_2((uint16_t)((int32_t)62));
		(&V_1)->set_NextZeroIdx_0((uint16_t)0);
		(&V_1)->set_NextOneIdx_1((uint16_t)0);
		TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F  L_845 = V_1;
		NullCheck(L_844);
		(L_844)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)164)), (TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F )L_845);
		TreeNodeU5BU5D_t5032BD830284F31E0A94DEB0072DB2B07126BABD* L_846 = L_844;
		il2cpp_codegen_initobj((&V_1), sizeof(TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F ));
		(&V_1)->set_Value_2((uint16_t)((int32_t)63));
		(&V_1)->set_NextZeroIdx_0((uint16_t)0);
		(&V_1)->set_NextOneIdx_1((uint16_t)0);
		TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F  L_847 = V_1;
		NullCheck(L_846);
		(L_846)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)165)), (TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F )L_847);
		TreeNodeU5BU5D_t5032BD830284F31E0A94DEB0072DB2B07126BABD* L_848 = L_846;
		il2cpp_codegen_initobj((&V_1), sizeof(TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F ));
		(&V_1)->set_Value_2((uint16_t)0);
		(&V_1)->set_NextZeroIdx_0((uint16_t)((int32_t)167));
		(&V_1)->set_NextOneIdx_1((uint16_t)((int32_t)215));
		TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F  L_849 = V_1;
		NullCheck(L_848);
		(L_848)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)166)), (TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F )L_849);
		TreeNodeU5BU5D_t5032BD830284F31E0A94DEB0072DB2B07126BABD* L_850 = L_848;
		il2cpp_codegen_initobj((&V_1), sizeof(TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F ));
		(&V_1)->set_Value_2((uint16_t)((int32_t)64));
		(&V_1)->set_NextZeroIdx_0((uint16_t)0);
		(&V_1)->set_NextOneIdx_1((uint16_t)0);
		TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F  L_851 = V_1;
		NullCheck(L_850);
		(L_850)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)167)), (TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F )L_851);
		TreeNodeU5BU5D_t5032BD830284F31E0A94DEB0072DB2B07126BABD* L_852 = L_850;
		il2cpp_codegen_initobj((&V_1), sizeof(TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F ));
		(&V_1)->set_Value_2((uint16_t)((int32_t)65));
		(&V_1)->set_NextZeroIdx_0((uint16_t)0);
		(&V_1)->set_NextOneIdx_1((uint16_t)0);
		TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F  L_853 = V_1;
		NullCheck(L_852);
		(L_852)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)168)), (TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F )L_853);
		TreeNodeU5BU5D_t5032BD830284F31E0A94DEB0072DB2B07126BABD* L_854 = L_852;
		il2cpp_codegen_initobj((&V_1), sizeof(TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F ));
		(&V_1)->set_Value_2((uint16_t)((int32_t)66));
		(&V_1)->set_NextZeroIdx_0((uint16_t)0);
		(&V_1)->set_NextOneIdx_1((uint16_t)0);
		TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F  L_855 = V_1;
		NullCheck(L_854);
		(L_854)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)169)), (TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F )L_855);
		TreeNodeU5BU5D_t5032BD830284F31E0A94DEB0072DB2B07126BABD* L_856 = L_854;
		il2cpp_codegen_initobj((&V_1), sizeof(TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F ));
		(&V_1)->set_Value_2((uint16_t)0);
		(&V_1)->set_NextZeroIdx_0((uint16_t)((int32_t)171));
		(&V_1)->set_NextOneIdx_1((uint16_t)((int32_t)172));
		TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F  L_857 = V_1;
		NullCheck(L_856);
		(L_856)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)170)), (TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F )L_857);
		TreeNodeU5BU5D_t5032BD830284F31E0A94DEB0072DB2B07126BABD* L_858 = L_856;
		il2cpp_codegen_initobj((&V_1), sizeof(TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F ));
		(&V_1)->set_Value_2((uint16_t)((int32_t)67));
		(&V_1)->set_NextZeroIdx_0((uint16_t)0);
		(&V_1)->set_NextOneIdx_1((uint16_t)0);
		TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F  L_859 = V_1;
		NullCheck(L_858);
		(L_858)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)171)), (TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F )L_859);
		TreeNodeU5BU5D_t5032BD830284F31E0A94DEB0072DB2B07126BABD* L_860 = L_858;
		il2cpp_codegen_initobj((&V_1), sizeof(TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F ));
		(&V_1)->set_Value_2((uint16_t)((int32_t)68));
		(&V_1)->set_NextZeroIdx_0((uint16_t)0);
		(&V_1)->set_NextOneIdx_1((uint16_t)0);
		TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F  L_861 = V_1;
		NullCheck(L_860);
		(L_860)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)172)), (TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F )L_861);
		TreeNodeU5BU5D_t5032BD830284F31E0A94DEB0072DB2B07126BABD* L_862 = L_860;
		il2cpp_codegen_initobj((&V_1), sizeof(TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F ));
		(&V_1)->set_Value_2((uint16_t)0);
		(&V_1)->set_NextZeroIdx_0((uint16_t)((int32_t)174));
		(&V_1)->set_NextOneIdx_1((uint16_t)((int32_t)189));
		TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F  L_863 = V_1;
		NullCheck(L_862);
		(L_862)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)173)), (TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F )L_863);
		TreeNodeU5BU5D_t5032BD830284F31E0A94DEB0072DB2B07126BABD* L_864 = L_862;
		il2cpp_codegen_initobj((&V_1), sizeof(TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F ));
		(&V_1)->set_Value_2((uint16_t)0);
		(&V_1)->set_NextZeroIdx_0((uint16_t)((int32_t)175));
		(&V_1)->set_NextOneIdx_1((uint16_t)((int32_t)182));
		TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F  L_865 = V_1;
		NullCheck(L_864);
		(L_864)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)174)), (TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F )L_865);
		TreeNodeU5BU5D_t5032BD830284F31E0A94DEB0072DB2B07126BABD* L_866 = L_864;
		il2cpp_codegen_initobj((&V_1), sizeof(TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F ));
		(&V_1)->set_Value_2((uint16_t)0);
		(&V_1)->set_NextZeroIdx_0((uint16_t)((int32_t)176));
		(&V_1)->set_NextOneIdx_1((uint16_t)((int32_t)179));
		TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F  L_867 = V_1;
		NullCheck(L_866);
		(L_866)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)175)), (TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F )L_867);
		TreeNodeU5BU5D_t5032BD830284F31E0A94DEB0072DB2B07126BABD* L_868 = L_866;
		il2cpp_codegen_initobj((&V_1), sizeof(TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F ));
		(&V_1)->set_Value_2((uint16_t)0);
		(&V_1)->set_NextZeroIdx_0((uint16_t)((int32_t)177));
		(&V_1)->set_NextOneIdx_1((uint16_t)((int32_t)178));
		TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F  L_869 = V_1;
		NullCheck(L_868);
		(L_868)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)176)), (TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F )L_869);
		TreeNodeU5BU5D_t5032BD830284F31E0A94DEB0072DB2B07126BABD* L_870 = L_868;
		il2cpp_codegen_initobj((&V_1), sizeof(TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F ));
		(&V_1)->set_Value_2((uint16_t)((int32_t)69));
		(&V_1)->set_NextZeroIdx_0((uint16_t)0);
		(&V_1)->set_NextOneIdx_1((uint16_t)0);
		TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F  L_871 = V_1;
		NullCheck(L_870);
		(L_870)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)177)), (TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F )L_871);
		TreeNodeU5BU5D_t5032BD830284F31E0A94DEB0072DB2B07126BABD* L_872 = L_870;
		il2cpp_codegen_initobj((&V_1), sizeof(TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F ));
		(&V_1)->set_Value_2((uint16_t)((int32_t)70));
		(&V_1)->set_NextZeroIdx_0((uint16_t)0);
		(&V_1)->set_NextOneIdx_1((uint16_t)0);
		TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F  L_873 = V_1;
		NullCheck(L_872);
		(L_872)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)178)), (TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F )L_873);
		TreeNodeU5BU5D_t5032BD830284F31E0A94DEB0072DB2B07126BABD* L_874 = L_872;
		il2cpp_codegen_initobj((&V_1), sizeof(TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F ));
		(&V_1)->set_Value_2((uint16_t)0);
		(&V_1)->set_NextZeroIdx_0((uint16_t)((int32_t)180));
		(&V_1)->set_NextOneIdx_1((uint16_t)((int32_t)181));
		TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F  L_875 = V_1;
		NullCheck(L_874);
		(L_874)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)179)), (TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F )L_875);
		TreeNodeU5BU5D_t5032BD830284F31E0A94DEB0072DB2B07126BABD* L_876 = L_874;
		il2cpp_codegen_initobj((&V_1), sizeof(TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F ));
		(&V_1)->set_Value_2((uint16_t)((int32_t)71));
		(&V_1)->set_NextZeroIdx_0((uint16_t)0);
		(&V_1)->set_NextOneIdx_1((uint16_t)0);
		TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F  L_877 = V_1;
		NullCheck(L_876);
		(L_876)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)180)), (TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F )L_877);
		TreeNodeU5BU5D_t5032BD830284F31E0A94DEB0072DB2B07126BABD* L_878 = L_876;
		il2cpp_codegen_initobj((&V_1), sizeof(TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F ));
		(&V_1)->set_Value_2((uint16_t)((int32_t)72));
		(&V_1)->set_NextZeroIdx_0((uint16_t)0);
		(&V_1)->set_NextOneIdx_1((uint16_t)0);
		TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F  L_879 = V_1;
		NullCheck(L_878);
		(L_878)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)181)), (TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F )L_879);
		TreeNodeU5BU5D_t5032BD830284F31E0A94DEB0072DB2B07126BABD* L_880 = L_878;
		il2cpp_codegen_initobj((&V_1), sizeof(TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F ));
		(&V_1)->set_Value_2((uint16_t)0);
		(&V_1)->set_NextZeroIdx_0((uint16_t)((int32_t)183));
		(&V_1)->set_NextOneIdx_1((uint16_t)((int32_t)186));
		TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F  L_881 = V_1;
		NullCheck(L_880);
		(L_880)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)182)), (TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F )L_881);
		TreeNodeU5BU5D_t5032BD830284F31E0A94DEB0072DB2B07126BABD* L_882 = L_880;
		il2cpp_codegen_initobj((&V_1), sizeof(TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F ));
		(&V_1)->set_Value_2((uint16_t)0);
		(&V_1)->set_NextZeroIdx_0((uint16_t)((int32_t)184));
		(&V_1)->set_NextOneIdx_1((uint16_t)((int32_t)185));
		TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F  L_883 = V_1;
		NullCheck(L_882);
		(L_882)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)183)), (TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F )L_883);
		TreeNodeU5BU5D_t5032BD830284F31E0A94DEB0072DB2B07126BABD* L_884 = L_882;
		il2cpp_codegen_initobj((&V_1), sizeof(TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F ));
		(&V_1)->set_Value_2((uint16_t)((int32_t)73));
		(&V_1)->set_NextZeroIdx_0((uint16_t)0);
		(&V_1)->set_NextOneIdx_1((uint16_t)0);
		TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F  L_885 = V_1;
		NullCheck(L_884);
		(L_884)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)184)), (TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F )L_885);
		TreeNodeU5BU5D_t5032BD830284F31E0A94DEB0072DB2B07126BABD* L_886 = L_884;
		il2cpp_codegen_initobj((&V_1), sizeof(TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F ));
		(&V_1)->set_Value_2((uint16_t)((int32_t)74));
		(&V_1)->set_NextZeroIdx_0((uint16_t)0);
		(&V_1)->set_NextOneIdx_1((uint16_t)0);
		TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F  L_887 = V_1;
		NullCheck(L_886);
		(L_886)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)185)), (TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F )L_887);
		TreeNodeU5BU5D_t5032BD830284F31E0A94DEB0072DB2B07126BABD* L_888 = L_886;
		il2cpp_codegen_initobj((&V_1), sizeof(TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F ));
		(&V_1)->set_Value_2((uint16_t)0);
		(&V_1)->set_NextZeroIdx_0((uint16_t)((int32_t)187));
		(&V_1)->set_NextOneIdx_1((uint16_t)((int32_t)188));
		TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F  L_889 = V_1;
		NullCheck(L_888);
		(L_888)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)186)), (TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F )L_889);
		TreeNodeU5BU5D_t5032BD830284F31E0A94DEB0072DB2B07126BABD* L_890 = L_888;
		il2cpp_codegen_initobj((&V_1), sizeof(TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F ));
		(&V_1)->set_Value_2((uint16_t)((int32_t)75));
		(&V_1)->set_NextZeroIdx_0((uint16_t)0);
		(&V_1)->set_NextOneIdx_1((uint16_t)0);
		TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F  L_891 = V_1;
		NullCheck(L_890);
		(L_890)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)187)), (TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F )L_891);
		TreeNodeU5BU5D_t5032BD830284F31E0A94DEB0072DB2B07126BABD* L_892 = L_890;
		il2cpp_codegen_initobj((&V_1), sizeof(TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F ));
		(&V_1)->set_Value_2((uint16_t)((int32_t)76));
		(&V_1)->set_NextZeroIdx_0((uint16_t)0);
		(&V_1)->set_NextOneIdx_1((uint16_t)0);
		TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F  L_893 = V_1;
		NullCheck(L_892);
		(L_892)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)188)), (TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F )L_893);
		TreeNodeU5BU5D_t5032BD830284F31E0A94DEB0072DB2B07126BABD* L_894 = L_892;
		il2cpp_codegen_initobj((&V_1), sizeof(TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F ));
		(&V_1)->set_Value_2((uint16_t)0);
		(&V_1)->set_NextZeroIdx_0((uint16_t)((int32_t)190));
		(&V_1)->set_NextOneIdx_1((uint16_t)((int32_t)197));
		TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F  L_895 = V_1;
		NullCheck(L_894);
		(L_894)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)189)), (TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F )L_895);
		TreeNodeU5BU5D_t5032BD830284F31E0A94DEB0072DB2B07126BABD* L_896 = L_894;
		il2cpp_codegen_initobj((&V_1), sizeof(TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F ));
		(&V_1)->set_Value_2((uint16_t)0);
		(&V_1)->set_NextZeroIdx_0((uint16_t)((int32_t)191));
		(&V_1)->set_NextOneIdx_1((uint16_t)((int32_t)194));
		TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F  L_897 = V_1;
		NullCheck(L_896);
		(L_896)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)190)), (TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F )L_897);
		TreeNodeU5BU5D_t5032BD830284F31E0A94DEB0072DB2B07126BABD* L_898 = L_896;
		il2cpp_codegen_initobj((&V_1), sizeof(TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F ));
		(&V_1)->set_Value_2((uint16_t)0);
		(&V_1)->set_NextZeroIdx_0((uint16_t)((int32_t)192));
		(&V_1)->set_NextOneIdx_1((uint16_t)((int32_t)193));
		TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F  L_899 = V_1;
		NullCheck(L_898);
		(L_898)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)191)), (TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F )L_899);
		TreeNodeU5BU5D_t5032BD830284F31E0A94DEB0072DB2B07126BABD* L_900 = L_898;
		il2cpp_codegen_initobj((&V_1), sizeof(TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F ));
		(&V_1)->set_Value_2((uint16_t)((int32_t)77));
		(&V_1)->set_NextZeroIdx_0((uint16_t)0);
		(&V_1)->set_NextOneIdx_1((uint16_t)0);
		TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F  L_901 = V_1;
		NullCheck(L_900);
		(L_900)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)192)), (TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F )L_901);
		TreeNodeU5BU5D_t5032BD830284F31E0A94DEB0072DB2B07126BABD* L_902 = L_900;
		il2cpp_codegen_initobj((&V_1), sizeof(TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F ));
		(&V_1)->set_Value_2((uint16_t)((int32_t)78));
		(&V_1)->set_NextZeroIdx_0((uint16_t)0);
		(&V_1)->set_NextOneIdx_1((uint16_t)0);
		TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F  L_903 = V_1;
		NullCheck(L_902);
		(L_902)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)193)), (TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F )L_903);
		TreeNodeU5BU5D_t5032BD830284F31E0A94DEB0072DB2B07126BABD* L_904 = L_902;
		il2cpp_codegen_initobj((&V_1), sizeof(TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F ));
		(&V_1)->set_Value_2((uint16_t)0);
		(&V_1)->set_NextZeroIdx_0((uint16_t)((int32_t)195));
		(&V_1)->set_NextOneIdx_1((uint16_t)((int32_t)196));
		TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F  L_905 = V_1;
		NullCheck(L_904);
		(L_904)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)194)), (TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F )L_905);
		TreeNodeU5BU5D_t5032BD830284F31E0A94DEB0072DB2B07126BABD* L_906 = L_904;
		il2cpp_codegen_initobj((&V_1), sizeof(TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F ));
		(&V_1)->set_Value_2((uint16_t)((int32_t)79));
		(&V_1)->set_NextZeroIdx_0((uint16_t)0);
		(&V_1)->set_NextOneIdx_1((uint16_t)0);
		TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F  L_907 = V_1;
		NullCheck(L_906);
		(L_906)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)195)), (TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F )L_907);
		TreeNodeU5BU5D_t5032BD830284F31E0A94DEB0072DB2B07126BABD* L_908 = L_906;
		il2cpp_codegen_initobj((&V_1), sizeof(TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F ));
		(&V_1)->set_Value_2((uint16_t)((int32_t)80));
		(&V_1)->set_NextZeroIdx_0((uint16_t)0);
		(&V_1)->set_NextOneIdx_1((uint16_t)0);
		TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F  L_909 = V_1;
		NullCheck(L_908);
		(L_908)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)196)), (TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F )L_909);
		TreeNodeU5BU5D_t5032BD830284F31E0A94DEB0072DB2B07126BABD* L_910 = L_908;
		il2cpp_codegen_initobj((&V_1), sizeof(TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F ));
		(&V_1)->set_Value_2((uint16_t)0);
		(&V_1)->set_NextZeroIdx_0((uint16_t)((int32_t)198));
		(&V_1)->set_NextOneIdx_1((uint16_t)((int32_t)201));
		TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F  L_911 = V_1;
		NullCheck(L_910);
		(L_910)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)197)), (TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F )L_911);
		TreeNodeU5BU5D_t5032BD830284F31E0A94DEB0072DB2B07126BABD* L_912 = L_910;
		il2cpp_codegen_initobj((&V_1), sizeof(TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F ));
		(&V_1)->set_Value_2((uint16_t)0);
		(&V_1)->set_NextZeroIdx_0((uint16_t)((int32_t)199));
		(&V_1)->set_NextOneIdx_1((uint16_t)((int32_t)200));
		TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F  L_913 = V_1;
		NullCheck(L_912);
		(L_912)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)198)), (TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F )L_913);
		TreeNodeU5BU5D_t5032BD830284F31E0A94DEB0072DB2B07126BABD* L_914 = L_912;
		il2cpp_codegen_initobj((&V_1), sizeof(TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F ));
		(&V_1)->set_Value_2((uint16_t)((int32_t)81));
		(&V_1)->set_NextZeroIdx_0((uint16_t)0);
		(&V_1)->set_NextOneIdx_1((uint16_t)0);
		TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F  L_915 = V_1;
		NullCheck(L_914);
		(L_914)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)199)), (TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F )L_915);
		TreeNodeU5BU5D_t5032BD830284F31E0A94DEB0072DB2B07126BABD* L_916 = L_914;
		il2cpp_codegen_initobj((&V_1), sizeof(TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F ));
		(&V_1)->set_Value_2((uint16_t)((int32_t)82));
		(&V_1)->set_NextZeroIdx_0((uint16_t)0);
		(&V_1)->set_NextOneIdx_1((uint16_t)0);
		TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F  L_917 = V_1;
		NullCheck(L_916);
		(L_916)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)200)), (TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F )L_917);
		TreeNodeU5BU5D_t5032BD830284F31E0A94DEB0072DB2B07126BABD* L_918 = L_916;
		il2cpp_codegen_initobj((&V_1), sizeof(TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F ));
		(&V_1)->set_Value_2((uint16_t)0);
		(&V_1)->set_NextZeroIdx_0((uint16_t)((int32_t)202));
		(&V_1)->set_NextOneIdx_1((uint16_t)((int32_t)203));
		TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F  L_919 = V_1;
		NullCheck(L_918);
		(L_918)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)201)), (TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F )L_919);
		TreeNodeU5BU5D_t5032BD830284F31E0A94DEB0072DB2B07126BABD* L_920 = L_918;
		il2cpp_codegen_initobj((&V_1), sizeof(TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F ));
		(&V_1)->set_Value_2((uint16_t)((int32_t)83));
		(&V_1)->set_NextZeroIdx_0((uint16_t)0);
		(&V_1)->set_NextOneIdx_1((uint16_t)0);
		TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F  L_921 = V_1;
		NullCheck(L_920);
		(L_920)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)202)), (TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F )L_921);
		TreeNodeU5BU5D_t5032BD830284F31E0A94DEB0072DB2B07126BABD* L_922 = L_920;
		il2cpp_codegen_initobj((&V_1), sizeof(TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F ));
		(&V_1)->set_Value_2((uint16_t)((int32_t)84));
		(&V_1)->set_NextZeroIdx_0((uint16_t)0);
		(&V_1)->set_NextOneIdx_1((uint16_t)0);
		TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F  L_923 = V_1;
		NullCheck(L_922);
		(L_922)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)203)), (TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F )L_923);
		TreeNodeU5BU5D_t5032BD830284F31E0A94DEB0072DB2B07126BABD* L_924 = L_922;
		il2cpp_codegen_initobj((&V_1), sizeof(TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F ));
		(&V_1)->set_Value_2((uint16_t)0);
		(&V_1)->set_NextZeroIdx_0((uint16_t)((int32_t)205));
		(&V_1)->set_NextOneIdx_1((uint16_t)((int32_t)242));
		TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F  L_925 = V_1;
		NullCheck(L_924);
		(L_924)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)204)), (TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F )L_925);
		TreeNodeU5BU5D_t5032BD830284F31E0A94DEB0072DB2B07126BABD* L_926 = L_924;
		il2cpp_codegen_initobj((&V_1), sizeof(TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F ));
		(&V_1)->set_Value_2((uint16_t)0);
		(&V_1)->set_NextZeroIdx_0((uint16_t)((int32_t)206));
		(&V_1)->set_NextOneIdx_1((uint16_t)((int32_t)209));
		TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F  L_927 = V_1;
		NullCheck(L_926);
		(L_926)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)205)), (TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F )L_927);
		TreeNodeU5BU5D_t5032BD830284F31E0A94DEB0072DB2B07126BABD* L_928 = L_926;
		il2cpp_codegen_initobj((&V_1), sizeof(TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F ));
		(&V_1)->set_Value_2((uint16_t)0);
		(&V_1)->set_NextZeroIdx_0((uint16_t)((int32_t)207));
		(&V_1)->set_NextOneIdx_1((uint16_t)((int32_t)208));
		TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F  L_929 = V_1;
		NullCheck(L_928);
		(L_928)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)206)), (TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F )L_929);
		TreeNodeU5BU5D_t5032BD830284F31E0A94DEB0072DB2B07126BABD* L_930 = L_928;
		il2cpp_codegen_initobj((&V_1), sizeof(TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F ));
		(&V_1)->set_Value_2((uint16_t)((int32_t)85));
		(&V_1)->set_NextZeroIdx_0((uint16_t)0);
		(&V_1)->set_NextOneIdx_1((uint16_t)0);
		TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F  L_931 = V_1;
		NullCheck(L_930);
		(L_930)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)207)), (TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F )L_931);
		TreeNodeU5BU5D_t5032BD830284F31E0A94DEB0072DB2B07126BABD* L_932 = L_930;
		il2cpp_codegen_initobj((&V_1), sizeof(TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F ));
		(&V_1)->set_Value_2((uint16_t)((int32_t)86));
		(&V_1)->set_NextZeroIdx_0((uint16_t)0);
		(&V_1)->set_NextOneIdx_1((uint16_t)0);
		TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F  L_933 = V_1;
		NullCheck(L_932);
		(L_932)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)208)), (TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F )L_933);
		TreeNodeU5BU5D_t5032BD830284F31E0A94DEB0072DB2B07126BABD* L_934 = L_932;
		il2cpp_codegen_initobj((&V_1), sizeof(TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F ));
		(&V_1)->set_Value_2((uint16_t)0);
		(&V_1)->set_NextZeroIdx_0((uint16_t)((int32_t)210));
		(&V_1)->set_NextOneIdx_1((uint16_t)((int32_t)213));
		TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F  L_935 = V_1;
		NullCheck(L_934);
		(L_934)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)209)), (TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F )L_935);
		TreeNodeU5BU5D_t5032BD830284F31E0A94DEB0072DB2B07126BABD* L_936 = L_934;
		il2cpp_codegen_initobj((&V_1), sizeof(TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F ));
		(&V_1)->set_Value_2((uint16_t)((int32_t)87));
		(&V_1)->set_NextZeroIdx_0((uint16_t)0);
		(&V_1)->set_NextOneIdx_1((uint16_t)0);
		TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F  L_937 = V_1;
		NullCheck(L_936);
		(L_936)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)210)), (TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F )L_937);
		TreeNodeU5BU5D_t5032BD830284F31E0A94DEB0072DB2B07126BABD* L_938 = L_936;
		il2cpp_codegen_initobj((&V_1), sizeof(TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F ));
		(&V_1)->set_Value_2((uint16_t)0);
		(&V_1)->set_NextZeroIdx_0((uint16_t)((int32_t)212));
		(&V_1)->set_NextOneIdx_1((uint16_t)((int32_t)214));
		TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F  L_939 = V_1;
		NullCheck(L_938);
		(L_938)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)211)), (TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F )L_939);
		TreeNodeU5BU5D_t5032BD830284F31E0A94DEB0072DB2B07126BABD* L_940 = L_938;
		il2cpp_codegen_initobj((&V_1), sizeof(TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F ));
		(&V_1)->set_Value_2((uint16_t)((int32_t)88));
		(&V_1)->set_NextZeroIdx_0((uint16_t)0);
		(&V_1)->set_NextOneIdx_1((uint16_t)0);
		TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F  L_941 = V_1;
		NullCheck(L_940);
		(L_940)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)212)), (TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F )L_941);
		TreeNodeU5BU5D_t5032BD830284F31E0A94DEB0072DB2B07126BABD* L_942 = L_940;
		il2cpp_codegen_initobj((&V_1), sizeof(TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F ));
		(&V_1)->set_Value_2((uint16_t)((int32_t)89));
		(&V_1)->set_NextZeroIdx_0((uint16_t)0);
		(&V_1)->set_NextOneIdx_1((uint16_t)0);
		TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F  L_943 = V_1;
		NullCheck(L_942);
		(L_942)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)213)), (TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F )L_943);
		TreeNodeU5BU5D_t5032BD830284F31E0A94DEB0072DB2B07126BABD* L_944 = L_942;
		il2cpp_codegen_initobj((&V_1), sizeof(TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F ));
		(&V_1)->set_Value_2((uint16_t)((int32_t)90));
		(&V_1)->set_NextZeroIdx_0((uint16_t)0);
		(&V_1)->set_NextOneIdx_1((uint16_t)0);
		TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F  L_945 = V_1;
		NullCheck(L_944);
		(L_944)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)214)), (TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F )L_945);
		TreeNodeU5BU5D_t5032BD830284F31E0A94DEB0072DB2B07126BABD* L_946 = L_944;
		il2cpp_codegen_initobj((&V_1), sizeof(TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F ));
		(&V_1)->set_Value_2((uint16_t)((int32_t)91));
		(&V_1)->set_NextZeroIdx_0((uint16_t)0);
		(&V_1)->set_NextOneIdx_1((uint16_t)0);
		TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F  L_947 = V_1;
		NullCheck(L_946);
		(L_946)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)215)), (TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F )L_947);
		TreeNodeU5BU5D_t5032BD830284F31E0A94DEB0072DB2B07126BABD* L_948 = L_946;
		il2cpp_codegen_initobj((&V_1), sizeof(TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F ));
		(&V_1)->set_Value_2((uint16_t)0);
		(&V_1)->set_NextZeroIdx_0((uint16_t)((int32_t)217));
		(&V_1)->set_NextOneIdx_1((uint16_t)((int32_t)286));
		TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F  L_949 = V_1;
		NullCheck(L_948);
		(L_948)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)216)), (TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F )L_949);
		TreeNodeU5BU5D_t5032BD830284F31E0A94DEB0072DB2B07126BABD* L_950 = L_948;
		il2cpp_codegen_initobj((&V_1), sizeof(TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F ));
		(&V_1)->set_Value_2((uint16_t)0);
		(&V_1)->set_NextZeroIdx_0((uint16_t)((int32_t)218));
		(&V_1)->set_NextOneIdx_1((uint16_t)((int32_t)276));
		TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F  L_951 = V_1;
		NullCheck(L_950);
		(L_950)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)217)), (TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F )L_951);
		TreeNodeU5BU5D_t5032BD830284F31E0A94DEB0072DB2B07126BABD* L_952 = L_950;
		il2cpp_codegen_initobj((&V_1), sizeof(TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F ));
		(&V_1)->set_Value_2((uint16_t)0);
		(&V_1)->set_NextZeroIdx_0((uint16_t)((int32_t)219));
		(&V_1)->set_NextOneIdx_1((uint16_t)((int32_t)410));
		TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F  L_953 = V_1;
		NullCheck(L_952);
		(L_952)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)218)), (TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F )L_953);
		TreeNodeU5BU5D_t5032BD830284F31E0A94DEB0072DB2B07126BABD* L_954 = L_952;
		il2cpp_codegen_initobj((&V_1), sizeof(TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F ));
		(&V_1)->set_Value_2((uint16_t)((int32_t)92));
		(&V_1)->set_NextZeroIdx_0((uint16_t)0);
		(&V_1)->set_NextOneIdx_1((uint16_t)0);
		TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F  L_955 = V_1;
		NullCheck(L_954);
		(L_954)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)219)), (TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F )L_955);
		TreeNodeU5BU5D_t5032BD830284F31E0A94DEB0072DB2B07126BABD* L_956 = L_954;
		il2cpp_codegen_initobj((&V_1), sizeof(TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F ));
		(&V_1)->set_Value_2((uint16_t)0);
		(&V_1)->set_NextZeroIdx_0((uint16_t)((int32_t)221));
		(&V_1)->set_NextOneIdx_1((uint16_t)((int32_t)273));
		TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F  L_957 = V_1;
		NullCheck(L_956);
		(L_956)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)220)), (TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F )L_957);
		TreeNodeU5BU5D_t5032BD830284F31E0A94DEB0072DB2B07126BABD* L_958 = L_956;
		il2cpp_codegen_initobj((&V_1), sizeof(TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F ));
		(&V_1)->set_Value_2((uint16_t)((int32_t)93));
		(&V_1)->set_NextZeroIdx_0((uint16_t)0);
		(&V_1)->set_NextOneIdx_1((uint16_t)0);
		TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F  L_959 = V_1;
		NullCheck(L_958);
		(L_958)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)221)), (TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F )L_959);
		TreeNodeU5BU5D_t5032BD830284F31E0A94DEB0072DB2B07126BABD* L_960 = L_958;
		il2cpp_codegen_initobj((&V_1), sizeof(TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F ));
		(&V_1)->set_Value_2((uint16_t)0);
		(&V_1)->set_NextZeroIdx_0((uint16_t)((int32_t)223));
		(&V_1)->set_NextOneIdx_1((uint16_t)((int32_t)272));
		TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F  L_961 = V_1;
		NullCheck(L_960);
		(L_960)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)222)), (TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F )L_961);
		TreeNodeU5BU5D_t5032BD830284F31E0A94DEB0072DB2B07126BABD* L_962 = L_960;
		il2cpp_codegen_initobj((&V_1), sizeof(TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F ));
		(&V_1)->set_Value_2((uint16_t)((int32_t)94));
		(&V_1)->set_NextZeroIdx_0((uint16_t)0);
		(&V_1)->set_NextOneIdx_1((uint16_t)0);
		TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F  L_963 = V_1;
		NullCheck(L_962);
		(L_962)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)223)), (TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F )L_963);
		TreeNodeU5BU5D_t5032BD830284F31E0A94DEB0072DB2B07126BABD* L_964 = L_962;
		il2cpp_codegen_initobj((&V_1), sizeof(TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F ));
		(&V_1)->set_Value_2((uint16_t)0);
		(&V_1)->set_NextZeroIdx_0((uint16_t)((int32_t)225));
		(&V_1)->set_NextOneIdx_1((uint16_t)((int32_t)228));
		TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F  L_965 = V_1;
		NullCheck(L_964);
		(L_964)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)224)), (TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F )L_965);
		TreeNodeU5BU5D_t5032BD830284F31E0A94DEB0072DB2B07126BABD* L_966 = L_964;
		il2cpp_codegen_initobj((&V_1), sizeof(TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F ));
		(&V_1)->set_Value_2((uint16_t)((int32_t)95));
		(&V_1)->set_NextZeroIdx_0((uint16_t)0);
		(&V_1)->set_NextOneIdx_1((uint16_t)0);
		TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F  L_967 = V_1;
		NullCheck(L_966);
		(L_966)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)225)), (TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F )L_967);
		TreeNodeU5BU5D_t5032BD830284F31E0A94DEB0072DB2B07126BABD* L_968 = L_966;
		il2cpp_codegen_initobj((&V_1), sizeof(TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F ));
		(&V_1)->set_Value_2((uint16_t)((int32_t)96));
		(&V_1)->set_NextZeroIdx_0((uint16_t)0);
		(&V_1)->set_NextOneIdx_1((uint16_t)0);
		TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F  L_969 = V_1;
		NullCheck(L_968);
		(L_968)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)226)), (TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F )L_969);
		TreeNodeU5BU5D_t5032BD830284F31E0A94DEB0072DB2B07126BABD* L_970 = L_968;
		il2cpp_codegen_initobj((&V_1), sizeof(TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F ));
		(&V_1)->set_Value_2((uint16_t)((int32_t)97));
		(&V_1)->set_NextZeroIdx_0((uint16_t)0);
		(&V_1)->set_NextOneIdx_1((uint16_t)0);
		TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F  L_971 = V_1;
		NullCheck(L_970);
		(L_970)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)227)), (TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F )L_971);
		TreeNodeU5BU5D_t5032BD830284F31E0A94DEB0072DB2B07126BABD* L_972 = L_970;
		il2cpp_codegen_initobj((&V_1), sizeof(TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F ));
		(&V_1)->set_Value_2((uint16_t)((int32_t)98));
		(&V_1)->set_NextZeroIdx_0((uint16_t)0);
		(&V_1)->set_NextOneIdx_1((uint16_t)0);
		TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F  L_973 = V_1;
		NullCheck(L_972);
		(L_972)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)228)), (TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F )L_973);
		TreeNodeU5BU5D_t5032BD830284F31E0A94DEB0072DB2B07126BABD* L_974 = L_972;
		il2cpp_codegen_initobj((&V_1), sizeof(TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F ));
		(&V_1)->set_Value_2((uint16_t)0);
		(&V_1)->set_NextZeroIdx_0((uint16_t)((int32_t)230));
		(&V_1)->set_NextOneIdx_1((uint16_t)((int32_t)240));
		TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F  L_975 = V_1;
		NullCheck(L_974);
		(L_974)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)229)), (TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F )L_975);
		TreeNodeU5BU5D_t5032BD830284F31E0A94DEB0072DB2B07126BABD* L_976 = L_974;
		il2cpp_codegen_initobj((&V_1), sizeof(TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F ));
		(&V_1)->set_Value_2((uint16_t)0);
		(&V_1)->set_NextZeroIdx_0((uint16_t)((int32_t)231));
		(&V_1)->set_NextOneIdx_1((uint16_t)((int32_t)235));
		TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F  L_977 = V_1;
		NullCheck(L_976);
		(L_976)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)230)), (TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F )L_977);
		TreeNodeU5BU5D_t5032BD830284F31E0A94DEB0072DB2B07126BABD* L_978 = L_976;
		il2cpp_codegen_initobj((&V_1), sizeof(TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F ));
		(&V_1)->set_Value_2((uint16_t)((int32_t)99));
		(&V_1)->set_NextZeroIdx_0((uint16_t)0);
		(&V_1)->set_NextOneIdx_1((uint16_t)0);
		TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F  L_979 = V_1;
		NullCheck(L_978);
		(L_978)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)231)), (TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F )L_979);
		TreeNodeU5BU5D_t5032BD830284F31E0A94DEB0072DB2B07126BABD* L_980 = L_978;
		il2cpp_codegen_initobj((&V_1), sizeof(TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F ));
		(&V_1)->set_Value_2((uint16_t)0);
		(&V_1)->set_NextZeroIdx_0((uint16_t)((int32_t)233));
		(&V_1)->set_NextOneIdx_1((uint16_t)((int32_t)237));
		TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F  L_981 = V_1;
		NullCheck(L_980);
		(L_980)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)232)), (TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F )L_981);
		TreeNodeU5BU5D_t5032BD830284F31E0A94DEB0072DB2B07126BABD* L_982 = L_980;
		il2cpp_codegen_initobj((&V_1), sizeof(TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F ));
		(&V_1)->set_Value_2((uint16_t)0);
		(&V_1)->set_NextZeroIdx_0((uint16_t)((int32_t)234));
		(&V_1)->set_NextOneIdx_1((uint16_t)((int32_t)236));
		TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F  L_983 = V_1;
		NullCheck(L_982);
		(L_982)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)233)), (TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F )L_983);
		TreeNodeU5BU5D_t5032BD830284F31E0A94DEB0072DB2B07126BABD* L_984 = L_982;
		il2cpp_codegen_initobj((&V_1), sizeof(TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F ));
		(&V_1)->set_Value_2((uint16_t)((int32_t)100));
		(&V_1)->set_NextZeroIdx_0((uint16_t)0);
		(&V_1)->set_NextOneIdx_1((uint16_t)0);
		TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F  L_985 = V_1;
		NullCheck(L_984);
		(L_984)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)234)), (TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F )L_985);
		TreeNodeU5BU5D_t5032BD830284F31E0A94DEB0072DB2B07126BABD* L_986 = L_984;
		il2cpp_codegen_initobj((&V_1), sizeof(TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F ));
		(&V_1)->set_Value_2((uint16_t)((int32_t)101));
		(&V_1)->set_NextZeroIdx_0((uint16_t)0);
		(&V_1)->set_NextOneIdx_1((uint16_t)0);
		TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F  L_987 = V_1;
		NullCheck(L_986);
		(L_986)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)235)), (TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F )L_987);
		TreeNodeU5BU5D_t5032BD830284F31E0A94DEB0072DB2B07126BABD* L_988 = L_986;
		il2cpp_codegen_initobj((&V_1), sizeof(TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F ));
		(&V_1)->set_Value_2((uint16_t)((int32_t)102));
		(&V_1)->set_NextZeroIdx_0((uint16_t)0);
		(&V_1)->set_NextOneIdx_1((uint16_t)0);
		TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F  L_989 = V_1;
		NullCheck(L_988);
		(L_988)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)236)), (TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F )L_989);
		TreeNodeU5BU5D_t5032BD830284F31E0A94DEB0072DB2B07126BABD* L_990 = L_988;
		il2cpp_codegen_initobj((&V_1), sizeof(TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F ));
		(&V_1)->set_Value_2((uint16_t)0);
		(&V_1)->set_NextZeroIdx_0((uint16_t)((int32_t)238));
		(&V_1)->set_NextOneIdx_1((uint16_t)((int32_t)239));
		TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F  L_991 = V_1;
		NullCheck(L_990);
		(L_990)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)237)), (TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F )L_991);
		TreeNodeU5BU5D_t5032BD830284F31E0A94DEB0072DB2B07126BABD* L_992 = L_990;
		il2cpp_codegen_initobj((&V_1), sizeof(TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F ));
		(&V_1)->set_Value_2((uint16_t)((int32_t)103));
		(&V_1)->set_NextZeroIdx_0((uint16_t)0);
		(&V_1)->set_NextOneIdx_1((uint16_t)0);
		TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F  L_993 = V_1;
		NullCheck(L_992);
		(L_992)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)238)), (TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F )L_993);
		TreeNodeU5BU5D_t5032BD830284F31E0A94DEB0072DB2B07126BABD* L_994 = L_992;
		il2cpp_codegen_initobj((&V_1), sizeof(TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F ));
		(&V_1)->set_Value_2((uint16_t)((int32_t)104));
		(&V_1)->set_NextZeroIdx_0((uint16_t)0);
		(&V_1)->set_NextOneIdx_1((uint16_t)0);
		TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F  L_995 = V_1;
		NullCheck(L_994);
		(L_994)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)239)), (TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F )L_995);
		TreeNodeU5BU5D_t5032BD830284F31E0A94DEB0072DB2B07126BABD* L_996 = L_994;
		il2cpp_codegen_initobj((&V_1), sizeof(TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F ));
		(&V_1)->set_Value_2((uint16_t)0);
		(&V_1)->set_NextZeroIdx_0((uint16_t)((int32_t)241));
		(&V_1)->set_NextOneIdx_1((uint16_t)((int32_t)252));
		TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F  L_997 = V_1;
		NullCheck(L_996);
		(L_996)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)240)), (TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F )L_997);
		TreeNodeU5BU5D_t5032BD830284F31E0A94DEB0072DB2B07126BABD* L_998 = L_996;
		il2cpp_codegen_initobj((&V_1), sizeof(TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F ));
		(&V_1)->set_Value_2((uint16_t)((int32_t)105));
		(&V_1)->set_NextZeroIdx_0((uint16_t)0);
		(&V_1)->set_NextOneIdx_1((uint16_t)0);
		TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F  L_999 = V_1;
		NullCheck(L_998);
		(L_998)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)241)), (TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F )L_999);
		TreeNodeU5BU5D_t5032BD830284F31E0A94DEB0072DB2B07126BABD* L_1000 = L_998;
		il2cpp_codegen_initobj((&V_1), sizeof(TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F ));
		(&V_1)->set_Value_2((uint16_t)0);
		(&V_1)->set_NextZeroIdx_0((uint16_t)((int32_t)243));
		(&V_1)->set_NextOneIdx_1((uint16_t)((int32_t)254));
		TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F  L_1001 = V_1;
		NullCheck(L_1000);
		(L_1000)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)242)), (TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F )L_1001);
		TreeNodeU5BU5D_t5032BD830284F31E0A94DEB0072DB2B07126BABD* L_1002 = L_1000;
		il2cpp_codegen_initobj((&V_1), sizeof(TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F ));
		(&V_1)->set_Value_2((uint16_t)0);
		(&V_1)->set_NextZeroIdx_0((uint16_t)((int32_t)244));
		(&V_1)->set_NextOneIdx_1((uint16_t)((int32_t)245));
		TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F  L_1003 = V_1;
		NullCheck(L_1002);
		(L_1002)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)243)), (TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F )L_1003);
		TreeNodeU5BU5D_t5032BD830284F31E0A94DEB0072DB2B07126BABD* L_1004 = L_1002;
		il2cpp_codegen_initobj((&V_1), sizeof(TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F ));
		(&V_1)->set_Value_2((uint16_t)((int32_t)106));
		(&V_1)->set_NextZeroIdx_0((uint16_t)0);
		(&V_1)->set_NextOneIdx_1((uint16_t)0);
		TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F  L_1005 = V_1;
		NullCheck(L_1004);
		(L_1004)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)244)), (TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F )L_1005);
		TreeNodeU5BU5D_t5032BD830284F31E0A94DEB0072DB2B07126BABD* L_1006 = L_1004;
		il2cpp_codegen_initobj((&V_1), sizeof(TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F ));
		(&V_1)->set_Value_2((uint16_t)((int32_t)107));
		(&V_1)->set_NextZeroIdx_0((uint16_t)0);
		(&V_1)->set_NextOneIdx_1((uint16_t)0);
		TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F  L_1007 = V_1;
		NullCheck(L_1006);
		(L_1006)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)245)), (TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F )L_1007);
		TreeNodeU5BU5D_t5032BD830284F31E0A94DEB0072DB2B07126BABD* L_1008 = L_1006;
		il2cpp_codegen_initobj((&V_1), sizeof(TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F ));
		(&V_1)->set_Value_2((uint16_t)0);
		(&V_1)->set_NextZeroIdx_0((uint16_t)((int32_t)247));
		(&V_1)->set_NextOneIdx_1((uint16_t)((int32_t)250));
		TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F  L_1009 = V_1;
		NullCheck(L_1008);
		(L_1008)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)246)), (TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F )L_1009);
		TreeNodeU5BU5D_t5032BD830284F31E0A94DEB0072DB2B07126BABD* L_1010 = L_1008;
		il2cpp_codegen_initobj((&V_1), sizeof(TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F ));
		(&V_1)->set_Value_2((uint16_t)0);
		(&V_1)->set_NextZeroIdx_0((uint16_t)((int32_t)248));
		(&V_1)->set_NextOneIdx_1((uint16_t)((int32_t)249));
		TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F  L_1011 = V_1;
		NullCheck(L_1010);
		(L_1010)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)247)), (TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F )L_1011);
		TreeNodeU5BU5D_t5032BD830284F31E0A94DEB0072DB2B07126BABD* L_1012 = L_1010;
		il2cpp_codegen_initobj((&V_1), sizeof(TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F ));
		(&V_1)->set_Value_2((uint16_t)((int32_t)108));
		(&V_1)->set_NextZeroIdx_0((uint16_t)0);
		(&V_1)->set_NextOneIdx_1((uint16_t)0);
		TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F  L_1013 = V_1;
		NullCheck(L_1012);
		(L_1012)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)248)), (TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F )L_1013);
		TreeNodeU5BU5D_t5032BD830284F31E0A94DEB0072DB2B07126BABD* L_1014 = L_1012;
		il2cpp_codegen_initobj((&V_1), sizeof(TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F ));
		(&V_1)->set_Value_2((uint16_t)((int32_t)109));
		(&V_1)->set_NextZeroIdx_0((uint16_t)0);
		(&V_1)->set_NextOneIdx_1((uint16_t)0);
		TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F  L_1015 = V_1;
		NullCheck(L_1014);
		(L_1014)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)249)), (TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F )L_1015);
		TreeNodeU5BU5D_t5032BD830284F31E0A94DEB0072DB2B07126BABD* L_1016 = L_1014;
		il2cpp_codegen_initobj((&V_1), sizeof(TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F ));
		(&V_1)->set_Value_2((uint16_t)0);
		(&V_1)->set_NextZeroIdx_0((uint16_t)((int32_t)251));
		(&V_1)->set_NextOneIdx_1((uint16_t)((int32_t)253));
		TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F  L_1017 = V_1;
		NullCheck(L_1016);
		(L_1016)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)250)), (TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F )L_1017);
		TreeNodeU5BU5D_t5032BD830284F31E0A94DEB0072DB2B07126BABD* L_1018 = L_1016;
		il2cpp_codegen_initobj((&V_1), sizeof(TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F ));
		(&V_1)->set_Value_2((uint16_t)((int32_t)110));
		(&V_1)->set_NextZeroIdx_0((uint16_t)0);
		(&V_1)->set_NextOneIdx_1((uint16_t)0);
		TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F  L_1019 = V_1;
		NullCheck(L_1018);
		(L_1018)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)251)), (TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F )L_1019);
		TreeNodeU5BU5D_t5032BD830284F31E0A94DEB0072DB2B07126BABD* L_1020 = L_1018;
		il2cpp_codegen_initobj((&V_1), sizeof(TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F ));
		(&V_1)->set_Value_2((uint16_t)((int32_t)111));
		(&V_1)->set_NextZeroIdx_0((uint16_t)0);
		(&V_1)->set_NextOneIdx_1((uint16_t)0);
		TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F  L_1021 = V_1;
		NullCheck(L_1020);
		(L_1020)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)252)), (TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F )L_1021);
		TreeNodeU5BU5D_t5032BD830284F31E0A94DEB0072DB2B07126BABD* L_1022 = L_1020;
		il2cpp_codegen_initobj((&V_1), sizeof(TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F ));
		(&V_1)->set_Value_2((uint16_t)((int32_t)112));
		(&V_1)->set_NextZeroIdx_0((uint16_t)0);
		(&V_1)->set_NextOneIdx_1((uint16_t)0);
		TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F  L_1023 = V_1;
		NullCheck(L_1022);
		(L_1022)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)253)), (TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F )L_1023);
		TreeNodeU5BU5D_t5032BD830284F31E0A94DEB0072DB2B07126BABD* L_1024 = L_1022;
		il2cpp_codegen_initobj((&V_1), sizeof(TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F ));
		(&V_1)->set_Value_2((uint16_t)0);
		(&V_1)->set_NextZeroIdx_0((uint16_t)((int32_t)255));
		(&V_1)->set_NextOneIdx_1((uint16_t)((int32_t)262));
		TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F  L_1025 = V_1;
		NullCheck(L_1024);
		(L_1024)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)254)), (TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F )L_1025);
		TreeNodeU5BU5D_t5032BD830284F31E0A94DEB0072DB2B07126BABD* L_1026 = L_1024;
		il2cpp_codegen_initobj((&V_1), sizeof(TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F ));
		(&V_1)->set_Value_2((uint16_t)((int32_t)113));
		(&V_1)->set_NextZeroIdx_0((uint16_t)0);
		(&V_1)->set_NextOneIdx_1((uint16_t)0);
		TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F  L_1027 = V_1;
		NullCheck(L_1026);
		(L_1026)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)255)), (TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F )L_1027);
		TreeNodeU5BU5D_t5032BD830284F31E0A94DEB0072DB2B07126BABD* L_1028 = L_1026;
		il2cpp_codegen_initobj((&V_1), sizeof(TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F ));
		(&V_1)->set_Value_2((uint16_t)0);
		(&V_1)->set_NextZeroIdx_0((uint16_t)((int32_t)257));
		(&V_1)->set_NextOneIdx_1((uint16_t)((int32_t)261));
		TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F  L_1029 = V_1;
		NullCheck(L_1028);
		(L_1028)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)256)), (TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F )L_1029);
		TreeNodeU5BU5D_t5032BD830284F31E0A94DEB0072DB2B07126BABD* L_1030 = L_1028;
		il2cpp_codegen_initobj((&V_1), sizeof(TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F ));
		(&V_1)->set_Value_2((uint16_t)((int32_t)114));
		(&V_1)->set_NextZeroIdx_0((uint16_t)0);
		(&V_1)->set_NextOneIdx_1((uint16_t)0);
		TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F  L_1031 = V_1;
		NullCheck(L_1030);
		(L_1030)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)257)), (TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F )L_1031);
		TreeNodeU5BU5D_t5032BD830284F31E0A94DEB0072DB2B07126BABD* L_1032 = L_1030;
		il2cpp_codegen_initobj((&V_1), sizeof(TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F ));
		(&V_1)->set_Value_2((uint16_t)0);
		(&V_1)->set_NextZeroIdx_0((uint16_t)((int32_t)259));
		(&V_1)->set_NextOneIdx_1((uint16_t)((int32_t)260));
		TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F  L_1033 = V_1;
		NullCheck(L_1032);
		(L_1032)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)258)), (TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F )L_1033);
		TreeNodeU5BU5D_t5032BD830284F31E0A94DEB0072DB2B07126BABD* L_1034 = L_1032;
		il2cpp_codegen_initobj((&V_1), sizeof(TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F ));
		(&V_1)->set_Value_2((uint16_t)((int32_t)115));
		(&V_1)->set_NextZeroIdx_0((uint16_t)0);
		(&V_1)->set_NextOneIdx_1((uint16_t)0);
		TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F  L_1035 = V_1;
		NullCheck(L_1034);
		(L_1034)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)259)), (TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F )L_1035);
		TreeNodeU5BU5D_t5032BD830284F31E0A94DEB0072DB2B07126BABD* L_1036 = L_1034;
		il2cpp_codegen_initobj((&V_1), sizeof(TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F ));
		(&V_1)->set_Value_2((uint16_t)((int32_t)116));
		(&V_1)->set_NextZeroIdx_0((uint16_t)0);
		(&V_1)->set_NextOneIdx_1((uint16_t)0);
		TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F  L_1037 = V_1;
		NullCheck(L_1036);
		(L_1036)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)260)), (TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F )L_1037);
		TreeNodeU5BU5D_t5032BD830284F31E0A94DEB0072DB2B07126BABD* L_1038 = L_1036;
		il2cpp_codegen_initobj((&V_1), sizeof(TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F ));
		(&V_1)->set_Value_2((uint16_t)((int32_t)117));
		(&V_1)->set_NextZeroIdx_0((uint16_t)0);
		(&V_1)->set_NextOneIdx_1((uint16_t)0);
		TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F  L_1039 = V_1;
		NullCheck(L_1038);
		(L_1038)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)261)), (TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F )L_1039);
		TreeNodeU5BU5D_t5032BD830284F31E0A94DEB0072DB2B07126BABD* L_1040 = L_1038;
		il2cpp_codegen_initobj((&V_1), sizeof(TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F ));
		(&V_1)->set_Value_2((uint16_t)((int32_t)118));
		(&V_1)->set_NextZeroIdx_0((uint16_t)0);
		(&V_1)->set_NextOneIdx_1((uint16_t)0);
		TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F  L_1041 = V_1;
		NullCheck(L_1040);
		(L_1040)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)262)), (TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F )L_1041);
		TreeNodeU5BU5D_t5032BD830284F31E0A94DEB0072DB2B07126BABD* L_1042 = L_1040;
		il2cpp_codegen_initobj((&V_1), sizeof(TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F ));
		(&V_1)->set_Value_2((uint16_t)0);
		(&V_1)->set_NextZeroIdx_0((uint16_t)((int32_t)264));
		(&V_1)->set_NextOneIdx_1((uint16_t)((int32_t)267));
		TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F  L_1043 = V_1;
		NullCheck(L_1042);
		(L_1042)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)263)), (TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F )L_1043);
		TreeNodeU5BU5D_t5032BD830284F31E0A94DEB0072DB2B07126BABD* L_1044 = L_1042;
		il2cpp_codegen_initobj((&V_1), sizeof(TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F ));
		(&V_1)->set_Value_2((uint16_t)0);
		(&V_1)->set_NextZeroIdx_0((uint16_t)((int32_t)265));
		(&V_1)->set_NextOneIdx_1((uint16_t)((int32_t)266));
		TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F  L_1045 = V_1;
		NullCheck(L_1044);
		(L_1044)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)264)), (TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F )L_1045);
		TreeNodeU5BU5D_t5032BD830284F31E0A94DEB0072DB2B07126BABD* L_1046 = L_1044;
		il2cpp_codegen_initobj((&V_1), sizeof(TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F ));
		(&V_1)->set_Value_2((uint16_t)((int32_t)119));
		(&V_1)->set_NextZeroIdx_0((uint16_t)0);
		(&V_1)->set_NextOneIdx_1((uint16_t)0);
		TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F  L_1047 = V_1;
		NullCheck(L_1046);
		(L_1046)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)265)), (TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F )L_1047);
		TreeNodeU5BU5D_t5032BD830284F31E0A94DEB0072DB2B07126BABD* L_1048 = L_1046;
		il2cpp_codegen_initobj((&V_1), sizeof(TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F ));
		(&V_1)->set_Value_2((uint16_t)((int32_t)120));
		(&V_1)->set_NextZeroIdx_0((uint16_t)0);
		(&V_1)->set_NextOneIdx_1((uint16_t)0);
		TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F  L_1049 = V_1;
		NullCheck(L_1048);
		(L_1048)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)266)), (TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F )L_1049);
		TreeNodeU5BU5D_t5032BD830284F31E0A94DEB0072DB2B07126BABD* L_1050 = L_1048;
		il2cpp_codegen_initobj((&V_1), sizeof(TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F ));
		(&V_1)->set_Value_2((uint16_t)0);
		(&V_1)->set_NextZeroIdx_0((uint16_t)((int32_t)268));
		(&V_1)->set_NextOneIdx_1((uint16_t)((int32_t)269));
		TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F  L_1051 = V_1;
		NullCheck(L_1050);
		(L_1050)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)267)), (TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F )L_1051);
		TreeNodeU5BU5D_t5032BD830284F31E0A94DEB0072DB2B07126BABD* L_1052 = L_1050;
		il2cpp_codegen_initobj((&V_1), sizeof(TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F ));
		(&V_1)->set_Value_2((uint16_t)((int32_t)121));
		(&V_1)->set_NextZeroIdx_0((uint16_t)0);
		(&V_1)->set_NextOneIdx_1((uint16_t)0);
		TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F  L_1053 = V_1;
		NullCheck(L_1052);
		(L_1052)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)268)), (TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F )L_1053);
		TreeNodeU5BU5D_t5032BD830284F31E0A94DEB0072DB2B07126BABD* L_1054 = L_1052;
		il2cpp_codegen_initobj((&V_1), sizeof(TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F ));
		(&V_1)->set_Value_2((uint16_t)((int32_t)122));
		(&V_1)->set_NextZeroIdx_0((uint16_t)0);
		(&V_1)->set_NextOneIdx_1((uint16_t)0);
		TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F  L_1055 = V_1;
		NullCheck(L_1054);
		(L_1054)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)269)), (TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F )L_1055);
		TreeNodeU5BU5D_t5032BD830284F31E0A94DEB0072DB2B07126BABD* L_1056 = L_1054;
		il2cpp_codegen_initobj((&V_1), sizeof(TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F ));
		(&V_1)->set_Value_2((uint16_t)((int32_t)123));
		(&V_1)->set_NextZeroIdx_0((uint16_t)0);
		(&V_1)->set_NextOneIdx_1((uint16_t)0);
		TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F  L_1057 = V_1;
		NullCheck(L_1056);
		(L_1056)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)270)), (TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F )L_1057);
		TreeNodeU5BU5D_t5032BD830284F31E0A94DEB0072DB2B07126BABD* L_1058 = L_1056;
		il2cpp_codegen_initobj((&V_1), sizeof(TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F ));
		(&V_1)->set_Value_2((uint16_t)((int32_t)124));
		(&V_1)->set_NextZeroIdx_0((uint16_t)0);
		(&V_1)->set_NextOneIdx_1((uint16_t)0);
		TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F  L_1059 = V_1;
		NullCheck(L_1058);
		(L_1058)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)271)), (TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F )L_1059);
		TreeNodeU5BU5D_t5032BD830284F31E0A94DEB0072DB2B07126BABD* L_1060 = L_1058;
		il2cpp_codegen_initobj((&V_1), sizeof(TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F ));
		(&V_1)->set_Value_2((uint16_t)((int32_t)125));
		(&V_1)->set_NextZeroIdx_0((uint16_t)0);
		(&V_1)->set_NextOneIdx_1((uint16_t)0);
		TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F  L_1061 = V_1;
		NullCheck(L_1060);
		(L_1060)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)272)), (TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F )L_1061);
		TreeNodeU5BU5D_t5032BD830284F31E0A94DEB0072DB2B07126BABD* L_1062 = L_1060;
		il2cpp_codegen_initobj((&V_1), sizeof(TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F ));
		(&V_1)->set_Value_2((uint16_t)((int32_t)126));
		(&V_1)->set_NextZeroIdx_0((uint16_t)0);
		(&V_1)->set_NextOneIdx_1((uint16_t)0);
		TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F  L_1063 = V_1;
		NullCheck(L_1062);
		(L_1062)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)273)), (TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F )L_1063);
		TreeNodeU5BU5D_t5032BD830284F31E0A94DEB0072DB2B07126BABD* L_1064 = L_1062;
		il2cpp_codegen_initobj((&V_1), sizeof(TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F ));
		(&V_1)->set_Value_2((uint16_t)0);
		(&V_1)->set_NextZeroIdx_0((uint16_t)((int32_t)275));
		(&V_1)->set_NextOneIdx_1((uint16_t)((int32_t)459));
		TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F  L_1065 = V_1;
		NullCheck(L_1064);
		(L_1064)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)274)), (TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F )L_1065);
		TreeNodeU5BU5D_t5032BD830284F31E0A94DEB0072DB2B07126BABD* L_1066 = L_1064;
		il2cpp_codegen_initobj((&V_1), sizeof(TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F ));
		(&V_1)->set_Value_2((uint16_t)((int32_t)127));
		(&V_1)->set_NextZeroIdx_0((uint16_t)0);
		(&V_1)->set_NextOneIdx_1((uint16_t)0);
		TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F  L_1067 = V_1;
		NullCheck(L_1066);
		(L_1066)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)275)), (TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F )L_1067);
		TreeNodeU5BU5D_t5032BD830284F31E0A94DEB0072DB2B07126BABD* L_1068 = L_1066;
		il2cpp_codegen_initobj((&V_1), sizeof(TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F ));
		(&V_1)->set_Value_2((uint16_t)0);
		(&V_1)->set_NextZeroIdx_0((uint16_t)((int32_t)436));
		(&V_1)->set_NextOneIdx_1((uint16_t)((int32_t)277));
		TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F  L_1069 = V_1;
		NullCheck(L_1068);
		(L_1068)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)276)), (TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F )L_1069);
		TreeNodeU5BU5D_t5032BD830284F31E0A94DEB0072DB2B07126BABD* L_1070 = L_1068;
		il2cpp_codegen_initobj((&V_1), sizeof(TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F ));
		(&V_1)->set_Value_2((uint16_t)0);
		(&V_1)->set_NextZeroIdx_0((uint16_t)((int32_t)278));
		(&V_1)->set_NextOneIdx_1((uint16_t)((int32_t)285));
		TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F  L_1071 = V_1;
		NullCheck(L_1070);
		(L_1070)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)277)), (TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F )L_1071);
		TreeNodeU5BU5D_t5032BD830284F31E0A94DEB0072DB2B07126BABD* L_1072 = L_1070;
		il2cpp_codegen_initobj((&V_1), sizeof(TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F ));
		(&V_1)->set_Value_2((uint16_t)((int32_t)128));
		(&V_1)->set_NextZeroIdx_0((uint16_t)0);
		(&V_1)->set_NextOneIdx_1((uint16_t)0);
		TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F  L_1073 = V_1;
		NullCheck(L_1072);
		(L_1072)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)278)), (TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F )L_1073);
		TreeNodeU5BU5D_t5032BD830284F31E0A94DEB0072DB2B07126BABD* L_1074 = L_1072;
		il2cpp_codegen_initobj((&V_1), sizeof(TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F ));
		(&V_1)->set_Value_2((uint16_t)0);
		(&V_1)->set_NextZeroIdx_0((uint16_t)((int32_t)372));
		(&V_1)->set_NextOneIdx_1((uint16_t)((int32_t)280));
		TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F  L_1075 = V_1;
		NullCheck(L_1074);
		(L_1074)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)279)), (TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F )L_1075);
		TreeNodeU5BU5D_t5032BD830284F31E0A94DEB0072DB2B07126BABD* L_1076 = L_1074;
		il2cpp_codegen_initobj((&V_1), sizeof(TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F ));
		(&V_1)->set_Value_2((uint16_t)0);
		(&V_1)->set_NextZeroIdx_0((uint16_t)((int32_t)281));
		(&V_1)->set_NextOneIdx_1((uint16_t)((int32_t)332));
		TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F  L_1077 = V_1;
		NullCheck(L_1076);
		(L_1076)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)280)), (TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F )L_1077);
		TreeNodeU5BU5D_t5032BD830284F31E0A94DEB0072DB2B07126BABD* L_1078 = L_1076;
		il2cpp_codegen_initobj((&V_1), sizeof(TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F ));
		(&V_1)->set_Value_2((uint16_t)0);
		(&V_1)->set_NextZeroIdx_0((uint16_t)((int32_t)282));
		(&V_1)->set_NextOneIdx_1((uint16_t)((int32_t)291));
		TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F  L_1079 = V_1;
		NullCheck(L_1078);
		(L_1078)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)281)), (TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F )L_1079);
		TreeNodeU5BU5D_t5032BD830284F31E0A94DEB0072DB2B07126BABD* L_1080 = L_1078;
		il2cpp_codegen_initobj((&V_1), sizeof(TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F ));
		(&V_1)->set_Value_2((uint16_t)0);
		(&V_1)->set_NextZeroIdx_0((uint16_t)((int32_t)473));
		(&V_1)->set_NextOneIdx_1((uint16_t)((int32_t)283));
		TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F  L_1081 = V_1;
		NullCheck(L_1080);
		(L_1080)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)282)), (TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F )L_1081);
		TreeNodeU5BU5D_t5032BD830284F31E0A94DEB0072DB2B07126BABD* L_1082 = L_1080;
		il2cpp_codegen_initobj((&V_1), sizeof(TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F ));
		(&V_1)->set_Value_2((uint16_t)0);
		(&V_1)->set_NextZeroIdx_0((uint16_t)((int32_t)284));
		(&V_1)->set_NextOneIdx_1((uint16_t)((int32_t)290));
		TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F  L_1083 = V_1;
		NullCheck(L_1082);
		(L_1082)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)283)), (TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F )L_1083);
		TreeNodeU5BU5D_t5032BD830284F31E0A94DEB0072DB2B07126BABD* L_1084 = L_1082;
		il2cpp_codegen_initobj((&V_1), sizeof(TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F ));
		(&V_1)->set_Value_2((uint16_t)((int32_t)129));
		(&V_1)->set_NextZeroIdx_0((uint16_t)0);
		(&V_1)->set_NextOneIdx_1((uint16_t)0);
		TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F  L_1085 = V_1;
		NullCheck(L_1084);
		(L_1084)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)284)), (TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F )L_1085);
		TreeNodeU5BU5D_t5032BD830284F31E0A94DEB0072DB2B07126BABD* L_1086 = L_1084;
		il2cpp_codegen_initobj((&V_1), sizeof(TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F ));
		(&V_1)->set_Value_2((uint16_t)((int32_t)130));
		(&V_1)->set_NextZeroIdx_0((uint16_t)0);
		(&V_1)->set_NextOneIdx_1((uint16_t)0);
		TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F  L_1087 = V_1;
		NullCheck(L_1086);
		(L_1086)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)285)), (TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F )L_1087);
		TreeNodeU5BU5D_t5032BD830284F31E0A94DEB0072DB2B07126BABD* L_1088 = L_1086;
		il2cpp_codegen_initobj((&V_1), sizeof(TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F ));
		(&V_1)->set_Value_2((uint16_t)0);
		(&V_1)->set_NextZeroIdx_0((uint16_t)((int32_t)287));
		(&V_1)->set_NextOneIdx_1((uint16_t)((int32_t)328));
		TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F  L_1089 = V_1;
		NullCheck(L_1088);
		(L_1088)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)286)), (TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F )L_1089);
		TreeNodeU5BU5D_t5032BD830284F31E0A94DEB0072DB2B07126BABD* L_1090 = L_1088;
		il2cpp_codegen_initobj((&V_1), sizeof(TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F ));
		(&V_1)->set_Value_2((uint16_t)0);
		(&V_1)->set_NextZeroIdx_0((uint16_t)((int32_t)288));
		(&V_1)->set_NextOneIdx_1((uint16_t)((int32_t)388));
		TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F  L_1091 = V_1;
		NullCheck(L_1090);
		(L_1090)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)287)), (TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F )L_1091);
		TreeNodeU5BU5D_t5032BD830284F31E0A94DEB0072DB2B07126BABD* L_1092 = L_1090;
		il2cpp_codegen_initobj((&V_1), sizeof(TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F ));
		(&V_1)->set_Value_2((uint16_t)0);
		(&V_1)->set_NextZeroIdx_0((uint16_t)((int32_t)289));
		(&V_1)->set_NextOneIdx_1((uint16_t)((int32_t)345));
		TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F  L_1093 = V_1;
		NullCheck(L_1092);
		(L_1092)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)288)), (TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F )L_1093);
		TreeNodeU5BU5D_t5032BD830284F31E0A94DEB0072DB2B07126BABD* L_1094 = L_1092;
		il2cpp_codegen_initobj((&V_1), sizeof(TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F ));
		(&V_1)->set_Value_2((uint16_t)((int32_t)131));
		(&V_1)->set_NextZeroIdx_0((uint16_t)0);
		(&V_1)->set_NextOneIdx_1((uint16_t)0);
		TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F  L_1095 = V_1;
		NullCheck(L_1094);
		(L_1094)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)289)), (TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F )L_1095);
		TreeNodeU5BU5D_t5032BD830284F31E0A94DEB0072DB2B07126BABD* L_1096 = L_1094;
		il2cpp_codegen_initobj((&V_1), sizeof(TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F ));
		(&V_1)->set_Value_2((uint16_t)((int32_t)132));
		(&V_1)->set_NextZeroIdx_0((uint16_t)0);
		(&V_1)->set_NextOneIdx_1((uint16_t)0);
		TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F  L_1097 = V_1;
		NullCheck(L_1096);
		(L_1096)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)290)), (TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F )L_1097);
		TreeNodeU5BU5D_t5032BD830284F31E0A94DEB0072DB2B07126BABD* L_1098 = L_1096;
		il2cpp_codegen_initobj((&V_1), sizeof(TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F ));
		(&V_1)->set_Value_2((uint16_t)0);
		(&V_1)->set_NextZeroIdx_0((uint16_t)((int32_t)292));
		(&V_1)->set_NextOneIdx_1((uint16_t)((int32_t)296));
		TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F  L_1099 = V_1;
		NullCheck(L_1098);
		(L_1098)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)291)), (TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F )L_1099);
		TreeNodeU5BU5D_t5032BD830284F31E0A94DEB0072DB2B07126BABD* L_1100 = L_1098;
		il2cpp_codegen_initobj((&V_1), sizeof(TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F ));
		(&V_1)->set_Value_2((uint16_t)0);
		(&V_1)->set_NextZeroIdx_0((uint16_t)((int32_t)293));
		(&V_1)->set_NextOneIdx_1((uint16_t)((int32_t)294));
		TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F  L_1101 = V_1;
		NullCheck(L_1100);
		(L_1100)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)292)), (TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F )L_1101);
		TreeNodeU5BU5D_t5032BD830284F31E0A94DEB0072DB2B07126BABD* L_1102 = L_1100;
		il2cpp_codegen_initobj((&V_1), sizeof(TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F ));
		(&V_1)->set_Value_2((uint16_t)((int32_t)133));
		(&V_1)->set_NextZeroIdx_0((uint16_t)0);
		(&V_1)->set_NextOneIdx_1((uint16_t)0);
		TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F  L_1103 = V_1;
		NullCheck(L_1102);
		(L_1102)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)293)), (TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F )L_1103);
		TreeNodeU5BU5D_t5032BD830284F31E0A94DEB0072DB2B07126BABD* L_1104 = L_1102;
		il2cpp_codegen_initobj((&V_1), sizeof(TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F ));
		(&V_1)->set_Value_2((uint16_t)((int32_t)134));
		(&V_1)->set_NextZeroIdx_0((uint16_t)0);
		(&V_1)->set_NextOneIdx_1((uint16_t)0);
		TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F  L_1105 = V_1;
		NullCheck(L_1104);
		(L_1104)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)294)), (TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F )L_1105);
		TreeNodeU5BU5D_t5032BD830284F31E0A94DEB0072DB2B07126BABD* L_1106 = L_1104;
		il2cpp_codegen_initobj((&V_1), sizeof(TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F ));
		(&V_1)->set_Value_2((uint16_t)((int32_t)135));
		(&V_1)->set_NextZeroIdx_0((uint16_t)0);
		(&V_1)->set_NextOneIdx_1((uint16_t)0);
		TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F  L_1107 = V_1;
		NullCheck(L_1106);
		(L_1106)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)295)), (TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F )L_1107);
		TreeNodeU5BU5D_t5032BD830284F31E0A94DEB0072DB2B07126BABD* L_1108 = L_1106;
		il2cpp_codegen_initobj((&V_1), sizeof(TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F ));
		(&V_1)->set_Value_2((uint16_t)0);
		(&V_1)->set_NextZeroIdx_0((uint16_t)((int32_t)297));
		(&V_1)->set_NextOneIdx_1((uint16_t)((int32_t)313));
		TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F  L_1109 = V_1;
		NullCheck(L_1108);
		(L_1108)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)296)), (TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F )L_1109);
		TreeNodeU5BU5D_t5032BD830284F31E0A94DEB0072DB2B07126BABD* L_1110 = L_1108;
		il2cpp_codegen_initobj((&V_1), sizeof(TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F ));
		(&V_1)->set_Value_2((uint16_t)((int32_t)136));
		(&V_1)->set_NextZeroIdx_0((uint16_t)0);
		(&V_1)->set_NextOneIdx_1((uint16_t)0);
		TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F  L_1111 = V_1;
		NullCheck(L_1110);
		(L_1110)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)297)), (TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F )L_1111);
		TreeNodeU5BU5D_t5032BD830284F31E0A94DEB0072DB2B07126BABD* L_1112 = L_1110;
		il2cpp_codegen_initobj((&V_1), sizeof(TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F ));
		(&V_1)->set_Value_2((uint16_t)0);
		(&V_1)->set_NextZeroIdx_0((uint16_t)((int32_t)299));
		(&V_1)->set_NextOneIdx_1((uint16_t)((int32_t)300));
		TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F  L_1113 = V_1;
		NullCheck(L_1112);
		(L_1112)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)298)), (TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F )L_1113);
		TreeNodeU5BU5D_t5032BD830284F31E0A94DEB0072DB2B07126BABD* L_1114 = L_1112;
		il2cpp_codegen_initobj((&V_1), sizeof(TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F ));
		(&V_1)->set_Value_2((uint16_t)((int32_t)137));
		(&V_1)->set_NextZeroIdx_0((uint16_t)0);
		(&V_1)->set_NextOneIdx_1((uint16_t)0);
		TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F  L_1115 = V_1;
		NullCheck(L_1114);
		(L_1114)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)299)), (TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F )L_1115);
		TreeNodeU5BU5D_t5032BD830284F31E0A94DEB0072DB2B07126BABD* L_1116 = L_1114;
		il2cpp_codegen_initobj((&V_1), sizeof(TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F ));
		(&V_1)->set_Value_2((uint16_t)((int32_t)138));
		(&V_1)->set_NextZeroIdx_0((uint16_t)0);
		(&V_1)->set_NextOneIdx_1((uint16_t)0);
		TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F  L_1117 = V_1;
		NullCheck(L_1116);
		(L_1116)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)300)), (TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F )L_1117);
		TreeNodeU5BU5D_t5032BD830284F31E0A94DEB0072DB2B07126BABD* L_1118 = L_1116;
		il2cpp_codegen_initobj((&V_1), sizeof(TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F ));
		(&V_1)->set_Value_2((uint16_t)0);
		(&V_1)->set_NextZeroIdx_0((uint16_t)((int32_t)302));
		(&V_1)->set_NextOneIdx_1((uint16_t)((int32_t)305));
		TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F  L_1119 = V_1;
		NullCheck(L_1118);
		(L_1118)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)301)), (TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F )L_1119);
		TreeNodeU5BU5D_t5032BD830284F31E0A94DEB0072DB2B07126BABD* L_1120 = L_1118;
		il2cpp_codegen_initobj((&V_1), sizeof(TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F ));
		(&V_1)->set_Value_2((uint16_t)0);
		(&V_1)->set_NextZeroIdx_0((uint16_t)((int32_t)303));
		(&V_1)->set_NextOneIdx_1((uint16_t)((int32_t)304));
		TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F  L_1121 = V_1;
		NullCheck(L_1120);
		(L_1120)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)302)), (TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F )L_1121);
		TreeNodeU5BU5D_t5032BD830284F31E0A94DEB0072DB2B07126BABD* L_1122 = L_1120;
		il2cpp_codegen_initobj((&V_1), sizeof(TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F ));
		(&V_1)->set_Value_2((uint16_t)((int32_t)139));
		(&V_1)->set_NextZeroIdx_0((uint16_t)0);
		(&V_1)->set_NextOneIdx_1((uint16_t)0);
		TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F  L_1123 = V_1;
		NullCheck(L_1122);
		(L_1122)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)303)), (TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F )L_1123);
		TreeNodeU5BU5D_t5032BD830284F31E0A94DEB0072DB2B07126BABD* L_1124 = L_1122;
		il2cpp_codegen_initobj((&V_1), sizeof(TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F ));
		(&V_1)->set_Value_2((uint16_t)((int32_t)140));
		(&V_1)->set_NextZeroIdx_0((uint16_t)0);
		(&V_1)->set_NextOneIdx_1((uint16_t)0);
		TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F  L_1125 = V_1;
		NullCheck(L_1124);
		(L_1124)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)304)), (TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F )L_1125);
		TreeNodeU5BU5D_t5032BD830284F31E0A94DEB0072DB2B07126BABD* L_1126 = L_1124;
		il2cpp_codegen_initobj((&V_1), sizeof(TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F ));
		(&V_1)->set_Value_2((uint16_t)0);
		(&V_1)->set_NextZeroIdx_0((uint16_t)((int32_t)306));
		(&V_1)->set_NextOneIdx_1((uint16_t)((int32_t)308));
		TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F  L_1127 = V_1;
		NullCheck(L_1126);
		(L_1126)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)305)), (TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F )L_1127);
		TreeNodeU5BU5D_t5032BD830284F31E0A94DEB0072DB2B07126BABD* L_1128 = L_1126;
		il2cpp_codegen_initobj((&V_1), sizeof(TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F ));
		(&V_1)->set_Value_2((uint16_t)((int32_t)141));
		(&V_1)->set_NextZeroIdx_0((uint16_t)0);
		(&V_1)->set_NextOneIdx_1((uint16_t)0);
		TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F  L_1129 = V_1;
		NullCheck(L_1128);
		(L_1128)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)306)), (TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F )L_1129);
		TreeNodeU5BU5D_t5032BD830284F31E0A94DEB0072DB2B07126BABD* L_1130 = L_1128;
		il2cpp_codegen_initobj((&V_1), sizeof(TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F ));
		(&V_1)->set_Value_2((uint16_t)((int32_t)142));
		(&V_1)->set_NextZeroIdx_0((uint16_t)0);
		(&V_1)->set_NextOneIdx_1((uint16_t)0);
		TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F  L_1131 = V_1;
		NullCheck(L_1130);
		(L_1130)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)307)), (TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F )L_1131);
		TreeNodeU5BU5D_t5032BD830284F31E0A94DEB0072DB2B07126BABD* L_1132 = L_1130;
		il2cpp_codegen_initobj((&V_1), sizeof(TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F ));
		(&V_1)->set_Value_2((uint16_t)((int32_t)143));
		(&V_1)->set_NextZeroIdx_0((uint16_t)0);
		(&V_1)->set_NextOneIdx_1((uint16_t)0);
		TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F  L_1133 = V_1;
		NullCheck(L_1132);
		(L_1132)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)308)), (TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F )L_1133);
		TreeNodeU5BU5D_t5032BD830284F31E0A94DEB0072DB2B07126BABD* L_1134 = L_1132;
		il2cpp_codegen_initobj((&V_1), sizeof(TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F ));
		(&V_1)->set_Value_2((uint16_t)0);
		(&V_1)->set_NextZeroIdx_0((uint16_t)((int32_t)310));
		(&V_1)->set_NextOneIdx_1((uint16_t)((int32_t)319));
		TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F  L_1135 = V_1;
		NullCheck(L_1134);
		(L_1134)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)309)), (TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F )L_1135);
		TreeNodeU5BU5D_t5032BD830284F31E0A94DEB0072DB2B07126BABD* L_1136 = L_1134;
		il2cpp_codegen_initobj((&V_1), sizeof(TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F ));
		(&V_1)->set_Value_2((uint16_t)0);
		(&V_1)->set_NextZeroIdx_0((uint16_t)((int32_t)311));
		(&V_1)->set_NextOneIdx_1((uint16_t)((int32_t)312));
		TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F  L_1137 = V_1;
		NullCheck(L_1136);
		(L_1136)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)310)), (TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F )L_1137);
		TreeNodeU5BU5D_t5032BD830284F31E0A94DEB0072DB2B07126BABD* L_1138 = L_1136;
		il2cpp_codegen_initobj((&V_1), sizeof(TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F ));
		(&V_1)->set_Value_2((uint16_t)((int32_t)144));
		(&V_1)->set_NextZeroIdx_0((uint16_t)0);
		(&V_1)->set_NextOneIdx_1((uint16_t)0);
		TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F  L_1139 = V_1;
		NullCheck(L_1138);
		(L_1138)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)311)), (TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F )L_1139);
		TreeNodeU5BU5D_t5032BD830284F31E0A94DEB0072DB2B07126BABD* L_1140 = L_1138;
		il2cpp_codegen_initobj((&V_1), sizeof(TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F ));
		(&V_1)->set_Value_2((uint16_t)((int32_t)145));
		(&V_1)->set_NextZeroIdx_0((uint16_t)0);
		(&V_1)->set_NextOneIdx_1((uint16_t)0);
		TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F  L_1141 = V_1;
		NullCheck(L_1140);
		(L_1140)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)312)), (TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F )L_1141);
		TreeNodeU5BU5D_t5032BD830284F31E0A94DEB0072DB2B07126BABD* L_1142 = L_1140;
		il2cpp_codegen_initobj((&V_1), sizeof(TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F ));
		(&V_1)->set_Value_2((uint16_t)((int32_t)146));
		(&V_1)->set_NextZeroIdx_0((uint16_t)0);
		(&V_1)->set_NextOneIdx_1((uint16_t)0);
		TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F  L_1143 = V_1;
		NullCheck(L_1142);
		(L_1142)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)313)), (TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F )L_1143);
		TreeNodeU5BU5D_t5032BD830284F31E0A94DEB0072DB2B07126BABD* L_1144 = L_1142;
		il2cpp_codegen_initobj((&V_1), sizeof(TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F ));
		(&V_1)->set_Value_2((uint16_t)0);
		(&V_1)->set_NextZeroIdx_0((uint16_t)((int32_t)315));
		(&V_1)->set_NextOneIdx_1((uint16_t)((int32_t)350));
		TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F  L_1145 = V_1;
		NullCheck(L_1144);
		(L_1144)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)314)), (TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F )L_1145);
		TreeNodeU5BU5D_t5032BD830284F31E0A94DEB0072DB2B07126BABD* L_1146 = L_1144;
		il2cpp_codegen_initobj((&V_1), sizeof(TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F ));
		(&V_1)->set_Value_2((uint16_t)0);
		(&V_1)->set_NextZeroIdx_0((uint16_t)((int32_t)316));
		(&V_1)->set_NextOneIdx_1((uint16_t)((int32_t)325));
		TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F  L_1147 = V_1;
		NullCheck(L_1146);
		(L_1146)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)315)), (TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F )L_1147);
		TreeNodeU5BU5D_t5032BD830284F31E0A94DEB0072DB2B07126BABD* L_1148 = L_1146;
		il2cpp_codegen_initobj((&V_1), sizeof(TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F ));
		(&V_1)->set_Value_2((uint16_t)0);
		(&V_1)->set_NextZeroIdx_0((uint16_t)((int32_t)317));
		(&V_1)->set_NextOneIdx_1((uint16_t)((int32_t)322));
		TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F  L_1149 = V_1;
		NullCheck(L_1148);
		(L_1148)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)316)), (TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F )L_1149);
		TreeNodeU5BU5D_t5032BD830284F31E0A94DEB0072DB2B07126BABD* L_1150 = L_1148;
		il2cpp_codegen_initobj((&V_1), sizeof(TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F ));
		(&V_1)->set_Value_2((uint16_t)0);
		(&V_1)->set_NextZeroIdx_0((uint16_t)((int32_t)318));
		(&V_1)->set_NextOneIdx_1((uint16_t)((int32_t)321));
		TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F  L_1151 = V_1;
		NullCheck(L_1150);
		(L_1150)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)317)), (TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F )L_1151);
		TreeNodeU5BU5D_t5032BD830284F31E0A94DEB0072DB2B07126BABD* L_1152 = L_1150;
		il2cpp_codegen_initobj((&V_1), sizeof(TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F ));
		(&V_1)->set_Value_2((uint16_t)((int32_t)147));
		(&V_1)->set_NextZeroIdx_0((uint16_t)0);
		(&V_1)->set_NextOneIdx_1((uint16_t)0);
		TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F  L_1153 = V_1;
		NullCheck(L_1152);
		(L_1152)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)318)), (TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F )L_1153);
		TreeNodeU5BU5D_t5032BD830284F31E0A94DEB0072DB2B07126BABD* L_1154 = L_1152;
		il2cpp_codegen_initobj((&V_1), sizeof(TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F ));
		(&V_1)->set_Value_2((uint16_t)0);
		(&V_1)->set_NextZeroIdx_0((uint16_t)((int32_t)320));
		(&V_1)->set_NextOneIdx_1((uint16_t)((int32_t)341));
		TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F  L_1155 = V_1;
		NullCheck(L_1154);
		(L_1154)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)319)), (TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F )L_1155);
		TreeNodeU5BU5D_t5032BD830284F31E0A94DEB0072DB2B07126BABD* L_1156 = L_1154;
		il2cpp_codegen_initobj((&V_1), sizeof(TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F ));
		(&V_1)->set_Value_2((uint16_t)((int32_t)148));
		(&V_1)->set_NextZeroIdx_0((uint16_t)0);
		(&V_1)->set_NextOneIdx_1((uint16_t)0);
		TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F  L_1157 = V_1;
		NullCheck(L_1156);
		(L_1156)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)320)), (TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F )L_1157);
		TreeNodeU5BU5D_t5032BD830284F31E0A94DEB0072DB2B07126BABD* L_1158 = L_1156;
		il2cpp_codegen_initobj((&V_1), sizeof(TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F ));
		(&V_1)->set_Value_2((uint16_t)((int32_t)149));
		(&V_1)->set_NextZeroIdx_0((uint16_t)0);
		(&V_1)->set_NextOneIdx_1((uint16_t)0);
		TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F  L_1159 = V_1;
		NullCheck(L_1158);
		(L_1158)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)321)), (TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F )L_1159);
		TreeNodeU5BU5D_t5032BD830284F31E0A94DEB0072DB2B07126BABD* L_1160 = L_1158;
		il2cpp_codegen_initobj((&V_1), sizeof(TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F ));
		(&V_1)->set_Value_2((uint16_t)0);
		(&V_1)->set_NextZeroIdx_0((uint16_t)((int32_t)323));
		(&V_1)->set_NextOneIdx_1((uint16_t)((int32_t)324));
		TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F  L_1161 = V_1;
		NullCheck(L_1160);
		(L_1160)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)322)), (TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F )L_1161);
		TreeNodeU5BU5D_t5032BD830284F31E0A94DEB0072DB2B07126BABD* L_1162 = L_1160;
		il2cpp_codegen_initobj((&V_1), sizeof(TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F ));
		(&V_1)->set_Value_2((uint16_t)((int32_t)150));
		(&V_1)->set_NextZeroIdx_0((uint16_t)0);
		(&V_1)->set_NextOneIdx_1((uint16_t)0);
		TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F  L_1163 = V_1;
		NullCheck(L_1162);
		(L_1162)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)323)), (TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F )L_1163);
		TreeNodeU5BU5D_t5032BD830284F31E0A94DEB0072DB2B07126BABD* L_1164 = L_1162;
		il2cpp_codegen_initobj((&V_1), sizeof(TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F ));
		(&V_1)->set_Value_2((uint16_t)((int32_t)151));
		(&V_1)->set_NextZeroIdx_0((uint16_t)0);
		(&V_1)->set_NextOneIdx_1((uint16_t)0);
		TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F  L_1165 = V_1;
		NullCheck(L_1164);
		(L_1164)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)324)), (TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F )L_1165);
		TreeNodeU5BU5D_t5032BD830284F31E0A94DEB0072DB2B07126BABD* L_1166 = L_1164;
		il2cpp_codegen_initobj((&V_1), sizeof(TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F ));
		(&V_1)->set_Value_2((uint16_t)0);
		(&V_1)->set_NextZeroIdx_0((uint16_t)((int32_t)326));
		(&V_1)->set_NextOneIdx_1((uint16_t)((int32_t)338));
		TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F  L_1167 = V_1;
		NullCheck(L_1166);
		(L_1166)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)325)), (TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F )L_1167);
		TreeNodeU5BU5D_t5032BD830284F31E0A94DEB0072DB2B07126BABD* L_1168 = L_1166;
		il2cpp_codegen_initobj((&V_1), sizeof(TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F ));
		(&V_1)->set_Value_2((uint16_t)0);
		(&V_1)->set_NextZeroIdx_0((uint16_t)((int32_t)327));
		(&V_1)->set_NextOneIdx_1((uint16_t)((int32_t)336));
		TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F  L_1169 = V_1;
		NullCheck(L_1168);
		(L_1168)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)326)), (TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F )L_1169);
		TreeNodeU5BU5D_t5032BD830284F31E0A94DEB0072DB2B07126BABD* L_1170 = L_1168;
		il2cpp_codegen_initobj((&V_1), sizeof(TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F ));
		(&V_1)->set_Value_2((uint16_t)((int32_t)152));
		(&V_1)->set_NextZeroIdx_0((uint16_t)0);
		(&V_1)->set_NextOneIdx_1((uint16_t)0);
		TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F  L_1171 = V_1;
		NullCheck(L_1170);
		(L_1170)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)327)), (TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F )L_1171);
		TreeNodeU5BU5D_t5032BD830284F31E0A94DEB0072DB2B07126BABD* L_1172 = L_1170;
		il2cpp_codegen_initobj((&V_1), sizeof(TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F ));
		(&V_1)->set_Value_2((uint16_t)0);
		(&V_1)->set_NextZeroIdx_0((uint16_t)((int32_t)465));
		(&V_1)->set_NextOneIdx_1((uint16_t)((int32_t)329));
		TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F  L_1173 = V_1;
		NullCheck(L_1172);
		(L_1172)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)328)), (TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F )L_1173);
		TreeNodeU5BU5D_t5032BD830284F31E0A94DEB0072DB2B07126BABD* L_1174 = L_1172;
		il2cpp_codegen_initobj((&V_1), sizeof(TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F ));
		(&V_1)->set_Value_2((uint16_t)0);
		(&V_1)->set_NextZeroIdx_0((uint16_t)((int32_t)330));
		(&V_1)->set_NextOneIdx_1((uint16_t)((int32_t)355));
		TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F  L_1175 = V_1;
		NullCheck(L_1174);
		(L_1174)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)329)), (TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F )L_1175);
		TreeNodeU5BU5D_t5032BD830284F31E0A94DEB0072DB2B07126BABD* L_1176 = L_1174;
		il2cpp_codegen_initobj((&V_1), sizeof(TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F ));
		(&V_1)->set_Value_2((uint16_t)0);
		(&V_1)->set_NextZeroIdx_0((uint16_t)((int32_t)331));
		(&V_1)->set_NextOneIdx_1((uint16_t)((int32_t)344));
		TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F  L_1177 = V_1;
		NullCheck(L_1176);
		(L_1176)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)330)), (TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F )L_1177);
		TreeNodeU5BU5D_t5032BD830284F31E0A94DEB0072DB2B07126BABD* L_1178 = L_1176;
		il2cpp_codegen_initobj((&V_1), sizeof(TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F ));
		(&V_1)->set_Value_2((uint16_t)((int32_t)153));
		(&V_1)->set_NextZeroIdx_0((uint16_t)0);
		(&V_1)->set_NextOneIdx_1((uint16_t)0);
		TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F  L_1179 = V_1;
		NullCheck(L_1178);
		(L_1178)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)331)), (TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F )L_1179);
		TreeNodeU5BU5D_t5032BD830284F31E0A94DEB0072DB2B07126BABD* L_1180 = L_1178;
		il2cpp_codegen_initobj((&V_1), sizeof(TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F ));
		(&V_1)->set_Value_2((uint16_t)0);
		(&V_1)->set_NextZeroIdx_0((uint16_t)((int32_t)333));
		(&V_1)->set_NextOneIdx_1((uint16_t)((int32_t)347));
		TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F  L_1181 = V_1;
		NullCheck(L_1180);
		(L_1180)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)332)), (TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F )L_1181);
		TreeNodeU5BU5D_t5032BD830284F31E0A94DEB0072DB2B07126BABD* L_1182 = L_1180;
		il2cpp_codegen_initobj((&V_1), sizeof(TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F ));
		(&V_1)->set_Value_2((uint16_t)0);
		(&V_1)->set_NextZeroIdx_0((uint16_t)((int32_t)334));
		(&V_1)->set_NextOneIdx_1((uint16_t)((int32_t)342));
		TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F  L_1183 = V_1;
		NullCheck(L_1182);
		(L_1182)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)333)), (TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F )L_1183);
		TreeNodeU5BU5D_t5032BD830284F31E0A94DEB0072DB2B07126BABD* L_1184 = L_1182;
		il2cpp_codegen_initobj((&V_1), sizeof(TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F ));
		(&V_1)->set_Value_2((uint16_t)0);
		(&V_1)->set_NextZeroIdx_0((uint16_t)((int32_t)335));
		(&V_1)->set_NextOneIdx_1((uint16_t)((int32_t)337));
		TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F  L_1185 = V_1;
		NullCheck(L_1184);
		(L_1184)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)334)), (TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F )L_1185);
		TreeNodeU5BU5D_t5032BD830284F31E0A94DEB0072DB2B07126BABD* L_1186 = L_1184;
		il2cpp_codegen_initobj((&V_1), sizeof(TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F ));
		(&V_1)->set_Value_2((uint16_t)((int32_t)154));
		(&V_1)->set_NextZeroIdx_0((uint16_t)0);
		(&V_1)->set_NextOneIdx_1((uint16_t)0);
		TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F  L_1187 = V_1;
		NullCheck(L_1186);
		(L_1186)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)335)), (TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F )L_1187);
		TreeNodeU5BU5D_t5032BD830284F31E0A94DEB0072DB2B07126BABD* L_1188 = L_1186;
		il2cpp_codegen_initobj((&V_1), sizeof(TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F ));
		(&V_1)->set_Value_2((uint16_t)((int32_t)155));
		(&V_1)->set_NextZeroIdx_0((uint16_t)0);
		(&V_1)->set_NextOneIdx_1((uint16_t)0);
		TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F  L_1189 = V_1;
		NullCheck(L_1188);
		(L_1188)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)336)), (TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F )L_1189);
		TreeNodeU5BU5D_t5032BD830284F31E0A94DEB0072DB2B07126BABD* L_1190 = L_1188;
		il2cpp_codegen_initobj((&V_1), sizeof(TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F ));
		(&V_1)->set_Value_2((uint16_t)((int32_t)156));
		(&V_1)->set_NextZeroIdx_0((uint16_t)0);
		(&V_1)->set_NextOneIdx_1((uint16_t)0);
		TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F  L_1191 = V_1;
		NullCheck(L_1190);
		(L_1190)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)337)), (TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F )L_1191);
		TreeNodeU5BU5D_t5032BD830284F31E0A94DEB0072DB2B07126BABD* L_1192 = L_1190;
		il2cpp_codegen_initobj((&V_1), sizeof(TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F ));
		(&V_1)->set_Value_2((uint16_t)0);
		(&V_1)->set_NextZeroIdx_0((uint16_t)((int32_t)339));
		(&V_1)->set_NextOneIdx_1((uint16_t)((int32_t)340));
		TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F  L_1193 = V_1;
		NullCheck(L_1192);
		(L_1192)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)338)), (TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F )L_1193);
		TreeNodeU5BU5D_t5032BD830284F31E0A94DEB0072DB2B07126BABD* L_1194 = L_1192;
		il2cpp_codegen_initobj((&V_1), sizeof(TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F ));
		(&V_1)->set_Value_2((uint16_t)((int32_t)157));
		(&V_1)->set_NextZeroIdx_0((uint16_t)0);
		(&V_1)->set_NextOneIdx_1((uint16_t)0);
		TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F  L_1195 = V_1;
		NullCheck(L_1194);
		(L_1194)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)339)), (TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F )L_1195);
		TreeNodeU5BU5D_t5032BD830284F31E0A94DEB0072DB2B07126BABD* L_1196 = L_1194;
		il2cpp_codegen_initobj((&V_1), sizeof(TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F ));
		(&V_1)->set_Value_2((uint16_t)((int32_t)158));
		(&V_1)->set_NextZeroIdx_0((uint16_t)0);
		(&V_1)->set_NextOneIdx_1((uint16_t)0);
		TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F  L_1197 = V_1;
		NullCheck(L_1196);
		(L_1196)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)340)), (TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F )L_1197);
		TreeNodeU5BU5D_t5032BD830284F31E0A94DEB0072DB2B07126BABD* L_1198 = L_1196;
		il2cpp_codegen_initobj((&V_1), sizeof(TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F ));
		(&V_1)->set_Value_2((uint16_t)((int32_t)159));
		(&V_1)->set_NextZeroIdx_0((uint16_t)0);
		(&V_1)->set_NextOneIdx_1((uint16_t)0);
		TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F  L_1199 = V_1;
		NullCheck(L_1198);
		(L_1198)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)341)), (TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F )L_1199);
		TreeNodeU5BU5D_t5032BD830284F31E0A94DEB0072DB2B07126BABD* L_1200 = L_1198;
		il2cpp_codegen_initobj((&V_1), sizeof(TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F ));
		(&V_1)->set_Value_2((uint16_t)0);
		(&V_1)->set_NextZeroIdx_0((uint16_t)((int32_t)343));
		(&V_1)->set_NextOneIdx_1((uint16_t)((int32_t)346));
		TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F  L_1201 = V_1;
		NullCheck(L_1200);
		(L_1200)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)342)), (TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F )L_1201);
		TreeNodeU5BU5D_t5032BD830284F31E0A94DEB0072DB2B07126BABD* L_1202 = L_1200;
		il2cpp_codegen_initobj((&V_1), sizeof(TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F ));
		(&V_1)->set_Value_2((uint16_t)((int32_t)160));
		(&V_1)->set_NextZeroIdx_0((uint16_t)0);
		(&V_1)->set_NextOneIdx_1((uint16_t)0);
		TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F  L_1203 = V_1;
		NullCheck(L_1202);
		(L_1202)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)343)), (TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F )L_1203);
		TreeNodeU5BU5D_t5032BD830284F31E0A94DEB0072DB2B07126BABD* L_1204 = L_1202;
		il2cpp_codegen_initobj((&V_1), sizeof(TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F ));
		(&V_1)->set_Value_2((uint16_t)((int32_t)161));
		(&V_1)->set_NextZeroIdx_0((uint16_t)0);
		(&V_1)->set_NextOneIdx_1((uint16_t)0);
		TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F  L_1205 = V_1;
		NullCheck(L_1204);
		(L_1204)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)344)), (TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F )L_1205);
		TreeNodeU5BU5D_t5032BD830284F31E0A94DEB0072DB2B07126BABD* L_1206 = L_1204;
		il2cpp_codegen_initobj((&V_1), sizeof(TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F ));
		(&V_1)->set_Value_2((uint16_t)((int32_t)162));
		(&V_1)->set_NextZeroIdx_0((uint16_t)0);
		(&V_1)->set_NextOneIdx_1((uint16_t)0);
		TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F  L_1207 = V_1;
		NullCheck(L_1206);
		(L_1206)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)345)), (TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F )L_1207);
		TreeNodeU5BU5D_t5032BD830284F31E0A94DEB0072DB2B07126BABD* L_1208 = L_1206;
		il2cpp_codegen_initobj((&V_1), sizeof(TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F ));
		(&V_1)->set_Value_2((uint16_t)((int32_t)163));
		(&V_1)->set_NextZeroIdx_0((uint16_t)0);
		(&V_1)->set_NextOneIdx_1((uint16_t)0);
		TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F  L_1209 = V_1;
		NullCheck(L_1208);
		(L_1208)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)346)), (TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F )L_1209);
		TreeNodeU5BU5D_t5032BD830284F31E0A94DEB0072DB2B07126BABD* L_1210 = L_1208;
		il2cpp_codegen_initobj((&V_1), sizeof(TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F ));
		(&V_1)->set_Value_2((uint16_t)0);
		(&V_1)->set_NextZeroIdx_0((uint16_t)((int32_t)348));
		(&V_1)->set_NextOneIdx_1((uint16_t)((int32_t)360));
		TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F  L_1211 = V_1;
		NullCheck(L_1210);
		(L_1210)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)347)), (TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F )L_1211);
		TreeNodeU5BU5D_t5032BD830284F31E0A94DEB0072DB2B07126BABD* L_1212 = L_1210;
		il2cpp_codegen_initobj((&V_1), sizeof(TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F ));
		(&V_1)->set_Value_2((uint16_t)0);
		(&V_1)->set_NextZeroIdx_0((uint16_t)((int32_t)349));
		(&V_1)->set_NextOneIdx_1((uint16_t)((int32_t)359));
		TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F  L_1213 = V_1;
		NullCheck(L_1212);
		(L_1212)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)348)), (TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F )L_1213);
		TreeNodeU5BU5D_t5032BD830284F31E0A94DEB0072DB2B07126BABD* L_1214 = L_1212;
		il2cpp_codegen_initobj((&V_1), sizeof(TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F ));
		(&V_1)->set_Value_2((uint16_t)((int32_t)164));
		(&V_1)->set_NextZeroIdx_0((uint16_t)0);
		(&V_1)->set_NextOneIdx_1((uint16_t)0);
		TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F  L_1215 = V_1;
		NullCheck(L_1214);
		(L_1214)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)349)), (TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F )L_1215);
		TreeNodeU5BU5D_t5032BD830284F31E0A94DEB0072DB2B07126BABD* L_1216 = L_1214;
		il2cpp_codegen_initobj((&V_1), sizeof(TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F ));
		(&V_1)->set_Value_2((uint16_t)0);
		(&V_1)->set_NextZeroIdx_0((uint16_t)((int32_t)351));
		(&V_1)->set_NextOneIdx_1((uint16_t)((int32_t)369));
		TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F  L_1217 = V_1;
		NullCheck(L_1216);
		(L_1216)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)350)), (TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F )L_1217);
		TreeNodeU5BU5D_t5032BD830284F31E0A94DEB0072DB2B07126BABD* L_1218 = L_1216;
		il2cpp_codegen_initobj((&V_1), sizeof(TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F ));
		(&V_1)->set_Value_2((uint16_t)0);
		(&V_1)->set_NextZeroIdx_0((uint16_t)((int32_t)352));
		(&V_1)->set_NextOneIdx_1((uint16_t)((int32_t)357));
		TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F  L_1219 = V_1;
		NullCheck(L_1218);
		(L_1218)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)351)), (TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F )L_1219);
		TreeNodeU5BU5D_t5032BD830284F31E0A94DEB0072DB2B07126BABD* L_1220 = L_1218;
		il2cpp_codegen_initobj((&V_1), sizeof(TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F ));
		(&V_1)->set_Value_2((uint16_t)0);
		(&V_1)->set_NextZeroIdx_0((uint16_t)((int32_t)353));
		(&V_1)->set_NextOneIdx_1((uint16_t)((int32_t)354));
		TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F  L_1221 = V_1;
		NullCheck(L_1220);
		(L_1220)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)352)), (TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F )L_1221);
		TreeNodeU5BU5D_t5032BD830284F31E0A94DEB0072DB2B07126BABD* L_1222 = L_1220;
		il2cpp_codegen_initobj((&V_1), sizeof(TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F ));
		(&V_1)->set_Value_2((uint16_t)((int32_t)165));
		(&V_1)->set_NextZeroIdx_0((uint16_t)0);
		(&V_1)->set_NextOneIdx_1((uint16_t)0);
		TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F  L_1223 = V_1;
		NullCheck(L_1222);
		(L_1222)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)353)), (TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F )L_1223);
		TreeNodeU5BU5D_t5032BD830284F31E0A94DEB0072DB2B07126BABD* L_1224 = L_1222;
		il2cpp_codegen_initobj((&V_1), sizeof(TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F ));
		(&V_1)->set_Value_2((uint16_t)((int32_t)166));
		(&V_1)->set_NextZeroIdx_0((uint16_t)0);
		(&V_1)->set_NextOneIdx_1((uint16_t)0);
		TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F  L_1225 = V_1;
		NullCheck(L_1224);
		(L_1224)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)354)), (TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F )L_1225);
		TreeNodeU5BU5D_t5032BD830284F31E0A94DEB0072DB2B07126BABD* L_1226 = L_1224;
		il2cpp_codegen_initobj((&V_1), sizeof(TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F ));
		(&V_1)->set_Value_2((uint16_t)0);
		(&V_1)->set_NextZeroIdx_0((uint16_t)((int32_t)356));
		(&V_1)->set_NextOneIdx_1((uint16_t)((int32_t)366));
		TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F  L_1227 = V_1;
		NullCheck(L_1226);
		(L_1226)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)355)), (TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F )L_1227);
		TreeNodeU5BU5D_t5032BD830284F31E0A94DEB0072DB2B07126BABD* L_1228 = L_1226;
		il2cpp_codegen_initobj((&V_1), sizeof(TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F ));
		(&V_1)->set_Value_2((uint16_t)((int32_t)167));
		(&V_1)->set_NextZeroIdx_0((uint16_t)0);
		(&V_1)->set_NextOneIdx_1((uint16_t)0);
		TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F  L_1229 = V_1;
		NullCheck(L_1228);
		(L_1228)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)356)), (TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F )L_1229);
		TreeNodeU5BU5D_t5032BD830284F31E0A94DEB0072DB2B07126BABD* L_1230 = L_1228;
		il2cpp_codegen_initobj((&V_1), sizeof(TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F ));
		(&V_1)->set_Value_2((uint16_t)0);
		(&V_1)->set_NextZeroIdx_0((uint16_t)((int32_t)358));
		(&V_1)->set_NextOneIdx_1((uint16_t)((int32_t)368));
		TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F  L_1231 = V_1;
		NullCheck(L_1230);
		(L_1230)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)357)), (TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F )L_1231);
		TreeNodeU5BU5D_t5032BD830284F31E0A94DEB0072DB2B07126BABD* L_1232 = L_1230;
		il2cpp_codegen_initobj((&V_1), sizeof(TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F ));
		(&V_1)->set_Value_2((uint16_t)((int32_t)168));
		(&V_1)->set_NextZeroIdx_0((uint16_t)0);
		(&V_1)->set_NextOneIdx_1((uint16_t)0);
		TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F  L_1233 = V_1;
		NullCheck(L_1232);
		(L_1232)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)358)), (TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F )L_1233);
		TreeNodeU5BU5D_t5032BD830284F31E0A94DEB0072DB2B07126BABD* L_1234 = L_1232;
		il2cpp_codegen_initobj((&V_1), sizeof(TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F ));
		(&V_1)->set_Value_2((uint16_t)((int32_t)169));
		(&V_1)->set_NextZeroIdx_0((uint16_t)0);
		(&V_1)->set_NextOneIdx_1((uint16_t)0);
		TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F  L_1235 = V_1;
		NullCheck(L_1234);
		(L_1234)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)359)), (TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F )L_1235);
		TreeNodeU5BU5D_t5032BD830284F31E0A94DEB0072DB2B07126BABD* L_1236 = L_1234;
		il2cpp_codegen_initobj((&V_1), sizeof(TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F ));
		(&V_1)->set_Value_2((uint16_t)0);
		(&V_1)->set_NextZeroIdx_0((uint16_t)((int32_t)361));
		(&V_1)->set_NextOneIdx_1((uint16_t)((int32_t)367));
		TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F  L_1237 = V_1;
		NullCheck(L_1236);
		(L_1236)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)360)), (TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F )L_1237);
		TreeNodeU5BU5D_t5032BD830284F31E0A94DEB0072DB2B07126BABD* L_1238 = L_1236;
		il2cpp_codegen_initobj((&V_1), sizeof(TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F ));
		(&V_1)->set_Value_2((uint16_t)((int32_t)170));
		(&V_1)->set_NextZeroIdx_0((uint16_t)0);
		(&V_1)->set_NextOneIdx_1((uint16_t)0);
		TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F  L_1239 = V_1;
		NullCheck(L_1238);
		(L_1238)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)361)), (TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F )L_1239);
		TreeNodeU5BU5D_t5032BD830284F31E0A94DEB0072DB2B07126BABD* L_1240 = L_1238;
		il2cpp_codegen_initobj((&V_1), sizeof(TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F ));
		(&V_1)->set_Value_2((uint16_t)0);
		(&V_1)->set_NextZeroIdx_0((uint16_t)((int32_t)363));
		(&V_1)->set_NextOneIdx_1((uint16_t)((int32_t)417));
		TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F  L_1241 = V_1;
		NullCheck(L_1240);
		(L_1240)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)362)), (TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F )L_1241);
		TreeNodeU5BU5D_t5032BD830284F31E0A94DEB0072DB2B07126BABD* L_1242 = L_1240;
		il2cpp_codegen_initobj((&V_1), sizeof(TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F ));
		(&V_1)->set_Value_2((uint16_t)0);
		(&V_1)->set_NextZeroIdx_0((uint16_t)((int32_t)364));
		(&V_1)->set_NextOneIdx_1((uint16_t)((int32_t)449));
		TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F  L_1243 = V_1;
		NullCheck(L_1242);
		(L_1242)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)363)), (TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F )L_1243);
		TreeNodeU5BU5D_t5032BD830284F31E0A94DEB0072DB2B07126BABD* L_1244 = L_1242;
		il2cpp_codegen_initobj((&V_1), sizeof(TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F ));
		(&V_1)->set_Value_2((uint16_t)0);
		(&V_1)->set_NextZeroIdx_0((uint16_t)((int32_t)365));
		(&V_1)->set_NextOneIdx_1((uint16_t)((int32_t)434));
		TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F  L_1245 = V_1;
		NullCheck(L_1244);
		(L_1244)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)364)), (TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F )L_1245);
		TreeNodeU5BU5D_t5032BD830284F31E0A94DEB0072DB2B07126BABD* L_1246 = L_1244;
		il2cpp_codegen_initobj((&V_1), sizeof(TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F ));
		(&V_1)->set_Value_2((uint16_t)((int32_t)171));
		(&V_1)->set_NextZeroIdx_0((uint16_t)0);
		(&V_1)->set_NextOneIdx_1((uint16_t)0);
		TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F  L_1247 = V_1;
		NullCheck(L_1246);
		(L_1246)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)365)), (TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F )L_1247);
		TreeNodeU5BU5D_t5032BD830284F31E0A94DEB0072DB2B07126BABD* L_1248 = L_1246;
		il2cpp_codegen_initobj((&V_1), sizeof(TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F ));
		(&V_1)->set_Value_2((uint16_t)((int32_t)172));
		(&V_1)->set_NextZeroIdx_0((uint16_t)0);
		(&V_1)->set_NextOneIdx_1((uint16_t)0);
		TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F  L_1249 = V_1;
		NullCheck(L_1248);
		(L_1248)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)366)), (TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F )L_1249);
		TreeNodeU5BU5D_t5032BD830284F31E0A94DEB0072DB2B07126BABD* L_1250 = L_1248;
		il2cpp_codegen_initobj((&V_1), sizeof(TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F ));
		(&V_1)->set_Value_2((uint16_t)((int32_t)173));
		(&V_1)->set_NextZeroIdx_0((uint16_t)0);
		(&V_1)->set_NextOneIdx_1((uint16_t)0);
		TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F  L_1251 = V_1;
		NullCheck(L_1250);
		(L_1250)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)367)), (TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F )L_1251);
		TreeNodeU5BU5D_t5032BD830284F31E0A94DEB0072DB2B07126BABD* L_1252 = L_1250;
		il2cpp_codegen_initobj((&V_1), sizeof(TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F ));
		(&V_1)->set_Value_2((uint16_t)((int32_t)174));
		(&V_1)->set_NextZeroIdx_0((uint16_t)0);
		(&V_1)->set_NextOneIdx_1((uint16_t)0);
		TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F  L_1253 = V_1;
		NullCheck(L_1252);
		(L_1252)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)368)), (TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F )L_1253);
		TreeNodeU5BU5D_t5032BD830284F31E0A94DEB0072DB2B07126BABD* L_1254 = L_1252;
		il2cpp_codegen_initobj((&V_1), sizeof(TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F ));
		(&V_1)->set_Value_2((uint16_t)0);
		(&V_1)->set_NextZeroIdx_0((uint16_t)((int32_t)370));
		(&V_1)->set_NextOneIdx_1((uint16_t)((int32_t)385));
		TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F  L_1255 = V_1;
		NullCheck(L_1254);
		(L_1254)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)369)), (TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F )L_1255);
		TreeNodeU5BU5D_t5032BD830284F31E0A94DEB0072DB2B07126BABD* L_1256 = L_1254;
		il2cpp_codegen_initobj((&V_1), sizeof(TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F ));
		(&V_1)->set_Value_2((uint16_t)0);
		(&V_1)->set_NextZeroIdx_0((uint16_t)((int32_t)371));
		(&V_1)->set_NextOneIdx_1((uint16_t)((int32_t)383));
		TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F  L_1257 = V_1;
		NullCheck(L_1256);
		(L_1256)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)370)), (TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F )L_1257);
		TreeNodeU5BU5D_t5032BD830284F31E0A94DEB0072DB2B07126BABD* L_1258 = L_1256;
		il2cpp_codegen_initobj((&V_1), sizeof(TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F ));
		(&V_1)->set_Value_2((uint16_t)((int32_t)175));
		(&V_1)->set_NextZeroIdx_0((uint16_t)0);
		(&V_1)->set_NextOneIdx_1((uint16_t)0);
		TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F  L_1259 = V_1;
		NullCheck(L_1258);
		(L_1258)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)371)), (TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F )L_1259);
		TreeNodeU5BU5D_t5032BD830284F31E0A94DEB0072DB2B07126BABD* L_1260 = L_1258;
		il2cpp_codegen_initobj((&V_1), sizeof(TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F ));
		(&V_1)->set_Value_2((uint16_t)0);
		(&V_1)->set_NextZeroIdx_0((uint16_t)((int32_t)373));
		(&V_1)->set_NextOneIdx_1((uint16_t)((int32_t)451));
		TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F  L_1261 = V_1;
		NullCheck(L_1260);
		(L_1260)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)372)), (TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F )L_1261);
		TreeNodeU5BU5D_t5032BD830284F31E0A94DEB0072DB2B07126BABD* L_1262 = L_1260;
		il2cpp_codegen_initobj((&V_1), sizeof(TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F ));
		(&V_1)->set_Value_2((uint16_t)0);
		(&V_1)->set_NextZeroIdx_0((uint16_t)((int32_t)374));
		(&V_1)->set_NextOneIdx_1((uint16_t)((int32_t)381));
		TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F  L_1263 = V_1;
		NullCheck(L_1262);
		(L_1262)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)373)), (TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F )L_1263);
		TreeNodeU5BU5D_t5032BD830284F31E0A94DEB0072DB2B07126BABD* L_1264 = L_1262;
		il2cpp_codegen_initobj((&V_1), sizeof(TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F ));
		(&V_1)->set_Value_2((uint16_t)0);
		(&V_1)->set_NextZeroIdx_0((uint16_t)((int32_t)375));
		(&V_1)->set_NextOneIdx_1((uint16_t)((int32_t)376));
		TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F  L_1265 = V_1;
		NullCheck(L_1264);
		(L_1264)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)374)), (TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F )L_1265);
		TreeNodeU5BU5D_t5032BD830284F31E0A94DEB0072DB2B07126BABD* L_1266 = L_1264;
		il2cpp_codegen_initobj((&V_1), sizeof(TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F ));
		(&V_1)->set_Value_2((uint16_t)((int32_t)176));
		(&V_1)->set_NextZeroIdx_0((uint16_t)0);
		(&V_1)->set_NextOneIdx_1((uint16_t)0);
		TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F  L_1267 = V_1;
		NullCheck(L_1266);
		(L_1266)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)375)), (TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F )L_1267);
		TreeNodeU5BU5D_t5032BD830284F31E0A94DEB0072DB2B07126BABD* L_1268 = L_1266;
		il2cpp_codegen_initobj((&V_1), sizeof(TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F ));
		(&V_1)->set_Value_2((uint16_t)((int32_t)177));
		(&V_1)->set_NextZeroIdx_0((uint16_t)0);
		(&V_1)->set_NextOneIdx_1((uint16_t)0);
		TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F  L_1269 = V_1;
		NullCheck(L_1268);
		(L_1268)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)376)), (TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F )L_1269);
		TreeNodeU5BU5D_t5032BD830284F31E0A94DEB0072DB2B07126BABD* L_1270 = L_1268;
		il2cpp_codegen_initobj((&V_1), sizeof(TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F ));
		(&V_1)->set_Value_2((uint16_t)0);
		(&V_1)->set_NextZeroIdx_0((uint16_t)((int32_t)378));
		(&V_1)->set_NextOneIdx_1((uint16_t)((int32_t)393));
		TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F  L_1271 = V_1;
		NullCheck(L_1270);
		(L_1270)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)377)), (TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F )L_1271);
		TreeNodeU5BU5D_t5032BD830284F31E0A94DEB0072DB2B07126BABD* L_1272 = L_1270;
		il2cpp_codegen_initobj((&V_1), sizeof(TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F ));
		(&V_1)->set_Value_2((uint16_t)0);
		(&V_1)->set_NextZeroIdx_0((uint16_t)((int32_t)379));
		(&V_1)->set_NextOneIdx_1((uint16_t)((int32_t)390));
		TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F  L_1273 = V_1;
		NullCheck(L_1272);
		(L_1272)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)378)), (TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F )L_1273);
		TreeNodeU5BU5D_t5032BD830284F31E0A94DEB0072DB2B07126BABD* L_1274 = L_1272;
		il2cpp_codegen_initobj((&V_1), sizeof(TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F ));
		(&V_1)->set_Value_2((uint16_t)0);
		(&V_1)->set_NextZeroIdx_0((uint16_t)((int32_t)380));
		(&V_1)->set_NextOneIdx_1((uint16_t)((int32_t)384));
		TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F  L_1275 = V_1;
		NullCheck(L_1274);
		(L_1274)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)379)), (TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F )L_1275);
		TreeNodeU5BU5D_t5032BD830284F31E0A94DEB0072DB2B07126BABD* L_1276 = L_1274;
		il2cpp_codegen_initobj((&V_1), sizeof(TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F ));
		(&V_1)->set_Value_2((uint16_t)((int32_t)178));
		(&V_1)->set_NextZeroIdx_0((uint16_t)0);
		(&V_1)->set_NextOneIdx_1((uint16_t)0);
		TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F  L_1277 = V_1;
		NullCheck(L_1276);
		(L_1276)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)380)), (TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F )L_1277);
		TreeNodeU5BU5D_t5032BD830284F31E0A94DEB0072DB2B07126BABD* L_1278 = L_1276;
		il2cpp_codegen_initobj((&V_1), sizeof(TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F ));
		(&V_1)->set_Value_2((uint16_t)0);
		(&V_1)->set_NextZeroIdx_0((uint16_t)((int32_t)382));
		(&V_1)->set_NextOneIdx_1((uint16_t)((int32_t)437));
		TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F  L_1279 = V_1;
		NullCheck(L_1278);
		(L_1278)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)381)), (TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F )L_1279);
		TreeNodeU5BU5D_t5032BD830284F31E0A94DEB0072DB2B07126BABD* L_1280 = L_1278;
		il2cpp_codegen_initobj((&V_1), sizeof(TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F ));
		(&V_1)->set_Value_2((uint16_t)((int32_t)179));
		(&V_1)->set_NextZeroIdx_0((uint16_t)0);
		(&V_1)->set_NextOneIdx_1((uint16_t)0);
		TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F  L_1281 = V_1;
		NullCheck(L_1280);
		(L_1280)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)382)), (TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F )L_1281);
		TreeNodeU5BU5D_t5032BD830284F31E0A94DEB0072DB2B07126BABD* L_1282 = L_1280;
		il2cpp_codegen_initobj((&V_1), sizeof(TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F ));
		(&V_1)->set_Value_2((uint16_t)((int32_t)180));
		(&V_1)->set_NextZeroIdx_0((uint16_t)0);
		(&V_1)->set_NextOneIdx_1((uint16_t)0);
		TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F  L_1283 = V_1;
		NullCheck(L_1282);
		(L_1282)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)383)), (TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F )L_1283);
		TreeNodeU5BU5D_t5032BD830284F31E0A94DEB0072DB2B07126BABD* L_1284 = L_1282;
		il2cpp_codegen_initobj((&V_1), sizeof(TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F ));
		(&V_1)->set_Value_2((uint16_t)((int32_t)181));
		(&V_1)->set_NextZeroIdx_0((uint16_t)0);
		(&V_1)->set_NextOneIdx_1((uint16_t)0);
		TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F  L_1285 = V_1;
		NullCheck(L_1284);
		(L_1284)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)384)), (TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F )L_1285);
		TreeNodeU5BU5D_t5032BD830284F31E0A94DEB0072DB2B07126BABD* L_1286 = L_1284;
		il2cpp_codegen_initobj((&V_1), sizeof(TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F ));
		(&V_1)->set_Value_2((uint16_t)0);
		(&V_1)->set_NextZeroIdx_0((uint16_t)((int32_t)386));
		(&V_1)->set_NextOneIdx_1((uint16_t)((int32_t)387));
		TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F  L_1287 = V_1;
		NullCheck(L_1286);
		(L_1286)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)385)), (TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F )L_1287);
		TreeNodeU5BU5D_t5032BD830284F31E0A94DEB0072DB2B07126BABD* L_1288 = L_1286;
		il2cpp_codegen_initobj((&V_1), sizeof(TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F ));
		(&V_1)->set_Value_2((uint16_t)((int32_t)182));
		(&V_1)->set_NextZeroIdx_0((uint16_t)0);
		(&V_1)->set_NextOneIdx_1((uint16_t)0);
		TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F  L_1289 = V_1;
		NullCheck(L_1288);
		(L_1288)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)386)), (TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F )L_1289);
		TreeNodeU5BU5D_t5032BD830284F31E0A94DEB0072DB2B07126BABD* L_1290 = L_1288;
		il2cpp_codegen_initobj((&V_1), sizeof(TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F ));
		(&V_1)->set_Value_2((uint16_t)((int32_t)183));
		(&V_1)->set_NextZeroIdx_0((uint16_t)0);
		(&V_1)->set_NextOneIdx_1((uint16_t)0);
		TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F  L_1291 = V_1;
		NullCheck(L_1290);
		(L_1290)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)387)), (TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F )L_1291);
		TreeNodeU5BU5D_t5032BD830284F31E0A94DEB0072DB2B07126BABD* L_1292 = L_1290;
		il2cpp_codegen_initobj((&V_1), sizeof(TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F ));
		(&V_1)->set_Value_2((uint16_t)0);
		(&V_1)->set_NextZeroIdx_0((uint16_t)((int32_t)389));
		(&V_1)->set_NextOneIdx_1((uint16_t)((int32_t)409));
		TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F  L_1293 = V_1;
		NullCheck(L_1292);
		(L_1292)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)388)), (TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F )L_1293);
		TreeNodeU5BU5D_t5032BD830284F31E0A94DEB0072DB2B07126BABD* L_1294 = L_1292;
		il2cpp_codegen_initobj((&V_1), sizeof(TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F ));
		(&V_1)->set_Value_2((uint16_t)((int32_t)184));
		(&V_1)->set_NextZeroIdx_0((uint16_t)0);
		(&V_1)->set_NextOneIdx_1((uint16_t)0);
		TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F  L_1295 = V_1;
		NullCheck(L_1294);
		(L_1294)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)389)), (TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F )L_1295);
		TreeNodeU5BU5D_t5032BD830284F31E0A94DEB0072DB2B07126BABD* L_1296 = L_1294;
		il2cpp_codegen_initobj((&V_1), sizeof(TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F ));
		(&V_1)->set_Value_2((uint16_t)0);
		(&V_1)->set_NextZeroIdx_0((uint16_t)((int32_t)391));
		(&V_1)->set_NextOneIdx_1((uint16_t)((int32_t)392));
		TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F  L_1297 = V_1;
		NullCheck(L_1296);
		(L_1296)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)390)), (TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F )L_1297);
		TreeNodeU5BU5D_t5032BD830284F31E0A94DEB0072DB2B07126BABD* L_1298 = L_1296;
		il2cpp_codegen_initobj((&V_1), sizeof(TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F ));
		(&V_1)->set_Value_2((uint16_t)((int32_t)185));
		(&V_1)->set_NextZeroIdx_0((uint16_t)0);
		(&V_1)->set_NextOneIdx_1((uint16_t)0);
		TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F  L_1299 = V_1;
		NullCheck(L_1298);
		(L_1298)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)391)), (TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F )L_1299);
		TreeNodeU5BU5D_t5032BD830284F31E0A94DEB0072DB2B07126BABD* L_1300 = L_1298;
		il2cpp_codegen_initobj((&V_1), sizeof(TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F ));
		(&V_1)->set_Value_2((uint16_t)((int32_t)186));
		(&V_1)->set_NextZeroIdx_0((uint16_t)0);
		(&V_1)->set_NextOneIdx_1((uint16_t)0);
		TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F  L_1301 = V_1;
		NullCheck(L_1300);
		(L_1300)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)392)), (TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F )L_1301);
		TreeNodeU5BU5D_t5032BD830284F31E0A94DEB0072DB2B07126BABD* L_1302 = L_1300;
		il2cpp_codegen_initobj((&V_1), sizeof(TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F ));
		(&V_1)->set_Value_2((uint16_t)0);
		(&V_1)->set_NextZeroIdx_0((uint16_t)((int32_t)394));
		(&V_1)->set_NextOneIdx_1((uint16_t)((int32_t)400));
		TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F  L_1303 = V_1;
		NullCheck(L_1302);
		(L_1302)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)393)), (TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F )L_1303);
		TreeNodeU5BU5D_t5032BD830284F31E0A94DEB0072DB2B07126BABD* L_1304 = L_1302;
		il2cpp_codegen_initobj((&V_1), sizeof(TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F ));
		(&V_1)->set_Value_2((uint16_t)0);
		(&V_1)->set_NextZeroIdx_0((uint16_t)((int32_t)395));
		(&V_1)->set_NextOneIdx_1((uint16_t)((int32_t)399));
		TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F  L_1305 = V_1;
		NullCheck(L_1304);
		(L_1304)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)394)), (TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F )L_1305);
		TreeNodeU5BU5D_t5032BD830284F31E0A94DEB0072DB2B07126BABD* L_1306 = L_1304;
		il2cpp_codegen_initobj((&V_1), sizeof(TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F ));
		(&V_1)->set_Value_2((uint16_t)((int32_t)187));
		(&V_1)->set_NextZeroIdx_0((uint16_t)0);
		(&V_1)->set_NextOneIdx_1((uint16_t)0);
		TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F  L_1307 = V_1;
		NullCheck(L_1306);
		(L_1306)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)395)), (TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F )L_1307);
		TreeNodeU5BU5D_t5032BD830284F31E0A94DEB0072DB2B07126BABD* L_1308 = L_1306;
		il2cpp_codegen_initobj((&V_1), sizeof(TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F ));
		(&V_1)->set_Value_2((uint16_t)0);
		(&V_1)->set_NextZeroIdx_0((uint16_t)((int32_t)397));
		(&V_1)->set_NextOneIdx_1((uint16_t)((int32_t)412));
		TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F  L_1309 = V_1;
		NullCheck(L_1308);
		(L_1308)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)396)), (TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F )L_1309);
		TreeNodeU5BU5D_t5032BD830284F31E0A94DEB0072DB2B07126BABD* L_1310 = L_1308;
		il2cpp_codegen_initobj((&V_1), sizeof(TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F ));
		(&V_1)->set_Value_2((uint16_t)0);
		(&V_1)->set_NextZeroIdx_0((uint16_t)((int32_t)398));
		(&V_1)->set_NextOneIdx_1((uint16_t)((int32_t)402));
		TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F  L_1311 = V_1;
		NullCheck(L_1310);
		(L_1310)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)397)), (TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F )L_1311);
		TreeNodeU5BU5D_t5032BD830284F31E0A94DEB0072DB2B07126BABD* L_1312 = L_1310;
		il2cpp_codegen_initobj((&V_1), sizeof(TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F ));
		(&V_1)->set_Value_2((uint16_t)((int32_t)188));
		(&V_1)->set_NextZeroIdx_0((uint16_t)0);
		(&V_1)->set_NextOneIdx_1((uint16_t)0);
		TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F  L_1313 = V_1;
		NullCheck(L_1312);
		(L_1312)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)398)), (TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F )L_1313);
		TreeNodeU5BU5D_t5032BD830284F31E0A94DEB0072DB2B07126BABD* L_1314 = L_1312;
		il2cpp_codegen_initobj((&V_1), sizeof(TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F ));
		(&V_1)->set_Value_2((uint16_t)((int32_t)189));
		(&V_1)->set_NextZeroIdx_0((uint16_t)0);
		(&V_1)->set_NextOneIdx_1((uint16_t)0);
		TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F  L_1315 = V_1;
		NullCheck(L_1314);
		(L_1314)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)399)), (TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F )L_1315);
		TreeNodeU5BU5D_t5032BD830284F31E0A94DEB0072DB2B07126BABD* L_1316 = L_1314;
		il2cpp_codegen_initobj((&V_1), sizeof(TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F ));
		(&V_1)->set_Value_2((uint16_t)0);
		(&V_1)->set_NextZeroIdx_0((uint16_t)((int32_t)401));
		(&V_1)->set_NextOneIdx_1((uint16_t)((int32_t)411));
		TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F  L_1317 = V_1;
		NullCheck(L_1316);
		(L_1316)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)400)), (TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F )L_1317);
		TreeNodeU5BU5D_t5032BD830284F31E0A94DEB0072DB2B07126BABD* L_1318 = L_1316;
		il2cpp_codegen_initobj((&V_1), sizeof(TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F ));
		(&V_1)->set_Value_2((uint16_t)((int32_t)190));
		(&V_1)->set_NextZeroIdx_0((uint16_t)0);
		(&V_1)->set_NextOneIdx_1((uint16_t)0);
		TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F  L_1319 = V_1;
		NullCheck(L_1318);
		(L_1318)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)401)), (TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F )L_1319);
		TreeNodeU5BU5D_t5032BD830284F31E0A94DEB0072DB2B07126BABD* L_1320 = L_1318;
		il2cpp_codegen_initobj((&V_1), sizeof(TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F ));
		(&V_1)->set_Value_2((uint16_t)((int32_t)191));
		(&V_1)->set_NextZeroIdx_0((uint16_t)0);
		(&V_1)->set_NextOneIdx_1((uint16_t)0);
		TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F  L_1321 = V_1;
		NullCheck(L_1320);
		(L_1320)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)402)), (TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F )L_1321);
		TreeNodeU5BU5D_t5032BD830284F31E0A94DEB0072DB2B07126BABD* L_1322 = L_1320;
		il2cpp_codegen_initobj((&V_1), sizeof(TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F ));
		(&V_1)->set_Value_2((uint16_t)0);
		(&V_1)->set_NextZeroIdx_0((uint16_t)((int32_t)404));
		(&V_1)->set_NextOneIdx_1((uint16_t)((int32_t)427));
		TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F  L_1323 = V_1;
		NullCheck(L_1322);
		(L_1322)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)403)), (TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F )L_1323);
		TreeNodeU5BU5D_t5032BD830284F31E0A94DEB0072DB2B07126BABD* L_1324 = L_1322;
		il2cpp_codegen_initobj((&V_1), sizeof(TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F ));
		(&V_1)->set_Value_2((uint16_t)0);
		(&V_1)->set_NextZeroIdx_0((uint16_t)((int32_t)405));
		(&V_1)->set_NextOneIdx_1((uint16_t)((int32_t)424));
		TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F  L_1325 = V_1;
		NullCheck(L_1324);
		(L_1324)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)404)), (TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F )L_1325);
		TreeNodeU5BU5D_t5032BD830284F31E0A94DEB0072DB2B07126BABD* L_1326 = L_1324;
		il2cpp_codegen_initobj((&V_1), sizeof(TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F ));
		(&V_1)->set_Value_2((uint16_t)0);
		(&V_1)->set_NextZeroIdx_0((uint16_t)((int32_t)406));
		(&V_1)->set_NextOneIdx_1((uint16_t)((int32_t)421));
		TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F  L_1327 = V_1;
		NullCheck(L_1326);
		(L_1326)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)405)), (TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F )L_1327);
		TreeNodeU5BU5D_t5032BD830284F31E0A94DEB0072DB2B07126BABD* L_1328 = L_1326;
		il2cpp_codegen_initobj((&V_1), sizeof(TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F ));
		(&V_1)->set_Value_2((uint16_t)0);
		(&V_1)->set_NextZeroIdx_0((uint16_t)((int32_t)407));
		(&V_1)->set_NextOneIdx_1((uint16_t)((int32_t)408));
		TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F  L_1329 = V_1;
		NullCheck(L_1328);
		(L_1328)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)406)), (TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F )L_1329);
		TreeNodeU5BU5D_t5032BD830284F31E0A94DEB0072DB2B07126BABD* L_1330 = L_1328;
		il2cpp_codegen_initobj((&V_1), sizeof(TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F ));
		(&V_1)->set_Value_2((uint16_t)((int32_t)192));
		(&V_1)->set_NextZeroIdx_0((uint16_t)0);
		(&V_1)->set_NextOneIdx_1((uint16_t)0);
		TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F  L_1331 = V_1;
		NullCheck(L_1330);
		(L_1330)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)407)), (TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F )L_1331);
		TreeNodeU5BU5D_t5032BD830284F31E0A94DEB0072DB2B07126BABD* L_1332 = L_1330;
		il2cpp_codegen_initobj((&V_1), sizeof(TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F ));
		(&V_1)->set_Value_2((uint16_t)((int32_t)193));
		(&V_1)->set_NextZeroIdx_0((uint16_t)0);
		(&V_1)->set_NextOneIdx_1((uint16_t)0);
		TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F  L_1333 = V_1;
		NullCheck(L_1332);
		(L_1332)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)408)), (TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F )L_1333);
		TreeNodeU5BU5D_t5032BD830284F31E0A94DEB0072DB2B07126BABD* L_1334 = L_1332;
		il2cpp_codegen_initobj((&V_1), sizeof(TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F ));
		(&V_1)->set_Value_2((uint16_t)((int32_t)194));
		(&V_1)->set_NextZeroIdx_0((uint16_t)0);
		(&V_1)->set_NextOneIdx_1((uint16_t)0);
		TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F  L_1335 = V_1;
		NullCheck(L_1334);
		(L_1334)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)409)), (TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F )L_1335);
		TreeNodeU5BU5D_t5032BD830284F31E0A94DEB0072DB2B07126BABD* L_1336 = L_1334;
		il2cpp_codegen_initobj((&V_1), sizeof(TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F ));
		(&V_1)->set_Value_2((uint16_t)((int32_t)195));
		(&V_1)->set_NextZeroIdx_0((uint16_t)0);
		(&V_1)->set_NextOneIdx_1((uint16_t)0);
		TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F  L_1337 = V_1;
		NullCheck(L_1336);
		(L_1336)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)410)), (TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F )L_1337);
		TreeNodeU5BU5D_t5032BD830284F31E0A94DEB0072DB2B07126BABD* L_1338 = L_1336;
		il2cpp_codegen_initobj((&V_1), sizeof(TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F ));
		(&V_1)->set_Value_2((uint16_t)((int32_t)196));
		(&V_1)->set_NextZeroIdx_0((uint16_t)0);
		(&V_1)->set_NextOneIdx_1((uint16_t)0);
		TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F  L_1339 = V_1;
		NullCheck(L_1338);
		(L_1338)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)411)), (TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F )L_1339);
		TreeNodeU5BU5D_t5032BD830284F31E0A94DEB0072DB2B07126BABD* L_1340 = L_1338;
		il2cpp_codegen_initobj((&V_1), sizeof(TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F ));
		(&V_1)->set_Value_2((uint16_t)0);
		(&V_1)->set_NextZeroIdx_0((uint16_t)((int32_t)413));
		(&V_1)->set_NextOneIdx_1((uint16_t)((int32_t)474));
		TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F  L_1341 = V_1;
		NullCheck(L_1340);
		(L_1340)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)412)), (TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F )L_1341);
		TreeNodeU5BU5D_t5032BD830284F31E0A94DEB0072DB2B07126BABD* L_1342 = L_1340;
		il2cpp_codegen_initobj((&V_1), sizeof(TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F ));
		(&V_1)->set_Value_2((uint16_t)((int32_t)197));
		(&V_1)->set_NextZeroIdx_0((uint16_t)0);
		(&V_1)->set_NextOneIdx_1((uint16_t)0);
		TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F  L_1343 = V_1;
		NullCheck(L_1342);
		(L_1342)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)413)), (TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F )L_1343);
		TreeNodeU5BU5D_t5032BD830284F31E0A94DEB0072DB2B07126BABD* L_1344 = L_1342;
		il2cpp_codegen_initobj((&V_1), sizeof(TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F ));
		(&V_1)->set_Value_2((uint16_t)0);
		(&V_1)->set_NextZeroIdx_0((uint16_t)((int32_t)415));
		(&V_1)->set_NextOneIdx_1((uint16_t)((int32_t)475));
		TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F  L_1345 = V_1;
		NullCheck(L_1344);
		(L_1344)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)414)), (TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F )L_1345);
		TreeNodeU5BU5D_t5032BD830284F31E0A94DEB0072DB2B07126BABD* L_1346 = L_1344;
		il2cpp_codegen_initobj((&V_1), sizeof(TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F ));
		(&V_1)->set_Value_2((uint16_t)0);
		(&V_1)->set_NextZeroIdx_0((uint16_t)((int32_t)416));
		(&V_1)->set_NextOneIdx_1((uint16_t)((int32_t)471));
		TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F  L_1347 = V_1;
		NullCheck(L_1346);
		(L_1346)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)415)), (TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F )L_1347);
		TreeNodeU5BU5D_t5032BD830284F31E0A94DEB0072DB2B07126BABD* L_1348 = L_1346;
		il2cpp_codegen_initobj((&V_1), sizeof(TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F ));
		(&V_1)->set_Value_2((uint16_t)((int32_t)198));
		(&V_1)->set_NextZeroIdx_0((uint16_t)0);
		(&V_1)->set_NextOneIdx_1((uint16_t)0);
		TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F  L_1349 = V_1;
		NullCheck(L_1348);
		(L_1348)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)416)), (TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F )L_1349);
		TreeNodeU5BU5D_t5032BD830284F31E0A94DEB0072DB2B07126BABD* L_1350 = L_1348;
		il2cpp_codegen_initobj((&V_1), sizeof(TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F ));
		(&V_1)->set_Value_2((uint16_t)0);
		(&V_1)->set_NextZeroIdx_0((uint16_t)((int32_t)481));
		(&V_1)->set_NextOneIdx_1((uint16_t)((int32_t)418));
		TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F  L_1351 = V_1;
		NullCheck(L_1350);
		(L_1350)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)417)), (TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F )L_1351);
		TreeNodeU5BU5D_t5032BD830284F31E0A94DEB0072DB2B07126BABD* L_1352 = L_1350;
		il2cpp_codegen_initobj((&V_1), sizeof(TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F ));
		(&V_1)->set_Value_2((uint16_t)0);
		(&V_1)->set_NextZeroIdx_0((uint16_t)((int32_t)419));
		(&V_1)->set_NextOneIdx_1((uint16_t)((int32_t)478));
		TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F  L_1353 = V_1;
		NullCheck(L_1352);
		(L_1352)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)418)), (TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F )L_1353);
		TreeNodeU5BU5D_t5032BD830284F31E0A94DEB0072DB2B07126BABD* L_1354 = L_1352;
		il2cpp_codegen_initobj((&V_1), sizeof(TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F ));
		(&V_1)->set_Value_2((uint16_t)0);
		(&V_1)->set_NextZeroIdx_0((uint16_t)((int32_t)420));
		(&V_1)->set_NextOneIdx_1((uint16_t)((int32_t)435));
		TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F  L_1355 = V_1;
		NullCheck(L_1354);
		(L_1354)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)419)), (TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F )L_1355);
		TreeNodeU5BU5D_t5032BD830284F31E0A94DEB0072DB2B07126BABD* L_1356 = L_1354;
		il2cpp_codegen_initobj((&V_1), sizeof(TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F ));
		(&V_1)->set_Value_2((uint16_t)((int32_t)199));
		(&V_1)->set_NextZeroIdx_0((uint16_t)0);
		(&V_1)->set_NextOneIdx_1((uint16_t)0);
		TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F  L_1357 = V_1;
		NullCheck(L_1356);
		(L_1356)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)420)), (TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F )L_1357);
		TreeNodeU5BU5D_t5032BD830284F31E0A94DEB0072DB2B07126BABD* L_1358 = L_1356;
		il2cpp_codegen_initobj((&V_1), sizeof(TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F ));
		(&V_1)->set_Value_2((uint16_t)0);
		(&V_1)->set_NextZeroIdx_0((uint16_t)((int32_t)422));
		(&V_1)->set_NextOneIdx_1((uint16_t)((int32_t)423));
		TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F  L_1359 = V_1;
		NullCheck(L_1358);
		(L_1358)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)421)), (TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F )L_1359);
		TreeNodeU5BU5D_t5032BD830284F31E0A94DEB0072DB2B07126BABD* L_1360 = L_1358;
		il2cpp_codegen_initobj((&V_1), sizeof(TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F ));
		(&V_1)->set_Value_2((uint16_t)((int32_t)200));
		(&V_1)->set_NextZeroIdx_0((uint16_t)0);
		(&V_1)->set_NextOneIdx_1((uint16_t)0);
		TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F  L_1361 = V_1;
		NullCheck(L_1360);
		(L_1360)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)422)), (TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F )L_1361);
		TreeNodeU5BU5D_t5032BD830284F31E0A94DEB0072DB2B07126BABD* L_1362 = L_1360;
		il2cpp_codegen_initobj((&V_1), sizeof(TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F ));
		(&V_1)->set_Value_2((uint16_t)((int32_t)201));
		(&V_1)->set_NextZeroIdx_0((uint16_t)0);
		(&V_1)->set_NextOneIdx_1((uint16_t)0);
		TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F  L_1363 = V_1;
		NullCheck(L_1362);
		(L_1362)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)423)), (TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F )L_1363);
		TreeNodeU5BU5D_t5032BD830284F31E0A94DEB0072DB2B07126BABD* L_1364 = L_1362;
		il2cpp_codegen_initobj((&V_1), sizeof(TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F ));
		(&V_1)->set_Value_2((uint16_t)0);
		(&V_1)->set_NextZeroIdx_0((uint16_t)((int32_t)425));
		(&V_1)->set_NextOneIdx_1((uint16_t)((int32_t)438));
		TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F  L_1365 = V_1;
		NullCheck(L_1364);
		(L_1364)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)424)), (TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F )L_1365);
		TreeNodeU5BU5D_t5032BD830284F31E0A94DEB0072DB2B07126BABD* L_1366 = L_1364;
		il2cpp_codegen_initobj((&V_1), sizeof(TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F ));
		(&V_1)->set_Value_2((uint16_t)0);
		(&V_1)->set_NextZeroIdx_0((uint16_t)((int32_t)426));
		(&V_1)->set_NextOneIdx_1((uint16_t)((int32_t)433));
		TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F  L_1367 = V_1;
		NullCheck(L_1366);
		(L_1366)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)425)), (TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F )L_1367);
		TreeNodeU5BU5D_t5032BD830284F31E0A94DEB0072DB2B07126BABD* L_1368 = L_1366;
		il2cpp_codegen_initobj((&V_1), sizeof(TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F ));
		(&V_1)->set_Value_2((uint16_t)((int32_t)202));
		(&V_1)->set_NextZeroIdx_0((uint16_t)0);
		(&V_1)->set_NextOneIdx_1((uint16_t)0);
		TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F  L_1369 = V_1;
		NullCheck(L_1368);
		(L_1368)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)426)), (TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F )L_1369);
		TreeNodeU5BU5D_t5032BD830284F31E0A94DEB0072DB2B07126BABD* L_1370 = L_1368;
		il2cpp_codegen_initobj((&V_1), sizeof(TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F ));
		(&V_1)->set_Value_2((uint16_t)0);
		(&V_1)->set_NextZeroIdx_0((uint16_t)((int32_t)455));
		(&V_1)->set_NextOneIdx_1((uint16_t)((int32_t)428));
		TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F  L_1371 = V_1;
		NullCheck(L_1370);
		(L_1370)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)427)), (TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F )L_1371);
		TreeNodeU5BU5D_t5032BD830284F31E0A94DEB0072DB2B07126BABD* L_1372 = L_1370;
		il2cpp_codegen_initobj((&V_1), sizeof(TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F ));
		(&V_1)->set_Value_2((uint16_t)0);
		(&V_1)->set_NextZeroIdx_0((uint16_t)((int32_t)490));
		(&V_1)->set_NextOneIdx_1((uint16_t)((int32_t)429));
		TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F  L_1373 = V_1;
		NullCheck(L_1372);
		(L_1372)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)428)), (TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F )L_1373);
		TreeNodeU5BU5D_t5032BD830284F31E0A94DEB0072DB2B07126BABD* L_1374 = L_1372;
		il2cpp_codegen_initobj((&V_1), sizeof(TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F ));
		(&V_1)->set_Value_2((uint16_t)0);
		(&V_1)->set_NextZeroIdx_0((uint16_t)((int32_t)511));
		(&V_1)->set_NextOneIdx_1((uint16_t)((int32_t)430));
		TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F  L_1375 = V_1;
		NullCheck(L_1374);
		(L_1374)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)429)), (TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F )L_1375);
		TreeNodeU5BU5D_t5032BD830284F31E0A94DEB0072DB2B07126BABD* L_1376 = L_1374;
		il2cpp_codegen_initobj((&V_1), sizeof(TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F ));
		(&V_1)->set_Value_2((uint16_t)0);
		(&V_1)->set_NextZeroIdx_0((uint16_t)((int32_t)431));
		(&V_1)->set_NextOneIdx_1((uint16_t)((int32_t)432));
		TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F  L_1377 = V_1;
		NullCheck(L_1376);
		(L_1376)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)430)), (TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F )L_1377);
		TreeNodeU5BU5D_t5032BD830284F31E0A94DEB0072DB2B07126BABD* L_1378 = L_1376;
		il2cpp_codegen_initobj((&V_1), sizeof(TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F ));
		(&V_1)->set_Value_2((uint16_t)((int32_t)203));
		(&V_1)->set_NextZeroIdx_0((uint16_t)0);
		(&V_1)->set_NextOneIdx_1((uint16_t)0);
		TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F  L_1379 = V_1;
		NullCheck(L_1378);
		(L_1378)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)431)), (TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F )L_1379);
		TreeNodeU5BU5D_t5032BD830284F31E0A94DEB0072DB2B07126BABD* L_1380 = L_1378;
		il2cpp_codegen_initobj((&V_1), sizeof(TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F ));
		(&V_1)->set_Value_2((uint16_t)((int32_t)204));
		(&V_1)->set_NextZeroIdx_0((uint16_t)0);
		(&V_1)->set_NextOneIdx_1((uint16_t)0);
		TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F  L_1381 = V_1;
		NullCheck(L_1380);
		(L_1380)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)432)), (TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F )L_1381);
		TreeNodeU5BU5D_t5032BD830284F31E0A94DEB0072DB2B07126BABD* L_1382 = L_1380;
		il2cpp_codegen_initobj((&V_1), sizeof(TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F ));
		(&V_1)->set_Value_2((uint16_t)((int32_t)205));
		(&V_1)->set_NextZeroIdx_0((uint16_t)0);
		(&V_1)->set_NextOneIdx_1((uint16_t)0);
		TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F  L_1383 = V_1;
		NullCheck(L_1382);
		(L_1382)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)433)), (TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F )L_1383);
		TreeNodeU5BU5D_t5032BD830284F31E0A94DEB0072DB2B07126BABD* L_1384 = L_1382;
		il2cpp_codegen_initobj((&V_1), sizeof(TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F ));
		(&V_1)->set_Value_2((uint16_t)((int32_t)206));
		(&V_1)->set_NextZeroIdx_0((uint16_t)0);
		(&V_1)->set_NextOneIdx_1((uint16_t)0);
		TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F  L_1385 = V_1;
		NullCheck(L_1384);
		(L_1384)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)434)), (TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F )L_1385);
		TreeNodeU5BU5D_t5032BD830284F31E0A94DEB0072DB2B07126BABD* L_1386 = L_1384;
		il2cpp_codegen_initobj((&V_1), sizeof(TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F ));
		(&V_1)->set_Value_2((uint16_t)((int32_t)207));
		(&V_1)->set_NextZeroIdx_0((uint16_t)0);
		(&V_1)->set_NextOneIdx_1((uint16_t)0);
		TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F  L_1387 = V_1;
		NullCheck(L_1386);
		(L_1386)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)435)), (TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F )L_1387);
		TreeNodeU5BU5D_t5032BD830284F31E0A94DEB0072DB2B07126BABD* L_1388 = L_1386;
		il2cpp_codegen_initobj((&V_1), sizeof(TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F ));
		(&V_1)->set_Value_2((uint16_t)((int32_t)208));
		(&V_1)->set_NextZeroIdx_0((uint16_t)0);
		(&V_1)->set_NextOneIdx_1((uint16_t)0);
		TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F  L_1389 = V_1;
		NullCheck(L_1388);
		(L_1388)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)436)), (TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F )L_1389);
		TreeNodeU5BU5D_t5032BD830284F31E0A94DEB0072DB2B07126BABD* L_1390 = L_1388;
		il2cpp_codegen_initobj((&V_1), sizeof(TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F ));
		(&V_1)->set_Value_2((uint16_t)((int32_t)209));
		(&V_1)->set_NextZeroIdx_0((uint16_t)0);
		(&V_1)->set_NextOneIdx_1((uint16_t)0);
		TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F  L_1391 = V_1;
		NullCheck(L_1390);
		(L_1390)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)437)), (TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F )L_1391);
		TreeNodeU5BU5D_t5032BD830284F31E0A94DEB0072DB2B07126BABD* L_1392 = L_1390;
		il2cpp_codegen_initobj((&V_1), sizeof(TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F ));
		(&V_1)->set_Value_2((uint16_t)0);
		(&V_1)->set_NextZeroIdx_0((uint16_t)((int32_t)439));
		(&V_1)->set_NextOneIdx_1((uint16_t)((int32_t)446));
		TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F  L_1393 = V_1;
		NullCheck(L_1392);
		(L_1392)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)438)), (TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F )L_1393);
		TreeNodeU5BU5D_t5032BD830284F31E0A94DEB0072DB2B07126BABD* L_1394 = L_1392;
		il2cpp_codegen_initobj((&V_1), sizeof(TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F ));
		(&V_1)->set_Value_2((uint16_t)((int32_t)210));
		(&V_1)->set_NextZeroIdx_0((uint16_t)0);
		(&V_1)->set_NextOneIdx_1((uint16_t)0);
		TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F  L_1395 = V_1;
		NullCheck(L_1394);
		(L_1394)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)439)), (TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F )L_1395);
		TreeNodeU5BU5D_t5032BD830284F31E0A94DEB0072DB2B07126BABD* L_1396 = L_1394;
		il2cpp_codegen_initobj((&V_1), sizeof(TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F ));
		(&V_1)->set_Value_2((uint16_t)0);
		(&V_1)->set_NextZeroIdx_0((uint16_t)((int32_t)441));
		(&V_1)->set_NextOneIdx_1((uint16_t)((int32_t)494));
		TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F  L_1397 = V_1;
		NullCheck(L_1396);
		(L_1396)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)440)), (TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F )L_1397);
		TreeNodeU5BU5D_t5032BD830284F31E0A94DEB0072DB2B07126BABD* L_1398 = L_1396;
		il2cpp_codegen_initobj((&V_1), sizeof(TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F ));
		(&V_1)->set_Value_2((uint16_t)0);
		(&V_1)->set_NextZeroIdx_0((uint16_t)((int32_t)442));
		(&V_1)->set_NextOneIdx_1((uint16_t)((int32_t)461));
		TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F  L_1399 = V_1;
		NullCheck(L_1398);
		(L_1398)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)441)), (TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F )L_1399);
		TreeNodeU5BU5D_t5032BD830284F31E0A94DEB0072DB2B07126BABD* L_1400 = L_1398;
		il2cpp_codegen_initobj((&V_1), sizeof(TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F ));
		(&V_1)->set_Value_2((uint16_t)0);
		(&V_1)->set_NextZeroIdx_0((uint16_t)((int32_t)443));
		(&V_1)->set_NextOneIdx_1((uint16_t)((int32_t)447));
		TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F  L_1401 = V_1;
		NullCheck(L_1400);
		(L_1400)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)442)), (TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F )L_1401);
		TreeNodeU5BU5D_t5032BD830284F31E0A94DEB0072DB2B07126BABD* L_1402 = L_1400;
		il2cpp_codegen_initobj((&V_1), sizeof(TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F ));
		(&V_1)->set_Value_2((uint16_t)0);
		(&V_1)->set_NextZeroIdx_0((uint16_t)((int32_t)444));
		(&V_1)->set_NextOneIdx_1((uint16_t)((int32_t)445));
		TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F  L_1403 = V_1;
		NullCheck(L_1402);
		(L_1402)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)443)), (TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F )L_1403);
		TreeNodeU5BU5D_t5032BD830284F31E0A94DEB0072DB2B07126BABD* L_1404 = L_1402;
		il2cpp_codegen_initobj((&V_1), sizeof(TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F ));
		(&V_1)->set_Value_2((uint16_t)((int32_t)211));
		(&V_1)->set_NextZeroIdx_0((uint16_t)0);
		(&V_1)->set_NextOneIdx_1((uint16_t)0);
		TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F  L_1405 = V_1;
		NullCheck(L_1404);
		(L_1404)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)444)), (TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F )L_1405);
		TreeNodeU5BU5D_t5032BD830284F31E0A94DEB0072DB2B07126BABD* L_1406 = L_1404;
		il2cpp_codegen_initobj((&V_1), sizeof(TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F ));
		(&V_1)->set_Value_2((uint16_t)((int32_t)212));
		(&V_1)->set_NextZeroIdx_0((uint16_t)0);
		(&V_1)->set_NextOneIdx_1((uint16_t)0);
		TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F  L_1407 = V_1;
		NullCheck(L_1406);
		(L_1406)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)445)), (TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F )L_1407);
		TreeNodeU5BU5D_t5032BD830284F31E0A94DEB0072DB2B07126BABD* L_1408 = L_1406;
		il2cpp_codegen_initobj((&V_1), sizeof(TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F ));
		(&V_1)->set_Value_2((uint16_t)((int32_t)213));
		(&V_1)->set_NextZeroIdx_0((uint16_t)0);
		(&V_1)->set_NextOneIdx_1((uint16_t)0);
		TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F  L_1409 = V_1;
		NullCheck(L_1408);
		(L_1408)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)446)), (TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F )L_1409);
		TreeNodeU5BU5D_t5032BD830284F31E0A94DEB0072DB2B07126BABD* L_1410 = L_1408;
		il2cpp_codegen_initobj((&V_1), sizeof(TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F ));
		(&V_1)->set_Value_2((uint16_t)0);
		(&V_1)->set_NextZeroIdx_0((uint16_t)((int32_t)448));
		(&V_1)->set_NextOneIdx_1((uint16_t)((int32_t)460));
		TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F  L_1411 = V_1;
		NullCheck(L_1410);
		(L_1410)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)447)), (TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F )L_1411);
		TreeNodeU5BU5D_t5032BD830284F31E0A94DEB0072DB2B07126BABD* L_1412 = L_1410;
		il2cpp_codegen_initobj((&V_1), sizeof(TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F ));
		(&V_1)->set_Value_2((uint16_t)((int32_t)214));
		(&V_1)->set_NextZeroIdx_0((uint16_t)0);
		(&V_1)->set_NextOneIdx_1((uint16_t)0);
		TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F  L_1413 = V_1;
		NullCheck(L_1412);
		(L_1412)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)448)), (TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F )L_1413);
		TreeNodeU5BU5D_t5032BD830284F31E0A94DEB0072DB2B07126BABD* L_1414 = L_1412;
		il2cpp_codegen_initobj((&V_1), sizeof(TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F ));
		(&V_1)->set_Value_2((uint16_t)0);
		(&V_1)->set_NextZeroIdx_0((uint16_t)((int32_t)450));
		(&V_1)->set_NextOneIdx_1((uint16_t)((int32_t)467));
		TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F  L_1415 = V_1;
		NullCheck(L_1414);
		(L_1414)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)449)), (TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F )L_1415);
		TreeNodeU5BU5D_t5032BD830284F31E0A94DEB0072DB2B07126BABD* L_1416 = L_1414;
		il2cpp_codegen_initobj((&V_1), sizeof(TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F ));
		(&V_1)->set_Value_2((uint16_t)((int32_t)215));
		(&V_1)->set_NextZeroIdx_0((uint16_t)0);
		(&V_1)->set_NextOneIdx_1((uint16_t)0);
		TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F  L_1417 = V_1;
		NullCheck(L_1416);
		(L_1416)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)450)), (TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F )L_1417);
		TreeNodeU5BU5D_t5032BD830284F31E0A94DEB0072DB2B07126BABD* L_1418 = L_1416;
		il2cpp_codegen_initobj((&V_1), sizeof(TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F ));
		(&V_1)->set_Value_2((uint16_t)0);
		(&V_1)->set_NextZeroIdx_0((uint16_t)((int32_t)452));
		(&V_1)->set_NextOneIdx_1((uint16_t)((int32_t)469));
		TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F  L_1419 = V_1;
		NullCheck(L_1418);
		(L_1418)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)451)), (TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F )L_1419);
		TreeNodeU5BU5D_t5032BD830284F31E0A94DEB0072DB2B07126BABD* L_1420 = L_1418;
		il2cpp_codegen_initobj((&V_1), sizeof(TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F ));
		(&V_1)->set_Value_2((uint16_t)0);
		(&V_1)->set_NextZeroIdx_0((uint16_t)((int32_t)453));
		(&V_1)->set_NextOneIdx_1((uint16_t)((int32_t)454));
		TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F  L_1421 = V_1;
		NullCheck(L_1420);
		(L_1420)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)452)), (TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F )L_1421);
		TreeNodeU5BU5D_t5032BD830284F31E0A94DEB0072DB2B07126BABD* L_1422 = L_1420;
		il2cpp_codegen_initobj((&V_1), sizeof(TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F ));
		(&V_1)->set_Value_2((uint16_t)((int32_t)216));
		(&V_1)->set_NextZeroIdx_0((uint16_t)0);
		(&V_1)->set_NextOneIdx_1((uint16_t)0);
		TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F  L_1423 = V_1;
		NullCheck(L_1422);
		(L_1422)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)453)), (TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F )L_1423);
		TreeNodeU5BU5D_t5032BD830284F31E0A94DEB0072DB2B07126BABD* L_1424 = L_1422;
		il2cpp_codegen_initobj((&V_1), sizeof(TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F ));
		(&V_1)->set_Value_2((uint16_t)((int32_t)217));
		(&V_1)->set_NextZeroIdx_0((uint16_t)0);
		(&V_1)->set_NextOneIdx_1((uint16_t)0);
		TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F  L_1425 = V_1;
		NullCheck(L_1424);
		(L_1424)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)454)), (TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F )L_1425);
		TreeNodeU5BU5D_t5032BD830284F31E0A94DEB0072DB2B07126BABD* L_1426 = L_1424;
		il2cpp_codegen_initobj((&V_1), sizeof(TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F ));
		(&V_1)->set_Value_2((uint16_t)0);
		(&V_1)->set_NextZeroIdx_0((uint16_t)((int32_t)456));
		(&V_1)->set_NextOneIdx_1((uint16_t)((int32_t)484));
		TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F  L_1427 = V_1;
		NullCheck(L_1426);
		(L_1426)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)455)), (TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F )L_1427);
		TreeNodeU5BU5D_t5032BD830284F31E0A94DEB0072DB2B07126BABD* L_1428 = L_1426;
		il2cpp_codegen_initobj((&V_1), sizeof(TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F ));
		(&V_1)->set_Value_2((uint16_t)0);
		(&V_1)->set_NextZeroIdx_0((uint16_t)((int32_t)457));
		(&V_1)->set_NextOneIdx_1((uint16_t)((int32_t)458));
		TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F  L_1429 = V_1;
		NullCheck(L_1428);
		(L_1428)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)456)), (TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F )L_1429);
		TreeNodeU5BU5D_t5032BD830284F31E0A94DEB0072DB2B07126BABD* L_1430 = L_1428;
		il2cpp_codegen_initobj((&V_1), sizeof(TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F ));
		(&V_1)->set_Value_2((uint16_t)((int32_t)218));
		(&V_1)->set_NextZeroIdx_0((uint16_t)0);
		(&V_1)->set_NextOneIdx_1((uint16_t)0);
		TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F  L_1431 = V_1;
		NullCheck(L_1430);
		(L_1430)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)457)), (TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F )L_1431);
		TreeNodeU5BU5D_t5032BD830284F31E0A94DEB0072DB2B07126BABD* L_1432 = L_1430;
		il2cpp_codegen_initobj((&V_1), sizeof(TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F ));
		(&V_1)->set_Value_2((uint16_t)((int32_t)219));
		(&V_1)->set_NextZeroIdx_0((uint16_t)0);
		(&V_1)->set_NextOneIdx_1((uint16_t)0);
		TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F  L_1433 = V_1;
		NullCheck(L_1432);
		(L_1432)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)458)), (TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F )L_1433);
		TreeNodeU5BU5D_t5032BD830284F31E0A94DEB0072DB2B07126BABD* L_1434 = L_1432;
		il2cpp_codegen_initobj((&V_1), sizeof(TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F ));
		(&V_1)->set_Value_2((uint16_t)((int32_t)220));
		(&V_1)->set_NextZeroIdx_0((uint16_t)0);
		(&V_1)->set_NextOneIdx_1((uint16_t)0);
		TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F  L_1435 = V_1;
		NullCheck(L_1434);
		(L_1434)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)459)), (TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F )L_1435);
		TreeNodeU5BU5D_t5032BD830284F31E0A94DEB0072DB2B07126BABD* L_1436 = L_1434;
		il2cpp_codegen_initobj((&V_1), sizeof(TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F ));
		(&V_1)->set_Value_2((uint16_t)((int32_t)221));
		(&V_1)->set_NextZeroIdx_0((uint16_t)0);
		(&V_1)->set_NextOneIdx_1((uint16_t)0);
		TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F  L_1437 = V_1;
		NullCheck(L_1436);
		(L_1436)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)460)), (TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F )L_1437);
		TreeNodeU5BU5D_t5032BD830284F31E0A94DEB0072DB2B07126BABD* L_1438 = L_1436;
		il2cpp_codegen_initobj((&V_1), sizeof(TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F ));
		(&V_1)->set_Value_2((uint16_t)0);
		(&V_1)->set_NextZeroIdx_0((uint16_t)((int32_t)462));
		(&V_1)->set_NextOneIdx_1((uint16_t)((int32_t)488));
		TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F  L_1439 = V_1;
		NullCheck(L_1438);
		(L_1438)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)461)), (TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F )L_1439);
		TreeNodeU5BU5D_t5032BD830284F31E0A94DEB0072DB2B07126BABD* L_1440 = L_1438;
		il2cpp_codegen_initobj((&V_1), sizeof(TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F ));
		(&V_1)->set_Value_2((uint16_t)0);
		(&V_1)->set_NextZeroIdx_0((uint16_t)((int32_t)463));
		(&V_1)->set_NextOneIdx_1((uint16_t)((int32_t)464));
		TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F  L_1441 = V_1;
		NullCheck(L_1440);
		(L_1440)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)462)), (TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F )L_1441);
		TreeNodeU5BU5D_t5032BD830284F31E0A94DEB0072DB2B07126BABD* L_1442 = L_1440;
		il2cpp_codegen_initobj((&V_1), sizeof(TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F ));
		(&V_1)->set_Value_2((uint16_t)((int32_t)222));
		(&V_1)->set_NextZeroIdx_0((uint16_t)0);
		(&V_1)->set_NextOneIdx_1((uint16_t)0);
		TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F  L_1443 = V_1;
		NullCheck(L_1442);
		(L_1442)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)463)), (TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F )L_1443);
		TreeNodeU5BU5D_t5032BD830284F31E0A94DEB0072DB2B07126BABD* L_1444 = L_1442;
		il2cpp_codegen_initobj((&V_1), sizeof(TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F ));
		(&V_1)->set_Value_2((uint16_t)((int32_t)223));
		(&V_1)->set_NextZeroIdx_0((uint16_t)0);
		(&V_1)->set_NextOneIdx_1((uint16_t)0);
		TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F  L_1445 = V_1;
		NullCheck(L_1444);
		(L_1444)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)464)), (TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F )L_1445);
		TreeNodeU5BU5D_t5032BD830284F31E0A94DEB0072DB2B07126BABD* L_1446 = L_1444;
		il2cpp_codegen_initobj((&V_1), sizeof(TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F ));
		(&V_1)->set_Value_2((uint16_t)0);
		(&V_1)->set_NextZeroIdx_0((uint16_t)((int32_t)466));
		(&V_1)->set_NextOneIdx_1((uint16_t)((int32_t)468));
		TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F  L_1447 = V_1;
		NullCheck(L_1446);
		(L_1446)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)465)), (TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F )L_1447);
		TreeNodeU5BU5D_t5032BD830284F31E0A94DEB0072DB2B07126BABD* L_1448 = L_1446;
		il2cpp_codegen_initobj((&V_1), sizeof(TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F ));
		(&V_1)->set_Value_2((uint16_t)((int32_t)224));
		(&V_1)->set_NextZeroIdx_0((uint16_t)0);
		(&V_1)->set_NextOneIdx_1((uint16_t)0);
		TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F  L_1449 = V_1;
		NullCheck(L_1448);
		(L_1448)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)466)), (TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F )L_1449);
		TreeNodeU5BU5D_t5032BD830284F31E0A94DEB0072DB2B07126BABD* L_1450 = L_1448;
		il2cpp_codegen_initobj((&V_1), sizeof(TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F ));
		(&V_1)->set_Value_2((uint16_t)((int32_t)225));
		(&V_1)->set_NextZeroIdx_0((uint16_t)0);
		(&V_1)->set_NextOneIdx_1((uint16_t)0);
		TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F  L_1451 = V_1;
		NullCheck(L_1450);
		(L_1450)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)467)), (TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F )L_1451);
		TreeNodeU5BU5D_t5032BD830284F31E0A94DEB0072DB2B07126BABD* L_1452 = L_1450;
		il2cpp_codegen_initobj((&V_1), sizeof(TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F ));
		(&V_1)->set_Value_2((uint16_t)((int32_t)226));
		(&V_1)->set_NextZeroIdx_0((uint16_t)0);
		(&V_1)->set_NextOneIdx_1((uint16_t)0);
		TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F  L_1453 = V_1;
		NullCheck(L_1452);
		(L_1452)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)468)), (TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F )L_1453);
		TreeNodeU5BU5D_t5032BD830284F31E0A94DEB0072DB2B07126BABD* L_1454 = L_1452;
		il2cpp_codegen_initobj((&V_1), sizeof(TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F ));
		(&V_1)->set_Value_2((uint16_t)0);
		(&V_1)->set_NextZeroIdx_0((uint16_t)((int32_t)470));
		(&V_1)->set_NextOneIdx_1((uint16_t)((int32_t)472));
		TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F  L_1455 = V_1;
		NullCheck(L_1454);
		(L_1454)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)469)), (TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F )L_1455);
		TreeNodeU5BU5D_t5032BD830284F31E0A94DEB0072DB2B07126BABD* L_1456 = L_1454;
		il2cpp_codegen_initobj((&V_1), sizeof(TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F ));
		(&V_1)->set_Value_2((uint16_t)((int32_t)227));
		(&V_1)->set_NextZeroIdx_0((uint16_t)0);
		(&V_1)->set_NextOneIdx_1((uint16_t)0);
		TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F  L_1457 = V_1;
		NullCheck(L_1456);
		(L_1456)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)470)), (TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F )L_1457);
		TreeNodeU5BU5D_t5032BD830284F31E0A94DEB0072DB2B07126BABD* L_1458 = L_1456;
		il2cpp_codegen_initobj((&V_1), sizeof(TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F ));
		(&V_1)->set_Value_2((uint16_t)((int32_t)228));
		(&V_1)->set_NextZeroIdx_0((uint16_t)0);
		(&V_1)->set_NextOneIdx_1((uint16_t)0);
		TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F  L_1459 = V_1;
		NullCheck(L_1458);
		(L_1458)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)471)), (TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F )L_1459);
		TreeNodeU5BU5D_t5032BD830284F31E0A94DEB0072DB2B07126BABD* L_1460 = L_1458;
		il2cpp_codegen_initobj((&V_1), sizeof(TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F ));
		(&V_1)->set_Value_2((uint16_t)((int32_t)229));
		(&V_1)->set_NextZeroIdx_0((uint16_t)0);
		(&V_1)->set_NextOneIdx_1((uint16_t)0);
		TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F  L_1461 = V_1;
		NullCheck(L_1460);
		(L_1460)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)472)), (TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F )L_1461);
		TreeNodeU5BU5D_t5032BD830284F31E0A94DEB0072DB2B07126BABD* L_1462 = L_1460;
		il2cpp_codegen_initobj((&V_1), sizeof(TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F ));
		(&V_1)->set_Value_2((uint16_t)((int32_t)230));
		(&V_1)->set_NextZeroIdx_0((uint16_t)0);
		(&V_1)->set_NextOneIdx_1((uint16_t)0);
		TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F  L_1463 = V_1;
		NullCheck(L_1462);
		(L_1462)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)473)), (TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F )L_1463);
		TreeNodeU5BU5D_t5032BD830284F31E0A94DEB0072DB2B07126BABD* L_1464 = L_1462;
		il2cpp_codegen_initobj((&V_1), sizeof(TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F ));
		(&V_1)->set_Value_2((uint16_t)((int32_t)231));
		(&V_1)->set_NextZeroIdx_0((uint16_t)0);
		(&V_1)->set_NextOneIdx_1((uint16_t)0);
		TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F  L_1465 = V_1;
		NullCheck(L_1464);
		(L_1464)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)474)), (TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F )L_1465);
		TreeNodeU5BU5D_t5032BD830284F31E0A94DEB0072DB2B07126BABD* L_1466 = L_1464;
		il2cpp_codegen_initobj((&V_1), sizeof(TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F ));
		(&V_1)->set_Value_2((uint16_t)0);
		(&V_1)->set_NextZeroIdx_0((uint16_t)((int32_t)476));
		(&V_1)->set_NextOneIdx_1((uint16_t)((int32_t)477));
		TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F  L_1467 = V_1;
		NullCheck(L_1466);
		(L_1466)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)475)), (TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F )L_1467);
		TreeNodeU5BU5D_t5032BD830284F31E0A94DEB0072DB2B07126BABD* L_1468 = L_1466;
		il2cpp_codegen_initobj((&V_1), sizeof(TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F ));
		(&V_1)->set_Value_2((uint16_t)((int32_t)232));
		(&V_1)->set_NextZeroIdx_0((uint16_t)0);
		(&V_1)->set_NextOneIdx_1((uint16_t)0);
		TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F  L_1469 = V_1;
		NullCheck(L_1468);
		(L_1468)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)476)), (TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F )L_1469);
		TreeNodeU5BU5D_t5032BD830284F31E0A94DEB0072DB2B07126BABD* L_1470 = L_1468;
		il2cpp_codegen_initobj((&V_1), sizeof(TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F ));
		(&V_1)->set_Value_2((uint16_t)((int32_t)233));
		(&V_1)->set_NextZeroIdx_0((uint16_t)0);
		(&V_1)->set_NextOneIdx_1((uint16_t)0);
		TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F  L_1471 = V_1;
		NullCheck(L_1470);
		(L_1470)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)477)), (TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F )L_1471);
		TreeNodeU5BU5D_t5032BD830284F31E0A94DEB0072DB2B07126BABD* L_1472 = L_1470;
		il2cpp_codegen_initobj((&V_1), sizeof(TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F ));
		(&V_1)->set_Value_2((uint16_t)0);
		(&V_1)->set_NextZeroIdx_0((uint16_t)((int32_t)479));
		(&V_1)->set_NextOneIdx_1((uint16_t)((int32_t)480));
		TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F  L_1473 = V_1;
		NullCheck(L_1472);
		(L_1472)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)478)), (TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F )L_1473);
		TreeNodeU5BU5D_t5032BD830284F31E0A94DEB0072DB2B07126BABD* L_1474 = L_1472;
		il2cpp_codegen_initobj((&V_1), sizeof(TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F ));
		(&V_1)->set_Value_2((uint16_t)((int32_t)234));
		(&V_1)->set_NextZeroIdx_0((uint16_t)0);
		(&V_1)->set_NextOneIdx_1((uint16_t)0);
		TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F  L_1475 = V_1;
		NullCheck(L_1474);
		(L_1474)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)479)), (TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F )L_1475);
		TreeNodeU5BU5D_t5032BD830284F31E0A94DEB0072DB2B07126BABD* L_1476 = L_1474;
		il2cpp_codegen_initobj((&V_1), sizeof(TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F ));
		(&V_1)->set_Value_2((uint16_t)((int32_t)235));
		(&V_1)->set_NextZeroIdx_0((uint16_t)0);
		(&V_1)->set_NextOneIdx_1((uint16_t)0);
		TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F  L_1477 = V_1;
		NullCheck(L_1476);
		(L_1476)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)480)), (TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F )L_1477);
		TreeNodeU5BU5D_t5032BD830284F31E0A94DEB0072DB2B07126BABD* L_1478 = L_1476;
		il2cpp_codegen_initobj((&V_1), sizeof(TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F ));
		(&V_1)->set_Value_2((uint16_t)0);
		(&V_1)->set_NextZeroIdx_0((uint16_t)((int32_t)482));
		(&V_1)->set_NextOneIdx_1((uint16_t)((int32_t)483));
		TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F  L_1479 = V_1;
		NullCheck(L_1478);
		(L_1478)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)481)), (TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F )L_1479);
		TreeNodeU5BU5D_t5032BD830284F31E0A94DEB0072DB2B07126BABD* L_1480 = L_1478;
		il2cpp_codegen_initobj((&V_1), sizeof(TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F ));
		(&V_1)->set_Value_2((uint16_t)((int32_t)236));
		(&V_1)->set_NextZeroIdx_0((uint16_t)0);
		(&V_1)->set_NextOneIdx_1((uint16_t)0);
		TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F  L_1481 = V_1;
		NullCheck(L_1480);
		(L_1480)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)482)), (TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F )L_1481);
		TreeNodeU5BU5D_t5032BD830284F31E0A94DEB0072DB2B07126BABD* L_1482 = L_1480;
		il2cpp_codegen_initobj((&V_1), sizeof(TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F ));
		(&V_1)->set_Value_2((uint16_t)((int32_t)237));
		(&V_1)->set_NextZeroIdx_0((uint16_t)0);
		(&V_1)->set_NextOneIdx_1((uint16_t)0);
		TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F  L_1483 = V_1;
		NullCheck(L_1482);
		(L_1482)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)483)), (TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F )L_1483);
		TreeNodeU5BU5D_t5032BD830284F31E0A94DEB0072DB2B07126BABD* L_1484 = L_1482;
		il2cpp_codegen_initobj((&V_1), sizeof(TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F ));
		(&V_1)->set_Value_2((uint16_t)0);
		(&V_1)->set_NextZeroIdx_0((uint16_t)((int32_t)485));
		(&V_1)->set_NextOneIdx_1((uint16_t)((int32_t)487));
		TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F  L_1485 = V_1;
		NullCheck(L_1484);
		(L_1484)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)484)), (TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F )L_1485);
		TreeNodeU5BU5D_t5032BD830284F31E0A94DEB0072DB2B07126BABD* L_1486 = L_1484;
		il2cpp_codegen_initobj((&V_1), sizeof(TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F ));
		(&V_1)->set_Value_2((uint16_t)((int32_t)238));
		(&V_1)->set_NextZeroIdx_0((uint16_t)0);
		(&V_1)->set_NextOneIdx_1((uint16_t)0);
		TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F  L_1487 = V_1;
		NullCheck(L_1486);
		(L_1486)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)485)), (TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F )L_1487);
		TreeNodeU5BU5D_t5032BD830284F31E0A94DEB0072DB2B07126BABD* L_1488 = L_1486;
		il2cpp_codegen_initobj((&V_1), sizeof(TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F ));
		(&V_1)->set_Value_2((uint16_t)((int32_t)239));
		(&V_1)->set_NextZeroIdx_0((uint16_t)0);
		(&V_1)->set_NextOneIdx_1((uint16_t)0);
		TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F  L_1489 = V_1;
		NullCheck(L_1488);
		(L_1488)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)486)), (TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F )L_1489);
		TreeNodeU5BU5D_t5032BD830284F31E0A94DEB0072DB2B07126BABD* L_1490 = L_1488;
		il2cpp_codegen_initobj((&V_1), sizeof(TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F ));
		(&V_1)->set_Value_2((uint16_t)((int32_t)240));
		(&V_1)->set_NextZeroIdx_0((uint16_t)0);
		(&V_1)->set_NextOneIdx_1((uint16_t)0);
		TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F  L_1491 = V_1;
		NullCheck(L_1490);
		(L_1490)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)487)), (TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F )L_1491);
		TreeNodeU5BU5D_t5032BD830284F31E0A94DEB0072DB2B07126BABD* L_1492 = L_1490;
		il2cpp_codegen_initobj((&V_1), sizeof(TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F ));
		(&V_1)->set_Value_2((uint16_t)0);
		(&V_1)->set_NextZeroIdx_0((uint16_t)((int32_t)489));
		(&V_1)->set_NextOneIdx_1((uint16_t)((int32_t)493));
		TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F  L_1493 = V_1;
		NullCheck(L_1492);
		(L_1492)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)488)), (TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F )L_1493);
		TreeNodeU5BU5D_t5032BD830284F31E0A94DEB0072DB2B07126BABD* L_1494 = L_1492;
		il2cpp_codegen_initobj((&V_1), sizeof(TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F ));
		(&V_1)->set_Value_2((uint16_t)((int32_t)241));
		(&V_1)->set_NextZeroIdx_0((uint16_t)0);
		(&V_1)->set_NextOneIdx_1((uint16_t)0);
		TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F  L_1495 = V_1;
		NullCheck(L_1494);
		(L_1494)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)489)), (TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F )L_1495);
		TreeNodeU5BU5D_t5032BD830284F31E0A94DEB0072DB2B07126BABD* L_1496 = L_1494;
		il2cpp_codegen_initobj((&V_1), sizeof(TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F ));
		(&V_1)->set_Value_2((uint16_t)0);
		(&V_1)->set_NextZeroIdx_0((uint16_t)((int32_t)491));
		(&V_1)->set_NextOneIdx_1((uint16_t)((int32_t)492));
		TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F  L_1497 = V_1;
		NullCheck(L_1496);
		(L_1496)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)490)), (TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F )L_1497);
		TreeNodeU5BU5D_t5032BD830284F31E0A94DEB0072DB2B07126BABD* L_1498 = L_1496;
		il2cpp_codegen_initobj((&V_1), sizeof(TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F ));
		(&V_1)->set_Value_2((uint16_t)((int32_t)242));
		(&V_1)->set_NextZeroIdx_0((uint16_t)0);
		(&V_1)->set_NextOneIdx_1((uint16_t)0);
		TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F  L_1499 = V_1;
		NullCheck(L_1498);
		(L_1498)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)491)), (TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F )L_1499);
		TreeNodeU5BU5D_t5032BD830284F31E0A94DEB0072DB2B07126BABD* L_1500 = L_1498;
		il2cpp_codegen_initobj((&V_1), sizeof(TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F ));
		(&V_1)->set_Value_2((uint16_t)((int32_t)243));
		(&V_1)->set_NextZeroIdx_0((uint16_t)0);
		(&V_1)->set_NextOneIdx_1((uint16_t)0);
		TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F  L_1501 = V_1;
		NullCheck(L_1500);
		(L_1500)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)492)), (TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F )L_1501);
		TreeNodeU5BU5D_t5032BD830284F31E0A94DEB0072DB2B07126BABD* L_1502 = L_1500;
		il2cpp_codegen_initobj((&V_1), sizeof(TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F ));
		(&V_1)->set_Value_2((uint16_t)((int32_t)244));
		(&V_1)->set_NextZeroIdx_0((uint16_t)0);
		(&V_1)->set_NextOneIdx_1((uint16_t)0);
		TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F  L_1503 = V_1;
		NullCheck(L_1502);
		(L_1502)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)493)), (TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F )L_1503);
		TreeNodeU5BU5D_t5032BD830284F31E0A94DEB0072DB2B07126BABD* L_1504 = L_1502;
		il2cpp_codegen_initobj((&V_1), sizeof(TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F ));
		(&V_1)->set_Value_2((uint16_t)0);
		(&V_1)->set_NextZeroIdx_0((uint16_t)((int32_t)495));
		(&V_1)->set_NextOneIdx_1((uint16_t)((int32_t)503));
		TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F  L_1505 = V_1;
		NullCheck(L_1504);
		(L_1504)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)494)), (TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F )L_1505);
		TreeNodeU5BU5D_t5032BD830284F31E0A94DEB0072DB2B07126BABD* L_1506 = L_1504;
		il2cpp_codegen_initobj((&V_1), sizeof(TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F ));
		(&V_1)->set_Value_2((uint16_t)0);
		(&V_1)->set_NextZeroIdx_0((uint16_t)((int32_t)496));
		(&V_1)->set_NextOneIdx_1((uint16_t)((int32_t)499));
		TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F  L_1507 = V_1;
		NullCheck(L_1506);
		(L_1506)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)495)), (TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F )L_1507);
		TreeNodeU5BU5D_t5032BD830284F31E0A94DEB0072DB2B07126BABD* L_1508 = L_1506;
		il2cpp_codegen_initobj((&V_1), sizeof(TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F ));
		(&V_1)->set_Value_2((uint16_t)0);
		(&V_1)->set_NextZeroIdx_0((uint16_t)((int32_t)497));
		(&V_1)->set_NextOneIdx_1((uint16_t)((int32_t)498));
		TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F  L_1509 = V_1;
		NullCheck(L_1508);
		(L_1508)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)496)), (TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F )L_1509);
		TreeNodeU5BU5D_t5032BD830284F31E0A94DEB0072DB2B07126BABD* L_1510 = L_1508;
		il2cpp_codegen_initobj((&V_1), sizeof(TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F ));
		(&V_1)->set_Value_2((uint16_t)((int32_t)245));
		(&V_1)->set_NextZeroIdx_0((uint16_t)0);
		(&V_1)->set_NextOneIdx_1((uint16_t)0);
		TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F  L_1511 = V_1;
		NullCheck(L_1510);
		(L_1510)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)497)), (TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F )L_1511);
		TreeNodeU5BU5D_t5032BD830284F31E0A94DEB0072DB2B07126BABD* L_1512 = L_1510;
		il2cpp_codegen_initobj((&V_1), sizeof(TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F ));
		(&V_1)->set_Value_2((uint16_t)((int32_t)246));
		(&V_1)->set_NextZeroIdx_0((uint16_t)0);
		(&V_1)->set_NextOneIdx_1((uint16_t)0);
		TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F  L_1513 = V_1;
		NullCheck(L_1512);
		(L_1512)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)498)), (TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F )L_1513);
		TreeNodeU5BU5D_t5032BD830284F31E0A94DEB0072DB2B07126BABD* L_1514 = L_1512;
		il2cpp_codegen_initobj((&V_1), sizeof(TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F ));
		(&V_1)->set_Value_2((uint16_t)0);
		(&V_1)->set_NextZeroIdx_0((uint16_t)((int32_t)500));
		(&V_1)->set_NextOneIdx_1((uint16_t)((int32_t)501));
		TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F  L_1515 = V_1;
		NullCheck(L_1514);
		(L_1514)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)499)), (TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F )L_1515);
		TreeNodeU5BU5D_t5032BD830284F31E0A94DEB0072DB2B07126BABD* L_1516 = L_1514;
		il2cpp_codegen_initobj((&V_1), sizeof(TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F ));
		(&V_1)->set_Value_2((uint16_t)((int32_t)247));
		(&V_1)->set_NextZeroIdx_0((uint16_t)0);
		(&V_1)->set_NextOneIdx_1((uint16_t)0);
		TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F  L_1517 = V_1;
		NullCheck(L_1516);
		(L_1516)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)500)), (TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F )L_1517);
		TreeNodeU5BU5D_t5032BD830284F31E0A94DEB0072DB2B07126BABD* L_1518 = L_1516;
		il2cpp_codegen_initobj((&V_1), sizeof(TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F ));
		(&V_1)->set_Value_2((uint16_t)((int32_t)248));
		(&V_1)->set_NextZeroIdx_0((uint16_t)0);
		(&V_1)->set_NextOneIdx_1((uint16_t)0);
		TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F  L_1519 = V_1;
		NullCheck(L_1518);
		(L_1518)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)501)), (TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F )L_1519);
		TreeNodeU5BU5D_t5032BD830284F31E0A94DEB0072DB2B07126BABD* L_1520 = L_1518;
		il2cpp_codegen_initobj((&V_1), sizeof(TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F ));
		(&V_1)->set_Value_2((uint16_t)((int32_t)249));
		(&V_1)->set_NextZeroIdx_0((uint16_t)0);
		(&V_1)->set_NextOneIdx_1((uint16_t)0);
		TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F  L_1521 = V_1;
		NullCheck(L_1520);
		(L_1520)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)502)), (TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F )L_1521);
		TreeNodeU5BU5D_t5032BD830284F31E0A94DEB0072DB2B07126BABD* L_1522 = L_1520;
		il2cpp_codegen_initobj((&V_1), sizeof(TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F ));
		(&V_1)->set_Value_2((uint16_t)0);
		(&V_1)->set_NextZeroIdx_0((uint16_t)((int32_t)504));
		(&V_1)->set_NextOneIdx_1((uint16_t)((int32_t)507));
		TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F  L_1523 = V_1;
		NullCheck(L_1522);
		(L_1522)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)503)), (TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F )L_1523);
		TreeNodeU5BU5D_t5032BD830284F31E0A94DEB0072DB2B07126BABD* L_1524 = L_1522;
		il2cpp_codegen_initobj((&V_1), sizeof(TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F ));
		(&V_1)->set_Value_2((uint16_t)0);
		(&V_1)->set_NextZeroIdx_0((uint16_t)((int32_t)505));
		(&V_1)->set_NextOneIdx_1((uint16_t)((int32_t)506));
		TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F  L_1525 = V_1;
		NullCheck(L_1524);
		(L_1524)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)504)), (TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F )L_1525);
		TreeNodeU5BU5D_t5032BD830284F31E0A94DEB0072DB2B07126BABD* L_1526 = L_1524;
		il2cpp_codegen_initobj((&V_1), sizeof(TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F ));
		(&V_1)->set_Value_2((uint16_t)((int32_t)250));
		(&V_1)->set_NextZeroIdx_0((uint16_t)0);
		(&V_1)->set_NextOneIdx_1((uint16_t)0);
		TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F  L_1527 = V_1;
		NullCheck(L_1526);
		(L_1526)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)505)), (TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F )L_1527);
		TreeNodeU5BU5D_t5032BD830284F31E0A94DEB0072DB2B07126BABD* L_1528 = L_1526;
		il2cpp_codegen_initobj((&V_1), sizeof(TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F ));
		(&V_1)->set_Value_2((uint16_t)((int32_t)251));
		(&V_1)->set_NextZeroIdx_0((uint16_t)0);
		(&V_1)->set_NextOneIdx_1((uint16_t)0);
		TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F  L_1529 = V_1;
		NullCheck(L_1528);
		(L_1528)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)506)), (TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F )L_1529);
		TreeNodeU5BU5D_t5032BD830284F31E0A94DEB0072DB2B07126BABD* L_1530 = L_1528;
		il2cpp_codegen_initobj((&V_1), sizeof(TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F ));
		(&V_1)->set_Value_2((uint16_t)0);
		(&V_1)->set_NextZeroIdx_0((uint16_t)((int32_t)508));
		(&V_1)->set_NextOneIdx_1((uint16_t)((int32_t)509));
		TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F  L_1531 = V_1;
		NullCheck(L_1530);
		(L_1530)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)507)), (TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F )L_1531);
		TreeNodeU5BU5D_t5032BD830284F31E0A94DEB0072DB2B07126BABD* L_1532 = L_1530;
		il2cpp_codegen_initobj((&V_1), sizeof(TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F ));
		(&V_1)->set_Value_2((uint16_t)((int32_t)252));
		(&V_1)->set_NextZeroIdx_0((uint16_t)0);
		(&V_1)->set_NextOneIdx_1((uint16_t)0);
		TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F  L_1533 = V_1;
		NullCheck(L_1532);
		(L_1532)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)508)), (TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F )L_1533);
		TreeNodeU5BU5D_t5032BD830284F31E0A94DEB0072DB2B07126BABD* L_1534 = L_1532;
		il2cpp_codegen_initobj((&V_1), sizeof(TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F ));
		(&V_1)->set_Value_2((uint16_t)((int32_t)253));
		(&V_1)->set_NextZeroIdx_0((uint16_t)0);
		(&V_1)->set_NextOneIdx_1((uint16_t)0);
		TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F  L_1535 = V_1;
		NullCheck(L_1534);
		(L_1534)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)509)), (TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F )L_1535);
		TreeNodeU5BU5D_t5032BD830284F31E0A94DEB0072DB2B07126BABD* L_1536 = L_1534;
		il2cpp_codegen_initobj((&V_1), sizeof(TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F ));
		(&V_1)->set_Value_2((uint16_t)((int32_t)254));
		(&V_1)->set_NextZeroIdx_0((uint16_t)0);
		(&V_1)->set_NextOneIdx_1((uint16_t)0);
		TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F  L_1537 = V_1;
		NullCheck(L_1536);
		(L_1536)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)510)), (TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F )L_1537);
		TreeNodeU5BU5D_t5032BD830284F31E0A94DEB0072DB2B07126BABD* L_1538 = L_1536;
		il2cpp_codegen_initobj((&V_1), sizeof(TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F ));
		(&V_1)->set_Value_2((uint16_t)((int32_t)255));
		(&V_1)->set_NextZeroIdx_0((uint16_t)0);
		(&V_1)->set_NextOneIdx_1((uint16_t)0);
		TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F  L_1539 = V_1;
		NullCheck(L_1538);
		(L_1538)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)511)), (TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F )L_1539);
		TreeNodeU5BU5D_t5032BD830284F31E0A94DEB0072DB2B07126BABD* L_1540 = L_1538;
		il2cpp_codegen_initobj((&V_1), sizeof(TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F ));
		(&V_1)->set_Value_2((uint16_t)((int32_t)256));
		(&V_1)->set_NextZeroIdx_0((uint16_t)0);
		(&V_1)->set_NextOneIdx_1((uint16_t)0);
		TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F  L_1541 = V_1;
		NullCheck(L_1540);
		(L_1540)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)512)), (TreeNode_tB2EE7E237815BBE181B2A410DDACDF3770D6D09F )L_1541);
		((HuffmanEncoder_tE53CF82173488443CE9781D476D0E545B4485E6C_StaticFields*)il2cpp_codegen_static_fields_for(HuffmanEncoder_tE53CF82173488443CE9781D476D0E545B4485E6C_il2cpp_TypeInfo_var))->set_HuffmanTree_2(L_1540);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void HubOptions_set_SkipNegotiation_m59BB857442CD42D47F5642B8DD8040EFF158E406_inline (HubOptions_t53B108AE0F01D633E571D739D96B573C5C68A6F7 * __this, bool ___value0, const RuntimeMethod* method)
{
	{
		// public bool SkipNegotiation { get; set; }
		bool L_0 = ___value0;
		__this->set_U3CSkipNegotiationU3Ek__BackingField_0(L_0);
		return;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void HubOptions_set_PreferedTransport_mA281AA3D280D43C4F104055FA9B31A6328504260_inline (HubOptions_t53B108AE0F01D633E571D739D96B573C5C68A6F7 * __this, int32_t ___value0, const RuntimeMethod* method)
{
	{
		// public TransportTypes PreferedTransport { get; set; }
		int32_t L_0 = ___value0;
		__this->set_U3CPreferedTransportU3Ek__BackingField_1(L_0);
		return;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void HubOptions_set_PingInterval_mA0F505D0A849192BF594DDD9968139AE2621FFC4_inline (HubOptions_t53B108AE0F01D633E571D739D96B573C5C68A6F7 * __this, TimeSpan_t4F6A0E13E703B65365CFCAB58E05EE0AF3EE6203  ___value0, const RuntimeMethod* method)
{
	{
		// public TimeSpan PingInterval { get; set; }
		TimeSpan_t4F6A0E13E703B65365CFCAB58E05EE0AF3EE6203  L_0 = ___value0;
		__this->set_U3CPingIntervalU3Ek__BackingField_2(L_0);
		return;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void HubOptions_set_PingTimeoutInterval_mE19EF89589B500776CA57FDDB5D4FC14EB5966DF_inline (HubOptions_t53B108AE0F01D633E571D739D96B573C5C68A6F7 * __this, TimeSpan_t4F6A0E13E703B65365CFCAB58E05EE0AF3EE6203  ___value0, const RuntimeMethod* method)
{
	{
		// public TimeSpan PingTimeoutInterval { get; set; }
		TimeSpan_t4F6A0E13E703B65365CFCAB58E05EE0AF3EE6203  L_0 = ___value0;
		__this->set_U3CPingTimeoutIntervalU3Ek__BackingField_3(L_0);
		return;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void HubOptions_set_MaxRedirects_m044A7BFC4105C2B38A2DF088B9A779275A08F994_inline (HubOptions_t53B108AE0F01D633E571D739D96B573C5C68A6F7 * __this, int32_t ___value0, const RuntimeMethod* method)
{
	{
		// public int MaxRedirects { get; set; }
		int32_t L_0 = ___value0;
		__this->set_U3CMaxRedirectsU3Ek__BackingField_4(L_0);
		return;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void HubOptions_set_ConnectTimeout_m4B2039D9EDEAD322404EDAE96A6D575924625E71_inline (HubOptions_t53B108AE0F01D633E571D739D96B573C5C68A6F7 * __this, TimeSpan_t4F6A0E13E703B65365CFCAB58E05EE0AF3EE6203  ___value0, const RuntimeMethod* method)
{
	{
		// public TimeSpan ConnectTimeout { get; set; }
		TimeSpan_t4F6A0E13E703B65365CFCAB58E05EE0AF3EE6203  L_0 = ___value0;
		__this->set_U3CConnectTimeoutU3Ek__BackingField_5(L_0);
		return;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR Uri_t4A915E1CC15B2C650F478099AD448E9466CBF612 * HubConnection_get_Uri_mBA6812461AA0DDC4D50DB39047299F2EE8EE59DA_inline (HubConnection_t797D4973C9BBE6895D85BF743512ADA7D419736D * __this, const RuntimeMethod* method)
{
	{
		// public Uri Uri { get; private set; }
		Uri_t4A915E1CC15B2C650F478099AD448E9466CBF612 * L_0 = __this->get_U3CUriU3Ek__BackingField_1();
		return L_0;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR NegotiationResult_tB56E4CA54D44A10740AEDCBAC199D03849AEE84C * HubConnection_get_NegotiationResult_mB9C9B17462369CA6EB10279D142F7639E63E5322_inline (HubConnection_t797D4973C9BBE6895D85BF743512ADA7D419736D * __this, const RuntimeMethod* method)
{
	{
		// public NegotiationResult NegotiationResult { get; private set; }
		NegotiationResult_tB56E4CA54D44A10740AEDCBAC199D03849AEE84C * L_0 = __this->get_U3CNegotiationResultU3Ek__BackingField_14();
		return L_0;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR String_t* NegotiationResult_get_AccessToken_m2A2669209F7FCD3D9E1799E9CCAEC0D95B49D1A4_inline (NegotiationResult_tB56E4CA54D44A10740AEDCBAC199D03849AEE84C * __this, const RuntimeMethod* method)
{
	{
		// public string AccessToken { get; private set; }
		String_t* L_0 = __this->get_U3CAccessTokenU3Ek__BackingField_5();
		return L_0;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR RuntimeObject* HubConnection_get_Transport_m1C10051EF86E5ED6DF6672BC9366D0C3DFDD166D_inline (HubConnection_t797D4973C9BBE6895D85BF743512ADA7D419736D * __this, const RuntimeMethod* method)
{
	{
		// public ITransport Transport { get; private set; }
		RuntimeObject* L_0 = __this->get_U3CTransportU3Ek__BackingField_3();
		return L_0;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR RuntimeObject* HubConnection_get_Protocol_mD9367B3CDFA166D9C0F7EE4671B02E6A61F839D9_inline (HubConnection_t797D4973C9BBE6895D85BF743512ADA7D419736D * __this, const RuntimeMethod* method)
{
	{
		// public IProtocol Protocol { get; private set; }
		RuntimeObject* L_0 = __this->get_U3CProtocolU3Ek__BackingField_4();
		return L_0;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void HubConnection_set_AuthenticationProvider_mB2FE128F4FC37EDDA68714B81AE5A861B3681C68_inline (HubConnection_t797D4973C9BBE6895D85BF743512ADA7D419736D * __this, RuntimeObject* ___value0, const RuntimeMethod* method)
{
	{
		// public IAuthenticationProvider AuthenticationProvider { get; set; }
		RuntimeObject* L_0 = ___value0;
		__this->set_U3CAuthenticationProviderU3Ek__BackingField_13(L_0);
		return;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR RuntimeObject* HubConnection_get_AuthenticationProvider_m136DD5105ABD5D51D8068992CD2FB5D0BD9F2BE3_inline (HubConnection_t797D4973C9BBE6895D85BF743512ADA7D419736D * __this, const RuntimeMethod* method)
{
	{
		// public IAuthenticationProvider AuthenticationProvider { get; set; }
		RuntimeObject* L_0 = __this->get_U3CAuthenticationProviderU3Ek__BackingField_13();
		return L_0;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR String_t* PreAuthAccessTokenAuthenticator_get_Token_m168ED3A5300106BB0EF7FC10D9DE2D666FAFCF82_inline (PreAuthAccessTokenAuthenticator_t424A1F277DA9B16ADCB16A5B9A7659E4B5DC6162 * __this, const RuntimeMethod* method)
{
	{
		// public string Token { get; private set; }
		String_t* L_0 = __this->get_U3CTokenU3Ek__BackingField_2();
		return L_0;
	}
}
