﻿#include "pch-cpp.hpp"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include <limits>
#include <stdint.h>



// System.Char[]
struct CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34;
// System.Reflection.AssemblyCompanyAttribute
struct AssemblyCompanyAttribute_t642AAB097D7DEAAB623BEBE4664327E9B01D1DE4;
// System.Reflection.AssemblyConfigurationAttribute
struct AssemblyConfigurationAttribute_t071B324A83314FBA14A43F37BE7206C420218B7C;
// System.Reflection.AssemblyCopyrightAttribute
struct AssemblyCopyrightAttribute_tA6A09319EF50B48D962810032000DEE7B12904EC;
// System.Reflection.AssemblyDescriptionAttribute
struct AssemblyDescriptionAttribute_tF4460CCB289F6E2F71841792BBC7E6907DF612B3;
// System.Reflection.AssemblyFileVersionAttribute
struct AssemblyFileVersionAttribute_tCC1036D0566155DC5688D9230EF3C07D82A1896F;
// System.Reflection.AssemblyProductAttribute
struct AssemblyProductAttribute_t6BB0E0F76C752E14A4C26B4D1E230019068601CA;
// System.Reflection.AssemblyTitleAttribute
struct AssemblyTitleAttribute_tABB894D0792C7F307694CC796C8AE5D6A20382E7;
// System.Reflection.AssemblyTrademarkAttribute
struct AssemblyTrademarkAttribute_t0602679435F8EBECC5DDB55CFE3A7A4A4CA2B5E2;
// System.AttributeUsageAttribute
struct AttributeUsageAttribute_tBB0BAAA82036E6FCDD80A688BBD039F6FFD8EA1C;
// System.CLSCompliantAttribute
struct CLSCompliantAttribute_tA28EF6D4ADBD3C5C429DE9A70DD1E927C8906249;
// System.Runtime.InteropServices.ComVisibleAttribute
struct ComVisibleAttribute_tCE3DF5E341F3ECE4C81FE85C15B3D782AB302A2A;
// System.Runtime.CompilerServices.CompilationRelaxationsAttribute
struct CompilationRelaxationsAttribute_t661FDDC06629BDA607A42BD660944F039FE03AFF;
// System.Runtime.CompilerServices.CompilerGeneratedAttribute
struct CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C;
// System.Diagnostics.DebuggableAttribute
struct DebuggableAttribute_tA8054EBD0FC7511695D494B690B5771658E3191B;
// System.Diagnostics.DebuggerHiddenAttribute
struct DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88;
// System.Reflection.DefaultMemberAttribute
struct DefaultMemberAttribute_t8C9B3330DEA69EE364962477FF14FD2CFE30D4B5;
// System.Runtime.CompilerServices.ExtensionAttribute
struct ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC;
// System.FlagsAttribute
struct FlagsAttribute_t511C558FACEF1CC64702A8FAB67CAF3CBA65DF36;
// System.Runtime.InteropServices.GuidAttribute
struct GuidAttribute_tBB494B31270577CCD589ABBB159C18CDAE20D063;
// System.Runtime.CompilerServices.InternalsVisibleToAttribute
struct InternalsVisibleToAttribute_t1D9772A02892BAC440952F880A43C257E6C3E68C;
// System.ObsoleteAttribute
struct ObsoleteAttribute_t14BAC1669C0409EB9F28D72D664FFA6764ACD671;
// System.ParamArrayAttribute
struct ParamArrayAttribute_t9DCEB4CDDB8EDDB1124171D4C51FA6069EEA5C5F;
// System.Runtime.CompilerServices.RuntimeCompatibilityAttribute
struct RuntimeCompatibilityAttribute_tFF99AB2963098F9CBCD47A20D9FD3D51C17C1C80;
// System.String
struct String_t;



IL2CPP_EXTERN_C_BEGIN
IL2CPP_EXTERN_C_END

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Object


// System.Attribute
struct Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71  : public RuntimeObject
{
public:

public:
};


// System.String
struct String_t  : public RuntimeObject
{
public:
	// System.Int32 System.String::m_stringLength
	int32_t ___m_stringLength_0;
	// System.Char System.String::m_firstChar
	Il2CppChar ___m_firstChar_1;

public:
	inline static int32_t get_offset_of_m_stringLength_0() { return static_cast<int32_t>(offsetof(String_t, ___m_stringLength_0)); }
	inline int32_t get_m_stringLength_0() const { return ___m_stringLength_0; }
	inline int32_t* get_address_of_m_stringLength_0() { return &___m_stringLength_0; }
	inline void set_m_stringLength_0(int32_t value)
	{
		___m_stringLength_0 = value;
	}

	inline static int32_t get_offset_of_m_firstChar_1() { return static_cast<int32_t>(offsetof(String_t, ___m_firstChar_1)); }
	inline Il2CppChar get_m_firstChar_1() const { return ___m_firstChar_1; }
	inline Il2CppChar* get_address_of_m_firstChar_1() { return &___m_firstChar_1; }
	inline void set_m_firstChar_1(Il2CppChar value)
	{
		___m_firstChar_1 = value;
	}
};

struct String_t_StaticFields
{
public:
	// System.String System.String::Empty
	String_t* ___Empty_5;

public:
	inline static int32_t get_offset_of_Empty_5() { return static_cast<int32_t>(offsetof(String_t_StaticFields, ___Empty_5)); }
	inline String_t* get_Empty_5() const { return ___Empty_5; }
	inline String_t** get_address_of_Empty_5() { return &___Empty_5; }
	inline void set_Empty_5(String_t* value)
	{
		___Empty_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___Empty_5), (void*)value);
	}
};


// System.ValueType
struct ValueType_tDBF999C1B75C48C68621878250DBF6CDBCF51E52  : public RuntimeObject
{
public:

public:
};

// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_tDBF999C1B75C48C68621878250DBF6CDBCF51E52_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_tDBF999C1B75C48C68621878250DBF6CDBCF51E52_marshaled_com
{
};

// System.Reflection.AssemblyCompanyAttribute
struct AssemblyCompanyAttribute_t642AAB097D7DEAAB623BEBE4664327E9B01D1DE4  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.String System.Reflection.AssemblyCompanyAttribute::m_company
	String_t* ___m_company_0;

public:
	inline static int32_t get_offset_of_m_company_0() { return static_cast<int32_t>(offsetof(AssemblyCompanyAttribute_t642AAB097D7DEAAB623BEBE4664327E9B01D1DE4, ___m_company_0)); }
	inline String_t* get_m_company_0() const { return ___m_company_0; }
	inline String_t** get_address_of_m_company_0() { return &___m_company_0; }
	inline void set_m_company_0(String_t* value)
	{
		___m_company_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_company_0), (void*)value);
	}
};


// System.Reflection.AssemblyConfigurationAttribute
struct AssemblyConfigurationAttribute_t071B324A83314FBA14A43F37BE7206C420218B7C  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.String System.Reflection.AssemblyConfigurationAttribute::m_configuration
	String_t* ___m_configuration_0;

public:
	inline static int32_t get_offset_of_m_configuration_0() { return static_cast<int32_t>(offsetof(AssemblyConfigurationAttribute_t071B324A83314FBA14A43F37BE7206C420218B7C, ___m_configuration_0)); }
	inline String_t* get_m_configuration_0() const { return ___m_configuration_0; }
	inline String_t** get_address_of_m_configuration_0() { return &___m_configuration_0; }
	inline void set_m_configuration_0(String_t* value)
	{
		___m_configuration_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_configuration_0), (void*)value);
	}
};


// System.Reflection.AssemblyCopyrightAttribute
struct AssemblyCopyrightAttribute_tA6A09319EF50B48D962810032000DEE7B12904EC  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.String System.Reflection.AssemblyCopyrightAttribute::m_copyright
	String_t* ___m_copyright_0;

public:
	inline static int32_t get_offset_of_m_copyright_0() { return static_cast<int32_t>(offsetof(AssemblyCopyrightAttribute_tA6A09319EF50B48D962810032000DEE7B12904EC, ___m_copyright_0)); }
	inline String_t* get_m_copyright_0() const { return ___m_copyright_0; }
	inline String_t** get_address_of_m_copyright_0() { return &___m_copyright_0; }
	inline void set_m_copyright_0(String_t* value)
	{
		___m_copyright_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_copyright_0), (void*)value);
	}
};


// System.Reflection.AssemblyDescriptionAttribute
struct AssemblyDescriptionAttribute_tF4460CCB289F6E2F71841792BBC7E6907DF612B3  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.String System.Reflection.AssemblyDescriptionAttribute::m_description
	String_t* ___m_description_0;

public:
	inline static int32_t get_offset_of_m_description_0() { return static_cast<int32_t>(offsetof(AssemblyDescriptionAttribute_tF4460CCB289F6E2F71841792BBC7E6907DF612B3, ___m_description_0)); }
	inline String_t* get_m_description_0() const { return ___m_description_0; }
	inline String_t** get_address_of_m_description_0() { return &___m_description_0; }
	inline void set_m_description_0(String_t* value)
	{
		___m_description_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_description_0), (void*)value);
	}
};


// System.Reflection.AssemblyFileVersionAttribute
struct AssemblyFileVersionAttribute_tCC1036D0566155DC5688D9230EF3C07D82A1896F  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.String System.Reflection.AssemblyFileVersionAttribute::_version
	String_t* ____version_0;

public:
	inline static int32_t get_offset_of__version_0() { return static_cast<int32_t>(offsetof(AssemblyFileVersionAttribute_tCC1036D0566155DC5688D9230EF3C07D82A1896F, ____version_0)); }
	inline String_t* get__version_0() const { return ____version_0; }
	inline String_t** get_address_of__version_0() { return &____version_0; }
	inline void set__version_0(String_t* value)
	{
		____version_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____version_0), (void*)value);
	}
};


// System.Reflection.AssemblyProductAttribute
struct AssemblyProductAttribute_t6BB0E0F76C752E14A4C26B4D1E230019068601CA  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.String System.Reflection.AssemblyProductAttribute::m_product
	String_t* ___m_product_0;

public:
	inline static int32_t get_offset_of_m_product_0() { return static_cast<int32_t>(offsetof(AssemblyProductAttribute_t6BB0E0F76C752E14A4C26B4D1E230019068601CA, ___m_product_0)); }
	inline String_t* get_m_product_0() const { return ___m_product_0; }
	inline String_t** get_address_of_m_product_0() { return &___m_product_0; }
	inline void set_m_product_0(String_t* value)
	{
		___m_product_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_product_0), (void*)value);
	}
};


// System.Reflection.AssemblyTitleAttribute
struct AssemblyTitleAttribute_tABB894D0792C7F307694CC796C8AE5D6A20382E7  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.String System.Reflection.AssemblyTitleAttribute::m_title
	String_t* ___m_title_0;

public:
	inline static int32_t get_offset_of_m_title_0() { return static_cast<int32_t>(offsetof(AssemblyTitleAttribute_tABB894D0792C7F307694CC796C8AE5D6A20382E7, ___m_title_0)); }
	inline String_t* get_m_title_0() const { return ___m_title_0; }
	inline String_t** get_address_of_m_title_0() { return &___m_title_0; }
	inline void set_m_title_0(String_t* value)
	{
		___m_title_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_title_0), (void*)value);
	}
};


// System.Reflection.AssemblyTrademarkAttribute
struct AssemblyTrademarkAttribute_t0602679435F8EBECC5DDB55CFE3A7A4A4CA2B5E2  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.String System.Reflection.AssemblyTrademarkAttribute::m_trademark
	String_t* ___m_trademark_0;

public:
	inline static int32_t get_offset_of_m_trademark_0() { return static_cast<int32_t>(offsetof(AssemblyTrademarkAttribute_t0602679435F8EBECC5DDB55CFE3A7A4A4CA2B5E2, ___m_trademark_0)); }
	inline String_t* get_m_trademark_0() const { return ___m_trademark_0; }
	inline String_t** get_address_of_m_trademark_0() { return &___m_trademark_0; }
	inline void set_m_trademark_0(String_t* value)
	{
		___m_trademark_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_trademark_0), (void*)value);
	}
};


// System.Boolean
struct Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37 
{
public:
	// System.Boolean System.Boolean::m_value
	bool ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37, ___m_value_0)); }
	inline bool get_m_value_0() const { return ___m_value_0; }
	inline bool* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(bool value)
	{
		___m_value_0 = value;
	}
};

struct Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37_StaticFields
{
public:
	// System.String System.Boolean::TrueString
	String_t* ___TrueString_5;
	// System.String System.Boolean::FalseString
	String_t* ___FalseString_6;

public:
	inline static int32_t get_offset_of_TrueString_5() { return static_cast<int32_t>(offsetof(Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37_StaticFields, ___TrueString_5)); }
	inline String_t* get_TrueString_5() const { return ___TrueString_5; }
	inline String_t** get_address_of_TrueString_5() { return &___TrueString_5; }
	inline void set_TrueString_5(String_t* value)
	{
		___TrueString_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___TrueString_5), (void*)value);
	}

	inline static int32_t get_offset_of_FalseString_6() { return static_cast<int32_t>(offsetof(Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37_StaticFields, ___FalseString_6)); }
	inline String_t* get_FalseString_6() const { return ___FalseString_6; }
	inline String_t** get_address_of_FalseString_6() { return &___FalseString_6; }
	inline void set_FalseString_6(String_t* value)
	{
		___FalseString_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___FalseString_6), (void*)value);
	}
};


// System.CLSCompliantAttribute
struct CLSCompliantAttribute_tA28EF6D4ADBD3C5C429DE9A70DD1E927C8906249  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.Boolean System.CLSCompliantAttribute::m_compliant
	bool ___m_compliant_0;

public:
	inline static int32_t get_offset_of_m_compliant_0() { return static_cast<int32_t>(offsetof(CLSCompliantAttribute_tA28EF6D4ADBD3C5C429DE9A70DD1E927C8906249, ___m_compliant_0)); }
	inline bool get_m_compliant_0() const { return ___m_compliant_0; }
	inline bool* get_address_of_m_compliant_0() { return &___m_compliant_0; }
	inline void set_m_compliant_0(bool value)
	{
		___m_compliant_0 = value;
	}
};


// System.Runtime.InteropServices.ComVisibleAttribute
struct ComVisibleAttribute_tCE3DF5E341F3ECE4C81FE85C15B3D782AB302A2A  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.Boolean System.Runtime.InteropServices.ComVisibleAttribute::_val
	bool ____val_0;

public:
	inline static int32_t get_offset_of__val_0() { return static_cast<int32_t>(offsetof(ComVisibleAttribute_tCE3DF5E341F3ECE4C81FE85C15B3D782AB302A2A, ____val_0)); }
	inline bool get__val_0() const { return ____val_0; }
	inline bool* get_address_of__val_0() { return &____val_0; }
	inline void set__val_0(bool value)
	{
		____val_0 = value;
	}
};


// System.Runtime.CompilerServices.CompilationRelaxationsAttribute
struct CompilationRelaxationsAttribute_t661FDDC06629BDA607A42BD660944F039FE03AFF  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.Int32 System.Runtime.CompilerServices.CompilationRelaxationsAttribute::m_relaxations
	int32_t ___m_relaxations_0;

public:
	inline static int32_t get_offset_of_m_relaxations_0() { return static_cast<int32_t>(offsetof(CompilationRelaxationsAttribute_t661FDDC06629BDA607A42BD660944F039FE03AFF, ___m_relaxations_0)); }
	inline int32_t get_m_relaxations_0() const { return ___m_relaxations_0; }
	inline int32_t* get_address_of_m_relaxations_0() { return &___m_relaxations_0; }
	inline void set_m_relaxations_0(int32_t value)
	{
		___m_relaxations_0 = value;
	}
};


// System.Runtime.CompilerServices.CompilerGeneratedAttribute
struct CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:

public:
};


// System.Diagnostics.DebuggerHiddenAttribute
struct DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:

public:
};


// System.Reflection.DefaultMemberAttribute
struct DefaultMemberAttribute_t8C9B3330DEA69EE364962477FF14FD2CFE30D4B5  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.String System.Reflection.DefaultMemberAttribute::m_memberName
	String_t* ___m_memberName_0;

public:
	inline static int32_t get_offset_of_m_memberName_0() { return static_cast<int32_t>(offsetof(DefaultMemberAttribute_t8C9B3330DEA69EE364962477FF14FD2CFE30D4B5, ___m_memberName_0)); }
	inline String_t* get_m_memberName_0() const { return ___m_memberName_0; }
	inline String_t** get_address_of_m_memberName_0() { return &___m_memberName_0; }
	inline void set_m_memberName_0(String_t* value)
	{
		___m_memberName_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_memberName_0), (void*)value);
	}
};


// System.Enum
struct Enum_t23B90B40F60E677A8025267341651C94AE079CDA  : public ValueType_tDBF999C1B75C48C68621878250DBF6CDBCF51E52
{
public:

public:
};

struct Enum_t23B90B40F60E677A8025267341651C94AE079CDA_StaticFields
{
public:
	// System.Char[] System.Enum::enumSeperatorCharArray
	CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34* ___enumSeperatorCharArray_0;

public:
	inline static int32_t get_offset_of_enumSeperatorCharArray_0() { return static_cast<int32_t>(offsetof(Enum_t23B90B40F60E677A8025267341651C94AE079CDA_StaticFields, ___enumSeperatorCharArray_0)); }
	inline CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34* get_enumSeperatorCharArray_0() const { return ___enumSeperatorCharArray_0; }
	inline CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34** get_address_of_enumSeperatorCharArray_0() { return &___enumSeperatorCharArray_0; }
	inline void set_enumSeperatorCharArray_0(CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34* value)
	{
		___enumSeperatorCharArray_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___enumSeperatorCharArray_0), (void*)value);
	}
};

// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t23B90B40F60E677A8025267341651C94AE079CDA_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t23B90B40F60E677A8025267341651C94AE079CDA_marshaled_com
{
};

// System.Runtime.CompilerServices.ExtensionAttribute
struct ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:

public:
};


// System.FlagsAttribute
struct FlagsAttribute_t511C558FACEF1CC64702A8FAB67CAF3CBA65DF36  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:

public:
};


// System.Runtime.InteropServices.GuidAttribute
struct GuidAttribute_tBB494B31270577CCD589ABBB159C18CDAE20D063  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.String System.Runtime.InteropServices.GuidAttribute::_val
	String_t* ____val_0;

public:
	inline static int32_t get_offset_of__val_0() { return static_cast<int32_t>(offsetof(GuidAttribute_tBB494B31270577CCD589ABBB159C18CDAE20D063, ____val_0)); }
	inline String_t* get__val_0() const { return ____val_0; }
	inline String_t** get_address_of__val_0() { return &____val_0; }
	inline void set__val_0(String_t* value)
	{
		____val_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____val_0), (void*)value);
	}
};


// System.Runtime.CompilerServices.InternalsVisibleToAttribute
struct InternalsVisibleToAttribute_t1D9772A02892BAC440952F880A43C257E6C3E68C  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.String System.Runtime.CompilerServices.InternalsVisibleToAttribute::_assemblyName
	String_t* ____assemblyName_0;
	// System.Boolean System.Runtime.CompilerServices.InternalsVisibleToAttribute::_allInternalsVisible
	bool ____allInternalsVisible_1;

public:
	inline static int32_t get_offset_of__assemblyName_0() { return static_cast<int32_t>(offsetof(InternalsVisibleToAttribute_t1D9772A02892BAC440952F880A43C257E6C3E68C, ____assemblyName_0)); }
	inline String_t* get__assemblyName_0() const { return ____assemblyName_0; }
	inline String_t** get_address_of__assemblyName_0() { return &____assemblyName_0; }
	inline void set__assemblyName_0(String_t* value)
	{
		____assemblyName_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____assemblyName_0), (void*)value);
	}

	inline static int32_t get_offset_of__allInternalsVisible_1() { return static_cast<int32_t>(offsetof(InternalsVisibleToAttribute_t1D9772A02892BAC440952F880A43C257E6C3E68C, ____allInternalsVisible_1)); }
	inline bool get__allInternalsVisible_1() const { return ____allInternalsVisible_1; }
	inline bool* get_address_of__allInternalsVisible_1() { return &____allInternalsVisible_1; }
	inline void set__allInternalsVisible_1(bool value)
	{
		____allInternalsVisible_1 = value;
	}
};


// System.ObsoleteAttribute
struct ObsoleteAttribute_t14BAC1669C0409EB9F28D72D664FFA6764ACD671  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.String System.ObsoleteAttribute::_message
	String_t* ____message_0;
	// System.Boolean System.ObsoleteAttribute::_error
	bool ____error_1;

public:
	inline static int32_t get_offset_of__message_0() { return static_cast<int32_t>(offsetof(ObsoleteAttribute_t14BAC1669C0409EB9F28D72D664FFA6764ACD671, ____message_0)); }
	inline String_t* get__message_0() const { return ____message_0; }
	inline String_t** get_address_of__message_0() { return &____message_0; }
	inline void set__message_0(String_t* value)
	{
		____message_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____message_0), (void*)value);
	}

	inline static int32_t get_offset_of__error_1() { return static_cast<int32_t>(offsetof(ObsoleteAttribute_t14BAC1669C0409EB9F28D72D664FFA6764ACD671, ____error_1)); }
	inline bool get__error_1() const { return ____error_1; }
	inline bool* get_address_of__error_1() { return &____error_1; }
	inline void set__error_1(bool value)
	{
		____error_1 = value;
	}
};


// System.ParamArrayAttribute
struct ParamArrayAttribute_t9DCEB4CDDB8EDDB1124171D4C51FA6069EEA5C5F  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:

public:
};


// System.Runtime.CompilerServices.RuntimeCompatibilityAttribute
struct RuntimeCompatibilityAttribute_tFF99AB2963098F9CBCD47A20D9FD3D51C17C1C80  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.Boolean System.Runtime.CompilerServices.RuntimeCompatibilityAttribute::m_wrapNonExceptionThrows
	bool ___m_wrapNonExceptionThrows_0;

public:
	inline static int32_t get_offset_of_m_wrapNonExceptionThrows_0() { return static_cast<int32_t>(offsetof(RuntimeCompatibilityAttribute_tFF99AB2963098F9CBCD47A20D9FD3D51C17C1C80, ___m_wrapNonExceptionThrows_0)); }
	inline bool get_m_wrapNonExceptionThrows_0() const { return ___m_wrapNonExceptionThrows_0; }
	inline bool* get_address_of_m_wrapNonExceptionThrows_0() { return &___m_wrapNonExceptionThrows_0; }
	inline void set_m_wrapNonExceptionThrows_0(bool value)
	{
		___m_wrapNonExceptionThrows_0 = value;
	}
};


// System.Void
struct Void_t700C6383A2A510C2CF4DD86DABD5CA9FF70ADAC5 
{
public:
	union
	{
		struct
		{
		};
		uint8_t Void_t700C6383A2A510C2CF4DD86DABD5CA9FF70ADAC5__padding[1];
	};

public:
};


// System.AttributeTargets
struct AttributeTargets_t5F71273DFE1D0CA9B8109F02A023A7DBA9BFC923 
{
public:
	// System.Int32 System.AttributeTargets::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(AttributeTargets_t5F71273DFE1D0CA9B8109F02A023A7DBA9BFC923, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// System.Diagnostics.DebuggableAttribute/DebuggingModes
struct DebuggingModes_t279D5B9C012ABA935887CB73C5A63A1F46AF08A8 
{
public:
	// System.Int32 System.Diagnostics.DebuggableAttribute/DebuggingModes::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(DebuggingModes_t279D5B9C012ABA935887CB73C5A63A1F46AF08A8, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// System.AttributeUsageAttribute
struct AttributeUsageAttribute_tBB0BAAA82036E6FCDD80A688BBD039F6FFD8EA1C  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.AttributeTargets System.AttributeUsageAttribute::m_attributeTarget
	int32_t ___m_attributeTarget_0;
	// System.Boolean System.AttributeUsageAttribute::m_allowMultiple
	bool ___m_allowMultiple_1;
	// System.Boolean System.AttributeUsageAttribute::m_inherited
	bool ___m_inherited_2;

public:
	inline static int32_t get_offset_of_m_attributeTarget_0() { return static_cast<int32_t>(offsetof(AttributeUsageAttribute_tBB0BAAA82036E6FCDD80A688BBD039F6FFD8EA1C, ___m_attributeTarget_0)); }
	inline int32_t get_m_attributeTarget_0() const { return ___m_attributeTarget_0; }
	inline int32_t* get_address_of_m_attributeTarget_0() { return &___m_attributeTarget_0; }
	inline void set_m_attributeTarget_0(int32_t value)
	{
		___m_attributeTarget_0 = value;
	}

	inline static int32_t get_offset_of_m_allowMultiple_1() { return static_cast<int32_t>(offsetof(AttributeUsageAttribute_tBB0BAAA82036E6FCDD80A688BBD039F6FFD8EA1C, ___m_allowMultiple_1)); }
	inline bool get_m_allowMultiple_1() const { return ___m_allowMultiple_1; }
	inline bool* get_address_of_m_allowMultiple_1() { return &___m_allowMultiple_1; }
	inline void set_m_allowMultiple_1(bool value)
	{
		___m_allowMultiple_1 = value;
	}

	inline static int32_t get_offset_of_m_inherited_2() { return static_cast<int32_t>(offsetof(AttributeUsageAttribute_tBB0BAAA82036E6FCDD80A688BBD039F6FFD8EA1C, ___m_inherited_2)); }
	inline bool get_m_inherited_2() const { return ___m_inherited_2; }
	inline bool* get_address_of_m_inherited_2() { return &___m_inherited_2; }
	inline void set_m_inherited_2(bool value)
	{
		___m_inherited_2 = value;
	}
};

struct AttributeUsageAttribute_tBB0BAAA82036E6FCDD80A688BBD039F6FFD8EA1C_StaticFields
{
public:
	// System.AttributeUsageAttribute System.AttributeUsageAttribute::Default
	AttributeUsageAttribute_tBB0BAAA82036E6FCDD80A688BBD039F6FFD8EA1C * ___Default_3;

public:
	inline static int32_t get_offset_of_Default_3() { return static_cast<int32_t>(offsetof(AttributeUsageAttribute_tBB0BAAA82036E6FCDD80A688BBD039F6FFD8EA1C_StaticFields, ___Default_3)); }
	inline AttributeUsageAttribute_tBB0BAAA82036E6FCDD80A688BBD039F6FFD8EA1C * get_Default_3() const { return ___Default_3; }
	inline AttributeUsageAttribute_tBB0BAAA82036E6FCDD80A688BBD039F6FFD8EA1C ** get_address_of_Default_3() { return &___Default_3; }
	inline void set_Default_3(AttributeUsageAttribute_tBB0BAAA82036E6FCDD80A688BBD039F6FFD8EA1C * value)
	{
		___Default_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___Default_3), (void*)value);
	}
};


// System.Diagnostics.DebuggableAttribute
struct DebuggableAttribute_tA8054EBD0FC7511695D494B690B5771658E3191B  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.Diagnostics.DebuggableAttribute/DebuggingModes System.Diagnostics.DebuggableAttribute::m_debuggingModes
	int32_t ___m_debuggingModes_0;

public:
	inline static int32_t get_offset_of_m_debuggingModes_0() { return static_cast<int32_t>(offsetof(DebuggableAttribute_tA8054EBD0FC7511695D494B690B5771658E3191B, ___m_debuggingModes_0)); }
	inline int32_t get_m_debuggingModes_0() const { return ___m_debuggingModes_0; }
	inline int32_t* get_address_of_m_debuggingModes_0() { return &___m_debuggingModes_0; }
	inline void set_m_debuggingModes_0(int32_t value)
	{
		___m_debuggingModes_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif



// System.Void System.CLSCompliantAttribute::.ctor(System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void CLSCompliantAttribute__ctor_m340EDA4DA5E45506AD631FE84241ADFB6B3F0270 (CLSCompliantAttribute_tA28EF6D4ADBD3C5C429DE9A70DD1E927C8906249 * __this, bool ___isCompliant0, const RuntimeMethod* method);
// System.Void System.Runtime.CompilerServices.CompilationRelaxationsAttribute::.ctor(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void CompilationRelaxationsAttribute__ctor_mAC3079EBC4EEAB474EED8208EF95DB39C922333B (CompilationRelaxationsAttribute_t661FDDC06629BDA607A42BD660944F039FE03AFF * __this, int32_t ___relaxations0, const RuntimeMethod* method);
// System.Void System.Runtime.CompilerServices.RuntimeCompatibilityAttribute::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void RuntimeCompatibilityAttribute__ctor_m551DDF1438CE97A984571949723F30F44CF7317C (RuntimeCompatibilityAttribute_tFF99AB2963098F9CBCD47A20D9FD3D51C17C1C80 * __this, const RuntimeMethod* method);
// System.Void System.Runtime.CompilerServices.RuntimeCompatibilityAttribute::set_WrapNonExceptionThrows(System.Boolean)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void RuntimeCompatibilityAttribute_set_WrapNonExceptionThrows_m8562196F90F3EBCEC23B5708EE0332842883C490_inline (RuntimeCompatibilityAttribute_tFF99AB2963098F9CBCD47A20D9FD3D51C17C1C80 * __this, bool ___value0, const RuntimeMethod* method);
// System.Void System.Diagnostics.DebuggableAttribute::.ctor(System.Diagnostics.DebuggableAttribute/DebuggingModes)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void DebuggableAttribute__ctor_m7FF445C8435494A4847123A668D889E692E55550 (DebuggableAttribute_tA8054EBD0FC7511695D494B690B5771658E3191B * __this, int32_t ___modes0, const RuntimeMethod* method);
// System.Void System.Reflection.AssemblyTitleAttribute::.ctor(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AssemblyTitleAttribute__ctor_mE239F206B3B369C48AE1F3B4211688778FE99E8D (AssemblyTitleAttribute_tABB894D0792C7F307694CC796C8AE5D6A20382E7 * __this, String_t* ___title0, const RuntimeMethod* method);
// System.Void System.Runtime.CompilerServices.InternalsVisibleToAttribute::.ctor(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void InternalsVisibleToAttribute__ctor_m420071A75DCEEC72356490C64B4B0B9270DA32B9 (InternalsVisibleToAttribute_t1D9772A02892BAC440952F880A43C257E6C3E68C * __this, String_t* ___assemblyName0, const RuntimeMethod* method);
// System.Void System.Runtime.CompilerServices.ExtensionAttribute::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * __this, const RuntimeMethod* method);
// System.Void System.Reflection.AssemblyConfigurationAttribute::.ctor(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AssemblyConfigurationAttribute__ctor_m6EE76F5A155EDEA71967A32F78D777038ADD0757 (AssemblyConfigurationAttribute_t071B324A83314FBA14A43F37BE7206C420218B7C * __this, String_t* ___configuration0, const RuntimeMethod* method);
// System.Void System.Reflection.AssemblyCompanyAttribute::.ctor(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AssemblyCompanyAttribute__ctor_m435C9FEC405646617645636E67860598A0C46FF0 (AssemblyCompanyAttribute_t642AAB097D7DEAAB623BEBE4664327E9B01D1DE4 * __this, String_t* ___company0, const RuntimeMethod* method);
// System.Void System.Reflection.AssemblyProductAttribute::.ctor(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AssemblyProductAttribute__ctor_m26DF1EBC1C86E7DA4786C66B44123899BE8DBCB8 (AssemblyProductAttribute_t6BB0E0F76C752E14A4C26B4D1E230019068601CA * __this, String_t* ___product0, const RuntimeMethod* method);
// System.Void System.Reflection.AssemblyCopyrightAttribute::.ctor(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AssemblyCopyrightAttribute__ctor_mB0B5F5C1A7A8B172289CC694E2711F07A37CE3F3 (AssemblyCopyrightAttribute_tA6A09319EF50B48D962810032000DEE7B12904EC * __this, String_t* ___copyright0, const RuntimeMethod* method);
// System.Void System.Reflection.AssemblyTrademarkAttribute::.ctor(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AssemblyTrademarkAttribute__ctor_m6FBD5AAE48F00120043AD8BECF2586896CFB6C02 (AssemblyTrademarkAttribute_t0602679435F8EBECC5DDB55CFE3A7A4A4CA2B5E2 * __this, String_t* ___trademark0, const RuntimeMethod* method);
// System.Void System.Runtime.InteropServices.ComVisibleAttribute::.ctor(System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ComVisibleAttribute__ctor_mBDE8E12A0233C07B98D6D5103511F4DD5B1FC172 (ComVisibleAttribute_tCE3DF5E341F3ECE4C81FE85C15B3D782AB302A2A * __this, bool ___visibility0, const RuntimeMethod* method);
// System.Void System.Runtime.InteropServices.GuidAttribute::.ctor(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void GuidAttribute__ctor_mCCEF3938DF601B23B5791CEE8F7AF05C98B6AFEA (GuidAttribute_tBB494B31270577CCD589ABBB159C18CDAE20D063 * __this, String_t* ___guid0, const RuntimeMethod* method);
// System.Void System.Reflection.AssemblyDescriptionAttribute::.ctor(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AssemblyDescriptionAttribute__ctor_m3A0BD500FF352A67235FBA499FBA58EFF15B1F25 (AssemblyDescriptionAttribute_tF4460CCB289F6E2F71841792BBC7E6907DF612B3 * __this, String_t* ___description0, const RuntimeMethod* method);
// System.Void System.Reflection.AssemblyFileVersionAttribute::.ctor(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AssemblyFileVersionAttribute__ctor_mF855AEBC51CB72F4FF913499256741AE57B0F13D (AssemblyFileVersionAttribute_tCC1036D0566155DC5688D9230EF3C07D82A1896F * __this, String_t* ___version0, const RuntimeMethod* method);
// System.Void System.FlagsAttribute::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void FlagsAttribute__ctor_mE8DCBA1BE0E6B0424FEF5E5F249733CF6A0E1229 (FlagsAttribute_t511C558FACEF1CC64702A8FAB67CAF3CBA65DF36 * __this, const RuntimeMethod* method);
// System.Void System.AttributeUsageAttribute::.ctor(System.AttributeTargets)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AttributeUsageAttribute__ctor_m5114E18826A49A025D48DC71904C430BD590656D (AttributeUsageAttribute_tBB0BAAA82036E6FCDD80A688BBD039F6FFD8EA1C * __this, int32_t ___validOn0, const RuntimeMethod* method);
// System.Void System.AttributeUsageAttribute::set_AllowMultiple(System.Boolean)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void AttributeUsageAttribute_set_AllowMultiple_mF412CDAFFE16D056721EF81A1EC04ACE63612055_inline (AttributeUsageAttribute_tBB0BAAA82036E6FCDD80A688BBD039F6FFD8EA1C * __this, bool ___value0, const RuntimeMethod* method);
// System.Void System.Runtime.CompilerServices.CompilerGeneratedAttribute::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35 (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * __this, const RuntimeMethod* method);
// System.Void System.Reflection.DefaultMemberAttribute::.ctor(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void DefaultMemberAttribute__ctor_mA025B6F5B3A9292696E01108027840C8DFF7F4D7 (DefaultMemberAttribute_t8C9B3330DEA69EE364962477FF14FD2CFE30D4B5 * __this, String_t* ___memberName0, const RuntimeMethod* method);
// System.Void System.ParamArrayAttribute::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ParamArrayAttribute__ctor_mCC72AFF718185BA7B87FD8D9471F1274400C5719 (ParamArrayAttribute_t9DCEB4CDDB8EDDB1124171D4C51FA6069EEA5C5F * __this, const RuntimeMethod* method);
// System.Void System.ObsoleteAttribute::.ctor(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ObsoleteAttribute__ctor_mAC32A5CCD287DA84CDA9F08282C1C8B0DB7B9868 (ObsoleteAttribute_t14BAC1669C0409EB9F28D72D664FFA6764ACD671 * __this, String_t* ___message0, const RuntimeMethod* method);
// System.Void System.Diagnostics.DebuggerHiddenAttribute::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3 (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * __this, const RuntimeMethod* method);
// System.Void System.AttributeUsageAttribute::set_Inherited(System.Boolean)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void AttributeUsageAttribute_set_Inherited_m56105980C36CB71AECD398C6077739BDFD2085E0_inline (AttributeUsageAttribute_tBB0BAAA82036E6FCDD80A688BBD039F6FFD8EA1C * __this, bool ___value0, const RuntimeMethod* method);
static void Newtonsoft_Json_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CLSCompliantAttribute_tA28EF6D4ADBD3C5C429DE9A70DD1E927C8906249 * tmp = (CLSCompliantAttribute_tA28EF6D4ADBD3C5C429DE9A70DD1E927C8906249 *)cache->attributes[0];
		CLSCompliantAttribute__ctor_m340EDA4DA5E45506AD631FE84241ADFB6B3F0270(tmp, true, NULL);
	}
	{
		CompilationRelaxationsAttribute_t661FDDC06629BDA607A42BD660944F039FE03AFF * tmp = (CompilationRelaxationsAttribute_t661FDDC06629BDA607A42BD660944F039FE03AFF *)cache->attributes[1];
		CompilationRelaxationsAttribute__ctor_mAC3079EBC4EEAB474EED8208EF95DB39C922333B(tmp, 8LL, NULL);
	}
	{
		RuntimeCompatibilityAttribute_tFF99AB2963098F9CBCD47A20D9FD3D51C17C1C80 * tmp = (RuntimeCompatibilityAttribute_tFF99AB2963098F9CBCD47A20D9FD3D51C17C1C80 *)cache->attributes[2];
		RuntimeCompatibilityAttribute__ctor_m551DDF1438CE97A984571949723F30F44CF7317C(tmp, NULL);
		RuntimeCompatibilityAttribute_set_WrapNonExceptionThrows_m8562196F90F3EBCEC23B5708EE0332842883C490_inline(tmp, true, NULL);
	}
	{
		DebuggableAttribute_tA8054EBD0FC7511695D494B690B5771658E3191B * tmp = (DebuggableAttribute_tA8054EBD0FC7511695D494B690B5771658E3191B *)cache->attributes[3];
		DebuggableAttribute__ctor_m7FF445C8435494A4847123A668D889E692E55550(tmp, 2LL, NULL);
	}
	{
		AssemblyTitleAttribute_tABB894D0792C7F307694CC796C8AE5D6A20382E7 * tmp = (AssemblyTitleAttribute_tABB894D0792C7F307694CC796C8AE5D6A20382E7 *)cache->attributes[4];
		AssemblyTitleAttribute__ctor_mE239F206B3B369C48AE1F3B4211688778FE99E8D(tmp, il2cpp_codegen_string_new_wrapper("\x4A\x73\x6F\x6E\x2E\x4E\x45\x54\x20\x55\x6E\x69\x74\x79\x33\x44\x20\x28\x4C\x69\x74\x65\x29"), NULL);
	}
	{
		InternalsVisibleToAttribute_t1D9772A02892BAC440952F880A43C257E6C3E68C * tmp = (InternalsVisibleToAttribute_t1D9772A02892BAC440952F880A43C257E6C3E68C *)cache->attributes[5];
		InternalsVisibleToAttribute__ctor_m420071A75DCEEC72356490C64B4B0B9270DA32B9(tmp, il2cpp_codegen_string_new_wrapper("\x4E\x65\x77\x74\x6F\x6E\x73\x6F\x66\x74\x2E\x4A\x73\x6F\x6E\x2E\x53\x63\x68\x65\x6D\x61"), NULL);
	}
	{
		InternalsVisibleToAttribute_t1D9772A02892BAC440952F880A43C257E6C3E68C * tmp = (InternalsVisibleToAttribute_t1D9772A02892BAC440952F880A43C257E6C3E68C *)cache->attributes[6];
		InternalsVisibleToAttribute__ctor_m420071A75DCEEC72356490C64B4B0B9270DA32B9(tmp, il2cpp_codegen_string_new_wrapper("\x4E\x65\x77\x74\x6F\x6E\x73\x6F\x66\x74\x2E\x4A\x73\x6F\x6E\x2E\x54\x65\x73\x74\x73"), NULL);
	}
	{
		InternalsVisibleToAttribute_t1D9772A02892BAC440952F880A43C257E6C3E68C * tmp = (InternalsVisibleToAttribute_t1D9772A02892BAC440952F880A43C257E6C3E68C *)cache->attributes[7];
		InternalsVisibleToAttribute__ctor_m420071A75DCEEC72356490C64B4B0B9270DA32B9(tmp, il2cpp_codegen_string_new_wrapper("\x41\x73\x73\x65\x6D\x62\x6C\x79\x2D\x43\x53\x68\x61\x72\x70"), NULL);
	}
	{
		InternalsVisibleToAttribute_t1D9772A02892BAC440952F880A43C257E6C3E68C * tmp = (InternalsVisibleToAttribute_t1D9772A02892BAC440952F880A43C257E6C3E68C *)cache->attributes[8];
		InternalsVisibleToAttribute__ctor_m420071A75DCEEC72356490C64B4B0B9270DA32B9(tmp, il2cpp_codegen_string_new_wrapper("\x41\x73\x73\x65\x6D\x62\x6C\x79\x2D\x43\x53\x68\x61\x72\x70\x2D\x45\x64\x69\x74\x6F\x72"), NULL);
	}
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[9];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
	{
		InternalsVisibleToAttribute_t1D9772A02892BAC440952F880A43C257E6C3E68C * tmp = (InternalsVisibleToAttribute_t1D9772A02892BAC440952F880A43C257E6C3E68C *)cache->attributes[10];
		InternalsVisibleToAttribute__ctor_m420071A75DCEEC72356490C64B4B0B9270DA32B9(tmp, il2cpp_codegen_string_new_wrapper("\x4E\x65\x77\x74\x6F\x6E\x73\x6F\x66\x74\x2E\x4A\x73\x6F\x6E\x2E\x44\x79\x6E\x61\x6D\x69\x63\x2C\x20\x50\x75\x62\x6C\x69\x63\x4B\x65\x79\x3D\x30\x30\x32\x34\x30\x30\x30\x30\x30\x34\x38\x30\x30\x30\x30\x30\x39\x34\x30\x30\x30\x30\x30\x30\x30\x36\x30\x32\x30\x30\x30\x30\x30\x30\x32\x34\x30\x30\x30\x30\x35\x32\x35\x33\x34\x31\x33\x31\x30\x30\x30\x34\x30\x30\x30\x30\x30\x31\x30\x30\x30\x31\x30\x30\x63\x62\x64\x38\x64\x35\x33\x62\x39\x64\x37\x64\x65\x33\x30\x66\x31\x66\x31\x32\x37\x38\x66\x36\x33\x36\x65\x63\x34\x36\x32\x63\x66\x39\x63\x32\x35\x34\x39\x39\x31\x32\x39\x31\x65\x36\x36\x65\x62\x62\x31\x35\x37\x61\x38\x38\x35\x36\x33\x38\x61\x35\x31\x37\x38\x38\x37\x36\x33\x33\x62\x38\x39\x38\x63\x63\x62\x63\x66\x30\x64\x35\x63\x35\x66\x66\x37\x62\x65\x38\x35\x61\x36\x61\x62\x65\x39\x65\x37\x36\x35\x64\x30\x61\x63\x37\x63\x64\x33\x33\x63\x36\x38\x64\x61\x63\x36\x37\x65\x37\x65\x36\x34\x35\x33\x30\x65\x38\x32\x32\x32\x31\x30\x31\x31\x30\x39\x66\x31\x35\x34\x61\x62\x31\x34\x61\x39\x34\x31\x63\x34\x39\x30\x61\x63\x31\x35\x35\x63\x64\x31\x64\x34\x66\x63\x62\x61\x30\x66\x61\x62\x62\x34\x39\x30\x31\x36\x62\x34\x65\x66\x32\x38\x35\x39\x33\x62\x30\x31\x35\x63\x61\x62\x35\x39\x33\x37\x64\x61\x33\x31\x31\x37\x32\x66\x30\x33\x66\x36\x37\x64\x30\x39\x65\x64\x64\x61\x34\x30\x34\x62\x38\x38\x61\x36\x30\x30\x32\x33\x66\x30\x36\x32\x61\x65\x37\x31\x64\x30\x62\x32\x65\x34\x34\x33\x38\x62\x37\x34\x63\x63\x31\x31\x64\x63\x39"), NULL);
	}
	{
		AssemblyConfigurationAttribute_t071B324A83314FBA14A43F37BE7206C420218B7C * tmp = (AssemblyConfigurationAttribute_t071B324A83314FBA14A43F37BE7206C420218B7C *)cache->attributes[11];
		AssemblyConfigurationAttribute__ctor_m6EE76F5A155EDEA71967A32F78D777038ADD0757(tmp, il2cpp_codegen_string_new_wrapper(""), NULL);
	}
	{
		AssemblyCompanyAttribute_t642AAB097D7DEAAB623BEBE4664327E9B01D1DE4 * tmp = (AssemblyCompanyAttribute_t642AAB097D7DEAAB623BEBE4664327E9B01D1DE4 *)cache->attributes[12];
		AssemblyCompanyAttribute__ctor_m435C9FEC405646617645636E67860598A0C46FF0(tmp, il2cpp_codegen_string_new_wrapper("\x4E\x65\x77\x74\x6F\x6E\x73\x6F\x66\x74"), NULL);
	}
	{
		AssemblyProductAttribute_t6BB0E0F76C752E14A4C26B4D1E230019068601CA * tmp = (AssemblyProductAttribute_t6BB0E0F76C752E14A4C26B4D1E230019068601CA *)cache->attributes[13];
		AssemblyProductAttribute__ctor_m26DF1EBC1C86E7DA4786C66B44123899BE8DBCB8(tmp, il2cpp_codegen_string_new_wrapper("\x4A\x73\x6F\x6E\x2E\x4E\x45\x54"), NULL);
	}
	{
		AssemblyCopyrightAttribute_tA6A09319EF50B48D962810032000DEE7B12904EC * tmp = (AssemblyCopyrightAttribute_tA6A09319EF50B48D962810032000DEE7B12904EC *)cache->attributes[14];
		AssemblyCopyrightAttribute__ctor_mB0B5F5C1A7A8B172289CC694E2711F07A37CE3F3(tmp, il2cpp_codegen_string_new_wrapper("\x43\x6F\x70\x79\x72\x69\x67\x68\x74\x20\xC2\xA9\x20\x4A\x61\x6D\x65\x73\x20\x4E\x65\x77\x74\x6F\x6E\x2D\x4B\x69\x6E\x67\x20\x32\x30\x30\x38"), NULL);
	}
	{
		AssemblyTrademarkAttribute_t0602679435F8EBECC5DDB55CFE3A7A4A4CA2B5E2 * tmp = (AssemblyTrademarkAttribute_t0602679435F8EBECC5DDB55CFE3A7A4A4CA2B5E2 *)cache->attributes[15];
		AssemblyTrademarkAttribute__ctor_m6FBD5AAE48F00120043AD8BECF2586896CFB6C02(tmp, il2cpp_codegen_string_new_wrapper(""), NULL);
	}
	{
		ComVisibleAttribute_tCE3DF5E341F3ECE4C81FE85C15B3D782AB302A2A * tmp = (ComVisibleAttribute_tCE3DF5E341F3ECE4C81FE85C15B3D782AB302A2A *)cache->attributes[16];
		ComVisibleAttribute__ctor_mBDE8E12A0233C07B98D6D5103511F4DD5B1FC172(tmp, false, NULL);
	}
	{
		GuidAttribute_tBB494B31270577CCD589ABBB159C18CDAE20D063 * tmp = (GuidAttribute_tBB494B31270577CCD589ABBB159C18CDAE20D063 *)cache->attributes[17];
		GuidAttribute__ctor_mCCEF3938DF601B23B5791CEE8F7AF05C98B6AFEA(tmp, il2cpp_codegen_string_new_wrapper("\x39\x63\x61\x33\x35\x38\x61\x61\x2D\x33\x31\x37\x62\x2D\x34\x39\x32\x35\x2D\x38\x61\x64\x61\x2D\x34\x61\x32\x39\x65\x39\x34\x33\x61\x33\x36\x33"), NULL);
	}
	{
		AssemblyDescriptionAttribute_tF4460CCB289F6E2F71841792BBC7E6907DF612B3 * tmp = (AssemblyDescriptionAttribute_tF4460CCB289F6E2F71841792BBC7E6907DF612B3 *)cache->attributes[18];
		AssemblyDescriptionAttribute__ctor_m3A0BD500FF352A67235FBA499FBA58EFF15B1F25(tmp, il2cpp_codegen_string_new_wrapper("\x4A\x73\x6F\x6E\x2E\x4E\x45\x54\x20\x69\x73\x20\x61\x20\x70\x6F\x70\x75\x6C\x61\x72\x20\x68\x69\x67\x68\x2D\x70\x65\x72\x66\x6F\x72\x6D\x61\x6E\x63\x65\x20\x4A\x53\x4F\x4E\x20\x66\x72\x61\x6D\x65\x77\x6F\x72\x6B\x20\x66\x6F\x72\x20\x2E\x4E\x45\x54"), NULL);
	}
	{
		AssemblyFileVersionAttribute_tCC1036D0566155DC5688D9230EF3C07D82A1896F * tmp = (AssemblyFileVersionAttribute_tCC1036D0566155DC5688D9230EF3C07D82A1896F *)cache->attributes[19];
		AssemblyFileVersionAttribute__ctor_mF855AEBC51CB72F4FF913499256741AE57B0F13D(tmp, il2cpp_codegen_string_new_wrapper("\x39\x2E\x30\x2E\x31\x2E\x31\x39\x38\x31\x33"), NULL);
	}
}
static void DefaultValueHandling_t1184EED87C3E17B62841C12572EB99E3A265660D_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		FlagsAttribute_t511C558FACEF1CC64702A8FAB67CAF3CBA65DF36 * tmp = (FlagsAttribute_t511C558FACEF1CC64702A8FAB67CAF3CBA65DF36 *)cache->attributes[0];
		FlagsAttribute__ctor_mE8DCBA1BE0E6B0424FEF5E5F249733CF6A0E1229(tmp, NULL);
	}
}
static void JsonArrayAttribute_tD3DA8286372EF5A93A7B5763096BB8FAE439DF75_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		AttributeUsageAttribute_tBB0BAAA82036E6FCDD80A688BBD039F6FFD8EA1C * tmp = (AttributeUsageAttribute_tBB0BAAA82036E6FCDD80A688BBD039F6FFD8EA1C *)cache->attributes[0];
		AttributeUsageAttribute__ctor_m5114E18826A49A025D48DC71904C430BD590656D(tmp, 1028LL, NULL);
		AttributeUsageAttribute_set_AllowMultiple_mF412CDAFFE16D056721EF81A1EC04ACE63612055_inline(tmp, false, NULL);
	}
}
static void JsonConstructorAttribute_t892706D7BC0282A74D608533AC8F56337D08F1D4_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		AttributeUsageAttribute_tBB0BAAA82036E6FCDD80A688BBD039F6FFD8EA1C * tmp = (AttributeUsageAttribute_tBB0BAAA82036E6FCDD80A688BBD039F6FFD8EA1C *)cache->attributes[0];
		AttributeUsageAttribute__ctor_m5114E18826A49A025D48DC71904C430BD590656D(tmp, 32LL, NULL);
		AttributeUsageAttribute_set_AllowMultiple_mF412CDAFFE16D056721EF81A1EC04ACE63612055_inline(tmp, false, NULL);
	}
}
static void JsonContainerAttribute_tDFAD9A1D2C6807836BC733B2D018398FB040332B_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		AttributeUsageAttribute_tBB0BAAA82036E6FCDD80A688BBD039F6FFD8EA1C * tmp = (AttributeUsageAttribute_tBB0BAAA82036E6FCDD80A688BBD039F6FFD8EA1C *)cache->attributes[0];
		AttributeUsageAttribute__ctor_m5114E18826A49A025D48DC71904C430BD590656D(tmp, 1028LL, NULL);
		AttributeUsageAttribute_set_AllowMultiple_mF412CDAFFE16D056721EF81A1EC04ACE63612055_inline(tmp, false, NULL);
	}
}
static void JsonContainerAttribute_tDFAD9A1D2C6807836BC733B2D018398FB040332B_CustomAttributesCacheGenerator_U3CItemConverterTypeU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void JsonContainerAttribute_tDFAD9A1D2C6807836BC733B2D018398FB040332B_CustomAttributesCacheGenerator_U3CItemConverterParametersU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void JsonContainerAttribute_tDFAD9A1D2C6807836BC733B2D018398FB040332B_CustomAttributesCacheGenerator_U3CNamingStrategyInstanceU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void JsonContainerAttribute_tDFAD9A1D2C6807836BC733B2D018398FB040332B_CustomAttributesCacheGenerator_JsonContainerAttribute_get_ItemConverterType_mE0959B267593FB7E6C69CC99954A1AB62C82551D(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void JsonContainerAttribute_tDFAD9A1D2C6807836BC733B2D018398FB040332B_CustomAttributesCacheGenerator_JsonContainerAttribute_get_ItemConverterParameters_mC22A7EB75C02CB7460E4D8E1DF4E3685C04413C5(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void JsonContainerAttribute_tDFAD9A1D2C6807836BC733B2D018398FB040332B_CustomAttributesCacheGenerator_JsonContainerAttribute_get_NamingStrategyInstance_mCB5C826F2FDD2091632B0FD8A25DDECF528C513C(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void JsonContainerAttribute_tDFAD9A1D2C6807836BC733B2D018398FB040332B_CustomAttributesCacheGenerator_JsonContainerAttribute_set_NamingStrategyInstance_mDF8D8278251CF937FC050DF7DC9B91C8F27BD5E4(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void JsonConvert_t4EB02F5D93E8B048375502BB1CF9DF714A930F07_CustomAttributesCacheGenerator_U3CDefaultSettingsU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void JsonConvert_t4EB02F5D93E8B048375502BB1CF9DF714A930F07_CustomAttributesCacheGenerator_JsonConvert_get_DefaultSettings_m214B88ECC58290FE283050B8A4C2E08F1755C83A(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void JsonConverterAttribute_t9794A7BB2F2729A673B5D04D9F86EFA140116F54_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		AttributeUsageAttribute_tBB0BAAA82036E6FCDD80A688BBD039F6FFD8EA1C * tmp = (AttributeUsageAttribute_tBB0BAAA82036E6FCDD80A688BBD039F6FFD8EA1C *)cache->attributes[0];
		AttributeUsageAttribute__ctor_m5114E18826A49A025D48DC71904C430BD590656D(tmp, 3484LL, NULL);
		AttributeUsageAttribute_set_AllowMultiple_mF412CDAFFE16D056721EF81A1EC04ACE63612055_inline(tmp, false, NULL);
	}
}
static void JsonConverterAttribute_t9794A7BB2F2729A673B5D04D9F86EFA140116F54_CustomAttributesCacheGenerator_U3CConverterParametersU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void JsonConverterAttribute_t9794A7BB2F2729A673B5D04D9F86EFA140116F54_CustomAttributesCacheGenerator_JsonConverterAttribute_get_ConverterParameters_m17E8F4F8CD589CD1E5C2D140C98F8B8863ADBF68(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void JsonDictionaryAttribute_tD03A79C1DB8271FF7805146BD10C58E572FC5878_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		AttributeUsageAttribute_tBB0BAAA82036E6FCDD80A688BBD039F6FFD8EA1C * tmp = (AttributeUsageAttribute_tBB0BAAA82036E6FCDD80A688BBD039F6FFD8EA1C *)cache->attributes[0];
		AttributeUsageAttribute__ctor_m5114E18826A49A025D48DC71904C430BD590656D(tmp, 1028LL, NULL);
		AttributeUsageAttribute_set_AllowMultiple_mF412CDAFFE16D056721EF81A1EC04ACE63612055_inline(tmp, false, NULL);
	}
}
static void JsonExtensionDataAttribute_t742C5494ED033ECB5BFA1DC6C68D6AF3C62E753C_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		AttributeUsageAttribute_tBB0BAAA82036E6FCDD80A688BBD039F6FFD8EA1C * tmp = (AttributeUsageAttribute_tBB0BAAA82036E6FCDD80A688BBD039F6FFD8EA1C *)cache->attributes[0];
		AttributeUsageAttribute__ctor_m5114E18826A49A025D48DC71904C430BD590656D(tmp, 384LL, NULL);
		AttributeUsageAttribute_set_AllowMultiple_mF412CDAFFE16D056721EF81A1EC04ACE63612055_inline(tmp, false, NULL);
	}
}
static void JsonExtensionDataAttribute_t742C5494ED033ECB5BFA1DC6C68D6AF3C62E753C_CustomAttributesCacheGenerator_U3CWriteDataU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void JsonExtensionDataAttribute_t742C5494ED033ECB5BFA1DC6C68D6AF3C62E753C_CustomAttributesCacheGenerator_U3CReadDataU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void JsonExtensionDataAttribute_t742C5494ED033ECB5BFA1DC6C68D6AF3C62E753C_CustomAttributesCacheGenerator_JsonExtensionDataAttribute_get_WriteData_m0718786B6D6C25112594F098A8CF31A8C90DC24A(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void JsonExtensionDataAttribute_t742C5494ED033ECB5BFA1DC6C68D6AF3C62E753C_CustomAttributesCacheGenerator_JsonExtensionDataAttribute_get_ReadData_mBF0CDE7323AB877197F0EC890140CA3CD80BA36F(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void JsonIgnoreAttribute_t2403F9AE18EEEA26B245AACF880F110B7DAA5E4C_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		AttributeUsageAttribute_tBB0BAAA82036E6FCDD80A688BBD039F6FFD8EA1C * tmp = (AttributeUsageAttribute_tBB0BAAA82036E6FCDD80A688BBD039F6FFD8EA1C *)cache->attributes[0];
		AttributeUsageAttribute__ctor_m5114E18826A49A025D48DC71904C430BD590656D(tmp, 384LL, NULL);
		AttributeUsageAttribute_set_AllowMultiple_mF412CDAFFE16D056721EF81A1EC04ACE63612055_inline(tmp, false, NULL);
	}
}
static void JsonObjectAttribute_t6B942E29C72B71A71664A8CD6C92C74DB4619E16_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		AttributeUsageAttribute_tBB0BAAA82036E6FCDD80A688BBD039F6FFD8EA1C * tmp = (AttributeUsageAttribute_tBB0BAAA82036E6FCDD80A688BBD039F6FFD8EA1C *)cache->attributes[0];
		AttributeUsageAttribute__ctor_m5114E18826A49A025D48DC71904C430BD590656D(tmp, 1036LL, NULL);
		AttributeUsageAttribute_set_AllowMultiple_mF412CDAFFE16D056721EF81A1EC04ACE63612055_inline(tmp, false, NULL);
	}
}
static void JsonPropertyAttribute_tC08FC7586306803D19861D2F27F971556191A976_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		AttributeUsageAttribute_tBB0BAAA82036E6FCDD80A688BBD039F6FFD8EA1C * tmp = (AttributeUsageAttribute_tBB0BAAA82036E6FCDD80A688BBD039F6FFD8EA1C *)cache->attributes[0];
		AttributeUsageAttribute__ctor_m5114E18826A49A025D48DC71904C430BD590656D(tmp, 2432LL, NULL);
		AttributeUsageAttribute_set_AllowMultiple_mF412CDAFFE16D056721EF81A1EC04ACE63612055_inline(tmp, false, NULL);
	}
}
static void JsonPropertyAttribute_tC08FC7586306803D19861D2F27F971556191A976_CustomAttributesCacheGenerator_U3CItemConverterTypeU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void JsonPropertyAttribute_tC08FC7586306803D19861D2F27F971556191A976_CustomAttributesCacheGenerator_U3CItemConverterParametersU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void JsonPropertyAttribute_tC08FC7586306803D19861D2F27F971556191A976_CustomAttributesCacheGenerator_U3CNamingStrategyTypeU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void JsonPropertyAttribute_tC08FC7586306803D19861D2F27F971556191A976_CustomAttributesCacheGenerator_U3CNamingStrategyParametersU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void JsonPropertyAttribute_tC08FC7586306803D19861D2F27F971556191A976_CustomAttributesCacheGenerator_U3CPropertyNameU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void JsonPropertyAttribute_tC08FC7586306803D19861D2F27F971556191A976_CustomAttributesCacheGenerator_JsonPropertyAttribute_get_ItemConverterType_mC4581A062D9DCF64DB3CE11BE06771A9347D9EB6(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void JsonPropertyAttribute_tC08FC7586306803D19861D2F27F971556191A976_CustomAttributesCacheGenerator_JsonPropertyAttribute_get_ItemConverterParameters_m3845C44A0AD6345D2E53707EBCDA7DEA54FC8CC8(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void JsonPropertyAttribute_tC08FC7586306803D19861D2F27F971556191A976_CustomAttributesCacheGenerator_JsonPropertyAttribute_get_NamingStrategyType_m9BD354DEDD5126AB21DA4D7C2A49A4396014BDE8(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void JsonPropertyAttribute_tC08FC7586306803D19861D2F27F971556191A976_CustomAttributesCacheGenerator_JsonPropertyAttribute_get_NamingStrategyParameters_mD0BC1CD962875A82871498CD866B6D840BF93196(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void JsonPropertyAttribute_tC08FC7586306803D19861D2F27F971556191A976_CustomAttributesCacheGenerator_JsonPropertyAttribute_get_PropertyName_m23FFB79CC575178480990AD5FB0FD1152A912D99(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void JsonReader_tD653CD612543D2FCECBC0A7B94961B76955BDA91_CustomAttributesCacheGenerator_U3CCloseInputU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void JsonReader_tD653CD612543D2FCECBC0A7B94961B76955BDA91_CustomAttributesCacheGenerator_U3CSupportMultipleContentU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void JsonReader_tD653CD612543D2FCECBC0A7B94961B76955BDA91_CustomAttributesCacheGenerator_JsonReader_get_CloseInput_m87EB3CF7C34E7FBA9BA84B812CC4CAE0B9FDE974(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void JsonReader_tD653CD612543D2FCECBC0A7B94961B76955BDA91_CustomAttributesCacheGenerator_JsonReader_set_CloseInput_mE443BF3B39BAE245D6D22180962F4F3B92C95EFD(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void JsonReader_tD653CD612543D2FCECBC0A7B94961B76955BDA91_CustomAttributesCacheGenerator_JsonReader_get_SupportMultipleContent_mB43563289F840AA64EE3133642CFC1D3340B2912(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void JsonReaderException_tBA4BC064CAFF75520942A57C880639F7B05ED591_CustomAttributesCacheGenerator_U3CLineNumberU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void JsonReaderException_tBA4BC064CAFF75520942A57C880639F7B05ED591_CustomAttributesCacheGenerator_U3CLinePositionU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void JsonReaderException_tBA4BC064CAFF75520942A57C880639F7B05ED591_CustomAttributesCacheGenerator_U3CPathU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void JsonReaderException_tBA4BC064CAFF75520942A57C880639F7B05ED591_CustomAttributesCacheGenerator_JsonReaderException_set_LineNumber_m9F168878C1D77923580E25AB5072BB89A2A1FE93(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void JsonReaderException_tBA4BC064CAFF75520942A57C880639F7B05ED591_CustomAttributesCacheGenerator_JsonReaderException_set_LinePosition_mCCABCCE6749D6A36BF93E146FEBCA1E539F83FDA(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void JsonReaderException_tBA4BC064CAFF75520942A57C880639F7B05ED591_CustomAttributesCacheGenerator_JsonReaderException_set_Path_mC99BD3BB99E4F85727B345B5CCDA60E43AB30AE0(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void JsonRequiredAttribute_t85A41B9B51790AC5ECB5DE64B221A52E91C835D9_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		AttributeUsageAttribute_tBB0BAAA82036E6FCDD80A688BBD039F6FFD8EA1C * tmp = (AttributeUsageAttribute_tBB0BAAA82036E6FCDD80A688BBD039F6FFD8EA1C *)cache->attributes[0];
		AttributeUsageAttribute__ctor_m5114E18826A49A025D48DC71904C430BD590656D(tmp, 384LL, NULL);
		AttributeUsageAttribute_set_AllowMultiple_mF412CDAFFE16D056721EF81A1EC04ACE63612055_inline(tmp, false, NULL);
	}
}
static void JsonSerializer_tCB6DBE5B2AA2EE465979EB49802CE4ECC4F141E3_CustomAttributesCacheGenerator_Error(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void JsonSerializer_tCB6DBE5B2AA2EE465979EB49802CE4ECC4F141E3_CustomAttributesCacheGenerator_JsonSerializer_add_Error_m5C077501D7D73DDF93019D736A6F72FCB733EFDD(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void JsonSerializer_tCB6DBE5B2AA2EE465979EB49802CE4ECC4F141E3_CustomAttributesCacheGenerator_JsonSerializer_remove_Error_mF178375434BC06707ADEE0A7DC544296D5146B76(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void JsonSerializerSettings_t7F7FFB2ACF58FFD6B39567AB672FA4ED7464BB1B_CustomAttributesCacheGenerator_U3CConvertersU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void JsonSerializerSettings_t7F7FFB2ACF58FFD6B39567AB672FA4ED7464BB1B_CustomAttributesCacheGenerator_U3CContractResolverU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void JsonSerializerSettings_t7F7FFB2ACF58FFD6B39567AB672FA4ED7464BB1B_CustomAttributesCacheGenerator_U3CEqualityComparerU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void JsonSerializerSettings_t7F7FFB2ACF58FFD6B39567AB672FA4ED7464BB1B_CustomAttributesCacheGenerator_U3CReferenceResolverProviderU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void JsonSerializerSettings_t7F7FFB2ACF58FFD6B39567AB672FA4ED7464BB1B_CustomAttributesCacheGenerator_U3CTraceWriterU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void JsonSerializerSettings_t7F7FFB2ACF58FFD6B39567AB672FA4ED7464BB1B_CustomAttributesCacheGenerator_U3CBinderU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void JsonSerializerSettings_t7F7FFB2ACF58FFD6B39567AB672FA4ED7464BB1B_CustomAttributesCacheGenerator_U3CErrorU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void JsonSerializerSettings_t7F7FFB2ACF58FFD6B39567AB672FA4ED7464BB1B_CustomAttributesCacheGenerator_JsonSerializerSettings_get_Converters_m862DFE5BDA2760F5946FB6DDB34DCA67FC6FF29D(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void JsonSerializerSettings_t7F7FFB2ACF58FFD6B39567AB672FA4ED7464BB1B_CustomAttributesCacheGenerator_JsonSerializerSettings_get_ContractResolver_mAE8A1AB42B6FA02463E9A3B613A777983107D591(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void JsonSerializerSettings_t7F7FFB2ACF58FFD6B39567AB672FA4ED7464BB1B_CustomAttributesCacheGenerator_JsonSerializerSettings_get_EqualityComparer_m41C02E38C29AF6A4CAC4894CC79FA724974CDC82(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void JsonSerializerSettings_t7F7FFB2ACF58FFD6B39567AB672FA4ED7464BB1B_CustomAttributesCacheGenerator_JsonSerializerSettings_get_ReferenceResolverProvider_mE84D665F375BFFC7BF287CB869DDEF0BE408853D(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void JsonSerializerSettings_t7F7FFB2ACF58FFD6B39567AB672FA4ED7464BB1B_CustomAttributesCacheGenerator_JsonSerializerSettings_get_TraceWriter_mE3D106D673BF56399607EADC698FAC00AFE65C72(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void JsonSerializerSettings_t7F7FFB2ACF58FFD6B39567AB672FA4ED7464BB1B_CustomAttributesCacheGenerator_JsonSerializerSettings_get_Binder_m9508F5EA174FF976E69F0C78B3934BFC56E2F035(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void JsonSerializerSettings_t7F7FFB2ACF58FFD6B39567AB672FA4ED7464BB1B_CustomAttributesCacheGenerator_JsonSerializerSettings_get_Error_mEDC7BE577D288DD35B65F534E00C681854578371(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void JsonTextWriter_t33A7556B4AA038ACC39A2D995AA874612F62A10B_CustomAttributesCacheGenerator_JsonTextWriter_WriteValue_m5BC424CE917FE84086E29B839EC6A292C75EE795(CustomAttributesCache* cache)
{
	{
		CLSCompliantAttribute_tA28EF6D4ADBD3C5C429DE9A70DD1E927C8906249 * tmp = (CLSCompliantAttribute_tA28EF6D4ADBD3C5C429DE9A70DD1E927C8906249 *)cache->attributes[0];
		CLSCompliantAttribute__ctor_m340EDA4DA5E45506AD631FE84241ADFB6B3F0270(tmp, false, NULL);
	}
}
static void JsonTextWriter_t33A7556B4AA038ACC39A2D995AA874612F62A10B_CustomAttributesCacheGenerator_JsonTextWriter_WriteValue_m14AAEAAC3218C11F806E7BD4E7F4D0592B321268(CustomAttributesCache* cache)
{
	{
		CLSCompliantAttribute_tA28EF6D4ADBD3C5C429DE9A70DD1E927C8906249 * tmp = (CLSCompliantAttribute_tA28EF6D4ADBD3C5C429DE9A70DD1E927C8906249 *)cache->attributes[0];
		CLSCompliantAttribute__ctor_m340EDA4DA5E45506AD631FE84241ADFB6B3F0270(tmp, false, NULL);
	}
}
static void JsonTextWriter_t33A7556B4AA038ACC39A2D995AA874612F62A10B_CustomAttributesCacheGenerator_JsonTextWriter_WriteValue_mD772006CDE4E7F9E184D192814132E2D4EC77884(CustomAttributesCache* cache)
{
	{
		CLSCompliantAttribute_tA28EF6D4ADBD3C5C429DE9A70DD1E927C8906249 * tmp = (CLSCompliantAttribute_tA28EF6D4ADBD3C5C429DE9A70DD1E927C8906249 *)cache->attributes[0];
		CLSCompliantAttribute__ctor_m340EDA4DA5E45506AD631FE84241ADFB6B3F0270(tmp, false, NULL);
	}
}
static void JsonTextWriter_t33A7556B4AA038ACC39A2D995AA874612F62A10B_CustomAttributesCacheGenerator_JsonTextWriter_WriteValue_m909BF8D81AD7BD056580595C10A84ECAEC244C67(CustomAttributesCache* cache)
{
	{
		CLSCompliantAttribute_tA28EF6D4ADBD3C5C429DE9A70DD1E927C8906249 * tmp = (CLSCompliantAttribute_tA28EF6D4ADBD3C5C429DE9A70DD1E927C8906249 *)cache->attributes[0];
		CLSCompliantAttribute__ctor_m340EDA4DA5E45506AD631FE84241ADFB6B3F0270(tmp, false, NULL);
	}
}
static void JsonWriter_tDA82E9438C69ABAC3DF260ADBF20E9E287D41BB2_CustomAttributesCacheGenerator_U3CCloseOutputU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void JsonWriter_tDA82E9438C69ABAC3DF260ADBF20E9E287D41BB2_CustomAttributesCacheGenerator_JsonWriter_get_CloseOutput_m5CADEDBE98FDF046F9BF209C242A07DB08C9678B(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void JsonWriter_tDA82E9438C69ABAC3DF260ADBF20E9E287D41BB2_CustomAttributesCacheGenerator_JsonWriter_set_CloseOutput_m583E020AD56A73E77CDE34C208DB0115E067C7E0(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void JsonWriter_tDA82E9438C69ABAC3DF260ADBF20E9E287D41BB2_CustomAttributesCacheGenerator_JsonWriter_WriteValue_m9B2A0C880487EFEE143F0A2F0282A24F58E39BB6(CustomAttributesCache* cache)
{
	{
		CLSCompliantAttribute_tA28EF6D4ADBD3C5C429DE9A70DD1E927C8906249 * tmp = (CLSCompliantAttribute_tA28EF6D4ADBD3C5C429DE9A70DD1E927C8906249 *)cache->attributes[0];
		CLSCompliantAttribute__ctor_m340EDA4DA5E45506AD631FE84241ADFB6B3F0270(tmp, false, NULL);
	}
}
static void JsonWriter_tDA82E9438C69ABAC3DF260ADBF20E9E287D41BB2_CustomAttributesCacheGenerator_JsonWriter_WriteValue_m3B79659B76CE197EA62C98D9380CA276779BA041(CustomAttributesCache* cache)
{
	{
		CLSCompliantAttribute_tA28EF6D4ADBD3C5C429DE9A70DD1E927C8906249 * tmp = (CLSCompliantAttribute_tA28EF6D4ADBD3C5C429DE9A70DD1E927C8906249 *)cache->attributes[0];
		CLSCompliantAttribute__ctor_m340EDA4DA5E45506AD631FE84241ADFB6B3F0270(tmp, false, NULL);
	}
}
static void JsonWriter_tDA82E9438C69ABAC3DF260ADBF20E9E287D41BB2_CustomAttributesCacheGenerator_JsonWriter_WriteValue_mD04367BAD3BA9FC4D4991C0BA35B49DA15675F96(CustomAttributesCache* cache)
{
	{
		CLSCompliantAttribute_tA28EF6D4ADBD3C5C429DE9A70DD1E927C8906249 * tmp = (CLSCompliantAttribute_tA28EF6D4ADBD3C5C429DE9A70DD1E927C8906249 *)cache->attributes[0];
		CLSCompliantAttribute__ctor_m340EDA4DA5E45506AD631FE84241ADFB6B3F0270(tmp, false, NULL);
	}
}
static void JsonWriter_tDA82E9438C69ABAC3DF260ADBF20E9E287D41BB2_CustomAttributesCacheGenerator_JsonWriter_WriteValue_m402DBD63BF18B24EAA0D34D7D6208CE8C696F3AF(CustomAttributesCache* cache)
{
	{
		CLSCompliantAttribute_tA28EF6D4ADBD3C5C429DE9A70DD1E927C8906249 * tmp = (CLSCompliantAttribute_tA28EF6D4ADBD3C5C429DE9A70DD1E927C8906249 *)cache->attributes[0];
		CLSCompliantAttribute__ctor_m340EDA4DA5E45506AD631FE84241ADFB6B3F0270(tmp, false, NULL);
	}
}
static void JsonWriter_tDA82E9438C69ABAC3DF260ADBF20E9E287D41BB2_CustomAttributesCacheGenerator_JsonWriter_WriteValue_m55DD87A8EFEBB6A73C70571592F6F2B63DFED99D(CustomAttributesCache* cache)
{
	{
		CLSCompliantAttribute_tA28EF6D4ADBD3C5C429DE9A70DD1E927C8906249 * tmp = (CLSCompliantAttribute_tA28EF6D4ADBD3C5C429DE9A70DD1E927C8906249 *)cache->attributes[0];
		CLSCompliantAttribute__ctor_m340EDA4DA5E45506AD631FE84241ADFB6B3F0270(tmp, false, NULL);
	}
}
static void JsonWriter_tDA82E9438C69ABAC3DF260ADBF20E9E287D41BB2_CustomAttributesCacheGenerator_JsonWriter_WriteValue_mE256A509ADFD1D2EB3C2CE59C48BD4B5F9A7D075(CustomAttributesCache* cache)
{
	{
		CLSCompliantAttribute_tA28EF6D4ADBD3C5C429DE9A70DD1E927C8906249 * tmp = (CLSCompliantAttribute_tA28EF6D4ADBD3C5C429DE9A70DD1E927C8906249 *)cache->attributes[0];
		CLSCompliantAttribute__ctor_m340EDA4DA5E45506AD631FE84241ADFB6B3F0270(tmp, false, NULL);
	}
}
static void JsonWriter_tDA82E9438C69ABAC3DF260ADBF20E9E287D41BB2_CustomAttributesCacheGenerator_JsonWriter_WriteValue_m497A20806672585D0FBD37DF1707485B294C5C3F(CustomAttributesCache* cache)
{
	{
		CLSCompliantAttribute_tA28EF6D4ADBD3C5C429DE9A70DD1E927C8906249 * tmp = (CLSCompliantAttribute_tA28EF6D4ADBD3C5C429DE9A70DD1E927C8906249 *)cache->attributes[0];
		CLSCompliantAttribute__ctor_m340EDA4DA5E45506AD631FE84241ADFB6B3F0270(tmp, false, NULL);
	}
}
static void JsonWriter_tDA82E9438C69ABAC3DF260ADBF20E9E287D41BB2_CustomAttributesCacheGenerator_JsonWriter_WriteValue_m06116AF904E6923B19234D2787602E24995B8F7E(CustomAttributesCache* cache)
{
	{
		CLSCompliantAttribute_tA28EF6D4ADBD3C5C429DE9A70DD1E927C8906249 * tmp = (CLSCompliantAttribute_tA28EF6D4ADBD3C5C429DE9A70DD1E927C8906249 *)cache->attributes[0];
		CLSCompliantAttribute__ctor_m340EDA4DA5E45506AD631FE84241ADFB6B3F0270(tmp, false, NULL);
	}
}
static void JsonWriterException_t984447F6A0D6DD5CBE45299A34E85119DB0BFC2F_CustomAttributesCacheGenerator_U3CPathU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void JsonWriterException_t984447F6A0D6DD5CBE45299A34E85119DB0BFC2F_CustomAttributesCacheGenerator_JsonWriterException_set_Path_mD53E9F4BDDD8ED20EB04BF6CCE733F2DCC593BD9(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void PreserveReferencesHandling_t1AA650F51C415CEB2460B728A5A0F56F8C897E1E_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		FlagsAttribute_t511C558FACEF1CC64702A8FAB67CAF3CBA65DF36 * tmp = (FlagsAttribute_t511C558FACEF1CC64702A8FAB67CAF3CBA65DF36 *)cache->attributes[0];
		FlagsAttribute__ctor_mE8DCBA1BE0E6B0424FEF5E5F249733CF6A0E1229(tmp, NULL);
	}
}
static void TypeNameHandling_t00FDBF2FB49B273344C8A6CA3B6685EDFE82AF46_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		FlagsAttribute_t511C558FACEF1CC64702A8FAB67CAF3CBA65DF36 * tmp = (FlagsAttribute_t511C558FACEF1CC64702A8FAB67CAF3CBA65DF36 *)cache->attributes[0];
		FlagsAttribute__ctor_mE8DCBA1BE0E6B0424FEF5E5F249733CF6A0E1229(tmp, NULL);
	}
}
static void CollectionUtils_tEA257A374A8D8D66306313AE24968769E787DFB8_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void CollectionUtils_tEA257A374A8D8D66306313AE24968769E787DFB8_CustomAttributesCacheGenerator_CollectionUtils_AddRange_mBDCB767F18960E8B5579E9154A3DED0B0875D6AB(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void CollectionUtils_tEA257A374A8D8D66306313AE24968769E787DFB8_CustomAttributesCacheGenerator_CollectionUtils_AddRange_m20628EBA8AA00702EA16A009D8E2B46065F63085(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void CollectionUtils_tEA257A374A8D8D66306313AE24968769E787DFB8_CustomAttributesCacheGenerator_CollectionUtils_IndexOf_m311AF18D5D87B26DA1B611F52081180396BDE116(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void CollectionUtils_tEA257A374A8D8D66306313AE24968769E787DFB8_CustomAttributesCacheGenerator_CollectionUtils_Contains_m3BD5EF73B3804FE80FB14DD5B92326202AE01498(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void TypeInformation_t30731224CF3BE05F8F32F6DC4FD5FAFFD4F6997D_CustomAttributesCacheGenerator_U3CTypeU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void TypeInformation_t30731224CF3BE05F8F32F6DC4FD5FAFFD4F6997D_CustomAttributesCacheGenerator_U3CTypeCodeU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void TypeInformation_t30731224CF3BE05F8F32F6DC4FD5FAFFD4F6997D_CustomAttributesCacheGenerator_TypeInformation_get_Type_mF16FF676DAA9DEC81AF0A9FB81DA0834709F1F9D(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void TypeInformation_t30731224CF3BE05F8F32F6DC4FD5FAFFD4F6997D_CustomAttributesCacheGenerator_TypeInformation_set_Type_m87F6F17BD4A47C4F020DBC8E25462D3F4651B750(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void TypeInformation_t30731224CF3BE05F8F32F6DC4FD5FAFFD4F6997D_CustomAttributesCacheGenerator_TypeInformation_get_TypeCode_m3F9374AB9A57BA647C5B53B633D16102723C8435(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void TypeInformation_t30731224CF3BE05F8F32F6DC4FD5FAFFD4F6997D_CustomAttributesCacheGenerator_TypeInformation_set_TypeCode_mD82AA32EF804000604122B9541C621AF00622767(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CU3Ec__DisplayClass9_0_t3A4E56C60CDCDA0E3AE51AF8172FB902A9BBC187_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void DateTimeUtils_t45E6C3A51ACB4C69919336FD7872190C2ABEC404_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void DateTimeUtils_t45E6C3A51ACB4C69919336FD7872190C2ABEC404_CustomAttributesCacheGenerator_DateTimeUtils_GetUtcOffset_m1B6B70139EF050BAA434164D316C6619F5FE292A(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void DictionaryWrapper_2_tAFD71BD180AB00234FC505689605910F8E273CC4_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		DefaultMemberAttribute_t8C9B3330DEA69EE364962477FF14FD2CFE30D4B5 * tmp = (DefaultMemberAttribute_t8C9B3330DEA69EE364962477FF14FD2CFE30D4B5 *)cache->attributes[0];
		DefaultMemberAttribute__ctor_mA025B6F5B3A9292696E01108027840C8DFF7F4D7(tmp, il2cpp_codegen_string_new_wrapper("\x49\x74\x65\x6D"), NULL);
	}
}
static void U3CU3Ec_tC317791BE9F418DD0572473673C4C8FBBA96DB72_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CU3Ec__DisplayClass3_0_t698D68F8A4832DC94BD91657BD958CE2ECB410E0_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CU3Ec__DisplayClass4_0_1_t1548DAB3294A751C98FB408011DDB4C7567B79A0_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CU3Ec__DisplayClass5_0_1_tAF04C3EBC11CA31E3E339AEE8C90FD877EC8F3F1_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CU3Ec__DisplayClass6_0_1_t033847C9D5EC6238D52290988D716C1C3FD74441_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CU3Ec__DisplayClass7_0_1_tB1B860CA798AA13CFD191E3C10ADFDA00F996CF2_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CU3Ec__DisplayClass8_0_1_t97B8B8B5A6BD38CC528E90C0CFF2440E539218CD_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CU3Ec__DisplayClass9_0_1_tEDB18B86DFE4957BE7801C439CA96AC2CD0BA550_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void MethodCall_2_tA79249CAB2CBC748AB9ACD584E1DD90058556A97_CustomAttributesCacheGenerator_MethodCall_2_Invoke_m4563B61D6ADB928FC200C74AE5FCFAE5CEBDCB0B____args1(CustomAttributesCache* cache)
{
	{
		ParamArrayAttribute_t9DCEB4CDDB8EDDB1124171D4C51FA6069EEA5C5F * tmp = (ParamArrayAttribute_t9DCEB4CDDB8EDDB1124171D4C51FA6069EEA5C5F *)cache->attributes[0];
		ParamArrayAttribute__ctor_mCC72AFF718185BA7B87FD8D9471F1274400C5719(tmp, NULL);
	}
}
static void ReflectionMember_t7C544D9360A243D9DF6E05C5F156138C33DAA99D_CustomAttributesCacheGenerator_U3CMemberTypeU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ReflectionMember_t7C544D9360A243D9DF6E05C5F156138C33DAA99D_CustomAttributesCacheGenerator_U3CGetterU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ReflectionMember_t7C544D9360A243D9DF6E05C5F156138C33DAA99D_CustomAttributesCacheGenerator_U3CSetterU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ReflectionMember_t7C544D9360A243D9DF6E05C5F156138C33DAA99D_CustomAttributesCacheGenerator_ReflectionMember_get_MemberType_m6B83E257F42DD1C53558CD81BE74005F43794860(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ReflectionMember_t7C544D9360A243D9DF6E05C5F156138C33DAA99D_CustomAttributesCacheGenerator_ReflectionMember_set_MemberType_m5909AE0B8D67C06D8069E6FC9676EFBEAE0F3CD9(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ReflectionMember_t7C544D9360A243D9DF6E05C5F156138C33DAA99D_CustomAttributesCacheGenerator_ReflectionMember_get_Getter_mCF5EE2696804BA8D7E7A2DB16B09840656FF9D35(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ReflectionMember_t7C544D9360A243D9DF6E05C5F156138C33DAA99D_CustomAttributesCacheGenerator_ReflectionMember_set_Getter_mB7C5D68B17069E9B971342D29BBCE72CE33F91CD(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ReflectionMember_t7C544D9360A243D9DF6E05C5F156138C33DAA99D_CustomAttributesCacheGenerator_ReflectionMember_set_Setter_mD573E9151D6A27AA511FA936A47E43A25BD33DB7(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ReflectionObject_tC343774B9EE64C90DF1DBB77EEC193C4C9DD7361_CustomAttributesCacheGenerator_U3CCreatorU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ReflectionObject_tC343774B9EE64C90DF1DBB77EEC193C4C9DD7361_CustomAttributesCacheGenerator_U3CMembersU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ReflectionObject_tC343774B9EE64C90DF1DBB77EEC193C4C9DD7361_CustomAttributesCacheGenerator_ReflectionObject_get_Creator_mC6AD5773E9CC38F72E1282A59B462E45FCE60164(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ReflectionObject_tC343774B9EE64C90DF1DBB77EEC193C4C9DD7361_CustomAttributesCacheGenerator_ReflectionObject_set_Creator_m1E3FB878C8548FA0D022F47BB82426A110FD394A(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ReflectionObject_tC343774B9EE64C90DF1DBB77EEC193C4C9DD7361_CustomAttributesCacheGenerator_ReflectionObject_get_Members_m58FCDAAA8FDBE6913E756A598748FC9856C1A8EA(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ReflectionObject_tC343774B9EE64C90DF1DBB77EEC193C4C9DD7361_CustomAttributesCacheGenerator_ReflectionObject_set_Members_m17AFF4F18816B21290FBAC6AAD3D45D8FA7FFF82(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ReflectionObject_tC343774B9EE64C90DF1DBB77EEC193C4C9DD7361_CustomAttributesCacheGenerator_ReflectionObject_Create_mF57E2BF8F217D0B124CE7B0B5C4A80E173279EBA____memberNames1(CustomAttributesCache* cache)
{
	{
		ParamArrayAttribute_t9DCEB4CDDB8EDDB1124171D4C51FA6069EEA5C5F * tmp = (ParamArrayAttribute_t9DCEB4CDDB8EDDB1124171D4C51FA6069EEA5C5F *)cache->attributes[0];
		ParamArrayAttribute__ctor_mCC72AFF718185BA7B87FD8D9471F1274400C5719(tmp, NULL);
	}
}
static void ReflectionObject_tC343774B9EE64C90DF1DBB77EEC193C4C9DD7361_CustomAttributesCacheGenerator_ReflectionObject_Create_m3E13A4F93F3B71B995104BD0FEBB204830B2545D____memberNames2(CustomAttributesCache* cache)
{
	{
		ParamArrayAttribute_t9DCEB4CDDB8EDDB1124171D4C51FA6069EEA5C5F * tmp = (ParamArrayAttribute_t9DCEB4CDDB8EDDB1124171D4C51FA6069EEA5C5F *)cache->attributes[0];
		ParamArrayAttribute__ctor_mCC72AFF718185BA7B87FD8D9471F1274400C5719(tmp, NULL);
	}
}
static void U3CU3Ec__DisplayClass13_0_tD27FF2F8A49FF27EF149F9E61BB6C981251F9544_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CU3Ec__DisplayClass13_1_t414A08FA62A08F6B9CB2E1510140AB05021714E8_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CU3Ec__DisplayClass13_2_tA55985BA4FFD8AF33D671764AB13EC410498FBCE_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ReflectionUtils_t52FD79CBF528CE948B0F0D12C860FF87C1A5213B_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void ReflectionUtils_t52FD79CBF528CE948B0F0D12C860FF87C1A5213B_CustomAttributesCacheGenerator_ReflectionUtils_IsVirtual_mDAB826DEBE824D7B3DED6ACE9F582D37ADBDECD1(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void ReflectionUtils_t52FD79CBF528CE948B0F0D12C860FF87C1A5213B_CustomAttributesCacheGenerator_ReflectionUtils_GetBaseDefinition_m1495E37E8666896240BC6A5904F061950A1783CB(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void ReflectionUtils_t52FD79CBF528CE948B0F0D12C860FF87C1A5213B_CustomAttributesCacheGenerator_ReflectionUtils_RemoveFlag_mC2CD5A61EDBBB52F4D3CA5DB138800195E89F8E3(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void U3CU3Ec_t87B663D83EAE8982030BB4BF4A7FB9C4BEAC982B_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CU3Ec__DisplayClass42_0_tA195C0D57C247970275535F099F05968450BDC91_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void StringReference_tF51D47B7A913E2F501FC14FB1B432AD2DEC8E27C_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		DefaultMemberAttribute_t8C9B3330DEA69EE364962477FF14FD2CFE30D4B5 * tmp = (DefaultMemberAttribute_t8C9B3330DEA69EE364962477FF14FD2CFE30D4B5 *)cache->attributes[0];
		DefaultMemberAttribute__ctor_mA025B6F5B3A9292696E01108027840C8DFF7F4D7(tmp, il2cpp_codegen_string_new_wrapper("\x49\x74\x65\x6D"), NULL);
	}
}
static void StringReferenceExtensions_tEBA311DB9C048C894D9C1B6D8F01A4E4501F3327_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void StringReferenceExtensions_tEBA311DB9C048C894D9C1B6D8F01A4E4501F3327_CustomAttributesCacheGenerator_StringReferenceExtensions_IndexOf_m0521F7953B075BB955750D6AD8053D53D7ECD6AC(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void StringReferenceExtensions_tEBA311DB9C048C894D9C1B6D8F01A4E4501F3327_CustomAttributesCacheGenerator_StringReferenceExtensions_StartsWith_m6D9020568686A182328C1F71CFC1800A870CFEB3(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void StringReferenceExtensions_tEBA311DB9C048C894D9C1B6D8F01A4E4501F3327_CustomAttributesCacheGenerator_StringReferenceExtensions_EndsWith_mD6F3B9EEC429EAB4F6E05E69EECF814DDF3F764B(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void StringUtils_t3EA5628D6D6700734183C5E2BCFB2E99847AFD49_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void StringUtils_t3EA5628D6D6700734183C5E2BCFB2E99847AFD49_CustomAttributesCacheGenerator_StringUtils_FormatWith_mF78320AE4049E77D6DDEC01680F7D98C44E2442D(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void StringUtils_t3EA5628D6D6700734183C5E2BCFB2E99847AFD49_CustomAttributesCacheGenerator_StringUtils_FormatWith_mEB092C5B96EC84FDC050432B1A40DF8A83BFA2E8(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void StringUtils_t3EA5628D6D6700734183C5E2BCFB2E99847AFD49_CustomAttributesCacheGenerator_StringUtils_FormatWith_m4FCAF6C2F661AFA5DECB79DDD2F4C1C63933AC02(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void StringUtils_t3EA5628D6D6700734183C5E2BCFB2E99847AFD49_CustomAttributesCacheGenerator_StringUtils_FormatWith_mFFF18C22B96311F0FF31E7711F474AB79C1D96EF(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void StringUtils_t3EA5628D6D6700734183C5E2BCFB2E99847AFD49_CustomAttributesCacheGenerator_StringUtils_FormatWith_mF72FDDA3EB515E398CA2D73E829FC7F8AD4F1D2B(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void StringUtils_t3EA5628D6D6700734183C5E2BCFB2E99847AFD49_CustomAttributesCacheGenerator_StringUtils_FormatWith_mF72FDDA3EB515E398CA2D73E829FC7F8AD4F1D2B____args2(CustomAttributesCache* cache)
{
	{
		ParamArrayAttribute_t9DCEB4CDDB8EDDB1124171D4C51FA6069EEA5C5F * tmp = (ParamArrayAttribute_t9DCEB4CDDB8EDDB1124171D4C51FA6069EEA5C5F *)cache->attributes[0];
		ParamArrayAttribute__ctor_mCC72AFF718185BA7B87FD8D9471F1274400C5719(tmp, NULL);
	}
}
static void StringUtils_t3EA5628D6D6700734183C5E2BCFB2E99847AFD49_CustomAttributesCacheGenerator_StringUtils_ForgivingCaseSensitiveFind_m18538F551C05EA50E4D460A66401B34CBA6AE0A1(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void StringUtils_t3EA5628D6D6700734183C5E2BCFB2E99847AFD49_CustomAttributesCacheGenerator_StringUtils_EndsWith_m66FA0222A273889E4E523E10DE65E833F5536F58(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void U3CU3Ec__DisplayClass13_0_1_t307E58AEE0D1014B75CDC74B421C887D0463ABED_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void TypeExtensions_tD99B77AB667AB754CDF59B0A6E9BCB48DFF86B7D_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void TypeExtensions_tD99B77AB667AB754CDF59B0A6E9BCB48DFF86B7D_CustomAttributesCacheGenerator_TypeExtensions_MemberType_m6B0C77E72F48DC1B4204737359510A81F3E464C4(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void TypeExtensions_tD99B77AB667AB754CDF59B0A6E9BCB48DFF86B7D_CustomAttributesCacheGenerator_TypeExtensions_ContainsGenericParameters_m291B9B1787BCE6E8D410828890476C7E868ED823(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void TypeExtensions_tD99B77AB667AB754CDF59B0A6E9BCB48DFF86B7D_CustomAttributesCacheGenerator_TypeExtensions_IsInterface_mC86FEECE611B133413962A5EFE5DC85B92D76381(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void TypeExtensions_tD99B77AB667AB754CDF59B0A6E9BCB48DFF86B7D_CustomAttributesCacheGenerator_TypeExtensions_IsGenericType_mD7A79EF6D299F4D40932AEA3CF8BC3DD8E0C06D2(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void TypeExtensions_tD99B77AB667AB754CDF59B0A6E9BCB48DFF86B7D_CustomAttributesCacheGenerator_TypeExtensions_IsGenericTypeDefinition_m16881F2378C11CF6BFB7B3A127643F4129293AF8(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void TypeExtensions_tD99B77AB667AB754CDF59B0A6E9BCB48DFF86B7D_CustomAttributesCacheGenerator_TypeExtensions_BaseType_mB412285DD088CA76F89534B78D1AE65FFF30F754(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void TypeExtensions_tD99B77AB667AB754CDF59B0A6E9BCB48DFF86B7D_CustomAttributesCacheGenerator_TypeExtensions_IsEnum_m3D3EA9AF4638A68716201C314A1C75C0D0FE6CAA(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void TypeExtensions_tD99B77AB667AB754CDF59B0A6E9BCB48DFF86B7D_CustomAttributesCacheGenerator_TypeExtensions_IsClass_mF1942239F1A4C2B093A05F39F7F8F7D94EE34A1D(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void TypeExtensions_tD99B77AB667AB754CDF59B0A6E9BCB48DFF86B7D_CustomAttributesCacheGenerator_TypeExtensions_IsSealed_m9BF5CB41B9F60342CE4634AA4DE803492059844F(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void TypeExtensions_tD99B77AB667AB754CDF59B0A6E9BCB48DFF86B7D_CustomAttributesCacheGenerator_TypeExtensions_IsAbstract_m3E114BBDB2B178E154B5AD58D0C203A51BE091F0(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void TypeExtensions_tD99B77AB667AB754CDF59B0A6E9BCB48DFF86B7D_CustomAttributesCacheGenerator_TypeExtensions_IsValueType_mCAEDF96A86663613DC39781684AF92CC1531DF2B(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void TypeExtensions_tD99B77AB667AB754CDF59B0A6E9BCB48DFF86B7D_CustomAttributesCacheGenerator_TypeExtensions_AssignableToTypeName_mD522101DAFD0E49737C49A7BE5162B7D23312CC0(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void TypeExtensions_tD99B77AB667AB754CDF59B0A6E9BCB48DFF86B7D_CustomAttributesCacheGenerator_TypeExtensions_AssignableToTypeName_mF0CE3AB37FDF6B916128D0B9735D8ABD7CC649A4(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void TypeExtensions_tD99B77AB667AB754CDF59B0A6E9BCB48DFF86B7D_CustomAttributesCacheGenerator_TypeExtensions_ImplementInterface_m7183F3D7AF62459483F1E37786034552734BE904(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void DefaultContractResolver_t191C29E332665EFC1B0B1F1082BEC4B7DE866297_CustomAttributesCacheGenerator_U3CDefaultMembersSearchFlagsU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void DefaultContractResolver_t191C29E332665EFC1B0B1F1082BEC4B7DE866297_CustomAttributesCacheGenerator_U3CSerializeCompilerGeneratedMembersU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void DefaultContractResolver_t191C29E332665EFC1B0B1F1082BEC4B7DE866297_CustomAttributesCacheGenerator_U3CIgnoreSerializableInterfaceU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void DefaultContractResolver_t191C29E332665EFC1B0B1F1082BEC4B7DE866297_CustomAttributesCacheGenerator_U3CIgnoreSerializableAttributeU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void DefaultContractResolver_t191C29E332665EFC1B0B1F1082BEC4B7DE866297_CustomAttributesCacheGenerator_U3CNamingStrategyU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void DefaultContractResolver_t191C29E332665EFC1B0B1F1082BEC4B7DE866297_CustomAttributesCacheGenerator_DefaultContractResolver_get_DefaultMembersSearchFlags_mA6F171D6825AF07E7AC585C5B89B8B1BA57FAEC3(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void DefaultContractResolver_t191C29E332665EFC1B0B1F1082BEC4B7DE866297_CustomAttributesCacheGenerator_DefaultContractResolver_set_DefaultMembersSearchFlags_m89CA59C8A801BB1C3023BAD591A244271051A248(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void DefaultContractResolver_t191C29E332665EFC1B0B1F1082BEC4B7DE866297_CustomAttributesCacheGenerator_DefaultContractResolver_get_SerializeCompilerGeneratedMembers_mCEC95D8DC060D61621A326CE8DB8B6D1769BB0CC(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void DefaultContractResolver_t191C29E332665EFC1B0B1F1082BEC4B7DE866297_CustomAttributesCacheGenerator_DefaultContractResolver_get_IgnoreSerializableInterface_m10A32A92DCF98D1EDD209D880F17359001FD0125(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void DefaultContractResolver_t191C29E332665EFC1B0B1F1082BEC4B7DE866297_CustomAttributesCacheGenerator_DefaultContractResolver_get_IgnoreSerializableAttribute_m1429E466AFA1212DADA0B9223A04C74BD5DCA8CE(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void DefaultContractResolver_t191C29E332665EFC1B0B1F1082BEC4B7DE866297_CustomAttributesCacheGenerator_DefaultContractResolver_set_IgnoreSerializableAttribute_m54DC842D81DEABFD3198F00DDEBF83E1EF2C6936(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void DefaultContractResolver_t191C29E332665EFC1B0B1F1082BEC4B7DE866297_CustomAttributesCacheGenerator_DefaultContractResolver_get_NamingStrategy_m5C917ABAD194876A14B94BF7FD9D5879527A842A(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void DefaultContractResolver_t191C29E332665EFC1B0B1F1082BEC4B7DE866297_CustomAttributesCacheGenerator_DefaultContractResolver__ctor_mCC6008697D963A3DD69FD7F4D94D3EAA5A9F6036(CustomAttributesCache* cache)
{
	{
		ObsoleteAttribute_t14BAC1669C0409EB9F28D72D664FFA6764ACD671 * tmp = (ObsoleteAttribute_t14BAC1669C0409EB9F28D72D664FFA6764ACD671 *)cache->attributes[0];
		ObsoleteAttribute__ctor_mAC32A5CCD287DA84CDA9F08282C1C8B0DB7B9868(tmp, il2cpp_codegen_string_new_wrapper("\x44\x65\x66\x61\x75\x6C\x74\x43\x6F\x6E\x74\x72\x61\x63\x74\x52\x65\x73\x6F\x6C\x76\x65\x72\x28\x62\x6F\x6F\x6C\x29\x20\x69\x73\x20\x6F\x62\x73\x6F\x6C\x65\x74\x65\x2E\x20\x55\x73\x65\x20\x74\x68\x65\x20\x70\x61\x72\x61\x6D\x65\x74\x65\x72\x6C\x65\x73\x73\x20\x63\x6F\x6E\x73\x74\x72\x75\x63\x74\x6F\x72\x20\x61\x6E\x64\x20\x63\x61\x63\x68\x65\x20\x69\x6E\x73\x74\x61\x6E\x63\x65\x73\x20\x6F\x66\x20\x74\x68\x65\x20\x63\x6F\x6E\x74\x72\x61\x63\x74\x20\x72\x65\x73\x6F\x6C\x76\x65\x72\x20\x77\x69\x74\x68\x69\x6E\x20\x79\x6F\x75\x72\x20\x61\x70\x70\x6C\x69\x63\x61\x74\x69\x6F\x6E\x20\x66\x6F\x72\x20\x6F\x70\x74\x69\x6D\x61\x6C\x20\x70\x65\x72\x66\x6F\x72\x6D\x61\x6E\x63\x65\x2E"), NULL);
	}
}
static void DefaultContractResolver_t191C29E332665EFC1B0B1F1082BEC4B7DE866297_CustomAttributesCacheGenerator_DefaultContractResolver_t191C29E332665EFC1B0B1F1082BEC4B7DE866297____DefaultMembersSearchFlags_PropertyInfo(CustomAttributesCache* cache)
{
	{
		ObsoleteAttribute_t14BAC1669C0409EB9F28D72D664FFA6764ACD671 * tmp = (ObsoleteAttribute_t14BAC1669C0409EB9F28D72D664FFA6764ACD671 *)cache->attributes[0];
		ObsoleteAttribute__ctor_mAC32A5CCD287DA84CDA9F08282C1C8B0DB7B9868(tmp, il2cpp_codegen_string_new_wrapper("\x44\x65\x66\x61\x75\x6C\x74\x4D\x65\x6D\x62\x65\x72\x73\x53\x65\x61\x72\x63\x68\x46\x6C\x61\x67\x73\x20\x69\x73\x20\x6F\x62\x73\x6F\x6C\x65\x74\x65\x2E\x20\x54\x6F\x20\x6D\x6F\x64\x69\x66\x79\x20\x74\x68\x65\x20\x6D\x65\x6D\x62\x65\x72\x73\x20\x73\x65\x72\x69\x61\x6C\x69\x7A\x65\x64\x20\x69\x6E\x68\x65\x72\x69\x74\x20\x66\x72\x6F\x6D\x20\x44\x65\x66\x61\x75\x6C\x74\x43\x6F\x6E\x74\x72\x61\x63\x74\x52\x65\x73\x6F\x6C\x76\x65\x72\x20\x61\x6E\x64\x20\x6F\x76\x65\x72\x72\x69\x64\x65\x20\x74\x68\x65\x20\x47\x65\x74\x53\x65\x72\x69\x61\x6C\x69\x7A\x61\x62\x6C\x65\x4D\x65\x6D\x62\x65\x72\x73\x20\x6D\x65\x74\x68\x6F\x64\x20\x69\x6E\x73\x74\x65\x61\x64\x2E"), NULL);
	}
}
static void U3CGetEnumeratorU3Ed__2_t451559940AEDCCEC6E550A43833F5747F6735C0B_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CGetEnumeratorU3Ed__2_t451559940AEDCCEC6E550A43833F5747F6735C0B_CustomAttributesCacheGenerator_U3CGetEnumeratorU3Ed__2__ctor_m282D27A36E401D60D0CEB656F332C08DF2C68A9D(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CGetEnumeratorU3Ed__2_t451559940AEDCCEC6E550A43833F5747F6735C0B_CustomAttributesCacheGenerator_U3CGetEnumeratorU3Ed__2_System_IDisposable_Dispose_m4A5FE06D9B3E31D62C920B23024F7FA523EBF5EE(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CGetEnumeratorU3Ed__2_t451559940AEDCCEC6E550A43833F5747F6735C0B_CustomAttributesCacheGenerator_U3CGetEnumeratorU3Ed__2_System_Collections_Generic_IEnumeratorU3CSystem_Collections_Generic_KeyValuePairU3CSystem_ObjectU2CSystem_ObjectU3EU3E_get_Current_mDCB15E4324723B4C6E695C3B96A5F79AEB296521(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CGetEnumeratorU3Ed__2_t451559940AEDCCEC6E550A43833F5747F6735C0B_CustomAttributesCacheGenerator_U3CGetEnumeratorU3Ed__2_System_Collections_IEnumerator_Reset_m9E947FD2E6912DE2D9BEEBC105AB32145E30F3EC(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CGetEnumeratorU3Ed__2_t451559940AEDCCEC6E550A43833F5747F6735C0B_CustomAttributesCacheGenerator_U3CGetEnumeratorU3Ed__2_System_Collections_IEnumerator_get_Current_mE36CD065E2DAEA5258D089A15BB291A19D910ED3(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CU3Ec_t30F8D20CDE2939ABE1BA132502CFBFDB37A519CA_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CU3Ec__DisplayClass38_0_tBC9EDBB30C9F971EB64E970CF8287B67D8EE96B5_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CU3Ec__DisplayClass38_1_t3AA8C8586136BB6D815B1DF622E7E918FA7B4E33_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CU3Ec__DisplayClass38_2_tCCA3CA4FB84DADEBB49F8B4DAEBC7A7B9DE74F14_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CU3Ec__DisplayClass52_0_t47D44CD99A914B975A5807FF43AA93BB479B9C8C_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CU3Ec__DisplayClass68_0_t766708EC19046F0BD02D671A2671CD88B70FEA3E_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CU3Ec__DisplayClass69_0_tFEECEE33CB16927EBB0FFEF0186F58FEFEA79F1E_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ErrorContext_tFCEA6129DB7CC94DFEFC6A3EE16A4420F57B417E_CustomAttributesCacheGenerator_U3CTracedU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ErrorContext_tFCEA6129DB7CC94DFEFC6A3EE16A4420F57B417E_CustomAttributesCacheGenerator_U3CErrorU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ErrorContext_tFCEA6129DB7CC94DFEFC6A3EE16A4420F57B417E_CustomAttributesCacheGenerator_U3COriginalObjectU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ErrorContext_tFCEA6129DB7CC94DFEFC6A3EE16A4420F57B417E_CustomAttributesCacheGenerator_U3CMemberU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ErrorContext_tFCEA6129DB7CC94DFEFC6A3EE16A4420F57B417E_CustomAttributesCacheGenerator_U3CPathU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ErrorContext_tFCEA6129DB7CC94DFEFC6A3EE16A4420F57B417E_CustomAttributesCacheGenerator_U3CHandledU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ErrorContext_tFCEA6129DB7CC94DFEFC6A3EE16A4420F57B417E_CustomAttributesCacheGenerator_ErrorContext_get_Traced_m2600308CFFECA1B408B14E621987926BBB1799C5(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ErrorContext_tFCEA6129DB7CC94DFEFC6A3EE16A4420F57B417E_CustomAttributesCacheGenerator_ErrorContext_set_Traced_m8BA7FD9673E83668AB685995DCF399BFFF77A21E(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ErrorContext_tFCEA6129DB7CC94DFEFC6A3EE16A4420F57B417E_CustomAttributesCacheGenerator_ErrorContext_get_Error_m4BA9AE89A1463A8187BCAA2081B10A7EE412B4B8(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ErrorContext_tFCEA6129DB7CC94DFEFC6A3EE16A4420F57B417E_CustomAttributesCacheGenerator_ErrorContext_set_Error_m3C47A4456A7D207FCB0AA8DBDFC0EACE349ACB27(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ErrorContext_tFCEA6129DB7CC94DFEFC6A3EE16A4420F57B417E_CustomAttributesCacheGenerator_ErrorContext_set_OriginalObject_m2D5A6F54F6DBB717BFEFFE41ABFD5F8FF944C8BA(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ErrorContext_tFCEA6129DB7CC94DFEFC6A3EE16A4420F57B417E_CustomAttributesCacheGenerator_ErrorContext_set_Member_mA60560017299B78E3269E2E30E178D46CD52DE5E(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ErrorContext_tFCEA6129DB7CC94DFEFC6A3EE16A4420F57B417E_CustomAttributesCacheGenerator_ErrorContext_set_Path_mAB5E68DCF6CFD684635470CEE002AFBA2ECCA6DF(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ErrorContext_tFCEA6129DB7CC94DFEFC6A3EE16A4420F57B417E_CustomAttributesCacheGenerator_ErrorContext_get_Handled_m00CB858B5DF909C699425AA6A1EB383ABA527158(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ErrorEventArgs_tA2C1B2B76DF50EC25B52AB0FDD51BB23A2855AB2_CustomAttributesCacheGenerator_U3CCurrentObjectU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ErrorEventArgs_tA2C1B2B76DF50EC25B52AB0FDD51BB23A2855AB2_CustomAttributesCacheGenerator_U3CErrorContextU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ErrorEventArgs_tA2C1B2B76DF50EC25B52AB0FDD51BB23A2855AB2_CustomAttributesCacheGenerator_ErrorEventArgs_set_CurrentObject_mE2464018DBF50495E7AC91C004C29BB678F408EC(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ErrorEventArgs_tA2C1B2B76DF50EC25B52AB0FDD51BB23A2855AB2_CustomAttributesCacheGenerator_ErrorEventArgs_set_ErrorContext_mD68107F654E6143A90BA4187E54E358ADA9F3EEF(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void JsonArrayContract_tE4BEF4DAEFA2D8A388C986A0E81255CC2B7FBF83_CustomAttributesCacheGenerator_U3CCollectionItemTypeU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void JsonArrayContract_tE4BEF4DAEFA2D8A388C986A0E81255CC2B7FBF83_CustomAttributesCacheGenerator_U3CIsMultidimensionalArrayU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void JsonArrayContract_tE4BEF4DAEFA2D8A388C986A0E81255CC2B7FBF83_CustomAttributesCacheGenerator_U3CIsArrayU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void JsonArrayContract_tE4BEF4DAEFA2D8A388C986A0E81255CC2B7FBF83_CustomAttributesCacheGenerator_U3CShouldCreateWrapperU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void JsonArrayContract_tE4BEF4DAEFA2D8A388C986A0E81255CC2B7FBF83_CustomAttributesCacheGenerator_U3CCanDeserializeU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void JsonArrayContract_tE4BEF4DAEFA2D8A388C986A0E81255CC2B7FBF83_CustomAttributesCacheGenerator_U3CHasParameterizedCreatorU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void JsonArrayContract_tE4BEF4DAEFA2D8A388C986A0E81255CC2B7FBF83_CustomAttributesCacheGenerator_JsonArrayContract_get_CollectionItemType_m3CECF4280B1768FDEF6A7111741C93DE7F7DDF22(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void JsonArrayContract_tE4BEF4DAEFA2D8A388C986A0E81255CC2B7FBF83_CustomAttributesCacheGenerator_JsonArrayContract_set_CollectionItemType_m459F1B1BB37AA94FC3386F62D51E850E3D3B9654(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void JsonArrayContract_tE4BEF4DAEFA2D8A388C986A0E81255CC2B7FBF83_CustomAttributesCacheGenerator_JsonArrayContract_get_IsMultidimensionalArray_m882BEE069C4D30998786CDEF57704C966CA33E62(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void JsonArrayContract_tE4BEF4DAEFA2D8A388C986A0E81255CC2B7FBF83_CustomAttributesCacheGenerator_JsonArrayContract_set_IsMultidimensionalArray_m85FD29E6E2FA63221F8CC1150E97D5D53016C251(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void JsonArrayContract_tE4BEF4DAEFA2D8A388C986A0E81255CC2B7FBF83_CustomAttributesCacheGenerator_JsonArrayContract_get_IsArray_m72D6C22445B07CFE617D61656301BEAB4C39CAD8(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void JsonArrayContract_tE4BEF4DAEFA2D8A388C986A0E81255CC2B7FBF83_CustomAttributesCacheGenerator_JsonArrayContract_set_IsArray_m7F43AFECAC00B630DBB56A94DA548141448AF781(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void JsonArrayContract_tE4BEF4DAEFA2D8A388C986A0E81255CC2B7FBF83_CustomAttributesCacheGenerator_JsonArrayContract_get_ShouldCreateWrapper_mE8AEA40AFF96BA8EA72B04C1FC377E98A5F21086(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void JsonArrayContract_tE4BEF4DAEFA2D8A388C986A0E81255CC2B7FBF83_CustomAttributesCacheGenerator_JsonArrayContract_set_ShouldCreateWrapper_m437A0E5C1DE254AFB886EB246A854E775EA12C72(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void JsonArrayContract_tE4BEF4DAEFA2D8A388C986A0E81255CC2B7FBF83_CustomAttributesCacheGenerator_JsonArrayContract_get_CanDeserialize_mBCB4583F10A61CBDC6C60CA4D88D07366CEE9583(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void JsonArrayContract_tE4BEF4DAEFA2D8A388C986A0E81255CC2B7FBF83_CustomAttributesCacheGenerator_JsonArrayContract_set_CanDeserialize_mCA795402C7F20B1704011379FF99BFDBEA09946E(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void JsonArrayContract_tE4BEF4DAEFA2D8A388C986A0E81255CC2B7FBF83_CustomAttributesCacheGenerator_JsonArrayContract_get_HasParameterizedCreator_m4BC9037E6219B65657C06880A51DE9B22B2F38B7(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void JsonArrayContract_tE4BEF4DAEFA2D8A388C986A0E81255CC2B7FBF83_CustomAttributesCacheGenerator_JsonArrayContract_set_HasParameterizedCreator_m85CC98F0D51C11A1570956CC7319BE4C21AAA962(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void JsonContainerContract_tE83C0C4EDD22FEDE907E1D5F027CAEBDF1DFADAC_CustomAttributesCacheGenerator_U3CItemConverterU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void JsonContainerContract_tE83C0C4EDD22FEDE907E1D5F027CAEBDF1DFADAC_CustomAttributesCacheGenerator_U3CItemIsReferenceU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void JsonContainerContract_tE83C0C4EDD22FEDE907E1D5F027CAEBDF1DFADAC_CustomAttributesCacheGenerator_U3CItemReferenceLoopHandlingU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void JsonContainerContract_tE83C0C4EDD22FEDE907E1D5F027CAEBDF1DFADAC_CustomAttributesCacheGenerator_U3CItemTypeNameHandlingU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void JsonContainerContract_tE83C0C4EDD22FEDE907E1D5F027CAEBDF1DFADAC_CustomAttributesCacheGenerator_JsonContainerContract_get_ItemConverter_mCC1FF5CD91BE1433DE49D71A5000D148A247D07B(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void JsonContainerContract_tE83C0C4EDD22FEDE907E1D5F027CAEBDF1DFADAC_CustomAttributesCacheGenerator_JsonContainerContract_set_ItemConverter_m22A233E3F1FEFEE00C494880802E72B9CE364301(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void JsonContainerContract_tE83C0C4EDD22FEDE907E1D5F027CAEBDF1DFADAC_CustomAttributesCacheGenerator_JsonContainerContract_get_ItemIsReference_mFF08D7B8117AD227F7B655F89690250790FCED61(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void JsonContainerContract_tE83C0C4EDD22FEDE907E1D5F027CAEBDF1DFADAC_CustomAttributesCacheGenerator_JsonContainerContract_set_ItemIsReference_m976F9ED94F787C37541FFE9CFCD2687AB7152CBF(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void JsonContainerContract_tE83C0C4EDD22FEDE907E1D5F027CAEBDF1DFADAC_CustomAttributesCacheGenerator_JsonContainerContract_get_ItemReferenceLoopHandling_m4497940C72F5F38E574A4D180D8D2CD919A812BC(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void JsonContainerContract_tE83C0C4EDD22FEDE907E1D5F027CAEBDF1DFADAC_CustomAttributesCacheGenerator_JsonContainerContract_set_ItemReferenceLoopHandling_m90E562BE972F1EF5478B7ED2703E27F5BE4ED27D(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void JsonContainerContract_tE83C0C4EDD22FEDE907E1D5F027CAEBDF1DFADAC_CustomAttributesCacheGenerator_JsonContainerContract_get_ItemTypeNameHandling_m805E6E8EB10B640579EF897F4A74E566D80F4498(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void JsonContainerContract_tE83C0C4EDD22FEDE907E1D5F027CAEBDF1DFADAC_CustomAttributesCacheGenerator_JsonContainerContract_set_ItemTypeNameHandling_mCA66B3AA082975A16A7DA94DB8F18722F5369447(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void JsonContract_tF3638831C408783F93E44D96C7C10A699A0F7F0D_CustomAttributesCacheGenerator_U3CUnderlyingTypeU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void JsonContract_tF3638831C408783F93E44D96C7C10A699A0F7F0D_CustomAttributesCacheGenerator_U3CIsReferenceU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void JsonContract_tF3638831C408783F93E44D96C7C10A699A0F7F0D_CustomAttributesCacheGenerator_U3CConverterU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void JsonContract_tF3638831C408783F93E44D96C7C10A699A0F7F0D_CustomAttributesCacheGenerator_U3CInternalConverterU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void JsonContract_tF3638831C408783F93E44D96C7C10A699A0F7F0D_CustomAttributesCacheGenerator_U3CDefaultCreatorU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void JsonContract_tF3638831C408783F93E44D96C7C10A699A0F7F0D_CustomAttributesCacheGenerator_U3CDefaultCreatorNonPublicU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void JsonContract_tF3638831C408783F93E44D96C7C10A699A0F7F0D_CustomAttributesCacheGenerator_JsonContract_get_UnderlyingType_m274B4718678409B2511BBE941FF2D647CA533DC3(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void JsonContract_tF3638831C408783F93E44D96C7C10A699A0F7F0D_CustomAttributesCacheGenerator_JsonContract_set_UnderlyingType_m82F3DEB8DC8339C6CB32187651D87CE330DE13AE(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void JsonContract_tF3638831C408783F93E44D96C7C10A699A0F7F0D_CustomAttributesCacheGenerator_JsonContract_get_IsReference_m85AD9382BF6556FBB63A383D1ECA6BC6C5C369A6(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void JsonContract_tF3638831C408783F93E44D96C7C10A699A0F7F0D_CustomAttributesCacheGenerator_JsonContract_set_IsReference_m21803C0B2D772F7C3EED60332E2E2CA69D6D54F3(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void JsonContract_tF3638831C408783F93E44D96C7C10A699A0F7F0D_CustomAttributesCacheGenerator_JsonContract_get_Converter_m92C6064189D577AA5D08771BE755B3BFBCB297A9(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void JsonContract_tF3638831C408783F93E44D96C7C10A699A0F7F0D_CustomAttributesCacheGenerator_JsonContract_set_Converter_mA7452FEC775D9674F8163B299C151249DC7E544F(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void JsonContract_tF3638831C408783F93E44D96C7C10A699A0F7F0D_CustomAttributesCacheGenerator_JsonContract_get_InternalConverter_mB36C4060D4251DE1F51662B767D2779C40C65BCA(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void JsonContract_tF3638831C408783F93E44D96C7C10A699A0F7F0D_CustomAttributesCacheGenerator_JsonContract_set_InternalConverter_mAB2F562DCD689CC7E50B4BE84AEE73E2995D782F(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void JsonContract_tF3638831C408783F93E44D96C7C10A699A0F7F0D_CustomAttributesCacheGenerator_JsonContract_get_DefaultCreator_m8271898AFA4F8DBDC5A018F79FD4EC5450839B81(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void JsonContract_tF3638831C408783F93E44D96C7C10A699A0F7F0D_CustomAttributesCacheGenerator_JsonContract_set_DefaultCreator_m0D4E0BD379C8342A4BD6C6D73F19BE330526F825(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void JsonContract_tF3638831C408783F93E44D96C7C10A699A0F7F0D_CustomAttributesCacheGenerator_JsonContract_get_DefaultCreatorNonPublic_mFEDABA69CB6CA3FDE1FB163D1192C74307F2A15F(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void JsonContract_tF3638831C408783F93E44D96C7C10A699A0F7F0D_CustomAttributesCacheGenerator_JsonContract_set_DefaultCreatorNonPublic_mDCCE7A1E9DDED83BFFD22F33BC6E660F70CB4FEF(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CU3Ec__DisplayClass73_0_tDED699F39B829613F6126BA5E1C887311346B7EE_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CU3Ec__DisplayClass74_0_tEB782E56C6643FDDCF2D7B0E5A2DF0A1AED3AEB8_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void JsonDictionaryContract_t66334CCEFE57D3900FF8DB5AC954360DEF1E2F8C_CustomAttributesCacheGenerator_U3CDictionaryKeyResolverU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void JsonDictionaryContract_t66334CCEFE57D3900FF8DB5AC954360DEF1E2F8C_CustomAttributesCacheGenerator_U3CDictionaryKeyTypeU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void JsonDictionaryContract_t66334CCEFE57D3900FF8DB5AC954360DEF1E2F8C_CustomAttributesCacheGenerator_U3CDictionaryValueTypeU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void JsonDictionaryContract_t66334CCEFE57D3900FF8DB5AC954360DEF1E2F8C_CustomAttributesCacheGenerator_U3CKeyContractU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void JsonDictionaryContract_t66334CCEFE57D3900FF8DB5AC954360DEF1E2F8C_CustomAttributesCacheGenerator_U3CShouldCreateWrapperU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void JsonDictionaryContract_t66334CCEFE57D3900FF8DB5AC954360DEF1E2F8C_CustomAttributesCacheGenerator_U3CHasParameterizedCreatorU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void JsonDictionaryContract_t66334CCEFE57D3900FF8DB5AC954360DEF1E2F8C_CustomAttributesCacheGenerator_JsonDictionaryContract_get_DictionaryKeyResolver_m9FEC62269B460CBE840125B2C29F98F35FD18568(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void JsonDictionaryContract_t66334CCEFE57D3900FF8DB5AC954360DEF1E2F8C_CustomAttributesCacheGenerator_JsonDictionaryContract_set_DictionaryKeyResolver_m7DBA6B179284AF2B20F35EFD0A53D1C1A7A1338E(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void JsonDictionaryContract_t66334CCEFE57D3900FF8DB5AC954360DEF1E2F8C_CustomAttributesCacheGenerator_JsonDictionaryContract_get_DictionaryKeyType_m955BFAF792651E88B651BD2C619097726DCB76FD(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void JsonDictionaryContract_t66334CCEFE57D3900FF8DB5AC954360DEF1E2F8C_CustomAttributesCacheGenerator_JsonDictionaryContract_set_DictionaryKeyType_m6B1EDD598D5F7E159443731827F01450C44B11CB(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void JsonDictionaryContract_t66334CCEFE57D3900FF8DB5AC954360DEF1E2F8C_CustomAttributesCacheGenerator_JsonDictionaryContract_get_DictionaryValueType_m45B9DDEF68FCAAD1D2FD90391817AF79A2915922(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void JsonDictionaryContract_t66334CCEFE57D3900FF8DB5AC954360DEF1E2F8C_CustomAttributesCacheGenerator_JsonDictionaryContract_set_DictionaryValueType_m05D9BED20D4CA80764E423E027F66DB3A0818E18(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void JsonDictionaryContract_t66334CCEFE57D3900FF8DB5AC954360DEF1E2F8C_CustomAttributesCacheGenerator_JsonDictionaryContract_get_KeyContract_mC7BAD25BBB5DF24FA9FA1B704FB9153A1526EB8B(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void JsonDictionaryContract_t66334CCEFE57D3900FF8DB5AC954360DEF1E2F8C_CustomAttributesCacheGenerator_JsonDictionaryContract_set_KeyContract_m522C30EF20C09B50E617E24698E0BE1D84033EFC(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void JsonDictionaryContract_t66334CCEFE57D3900FF8DB5AC954360DEF1E2F8C_CustomAttributesCacheGenerator_JsonDictionaryContract_get_ShouldCreateWrapper_mD43CBCD1A26F23DFF9EF7F4069AA4BC99CE03BCC(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void JsonDictionaryContract_t66334CCEFE57D3900FF8DB5AC954360DEF1E2F8C_CustomAttributesCacheGenerator_JsonDictionaryContract_set_ShouldCreateWrapper_m81F5B13DB3FE9CC060B07F1943975289E930DBD0(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void JsonDictionaryContract_t66334CCEFE57D3900FF8DB5AC954360DEF1E2F8C_CustomAttributesCacheGenerator_JsonDictionaryContract_get_HasParameterizedCreator_m2AC31619D694866F47219F7228D781FDB7559361(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void JsonDictionaryContract_t66334CCEFE57D3900FF8DB5AC954360DEF1E2F8C_CustomAttributesCacheGenerator_JsonDictionaryContract_set_HasParameterizedCreator_m5C3555EBD3982F9FC072BDAF78014CC2C49F777D(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void JsonISerializableContract_t84536AF410FF3F89CC83FEFAB7530B8F51D347BF_CustomAttributesCacheGenerator_U3CISerializableCreatorU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void JsonISerializableContract_t84536AF410FF3F89CC83FEFAB7530B8F51D347BF_CustomAttributesCacheGenerator_JsonISerializableContract_set_ISerializableCreator_m5295CF43688CAD573D840026F52A80256EF73A24(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void JsonObjectContract_tB36EC347E258A3A2B5A8560E8DC6D2A4BEF6DEB8_CustomAttributesCacheGenerator_U3CMemberSerializationU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void JsonObjectContract_tB36EC347E258A3A2B5A8560E8DC6D2A4BEF6DEB8_CustomAttributesCacheGenerator_U3CItemRequiredU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void JsonObjectContract_tB36EC347E258A3A2B5A8560E8DC6D2A4BEF6DEB8_CustomAttributesCacheGenerator_U3CPropertiesU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void JsonObjectContract_tB36EC347E258A3A2B5A8560E8DC6D2A4BEF6DEB8_CustomAttributesCacheGenerator_U3CExtensionDataSetterU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void JsonObjectContract_tB36EC347E258A3A2B5A8560E8DC6D2A4BEF6DEB8_CustomAttributesCacheGenerator_U3CExtensionDataGetterU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void JsonObjectContract_tB36EC347E258A3A2B5A8560E8DC6D2A4BEF6DEB8_CustomAttributesCacheGenerator_JsonObjectContract_get_MemberSerialization_m927C496830ECEE969FEFCBD9015D2975B64EB7EA(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void JsonObjectContract_tB36EC347E258A3A2B5A8560E8DC6D2A4BEF6DEB8_CustomAttributesCacheGenerator_JsonObjectContract_set_MemberSerialization_m80C86B38A9A42F0A2D096B74CFC52E8EE250175E(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void JsonObjectContract_tB36EC347E258A3A2B5A8560E8DC6D2A4BEF6DEB8_CustomAttributesCacheGenerator_JsonObjectContract_get_ItemRequired_m75C6C4175B7C1473543857FFB985D37C39AD8C82(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void JsonObjectContract_tB36EC347E258A3A2B5A8560E8DC6D2A4BEF6DEB8_CustomAttributesCacheGenerator_JsonObjectContract_set_ItemRequired_m322E068F6060AB63F5C6B2E61DC986925C674E07(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void JsonObjectContract_tB36EC347E258A3A2B5A8560E8DC6D2A4BEF6DEB8_CustomAttributesCacheGenerator_JsonObjectContract_get_Properties_mD4B212D9A137993DE37F53402D769D0D905E3110(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void JsonObjectContract_tB36EC347E258A3A2B5A8560E8DC6D2A4BEF6DEB8_CustomAttributesCacheGenerator_JsonObjectContract_set_Properties_mDC67F335196A048B9DF12F878BAAE0B20328314B(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void JsonObjectContract_tB36EC347E258A3A2B5A8560E8DC6D2A4BEF6DEB8_CustomAttributesCacheGenerator_JsonObjectContract_get_ExtensionDataSetter_m2DFB59696B0D14849D0F4E443DD82AA5A76B243C(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void JsonObjectContract_tB36EC347E258A3A2B5A8560E8DC6D2A4BEF6DEB8_CustomAttributesCacheGenerator_JsonObjectContract_set_ExtensionDataSetter_m39A79B22B522DB922D27C30377BD082401A787B6(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void JsonObjectContract_tB36EC347E258A3A2B5A8560E8DC6D2A4BEF6DEB8_CustomAttributesCacheGenerator_JsonObjectContract_get_ExtensionDataGetter_m80664EEE79810B192D96E02FCD344B03EE7ACF35(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void JsonObjectContract_tB36EC347E258A3A2B5A8560E8DC6D2A4BEF6DEB8_CustomAttributesCacheGenerator_JsonObjectContract_set_ExtensionDataGetter_m52A1E838E07D96D828C5BB724E77BE2F17313980(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void JsonObjectContract_tB36EC347E258A3A2B5A8560E8DC6D2A4BEF6DEB8_CustomAttributesCacheGenerator_JsonObjectContract_tB36EC347E258A3A2B5A8560E8DC6D2A4BEF6DEB8____OverrideConstructor_PropertyInfo(CustomAttributesCache* cache)
{
	{
		ObsoleteAttribute_t14BAC1669C0409EB9F28D72D664FFA6764ACD671 * tmp = (ObsoleteAttribute_t14BAC1669C0409EB9F28D72D664FFA6764ACD671 *)cache->attributes[0];
		ObsoleteAttribute__ctor_mAC32A5CCD287DA84CDA9F08282C1C8B0DB7B9868(tmp, il2cpp_codegen_string_new_wrapper("\x4F\x76\x65\x72\x72\x69\x64\x65\x43\x6F\x6E\x73\x74\x72\x75\x63\x74\x6F\x72\x20\x69\x73\x20\x6F\x62\x73\x6F\x6C\x65\x74\x65\x2E\x20\x55\x73\x65\x20\x4F\x76\x65\x72\x72\x69\x64\x65\x43\x72\x65\x61\x74\x6F\x72\x20\x69\x6E\x73\x74\x65\x61\x64\x2E"), NULL);
	}
}
static void JsonObjectContract_tB36EC347E258A3A2B5A8560E8DC6D2A4BEF6DEB8_CustomAttributesCacheGenerator_JsonObjectContract_tB36EC347E258A3A2B5A8560E8DC6D2A4BEF6DEB8____ParametrizedConstructor_PropertyInfo(CustomAttributesCache* cache)
{
	{
		ObsoleteAttribute_t14BAC1669C0409EB9F28D72D664FFA6764ACD671 * tmp = (ObsoleteAttribute_t14BAC1669C0409EB9F28D72D664FFA6764ACD671 *)cache->attributes[0];
		ObsoleteAttribute__ctor_mAC32A5CCD287DA84CDA9F08282C1C8B0DB7B9868(tmp, il2cpp_codegen_string_new_wrapper("\x50\x61\x72\x61\x6D\x65\x74\x72\x69\x7A\x65\x64\x43\x6F\x6E\x73\x74\x72\x75\x63\x74\x6F\x72\x20\x69\x73\x20\x6F\x62\x73\x6F\x6C\x65\x74\x65\x2E\x20\x55\x73\x65\x20\x4F\x76\x65\x72\x72\x69\x64\x65\x43\x72\x65\x61\x74\x6F\x72\x20\x69\x6E\x73\x74\x65\x61\x64\x2E"), NULL);
	}
}
static void JsonPrimitiveContract_tA595E8BA6018728333455EC3265935664D8EDC22_CustomAttributesCacheGenerator_U3CTypeCodeU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void JsonPrimitiveContract_tA595E8BA6018728333455EC3265935664D8EDC22_CustomAttributesCacheGenerator_JsonPrimitiveContract_get_TypeCode_mEED6CF3DA675CB2F244C3E02209F37FC8887C544(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void JsonPrimitiveContract_tA595E8BA6018728333455EC3265935664D8EDC22_CustomAttributesCacheGenerator_JsonPrimitiveContract_set_TypeCode_mD01C4FD88E81143FD4FE9446D08E10CBED5D16CB(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void JsonProperty_t41E840165F50D3CE4A2E9A48DD6064FCD59AAA0D_CustomAttributesCacheGenerator_U3CPropertyContractU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void JsonProperty_t41E840165F50D3CE4A2E9A48DD6064FCD59AAA0D_CustomAttributesCacheGenerator_U3CDeclaringTypeU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void JsonProperty_t41E840165F50D3CE4A2E9A48DD6064FCD59AAA0D_CustomAttributesCacheGenerator_U3COrderU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void JsonProperty_t41E840165F50D3CE4A2E9A48DD6064FCD59AAA0D_CustomAttributesCacheGenerator_U3CUnderlyingNameU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void JsonProperty_t41E840165F50D3CE4A2E9A48DD6064FCD59AAA0D_CustomAttributesCacheGenerator_U3CValueProviderU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void JsonProperty_t41E840165F50D3CE4A2E9A48DD6064FCD59AAA0D_CustomAttributesCacheGenerator_U3CAttributeProviderU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void JsonProperty_t41E840165F50D3CE4A2E9A48DD6064FCD59AAA0D_CustomAttributesCacheGenerator_U3CConverterU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void JsonProperty_t41E840165F50D3CE4A2E9A48DD6064FCD59AAA0D_CustomAttributesCacheGenerator_U3CMemberConverterU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void JsonProperty_t41E840165F50D3CE4A2E9A48DD6064FCD59AAA0D_CustomAttributesCacheGenerator_U3CIgnoredU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void JsonProperty_t41E840165F50D3CE4A2E9A48DD6064FCD59AAA0D_CustomAttributesCacheGenerator_U3CReadableU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void JsonProperty_t41E840165F50D3CE4A2E9A48DD6064FCD59AAA0D_CustomAttributesCacheGenerator_U3CWritableU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void JsonProperty_t41E840165F50D3CE4A2E9A48DD6064FCD59AAA0D_CustomAttributesCacheGenerator_U3CHasMemberAttributeU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void JsonProperty_t41E840165F50D3CE4A2E9A48DD6064FCD59AAA0D_CustomAttributesCacheGenerator_U3CIsReferenceU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void JsonProperty_t41E840165F50D3CE4A2E9A48DD6064FCD59AAA0D_CustomAttributesCacheGenerator_U3CNullValueHandlingU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void JsonProperty_t41E840165F50D3CE4A2E9A48DD6064FCD59AAA0D_CustomAttributesCacheGenerator_U3CDefaultValueHandlingU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void JsonProperty_t41E840165F50D3CE4A2E9A48DD6064FCD59AAA0D_CustomAttributesCacheGenerator_U3CReferenceLoopHandlingU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void JsonProperty_t41E840165F50D3CE4A2E9A48DD6064FCD59AAA0D_CustomAttributesCacheGenerator_U3CObjectCreationHandlingU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void JsonProperty_t41E840165F50D3CE4A2E9A48DD6064FCD59AAA0D_CustomAttributesCacheGenerator_U3CTypeNameHandlingU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void JsonProperty_t41E840165F50D3CE4A2E9A48DD6064FCD59AAA0D_CustomAttributesCacheGenerator_U3CShouldSerializeU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void JsonProperty_t41E840165F50D3CE4A2E9A48DD6064FCD59AAA0D_CustomAttributesCacheGenerator_U3CShouldDeserializeU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void JsonProperty_t41E840165F50D3CE4A2E9A48DD6064FCD59AAA0D_CustomAttributesCacheGenerator_U3CGetIsSpecifiedU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void JsonProperty_t41E840165F50D3CE4A2E9A48DD6064FCD59AAA0D_CustomAttributesCacheGenerator_U3CSetIsSpecifiedU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void JsonProperty_t41E840165F50D3CE4A2E9A48DD6064FCD59AAA0D_CustomAttributesCacheGenerator_U3CItemConverterU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void JsonProperty_t41E840165F50D3CE4A2E9A48DD6064FCD59AAA0D_CustomAttributesCacheGenerator_U3CItemIsReferenceU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void JsonProperty_t41E840165F50D3CE4A2E9A48DD6064FCD59AAA0D_CustomAttributesCacheGenerator_U3CItemTypeNameHandlingU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void JsonProperty_t41E840165F50D3CE4A2E9A48DD6064FCD59AAA0D_CustomAttributesCacheGenerator_U3CItemReferenceLoopHandlingU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void JsonProperty_t41E840165F50D3CE4A2E9A48DD6064FCD59AAA0D_CustomAttributesCacheGenerator_JsonProperty_get_PropertyContract_m1A07AE508ED64BF7DAFD6B768CAB11E8F07AC218(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void JsonProperty_t41E840165F50D3CE4A2E9A48DD6064FCD59AAA0D_CustomAttributesCacheGenerator_JsonProperty_set_PropertyContract_mA51FB41ACB2877BDA0005FFB84D6724AD6269B3D(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void JsonProperty_t41E840165F50D3CE4A2E9A48DD6064FCD59AAA0D_CustomAttributesCacheGenerator_JsonProperty_get_DeclaringType_m0A64AF3ECCED18859E78E63E3A264AAB74586322(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void JsonProperty_t41E840165F50D3CE4A2E9A48DD6064FCD59AAA0D_CustomAttributesCacheGenerator_JsonProperty_set_DeclaringType_m4FA21E3ADA7905038A4B8410922A4D678DB627BD(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void JsonProperty_t41E840165F50D3CE4A2E9A48DD6064FCD59AAA0D_CustomAttributesCacheGenerator_JsonProperty_get_Order_mEF3E149DB2B59B8CA34317D3C09DFF703F31EF81(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void JsonProperty_t41E840165F50D3CE4A2E9A48DD6064FCD59AAA0D_CustomAttributesCacheGenerator_JsonProperty_set_Order_m360E9E66EB70C639663D3A29A37CD4FFB9217F56(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void JsonProperty_t41E840165F50D3CE4A2E9A48DD6064FCD59AAA0D_CustomAttributesCacheGenerator_JsonProperty_get_UnderlyingName_m8204307F227D4B3D9701133A47172F819B08219C(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void JsonProperty_t41E840165F50D3CE4A2E9A48DD6064FCD59AAA0D_CustomAttributesCacheGenerator_JsonProperty_set_UnderlyingName_mCD3B44976B4DFFD01B148B7D7A88AE92A71F6C2F(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void JsonProperty_t41E840165F50D3CE4A2E9A48DD6064FCD59AAA0D_CustomAttributesCacheGenerator_JsonProperty_get_ValueProvider_mB9CD0B24D58FCF1EE82DA3A3935973CD560C971B(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void JsonProperty_t41E840165F50D3CE4A2E9A48DD6064FCD59AAA0D_CustomAttributesCacheGenerator_JsonProperty_set_ValueProvider_m95563EC88EE32866DACCC6C2AE8F0FE656A5D847(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void JsonProperty_t41E840165F50D3CE4A2E9A48DD6064FCD59AAA0D_CustomAttributesCacheGenerator_JsonProperty_set_AttributeProvider_mC95FBFBE2AA39AD182245F28F860C0E77A2A7FD2(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void JsonProperty_t41E840165F50D3CE4A2E9A48DD6064FCD59AAA0D_CustomAttributesCacheGenerator_JsonProperty_get_Converter_mD78430E0A86FFCAD9077CA45A8515433C1A7ACAF(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void JsonProperty_t41E840165F50D3CE4A2E9A48DD6064FCD59AAA0D_CustomAttributesCacheGenerator_JsonProperty_set_Converter_m09958BDD926D75B7CCBA933BE95D63DF69DE2831(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void JsonProperty_t41E840165F50D3CE4A2E9A48DD6064FCD59AAA0D_CustomAttributesCacheGenerator_JsonProperty_get_MemberConverter_m4AAF8854081CD744699C3A0BAF9314E817FDDD43(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void JsonProperty_t41E840165F50D3CE4A2E9A48DD6064FCD59AAA0D_CustomAttributesCacheGenerator_JsonProperty_set_MemberConverter_m02E881B46A470612898913FEDB7568219E7254BB(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void JsonProperty_t41E840165F50D3CE4A2E9A48DD6064FCD59AAA0D_CustomAttributesCacheGenerator_JsonProperty_get_Ignored_m46548BB8982F27C3B6FA9874BB36BBBE93C5BE5C(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void JsonProperty_t41E840165F50D3CE4A2E9A48DD6064FCD59AAA0D_CustomAttributesCacheGenerator_JsonProperty_set_Ignored_m633FB01A6826E5E0A0AD5EADA49C0AAB4FC61943(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void JsonProperty_t41E840165F50D3CE4A2E9A48DD6064FCD59AAA0D_CustomAttributesCacheGenerator_JsonProperty_get_Readable_m7AEAE72E61CEE2D7204DF7DDE4BB46FD49ACF13A(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void JsonProperty_t41E840165F50D3CE4A2E9A48DD6064FCD59AAA0D_CustomAttributesCacheGenerator_JsonProperty_set_Readable_m200607F2CB3518230B19840B81318784938634C1(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void JsonProperty_t41E840165F50D3CE4A2E9A48DD6064FCD59AAA0D_CustomAttributesCacheGenerator_JsonProperty_get_Writable_mD0AD78FD1A02462591351FA144C3A3E3F3D71C04(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void JsonProperty_t41E840165F50D3CE4A2E9A48DD6064FCD59AAA0D_CustomAttributesCacheGenerator_JsonProperty_set_Writable_m1EAC194E3A9B4958ADBC14262F398F126E22669D(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void JsonProperty_t41E840165F50D3CE4A2E9A48DD6064FCD59AAA0D_CustomAttributesCacheGenerator_JsonProperty_get_HasMemberAttribute_m8D7C6BB1CB98AB72EFA491333E9BD9DD9A8EDD4B(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void JsonProperty_t41E840165F50D3CE4A2E9A48DD6064FCD59AAA0D_CustomAttributesCacheGenerator_JsonProperty_set_HasMemberAttribute_m39D8D4BE4DD54068759166CF360FF13F27479EC6(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void JsonProperty_t41E840165F50D3CE4A2E9A48DD6064FCD59AAA0D_CustomAttributesCacheGenerator_JsonProperty_get_IsReference_m153CA8FA5C547CC6B05661BE83264D4C965B9396(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void JsonProperty_t41E840165F50D3CE4A2E9A48DD6064FCD59AAA0D_CustomAttributesCacheGenerator_JsonProperty_set_IsReference_mB2D4152E154865A7D98A41608BB5DF115514B3C1(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void JsonProperty_t41E840165F50D3CE4A2E9A48DD6064FCD59AAA0D_CustomAttributesCacheGenerator_JsonProperty_get_NullValueHandling_m42B929FF14E1D403D8DCBEE4C71937234A81ACB1(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void JsonProperty_t41E840165F50D3CE4A2E9A48DD6064FCD59AAA0D_CustomAttributesCacheGenerator_JsonProperty_set_NullValueHandling_m0DFFF6AA9EB90290031B2A8AA34B5F4860650701(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void JsonProperty_t41E840165F50D3CE4A2E9A48DD6064FCD59AAA0D_CustomAttributesCacheGenerator_JsonProperty_get_DefaultValueHandling_m07D9A4D362F431188F9B92F069FA06522F8313AE(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void JsonProperty_t41E840165F50D3CE4A2E9A48DD6064FCD59AAA0D_CustomAttributesCacheGenerator_JsonProperty_set_DefaultValueHandling_mC9F3205408913D8B21FA525CCDAC7AD032E71EDA(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void JsonProperty_t41E840165F50D3CE4A2E9A48DD6064FCD59AAA0D_CustomAttributesCacheGenerator_JsonProperty_get_ReferenceLoopHandling_m36303DDEA2267BA0C5AEE6F04C31376769E24DA2(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void JsonProperty_t41E840165F50D3CE4A2E9A48DD6064FCD59AAA0D_CustomAttributesCacheGenerator_JsonProperty_set_ReferenceLoopHandling_m6CE09B4B487EDFDF5D4ADF5B9750278478C1728A(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void JsonProperty_t41E840165F50D3CE4A2E9A48DD6064FCD59AAA0D_CustomAttributesCacheGenerator_JsonProperty_get_ObjectCreationHandling_mFD14FC71400EAA2ABE2953F94D8ED12B9C5EA137(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void JsonProperty_t41E840165F50D3CE4A2E9A48DD6064FCD59AAA0D_CustomAttributesCacheGenerator_JsonProperty_set_ObjectCreationHandling_m049C8E802C5BED6F45FC9BC69F6FCF492F3AC05D(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void JsonProperty_t41E840165F50D3CE4A2E9A48DD6064FCD59AAA0D_CustomAttributesCacheGenerator_JsonProperty_get_TypeNameHandling_m8AEB537470EE0ED5BFE82E69FC9351EFFF39E20D(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void JsonProperty_t41E840165F50D3CE4A2E9A48DD6064FCD59AAA0D_CustomAttributesCacheGenerator_JsonProperty_set_TypeNameHandling_mFF34196E4C937D5CB8C7BFAC959C9EDBABABD42B(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void JsonProperty_t41E840165F50D3CE4A2E9A48DD6064FCD59AAA0D_CustomAttributesCacheGenerator_JsonProperty_get_ShouldSerialize_m3DEC6D4BDF9DD0467C9496E31D064F1D584131AD(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void JsonProperty_t41E840165F50D3CE4A2E9A48DD6064FCD59AAA0D_CustomAttributesCacheGenerator_JsonProperty_set_ShouldSerialize_m691B604D63F062A266B8AA0FA42CE22A52D1C80A(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void JsonProperty_t41E840165F50D3CE4A2E9A48DD6064FCD59AAA0D_CustomAttributesCacheGenerator_JsonProperty_get_ShouldDeserialize_m5E69820CB6EF8CBCA0B173519DB4B32C7D6B3F26(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void JsonProperty_t41E840165F50D3CE4A2E9A48DD6064FCD59AAA0D_CustomAttributesCacheGenerator_JsonProperty_get_GetIsSpecified_m2D32E7F92B46871C97992E41AB0BADCCDE3715A3(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void JsonProperty_t41E840165F50D3CE4A2E9A48DD6064FCD59AAA0D_CustomAttributesCacheGenerator_JsonProperty_set_GetIsSpecified_m69FEAC27F917FDA7B479399D6127C5A8BFDFD1A8(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void JsonProperty_t41E840165F50D3CE4A2E9A48DD6064FCD59AAA0D_CustomAttributesCacheGenerator_JsonProperty_get_SetIsSpecified_mB3A71DEAD82264AA63FB0A0E53E85DAB582AF220(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void JsonProperty_t41E840165F50D3CE4A2E9A48DD6064FCD59AAA0D_CustomAttributesCacheGenerator_JsonProperty_set_SetIsSpecified_m43AD00A6F292CE4CD5DD72CF33B5AEB186BC115E(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void JsonProperty_t41E840165F50D3CE4A2E9A48DD6064FCD59AAA0D_CustomAttributesCacheGenerator_JsonProperty_get_ItemConverter_mF43A1CB4574AACC026916EB46DD3665428AD9E9E(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void JsonProperty_t41E840165F50D3CE4A2E9A48DD6064FCD59AAA0D_CustomAttributesCacheGenerator_JsonProperty_set_ItemConverter_mE132EF0F991D7A67FF1CC4B190F3A20680A74484(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void JsonProperty_t41E840165F50D3CE4A2E9A48DD6064FCD59AAA0D_CustomAttributesCacheGenerator_JsonProperty_get_ItemIsReference_m4FB1049E629A8E0C7532E7F42B5784838B641FE0(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void JsonProperty_t41E840165F50D3CE4A2E9A48DD6064FCD59AAA0D_CustomAttributesCacheGenerator_JsonProperty_set_ItemIsReference_m5520B121F62448B77C3005291868E4A6C363F04B(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void JsonProperty_t41E840165F50D3CE4A2E9A48DD6064FCD59AAA0D_CustomAttributesCacheGenerator_JsonProperty_get_ItemTypeNameHandling_mE9229CB08007B7C54F5FB9E1E100741102577526(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void JsonProperty_t41E840165F50D3CE4A2E9A48DD6064FCD59AAA0D_CustomAttributesCacheGenerator_JsonProperty_set_ItemTypeNameHandling_mB26022D26C0E5DF718210F7F91150A8AC42E79A6(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void JsonProperty_t41E840165F50D3CE4A2E9A48DD6064FCD59AAA0D_CustomAttributesCacheGenerator_JsonProperty_get_ItemReferenceLoopHandling_mCF84F2423F3441D4FF3A55CA0EB91A3C13F7C20E(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void JsonProperty_t41E840165F50D3CE4A2E9A48DD6064FCD59AAA0D_CustomAttributesCacheGenerator_JsonProperty_set_ItemReferenceLoopHandling_mA482C2F7321AB96C932D7FD5A3C4FBF6283E9B16(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CU3Ec__DisplayClass31_0_tEBC8FB4405C0886618B7E60D5CC88E48F6F53DB3_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CU3Ec_t57015DC78EDF73461DBB54732385DD028CFEC6EE_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CU3Ec__DisplayClass20_0_t4372BFC466654BC874AE3B7E8377F4018012ED74_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CU3Ec_t848677FBF04141492E7BF64DD58B81754F46E5E8_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void NamingStrategy_t5D39FF458CF086DD0CE681796D85AEB675DF3568_CustomAttributesCacheGenerator_U3CProcessDictionaryKeysU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void NamingStrategy_t5D39FF458CF086DD0CE681796D85AEB675DF3568_CustomAttributesCacheGenerator_U3COverrideSpecifiedNamesU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void NamingStrategy_t5D39FF458CF086DD0CE681796D85AEB675DF3568_CustomAttributesCacheGenerator_NamingStrategy_get_ProcessDictionaryKeys_mC1D4A8A7BDBC0305DBAE29FFEA3FFAB83177E249(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void NamingStrategy_t5D39FF458CF086DD0CE681796D85AEB675DF3568_CustomAttributesCacheGenerator_NamingStrategy_get_OverrideSpecifiedNames_m45FEC93C0AC20A53AEC9AD731A4D9B276AF26AF4(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ObjectConstructor_1_t527F32993C900EBBF0FDDEEFF0056620D5DA4751_CustomAttributesCacheGenerator_ObjectConstructor_1_Invoke_mA5F3ED8DF054AD93D77B657A673E2D065CE4539B____args0(CustomAttributesCache* cache)
{
	{
		ParamArrayAttribute_t9DCEB4CDDB8EDDB1124171D4C51FA6069EEA5C5F * tmp = (ParamArrayAttribute_t9DCEB4CDDB8EDDB1124171D4C51FA6069EEA5C5F *)cache->attributes[0];
		ParamArrayAttribute__ctor_mCC72AFF718185BA7B87FD8D9471F1274400C5719(tmp, NULL);
	}
}
static void OnErrorAttribute_t9863ED83B740ED4351812871247A2AAEB96F4CC6_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		AttributeUsageAttribute_tBB0BAAA82036E6FCDD80A688BBD039F6FFD8EA1C * tmp = (AttributeUsageAttribute_tBB0BAAA82036E6FCDD80A688BBD039F6FFD8EA1C *)cache->attributes[0];
		AttributeUsageAttribute__ctor_m5114E18826A49A025D48DC71904C430BD590656D(tmp, 64LL, NULL);
		AttributeUsageAttribute_set_Inherited_m56105980C36CB71AECD398C6077739BDFD2085E0_inline(tmp, false, NULL);
	}
}
static void U3CPrivateImplementationDetailsU3E_t687956554250C86B62013D8E7A369BD04CED3D5D_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
IL2CPP_EXTERN_C const CustomAttributesCacheGenerator g_Newtonsoft_Json_AttributeGenerators[];
const CustomAttributesCacheGenerator g_Newtonsoft_Json_AttributeGenerators[385] = 
{
	DefaultValueHandling_t1184EED87C3E17B62841C12572EB99E3A265660D_CustomAttributesCacheGenerator,
	JsonArrayAttribute_tD3DA8286372EF5A93A7B5763096BB8FAE439DF75_CustomAttributesCacheGenerator,
	JsonConstructorAttribute_t892706D7BC0282A74D608533AC8F56337D08F1D4_CustomAttributesCacheGenerator,
	JsonContainerAttribute_tDFAD9A1D2C6807836BC733B2D018398FB040332B_CustomAttributesCacheGenerator,
	JsonConverterAttribute_t9794A7BB2F2729A673B5D04D9F86EFA140116F54_CustomAttributesCacheGenerator,
	JsonDictionaryAttribute_tD03A79C1DB8271FF7805146BD10C58E572FC5878_CustomAttributesCacheGenerator,
	JsonExtensionDataAttribute_t742C5494ED033ECB5BFA1DC6C68D6AF3C62E753C_CustomAttributesCacheGenerator,
	JsonIgnoreAttribute_t2403F9AE18EEEA26B245AACF880F110B7DAA5E4C_CustomAttributesCacheGenerator,
	JsonObjectAttribute_t6B942E29C72B71A71664A8CD6C92C74DB4619E16_CustomAttributesCacheGenerator,
	JsonPropertyAttribute_tC08FC7586306803D19861D2F27F971556191A976_CustomAttributesCacheGenerator,
	JsonRequiredAttribute_t85A41B9B51790AC5ECB5DE64B221A52E91C835D9_CustomAttributesCacheGenerator,
	PreserveReferencesHandling_t1AA650F51C415CEB2460B728A5A0F56F8C897E1E_CustomAttributesCacheGenerator,
	TypeNameHandling_t00FDBF2FB49B273344C8A6CA3B6685EDFE82AF46_CustomAttributesCacheGenerator,
	CollectionUtils_tEA257A374A8D8D66306313AE24968769E787DFB8_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass9_0_t3A4E56C60CDCDA0E3AE51AF8172FB902A9BBC187_CustomAttributesCacheGenerator,
	DateTimeUtils_t45E6C3A51ACB4C69919336FD7872190C2ABEC404_CustomAttributesCacheGenerator,
	DictionaryWrapper_2_tAFD71BD180AB00234FC505689605910F8E273CC4_CustomAttributesCacheGenerator,
	U3CU3Ec_tC317791BE9F418DD0572473673C4C8FBBA96DB72_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass3_0_t698D68F8A4832DC94BD91657BD958CE2ECB410E0_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass4_0_1_t1548DAB3294A751C98FB408011DDB4C7567B79A0_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass5_0_1_tAF04C3EBC11CA31E3E339AEE8C90FD877EC8F3F1_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass6_0_1_t033847C9D5EC6238D52290988D716C1C3FD74441_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass7_0_1_tB1B860CA798AA13CFD191E3C10ADFDA00F996CF2_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass8_0_1_t97B8B8B5A6BD38CC528E90C0CFF2440E539218CD_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass9_0_1_tEDB18B86DFE4957BE7801C439CA96AC2CD0BA550_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass13_0_tD27FF2F8A49FF27EF149F9E61BB6C981251F9544_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass13_1_t414A08FA62A08F6B9CB2E1510140AB05021714E8_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass13_2_tA55985BA4FFD8AF33D671764AB13EC410498FBCE_CustomAttributesCacheGenerator,
	ReflectionUtils_t52FD79CBF528CE948B0F0D12C860FF87C1A5213B_CustomAttributesCacheGenerator,
	U3CU3Ec_t87B663D83EAE8982030BB4BF4A7FB9C4BEAC982B_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass42_0_tA195C0D57C247970275535F099F05968450BDC91_CustomAttributesCacheGenerator,
	StringReference_tF51D47B7A913E2F501FC14FB1B432AD2DEC8E27C_CustomAttributesCacheGenerator,
	StringReferenceExtensions_tEBA311DB9C048C894D9C1B6D8F01A4E4501F3327_CustomAttributesCacheGenerator,
	StringUtils_t3EA5628D6D6700734183C5E2BCFB2E99847AFD49_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass13_0_1_t307E58AEE0D1014B75CDC74B421C887D0463ABED_CustomAttributesCacheGenerator,
	TypeExtensions_tD99B77AB667AB754CDF59B0A6E9BCB48DFF86B7D_CustomAttributesCacheGenerator,
	U3CGetEnumeratorU3Ed__2_t451559940AEDCCEC6E550A43833F5747F6735C0B_CustomAttributesCacheGenerator,
	U3CU3Ec_t30F8D20CDE2939ABE1BA132502CFBFDB37A519CA_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass38_0_tBC9EDBB30C9F971EB64E970CF8287B67D8EE96B5_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass38_1_t3AA8C8586136BB6D815B1DF622E7E918FA7B4E33_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass38_2_tCCA3CA4FB84DADEBB49F8B4DAEBC7A7B9DE74F14_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass52_0_t47D44CD99A914B975A5807FF43AA93BB479B9C8C_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass68_0_t766708EC19046F0BD02D671A2671CD88B70FEA3E_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass69_0_tFEECEE33CB16927EBB0FFEF0186F58FEFEA79F1E_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass73_0_tDED699F39B829613F6126BA5E1C887311346B7EE_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass74_0_tEB782E56C6643FDDCF2D7B0E5A2DF0A1AED3AEB8_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass31_0_tEBC8FB4405C0886618B7E60D5CC88E48F6F53DB3_CustomAttributesCacheGenerator,
	U3CU3Ec_t57015DC78EDF73461DBB54732385DD028CFEC6EE_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass20_0_t4372BFC466654BC874AE3B7E8377F4018012ED74_CustomAttributesCacheGenerator,
	U3CU3Ec_t848677FBF04141492E7BF64DD58B81754F46E5E8_CustomAttributesCacheGenerator,
	OnErrorAttribute_t9863ED83B740ED4351812871247A2AAEB96F4CC6_CustomAttributesCacheGenerator,
	U3CPrivateImplementationDetailsU3E_t687956554250C86B62013D8E7A369BD04CED3D5D_CustomAttributesCacheGenerator,
	JsonContainerAttribute_tDFAD9A1D2C6807836BC733B2D018398FB040332B_CustomAttributesCacheGenerator_U3CItemConverterTypeU3Ek__BackingField,
	JsonContainerAttribute_tDFAD9A1D2C6807836BC733B2D018398FB040332B_CustomAttributesCacheGenerator_U3CItemConverterParametersU3Ek__BackingField,
	JsonContainerAttribute_tDFAD9A1D2C6807836BC733B2D018398FB040332B_CustomAttributesCacheGenerator_U3CNamingStrategyInstanceU3Ek__BackingField,
	JsonConvert_t4EB02F5D93E8B048375502BB1CF9DF714A930F07_CustomAttributesCacheGenerator_U3CDefaultSettingsU3Ek__BackingField,
	JsonConverterAttribute_t9794A7BB2F2729A673B5D04D9F86EFA140116F54_CustomAttributesCacheGenerator_U3CConverterParametersU3Ek__BackingField,
	JsonExtensionDataAttribute_t742C5494ED033ECB5BFA1DC6C68D6AF3C62E753C_CustomAttributesCacheGenerator_U3CWriteDataU3Ek__BackingField,
	JsonExtensionDataAttribute_t742C5494ED033ECB5BFA1DC6C68D6AF3C62E753C_CustomAttributesCacheGenerator_U3CReadDataU3Ek__BackingField,
	JsonPropertyAttribute_tC08FC7586306803D19861D2F27F971556191A976_CustomAttributesCacheGenerator_U3CItemConverterTypeU3Ek__BackingField,
	JsonPropertyAttribute_tC08FC7586306803D19861D2F27F971556191A976_CustomAttributesCacheGenerator_U3CItemConverterParametersU3Ek__BackingField,
	JsonPropertyAttribute_tC08FC7586306803D19861D2F27F971556191A976_CustomAttributesCacheGenerator_U3CNamingStrategyTypeU3Ek__BackingField,
	JsonPropertyAttribute_tC08FC7586306803D19861D2F27F971556191A976_CustomAttributesCacheGenerator_U3CNamingStrategyParametersU3Ek__BackingField,
	JsonPropertyAttribute_tC08FC7586306803D19861D2F27F971556191A976_CustomAttributesCacheGenerator_U3CPropertyNameU3Ek__BackingField,
	JsonReader_tD653CD612543D2FCECBC0A7B94961B76955BDA91_CustomAttributesCacheGenerator_U3CCloseInputU3Ek__BackingField,
	JsonReader_tD653CD612543D2FCECBC0A7B94961B76955BDA91_CustomAttributesCacheGenerator_U3CSupportMultipleContentU3Ek__BackingField,
	JsonReaderException_tBA4BC064CAFF75520942A57C880639F7B05ED591_CustomAttributesCacheGenerator_U3CLineNumberU3Ek__BackingField,
	JsonReaderException_tBA4BC064CAFF75520942A57C880639F7B05ED591_CustomAttributesCacheGenerator_U3CLinePositionU3Ek__BackingField,
	JsonReaderException_tBA4BC064CAFF75520942A57C880639F7B05ED591_CustomAttributesCacheGenerator_U3CPathU3Ek__BackingField,
	JsonSerializer_tCB6DBE5B2AA2EE465979EB49802CE4ECC4F141E3_CustomAttributesCacheGenerator_Error,
	JsonSerializerSettings_t7F7FFB2ACF58FFD6B39567AB672FA4ED7464BB1B_CustomAttributesCacheGenerator_U3CConvertersU3Ek__BackingField,
	JsonSerializerSettings_t7F7FFB2ACF58FFD6B39567AB672FA4ED7464BB1B_CustomAttributesCacheGenerator_U3CContractResolverU3Ek__BackingField,
	JsonSerializerSettings_t7F7FFB2ACF58FFD6B39567AB672FA4ED7464BB1B_CustomAttributesCacheGenerator_U3CEqualityComparerU3Ek__BackingField,
	JsonSerializerSettings_t7F7FFB2ACF58FFD6B39567AB672FA4ED7464BB1B_CustomAttributesCacheGenerator_U3CReferenceResolverProviderU3Ek__BackingField,
	JsonSerializerSettings_t7F7FFB2ACF58FFD6B39567AB672FA4ED7464BB1B_CustomAttributesCacheGenerator_U3CTraceWriterU3Ek__BackingField,
	JsonSerializerSettings_t7F7FFB2ACF58FFD6B39567AB672FA4ED7464BB1B_CustomAttributesCacheGenerator_U3CBinderU3Ek__BackingField,
	JsonSerializerSettings_t7F7FFB2ACF58FFD6B39567AB672FA4ED7464BB1B_CustomAttributesCacheGenerator_U3CErrorU3Ek__BackingField,
	JsonWriter_tDA82E9438C69ABAC3DF260ADBF20E9E287D41BB2_CustomAttributesCacheGenerator_U3CCloseOutputU3Ek__BackingField,
	JsonWriterException_t984447F6A0D6DD5CBE45299A34E85119DB0BFC2F_CustomAttributesCacheGenerator_U3CPathU3Ek__BackingField,
	TypeInformation_t30731224CF3BE05F8F32F6DC4FD5FAFFD4F6997D_CustomAttributesCacheGenerator_U3CTypeU3Ek__BackingField,
	TypeInformation_t30731224CF3BE05F8F32F6DC4FD5FAFFD4F6997D_CustomAttributesCacheGenerator_U3CTypeCodeU3Ek__BackingField,
	ReflectionMember_t7C544D9360A243D9DF6E05C5F156138C33DAA99D_CustomAttributesCacheGenerator_U3CMemberTypeU3Ek__BackingField,
	ReflectionMember_t7C544D9360A243D9DF6E05C5F156138C33DAA99D_CustomAttributesCacheGenerator_U3CGetterU3Ek__BackingField,
	ReflectionMember_t7C544D9360A243D9DF6E05C5F156138C33DAA99D_CustomAttributesCacheGenerator_U3CSetterU3Ek__BackingField,
	ReflectionObject_tC343774B9EE64C90DF1DBB77EEC193C4C9DD7361_CustomAttributesCacheGenerator_U3CCreatorU3Ek__BackingField,
	ReflectionObject_tC343774B9EE64C90DF1DBB77EEC193C4C9DD7361_CustomAttributesCacheGenerator_U3CMembersU3Ek__BackingField,
	DefaultContractResolver_t191C29E332665EFC1B0B1F1082BEC4B7DE866297_CustomAttributesCacheGenerator_U3CDefaultMembersSearchFlagsU3Ek__BackingField,
	DefaultContractResolver_t191C29E332665EFC1B0B1F1082BEC4B7DE866297_CustomAttributesCacheGenerator_U3CSerializeCompilerGeneratedMembersU3Ek__BackingField,
	DefaultContractResolver_t191C29E332665EFC1B0B1F1082BEC4B7DE866297_CustomAttributesCacheGenerator_U3CIgnoreSerializableInterfaceU3Ek__BackingField,
	DefaultContractResolver_t191C29E332665EFC1B0B1F1082BEC4B7DE866297_CustomAttributesCacheGenerator_U3CIgnoreSerializableAttributeU3Ek__BackingField,
	DefaultContractResolver_t191C29E332665EFC1B0B1F1082BEC4B7DE866297_CustomAttributesCacheGenerator_U3CNamingStrategyU3Ek__BackingField,
	ErrorContext_tFCEA6129DB7CC94DFEFC6A3EE16A4420F57B417E_CustomAttributesCacheGenerator_U3CTracedU3Ek__BackingField,
	ErrorContext_tFCEA6129DB7CC94DFEFC6A3EE16A4420F57B417E_CustomAttributesCacheGenerator_U3CErrorU3Ek__BackingField,
	ErrorContext_tFCEA6129DB7CC94DFEFC6A3EE16A4420F57B417E_CustomAttributesCacheGenerator_U3COriginalObjectU3Ek__BackingField,
	ErrorContext_tFCEA6129DB7CC94DFEFC6A3EE16A4420F57B417E_CustomAttributesCacheGenerator_U3CMemberU3Ek__BackingField,
	ErrorContext_tFCEA6129DB7CC94DFEFC6A3EE16A4420F57B417E_CustomAttributesCacheGenerator_U3CPathU3Ek__BackingField,
	ErrorContext_tFCEA6129DB7CC94DFEFC6A3EE16A4420F57B417E_CustomAttributesCacheGenerator_U3CHandledU3Ek__BackingField,
	ErrorEventArgs_tA2C1B2B76DF50EC25B52AB0FDD51BB23A2855AB2_CustomAttributesCacheGenerator_U3CCurrentObjectU3Ek__BackingField,
	ErrorEventArgs_tA2C1B2B76DF50EC25B52AB0FDD51BB23A2855AB2_CustomAttributesCacheGenerator_U3CErrorContextU3Ek__BackingField,
	JsonArrayContract_tE4BEF4DAEFA2D8A388C986A0E81255CC2B7FBF83_CustomAttributesCacheGenerator_U3CCollectionItemTypeU3Ek__BackingField,
	JsonArrayContract_tE4BEF4DAEFA2D8A388C986A0E81255CC2B7FBF83_CustomAttributesCacheGenerator_U3CIsMultidimensionalArrayU3Ek__BackingField,
	JsonArrayContract_tE4BEF4DAEFA2D8A388C986A0E81255CC2B7FBF83_CustomAttributesCacheGenerator_U3CIsArrayU3Ek__BackingField,
	JsonArrayContract_tE4BEF4DAEFA2D8A388C986A0E81255CC2B7FBF83_CustomAttributesCacheGenerator_U3CShouldCreateWrapperU3Ek__BackingField,
	JsonArrayContract_tE4BEF4DAEFA2D8A388C986A0E81255CC2B7FBF83_CustomAttributesCacheGenerator_U3CCanDeserializeU3Ek__BackingField,
	JsonArrayContract_tE4BEF4DAEFA2D8A388C986A0E81255CC2B7FBF83_CustomAttributesCacheGenerator_U3CHasParameterizedCreatorU3Ek__BackingField,
	JsonContainerContract_tE83C0C4EDD22FEDE907E1D5F027CAEBDF1DFADAC_CustomAttributesCacheGenerator_U3CItemConverterU3Ek__BackingField,
	JsonContainerContract_tE83C0C4EDD22FEDE907E1D5F027CAEBDF1DFADAC_CustomAttributesCacheGenerator_U3CItemIsReferenceU3Ek__BackingField,
	JsonContainerContract_tE83C0C4EDD22FEDE907E1D5F027CAEBDF1DFADAC_CustomAttributesCacheGenerator_U3CItemReferenceLoopHandlingU3Ek__BackingField,
	JsonContainerContract_tE83C0C4EDD22FEDE907E1D5F027CAEBDF1DFADAC_CustomAttributesCacheGenerator_U3CItemTypeNameHandlingU3Ek__BackingField,
	JsonContract_tF3638831C408783F93E44D96C7C10A699A0F7F0D_CustomAttributesCacheGenerator_U3CUnderlyingTypeU3Ek__BackingField,
	JsonContract_tF3638831C408783F93E44D96C7C10A699A0F7F0D_CustomAttributesCacheGenerator_U3CIsReferenceU3Ek__BackingField,
	JsonContract_tF3638831C408783F93E44D96C7C10A699A0F7F0D_CustomAttributesCacheGenerator_U3CConverterU3Ek__BackingField,
	JsonContract_tF3638831C408783F93E44D96C7C10A699A0F7F0D_CustomAttributesCacheGenerator_U3CInternalConverterU3Ek__BackingField,
	JsonContract_tF3638831C408783F93E44D96C7C10A699A0F7F0D_CustomAttributesCacheGenerator_U3CDefaultCreatorU3Ek__BackingField,
	JsonContract_tF3638831C408783F93E44D96C7C10A699A0F7F0D_CustomAttributesCacheGenerator_U3CDefaultCreatorNonPublicU3Ek__BackingField,
	JsonDictionaryContract_t66334CCEFE57D3900FF8DB5AC954360DEF1E2F8C_CustomAttributesCacheGenerator_U3CDictionaryKeyResolverU3Ek__BackingField,
	JsonDictionaryContract_t66334CCEFE57D3900FF8DB5AC954360DEF1E2F8C_CustomAttributesCacheGenerator_U3CDictionaryKeyTypeU3Ek__BackingField,
	JsonDictionaryContract_t66334CCEFE57D3900FF8DB5AC954360DEF1E2F8C_CustomAttributesCacheGenerator_U3CDictionaryValueTypeU3Ek__BackingField,
	JsonDictionaryContract_t66334CCEFE57D3900FF8DB5AC954360DEF1E2F8C_CustomAttributesCacheGenerator_U3CKeyContractU3Ek__BackingField,
	JsonDictionaryContract_t66334CCEFE57D3900FF8DB5AC954360DEF1E2F8C_CustomAttributesCacheGenerator_U3CShouldCreateWrapperU3Ek__BackingField,
	JsonDictionaryContract_t66334CCEFE57D3900FF8DB5AC954360DEF1E2F8C_CustomAttributesCacheGenerator_U3CHasParameterizedCreatorU3Ek__BackingField,
	JsonISerializableContract_t84536AF410FF3F89CC83FEFAB7530B8F51D347BF_CustomAttributesCacheGenerator_U3CISerializableCreatorU3Ek__BackingField,
	JsonObjectContract_tB36EC347E258A3A2B5A8560E8DC6D2A4BEF6DEB8_CustomAttributesCacheGenerator_U3CMemberSerializationU3Ek__BackingField,
	JsonObjectContract_tB36EC347E258A3A2B5A8560E8DC6D2A4BEF6DEB8_CustomAttributesCacheGenerator_U3CItemRequiredU3Ek__BackingField,
	JsonObjectContract_tB36EC347E258A3A2B5A8560E8DC6D2A4BEF6DEB8_CustomAttributesCacheGenerator_U3CPropertiesU3Ek__BackingField,
	JsonObjectContract_tB36EC347E258A3A2B5A8560E8DC6D2A4BEF6DEB8_CustomAttributesCacheGenerator_U3CExtensionDataSetterU3Ek__BackingField,
	JsonObjectContract_tB36EC347E258A3A2B5A8560E8DC6D2A4BEF6DEB8_CustomAttributesCacheGenerator_U3CExtensionDataGetterU3Ek__BackingField,
	JsonPrimitiveContract_tA595E8BA6018728333455EC3265935664D8EDC22_CustomAttributesCacheGenerator_U3CTypeCodeU3Ek__BackingField,
	JsonProperty_t41E840165F50D3CE4A2E9A48DD6064FCD59AAA0D_CustomAttributesCacheGenerator_U3CPropertyContractU3Ek__BackingField,
	JsonProperty_t41E840165F50D3CE4A2E9A48DD6064FCD59AAA0D_CustomAttributesCacheGenerator_U3CDeclaringTypeU3Ek__BackingField,
	JsonProperty_t41E840165F50D3CE4A2E9A48DD6064FCD59AAA0D_CustomAttributesCacheGenerator_U3COrderU3Ek__BackingField,
	JsonProperty_t41E840165F50D3CE4A2E9A48DD6064FCD59AAA0D_CustomAttributesCacheGenerator_U3CUnderlyingNameU3Ek__BackingField,
	JsonProperty_t41E840165F50D3CE4A2E9A48DD6064FCD59AAA0D_CustomAttributesCacheGenerator_U3CValueProviderU3Ek__BackingField,
	JsonProperty_t41E840165F50D3CE4A2E9A48DD6064FCD59AAA0D_CustomAttributesCacheGenerator_U3CAttributeProviderU3Ek__BackingField,
	JsonProperty_t41E840165F50D3CE4A2E9A48DD6064FCD59AAA0D_CustomAttributesCacheGenerator_U3CConverterU3Ek__BackingField,
	JsonProperty_t41E840165F50D3CE4A2E9A48DD6064FCD59AAA0D_CustomAttributesCacheGenerator_U3CMemberConverterU3Ek__BackingField,
	JsonProperty_t41E840165F50D3CE4A2E9A48DD6064FCD59AAA0D_CustomAttributesCacheGenerator_U3CIgnoredU3Ek__BackingField,
	JsonProperty_t41E840165F50D3CE4A2E9A48DD6064FCD59AAA0D_CustomAttributesCacheGenerator_U3CReadableU3Ek__BackingField,
	JsonProperty_t41E840165F50D3CE4A2E9A48DD6064FCD59AAA0D_CustomAttributesCacheGenerator_U3CWritableU3Ek__BackingField,
	JsonProperty_t41E840165F50D3CE4A2E9A48DD6064FCD59AAA0D_CustomAttributesCacheGenerator_U3CHasMemberAttributeU3Ek__BackingField,
	JsonProperty_t41E840165F50D3CE4A2E9A48DD6064FCD59AAA0D_CustomAttributesCacheGenerator_U3CIsReferenceU3Ek__BackingField,
	JsonProperty_t41E840165F50D3CE4A2E9A48DD6064FCD59AAA0D_CustomAttributesCacheGenerator_U3CNullValueHandlingU3Ek__BackingField,
	JsonProperty_t41E840165F50D3CE4A2E9A48DD6064FCD59AAA0D_CustomAttributesCacheGenerator_U3CDefaultValueHandlingU3Ek__BackingField,
	JsonProperty_t41E840165F50D3CE4A2E9A48DD6064FCD59AAA0D_CustomAttributesCacheGenerator_U3CReferenceLoopHandlingU3Ek__BackingField,
	JsonProperty_t41E840165F50D3CE4A2E9A48DD6064FCD59AAA0D_CustomAttributesCacheGenerator_U3CObjectCreationHandlingU3Ek__BackingField,
	JsonProperty_t41E840165F50D3CE4A2E9A48DD6064FCD59AAA0D_CustomAttributesCacheGenerator_U3CTypeNameHandlingU3Ek__BackingField,
	JsonProperty_t41E840165F50D3CE4A2E9A48DD6064FCD59AAA0D_CustomAttributesCacheGenerator_U3CShouldSerializeU3Ek__BackingField,
	JsonProperty_t41E840165F50D3CE4A2E9A48DD6064FCD59AAA0D_CustomAttributesCacheGenerator_U3CShouldDeserializeU3Ek__BackingField,
	JsonProperty_t41E840165F50D3CE4A2E9A48DD6064FCD59AAA0D_CustomAttributesCacheGenerator_U3CGetIsSpecifiedU3Ek__BackingField,
	JsonProperty_t41E840165F50D3CE4A2E9A48DD6064FCD59AAA0D_CustomAttributesCacheGenerator_U3CSetIsSpecifiedU3Ek__BackingField,
	JsonProperty_t41E840165F50D3CE4A2E9A48DD6064FCD59AAA0D_CustomAttributesCacheGenerator_U3CItemConverterU3Ek__BackingField,
	JsonProperty_t41E840165F50D3CE4A2E9A48DD6064FCD59AAA0D_CustomAttributesCacheGenerator_U3CItemIsReferenceU3Ek__BackingField,
	JsonProperty_t41E840165F50D3CE4A2E9A48DD6064FCD59AAA0D_CustomAttributesCacheGenerator_U3CItemTypeNameHandlingU3Ek__BackingField,
	JsonProperty_t41E840165F50D3CE4A2E9A48DD6064FCD59AAA0D_CustomAttributesCacheGenerator_U3CItemReferenceLoopHandlingU3Ek__BackingField,
	NamingStrategy_t5D39FF458CF086DD0CE681796D85AEB675DF3568_CustomAttributesCacheGenerator_U3CProcessDictionaryKeysU3Ek__BackingField,
	NamingStrategy_t5D39FF458CF086DD0CE681796D85AEB675DF3568_CustomAttributesCacheGenerator_U3COverrideSpecifiedNamesU3Ek__BackingField,
	JsonContainerAttribute_tDFAD9A1D2C6807836BC733B2D018398FB040332B_CustomAttributesCacheGenerator_JsonContainerAttribute_get_ItemConverterType_mE0959B267593FB7E6C69CC99954A1AB62C82551D,
	JsonContainerAttribute_tDFAD9A1D2C6807836BC733B2D018398FB040332B_CustomAttributesCacheGenerator_JsonContainerAttribute_get_ItemConverterParameters_mC22A7EB75C02CB7460E4D8E1DF4E3685C04413C5,
	JsonContainerAttribute_tDFAD9A1D2C6807836BC733B2D018398FB040332B_CustomAttributesCacheGenerator_JsonContainerAttribute_get_NamingStrategyInstance_mCB5C826F2FDD2091632B0FD8A25DDECF528C513C,
	JsonContainerAttribute_tDFAD9A1D2C6807836BC733B2D018398FB040332B_CustomAttributesCacheGenerator_JsonContainerAttribute_set_NamingStrategyInstance_mDF8D8278251CF937FC050DF7DC9B91C8F27BD5E4,
	JsonConvert_t4EB02F5D93E8B048375502BB1CF9DF714A930F07_CustomAttributesCacheGenerator_JsonConvert_get_DefaultSettings_m214B88ECC58290FE283050B8A4C2E08F1755C83A,
	JsonConverterAttribute_t9794A7BB2F2729A673B5D04D9F86EFA140116F54_CustomAttributesCacheGenerator_JsonConverterAttribute_get_ConverterParameters_m17E8F4F8CD589CD1E5C2D140C98F8B8863ADBF68,
	JsonExtensionDataAttribute_t742C5494ED033ECB5BFA1DC6C68D6AF3C62E753C_CustomAttributesCacheGenerator_JsonExtensionDataAttribute_get_WriteData_m0718786B6D6C25112594F098A8CF31A8C90DC24A,
	JsonExtensionDataAttribute_t742C5494ED033ECB5BFA1DC6C68D6AF3C62E753C_CustomAttributesCacheGenerator_JsonExtensionDataAttribute_get_ReadData_mBF0CDE7323AB877197F0EC890140CA3CD80BA36F,
	JsonPropertyAttribute_tC08FC7586306803D19861D2F27F971556191A976_CustomAttributesCacheGenerator_JsonPropertyAttribute_get_ItemConverterType_mC4581A062D9DCF64DB3CE11BE06771A9347D9EB6,
	JsonPropertyAttribute_tC08FC7586306803D19861D2F27F971556191A976_CustomAttributesCacheGenerator_JsonPropertyAttribute_get_ItemConverterParameters_m3845C44A0AD6345D2E53707EBCDA7DEA54FC8CC8,
	JsonPropertyAttribute_tC08FC7586306803D19861D2F27F971556191A976_CustomAttributesCacheGenerator_JsonPropertyAttribute_get_NamingStrategyType_m9BD354DEDD5126AB21DA4D7C2A49A4396014BDE8,
	JsonPropertyAttribute_tC08FC7586306803D19861D2F27F971556191A976_CustomAttributesCacheGenerator_JsonPropertyAttribute_get_NamingStrategyParameters_mD0BC1CD962875A82871498CD866B6D840BF93196,
	JsonPropertyAttribute_tC08FC7586306803D19861D2F27F971556191A976_CustomAttributesCacheGenerator_JsonPropertyAttribute_get_PropertyName_m23FFB79CC575178480990AD5FB0FD1152A912D99,
	JsonReader_tD653CD612543D2FCECBC0A7B94961B76955BDA91_CustomAttributesCacheGenerator_JsonReader_get_CloseInput_m87EB3CF7C34E7FBA9BA84B812CC4CAE0B9FDE974,
	JsonReader_tD653CD612543D2FCECBC0A7B94961B76955BDA91_CustomAttributesCacheGenerator_JsonReader_set_CloseInput_mE443BF3B39BAE245D6D22180962F4F3B92C95EFD,
	JsonReader_tD653CD612543D2FCECBC0A7B94961B76955BDA91_CustomAttributesCacheGenerator_JsonReader_get_SupportMultipleContent_mB43563289F840AA64EE3133642CFC1D3340B2912,
	JsonReaderException_tBA4BC064CAFF75520942A57C880639F7B05ED591_CustomAttributesCacheGenerator_JsonReaderException_set_LineNumber_m9F168878C1D77923580E25AB5072BB89A2A1FE93,
	JsonReaderException_tBA4BC064CAFF75520942A57C880639F7B05ED591_CustomAttributesCacheGenerator_JsonReaderException_set_LinePosition_mCCABCCE6749D6A36BF93E146FEBCA1E539F83FDA,
	JsonReaderException_tBA4BC064CAFF75520942A57C880639F7B05ED591_CustomAttributesCacheGenerator_JsonReaderException_set_Path_mC99BD3BB99E4F85727B345B5CCDA60E43AB30AE0,
	JsonSerializer_tCB6DBE5B2AA2EE465979EB49802CE4ECC4F141E3_CustomAttributesCacheGenerator_JsonSerializer_add_Error_m5C077501D7D73DDF93019D736A6F72FCB733EFDD,
	JsonSerializer_tCB6DBE5B2AA2EE465979EB49802CE4ECC4F141E3_CustomAttributesCacheGenerator_JsonSerializer_remove_Error_mF178375434BC06707ADEE0A7DC544296D5146B76,
	JsonSerializerSettings_t7F7FFB2ACF58FFD6B39567AB672FA4ED7464BB1B_CustomAttributesCacheGenerator_JsonSerializerSettings_get_Converters_m862DFE5BDA2760F5946FB6DDB34DCA67FC6FF29D,
	JsonSerializerSettings_t7F7FFB2ACF58FFD6B39567AB672FA4ED7464BB1B_CustomAttributesCacheGenerator_JsonSerializerSettings_get_ContractResolver_mAE8A1AB42B6FA02463E9A3B613A777983107D591,
	JsonSerializerSettings_t7F7FFB2ACF58FFD6B39567AB672FA4ED7464BB1B_CustomAttributesCacheGenerator_JsonSerializerSettings_get_EqualityComparer_m41C02E38C29AF6A4CAC4894CC79FA724974CDC82,
	JsonSerializerSettings_t7F7FFB2ACF58FFD6B39567AB672FA4ED7464BB1B_CustomAttributesCacheGenerator_JsonSerializerSettings_get_ReferenceResolverProvider_mE84D665F375BFFC7BF287CB869DDEF0BE408853D,
	JsonSerializerSettings_t7F7FFB2ACF58FFD6B39567AB672FA4ED7464BB1B_CustomAttributesCacheGenerator_JsonSerializerSettings_get_TraceWriter_mE3D106D673BF56399607EADC698FAC00AFE65C72,
	JsonSerializerSettings_t7F7FFB2ACF58FFD6B39567AB672FA4ED7464BB1B_CustomAttributesCacheGenerator_JsonSerializerSettings_get_Binder_m9508F5EA174FF976E69F0C78B3934BFC56E2F035,
	JsonSerializerSettings_t7F7FFB2ACF58FFD6B39567AB672FA4ED7464BB1B_CustomAttributesCacheGenerator_JsonSerializerSettings_get_Error_mEDC7BE577D288DD35B65F534E00C681854578371,
	JsonTextWriter_t33A7556B4AA038ACC39A2D995AA874612F62A10B_CustomAttributesCacheGenerator_JsonTextWriter_WriteValue_m5BC424CE917FE84086E29B839EC6A292C75EE795,
	JsonTextWriter_t33A7556B4AA038ACC39A2D995AA874612F62A10B_CustomAttributesCacheGenerator_JsonTextWriter_WriteValue_m14AAEAAC3218C11F806E7BD4E7F4D0592B321268,
	JsonTextWriter_t33A7556B4AA038ACC39A2D995AA874612F62A10B_CustomAttributesCacheGenerator_JsonTextWriter_WriteValue_mD772006CDE4E7F9E184D192814132E2D4EC77884,
	JsonTextWriter_t33A7556B4AA038ACC39A2D995AA874612F62A10B_CustomAttributesCacheGenerator_JsonTextWriter_WriteValue_m909BF8D81AD7BD056580595C10A84ECAEC244C67,
	JsonWriter_tDA82E9438C69ABAC3DF260ADBF20E9E287D41BB2_CustomAttributesCacheGenerator_JsonWriter_get_CloseOutput_m5CADEDBE98FDF046F9BF209C242A07DB08C9678B,
	JsonWriter_tDA82E9438C69ABAC3DF260ADBF20E9E287D41BB2_CustomAttributesCacheGenerator_JsonWriter_set_CloseOutput_m583E020AD56A73E77CDE34C208DB0115E067C7E0,
	JsonWriter_tDA82E9438C69ABAC3DF260ADBF20E9E287D41BB2_CustomAttributesCacheGenerator_JsonWriter_WriteValue_m9B2A0C880487EFEE143F0A2F0282A24F58E39BB6,
	JsonWriter_tDA82E9438C69ABAC3DF260ADBF20E9E287D41BB2_CustomAttributesCacheGenerator_JsonWriter_WriteValue_m3B79659B76CE197EA62C98D9380CA276779BA041,
	JsonWriter_tDA82E9438C69ABAC3DF260ADBF20E9E287D41BB2_CustomAttributesCacheGenerator_JsonWriter_WriteValue_mD04367BAD3BA9FC4D4991C0BA35B49DA15675F96,
	JsonWriter_tDA82E9438C69ABAC3DF260ADBF20E9E287D41BB2_CustomAttributesCacheGenerator_JsonWriter_WriteValue_m402DBD63BF18B24EAA0D34D7D6208CE8C696F3AF,
	JsonWriter_tDA82E9438C69ABAC3DF260ADBF20E9E287D41BB2_CustomAttributesCacheGenerator_JsonWriter_WriteValue_m55DD87A8EFEBB6A73C70571592F6F2B63DFED99D,
	JsonWriter_tDA82E9438C69ABAC3DF260ADBF20E9E287D41BB2_CustomAttributesCacheGenerator_JsonWriter_WriteValue_mE256A509ADFD1D2EB3C2CE59C48BD4B5F9A7D075,
	JsonWriter_tDA82E9438C69ABAC3DF260ADBF20E9E287D41BB2_CustomAttributesCacheGenerator_JsonWriter_WriteValue_m497A20806672585D0FBD37DF1707485B294C5C3F,
	JsonWriter_tDA82E9438C69ABAC3DF260ADBF20E9E287D41BB2_CustomAttributesCacheGenerator_JsonWriter_WriteValue_m06116AF904E6923B19234D2787602E24995B8F7E,
	JsonWriterException_t984447F6A0D6DD5CBE45299A34E85119DB0BFC2F_CustomAttributesCacheGenerator_JsonWriterException_set_Path_mD53E9F4BDDD8ED20EB04BF6CCE733F2DCC593BD9,
	CollectionUtils_tEA257A374A8D8D66306313AE24968769E787DFB8_CustomAttributesCacheGenerator_CollectionUtils_AddRange_mBDCB767F18960E8B5579E9154A3DED0B0875D6AB,
	CollectionUtils_tEA257A374A8D8D66306313AE24968769E787DFB8_CustomAttributesCacheGenerator_CollectionUtils_AddRange_m20628EBA8AA00702EA16A009D8E2B46065F63085,
	CollectionUtils_tEA257A374A8D8D66306313AE24968769E787DFB8_CustomAttributesCacheGenerator_CollectionUtils_IndexOf_m311AF18D5D87B26DA1B611F52081180396BDE116,
	CollectionUtils_tEA257A374A8D8D66306313AE24968769E787DFB8_CustomAttributesCacheGenerator_CollectionUtils_Contains_m3BD5EF73B3804FE80FB14DD5B92326202AE01498,
	TypeInformation_t30731224CF3BE05F8F32F6DC4FD5FAFFD4F6997D_CustomAttributesCacheGenerator_TypeInformation_get_Type_mF16FF676DAA9DEC81AF0A9FB81DA0834709F1F9D,
	TypeInformation_t30731224CF3BE05F8F32F6DC4FD5FAFFD4F6997D_CustomAttributesCacheGenerator_TypeInformation_set_Type_m87F6F17BD4A47C4F020DBC8E25462D3F4651B750,
	TypeInformation_t30731224CF3BE05F8F32F6DC4FD5FAFFD4F6997D_CustomAttributesCacheGenerator_TypeInformation_get_TypeCode_m3F9374AB9A57BA647C5B53B633D16102723C8435,
	TypeInformation_t30731224CF3BE05F8F32F6DC4FD5FAFFD4F6997D_CustomAttributesCacheGenerator_TypeInformation_set_TypeCode_mD82AA32EF804000604122B9541C621AF00622767,
	DateTimeUtils_t45E6C3A51ACB4C69919336FD7872190C2ABEC404_CustomAttributesCacheGenerator_DateTimeUtils_GetUtcOffset_m1B6B70139EF050BAA434164D316C6619F5FE292A,
	ReflectionMember_t7C544D9360A243D9DF6E05C5F156138C33DAA99D_CustomAttributesCacheGenerator_ReflectionMember_get_MemberType_m6B83E257F42DD1C53558CD81BE74005F43794860,
	ReflectionMember_t7C544D9360A243D9DF6E05C5F156138C33DAA99D_CustomAttributesCacheGenerator_ReflectionMember_set_MemberType_m5909AE0B8D67C06D8069E6FC9676EFBEAE0F3CD9,
	ReflectionMember_t7C544D9360A243D9DF6E05C5F156138C33DAA99D_CustomAttributesCacheGenerator_ReflectionMember_get_Getter_mCF5EE2696804BA8D7E7A2DB16B09840656FF9D35,
	ReflectionMember_t7C544D9360A243D9DF6E05C5F156138C33DAA99D_CustomAttributesCacheGenerator_ReflectionMember_set_Getter_mB7C5D68B17069E9B971342D29BBCE72CE33F91CD,
	ReflectionMember_t7C544D9360A243D9DF6E05C5F156138C33DAA99D_CustomAttributesCacheGenerator_ReflectionMember_set_Setter_mD573E9151D6A27AA511FA936A47E43A25BD33DB7,
	ReflectionObject_tC343774B9EE64C90DF1DBB77EEC193C4C9DD7361_CustomAttributesCacheGenerator_ReflectionObject_get_Creator_mC6AD5773E9CC38F72E1282A59B462E45FCE60164,
	ReflectionObject_tC343774B9EE64C90DF1DBB77EEC193C4C9DD7361_CustomAttributesCacheGenerator_ReflectionObject_set_Creator_m1E3FB878C8548FA0D022F47BB82426A110FD394A,
	ReflectionObject_tC343774B9EE64C90DF1DBB77EEC193C4C9DD7361_CustomAttributesCacheGenerator_ReflectionObject_get_Members_m58FCDAAA8FDBE6913E756A598748FC9856C1A8EA,
	ReflectionObject_tC343774B9EE64C90DF1DBB77EEC193C4C9DD7361_CustomAttributesCacheGenerator_ReflectionObject_set_Members_m17AFF4F18816B21290FBAC6AAD3D45D8FA7FFF82,
	ReflectionUtils_t52FD79CBF528CE948B0F0D12C860FF87C1A5213B_CustomAttributesCacheGenerator_ReflectionUtils_IsVirtual_mDAB826DEBE824D7B3DED6ACE9F582D37ADBDECD1,
	ReflectionUtils_t52FD79CBF528CE948B0F0D12C860FF87C1A5213B_CustomAttributesCacheGenerator_ReflectionUtils_GetBaseDefinition_m1495E37E8666896240BC6A5904F061950A1783CB,
	ReflectionUtils_t52FD79CBF528CE948B0F0D12C860FF87C1A5213B_CustomAttributesCacheGenerator_ReflectionUtils_RemoveFlag_mC2CD5A61EDBBB52F4D3CA5DB138800195E89F8E3,
	StringReferenceExtensions_tEBA311DB9C048C894D9C1B6D8F01A4E4501F3327_CustomAttributesCacheGenerator_StringReferenceExtensions_IndexOf_m0521F7953B075BB955750D6AD8053D53D7ECD6AC,
	StringReferenceExtensions_tEBA311DB9C048C894D9C1B6D8F01A4E4501F3327_CustomAttributesCacheGenerator_StringReferenceExtensions_StartsWith_m6D9020568686A182328C1F71CFC1800A870CFEB3,
	StringReferenceExtensions_tEBA311DB9C048C894D9C1B6D8F01A4E4501F3327_CustomAttributesCacheGenerator_StringReferenceExtensions_EndsWith_mD6F3B9EEC429EAB4F6E05E69EECF814DDF3F764B,
	StringUtils_t3EA5628D6D6700734183C5E2BCFB2E99847AFD49_CustomAttributesCacheGenerator_StringUtils_FormatWith_mF78320AE4049E77D6DDEC01680F7D98C44E2442D,
	StringUtils_t3EA5628D6D6700734183C5E2BCFB2E99847AFD49_CustomAttributesCacheGenerator_StringUtils_FormatWith_mEB092C5B96EC84FDC050432B1A40DF8A83BFA2E8,
	StringUtils_t3EA5628D6D6700734183C5E2BCFB2E99847AFD49_CustomAttributesCacheGenerator_StringUtils_FormatWith_m4FCAF6C2F661AFA5DECB79DDD2F4C1C63933AC02,
	StringUtils_t3EA5628D6D6700734183C5E2BCFB2E99847AFD49_CustomAttributesCacheGenerator_StringUtils_FormatWith_mFFF18C22B96311F0FF31E7711F474AB79C1D96EF,
	StringUtils_t3EA5628D6D6700734183C5E2BCFB2E99847AFD49_CustomAttributesCacheGenerator_StringUtils_FormatWith_mF72FDDA3EB515E398CA2D73E829FC7F8AD4F1D2B,
	StringUtils_t3EA5628D6D6700734183C5E2BCFB2E99847AFD49_CustomAttributesCacheGenerator_StringUtils_ForgivingCaseSensitiveFind_m18538F551C05EA50E4D460A66401B34CBA6AE0A1,
	StringUtils_t3EA5628D6D6700734183C5E2BCFB2E99847AFD49_CustomAttributesCacheGenerator_StringUtils_EndsWith_m66FA0222A273889E4E523E10DE65E833F5536F58,
	TypeExtensions_tD99B77AB667AB754CDF59B0A6E9BCB48DFF86B7D_CustomAttributesCacheGenerator_TypeExtensions_MemberType_m6B0C77E72F48DC1B4204737359510A81F3E464C4,
	TypeExtensions_tD99B77AB667AB754CDF59B0A6E9BCB48DFF86B7D_CustomAttributesCacheGenerator_TypeExtensions_ContainsGenericParameters_m291B9B1787BCE6E8D410828890476C7E868ED823,
	TypeExtensions_tD99B77AB667AB754CDF59B0A6E9BCB48DFF86B7D_CustomAttributesCacheGenerator_TypeExtensions_IsInterface_mC86FEECE611B133413962A5EFE5DC85B92D76381,
	TypeExtensions_tD99B77AB667AB754CDF59B0A6E9BCB48DFF86B7D_CustomAttributesCacheGenerator_TypeExtensions_IsGenericType_mD7A79EF6D299F4D40932AEA3CF8BC3DD8E0C06D2,
	TypeExtensions_tD99B77AB667AB754CDF59B0A6E9BCB48DFF86B7D_CustomAttributesCacheGenerator_TypeExtensions_IsGenericTypeDefinition_m16881F2378C11CF6BFB7B3A127643F4129293AF8,
	TypeExtensions_tD99B77AB667AB754CDF59B0A6E9BCB48DFF86B7D_CustomAttributesCacheGenerator_TypeExtensions_BaseType_mB412285DD088CA76F89534B78D1AE65FFF30F754,
	TypeExtensions_tD99B77AB667AB754CDF59B0A6E9BCB48DFF86B7D_CustomAttributesCacheGenerator_TypeExtensions_IsEnum_m3D3EA9AF4638A68716201C314A1C75C0D0FE6CAA,
	TypeExtensions_tD99B77AB667AB754CDF59B0A6E9BCB48DFF86B7D_CustomAttributesCacheGenerator_TypeExtensions_IsClass_mF1942239F1A4C2B093A05F39F7F8F7D94EE34A1D,
	TypeExtensions_tD99B77AB667AB754CDF59B0A6E9BCB48DFF86B7D_CustomAttributesCacheGenerator_TypeExtensions_IsSealed_m9BF5CB41B9F60342CE4634AA4DE803492059844F,
	TypeExtensions_tD99B77AB667AB754CDF59B0A6E9BCB48DFF86B7D_CustomAttributesCacheGenerator_TypeExtensions_IsAbstract_m3E114BBDB2B178E154B5AD58D0C203A51BE091F0,
	TypeExtensions_tD99B77AB667AB754CDF59B0A6E9BCB48DFF86B7D_CustomAttributesCacheGenerator_TypeExtensions_IsValueType_mCAEDF96A86663613DC39781684AF92CC1531DF2B,
	TypeExtensions_tD99B77AB667AB754CDF59B0A6E9BCB48DFF86B7D_CustomAttributesCacheGenerator_TypeExtensions_AssignableToTypeName_mD522101DAFD0E49737C49A7BE5162B7D23312CC0,
	TypeExtensions_tD99B77AB667AB754CDF59B0A6E9BCB48DFF86B7D_CustomAttributesCacheGenerator_TypeExtensions_AssignableToTypeName_mF0CE3AB37FDF6B916128D0B9735D8ABD7CC649A4,
	TypeExtensions_tD99B77AB667AB754CDF59B0A6E9BCB48DFF86B7D_CustomAttributesCacheGenerator_TypeExtensions_ImplementInterface_m7183F3D7AF62459483F1E37786034552734BE904,
	DefaultContractResolver_t191C29E332665EFC1B0B1F1082BEC4B7DE866297_CustomAttributesCacheGenerator_DefaultContractResolver_get_DefaultMembersSearchFlags_mA6F171D6825AF07E7AC585C5B89B8B1BA57FAEC3,
	DefaultContractResolver_t191C29E332665EFC1B0B1F1082BEC4B7DE866297_CustomAttributesCacheGenerator_DefaultContractResolver_set_DefaultMembersSearchFlags_m89CA59C8A801BB1C3023BAD591A244271051A248,
	DefaultContractResolver_t191C29E332665EFC1B0B1F1082BEC4B7DE866297_CustomAttributesCacheGenerator_DefaultContractResolver_get_SerializeCompilerGeneratedMembers_mCEC95D8DC060D61621A326CE8DB8B6D1769BB0CC,
	DefaultContractResolver_t191C29E332665EFC1B0B1F1082BEC4B7DE866297_CustomAttributesCacheGenerator_DefaultContractResolver_get_IgnoreSerializableInterface_m10A32A92DCF98D1EDD209D880F17359001FD0125,
	DefaultContractResolver_t191C29E332665EFC1B0B1F1082BEC4B7DE866297_CustomAttributesCacheGenerator_DefaultContractResolver_get_IgnoreSerializableAttribute_m1429E466AFA1212DADA0B9223A04C74BD5DCA8CE,
	DefaultContractResolver_t191C29E332665EFC1B0B1F1082BEC4B7DE866297_CustomAttributesCacheGenerator_DefaultContractResolver_set_IgnoreSerializableAttribute_m54DC842D81DEABFD3198F00DDEBF83E1EF2C6936,
	DefaultContractResolver_t191C29E332665EFC1B0B1F1082BEC4B7DE866297_CustomAttributesCacheGenerator_DefaultContractResolver_get_NamingStrategy_m5C917ABAD194876A14B94BF7FD9D5879527A842A,
	DefaultContractResolver_t191C29E332665EFC1B0B1F1082BEC4B7DE866297_CustomAttributesCacheGenerator_DefaultContractResolver__ctor_mCC6008697D963A3DD69FD7F4D94D3EAA5A9F6036,
	U3CGetEnumeratorU3Ed__2_t451559940AEDCCEC6E550A43833F5747F6735C0B_CustomAttributesCacheGenerator_U3CGetEnumeratorU3Ed__2__ctor_m282D27A36E401D60D0CEB656F332C08DF2C68A9D,
	U3CGetEnumeratorU3Ed__2_t451559940AEDCCEC6E550A43833F5747F6735C0B_CustomAttributesCacheGenerator_U3CGetEnumeratorU3Ed__2_System_IDisposable_Dispose_m4A5FE06D9B3E31D62C920B23024F7FA523EBF5EE,
	U3CGetEnumeratorU3Ed__2_t451559940AEDCCEC6E550A43833F5747F6735C0B_CustomAttributesCacheGenerator_U3CGetEnumeratorU3Ed__2_System_Collections_Generic_IEnumeratorU3CSystem_Collections_Generic_KeyValuePairU3CSystem_ObjectU2CSystem_ObjectU3EU3E_get_Current_mDCB15E4324723B4C6E695C3B96A5F79AEB296521,
	U3CGetEnumeratorU3Ed__2_t451559940AEDCCEC6E550A43833F5747F6735C0B_CustomAttributesCacheGenerator_U3CGetEnumeratorU3Ed__2_System_Collections_IEnumerator_Reset_m9E947FD2E6912DE2D9BEEBC105AB32145E30F3EC,
	U3CGetEnumeratorU3Ed__2_t451559940AEDCCEC6E550A43833F5747F6735C0B_CustomAttributesCacheGenerator_U3CGetEnumeratorU3Ed__2_System_Collections_IEnumerator_get_Current_mE36CD065E2DAEA5258D089A15BB291A19D910ED3,
	ErrorContext_tFCEA6129DB7CC94DFEFC6A3EE16A4420F57B417E_CustomAttributesCacheGenerator_ErrorContext_get_Traced_m2600308CFFECA1B408B14E621987926BBB1799C5,
	ErrorContext_tFCEA6129DB7CC94DFEFC6A3EE16A4420F57B417E_CustomAttributesCacheGenerator_ErrorContext_set_Traced_m8BA7FD9673E83668AB685995DCF399BFFF77A21E,
	ErrorContext_tFCEA6129DB7CC94DFEFC6A3EE16A4420F57B417E_CustomAttributesCacheGenerator_ErrorContext_get_Error_m4BA9AE89A1463A8187BCAA2081B10A7EE412B4B8,
	ErrorContext_tFCEA6129DB7CC94DFEFC6A3EE16A4420F57B417E_CustomAttributesCacheGenerator_ErrorContext_set_Error_m3C47A4456A7D207FCB0AA8DBDFC0EACE349ACB27,
	ErrorContext_tFCEA6129DB7CC94DFEFC6A3EE16A4420F57B417E_CustomAttributesCacheGenerator_ErrorContext_set_OriginalObject_m2D5A6F54F6DBB717BFEFFE41ABFD5F8FF944C8BA,
	ErrorContext_tFCEA6129DB7CC94DFEFC6A3EE16A4420F57B417E_CustomAttributesCacheGenerator_ErrorContext_set_Member_mA60560017299B78E3269E2E30E178D46CD52DE5E,
	ErrorContext_tFCEA6129DB7CC94DFEFC6A3EE16A4420F57B417E_CustomAttributesCacheGenerator_ErrorContext_set_Path_mAB5E68DCF6CFD684635470CEE002AFBA2ECCA6DF,
	ErrorContext_tFCEA6129DB7CC94DFEFC6A3EE16A4420F57B417E_CustomAttributesCacheGenerator_ErrorContext_get_Handled_m00CB858B5DF909C699425AA6A1EB383ABA527158,
	ErrorEventArgs_tA2C1B2B76DF50EC25B52AB0FDD51BB23A2855AB2_CustomAttributesCacheGenerator_ErrorEventArgs_set_CurrentObject_mE2464018DBF50495E7AC91C004C29BB678F408EC,
	ErrorEventArgs_tA2C1B2B76DF50EC25B52AB0FDD51BB23A2855AB2_CustomAttributesCacheGenerator_ErrorEventArgs_set_ErrorContext_mD68107F654E6143A90BA4187E54E358ADA9F3EEF,
	JsonArrayContract_tE4BEF4DAEFA2D8A388C986A0E81255CC2B7FBF83_CustomAttributesCacheGenerator_JsonArrayContract_get_CollectionItemType_m3CECF4280B1768FDEF6A7111741C93DE7F7DDF22,
	JsonArrayContract_tE4BEF4DAEFA2D8A388C986A0E81255CC2B7FBF83_CustomAttributesCacheGenerator_JsonArrayContract_set_CollectionItemType_m459F1B1BB37AA94FC3386F62D51E850E3D3B9654,
	JsonArrayContract_tE4BEF4DAEFA2D8A388C986A0E81255CC2B7FBF83_CustomAttributesCacheGenerator_JsonArrayContract_get_IsMultidimensionalArray_m882BEE069C4D30998786CDEF57704C966CA33E62,
	JsonArrayContract_tE4BEF4DAEFA2D8A388C986A0E81255CC2B7FBF83_CustomAttributesCacheGenerator_JsonArrayContract_set_IsMultidimensionalArray_m85FD29E6E2FA63221F8CC1150E97D5D53016C251,
	JsonArrayContract_tE4BEF4DAEFA2D8A388C986A0E81255CC2B7FBF83_CustomAttributesCacheGenerator_JsonArrayContract_get_IsArray_m72D6C22445B07CFE617D61656301BEAB4C39CAD8,
	JsonArrayContract_tE4BEF4DAEFA2D8A388C986A0E81255CC2B7FBF83_CustomAttributesCacheGenerator_JsonArrayContract_set_IsArray_m7F43AFECAC00B630DBB56A94DA548141448AF781,
	JsonArrayContract_tE4BEF4DAEFA2D8A388C986A0E81255CC2B7FBF83_CustomAttributesCacheGenerator_JsonArrayContract_get_ShouldCreateWrapper_mE8AEA40AFF96BA8EA72B04C1FC377E98A5F21086,
	JsonArrayContract_tE4BEF4DAEFA2D8A388C986A0E81255CC2B7FBF83_CustomAttributesCacheGenerator_JsonArrayContract_set_ShouldCreateWrapper_m437A0E5C1DE254AFB886EB246A854E775EA12C72,
	JsonArrayContract_tE4BEF4DAEFA2D8A388C986A0E81255CC2B7FBF83_CustomAttributesCacheGenerator_JsonArrayContract_get_CanDeserialize_mBCB4583F10A61CBDC6C60CA4D88D07366CEE9583,
	JsonArrayContract_tE4BEF4DAEFA2D8A388C986A0E81255CC2B7FBF83_CustomAttributesCacheGenerator_JsonArrayContract_set_CanDeserialize_mCA795402C7F20B1704011379FF99BFDBEA09946E,
	JsonArrayContract_tE4BEF4DAEFA2D8A388C986A0E81255CC2B7FBF83_CustomAttributesCacheGenerator_JsonArrayContract_get_HasParameterizedCreator_m4BC9037E6219B65657C06880A51DE9B22B2F38B7,
	JsonArrayContract_tE4BEF4DAEFA2D8A388C986A0E81255CC2B7FBF83_CustomAttributesCacheGenerator_JsonArrayContract_set_HasParameterizedCreator_m85CC98F0D51C11A1570956CC7319BE4C21AAA962,
	JsonContainerContract_tE83C0C4EDD22FEDE907E1D5F027CAEBDF1DFADAC_CustomAttributesCacheGenerator_JsonContainerContract_get_ItemConverter_mCC1FF5CD91BE1433DE49D71A5000D148A247D07B,
	JsonContainerContract_tE83C0C4EDD22FEDE907E1D5F027CAEBDF1DFADAC_CustomAttributesCacheGenerator_JsonContainerContract_set_ItemConverter_m22A233E3F1FEFEE00C494880802E72B9CE364301,
	JsonContainerContract_tE83C0C4EDD22FEDE907E1D5F027CAEBDF1DFADAC_CustomAttributesCacheGenerator_JsonContainerContract_get_ItemIsReference_mFF08D7B8117AD227F7B655F89690250790FCED61,
	JsonContainerContract_tE83C0C4EDD22FEDE907E1D5F027CAEBDF1DFADAC_CustomAttributesCacheGenerator_JsonContainerContract_set_ItemIsReference_m976F9ED94F787C37541FFE9CFCD2687AB7152CBF,
	JsonContainerContract_tE83C0C4EDD22FEDE907E1D5F027CAEBDF1DFADAC_CustomAttributesCacheGenerator_JsonContainerContract_get_ItemReferenceLoopHandling_m4497940C72F5F38E574A4D180D8D2CD919A812BC,
	JsonContainerContract_tE83C0C4EDD22FEDE907E1D5F027CAEBDF1DFADAC_CustomAttributesCacheGenerator_JsonContainerContract_set_ItemReferenceLoopHandling_m90E562BE972F1EF5478B7ED2703E27F5BE4ED27D,
	JsonContainerContract_tE83C0C4EDD22FEDE907E1D5F027CAEBDF1DFADAC_CustomAttributesCacheGenerator_JsonContainerContract_get_ItemTypeNameHandling_m805E6E8EB10B640579EF897F4A74E566D80F4498,
	JsonContainerContract_tE83C0C4EDD22FEDE907E1D5F027CAEBDF1DFADAC_CustomAttributesCacheGenerator_JsonContainerContract_set_ItemTypeNameHandling_mCA66B3AA082975A16A7DA94DB8F18722F5369447,
	JsonContract_tF3638831C408783F93E44D96C7C10A699A0F7F0D_CustomAttributesCacheGenerator_JsonContract_get_UnderlyingType_m274B4718678409B2511BBE941FF2D647CA533DC3,
	JsonContract_tF3638831C408783F93E44D96C7C10A699A0F7F0D_CustomAttributesCacheGenerator_JsonContract_set_UnderlyingType_m82F3DEB8DC8339C6CB32187651D87CE330DE13AE,
	JsonContract_tF3638831C408783F93E44D96C7C10A699A0F7F0D_CustomAttributesCacheGenerator_JsonContract_get_IsReference_m85AD9382BF6556FBB63A383D1ECA6BC6C5C369A6,
	JsonContract_tF3638831C408783F93E44D96C7C10A699A0F7F0D_CustomAttributesCacheGenerator_JsonContract_set_IsReference_m21803C0B2D772F7C3EED60332E2E2CA69D6D54F3,
	JsonContract_tF3638831C408783F93E44D96C7C10A699A0F7F0D_CustomAttributesCacheGenerator_JsonContract_get_Converter_m92C6064189D577AA5D08771BE755B3BFBCB297A9,
	JsonContract_tF3638831C408783F93E44D96C7C10A699A0F7F0D_CustomAttributesCacheGenerator_JsonContract_set_Converter_mA7452FEC775D9674F8163B299C151249DC7E544F,
	JsonContract_tF3638831C408783F93E44D96C7C10A699A0F7F0D_CustomAttributesCacheGenerator_JsonContract_get_InternalConverter_mB36C4060D4251DE1F51662B767D2779C40C65BCA,
	JsonContract_tF3638831C408783F93E44D96C7C10A699A0F7F0D_CustomAttributesCacheGenerator_JsonContract_set_InternalConverter_mAB2F562DCD689CC7E50B4BE84AEE73E2995D782F,
	JsonContract_tF3638831C408783F93E44D96C7C10A699A0F7F0D_CustomAttributesCacheGenerator_JsonContract_get_DefaultCreator_m8271898AFA4F8DBDC5A018F79FD4EC5450839B81,
	JsonContract_tF3638831C408783F93E44D96C7C10A699A0F7F0D_CustomAttributesCacheGenerator_JsonContract_set_DefaultCreator_m0D4E0BD379C8342A4BD6C6D73F19BE330526F825,
	JsonContract_tF3638831C408783F93E44D96C7C10A699A0F7F0D_CustomAttributesCacheGenerator_JsonContract_get_DefaultCreatorNonPublic_mFEDABA69CB6CA3FDE1FB163D1192C74307F2A15F,
	JsonContract_tF3638831C408783F93E44D96C7C10A699A0F7F0D_CustomAttributesCacheGenerator_JsonContract_set_DefaultCreatorNonPublic_mDCCE7A1E9DDED83BFFD22F33BC6E660F70CB4FEF,
	JsonDictionaryContract_t66334CCEFE57D3900FF8DB5AC954360DEF1E2F8C_CustomAttributesCacheGenerator_JsonDictionaryContract_get_DictionaryKeyResolver_m9FEC62269B460CBE840125B2C29F98F35FD18568,
	JsonDictionaryContract_t66334CCEFE57D3900FF8DB5AC954360DEF1E2F8C_CustomAttributesCacheGenerator_JsonDictionaryContract_set_DictionaryKeyResolver_m7DBA6B179284AF2B20F35EFD0A53D1C1A7A1338E,
	JsonDictionaryContract_t66334CCEFE57D3900FF8DB5AC954360DEF1E2F8C_CustomAttributesCacheGenerator_JsonDictionaryContract_get_DictionaryKeyType_m955BFAF792651E88B651BD2C619097726DCB76FD,
	JsonDictionaryContract_t66334CCEFE57D3900FF8DB5AC954360DEF1E2F8C_CustomAttributesCacheGenerator_JsonDictionaryContract_set_DictionaryKeyType_m6B1EDD598D5F7E159443731827F01450C44B11CB,
	JsonDictionaryContract_t66334CCEFE57D3900FF8DB5AC954360DEF1E2F8C_CustomAttributesCacheGenerator_JsonDictionaryContract_get_DictionaryValueType_m45B9DDEF68FCAAD1D2FD90391817AF79A2915922,
	JsonDictionaryContract_t66334CCEFE57D3900FF8DB5AC954360DEF1E2F8C_CustomAttributesCacheGenerator_JsonDictionaryContract_set_DictionaryValueType_m05D9BED20D4CA80764E423E027F66DB3A0818E18,
	JsonDictionaryContract_t66334CCEFE57D3900FF8DB5AC954360DEF1E2F8C_CustomAttributesCacheGenerator_JsonDictionaryContract_get_KeyContract_mC7BAD25BBB5DF24FA9FA1B704FB9153A1526EB8B,
	JsonDictionaryContract_t66334CCEFE57D3900FF8DB5AC954360DEF1E2F8C_CustomAttributesCacheGenerator_JsonDictionaryContract_set_KeyContract_m522C30EF20C09B50E617E24698E0BE1D84033EFC,
	JsonDictionaryContract_t66334CCEFE57D3900FF8DB5AC954360DEF1E2F8C_CustomAttributesCacheGenerator_JsonDictionaryContract_get_ShouldCreateWrapper_mD43CBCD1A26F23DFF9EF7F4069AA4BC99CE03BCC,
	JsonDictionaryContract_t66334CCEFE57D3900FF8DB5AC954360DEF1E2F8C_CustomAttributesCacheGenerator_JsonDictionaryContract_set_ShouldCreateWrapper_m81F5B13DB3FE9CC060B07F1943975289E930DBD0,
	JsonDictionaryContract_t66334CCEFE57D3900FF8DB5AC954360DEF1E2F8C_CustomAttributesCacheGenerator_JsonDictionaryContract_get_HasParameterizedCreator_m2AC31619D694866F47219F7228D781FDB7559361,
	JsonDictionaryContract_t66334CCEFE57D3900FF8DB5AC954360DEF1E2F8C_CustomAttributesCacheGenerator_JsonDictionaryContract_set_HasParameterizedCreator_m5C3555EBD3982F9FC072BDAF78014CC2C49F777D,
	JsonISerializableContract_t84536AF410FF3F89CC83FEFAB7530B8F51D347BF_CustomAttributesCacheGenerator_JsonISerializableContract_set_ISerializableCreator_m5295CF43688CAD573D840026F52A80256EF73A24,
	JsonObjectContract_tB36EC347E258A3A2B5A8560E8DC6D2A4BEF6DEB8_CustomAttributesCacheGenerator_JsonObjectContract_get_MemberSerialization_m927C496830ECEE969FEFCBD9015D2975B64EB7EA,
	JsonObjectContract_tB36EC347E258A3A2B5A8560E8DC6D2A4BEF6DEB8_CustomAttributesCacheGenerator_JsonObjectContract_set_MemberSerialization_m80C86B38A9A42F0A2D096B74CFC52E8EE250175E,
	JsonObjectContract_tB36EC347E258A3A2B5A8560E8DC6D2A4BEF6DEB8_CustomAttributesCacheGenerator_JsonObjectContract_get_ItemRequired_m75C6C4175B7C1473543857FFB985D37C39AD8C82,
	JsonObjectContract_tB36EC347E258A3A2B5A8560E8DC6D2A4BEF6DEB8_CustomAttributesCacheGenerator_JsonObjectContract_set_ItemRequired_m322E068F6060AB63F5C6B2E61DC986925C674E07,
	JsonObjectContract_tB36EC347E258A3A2B5A8560E8DC6D2A4BEF6DEB8_CustomAttributesCacheGenerator_JsonObjectContract_get_Properties_mD4B212D9A137993DE37F53402D769D0D905E3110,
	JsonObjectContract_tB36EC347E258A3A2B5A8560E8DC6D2A4BEF6DEB8_CustomAttributesCacheGenerator_JsonObjectContract_set_Properties_mDC67F335196A048B9DF12F878BAAE0B20328314B,
	JsonObjectContract_tB36EC347E258A3A2B5A8560E8DC6D2A4BEF6DEB8_CustomAttributesCacheGenerator_JsonObjectContract_get_ExtensionDataSetter_m2DFB59696B0D14849D0F4E443DD82AA5A76B243C,
	JsonObjectContract_tB36EC347E258A3A2B5A8560E8DC6D2A4BEF6DEB8_CustomAttributesCacheGenerator_JsonObjectContract_set_ExtensionDataSetter_m39A79B22B522DB922D27C30377BD082401A787B6,
	JsonObjectContract_tB36EC347E258A3A2B5A8560E8DC6D2A4BEF6DEB8_CustomAttributesCacheGenerator_JsonObjectContract_get_ExtensionDataGetter_m80664EEE79810B192D96E02FCD344B03EE7ACF35,
	JsonObjectContract_tB36EC347E258A3A2B5A8560E8DC6D2A4BEF6DEB8_CustomAttributesCacheGenerator_JsonObjectContract_set_ExtensionDataGetter_m52A1E838E07D96D828C5BB724E77BE2F17313980,
	JsonPrimitiveContract_tA595E8BA6018728333455EC3265935664D8EDC22_CustomAttributesCacheGenerator_JsonPrimitiveContract_get_TypeCode_mEED6CF3DA675CB2F244C3E02209F37FC8887C544,
	JsonPrimitiveContract_tA595E8BA6018728333455EC3265935664D8EDC22_CustomAttributesCacheGenerator_JsonPrimitiveContract_set_TypeCode_mD01C4FD88E81143FD4FE9446D08E10CBED5D16CB,
	JsonProperty_t41E840165F50D3CE4A2E9A48DD6064FCD59AAA0D_CustomAttributesCacheGenerator_JsonProperty_get_PropertyContract_m1A07AE508ED64BF7DAFD6B768CAB11E8F07AC218,
	JsonProperty_t41E840165F50D3CE4A2E9A48DD6064FCD59AAA0D_CustomAttributesCacheGenerator_JsonProperty_set_PropertyContract_mA51FB41ACB2877BDA0005FFB84D6724AD6269B3D,
	JsonProperty_t41E840165F50D3CE4A2E9A48DD6064FCD59AAA0D_CustomAttributesCacheGenerator_JsonProperty_get_DeclaringType_m0A64AF3ECCED18859E78E63E3A264AAB74586322,
	JsonProperty_t41E840165F50D3CE4A2E9A48DD6064FCD59AAA0D_CustomAttributesCacheGenerator_JsonProperty_set_DeclaringType_m4FA21E3ADA7905038A4B8410922A4D678DB627BD,
	JsonProperty_t41E840165F50D3CE4A2E9A48DD6064FCD59AAA0D_CustomAttributesCacheGenerator_JsonProperty_get_Order_mEF3E149DB2B59B8CA34317D3C09DFF703F31EF81,
	JsonProperty_t41E840165F50D3CE4A2E9A48DD6064FCD59AAA0D_CustomAttributesCacheGenerator_JsonProperty_set_Order_m360E9E66EB70C639663D3A29A37CD4FFB9217F56,
	JsonProperty_t41E840165F50D3CE4A2E9A48DD6064FCD59AAA0D_CustomAttributesCacheGenerator_JsonProperty_get_UnderlyingName_m8204307F227D4B3D9701133A47172F819B08219C,
	JsonProperty_t41E840165F50D3CE4A2E9A48DD6064FCD59AAA0D_CustomAttributesCacheGenerator_JsonProperty_set_UnderlyingName_mCD3B44976B4DFFD01B148B7D7A88AE92A71F6C2F,
	JsonProperty_t41E840165F50D3CE4A2E9A48DD6064FCD59AAA0D_CustomAttributesCacheGenerator_JsonProperty_get_ValueProvider_mB9CD0B24D58FCF1EE82DA3A3935973CD560C971B,
	JsonProperty_t41E840165F50D3CE4A2E9A48DD6064FCD59AAA0D_CustomAttributesCacheGenerator_JsonProperty_set_ValueProvider_m95563EC88EE32866DACCC6C2AE8F0FE656A5D847,
	JsonProperty_t41E840165F50D3CE4A2E9A48DD6064FCD59AAA0D_CustomAttributesCacheGenerator_JsonProperty_set_AttributeProvider_mC95FBFBE2AA39AD182245F28F860C0E77A2A7FD2,
	JsonProperty_t41E840165F50D3CE4A2E9A48DD6064FCD59AAA0D_CustomAttributesCacheGenerator_JsonProperty_get_Converter_mD78430E0A86FFCAD9077CA45A8515433C1A7ACAF,
	JsonProperty_t41E840165F50D3CE4A2E9A48DD6064FCD59AAA0D_CustomAttributesCacheGenerator_JsonProperty_set_Converter_m09958BDD926D75B7CCBA933BE95D63DF69DE2831,
	JsonProperty_t41E840165F50D3CE4A2E9A48DD6064FCD59AAA0D_CustomAttributesCacheGenerator_JsonProperty_get_MemberConverter_m4AAF8854081CD744699C3A0BAF9314E817FDDD43,
	JsonProperty_t41E840165F50D3CE4A2E9A48DD6064FCD59AAA0D_CustomAttributesCacheGenerator_JsonProperty_set_MemberConverter_m02E881B46A470612898913FEDB7568219E7254BB,
	JsonProperty_t41E840165F50D3CE4A2E9A48DD6064FCD59AAA0D_CustomAttributesCacheGenerator_JsonProperty_get_Ignored_m46548BB8982F27C3B6FA9874BB36BBBE93C5BE5C,
	JsonProperty_t41E840165F50D3CE4A2E9A48DD6064FCD59AAA0D_CustomAttributesCacheGenerator_JsonProperty_set_Ignored_m633FB01A6826E5E0A0AD5EADA49C0AAB4FC61943,
	JsonProperty_t41E840165F50D3CE4A2E9A48DD6064FCD59AAA0D_CustomAttributesCacheGenerator_JsonProperty_get_Readable_m7AEAE72E61CEE2D7204DF7DDE4BB46FD49ACF13A,
	JsonProperty_t41E840165F50D3CE4A2E9A48DD6064FCD59AAA0D_CustomAttributesCacheGenerator_JsonProperty_set_Readable_m200607F2CB3518230B19840B81318784938634C1,
	JsonProperty_t41E840165F50D3CE4A2E9A48DD6064FCD59AAA0D_CustomAttributesCacheGenerator_JsonProperty_get_Writable_mD0AD78FD1A02462591351FA144C3A3E3F3D71C04,
	JsonProperty_t41E840165F50D3CE4A2E9A48DD6064FCD59AAA0D_CustomAttributesCacheGenerator_JsonProperty_set_Writable_m1EAC194E3A9B4958ADBC14262F398F126E22669D,
	JsonProperty_t41E840165F50D3CE4A2E9A48DD6064FCD59AAA0D_CustomAttributesCacheGenerator_JsonProperty_get_HasMemberAttribute_m8D7C6BB1CB98AB72EFA491333E9BD9DD9A8EDD4B,
	JsonProperty_t41E840165F50D3CE4A2E9A48DD6064FCD59AAA0D_CustomAttributesCacheGenerator_JsonProperty_set_HasMemberAttribute_m39D8D4BE4DD54068759166CF360FF13F27479EC6,
	JsonProperty_t41E840165F50D3CE4A2E9A48DD6064FCD59AAA0D_CustomAttributesCacheGenerator_JsonProperty_get_IsReference_m153CA8FA5C547CC6B05661BE83264D4C965B9396,
	JsonProperty_t41E840165F50D3CE4A2E9A48DD6064FCD59AAA0D_CustomAttributesCacheGenerator_JsonProperty_set_IsReference_mB2D4152E154865A7D98A41608BB5DF115514B3C1,
	JsonProperty_t41E840165F50D3CE4A2E9A48DD6064FCD59AAA0D_CustomAttributesCacheGenerator_JsonProperty_get_NullValueHandling_m42B929FF14E1D403D8DCBEE4C71937234A81ACB1,
	JsonProperty_t41E840165F50D3CE4A2E9A48DD6064FCD59AAA0D_CustomAttributesCacheGenerator_JsonProperty_set_NullValueHandling_m0DFFF6AA9EB90290031B2A8AA34B5F4860650701,
	JsonProperty_t41E840165F50D3CE4A2E9A48DD6064FCD59AAA0D_CustomAttributesCacheGenerator_JsonProperty_get_DefaultValueHandling_m07D9A4D362F431188F9B92F069FA06522F8313AE,
	JsonProperty_t41E840165F50D3CE4A2E9A48DD6064FCD59AAA0D_CustomAttributesCacheGenerator_JsonProperty_set_DefaultValueHandling_mC9F3205408913D8B21FA525CCDAC7AD032E71EDA,
	JsonProperty_t41E840165F50D3CE4A2E9A48DD6064FCD59AAA0D_CustomAttributesCacheGenerator_JsonProperty_get_ReferenceLoopHandling_m36303DDEA2267BA0C5AEE6F04C31376769E24DA2,
	JsonProperty_t41E840165F50D3CE4A2E9A48DD6064FCD59AAA0D_CustomAttributesCacheGenerator_JsonProperty_set_ReferenceLoopHandling_m6CE09B4B487EDFDF5D4ADF5B9750278478C1728A,
	JsonProperty_t41E840165F50D3CE4A2E9A48DD6064FCD59AAA0D_CustomAttributesCacheGenerator_JsonProperty_get_ObjectCreationHandling_mFD14FC71400EAA2ABE2953F94D8ED12B9C5EA137,
	JsonProperty_t41E840165F50D3CE4A2E9A48DD6064FCD59AAA0D_CustomAttributesCacheGenerator_JsonProperty_set_ObjectCreationHandling_m049C8E802C5BED6F45FC9BC69F6FCF492F3AC05D,
	JsonProperty_t41E840165F50D3CE4A2E9A48DD6064FCD59AAA0D_CustomAttributesCacheGenerator_JsonProperty_get_TypeNameHandling_m8AEB537470EE0ED5BFE82E69FC9351EFFF39E20D,
	JsonProperty_t41E840165F50D3CE4A2E9A48DD6064FCD59AAA0D_CustomAttributesCacheGenerator_JsonProperty_set_TypeNameHandling_mFF34196E4C937D5CB8C7BFAC959C9EDBABABD42B,
	JsonProperty_t41E840165F50D3CE4A2E9A48DD6064FCD59AAA0D_CustomAttributesCacheGenerator_JsonProperty_get_ShouldSerialize_m3DEC6D4BDF9DD0467C9496E31D064F1D584131AD,
	JsonProperty_t41E840165F50D3CE4A2E9A48DD6064FCD59AAA0D_CustomAttributesCacheGenerator_JsonProperty_set_ShouldSerialize_m691B604D63F062A266B8AA0FA42CE22A52D1C80A,
	JsonProperty_t41E840165F50D3CE4A2E9A48DD6064FCD59AAA0D_CustomAttributesCacheGenerator_JsonProperty_get_ShouldDeserialize_m5E69820CB6EF8CBCA0B173519DB4B32C7D6B3F26,
	JsonProperty_t41E840165F50D3CE4A2E9A48DD6064FCD59AAA0D_CustomAttributesCacheGenerator_JsonProperty_get_GetIsSpecified_m2D32E7F92B46871C97992E41AB0BADCCDE3715A3,
	JsonProperty_t41E840165F50D3CE4A2E9A48DD6064FCD59AAA0D_CustomAttributesCacheGenerator_JsonProperty_set_GetIsSpecified_m69FEAC27F917FDA7B479399D6127C5A8BFDFD1A8,
	JsonProperty_t41E840165F50D3CE4A2E9A48DD6064FCD59AAA0D_CustomAttributesCacheGenerator_JsonProperty_get_SetIsSpecified_mB3A71DEAD82264AA63FB0A0E53E85DAB582AF220,
	JsonProperty_t41E840165F50D3CE4A2E9A48DD6064FCD59AAA0D_CustomAttributesCacheGenerator_JsonProperty_set_SetIsSpecified_m43AD00A6F292CE4CD5DD72CF33B5AEB186BC115E,
	JsonProperty_t41E840165F50D3CE4A2E9A48DD6064FCD59AAA0D_CustomAttributesCacheGenerator_JsonProperty_get_ItemConverter_mF43A1CB4574AACC026916EB46DD3665428AD9E9E,
	JsonProperty_t41E840165F50D3CE4A2E9A48DD6064FCD59AAA0D_CustomAttributesCacheGenerator_JsonProperty_set_ItemConverter_mE132EF0F991D7A67FF1CC4B190F3A20680A74484,
	JsonProperty_t41E840165F50D3CE4A2E9A48DD6064FCD59AAA0D_CustomAttributesCacheGenerator_JsonProperty_get_ItemIsReference_m4FB1049E629A8E0C7532E7F42B5784838B641FE0,
	JsonProperty_t41E840165F50D3CE4A2E9A48DD6064FCD59AAA0D_CustomAttributesCacheGenerator_JsonProperty_set_ItemIsReference_m5520B121F62448B77C3005291868E4A6C363F04B,
	JsonProperty_t41E840165F50D3CE4A2E9A48DD6064FCD59AAA0D_CustomAttributesCacheGenerator_JsonProperty_get_ItemTypeNameHandling_mE9229CB08007B7C54F5FB9E1E100741102577526,
	JsonProperty_t41E840165F50D3CE4A2E9A48DD6064FCD59AAA0D_CustomAttributesCacheGenerator_JsonProperty_set_ItemTypeNameHandling_mB26022D26C0E5DF718210F7F91150A8AC42E79A6,
	JsonProperty_t41E840165F50D3CE4A2E9A48DD6064FCD59AAA0D_CustomAttributesCacheGenerator_JsonProperty_get_ItemReferenceLoopHandling_mCF84F2423F3441D4FF3A55CA0EB91A3C13F7C20E,
	JsonProperty_t41E840165F50D3CE4A2E9A48DD6064FCD59AAA0D_CustomAttributesCacheGenerator_JsonProperty_set_ItemReferenceLoopHandling_mA482C2F7321AB96C932D7FD5A3C4FBF6283E9B16,
	NamingStrategy_t5D39FF458CF086DD0CE681796D85AEB675DF3568_CustomAttributesCacheGenerator_NamingStrategy_get_ProcessDictionaryKeys_mC1D4A8A7BDBC0305DBAE29FFEA3FFAB83177E249,
	NamingStrategy_t5D39FF458CF086DD0CE681796D85AEB675DF3568_CustomAttributesCacheGenerator_NamingStrategy_get_OverrideSpecifiedNames_m45FEC93C0AC20A53AEC9AD731A4D9B276AF26AF4,
	MethodCall_2_tA79249CAB2CBC748AB9ACD584E1DD90058556A97_CustomAttributesCacheGenerator_MethodCall_2_Invoke_m4563B61D6ADB928FC200C74AE5FCFAE5CEBDCB0B____args1,
	ReflectionObject_tC343774B9EE64C90DF1DBB77EEC193C4C9DD7361_CustomAttributesCacheGenerator_ReflectionObject_Create_mF57E2BF8F217D0B124CE7B0B5C4A80E173279EBA____memberNames1,
	ReflectionObject_tC343774B9EE64C90DF1DBB77EEC193C4C9DD7361_CustomAttributesCacheGenerator_ReflectionObject_Create_m3E13A4F93F3B71B995104BD0FEBB204830B2545D____memberNames2,
	StringUtils_t3EA5628D6D6700734183C5E2BCFB2E99847AFD49_CustomAttributesCacheGenerator_StringUtils_FormatWith_mF72FDDA3EB515E398CA2D73E829FC7F8AD4F1D2B____args2,
	ObjectConstructor_1_t527F32993C900EBBF0FDDEEFF0056620D5DA4751_CustomAttributesCacheGenerator_ObjectConstructor_1_Invoke_mA5F3ED8DF054AD93D77B657A673E2D065CE4539B____args0,
	DefaultContractResolver_t191C29E332665EFC1B0B1F1082BEC4B7DE866297_CustomAttributesCacheGenerator_DefaultContractResolver_t191C29E332665EFC1B0B1F1082BEC4B7DE866297____DefaultMembersSearchFlags_PropertyInfo,
	JsonObjectContract_tB36EC347E258A3A2B5A8560E8DC6D2A4BEF6DEB8_CustomAttributesCacheGenerator_JsonObjectContract_tB36EC347E258A3A2B5A8560E8DC6D2A4BEF6DEB8____OverrideConstructor_PropertyInfo,
	JsonObjectContract_tB36EC347E258A3A2B5A8560E8DC6D2A4BEF6DEB8_CustomAttributesCacheGenerator_JsonObjectContract_tB36EC347E258A3A2B5A8560E8DC6D2A4BEF6DEB8____ParametrizedConstructor_PropertyInfo,
	Newtonsoft_Json_CustomAttributesCacheGenerator,
};
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void RuntimeCompatibilityAttribute_set_WrapNonExceptionThrows_m8562196F90F3EBCEC23B5708EE0332842883C490_inline (RuntimeCompatibilityAttribute_tFF99AB2963098F9CBCD47A20D9FD3D51C17C1C80 * __this, bool ___value0, const RuntimeMethod* method)
{
	{
		bool L_0 = ___value0;
		__this->set_m_wrapNonExceptionThrows_0(L_0);
		return;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void AttributeUsageAttribute_set_AllowMultiple_mF412CDAFFE16D056721EF81A1EC04ACE63612055_inline (AttributeUsageAttribute_tBB0BAAA82036E6FCDD80A688BBD039F6FFD8EA1C * __this, bool ___value0, const RuntimeMethod* method)
{
	{
		bool L_0 = ___value0;
		__this->set_m_allowMultiple_1(L_0);
		return;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void AttributeUsageAttribute_set_Inherited_m56105980C36CB71AECD398C6077739BDFD2085E0_inline (AttributeUsageAttribute_tBB0BAAA82036E6FCDD80A688BBD039F6FFD8EA1C * __this, bool ___value0, const RuntimeMethod* method)
{
	{
		bool L_0 = ___value0;
		__this->set_m_inherited_2(L_0);
		return;
	}
}
