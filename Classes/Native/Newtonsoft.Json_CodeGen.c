﻿#include "pch-c.h"
#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include "codegen/il2cpp-codegen-metadata.h"





// 0x00000001 T[] Newtonsoft.Json.IArrayPool`1::Rent(System.Int32)
// 0x00000002 System.Void Newtonsoft.Json.IArrayPool`1::Return(T[])
// 0x00000003 System.Boolean Newtonsoft.Json.IJsonLineInfo::HasLineInfo()
// 0x00000004 System.Int32 Newtonsoft.Json.IJsonLineInfo::get_LineNumber()
// 0x00000005 System.Int32 Newtonsoft.Json.IJsonLineInfo::get_LinePosition()
// 0x00000006 System.Type Newtonsoft.Json.JsonContainerAttribute::get_ItemConverterType()
extern void JsonContainerAttribute_get_ItemConverterType_mE0959B267593FB7E6C69CC99954A1AB62C82551D (void);
// 0x00000007 System.Object[] Newtonsoft.Json.JsonContainerAttribute::get_ItemConverterParameters()
extern void JsonContainerAttribute_get_ItemConverterParameters_mC22A7EB75C02CB7460E4D8E1DF4E3685C04413C5 (void);
// 0x00000008 System.Type Newtonsoft.Json.JsonContainerAttribute::get_NamingStrategyType()
extern void JsonContainerAttribute_get_NamingStrategyType_mB10ABE6F04C12E460ED8A6D77A977EF21C1AFBDA (void);
// 0x00000009 System.Object[] Newtonsoft.Json.JsonContainerAttribute::get_NamingStrategyParameters()
extern void JsonContainerAttribute_get_NamingStrategyParameters_m8239FB6299983482C92247CACDEA26EDDB335C53 (void);
// 0x0000000A Newtonsoft.Json.Serialization.NamingStrategy Newtonsoft.Json.JsonContainerAttribute::get_NamingStrategyInstance()
extern void JsonContainerAttribute_get_NamingStrategyInstance_mCB5C826F2FDD2091632B0FD8A25DDECF528C513C (void);
// 0x0000000B System.Void Newtonsoft.Json.JsonContainerAttribute::set_NamingStrategyInstance(Newtonsoft.Json.Serialization.NamingStrategy)
extern void JsonContainerAttribute_set_NamingStrategyInstance_mDF8D8278251CF937FC050DF7DC9B91C8F27BD5E4 (void);
// 0x0000000C System.Func`1<Newtonsoft.Json.JsonSerializerSettings> Newtonsoft.Json.JsonConvert::get_DefaultSettings()
extern void JsonConvert_get_DefaultSettings_m214B88ECC58290FE283050B8A4C2E08F1755C83A (void);
// 0x0000000D System.String Newtonsoft.Json.JsonConvert::ToString(System.Boolean)
extern void JsonConvert_ToString_mD0E99B0E39DDDC716CA34FCEDBD645ACBFB90E4C (void);
// 0x0000000E System.String Newtonsoft.Json.JsonConvert::ToString(System.Char)
extern void JsonConvert_ToString_mAB220E124F4B6472230B4E092F46087E03246ED3 (void);
// 0x0000000F System.String Newtonsoft.Json.JsonConvert::ToString(System.Single,Newtonsoft.Json.FloatFormatHandling,System.Char,System.Boolean)
extern void JsonConvert_ToString_m99422A0B86718ECAD838E5A955774E0B0FDD6B33 (void);
// 0x00000010 System.String Newtonsoft.Json.JsonConvert::EnsureFloatFormat(System.Double,System.String,Newtonsoft.Json.FloatFormatHandling,System.Char,System.Boolean)
extern void JsonConvert_EnsureFloatFormat_m7DE595A6055A8F778FF4B1C4CB13C563F1672EC7 (void);
// 0x00000011 System.String Newtonsoft.Json.JsonConvert::ToString(System.Double,Newtonsoft.Json.FloatFormatHandling,System.Char,System.Boolean)
extern void JsonConvert_ToString_m6E4BA77374791993DC635CF31DF61855CAB8B1D5 (void);
// 0x00000012 System.String Newtonsoft.Json.JsonConvert::EnsureDecimalPlace(System.Double,System.String)
extern void JsonConvert_EnsureDecimalPlace_m92BA4ECF36C849E5C8EC554F2D6F6AC8CF86DA9F (void);
// 0x00000013 System.String Newtonsoft.Json.JsonConvert::EnsureDecimalPlace(System.String)
extern void JsonConvert_EnsureDecimalPlace_mDF59953E3548280017BB06611885F36BE50F570A (void);
// 0x00000014 System.String Newtonsoft.Json.JsonConvert::ToString(System.Decimal)
extern void JsonConvert_ToString_mB9EEC617DD77B39CCD0497B93F908D69F5CC5F74 (void);
// 0x00000015 System.String Newtonsoft.Json.JsonConvert::ToString(System.String)
extern void JsonConvert_ToString_mB521F78145736C931F7A69BD0134240267AB47C6 (void);
// 0x00000016 System.String Newtonsoft.Json.JsonConvert::ToString(System.String,System.Char)
extern void JsonConvert_ToString_mFBE8316A78A5A5C88FE309D5977BB6F185F0B412 (void);
// 0x00000017 System.String Newtonsoft.Json.JsonConvert::ToString(System.String,System.Char,Newtonsoft.Json.StringEscapeHandling)
extern void JsonConvert_ToString_mF8D34F71CA5E49C6051840E9A4B8B304FC1B43DD (void);
// 0x00000018 System.String Newtonsoft.Json.JsonConvert::SerializeObject(System.Object)
extern void JsonConvert_SerializeObject_m34096C3C3C8C68316091065A81BFD4D09DBB010E (void);
// 0x00000019 System.String Newtonsoft.Json.JsonConvert::SerializeObject(System.Object,System.Type,Newtonsoft.Json.JsonSerializerSettings)
extern void JsonConvert_SerializeObject_mFF58B0EEAB5078F452B40E34EA1B6F9D5FC6ED3E (void);
// 0x0000001A System.String Newtonsoft.Json.JsonConvert::SerializeObjectInternal(System.Object,System.Type,Newtonsoft.Json.JsonSerializer)
extern void JsonConvert_SerializeObjectInternal_m310265D41E19BA0B4541BCA402B9964614AD8096 (void);
// 0x0000001B T Newtonsoft.Json.JsonConvert::DeserializeObject(System.String)
// 0x0000001C T Newtonsoft.Json.JsonConvert::DeserializeObject(System.String,Newtonsoft.Json.JsonSerializerSettings)
// 0x0000001D System.Object Newtonsoft.Json.JsonConvert::DeserializeObject(System.String,System.Type,Newtonsoft.Json.JsonSerializerSettings)
extern void JsonConvert_DeserializeObject_mB54156646D8625F24D192C6B4CEFD186913EB39A (void);
// 0x0000001E System.Void Newtonsoft.Json.JsonConvert::.cctor()
extern void JsonConvert__cctor_m9BD28BF24255CABF6422B599CF9D9302F40A669F (void);
// 0x0000001F System.Void Newtonsoft.Json.JsonConverter::WriteJson(Newtonsoft.Json.JsonWriter,System.Object,Newtonsoft.Json.JsonSerializer)
// 0x00000020 System.Object Newtonsoft.Json.JsonConverter::ReadJson(Newtonsoft.Json.JsonReader,System.Type,System.Object,Newtonsoft.Json.JsonSerializer)
// 0x00000021 System.Boolean Newtonsoft.Json.JsonConverter::CanConvert(System.Type)
// 0x00000022 System.Boolean Newtonsoft.Json.JsonConverter::get_CanRead()
extern void JsonConverter_get_CanRead_mDAF858A38341BEDCEF64D1A0C63456A12E9B7EFE (void);
// 0x00000023 System.Boolean Newtonsoft.Json.JsonConverter::get_CanWrite()
extern void JsonConverter_get_CanWrite_m25E86A75449F0DF79462DC473771CCF76A9CF5D6 (void);
// 0x00000024 System.Void Newtonsoft.Json.JsonConverter::.ctor()
extern void JsonConverter__ctor_mBE413F963C1E1D20875BCFE94DB01E09836EA97B (void);
// 0x00000025 System.Type Newtonsoft.Json.JsonConverterAttribute::get_ConverterType()
extern void JsonConverterAttribute_get_ConverterType_mF585DD254EC1CCC3F79C615264FA86EACA9A87E0 (void);
// 0x00000026 System.Object[] Newtonsoft.Json.JsonConverterAttribute::get_ConverterParameters()
extern void JsonConverterAttribute_get_ConverterParameters_m17E8F4F8CD589CD1E5C2D140C98F8B8863ADBF68 (void);
// 0x00000027 System.Void Newtonsoft.Json.JsonConverterCollection::.ctor()
extern void JsonConverterCollection__ctor_m9107FABC0C640CCC2319C8EA80396DDA591338D1 (void);
// 0x00000028 System.Void Newtonsoft.Json.JsonException::.ctor()
extern void JsonException__ctor_m5BAA60E6F46A629602A17098B65E7C5BA1EC4525 (void);
// 0x00000029 System.Void Newtonsoft.Json.JsonException::.ctor(System.String)
extern void JsonException__ctor_mF60EC8D80BCE3097C5F731299209E8C4FD800D32 (void);
// 0x0000002A System.Void Newtonsoft.Json.JsonException::.ctor(System.String,System.Exception)
extern void JsonException__ctor_mEEBCB87D800D5FF413A4B0B17783DE362C8C8B2F (void);
// 0x0000002B System.Void Newtonsoft.Json.JsonException::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern void JsonException__ctor_m8F5B00585F8045EA6F9F026E16EC63DDC86AFA9E (void);
// 0x0000002C System.Boolean Newtonsoft.Json.JsonExtensionDataAttribute::get_WriteData()
extern void JsonExtensionDataAttribute_get_WriteData_m0718786B6D6C25112594F098A8CF31A8C90DC24A (void);
// 0x0000002D System.Boolean Newtonsoft.Json.JsonExtensionDataAttribute::get_ReadData()
extern void JsonExtensionDataAttribute_get_ReadData_mBF0CDE7323AB877197F0EC890140CA3CD80BA36F (void);
// 0x0000002E System.Void Newtonsoft.Json.JsonIgnoreAttribute::.ctor()
extern void JsonIgnoreAttribute__ctor_mC097DBAA814AAB0C93437290A1185B6285C73223 (void);
// 0x0000002F Newtonsoft.Json.MemberSerialization Newtonsoft.Json.JsonObjectAttribute::get_MemberSerialization()
extern void JsonObjectAttribute_get_MemberSerialization_mFA25361BA866DD5B146D4875AD57AA6C1BD044D6 (void);
// 0x00000030 System.Void Newtonsoft.Json.JsonPosition::.ctor(Newtonsoft.Json.JsonContainerType)
extern void JsonPosition__ctor_m3C4DB92D84AE311EE082E3D1C35558CF05969C28 (void);
// 0x00000031 System.Int32 Newtonsoft.Json.JsonPosition::CalculateLength()
extern void JsonPosition_CalculateLength_mF22E55ABE3DE00539186EC9136AF9FC493E0CCBD (void);
// 0x00000032 System.Void Newtonsoft.Json.JsonPosition::WriteTo(System.Text.StringBuilder)
extern void JsonPosition_WriteTo_mEFF7AE829F518C06937587D13B92A88F35FCC73D (void);
// 0x00000033 System.Boolean Newtonsoft.Json.JsonPosition::TypeHasIndex(Newtonsoft.Json.JsonContainerType)
extern void JsonPosition_TypeHasIndex_m4A102E3F53126A23B7200E21A8A391EA1971AD33 (void);
// 0x00000034 System.String Newtonsoft.Json.JsonPosition::BuildPath(System.Collections.Generic.List`1<Newtonsoft.Json.JsonPosition>,System.Nullable`1<Newtonsoft.Json.JsonPosition>)
extern void JsonPosition_BuildPath_m40CD2C855E89F06AE22E0F85895BC6B3CB2450F4 (void);
// 0x00000035 System.String Newtonsoft.Json.JsonPosition::FormatMessage(Newtonsoft.Json.IJsonLineInfo,System.String,System.String)
extern void JsonPosition_FormatMessage_m1D34B70753BD15131332960EB353A37B1F1E62D7 (void);
// 0x00000036 System.Void Newtonsoft.Json.JsonPosition::.cctor()
extern void JsonPosition__cctor_mCE4C541548C423ADDFC903FF2AC682996B9B4138 (void);
// 0x00000037 System.Type Newtonsoft.Json.JsonPropertyAttribute::get_ItemConverterType()
extern void JsonPropertyAttribute_get_ItemConverterType_mC4581A062D9DCF64DB3CE11BE06771A9347D9EB6 (void);
// 0x00000038 System.Object[] Newtonsoft.Json.JsonPropertyAttribute::get_ItemConverterParameters()
extern void JsonPropertyAttribute_get_ItemConverterParameters_m3845C44A0AD6345D2E53707EBCDA7DEA54FC8CC8 (void);
// 0x00000039 System.Type Newtonsoft.Json.JsonPropertyAttribute::get_NamingStrategyType()
extern void JsonPropertyAttribute_get_NamingStrategyType_m9BD354DEDD5126AB21DA4D7C2A49A4396014BDE8 (void);
// 0x0000003A System.Object[] Newtonsoft.Json.JsonPropertyAttribute::get_NamingStrategyParameters()
extern void JsonPropertyAttribute_get_NamingStrategyParameters_mD0BC1CD962875A82871498CD866B6D840BF93196 (void);
// 0x0000003B System.String Newtonsoft.Json.JsonPropertyAttribute::get_PropertyName()
extern void JsonPropertyAttribute_get_PropertyName_m23FFB79CC575178480990AD5FB0FD1152A912D99 (void);
// 0x0000003C Newtonsoft.Json.JsonReader/State Newtonsoft.Json.JsonReader::get_CurrentState()
extern void JsonReader_get_CurrentState_m4E1A189AEAC68E9BC1A4C4424E87FFB208BDCFBC (void);
// 0x0000003D System.Boolean Newtonsoft.Json.JsonReader::get_CloseInput()
extern void JsonReader_get_CloseInput_m87EB3CF7C34E7FBA9BA84B812CC4CAE0B9FDE974 (void);
// 0x0000003E System.Void Newtonsoft.Json.JsonReader::set_CloseInput(System.Boolean)
extern void JsonReader_set_CloseInput_mE443BF3B39BAE245D6D22180962F4F3B92C95EFD (void);
// 0x0000003F System.Boolean Newtonsoft.Json.JsonReader::get_SupportMultipleContent()
extern void JsonReader_get_SupportMultipleContent_mB43563289F840AA64EE3133642CFC1D3340B2912 (void);
// 0x00000040 Newtonsoft.Json.DateTimeZoneHandling Newtonsoft.Json.JsonReader::get_DateTimeZoneHandling()
extern void JsonReader_get_DateTimeZoneHandling_mBB69C4CE703B559168E1507D92DCC376C92A85F4 (void);
// 0x00000041 System.Void Newtonsoft.Json.JsonReader::set_DateTimeZoneHandling(Newtonsoft.Json.DateTimeZoneHandling)
extern void JsonReader_set_DateTimeZoneHandling_m0E5237BB4D2596DD809E68DF74D1E3B4BAFE2C90 (void);
// 0x00000042 Newtonsoft.Json.DateParseHandling Newtonsoft.Json.JsonReader::get_DateParseHandling()
extern void JsonReader_get_DateParseHandling_m63AFEAFCBB887D45584BF4545F50BAE69B76C1A2 (void);
// 0x00000043 System.Void Newtonsoft.Json.JsonReader::set_DateParseHandling(Newtonsoft.Json.DateParseHandling)
extern void JsonReader_set_DateParseHandling_mD2CB16410575ABE46D12548EBFD78F5729E5A799 (void);
// 0x00000044 Newtonsoft.Json.FloatParseHandling Newtonsoft.Json.JsonReader::get_FloatParseHandling()
extern void JsonReader_get_FloatParseHandling_mD99A882ED5135C5DF30ED4A75D8F421189E45806 (void);
// 0x00000045 System.Void Newtonsoft.Json.JsonReader::set_FloatParseHandling(Newtonsoft.Json.FloatParseHandling)
extern void JsonReader_set_FloatParseHandling_m6B04F861A2FDDEAFE7A2188086B1ADDC1905062E (void);
// 0x00000046 System.String Newtonsoft.Json.JsonReader::get_DateFormatString()
extern void JsonReader_get_DateFormatString_m7F87932F9E5D2B74F3B54789A28381E9A9840B2E (void);
// 0x00000047 System.Void Newtonsoft.Json.JsonReader::set_DateFormatString(System.String)
extern void JsonReader_set_DateFormatString_m391A0D253EFB555727DD530CD06D53CCB9FFA2E1 (void);
// 0x00000048 System.Nullable`1<System.Int32> Newtonsoft.Json.JsonReader::get_MaxDepth()
extern void JsonReader_get_MaxDepth_mBD37C2D7F28221DCECBE06F07EA6A7FFC72682E6 (void);
// 0x00000049 System.Void Newtonsoft.Json.JsonReader::set_MaxDepth(System.Nullable`1<System.Int32>)
extern void JsonReader_set_MaxDepth_m89E61E3B0E0CF20C1FF7F41EE335CF7163ACC05E (void);
// 0x0000004A Newtonsoft.Json.JsonToken Newtonsoft.Json.JsonReader::get_TokenType()
extern void JsonReader_get_TokenType_m7AD71FFD4018980CD8D4326ADC9E41CEF4349AC7 (void);
// 0x0000004B System.Object Newtonsoft.Json.JsonReader::get_Value()
extern void JsonReader_get_Value_m45A724E8BB32073B46D12BF8C6307379ADF7CCBC (void);
// 0x0000004C System.Type Newtonsoft.Json.JsonReader::get_ValueType()
extern void JsonReader_get_ValueType_m9BFCDA53FB3DCC92EFB30FFDA930762035277DCB (void);
// 0x0000004D System.Int32 Newtonsoft.Json.JsonReader::get_Depth()
extern void JsonReader_get_Depth_m7DBC14E5C00031BF8FCC6FC196F8B8EB8C107031 (void);
// 0x0000004E System.String Newtonsoft.Json.JsonReader::get_Path()
extern void JsonReader_get_Path_m691C59CC56257EAA614A55F1486810F2E6018715 (void);
// 0x0000004F System.Globalization.CultureInfo Newtonsoft.Json.JsonReader::get_Culture()
extern void JsonReader_get_Culture_m6D0CD10FAFA5D1F263B70A5B8E1989C4CBA235E4 (void);
// 0x00000050 System.Void Newtonsoft.Json.JsonReader::set_Culture(System.Globalization.CultureInfo)
extern void JsonReader_set_Culture_mD14841014D4DBFBCBCC1CF76D08D5BBB97251CB6 (void);
// 0x00000051 Newtonsoft.Json.JsonPosition Newtonsoft.Json.JsonReader::GetPosition(System.Int32)
extern void JsonReader_GetPosition_mD19B14381DDEB6225D51F2451BD01655F4ED6963 (void);
// 0x00000052 System.Void Newtonsoft.Json.JsonReader::.ctor()
extern void JsonReader__ctor_m538C842DADDC1A2123EC0A967DAE5B2D238B4FB9 (void);
// 0x00000053 System.Void Newtonsoft.Json.JsonReader::Push(Newtonsoft.Json.JsonContainerType)
extern void JsonReader_Push_m9EF8950F853F0A18E15AF98AA17808A8844BE974 (void);
// 0x00000054 Newtonsoft.Json.JsonContainerType Newtonsoft.Json.JsonReader::Pop()
extern void JsonReader_Pop_m456A183649D027C8568F312336F21B5E97B097B1 (void);
// 0x00000055 Newtonsoft.Json.JsonContainerType Newtonsoft.Json.JsonReader::Peek()
extern void JsonReader_Peek_m5B31B77B96E6C1C903DB7C1F26AF5E50CA5B71F5 (void);
// 0x00000056 System.Boolean Newtonsoft.Json.JsonReader::Read()
// 0x00000057 System.Nullable`1<System.Int32> Newtonsoft.Json.JsonReader::ReadAsInt32()
extern void JsonReader_ReadAsInt32_m5145898AE873C4309FE67FAD07A61A28DBD3E5EF (void);
// 0x00000058 System.Nullable`1<System.Int32> Newtonsoft.Json.JsonReader::ReadInt32String(System.String)
extern void JsonReader_ReadInt32String_mE034741F9523F88212BD02B315B1F4706640E0B0 (void);
// 0x00000059 System.String Newtonsoft.Json.JsonReader::ReadAsString()
extern void JsonReader_ReadAsString_mEED3E79A7265120F15FE9A923A4B856597FB6E95 (void);
// 0x0000005A System.Byte[] Newtonsoft.Json.JsonReader::ReadAsBytes()
extern void JsonReader_ReadAsBytes_mA9850F1C70F0E52C074368A41A923DF8D87EA248 (void);
// 0x0000005B System.Byte[] Newtonsoft.Json.JsonReader::ReadArrayIntoByteArray()
extern void JsonReader_ReadArrayIntoByteArray_m7A2400F8D6CF59393BF52F49CF3BF8B7EE8033C3 (void);
// 0x0000005C System.Nullable`1<System.Double> Newtonsoft.Json.JsonReader::ReadAsDouble()
extern void JsonReader_ReadAsDouble_m52807B15F5E3B8BE1592224C27FC3F2CF475D635 (void);
// 0x0000005D System.Nullable`1<System.Double> Newtonsoft.Json.JsonReader::ReadDoubleString(System.String)
extern void JsonReader_ReadDoubleString_m0EC90F773B5C56971311C6D049835D1B686DD5D4 (void);
// 0x0000005E System.Nullable`1<System.Boolean> Newtonsoft.Json.JsonReader::ReadAsBoolean()
extern void JsonReader_ReadAsBoolean_m54007931B6EDB0B90463F77D2E67E65BDFD147BB (void);
// 0x0000005F System.Nullable`1<System.Boolean> Newtonsoft.Json.JsonReader::ReadBooleanString(System.String)
extern void JsonReader_ReadBooleanString_m1F7D6439D9B81A4A2F47C9A64B0D7E4D57936B5C (void);
// 0x00000060 System.Nullable`1<System.Decimal> Newtonsoft.Json.JsonReader::ReadAsDecimal()
extern void JsonReader_ReadAsDecimal_mE3B0E8DB3DB6BEA704B527E0B1E8A582F652A1D8 (void);
// 0x00000061 System.Nullable`1<System.Decimal> Newtonsoft.Json.JsonReader::ReadDecimalString(System.String)
extern void JsonReader_ReadDecimalString_mC146603458D7A61DA56F11B03AEFD377AD076414 (void);
// 0x00000062 System.Nullable`1<System.DateTime> Newtonsoft.Json.JsonReader::ReadAsDateTime()
extern void JsonReader_ReadAsDateTime_m85AFE4D30022400C2F86DDE61CF6230C31E8C9B8 (void);
// 0x00000063 System.Nullable`1<System.DateTime> Newtonsoft.Json.JsonReader::ReadDateTimeString(System.String)
extern void JsonReader_ReadDateTimeString_mABFB2A6AA0E82A267DDA0BE8612E7B57B9FDDB90 (void);
// 0x00000064 System.Nullable`1<System.DateTimeOffset> Newtonsoft.Json.JsonReader::ReadAsDateTimeOffset()
extern void JsonReader_ReadAsDateTimeOffset_m9C56CF78B5C18254645F0EDA346379720BE1A8C5 (void);
// 0x00000065 System.Nullable`1<System.DateTimeOffset> Newtonsoft.Json.JsonReader::ReadDateTimeOffsetString(System.String)
extern void JsonReader_ReadDateTimeOffsetString_m05BC061E7926F996B22B4CF3E77293C3084B174E (void);
// 0x00000066 System.Void Newtonsoft.Json.JsonReader::ReaderReadAndAssert()
extern void JsonReader_ReaderReadAndAssert_mA814836B53ACFDF128319498FC3C9D976F3CD613 (void);
// 0x00000067 Newtonsoft.Json.JsonReaderException Newtonsoft.Json.JsonReader::CreateUnexpectedEndException()
extern void JsonReader_CreateUnexpectedEndException_mC86C2D5D04C011EF9A3DF77CCC35F43EC3403084 (void);
// 0x00000068 System.Void Newtonsoft.Json.JsonReader::ReadIntoWrappedTypeObject()
extern void JsonReader_ReadIntoWrappedTypeObject_m63038E486E9A183C7234AEBF07FE634DEC08839D (void);
// 0x00000069 System.Void Newtonsoft.Json.JsonReader::Skip()
extern void JsonReader_Skip_mC8CB495BD0C5108D4AFFD1D660CDFCB659A25591 (void);
// 0x0000006A System.Void Newtonsoft.Json.JsonReader::SetToken(Newtonsoft.Json.JsonToken)
extern void JsonReader_SetToken_mD5DBB8280CBF021243DFBB6CFCCD3A6A8C0F17E9 (void);
// 0x0000006B System.Void Newtonsoft.Json.JsonReader::SetToken(Newtonsoft.Json.JsonToken,System.Object)
extern void JsonReader_SetToken_m4CA8A55D6760B8E56CF0B9303FCA3D25541AAF84 (void);
// 0x0000006C System.Void Newtonsoft.Json.JsonReader::SetToken(Newtonsoft.Json.JsonToken,System.Object,System.Boolean)
extern void JsonReader_SetToken_m9755E98D6799629F9662BB2252E1D129102DD8CF (void);
// 0x0000006D System.Void Newtonsoft.Json.JsonReader::SetPostValueState(System.Boolean)
extern void JsonReader_SetPostValueState_m454C9C82F81D2C182B99CDB1075CE3433E661A98 (void);
// 0x0000006E System.Void Newtonsoft.Json.JsonReader::UpdateScopeWithFinishedValue()
extern void JsonReader_UpdateScopeWithFinishedValue_mF6126F4AF88B9EF97B93846005AD1141DAC9A73F (void);
// 0x0000006F System.Void Newtonsoft.Json.JsonReader::ValidateEnd(Newtonsoft.Json.JsonToken)
extern void JsonReader_ValidateEnd_m1A4EAAC2322EB0DC4E5F1A0FBBEAEA7E5CFAD687 (void);
// 0x00000070 System.Void Newtonsoft.Json.JsonReader::SetStateBasedOnCurrent()
extern void JsonReader_SetStateBasedOnCurrent_mD27DD404ECA14B79FE0785CE0DCF22E321FA4D4A (void);
// 0x00000071 System.Void Newtonsoft.Json.JsonReader::SetFinished()
extern void JsonReader_SetFinished_m787790578196C7BD49FBFF2C056DA5805CA85801 (void);
// 0x00000072 Newtonsoft.Json.JsonContainerType Newtonsoft.Json.JsonReader::GetTypeForCloseToken(Newtonsoft.Json.JsonToken)
extern void JsonReader_GetTypeForCloseToken_m3F40F2BC543DAC646D8C6458DC4038D0B8387C8D (void);
// 0x00000073 System.Void Newtonsoft.Json.JsonReader::System.IDisposable.Dispose()
extern void JsonReader_System_IDisposable_Dispose_m8C100C048698A1E9BDC3FD290DCF9011E9DC3DDB (void);
// 0x00000074 System.Void Newtonsoft.Json.JsonReader::Dispose(System.Boolean)
extern void JsonReader_Dispose_m931F4D5CEFFFE7A6DAA4BDADDF55EA3D8267E582 (void);
// 0x00000075 System.Void Newtonsoft.Json.JsonReader::Close()
extern void JsonReader_Close_mAA1C1B8767BCAD23B09D66C35C3808B7CAA40AA0 (void);
// 0x00000076 System.Void Newtonsoft.Json.JsonReader::ReadAndAssert()
extern void JsonReader_ReadAndAssert_mA82EE782D85622671FEC7696CDB268A9864B45D3 (void);
// 0x00000077 System.Boolean Newtonsoft.Json.JsonReader::ReadAndMoveToContent()
extern void JsonReader_ReadAndMoveToContent_mD9883A03037CBE7F89B7F261B713D61AE9865AF2 (void);
// 0x00000078 System.Boolean Newtonsoft.Json.JsonReader::MoveToContent()
extern void JsonReader_MoveToContent_m3DBD33F19BD356A29544FD39C6BFE643DAB035FA (void);
// 0x00000079 Newtonsoft.Json.JsonToken Newtonsoft.Json.JsonReader::GetContentToken()
extern void JsonReader_GetContentToken_m597AF0481D4A5EFFB8F1875EC2AD7FE490C2E80E (void);
// 0x0000007A System.Void Newtonsoft.Json.JsonReaderException::set_LineNumber(System.Int32)
extern void JsonReaderException_set_LineNumber_m9F168878C1D77923580E25AB5072BB89A2A1FE93 (void);
// 0x0000007B System.Void Newtonsoft.Json.JsonReaderException::set_LinePosition(System.Int32)
extern void JsonReaderException_set_LinePosition_mCCABCCE6749D6A36BF93E146FEBCA1E539F83FDA (void);
// 0x0000007C System.Void Newtonsoft.Json.JsonReaderException::set_Path(System.String)
extern void JsonReaderException_set_Path_mC99BD3BB99E4F85727B345B5CCDA60E43AB30AE0 (void);
// 0x0000007D System.Void Newtonsoft.Json.JsonReaderException::.ctor()
extern void JsonReaderException__ctor_m0CB8C02A7D13588E2F1B35D181AD5EB5E14F808C (void);
// 0x0000007E System.Void Newtonsoft.Json.JsonReaderException::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern void JsonReaderException__ctor_mD75FFFBE2EFF11240A34CCED588B884EB4F41E05 (void);
// 0x0000007F System.Void Newtonsoft.Json.JsonReaderException::.ctor(System.String,System.Exception,System.String,System.Int32,System.Int32)
extern void JsonReaderException__ctor_m48D67CFFB089EFD85C083C7029BDE3D86ED72CA4 (void);
// 0x00000080 Newtonsoft.Json.JsonReaderException Newtonsoft.Json.JsonReaderException::Create(Newtonsoft.Json.JsonReader,System.String)
extern void JsonReaderException_Create_m9D60924AD1199D5FBA2F048E12C914D6E00BB8BD (void);
// 0x00000081 Newtonsoft.Json.JsonReaderException Newtonsoft.Json.JsonReaderException::Create(Newtonsoft.Json.JsonReader,System.String,System.Exception)
extern void JsonReaderException_Create_mB0D4CC9583C3A7AF5F588DAB965AC1B7B6C5C97A (void);
// 0x00000082 Newtonsoft.Json.JsonReaderException Newtonsoft.Json.JsonReaderException::Create(Newtonsoft.Json.IJsonLineInfo,System.String,System.String,System.Exception)
extern void JsonReaderException_Create_mF5A77FD722D66EC83FD22D6E93740FD4DD546A64 (void);
// 0x00000083 System.Void Newtonsoft.Json.JsonSerializationException::.ctor()
extern void JsonSerializationException__ctor_m51DC80D4F299ACC4A3340B74A3A81A09A003AAE8 (void);
// 0x00000084 System.Void Newtonsoft.Json.JsonSerializationException::.ctor(System.String)
extern void JsonSerializationException__ctor_m6FE8C668628845DB845F9DDEC79C50C300007444 (void);
// 0x00000085 System.Void Newtonsoft.Json.JsonSerializationException::.ctor(System.String,System.Exception)
extern void JsonSerializationException__ctor_m4FCF2C5B84A90CDFD7C21E1C40E96C9315A9B552 (void);
// 0x00000086 System.Void Newtonsoft.Json.JsonSerializationException::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern void JsonSerializationException__ctor_m8AF0A76617292B37ED2C6304A45CA83C524B6B45 (void);
// 0x00000087 Newtonsoft.Json.JsonSerializationException Newtonsoft.Json.JsonSerializationException::Create(Newtonsoft.Json.JsonReader,System.String)
extern void JsonSerializationException_Create_m59D9D0A78A9893C93F966A7ADF83DF696415D38A (void);
// 0x00000088 Newtonsoft.Json.JsonSerializationException Newtonsoft.Json.JsonSerializationException::Create(Newtonsoft.Json.JsonReader,System.String,System.Exception)
extern void JsonSerializationException_Create_m55E8DF615AD31911F8A5BBFDED20ED83549BD64E (void);
// 0x00000089 Newtonsoft.Json.JsonSerializationException Newtonsoft.Json.JsonSerializationException::Create(Newtonsoft.Json.IJsonLineInfo,System.String,System.String,System.Exception)
extern void JsonSerializationException_Create_m7BBB6C36BB6D2B61654B39C30B54097857521242 (void);
// 0x0000008A System.Void Newtonsoft.Json.JsonSerializer::add_Error(System.EventHandler`1<Newtonsoft.Json.Serialization.ErrorEventArgs>)
extern void JsonSerializer_add_Error_m5C077501D7D73DDF93019D736A6F72FCB733EFDD (void);
// 0x0000008B System.Void Newtonsoft.Json.JsonSerializer::remove_Error(System.EventHandler`1<Newtonsoft.Json.Serialization.ErrorEventArgs>)
extern void JsonSerializer_remove_Error_mF178375434BC06707ADEE0A7DC544296D5146B76 (void);
// 0x0000008C System.Void Newtonsoft.Json.JsonSerializer::set_ReferenceResolver(Newtonsoft.Json.Serialization.IReferenceResolver)
extern void JsonSerializer_set_ReferenceResolver_m75556B8339BEC4059A2F556CA8A0B60D5CDD62DE (void);
// 0x0000008D System.Void Newtonsoft.Json.JsonSerializer::set_Binder(System.Runtime.Serialization.SerializationBinder)
extern void JsonSerializer_set_Binder_mD26D3C7E5F24601A2F09DF2C0BF8F69310FC0835 (void);
// 0x0000008E Newtonsoft.Json.Serialization.ITraceWriter Newtonsoft.Json.JsonSerializer::get_TraceWriter()
extern void JsonSerializer_get_TraceWriter_mB1337CED6084DADE1E3C62F9EE58AE1E5C0BCF41 (void);
// 0x0000008F System.Void Newtonsoft.Json.JsonSerializer::set_TraceWriter(Newtonsoft.Json.Serialization.ITraceWriter)
extern void JsonSerializer_set_TraceWriter_mE16C70457284F8CA52CE1836DEA2C8D54F3C8289 (void);
// 0x00000090 System.Void Newtonsoft.Json.JsonSerializer::set_EqualityComparer(System.Collections.IEqualityComparer)
extern void JsonSerializer_set_EqualityComparer_m8494E039197F35E6D1B5EC199EB8BBB4328EFF6C (void);
// 0x00000091 System.Void Newtonsoft.Json.JsonSerializer::set_TypeNameHandling(Newtonsoft.Json.TypeNameHandling)
extern void JsonSerializer_set_TypeNameHandling_m397964590BDEB713460224E00325B456D29D2678 (void);
// 0x00000092 System.Void Newtonsoft.Json.JsonSerializer::set_TypeNameAssemblyFormat(System.Runtime.Serialization.Formatters.FormatterAssemblyStyle)
extern void JsonSerializer_set_TypeNameAssemblyFormat_mD3C6C79E85CE811738242E26FC1F13264D749EC6 (void);
// 0x00000093 System.Void Newtonsoft.Json.JsonSerializer::set_PreserveReferencesHandling(Newtonsoft.Json.PreserveReferencesHandling)
extern void JsonSerializer_set_PreserveReferencesHandling_m7CBE2D82706C9A69443E539D98B57BF95D5B5B3F (void);
// 0x00000094 System.Void Newtonsoft.Json.JsonSerializer::set_ReferenceLoopHandling(Newtonsoft.Json.ReferenceLoopHandling)
extern void JsonSerializer_set_ReferenceLoopHandling_m05EAE1196EFE2ABA674535EEB00D8479D19B4BB5 (void);
// 0x00000095 System.Void Newtonsoft.Json.JsonSerializer::set_MissingMemberHandling(Newtonsoft.Json.MissingMemberHandling)
extern void JsonSerializer_set_MissingMemberHandling_m93EADF9C8A721C50B3FB4A078E221EEE81EB36D6 (void);
// 0x00000096 System.Void Newtonsoft.Json.JsonSerializer::set_NullValueHandling(Newtonsoft.Json.NullValueHandling)
extern void JsonSerializer_set_NullValueHandling_mBF4C642BE01DDC827B133DED967ED739EDA25E42 (void);
// 0x00000097 System.Void Newtonsoft.Json.JsonSerializer::set_DefaultValueHandling(Newtonsoft.Json.DefaultValueHandling)
extern void JsonSerializer_set_DefaultValueHandling_mA55AFC08ED20A575679BA593400426C27ECD164F (void);
// 0x00000098 System.Void Newtonsoft.Json.JsonSerializer::set_ObjectCreationHandling(Newtonsoft.Json.ObjectCreationHandling)
extern void JsonSerializer_set_ObjectCreationHandling_m7866DEABB0F449EBE1B6AE41591133440F985DF3 (void);
// 0x00000099 System.Void Newtonsoft.Json.JsonSerializer::set_ConstructorHandling(Newtonsoft.Json.ConstructorHandling)
extern void JsonSerializer_set_ConstructorHandling_mD2CE70B58224E9711FA6F105DB05BEAAAB245654 (void);
// 0x0000009A Newtonsoft.Json.MetadataPropertyHandling Newtonsoft.Json.JsonSerializer::get_MetadataPropertyHandling()
extern void JsonSerializer_get_MetadataPropertyHandling_m8D89B353DC55367E723C801CB52084011A735B40 (void);
// 0x0000009B System.Void Newtonsoft.Json.JsonSerializer::set_MetadataPropertyHandling(Newtonsoft.Json.MetadataPropertyHandling)
extern void JsonSerializer_set_MetadataPropertyHandling_mA2D6F880BF40FDCC4C94B43E212368CC40043470 (void);
// 0x0000009C Newtonsoft.Json.JsonConverterCollection Newtonsoft.Json.JsonSerializer::get_Converters()
extern void JsonSerializer_get_Converters_m5FD1DB9720DDAD15A404A3FFBDCF761892D0A965 (void);
// 0x0000009D Newtonsoft.Json.Serialization.IContractResolver Newtonsoft.Json.JsonSerializer::get_ContractResolver()
extern void JsonSerializer_get_ContractResolver_m2B3249B713D8C5FE5E05DEF4DBF6290F2892025D (void);
// 0x0000009E System.Void Newtonsoft.Json.JsonSerializer::set_ContractResolver(Newtonsoft.Json.Serialization.IContractResolver)
extern void JsonSerializer_set_ContractResolver_m827D6D814BA33D3793060CF74F2F2E7CAAF07EE4 (void);
// 0x0000009F System.Runtime.Serialization.StreamingContext Newtonsoft.Json.JsonSerializer::get_Context()
extern void JsonSerializer_get_Context_mE40E7BB6FEE586934C0B31F0DE2DB65C4BF12C65 (void);
// 0x000000A0 System.Void Newtonsoft.Json.JsonSerializer::set_Context(System.Runtime.Serialization.StreamingContext)
extern void JsonSerializer_set_Context_m1D8E4106F702203C6E81801BCC5F8A10F5124C39 (void);
// 0x000000A1 Newtonsoft.Json.Formatting Newtonsoft.Json.JsonSerializer::get_Formatting()
extern void JsonSerializer_get_Formatting_mDBB1A0DD792AD14B9D395F4998D5BAF25DFC586D (void);
// 0x000000A2 System.Boolean Newtonsoft.Json.JsonSerializer::get_CheckAdditionalContent()
extern void JsonSerializer_get_CheckAdditionalContent_mDBFF4CC55D193273172E9BD6FC60022A434A9698 (void);
// 0x000000A3 System.Void Newtonsoft.Json.JsonSerializer::set_CheckAdditionalContent(System.Boolean)
extern void JsonSerializer_set_CheckAdditionalContent_m6B7F7AAFB1CEA4D15174DE2B4C4B713B51F23B8B (void);
// 0x000000A4 System.Boolean Newtonsoft.Json.JsonSerializer::IsCheckAdditionalContentSet()
extern void JsonSerializer_IsCheckAdditionalContentSet_m0E08FBB61882777E98BEFADE819DB8B093D32067 (void);
// 0x000000A5 System.Void Newtonsoft.Json.JsonSerializer::.ctor()
extern void JsonSerializer__ctor_mEEA9E2EA97F144384EC57BD0779DE4AEAD2484C8 (void);
// 0x000000A6 Newtonsoft.Json.JsonSerializer Newtonsoft.Json.JsonSerializer::Create()
extern void JsonSerializer_Create_m21A9167E10C2F453C4A8C31DF1C9DB0BA16B1C2D (void);
// 0x000000A7 Newtonsoft.Json.JsonSerializer Newtonsoft.Json.JsonSerializer::Create(Newtonsoft.Json.JsonSerializerSettings)
extern void JsonSerializer_Create_m270F5EC9830F09ED5E30B71E603F1DE24545C3B4 (void);
// 0x000000A8 Newtonsoft.Json.JsonSerializer Newtonsoft.Json.JsonSerializer::CreateDefault()
extern void JsonSerializer_CreateDefault_mFE0495565E208070F09F290F850524CCE98EF9A1 (void);
// 0x000000A9 Newtonsoft.Json.JsonSerializer Newtonsoft.Json.JsonSerializer::CreateDefault(Newtonsoft.Json.JsonSerializerSettings)
extern void JsonSerializer_CreateDefault_m0E1231433DC981D1ED70DCE89AB6AC9E6091484C (void);
// 0x000000AA System.Void Newtonsoft.Json.JsonSerializer::ApplySerializerSettings(Newtonsoft.Json.JsonSerializer,Newtonsoft.Json.JsonSerializerSettings)
extern void JsonSerializer_ApplySerializerSettings_mE0DBDB2D8F2169D65CDCC34A5D57BAD1D73DAF4A (void);
// 0x000000AB T Newtonsoft.Json.JsonSerializer::Deserialize(Newtonsoft.Json.JsonReader)
// 0x000000AC System.Object Newtonsoft.Json.JsonSerializer::Deserialize(Newtonsoft.Json.JsonReader,System.Type)
extern void JsonSerializer_Deserialize_mA9114FAC8423AE9FEF695320ABD2C6EEA7B6A0F1 (void);
// 0x000000AD System.Object Newtonsoft.Json.JsonSerializer::DeserializeInternal(Newtonsoft.Json.JsonReader,System.Type)
extern void JsonSerializer_DeserializeInternal_m3C23ED58C2B6AA6A362B8AF3F4BF61E819B8D62C (void);
// 0x000000AE System.Void Newtonsoft.Json.JsonSerializer::SetupReader(Newtonsoft.Json.JsonReader,System.Globalization.CultureInfo&,System.Nullable`1<Newtonsoft.Json.DateTimeZoneHandling>&,System.Nullable`1<Newtonsoft.Json.DateParseHandling>&,System.Nullable`1<Newtonsoft.Json.FloatParseHandling>&,System.Nullable`1<System.Int32>&,System.String&)
extern void JsonSerializer_SetupReader_m6355C359F82DF266F72557EA50B1D4949C12BAFC (void);
// 0x000000AF System.Void Newtonsoft.Json.JsonSerializer::ResetReader(Newtonsoft.Json.JsonReader,System.Globalization.CultureInfo,System.Nullable`1<Newtonsoft.Json.DateTimeZoneHandling>,System.Nullable`1<Newtonsoft.Json.DateParseHandling>,System.Nullable`1<Newtonsoft.Json.FloatParseHandling>,System.Nullable`1<System.Int32>,System.String)
extern void JsonSerializer_ResetReader_m91B70A8577CCE724D494AA0E3444CA7E8D37A2BE (void);
// 0x000000B0 System.Void Newtonsoft.Json.JsonSerializer::Serialize(Newtonsoft.Json.JsonWriter,System.Object,System.Type)
extern void JsonSerializer_Serialize_m3D0B5F576D4705243F3D5706CAD3417D333686E9 (void);
// 0x000000B1 System.Void Newtonsoft.Json.JsonSerializer::Serialize(Newtonsoft.Json.JsonWriter,System.Object)
extern void JsonSerializer_Serialize_mFED2010B103C1A19C4601BFAAB18AEEAAB11DE2B (void);
// 0x000000B2 System.Void Newtonsoft.Json.JsonSerializer::SerializeInternal(Newtonsoft.Json.JsonWriter,System.Object,System.Type)
extern void JsonSerializer_SerializeInternal_m7A4AB1E8CEB86E78833272778CA662F77B92AF5F (void);
// 0x000000B3 Newtonsoft.Json.Serialization.IReferenceResolver Newtonsoft.Json.JsonSerializer::GetReferenceResolver()
extern void JsonSerializer_GetReferenceResolver_m862EC8CEEC3E04D76AD12A27FF37432A3D87C144 (void);
// 0x000000B4 Newtonsoft.Json.JsonConverter Newtonsoft.Json.JsonSerializer::GetMatchingConverter(System.Type)
extern void JsonSerializer_GetMatchingConverter_m35B81E973C750954126CDE53BCDF9141B5C00482 (void);
// 0x000000B5 Newtonsoft.Json.JsonConverter Newtonsoft.Json.JsonSerializer::GetMatchingConverter(System.Collections.Generic.IList`1<Newtonsoft.Json.JsonConverter>,System.Type)
extern void JsonSerializer_GetMatchingConverter_mFCCC2F4C1165495D46898B673F1683DAA5C13671 (void);
// 0x000000B6 System.Void Newtonsoft.Json.JsonSerializer::OnError(Newtonsoft.Json.Serialization.ErrorEventArgs)
extern void JsonSerializer_OnError_m241DB4FB4B83E5BFB2E42194C266EF2326719803 (void);
// 0x000000B7 Newtonsoft.Json.ReferenceLoopHandling Newtonsoft.Json.JsonSerializerSettings::get_ReferenceLoopHandling()
extern void JsonSerializerSettings_get_ReferenceLoopHandling_m157C6A9C3DD60D87575C180A13710D626854A3AA (void);
// 0x000000B8 Newtonsoft.Json.MissingMemberHandling Newtonsoft.Json.JsonSerializerSettings::get_MissingMemberHandling()
extern void JsonSerializerSettings_get_MissingMemberHandling_m17A7ADA7EA3932A4643E6F9D06B4D1891D51F7F1 (void);
// 0x000000B9 Newtonsoft.Json.ObjectCreationHandling Newtonsoft.Json.JsonSerializerSettings::get_ObjectCreationHandling()
extern void JsonSerializerSettings_get_ObjectCreationHandling_mCE8BF67E879C6FC72207919951EE5E3FAF67E2D4 (void);
// 0x000000BA Newtonsoft.Json.NullValueHandling Newtonsoft.Json.JsonSerializerSettings::get_NullValueHandling()
extern void JsonSerializerSettings_get_NullValueHandling_mB7E2F6FBE8924F97F7A393688AEC8D0FD9F36E2F (void);
// 0x000000BB Newtonsoft.Json.DefaultValueHandling Newtonsoft.Json.JsonSerializerSettings::get_DefaultValueHandling()
extern void JsonSerializerSettings_get_DefaultValueHandling_m44640DC8021650A01ABFC24F27A59C7332826AA4 (void);
// 0x000000BC System.Collections.Generic.IList`1<Newtonsoft.Json.JsonConverter> Newtonsoft.Json.JsonSerializerSettings::get_Converters()
extern void JsonSerializerSettings_get_Converters_m862DFE5BDA2760F5946FB6DDB34DCA67FC6FF29D (void);
// 0x000000BD Newtonsoft.Json.PreserveReferencesHandling Newtonsoft.Json.JsonSerializerSettings::get_PreserveReferencesHandling()
extern void JsonSerializerSettings_get_PreserveReferencesHandling_m9C0757967BEB3C25C034F36CDE2764CDC619988B (void);
// 0x000000BE Newtonsoft.Json.TypeNameHandling Newtonsoft.Json.JsonSerializerSettings::get_TypeNameHandling()
extern void JsonSerializerSettings_get_TypeNameHandling_m23E1F5B25A7A92865E827291C1B64FBBC2B95085 (void);
// 0x000000BF Newtonsoft.Json.MetadataPropertyHandling Newtonsoft.Json.JsonSerializerSettings::get_MetadataPropertyHandling()
extern void JsonSerializerSettings_get_MetadataPropertyHandling_m35544141ADB21B96E7B9A393BC5F7DEF5D6CD351 (void);
// 0x000000C0 System.Runtime.Serialization.Formatters.FormatterAssemblyStyle Newtonsoft.Json.JsonSerializerSettings::get_TypeNameAssemblyFormat()
extern void JsonSerializerSettings_get_TypeNameAssemblyFormat_mCFE36076151C13EEE61BC890307ADD081E07F9C0 (void);
// 0x000000C1 Newtonsoft.Json.ConstructorHandling Newtonsoft.Json.JsonSerializerSettings::get_ConstructorHandling()
extern void JsonSerializerSettings_get_ConstructorHandling_m0ABCC68D14B15898FE5424D80284B7DCEF481D6B (void);
// 0x000000C2 Newtonsoft.Json.Serialization.IContractResolver Newtonsoft.Json.JsonSerializerSettings::get_ContractResolver()
extern void JsonSerializerSettings_get_ContractResolver_mAE8A1AB42B6FA02463E9A3B613A777983107D591 (void);
// 0x000000C3 System.Collections.IEqualityComparer Newtonsoft.Json.JsonSerializerSettings::get_EqualityComparer()
extern void JsonSerializerSettings_get_EqualityComparer_m41C02E38C29AF6A4CAC4894CC79FA724974CDC82 (void);
// 0x000000C4 System.Func`1<Newtonsoft.Json.Serialization.IReferenceResolver> Newtonsoft.Json.JsonSerializerSettings::get_ReferenceResolverProvider()
extern void JsonSerializerSettings_get_ReferenceResolverProvider_mE84D665F375BFFC7BF287CB869DDEF0BE408853D (void);
// 0x000000C5 Newtonsoft.Json.Serialization.ITraceWriter Newtonsoft.Json.JsonSerializerSettings::get_TraceWriter()
extern void JsonSerializerSettings_get_TraceWriter_mE3D106D673BF56399607EADC698FAC00AFE65C72 (void);
// 0x000000C6 System.Runtime.Serialization.SerializationBinder Newtonsoft.Json.JsonSerializerSettings::get_Binder()
extern void JsonSerializerSettings_get_Binder_m9508F5EA174FF976E69F0C78B3934BFC56E2F035 (void);
// 0x000000C7 System.EventHandler`1<Newtonsoft.Json.Serialization.ErrorEventArgs> Newtonsoft.Json.JsonSerializerSettings::get_Error()
extern void JsonSerializerSettings_get_Error_mEDC7BE577D288DD35B65F534E00C681854578371 (void);
// 0x000000C8 System.Runtime.Serialization.StreamingContext Newtonsoft.Json.JsonSerializerSettings::get_Context()
extern void JsonSerializerSettings_get_Context_mE4B431CE4CE95A0D7E3BE9AFD454A97521562D2A (void);
// 0x000000C9 System.Void Newtonsoft.Json.JsonSerializerSettings::.cctor()
extern void JsonSerializerSettings__cctor_mD0353F0CB2B4593EF48159E5684B31906DFA9AB3 (void);
// 0x000000CA System.Void Newtonsoft.Json.JsonTextReader::.ctor(System.IO.TextReader)
extern void JsonTextReader__ctor_m06ACC9F0CFD321E7211EFD16355C213B2E6E4E05 (void);
// 0x000000CB System.Void Newtonsoft.Json.JsonTextReader::EnsureBufferNotEmpty()
extern void JsonTextReader_EnsureBufferNotEmpty_m7355FDB571D125D65BE7D61235F2C60464436B25 (void);
// 0x000000CC System.Void Newtonsoft.Json.JsonTextReader::OnNewLine(System.Int32)
extern void JsonTextReader_OnNewLine_m582BF1C40C7339C59BE7BD03BD83BF54118B2B57 (void);
// 0x000000CD System.Void Newtonsoft.Json.JsonTextReader::ParseString(System.Char,Newtonsoft.Json.ReadType)
extern void JsonTextReader_ParseString_m6467AEDB7C8DF6B92A3DD84B3E37B72C8A3FE342 (void);
// 0x000000CE System.Void Newtonsoft.Json.JsonTextReader::BlockCopyChars(System.Char[],System.Int32,System.Char[],System.Int32,System.Int32)
extern void JsonTextReader_BlockCopyChars_mB6E7365C5F1F14BA57632535B0A7A278555F0691 (void);
// 0x000000CF System.Void Newtonsoft.Json.JsonTextReader::ShiftBufferIfNeeded()
extern void JsonTextReader_ShiftBufferIfNeeded_m3E8D22377380A3CCE23A17A06F715E87264FC4E4 (void);
// 0x000000D0 System.Int32 Newtonsoft.Json.JsonTextReader::ReadData(System.Boolean)
extern void JsonTextReader_ReadData_mBE18A474B103A29E2A479E09B9E6DBDC804A46B0 (void);
// 0x000000D1 System.Int32 Newtonsoft.Json.JsonTextReader::ReadData(System.Boolean,System.Int32)
extern void JsonTextReader_ReadData_m1E0613006BB76DDEA88200B19D58D5A5C550181D (void);
// 0x000000D2 System.Boolean Newtonsoft.Json.JsonTextReader::EnsureChars(System.Int32,System.Boolean)
extern void JsonTextReader_EnsureChars_m86780F558D161C61231205708800459DDBAA6D74 (void);
// 0x000000D3 System.Boolean Newtonsoft.Json.JsonTextReader::ReadChars(System.Int32,System.Boolean)
extern void JsonTextReader_ReadChars_mE7E9AEB29792C6EA77283D150A83E7F6C4AB4CE5 (void);
// 0x000000D4 System.Boolean Newtonsoft.Json.JsonTextReader::Read()
extern void JsonTextReader_Read_mC2B5F6EFA1BA940FFC3D7EFBF55E95D0DB43C88A (void);
// 0x000000D5 System.Nullable`1<System.Int32> Newtonsoft.Json.JsonTextReader::ReadAsInt32()
extern void JsonTextReader_ReadAsInt32_m9C94B1752FDA9D891FD4806A7440EEC75404A399 (void);
// 0x000000D6 System.Nullable`1<System.DateTime> Newtonsoft.Json.JsonTextReader::ReadAsDateTime()
extern void JsonTextReader_ReadAsDateTime_mD60471AA24C84837063F93A8EE1B0DF585435586 (void);
// 0x000000D7 System.String Newtonsoft.Json.JsonTextReader::ReadAsString()
extern void JsonTextReader_ReadAsString_m4DF18D5C7A49CAFD5B19DF5C661B5BCDC59767D6 (void);
// 0x000000D8 System.Byte[] Newtonsoft.Json.JsonTextReader::ReadAsBytes()
extern void JsonTextReader_ReadAsBytes_m7CA56D7E09BDA42BD5D6FF1C96FF90344777843C (void);
// 0x000000D9 System.Object Newtonsoft.Json.JsonTextReader::ReadStringValue(Newtonsoft.Json.ReadType)
extern void JsonTextReader_ReadStringValue_m1BE5443E3B38ADE0C651B102A032318428EC3815 (void);
// 0x000000DA Newtonsoft.Json.JsonReaderException Newtonsoft.Json.JsonTextReader::CreateUnexpectedCharacterException(System.Char)
extern void JsonTextReader_CreateUnexpectedCharacterException_m24DCCF2AB7757041F94D527B9FF2ED3B59D5D31F (void);
// 0x000000DB System.Nullable`1<System.Boolean> Newtonsoft.Json.JsonTextReader::ReadAsBoolean()
extern void JsonTextReader_ReadAsBoolean_m9CDDD7048A7D6E3C9748120A9F237DF26DFDAE9A (void);
// 0x000000DC System.Void Newtonsoft.Json.JsonTextReader::ProcessValueComma()
extern void JsonTextReader_ProcessValueComma_m791FA99C7CB07102A0A6A3A5BA66DE76079EC83B (void);
// 0x000000DD System.Object Newtonsoft.Json.JsonTextReader::ReadNumberValue(Newtonsoft.Json.ReadType)
extern void JsonTextReader_ReadNumberValue_mA2DCFEE2517C40092E342610B459F9B958932C4A (void);
// 0x000000DE System.Nullable`1<System.DateTimeOffset> Newtonsoft.Json.JsonTextReader::ReadAsDateTimeOffset()
extern void JsonTextReader_ReadAsDateTimeOffset_mEA8C87CEBD105784B897C3A9DE3C65E110AF0A50 (void);
// 0x000000DF System.Nullable`1<System.Decimal> Newtonsoft.Json.JsonTextReader::ReadAsDecimal()
extern void JsonTextReader_ReadAsDecimal_m6D0EA57C831C43A05D94BC98D83C26ABFF3B4434 (void);
// 0x000000E0 System.Nullable`1<System.Double> Newtonsoft.Json.JsonTextReader::ReadAsDouble()
extern void JsonTextReader_ReadAsDouble_m04166D16390430DBF24603D47AB85EB68F1878A2 (void);
// 0x000000E1 System.Void Newtonsoft.Json.JsonTextReader::HandleNull()
extern void JsonTextReader_HandleNull_mC51667891782E90C011E89A519C5EB3ACDB4579D (void);
// 0x000000E2 System.Void Newtonsoft.Json.JsonTextReader::ReadFinished()
extern void JsonTextReader_ReadFinished_m564A05DAB39D8DAAD04CF2030AD27D828359E973 (void);
// 0x000000E3 System.Boolean Newtonsoft.Json.JsonTextReader::ReadNullChar()
extern void JsonTextReader_ReadNullChar_m62DC2673AB513DD4A81A1B988CD97EE4D3B30795 (void);
// 0x000000E4 System.Void Newtonsoft.Json.JsonTextReader::EnsureBuffer()
extern void JsonTextReader_EnsureBuffer_m304C435886673BC93B1032E1E779302CE41527D9 (void);
// 0x000000E5 System.Void Newtonsoft.Json.JsonTextReader::ReadStringIntoBuffer(System.Char)
extern void JsonTextReader_ReadStringIntoBuffer_mEE64F5783FAE0AE0B9A6BFAF1BF02832CCBD313A (void);
// 0x000000E6 System.Void Newtonsoft.Json.JsonTextReader::WriteCharToBuffer(System.Char,System.Int32,System.Int32)
extern void JsonTextReader_WriteCharToBuffer_mD005722B4B821C657F56C2F7C65BA9BC238CFC30 (void);
// 0x000000E7 System.Char Newtonsoft.Json.JsonTextReader::ParseUnicode()
extern void JsonTextReader_ParseUnicode_m6CDB5D90B6CF95C63EFB13CCCC1034E9F7CC3920 (void);
// 0x000000E8 System.Void Newtonsoft.Json.JsonTextReader::ReadNumberIntoBuffer()
extern void JsonTextReader_ReadNumberIntoBuffer_m2BCBC9B74CC5850348F922F98C6E9D9AC3DC6F64 (void);
// 0x000000E9 System.Void Newtonsoft.Json.JsonTextReader::ClearRecentString()
extern void JsonTextReader_ClearRecentString_m7E76B1C8543F5E79E2DBDC1A62A13D01B6622F4C (void);
// 0x000000EA System.Boolean Newtonsoft.Json.JsonTextReader::ParsePostValue()
extern void JsonTextReader_ParsePostValue_mAFA8A18FBACA01B355A97D6285A7C51C5410E891 (void);
// 0x000000EB System.Boolean Newtonsoft.Json.JsonTextReader::ParseObject()
extern void JsonTextReader_ParseObject_m2AF08458DE9247B2F42E412263129F211B6827FD (void);
// 0x000000EC System.Boolean Newtonsoft.Json.JsonTextReader::ParseProperty()
extern void JsonTextReader_ParseProperty_mDB11C172A1698122C5842F36B6CFEF3F0F0727E8 (void);
// 0x000000ED System.Boolean Newtonsoft.Json.JsonTextReader::ValidIdentifierChar(System.Char)
extern void JsonTextReader_ValidIdentifierChar_m20031CE2F94DED232AE1BEEBA383EFBB6EF37812 (void);
// 0x000000EE System.Void Newtonsoft.Json.JsonTextReader::ParseUnquotedProperty()
extern void JsonTextReader_ParseUnquotedProperty_m165787F113F0BA1DF08DB70C8BF7F90A7A7B2CA1 (void);
// 0x000000EF System.Boolean Newtonsoft.Json.JsonTextReader::ParseValue()
extern void JsonTextReader_ParseValue_m3EE97234F3EF348EB23F457158124996743D4EB3 (void);
// 0x000000F0 System.Void Newtonsoft.Json.JsonTextReader::ProcessLineFeed()
extern void JsonTextReader_ProcessLineFeed_m424752F705A641B3799C6EE717276DDD54FFFBA8 (void);
// 0x000000F1 System.Void Newtonsoft.Json.JsonTextReader::ProcessCarriageReturn(System.Boolean)
extern void JsonTextReader_ProcessCarriageReturn_m9D9B468775FA20E403D4AB8F5C2793188A0D3116 (void);
// 0x000000F2 System.Boolean Newtonsoft.Json.JsonTextReader::EatWhitespace(System.Boolean)
extern void JsonTextReader_EatWhitespace_m874203972F0D0C280B05BFFD23F714139F7935F7 (void);
// 0x000000F3 System.Void Newtonsoft.Json.JsonTextReader::ParseConstructor()
extern void JsonTextReader_ParseConstructor_m62E8F83002853986BC55F7A96CE81B705E23D8C5 (void);
// 0x000000F4 System.Void Newtonsoft.Json.JsonTextReader::ParseNumber(Newtonsoft.Json.ReadType)
extern void JsonTextReader_ParseNumber_m3CD060C4B577BFED08E5AE7A29865B0C23D4DE88 (void);
// 0x000000F5 System.Void Newtonsoft.Json.JsonTextReader::ParseComment(System.Boolean)
extern void JsonTextReader_ParseComment_m1D12FA3D94542985D227885F9F09EDACC14C2D9A (void);
// 0x000000F6 System.Void Newtonsoft.Json.JsonTextReader::EndComment(System.Boolean,System.Int32,System.Int32)
extern void JsonTextReader_EndComment_mC319298FEC64DD66DD6C910C31ECDA16CED8BC82 (void);
// 0x000000F7 System.Boolean Newtonsoft.Json.JsonTextReader::MatchValue(System.String)
extern void JsonTextReader_MatchValue_m9904A8B0FFC184AA30F4ED2D5289B1EA81FCF622 (void);
// 0x000000F8 System.Boolean Newtonsoft.Json.JsonTextReader::MatchValueWithTrailingSeparator(System.String)
extern void JsonTextReader_MatchValueWithTrailingSeparator_m193F3B827101D97DAB81CD58F3E37E72586063C1 (void);
// 0x000000F9 System.Boolean Newtonsoft.Json.JsonTextReader::IsSeparator(System.Char)
extern void JsonTextReader_IsSeparator_m797230AE86E47CF40FD5D4348633BE5AC6C8826A (void);
// 0x000000FA System.Void Newtonsoft.Json.JsonTextReader::ParseTrue()
extern void JsonTextReader_ParseTrue_mCE289BB4C4A01C54D230FA78B37333C7A1AE253A (void);
// 0x000000FB System.Void Newtonsoft.Json.JsonTextReader::ParseNull()
extern void JsonTextReader_ParseNull_mCEC838A3EE3FD7F27D6096251B16E698A8D2EC1F (void);
// 0x000000FC System.Void Newtonsoft.Json.JsonTextReader::ParseUndefined()
extern void JsonTextReader_ParseUndefined_m7353BA0E92FB8B49671B49F01C6DBC495DCA63C1 (void);
// 0x000000FD System.Void Newtonsoft.Json.JsonTextReader::ParseFalse()
extern void JsonTextReader_ParseFalse_mA22082DD717016BFD4CE9B4B22A3EFBD35FD1C2B (void);
// 0x000000FE System.Object Newtonsoft.Json.JsonTextReader::ParseNumberNegativeInfinity(Newtonsoft.Json.ReadType)
extern void JsonTextReader_ParseNumberNegativeInfinity_m8D9A6D80029CF74D066D38305476A4686EA5075A (void);
// 0x000000FF System.Object Newtonsoft.Json.JsonTextReader::ParseNumberPositiveInfinity(Newtonsoft.Json.ReadType)
extern void JsonTextReader_ParseNumberPositiveInfinity_m571720F3E3D4701116DB75EBC528844AC0412E11 (void);
// 0x00000100 System.Object Newtonsoft.Json.JsonTextReader::ParseNumberNaN(Newtonsoft.Json.ReadType)
extern void JsonTextReader_ParseNumberNaN_m7C8B9D84EDDC4FB7C92EEB4B8160F0881C86E3D7 (void);
// 0x00000101 System.Void Newtonsoft.Json.JsonTextReader::Close()
extern void JsonTextReader_Close_m1309D7A5CF41328B7280DB628ABE83D11C3FF4C6 (void);
// 0x00000102 System.Boolean Newtonsoft.Json.JsonTextReader::HasLineInfo()
extern void JsonTextReader_HasLineInfo_mC8EABEC58BB5FD218E87B3CA84E261B9512E234F (void);
// 0x00000103 System.Int32 Newtonsoft.Json.JsonTextReader::get_LineNumber()
extern void JsonTextReader_get_LineNumber_mCC497D53352BF5BAB5AE9FA600D84E7148361E2F (void);
// 0x00000104 System.Int32 Newtonsoft.Json.JsonTextReader::get_LinePosition()
extern void JsonTextReader_get_LinePosition_m0A76131C697A84985FF4CDAD33C3363B7C9C6B8F (void);
// 0x00000105 Newtonsoft.Json.Utilities.Base64Encoder Newtonsoft.Json.JsonTextWriter::get_Base64Encoder()
extern void JsonTextWriter_get_Base64Encoder_mF7A61C0288E78B9CF88F788181462F7FE0D54B72 (void);
// 0x00000106 System.Char Newtonsoft.Json.JsonTextWriter::get_QuoteChar()
extern void JsonTextWriter_get_QuoteChar_m2AA4D871344E874303953504844A2A8916A1D2C4 (void);
// 0x00000107 System.Void Newtonsoft.Json.JsonTextWriter::.ctor(System.IO.TextWriter)
extern void JsonTextWriter__ctor_mC16FDDC0DAEC5D2B51445A44A40273E4E45D03C8 (void);
// 0x00000108 System.Void Newtonsoft.Json.JsonTextWriter::Close()
extern void JsonTextWriter_Close_mA44AC213BBBBFE14DEF9182105B22824CD2BCE33 (void);
// 0x00000109 System.Void Newtonsoft.Json.JsonTextWriter::WriteStartObject()
extern void JsonTextWriter_WriteStartObject_mA9225FFF1F59FEFAF7E286846F959BB3C346E852 (void);
// 0x0000010A System.Void Newtonsoft.Json.JsonTextWriter::WriteStartArray()
extern void JsonTextWriter_WriteStartArray_m67717606BC5BB1D07EEDD3F201421C1FACF9D6EE (void);
// 0x0000010B System.Void Newtonsoft.Json.JsonTextWriter::WriteStartConstructor(System.String)
extern void JsonTextWriter_WriteStartConstructor_mCD8166C3054966692CC42D984237154366FF9009 (void);
// 0x0000010C System.Void Newtonsoft.Json.JsonTextWriter::WriteEnd(Newtonsoft.Json.JsonToken)
extern void JsonTextWriter_WriteEnd_m2BE2304BACCCCC344715B2B2368AE86540E8846E (void);
// 0x0000010D System.Void Newtonsoft.Json.JsonTextWriter::WritePropertyName(System.String)
extern void JsonTextWriter_WritePropertyName_mF4930040832DB4C17FCF6DAA88AC2EB165612A35 (void);
// 0x0000010E System.Void Newtonsoft.Json.JsonTextWriter::WritePropertyName(System.String,System.Boolean)
extern void JsonTextWriter_WritePropertyName_m316550B0B5B7AA0BDD06BC8A55ED7004518E1D34 (void);
// 0x0000010F System.Void Newtonsoft.Json.JsonTextWriter::OnStringEscapeHandlingChanged()
extern void JsonTextWriter_OnStringEscapeHandlingChanged_m34B48FDE6C7B276920BBB7987CAC438FD5DBAD37 (void);
// 0x00000110 System.Void Newtonsoft.Json.JsonTextWriter::UpdateCharEscapeFlags()
extern void JsonTextWriter_UpdateCharEscapeFlags_m22D06D2618B27EC099B02D9E4C498F78CC5EE045 (void);
// 0x00000111 System.Void Newtonsoft.Json.JsonTextWriter::WriteIndent()
extern void JsonTextWriter_WriteIndent_mB8EFAEB0EE303AC6B054286E5519D0BE94F0009C (void);
// 0x00000112 System.Void Newtonsoft.Json.JsonTextWriter::WriteValueDelimiter()
extern void JsonTextWriter_WriteValueDelimiter_mB96308335A8C2C90B44C54E2B17FD99184FA27F8 (void);
// 0x00000113 System.Void Newtonsoft.Json.JsonTextWriter::WriteIndentSpace()
extern void JsonTextWriter_WriteIndentSpace_m75850BC7570E76FC461236390147BEC4A1ADACAF (void);
// 0x00000114 System.Void Newtonsoft.Json.JsonTextWriter::WriteValueInternal(System.String,Newtonsoft.Json.JsonToken)
extern void JsonTextWriter_WriteValueInternal_m083E0E9A0369C4F57F3EC5189681D46CAFD179B7 (void);
// 0x00000115 System.Void Newtonsoft.Json.JsonTextWriter::WriteNull()
extern void JsonTextWriter_WriteNull_mC9711AAA59CE615F3999165773A65195E299C5E2 (void);
// 0x00000116 System.Void Newtonsoft.Json.JsonTextWriter::WriteUndefined()
extern void JsonTextWriter_WriteUndefined_m502FEA1A07FCD29627DA574CC8F993818EE8D6FF (void);
// 0x00000117 System.Void Newtonsoft.Json.JsonTextWriter::WriteRaw(System.String)
extern void JsonTextWriter_WriteRaw_mFFD05486F43FE37AACB4BB0FD92158FF869CFBE8 (void);
// 0x00000118 System.Void Newtonsoft.Json.JsonTextWriter::WriteValue(System.String)
extern void JsonTextWriter_WriteValue_m3B97EBDBBB56F9AC0A783B93D6A5E2789B62E2F6 (void);
// 0x00000119 System.Void Newtonsoft.Json.JsonTextWriter::WriteEscapedString(System.String,System.Boolean)
extern void JsonTextWriter_WriteEscapedString_m0A4050B72FB45C522F730DD4FEA7018D62D22C8B (void);
// 0x0000011A System.Void Newtonsoft.Json.JsonTextWriter::WriteValue(System.Int32)
extern void JsonTextWriter_WriteValue_m308F2014568514E7CFA6798E6E804D36F8B525B3 (void);
// 0x0000011B System.Void Newtonsoft.Json.JsonTextWriter::WriteValue(System.UInt32)
extern void JsonTextWriter_WriteValue_m5BC424CE917FE84086E29B839EC6A292C75EE795 (void);
// 0x0000011C System.Void Newtonsoft.Json.JsonTextWriter::WriteValue(System.Int64)
extern void JsonTextWriter_WriteValue_mA071C2F8A06B852BB11AB4083ECD6184DF73E4B0 (void);
// 0x0000011D System.Void Newtonsoft.Json.JsonTextWriter::WriteValue(System.UInt64)
extern void JsonTextWriter_WriteValue_m14AAEAAC3218C11F806E7BD4E7F4D0592B321268 (void);
// 0x0000011E System.Void Newtonsoft.Json.JsonTextWriter::WriteValue(System.Single)
extern void JsonTextWriter_WriteValue_m68DF3FE43E8918A4BEDF20E082A38719DB9CAE14 (void);
// 0x0000011F System.Void Newtonsoft.Json.JsonTextWriter::WriteValue(System.Nullable`1<System.Single>)
extern void JsonTextWriter_WriteValue_mEDB5BCC7920B361EF29099F684DF1A4F96BC2163 (void);
// 0x00000120 System.Void Newtonsoft.Json.JsonTextWriter::WriteValue(System.Double)
extern void JsonTextWriter_WriteValue_mA94B419350ABB45DC7BBF3071EEA37A2FF89CCE1 (void);
// 0x00000121 System.Void Newtonsoft.Json.JsonTextWriter::WriteValue(System.Nullable`1<System.Double>)
extern void JsonTextWriter_WriteValue_m3AC83EC306770632B846C16A1EB036921FA213E6 (void);
// 0x00000122 System.Void Newtonsoft.Json.JsonTextWriter::WriteValue(System.Boolean)
extern void JsonTextWriter_WriteValue_mE402870CAB50FA196BB0F4D09B38251C623F9EE1 (void);
// 0x00000123 System.Void Newtonsoft.Json.JsonTextWriter::WriteValue(System.Int16)
extern void JsonTextWriter_WriteValue_m265CD7AB8D7BBC284891D73EAE64D4968F6CCAE5 (void);
// 0x00000124 System.Void Newtonsoft.Json.JsonTextWriter::WriteValue(System.UInt16)
extern void JsonTextWriter_WriteValue_mD772006CDE4E7F9E184D192814132E2D4EC77884 (void);
// 0x00000125 System.Void Newtonsoft.Json.JsonTextWriter::WriteValue(System.Char)
extern void JsonTextWriter_WriteValue_m0E9D82A09CE959FFBAA6F5467F633956A67D5A62 (void);
// 0x00000126 System.Void Newtonsoft.Json.JsonTextWriter::WriteValue(System.Byte)
extern void JsonTextWriter_WriteValue_m120FB9807C7803ACDDB286B147980A7AC20E8BDF (void);
// 0x00000127 System.Void Newtonsoft.Json.JsonTextWriter::WriteValue(System.SByte)
extern void JsonTextWriter_WriteValue_m909BF8D81AD7BD056580595C10A84ECAEC244C67 (void);
// 0x00000128 System.Void Newtonsoft.Json.JsonTextWriter::WriteValue(System.Decimal)
extern void JsonTextWriter_WriteValue_mD7BBF779E1FE4721F447208F3AE92C6CC83D618D (void);
// 0x00000129 System.Void Newtonsoft.Json.JsonTextWriter::WriteValue(System.DateTime)
extern void JsonTextWriter_WriteValue_m5D22A35E72B70F9D6694B07656C73A01F351094F (void);
// 0x0000012A System.Void Newtonsoft.Json.JsonTextWriter::WriteValue(System.Byte[])
extern void JsonTextWriter_WriteValue_m2F66121E669E6AD2627D85DACFEC5DDFDBD3CCE7 (void);
// 0x0000012B System.Void Newtonsoft.Json.JsonTextWriter::WriteValue(System.DateTimeOffset)
extern void JsonTextWriter_WriteValue_mAAEAD281D68ADF7BE51F2E830FB7597B2A812847 (void);
// 0x0000012C System.Void Newtonsoft.Json.JsonTextWriter::WriteValue(System.Guid)
extern void JsonTextWriter_WriteValue_m4AC01A5A85731A4358638F592CC8C916081E5304 (void);
// 0x0000012D System.Void Newtonsoft.Json.JsonTextWriter::WriteValue(System.TimeSpan)
extern void JsonTextWriter_WriteValue_mE728F5A883480A453D7D149899205AAFC4F5A743 (void);
// 0x0000012E System.Void Newtonsoft.Json.JsonTextWriter::WriteValue(System.Uri)
extern void JsonTextWriter_WriteValue_mB9DC613E1F00289F0E4D2B3E04DB101E441E63B0 (void);
// 0x0000012F System.Void Newtonsoft.Json.JsonTextWriter::WriteComment(System.String)
extern void JsonTextWriter_WriteComment_mEA109E10AF103412C18A849DBDB6A828DD0CBC1E (void);
// 0x00000130 System.Void Newtonsoft.Json.JsonTextWriter::EnsureWriteBuffer()
extern void JsonTextWriter_EnsureWriteBuffer_mA9412FB962FDB40B1A2B6DAA11DE34C1DA77203E (void);
// 0x00000131 System.Void Newtonsoft.Json.JsonTextWriter::WriteIntegerValue(System.Int64)
extern void JsonTextWriter_WriteIntegerValue_m0129BBC377D1E19AC8A7E50985FA7BD79743F654 (void);
// 0x00000132 System.Void Newtonsoft.Json.JsonTextWriter::WriteIntegerValue(System.UInt64)
extern void JsonTextWriter_WriteIntegerValue_m0D21F44FC6A21CDDB81D8BC2FB52D4DE559C6150 (void);
// 0x00000133 Newtonsoft.Json.JsonWriter/State[][] Newtonsoft.Json.JsonWriter::BuildStateArray()
extern void JsonWriter_BuildStateArray_m4F4220F4E23032D9268FE5EA43E1D17BF33AA23F (void);
// 0x00000134 System.Void Newtonsoft.Json.JsonWriter::.cctor()
extern void JsonWriter__cctor_m1632023B0C28407CE208CCF8277D780B026A1B62 (void);
// 0x00000135 System.Boolean Newtonsoft.Json.JsonWriter::get_CloseOutput()
extern void JsonWriter_get_CloseOutput_m5CADEDBE98FDF046F9BF209C242A07DB08C9678B (void);
// 0x00000136 System.Void Newtonsoft.Json.JsonWriter::set_CloseOutput(System.Boolean)
extern void JsonWriter_set_CloseOutput_m583E020AD56A73E77CDE34C208DB0115E067C7E0 (void);
// 0x00000137 System.Int32 Newtonsoft.Json.JsonWriter::get_Top()
extern void JsonWriter_get_Top_mC1FF46132B4A44FB748FBB30230BAD1F22C7147F (void);
// 0x00000138 Newtonsoft.Json.WriteState Newtonsoft.Json.JsonWriter::get_WriteState()
extern void JsonWriter_get_WriteState_m063AA8C1F1CFC3942B5866FBCE85969CCE98E426 (void);
// 0x00000139 System.String Newtonsoft.Json.JsonWriter::get_ContainerPath()
extern void JsonWriter_get_ContainerPath_m7D22BB50285380B5CACBFDEF88BE31E614CD52A8 (void);
// 0x0000013A System.String Newtonsoft.Json.JsonWriter::get_Path()
extern void JsonWriter_get_Path_m115B40757B4772E99AA6A901ABBD845557B19A2C (void);
// 0x0000013B Newtonsoft.Json.Formatting Newtonsoft.Json.JsonWriter::get_Formatting()
extern void JsonWriter_get_Formatting_m8A8EB004835AA8C5D570DB62B31374EBE17BA77E (void);
// 0x0000013C System.Void Newtonsoft.Json.JsonWriter::set_Formatting(Newtonsoft.Json.Formatting)
extern void JsonWriter_set_Formatting_m095D00C9220A17CD8CF9A678804C7F53AA436D68 (void);
// 0x0000013D Newtonsoft.Json.DateFormatHandling Newtonsoft.Json.JsonWriter::get_DateFormatHandling()
extern void JsonWriter_get_DateFormatHandling_m41BD1D20C51ECF8DA8202C37B62A9FF0E28823DD (void);
// 0x0000013E System.Void Newtonsoft.Json.JsonWriter::set_DateFormatHandling(Newtonsoft.Json.DateFormatHandling)
extern void JsonWriter_set_DateFormatHandling_mDE99B2D5CE84B4848D5B986BCEDA0B3ECF022923 (void);
// 0x0000013F Newtonsoft.Json.DateTimeZoneHandling Newtonsoft.Json.JsonWriter::get_DateTimeZoneHandling()
extern void JsonWriter_get_DateTimeZoneHandling_mD0C65660E7AE6E3C2FA819B280CCB795B7A2728A (void);
// 0x00000140 System.Void Newtonsoft.Json.JsonWriter::set_DateTimeZoneHandling(Newtonsoft.Json.DateTimeZoneHandling)
extern void JsonWriter_set_DateTimeZoneHandling_m05E9A774E4CD7AFC7DB4F8F072C653360648BF20 (void);
// 0x00000141 Newtonsoft.Json.StringEscapeHandling Newtonsoft.Json.JsonWriter::get_StringEscapeHandling()
extern void JsonWriter_get_StringEscapeHandling_m113D87EAADC49F42DC89E128A7DFB12D46693927 (void);
// 0x00000142 System.Void Newtonsoft.Json.JsonWriter::set_StringEscapeHandling(Newtonsoft.Json.StringEscapeHandling)
extern void JsonWriter_set_StringEscapeHandling_m02C95A592E7E217963FEB61C85555751805F4C06 (void);
// 0x00000143 System.Void Newtonsoft.Json.JsonWriter::OnStringEscapeHandlingChanged()
extern void JsonWriter_OnStringEscapeHandlingChanged_mC3CF4D26C7EAC54AA304922EA8D39061398B7F27 (void);
// 0x00000144 Newtonsoft.Json.FloatFormatHandling Newtonsoft.Json.JsonWriter::get_FloatFormatHandling()
extern void JsonWriter_get_FloatFormatHandling_mD88C6F4D05A2BA565E093D2D864537B3BB78FF52 (void);
// 0x00000145 System.Void Newtonsoft.Json.JsonWriter::set_FloatFormatHandling(Newtonsoft.Json.FloatFormatHandling)
extern void JsonWriter_set_FloatFormatHandling_m0AED0049F5DCE536ED7C5B9D32EA996CC6B6D8A1 (void);
// 0x00000146 System.String Newtonsoft.Json.JsonWriter::get_DateFormatString()
extern void JsonWriter_get_DateFormatString_m69A32477FF1B1AF8DF997B4D018C24BD5825FCCB (void);
// 0x00000147 System.Void Newtonsoft.Json.JsonWriter::set_DateFormatString(System.String)
extern void JsonWriter_set_DateFormatString_mF4ECA8F9127550A893C594291DBFBB0BA1B31F3F (void);
// 0x00000148 System.Globalization.CultureInfo Newtonsoft.Json.JsonWriter::get_Culture()
extern void JsonWriter_get_Culture_mC42B914B29D7FEC2B0F98DE7AA32F289DB318F89 (void);
// 0x00000149 System.Void Newtonsoft.Json.JsonWriter::set_Culture(System.Globalization.CultureInfo)
extern void JsonWriter_set_Culture_mDEE561B11776B97A927F1300FF7B6A3ADE81B2CA (void);
// 0x0000014A System.Void Newtonsoft.Json.JsonWriter::.ctor()
extern void JsonWriter__ctor_m831946FD2352BD57949E806795F738B790BF4471 (void);
// 0x0000014B System.Void Newtonsoft.Json.JsonWriter::UpdateScopeWithFinishedValue()
extern void JsonWriter_UpdateScopeWithFinishedValue_m1494B1E2F01253905C5F6C856042DAD4293841CF (void);
// 0x0000014C System.Void Newtonsoft.Json.JsonWriter::Push(Newtonsoft.Json.JsonContainerType)
extern void JsonWriter_Push_mFE69FC6187F3DBC82D5BD164CB241978F05E602A (void);
// 0x0000014D Newtonsoft.Json.JsonContainerType Newtonsoft.Json.JsonWriter::Pop()
extern void JsonWriter_Pop_m057A476783F0ABA0D103DE3F7A9A9D42770BC7A4 (void);
// 0x0000014E Newtonsoft.Json.JsonContainerType Newtonsoft.Json.JsonWriter::Peek()
extern void JsonWriter_Peek_mA9174FC2495768EA21224B1487DC920BFB013E24 (void);
// 0x0000014F System.Void Newtonsoft.Json.JsonWriter::Close()
extern void JsonWriter_Close_mB691844CE67B0DF402BBB9D373F9031EC8A441CD (void);
// 0x00000150 System.Void Newtonsoft.Json.JsonWriter::WriteStartObject()
extern void JsonWriter_WriteStartObject_mAD048398098C2F44099A1479284A011012215F5D (void);
// 0x00000151 System.Void Newtonsoft.Json.JsonWriter::WriteEndObject()
extern void JsonWriter_WriteEndObject_mFA72AE0AA3FC180698A8D2B9BFD54799EF9E5BEB (void);
// 0x00000152 System.Void Newtonsoft.Json.JsonWriter::WriteStartArray()
extern void JsonWriter_WriteStartArray_m645721F97E9CEEE7E587FDF935E35A6E7BBF3292 (void);
// 0x00000153 System.Void Newtonsoft.Json.JsonWriter::WriteEndArray()
extern void JsonWriter_WriteEndArray_m9CD3DFB809FDFAD3C5F607A6E06B41ED4F33CA7B (void);
// 0x00000154 System.Void Newtonsoft.Json.JsonWriter::WriteStartConstructor(System.String)
extern void JsonWriter_WriteStartConstructor_mA7E763E836F04492AF16B86F6675F2BC1584206E (void);
// 0x00000155 System.Void Newtonsoft.Json.JsonWriter::WriteEndConstructor()
extern void JsonWriter_WriteEndConstructor_m178608E8B682B73E3E06D9C5B89A2D449A170094 (void);
// 0x00000156 System.Void Newtonsoft.Json.JsonWriter::WritePropertyName(System.String)
extern void JsonWriter_WritePropertyName_mB9C984656F23C60FB0B42602B4B6770C20758106 (void);
// 0x00000157 System.Void Newtonsoft.Json.JsonWriter::WritePropertyName(System.String,System.Boolean)
extern void JsonWriter_WritePropertyName_mE9DEA27A5A9BBAD08BC1611823ED575158A3C474 (void);
// 0x00000158 System.Void Newtonsoft.Json.JsonWriter::WriteEnd()
extern void JsonWriter_WriteEnd_m3E4EC1233787C4959480B758E529B08302C52EAD (void);
// 0x00000159 System.Void Newtonsoft.Json.JsonWriter::WriteToken(Newtonsoft.Json.JsonToken,System.Object)
extern void JsonWriter_WriteToken_m7FF2C9C3D1C060A6A42BE8580916BCF88CD85521 (void);
// 0x0000015A System.Void Newtonsoft.Json.JsonWriter::WriteToken(Newtonsoft.Json.JsonReader,System.Boolean,System.Boolean,System.Boolean)
extern void JsonWriter_WriteToken_mA6815D2FA925A2228379028960B68BA3105A6A52 (void);
// 0x0000015B System.Void Newtonsoft.Json.JsonWriter::WriteConstructorDate(Newtonsoft.Json.JsonReader)
extern void JsonWriter_WriteConstructorDate_mAB9A74738C407D103D4B9926B4D076755E5736C1 (void);
// 0x0000015C System.Void Newtonsoft.Json.JsonWriter::WriteEnd(Newtonsoft.Json.JsonContainerType)
extern void JsonWriter_WriteEnd_m13FABF850F81B9E64D2AAE495AEB139BC1DB6740 (void);
// 0x0000015D System.Void Newtonsoft.Json.JsonWriter::AutoCompleteAll()
extern void JsonWriter_AutoCompleteAll_mA2FD8E3D679166C19D7622676AA75A53343F6E86 (void);
// 0x0000015E Newtonsoft.Json.JsonToken Newtonsoft.Json.JsonWriter::GetCloseTokenForType(Newtonsoft.Json.JsonContainerType)
extern void JsonWriter_GetCloseTokenForType_m9CB97B7C53811B52453B193B402AF3EA8B81BA92 (void);
// 0x0000015F System.Void Newtonsoft.Json.JsonWriter::AutoCompleteClose(Newtonsoft.Json.JsonContainerType)
extern void JsonWriter_AutoCompleteClose_mE666286DF09F35E57BE286544C755288171FB1F7 (void);
// 0x00000160 System.Void Newtonsoft.Json.JsonWriter::WriteEnd(Newtonsoft.Json.JsonToken)
extern void JsonWriter_WriteEnd_mDFDECDE31664E9BC21FB6B17B9E5E1529CE49A54 (void);
// 0x00000161 System.Void Newtonsoft.Json.JsonWriter::WriteIndent()
extern void JsonWriter_WriteIndent_m9E01216775D39ADF22265B3A972095B9F9EE53E0 (void);
// 0x00000162 System.Void Newtonsoft.Json.JsonWriter::WriteValueDelimiter()
extern void JsonWriter_WriteValueDelimiter_m699B96BC6B068A5DE28CFB0DBA18E73443EB01AE (void);
// 0x00000163 System.Void Newtonsoft.Json.JsonWriter::WriteIndentSpace()
extern void JsonWriter_WriteIndentSpace_m8E466D77C850ADA436D8EB1BE0842853BBDF2456 (void);
// 0x00000164 System.Void Newtonsoft.Json.JsonWriter::AutoComplete(Newtonsoft.Json.JsonToken)
extern void JsonWriter_AutoComplete_mF0F05182DCE58A502FCA9886491D021C5BE77891 (void);
// 0x00000165 System.Void Newtonsoft.Json.JsonWriter::WriteNull()
extern void JsonWriter_WriteNull_m88B5205E33D80E0D45B4401511113EC6712DCF89 (void);
// 0x00000166 System.Void Newtonsoft.Json.JsonWriter::WriteUndefined()
extern void JsonWriter_WriteUndefined_m3B0651930350243F73ACE48B331FEB7255B80878 (void);
// 0x00000167 System.Void Newtonsoft.Json.JsonWriter::WriteRaw(System.String)
extern void JsonWriter_WriteRaw_mF11816DF21E2285E0E6E6E51440290572C1E8BFC (void);
// 0x00000168 System.Void Newtonsoft.Json.JsonWriter::WriteRawValue(System.String)
extern void JsonWriter_WriteRawValue_m39ABA99A893489F45A5C8205B839DB411A85CDA1 (void);
// 0x00000169 System.Void Newtonsoft.Json.JsonWriter::WriteValue(System.String)
extern void JsonWriter_WriteValue_mB0E1A92D2F72F3463065BEF9972FB452631C08F6 (void);
// 0x0000016A System.Void Newtonsoft.Json.JsonWriter::WriteValue(System.Int32)
extern void JsonWriter_WriteValue_m1FEFF250C239D3D1837EC5BCA4AFBFFC5D6E0D34 (void);
// 0x0000016B System.Void Newtonsoft.Json.JsonWriter::WriteValue(System.UInt32)
extern void JsonWriter_WriteValue_m9B2A0C880487EFEE143F0A2F0282A24F58E39BB6 (void);
// 0x0000016C System.Void Newtonsoft.Json.JsonWriter::WriteValue(System.Int64)
extern void JsonWriter_WriteValue_m93F9CEFC9ED2EF7A9449B780DEB568D44430F72F (void);
// 0x0000016D System.Void Newtonsoft.Json.JsonWriter::WriteValue(System.UInt64)
extern void JsonWriter_WriteValue_m3B79659B76CE197EA62C98D9380CA276779BA041 (void);
// 0x0000016E System.Void Newtonsoft.Json.JsonWriter::WriteValue(System.Single)
extern void JsonWriter_WriteValue_mBAD37C743F5E6B0E0CF3AD23B6C475BF651E7CCF (void);
// 0x0000016F System.Void Newtonsoft.Json.JsonWriter::WriteValue(System.Double)
extern void JsonWriter_WriteValue_m3ECA9D982522775E820520C0DEF96D5A10A7ABE9 (void);
// 0x00000170 System.Void Newtonsoft.Json.JsonWriter::WriteValue(System.Boolean)
extern void JsonWriter_WriteValue_m23C293A28ADF73EAC984FBFA034E995CA39C96C8 (void);
// 0x00000171 System.Void Newtonsoft.Json.JsonWriter::WriteValue(System.Int16)
extern void JsonWriter_WriteValue_mC2E8EEA42CBCEF2CE3D63B1CC50EDC4029060DB0 (void);
// 0x00000172 System.Void Newtonsoft.Json.JsonWriter::WriteValue(System.UInt16)
extern void JsonWriter_WriteValue_mD04367BAD3BA9FC4D4991C0BA35B49DA15675F96 (void);
// 0x00000173 System.Void Newtonsoft.Json.JsonWriter::WriteValue(System.Char)
extern void JsonWriter_WriteValue_m248832B7521A3E12DC721F129DCADA8A5E6B0D64 (void);
// 0x00000174 System.Void Newtonsoft.Json.JsonWriter::WriteValue(System.Byte)
extern void JsonWriter_WriteValue_m97D7CA12924339EEDB30C971A6C94F93A8C430FE (void);
// 0x00000175 System.Void Newtonsoft.Json.JsonWriter::WriteValue(System.SByte)
extern void JsonWriter_WriteValue_m402DBD63BF18B24EAA0D34D7D6208CE8C696F3AF (void);
// 0x00000176 System.Void Newtonsoft.Json.JsonWriter::WriteValue(System.Decimal)
extern void JsonWriter_WriteValue_mC37C52C1891FE1988D9C92D8D55BB9E890B1B6ED (void);
// 0x00000177 System.Void Newtonsoft.Json.JsonWriter::WriteValue(System.DateTime)
extern void JsonWriter_WriteValue_m65ADF4B7E727362C28EE741700A6B1B070379672 (void);
// 0x00000178 System.Void Newtonsoft.Json.JsonWriter::WriteValue(System.DateTimeOffset)
extern void JsonWriter_WriteValue_m0E6A04360432620F55CC03FF7E274ECF8171CD33 (void);
// 0x00000179 System.Void Newtonsoft.Json.JsonWriter::WriteValue(System.Guid)
extern void JsonWriter_WriteValue_m82BA7513906CB1637720C0209867F2542A6B84D0 (void);
// 0x0000017A System.Void Newtonsoft.Json.JsonWriter::WriteValue(System.TimeSpan)
extern void JsonWriter_WriteValue_m1BFD61E3B8431D93875DED43BDBA437CF2E43345 (void);
// 0x0000017B System.Void Newtonsoft.Json.JsonWriter::WriteValue(System.Nullable`1<System.Int32>)
extern void JsonWriter_WriteValue_mA4A58068231556A9AA3B9BD4A0797B178D687B9F (void);
// 0x0000017C System.Void Newtonsoft.Json.JsonWriter::WriteValue(System.Nullable`1<System.UInt32>)
extern void JsonWriter_WriteValue_m55DD87A8EFEBB6A73C70571592F6F2B63DFED99D (void);
// 0x0000017D System.Void Newtonsoft.Json.JsonWriter::WriteValue(System.Nullable`1<System.Int64>)
extern void JsonWriter_WriteValue_m54BE6FA02D5EDB12862AA0B8E8A58D4525649E88 (void);
// 0x0000017E System.Void Newtonsoft.Json.JsonWriter::WriteValue(System.Nullable`1<System.UInt64>)
extern void JsonWriter_WriteValue_mE256A509ADFD1D2EB3C2CE59C48BD4B5F9A7D075 (void);
// 0x0000017F System.Void Newtonsoft.Json.JsonWriter::WriteValue(System.Nullable`1<System.Single>)
extern void JsonWriter_WriteValue_m57C10991081DF13719790B2CB6F91A527F2FFF1A (void);
// 0x00000180 System.Void Newtonsoft.Json.JsonWriter::WriteValue(System.Nullable`1<System.Double>)
extern void JsonWriter_WriteValue_mEBECAA46B25096965E7D208F3923AA99F1561B6A (void);
// 0x00000181 System.Void Newtonsoft.Json.JsonWriter::WriteValue(System.Nullable`1<System.Boolean>)
extern void JsonWriter_WriteValue_mE53D6A58FE531435115CB256CF213DBA4FDDAA66 (void);
// 0x00000182 System.Void Newtonsoft.Json.JsonWriter::WriteValue(System.Nullable`1<System.Int16>)
extern void JsonWriter_WriteValue_m22EF969D9C1055345309F4A0D50126DB5916A56C (void);
// 0x00000183 System.Void Newtonsoft.Json.JsonWriter::WriteValue(System.Nullable`1<System.UInt16>)
extern void JsonWriter_WriteValue_m497A20806672585D0FBD37DF1707485B294C5C3F (void);
// 0x00000184 System.Void Newtonsoft.Json.JsonWriter::WriteValue(System.Nullable`1<System.Char>)
extern void JsonWriter_WriteValue_m60EFE07D40ABA81ACB9FC5A48C32C653B269B89B (void);
// 0x00000185 System.Void Newtonsoft.Json.JsonWriter::WriteValue(System.Nullable`1<System.Byte>)
extern void JsonWriter_WriteValue_mAC90C70E3B38A618B91D7FDDF8F4C4CD9BD59FB9 (void);
// 0x00000186 System.Void Newtonsoft.Json.JsonWriter::WriteValue(System.Nullable`1<System.SByte>)
extern void JsonWriter_WriteValue_m06116AF904E6923B19234D2787602E24995B8F7E (void);
// 0x00000187 System.Void Newtonsoft.Json.JsonWriter::WriteValue(System.Nullable`1<System.Decimal>)
extern void JsonWriter_WriteValue_m9148650421E38CD2C050395BB8F34E5E3098D1F6 (void);
// 0x00000188 System.Void Newtonsoft.Json.JsonWriter::WriteValue(System.Nullable`1<System.DateTime>)
extern void JsonWriter_WriteValue_m194E49E503F3ACFE01B7ED699643EB75F6B95706 (void);
// 0x00000189 System.Void Newtonsoft.Json.JsonWriter::WriteValue(System.Nullable`1<System.DateTimeOffset>)
extern void JsonWriter_WriteValue_mA87A6026985736F0B036F5FA43C1549838EAF782 (void);
// 0x0000018A System.Void Newtonsoft.Json.JsonWriter::WriteValue(System.Nullable`1<System.Guid>)
extern void JsonWriter_WriteValue_mDF994376EDAD173BD0203B6B780EB86CD283920D (void);
// 0x0000018B System.Void Newtonsoft.Json.JsonWriter::WriteValue(System.Nullable`1<System.TimeSpan>)
extern void JsonWriter_WriteValue_mBD6E128DC5F21B7C3A771BCE25458698ED5EF670 (void);
// 0x0000018C System.Void Newtonsoft.Json.JsonWriter::WriteValue(System.Byte[])
extern void JsonWriter_WriteValue_mEEE2D6400B690F240DC6E8B58AD3613DD6719821 (void);
// 0x0000018D System.Void Newtonsoft.Json.JsonWriter::WriteValue(System.Uri)
extern void JsonWriter_WriteValue_mE84B57D0BC684CB7BEA071587B6E5F04DAB3240F (void);
// 0x0000018E System.Void Newtonsoft.Json.JsonWriter::WriteComment(System.String)
extern void JsonWriter_WriteComment_m40A51166C1CABA6F1F5637A5373F2503C91083BC (void);
// 0x0000018F System.Void Newtonsoft.Json.JsonWriter::System.IDisposable.Dispose()
extern void JsonWriter_System_IDisposable_Dispose_m8287E4A9B3A83014642A1815081136A31DA7ABE9 (void);
// 0x00000190 System.Void Newtonsoft.Json.JsonWriter::Dispose(System.Boolean)
extern void JsonWriter_Dispose_mABF23CF5D09E87B6917E6BE893D147877CB63F37 (void);
// 0x00000191 System.Void Newtonsoft.Json.JsonWriter::WriteValue(Newtonsoft.Json.JsonWriter,Newtonsoft.Json.Utilities.PrimitiveTypeCode,System.Object)
extern void JsonWriter_WriteValue_m4A98E7D7B10B3C530016466AEA474B56F4D566C5 (void);
// 0x00000192 Newtonsoft.Json.JsonWriterException Newtonsoft.Json.JsonWriter::CreateUnsupportedTypeException(Newtonsoft.Json.JsonWriter,System.Object)
extern void JsonWriter_CreateUnsupportedTypeException_m398B23C6D40B7290827B4AE2C9557DEC38491AEC (void);
// 0x00000193 System.Void Newtonsoft.Json.JsonWriter::InternalWriteEnd(Newtonsoft.Json.JsonContainerType)
extern void JsonWriter_InternalWriteEnd_mB5E21FFBB1CEE513707B5711972050EDD83D3D56 (void);
// 0x00000194 System.Void Newtonsoft.Json.JsonWriter::InternalWritePropertyName(System.String)
extern void JsonWriter_InternalWritePropertyName_m7C0D3E30B796AD03833B954BFB91E2DE7C63198A (void);
// 0x00000195 System.Void Newtonsoft.Json.JsonWriter::InternalWriteRaw()
extern void JsonWriter_InternalWriteRaw_mA0A1CD55303C5E56AB15BCE03B8BE1D08B9E0D26 (void);
// 0x00000196 System.Void Newtonsoft.Json.JsonWriter::InternalWriteStart(Newtonsoft.Json.JsonToken,Newtonsoft.Json.JsonContainerType)
extern void JsonWriter_InternalWriteStart_m8E13CAC34A8FCC7BD4E858C9D30D080B3C986FF6 (void);
// 0x00000197 System.Void Newtonsoft.Json.JsonWriter::InternalWriteValue(Newtonsoft.Json.JsonToken)
extern void JsonWriter_InternalWriteValue_m28397BF3152152BC8AE2D8CC9A864E87D66FDC48 (void);
// 0x00000198 System.Void Newtonsoft.Json.JsonWriter::InternalWriteComment()
extern void JsonWriter_InternalWriteComment_m5093DCECEAC4712591E4E75168FB90351CEEE8FF (void);
// 0x00000199 System.Void Newtonsoft.Json.JsonWriterException::set_Path(System.String)
extern void JsonWriterException_set_Path_mD53E9F4BDDD8ED20EB04BF6CCE733F2DCC593BD9 (void);
// 0x0000019A System.Void Newtonsoft.Json.JsonWriterException::.ctor()
extern void JsonWriterException__ctor_m8055EB32091B663257632E594A774CB5FEF38E3E (void);
// 0x0000019B System.Void Newtonsoft.Json.JsonWriterException::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern void JsonWriterException__ctor_m4B0DB597DB75DB8DCA879D8E6FFFD1EBD79092CC (void);
// 0x0000019C System.Void Newtonsoft.Json.JsonWriterException::.ctor(System.String,System.Exception,System.String)
extern void JsonWriterException__ctor_mF68891823167959E702D8AEAF9D77F35B1F7F849 (void);
// 0x0000019D Newtonsoft.Json.JsonWriterException Newtonsoft.Json.JsonWriterException::Create(Newtonsoft.Json.JsonWriter,System.String,System.Exception)
extern void JsonWriterException_Create_m0D1BE0A49EF6360C1596523F912D44A69790881E (void);
// 0x0000019E Newtonsoft.Json.JsonWriterException Newtonsoft.Json.JsonWriterException::Create(System.String,System.String,System.Exception)
extern void JsonWriterException_Create_mD8005E323C3C20A43F1F7563F3D6925FCD2DCCCC (void);
// 0x0000019F System.Void Newtonsoft.Json.Utilities.Base64Encoder::.ctor(System.IO.TextWriter)
extern void Base64Encoder__ctor_m65B2F1A0C73150DF84B2BFB890BA113A660FB2B9 (void);
// 0x000001A0 System.Void Newtonsoft.Json.Utilities.Base64Encoder::Encode(System.Byte[],System.Int32,System.Int32)
extern void Base64Encoder_Encode_mECE2AF7E926D851B15D734BAC400092B916F3981 (void);
// 0x000001A1 System.Void Newtonsoft.Json.Utilities.Base64Encoder::Flush()
extern void Base64Encoder_Flush_mA3BCB12F40C04027A0C9F52EDFFB3CE2E33ACAC6 (void);
// 0x000001A2 System.Void Newtonsoft.Json.Utilities.Base64Encoder::WriteChars(System.Char[],System.Int32,System.Int32)
extern void Base64Encoder_WriteChars_m9E49BFFC9056F93A431C04AB0DC4698DDCD36B37 (void);
// 0x000001A3 System.Void Newtonsoft.Json.Utilities.BidirectionalDictionary`2::.ctor(System.Collections.Generic.IEqualityComparer`1<TFirst>,System.Collections.Generic.IEqualityComparer`1<TSecond>)
// 0x000001A4 System.Void Newtonsoft.Json.Utilities.BidirectionalDictionary`2::.ctor(System.Collections.Generic.IEqualityComparer`1<TFirst>,System.Collections.Generic.IEqualityComparer`1<TSecond>,System.String,System.String)
// 0x000001A5 System.Void Newtonsoft.Json.Utilities.BidirectionalDictionary`2::Set(TFirst,TSecond)
// 0x000001A6 System.Boolean Newtonsoft.Json.Utilities.BidirectionalDictionary`2::TryGetByFirst(TFirst,TSecond&)
// 0x000001A7 System.Boolean Newtonsoft.Json.Utilities.BidirectionalDictionary`2::TryGetBySecond(TSecond,TFirst&)
// 0x000001A8 System.Boolean Newtonsoft.Json.Utilities.CollectionUtils::IsNullOrEmpty(System.Collections.Generic.ICollection`1<T>)
// 0x000001A9 System.Void Newtonsoft.Json.Utilities.CollectionUtils::AddRange(System.Collections.Generic.IList`1<T>,System.Collections.Generic.IEnumerable`1<T>)
// 0x000001AA System.Void Newtonsoft.Json.Utilities.CollectionUtils::AddRange(System.Collections.Generic.IList`1<T>,System.Collections.IEnumerable)
// 0x000001AB System.Boolean Newtonsoft.Json.Utilities.CollectionUtils::IsDictionaryType(System.Type)
extern void CollectionUtils_IsDictionaryType_m54F454CB807B6A4D6207FEBAB94AE3746BEFE13E (void);
// 0x000001AC System.Reflection.ConstructorInfo Newtonsoft.Json.Utilities.CollectionUtils::ResolveEnumerableCollectionConstructor(System.Type,System.Type)
extern void CollectionUtils_ResolveEnumerableCollectionConstructor_m78C54F3D43C7870013DAB6566A0974AB43072C84 (void);
// 0x000001AD System.Reflection.ConstructorInfo Newtonsoft.Json.Utilities.CollectionUtils::ResolveEnumerableCollectionConstructor(System.Type,System.Type,System.Type)
extern void CollectionUtils_ResolveEnumerableCollectionConstructor_m0B20B7714B1556A1CA80EA2F2D53F0198C1A5752 (void);
// 0x000001AE System.Int32 Newtonsoft.Json.Utilities.CollectionUtils::IndexOf(System.Collections.Generic.IEnumerable`1<T>,System.Func`2<T,System.Boolean>)
// 0x000001AF System.Boolean Newtonsoft.Json.Utilities.CollectionUtils::Contains(System.Collections.Generic.List`1<T>,T,System.Collections.IEqualityComparer)
// 0x000001B0 System.Collections.Generic.IList`1<System.Int32> Newtonsoft.Json.Utilities.CollectionUtils::GetDimensions(System.Collections.IList,System.Int32)
extern void CollectionUtils_GetDimensions_m4F2292D6D4C8351343188E6EAC0210F7B649170A (void);
// 0x000001B1 System.Void Newtonsoft.Json.Utilities.CollectionUtils::CopyFromJaggedToMultidimensionalArray(System.Collections.IList,System.Array,System.Int32[])
extern void CollectionUtils_CopyFromJaggedToMultidimensionalArray_m91BBE92C8DBE92331B859CCEDB7CCD34356E4497 (void);
// 0x000001B2 System.Object Newtonsoft.Json.Utilities.CollectionUtils::JaggedArrayGetValue(System.Collections.IList,System.Int32[])
extern void CollectionUtils_JaggedArrayGetValue_m9008D23A3AB4C82A795490D4804CF0B9D35B8855 (void);
// 0x000001B3 System.Array Newtonsoft.Json.Utilities.CollectionUtils::ToMultidimensionalArray(System.Collections.IList,System.Type,System.Int32)
extern void CollectionUtils_ToMultidimensionalArray_m99EF2F761503F27C1620927479D454B88CA1E0A4 (void);
// 0x000001B4 System.Object Newtonsoft.Json.Utilities.IWrappedCollection::get_UnderlyingCollection()
// 0x000001B5 System.Type Newtonsoft.Json.Utilities.TypeInformation::get_Type()
extern void TypeInformation_get_Type_mF16FF676DAA9DEC81AF0A9FB81DA0834709F1F9D (void);
// 0x000001B6 System.Void Newtonsoft.Json.Utilities.TypeInformation::set_Type(System.Type)
extern void TypeInformation_set_Type_m87F6F17BD4A47C4F020DBC8E25462D3F4651B750 (void);
// 0x000001B7 Newtonsoft.Json.Utilities.PrimitiveTypeCode Newtonsoft.Json.Utilities.TypeInformation::get_TypeCode()
extern void TypeInformation_get_TypeCode_m3F9374AB9A57BA647C5B53B633D16102723C8435 (void);
// 0x000001B8 System.Void Newtonsoft.Json.Utilities.TypeInformation::set_TypeCode(Newtonsoft.Json.Utilities.PrimitiveTypeCode)
extern void TypeInformation_set_TypeCode_mD82AA32EF804000604122B9541C621AF00622767 (void);
// 0x000001B9 System.Void Newtonsoft.Json.Utilities.TypeInformation::.ctor()
extern void TypeInformation__ctor_m957F0EFDD41CC462F8CA296702E33F67DBB3CB98 (void);
// 0x000001BA Newtonsoft.Json.Utilities.PrimitiveTypeCode Newtonsoft.Json.Utilities.ConvertUtils::GetTypeCode(System.Type)
extern void ConvertUtils_GetTypeCode_m5FC5A22E9C4DBCA27C5543DD30AC662CDF9F11E7 (void);
// 0x000001BB Newtonsoft.Json.Utilities.PrimitiveTypeCode Newtonsoft.Json.Utilities.ConvertUtils::GetTypeCode(System.Type,System.Boolean&)
extern void ConvertUtils_GetTypeCode_mD6AD89EBDDB14DBE10A270A17D8E7CA936004913 (void);
// 0x000001BC Newtonsoft.Json.Utilities.TypeInformation Newtonsoft.Json.Utilities.ConvertUtils::GetTypeInformation(System.IConvertible)
extern void ConvertUtils_GetTypeInformation_m0B253C7635BD26C35A9F144129811957E60BDC70 (void);
// 0x000001BD System.Boolean Newtonsoft.Json.Utilities.ConvertUtils::IsConvertible(System.Type)
extern void ConvertUtils_IsConvertible_m5DF9914844429A628A3B0E46D52F8F28998FE399 (void);
// 0x000001BE System.TimeSpan Newtonsoft.Json.Utilities.ConvertUtils::ParseTimeSpan(System.String)
extern void ConvertUtils_ParseTimeSpan_m6176F121BB315FEB849C44D9560C0792184210FE (void);
// 0x000001BF System.Func`2<System.Object,System.Object> Newtonsoft.Json.Utilities.ConvertUtils::CreateCastConverter(Newtonsoft.Json.Utilities.ConvertUtils/TypeConvertKey)
extern void ConvertUtils_CreateCastConverter_m302B3ABDB587A51DA380B2892A6F4314C1258491 (void);
// 0x000001C0 System.Boolean Newtonsoft.Json.Utilities.ConvertUtils::TryConvert(System.Object,System.Globalization.CultureInfo,System.Type,System.Object&)
extern void ConvertUtils_TryConvert_m1B5389E14C0AEB4373E66D9BDDAE5FF4C499FAB1 (void);
// 0x000001C1 Newtonsoft.Json.Utilities.ConvertUtils/ConvertResult Newtonsoft.Json.Utilities.ConvertUtils::TryConvertInternal(System.Object,System.Globalization.CultureInfo,System.Type,System.Object&)
extern void ConvertUtils_TryConvertInternal_mE463A84642C8EBFAD6843BA9AB3BC88533D4E40B (void);
// 0x000001C2 System.Object Newtonsoft.Json.Utilities.ConvertUtils::ConvertOrCast(System.Object,System.Globalization.CultureInfo,System.Type)
extern void ConvertUtils_ConvertOrCast_m7862E65C180FF18A2F4847F8051C181290AEDDA8 (void);
// 0x000001C3 System.Object Newtonsoft.Json.Utilities.ConvertUtils::EnsureTypeAssignable(System.Object,System.Type,System.Type)
extern void ConvertUtils_EnsureTypeAssignable_mAF9E91775390D694C4C02C88173F40942FD26531 (void);
// 0x000001C4 System.ComponentModel.TypeConverter Newtonsoft.Json.Utilities.ConvertUtils::GetConverter(System.Type)
extern void ConvertUtils_GetConverter_mAE05CA2EEC6E063E2108C8B9167AB891384FA2CC (void);
// 0x000001C5 System.Boolean Newtonsoft.Json.Utilities.ConvertUtils::VersionTryParse(System.String,System.Version&)
extern void ConvertUtils_VersionTryParse_mA4AC347AF3AE558B9FBA3FD042760A7648A26DEF (void);
// 0x000001C6 System.Boolean Newtonsoft.Json.Utilities.ConvertUtils::IsInteger(System.Object)
extern void ConvertUtils_IsInteger_m99C9EEA9B9D813CFBA8A9CE54DA9C7373C2697A6 (void);
// 0x000001C7 Newtonsoft.Json.Utilities.ParseResult Newtonsoft.Json.Utilities.ConvertUtils::Int32TryParse(System.Char[],System.Int32,System.Int32,System.Int32&)
extern void ConvertUtils_Int32TryParse_m766BAB9862F8082B0B3953727DF54CFF80F23729 (void);
// 0x000001C8 Newtonsoft.Json.Utilities.ParseResult Newtonsoft.Json.Utilities.ConvertUtils::Int64TryParse(System.Char[],System.Int32,System.Int32,System.Int64&)
extern void ConvertUtils_Int64TryParse_m7995B30A21526FA60DC9EDCE687B69DC9D99B57B (void);
// 0x000001C9 System.Boolean Newtonsoft.Json.Utilities.ConvertUtils::TryConvertGuid(System.String,System.Guid&)
extern void ConvertUtils_TryConvertGuid_m7FB11DA604AA7E5814533E36F6F7BB2263D5EE27 (void);
// 0x000001CA System.Int32 Newtonsoft.Json.Utilities.ConvertUtils::HexTextToInt(System.Char[],System.Int32,System.Int32)
extern void ConvertUtils_HexTextToInt_mC7D7166598DD76A6794E4B833FD5DDD9C59ADEC0 (void);
// 0x000001CB System.Int32 Newtonsoft.Json.Utilities.ConvertUtils::HexCharToInt(System.Char)
extern void ConvertUtils_HexCharToInt_m849061D4FE85890E633A0CC2DC7980BE21E39DF4 (void);
// 0x000001CC System.Void Newtonsoft.Json.Utilities.ConvertUtils::.cctor()
extern void ConvertUtils__cctor_m7614D53BF4BD500E428C7DFB054047312CD4E49F (void);
// 0x000001CD System.Type Newtonsoft.Json.Utilities.ConvertUtils/TypeConvertKey::get_InitialType()
extern void TypeConvertKey_get_InitialType_m0F65A2432EB92CAA1377B113C1BAEF2C0283F634 (void);
// 0x000001CE System.Type Newtonsoft.Json.Utilities.ConvertUtils/TypeConvertKey::get_TargetType()
extern void TypeConvertKey_get_TargetType_m8F2288FCB1DB8039CEE75810A72EC31B0085805D (void);
// 0x000001CF System.Void Newtonsoft.Json.Utilities.ConvertUtils/TypeConvertKey::.ctor(System.Type,System.Type)
extern void TypeConvertKey__ctor_mAE2DD8A2A907D194638D666DED0AAC9FF3AA9E48 (void);
// 0x000001D0 System.Int32 Newtonsoft.Json.Utilities.ConvertUtils/TypeConvertKey::GetHashCode()
extern void TypeConvertKey_GetHashCode_mC12EC356438B1840646DBA4ADEB7F6EB4AFDE894 (void);
// 0x000001D1 System.Boolean Newtonsoft.Json.Utilities.ConvertUtils/TypeConvertKey::Equals(System.Object)
extern void TypeConvertKey_Equals_m4DE5600EBFEAFAA076537C2075A96573AB87D2BE (void);
// 0x000001D2 System.Boolean Newtonsoft.Json.Utilities.ConvertUtils/TypeConvertKey::Equals(Newtonsoft.Json.Utilities.ConvertUtils/TypeConvertKey)
extern void TypeConvertKey_Equals_mCCE90DDED2DF5DA439CDA3277A1BBB36E6E94C13 (void);
// 0x000001D3 System.Void Newtonsoft.Json.Utilities.ConvertUtils/<>c__DisplayClass9_0::.ctor()
extern void U3CU3Ec__DisplayClass9_0__ctor_mEE0F27194D15971B9E25917E60E41FD430A3E92B (void);
// 0x000001D4 System.Object Newtonsoft.Json.Utilities.ConvertUtils/<>c__DisplayClass9_0::<CreateCastConverter>b__0(System.Object)
extern void U3CU3Ec__DisplayClass9_0_U3CCreateCastConverterU3Eb__0_mABC83D0C63CE3EBCE00AA99596C34DECE06E40A6 (void);
// 0x000001D5 System.Void Newtonsoft.Json.Utilities.DateTimeParser::.cctor()
extern void DateTimeParser__cctor_mB2B30EBF44E082346347244B34D188E60294558B (void);
// 0x000001D6 System.Boolean Newtonsoft.Json.Utilities.DateTimeParser::Parse(System.Char[],System.Int32,System.Int32)
extern void DateTimeParser_Parse_m64820EE59B4BB49B549346281AD8B2EF330B3EDD (void);
// 0x000001D7 System.Boolean Newtonsoft.Json.Utilities.DateTimeParser::ParseDate(System.Int32)
extern void DateTimeParser_ParseDate_m1A8649D480733E2FFD4F24827B2FC6C951C069E7 (void);
// 0x000001D8 System.Boolean Newtonsoft.Json.Utilities.DateTimeParser::ParseTimeAndZoneAndWhitespace(System.Int32)
extern void DateTimeParser_ParseTimeAndZoneAndWhitespace_mD103AD070AF0E15C3783EA88A21A1C71732FEF70 (void);
// 0x000001D9 System.Boolean Newtonsoft.Json.Utilities.DateTimeParser::ParseTime(System.Int32&)
extern void DateTimeParser_ParseTime_mB6558BEA3B81EB444F3FF78CD7288979EF071D20 (void);
// 0x000001DA System.Boolean Newtonsoft.Json.Utilities.DateTimeParser::ParseZone(System.Int32)
extern void DateTimeParser_ParseZone_mC630CC51579EADDAA8D595F081F89E5FEB7977CE (void);
// 0x000001DB System.Boolean Newtonsoft.Json.Utilities.DateTimeParser::Parse4Digit(System.Int32,System.Int32&)
extern void DateTimeParser_Parse4Digit_mFE4C2AE33BC9C3EACC29964487AE45CB86D66DDA (void);
// 0x000001DC System.Boolean Newtonsoft.Json.Utilities.DateTimeParser::Parse2Digit(System.Int32,System.Int32&)
extern void DateTimeParser_Parse2Digit_m7D1B3E711C5BB1704A70ABB399E0711AF064234E (void);
// 0x000001DD System.Boolean Newtonsoft.Json.Utilities.DateTimeParser::ParseChar(System.Int32,System.Char)
extern void DateTimeParser_ParseChar_m510297FF2814F39BC1DF2AFF91D2556AE81B7E5A (void);
// 0x000001DE System.Void Newtonsoft.Json.Utilities.DateTimeUtils::.cctor()
extern void DateTimeUtils__cctor_mE72C26CEC9865469DAE34645B975F197730E78CF (void);
// 0x000001DF System.TimeSpan Newtonsoft.Json.Utilities.DateTimeUtils::GetUtcOffset(System.DateTime)
extern void DateTimeUtils_GetUtcOffset_m1B6B70139EF050BAA434164D316C6619F5FE292A (void);
// 0x000001E0 System.DateTime Newtonsoft.Json.Utilities.DateTimeUtils::EnsureDateTime(System.DateTime,Newtonsoft.Json.DateTimeZoneHandling)
extern void DateTimeUtils_EnsureDateTime_m0BE481EB6EB9238EAA99E20D546F6CAE3B17FF58 (void);
// 0x000001E1 System.DateTime Newtonsoft.Json.Utilities.DateTimeUtils::SwitchToLocalTime(System.DateTime)
extern void DateTimeUtils_SwitchToLocalTime_m8DE52AE6DB4497048A98A93249103BE9A91BAAA1 (void);
// 0x000001E2 System.DateTime Newtonsoft.Json.Utilities.DateTimeUtils::SwitchToUtcTime(System.DateTime)
extern void DateTimeUtils_SwitchToUtcTime_mE273FFC22EDB60C70E63BEDFAC09A2C554020073 (void);
// 0x000001E3 System.Int64 Newtonsoft.Json.Utilities.DateTimeUtils::ToUniversalTicks(System.DateTime,System.TimeSpan)
extern void DateTimeUtils_ToUniversalTicks_m54ABC31DF2B3FFD7F10963F2AD95EBA4B7CEE434 (void);
// 0x000001E4 System.Int64 Newtonsoft.Json.Utilities.DateTimeUtils::ConvertDateTimeToJavaScriptTicks(System.DateTime,System.TimeSpan)
extern void DateTimeUtils_ConvertDateTimeToJavaScriptTicks_mD5FEACA25BA3D250F62431EBC820D6D00C977D00 (void);
// 0x000001E5 System.Int64 Newtonsoft.Json.Utilities.DateTimeUtils::UniversialTicksToJavaScriptTicks(System.Int64)
extern void DateTimeUtils_UniversialTicksToJavaScriptTicks_mF0C4425E4D7CE537E5DC6F8FF978F18F39D956B4 (void);
// 0x000001E6 System.DateTime Newtonsoft.Json.Utilities.DateTimeUtils::ConvertJavaScriptTicksToDateTime(System.Int64)
extern void DateTimeUtils_ConvertJavaScriptTicksToDateTime_mF6B797819AA97A8D120DA8B618675C262893D3B8 (void);
// 0x000001E7 System.Boolean Newtonsoft.Json.Utilities.DateTimeUtils::TryParseDateTimeIso(Newtonsoft.Json.Utilities.StringReference,Newtonsoft.Json.DateTimeZoneHandling,System.DateTime&)
extern void DateTimeUtils_TryParseDateTimeIso_mA0B1BA11F3E8A10ECDEDD2FAB44B20EBFB3C5072 (void);
// 0x000001E8 System.Boolean Newtonsoft.Json.Utilities.DateTimeUtils::TryParseDateTimeOffsetIso(Newtonsoft.Json.Utilities.StringReference,System.DateTimeOffset&)
extern void DateTimeUtils_TryParseDateTimeOffsetIso_mBBCC805DB6E217123EA673ECCB13A4FBE96DFC9A (void);
// 0x000001E9 System.DateTime Newtonsoft.Json.Utilities.DateTimeUtils::CreateDateTime(Newtonsoft.Json.Utilities.DateTimeParser)
extern void DateTimeUtils_CreateDateTime_m18FFB80C04E2A3D29E55ACC64A8B4CB9353401A0 (void);
// 0x000001EA System.Boolean Newtonsoft.Json.Utilities.DateTimeUtils::TryParseDateTime(Newtonsoft.Json.Utilities.StringReference,Newtonsoft.Json.DateTimeZoneHandling,System.String,System.Globalization.CultureInfo,System.DateTime&)
extern void DateTimeUtils_TryParseDateTime_m5E3D0A12556D749DEFCBDC464D5AE04E7298412F (void);
// 0x000001EB System.Boolean Newtonsoft.Json.Utilities.DateTimeUtils::TryParseDateTime(System.String,Newtonsoft.Json.DateTimeZoneHandling,System.String,System.Globalization.CultureInfo,System.DateTime&)
extern void DateTimeUtils_TryParseDateTime_m2A7376C659FC164B24EDD3EEA336056765F9600F (void);
// 0x000001EC System.Boolean Newtonsoft.Json.Utilities.DateTimeUtils::TryParseDateTimeOffset(Newtonsoft.Json.Utilities.StringReference,System.String,System.Globalization.CultureInfo,System.DateTimeOffset&)
extern void DateTimeUtils_TryParseDateTimeOffset_mB6E14078F4C19D99EA8194F0F99489B8F966799A (void);
// 0x000001ED System.Boolean Newtonsoft.Json.Utilities.DateTimeUtils::TryParseDateTimeOffset(System.String,System.String,System.Globalization.CultureInfo,System.DateTimeOffset&)
extern void DateTimeUtils_TryParseDateTimeOffset_m258BE951D1F277E472D9B925EA5F5BAA4130FB5C (void);
// 0x000001EE System.Boolean Newtonsoft.Json.Utilities.DateTimeUtils::TryParseMicrosoftDate(Newtonsoft.Json.Utilities.StringReference,System.Int64&,System.TimeSpan&,System.DateTimeKind&)
extern void DateTimeUtils_TryParseMicrosoftDate_m573C612CCA75A9E28B55ED65217D5A1982C94F07 (void);
// 0x000001EF System.Boolean Newtonsoft.Json.Utilities.DateTimeUtils::TryParseDateTimeMicrosoft(Newtonsoft.Json.Utilities.StringReference,Newtonsoft.Json.DateTimeZoneHandling,System.DateTime&)
extern void DateTimeUtils_TryParseDateTimeMicrosoft_m53F8407110F4954EB4D59691C0E0C1A5A66ED1A0 (void);
// 0x000001F0 System.Boolean Newtonsoft.Json.Utilities.DateTimeUtils::TryParseDateTimeExact(System.String,Newtonsoft.Json.DateTimeZoneHandling,System.String,System.Globalization.CultureInfo,System.DateTime&)
extern void DateTimeUtils_TryParseDateTimeExact_mB79BF99C8BB9AB29D718B28ECB2964C528859795 (void);
// 0x000001F1 System.Boolean Newtonsoft.Json.Utilities.DateTimeUtils::TryParseDateTimeOffsetMicrosoft(Newtonsoft.Json.Utilities.StringReference,System.DateTimeOffset&)
extern void DateTimeUtils_TryParseDateTimeOffsetMicrosoft_mAB8502A5A077608518332A134C6673D7C431B106 (void);
// 0x000001F2 System.Boolean Newtonsoft.Json.Utilities.DateTimeUtils::TryParseDateTimeOffsetExact(System.String,System.String,System.Globalization.CultureInfo,System.DateTimeOffset&)
extern void DateTimeUtils_TryParseDateTimeOffsetExact_mAB0710EB3CBD048B6AD588D848966FE735728185 (void);
// 0x000001F3 System.Boolean Newtonsoft.Json.Utilities.DateTimeUtils::TryReadOffset(Newtonsoft.Json.Utilities.StringReference,System.Int32,System.TimeSpan&)
extern void DateTimeUtils_TryReadOffset_mFCB0549B55FE27DF5528E259E74FEFBA955548A9 (void);
// 0x000001F4 System.Void Newtonsoft.Json.Utilities.DateTimeUtils::WriteDateTimeString(System.IO.TextWriter,System.DateTime,Newtonsoft.Json.DateFormatHandling,System.String,System.Globalization.CultureInfo)
extern void DateTimeUtils_WriteDateTimeString_mD6F5858607DDBD5ECF1CB53F37EBB34983690BF6 (void);
// 0x000001F5 System.Int32 Newtonsoft.Json.Utilities.DateTimeUtils::WriteDateTimeString(System.Char[],System.Int32,System.DateTime,System.Nullable`1<System.TimeSpan>,System.DateTimeKind,Newtonsoft.Json.DateFormatHandling)
extern void DateTimeUtils_WriteDateTimeString_mBF8858BAD38FC107952363A787955E262183EE84 (void);
// 0x000001F6 System.Int32 Newtonsoft.Json.Utilities.DateTimeUtils::WriteDefaultIsoDate(System.Char[],System.Int32,System.DateTime)
extern void DateTimeUtils_WriteDefaultIsoDate_m610D74BE2F21BD196EA98F6343C52F711FEAF2F7 (void);
// 0x000001F7 System.Void Newtonsoft.Json.Utilities.DateTimeUtils::CopyIntToCharArray(System.Char[],System.Int32,System.Int32,System.Int32)
extern void DateTimeUtils_CopyIntToCharArray_m9BBC30E960FE31739EBC40F34C433D9F518E2960 (void);
// 0x000001F8 System.Int32 Newtonsoft.Json.Utilities.DateTimeUtils::WriteDateTimeOffset(System.Char[],System.Int32,System.TimeSpan,Newtonsoft.Json.DateFormatHandling)
extern void DateTimeUtils_WriteDateTimeOffset_mAEC50C298C3B5660F2F5B633CFF95116A26F0B63 (void);
// 0x000001F9 System.Void Newtonsoft.Json.Utilities.DateTimeUtils::WriteDateTimeOffsetString(System.IO.TextWriter,System.DateTimeOffset,Newtonsoft.Json.DateFormatHandling,System.String,System.Globalization.CultureInfo)
extern void DateTimeUtils_WriteDateTimeOffsetString_mEB8E69D99F7742268043030B7957EE386541BF5A (void);
// 0x000001FA System.Void Newtonsoft.Json.Utilities.DateTimeUtils::GetDateValues(System.DateTime,System.Int32&,System.Int32&,System.Int32&)
extern void DateTimeUtils_GetDateValues_mAD3C75D7B22E067AF08D9263744E186FE2754A48 (void);
// 0x000001FB System.Object Newtonsoft.Json.Utilities.IWrappedDictionary::get_UnderlyingDictionary()
// 0x000001FC Newtonsoft.Json.Utilities.BidirectionalDictionary`2<System.String,System.String> Newtonsoft.Json.Utilities.EnumUtils::InitializeEnumType(System.Type)
extern void EnumUtils_InitializeEnumType_mDD8D7AB87FBF4367C4E1FE7843F8829E9DE70D89 (void);
// 0x000001FD System.Collections.Generic.IList`1<System.Object> Newtonsoft.Json.Utilities.EnumUtils::GetValues(System.Type)
extern void EnumUtils_GetValues_m8CF47AF13AE49364AF5DCBFF0BA95B1210774BF3 (void);
// 0x000001FE System.Void Newtonsoft.Json.Utilities.EnumUtils::.cctor()
extern void EnumUtils__cctor_m155775286EF750EEA58BB59F603C804858BD1A07 (void);
// 0x000001FF System.Void Newtonsoft.Json.Utilities.EnumUtils/<>c::.cctor()
extern void U3CU3Ec__cctor_mCA6120DDC165F9A19952B593D0502A71AB4398AE (void);
// 0x00000200 System.Void Newtonsoft.Json.Utilities.EnumUtils/<>c::.ctor()
extern void U3CU3Ec__ctor_m9257036A860F5D9D46AC71795DC0DC889CB519C3 (void);
// 0x00000201 System.String Newtonsoft.Json.Utilities.EnumUtils/<>c::<InitializeEnumType>b__1_0(System.Runtime.Serialization.EnumMemberAttribute)
extern void U3CU3Ec_U3CInitializeEnumTypeU3Eb__1_0_mD95ED21242B7DCD2DD87E0715E473DEE541CAE02 (void);
// 0x00000202 System.Boolean Newtonsoft.Json.Utilities.EnumUtils/<>c::<GetValues>b__5_0(System.Reflection.FieldInfo)
extern void U3CU3Ec_U3CGetValuesU3Eb__5_0_m643A368F4C078D3341829711A08DDE21CD2A3681 (void);
// 0x00000203 System.Char[] Newtonsoft.Json.Utilities.BufferUtils::RentBuffer(Newtonsoft.Json.IArrayPool`1<System.Char>,System.Int32)
extern void BufferUtils_RentBuffer_m4571362D52D6FA4B460A644F8FFA669102B842D1 (void);
// 0x00000204 System.Void Newtonsoft.Json.Utilities.BufferUtils::ReturnBuffer(Newtonsoft.Json.IArrayPool`1<System.Char>,System.Char[])
extern void BufferUtils_ReturnBuffer_m8CED72FDBE1C197A91AE32EC7C89D2A8B94FFA4D (void);
// 0x00000205 System.Char[] Newtonsoft.Json.Utilities.BufferUtils::EnsureBufferSize(Newtonsoft.Json.IArrayPool`1<System.Char>,System.Int32,System.Char[])
extern void BufferUtils_EnsureBufferSize_m8E73CC83B564A1BDB7B35413EDDCFB31D4636A30 (void);
// 0x00000206 System.Void Newtonsoft.Json.Utilities.JavaScriptUtils::.cctor()
extern void JavaScriptUtils__cctor_m7DAA61408E54EACC10FBE09BAAE48B8F02CC31EF (void);
// 0x00000207 System.Boolean[] Newtonsoft.Json.Utilities.JavaScriptUtils::GetCharEscapeFlags(Newtonsoft.Json.StringEscapeHandling,System.Char)
extern void JavaScriptUtils_GetCharEscapeFlags_mB93D0DCF197FCE0EE44262104BA6E28B6519A1AA (void);
// 0x00000208 System.Boolean Newtonsoft.Json.Utilities.JavaScriptUtils::ShouldEscapeJavaScriptString(System.String,System.Boolean[])
extern void JavaScriptUtils_ShouldEscapeJavaScriptString_m2931D6D41D46BA432BC90CC41203B8C426E46DEA (void);
// 0x00000209 System.Void Newtonsoft.Json.Utilities.JavaScriptUtils::WriteEscapedJavaScriptString(System.IO.TextWriter,System.String,System.Char,System.Boolean,System.Boolean[],Newtonsoft.Json.StringEscapeHandling,Newtonsoft.Json.IArrayPool`1<System.Char>,System.Char[]&)
extern void JavaScriptUtils_WriteEscapedJavaScriptString_m92CA93738F12AEEEA91D9BCDA3D8477FB6470373 (void);
// 0x0000020A System.String Newtonsoft.Json.Utilities.JavaScriptUtils::ToEscapedJavaScriptString(System.String,System.Char,System.Boolean,Newtonsoft.Json.StringEscapeHandling)
extern void JavaScriptUtils_ToEscapedJavaScriptString_mEAA8877D408DCC207591BD1C32CA3173F7ED6549 (void);
// 0x0000020B System.Boolean Newtonsoft.Json.Utilities.JsonTokenUtils::IsEndToken(Newtonsoft.Json.JsonToken)
extern void JsonTokenUtils_IsEndToken_m173F228C3F92E2A7CFBA1B850DF8500E89B3C25B (void);
// 0x0000020C System.Boolean Newtonsoft.Json.Utilities.JsonTokenUtils::IsStartToken(Newtonsoft.Json.JsonToken)
extern void JsonTokenUtils_IsStartToken_m27A34B7AF41C6EAE33E4886A571ADDAAFD7EFE65 (void);
// 0x0000020D System.Boolean Newtonsoft.Json.Utilities.JsonTokenUtils::IsPrimitiveToken(Newtonsoft.Json.JsonToken)
extern void JsonTokenUtils_IsPrimitiveToken_m64FF2DA221A7E6D74E41202E713FB60986928F57 (void);
// 0x0000020E Newtonsoft.Json.Utilities.ReflectionDelegateFactory Newtonsoft.Json.Utilities.LateBoundReflectionDelegateFactory::get_Instance()
extern void LateBoundReflectionDelegateFactory_get_Instance_m11EAE301361278E5C016F812D62BD96E0D25C2E5 (void);
// 0x0000020F Newtonsoft.Json.Serialization.ObjectConstructor`1<System.Object> Newtonsoft.Json.Utilities.LateBoundReflectionDelegateFactory::CreateParameterizedConstructor(System.Reflection.MethodBase)
extern void LateBoundReflectionDelegateFactory_CreateParameterizedConstructor_m0EC460EFA424E8D27D295A005E42C4ED9A5D3D0D (void);
// 0x00000210 Newtonsoft.Json.Utilities.MethodCall`2<T,System.Object> Newtonsoft.Json.Utilities.LateBoundReflectionDelegateFactory::CreateMethodCall(System.Reflection.MethodBase)
// 0x00000211 System.Func`1<T> Newtonsoft.Json.Utilities.LateBoundReflectionDelegateFactory::CreateDefaultConstructor(System.Type)
// 0x00000212 System.Func`2<T,System.Object> Newtonsoft.Json.Utilities.LateBoundReflectionDelegateFactory::CreateGet(System.Reflection.PropertyInfo)
// 0x00000213 System.Func`2<T,System.Object> Newtonsoft.Json.Utilities.LateBoundReflectionDelegateFactory::CreateGet(System.Reflection.FieldInfo)
// 0x00000214 System.Action`2<T,System.Object> Newtonsoft.Json.Utilities.LateBoundReflectionDelegateFactory::CreateSet(System.Reflection.FieldInfo)
// 0x00000215 System.Action`2<T,System.Object> Newtonsoft.Json.Utilities.LateBoundReflectionDelegateFactory::CreateSet(System.Reflection.PropertyInfo)
// 0x00000216 System.Void Newtonsoft.Json.Utilities.LateBoundReflectionDelegateFactory::.ctor()
extern void LateBoundReflectionDelegateFactory__ctor_mA7C3C8B28E78B37EECA0093CC8664E3C9FB2B1BE (void);
// 0x00000217 System.Void Newtonsoft.Json.Utilities.LateBoundReflectionDelegateFactory::.cctor()
extern void LateBoundReflectionDelegateFactory__cctor_m5E969F84987B438B4EF8C701D3DAD008309BF8D9 (void);
// 0x00000218 System.Void Newtonsoft.Json.Utilities.LateBoundReflectionDelegateFactory/<>c__DisplayClass3_0::.ctor()
extern void U3CU3Ec__DisplayClass3_0__ctor_mD2FC00D4390F4313D4BB7BDA84E21AEEC8E30905 (void);
// 0x00000219 System.Object Newtonsoft.Json.Utilities.LateBoundReflectionDelegateFactory/<>c__DisplayClass3_0::<CreateParameterizedConstructor>b__0(System.Object[])
extern void U3CU3Ec__DisplayClass3_0_U3CCreateParameterizedConstructorU3Eb__0_m3B6E3896DB55426A7BCBA2E3F4798B1B3CA9F572 (void);
// 0x0000021A System.Object Newtonsoft.Json.Utilities.LateBoundReflectionDelegateFactory/<>c__DisplayClass3_0::<CreateParameterizedConstructor>b__1(System.Object[])
extern void U3CU3Ec__DisplayClass3_0_U3CCreateParameterizedConstructorU3Eb__1_mA1EB9CF9119962212F08D067D713E1E43822B606 (void);
// 0x0000021B System.Void Newtonsoft.Json.Utilities.LateBoundReflectionDelegateFactory/<>c__DisplayClass4_0`1::.ctor()
// 0x0000021C System.Object Newtonsoft.Json.Utilities.LateBoundReflectionDelegateFactory/<>c__DisplayClass4_0`1::<CreateMethodCall>b__0(T,System.Object[])
// 0x0000021D System.Object Newtonsoft.Json.Utilities.LateBoundReflectionDelegateFactory/<>c__DisplayClass4_0`1::<CreateMethodCall>b__1(T,System.Object[])
// 0x0000021E System.Void Newtonsoft.Json.Utilities.LateBoundReflectionDelegateFactory/<>c__DisplayClass5_0`1::.ctor()
// 0x0000021F T Newtonsoft.Json.Utilities.LateBoundReflectionDelegateFactory/<>c__DisplayClass5_0`1::<CreateDefaultConstructor>b__0()
// 0x00000220 T Newtonsoft.Json.Utilities.LateBoundReflectionDelegateFactory/<>c__DisplayClass5_0`1::<CreateDefaultConstructor>b__1()
// 0x00000221 System.Void Newtonsoft.Json.Utilities.LateBoundReflectionDelegateFactory/<>c__DisplayClass6_0`1::.ctor()
// 0x00000222 System.Object Newtonsoft.Json.Utilities.LateBoundReflectionDelegateFactory/<>c__DisplayClass6_0`1::<CreateGet>b__0(T)
// 0x00000223 System.Void Newtonsoft.Json.Utilities.LateBoundReflectionDelegateFactory/<>c__DisplayClass7_0`1::.ctor()
// 0x00000224 System.Object Newtonsoft.Json.Utilities.LateBoundReflectionDelegateFactory/<>c__DisplayClass7_0`1::<CreateGet>b__0(T)
// 0x00000225 System.Void Newtonsoft.Json.Utilities.LateBoundReflectionDelegateFactory/<>c__DisplayClass8_0`1::.ctor()
// 0x00000226 System.Void Newtonsoft.Json.Utilities.LateBoundReflectionDelegateFactory/<>c__DisplayClass8_0`1::<CreateSet>b__0(T,System.Object)
// 0x00000227 System.Void Newtonsoft.Json.Utilities.LateBoundReflectionDelegateFactory/<>c__DisplayClass9_0`1::.ctor()
// 0x00000228 System.Void Newtonsoft.Json.Utilities.LateBoundReflectionDelegateFactory/<>c__DisplayClass9_0`1::<CreateSet>b__0(T,System.Object)
// 0x00000229 System.Int32 Newtonsoft.Json.Utilities.MathUtils::IntLength(System.UInt64)
extern void MathUtils_IntLength_mAF606419E1B2F6FBF74FA80BD2353BFDF89A38B6 (void);
// 0x0000022A System.Char Newtonsoft.Json.Utilities.MathUtils::IntToHex(System.Int32)
extern void MathUtils_IntToHex_m47CB9EFD50D3ABF2C2F3E9E19F2B1438197B6286 (void);
// 0x0000022B System.Boolean Newtonsoft.Json.Utilities.MathUtils::ApproxEquals(System.Double,System.Double)
extern void MathUtils_ApproxEquals_mB7B83D6DA9CE8CE87ABB8637399194B3C0C3E477 (void);
// 0x0000022C System.Void Newtonsoft.Json.Utilities.MethodCall`2::.ctor(System.Object,System.IntPtr)
// 0x0000022D TResult Newtonsoft.Json.Utilities.MethodCall`2::Invoke(T,System.Object[])
// 0x0000022E System.IAsyncResult Newtonsoft.Json.Utilities.MethodCall`2::BeginInvoke(T,System.Object[],System.AsyncCallback,System.Object)
// 0x0000022F TResult Newtonsoft.Json.Utilities.MethodCall`2::EndInvoke(System.IAsyncResult)
// 0x00000230 System.Boolean Newtonsoft.Json.Utilities.MiscellaneousUtils::ValueEquals(System.Object,System.Object)
extern void MiscellaneousUtils_ValueEquals_m64A26242B2CABA1D40898CA9B0E5BA463741C147 (void);
// 0x00000231 System.ArgumentOutOfRangeException Newtonsoft.Json.Utilities.MiscellaneousUtils::CreateArgumentOutOfRangeException(System.String,System.Object,System.String)
extern void MiscellaneousUtils_CreateArgumentOutOfRangeException_m8DA9EF647BCB6D06005220A4F0CEC41B2D60E772 (void);
// 0x00000232 System.String Newtonsoft.Json.Utilities.MiscellaneousUtils::FormatValueForPrint(System.Object)
extern void MiscellaneousUtils_FormatValueForPrint_mFFFDA40B3A3DA1DBD8F875A305D995A430BC700F (void);
// 0x00000233 System.Void Newtonsoft.Json.Utilities.PropertyNameTable::.cctor()
extern void PropertyNameTable__cctor_m39B778A3FB109A7512A6CF8C1C6249FA7DB7B2DE (void);
// 0x00000234 System.Void Newtonsoft.Json.Utilities.PropertyNameTable::.ctor()
extern void PropertyNameTable__ctor_m7E8E7514D4B08FFA34E27EF2E799831182EDF361 (void);
// 0x00000235 System.String Newtonsoft.Json.Utilities.PropertyNameTable::Get(System.Char[],System.Int32,System.Int32)
extern void PropertyNameTable_Get_mAC5F5741F65A64E907D0782C465D0A4B32A34C0D (void);
// 0x00000236 System.String Newtonsoft.Json.Utilities.PropertyNameTable::Add(System.String)
extern void PropertyNameTable_Add_mF3AA8BCAE6D4D3D1695B7D5DC2935AE4FCB77C61 (void);
// 0x00000237 System.String Newtonsoft.Json.Utilities.PropertyNameTable::AddEntry(System.String,System.Int32)
extern void PropertyNameTable_AddEntry_mDDA03E128909E6747F0B65F4DC484E9CDCD897AC (void);
// 0x00000238 System.Void Newtonsoft.Json.Utilities.PropertyNameTable::Grow()
extern void PropertyNameTable_Grow_mDDA55289A49DD5448ABF932426763DF683F26D95 (void);
// 0x00000239 System.Boolean Newtonsoft.Json.Utilities.PropertyNameTable::TextEquals(System.String,System.Char[],System.Int32,System.Int32)
extern void PropertyNameTable_TextEquals_m383571D165562A381F13461D9F5DF6B2AF05916E (void);
// 0x0000023A System.Void Newtonsoft.Json.Utilities.PropertyNameTable/Entry::.ctor(System.String,System.Int32,Newtonsoft.Json.Utilities.PropertyNameTable/Entry)
extern void Entry__ctor_m6EA60CD174E302820D6EE496A08E9D047D936F8F (void);
// 0x0000023B System.Func`2<T,System.Object> Newtonsoft.Json.Utilities.ReflectionDelegateFactory::CreateGet(System.Reflection.MemberInfo)
// 0x0000023C System.Action`2<T,System.Object> Newtonsoft.Json.Utilities.ReflectionDelegateFactory::CreateSet(System.Reflection.MemberInfo)
// 0x0000023D Newtonsoft.Json.Utilities.MethodCall`2<T,System.Object> Newtonsoft.Json.Utilities.ReflectionDelegateFactory::CreateMethodCall(System.Reflection.MethodBase)
// 0x0000023E Newtonsoft.Json.Serialization.ObjectConstructor`1<System.Object> Newtonsoft.Json.Utilities.ReflectionDelegateFactory::CreateParameterizedConstructor(System.Reflection.MethodBase)
// 0x0000023F System.Func`1<T> Newtonsoft.Json.Utilities.ReflectionDelegateFactory::CreateDefaultConstructor(System.Type)
// 0x00000240 System.Func`2<T,System.Object> Newtonsoft.Json.Utilities.ReflectionDelegateFactory::CreateGet(System.Reflection.PropertyInfo)
// 0x00000241 System.Func`2<T,System.Object> Newtonsoft.Json.Utilities.ReflectionDelegateFactory::CreateGet(System.Reflection.FieldInfo)
// 0x00000242 System.Action`2<T,System.Object> Newtonsoft.Json.Utilities.ReflectionDelegateFactory::CreateSet(System.Reflection.FieldInfo)
// 0x00000243 System.Action`2<T,System.Object> Newtonsoft.Json.Utilities.ReflectionDelegateFactory::CreateSet(System.Reflection.PropertyInfo)
// 0x00000244 System.Void Newtonsoft.Json.Utilities.ReflectionDelegateFactory::.ctor()
extern void ReflectionDelegateFactory__ctor_m28F603C6DB820BD86A756B313F5CC3BD2FDE6A4A (void);
// 0x00000245 System.Type Newtonsoft.Json.Utilities.ReflectionMember::get_MemberType()
extern void ReflectionMember_get_MemberType_m6B83E257F42DD1C53558CD81BE74005F43794860 (void);
// 0x00000246 System.Void Newtonsoft.Json.Utilities.ReflectionMember::set_MemberType(System.Type)
extern void ReflectionMember_set_MemberType_m5909AE0B8D67C06D8069E6FC9676EFBEAE0F3CD9 (void);
// 0x00000247 System.Func`2<System.Object,System.Object> Newtonsoft.Json.Utilities.ReflectionMember::get_Getter()
extern void ReflectionMember_get_Getter_mCF5EE2696804BA8D7E7A2DB16B09840656FF9D35 (void);
// 0x00000248 System.Void Newtonsoft.Json.Utilities.ReflectionMember::set_Getter(System.Func`2<System.Object,System.Object>)
extern void ReflectionMember_set_Getter_mB7C5D68B17069E9B971342D29BBCE72CE33F91CD (void);
// 0x00000249 System.Void Newtonsoft.Json.Utilities.ReflectionMember::set_Setter(System.Action`2<System.Object,System.Object>)
extern void ReflectionMember_set_Setter_mD573E9151D6A27AA511FA936A47E43A25BD33DB7 (void);
// 0x0000024A System.Void Newtonsoft.Json.Utilities.ReflectionMember::.ctor()
extern void ReflectionMember__ctor_m98CCF5ED06AF462AFAB7B18C5804AAE9F5629D1B (void);
// 0x0000024B Newtonsoft.Json.Serialization.ObjectConstructor`1<System.Object> Newtonsoft.Json.Utilities.ReflectionObject::get_Creator()
extern void ReflectionObject_get_Creator_mC6AD5773E9CC38F72E1282A59B462E45FCE60164 (void);
// 0x0000024C System.Void Newtonsoft.Json.Utilities.ReflectionObject::set_Creator(Newtonsoft.Json.Serialization.ObjectConstructor`1<System.Object>)
extern void ReflectionObject_set_Creator_m1E3FB878C8548FA0D022F47BB82426A110FD394A (void);
// 0x0000024D System.Collections.Generic.IDictionary`2<System.String,Newtonsoft.Json.Utilities.ReflectionMember> Newtonsoft.Json.Utilities.ReflectionObject::get_Members()
extern void ReflectionObject_get_Members_m58FCDAAA8FDBE6913E756A598748FC9856C1A8EA (void);
// 0x0000024E System.Void Newtonsoft.Json.Utilities.ReflectionObject::set_Members(System.Collections.Generic.IDictionary`2<System.String,Newtonsoft.Json.Utilities.ReflectionMember>)
extern void ReflectionObject_set_Members_m17AFF4F18816B21290FBAC6AAD3D45D8FA7FFF82 (void);
// 0x0000024F System.Void Newtonsoft.Json.Utilities.ReflectionObject::.ctor()
extern void ReflectionObject__ctor_mD864AF2C633A9F94BE30D5A8F01F203EBA680AC9 (void);
// 0x00000250 System.Object Newtonsoft.Json.Utilities.ReflectionObject::GetValue(System.Object,System.String)
extern void ReflectionObject_GetValue_mC3AE87D89C1510B1BB780F58D712F3E3F8AA51DF (void);
// 0x00000251 System.Type Newtonsoft.Json.Utilities.ReflectionObject::GetType(System.String)
extern void ReflectionObject_GetType_m40AA4CFE2CA888C20AA1F8083A3F25D17D464DF8 (void);
// 0x00000252 Newtonsoft.Json.Utilities.ReflectionObject Newtonsoft.Json.Utilities.ReflectionObject::Create(System.Type,System.String[])
extern void ReflectionObject_Create_mF57E2BF8F217D0B124CE7B0B5C4A80E173279EBA (void);
// 0x00000253 Newtonsoft.Json.Utilities.ReflectionObject Newtonsoft.Json.Utilities.ReflectionObject::Create(System.Type,System.Reflection.MethodBase,System.String[])
extern void ReflectionObject_Create_m3E13A4F93F3B71B995104BD0FEBB204830B2545D (void);
// 0x00000254 System.Void Newtonsoft.Json.Utilities.ReflectionObject/<>c__DisplayClass13_0::.ctor()
extern void U3CU3Ec__DisplayClass13_0__ctor_m194BBD26BC7DA32EC4879BE571E60D14A58AB2C8 (void);
// 0x00000255 System.Object Newtonsoft.Json.Utilities.ReflectionObject/<>c__DisplayClass13_0::<Create>b__0(System.Object[])
extern void U3CU3Ec__DisplayClass13_0_U3CCreateU3Eb__0_m1916C10C0CB08965E2A5FDA9C7927E1D792DD06A (void);
// 0x00000256 System.Void Newtonsoft.Json.Utilities.ReflectionObject/<>c__DisplayClass13_1::.ctor()
extern void U3CU3Ec__DisplayClass13_1__ctor_mC40724E6465DF77E0B9F1AA336696A8A7532950D (void);
// 0x00000257 System.Object Newtonsoft.Json.Utilities.ReflectionObject/<>c__DisplayClass13_1::<Create>b__1(System.Object)
extern void U3CU3Ec__DisplayClass13_1_U3CCreateU3Eb__1_mCCC4412A143E54E9BE84A5F07B6669851E1F5568 (void);
// 0x00000258 System.Void Newtonsoft.Json.Utilities.ReflectionObject/<>c__DisplayClass13_2::.ctor()
extern void U3CU3Ec__DisplayClass13_2__ctor_m04577715D78911D1017E05613577AD31B44FC8E1 (void);
// 0x00000259 System.Void Newtonsoft.Json.Utilities.ReflectionObject/<>c__DisplayClass13_2::<Create>b__2(System.Object,System.Object)
extern void U3CU3Ec__DisplayClass13_2_U3CCreateU3Eb__2_m0C4B4C711AD85E71827E280A4AE041B65E6F7CC1 (void);
// 0x0000025A System.Void Newtonsoft.Json.Utilities.ReflectionUtils::.cctor()
extern void ReflectionUtils__cctor_m38781904F86ED6216EC00807E69F489E04F54B50 (void);
// 0x0000025B System.Boolean Newtonsoft.Json.Utilities.ReflectionUtils::IsVirtual(System.Reflection.PropertyInfo)
extern void ReflectionUtils_IsVirtual_mDAB826DEBE824D7B3DED6ACE9F582D37ADBDECD1 (void);
// 0x0000025C System.Reflection.MethodInfo Newtonsoft.Json.Utilities.ReflectionUtils::GetBaseDefinition(System.Reflection.PropertyInfo)
extern void ReflectionUtils_GetBaseDefinition_m1495E37E8666896240BC6A5904F061950A1783CB (void);
// 0x0000025D System.Boolean Newtonsoft.Json.Utilities.ReflectionUtils::IsPublic(System.Reflection.PropertyInfo)
extern void ReflectionUtils_IsPublic_m94B2E4FC561ACEC657A0B430479ECCCBC7A6FFCF (void);
// 0x0000025E System.Type Newtonsoft.Json.Utilities.ReflectionUtils::GetObjectType(System.Object)
extern void ReflectionUtils_GetObjectType_mC88354B6E2E7A667030B8AC78D8F0715C83D73C5 (void);
// 0x0000025F System.String Newtonsoft.Json.Utilities.ReflectionUtils::GetTypeName(System.Type,System.Runtime.Serialization.Formatters.FormatterAssemblyStyle,System.Runtime.Serialization.SerializationBinder)
extern void ReflectionUtils_GetTypeName_m20CF3039B0C1FDF3C40915E6FBAB1DD5C9B54607 (void);
// 0x00000260 System.String Newtonsoft.Json.Utilities.ReflectionUtils::RemoveAssemblyDetails(System.String)
extern void ReflectionUtils_RemoveAssemblyDetails_m3E7EEA0520EFA20398CC10217E6271558F988C91 (void);
// 0x00000261 System.Boolean Newtonsoft.Json.Utilities.ReflectionUtils::HasDefaultConstructor(System.Type,System.Boolean)
extern void ReflectionUtils_HasDefaultConstructor_m90A5CAF6B571D87B227C0A6E371E6DD76C95BD27 (void);
// 0x00000262 System.Reflection.ConstructorInfo Newtonsoft.Json.Utilities.ReflectionUtils::GetDefaultConstructor(System.Type)
extern void ReflectionUtils_GetDefaultConstructor_m1E576437D830C074746C3F21EB2C223603CB84E2 (void);
// 0x00000263 System.Reflection.ConstructorInfo Newtonsoft.Json.Utilities.ReflectionUtils::GetDefaultConstructor(System.Type,System.Boolean)
extern void ReflectionUtils_GetDefaultConstructor_m0ECDE909BEADC23A79948893BD9AFE4BC2EA9F50 (void);
// 0x00000264 System.Boolean Newtonsoft.Json.Utilities.ReflectionUtils::IsNullable(System.Type)
extern void ReflectionUtils_IsNullable_m7027B69E0FEE51A4B7D4881069F9DEB8537C07BC (void);
// 0x00000265 System.Boolean Newtonsoft.Json.Utilities.ReflectionUtils::IsNullableType(System.Type)
extern void ReflectionUtils_IsNullableType_m087178E090D00C75843BB2F93A7CAA414FEFAFB6 (void);
// 0x00000266 System.Type Newtonsoft.Json.Utilities.ReflectionUtils::EnsureNotNullableType(System.Type)
extern void ReflectionUtils_EnsureNotNullableType_mA0D0761F8427803196C446A08267DFFE9FA84687 (void);
// 0x00000267 System.Boolean Newtonsoft.Json.Utilities.ReflectionUtils::IsGenericDefinition(System.Type,System.Type)
extern void ReflectionUtils_IsGenericDefinition_m8E49178573A6D6B940FAA2772355261CE7D97665 (void);
// 0x00000268 System.Boolean Newtonsoft.Json.Utilities.ReflectionUtils::ImplementsGenericDefinition(System.Type,System.Type)
extern void ReflectionUtils_ImplementsGenericDefinition_m377CF88EF82FF1E10335FF74C984B4B9245DD02B (void);
// 0x00000269 System.Boolean Newtonsoft.Json.Utilities.ReflectionUtils::ImplementsGenericDefinition(System.Type,System.Type,System.Type&)
extern void ReflectionUtils_ImplementsGenericDefinition_m5C3C97154DE1303207DD359EAE64A00B3533A8AC (void);
// 0x0000026A System.Boolean Newtonsoft.Json.Utilities.ReflectionUtils::InheritsGenericDefinition(System.Type,System.Type)
extern void ReflectionUtils_InheritsGenericDefinition_mC249D71C553B57BA04ABEBD54463959581C89C74 (void);
// 0x0000026B System.Boolean Newtonsoft.Json.Utilities.ReflectionUtils::InheritsGenericDefinition(System.Type,System.Type,System.Type&)
extern void ReflectionUtils_InheritsGenericDefinition_m0E35465B169193FF20D72965EEF1EE7CD6529A8E (void);
// 0x0000026C System.Boolean Newtonsoft.Json.Utilities.ReflectionUtils::InheritsGenericDefinitionInternal(System.Type,System.Type,System.Type&)
extern void ReflectionUtils_InheritsGenericDefinitionInternal_mC94DF59F57D5A8C232DB38ABE39CA8567BF86A0B (void);
// 0x0000026D System.Type Newtonsoft.Json.Utilities.ReflectionUtils::GetCollectionItemType(System.Type)
extern void ReflectionUtils_GetCollectionItemType_m9C5B96AD4C1BC8FEBD23A09C4CF6E162DD8F0F91 (void);
// 0x0000026E System.Void Newtonsoft.Json.Utilities.ReflectionUtils::GetDictionaryKeyValueTypes(System.Type,System.Type&,System.Type&)
extern void ReflectionUtils_GetDictionaryKeyValueTypes_mFF98FCA2C8DCF09C87F941D31F3705B44F594A06 (void);
// 0x0000026F System.Type Newtonsoft.Json.Utilities.ReflectionUtils::GetMemberUnderlyingType(System.Reflection.MemberInfo)
extern void ReflectionUtils_GetMemberUnderlyingType_m369A749B80A046506D574AFBB1DA224B610DAAF3 (void);
// 0x00000270 System.Boolean Newtonsoft.Json.Utilities.ReflectionUtils::IsIndexedProperty(System.Reflection.MemberInfo)
extern void ReflectionUtils_IsIndexedProperty_m1A60AB81E965E535B27CDA1FFF29DEB3C755033D (void);
// 0x00000271 System.Boolean Newtonsoft.Json.Utilities.ReflectionUtils::IsIndexedProperty(System.Reflection.PropertyInfo)
extern void ReflectionUtils_IsIndexedProperty_m510414BA1376369ED3B0E9F3C4A4D9A54A768E04 (void);
// 0x00000272 System.Object Newtonsoft.Json.Utilities.ReflectionUtils::GetMemberValue(System.Reflection.MemberInfo,System.Object)
extern void ReflectionUtils_GetMemberValue_mE5AF9C55EFB2EF538341A06DE8839EB2138B66E8 (void);
// 0x00000273 System.Void Newtonsoft.Json.Utilities.ReflectionUtils::SetMemberValue(System.Reflection.MemberInfo,System.Object,System.Object)
extern void ReflectionUtils_SetMemberValue_m1E2CC0C74BC61975558464CEC2E6B42F4F16ADD1 (void);
// 0x00000274 System.Boolean Newtonsoft.Json.Utilities.ReflectionUtils::CanReadMemberValue(System.Reflection.MemberInfo,System.Boolean)
extern void ReflectionUtils_CanReadMemberValue_m2CA3EDD4A8D77B8755BC39203EC2CC199E68FC28 (void);
// 0x00000275 System.Boolean Newtonsoft.Json.Utilities.ReflectionUtils::CanSetMemberValue(System.Reflection.MemberInfo,System.Boolean,System.Boolean)
extern void ReflectionUtils_CanSetMemberValue_mCBC362D65D00A3CE3541660FC8767893E95E39FF (void);
// 0x00000276 System.Collections.Generic.List`1<System.Reflection.MemberInfo> Newtonsoft.Json.Utilities.ReflectionUtils::GetFieldsAndProperties(System.Type,System.Reflection.BindingFlags)
extern void ReflectionUtils_GetFieldsAndProperties_m0EE6098D13F78E595B6E77237CE20DC6C3038B32 (void);
// 0x00000277 System.Boolean Newtonsoft.Json.Utilities.ReflectionUtils::IsOverridenGenericMember(System.Reflection.MemberInfo,System.Reflection.BindingFlags)
extern void ReflectionUtils_IsOverridenGenericMember_m23C99D639780A73CB8C8D8B8FE38B13BF286F38C (void);
// 0x00000278 T Newtonsoft.Json.Utilities.ReflectionUtils::GetAttribute(System.Object)
// 0x00000279 T Newtonsoft.Json.Utilities.ReflectionUtils::GetAttribute(System.Object,System.Boolean)
// 0x0000027A T[] Newtonsoft.Json.Utilities.ReflectionUtils::GetAttributes(System.Object,System.Boolean)
// 0x0000027B System.Attribute[] Newtonsoft.Json.Utilities.ReflectionUtils::GetAttributes(System.Object,System.Type,System.Boolean)
extern void ReflectionUtils_GetAttributes_m2E4ADE14EA1DD97E34839435060F77446A601E9A (void);
// 0x0000027C System.Void Newtonsoft.Json.Utilities.ReflectionUtils::SplitFullyQualifiedTypeName(System.String,System.String&,System.String&)
extern void ReflectionUtils_SplitFullyQualifiedTypeName_mE4BA0DF98772645EDE016EA34BFEED01B7BFA047 (void);
// 0x0000027D System.Nullable`1<System.Int32> Newtonsoft.Json.Utilities.ReflectionUtils::GetAssemblyDelimiterIndex(System.String)
extern void ReflectionUtils_GetAssemblyDelimiterIndex_mF133EECC700C82646C566FF8ADE571171A56D125 (void);
// 0x0000027E System.Reflection.MemberInfo Newtonsoft.Json.Utilities.ReflectionUtils::GetMemberInfoFromType(System.Type,System.Reflection.MemberInfo)
extern void ReflectionUtils_GetMemberInfoFromType_m524B7C4E43AEFAA69AD2569ED76F9FEEDA234AC8 (void);
// 0x0000027F System.Collections.Generic.IEnumerable`1<System.Reflection.FieldInfo> Newtonsoft.Json.Utilities.ReflectionUtils::GetFields(System.Type,System.Reflection.BindingFlags)
extern void ReflectionUtils_GetFields_mC8557AC07C431D1AA3B540954BDE9DFDDEEEB427 (void);
// 0x00000280 System.Void Newtonsoft.Json.Utilities.ReflectionUtils::GetChildPrivateFields(System.Collections.Generic.IList`1<System.Reflection.MemberInfo>,System.Type,System.Reflection.BindingFlags)
extern void ReflectionUtils_GetChildPrivateFields_mAF6C30229F19E90DCB8DD58D73A2D4998A823633 (void);
// 0x00000281 System.Collections.Generic.IEnumerable`1<System.Reflection.PropertyInfo> Newtonsoft.Json.Utilities.ReflectionUtils::GetProperties(System.Type,System.Reflection.BindingFlags)
extern void ReflectionUtils_GetProperties_mAAE0D4CB04861AD2DBF93423D1D38241D21A5F6A (void);
// 0x00000282 System.Reflection.BindingFlags Newtonsoft.Json.Utilities.ReflectionUtils::RemoveFlag(System.Reflection.BindingFlags,System.Reflection.BindingFlags)
extern void ReflectionUtils_RemoveFlag_mC2CD5A61EDBBB52F4D3CA5DB138800195E89F8E3 (void);
// 0x00000283 System.Void Newtonsoft.Json.Utilities.ReflectionUtils::GetChildPrivateProperties(System.Collections.Generic.IList`1<System.Reflection.PropertyInfo>,System.Type,System.Reflection.BindingFlags)
extern void ReflectionUtils_GetChildPrivateProperties_m1FCFADAE9A7D08A43786DEAF720C4623F7C95CB2 (void);
// 0x00000284 System.Object Newtonsoft.Json.Utilities.ReflectionUtils::GetDefaultValue(System.Type)
extern void ReflectionUtils_GetDefaultValue_mF81B1B14955A1F076958EA87A8D0171566630E07 (void);
// 0x00000285 System.Void Newtonsoft.Json.Utilities.ReflectionUtils/<>c::.cctor()
extern void U3CU3Ec__cctor_m79E2F51886AC0D350C73A7341E570086801F28F1 (void);
// 0x00000286 System.Void Newtonsoft.Json.Utilities.ReflectionUtils/<>c::.ctor()
extern void U3CU3Ec__ctor_mF109C838FBEC05DBB2F0E89B8543BE1E18D1F902 (void);
// 0x00000287 System.Boolean Newtonsoft.Json.Utilities.ReflectionUtils/<>c::<GetDefaultConstructor>b__10_0(System.Reflection.ConstructorInfo)
extern void U3CU3Ec_U3CGetDefaultConstructorU3Eb__10_0_mEBAC516691175B5FD7C7844C45C45898D3478DF6 (void);
// 0x00000288 System.String Newtonsoft.Json.Utilities.ReflectionUtils/<>c::<GetFieldsAndProperties>b__29_0(System.Reflection.MemberInfo)
extern void U3CU3Ec_U3CGetFieldsAndPropertiesU3Eb__29_0_m183FF91D6BBD2D1F05B4E57E690128FC298B35B1 (void);
// 0x00000289 System.Type Newtonsoft.Json.Utilities.ReflectionUtils/<>c::<GetMemberInfoFromType>b__37_0(System.Reflection.ParameterInfo)
extern void U3CU3Ec_U3CGetMemberInfoFromTypeU3Eb__37_0_m871095E1FABE62782EA72E595F1E18E758F07850 (void);
// 0x0000028A System.Boolean Newtonsoft.Json.Utilities.ReflectionUtils/<>c::<GetChildPrivateFields>b__39_0(System.Reflection.FieldInfo)
extern void U3CU3Ec_U3CGetChildPrivateFieldsU3Eb__39_0_m0D7F2247B937D9C2120BD8779A685032A08FFD39 (void);
// 0x0000028B System.Void Newtonsoft.Json.Utilities.ReflectionUtils/<>c__DisplayClass42_0::.ctor()
extern void U3CU3Ec__DisplayClass42_0__ctor_m41469F7DA375544E91F8CDCE9BD78BBDA4D3DC82 (void);
// 0x0000028C System.Boolean Newtonsoft.Json.Utilities.ReflectionUtils/<>c__DisplayClass42_0::<GetChildPrivateProperties>b__0(System.Reflection.PropertyInfo)
extern void U3CU3Ec__DisplayClass42_0_U3CGetChildPrivatePropertiesU3Eb__0_m5380F6591C2980BEB7CB9E82A691AAC2D17A5CE3 (void);
// 0x0000028D System.Boolean Newtonsoft.Json.Utilities.ReflectionUtils/<>c__DisplayClass42_0::<GetChildPrivateProperties>b__1(System.Reflection.PropertyInfo)
extern void U3CU3Ec__DisplayClass42_0_U3CGetChildPrivatePropertiesU3Eb__1_m87A8496C6B2964587C139987D42A5D22EB605FCE (void);
// 0x0000028E System.Boolean Newtonsoft.Json.Utilities.ReflectionUtils/<>c__DisplayClass42_0::<GetChildPrivateProperties>b__2(System.Reflection.PropertyInfo)
extern void U3CU3Ec__DisplayClass42_0_U3CGetChildPrivatePropertiesU3Eb__2_m222C3A1773447DE952CDB65AD6316A411747C3E5 (void);
// 0x0000028F System.Int32 Newtonsoft.Json.Utilities.StringBuffer::get_Position()
extern void StringBuffer_get_Position_m424CA0E4A0274918489C5986D189CB6DDEA6F11F (void);
// 0x00000290 System.Void Newtonsoft.Json.Utilities.StringBuffer::set_Position(System.Int32)
extern void StringBuffer_set_Position_mE931269D642EEE14E2D84EEE1F0706536D65401F (void);
// 0x00000291 System.Boolean Newtonsoft.Json.Utilities.StringBuffer::get_IsEmpty()
extern void StringBuffer_get_IsEmpty_m3B5208A8E035C3BF103F48C4BD666BA7D16C7682 (void);
// 0x00000292 System.Void Newtonsoft.Json.Utilities.StringBuffer::.ctor(Newtonsoft.Json.IArrayPool`1<System.Char>,System.Int32)
extern void StringBuffer__ctor_m658802150D63B715C3470D6B2C0170551ECB7DD5 (void);
// 0x00000293 System.Void Newtonsoft.Json.Utilities.StringBuffer::.ctor(System.Char[])
extern void StringBuffer__ctor_m80178B6D27B163F78337AC8C7BA4D6026696FA83 (void);
// 0x00000294 System.Void Newtonsoft.Json.Utilities.StringBuffer::Append(Newtonsoft.Json.IArrayPool`1<System.Char>,System.Char)
extern void StringBuffer_Append_m0FCC4FB58E15B226B908B153D32045BBFF1D76F0 (void);
// 0x00000295 System.Void Newtonsoft.Json.Utilities.StringBuffer::Append(Newtonsoft.Json.IArrayPool`1<System.Char>,System.Char[],System.Int32,System.Int32)
extern void StringBuffer_Append_m603D9B2C24029588D05093561F805BC76C4DF0EA (void);
// 0x00000296 System.Void Newtonsoft.Json.Utilities.StringBuffer::Clear(Newtonsoft.Json.IArrayPool`1<System.Char>)
extern void StringBuffer_Clear_m56DC59C94CAF5F3A5830C14E2E38F0C205F78610 (void);
// 0x00000297 System.Void Newtonsoft.Json.Utilities.StringBuffer::EnsureSize(Newtonsoft.Json.IArrayPool`1<System.Char>,System.Int32)
extern void StringBuffer_EnsureSize_m76D11C8EED77ACD0299390A98CE072C5F2FE9EC0 (void);
// 0x00000298 System.String Newtonsoft.Json.Utilities.StringBuffer::ToString()
extern void StringBuffer_ToString_m10B8FF428EAE38BBC65EEC7809ADC392532B5927 (void);
// 0x00000299 System.String Newtonsoft.Json.Utilities.StringBuffer::ToString(System.Int32,System.Int32)
extern void StringBuffer_ToString_m88EAF73086605E7A0C898D962CA6B5CF8A6B0F07 (void);
// 0x0000029A System.Char[] Newtonsoft.Json.Utilities.StringBuffer::get_InternalBuffer()
extern void StringBuffer_get_InternalBuffer_mF41DB26CCDD88E4ACE7B65840ADB74BF5EEE748A (void);
// 0x0000029B System.Char Newtonsoft.Json.Utilities.StringReference::get_Item(System.Int32)
extern void StringReference_get_Item_m34C3ABEC482DD4EC5541D0A56EC05D05F669B0A0 (void);
// 0x0000029C System.Char[] Newtonsoft.Json.Utilities.StringReference::get_Chars()
extern void StringReference_get_Chars_m5EF63176FDB3554674E394261BE5786170C47024 (void);
// 0x0000029D System.Int32 Newtonsoft.Json.Utilities.StringReference::get_StartIndex()
extern void StringReference_get_StartIndex_mB09EFAA44487DB78CF15BDF61052689823A4C2C9 (void);
// 0x0000029E System.Int32 Newtonsoft.Json.Utilities.StringReference::get_Length()
extern void StringReference_get_Length_m194CFDC809C99311F4040820C9F4A3B5E5F77EA4 (void);
// 0x0000029F System.Void Newtonsoft.Json.Utilities.StringReference::.ctor(System.Char[],System.Int32,System.Int32)
extern void StringReference__ctor_m3AE890FED005EFC9FF5CDF3C7F740E743168B616 (void);
// 0x000002A0 System.String Newtonsoft.Json.Utilities.StringReference::ToString()
extern void StringReference_ToString_mFE493A141754B54CA6B7DC1903730740DAB2A881 (void);
// 0x000002A1 System.Int32 Newtonsoft.Json.Utilities.StringReferenceExtensions::IndexOf(Newtonsoft.Json.Utilities.StringReference,System.Char,System.Int32,System.Int32)
extern void StringReferenceExtensions_IndexOf_m0521F7953B075BB955750D6AD8053D53D7ECD6AC (void);
// 0x000002A2 System.Boolean Newtonsoft.Json.Utilities.StringReferenceExtensions::StartsWith(Newtonsoft.Json.Utilities.StringReference,System.String)
extern void StringReferenceExtensions_StartsWith_m6D9020568686A182328C1F71CFC1800A870CFEB3 (void);
// 0x000002A3 System.Boolean Newtonsoft.Json.Utilities.StringReferenceExtensions::EndsWith(Newtonsoft.Json.Utilities.StringReference,System.String)
extern void StringReferenceExtensions_EndsWith_mD6F3B9EEC429EAB4F6E05E69EECF814DDF3F764B (void);
// 0x000002A4 System.String Newtonsoft.Json.Utilities.StringUtils::FormatWith(System.String,System.IFormatProvider,System.Object)
extern void StringUtils_FormatWith_mF78320AE4049E77D6DDEC01680F7D98C44E2442D (void);
// 0x000002A5 System.String Newtonsoft.Json.Utilities.StringUtils::FormatWith(System.String,System.IFormatProvider,System.Object,System.Object)
extern void StringUtils_FormatWith_mEB092C5B96EC84FDC050432B1A40DF8A83BFA2E8 (void);
// 0x000002A6 System.String Newtonsoft.Json.Utilities.StringUtils::FormatWith(System.String,System.IFormatProvider,System.Object,System.Object,System.Object)
extern void StringUtils_FormatWith_m4FCAF6C2F661AFA5DECB79DDD2F4C1C63933AC02 (void);
// 0x000002A7 System.String Newtonsoft.Json.Utilities.StringUtils::FormatWith(System.String,System.IFormatProvider,System.Object,System.Object,System.Object,System.Object)
extern void StringUtils_FormatWith_mFFF18C22B96311F0FF31E7711F474AB79C1D96EF (void);
// 0x000002A8 System.String Newtonsoft.Json.Utilities.StringUtils::FormatWith(System.String,System.IFormatProvider,System.Object[])
extern void StringUtils_FormatWith_mF72FDDA3EB515E398CA2D73E829FC7F8AD4F1D2B (void);
// 0x000002A9 System.IO.StringWriter Newtonsoft.Json.Utilities.StringUtils::CreateStringWriter(System.Int32)
extern void StringUtils_CreateStringWriter_mECD1F544CCFBE0B6981309D5820C726B5BA7D629 (void);
// 0x000002AA System.Void Newtonsoft.Json.Utilities.StringUtils::ToCharAsUnicode(System.Char,System.Char[])
extern void StringUtils_ToCharAsUnicode_mB1A7A9E1E08E5D6C9FDDB3503C94A0478BCD7A83 (void);
// 0x000002AB TSource Newtonsoft.Json.Utilities.StringUtils::ForgivingCaseSensitiveFind(System.Collections.Generic.IEnumerable`1<TSource>,System.Func`2<TSource,System.String>,System.String)
// 0x000002AC System.Boolean Newtonsoft.Json.Utilities.StringUtils::IsHighSurrogate(System.Char)
extern void StringUtils_IsHighSurrogate_mF2BB7B9F8B5B5AE318D47AC53DA3DF1F4D759542 (void);
// 0x000002AD System.Boolean Newtonsoft.Json.Utilities.StringUtils::IsLowSurrogate(System.Char)
extern void StringUtils_IsLowSurrogate_mFB8EFC08BA665DD434D3752CFA6642393EFEA55D (void);
// 0x000002AE System.Boolean Newtonsoft.Json.Utilities.StringUtils::EndsWith(System.String,System.Char)
extern void StringUtils_EndsWith_m66FA0222A273889E4E523E10DE65E833F5536F58 (void);
// 0x000002AF System.Void Newtonsoft.Json.Utilities.StringUtils/<>c__DisplayClass13_0`1::.ctor()
// 0x000002B0 System.Boolean Newtonsoft.Json.Utilities.StringUtils/<>c__DisplayClass13_0`1::<ForgivingCaseSensitiveFind>b__0(TSource)
// 0x000002B1 System.Boolean Newtonsoft.Json.Utilities.StringUtils/<>c__DisplayClass13_0`1::<ForgivingCaseSensitiveFind>b__1(TSource)
// 0x000002B2 System.Void Newtonsoft.Json.Utilities.ThreadSafeStore`2::.ctor(System.Func`2<TKey,TValue>)
// 0x000002B3 TValue Newtonsoft.Json.Utilities.ThreadSafeStore`2::Get(TKey)
// 0x000002B4 TValue Newtonsoft.Json.Utilities.ThreadSafeStore`2::AddValue(TKey)
// 0x000002B5 System.Reflection.MemberTypes Newtonsoft.Json.Utilities.TypeExtensions::MemberType(System.Reflection.MemberInfo)
extern void TypeExtensions_MemberType_m6B0C77E72F48DC1B4204737359510A81F3E464C4 (void);
// 0x000002B6 System.Boolean Newtonsoft.Json.Utilities.TypeExtensions::ContainsGenericParameters(System.Type)
extern void TypeExtensions_ContainsGenericParameters_m291B9B1787BCE6E8D410828890476C7E868ED823 (void);
// 0x000002B7 System.Boolean Newtonsoft.Json.Utilities.TypeExtensions::IsInterface(System.Type)
extern void TypeExtensions_IsInterface_mC86FEECE611B133413962A5EFE5DC85B92D76381 (void);
// 0x000002B8 System.Boolean Newtonsoft.Json.Utilities.TypeExtensions::IsGenericType(System.Type)
extern void TypeExtensions_IsGenericType_mD7A79EF6D299F4D40932AEA3CF8BC3DD8E0C06D2 (void);
// 0x000002B9 System.Boolean Newtonsoft.Json.Utilities.TypeExtensions::IsGenericTypeDefinition(System.Type)
extern void TypeExtensions_IsGenericTypeDefinition_m16881F2378C11CF6BFB7B3A127643F4129293AF8 (void);
// 0x000002BA System.Type Newtonsoft.Json.Utilities.TypeExtensions::BaseType(System.Type)
extern void TypeExtensions_BaseType_mB412285DD088CA76F89534B78D1AE65FFF30F754 (void);
// 0x000002BB System.Boolean Newtonsoft.Json.Utilities.TypeExtensions::IsEnum(System.Type)
extern void TypeExtensions_IsEnum_m3D3EA9AF4638A68716201C314A1C75C0D0FE6CAA (void);
// 0x000002BC System.Boolean Newtonsoft.Json.Utilities.TypeExtensions::IsClass(System.Type)
extern void TypeExtensions_IsClass_mF1942239F1A4C2B093A05F39F7F8F7D94EE34A1D (void);
// 0x000002BD System.Boolean Newtonsoft.Json.Utilities.TypeExtensions::IsSealed(System.Type)
extern void TypeExtensions_IsSealed_m9BF5CB41B9F60342CE4634AA4DE803492059844F (void);
// 0x000002BE System.Boolean Newtonsoft.Json.Utilities.TypeExtensions::IsAbstract(System.Type)
extern void TypeExtensions_IsAbstract_m3E114BBDB2B178E154B5AD58D0C203A51BE091F0 (void);
// 0x000002BF System.Boolean Newtonsoft.Json.Utilities.TypeExtensions::IsValueType(System.Type)
extern void TypeExtensions_IsValueType_mCAEDF96A86663613DC39781684AF92CC1531DF2B (void);
// 0x000002C0 System.Boolean Newtonsoft.Json.Utilities.TypeExtensions::AssignableToTypeName(System.Type,System.String,System.Type&)
extern void TypeExtensions_AssignableToTypeName_mD522101DAFD0E49737C49A7BE5162B7D23312CC0 (void);
// 0x000002C1 System.Boolean Newtonsoft.Json.Utilities.TypeExtensions::AssignableToTypeName(System.Type,System.String)
extern void TypeExtensions_AssignableToTypeName_mF0CE3AB37FDF6B916128D0B9735D8ABD7CC649A4 (void);
// 0x000002C2 System.Boolean Newtonsoft.Json.Utilities.TypeExtensions::ImplementInterface(System.Type,System.Type)
extern void TypeExtensions_ImplementInterface_m7183F3D7AF62459483F1E37786034552734BE904 (void);
// 0x000002C3 System.Void Newtonsoft.Json.Utilities.ValidationUtils::ArgumentNotNull(System.Object,System.String)
extern void ValidationUtils_ArgumentNotNull_mA8AC1AD3B50DBCFC06ABF784BB1311468771A495 (void);
// 0x000002C4 T Newtonsoft.Json.Serialization.CachedAttributeGetter`1::GetAttribute(System.Object)
// 0x000002C5 System.Void Newtonsoft.Json.Serialization.CachedAttributeGetter`1::.cctor()
// 0x000002C6 System.Void Newtonsoft.Json.Serialization.ResolverContractKey::.ctor(System.Type,System.Type)
extern void ResolverContractKey__ctor_m5C7C616DF53AB9D3EE81A19395431BA9B0A3EB9C (void);
// 0x000002C7 System.Int32 Newtonsoft.Json.Serialization.ResolverContractKey::GetHashCode()
extern void ResolverContractKey_GetHashCode_mCF78DF3E9A1EB11DACA737FA5A306F172A8A0D6B (void);
// 0x000002C8 System.Boolean Newtonsoft.Json.Serialization.ResolverContractKey::Equals(System.Object)
extern void ResolverContractKey_Equals_m7EADD44C28175D677F9881E4D0021CF8936E0732 (void);
// 0x000002C9 System.Boolean Newtonsoft.Json.Serialization.ResolverContractKey::Equals(Newtonsoft.Json.Serialization.ResolverContractKey)
extern void ResolverContractKey_Equals_m7F6E1B33EEBDE4971264A88E6662D9CDDE4CFC18 (void);
// 0x000002CA System.Void Newtonsoft.Json.Serialization.DefaultContractResolverState::.ctor()
extern void DefaultContractResolverState__ctor_mE28AAD3451BC7C048695944EC126C36024326631 (void);
// 0x000002CB Newtonsoft.Json.Serialization.IContractResolver Newtonsoft.Json.Serialization.DefaultContractResolver::get_Instance()
extern void DefaultContractResolver_get_Instance_m90FDF8E006FCD7E71EE5D54185A6BFAD04684F8E (void);
// 0x000002CC System.Reflection.BindingFlags Newtonsoft.Json.Serialization.DefaultContractResolver::get_DefaultMembersSearchFlags()
extern void DefaultContractResolver_get_DefaultMembersSearchFlags_mA6F171D6825AF07E7AC585C5B89B8B1BA57FAEC3 (void);
// 0x000002CD System.Void Newtonsoft.Json.Serialization.DefaultContractResolver::set_DefaultMembersSearchFlags(System.Reflection.BindingFlags)
extern void DefaultContractResolver_set_DefaultMembersSearchFlags_m89CA59C8A801BB1C3023BAD591A244271051A248 (void);
// 0x000002CE System.Boolean Newtonsoft.Json.Serialization.DefaultContractResolver::get_SerializeCompilerGeneratedMembers()
extern void DefaultContractResolver_get_SerializeCompilerGeneratedMembers_mCEC95D8DC060D61621A326CE8DB8B6D1769BB0CC (void);
// 0x000002CF System.Boolean Newtonsoft.Json.Serialization.DefaultContractResolver::get_IgnoreSerializableInterface()
extern void DefaultContractResolver_get_IgnoreSerializableInterface_m10A32A92DCF98D1EDD209D880F17359001FD0125 (void);
// 0x000002D0 System.Boolean Newtonsoft.Json.Serialization.DefaultContractResolver::get_IgnoreSerializableAttribute()
extern void DefaultContractResolver_get_IgnoreSerializableAttribute_m1429E466AFA1212DADA0B9223A04C74BD5DCA8CE (void);
// 0x000002D1 System.Void Newtonsoft.Json.Serialization.DefaultContractResolver::set_IgnoreSerializableAttribute(System.Boolean)
extern void DefaultContractResolver_set_IgnoreSerializableAttribute_m54DC842D81DEABFD3198F00DDEBF83E1EF2C6936 (void);
// 0x000002D2 Newtonsoft.Json.Serialization.NamingStrategy Newtonsoft.Json.Serialization.DefaultContractResolver::get_NamingStrategy()
extern void DefaultContractResolver_get_NamingStrategy_m5C917ABAD194876A14B94BF7FD9D5879527A842A (void);
// 0x000002D3 System.Void Newtonsoft.Json.Serialization.DefaultContractResolver::.ctor()
extern void DefaultContractResolver__ctor_m1D763AE5E45591246C4D6647F7102D131422D3BE (void);
// 0x000002D4 System.Void Newtonsoft.Json.Serialization.DefaultContractResolver::.ctor(System.Boolean)
extern void DefaultContractResolver__ctor_mCC6008697D963A3DD69FD7F4D94D3EAA5A9F6036 (void);
// 0x000002D5 Newtonsoft.Json.Serialization.DefaultContractResolverState Newtonsoft.Json.Serialization.DefaultContractResolver::GetState()
extern void DefaultContractResolver_GetState_m18601177DCDAC796883BA5DFA043E8EFC0184D03 (void);
// 0x000002D6 Newtonsoft.Json.Serialization.JsonContract Newtonsoft.Json.Serialization.DefaultContractResolver::ResolveContract(System.Type)
extern void DefaultContractResolver_ResolveContract_m6A394909C187327992FEECEEB186CD9554120DFE (void);
// 0x000002D7 System.Collections.Generic.List`1<System.Reflection.MemberInfo> Newtonsoft.Json.Serialization.DefaultContractResolver::GetSerializableMembers(System.Type)
extern void DefaultContractResolver_GetSerializableMembers_m8CC788F3C44419695C20618447DD8B3F57988BDD (void);
// 0x000002D8 System.Boolean Newtonsoft.Json.Serialization.DefaultContractResolver::ShouldSerializeEntityMember(System.Reflection.MemberInfo)
extern void DefaultContractResolver_ShouldSerializeEntityMember_mB90D5F9FAF703C796A362AC332132F1A12DD20F1 (void);
// 0x000002D9 Newtonsoft.Json.Serialization.JsonObjectContract Newtonsoft.Json.Serialization.DefaultContractResolver::CreateObjectContract(System.Type)
extern void DefaultContractResolver_CreateObjectContract_m6AA964F38C7F57543CCE1257AE4088CCB7C6A0EA (void);
// 0x000002DA System.Reflection.MemberInfo Newtonsoft.Json.Serialization.DefaultContractResolver::GetExtensionDataMemberForType(System.Type)
extern void DefaultContractResolver_GetExtensionDataMemberForType_m1F4CE56A95386EBF1C08D38E2A63634439EAAD7D (void);
// 0x000002DB System.Void Newtonsoft.Json.Serialization.DefaultContractResolver::SetExtensionDataDelegates(Newtonsoft.Json.Serialization.JsonObjectContract,System.Reflection.MemberInfo)
extern void DefaultContractResolver_SetExtensionDataDelegates_m54A4E0A44B6DAC1466FB14F81CA2DE70182D6000 (void);
// 0x000002DC System.Reflection.ConstructorInfo Newtonsoft.Json.Serialization.DefaultContractResolver::GetAttributeConstructor(System.Type)
extern void DefaultContractResolver_GetAttributeConstructor_m010BCF1898D26643560E364CC040C11F3289F05B (void);
// 0x000002DD System.Reflection.ConstructorInfo Newtonsoft.Json.Serialization.DefaultContractResolver::GetParameterizedConstructor(System.Type)
extern void DefaultContractResolver_GetParameterizedConstructor_m54EA6C46871CCAA7B8626090134D6E07616B1CD1 (void);
// 0x000002DE System.Collections.Generic.IList`1<Newtonsoft.Json.Serialization.JsonProperty> Newtonsoft.Json.Serialization.DefaultContractResolver::CreateConstructorParameters(System.Reflection.ConstructorInfo,Newtonsoft.Json.Serialization.JsonPropertyCollection)
extern void DefaultContractResolver_CreateConstructorParameters_mAE76A97CB17E7C3B3E139202D7A13F23C35BBE31 (void);
// 0x000002DF Newtonsoft.Json.Serialization.JsonProperty Newtonsoft.Json.Serialization.DefaultContractResolver::CreatePropertyFromConstructorParameter(Newtonsoft.Json.Serialization.JsonProperty,System.Reflection.ParameterInfo)
extern void DefaultContractResolver_CreatePropertyFromConstructorParameter_m26EEB6ED59FB3FDC8CA40D4471A9D5090D316D02 (void);
// 0x000002E0 Newtonsoft.Json.JsonConverter Newtonsoft.Json.Serialization.DefaultContractResolver::ResolveContractConverter(System.Type)
extern void DefaultContractResolver_ResolveContractConverter_m23E1780FD23E01BFABA06F322B9D601ACE46B896 (void);
// 0x000002E1 System.Func`1<System.Object> Newtonsoft.Json.Serialization.DefaultContractResolver::GetDefaultCreator(System.Type)
extern void DefaultContractResolver_GetDefaultCreator_m8D927E78AF0F223216F58B223F0B3179EEC99927 (void);
// 0x000002E2 System.Void Newtonsoft.Json.Serialization.DefaultContractResolver::InitializeContract(Newtonsoft.Json.Serialization.JsonContract)
extern void DefaultContractResolver_InitializeContract_mD8156F1B6C83C65F429C4BBC8916BF2391CE11A7 (void);
// 0x000002E3 System.Void Newtonsoft.Json.Serialization.DefaultContractResolver::ResolveCallbackMethods(Newtonsoft.Json.Serialization.JsonContract,System.Type)
extern void DefaultContractResolver_ResolveCallbackMethods_mC27F18AF4B4006DA0CF2B3CF04B6DFD595F9E55B (void);
// 0x000002E4 System.Void Newtonsoft.Json.Serialization.DefaultContractResolver::GetCallbackMethodsForType(System.Type,System.Collections.Generic.List`1<Newtonsoft.Json.Serialization.SerializationCallback>&,System.Collections.Generic.List`1<Newtonsoft.Json.Serialization.SerializationCallback>&,System.Collections.Generic.List`1<Newtonsoft.Json.Serialization.SerializationCallback>&,System.Collections.Generic.List`1<Newtonsoft.Json.Serialization.SerializationCallback>&,System.Collections.Generic.List`1<Newtonsoft.Json.Serialization.SerializationErrorCallback>&)
extern void DefaultContractResolver_GetCallbackMethodsForType_m22B6B6BBDA8F434AB03BA7C5BB52F3FFC8051B55 (void);
// 0x000002E5 System.Boolean Newtonsoft.Json.Serialization.DefaultContractResolver::ShouldSkipDeserialized(System.Type)
extern void DefaultContractResolver_ShouldSkipDeserialized_mC687F862EDFB6D3B7DA18984B3201CBCB3B32312 (void);
// 0x000002E6 System.Boolean Newtonsoft.Json.Serialization.DefaultContractResolver::ShouldSkipSerializing(System.Type)
extern void DefaultContractResolver_ShouldSkipSerializing_mEDA1B821A3F7C8A45200A77A02ABCBA16A130771 (void);
// 0x000002E7 System.Collections.Generic.List`1<System.Type> Newtonsoft.Json.Serialization.DefaultContractResolver::GetClassHierarchyForType(System.Type)
extern void DefaultContractResolver_GetClassHierarchyForType_mACD438778C3967B9C596B1748418E1D1B85C15F1 (void);
// 0x000002E8 Newtonsoft.Json.Serialization.JsonDictionaryContract Newtonsoft.Json.Serialization.DefaultContractResolver::CreateDictionaryContract(System.Type)
extern void DefaultContractResolver_CreateDictionaryContract_m7D68ABF113D252A9F4D07931E0D26F75CFFDF740 (void);
// 0x000002E9 Newtonsoft.Json.Serialization.JsonArrayContract Newtonsoft.Json.Serialization.DefaultContractResolver::CreateArrayContract(System.Type)
extern void DefaultContractResolver_CreateArrayContract_m55811CDD16D1153B33D20EE9D7BF1C90D8AB8707 (void);
// 0x000002EA Newtonsoft.Json.Serialization.JsonPrimitiveContract Newtonsoft.Json.Serialization.DefaultContractResolver::CreatePrimitiveContract(System.Type)
extern void DefaultContractResolver_CreatePrimitiveContract_m905EE823DC54E8453E0B08AF0A152ED957434868 (void);
// 0x000002EB Newtonsoft.Json.Serialization.JsonISerializableContract Newtonsoft.Json.Serialization.DefaultContractResolver::CreateISerializableContract(System.Type)
extern void DefaultContractResolver_CreateISerializableContract_m285FDAFC72C62C89DA24506B12BAB14D10D565D1 (void);
// 0x000002EC Newtonsoft.Json.Serialization.JsonStringContract Newtonsoft.Json.Serialization.DefaultContractResolver::CreateStringContract(System.Type)
extern void DefaultContractResolver_CreateStringContract_m0FF8D2CF443C24B639563092357BE3F23B484B3F (void);
// 0x000002ED Newtonsoft.Json.Serialization.JsonContract Newtonsoft.Json.Serialization.DefaultContractResolver::CreateContract(System.Type)
extern void DefaultContractResolver_CreateContract_m8FE88E99A02200C59EE2AAF2E84D57B20A3AC4DB (void);
// 0x000002EE System.Boolean Newtonsoft.Json.Serialization.DefaultContractResolver::IsJsonPrimitiveType(System.Type)
extern void DefaultContractResolver_IsJsonPrimitiveType_mB267C86CE80DD697664A01771EE4BD85C6478CF4 (void);
// 0x000002EF System.Boolean Newtonsoft.Json.Serialization.DefaultContractResolver::IsIConvertible(System.Type)
extern void DefaultContractResolver_IsIConvertible_mFBEFC1C86B9A2B94D8CFA8770BAA3022AD413E6A (void);
// 0x000002F0 System.Boolean Newtonsoft.Json.Serialization.DefaultContractResolver::CanConvertToString(System.Type)
extern void DefaultContractResolver_CanConvertToString_m0B0E02CFCA83FD471FD9FEC098454FFB4AEEF3B3 (void);
// 0x000002F1 System.Boolean Newtonsoft.Json.Serialization.DefaultContractResolver::IsValidCallback(System.Reflection.MethodInfo,System.Reflection.ParameterInfo[],System.Type,System.Reflection.MethodInfo,System.Type&)
extern void DefaultContractResolver_IsValidCallback_m926057B8F2EBEE755CE8904CC0722EB4466FC02A (void);
// 0x000002F2 System.String Newtonsoft.Json.Serialization.DefaultContractResolver::GetClrTypeFullName(System.Type)
extern void DefaultContractResolver_GetClrTypeFullName_m874A85E4D811BEB8F07F8AA427CA76E435AEDC68 (void);
// 0x000002F3 System.Collections.Generic.IList`1<Newtonsoft.Json.Serialization.JsonProperty> Newtonsoft.Json.Serialization.DefaultContractResolver::CreateProperties(System.Type,Newtonsoft.Json.MemberSerialization)
extern void DefaultContractResolver_CreateProperties_mC30AD58E60D7A798C17AB357CE5A0ACDF0C0E150 (void);
// 0x000002F4 Newtonsoft.Json.Serialization.IValueProvider Newtonsoft.Json.Serialization.DefaultContractResolver::CreateMemberValueProvider(System.Reflection.MemberInfo)
extern void DefaultContractResolver_CreateMemberValueProvider_mBDA94C8EC44BD0ACEA482BC2ED53387AB2EA672D (void);
// 0x000002F5 Newtonsoft.Json.Serialization.JsonProperty Newtonsoft.Json.Serialization.DefaultContractResolver::CreateProperty(System.Reflection.MemberInfo,Newtonsoft.Json.MemberSerialization)
extern void DefaultContractResolver_CreateProperty_m770806DA14683F330DC280DF6695D2D5E794BDB6 (void);
// 0x000002F6 System.Void Newtonsoft.Json.Serialization.DefaultContractResolver::SetPropertySettingsFromAttributes(Newtonsoft.Json.Serialization.JsonProperty,System.Object,System.String,System.Type,Newtonsoft.Json.MemberSerialization,System.Boolean&)
extern void DefaultContractResolver_SetPropertySettingsFromAttributes_mC3D54B83E29C703B94A7EC4CFB629956937D1DD9 (void);
// 0x000002F7 System.Predicate`1<System.Object> Newtonsoft.Json.Serialization.DefaultContractResolver::CreateShouldSerializeTest(System.Reflection.MemberInfo)
extern void DefaultContractResolver_CreateShouldSerializeTest_mEEF774DB36FC2319E4FEAAA5D17155DC9D7E8515 (void);
// 0x000002F8 System.Void Newtonsoft.Json.Serialization.DefaultContractResolver::SetIsSpecifiedActions(Newtonsoft.Json.Serialization.JsonProperty,System.Reflection.MemberInfo,System.Boolean)
extern void DefaultContractResolver_SetIsSpecifiedActions_mD5B4FC516A78292F6FA508C8F273E7348DA6533F (void);
// 0x000002F9 System.String Newtonsoft.Json.Serialization.DefaultContractResolver::ResolvePropertyName(System.String)
extern void DefaultContractResolver_ResolvePropertyName_m2D677E560F07FC358844E4F61CF9B0EEB0755B84 (void);
// 0x000002FA System.String Newtonsoft.Json.Serialization.DefaultContractResolver::ResolveDictionaryKey(System.String)
extern void DefaultContractResolver_ResolveDictionaryKey_m345C32D87B4BB0AC7C82A84279DAEDF70CD1A371 (void);
// 0x000002FB System.String Newtonsoft.Json.Serialization.DefaultContractResolver::GetResolvedPropertyName(System.String)
extern void DefaultContractResolver_GetResolvedPropertyName_mDAF5D5500B14C083619BAC1C8656C018541DC91A (void);
// 0x000002FC System.Void Newtonsoft.Json.Serialization.DefaultContractResolver::.cctor()
extern void DefaultContractResolver__cctor_mCB187CBFD05F59F9AE5EAC55F54257999FD130BC (void);
// 0x000002FD System.Void Newtonsoft.Json.Serialization.DefaultContractResolver/EnumerableDictionaryWrapper`2::.ctor(System.Collections.Generic.IEnumerable`1<System.Collections.Generic.KeyValuePair`2<TEnumeratorKey,TEnumeratorValue>>)
// 0x000002FE System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>> Newtonsoft.Json.Serialization.DefaultContractResolver/EnumerableDictionaryWrapper`2::GetEnumerator()
// 0x000002FF System.Collections.IEnumerator Newtonsoft.Json.Serialization.DefaultContractResolver/EnumerableDictionaryWrapper`2::System.Collections.IEnumerable.GetEnumerator()
// 0x00000300 System.Void Newtonsoft.Json.Serialization.DefaultContractResolver/EnumerableDictionaryWrapper`2/<GetEnumerator>d__2::.ctor(System.Int32)
// 0x00000301 System.Void Newtonsoft.Json.Serialization.DefaultContractResolver/EnumerableDictionaryWrapper`2/<GetEnumerator>d__2::System.IDisposable.Dispose()
// 0x00000302 System.Boolean Newtonsoft.Json.Serialization.DefaultContractResolver/EnumerableDictionaryWrapper`2/<GetEnumerator>d__2::MoveNext()
// 0x00000303 System.Void Newtonsoft.Json.Serialization.DefaultContractResolver/EnumerableDictionaryWrapper`2/<GetEnumerator>d__2::<>m__Finally1()
// 0x00000304 System.Collections.Generic.KeyValuePair`2<System.Object,System.Object> Newtonsoft.Json.Serialization.DefaultContractResolver/EnumerableDictionaryWrapper`2/<GetEnumerator>d__2::System.Collections.Generic.IEnumerator<System.Collections.Generic.KeyValuePair<System.Object,System.Object>>.get_Current()
// 0x00000305 System.Void Newtonsoft.Json.Serialization.DefaultContractResolver/EnumerableDictionaryWrapper`2/<GetEnumerator>d__2::System.Collections.IEnumerator.Reset()
// 0x00000306 System.Object Newtonsoft.Json.Serialization.DefaultContractResolver/EnumerableDictionaryWrapper`2/<GetEnumerator>d__2::System.Collections.IEnumerator.get_Current()
// 0x00000307 System.Void Newtonsoft.Json.Serialization.DefaultContractResolver/<>c::.cctor()
extern void U3CU3Ec__cctor_mC6D9E94CA60CFC141FFC835625A409CFA85F4E47 (void);
// 0x00000308 System.Void Newtonsoft.Json.Serialization.DefaultContractResolver/<>c::.ctor()
extern void U3CU3Ec__ctor_mE66D46B991808823CB3824E4B29BFA6C149E3738 (void);
// 0x00000309 System.Boolean Newtonsoft.Json.Serialization.DefaultContractResolver/<>c::<GetSerializableMembers>b__34_0(System.Reflection.MemberInfo)
extern void U3CU3Ec_U3CGetSerializableMembersU3Eb__34_0_m65BB6F3B51232E8E2926903E2CEF175983AC3889 (void);
// 0x0000030A System.Boolean Newtonsoft.Json.Serialization.DefaultContractResolver/<>c::<GetSerializableMembers>b__34_1(System.Reflection.MemberInfo)
extern void U3CU3Ec_U3CGetSerializableMembersU3Eb__34_1_m983D29D4AF1EA10B3ECA7BED337AEC11157DEF5A (void);
// 0x0000030B System.Collections.Generic.IEnumerable`1<System.Reflection.MemberInfo> Newtonsoft.Json.Serialization.DefaultContractResolver/<>c::<GetExtensionDataMemberForType>b__37_0(System.Type)
extern void U3CU3Ec_U3CGetExtensionDataMemberForTypeU3Eb__37_0_m318BBA49484CCF1794EEE5417693A55717838349 (void);
// 0x0000030C System.Boolean Newtonsoft.Json.Serialization.DefaultContractResolver/<>c::<GetExtensionDataMemberForType>b__37_1(System.Reflection.MemberInfo)
extern void U3CU3Ec_U3CGetExtensionDataMemberForTypeU3Eb__37_1_m828226D1FB4AA70347E063378F6BF4064E7002D3 (void);
// 0x0000030D System.Boolean Newtonsoft.Json.Serialization.DefaultContractResolver/<>c::<GetAttributeConstructor>b__40_0(System.Reflection.ConstructorInfo)
extern void U3CU3Ec_U3CGetAttributeConstructorU3Eb__40_0_m38369B1996FCCE4B636E39DDC6A86D92FD3A9B50 (void);
// 0x0000030E System.Int32 Newtonsoft.Json.Serialization.DefaultContractResolver/<>c::<CreateProperties>b__64_0(Newtonsoft.Json.Serialization.JsonProperty)
extern void U3CU3Ec_U3CCreatePropertiesU3Eb__64_0_m9D1FE6CBD5DC4C760920F028871456CF1ED36668 (void);
// 0x0000030F System.Void Newtonsoft.Json.Serialization.DefaultContractResolver/<>c__DisplayClass38_0::.ctor()
extern void U3CU3Ec__DisplayClass38_0__ctor_m9657B517275E07644EA8F2780E8530AB46582701 (void);
// 0x00000310 System.Void Newtonsoft.Json.Serialization.DefaultContractResolver/<>c__DisplayClass38_1::.ctor()
extern void U3CU3Ec__DisplayClass38_1__ctor_m480F13BCCC0D3EC2A81ACBD4DC1D40A0D14A006B (void);
// 0x00000311 System.Void Newtonsoft.Json.Serialization.DefaultContractResolver/<>c__DisplayClass38_1::<SetExtensionDataDelegates>b__0(System.Object,System.String,System.Object)
extern void U3CU3Ec__DisplayClass38_1_U3CSetExtensionDataDelegatesU3Eb__0_m1351516A62F1D2E6BBD3950DCE4699F3F427F103 (void);
// 0x00000312 System.Void Newtonsoft.Json.Serialization.DefaultContractResolver/<>c__DisplayClass38_2::.ctor()
extern void U3CU3Ec__DisplayClass38_2__ctor_m3B6A995ED361FD637F5C890F6A330DB29C642C46 (void);
// 0x00000313 System.Collections.Generic.IEnumerable`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>> Newtonsoft.Json.Serialization.DefaultContractResolver/<>c__DisplayClass38_2::<SetExtensionDataDelegates>b__1(System.Object)
extern void U3CU3Ec__DisplayClass38_2_U3CSetExtensionDataDelegatesU3Eb__1_m49B9CB881ECC01B15E43254D46A5692A06D2D2D4 (void);
// 0x00000314 System.Void Newtonsoft.Json.Serialization.DefaultContractResolver/<>c__DisplayClass52_0::.ctor()
extern void U3CU3Ec__DisplayClass52_0__ctor_m0E4A38D649056BACFD2EABC64E065902BCA5AB1C (void);
// 0x00000315 System.String Newtonsoft.Json.Serialization.DefaultContractResolver/<>c__DisplayClass52_0::<CreateDictionaryContract>b__0(System.String)
extern void U3CU3Ec__DisplayClass52_0_U3CCreateDictionaryContractU3Eb__0_mD31CCB9C2E5FCCD990199DF086122D9EA0607600 (void);
// 0x00000316 System.Void Newtonsoft.Json.Serialization.DefaultContractResolver/<>c__DisplayClass68_0::.ctor()
extern void U3CU3Ec__DisplayClass68_0__ctor_mE7807E8FF0E1599649F65AF657A2B02F90E18829 (void);
// 0x00000317 System.Boolean Newtonsoft.Json.Serialization.DefaultContractResolver/<>c__DisplayClass68_0::<CreateShouldSerializeTest>b__0(System.Object)
extern void U3CU3Ec__DisplayClass68_0_U3CCreateShouldSerializeTestU3Eb__0_mA20EFBB3085AFE25FE3DA66E0ADD0A770E158F1D (void);
// 0x00000318 System.Void Newtonsoft.Json.Serialization.DefaultContractResolver/<>c__DisplayClass69_0::.ctor()
extern void U3CU3Ec__DisplayClass69_0__ctor_m828A909BC5D34B88E20B77CD87E7ED09ED12EBDD (void);
// 0x00000319 System.Boolean Newtonsoft.Json.Serialization.DefaultContractResolver/<>c__DisplayClass69_0::<SetIsSpecifiedActions>b__0(System.Object)
extern void U3CU3Ec__DisplayClass69_0_U3CSetIsSpecifiedActionsU3Eb__0_m8E32452942E950B4073E43EF6D9292CB07E4A382 (void);
// 0x0000031A Newtonsoft.Json.Utilities.BidirectionalDictionary`2<System.String,System.Object> Newtonsoft.Json.Serialization.DefaultReferenceResolver::GetMappings(System.Object)
extern void DefaultReferenceResolver_GetMappings_mC93AD74A9E72769593AC962549A995F502CB03EC (void);
// 0x0000031B System.Object Newtonsoft.Json.Serialization.DefaultReferenceResolver::ResolveReference(System.Object,System.String)
extern void DefaultReferenceResolver_ResolveReference_m8090D406BAE4BE95231765B7BC8211AB00CAE209 (void);
// 0x0000031C System.String Newtonsoft.Json.Serialization.DefaultReferenceResolver::GetReference(System.Object,System.Object)
extern void DefaultReferenceResolver_GetReference_m070DCB3C0946C06186E8F944CAAC520A003169F1 (void);
// 0x0000031D System.Void Newtonsoft.Json.Serialization.DefaultReferenceResolver::AddReference(System.Object,System.String,System.Object)
extern void DefaultReferenceResolver_AddReference_m98949E37CB315CC7E2E79647A164D49792B2E054 (void);
// 0x0000031E System.Boolean Newtonsoft.Json.Serialization.DefaultReferenceResolver::IsReferenced(System.Object,System.Object)
extern void DefaultReferenceResolver_IsReferenced_m2F6C90F4E3CC680BB0BEB8F1B89166F51C109FE3 (void);
// 0x0000031F System.Void Newtonsoft.Json.Serialization.DefaultReferenceResolver::.ctor()
extern void DefaultReferenceResolver__ctor_m1BB6EC923095F4B46B37EEBEAE3B8BD3D85198E2 (void);
// 0x00000320 System.Type Newtonsoft.Json.Serialization.DefaultSerializationBinder::GetTypeFromTypeNameKey(Newtonsoft.Json.Serialization.DefaultSerializationBinder/TypeNameKey)
extern void DefaultSerializationBinder_GetTypeFromTypeNameKey_mE3EA2E940C753D1755172E66B6D39377020AC4BC (void);
// 0x00000321 System.Type Newtonsoft.Json.Serialization.DefaultSerializationBinder::BindToType(System.String,System.String)
extern void DefaultSerializationBinder_BindToType_m988DE7A8EEDC0732A87703F23BC85ECB426FFCF1 (void);
// 0x00000322 System.Void Newtonsoft.Json.Serialization.DefaultSerializationBinder::.ctor()
extern void DefaultSerializationBinder__ctor_m8836533BB55F5E0F9B11B6322FC8B153FD1B1F64 (void);
// 0x00000323 System.Void Newtonsoft.Json.Serialization.DefaultSerializationBinder::.cctor()
extern void DefaultSerializationBinder__cctor_mF0026829E183636EE3EC682FA11168FF52938FFA (void);
// 0x00000324 System.Void Newtonsoft.Json.Serialization.DefaultSerializationBinder/TypeNameKey::.ctor(System.String,System.String)
extern void TypeNameKey__ctor_mDC4275014619C46DE6BB5BE66BEEEA2962D1DDCD (void);
// 0x00000325 System.Int32 Newtonsoft.Json.Serialization.DefaultSerializationBinder/TypeNameKey::GetHashCode()
extern void TypeNameKey_GetHashCode_m6F14E3CA5E0098A41949DDA45EC588069B2F7A4A (void);
// 0x00000326 System.Boolean Newtonsoft.Json.Serialization.DefaultSerializationBinder/TypeNameKey::Equals(System.Object)
extern void TypeNameKey_Equals_m2508626D8FED8CA829246608EE62F8E2D34B5AA5 (void);
// 0x00000327 System.Boolean Newtonsoft.Json.Serialization.DefaultSerializationBinder/TypeNameKey::Equals(Newtonsoft.Json.Serialization.DefaultSerializationBinder/TypeNameKey)
extern void TypeNameKey_Equals_m4114698DBBE4F208BBD13645CF9DF3500A5B2F6E (void);
// 0x00000328 System.Void Newtonsoft.Json.Serialization.ErrorContext::.ctor(System.Object,System.Object,System.String,System.Exception)
extern void ErrorContext__ctor_m37814E73790A84177A3CBA0F04B6C29F170AFA4A (void);
// 0x00000329 System.Boolean Newtonsoft.Json.Serialization.ErrorContext::get_Traced()
extern void ErrorContext_get_Traced_m2600308CFFECA1B408B14E621987926BBB1799C5 (void);
// 0x0000032A System.Void Newtonsoft.Json.Serialization.ErrorContext::set_Traced(System.Boolean)
extern void ErrorContext_set_Traced_m8BA7FD9673E83668AB685995DCF399BFFF77A21E (void);
// 0x0000032B System.Exception Newtonsoft.Json.Serialization.ErrorContext::get_Error()
extern void ErrorContext_get_Error_m4BA9AE89A1463A8187BCAA2081B10A7EE412B4B8 (void);
// 0x0000032C System.Void Newtonsoft.Json.Serialization.ErrorContext::set_Error(System.Exception)
extern void ErrorContext_set_Error_m3C47A4456A7D207FCB0AA8DBDFC0EACE349ACB27 (void);
// 0x0000032D System.Void Newtonsoft.Json.Serialization.ErrorContext::set_OriginalObject(System.Object)
extern void ErrorContext_set_OriginalObject_m2D5A6F54F6DBB717BFEFFE41ABFD5F8FF944C8BA (void);
// 0x0000032E System.Void Newtonsoft.Json.Serialization.ErrorContext::set_Member(System.Object)
extern void ErrorContext_set_Member_mA60560017299B78E3269E2E30E178D46CD52DE5E (void);
// 0x0000032F System.Void Newtonsoft.Json.Serialization.ErrorContext::set_Path(System.String)
extern void ErrorContext_set_Path_mAB5E68DCF6CFD684635470CEE002AFBA2ECCA6DF (void);
// 0x00000330 System.Boolean Newtonsoft.Json.Serialization.ErrorContext::get_Handled()
extern void ErrorContext_get_Handled_m00CB858B5DF909C699425AA6A1EB383ABA527158 (void);
// 0x00000331 System.Void Newtonsoft.Json.Serialization.ErrorEventArgs::set_CurrentObject(System.Object)
extern void ErrorEventArgs_set_CurrentObject_mE2464018DBF50495E7AC91C004C29BB678F408EC (void);
// 0x00000332 System.Void Newtonsoft.Json.Serialization.ErrorEventArgs::set_ErrorContext(Newtonsoft.Json.Serialization.ErrorContext)
extern void ErrorEventArgs_set_ErrorContext_mD68107F654E6143A90BA4187E54E358ADA9F3EEF (void);
// 0x00000333 System.Void Newtonsoft.Json.Serialization.ErrorEventArgs::.ctor(System.Object,Newtonsoft.Json.Serialization.ErrorContext)
extern void ErrorEventArgs__ctor_mBC42EEE6BE59E2670574EE359B63B14E374232AE (void);
// 0x00000334 Newtonsoft.Json.Serialization.JsonContract Newtonsoft.Json.Serialization.IContractResolver::ResolveContract(System.Type)
// 0x00000335 System.Object Newtonsoft.Json.Serialization.IReferenceResolver::ResolveReference(System.Object,System.String)
// 0x00000336 System.String Newtonsoft.Json.Serialization.IReferenceResolver::GetReference(System.Object,System.Object)
// 0x00000337 System.Boolean Newtonsoft.Json.Serialization.IReferenceResolver::IsReferenced(System.Object,System.Object)
// 0x00000338 System.Void Newtonsoft.Json.Serialization.IReferenceResolver::AddReference(System.Object,System.String,System.Object)
// 0x00000339 System.Diagnostics.TraceLevel Newtonsoft.Json.Serialization.ITraceWriter::get_LevelFilter()
// 0x0000033A System.Void Newtonsoft.Json.Serialization.ITraceWriter::Trace(System.Diagnostics.TraceLevel,System.String,System.Exception)
// 0x0000033B System.Void Newtonsoft.Json.Serialization.IValueProvider::SetValue(System.Object,System.Object)
// 0x0000033C System.Object Newtonsoft.Json.Serialization.IValueProvider::GetValue(System.Object)
// 0x0000033D System.Type Newtonsoft.Json.Serialization.JsonArrayContract::get_CollectionItemType()
extern void JsonArrayContract_get_CollectionItemType_m3CECF4280B1768FDEF6A7111741C93DE7F7DDF22 (void);
// 0x0000033E System.Void Newtonsoft.Json.Serialization.JsonArrayContract::set_CollectionItemType(System.Type)
extern void JsonArrayContract_set_CollectionItemType_m459F1B1BB37AA94FC3386F62D51E850E3D3B9654 (void);
// 0x0000033F System.Boolean Newtonsoft.Json.Serialization.JsonArrayContract::get_IsMultidimensionalArray()
extern void JsonArrayContract_get_IsMultidimensionalArray_m882BEE069C4D30998786CDEF57704C966CA33E62 (void);
// 0x00000340 System.Void Newtonsoft.Json.Serialization.JsonArrayContract::set_IsMultidimensionalArray(System.Boolean)
extern void JsonArrayContract_set_IsMultidimensionalArray_m85FD29E6E2FA63221F8CC1150E97D5D53016C251 (void);
// 0x00000341 System.Boolean Newtonsoft.Json.Serialization.JsonArrayContract::get_IsArray()
extern void JsonArrayContract_get_IsArray_m72D6C22445B07CFE617D61656301BEAB4C39CAD8 (void);
// 0x00000342 System.Void Newtonsoft.Json.Serialization.JsonArrayContract::set_IsArray(System.Boolean)
extern void JsonArrayContract_set_IsArray_m7F43AFECAC00B630DBB56A94DA548141448AF781 (void);
// 0x00000343 System.Boolean Newtonsoft.Json.Serialization.JsonArrayContract::get_ShouldCreateWrapper()
extern void JsonArrayContract_get_ShouldCreateWrapper_mE8AEA40AFF96BA8EA72B04C1FC377E98A5F21086 (void);
// 0x00000344 System.Void Newtonsoft.Json.Serialization.JsonArrayContract::set_ShouldCreateWrapper(System.Boolean)
extern void JsonArrayContract_set_ShouldCreateWrapper_m437A0E5C1DE254AFB886EB246A854E775EA12C72 (void);
// 0x00000345 System.Boolean Newtonsoft.Json.Serialization.JsonArrayContract::get_CanDeserialize()
extern void JsonArrayContract_get_CanDeserialize_mBCB4583F10A61CBDC6C60CA4D88D07366CEE9583 (void);
// 0x00000346 System.Void Newtonsoft.Json.Serialization.JsonArrayContract::set_CanDeserialize(System.Boolean)
extern void JsonArrayContract_set_CanDeserialize_mCA795402C7F20B1704011379FF99BFDBEA09946E (void);
// 0x00000347 Newtonsoft.Json.Serialization.ObjectConstructor`1<System.Object> Newtonsoft.Json.Serialization.JsonArrayContract::get_ParameterizedCreator()
extern void JsonArrayContract_get_ParameterizedCreator_m6BE2BE1919355F38BCA0E64EACD9F42EA028E9F0 (void);
// 0x00000348 Newtonsoft.Json.Serialization.ObjectConstructor`1<System.Object> Newtonsoft.Json.Serialization.JsonArrayContract::get_OverrideCreator()
extern void JsonArrayContract_get_OverrideCreator_m73CB534700A57A13B8D02F6170FF8506C6280884 (void);
// 0x00000349 System.Void Newtonsoft.Json.Serialization.JsonArrayContract::set_OverrideCreator(Newtonsoft.Json.Serialization.ObjectConstructor`1<System.Object>)
extern void JsonArrayContract_set_OverrideCreator_mFD5EEF176B2E1C97BF6CBC050BFC6DD8F0857E55 (void);
// 0x0000034A System.Boolean Newtonsoft.Json.Serialization.JsonArrayContract::get_HasParameterizedCreator()
extern void JsonArrayContract_get_HasParameterizedCreator_m4BC9037E6219B65657C06880A51DE9B22B2F38B7 (void);
// 0x0000034B System.Void Newtonsoft.Json.Serialization.JsonArrayContract::set_HasParameterizedCreator(System.Boolean)
extern void JsonArrayContract_set_HasParameterizedCreator_m85CC98F0D51C11A1570956CC7319BE4C21AAA962 (void);
// 0x0000034C System.Boolean Newtonsoft.Json.Serialization.JsonArrayContract::get_HasParameterizedCreatorInternal()
extern void JsonArrayContract_get_HasParameterizedCreatorInternal_m3C21C185FC5FD64814F6A5087FF1EAB9A8FBEB9E (void);
// 0x0000034D System.Void Newtonsoft.Json.Serialization.JsonArrayContract::.ctor(System.Type)
extern void JsonArrayContract__ctor_mA86B976D1A8C684E2D95D734631E8C5331CC7298 (void);
// 0x0000034E Newtonsoft.Json.Utilities.IWrappedCollection Newtonsoft.Json.Serialization.JsonArrayContract::CreateWrapper(System.Object)
extern void JsonArrayContract_CreateWrapper_m60F6BC00877480B1179F57162ED7974F246B1B9A (void);
// 0x0000034F System.Collections.IList Newtonsoft.Json.Serialization.JsonArrayContract::CreateTemporaryCollection()
extern void JsonArrayContract_CreateTemporaryCollection_m6B57285CB312925F2B0E951982023A55F9A830FC (void);
// 0x00000350 Newtonsoft.Json.Serialization.JsonContract Newtonsoft.Json.Serialization.JsonContainerContract::get_ItemContract()
extern void JsonContainerContract_get_ItemContract_m463F7BCD86D434C87FB39B8AA51E75F527347CAF (void);
// 0x00000351 System.Void Newtonsoft.Json.Serialization.JsonContainerContract::set_ItemContract(Newtonsoft.Json.Serialization.JsonContract)
extern void JsonContainerContract_set_ItemContract_m48A45754147BCD70811C5CA691445F5B396A1AE0 (void);
// 0x00000352 Newtonsoft.Json.Serialization.JsonContract Newtonsoft.Json.Serialization.JsonContainerContract::get_FinalItemContract()
extern void JsonContainerContract_get_FinalItemContract_mB64876030157CE20B3AB40938213BD49EFDD5121 (void);
// 0x00000353 Newtonsoft.Json.JsonConverter Newtonsoft.Json.Serialization.JsonContainerContract::get_ItemConverter()
extern void JsonContainerContract_get_ItemConverter_mCC1FF5CD91BE1433DE49D71A5000D148A247D07B (void);
// 0x00000354 System.Void Newtonsoft.Json.Serialization.JsonContainerContract::set_ItemConverter(Newtonsoft.Json.JsonConverter)
extern void JsonContainerContract_set_ItemConverter_m22A233E3F1FEFEE00C494880802E72B9CE364301 (void);
// 0x00000355 System.Nullable`1<System.Boolean> Newtonsoft.Json.Serialization.JsonContainerContract::get_ItemIsReference()
extern void JsonContainerContract_get_ItemIsReference_mFF08D7B8117AD227F7B655F89690250790FCED61 (void);
// 0x00000356 System.Void Newtonsoft.Json.Serialization.JsonContainerContract::set_ItemIsReference(System.Nullable`1<System.Boolean>)
extern void JsonContainerContract_set_ItemIsReference_m976F9ED94F787C37541FFE9CFCD2687AB7152CBF (void);
// 0x00000357 System.Nullable`1<Newtonsoft.Json.ReferenceLoopHandling> Newtonsoft.Json.Serialization.JsonContainerContract::get_ItemReferenceLoopHandling()
extern void JsonContainerContract_get_ItemReferenceLoopHandling_m4497940C72F5F38E574A4D180D8D2CD919A812BC (void);
// 0x00000358 System.Void Newtonsoft.Json.Serialization.JsonContainerContract::set_ItemReferenceLoopHandling(System.Nullable`1<Newtonsoft.Json.ReferenceLoopHandling>)
extern void JsonContainerContract_set_ItemReferenceLoopHandling_m90E562BE972F1EF5478B7ED2703E27F5BE4ED27D (void);
// 0x00000359 System.Nullable`1<Newtonsoft.Json.TypeNameHandling> Newtonsoft.Json.Serialization.JsonContainerContract::get_ItemTypeNameHandling()
extern void JsonContainerContract_get_ItemTypeNameHandling_m805E6E8EB10B640579EF897F4A74E566D80F4498 (void);
// 0x0000035A System.Void Newtonsoft.Json.Serialization.JsonContainerContract::set_ItemTypeNameHandling(System.Nullable`1<Newtonsoft.Json.TypeNameHandling>)
extern void JsonContainerContract_set_ItemTypeNameHandling_mCA66B3AA082975A16A7DA94DB8F18722F5369447 (void);
// 0x0000035B System.Void Newtonsoft.Json.Serialization.JsonContainerContract::.ctor(System.Type)
extern void JsonContainerContract__ctor_m276B184509C72D124EA627FFD5877FA5D4707A54 (void);
// 0x0000035C System.Void Newtonsoft.Json.Serialization.SerializationCallback::.ctor(System.Object,System.IntPtr)
extern void SerializationCallback__ctor_m6390FE6B107CA5EAB4EC582D0E814888EC3680CA (void);
// 0x0000035D System.Void Newtonsoft.Json.Serialization.SerializationCallback::Invoke(System.Object,System.Runtime.Serialization.StreamingContext)
extern void SerializationCallback_Invoke_m98C5D43F75883EA2EFD0B8E5DF2B3996A1A3D820 (void);
// 0x0000035E System.IAsyncResult Newtonsoft.Json.Serialization.SerializationCallback::BeginInvoke(System.Object,System.Runtime.Serialization.StreamingContext,System.AsyncCallback,System.Object)
extern void SerializationCallback_BeginInvoke_mF0BA12B078DD0258912A68952DD078100F2A2FC7 (void);
// 0x0000035F System.Void Newtonsoft.Json.Serialization.SerializationCallback::EndInvoke(System.IAsyncResult)
extern void SerializationCallback_EndInvoke_mA939F9CCA8AD61FF04550DD641140185A89100EB (void);
// 0x00000360 System.Void Newtonsoft.Json.Serialization.SerializationErrorCallback::.ctor(System.Object,System.IntPtr)
extern void SerializationErrorCallback__ctor_m11C9CA3B46DB9BD502DF841CE81F601DDC2683EE (void);
// 0x00000361 System.Void Newtonsoft.Json.Serialization.SerializationErrorCallback::Invoke(System.Object,System.Runtime.Serialization.StreamingContext,Newtonsoft.Json.Serialization.ErrorContext)
extern void SerializationErrorCallback_Invoke_m1A623FEF15281AAF20963A8725839BFC98E1FA64 (void);
// 0x00000362 System.IAsyncResult Newtonsoft.Json.Serialization.SerializationErrorCallback::BeginInvoke(System.Object,System.Runtime.Serialization.StreamingContext,Newtonsoft.Json.Serialization.ErrorContext,System.AsyncCallback,System.Object)
extern void SerializationErrorCallback_BeginInvoke_m99B5D1C187200AD2A22155C76FA0B53730CA44B3 (void);
// 0x00000363 System.Void Newtonsoft.Json.Serialization.SerializationErrorCallback::EndInvoke(System.IAsyncResult)
extern void SerializationErrorCallback_EndInvoke_mF47C19FD1740E216FA2A34D9689B9F0520864D93 (void);
// 0x00000364 System.Void Newtonsoft.Json.Serialization.ExtensionDataSetter::.ctor(System.Object,System.IntPtr)
extern void ExtensionDataSetter__ctor_m5E3910D1EDD285D8A810AE6E0259DB1ED750F33F (void);
// 0x00000365 System.Void Newtonsoft.Json.Serialization.ExtensionDataSetter::Invoke(System.Object,System.String,System.Object)
extern void ExtensionDataSetter_Invoke_m0932F6E9B6CEBA1BB5AE16DCBCD3968A76061162 (void);
// 0x00000366 System.IAsyncResult Newtonsoft.Json.Serialization.ExtensionDataSetter::BeginInvoke(System.Object,System.String,System.Object,System.AsyncCallback,System.Object)
extern void ExtensionDataSetter_BeginInvoke_mEC6C7BC0462B31CAE86B47A03959AEC34676792A (void);
// 0x00000367 System.Void Newtonsoft.Json.Serialization.ExtensionDataSetter::EndInvoke(System.IAsyncResult)
extern void ExtensionDataSetter_EndInvoke_m3CC583BF62EAD752BE77B44756E219BFE19CF10D (void);
// 0x00000368 System.Void Newtonsoft.Json.Serialization.ExtensionDataGetter::.ctor(System.Object,System.IntPtr)
extern void ExtensionDataGetter__ctor_mD4E6092AD33162E3FAF8C58BD0CBA698CA2152B4 (void);
// 0x00000369 System.Collections.Generic.IEnumerable`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>> Newtonsoft.Json.Serialization.ExtensionDataGetter::Invoke(System.Object)
extern void ExtensionDataGetter_Invoke_m4F90DCC828CF287792A0B884359B5AA86EE1F96D (void);
// 0x0000036A System.IAsyncResult Newtonsoft.Json.Serialization.ExtensionDataGetter::BeginInvoke(System.Object,System.AsyncCallback,System.Object)
extern void ExtensionDataGetter_BeginInvoke_m02765E41942A104693DC26C6344F8E18AA3FC75C (void);
// 0x0000036B System.Collections.Generic.IEnumerable`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>> Newtonsoft.Json.Serialization.ExtensionDataGetter::EndInvoke(System.IAsyncResult)
extern void ExtensionDataGetter_EndInvoke_mEA0F820536DC1D475F0F4932E773D132916BA8A6 (void);
// 0x0000036C System.Type Newtonsoft.Json.Serialization.JsonContract::get_UnderlyingType()
extern void JsonContract_get_UnderlyingType_m274B4718678409B2511BBE941FF2D647CA533DC3 (void);
// 0x0000036D System.Void Newtonsoft.Json.Serialization.JsonContract::set_UnderlyingType(System.Type)
extern void JsonContract_set_UnderlyingType_m82F3DEB8DC8339C6CB32187651D87CE330DE13AE (void);
// 0x0000036E System.Type Newtonsoft.Json.Serialization.JsonContract::get_CreatedType()
extern void JsonContract_get_CreatedType_m083775257FF46EBCD6F1CD5F2195F8A8E4F9ECBF (void);
// 0x0000036F System.Void Newtonsoft.Json.Serialization.JsonContract::set_CreatedType(System.Type)
extern void JsonContract_set_CreatedType_mCC0F045EE40A673F6567490F79A9BA9C57F5498A (void);
// 0x00000370 System.Nullable`1<System.Boolean> Newtonsoft.Json.Serialization.JsonContract::get_IsReference()
extern void JsonContract_get_IsReference_m85AD9382BF6556FBB63A383D1ECA6BC6C5C369A6 (void);
// 0x00000371 System.Void Newtonsoft.Json.Serialization.JsonContract::set_IsReference(System.Nullable`1<System.Boolean>)
extern void JsonContract_set_IsReference_m21803C0B2D772F7C3EED60332E2E2CA69D6D54F3 (void);
// 0x00000372 Newtonsoft.Json.JsonConverter Newtonsoft.Json.Serialization.JsonContract::get_Converter()
extern void JsonContract_get_Converter_m92C6064189D577AA5D08771BE755B3BFBCB297A9 (void);
// 0x00000373 System.Void Newtonsoft.Json.Serialization.JsonContract::set_Converter(Newtonsoft.Json.JsonConverter)
extern void JsonContract_set_Converter_mA7452FEC775D9674F8163B299C151249DC7E544F (void);
// 0x00000374 Newtonsoft.Json.JsonConverter Newtonsoft.Json.Serialization.JsonContract::get_InternalConverter()
extern void JsonContract_get_InternalConverter_mB36C4060D4251DE1F51662B767D2779C40C65BCA (void);
// 0x00000375 System.Void Newtonsoft.Json.Serialization.JsonContract::set_InternalConverter(Newtonsoft.Json.JsonConverter)
extern void JsonContract_set_InternalConverter_mAB2F562DCD689CC7E50B4BE84AEE73E2995D782F (void);
// 0x00000376 System.Collections.Generic.IList`1<Newtonsoft.Json.Serialization.SerializationCallback> Newtonsoft.Json.Serialization.JsonContract::get_OnDeserializedCallbacks()
extern void JsonContract_get_OnDeserializedCallbacks_m51D3705D21A5D46F5C44CD2AEC69D35FB805E88B (void);
// 0x00000377 System.Collections.Generic.IList`1<Newtonsoft.Json.Serialization.SerializationCallback> Newtonsoft.Json.Serialization.JsonContract::get_OnDeserializingCallbacks()
extern void JsonContract_get_OnDeserializingCallbacks_mA7864AB96D49B6FC9E2CFBA9EBEBD9B82D95C63E (void);
// 0x00000378 System.Collections.Generic.IList`1<Newtonsoft.Json.Serialization.SerializationCallback> Newtonsoft.Json.Serialization.JsonContract::get_OnSerializedCallbacks()
extern void JsonContract_get_OnSerializedCallbacks_m669138963D8D20031F8F005C94AEB4CD7592F7DD (void);
// 0x00000379 System.Collections.Generic.IList`1<Newtonsoft.Json.Serialization.SerializationCallback> Newtonsoft.Json.Serialization.JsonContract::get_OnSerializingCallbacks()
extern void JsonContract_get_OnSerializingCallbacks_m4EB4397D6E4F21716A1DDFB5F9BCD107EF04AB08 (void);
// 0x0000037A System.Collections.Generic.IList`1<Newtonsoft.Json.Serialization.SerializationErrorCallback> Newtonsoft.Json.Serialization.JsonContract::get_OnErrorCallbacks()
extern void JsonContract_get_OnErrorCallbacks_m1B7A5E421C1FCB7CF207CBBD0C3BC5E0EB1058FF (void);
// 0x0000037B System.Func`1<System.Object> Newtonsoft.Json.Serialization.JsonContract::get_DefaultCreator()
extern void JsonContract_get_DefaultCreator_m8271898AFA4F8DBDC5A018F79FD4EC5450839B81 (void);
// 0x0000037C System.Void Newtonsoft.Json.Serialization.JsonContract::set_DefaultCreator(System.Func`1<System.Object>)
extern void JsonContract_set_DefaultCreator_m0D4E0BD379C8342A4BD6C6D73F19BE330526F825 (void);
// 0x0000037D System.Boolean Newtonsoft.Json.Serialization.JsonContract::get_DefaultCreatorNonPublic()
extern void JsonContract_get_DefaultCreatorNonPublic_mFEDABA69CB6CA3FDE1FB163D1192C74307F2A15F (void);
// 0x0000037E System.Void Newtonsoft.Json.Serialization.JsonContract::set_DefaultCreatorNonPublic(System.Boolean)
extern void JsonContract_set_DefaultCreatorNonPublic_mDCCE7A1E9DDED83BFFD22F33BC6E660F70CB4FEF (void);
// 0x0000037F System.Void Newtonsoft.Json.Serialization.JsonContract::.ctor(System.Type)
extern void JsonContract__ctor_m735CB01EA1C75415DEC0BCD297F9622B94477F78 (void);
// 0x00000380 System.Void Newtonsoft.Json.Serialization.JsonContract::InvokeOnSerializing(System.Object,System.Runtime.Serialization.StreamingContext)
extern void JsonContract_InvokeOnSerializing_mE71754D61053B520E71AB5333F329969370553FB (void);
// 0x00000381 System.Void Newtonsoft.Json.Serialization.JsonContract::InvokeOnSerialized(System.Object,System.Runtime.Serialization.StreamingContext)
extern void JsonContract_InvokeOnSerialized_m50B6E71E04D2AA8A1253102A0C7EDFB1B5123385 (void);
// 0x00000382 System.Void Newtonsoft.Json.Serialization.JsonContract::InvokeOnDeserializing(System.Object,System.Runtime.Serialization.StreamingContext)
extern void JsonContract_InvokeOnDeserializing_m963E996327E2AE515165D915543B20799FB865AA (void);
// 0x00000383 System.Void Newtonsoft.Json.Serialization.JsonContract::InvokeOnDeserialized(System.Object,System.Runtime.Serialization.StreamingContext)
extern void JsonContract_InvokeOnDeserialized_mB59C124B20738ECDBA8CEE41C7B5E567BB945C4C (void);
// 0x00000384 System.Void Newtonsoft.Json.Serialization.JsonContract::InvokeOnError(System.Object,System.Runtime.Serialization.StreamingContext,Newtonsoft.Json.Serialization.ErrorContext)
extern void JsonContract_InvokeOnError_mEB2325BA5AFE7EEC24B6C84E6F14985A74F95D66 (void);
// 0x00000385 Newtonsoft.Json.Serialization.SerializationCallback Newtonsoft.Json.Serialization.JsonContract::CreateSerializationCallback(System.Reflection.MethodInfo)
extern void JsonContract_CreateSerializationCallback_mA43D137D94C5B97C73A7589A19C4873CB7E21128 (void);
// 0x00000386 Newtonsoft.Json.Serialization.SerializationErrorCallback Newtonsoft.Json.Serialization.JsonContract::CreateSerializationErrorCallback(System.Reflection.MethodInfo)
extern void JsonContract_CreateSerializationErrorCallback_m2D1541916958EDA3A52FACF50A026112EEDC94D6 (void);
// 0x00000387 System.Void Newtonsoft.Json.Serialization.JsonContract/<>c__DisplayClass73_0::.ctor()
extern void U3CU3Ec__DisplayClass73_0__ctor_m327FFFA0BFB55C64621A1600416F6051D2CC8F2C (void);
// 0x00000388 System.Void Newtonsoft.Json.Serialization.JsonContract/<>c__DisplayClass73_0::<CreateSerializationCallback>b__0(System.Object,System.Runtime.Serialization.StreamingContext)
extern void U3CU3Ec__DisplayClass73_0_U3CCreateSerializationCallbackU3Eb__0_mE55246604A2198788812A1B4BA3B191EB3103BD8 (void);
// 0x00000389 System.Void Newtonsoft.Json.Serialization.JsonContract/<>c__DisplayClass74_0::.ctor()
extern void U3CU3Ec__DisplayClass74_0__ctor_m32174118CAC919A99277D1670F6F5A45B2A5DE11 (void);
// 0x0000038A System.Void Newtonsoft.Json.Serialization.JsonContract/<>c__DisplayClass74_0::<CreateSerializationErrorCallback>b__0(System.Object,System.Runtime.Serialization.StreamingContext,Newtonsoft.Json.Serialization.ErrorContext)
extern void U3CU3Ec__DisplayClass74_0_U3CCreateSerializationErrorCallbackU3Eb__0_m4F0A8CF50687635531818316C9BE6CF622827EF6 (void);
// 0x0000038B System.Func`2<System.String,System.String> Newtonsoft.Json.Serialization.JsonDictionaryContract::get_DictionaryKeyResolver()
extern void JsonDictionaryContract_get_DictionaryKeyResolver_m9FEC62269B460CBE840125B2C29F98F35FD18568 (void);
// 0x0000038C System.Void Newtonsoft.Json.Serialization.JsonDictionaryContract::set_DictionaryKeyResolver(System.Func`2<System.String,System.String>)
extern void JsonDictionaryContract_set_DictionaryKeyResolver_m7DBA6B179284AF2B20F35EFD0A53D1C1A7A1338E (void);
// 0x0000038D System.Type Newtonsoft.Json.Serialization.JsonDictionaryContract::get_DictionaryKeyType()
extern void JsonDictionaryContract_get_DictionaryKeyType_m955BFAF792651E88B651BD2C619097726DCB76FD (void);
// 0x0000038E System.Void Newtonsoft.Json.Serialization.JsonDictionaryContract::set_DictionaryKeyType(System.Type)
extern void JsonDictionaryContract_set_DictionaryKeyType_m6B1EDD598D5F7E159443731827F01450C44B11CB (void);
// 0x0000038F System.Type Newtonsoft.Json.Serialization.JsonDictionaryContract::get_DictionaryValueType()
extern void JsonDictionaryContract_get_DictionaryValueType_m45B9DDEF68FCAAD1D2FD90391817AF79A2915922 (void);
// 0x00000390 System.Void Newtonsoft.Json.Serialization.JsonDictionaryContract::set_DictionaryValueType(System.Type)
extern void JsonDictionaryContract_set_DictionaryValueType_m05D9BED20D4CA80764E423E027F66DB3A0818E18 (void);
// 0x00000391 Newtonsoft.Json.Serialization.JsonContract Newtonsoft.Json.Serialization.JsonDictionaryContract::get_KeyContract()
extern void JsonDictionaryContract_get_KeyContract_mC7BAD25BBB5DF24FA9FA1B704FB9153A1526EB8B (void);
// 0x00000392 System.Void Newtonsoft.Json.Serialization.JsonDictionaryContract::set_KeyContract(Newtonsoft.Json.Serialization.JsonContract)
extern void JsonDictionaryContract_set_KeyContract_m522C30EF20C09B50E617E24698E0BE1D84033EFC (void);
// 0x00000393 System.Boolean Newtonsoft.Json.Serialization.JsonDictionaryContract::get_ShouldCreateWrapper()
extern void JsonDictionaryContract_get_ShouldCreateWrapper_mD43CBCD1A26F23DFF9EF7F4069AA4BC99CE03BCC (void);
// 0x00000394 System.Void Newtonsoft.Json.Serialization.JsonDictionaryContract::set_ShouldCreateWrapper(System.Boolean)
extern void JsonDictionaryContract_set_ShouldCreateWrapper_m81F5B13DB3FE9CC060B07F1943975289E930DBD0 (void);
// 0x00000395 Newtonsoft.Json.Serialization.ObjectConstructor`1<System.Object> Newtonsoft.Json.Serialization.JsonDictionaryContract::get_ParameterizedCreator()
extern void JsonDictionaryContract_get_ParameterizedCreator_m709BFD1415EEBFC68DD858E1CF1A0213B63C0D1D (void);
// 0x00000396 Newtonsoft.Json.Serialization.ObjectConstructor`1<System.Object> Newtonsoft.Json.Serialization.JsonDictionaryContract::get_OverrideCreator()
extern void JsonDictionaryContract_get_OverrideCreator_mD200635EDEDDC59BAB34FA2372D037C2C7F07BD6 (void);
// 0x00000397 System.Void Newtonsoft.Json.Serialization.JsonDictionaryContract::set_OverrideCreator(Newtonsoft.Json.Serialization.ObjectConstructor`1<System.Object>)
extern void JsonDictionaryContract_set_OverrideCreator_m28EBE8929923BCACFB3A8322FF3A841F33368C52 (void);
// 0x00000398 System.Boolean Newtonsoft.Json.Serialization.JsonDictionaryContract::get_HasParameterizedCreator()
extern void JsonDictionaryContract_get_HasParameterizedCreator_m2AC31619D694866F47219F7228D781FDB7559361 (void);
// 0x00000399 System.Void Newtonsoft.Json.Serialization.JsonDictionaryContract::set_HasParameterizedCreator(System.Boolean)
extern void JsonDictionaryContract_set_HasParameterizedCreator_m5C3555EBD3982F9FC072BDAF78014CC2C49F777D (void);
// 0x0000039A System.Boolean Newtonsoft.Json.Serialization.JsonDictionaryContract::get_HasParameterizedCreatorInternal()
extern void JsonDictionaryContract_get_HasParameterizedCreatorInternal_m63D219D7A4FA312DAA9DC21DC5E1FB06D6E5DBE4 (void);
// 0x0000039B System.Void Newtonsoft.Json.Serialization.JsonDictionaryContract::.ctor(System.Type)
extern void JsonDictionaryContract__ctor_m6D73B62F2292E1E876B950AE5AFD8D33E5D51366 (void);
// 0x0000039C Newtonsoft.Json.Utilities.IWrappedDictionary Newtonsoft.Json.Serialization.JsonDictionaryContract::CreateWrapper(System.Object)
extern void JsonDictionaryContract_CreateWrapper_mDF0D8C2A477D8AF463BD24A2ABAE7B90611755BA (void);
// 0x0000039D System.Collections.IDictionary Newtonsoft.Json.Serialization.JsonDictionaryContract::CreateTemporaryDictionary()
extern void JsonDictionaryContract_CreateTemporaryDictionary_m0B22B816AF9C5B5CA42BC98A1EAEB486E95A40B0 (void);
// 0x0000039E System.Void Newtonsoft.Json.Serialization.JsonISerializableContract::set_ISerializableCreator(Newtonsoft.Json.Serialization.ObjectConstructor`1<System.Object>)
extern void JsonISerializableContract_set_ISerializableCreator_m5295CF43688CAD573D840026F52A80256EF73A24 (void);
// 0x0000039F System.Void Newtonsoft.Json.Serialization.JsonISerializableContract::.ctor(System.Type)
extern void JsonISerializableContract__ctor_m56BCC3490F830E1A520D20A155264E4D9DC676BB (void);
// 0x000003A0 Newtonsoft.Json.MemberSerialization Newtonsoft.Json.Serialization.JsonObjectContract::get_MemberSerialization()
extern void JsonObjectContract_get_MemberSerialization_m927C496830ECEE969FEFCBD9015D2975B64EB7EA (void);
// 0x000003A1 System.Void Newtonsoft.Json.Serialization.JsonObjectContract::set_MemberSerialization(Newtonsoft.Json.MemberSerialization)
extern void JsonObjectContract_set_MemberSerialization_m80C86B38A9A42F0A2D096B74CFC52E8EE250175E (void);
// 0x000003A2 System.Nullable`1<Newtonsoft.Json.Required> Newtonsoft.Json.Serialization.JsonObjectContract::get_ItemRequired()
extern void JsonObjectContract_get_ItemRequired_m75C6C4175B7C1473543857FFB985D37C39AD8C82 (void);
// 0x000003A3 System.Void Newtonsoft.Json.Serialization.JsonObjectContract::set_ItemRequired(System.Nullable`1<Newtonsoft.Json.Required>)
extern void JsonObjectContract_set_ItemRequired_m322E068F6060AB63F5C6B2E61DC986925C674E07 (void);
// 0x000003A4 Newtonsoft.Json.Serialization.JsonPropertyCollection Newtonsoft.Json.Serialization.JsonObjectContract::get_Properties()
extern void JsonObjectContract_get_Properties_mD4B212D9A137993DE37F53402D769D0D905E3110 (void);
// 0x000003A5 System.Void Newtonsoft.Json.Serialization.JsonObjectContract::set_Properties(Newtonsoft.Json.Serialization.JsonPropertyCollection)
extern void JsonObjectContract_set_Properties_mDC67F335196A048B9DF12F878BAAE0B20328314B (void);
// 0x000003A6 Newtonsoft.Json.Serialization.JsonPropertyCollection Newtonsoft.Json.Serialization.JsonObjectContract::get_CreatorParameters()
extern void JsonObjectContract_get_CreatorParameters_m2537D42265D44FCD577D1FE3A3AFD91048356123 (void);
// 0x000003A7 System.Void Newtonsoft.Json.Serialization.JsonObjectContract::set_OverrideConstructor(System.Reflection.ConstructorInfo)
extern void JsonObjectContract_set_OverrideConstructor_m235F3E2A09CB30F758F80DE69FF0A8449737C2C5 (void);
// 0x000003A8 System.Void Newtonsoft.Json.Serialization.JsonObjectContract::set_ParametrizedConstructor(System.Reflection.ConstructorInfo)
extern void JsonObjectContract_set_ParametrizedConstructor_m569FE50A49972AC16601C46A9ACDAB0F83DD1242 (void);
// 0x000003A9 Newtonsoft.Json.Serialization.ObjectConstructor`1<System.Object> Newtonsoft.Json.Serialization.JsonObjectContract::get_OverrideCreator()
extern void JsonObjectContract_get_OverrideCreator_m9BE981868AC8BA4316074FC143FA8DC9F2911275 (void);
// 0x000003AA Newtonsoft.Json.Serialization.ObjectConstructor`1<System.Object> Newtonsoft.Json.Serialization.JsonObjectContract::get_ParameterizedCreator()
extern void JsonObjectContract_get_ParameterizedCreator_m53CF73CCDCB1F71922CCABF43F8A95C1FDA3B41F (void);
// 0x000003AB Newtonsoft.Json.Serialization.ExtensionDataSetter Newtonsoft.Json.Serialization.JsonObjectContract::get_ExtensionDataSetter()
extern void JsonObjectContract_get_ExtensionDataSetter_m2DFB59696B0D14849D0F4E443DD82AA5A76B243C (void);
// 0x000003AC System.Void Newtonsoft.Json.Serialization.JsonObjectContract::set_ExtensionDataSetter(Newtonsoft.Json.Serialization.ExtensionDataSetter)
extern void JsonObjectContract_set_ExtensionDataSetter_m39A79B22B522DB922D27C30377BD082401A787B6 (void);
// 0x000003AD Newtonsoft.Json.Serialization.ExtensionDataGetter Newtonsoft.Json.Serialization.JsonObjectContract::get_ExtensionDataGetter()
extern void JsonObjectContract_get_ExtensionDataGetter_m80664EEE79810B192D96E02FCD344B03EE7ACF35 (void);
// 0x000003AE System.Void Newtonsoft.Json.Serialization.JsonObjectContract::set_ExtensionDataGetter(Newtonsoft.Json.Serialization.ExtensionDataGetter)
extern void JsonObjectContract_set_ExtensionDataGetter_m52A1E838E07D96D828C5BB724E77BE2F17313980 (void);
// 0x000003AF System.Void Newtonsoft.Json.Serialization.JsonObjectContract::set_ExtensionDataValueType(System.Type)
extern void JsonObjectContract_set_ExtensionDataValueType_mB3DF262A7425DBC2F147CA65A6CC3BAE00CC7E0B (void);
// 0x000003B0 System.Boolean Newtonsoft.Json.Serialization.JsonObjectContract::get_HasRequiredOrDefaultValueProperties()
extern void JsonObjectContract_get_HasRequiredOrDefaultValueProperties_mAF97B1C6E6C1BDA031FB5EC9C626C864ADBC44D5 (void);
// 0x000003B1 System.Void Newtonsoft.Json.Serialization.JsonObjectContract::.ctor(System.Type)
extern void JsonObjectContract__ctor_m75B16514965E3A3BF86D3BBB9F4656D424927116 (void);
// 0x000003B2 System.Object Newtonsoft.Json.Serialization.JsonObjectContract::GetUninitializedObject()
extern void JsonObjectContract_GetUninitializedObject_m9E5A10E243C300FCD8448D31E3B0B3B18F890AC4 (void);
// 0x000003B3 Newtonsoft.Json.Utilities.PrimitiveTypeCode Newtonsoft.Json.Serialization.JsonPrimitiveContract::get_TypeCode()
extern void JsonPrimitiveContract_get_TypeCode_mEED6CF3DA675CB2F244C3E02209F37FC8887C544 (void);
// 0x000003B4 System.Void Newtonsoft.Json.Serialization.JsonPrimitiveContract::set_TypeCode(Newtonsoft.Json.Utilities.PrimitiveTypeCode)
extern void JsonPrimitiveContract_set_TypeCode_mD01C4FD88E81143FD4FE9446D08E10CBED5D16CB (void);
// 0x000003B5 System.Void Newtonsoft.Json.Serialization.JsonPrimitiveContract::.ctor(System.Type)
extern void JsonPrimitiveContract__ctor_m3E1FB6F5CF9442C0BDA4516FE390E1F27A37D5B4 (void);
// 0x000003B6 System.Void Newtonsoft.Json.Serialization.JsonPrimitiveContract::.cctor()
extern void JsonPrimitiveContract__cctor_m2349BE67983674123BCEC8BD6CDCEBF55C87E511 (void);
// 0x000003B7 Newtonsoft.Json.Serialization.JsonContract Newtonsoft.Json.Serialization.JsonProperty::get_PropertyContract()
extern void JsonProperty_get_PropertyContract_m1A07AE508ED64BF7DAFD6B768CAB11E8F07AC218 (void);
// 0x000003B8 System.Void Newtonsoft.Json.Serialization.JsonProperty::set_PropertyContract(Newtonsoft.Json.Serialization.JsonContract)
extern void JsonProperty_set_PropertyContract_mA51FB41ACB2877BDA0005FFB84D6724AD6269B3D (void);
// 0x000003B9 System.String Newtonsoft.Json.Serialization.JsonProperty::get_PropertyName()
extern void JsonProperty_get_PropertyName_m72817401FC680F87EC4202862CD5B52CF6300C60 (void);
// 0x000003BA System.Void Newtonsoft.Json.Serialization.JsonProperty::set_PropertyName(System.String)
extern void JsonProperty_set_PropertyName_m99CCA5636B8B8F8FE8F7100266FC4830FD8DA846 (void);
// 0x000003BB System.Type Newtonsoft.Json.Serialization.JsonProperty::get_DeclaringType()
extern void JsonProperty_get_DeclaringType_m0A64AF3ECCED18859E78E63E3A264AAB74586322 (void);
// 0x000003BC System.Void Newtonsoft.Json.Serialization.JsonProperty::set_DeclaringType(System.Type)
extern void JsonProperty_set_DeclaringType_m4FA21E3ADA7905038A4B8410922A4D678DB627BD (void);
// 0x000003BD System.Nullable`1<System.Int32> Newtonsoft.Json.Serialization.JsonProperty::get_Order()
extern void JsonProperty_get_Order_mEF3E149DB2B59B8CA34317D3C09DFF703F31EF81 (void);
// 0x000003BE System.Void Newtonsoft.Json.Serialization.JsonProperty::set_Order(System.Nullable`1<System.Int32>)
extern void JsonProperty_set_Order_m360E9E66EB70C639663D3A29A37CD4FFB9217F56 (void);
// 0x000003BF System.String Newtonsoft.Json.Serialization.JsonProperty::get_UnderlyingName()
extern void JsonProperty_get_UnderlyingName_m8204307F227D4B3D9701133A47172F819B08219C (void);
// 0x000003C0 System.Void Newtonsoft.Json.Serialization.JsonProperty::set_UnderlyingName(System.String)
extern void JsonProperty_set_UnderlyingName_mCD3B44976B4DFFD01B148B7D7A88AE92A71F6C2F (void);
// 0x000003C1 Newtonsoft.Json.Serialization.IValueProvider Newtonsoft.Json.Serialization.JsonProperty::get_ValueProvider()
extern void JsonProperty_get_ValueProvider_mB9CD0B24D58FCF1EE82DA3A3935973CD560C971B (void);
// 0x000003C2 System.Void Newtonsoft.Json.Serialization.JsonProperty::set_ValueProvider(Newtonsoft.Json.Serialization.IValueProvider)
extern void JsonProperty_set_ValueProvider_m95563EC88EE32866DACCC6C2AE8F0FE656A5D847 (void);
// 0x000003C3 System.Void Newtonsoft.Json.Serialization.JsonProperty::set_AttributeProvider(Newtonsoft.Json.Serialization.IAttributeProvider)
extern void JsonProperty_set_AttributeProvider_mC95FBFBE2AA39AD182245F28F860C0E77A2A7FD2 (void);
// 0x000003C4 System.Type Newtonsoft.Json.Serialization.JsonProperty::get_PropertyType()
extern void JsonProperty_get_PropertyType_mA61CDE8B995125100D36BA65D8112CA75961F019 (void);
// 0x000003C5 System.Void Newtonsoft.Json.Serialization.JsonProperty::set_PropertyType(System.Type)
extern void JsonProperty_set_PropertyType_mC3FA4BCD6DD37EA6F7E8AB42312F70A76C294484 (void);
// 0x000003C6 Newtonsoft.Json.JsonConverter Newtonsoft.Json.Serialization.JsonProperty::get_Converter()
extern void JsonProperty_get_Converter_mD78430E0A86FFCAD9077CA45A8515433C1A7ACAF (void);
// 0x000003C7 System.Void Newtonsoft.Json.Serialization.JsonProperty::set_Converter(Newtonsoft.Json.JsonConverter)
extern void JsonProperty_set_Converter_m09958BDD926D75B7CCBA933BE95D63DF69DE2831 (void);
// 0x000003C8 Newtonsoft.Json.JsonConverter Newtonsoft.Json.Serialization.JsonProperty::get_MemberConverter()
extern void JsonProperty_get_MemberConverter_m4AAF8854081CD744699C3A0BAF9314E817FDDD43 (void);
// 0x000003C9 System.Void Newtonsoft.Json.Serialization.JsonProperty::set_MemberConverter(Newtonsoft.Json.JsonConverter)
extern void JsonProperty_set_MemberConverter_m02E881B46A470612898913FEDB7568219E7254BB (void);
// 0x000003CA System.Boolean Newtonsoft.Json.Serialization.JsonProperty::get_Ignored()
extern void JsonProperty_get_Ignored_m46548BB8982F27C3B6FA9874BB36BBBE93C5BE5C (void);
// 0x000003CB System.Void Newtonsoft.Json.Serialization.JsonProperty::set_Ignored(System.Boolean)
extern void JsonProperty_set_Ignored_m633FB01A6826E5E0A0AD5EADA49C0AAB4FC61943 (void);
// 0x000003CC System.Boolean Newtonsoft.Json.Serialization.JsonProperty::get_Readable()
extern void JsonProperty_get_Readable_m7AEAE72E61CEE2D7204DF7DDE4BB46FD49ACF13A (void);
// 0x000003CD System.Void Newtonsoft.Json.Serialization.JsonProperty::set_Readable(System.Boolean)
extern void JsonProperty_set_Readable_m200607F2CB3518230B19840B81318784938634C1 (void);
// 0x000003CE System.Boolean Newtonsoft.Json.Serialization.JsonProperty::get_Writable()
extern void JsonProperty_get_Writable_mD0AD78FD1A02462591351FA144C3A3E3F3D71C04 (void);
// 0x000003CF System.Void Newtonsoft.Json.Serialization.JsonProperty::set_Writable(System.Boolean)
extern void JsonProperty_set_Writable_m1EAC194E3A9B4958ADBC14262F398F126E22669D (void);
// 0x000003D0 System.Boolean Newtonsoft.Json.Serialization.JsonProperty::get_HasMemberAttribute()
extern void JsonProperty_get_HasMemberAttribute_m8D7C6BB1CB98AB72EFA491333E9BD9DD9A8EDD4B (void);
// 0x000003D1 System.Void Newtonsoft.Json.Serialization.JsonProperty::set_HasMemberAttribute(System.Boolean)
extern void JsonProperty_set_HasMemberAttribute_m39D8D4BE4DD54068759166CF360FF13F27479EC6 (void);
// 0x000003D2 System.Object Newtonsoft.Json.Serialization.JsonProperty::get_DefaultValue()
extern void JsonProperty_get_DefaultValue_m022CCDE65C84390EDB18D613BB1D6639825FCD36 (void);
// 0x000003D3 System.Void Newtonsoft.Json.Serialization.JsonProperty::set_DefaultValue(System.Object)
extern void JsonProperty_set_DefaultValue_m83E7EE87272CAA2FAB10BE523817E5D6C248F64C (void);
// 0x000003D4 System.Object Newtonsoft.Json.Serialization.JsonProperty::GetResolvedDefaultValue()
extern void JsonProperty_GetResolvedDefaultValue_m2F663D0C4330BFEF8954915CD2804D47A20DE6A9 (void);
// 0x000003D5 Newtonsoft.Json.Required Newtonsoft.Json.Serialization.JsonProperty::get_Required()
extern void JsonProperty_get_Required_m047BD7FEDA7B029D9E4E1C7190CDD4BAA66F95AD (void);
// 0x000003D6 System.Nullable`1<System.Boolean> Newtonsoft.Json.Serialization.JsonProperty::get_IsReference()
extern void JsonProperty_get_IsReference_m153CA8FA5C547CC6B05661BE83264D4C965B9396 (void);
// 0x000003D7 System.Void Newtonsoft.Json.Serialization.JsonProperty::set_IsReference(System.Nullable`1<System.Boolean>)
extern void JsonProperty_set_IsReference_mB2D4152E154865A7D98A41608BB5DF115514B3C1 (void);
// 0x000003D8 System.Nullable`1<Newtonsoft.Json.NullValueHandling> Newtonsoft.Json.Serialization.JsonProperty::get_NullValueHandling()
extern void JsonProperty_get_NullValueHandling_m42B929FF14E1D403D8DCBEE4C71937234A81ACB1 (void);
// 0x000003D9 System.Void Newtonsoft.Json.Serialization.JsonProperty::set_NullValueHandling(System.Nullable`1<Newtonsoft.Json.NullValueHandling>)
extern void JsonProperty_set_NullValueHandling_m0DFFF6AA9EB90290031B2A8AA34B5F4860650701 (void);
// 0x000003DA System.Nullable`1<Newtonsoft.Json.DefaultValueHandling> Newtonsoft.Json.Serialization.JsonProperty::get_DefaultValueHandling()
extern void JsonProperty_get_DefaultValueHandling_m07D9A4D362F431188F9B92F069FA06522F8313AE (void);
// 0x000003DB System.Void Newtonsoft.Json.Serialization.JsonProperty::set_DefaultValueHandling(System.Nullable`1<Newtonsoft.Json.DefaultValueHandling>)
extern void JsonProperty_set_DefaultValueHandling_mC9F3205408913D8B21FA525CCDAC7AD032E71EDA (void);
// 0x000003DC System.Nullable`1<Newtonsoft.Json.ReferenceLoopHandling> Newtonsoft.Json.Serialization.JsonProperty::get_ReferenceLoopHandling()
extern void JsonProperty_get_ReferenceLoopHandling_m36303DDEA2267BA0C5AEE6F04C31376769E24DA2 (void);
// 0x000003DD System.Void Newtonsoft.Json.Serialization.JsonProperty::set_ReferenceLoopHandling(System.Nullable`1<Newtonsoft.Json.ReferenceLoopHandling>)
extern void JsonProperty_set_ReferenceLoopHandling_m6CE09B4B487EDFDF5D4ADF5B9750278478C1728A (void);
// 0x000003DE System.Nullable`1<Newtonsoft.Json.ObjectCreationHandling> Newtonsoft.Json.Serialization.JsonProperty::get_ObjectCreationHandling()
extern void JsonProperty_get_ObjectCreationHandling_mFD14FC71400EAA2ABE2953F94D8ED12B9C5EA137 (void);
// 0x000003DF System.Void Newtonsoft.Json.Serialization.JsonProperty::set_ObjectCreationHandling(System.Nullable`1<Newtonsoft.Json.ObjectCreationHandling>)
extern void JsonProperty_set_ObjectCreationHandling_m049C8E802C5BED6F45FC9BC69F6FCF492F3AC05D (void);
// 0x000003E0 System.Nullable`1<Newtonsoft.Json.TypeNameHandling> Newtonsoft.Json.Serialization.JsonProperty::get_TypeNameHandling()
extern void JsonProperty_get_TypeNameHandling_m8AEB537470EE0ED5BFE82E69FC9351EFFF39E20D (void);
// 0x000003E1 System.Void Newtonsoft.Json.Serialization.JsonProperty::set_TypeNameHandling(System.Nullable`1<Newtonsoft.Json.TypeNameHandling>)
extern void JsonProperty_set_TypeNameHandling_mFF34196E4C937D5CB8C7BFAC959C9EDBABABD42B (void);
// 0x000003E2 System.Predicate`1<System.Object> Newtonsoft.Json.Serialization.JsonProperty::get_ShouldSerialize()
extern void JsonProperty_get_ShouldSerialize_m3DEC6D4BDF9DD0467C9496E31D064F1D584131AD (void);
// 0x000003E3 System.Void Newtonsoft.Json.Serialization.JsonProperty::set_ShouldSerialize(System.Predicate`1<System.Object>)
extern void JsonProperty_set_ShouldSerialize_m691B604D63F062A266B8AA0FA42CE22A52D1C80A (void);
// 0x000003E4 System.Predicate`1<System.Object> Newtonsoft.Json.Serialization.JsonProperty::get_ShouldDeserialize()
extern void JsonProperty_get_ShouldDeserialize_m5E69820CB6EF8CBCA0B173519DB4B32C7D6B3F26 (void);
// 0x000003E5 System.Predicate`1<System.Object> Newtonsoft.Json.Serialization.JsonProperty::get_GetIsSpecified()
extern void JsonProperty_get_GetIsSpecified_m2D32E7F92B46871C97992E41AB0BADCCDE3715A3 (void);
// 0x000003E6 System.Void Newtonsoft.Json.Serialization.JsonProperty::set_GetIsSpecified(System.Predicate`1<System.Object>)
extern void JsonProperty_set_GetIsSpecified_m69FEAC27F917FDA7B479399D6127C5A8BFDFD1A8 (void);
// 0x000003E7 System.Action`2<System.Object,System.Object> Newtonsoft.Json.Serialization.JsonProperty::get_SetIsSpecified()
extern void JsonProperty_get_SetIsSpecified_mB3A71DEAD82264AA63FB0A0E53E85DAB582AF220 (void);
// 0x000003E8 System.Void Newtonsoft.Json.Serialization.JsonProperty::set_SetIsSpecified(System.Action`2<System.Object,System.Object>)
extern void JsonProperty_set_SetIsSpecified_m43AD00A6F292CE4CD5DD72CF33B5AEB186BC115E (void);
// 0x000003E9 System.String Newtonsoft.Json.Serialization.JsonProperty::ToString()
extern void JsonProperty_ToString_mE92D7E46DB796643D47E57BFDD6398730839873E (void);
// 0x000003EA Newtonsoft.Json.JsonConverter Newtonsoft.Json.Serialization.JsonProperty::get_ItemConverter()
extern void JsonProperty_get_ItemConverter_mF43A1CB4574AACC026916EB46DD3665428AD9E9E (void);
// 0x000003EB System.Void Newtonsoft.Json.Serialization.JsonProperty::set_ItemConverter(Newtonsoft.Json.JsonConverter)
extern void JsonProperty_set_ItemConverter_mE132EF0F991D7A67FF1CC4B190F3A20680A74484 (void);
// 0x000003EC System.Nullable`1<System.Boolean> Newtonsoft.Json.Serialization.JsonProperty::get_ItemIsReference()
extern void JsonProperty_get_ItemIsReference_m4FB1049E629A8E0C7532E7F42B5784838B641FE0 (void);
// 0x000003ED System.Void Newtonsoft.Json.Serialization.JsonProperty::set_ItemIsReference(System.Nullable`1<System.Boolean>)
extern void JsonProperty_set_ItemIsReference_m5520B121F62448B77C3005291868E4A6C363F04B (void);
// 0x000003EE System.Nullable`1<Newtonsoft.Json.TypeNameHandling> Newtonsoft.Json.Serialization.JsonProperty::get_ItemTypeNameHandling()
extern void JsonProperty_get_ItemTypeNameHandling_mE9229CB08007B7C54F5FB9E1E100741102577526 (void);
// 0x000003EF System.Void Newtonsoft.Json.Serialization.JsonProperty::set_ItemTypeNameHandling(System.Nullable`1<Newtonsoft.Json.TypeNameHandling>)
extern void JsonProperty_set_ItemTypeNameHandling_mB26022D26C0E5DF718210F7F91150A8AC42E79A6 (void);
// 0x000003F0 System.Nullable`1<Newtonsoft.Json.ReferenceLoopHandling> Newtonsoft.Json.Serialization.JsonProperty::get_ItemReferenceLoopHandling()
extern void JsonProperty_get_ItemReferenceLoopHandling_mCF84F2423F3441D4FF3A55CA0EB91A3C13F7C20E (void);
// 0x000003F1 System.Void Newtonsoft.Json.Serialization.JsonProperty::set_ItemReferenceLoopHandling(System.Nullable`1<Newtonsoft.Json.ReferenceLoopHandling>)
extern void JsonProperty_set_ItemReferenceLoopHandling_mA482C2F7321AB96C932D7FD5A3C4FBF6283E9B16 (void);
// 0x000003F2 System.Void Newtonsoft.Json.Serialization.JsonProperty::WritePropertyName(Newtonsoft.Json.JsonWriter)
extern void JsonProperty_WritePropertyName_mA063F926230E98D32E25F607C1844D680E902F7C (void);
// 0x000003F3 System.Void Newtonsoft.Json.Serialization.JsonProperty::.ctor()
extern void JsonProperty__ctor_m0C17B59D12EFA92F9504D5EAB99DCEF61D2A021D (void);
// 0x000003F4 System.Void Newtonsoft.Json.Serialization.JsonPropertyCollection::.ctor(System.Type)
extern void JsonPropertyCollection__ctor_m10AF88EAE4E37DD81F6985739229547D87855F26 (void);
// 0x000003F5 System.String Newtonsoft.Json.Serialization.JsonPropertyCollection::GetKeyForItem(Newtonsoft.Json.Serialization.JsonProperty)
extern void JsonPropertyCollection_GetKeyForItem_m74EE6C0E7D1BFEE99644E7BBA97E163E55384670 (void);
// 0x000003F6 System.Void Newtonsoft.Json.Serialization.JsonPropertyCollection::AddProperty(Newtonsoft.Json.Serialization.JsonProperty)
extern void JsonPropertyCollection_AddProperty_m55D0AC4383A26BC34CAFE2577450F02260002BE2 (void);
// 0x000003F7 Newtonsoft.Json.Serialization.JsonProperty Newtonsoft.Json.Serialization.JsonPropertyCollection::GetClosestMatchProperty(System.String)
extern void JsonPropertyCollection_GetClosestMatchProperty_mC56AF3BDDFA034EBC09EDB347BB1A61B7C3E03B3 (void);
// 0x000003F8 System.Boolean Newtonsoft.Json.Serialization.JsonPropertyCollection::TryGetValue(System.String,Newtonsoft.Json.Serialization.JsonProperty&)
extern void JsonPropertyCollection_TryGetValue_mA7A8A893D9A31EA6D12639E323991E183105B810 (void);
// 0x000003F9 Newtonsoft.Json.Serialization.JsonProperty Newtonsoft.Json.Serialization.JsonPropertyCollection::GetProperty(System.String,System.StringComparison)
extern void JsonPropertyCollection_GetProperty_mD5A03BC6B22DDFA6C4175238DCE4C697C0850F04 (void);
// 0x000003FA System.Void Newtonsoft.Json.Serialization.JsonSerializerInternalBase::.ctor(Newtonsoft.Json.JsonSerializer)
extern void JsonSerializerInternalBase__ctor_mF661DD347384ACEA2E8D0F8772FDE3FCE318A040 (void);
// 0x000003FB Newtonsoft.Json.Utilities.BidirectionalDictionary`2<System.String,System.Object> Newtonsoft.Json.Serialization.JsonSerializerInternalBase::get_DefaultReferenceMappings()
extern void JsonSerializerInternalBase_get_DefaultReferenceMappings_m0AD1C4B7991FB623BC3284D35984092D300EC513 (void);
// 0x000003FC Newtonsoft.Json.Serialization.ErrorContext Newtonsoft.Json.Serialization.JsonSerializerInternalBase::GetErrorContext(System.Object,System.Object,System.String,System.Exception)
extern void JsonSerializerInternalBase_GetErrorContext_mD795A0D91081E591F9260CE33A3AE297FBD1FF7D (void);
// 0x000003FD System.Void Newtonsoft.Json.Serialization.JsonSerializerInternalBase::ClearErrorContext()
extern void JsonSerializerInternalBase_ClearErrorContext_m0DBFE7B75E7D0828B2A411205B35C091608B3299 (void);
// 0x000003FE System.Boolean Newtonsoft.Json.Serialization.JsonSerializerInternalBase::IsErrorHandled(System.Object,Newtonsoft.Json.Serialization.JsonContract,System.Object,Newtonsoft.Json.IJsonLineInfo,System.String,System.Exception)
extern void JsonSerializerInternalBase_IsErrorHandled_mC5DE21D3927A503792F8AE5962BC965BB4689E85 (void);
// 0x000003FF System.Boolean Newtonsoft.Json.Serialization.JsonSerializerInternalBase/ReferenceEqualsEqualityComparer::System.Collections.Generic.IEqualityComparer<System.Object>.Equals(System.Object,System.Object)
extern void ReferenceEqualsEqualityComparer_System_Collections_Generic_IEqualityComparerU3CSystem_ObjectU3E_Equals_m9C19FB863E99FB0E8F684911A1A1B0AFAF67AA34 (void);
// 0x00000400 System.Int32 Newtonsoft.Json.Serialization.JsonSerializerInternalBase/ReferenceEqualsEqualityComparer::System.Collections.Generic.IEqualityComparer<System.Object>.GetHashCode(System.Object)
extern void ReferenceEqualsEqualityComparer_System_Collections_Generic_IEqualityComparerU3CSystem_ObjectU3E_GetHashCode_m959D403ACF199A3EB9C7EA6769AE2A2B394FF44A (void);
// 0x00000401 System.Void Newtonsoft.Json.Serialization.JsonSerializerInternalBase/ReferenceEqualsEqualityComparer::.ctor()
extern void ReferenceEqualsEqualityComparer__ctor_m0D20F7AF62826C7CA9086E605EBD9B8585C07952 (void);
// 0x00000402 System.Void Newtonsoft.Json.Serialization.JsonSerializerInternalReader::.ctor(Newtonsoft.Json.JsonSerializer)
extern void JsonSerializerInternalReader__ctor_mA15E63A6DBAEC12311813C5EA19737B00F87129B (void);
// 0x00000403 Newtonsoft.Json.Serialization.JsonContract Newtonsoft.Json.Serialization.JsonSerializerInternalReader::GetContractSafe(System.Type)
extern void JsonSerializerInternalReader_GetContractSafe_m133F75523E811E79248B64D1E180030793C00A8D (void);
// 0x00000404 System.Object Newtonsoft.Json.Serialization.JsonSerializerInternalReader::Deserialize(Newtonsoft.Json.JsonReader,System.Type,System.Boolean)
extern void JsonSerializerInternalReader_Deserialize_m7A8F42D36257DAD366236258CFAD83426DE44E84 (void);
// 0x00000405 Newtonsoft.Json.Serialization.JsonSerializerProxy Newtonsoft.Json.Serialization.JsonSerializerInternalReader::GetInternalSerializer()
extern void JsonSerializerInternalReader_GetInternalSerializer_m1916C24804A7B3BA115F09D72FDC7197634616CA (void);
// 0x00000406 System.Object Newtonsoft.Json.Serialization.JsonSerializerInternalReader::CreateValueInternal(Newtonsoft.Json.JsonReader,System.Type,Newtonsoft.Json.Serialization.JsonContract,Newtonsoft.Json.Serialization.JsonProperty,Newtonsoft.Json.Serialization.JsonContainerContract,Newtonsoft.Json.Serialization.JsonProperty,System.Object)
extern void JsonSerializerInternalReader_CreateValueInternal_m3EC2690B80CB81A28115170993AFE08907AA16BB (void);
// 0x00000407 System.Boolean Newtonsoft.Json.Serialization.JsonSerializerInternalReader::CoerceEmptyStringToNull(System.Type,Newtonsoft.Json.Serialization.JsonContract,System.String)
extern void JsonSerializerInternalReader_CoerceEmptyStringToNull_mFA8BAE9EEA76DABEF2FAAE26BB49FC8BD02A9C4D (void);
// 0x00000408 System.String Newtonsoft.Json.Serialization.JsonSerializerInternalReader::GetExpectedDescription(Newtonsoft.Json.Serialization.JsonContract)
extern void JsonSerializerInternalReader_GetExpectedDescription_m7160A0626A80CF5F80E565D705F3495D0731D556 (void);
// 0x00000409 Newtonsoft.Json.JsonConverter Newtonsoft.Json.Serialization.JsonSerializerInternalReader::GetConverter(Newtonsoft.Json.Serialization.JsonContract,Newtonsoft.Json.JsonConverter,Newtonsoft.Json.Serialization.JsonContainerContract,Newtonsoft.Json.Serialization.JsonProperty)
extern void JsonSerializerInternalReader_GetConverter_m2F2488670D8CFEE6E8C15B3B594705361AFA8C2E (void);
// 0x0000040A System.Object Newtonsoft.Json.Serialization.JsonSerializerInternalReader::CreateObject(Newtonsoft.Json.JsonReader,System.Type,Newtonsoft.Json.Serialization.JsonContract,Newtonsoft.Json.Serialization.JsonProperty,Newtonsoft.Json.Serialization.JsonContainerContract,Newtonsoft.Json.Serialization.JsonProperty,System.Object)
extern void JsonSerializerInternalReader_CreateObject_m92A6187A99FAA790FDE4E5613CC79165CC1D0E42 (void);
// 0x0000040B System.Boolean Newtonsoft.Json.Serialization.JsonSerializerInternalReader::ReadMetadataProperties(Newtonsoft.Json.JsonReader,System.Type&,Newtonsoft.Json.Serialization.JsonContract&,Newtonsoft.Json.Serialization.JsonProperty,Newtonsoft.Json.Serialization.JsonContainerContract,Newtonsoft.Json.Serialization.JsonProperty,System.Object,System.Object&,System.String&)
extern void JsonSerializerInternalReader_ReadMetadataProperties_m9BE7A38FF267C65A82D7B77C701534793F82D188 (void);
// 0x0000040C System.Void Newtonsoft.Json.Serialization.JsonSerializerInternalReader::ResolveTypeName(Newtonsoft.Json.JsonReader,System.Type&,Newtonsoft.Json.Serialization.JsonContract&,Newtonsoft.Json.Serialization.JsonProperty,Newtonsoft.Json.Serialization.JsonContainerContract,Newtonsoft.Json.Serialization.JsonProperty,System.String)
extern void JsonSerializerInternalReader_ResolveTypeName_mFD4144F8CE8539087E30792ED22CECA87FC5BD1F (void);
// 0x0000040D Newtonsoft.Json.Serialization.JsonArrayContract Newtonsoft.Json.Serialization.JsonSerializerInternalReader::EnsureArrayContract(Newtonsoft.Json.JsonReader,System.Type,Newtonsoft.Json.Serialization.JsonContract)
extern void JsonSerializerInternalReader_EnsureArrayContract_m9BC600DA8AB0E77E7EEB608805D841C403D5509A (void);
// 0x0000040E System.Object Newtonsoft.Json.Serialization.JsonSerializerInternalReader::CreateList(Newtonsoft.Json.JsonReader,System.Type,Newtonsoft.Json.Serialization.JsonContract,Newtonsoft.Json.Serialization.JsonProperty,System.Object,System.String)
extern void JsonSerializerInternalReader_CreateList_m205E8682EFD6B5E81D3694813CE4056F8F843A2F (void);
// 0x0000040F System.Boolean Newtonsoft.Json.Serialization.JsonSerializerInternalReader::HasNoDefinedType(Newtonsoft.Json.Serialization.JsonContract)
extern void JsonSerializerInternalReader_HasNoDefinedType_mE8C3693A8F7B03259665691EC4E35782A1BFDEA2 (void);
// 0x00000410 System.Object Newtonsoft.Json.Serialization.JsonSerializerInternalReader::EnsureType(Newtonsoft.Json.JsonReader,System.Object,System.Globalization.CultureInfo,Newtonsoft.Json.Serialization.JsonContract,System.Type)
extern void JsonSerializerInternalReader_EnsureType_m039985AE8A056134EE7998F2B369A212CBFDC7F3 (void);
// 0x00000411 System.Boolean Newtonsoft.Json.Serialization.JsonSerializerInternalReader::SetPropertyValue(Newtonsoft.Json.Serialization.JsonProperty,Newtonsoft.Json.JsonConverter,Newtonsoft.Json.Serialization.JsonContainerContract,Newtonsoft.Json.Serialization.JsonProperty,Newtonsoft.Json.JsonReader,System.Object)
extern void JsonSerializerInternalReader_SetPropertyValue_mF9395418926887BB8E007F238CA4C017F5C7984C (void);
// 0x00000412 System.Boolean Newtonsoft.Json.Serialization.JsonSerializerInternalReader::CalculatePropertyDetails(Newtonsoft.Json.Serialization.JsonProperty,Newtonsoft.Json.JsonConverter&,Newtonsoft.Json.Serialization.JsonContainerContract,Newtonsoft.Json.Serialization.JsonProperty,Newtonsoft.Json.JsonReader,System.Object,System.Boolean&,System.Object&,Newtonsoft.Json.Serialization.JsonContract&,System.Boolean&)
extern void JsonSerializerInternalReader_CalculatePropertyDetails_mE8FE25A1E556F355B0E3348120E106453B2F772A (void);
// 0x00000413 System.Void Newtonsoft.Json.Serialization.JsonSerializerInternalReader::AddReference(Newtonsoft.Json.JsonReader,System.String,System.Object)
extern void JsonSerializerInternalReader_AddReference_m26BDB86BA9303743D16411E7A9B75DF9C8900D91 (void);
// 0x00000414 System.Boolean Newtonsoft.Json.Serialization.JsonSerializerInternalReader::HasFlag(Newtonsoft.Json.DefaultValueHandling,Newtonsoft.Json.DefaultValueHandling)
extern void JsonSerializerInternalReader_HasFlag_mF3538606BFF79804E64F15A436CFD853676E21F0 (void);
// 0x00000415 System.Boolean Newtonsoft.Json.Serialization.JsonSerializerInternalReader::ShouldSetPropertyValue(Newtonsoft.Json.Serialization.JsonProperty,System.Object)
extern void JsonSerializerInternalReader_ShouldSetPropertyValue_mB87EAB4DA3B7A332CE88038FEA94A9EAEAD556BB (void);
// 0x00000416 System.Collections.IList Newtonsoft.Json.Serialization.JsonSerializerInternalReader::CreateNewList(Newtonsoft.Json.JsonReader,Newtonsoft.Json.Serialization.JsonArrayContract,System.Boolean&)
extern void JsonSerializerInternalReader_CreateNewList_mD65C2185CA2E698B9A53FB00F367170E7F7FBE35 (void);
// 0x00000417 System.Collections.IDictionary Newtonsoft.Json.Serialization.JsonSerializerInternalReader::CreateNewDictionary(Newtonsoft.Json.JsonReader,Newtonsoft.Json.Serialization.JsonDictionaryContract,System.Boolean&)
extern void JsonSerializerInternalReader_CreateNewDictionary_m3395D04EDC8B10A5803D5AFED9E7EBAE5EC90E07 (void);
// 0x00000418 System.Void Newtonsoft.Json.Serialization.JsonSerializerInternalReader::OnDeserializing(Newtonsoft.Json.JsonReader,Newtonsoft.Json.Serialization.JsonContract,System.Object)
extern void JsonSerializerInternalReader_OnDeserializing_m5A2F1A8A0126871AC41EB8767E18F38603004775 (void);
// 0x00000419 System.Void Newtonsoft.Json.Serialization.JsonSerializerInternalReader::OnDeserialized(Newtonsoft.Json.JsonReader,Newtonsoft.Json.Serialization.JsonContract,System.Object)
extern void JsonSerializerInternalReader_OnDeserialized_m9603B44FD17BC2F0C15AF6A407097C7C9376D24A (void);
// 0x0000041A System.Object Newtonsoft.Json.Serialization.JsonSerializerInternalReader::PopulateDictionary(System.Collections.IDictionary,Newtonsoft.Json.JsonReader,Newtonsoft.Json.Serialization.JsonDictionaryContract,Newtonsoft.Json.Serialization.JsonProperty,System.String)
extern void JsonSerializerInternalReader_PopulateDictionary_mD14FE13DF213661A8FCBA06FB7C145E7336C09A5 (void);
// 0x0000041B System.Object Newtonsoft.Json.Serialization.JsonSerializerInternalReader::PopulateMultidimensionalArray(System.Collections.IList,Newtonsoft.Json.JsonReader,Newtonsoft.Json.Serialization.JsonArrayContract,Newtonsoft.Json.Serialization.JsonProperty,System.String)
extern void JsonSerializerInternalReader_PopulateMultidimensionalArray_mC47FBA8A46AB0FF8C6489270C784C50BC25CF4EA (void);
// 0x0000041C System.Void Newtonsoft.Json.Serialization.JsonSerializerInternalReader::ThrowUnexpectedEndException(Newtonsoft.Json.JsonReader,Newtonsoft.Json.Serialization.JsonContract,System.Object,System.String)
extern void JsonSerializerInternalReader_ThrowUnexpectedEndException_m05CCBCD2F5FFC11D32C3E8CAE34B7231562700ED (void);
// 0x0000041D System.Object Newtonsoft.Json.Serialization.JsonSerializerInternalReader::PopulateList(System.Collections.IList,Newtonsoft.Json.JsonReader,Newtonsoft.Json.Serialization.JsonArrayContract,Newtonsoft.Json.Serialization.JsonProperty,System.String)
extern void JsonSerializerInternalReader_PopulateList_mFCFE2F29D66EF5B96160573D7E0565C8E77CB7CF (void);
// 0x0000041E System.Object Newtonsoft.Json.Serialization.JsonSerializerInternalReader::CreateObjectUsingCreatorWithParameters(Newtonsoft.Json.JsonReader,Newtonsoft.Json.Serialization.JsonObjectContract,Newtonsoft.Json.Serialization.JsonProperty,Newtonsoft.Json.Serialization.ObjectConstructor`1<System.Object>,System.String)
extern void JsonSerializerInternalReader_CreateObjectUsingCreatorWithParameters_m732582D4C8F53A416C5F47CC5B90667E4C26C7CC (void);
// 0x0000041F System.Object Newtonsoft.Json.Serialization.JsonSerializerInternalReader::DeserializeConvertable(Newtonsoft.Json.JsonConverter,Newtonsoft.Json.JsonReader,System.Type,System.Object)
extern void JsonSerializerInternalReader_DeserializeConvertable_mA54865AF80F0207BF6F4DCAA7282FAAE7C7A13EC (void);
// 0x00000420 System.Collections.Generic.List`1<Newtonsoft.Json.Serialization.JsonSerializerInternalReader/CreatorPropertyContext> Newtonsoft.Json.Serialization.JsonSerializerInternalReader::ResolvePropertyAndCreatorValues(Newtonsoft.Json.Serialization.JsonObjectContract,Newtonsoft.Json.Serialization.JsonProperty,Newtonsoft.Json.JsonReader,System.Type)
extern void JsonSerializerInternalReader_ResolvePropertyAndCreatorValues_mD97877C5DDC4819C7473C73F4BA973A6F1982AE3 (void);
// 0x00000421 System.Boolean Newtonsoft.Json.Serialization.JsonSerializerInternalReader::ReadForType(Newtonsoft.Json.JsonReader,Newtonsoft.Json.Serialization.JsonContract,System.Boolean)
extern void JsonSerializerInternalReader_ReadForType_m7A4CD8CDCE496D22DF7FFE49749F4DA2FAF33A88 (void);
// 0x00000422 System.Object Newtonsoft.Json.Serialization.JsonSerializerInternalReader::CreateNewObject(Newtonsoft.Json.JsonReader,Newtonsoft.Json.Serialization.JsonObjectContract,Newtonsoft.Json.Serialization.JsonProperty,Newtonsoft.Json.Serialization.JsonProperty,System.String,System.Boolean&)
extern void JsonSerializerInternalReader_CreateNewObject_m3376CD7A093DA0440AABBC5AF7055A57558B60B3 (void);
// 0x00000423 System.Object Newtonsoft.Json.Serialization.JsonSerializerInternalReader::PopulateObject(System.Object,Newtonsoft.Json.JsonReader,Newtonsoft.Json.Serialization.JsonObjectContract,Newtonsoft.Json.Serialization.JsonProperty,System.String)
extern void JsonSerializerInternalReader_PopulateObject_m09807E53DD11A8886863F54EF38F276508E4CFDB (void);
// 0x00000424 System.Boolean Newtonsoft.Json.Serialization.JsonSerializerInternalReader::ShouldDeserialize(Newtonsoft.Json.JsonReader,Newtonsoft.Json.Serialization.JsonProperty,System.Object)
extern void JsonSerializerInternalReader_ShouldDeserialize_m03544CC59E193E3328248C20ABB9E3C9E7F48548 (void);
// 0x00000425 System.Boolean Newtonsoft.Json.Serialization.JsonSerializerInternalReader::CheckPropertyName(Newtonsoft.Json.JsonReader,System.String)
extern void JsonSerializerInternalReader_CheckPropertyName_m3651505C873DF94E56E5A6473AE4A5A322396D08 (void);
// 0x00000426 System.Void Newtonsoft.Json.Serialization.JsonSerializerInternalReader::SetExtensionData(Newtonsoft.Json.Serialization.JsonObjectContract,Newtonsoft.Json.Serialization.JsonProperty,Newtonsoft.Json.JsonReader,System.String,System.Object)
extern void JsonSerializerInternalReader_SetExtensionData_mEE71B5B95C6D65AAB92A10253800EE7362E923EA (void);
// 0x00000427 System.Object Newtonsoft.Json.Serialization.JsonSerializerInternalReader::ReadExtensionDataValue(Newtonsoft.Json.Serialization.JsonObjectContract,Newtonsoft.Json.Serialization.JsonProperty,Newtonsoft.Json.JsonReader)
extern void JsonSerializerInternalReader_ReadExtensionDataValue_m7C52479883C81B92FC7206283AE53E85327F3ED3 (void);
// 0x00000428 System.Void Newtonsoft.Json.Serialization.JsonSerializerInternalReader::EndProcessProperty(System.Object,Newtonsoft.Json.JsonReader,Newtonsoft.Json.Serialization.JsonObjectContract,System.Int32,Newtonsoft.Json.Serialization.JsonProperty,Newtonsoft.Json.Serialization.JsonSerializerInternalReader/PropertyPresence,System.Boolean)
extern void JsonSerializerInternalReader_EndProcessProperty_m5779521285FF3FCA14A3B8A145BE289EEEB81C18 (void);
// 0x00000429 System.Void Newtonsoft.Json.Serialization.JsonSerializerInternalReader::SetPropertyPresence(Newtonsoft.Json.JsonReader,Newtonsoft.Json.Serialization.JsonProperty,System.Collections.Generic.Dictionary`2<Newtonsoft.Json.Serialization.JsonProperty,Newtonsoft.Json.Serialization.JsonSerializerInternalReader/PropertyPresence>)
extern void JsonSerializerInternalReader_SetPropertyPresence_m64A239C9F207366C579F2779BEDEBA2A72FA176B (void);
// 0x0000042A System.Void Newtonsoft.Json.Serialization.JsonSerializerInternalReader::HandleError(Newtonsoft.Json.JsonReader,System.Boolean,System.Int32)
extern void JsonSerializerInternalReader_HandleError_m8F99D22C1F0B18119C6957394310D4E535CDCCF2 (void);
// 0x0000042B System.Void Newtonsoft.Json.Serialization.JsonSerializerInternalReader/CreatorPropertyContext::.ctor()
extern void CreatorPropertyContext__ctor_mB81D9D361C71DAE2AAB86DB1F791AD259D0674C4 (void);
// 0x0000042C System.Void Newtonsoft.Json.Serialization.JsonSerializerInternalReader/<>c__DisplayClass31_0::.ctor()
extern void U3CU3Ec__DisplayClass31_0__ctor_m5632FD9B61D2888DFADC0EF8B8A8B04D46FF7BB3 (void);
// 0x0000042D System.Boolean Newtonsoft.Json.Serialization.JsonSerializerInternalReader/<>c__DisplayClass31_0::<CreateObjectUsingCreatorWithParameters>b__1(Newtonsoft.Json.Serialization.JsonSerializerInternalReader/CreatorPropertyContext)
extern void U3CU3Ec__DisplayClass31_0_U3CCreateObjectUsingCreatorWithParametersU3Eb__1_mE93AE83B0F3042FE741E836CC5C6E719D182FA77 (void);
// 0x0000042E System.Void Newtonsoft.Json.Serialization.JsonSerializerInternalReader/<>c::.cctor()
extern void U3CU3Ec__cctor_mB684917E51C88F503F61405714BD76633F355FBB (void);
// 0x0000042F System.Void Newtonsoft.Json.Serialization.JsonSerializerInternalReader/<>c::.ctor()
extern void U3CU3Ec__ctor_mF4F6C6B34C47B9F6E948DBC0A421CE0D2D9FE54C (void);
// 0x00000430 System.String Newtonsoft.Json.Serialization.JsonSerializerInternalReader/<>c::<CreateObjectUsingCreatorWithParameters>b__31_0(Newtonsoft.Json.Serialization.JsonProperty)
extern void U3CU3Ec_U3CCreateObjectUsingCreatorWithParametersU3Eb__31_0_m77B222BDA92C5E53BB37296128EE435BACE70C5D (void);
// 0x00000431 System.String Newtonsoft.Json.Serialization.JsonSerializerInternalReader/<>c::<CreateObjectUsingCreatorWithParameters>b__31_2(Newtonsoft.Json.Serialization.JsonProperty)
extern void U3CU3Ec_U3CCreateObjectUsingCreatorWithParametersU3Eb__31_2_mEBDE616CFA8FDA5E6545636E1E71CC0F7E7E0D78 (void);
// 0x00000432 Newtonsoft.Json.Serialization.JsonProperty Newtonsoft.Json.Serialization.JsonSerializerInternalReader/<>c::<PopulateObject>b__36_0(Newtonsoft.Json.Serialization.JsonProperty)
extern void U3CU3Ec_U3CPopulateObjectU3Eb__36_0_m374EB5D2938C8599E51751DB03A1C23F7979B5E9 (void);
// 0x00000433 Newtonsoft.Json.Serialization.JsonSerializerInternalReader/PropertyPresence Newtonsoft.Json.Serialization.JsonSerializerInternalReader/<>c::<PopulateObject>b__36_1(Newtonsoft.Json.Serialization.JsonProperty)
extern void U3CU3Ec_U3CPopulateObjectU3Eb__36_1_mAB3EE0F153328A00A83058CBE6AE8B6423D0C320 (void);
// 0x00000434 System.Void Newtonsoft.Json.Serialization.JsonSerializerInternalWriter::.ctor(Newtonsoft.Json.JsonSerializer)
extern void JsonSerializerInternalWriter__ctor_m1AFA8C1FA24A1A4EA510C59AECE65B149EDE9355 (void);
// 0x00000435 System.Void Newtonsoft.Json.Serialization.JsonSerializerInternalWriter::Serialize(Newtonsoft.Json.JsonWriter,System.Object,System.Type)
extern void JsonSerializerInternalWriter_Serialize_mF69E647FFD576F1A40BBE6193F383B0417BD5BE1 (void);
// 0x00000436 Newtonsoft.Json.Serialization.JsonSerializerProxy Newtonsoft.Json.Serialization.JsonSerializerInternalWriter::GetInternalSerializer()
extern void JsonSerializerInternalWriter_GetInternalSerializer_m9B37BA0257BD0F4863114C0A3E90F1483A182825 (void);
// 0x00000437 Newtonsoft.Json.Serialization.JsonContract Newtonsoft.Json.Serialization.JsonSerializerInternalWriter::GetContractSafe(System.Object)
extern void JsonSerializerInternalWriter_GetContractSafe_m8D7527C6F95E6E8F9BFB664501251637E30F2C48 (void);
// 0x00000438 System.Void Newtonsoft.Json.Serialization.JsonSerializerInternalWriter::SerializePrimitive(Newtonsoft.Json.JsonWriter,System.Object,Newtonsoft.Json.Serialization.JsonPrimitiveContract,Newtonsoft.Json.Serialization.JsonProperty,Newtonsoft.Json.Serialization.JsonContainerContract,Newtonsoft.Json.Serialization.JsonProperty)
extern void JsonSerializerInternalWriter_SerializePrimitive_m3C715BAF1C55D696DEFB8BB6F52CB7979F9236FD (void);
// 0x00000439 System.Void Newtonsoft.Json.Serialization.JsonSerializerInternalWriter::SerializeValue(Newtonsoft.Json.JsonWriter,System.Object,Newtonsoft.Json.Serialization.JsonContract,Newtonsoft.Json.Serialization.JsonProperty,Newtonsoft.Json.Serialization.JsonContainerContract,Newtonsoft.Json.Serialization.JsonProperty)
extern void JsonSerializerInternalWriter_SerializeValue_m757192B4598E695F87E7733815B124717BE82F0F (void);
// 0x0000043A System.Nullable`1<System.Boolean> Newtonsoft.Json.Serialization.JsonSerializerInternalWriter::ResolveIsReference(Newtonsoft.Json.Serialization.JsonContract,Newtonsoft.Json.Serialization.JsonProperty,Newtonsoft.Json.Serialization.JsonContainerContract,Newtonsoft.Json.Serialization.JsonProperty)
extern void JsonSerializerInternalWriter_ResolveIsReference_m43AC53037DDDB97F4263DB53CA3C711F8E31CB64 (void);
// 0x0000043B System.Boolean Newtonsoft.Json.Serialization.JsonSerializerInternalWriter::ShouldWriteReference(System.Object,Newtonsoft.Json.Serialization.JsonProperty,Newtonsoft.Json.Serialization.JsonContract,Newtonsoft.Json.Serialization.JsonContainerContract,Newtonsoft.Json.Serialization.JsonProperty)
extern void JsonSerializerInternalWriter_ShouldWriteReference_m3D5F3A3348DE09B87C10215A21EC7867C50A888B (void);
// 0x0000043C System.Boolean Newtonsoft.Json.Serialization.JsonSerializerInternalWriter::ShouldWriteProperty(System.Object,Newtonsoft.Json.Serialization.JsonProperty)
extern void JsonSerializerInternalWriter_ShouldWriteProperty_mD57E393611046E8B54B3BBF989C331A57FD94F23 (void);
// 0x0000043D System.Boolean Newtonsoft.Json.Serialization.JsonSerializerInternalWriter::CheckForCircularReference(Newtonsoft.Json.JsonWriter,System.Object,Newtonsoft.Json.Serialization.JsonProperty,Newtonsoft.Json.Serialization.JsonContract,Newtonsoft.Json.Serialization.JsonContainerContract,Newtonsoft.Json.Serialization.JsonProperty)
extern void JsonSerializerInternalWriter_CheckForCircularReference_m4967876D9EFED00AB5B1F1EFE0AE8E575F5ECC8A (void);
// 0x0000043E System.Void Newtonsoft.Json.Serialization.JsonSerializerInternalWriter::WriteReference(Newtonsoft.Json.JsonWriter,System.Object)
extern void JsonSerializerInternalWriter_WriteReference_mD3C2F169D5F8DBEE5A6E63DBB480D0723D33BC5D (void);
// 0x0000043F System.String Newtonsoft.Json.Serialization.JsonSerializerInternalWriter::GetReference(Newtonsoft.Json.JsonWriter,System.Object)
extern void JsonSerializerInternalWriter_GetReference_mD7EC662C19A3426BC564ED9890E9D2E83B1F2B30 (void);
// 0x00000440 System.Boolean Newtonsoft.Json.Serialization.JsonSerializerInternalWriter::TryConvertToString(System.Object,System.Type,System.String&)
extern void JsonSerializerInternalWriter_TryConvertToString_mE7CF555EB2CAB24CA1D859F69BBC55EC84D21E7A (void);
// 0x00000441 System.Void Newtonsoft.Json.Serialization.JsonSerializerInternalWriter::SerializeString(Newtonsoft.Json.JsonWriter,System.Object,Newtonsoft.Json.Serialization.JsonStringContract)
extern void JsonSerializerInternalWriter_SerializeString_m1BC7D0E71EE24187445A74A216A5CC5CCF8719C9 (void);
// 0x00000442 System.Void Newtonsoft.Json.Serialization.JsonSerializerInternalWriter::OnSerializing(Newtonsoft.Json.JsonWriter,Newtonsoft.Json.Serialization.JsonContract,System.Object)
extern void JsonSerializerInternalWriter_OnSerializing_m350FCA9D440F26F8BCCA4C3EAC48B23C1DEC9994 (void);
// 0x00000443 System.Void Newtonsoft.Json.Serialization.JsonSerializerInternalWriter::OnSerialized(Newtonsoft.Json.JsonWriter,Newtonsoft.Json.Serialization.JsonContract,System.Object)
extern void JsonSerializerInternalWriter_OnSerialized_mA1CBF9C633BC2AC8C827E9AFB704D9141E299200 (void);
// 0x00000444 System.Void Newtonsoft.Json.Serialization.JsonSerializerInternalWriter::SerializeObject(Newtonsoft.Json.JsonWriter,System.Object,Newtonsoft.Json.Serialization.JsonObjectContract,Newtonsoft.Json.Serialization.JsonProperty,Newtonsoft.Json.Serialization.JsonContainerContract,Newtonsoft.Json.Serialization.JsonProperty)
extern void JsonSerializerInternalWriter_SerializeObject_m2EB8F81333F0A7A0BA067F716172C0D8F5B66FE9 (void);
// 0x00000445 System.Boolean Newtonsoft.Json.Serialization.JsonSerializerInternalWriter::CalculatePropertyValues(Newtonsoft.Json.JsonWriter,System.Object,Newtonsoft.Json.Serialization.JsonContainerContract,Newtonsoft.Json.Serialization.JsonProperty,Newtonsoft.Json.Serialization.JsonProperty,Newtonsoft.Json.Serialization.JsonContract&,System.Object&)
extern void JsonSerializerInternalWriter_CalculatePropertyValues_m4DEA924FF49865E23E6C2362B363B3CEA1320216 (void);
// 0x00000446 System.Void Newtonsoft.Json.Serialization.JsonSerializerInternalWriter::WriteObjectStart(Newtonsoft.Json.JsonWriter,System.Object,Newtonsoft.Json.Serialization.JsonContract,Newtonsoft.Json.Serialization.JsonProperty,Newtonsoft.Json.Serialization.JsonContainerContract,Newtonsoft.Json.Serialization.JsonProperty)
extern void JsonSerializerInternalWriter_WriteObjectStart_m2D1D6B3368417AFAFABBA4D872AA211990826425 (void);
// 0x00000447 System.Void Newtonsoft.Json.Serialization.JsonSerializerInternalWriter::WriteReferenceIdProperty(Newtonsoft.Json.JsonWriter,System.Type,System.Object)
extern void JsonSerializerInternalWriter_WriteReferenceIdProperty_mDEF0C77110DA0AB92C793BCEE4BC9C9A7A0F1E46 (void);
// 0x00000448 System.Void Newtonsoft.Json.Serialization.JsonSerializerInternalWriter::WriteTypeProperty(Newtonsoft.Json.JsonWriter,System.Type)
extern void JsonSerializerInternalWriter_WriteTypeProperty_mEF26BE0513326E64C2AF3D8F82CB172E84A20C83 (void);
// 0x00000449 System.Boolean Newtonsoft.Json.Serialization.JsonSerializerInternalWriter::HasFlag(Newtonsoft.Json.DefaultValueHandling,Newtonsoft.Json.DefaultValueHandling)
extern void JsonSerializerInternalWriter_HasFlag_m0F6B733C1D24AA5FEFA1471226AD7AF0E3C83CC8 (void);
// 0x0000044A System.Boolean Newtonsoft.Json.Serialization.JsonSerializerInternalWriter::HasFlag(Newtonsoft.Json.PreserveReferencesHandling,Newtonsoft.Json.PreserveReferencesHandling)
extern void JsonSerializerInternalWriter_HasFlag_m55EE1C0E1D06C50B5B303146BAABF5D5B684D142 (void);
// 0x0000044B System.Boolean Newtonsoft.Json.Serialization.JsonSerializerInternalWriter::HasFlag(Newtonsoft.Json.TypeNameHandling,Newtonsoft.Json.TypeNameHandling)
extern void JsonSerializerInternalWriter_HasFlag_mCEF1AB8FC23B3609315E52BB4731C62CBECE70D6 (void);
// 0x0000044C System.Void Newtonsoft.Json.Serialization.JsonSerializerInternalWriter::SerializeConvertable(Newtonsoft.Json.JsonWriter,Newtonsoft.Json.JsonConverter,System.Object,Newtonsoft.Json.Serialization.JsonContract,Newtonsoft.Json.Serialization.JsonContainerContract,Newtonsoft.Json.Serialization.JsonProperty)
extern void JsonSerializerInternalWriter_SerializeConvertable_m8C0D75BF2DFBAABBE49ECC078D66300BA7FFE9DB (void);
// 0x0000044D System.Void Newtonsoft.Json.Serialization.JsonSerializerInternalWriter::SerializeList(Newtonsoft.Json.JsonWriter,System.Collections.IEnumerable,Newtonsoft.Json.Serialization.JsonArrayContract,Newtonsoft.Json.Serialization.JsonProperty,Newtonsoft.Json.Serialization.JsonContainerContract,Newtonsoft.Json.Serialization.JsonProperty)
extern void JsonSerializerInternalWriter_SerializeList_m42784DB5EF17DEBC92BBF7DD50122DAB8B75D89A (void);
// 0x0000044E System.Void Newtonsoft.Json.Serialization.JsonSerializerInternalWriter::SerializeMultidimensionalArray(Newtonsoft.Json.JsonWriter,System.Array,Newtonsoft.Json.Serialization.JsonArrayContract,Newtonsoft.Json.Serialization.JsonProperty,Newtonsoft.Json.Serialization.JsonContainerContract,Newtonsoft.Json.Serialization.JsonProperty)
extern void JsonSerializerInternalWriter_SerializeMultidimensionalArray_mF82AC318E144799FB7F4FE021F9379CC48527856 (void);
// 0x0000044F System.Void Newtonsoft.Json.Serialization.JsonSerializerInternalWriter::SerializeMultidimensionalArray(Newtonsoft.Json.JsonWriter,System.Array,Newtonsoft.Json.Serialization.JsonArrayContract,Newtonsoft.Json.Serialization.JsonProperty,System.Int32,System.Int32[])
extern void JsonSerializerInternalWriter_SerializeMultidimensionalArray_m14B956BC4A5CFA954F0A401F08347347EEFC24DB (void);
// 0x00000450 System.Boolean Newtonsoft.Json.Serialization.JsonSerializerInternalWriter::WriteStartArray(Newtonsoft.Json.JsonWriter,System.Object,Newtonsoft.Json.Serialization.JsonArrayContract,Newtonsoft.Json.Serialization.JsonProperty,Newtonsoft.Json.Serialization.JsonContainerContract,Newtonsoft.Json.Serialization.JsonProperty)
extern void JsonSerializerInternalWriter_WriteStartArray_m66A8198BE4036F4AC986ED13DEFB748FDF906E49 (void);
// 0x00000451 System.Boolean Newtonsoft.Json.Serialization.JsonSerializerInternalWriter::ShouldWriteType(Newtonsoft.Json.TypeNameHandling,Newtonsoft.Json.Serialization.JsonContract,Newtonsoft.Json.Serialization.JsonProperty,Newtonsoft.Json.Serialization.JsonContainerContract,Newtonsoft.Json.Serialization.JsonProperty)
extern void JsonSerializerInternalWriter_ShouldWriteType_m2516242599D93E812B249A35057B4F07A0370DD9 (void);
// 0x00000452 System.Void Newtonsoft.Json.Serialization.JsonSerializerInternalWriter::SerializeDictionary(Newtonsoft.Json.JsonWriter,System.Collections.IDictionary,Newtonsoft.Json.Serialization.JsonDictionaryContract,Newtonsoft.Json.Serialization.JsonProperty,Newtonsoft.Json.Serialization.JsonContainerContract,Newtonsoft.Json.Serialization.JsonProperty)
extern void JsonSerializerInternalWriter_SerializeDictionary_m29272E678DE06CB92D9CB467988FE70AF003F558 (void);
// 0x00000453 System.String Newtonsoft.Json.Serialization.JsonSerializerInternalWriter::GetPropertyName(Newtonsoft.Json.JsonWriter,System.Object,Newtonsoft.Json.Serialization.JsonContract,System.Boolean&)
extern void JsonSerializerInternalWriter_GetPropertyName_mD212731827B88B0BAA4AD5C083D2BEC615B4BDE1 (void);
// 0x00000454 System.Void Newtonsoft.Json.Serialization.JsonSerializerInternalWriter::HandleError(Newtonsoft.Json.JsonWriter,System.Int32)
extern void JsonSerializerInternalWriter_HandleError_m1365318B9672FFEA57192F3E59BAA6426004380C (void);
// 0x00000455 System.Boolean Newtonsoft.Json.Serialization.JsonSerializerInternalWriter::ShouldSerialize(Newtonsoft.Json.JsonWriter,Newtonsoft.Json.Serialization.JsonProperty,System.Object)
extern void JsonSerializerInternalWriter_ShouldSerialize_m5FC940308CCEF6A4D0C24B5B13AF595B3C206FF3 (void);
// 0x00000456 System.Boolean Newtonsoft.Json.Serialization.JsonSerializerInternalWriter::IsSpecified(Newtonsoft.Json.JsonWriter,Newtonsoft.Json.Serialization.JsonProperty,System.Object)
extern void JsonSerializerInternalWriter_IsSpecified_mD5AFC0F88045079C05C3491DD01DAA2F9C19EB0A (void);
// 0x00000457 System.Void Newtonsoft.Json.Serialization.JsonSerializerProxy::add_Error(System.EventHandler`1<Newtonsoft.Json.Serialization.ErrorEventArgs>)
extern void JsonSerializerProxy_add_Error_m5332EF6713169F2612F0D6F53F759D6D79930E87 (void);
// 0x00000458 System.Void Newtonsoft.Json.Serialization.JsonSerializerProxy::remove_Error(System.EventHandler`1<Newtonsoft.Json.Serialization.ErrorEventArgs>)
extern void JsonSerializerProxy_remove_Error_m05F707DD2814E2AA3505E19E3C07167154F19D3B (void);
// 0x00000459 System.Void Newtonsoft.Json.Serialization.JsonSerializerProxy::set_ReferenceResolver(Newtonsoft.Json.Serialization.IReferenceResolver)
extern void JsonSerializerProxy_set_ReferenceResolver_m3183CBB2D4AB47F676D9A2B16C577EEF52407054 (void);
// 0x0000045A Newtonsoft.Json.Serialization.ITraceWriter Newtonsoft.Json.Serialization.JsonSerializerProxy::get_TraceWriter()
extern void JsonSerializerProxy_get_TraceWriter_m4B48C90B910658F7A75CB33CF2C265CFC180FF5F (void);
// 0x0000045B System.Void Newtonsoft.Json.Serialization.JsonSerializerProxy::set_TraceWriter(Newtonsoft.Json.Serialization.ITraceWriter)
extern void JsonSerializerProxy_set_TraceWriter_mB386E800DF7CE2C22EC2A3AA54348159C0ADCF66 (void);
// 0x0000045C System.Void Newtonsoft.Json.Serialization.JsonSerializerProxy::set_EqualityComparer(System.Collections.IEqualityComparer)
extern void JsonSerializerProxy_set_EqualityComparer_m3FC82F0A23DB0367B883DCA413B1225B25ACD785 (void);
// 0x0000045D Newtonsoft.Json.JsonConverterCollection Newtonsoft.Json.Serialization.JsonSerializerProxy::get_Converters()
extern void JsonSerializerProxy_get_Converters_m7F576773F805FE428EBC087D0CA67151DACD479A (void);
// 0x0000045E System.Void Newtonsoft.Json.Serialization.JsonSerializerProxy::set_DefaultValueHandling(Newtonsoft.Json.DefaultValueHandling)
extern void JsonSerializerProxy_set_DefaultValueHandling_m620D5C0D6D767919A76AC47EAD7B215A508C2F02 (void);
// 0x0000045F Newtonsoft.Json.Serialization.IContractResolver Newtonsoft.Json.Serialization.JsonSerializerProxy::get_ContractResolver()
extern void JsonSerializerProxy_get_ContractResolver_mBC99F18C5B6A2C28F36AEAED75770A84F626E4B4 (void);
// 0x00000460 System.Void Newtonsoft.Json.Serialization.JsonSerializerProxy::set_ContractResolver(Newtonsoft.Json.Serialization.IContractResolver)
extern void JsonSerializerProxy_set_ContractResolver_m85BF647755B9EF0A359660D4E9AB2FA7156AA0BD (void);
// 0x00000461 System.Void Newtonsoft.Json.Serialization.JsonSerializerProxy::set_MissingMemberHandling(Newtonsoft.Json.MissingMemberHandling)
extern void JsonSerializerProxy_set_MissingMemberHandling_mDC155116044AD028B55952DBBA1CC02655CF6A62 (void);
// 0x00000462 System.Void Newtonsoft.Json.Serialization.JsonSerializerProxy::set_NullValueHandling(Newtonsoft.Json.NullValueHandling)
extern void JsonSerializerProxy_set_NullValueHandling_m22CA9B7C8631C58D49BBD81D90316AF42C010558 (void);
// 0x00000463 System.Void Newtonsoft.Json.Serialization.JsonSerializerProxy::set_ObjectCreationHandling(Newtonsoft.Json.ObjectCreationHandling)
extern void JsonSerializerProxy_set_ObjectCreationHandling_m5AF86DDDAADE15712DCB515C93F2E6BCEC49F200 (void);
// 0x00000464 System.Void Newtonsoft.Json.Serialization.JsonSerializerProxy::set_ReferenceLoopHandling(Newtonsoft.Json.ReferenceLoopHandling)
extern void JsonSerializerProxy_set_ReferenceLoopHandling_m5FB2D1EDA234E5A7E6EA1F94F956D3407A98187B (void);
// 0x00000465 System.Void Newtonsoft.Json.Serialization.JsonSerializerProxy::set_PreserveReferencesHandling(Newtonsoft.Json.PreserveReferencesHandling)
extern void JsonSerializerProxy_set_PreserveReferencesHandling_m182AB3B9E497A6344B6721414631778B03D5194C (void);
// 0x00000466 System.Void Newtonsoft.Json.Serialization.JsonSerializerProxy::set_TypeNameHandling(Newtonsoft.Json.TypeNameHandling)
extern void JsonSerializerProxy_set_TypeNameHandling_mBB52993FB72C8EA27759485A6E978157FB857F16 (void);
// 0x00000467 Newtonsoft.Json.MetadataPropertyHandling Newtonsoft.Json.Serialization.JsonSerializerProxy::get_MetadataPropertyHandling()
extern void JsonSerializerProxy_get_MetadataPropertyHandling_m6DC75A09F1045C6CC6AD959A4A8231D21A0978FC (void);
// 0x00000468 System.Void Newtonsoft.Json.Serialization.JsonSerializerProxy::set_MetadataPropertyHandling(Newtonsoft.Json.MetadataPropertyHandling)
extern void JsonSerializerProxy_set_MetadataPropertyHandling_m32113B3765BAB3C3407E72B10DF7E29DB2A509FA (void);
// 0x00000469 System.Void Newtonsoft.Json.Serialization.JsonSerializerProxy::set_TypeNameAssemblyFormat(System.Runtime.Serialization.Formatters.FormatterAssemblyStyle)
extern void JsonSerializerProxy_set_TypeNameAssemblyFormat_mDD85866973259A1326A37896E25B8CF0535CBA85 (void);
// 0x0000046A System.Void Newtonsoft.Json.Serialization.JsonSerializerProxy::set_ConstructorHandling(Newtonsoft.Json.ConstructorHandling)
extern void JsonSerializerProxy_set_ConstructorHandling_m150E9008C4BBED96D5D9C758CECD8DE0AD48020F (void);
// 0x0000046B System.Void Newtonsoft.Json.Serialization.JsonSerializerProxy::set_Binder(System.Runtime.Serialization.SerializationBinder)
extern void JsonSerializerProxy_set_Binder_m4A3EA675D117467F4DD5C00CD223E09E54604AC5 (void);
// 0x0000046C System.Runtime.Serialization.StreamingContext Newtonsoft.Json.Serialization.JsonSerializerProxy::get_Context()
extern void JsonSerializerProxy_get_Context_mD33F8DBF63D2AC665AB7FB3EB83101E34D6BC346 (void);
// 0x0000046D System.Void Newtonsoft.Json.Serialization.JsonSerializerProxy::set_Context(System.Runtime.Serialization.StreamingContext)
extern void JsonSerializerProxy_set_Context_m8ADC56B4CD9666B64420B45E4E7B0EBDC8D3831A (void);
// 0x0000046E Newtonsoft.Json.Formatting Newtonsoft.Json.Serialization.JsonSerializerProxy::get_Formatting()
extern void JsonSerializerProxy_get_Formatting_m3539A08D0819B39C52BB1C268B037A9FE5F90B5A (void);
// 0x0000046F System.Boolean Newtonsoft.Json.Serialization.JsonSerializerProxy::get_CheckAdditionalContent()
extern void JsonSerializerProxy_get_CheckAdditionalContent_mF713CC40ACC4654A11BE2779372517EFCD87FA3D (void);
// 0x00000470 System.Void Newtonsoft.Json.Serialization.JsonSerializerProxy::set_CheckAdditionalContent(System.Boolean)
extern void JsonSerializerProxy_set_CheckAdditionalContent_m2C74C094FFE58CFDA4EEF0419859CB80969AD224 (void);
// 0x00000471 Newtonsoft.Json.Serialization.JsonSerializerInternalBase Newtonsoft.Json.Serialization.JsonSerializerProxy::GetInternalSerializer()
extern void JsonSerializerProxy_GetInternalSerializer_mA9C47B00437D61801910CFA99C97BC0D3CCB9AAA (void);
// 0x00000472 System.Void Newtonsoft.Json.Serialization.JsonSerializerProxy::.ctor(Newtonsoft.Json.Serialization.JsonSerializerInternalReader)
extern void JsonSerializerProxy__ctor_m42CE32C8F24EB13CBC947AD94381CAC11CF0EBDE (void);
// 0x00000473 System.Void Newtonsoft.Json.Serialization.JsonSerializerProxy::.ctor(Newtonsoft.Json.Serialization.JsonSerializerInternalWriter)
extern void JsonSerializerProxy__ctor_mEBEAF9B2186D7C7DAFAA0F4B2EB90769C3D1B646 (void);
// 0x00000474 System.Object Newtonsoft.Json.Serialization.JsonSerializerProxy::DeserializeInternal(Newtonsoft.Json.JsonReader,System.Type)
extern void JsonSerializerProxy_DeserializeInternal_m232ECAC23E34C1C3B2B0E562BC1AD3827EC4C16E (void);
// 0x00000475 System.Void Newtonsoft.Json.Serialization.JsonSerializerProxy::SerializeInternal(Newtonsoft.Json.JsonWriter,System.Object,System.Type)
extern void JsonSerializerProxy_SerializeInternal_m0189768E4B5AFC15EFD47159A4741F477684DD46 (void);
// 0x00000476 System.Void Newtonsoft.Json.Serialization.JsonStringContract::.ctor(System.Type)
extern void JsonStringContract__ctor_m895EB8BF3664D947CAAABBDEDF08B79B0C81E668 (void);
// 0x00000477 T Newtonsoft.Json.Serialization.JsonTypeReflector::GetCachedAttribute(System.Object)
// 0x00000478 System.Runtime.Serialization.DataContractAttribute Newtonsoft.Json.Serialization.JsonTypeReflector::GetDataContractAttribute(System.Type)
extern void JsonTypeReflector_GetDataContractAttribute_m5845752C4A046C85DA96A5D9B4FEC89D35DAC246 (void);
// 0x00000479 System.Runtime.Serialization.DataMemberAttribute Newtonsoft.Json.Serialization.JsonTypeReflector::GetDataMemberAttribute(System.Reflection.MemberInfo)
extern void JsonTypeReflector_GetDataMemberAttribute_mB14D9536F26AB591B1B71906ECBBC7EF9BA3150D (void);
// 0x0000047A Newtonsoft.Json.MemberSerialization Newtonsoft.Json.Serialization.JsonTypeReflector::GetObjectMemberSerialization(System.Type,System.Boolean)
extern void JsonTypeReflector_GetObjectMemberSerialization_mA9F7123352A69D75136EF08D4076B4F677319B77 (void);
// 0x0000047B Newtonsoft.Json.JsonConverter Newtonsoft.Json.Serialization.JsonTypeReflector::GetJsonConverter(System.Object)
extern void JsonTypeReflector_GetJsonConverter_m698F0AA36908AA5EB808E9AAD99388170B4B8322 (void);
// 0x0000047C Newtonsoft.Json.JsonConverter Newtonsoft.Json.Serialization.JsonTypeReflector::CreateJsonConverterInstance(System.Type,System.Object[])
extern void JsonTypeReflector_CreateJsonConverterInstance_m0A1C1696369A50698204A0C0B74C5021EECA6D0E (void);
// 0x0000047D Newtonsoft.Json.Serialization.NamingStrategy Newtonsoft.Json.Serialization.JsonTypeReflector::CreateNamingStrategyInstance(System.Type,System.Object[])
extern void JsonTypeReflector_CreateNamingStrategyInstance_m983469EB051584F0BE993F3B435CE83DCB954749 (void);
// 0x0000047E Newtonsoft.Json.Serialization.NamingStrategy Newtonsoft.Json.Serialization.JsonTypeReflector::GetContainerNamingStrategy(Newtonsoft.Json.JsonContainerAttribute)
extern void JsonTypeReflector_GetContainerNamingStrategy_mC935BD957093DF747AC70883829ED9B12007BC92 (void);
// 0x0000047F System.Func`2<System.Object[],System.Object> Newtonsoft.Json.Serialization.JsonTypeReflector::GetCreator(System.Type)
extern void JsonTypeReflector_GetCreator_mD27CA71228B825164E78F2A6FAE899257BE4122C (void);
// 0x00000480 System.ComponentModel.TypeConverter Newtonsoft.Json.Serialization.JsonTypeReflector::GetTypeConverter(System.Type)
extern void JsonTypeReflector_GetTypeConverter_m106FD3E440CAA5076DC226EA10B16305144A3577 (void);
// 0x00000481 System.Type Newtonsoft.Json.Serialization.JsonTypeReflector::GetAssociatedMetadataType(System.Type)
extern void JsonTypeReflector_GetAssociatedMetadataType_m9372B1DA0BDB89701CC390F29DA0FEECA4ACE778 (void);
// 0x00000482 System.Type Newtonsoft.Json.Serialization.JsonTypeReflector::GetAssociateMetadataTypeFromAttribute(System.Type)
extern void JsonTypeReflector_GetAssociateMetadataTypeFromAttribute_mBFBBCF6E816B7C9CE33F1321EF47150381629594 (void);
// 0x00000483 T Newtonsoft.Json.Serialization.JsonTypeReflector::GetAttribute(System.Type)
// 0x00000484 T Newtonsoft.Json.Serialization.JsonTypeReflector::GetAttribute(System.Reflection.MemberInfo)
// 0x00000485 T Newtonsoft.Json.Serialization.JsonTypeReflector::GetAttribute(System.Object)
// 0x00000486 System.Boolean Newtonsoft.Json.Serialization.JsonTypeReflector::get_FullyTrusted()
extern void JsonTypeReflector_get_FullyTrusted_mBED536FA8A1687A54B609F1FCE1D0A37336CCC25 (void);
// 0x00000487 Newtonsoft.Json.Utilities.ReflectionDelegateFactory Newtonsoft.Json.Serialization.JsonTypeReflector::get_ReflectionDelegateFactory()
extern void JsonTypeReflector_get_ReflectionDelegateFactory_mD5A0D3AA68CA62CCD31B219ED5B1C03C63AAAEC6 (void);
// 0x00000488 System.Void Newtonsoft.Json.Serialization.JsonTypeReflector::.cctor()
extern void JsonTypeReflector__cctor_mF726C9808106B6CE44C99C1DEFB538DDE3697C09 (void);
// 0x00000489 System.Void Newtonsoft.Json.Serialization.JsonTypeReflector/<>c__DisplayClass20_0::.ctor()
extern void U3CU3Ec__DisplayClass20_0__ctor_m6FF8C47EB6FB31F8952DA33F7B70A293F502A0E2 (void);
// 0x0000048A System.Object Newtonsoft.Json.Serialization.JsonTypeReflector/<>c__DisplayClass20_0::<GetCreator>b__0(System.Object[])
extern void U3CU3Ec__DisplayClass20_0_U3CGetCreatorU3Eb__0_m987F7CF1E29CCD749193506951C9C0348D193BCB (void);
// 0x0000048B System.Void Newtonsoft.Json.Serialization.JsonTypeReflector/<>c::.cctor()
extern void U3CU3Ec__cctor_m57D7B5CC6C5C25588D21AA16B15B972D7EA17B16 (void);
// 0x0000048C System.Void Newtonsoft.Json.Serialization.JsonTypeReflector/<>c::.ctor()
extern void U3CU3Ec__ctor_m10DDD7E3D143A479BA4ABEA1D4361ACC61E136E5 (void);
// 0x0000048D System.Type Newtonsoft.Json.Serialization.JsonTypeReflector/<>c::<GetCreator>b__20_1(System.Object)
extern void U3CU3Ec_U3CGetCreatorU3Eb__20_1_mC6037338F8D1D9AFA8755A32D4D207646963D8E1 (void);
// 0x0000048E System.Boolean Newtonsoft.Json.Serialization.NamingStrategy::get_ProcessDictionaryKeys()
extern void NamingStrategy_get_ProcessDictionaryKeys_mC1D4A8A7BDBC0305DBAE29FFEA3FFAB83177E249 (void);
// 0x0000048F System.Boolean Newtonsoft.Json.Serialization.NamingStrategy::get_OverrideSpecifiedNames()
extern void NamingStrategy_get_OverrideSpecifiedNames_m45FEC93C0AC20A53AEC9AD731A4D9B276AF26AF4 (void);
// 0x00000490 System.String Newtonsoft.Json.Serialization.NamingStrategy::GetPropertyName(System.String,System.Boolean)
extern void NamingStrategy_GetPropertyName_m6949AB856F0450C007F63B63484FE2372142CF6E (void);
// 0x00000491 System.String Newtonsoft.Json.Serialization.NamingStrategy::GetDictionaryKey(System.String)
extern void NamingStrategy_GetDictionaryKey_m0F48B97AF21AA7A066CDB8905D1605455B4E907A (void);
// 0x00000492 System.String Newtonsoft.Json.Serialization.NamingStrategy::ResolvePropertyName(System.String)
// 0x00000493 System.Void Newtonsoft.Json.Serialization.ObjectConstructor`1::.ctor(System.Object,System.IntPtr)
// 0x00000494 System.Object Newtonsoft.Json.Serialization.ObjectConstructor`1::Invoke(System.Object[])
// 0x00000495 System.IAsyncResult Newtonsoft.Json.Serialization.ObjectConstructor`1::BeginInvoke(System.Object[],System.AsyncCallback,System.Object)
// 0x00000496 System.Object Newtonsoft.Json.Serialization.ObjectConstructor`1::EndInvoke(System.IAsyncResult)
// 0x00000497 System.Void Newtonsoft.Json.Serialization.ReflectionAttributeProvider::.ctor(System.Object)
extern void ReflectionAttributeProvider__ctor_m3A03DE577A627DFBF44D58AA17E191E2C0FD0E60 (void);
// 0x00000498 System.Void Newtonsoft.Json.Serialization.ReflectionValueProvider::.ctor(System.Reflection.MemberInfo)
extern void ReflectionValueProvider__ctor_m4CCF1B6E29FAC08128AD2D0E980F29107FD29C19 (void);
// 0x00000499 System.Void Newtonsoft.Json.Serialization.ReflectionValueProvider::SetValue(System.Object,System.Object)
extern void ReflectionValueProvider_SetValue_mF3A83C542EB0F0D53ADE0E384A043DA559182421 (void);
// 0x0000049A System.Object Newtonsoft.Json.Serialization.ReflectionValueProvider::GetValue(System.Object)
extern void ReflectionValueProvider_GetValue_m26EB30AFB2BDE690A4955BBFDC482CB2E67B4DE7 (void);
// 0x0000049B System.Void Newtonsoft.Json.Serialization.TraceJsonReader::.ctor(Newtonsoft.Json.JsonReader)
extern void TraceJsonReader__ctor_mDF664DDFC7FF1739F887F50E0CE389D056EC382A (void);
// 0x0000049C System.String Newtonsoft.Json.Serialization.TraceJsonReader::GetDeserializedJsonMessage()
extern void TraceJsonReader_GetDeserializedJsonMessage_m0AE711BAFA1A2B758D450B50F5992D2DE714EED7 (void);
// 0x0000049D System.Boolean Newtonsoft.Json.Serialization.TraceJsonReader::Read()
extern void TraceJsonReader_Read_mB5B1144C2D86F9AA7E2BE7F332ACA7342D916838 (void);
// 0x0000049E System.Nullable`1<System.Int32> Newtonsoft.Json.Serialization.TraceJsonReader::ReadAsInt32()
extern void TraceJsonReader_ReadAsInt32_mDD9B61D65D8F0BFDC4D785CBD169A5C0B0E20BD4 (void);
// 0x0000049F System.String Newtonsoft.Json.Serialization.TraceJsonReader::ReadAsString()
extern void TraceJsonReader_ReadAsString_mDB0A396A74EE34E81D9510752177369AD220B95C (void);
// 0x000004A0 System.Byte[] Newtonsoft.Json.Serialization.TraceJsonReader::ReadAsBytes()
extern void TraceJsonReader_ReadAsBytes_mF34242EE6F6C15B4BCACEA72F6EC654279B8E954 (void);
// 0x000004A1 System.Nullable`1<System.Decimal> Newtonsoft.Json.Serialization.TraceJsonReader::ReadAsDecimal()
extern void TraceJsonReader_ReadAsDecimal_m2866C8E8D0D8A1786B23940FFF3550809322D130 (void);
// 0x000004A2 System.Nullable`1<System.Double> Newtonsoft.Json.Serialization.TraceJsonReader::ReadAsDouble()
extern void TraceJsonReader_ReadAsDouble_mD59C86D67B8FC8AD056DFC9A7A7E55B9A1DCFB74 (void);
// 0x000004A3 System.Nullable`1<System.Boolean> Newtonsoft.Json.Serialization.TraceJsonReader::ReadAsBoolean()
extern void TraceJsonReader_ReadAsBoolean_m189B58C223EC61B7A0114228F697D07C2B376AD6 (void);
// 0x000004A4 System.Nullable`1<System.DateTime> Newtonsoft.Json.Serialization.TraceJsonReader::ReadAsDateTime()
extern void TraceJsonReader_ReadAsDateTime_m31E9688C8F20CB5F1A3517A80EF27920679DFD1A (void);
// 0x000004A5 System.Nullable`1<System.DateTimeOffset> Newtonsoft.Json.Serialization.TraceJsonReader::ReadAsDateTimeOffset()
extern void TraceJsonReader_ReadAsDateTimeOffset_mDAEE1C904D638231A033EAC8DE603B42779AA3FD (void);
// 0x000004A6 System.Int32 Newtonsoft.Json.Serialization.TraceJsonReader::get_Depth()
extern void TraceJsonReader_get_Depth_mE3AFA404FCA65349C5C5DCBC71F0F3770177DA3C (void);
// 0x000004A7 System.String Newtonsoft.Json.Serialization.TraceJsonReader::get_Path()
extern void TraceJsonReader_get_Path_m1F5468102FAA80E5B8DB599FD71A5B30F0E0CA60 (void);
// 0x000004A8 Newtonsoft.Json.JsonToken Newtonsoft.Json.Serialization.TraceJsonReader::get_TokenType()
extern void TraceJsonReader_get_TokenType_m3934A485BDEC03D4E11CFD340077E6B0DADBF6BD (void);
// 0x000004A9 System.Object Newtonsoft.Json.Serialization.TraceJsonReader::get_Value()
extern void TraceJsonReader_get_Value_mB8C60D4BBD15E4ECDBDE49556E9E07F55047F93E (void);
// 0x000004AA System.Type Newtonsoft.Json.Serialization.TraceJsonReader::get_ValueType()
extern void TraceJsonReader_get_ValueType_mD2045D7D802F3759CB8DE2FB293AB76D3E16EF12 (void);
// 0x000004AB System.Void Newtonsoft.Json.Serialization.TraceJsonReader::Close()
extern void TraceJsonReader_Close_mDB33FA7E4687365BDD88D0AFDEAD0253C1A3D1AF (void);
// 0x000004AC System.Boolean Newtonsoft.Json.Serialization.TraceJsonReader::Newtonsoft.Json.IJsonLineInfo.HasLineInfo()
extern void TraceJsonReader_Newtonsoft_Json_IJsonLineInfo_HasLineInfo_m90913CA01160A53528846997BDA02BBFEF36123F (void);
// 0x000004AD System.Int32 Newtonsoft.Json.Serialization.TraceJsonReader::Newtonsoft.Json.IJsonLineInfo.get_LineNumber()
extern void TraceJsonReader_Newtonsoft_Json_IJsonLineInfo_get_LineNumber_m3BF75683A19B5FC29E3D5290B5B3C1FA134C3148 (void);
// 0x000004AE System.Int32 Newtonsoft.Json.Serialization.TraceJsonReader::Newtonsoft.Json.IJsonLineInfo.get_LinePosition()
extern void TraceJsonReader_Newtonsoft_Json_IJsonLineInfo_get_LinePosition_m7B4B018D9ECFC2AD388057DD8F0215AB21183D06 (void);
// 0x000004AF System.Void Newtonsoft.Json.Serialization.TraceJsonWriter::.ctor(Newtonsoft.Json.JsonWriter)
extern void TraceJsonWriter__ctor_m29B26F2DC7065ED209EB7F1BD7454D085A7C1C41 (void);
// 0x000004B0 System.String Newtonsoft.Json.Serialization.TraceJsonWriter::GetSerializedJsonMessage()
extern void TraceJsonWriter_GetSerializedJsonMessage_m5B843782EA9D7D098F54283024609893F47AC118 (void);
// 0x000004B1 System.Void Newtonsoft.Json.Serialization.TraceJsonWriter::WriteValue(System.Decimal)
extern void TraceJsonWriter_WriteValue_m9BF104BFCC9361F1F1177B7DDDC55A5D0994AC9A (void);
// 0x000004B2 System.Void Newtonsoft.Json.Serialization.TraceJsonWriter::WriteValue(System.Boolean)
extern void TraceJsonWriter_WriteValue_m3E4B078F2B00FD5250754A6A66DC5973AD6CA60C (void);
// 0x000004B3 System.Void Newtonsoft.Json.Serialization.TraceJsonWriter::WriteValue(System.Byte)
extern void TraceJsonWriter_WriteValue_mAE9F6E644A236BD55983E01C96D04F2E8A92F3B8 (void);
// 0x000004B4 System.Void Newtonsoft.Json.Serialization.TraceJsonWriter::WriteValue(System.Nullable`1<System.Byte>)
extern void TraceJsonWriter_WriteValue_m6FE35B7C9F77BDD538F59AE2C597F27E1E155426 (void);
// 0x000004B5 System.Void Newtonsoft.Json.Serialization.TraceJsonWriter::WriteValue(System.Char)
extern void TraceJsonWriter_WriteValue_m9707DAACD55CB7D657F0971CB43B9228DC55609E (void);
// 0x000004B6 System.Void Newtonsoft.Json.Serialization.TraceJsonWriter::WriteValue(System.Byte[])
extern void TraceJsonWriter_WriteValue_m91400888E8FFDE94292C9F7B1FACEC38A92627B0 (void);
// 0x000004B7 System.Void Newtonsoft.Json.Serialization.TraceJsonWriter::WriteValue(System.DateTime)
extern void TraceJsonWriter_WriteValue_m0D48CE333EDDA2EEF769CB10E783215A805ED1F2 (void);
// 0x000004B8 System.Void Newtonsoft.Json.Serialization.TraceJsonWriter::WriteValue(System.DateTimeOffset)
extern void TraceJsonWriter_WriteValue_m93AC35D2DC22F752D361B42AB92E09BE043FB55B (void);
// 0x000004B9 System.Void Newtonsoft.Json.Serialization.TraceJsonWriter::WriteValue(System.Double)
extern void TraceJsonWriter_WriteValue_m06D2926D295E1814C93DCB9468ADF5064897438C (void);
// 0x000004BA System.Void Newtonsoft.Json.Serialization.TraceJsonWriter::WriteUndefined()
extern void TraceJsonWriter_WriteUndefined_m1D80B8C61BA78FE70E157994F1F461A43C6DA9D5 (void);
// 0x000004BB System.Void Newtonsoft.Json.Serialization.TraceJsonWriter::WriteNull()
extern void TraceJsonWriter_WriteNull_m1C114CBB5198BAD1346622F30F2E06536E6A67E7 (void);
// 0x000004BC System.Void Newtonsoft.Json.Serialization.TraceJsonWriter::WriteValue(System.Single)
extern void TraceJsonWriter_WriteValue_m9CFECDBD50358F59CC88998F625E88D08137E25B (void);
// 0x000004BD System.Void Newtonsoft.Json.Serialization.TraceJsonWriter::WriteValue(System.Guid)
extern void TraceJsonWriter_WriteValue_m8F557D0F46C8D9DFE5D77540E300E6653DBC2E23 (void);
// 0x000004BE System.Void Newtonsoft.Json.Serialization.TraceJsonWriter::WriteValue(System.Int32)
extern void TraceJsonWriter_WriteValue_m86B387D4711067F1A4BA39F1A2F0C2CF1FC4CD8E (void);
// 0x000004BF System.Void Newtonsoft.Json.Serialization.TraceJsonWriter::WriteValue(System.Int64)
extern void TraceJsonWriter_WriteValue_mC1359523457AF58B6E45E73EB4FD7DC6ADB9514F (void);
// 0x000004C0 System.Void Newtonsoft.Json.Serialization.TraceJsonWriter::WriteValue(System.SByte)
extern void TraceJsonWriter_WriteValue_mCF70906D73F2F874C153CCF43176C7F270DCD534 (void);
// 0x000004C1 System.Void Newtonsoft.Json.Serialization.TraceJsonWriter::WriteValue(System.Int16)
extern void TraceJsonWriter_WriteValue_mAB836F999AC85984937844DA3DB99EAFB593D291 (void);
// 0x000004C2 System.Void Newtonsoft.Json.Serialization.TraceJsonWriter::WriteValue(System.String)
extern void TraceJsonWriter_WriteValue_mD0DB38AA9308B0E181BC2AEE2EFF513A75E2F4FC (void);
// 0x000004C3 System.Void Newtonsoft.Json.Serialization.TraceJsonWriter::WriteValue(System.TimeSpan)
extern void TraceJsonWriter_WriteValue_m348A56226E7608135E82F3FCAC6F91D578C11C86 (void);
// 0x000004C4 System.Void Newtonsoft.Json.Serialization.TraceJsonWriter::WriteValue(System.UInt32)
extern void TraceJsonWriter_WriteValue_m3C731AE9FD7B5898C197719457A3F87C549BF43D (void);
// 0x000004C5 System.Void Newtonsoft.Json.Serialization.TraceJsonWriter::WriteValue(System.UInt64)
extern void TraceJsonWriter_WriteValue_mCE1C9ED077BDA0E5DEEFD3E4A106C5A57EA90850 (void);
// 0x000004C6 System.Void Newtonsoft.Json.Serialization.TraceJsonWriter::WriteValue(System.Uri)
extern void TraceJsonWriter_WriteValue_m3B0429ABD3FCB2E96E4BC4C3205D947C6F15B861 (void);
// 0x000004C7 System.Void Newtonsoft.Json.Serialization.TraceJsonWriter::WriteValue(System.UInt16)
extern void TraceJsonWriter_WriteValue_m516ABC62B17BBF4E077B78D574738B3955C51B72 (void);
// 0x000004C8 System.Void Newtonsoft.Json.Serialization.TraceJsonWriter::WriteComment(System.String)
extern void TraceJsonWriter_WriteComment_m7B6A62C629652A13C48DB7BF8F9E99FB0E2F18FE (void);
// 0x000004C9 System.Void Newtonsoft.Json.Serialization.TraceJsonWriter::WriteStartArray()
extern void TraceJsonWriter_WriteStartArray_m62E5F20C404EA5F6B7C2F8BE8BCEB8D5CF3607CF (void);
// 0x000004CA System.Void Newtonsoft.Json.Serialization.TraceJsonWriter::WriteEndArray()
extern void TraceJsonWriter_WriteEndArray_m4292FCDB4F4E922D9199500BED9C07888680690C (void);
// 0x000004CB System.Void Newtonsoft.Json.Serialization.TraceJsonWriter::WriteStartConstructor(System.String)
extern void TraceJsonWriter_WriteStartConstructor_m8C1F7EA7AF1EE915319DD74B1FD74165F925FE7F (void);
// 0x000004CC System.Void Newtonsoft.Json.Serialization.TraceJsonWriter::WriteEndConstructor()
extern void TraceJsonWriter_WriteEndConstructor_m82D598D7C412DF807185B9FC4C712CD69C005D34 (void);
// 0x000004CD System.Void Newtonsoft.Json.Serialization.TraceJsonWriter::WritePropertyName(System.String)
extern void TraceJsonWriter_WritePropertyName_m8E987A764C0769862EEE598BFF9EB5B03B571B64 (void);
// 0x000004CE System.Void Newtonsoft.Json.Serialization.TraceJsonWriter::WritePropertyName(System.String,System.Boolean)
extern void TraceJsonWriter_WritePropertyName_mBA2E40CB1DAA827635B2FCDA37A66919E1507835 (void);
// 0x000004CF System.Void Newtonsoft.Json.Serialization.TraceJsonWriter::WriteStartObject()
extern void TraceJsonWriter_WriteStartObject_m2EE8F71A1FD5BDDEEFCAE0E5C3689C5DC8B24A30 (void);
// 0x000004D0 System.Void Newtonsoft.Json.Serialization.TraceJsonWriter::WriteEndObject()
extern void TraceJsonWriter_WriteEndObject_m7417900A02D479036FB4FE283727929A303AE56E (void);
// 0x000004D1 System.Void Newtonsoft.Json.Serialization.TraceJsonWriter::WriteRawValue(System.String)
extern void TraceJsonWriter_WriteRawValue_m02839B921BF871F752B464D19C65D92389B1C8B1 (void);
// 0x000004D2 System.Void Newtonsoft.Json.Serialization.TraceJsonWriter::WriteRaw(System.String)
extern void TraceJsonWriter_WriteRaw_mF5798374D6F0A1AFEB8E3AEBEBA41B7171393138 (void);
// 0x000004D3 System.Void Newtonsoft.Json.Serialization.TraceJsonWriter::Close()
extern void TraceJsonWriter_Close_mD31D297A991DB57260137EF39AD6C641E1F8C852 (void);
// 0x000004D4 System.Void Newtonsoft.Json.Converters.BinaryConverter::WriteJson(Newtonsoft.Json.JsonWriter,System.Object,Newtonsoft.Json.JsonSerializer)
extern void BinaryConverter_WriteJson_m38F6F9B3820776D5E3F0D1C3534E4C65E257746B (void);
// 0x000004D5 System.Byte[] Newtonsoft.Json.Converters.BinaryConverter::GetByteArray(System.Object)
extern void BinaryConverter_GetByteArray_m0DEF709EDA659437BAE64675B81F7949A3BD9D80 (void);
// 0x000004D6 System.Void Newtonsoft.Json.Converters.BinaryConverter::EnsureReflectionObject(System.Type)
extern void BinaryConverter_EnsureReflectionObject_mD05B38334EFC9C8EC364BE76705BD3C16031452F (void);
// 0x000004D7 System.Object Newtonsoft.Json.Converters.BinaryConverter::ReadJson(Newtonsoft.Json.JsonReader,System.Type,System.Object,Newtonsoft.Json.JsonSerializer)
extern void BinaryConverter_ReadJson_m3458DDB255892438725E2070C0EFCA940B384B92 (void);
// 0x000004D8 System.Byte[] Newtonsoft.Json.Converters.BinaryConverter::ReadByteArray(Newtonsoft.Json.JsonReader)
extern void BinaryConverter_ReadByteArray_mD8A936BF1CE0542DEB8A2A9D434C8D2CE7609027 (void);
// 0x000004D9 System.Boolean Newtonsoft.Json.Converters.BinaryConverter::CanConvert(System.Type)
extern void BinaryConverter_CanConvert_mA6F5A4F148236D9944C7ED8C30B12B1C0E69EB23 (void);
// 0x000004DA System.Void Newtonsoft.Json.Converters.BinaryConverter::.ctor()
extern void BinaryConverter__ctor_mFD2281076324F20D92F44E5A19650FB5518F9F19 (void);
// 0x000004DB Newtonsoft.Json.Utilities.ReflectionObject Newtonsoft.Json.Converters.KeyValuePairConverter::InitializeReflectionObject(System.Type)
extern void KeyValuePairConverter_InitializeReflectionObject_m06AF5C794B04A48B532F925A5D6576D37908690A (void);
// 0x000004DC System.Void Newtonsoft.Json.Converters.KeyValuePairConverter::WriteJson(Newtonsoft.Json.JsonWriter,System.Object,Newtonsoft.Json.JsonSerializer)
extern void KeyValuePairConverter_WriteJson_m56A13234A85C715D67F54DEAD005F187CD494204 (void);
// 0x000004DD System.Object Newtonsoft.Json.Converters.KeyValuePairConverter::ReadJson(Newtonsoft.Json.JsonReader,System.Type,System.Object,Newtonsoft.Json.JsonSerializer)
extern void KeyValuePairConverter_ReadJson_mF8F822C2D404938169B5C3B5701C5AC1D85CEC69 (void);
// 0x000004DE System.Boolean Newtonsoft.Json.Converters.KeyValuePairConverter::CanConvert(System.Type)
extern void KeyValuePairConverter_CanConvert_m211BB20B62F84C81DB1704B890B31525BC899BB7 (void);
// 0x000004DF System.Void Newtonsoft.Json.Converters.KeyValuePairConverter::.ctor()
extern void KeyValuePairConverter__ctor_m4B1F9194CB861A99BF850097D012624E5807505B (void);
// 0x000004E0 System.Void Newtonsoft.Json.Converters.KeyValuePairConverter::.cctor()
extern void KeyValuePairConverter__cctor_mF4627C8F3CADAA2911CE4940B677D0B5D79259B6 (void);
// 0x000004E1 System.Void Newtonsoft.Json.Converters.RegexConverter::WriteJson(Newtonsoft.Json.JsonWriter,System.Object,Newtonsoft.Json.JsonSerializer)
extern void RegexConverter_WriteJson_m2DFD8C928EE8DF8C5F5B2142C1BD4FD725A3A278 (void);
// 0x000004E2 System.Void Newtonsoft.Json.Converters.RegexConverter::WriteJson(Newtonsoft.Json.JsonWriter,System.Text.RegularExpressions.Regex,Newtonsoft.Json.JsonSerializer)
extern void RegexConverter_WriteJson_m3232C1B85602852FB040265237936D883586D344 (void);
// 0x000004E3 System.Object Newtonsoft.Json.Converters.RegexConverter::ReadJson(Newtonsoft.Json.JsonReader,System.Type,System.Object,Newtonsoft.Json.JsonSerializer)
extern void RegexConverter_ReadJson_m670966D979ED51B085DCEBDFED7B53697D3B075B (void);
// 0x000004E4 System.Object Newtonsoft.Json.Converters.RegexConverter::ReadRegexString(Newtonsoft.Json.JsonReader)
extern void RegexConverter_ReadRegexString_m8416EBC385B8AD4B3DB08E1437E37DCAA9FA9734 (void);
// 0x000004E5 System.Text.RegularExpressions.Regex Newtonsoft.Json.Converters.RegexConverter::ReadRegexObject(Newtonsoft.Json.JsonReader,Newtonsoft.Json.JsonSerializer)
extern void RegexConverter_ReadRegexObject_m305F9A55E4024BD86481052D19B398005B06C745 (void);
// 0x000004E6 System.Boolean Newtonsoft.Json.Converters.RegexConverter::CanConvert(System.Type)
extern void RegexConverter_CanConvert_mB8EE7F3824E272C0D5119E0004256A78500A00E9 (void);
// 0x000004E7 System.Void Newtonsoft.Json.Converters.RegexConverter::.ctor()
extern void RegexConverter__ctor_mA8551DA4763F879B008CAA285672515BACAE949E (void);
static Il2CppMethodPointer s_methodPointers[1255] = 
{
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	JsonContainerAttribute_get_ItemConverterType_mE0959B267593FB7E6C69CC99954A1AB62C82551D,
	JsonContainerAttribute_get_ItemConverterParameters_mC22A7EB75C02CB7460E4D8E1DF4E3685C04413C5,
	JsonContainerAttribute_get_NamingStrategyType_mB10ABE6F04C12E460ED8A6D77A977EF21C1AFBDA,
	JsonContainerAttribute_get_NamingStrategyParameters_m8239FB6299983482C92247CACDEA26EDDB335C53,
	JsonContainerAttribute_get_NamingStrategyInstance_mCB5C826F2FDD2091632B0FD8A25DDECF528C513C,
	JsonContainerAttribute_set_NamingStrategyInstance_mDF8D8278251CF937FC050DF7DC9B91C8F27BD5E4,
	JsonConvert_get_DefaultSettings_m214B88ECC58290FE283050B8A4C2E08F1755C83A,
	JsonConvert_ToString_mD0E99B0E39DDDC716CA34FCEDBD645ACBFB90E4C,
	JsonConvert_ToString_mAB220E124F4B6472230B4E092F46087E03246ED3,
	JsonConvert_ToString_m99422A0B86718ECAD838E5A955774E0B0FDD6B33,
	JsonConvert_EnsureFloatFormat_m7DE595A6055A8F778FF4B1C4CB13C563F1672EC7,
	JsonConvert_ToString_m6E4BA77374791993DC635CF31DF61855CAB8B1D5,
	JsonConvert_EnsureDecimalPlace_m92BA4ECF36C849E5C8EC554F2D6F6AC8CF86DA9F,
	JsonConvert_EnsureDecimalPlace_mDF59953E3548280017BB06611885F36BE50F570A,
	JsonConvert_ToString_mB9EEC617DD77B39CCD0497B93F908D69F5CC5F74,
	JsonConvert_ToString_mB521F78145736C931F7A69BD0134240267AB47C6,
	JsonConvert_ToString_mFBE8316A78A5A5C88FE309D5977BB6F185F0B412,
	JsonConvert_ToString_mF8D34F71CA5E49C6051840E9A4B8B304FC1B43DD,
	JsonConvert_SerializeObject_m34096C3C3C8C68316091065A81BFD4D09DBB010E,
	JsonConvert_SerializeObject_mFF58B0EEAB5078F452B40E34EA1B6F9D5FC6ED3E,
	JsonConvert_SerializeObjectInternal_m310265D41E19BA0B4541BCA402B9964614AD8096,
	NULL,
	NULL,
	JsonConvert_DeserializeObject_mB54156646D8625F24D192C6B4CEFD186913EB39A,
	JsonConvert__cctor_m9BD28BF24255CABF6422B599CF9D9302F40A669F,
	NULL,
	NULL,
	NULL,
	JsonConverter_get_CanRead_mDAF858A38341BEDCEF64D1A0C63456A12E9B7EFE,
	JsonConverter_get_CanWrite_m25E86A75449F0DF79462DC473771CCF76A9CF5D6,
	JsonConverter__ctor_mBE413F963C1E1D20875BCFE94DB01E09836EA97B,
	JsonConverterAttribute_get_ConverterType_mF585DD254EC1CCC3F79C615264FA86EACA9A87E0,
	JsonConverterAttribute_get_ConverterParameters_m17E8F4F8CD589CD1E5C2D140C98F8B8863ADBF68,
	JsonConverterCollection__ctor_m9107FABC0C640CCC2319C8EA80396DDA591338D1,
	JsonException__ctor_m5BAA60E6F46A629602A17098B65E7C5BA1EC4525,
	JsonException__ctor_mF60EC8D80BCE3097C5F731299209E8C4FD800D32,
	JsonException__ctor_mEEBCB87D800D5FF413A4B0B17783DE362C8C8B2F,
	JsonException__ctor_m8F5B00585F8045EA6F9F026E16EC63DDC86AFA9E,
	JsonExtensionDataAttribute_get_WriteData_m0718786B6D6C25112594F098A8CF31A8C90DC24A,
	JsonExtensionDataAttribute_get_ReadData_mBF0CDE7323AB877197F0EC890140CA3CD80BA36F,
	JsonIgnoreAttribute__ctor_mC097DBAA814AAB0C93437290A1185B6285C73223,
	JsonObjectAttribute_get_MemberSerialization_mFA25361BA866DD5B146D4875AD57AA6C1BD044D6,
	JsonPosition__ctor_m3C4DB92D84AE311EE082E3D1C35558CF05969C28,
	JsonPosition_CalculateLength_mF22E55ABE3DE00539186EC9136AF9FC493E0CCBD,
	JsonPosition_WriteTo_mEFF7AE829F518C06937587D13B92A88F35FCC73D,
	JsonPosition_TypeHasIndex_m4A102E3F53126A23B7200E21A8A391EA1971AD33,
	JsonPosition_BuildPath_m40CD2C855E89F06AE22E0F85895BC6B3CB2450F4,
	JsonPosition_FormatMessage_m1D34B70753BD15131332960EB353A37B1F1E62D7,
	JsonPosition__cctor_mCE4C541548C423ADDFC903FF2AC682996B9B4138,
	JsonPropertyAttribute_get_ItemConverterType_mC4581A062D9DCF64DB3CE11BE06771A9347D9EB6,
	JsonPropertyAttribute_get_ItemConverterParameters_m3845C44A0AD6345D2E53707EBCDA7DEA54FC8CC8,
	JsonPropertyAttribute_get_NamingStrategyType_m9BD354DEDD5126AB21DA4D7C2A49A4396014BDE8,
	JsonPropertyAttribute_get_NamingStrategyParameters_mD0BC1CD962875A82871498CD866B6D840BF93196,
	JsonPropertyAttribute_get_PropertyName_m23FFB79CC575178480990AD5FB0FD1152A912D99,
	JsonReader_get_CurrentState_m4E1A189AEAC68E9BC1A4C4424E87FFB208BDCFBC,
	JsonReader_get_CloseInput_m87EB3CF7C34E7FBA9BA84B812CC4CAE0B9FDE974,
	JsonReader_set_CloseInput_mE443BF3B39BAE245D6D22180962F4F3B92C95EFD,
	JsonReader_get_SupportMultipleContent_mB43563289F840AA64EE3133642CFC1D3340B2912,
	JsonReader_get_DateTimeZoneHandling_mBB69C4CE703B559168E1507D92DCC376C92A85F4,
	JsonReader_set_DateTimeZoneHandling_m0E5237BB4D2596DD809E68DF74D1E3B4BAFE2C90,
	JsonReader_get_DateParseHandling_m63AFEAFCBB887D45584BF4545F50BAE69B76C1A2,
	JsonReader_set_DateParseHandling_mD2CB16410575ABE46D12548EBFD78F5729E5A799,
	JsonReader_get_FloatParseHandling_mD99A882ED5135C5DF30ED4A75D8F421189E45806,
	JsonReader_set_FloatParseHandling_m6B04F861A2FDDEAFE7A2188086B1ADDC1905062E,
	JsonReader_get_DateFormatString_m7F87932F9E5D2B74F3B54789A28381E9A9840B2E,
	JsonReader_set_DateFormatString_m391A0D253EFB555727DD530CD06D53CCB9FFA2E1,
	JsonReader_get_MaxDepth_mBD37C2D7F28221DCECBE06F07EA6A7FFC72682E6,
	JsonReader_set_MaxDepth_m89E61E3B0E0CF20C1FF7F41EE335CF7163ACC05E,
	JsonReader_get_TokenType_m7AD71FFD4018980CD8D4326ADC9E41CEF4349AC7,
	JsonReader_get_Value_m45A724E8BB32073B46D12BF8C6307379ADF7CCBC,
	JsonReader_get_ValueType_m9BFCDA53FB3DCC92EFB30FFDA930762035277DCB,
	JsonReader_get_Depth_m7DBC14E5C00031BF8FCC6FC196F8B8EB8C107031,
	JsonReader_get_Path_m691C59CC56257EAA614A55F1486810F2E6018715,
	JsonReader_get_Culture_m6D0CD10FAFA5D1F263B70A5B8E1989C4CBA235E4,
	JsonReader_set_Culture_mD14841014D4DBFBCBCC1CF76D08D5BBB97251CB6,
	JsonReader_GetPosition_mD19B14381DDEB6225D51F2451BD01655F4ED6963,
	JsonReader__ctor_m538C842DADDC1A2123EC0A967DAE5B2D238B4FB9,
	JsonReader_Push_m9EF8950F853F0A18E15AF98AA17808A8844BE974,
	JsonReader_Pop_m456A183649D027C8568F312336F21B5E97B097B1,
	JsonReader_Peek_m5B31B77B96E6C1C903DB7C1F26AF5E50CA5B71F5,
	NULL,
	JsonReader_ReadAsInt32_m5145898AE873C4309FE67FAD07A61A28DBD3E5EF,
	JsonReader_ReadInt32String_mE034741F9523F88212BD02B315B1F4706640E0B0,
	JsonReader_ReadAsString_mEED3E79A7265120F15FE9A923A4B856597FB6E95,
	JsonReader_ReadAsBytes_mA9850F1C70F0E52C074368A41A923DF8D87EA248,
	JsonReader_ReadArrayIntoByteArray_m7A2400F8D6CF59393BF52F49CF3BF8B7EE8033C3,
	JsonReader_ReadAsDouble_m52807B15F5E3B8BE1592224C27FC3F2CF475D635,
	JsonReader_ReadDoubleString_m0EC90F773B5C56971311C6D049835D1B686DD5D4,
	JsonReader_ReadAsBoolean_m54007931B6EDB0B90463F77D2E67E65BDFD147BB,
	JsonReader_ReadBooleanString_m1F7D6439D9B81A4A2F47C9A64B0D7E4D57936B5C,
	JsonReader_ReadAsDecimal_mE3B0E8DB3DB6BEA704B527E0B1E8A582F652A1D8,
	JsonReader_ReadDecimalString_mC146603458D7A61DA56F11B03AEFD377AD076414,
	JsonReader_ReadAsDateTime_m85AFE4D30022400C2F86DDE61CF6230C31E8C9B8,
	JsonReader_ReadDateTimeString_mABFB2A6AA0E82A267DDA0BE8612E7B57B9FDDB90,
	JsonReader_ReadAsDateTimeOffset_m9C56CF78B5C18254645F0EDA346379720BE1A8C5,
	JsonReader_ReadDateTimeOffsetString_m05BC061E7926F996B22B4CF3E77293C3084B174E,
	JsonReader_ReaderReadAndAssert_mA814836B53ACFDF128319498FC3C9D976F3CD613,
	JsonReader_CreateUnexpectedEndException_mC86C2D5D04C011EF9A3DF77CCC35F43EC3403084,
	JsonReader_ReadIntoWrappedTypeObject_m63038E486E9A183C7234AEBF07FE634DEC08839D,
	JsonReader_Skip_mC8CB495BD0C5108D4AFFD1D660CDFCB659A25591,
	JsonReader_SetToken_mD5DBB8280CBF021243DFBB6CFCCD3A6A8C0F17E9,
	JsonReader_SetToken_m4CA8A55D6760B8E56CF0B9303FCA3D25541AAF84,
	JsonReader_SetToken_m9755E98D6799629F9662BB2252E1D129102DD8CF,
	JsonReader_SetPostValueState_m454C9C82F81D2C182B99CDB1075CE3433E661A98,
	JsonReader_UpdateScopeWithFinishedValue_mF6126F4AF88B9EF97B93846005AD1141DAC9A73F,
	JsonReader_ValidateEnd_m1A4EAAC2322EB0DC4E5F1A0FBBEAEA7E5CFAD687,
	JsonReader_SetStateBasedOnCurrent_mD27DD404ECA14B79FE0785CE0DCF22E321FA4D4A,
	JsonReader_SetFinished_m787790578196C7BD49FBFF2C056DA5805CA85801,
	JsonReader_GetTypeForCloseToken_m3F40F2BC543DAC646D8C6458DC4038D0B8387C8D,
	JsonReader_System_IDisposable_Dispose_m8C100C048698A1E9BDC3FD290DCF9011E9DC3DDB,
	JsonReader_Dispose_m931F4D5CEFFFE7A6DAA4BDADDF55EA3D8267E582,
	JsonReader_Close_mAA1C1B8767BCAD23B09D66C35C3808B7CAA40AA0,
	JsonReader_ReadAndAssert_mA82EE782D85622671FEC7696CDB268A9864B45D3,
	JsonReader_ReadAndMoveToContent_mD9883A03037CBE7F89B7F261B713D61AE9865AF2,
	JsonReader_MoveToContent_m3DBD33F19BD356A29544FD39C6BFE643DAB035FA,
	JsonReader_GetContentToken_m597AF0481D4A5EFFB8F1875EC2AD7FE490C2E80E,
	JsonReaderException_set_LineNumber_m9F168878C1D77923580E25AB5072BB89A2A1FE93,
	JsonReaderException_set_LinePosition_mCCABCCE6749D6A36BF93E146FEBCA1E539F83FDA,
	JsonReaderException_set_Path_mC99BD3BB99E4F85727B345B5CCDA60E43AB30AE0,
	JsonReaderException__ctor_m0CB8C02A7D13588E2F1B35D181AD5EB5E14F808C,
	JsonReaderException__ctor_mD75FFFBE2EFF11240A34CCED588B884EB4F41E05,
	JsonReaderException__ctor_m48D67CFFB089EFD85C083C7029BDE3D86ED72CA4,
	JsonReaderException_Create_m9D60924AD1199D5FBA2F048E12C914D6E00BB8BD,
	JsonReaderException_Create_mB0D4CC9583C3A7AF5F588DAB965AC1B7B6C5C97A,
	JsonReaderException_Create_mF5A77FD722D66EC83FD22D6E93740FD4DD546A64,
	JsonSerializationException__ctor_m51DC80D4F299ACC4A3340B74A3A81A09A003AAE8,
	JsonSerializationException__ctor_m6FE8C668628845DB845F9DDEC79C50C300007444,
	JsonSerializationException__ctor_m4FCF2C5B84A90CDFD7C21E1C40E96C9315A9B552,
	JsonSerializationException__ctor_m8AF0A76617292B37ED2C6304A45CA83C524B6B45,
	JsonSerializationException_Create_m59D9D0A78A9893C93F966A7ADF83DF696415D38A,
	JsonSerializationException_Create_m55E8DF615AD31911F8A5BBFDED20ED83549BD64E,
	JsonSerializationException_Create_m7BBB6C36BB6D2B61654B39C30B54097857521242,
	JsonSerializer_add_Error_m5C077501D7D73DDF93019D736A6F72FCB733EFDD,
	JsonSerializer_remove_Error_mF178375434BC06707ADEE0A7DC544296D5146B76,
	JsonSerializer_set_ReferenceResolver_m75556B8339BEC4059A2F556CA8A0B60D5CDD62DE,
	JsonSerializer_set_Binder_mD26D3C7E5F24601A2F09DF2C0BF8F69310FC0835,
	JsonSerializer_get_TraceWriter_mB1337CED6084DADE1E3C62F9EE58AE1E5C0BCF41,
	JsonSerializer_set_TraceWriter_mE16C70457284F8CA52CE1836DEA2C8D54F3C8289,
	JsonSerializer_set_EqualityComparer_m8494E039197F35E6D1B5EC199EB8BBB4328EFF6C,
	JsonSerializer_set_TypeNameHandling_m397964590BDEB713460224E00325B456D29D2678,
	JsonSerializer_set_TypeNameAssemblyFormat_mD3C6C79E85CE811738242E26FC1F13264D749EC6,
	JsonSerializer_set_PreserveReferencesHandling_m7CBE2D82706C9A69443E539D98B57BF95D5B5B3F,
	JsonSerializer_set_ReferenceLoopHandling_m05EAE1196EFE2ABA674535EEB00D8479D19B4BB5,
	JsonSerializer_set_MissingMemberHandling_m93EADF9C8A721C50B3FB4A078E221EEE81EB36D6,
	JsonSerializer_set_NullValueHandling_mBF4C642BE01DDC827B133DED967ED739EDA25E42,
	JsonSerializer_set_DefaultValueHandling_mA55AFC08ED20A575679BA593400426C27ECD164F,
	JsonSerializer_set_ObjectCreationHandling_m7866DEABB0F449EBE1B6AE41591133440F985DF3,
	JsonSerializer_set_ConstructorHandling_mD2CE70B58224E9711FA6F105DB05BEAAAB245654,
	JsonSerializer_get_MetadataPropertyHandling_m8D89B353DC55367E723C801CB52084011A735B40,
	JsonSerializer_set_MetadataPropertyHandling_mA2D6F880BF40FDCC4C94B43E212368CC40043470,
	JsonSerializer_get_Converters_m5FD1DB9720DDAD15A404A3FFBDCF761892D0A965,
	JsonSerializer_get_ContractResolver_m2B3249B713D8C5FE5E05DEF4DBF6290F2892025D,
	JsonSerializer_set_ContractResolver_m827D6D814BA33D3793060CF74F2F2E7CAAF07EE4,
	JsonSerializer_get_Context_mE40E7BB6FEE586934C0B31F0DE2DB65C4BF12C65,
	JsonSerializer_set_Context_m1D8E4106F702203C6E81801BCC5F8A10F5124C39,
	JsonSerializer_get_Formatting_mDBB1A0DD792AD14B9D395F4998D5BAF25DFC586D,
	JsonSerializer_get_CheckAdditionalContent_mDBFF4CC55D193273172E9BD6FC60022A434A9698,
	JsonSerializer_set_CheckAdditionalContent_m6B7F7AAFB1CEA4D15174DE2B4C4B713B51F23B8B,
	JsonSerializer_IsCheckAdditionalContentSet_m0E08FBB61882777E98BEFADE819DB8B093D32067,
	JsonSerializer__ctor_mEEA9E2EA97F144384EC57BD0779DE4AEAD2484C8,
	JsonSerializer_Create_m21A9167E10C2F453C4A8C31DF1C9DB0BA16B1C2D,
	JsonSerializer_Create_m270F5EC9830F09ED5E30B71E603F1DE24545C3B4,
	JsonSerializer_CreateDefault_mFE0495565E208070F09F290F850524CCE98EF9A1,
	JsonSerializer_CreateDefault_m0E1231433DC981D1ED70DCE89AB6AC9E6091484C,
	JsonSerializer_ApplySerializerSettings_mE0DBDB2D8F2169D65CDCC34A5D57BAD1D73DAF4A,
	NULL,
	JsonSerializer_Deserialize_mA9114FAC8423AE9FEF695320ABD2C6EEA7B6A0F1,
	JsonSerializer_DeserializeInternal_m3C23ED58C2B6AA6A362B8AF3F4BF61E819B8D62C,
	JsonSerializer_SetupReader_m6355C359F82DF266F72557EA50B1D4949C12BAFC,
	JsonSerializer_ResetReader_m91B70A8577CCE724D494AA0E3444CA7E8D37A2BE,
	JsonSerializer_Serialize_m3D0B5F576D4705243F3D5706CAD3417D333686E9,
	JsonSerializer_Serialize_mFED2010B103C1A19C4601BFAAB18AEEAAB11DE2B,
	JsonSerializer_SerializeInternal_m7A4AB1E8CEB86E78833272778CA662F77B92AF5F,
	JsonSerializer_GetReferenceResolver_m862EC8CEEC3E04D76AD12A27FF37432A3D87C144,
	JsonSerializer_GetMatchingConverter_m35B81E973C750954126CDE53BCDF9141B5C00482,
	JsonSerializer_GetMatchingConverter_mFCCC2F4C1165495D46898B673F1683DAA5C13671,
	JsonSerializer_OnError_m241DB4FB4B83E5BFB2E42194C266EF2326719803,
	JsonSerializerSettings_get_ReferenceLoopHandling_m157C6A9C3DD60D87575C180A13710D626854A3AA,
	JsonSerializerSettings_get_MissingMemberHandling_m17A7ADA7EA3932A4643E6F9D06B4D1891D51F7F1,
	JsonSerializerSettings_get_ObjectCreationHandling_mCE8BF67E879C6FC72207919951EE5E3FAF67E2D4,
	JsonSerializerSettings_get_NullValueHandling_mB7E2F6FBE8924F97F7A393688AEC8D0FD9F36E2F,
	JsonSerializerSettings_get_DefaultValueHandling_m44640DC8021650A01ABFC24F27A59C7332826AA4,
	JsonSerializerSettings_get_Converters_m862DFE5BDA2760F5946FB6DDB34DCA67FC6FF29D,
	JsonSerializerSettings_get_PreserveReferencesHandling_m9C0757967BEB3C25C034F36CDE2764CDC619988B,
	JsonSerializerSettings_get_TypeNameHandling_m23E1F5B25A7A92865E827291C1B64FBBC2B95085,
	JsonSerializerSettings_get_MetadataPropertyHandling_m35544141ADB21B96E7B9A393BC5F7DEF5D6CD351,
	JsonSerializerSettings_get_TypeNameAssemblyFormat_mCFE36076151C13EEE61BC890307ADD081E07F9C0,
	JsonSerializerSettings_get_ConstructorHandling_m0ABCC68D14B15898FE5424D80284B7DCEF481D6B,
	JsonSerializerSettings_get_ContractResolver_mAE8A1AB42B6FA02463E9A3B613A777983107D591,
	JsonSerializerSettings_get_EqualityComparer_m41C02E38C29AF6A4CAC4894CC79FA724974CDC82,
	JsonSerializerSettings_get_ReferenceResolverProvider_mE84D665F375BFFC7BF287CB869DDEF0BE408853D,
	JsonSerializerSettings_get_TraceWriter_mE3D106D673BF56399607EADC698FAC00AFE65C72,
	JsonSerializerSettings_get_Binder_m9508F5EA174FF976E69F0C78B3934BFC56E2F035,
	JsonSerializerSettings_get_Error_mEDC7BE577D288DD35B65F534E00C681854578371,
	JsonSerializerSettings_get_Context_mE4B431CE4CE95A0D7E3BE9AFD454A97521562D2A,
	JsonSerializerSettings__cctor_mD0353F0CB2B4593EF48159E5684B31906DFA9AB3,
	JsonTextReader__ctor_m06ACC9F0CFD321E7211EFD16355C213B2E6E4E05,
	JsonTextReader_EnsureBufferNotEmpty_m7355FDB571D125D65BE7D61235F2C60464436B25,
	JsonTextReader_OnNewLine_m582BF1C40C7339C59BE7BD03BD83BF54118B2B57,
	JsonTextReader_ParseString_m6467AEDB7C8DF6B92A3DD84B3E37B72C8A3FE342,
	JsonTextReader_BlockCopyChars_mB6E7365C5F1F14BA57632535B0A7A278555F0691,
	JsonTextReader_ShiftBufferIfNeeded_m3E8D22377380A3CCE23A17A06F715E87264FC4E4,
	JsonTextReader_ReadData_mBE18A474B103A29E2A479E09B9E6DBDC804A46B0,
	JsonTextReader_ReadData_m1E0613006BB76DDEA88200B19D58D5A5C550181D,
	JsonTextReader_EnsureChars_m86780F558D161C61231205708800459DDBAA6D74,
	JsonTextReader_ReadChars_mE7E9AEB29792C6EA77283D150A83E7F6C4AB4CE5,
	JsonTextReader_Read_mC2B5F6EFA1BA940FFC3D7EFBF55E95D0DB43C88A,
	JsonTextReader_ReadAsInt32_m9C94B1752FDA9D891FD4806A7440EEC75404A399,
	JsonTextReader_ReadAsDateTime_mD60471AA24C84837063F93A8EE1B0DF585435586,
	JsonTextReader_ReadAsString_m4DF18D5C7A49CAFD5B19DF5C661B5BCDC59767D6,
	JsonTextReader_ReadAsBytes_m7CA56D7E09BDA42BD5D6FF1C96FF90344777843C,
	JsonTextReader_ReadStringValue_m1BE5443E3B38ADE0C651B102A032318428EC3815,
	JsonTextReader_CreateUnexpectedCharacterException_m24DCCF2AB7757041F94D527B9FF2ED3B59D5D31F,
	JsonTextReader_ReadAsBoolean_m9CDDD7048A7D6E3C9748120A9F237DF26DFDAE9A,
	JsonTextReader_ProcessValueComma_m791FA99C7CB07102A0A6A3A5BA66DE76079EC83B,
	JsonTextReader_ReadNumberValue_mA2DCFEE2517C40092E342610B459F9B958932C4A,
	JsonTextReader_ReadAsDateTimeOffset_mEA8C87CEBD105784B897C3A9DE3C65E110AF0A50,
	JsonTextReader_ReadAsDecimal_m6D0EA57C831C43A05D94BC98D83C26ABFF3B4434,
	JsonTextReader_ReadAsDouble_m04166D16390430DBF24603D47AB85EB68F1878A2,
	JsonTextReader_HandleNull_mC51667891782E90C011E89A519C5EB3ACDB4579D,
	JsonTextReader_ReadFinished_m564A05DAB39D8DAAD04CF2030AD27D828359E973,
	JsonTextReader_ReadNullChar_m62DC2673AB513DD4A81A1B988CD97EE4D3B30795,
	JsonTextReader_EnsureBuffer_m304C435886673BC93B1032E1E779302CE41527D9,
	JsonTextReader_ReadStringIntoBuffer_mEE64F5783FAE0AE0B9A6BFAF1BF02832CCBD313A,
	JsonTextReader_WriteCharToBuffer_mD005722B4B821C657F56C2F7C65BA9BC238CFC30,
	JsonTextReader_ParseUnicode_m6CDB5D90B6CF95C63EFB13CCCC1034E9F7CC3920,
	JsonTextReader_ReadNumberIntoBuffer_m2BCBC9B74CC5850348F922F98C6E9D9AC3DC6F64,
	JsonTextReader_ClearRecentString_m7E76B1C8543F5E79E2DBDC1A62A13D01B6622F4C,
	JsonTextReader_ParsePostValue_mAFA8A18FBACA01B355A97D6285A7C51C5410E891,
	JsonTextReader_ParseObject_m2AF08458DE9247B2F42E412263129F211B6827FD,
	JsonTextReader_ParseProperty_mDB11C172A1698122C5842F36B6CFEF3F0F0727E8,
	JsonTextReader_ValidIdentifierChar_m20031CE2F94DED232AE1BEEBA383EFBB6EF37812,
	JsonTextReader_ParseUnquotedProperty_m165787F113F0BA1DF08DB70C8BF7F90A7A7B2CA1,
	JsonTextReader_ParseValue_m3EE97234F3EF348EB23F457158124996743D4EB3,
	JsonTextReader_ProcessLineFeed_m424752F705A641B3799C6EE717276DDD54FFFBA8,
	JsonTextReader_ProcessCarriageReturn_m9D9B468775FA20E403D4AB8F5C2793188A0D3116,
	JsonTextReader_EatWhitespace_m874203972F0D0C280B05BFFD23F714139F7935F7,
	JsonTextReader_ParseConstructor_m62E8F83002853986BC55F7A96CE81B705E23D8C5,
	JsonTextReader_ParseNumber_m3CD060C4B577BFED08E5AE7A29865B0C23D4DE88,
	JsonTextReader_ParseComment_m1D12FA3D94542985D227885F9F09EDACC14C2D9A,
	JsonTextReader_EndComment_mC319298FEC64DD66DD6C910C31ECDA16CED8BC82,
	JsonTextReader_MatchValue_m9904A8B0FFC184AA30F4ED2D5289B1EA81FCF622,
	JsonTextReader_MatchValueWithTrailingSeparator_m193F3B827101D97DAB81CD58F3E37E72586063C1,
	JsonTextReader_IsSeparator_m797230AE86E47CF40FD5D4348633BE5AC6C8826A,
	JsonTextReader_ParseTrue_mCE289BB4C4A01C54D230FA78B37333C7A1AE253A,
	JsonTextReader_ParseNull_mCEC838A3EE3FD7F27D6096251B16E698A8D2EC1F,
	JsonTextReader_ParseUndefined_m7353BA0E92FB8B49671B49F01C6DBC495DCA63C1,
	JsonTextReader_ParseFalse_mA22082DD717016BFD4CE9B4B22A3EFBD35FD1C2B,
	JsonTextReader_ParseNumberNegativeInfinity_m8D9A6D80029CF74D066D38305476A4686EA5075A,
	JsonTextReader_ParseNumberPositiveInfinity_m571720F3E3D4701116DB75EBC528844AC0412E11,
	JsonTextReader_ParseNumberNaN_m7C8B9D84EDDC4FB7C92EEB4B8160F0881C86E3D7,
	JsonTextReader_Close_m1309D7A5CF41328B7280DB628ABE83D11C3FF4C6,
	JsonTextReader_HasLineInfo_mC8EABEC58BB5FD218E87B3CA84E261B9512E234F,
	JsonTextReader_get_LineNumber_mCC497D53352BF5BAB5AE9FA600D84E7148361E2F,
	JsonTextReader_get_LinePosition_m0A76131C697A84985FF4CDAD33C3363B7C9C6B8F,
	JsonTextWriter_get_Base64Encoder_mF7A61C0288E78B9CF88F788181462F7FE0D54B72,
	JsonTextWriter_get_QuoteChar_m2AA4D871344E874303953504844A2A8916A1D2C4,
	JsonTextWriter__ctor_mC16FDDC0DAEC5D2B51445A44A40273E4E45D03C8,
	JsonTextWriter_Close_mA44AC213BBBBFE14DEF9182105B22824CD2BCE33,
	JsonTextWriter_WriteStartObject_mA9225FFF1F59FEFAF7E286846F959BB3C346E852,
	JsonTextWriter_WriteStartArray_m67717606BC5BB1D07EEDD3F201421C1FACF9D6EE,
	JsonTextWriter_WriteStartConstructor_mCD8166C3054966692CC42D984237154366FF9009,
	JsonTextWriter_WriteEnd_m2BE2304BACCCCC344715B2B2368AE86540E8846E,
	JsonTextWriter_WritePropertyName_mF4930040832DB4C17FCF6DAA88AC2EB165612A35,
	JsonTextWriter_WritePropertyName_m316550B0B5B7AA0BDD06BC8A55ED7004518E1D34,
	JsonTextWriter_OnStringEscapeHandlingChanged_m34B48FDE6C7B276920BBB7987CAC438FD5DBAD37,
	JsonTextWriter_UpdateCharEscapeFlags_m22D06D2618B27EC099B02D9E4C498F78CC5EE045,
	JsonTextWriter_WriteIndent_mB8EFAEB0EE303AC6B054286E5519D0BE94F0009C,
	JsonTextWriter_WriteValueDelimiter_mB96308335A8C2C90B44C54E2B17FD99184FA27F8,
	JsonTextWriter_WriteIndentSpace_m75850BC7570E76FC461236390147BEC4A1ADACAF,
	JsonTextWriter_WriteValueInternal_m083E0E9A0369C4F57F3EC5189681D46CAFD179B7,
	JsonTextWriter_WriteNull_mC9711AAA59CE615F3999165773A65195E299C5E2,
	JsonTextWriter_WriteUndefined_m502FEA1A07FCD29627DA574CC8F993818EE8D6FF,
	JsonTextWriter_WriteRaw_mFFD05486F43FE37AACB4BB0FD92158FF869CFBE8,
	JsonTextWriter_WriteValue_m3B97EBDBBB56F9AC0A783B93D6A5E2789B62E2F6,
	JsonTextWriter_WriteEscapedString_m0A4050B72FB45C522F730DD4FEA7018D62D22C8B,
	JsonTextWriter_WriteValue_m308F2014568514E7CFA6798E6E804D36F8B525B3,
	JsonTextWriter_WriteValue_m5BC424CE917FE84086E29B839EC6A292C75EE795,
	JsonTextWriter_WriteValue_mA071C2F8A06B852BB11AB4083ECD6184DF73E4B0,
	JsonTextWriter_WriteValue_m14AAEAAC3218C11F806E7BD4E7F4D0592B321268,
	JsonTextWriter_WriteValue_m68DF3FE43E8918A4BEDF20E082A38719DB9CAE14,
	JsonTextWriter_WriteValue_mEDB5BCC7920B361EF29099F684DF1A4F96BC2163,
	JsonTextWriter_WriteValue_mA94B419350ABB45DC7BBF3071EEA37A2FF89CCE1,
	JsonTextWriter_WriteValue_m3AC83EC306770632B846C16A1EB036921FA213E6,
	JsonTextWriter_WriteValue_mE402870CAB50FA196BB0F4D09B38251C623F9EE1,
	JsonTextWriter_WriteValue_m265CD7AB8D7BBC284891D73EAE64D4968F6CCAE5,
	JsonTextWriter_WriteValue_mD772006CDE4E7F9E184D192814132E2D4EC77884,
	JsonTextWriter_WriteValue_m0E9D82A09CE959FFBAA6F5467F633956A67D5A62,
	JsonTextWriter_WriteValue_m120FB9807C7803ACDDB286B147980A7AC20E8BDF,
	JsonTextWriter_WriteValue_m909BF8D81AD7BD056580595C10A84ECAEC244C67,
	JsonTextWriter_WriteValue_mD7BBF779E1FE4721F447208F3AE92C6CC83D618D,
	JsonTextWriter_WriteValue_m5D22A35E72B70F9D6694B07656C73A01F351094F,
	JsonTextWriter_WriteValue_m2F66121E669E6AD2627D85DACFEC5DDFDBD3CCE7,
	JsonTextWriter_WriteValue_mAAEAD281D68ADF7BE51F2E830FB7597B2A812847,
	JsonTextWriter_WriteValue_m4AC01A5A85731A4358638F592CC8C916081E5304,
	JsonTextWriter_WriteValue_mE728F5A883480A453D7D149899205AAFC4F5A743,
	JsonTextWriter_WriteValue_mB9DC613E1F00289F0E4D2B3E04DB101E441E63B0,
	JsonTextWriter_WriteComment_mEA109E10AF103412C18A849DBDB6A828DD0CBC1E,
	JsonTextWriter_EnsureWriteBuffer_mA9412FB962FDB40B1A2B6DAA11DE34C1DA77203E,
	JsonTextWriter_WriteIntegerValue_m0129BBC377D1E19AC8A7E50985FA7BD79743F654,
	JsonTextWriter_WriteIntegerValue_m0D21F44FC6A21CDDB81D8BC2FB52D4DE559C6150,
	JsonWriter_BuildStateArray_m4F4220F4E23032D9268FE5EA43E1D17BF33AA23F,
	JsonWriter__cctor_m1632023B0C28407CE208CCF8277D780B026A1B62,
	JsonWriter_get_CloseOutput_m5CADEDBE98FDF046F9BF209C242A07DB08C9678B,
	JsonWriter_set_CloseOutput_m583E020AD56A73E77CDE34C208DB0115E067C7E0,
	JsonWriter_get_Top_mC1FF46132B4A44FB748FBB30230BAD1F22C7147F,
	JsonWriter_get_WriteState_m063AA8C1F1CFC3942B5866FBCE85969CCE98E426,
	JsonWriter_get_ContainerPath_m7D22BB50285380B5CACBFDEF88BE31E614CD52A8,
	JsonWriter_get_Path_m115B40757B4772E99AA6A901ABBD845557B19A2C,
	JsonWriter_get_Formatting_m8A8EB004835AA8C5D570DB62B31374EBE17BA77E,
	JsonWriter_set_Formatting_m095D00C9220A17CD8CF9A678804C7F53AA436D68,
	JsonWriter_get_DateFormatHandling_m41BD1D20C51ECF8DA8202C37B62A9FF0E28823DD,
	JsonWriter_set_DateFormatHandling_mDE99B2D5CE84B4848D5B986BCEDA0B3ECF022923,
	JsonWriter_get_DateTimeZoneHandling_mD0C65660E7AE6E3C2FA819B280CCB795B7A2728A,
	JsonWriter_set_DateTimeZoneHandling_m05E9A774E4CD7AFC7DB4F8F072C653360648BF20,
	JsonWriter_get_StringEscapeHandling_m113D87EAADC49F42DC89E128A7DFB12D46693927,
	JsonWriter_set_StringEscapeHandling_m02C95A592E7E217963FEB61C85555751805F4C06,
	JsonWriter_OnStringEscapeHandlingChanged_mC3CF4D26C7EAC54AA304922EA8D39061398B7F27,
	JsonWriter_get_FloatFormatHandling_mD88C6F4D05A2BA565E093D2D864537B3BB78FF52,
	JsonWriter_set_FloatFormatHandling_m0AED0049F5DCE536ED7C5B9D32EA996CC6B6D8A1,
	JsonWriter_get_DateFormatString_m69A32477FF1B1AF8DF997B4D018C24BD5825FCCB,
	JsonWriter_set_DateFormatString_mF4ECA8F9127550A893C594291DBFBB0BA1B31F3F,
	JsonWriter_get_Culture_mC42B914B29D7FEC2B0F98DE7AA32F289DB318F89,
	JsonWriter_set_Culture_mDEE561B11776B97A927F1300FF7B6A3ADE81B2CA,
	JsonWriter__ctor_m831946FD2352BD57949E806795F738B790BF4471,
	JsonWriter_UpdateScopeWithFinishedValue_m1494B1E2F01253905C5F6C856042DAD4293841CF,
	JsonWriter_Push_mFE69FC6187F3DBC82D5BD164CB241978F05E602A,
	JsonWriter_Pop_m057A476783F0ABA0D103DE3F7A9A9D42770BC7A4,
	JsonWriter_Peek_mA9174FC2495768EA21224B1487DC920BFB013E24,
	JsonWriter_Close_mB691844CE67B0DF402BBB9D373F9031EC8A441CD,
	JsonWriter_WriteStartObject_mAD048398098C2F44099A1479284A011012215F5D,
	JsonWriter_WriteEndObject_mFA72AE0AA3FC180698A8D2B9BFD54799EF9E5BEB,
	JsonWriter_WriteStartArray_m645721F97E9CEEE7E587FDF935E35A6E7BBF3292,
	JsonWriter_WriteEndArray_m9CD3DFB809FDFAD3C5F607A6E06B41ED4F33CA7B,
	JsonWriter_WriteStartConstructor_mA7E763E836F04492AF16B86F6675F2BC1584206E,
	JsonWriter_WriteEndConstructor_m178608E8B682B73E3E06D9C5B89A2D449A170094,
	JsonWriter_WritePropertyName_mB9C984656F23C60FB0B42602B4B6770C20758106,
	JsonWriter_WritePropertyName_mE9DEA27A5A9BBAD08BC1611823ED575158A3C474,
	JsonWriter_WriteEnd_m3E4EC1233787C4959480B758E529B08302C52EAD,
	JsonWriter_WriteToken_m7FF2C9C3D1C060A6A42BE8580916BCF88CD85521,
	JsonWriter_WriteToken_mA6815D2FA925A2228379028960B68BA3105A6A52,
	JsonWriter_WriteConstructorDate_mAB9A74738C407D103D4B9926B4D076755E5736C1,
	JsonWriter_WriteEnd_m13FABF850F81B9E64D2AAE495AEB139BC1DB6740,
	JsonWriter_AutoCompleteAll_mA2FD8E3D679166C19D7622676AA75A53343F6E86,
	JsonWriter_GetCloseTokenForType_m9CB97B7C53811B52453B193B402AF3EA8B81BA92,
	JsonWriter_AutoCompleteClose_mE666286DF09F35E57BE286544C755288171FB1F7,
	JsonWriter_WriteEnd_mDFDECDE31664E9BC21FB6B17B9E5E1529CE49A54,
	JsonWriter_WriteIndent_m9E01216775D39ADF22265B3A972095B9F9EE53E0,
	JsonWriter_WriteValueDelimiter_m699B96BC6B068A5DE28CFB0DBA18E73443EB01AE,
	JsonWriter_WriteIndentSpace_m8E466D77C850ADA436D8EB1BE0842853BBDF2456,
	JsonWriter_AutoComplete_mF0F05182DCE58A502FCA9886491D021C5BE77891,
	JsonWriter_WriteNull_m88B5205E33D80E0D45B4401511113EC6712DCF89,
	JsonWriter_WriteUndefined_m3B0651930350243F73ACE48B331FEB7255B80878,
	JsonWriter_WriteRaw_mF11816DF21E2285E0E6E6E51440290572C1E8BFC,
	JsonWriter_WriteRawValue_m39ABA99A893489F45A5C8205B839DB411A85CDA1,
	JsonWriter_WriteValue_mB0E1A92D2F72F3463065BEF9972FB452631C08F6,
	JsonWriter_WriteValue_m1FEFF250C239D3D1837EC5BCA4AFBFFC5D6E0D34,
	JsonWriter_WriteValue_m9B2A0C880487EFEE143F0A2F0282A24F58E39BB6,
	JsonWriter_WriteValue_m93F9CEFC9ED2EF7A9449B780DEB568D44430F72F,
	JsonWriter_WriteValue_m3B79659B76CE197EA62C98D9380CA276779BA041,
	JsonWriter_WriteValue_mBAD37C743F5E6B0E0CF3AD23B6C475BF651E7CCF,
	JsonWriter_WriteValue_m3ECA9D982522775E820520C0DEF96D5A10A7ABE9,
	JsonWriter_WriteValue_m23C293A28ADF73EAC984FBFA034E995CA39C96C8,
	JsonWriter_WriteValue_mC2E8EEA42CBCEF2CE3D63B1CC50EDC4029060DB0,
	JsonWriter_WriteValue_mD04367BAD3BA9FC4D4991C0BA35B49DA15675F96,
	JsonWriter_WriteValue_m248832B7521A3E12DC721F129DCADA8A5E6B0D64,
	JsonWriter_WriteValue_m97D7CA12924339EEDB30C971A6C94F93A8C430FE,
	JsonWriter_WriteValue_m402DBD63BF18B24EAA0D34D7D6208CE8C696F3AF,
	JsonWriter_WriteValue_mC37C52C1891FE1988D9C92D8D55BB9E890B1B6ED,
	JsonWriter_WriteValue_m65ADF4B7E727362C28EE741700A6B1B070379672,
	JsonWriter_WriteValue_m0E6A04360432620F55CC03FF7E274ECF8171CD33,
	JsonWriter_WriteValue_m82BA7513906CB1637720C0209867F2542A6B84D0,
	JsonWriter_WriteValue_m1BFD61E3B8431D93875DED43BDBA437CF2E43345,
	JsonWriter_WriteValue_mA4A58068231556A9AA3B9BD4A0797B178D687B9F,
	JsonWriter_WriteValue_m55DD87A8EFEBB6A73C70571592F6F2B63DFED99D,
	JsonWriter_WriteValue_m54BE6FA02D5EDB12862AA0B8E8A58D4525649E88,
	JsonWriter_WriteValue_mE256A509ADFD1D2EB3C2CE59C48BD4B5F9A7D075,
	JsonWriter_WriteValue_m57C10991081DF13719790B2CB6F91A527F2FFF1A,
	JsonWriter_WriteValue_mEBECAA46B25096965E7D208F3923AA99F1561B6A,
	JsonWriter_WriteValue_mE53D6A58FE531435115CB256CF213DBA4FDDAA66,
	JsonWriter_WriteValue_m22EF969D9C1055345309F4A0D50126DB5916A56C,
	JsonWriter_WriteValue_m497A20806672585D0FBD37DF1707485B294C5C3F,
	JsonWriter_WriteValue_m60EFE07D40ABA81ACB9FC5A48C32C653B269B89B,
	JsonWriter_WriteValue_mAC90C70E3B38A618B91D7FDDF8F4C4CD9BD59FB9,
	JsonWriter_WriteValue_m06116AF904E6923B19234D2787602E24995B8F7E,
	JsonWriter_WriteValue_m9148650421E38CD2C050395BB8F34E5E3098D1F6,
	JsonWriter_WriteValue_m194E49E503F3ACFE01B7ED699643EB75F6B95706,
	JsonWriter_WriteValue_mA87A6026985736F0B036F5FA43C1549838EAF782,
	JsonWriter_WriteValue_mDF994376EDAD173BD0203B6B780EB86CD283920D,
	JsonWriter_WriteValue_mBD6E128DC5F21B7C3A771BCE25458698ED5EF670,
	JsonWriter_WriteValue_mEEE2D6400B690F240DC6E8B58AD3613DD6719821,
	JsonWriter_WriteValue_mE84B57D0BC684CB7BEA071587B6E5F04DAB3240F,
	JsonWriter_WriteComment_m40A51166C1CABA6F1F5637A5373F2503C91083BC,
	JsonWriter_System_IDisposable_Dispose_m8287E4A9B3A83014642A1815081136A31DA7ABE9,
	JsonWriter_Dispose_mABF23CF5D09E87B6917E6BE893D147877CB63F37,
	JsonWriter_WriteValue_m4A98E7D7B10B3C530016466AEA474B56F4D566C5,
	JsonWriter_CreateUnsupportedTypeException_m398B23C6D40B7290827B4AE2C9557DEC38491AEC,
	JsonWriter_InternalWriteEnd_mB5E21FFBB1CEE513707B5711972050EDD83D3D56,
	JsonWriter_InternalWritePropertyName_m7C0D3E30B796AD03833B954BFB91E2DE7C63198A,
	JsonWriter_InternalWriteRaw_mA0A1CD55303C5E56AB15BCE03B8BE1D08B9E0D26,
	JsonWriter_InternalWriteStart_m8E13CAC34A8FCC7BD4E858C9D30D080B3C986FF6,
	JsonWriter_InternalWriteValue_m28397BF3152152BC8AE2D8CC9A864E87D66FDC48,
	JsonWriter_InternalWriteComment_m5093DCECEAC4712591E4E75168FB90351CEEE8FF,
	JsonWriterException_set_Path_mD53E9F4BDDD8ED20EB04BF6CCE733F2DCC593BD9,
	JsonWriterException__ctor_m8055EB32091B663257632E594A774CB5FEF38E3E,
	JsonWriterException__ctor_m4B0DB597DB75DB8DCA879D8E6FFFD1EBD79092CC,
	JsonWriterException__ctor_mF68891823167959E702D8AEAF9D77F35B1F7F849,
	JsonWriterException_Create_m0D1BE0A49EF6360C1596523F912D44A69790881E,
	JsonWriterException_Create_mD8005E323C3C20A43F1F7563F3D6925FCD2DCCCC,
	Base64Encoder__ctor_m65B2F1A0C73150DF84B2BFB890BA113A660FB2B9,
	Base64Encoder_Encode_mECE2AF7E926D851B15D734BAC400092B916F3981,
	Base64Encoder_Flush_mA3BCB12F40C04027A0C9F52EDFFB3CE2E33ACAC6,
	Base64Encoder_WriteChars_m9E49BFFC9056F93A431C04AB0DC4698DDCD36B37,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	CollectionUtils_IsDictionaryType_m54F454CB807B6A4D6207FEBAB94AE3746BEFE13E,
	CollectionUtils_ResolveEnumerableCollectionConstructor_m78C54F3D43C7870013DAB6566A0974AB43072C84,
	CollectionUtils_ResolveEnumerableCollectionConstructor_m0B20B7714B1556A1CA80EA2F2D53F0198C1A5752,
	NULL,
	NULL,
	CollectionUtils_GetDimensions_m4F2292D6D4C8351343188E6EAC0210F7B649170A,
	CollectionUtils_CopyFromJaggedToMultidimensionalArray_m91BBE92C8DBE92331B859CCEDB7CCD34356E4497,
	CollectionUtils_JaggedArrayGetValue_m9008D23A3AB4C82A795490D4804CF0B9D35B8855,
	CollectionUtils_ToMultidimensionalArray_m99EF2F761503F27C1620927479D454B88CA1E0A4,
	NULL,
	TypeInformation_get_Type_mF16FF676DAA9DEC81AF0A9FB81DA0834709F1F9D,
	TypeInformation_set_Type_m87F6F17BD4A47C4F020DBC8E25462D3F4651B750,
	TypeInformation_get_TypeCode_m3F9374AB9A57BA647C5B53B633D16102723C8435,
	TypeInformation_set_TypeCode_mD82AA32EF804000604122B9541C621AF00622767,
	TypeInformation__ctor_m957F0EFDD41CC462F8CA296702E33F67DBB3CB98,
	ConvertUtils_GetTypeCode_m5FC5A22E9C4DBCA27C5543DD30AC662CDF9F11E7,
	ConvertUtils_GetTypeCode_mD6AD89EBDDB14DBE10A270A17D8E7CA936004913,
	ConvertUtils_GetTypeInformation_m0B253C7635BD26C35A9F144129811957E60BDC70,
	ConvertUtils_IsConvertible_m5DF9914844429A628A3B0E46D52F8F28998FE399,
	ConvertUtils_ParseTimeSpan_m6176F121BB315FEB849C44D9560C0792184210FE,
	ConvertUtils_CreateCastConverter_m302B3ABDB587A51DA380B2892A6F4314C1258491,
	ConvertUtils_TryConvert_m1B5389E14C0AEB4373E66D9BDDAE5FF4C499FAB1,
	ConvertUtils_TryConvertInternal_mE463A84642C8EBFAD6843BA9AB3BC88533D4E40B,
	ConvertUtils_ConvertOrCast_m7862E65C180FF18A2F4847F8051C181290AEDDA8,
	ConvertUtils_EnsureTypeAssignable_mAF9E91775390D694C4C02C88173F40942FD26531,
	ConvertUtils_GetConverter_mAE05CA2EEC6E063E2108C8B9167AB891384FA2CC,
	ConvertUtils_VersionTryParse_mA4AC347AF3AE558B9FBA3FD042760A7648A26DEF,
	ConvertUtils_IsInteger_m99C9EEA9B9D813CFBA8A9CE54DA9C7373C2697A6,
	ConvertUtils_Int32TryParse_m766BAB9862F8082B0B3953727DF54CFF80F23729,
	ConvertUtils_Int64TryParse_m7995B30A21526FA60DC9EDCE687B69DC9D99B57B,
	ConvertUtils_TryConvertGuid_m7FB11DA604AA7E5814533E36F6F7BB2263D5EE27,
	ConvertUtils_HexTextToInt_mC7D7166598DD76A6794E4B833FD5DDD9C59ADEC0,
	ConvertUtils_HexCharToInt_m849061D4FE85890E633A0CC2DC7980BE21E39DF4,
	ConvertUtils__cctor_m7614D53BF4BD500E428C7DFB054047312CD4E49F,
	TypeConvertKey_get_InitialType_m0F65A2432EB92CAA1377B113C1BAEF2C0283F634,
	TypeConvertKey_get_TargetType_m8F2288FCB1DB8039CEE75810A72EC31B0085805D,
	TypeConvertKey__ctor_mAE2DD8A2A907D194638D666DED0AAC9FF3AA9E48,
	TypeConvertKey_GetHashCode_mC12EC356438B1840646DBA4ADEB7F6EB4AFDE894,
	TypeConvertKey_Equals_m4DE5600EBFEAFAA076537C2075A96573AB87D2BE,
	TypeConvertKey_Equals_mCCE90DDED2DF5DA439CDA3277A1BBB36E6E94C13,
	U3CU3Ec__DisplayClass9_0__ctor_mEE0F27194D15971B9E25917E60E41FD430A3E92B,
	U3CU3Ec__DisplayClass9_0_U3CCreateCastConverterU3Eb__0_mABC83D0C63CE3EBCE00AA99596C34DECE06E40A6,
	DateTimeParser__cctor_mB2B30EBF44E082346347244B34D188E60294558B,
	DateTimeParser_Parse_m64820EE59B4BB49B549346281AD8B2EF330B3EDD,
	DateTimeParser_ParseDate_m1A8649D480733E2FFD4F24827B2FC6C951C069E7,
	DateTimeParser_ParseTimeAndZoneAndWhitespace_mD103AD070AF0E15C3783EA88A21A1C71732FEF70,
	DateTimeParser_ParseTime_mB6558BEA3B81EB444F3FF78CD7288979EF071D20,
	DateTimeParser_ParseZone_mC630CC51579EADDAA8D595F081F89E5FEB7977CE,
	DateTimeParser_Parse4Digit_mFE4C2AE33BC9C3EACC29964487AE45CB86D66DDA,
	DateTimeParser_Parse2Digit_m7D1B3E711C5BB1704A70ABB399E0711AF064234E,
	DateTimeParser_ParseChar_m510297FF2814F39BC1DF2AFF91D2556AE81B7E5A,
	DateTimeUtils__cctor_mE72C26CEC9865469DAE34645B975F197730E78CF,
	DateTimeUtils_GetUtcOffset_m1B6B70139EF050BAA434164D316C6619F5FE292A,
	DateTimeUtils_EnsureDateTime_m0BE481EB6EB9238EAA99E20D546F6CAE3B17FF58,
	DateTimeUtils_SwitchToLocalTime_m8DE52AE6DB4497048A98A93249103BE9A91BAAA1,
	DateTimeUtils_SwitchToUtcTime_mE273FFC22EDB60C70E63BEDFAC09A2C554020073,
	DateTimeUtils_ToUniversalTicks_m54ABC31DF2B3FFD7F10963F2AD95EBA4B7CEE434,
	DateTimeUtils_ConvertDateTimeToJavaScriptTicks_mD5FEACA25BA3D250F62431EBC820D6D00C977D00,
	DateTimeUtils_UniversialTicksToJavaScriptTicks_mF0C4425E4D7CE537E5DC6F8FF978F18F39D956B4,
	DateTimeUtils_ConvertJavaScriptTicksToDateTime_mF6B797819AA97A8D120DA8B618675C262893D3B8,
	DateTimeUtils_TryParseDateTimeIso_mA0B1BA11F3E8A10ECDEDD2FAB44B20EBFB3C5072,
	DateTimeUtils_TryParseDateTimeOffsetIso_mBBCC805DB6E217123EA673ECCB13A4FBE96DFC9A,
	DateTimeUtils_CreateDateTime_m18FFB80C04E2A3D29E55ACC64A8B4CB9353401A0,
	DateTimeUtils_TryParseDateTime_m5E3D0A12556D749DEFCBDC464D5AE04E7298412F,
	DateTimeUtils_TryParseDateTime_m2A7376C659FC164B24EDD3EEA336056765F9600F,
	DateTimeUtils_TryParseDateTimeOffset_mB6E14078F4C19D99EA8194F0F99489B8F966799A,
	DateTimeUtils_TryParseDateTimeOffset_m258BE951D1F277E472D9B925EA5F5BAA4130FB5C,
	DateTimeUtils_TryParseMicrosoftDate_m573C612CCA75A9E28B55ED65217D5A1982C94F07,
	DateTimeUtils_TryParseDateTimeMicrosoft_m53F8407110F4954EB4D59691C0E0C1A5A66ED1A0,
	DateTimeUtils_TryParseDateTimeExact_mB79BF99C8BB9AB29D718B28ECB2964C528859795,
	DateTimeUtils_TryParseDateTimeOffsetMicrosoft_mAB8502A5A077608518332A134C6673D7C431B106,
	DateTimeUtils_TryParseDateTimeOffsetExact_mAB0710EB3CBD048B6AD588D848966FE735728185,
	DateTimeUtils_TryReadOffset_mFCB0549B55FE27DF5528E259E74FEFBA955548A9,
	DateTimeUtils_WriteDateTimeString_mD6F5858607DDBD5ECF1CB53F37EBB34983690BF6,
	DateTimeUtils_WriteDateTimeString_mBF8858BAD38FC107952363A787955E262183EE84,
	DateTimeUtils_WriteDefaultIsoDate_m610D74BE2F21BD196EA98F6343C52F711FEAF2F7,
	DateTimeUtils_CopyIntToCharArray_m9BBC30E960FE31739EBC40F34C433D9F518E2960,
	DateTimeUtils_WriteDateTimeOffset_mAEC50C298C3B5660F2F5B633CFF95116A26F0B63,
	DateTimeUtils_WriteDateTimeOffsetString_mEB8E69D99F7742268043030B7957EE386541BF5A,
	DateTimeUtils_GetDateValues_mAD3C75D7B22E067AF08D9263744E186FE2754A48,
	NULL,
	EnumUtils_InitializeEnumType_mDD8D7AB87FBF4367C4E1FE7843F8829E9DE70D89,
	EnumUtils_GetValues_m8CF47AF13AE49364AF5DCBFF0BA95B1210774BF3,
	EnumUtils__cctor_m155775286EF750EEA58BB59F603C804858BD1A07,
	U3CU3Ec__cctor_mCA6120DDC165F9A19952B593D0502A71AB4398AE,
	U3CU3Ec__ctor_m9257036A860F5D9D46AC71795DC0DC889CB519C3,
	U3CU3Ec_U3CInitializeEnumTypeU3Eb__1_0_mD95ED21242B7DCD2DD87E0715E473DEE541CAE02,
	U3CU3Ec_U3CGetValuesU3Eb__5_0_m643A368F4C078D3341829711A08DDE21CD2A3681,
	BufferUtils_RentBuffer_m4571362D52D6FA4B460A644F8FFA669102B842D1,
	BufferUtils_ReturnBuffer_m8CED72FDBE1C197A91AE32EC7C89D2A8B94FFA4D,
	BufferUtils_EnsureBufferSize_m8E73CC83B564A1BDB7B35413EDDCFB31D4636A30,
	JavaScriptUtils__cctor_m7DAA61408E54EACC10FBE09BAAE48B8F02CC31EF,
	JavaScriptUtils_GetCharEscapeFlags_mB93D0DCF197FCE0EE44262104BA6E28B6519A1AA,
	JavaScriptUtils_ShouldEscapeJavaScriptString_m2931D6D41D46BA432BC90CC41203B8C426E46DEA,
	JavaScriptUtils_WriteEscapedJavaScriptString_m92CA93738F12AEEEA91D9BCDA3D8477FB6470373,
	JavaScriptUtils_ToEscapedJavaScriptString_mEAA8877D408DCC207591BD1C32CA3173F7ED6549,
	JsonTokenUtils_IsEndToken_m173F228C3F92E2A7CFBA1B850DF8500E89B3C25B,
	JsonTokenUtils_IsStartToken_m27A34B7AF41C6EAE33E4886A571ADDAAFD7EFE65,
	JsonTokenUtils_IsPrimitiveToken_m64FF2DA221A7E6D74E41202E713FB60986928F57,
	LateBoundReflectionDelegateFactory_get_Instance_m11EAE301361278E5C016F812D62BD96E0D25C2E5,
	LateBoundReflectionDelegateFactory_CreateParameterizedConstructor_m0EC460EFA424E8D27D295A005E42C4ED9A5D3D0D,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	LateBoundReflectionDelegateFactory__ctor_mA7C3C8B28E78B37EECA0093CC8664E3C9FB2B1BE,
	LateBoundReflectionDelegateFactory__cctor_m5E969F84987B438B4EF8C701D3DAD008309BF8D9,
	U3CU3Ec__DisplayClass3_0__ctor_mD2FC00D4390F4313D4BB7BDA84E21AEEC8E30905,
	U3CU3Ec__DisplayClass3_0_U3CCreateParameterizedConstructorU3Eb__0_m3B6E3896DB55426A7BCBA2E3F4798B1B3CA9F572,
	U3CU3Ec__DisplayClass3_0_U3CCreateParameterizedConstructorU3Eb__1_mA1EB9CF9119962212F08D067D713E1E43822B606,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	MathUtils_IntLength_mAF606419E1B2F6FBF74FA80BD2353BFDF89A38B6,
	MathUtils_IntToHex_m47CB9EFD50D3ABF2C2F3E9E19F2B1438197B6286,
	MathUtils_ApproxEquals_mB7B83D6DA9CE8CE87ABB8637399194B3C0C3E477,
	NULL,
	NULL,
	NULL,
	NULL,
	MiscellaneousUtils_ValueEquals_m64A26242B2CABA1D40898CA9B0E5BA463741C147,
	MiscellaneousUtils_CreateArgumentOutOfRangeException_m8DA9EF647BCB6D06005220A4F0CEC41B2D60E772,
	MiscellaneousUtils_FormatValueForPrint_mFFFDA40B3A3DA1DBD8F875A305D995A430BC700F,
	PropertyNameTable__cctor_m39B778A3FB109A7512A6CF8C1C6249FA7DB7B2DE,
	PropertyNameTable__ctor_m7E8E7514D4B08FFA34E27EF2E799831182EDF361,
	PropertyNameTable_Get_mAC5F5741F65A64E907D0782C465D0A4B32A34C0D,
	PropertyNameTable_Add_mF3AA8BCAE6D4D3D1695B7D5DC2935AE4FCB77C61,
	PropertyNameTable_AddEntry_mDDA03E128909E6747F0B65F4DC484E9CDCD897AC,
	PropertyNameTable_Grow_mDDA55289A49DD5448ABF932426763DF683F26D95,
	PropertyNameTable_TextEquals_m383571D165562A381F13461D9F5DF6B2AF05916E,
	Entry__ctor_m6EA60CD174E302820D6EE496A08E9D047D936F8F,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	ReflectionDelegateFactory__ctor_m28F603C6DB820BD86A756B313F5CC3BD2FDE6A4A,
	ReflectionMember_get_MemberType_m6B83E257F42DD1C53558CD81BE74005F43794860,
	ReflectionMember_set_MemberType_m5909AE0B8D67C06D8069E6FC9676EFBEAE0F3CD9,
	ReflectionMember_get_Getter_mCF5EE2696804BA8D7E7A2DB16B09840656FF9D35,
	ReflectionMember_set_Getter_mB7C5D68B17069E9B971342D29BBCE72CE33F91CD,
	ReflectionMember_set_Setter_mD573E9151D6A27AA511FA936A47E43A25BD33DB7,
	ReflectionMember__ctor_m98CCF5ED06AF462AFAB7B18C5804AAE9F5629D1B,
	ReflectionObject_get_Creator_mC6AD5773E9CC38F72E1282A59B462E45FCE60164,
	ReflectionObject_set_Creator_m1E3FB878C8548FA0D022F47BB82426A110FD394A,
	ReflectionObject_get_Members_m58FCDAAA8FDBE6913E756A598748FC9856C1A8EA,
	ReflectionObject_set_Members_m17AFF4F18816B21290FBAC6AAD3D45D8FA7FFF82,
	ReflectionObject__ctor_mD864AF2C633A9F94BE30D5A8F01F203EBA680AC9,
	ReflectionObject_GetValue_mC3AE87D89C1510B1BB780F58D712F3E3F8AA51DF,
	ReflectionObject_GetType_m40AA4CFE2CA888C20AA1F8083A3F25D17D464DF8,
	ReflectionObject_Create_mF57E2BF8F217D0B124CE7B0B5C4A80E173279EBA,
	ReflectionObject_Create_m3E13A4F93F3B71B995104BD0FEBB204830B2545D,
	U3CU3Ec__DisplayClass13_0__ctor_m194BBD26BC7DA32EC4879BE571E60D14A58AB2C8,
	U3CU3Ec__DisplayClass13_0_U3CCreateU3Eb__0_m1916C10C0CB08965E2A5FDA9C7927E1D792DD06A,
	U3CU3Ec__DisplayClass13_1__ctor_mC40724E6465DF77E0B9F1AA336696A8A7532950D,
	U3CU3Ec__DisplayClass13_1_U3CCreateU3Eb__1_mCCC4412A143E54E9BE84A5F07B6669851E1F5568,
	U3CU3Ec__DisplayClass13_2__ctor_m04577715D78911D1017E05613577AD31B44FC8E1,
	U3CU3Ec__DisplayClass13_2_U3CCreateU3Eb__2_m0C4B4C711AD85E71827E280A4AE041B65E6F7CC1,
	ReflectionUtils__cctor_m38781904F86ED6216EC00807E69F489E04F54B50,
	ReflectionUtils_IsVirtual_mDAB826DEBE824D7B3DED6ACE9F582D37ADBDECD1,
	ReflectionUtils_GetBaseDefinition_m1495E37E8666896240BC6A5904F061950A1783CB,
	ReflectionUtils_IsPublic_m94B2E4FC561ACEC657A0B430479ECCCBC7A6FFCF,
	ReflectionUtils_GetObjectType_mC88354B6E2E7A667030B8AC78D8F0715C83D73C5,
	ReflectionUtils_GetTypeName_m20CF3039B0C1FDF3C40915E6FBAB1DD5C9B54607,
	ReflectionUtils_RemoveAssemblyDetails_m3E7EEA0520EFA20398CC10217E6271558F988C91,
	ReflectionUtils_HasDefaultConstructor_m90A5CAF6B571D87B227C0A6E371E6DD76C95BD27,
	ReflectionUtils_GetDefaultConstructor_m1E576437D830C074746C3F21EB2C223603CB84E2,
	ReflectionUtils_GetDefaultConstructor_m0ECDE909BEADC23A79948893BD9AFE4BC2EA9F50,
	ReflectionUtils_IsNullable_m7027B69E0FEE51A4B7D4881069F9DEB8537C07BC,
	ReflectionUtils_IsNullableType_m087178E090D00C75843BB2F93A7CAA414FEFAFB6,
	ReflectionUtils_EnsureNotNullableType_mA0D0761F8427803196C446A08267DFFE9FA84687,
	ReflectionUtils_IsGenericDefinition_m8E49178573A6D6B940FAA2772355261CE7D97665,
	ReflectionUtils_ImplementsGenericDefinition_m377CF88EF82FF1E10335FF74C984B4B9245DD02B,
	ReflectionUtils_ImplementsGenericDefinition_m5C3C97154DE1303207DD359EAE64A00B3533A8AC,
	ReflectionUtils_InheritsGenericDefinition_mC249D71C553B57BA04ABEBD54463959581C89C74,
	ReflectionUtils_InheritsGenericDefinition_m0E35465B169193FF20D72965EEF1EE7CD6529A8E,
	ReflectionUtils_InheritsGenericDefinitionInternal_mC94DF59F57D5A8C232DB38ABE39CA8567BF86A0B,
	ReflectionUtils_GetCollectionItemType_m9C5B96AD4C1BC8FEBD23A09C4CF6E162DD8F0F91,
	ReflectionUtils_GetDictionaryKeyValueTypes_mFF98FCA2C8DCF09C87F941D31F3705B44F594A06,
	ReflectionUtils_GetMemberUnderlyingType_m369A749B80A046506D574AFBB1DA224B610DAAF3,
	ReflectionUtils_IsIndexedProperty_m1A60AB81E965E535B27CDA1FFF29DEB3C755033D,
	ReflectionUtils_IsIndexedProperty_m510414BA1376369ED3B0E9F3C4A4D9A54A768E04,
	ReflectionUtils_GetMemberValue_mE5AF9C55EFB2EF538341A06DE8839EB2138B66E8,
	ReflectionUtils_SetMemberValue_m1E2CC0C74BC61975558464CEC2E6B42F4F16ADD1,
	ReflectionUtils_CanReadMemberValue_m2CA3EDD4A8D77B8755BC39203EC2CC199E68FC28,
	ReflectionUtils_CanSetMemberValue_mCBC362D65D00A3CE3541660FC8767893E95E39FF,
	ReflectionUtils_GetFieldsAndProperties_m0EE6098D13F78E595B6E77237CE20DC6C3038B32,
	ReflectionUtils_IsOverridenGenericMember_m23C99D639780A73CB8C8D8B8FE38B13BF286F38C,
	NULL,
	NULL,
	NULL,
	ReflectionUtils_GetAttributes_m2E4ADE14EA1DD97E34839435060F77446A601E9A,
	ReflectionUtils_SplitFullyQualifiedTypeName_mE4BA0DF98772645EDE016EA34BFEED01B7BFA047,
	ReflectionUtils_GetAssemblyDelimiterIndex_mF133EECC700C82646C566FF8ADE571171A56D125,
	ReflectionUtils_GetMemberInfoFromType_m524B7C4E43AEFAA69AD2569ED76F9FEEDA234AC8,
	ReflectionUtils_GetFields_mC8557AC07C431D1AA3B540954BDE9DFDDEEEB427,
	ReflectionUtils_GetChildPrivateFields_mAF6C30229F19E90DCB8DD58D73A2D4998A823633,
	ReflectionUtils_GetProperties_mAAE0D4CB04861AD2DBF93423D1D38241D21A5F6A,
	ReflectionUtils_RemoveFlag_mC2CD5A61EDBBB52F4D3CA5DB138800195E89F8E3,
	ReflectionUtils_GetChildPrivateProperties_m1FCFADAE9A7D08A43786DEAF720C4623F7C95CB2,
	ReflectionUtils_GetDefaultValue_mF81B1B14955A1F076958EA87A8D0171566630E07,
	U3CU3Ec__cctor_m79E2F51886AC0D350C73A7341E570086801F28F1,
	U3CU3Ec__ctor_mF109C838FBEC05DBB2F0E89B8543BE1E18D1F902,
	U3CU3Ec_U3CGetDefaultConstructorU3Eb__10_0_mEBAC516691175B5FD7C7844C45C45898D3478DF6,
	U3CU3Ec_U3CGetFieldsAndPropertiesU3Eb__29_0_m183FF91D6BBD2D1F05B4E57E690128FC298B35B1,
	U3CU3Ec_U3CGetMemberInfoFromTypeU3Eb__37_0_m871095E1FABE62782EA72E595F1E18E758F07850,
	U3CU3Ec_U3CGetChildPrivateFieldsU3Eb__39_0_m0D7F2247B937D9C2120BD8779A685032A08FFD39,
	U3CU3Ec__DisplayClass42_0__ctor_m41469F7DA375544E91F8CDCE9BD78BBDA4D3DC82,
	U3CU3Ec__DisplayClass42_0_U3CGetChildPrivatePropertiesU3Eb__0_m5380F6591C2980BEB7CB9E82A691AAC2D17A5CE3,
	U3CU3Ec__DisplayClass42_0_U3CGetChildPrivatePropertiesU3Eb__1_m87A8496C6B2964587C139987D42A5D22EB605FCE,
	U3CU3Ec__DisplayClass42_0_U3CGetChildPrivatePropertiesU3Eb__2_m222C3A1773447DE952CDB65AD6316A411747C3E5,
	StringBuffer_get_Position_m424CA0E4A0274918489C5986D189CB6DDEA6F11F,
	StringBuffer_set_Position_mE931269D642EEE14E2D84EEE1F0706536D65401F,
	StringBuffer_get_IsEmpty_m3B5208A8E035C3BF103F48C4BD666BA7D16C7682,
	StringBuffer__ctor_m658802150D63B715C3470D6B2C0170551ECB7DD5,
	StringBuffer__ctor_m80178B6D27B163F78337AC8C7BA4D6026696FA83,
	StringBuffer_Append_m0FCC4FB58E15B226B908B153D32045BBFF1D76F0,
	StringBuffer_Append_m603D9B2C24029588D05093561F805BC76C4DF0EA,
	StringBuffer_Clear_m56DC59C94CAF5F3A5830C14E2E38F0C205F78610,
	StringBuffer_EnsureSize_m76D11C8EED77ACD0299390A98CE072C5F2FE9EC0,
	StringBuffer_ToString_m10B8FF428EAE38BBC65EEC7809ADC392532B5927,
	StringBuffer_ToString_m88EAF73086605E7A0C898D962CA6B5CF8A6B0F07,
	StringBuffer_get_InternalBuffer_mF41DB26CCDD88E4ACE7B65840ADB74BF5EEE748A,
	StringReference_get_Item_m34C3ABEC482DD4EC5541D0A56EC05D05F669B0A0,
	StringReference_get_Chars_m5EF63176FDB3554674E394261BE5786170C47024,
	StringReference_get_StartIndex_mB09EFAA44487DB78CF15BDF61052689823A4C2C9,
	StringReference_get_Length_m194CFDC809C99311F4040820C9F4A3B5E5F77EA4,
	StringReference__ctor_m3AE890FED005EFC9FF5CDF3C7F740E743168B616,
	StringReference_ToString_mFE493A141754B54CA6B7DC1903730740DAB2A881,
	StringReferenceExtensions_IndexOf_m0521F7953B075BB955750D6AD8053D53D7ECD6AC,
	StringReferenceExtensions_StartsWith_m6D9020568686A182328C1F71CFC1800A870CFEB3,
	StringReferenceExtensions_EndsWith_mD6F3B9EEC429EAB4F6E05E69EECF814DDF3F764B,
	StringUtils_FormatWith_mF78320AE4049E77D6DDEC01680F7D98C44E2442D,
	StringUtils_FormatWith_mEB092C5B96EC84FDC050432B1A40DF8A83BFA2E8,
	StringUtils_FormatWith_m4FCAF6C2F661AFA5DECB79DDD2F4C1C63933AC02,
	StringUtils_FormatWith_mFFF18C22B96311F0FF31E7711F474AB79C1D96EF,
	StringUtils_FormatWith_mF72FDDA3EB515E398CA2D73E829FC7F8AD4F1D2B,
	StringUtils_CreateStringWriter_mECD1F544CCFBE0B6981309D5820C726B5BA7D629,
	StringUtils_ToCharAsUnicode_mB1A7A9E1E08E5D6C9FDDB3503C94A0478BCD7A83,
	NULL,
	StringUtils_IsHighSurrogate_mF2BB7B9F8B5B5AE318D47AC53DA3DF1F4D759542,
	StringUtils_IsLowSurrogate_mFB8EFC08BA665DD434D3752CFA6642393EFEA55D,
	StringUtils_EndsWith_m66FA0222A273889E4E523E10DE65E833F5536F58,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	TypeExtensions_MemberType_m6B0C77E72F48DC1B4204737359510A81F3E464C4,
	TypeExtensions_ContainsGenericParameters_m291B9B1787BCE6E8D410828890476C7E868ED823,
	TypeExtensions_IsInterface_mC86FEECE611B133413962A5EFE5DC85B92D76381,
	TypeExtensions_IsGenericType_mD7A79EF6D299F4D40932AEA3CF8BC3DD8E0C06D2,
	TypeExtensions_IsGenericTypeDefinition_m16881F2378C11CF6BFB7B3A127643F4129293AF8,
	TypeExtensions_BaseType_mB412285DD088CA76F89534B78D1AE65FFF30F754,
	TypeExtensions_IsEnum_m3D3EA9AF4638A68716201C314A1C75C0D0FE6CAA,
	TypeExtensions_IsClass_mF1942239F1A4C2B093A05F39F7F8F7D94EE34A1D,
	TypeExtensions_IsSealed_m9BF5CB41B9F60342CE4634AA4DE803492059844F,
	TypeExtensions_IsAbstract_m3E114BBDB2B178E154B5AD58D0C203A51BE091F0,
	TypeExtensions_IsValueType_mCAEDF96A86663613DC39781684AF92CC1531DF2B,
	TypeExtensions_AssignableToTypeName_mD522101DAFD0E49737C49A7BE5162B7D23312CC0,
	TypeExtensions_AssignableToTypeName_mF0CE3AB37FDF6B916128D0B9735D8ABD7CC649A4,
	TypeExtensions_ImplementInterface_m7183F3D7AF62459483F1E37786034552734BE904,
	ValidationUtils_ArgumentNotNull_mA8AC1AD3B50DBCFC06ABF784BB1311468771A495,
	NULL,
	NULL,
	ResolverContractKey__ctor_m5C7C616DF53AB9D3EE81A19395431BA9B0A3EB9C,
	ResolverContractKey_GetHashCode_mCF78DF3E9A1EB11DACA737FA5A306F172A8A0D6B,
	ResolverContractKey_Equals_m7EADD44C28175D677F9881E4D0021CF8936E0732,
	ResolverContractKey_Equals_m7F6E1B33EEBDE4971264A88E6662D9CDDE4CFC18,
	DefaultContractResolverState__ctor_mE28AAD3451BC7C048695944EC126C36024326631,
	DefaultContractResolver_get_Instance_m90FDF8E006FCD7E71EE5D54185A6BFAD04684F8E,
	DefaultContractResolver_get_DefaultMembersSearchFlags_mA6F171D6825AF07E7AC585C5B89B8B1BA57FAEC3,
	DefaultContractResolver_set_DefaultMembersSearchFlags_m89CA59C8A801BB1C3023BAD591A244271051A248,
	DefaultContractResolver_get_SerializeCompilerGeneratedMembers_mCEC95D8DC060D61621A326CE8DB8B6D1769BB0CC,
	DefaultContractResolver_get_IgnoreSerializableInterface_m10A32A92DCF98D1EDD209D880F17359001FD0125,
	DefaultContractResolver_get_IgnoreSerializableAttribute_m1429E466AFA1212DADA0B9223A04C74BD5DCA8CE,
	DefaultContractResolver_set_IgnoreSerializableAttribute_m54DC842D81DEABFD3198F00DDEBF83E1EF2C6936,
	DefaultContractResolver_get_NamingStrategy_m5C917ABAD194876A14B94BF7FD9D5879527A842A,
	DefaultContractResolver__ctor_m1D763AE5E45591246C4D6647F7102D131422D3BE,
	DefaultContractResolver__ctor_mCC6008697D963A3DD69FD7F4D94D3EAA5A9F6036,
	DefaultContractResolver_GetState_m18601177DCDAC796883BA5DFA043E8EFC0184D03,
	DefaultContractResolver_ResolveContract_m6A394909C187327992FEECEEB186CD9554120DFE,
	DefaultContractResolver_GetSerializableMembers_m8CC788F3C44419695C20618447DD8B3F57988BDD,
	DefaultContractResolver_ShouldSerializeEntityMember_mB90D5F9FAF703C796A362AC332132F1A12DD20F1,
	DefaultContractResolver_CreateObjectContract_m6AA964F38C7F57543CCE1257AE4088CCB7C6A0EA,
	DefaultContractResolver_GetExtensionDataMemberForType_m1F4CE56A95386EBF1C08D38E2A63634439EAAD7D,
	DefaultContractResolver_SetExtensionDataDelegates_m54A4E0A44B6DAC1466FB14F81CA2DE70182D6000,
	DefaultContractResolver_GetAttributeConstructor_m010BCF1898D26643560E364CC040C11F3289F05B,
	DefaultContractResolver_GetParameterizedConstructor_m54EA6C46871CCAA7B8626090134D6E07616B1CD1,
	DefaultContractResolver_CreateConstructorParameters_mAE76A97CB17E7C3B3E139202D7A13F23C35BBE31,
	DefaultContractResolver_CreatePropertyFromConstructorParameter_m26EEB6ED59FB3FDC8CA40D4471A9D5090D316D02,
	DefaultContractResolver_ResolveContractConverter_m23E1780FD23E01BFABA06F322B9D601ACE46B896,
	DefaultContractResolver_GetDefaultCreator_m8D927E78AF0F223216F58B223F0B3179EEC99927,
	DefaultContractResolver_InitializeContract_mD8156F1B6C83C65F429C4BBC8916BF2391CE11A7,
	DefaultContractResolver_ResolveCallbackMethods_mC27F18AF4B4006DA0CF2B3CF04B6DFD595F9E55B,
	DefaultContractResolver_GetCallbackMethodsForType_m22B6B6BBDA8F434AB03BA7C5BB52F3FFC8051B55,
	DefaultContractResolver_ShouldSkipDeserialized_mC687F862EDFB6D3B7DA18984B3201CBCB3B32312,
	DefaultContractResolver_ShouldSkipSerializing_mEDA1B821A3F7C8A45200A77A02ABCBA16A130771,
	DefaultContractResolver_GetClassHierarchyForType_mACD438778C3967B9C596B1748418E1D1B85C15F1,
	DefaultContractResolver_CreateDictionaryContract_m7D68ABF113D252A9F4D07931E0D26F75CFFDF740,
	DefaultContractResolver_CreateArrayContract_m55811CDD16D1153B33D20EE9D7BF1C90D8AB8707,
	DefaultContractResolver_CreatePrimitiveContract_m905EE823DC54E8453E0B08AF0A152ED957434868,
	DefaultContractResolver_CreateISerializableContract_m285FDAFC72C62C89DA24506B12BAB14D10D565D1,
	DefaultContractResolver_CreateStringContract_m0FF8D2CF443C24B639563092357BE3F23B484B3F,
	DefaultContractResolver_CreateContract_m8FE88E99A02200C59EE2AAF2E84D57B20A3AC4DB,
	DefaultContractResolver_IsJsonPrimitiveType_mB267C86CE80DD697664A01771EE4BD85C6478CF4,
	DefaultContractResolver_IsIConvertible_mFBEFC1C86B9A2B94D8CFA8770BAA3022AD413E6A,
	DefaultContractResolver_CanConvertToString_m0B0E02CFCA83FD471FD9FEC098454FFB4AEEF3B3,
	DefaultContractResolver_IsValidCallback_m926057B8F2EBEE755CE8904CC0722EB4466FC02A,
	DefaultContractResolver_GetClrTypeFullName_m874A85E4D811BEB8F07F8AA427CA76E435AEDC68,
	DefaultContractResolver_CreateProperties_mC30AD58E60D7A798C17AB357CE5A0ACDF0C0E150,
	DefaultContractResolver_CreateMemberValueProvider_mBDA94C8EC44BD0ACEA482BC2ED53387AB2EA672D,
	DefaultContractResolver_CreateProperty_m770806DA14683F330DC280DF6695D2D5E794BDB6,
	DefaultContractResolver_SetPropertySettingsFromAttributes_mC3D54B83E29C703B94A7EC4CFB629956937D1DD9,
	DefaultContractResolver_CreateShouldSerializeTest_mEEF774DB36FC2319E4FEAAA5D17155DC9D7E8515,
	DefaultContractResolver_SetIsSpecifiedActions_mD5B4FC516A78292F6FA508C8F273E7348DA6533F,
	DefaultContractResolver_ResolvePropertyName_m2D677E560F07FC358844E4F61CF9B0EEB0755B84,
	DefaultContractResolver_ResolveDictionaryKey_m345C32D87B4BB0AC7C82A84279DAEDF70CD1A371,
	DefaultContractResolver_GetResolvedPropertyName_mDAF5D5500B14C083619BAC1C8656C018541DC91A,
	DefaultContractResolver__cctor_mCB187CBFD05F59F9AE5EAC55F54257999FD130BC,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	U3CU3Ec__cctor_mC6D9E94CA60CFC141FFC835625A409CFA85F4E47,
	U3CU3Ec__ctor_mE66D46B991808823CB3824E4B29BFA6C149E3738,
	U3CU3Ec_U3CGetSerializableMembersU3Eb__34_0_m65BB6F3B51232E8E2926903E2CEF175983AC3889,
	U3CU3Ec_U3CGetSerializableMembersU3Eb__34_1_m983D29D4AF1EA10B3ECA7BED337AEC11157DEF5A,
	U3CU3Ec_U3CGetExtensionDataMemberForTypeU3Eb__37_0_m318BBA49484CCF1794EEE5417693A55717838349,
	U3CU3Ec_U3CGetExtensionDataMemberForTypeU3Eb__37_1_m828226D1FB4AA70347E063378F6BF4064E7002D3,
	U3CU3Ec_U3CGetAttributeConstructorU3Eb__40_0_m38369B1996FCCE4B636E39DDC6A86D92FD3A9B50,
	U3CU3Ec_U3CCreatePropertiesU3Eb__64_0_m9D1FE6CBD5DC4C760920F028871456CF1ED36668,
	U3CU3Ec__DisplayClass38_0__ctor_m9657B517275E07644EA8F2780E8530AB46582701,
	U3CU3Ec__DisplayClass38_1__ctor_m480F13BCCC0D3EC2A81ACBD4DC1D40A0D14A006B,
	U3CU3Ec__DisplayClass38_1_U3CSetExtensionDataDelegatesU3Eb__0_m1351516A62F1D2E6BBD3950DCE4699F3F427F103,
	U3CU3Ec__DisplayClass38_2__ctor_m3B6A995ED361FD637F5C890F6A330DB29C642C46,
	U3CU3Ec__DisplayClass38_2_U3CSetExtensionDataDelegatesU3Eb__1_m49B9CB881ECC01B15E43254D46A5692A06D2D2D4,
	U3CU3Ec__DisplayClass52_0__ctor_m0E4A38D649056BACFD2EABC64E065902BCA5AB1C,
	U3CU3Ec__DisplayClass52_0_U3CCreateDictionaryContractU3Eb__0_mD31CCB9C2E5FCCD990199DF086122D9EA0607600,
	U3CU3Ec__DisplayClass68_0__ctor_mE7807E8FF0E1599649F65AF657A2B02F90E18829,
	U3CU3Ec__DisplayClass68_0_U3CCreateShouldSerializeTestU3Eb__0_mA20EFBB3085AFE25FE3DA66E0ADD0A770E158F1D,
	U3CU3Ec__DisplayClass69_0__ctor_m828A909BC5D34B88E20B77CD87E7ED09ED12EBDD,
	U3CU3Ec__DisplayClass69_0_U3CSetIsSpecifiedActionsU3Eb__0_m8E32452942E950B4073E43EF6D9292CB07E4A382,
	DefaultReferenceResolver_GetMappings_mC93AD74A9E72769593AC962549A995F502CB03EC,
	DefaultReferenceResolver_ResolveReference_m8090D406BAE4BE95231765B7BC8211AB00CAE209,
	DefaultReferenceResolver_GetReference_m070DCB3C0946C06186E8F944CAAC520A003169F1,
	DefaultReferenceResolver_AddReference_m98949E37CB315CC7E2E79647A164D49792B2E054,
	DefaultReferenceResolver_IsReferenced_m2F6C90F4E3CC680BB0BEB8F1B89166F51C109FE3,
	DefaultReferenceResolver__ctor_m1BB6EC923095F4B46B37EEBEAE3B8BD3D85198E2,
	DefaultSerializationBinder_GetTypeFromTypeNameKey_mE3EA2E940C753D1755172E66B6D39377020AC4BC,
	DefaultSerializationBinder_BindToType_m988DE7A8EEDC0732A87703F23BC85ECB426FFCF1,
	DefaultSerializationBinder__ctor_m8836533BB55F5E0F9B11B6322FC8B153FD1B1F64,
	DefaultSerializationBinder__cctor_mF0026829E183636EE3EC682FA11168FF52938FFA,
	TypeNameKey__ctor_mDC4275014619C46DE6BB5BE66BEEEA2962D1DDCD,
	TypeNameKey_GetHashCode_m6F14E3CA5E0098A41949DDA45EC588069B2F7A4A,
	TypeNameKey_Equals_m2508626D8FED8CA829246608EE62F8E2D34B5AA5,
	TypeNameKey_Equals_m4114698DBBE4F208BBD13645CF9DF3500A5B2F6E,
	ErrorContext__ctor_m37814E73790A84177A3CBA0F04B6C29F170AFA4A,
	ErrorContext_get_Traced_m2600308CFFECA1B408B14E621987926BBB1799C5,
	ErrorContext_set_Traced_m8BA7FD9673E83668AB685995DCF399BFFF77A21E,
	ErrorContext_get_Error_m4BA9AE89A1463A8187BCAA2081B10A7EE412B4B8,
	ErrorContext_set_Error_m3C47A4456A7D207FCB0AA8DBDFC0EACE349ACB27,
	ErrorContext_set_OriginalObject_m2D5A6F54F6DBB717BFEFFE41ABFD5F8FF944C8BA,
	ErrorContext_set_Member_mA60560017299B78E3269E2E30E178D46CD52DE5E,
	ErrorContext_set_Path_mAB5E68DCF6CFD684635470CEE002AFBA2ECCA6DF,
	ErrorContext_get_Handled_m00CB858B5DF909C699425AA6A1EB383ABA527158,
	ErrorEventArgs_set_CurrentObject_mE2464018DBF50495E7AC91C004C29BB678F408EC,
	ErrorEventArgs_set_ErrorContext_mD68107F654E6143A90BA4187E54E358ADA9F3EEF,
	ErrorEventArgs__ctor_mBC42EEE6BE59E2670574EE359B63B14E374232AE,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	JsonArrayContract_get_CollectionItemType_m3CECF4280B1768FDEF6A7111741C93DE7F7DDF22,
	JsonArrayContract_set_CollectionItemType_m459F1B1BB37AA94FC3386F62D51E850E3D3B9654,
	JsonArrayContract_get_IsMultidimensionalArray_m882BEE069C4D30998786CDEF57704C966CA33E62,
	JsonArrayContract_set_IsMultidimensionalArray_m85FD29E6E2FA63221F8CC1150E97D5D53016C251,
	JsonArrayContract_get_IsArray_m72D6C22445B07CFE617D61656301BEAB4C39CAD8,
	JsonArrayContract_set_IsArray_m7F43AFECAC00B630DBB56A94DA548141448AF781,
	JsonArrayContract_get_ShouldCreateWrapper_mE8AEA40AFF96BA8EA72B04C1FC377E98A5F21086,
	JsonArrayContract_set_ShouldCreateWrapper_m437A0E5C1DE254AFB886EB246A854E775EA12C72,
	JsonArrayContract_get_CanDeserialize_mBCB4583F10A61CBDC6C60CA4D88D07366CEE9583,
	JsonArrayContract_set_CanDeserialize_mCA795402C7F20B1704011379FF99BFDBEA09946E,
	JsonArrayContract_get_ParameterizedCreator_m6BE2BE1919355F38BCA0E64EACD9F42EA028E9F0,
	JsonArrayContract_get_OverrideCreator_m73CB534700A57A13B8D02F6170FF8506C6280884,
	JsonArrayContract_set_OverrideCreator_mFD5EEF176B2E1C97BF6CBC050BFC6DD8F0857E55,
	JsonArrayContract_get_HasParameterizedCreator_m4BC9037E6219B65657C06880A51DE9B22B2F38B7,
	JsonArrayContract_set_HasParameterizedCreator_m85CC98F0D51C11A1570956CC7319BE4C21AAA962,
	JsonArrayContract_get_HasParameterizedCreatorInternal_m3C21C185FC5FD64814F6A5087FF1EAB9A8FBEB9E,
	JsonArrayContract__ctor_mA86B976D1A8C684E2D95D734631E8C5331CC7298,
	JsonArrayContract_CreateWrapper_m60F6BC00877480B1179F57162ED7974F246B1B9A,
	JsonArrayContract_CreateTemporaryCollection_m6B57285CB312925F2B0E951982023A55F9A830FC,
	JsonContainerContract_get_ItemContract_m463F7BCD86D434C87FB39B8AA51E75F527347CAF,
	JsonContainerContract_set_ItemContract_m48A45754147BCD70811C5CA691445F5B396A1AE0,
	JsonContainerContract_get_FinalItemContract_mB64876030157CE20B3AB40938213BD49EFDD5121,
	JsonContainerContract_get_ItemConverter_mCC1FF5CD91BE1433DE49D71A5000D148A247D07B,
	JsonContainerContract_set_ItemConverter_m22A233E3F1FEFEE00C494880802E72B9CE364301,
	JsonContainerContract_get_ItemIsReference_mFF08D7B8117AD227F7B655F89690250790FCED61,
	JsonContainerContract_set_ItemIsReference_m976F9ED94F787C37541FFE9CFCD2687AB7152CBF,
	JsonContainerContract_get_ItemReferenceLoopHandling_m4497940C72F5F38E574A4D180D8D2CD919A812BC,
	JsonContainerContract_set_ItemReferenceLoopHandling_m90E562BE972F1EF5478B7ED2703E27F5BE4ED27D,
	JsonContainerContract_get_ItemTypeNameHandling_m805E6E8EB10B640579EF897F4A74E566D80F4498,
	JsonContainerContract_set_ItemTypeNameHandling_mCA66B3AA082975A16A7DA94DB8F18722F5369447,
	JsonContainerContract__ctor_m276B184509C72D124EA627FFD5877FA5D4707A54,
	SerializationCallback__ctor_m6390FE6B107CA5EAB4EC582D0E814888EC3680CA,
	SerializationCallback_Invoke_m98C5D43F75883EA2EFD0B8E5DF2B3996A1A3D820,
	SerializationCallback_BeginInvoke_mF0BA12B078DD0258912A68952DD078100F2A2FC7,
	SerializationCallback_EndInvoke_mA939F9CCA8AD61FF04550DD641140185A89100EB,
	SerializationErrorCallback__ctor_m11C9CA3B46DB9BD502DF841CE81F601DDC2683EE,
	SerializationErrorCallback_Invoke_m1A623FEF15281AAF20963A8725839BFC98E1FA64,
	SerializationErrorCallback_BeginInvoke_m99B5D1C187200AD2A22155C76FA0B53730CA44B3,
	SerializationErrorCallback_EndInvoke_mF47C19FD1740E216FA2A34D9689B9F0520864D93,
	ExtensionDataSetter__ctor_m5E3910D1EDD285D8A810AE6E0259DB1ED750F33F,
	ExtensionDataSetter_Invoke_m0932F6E9B6CEBA1BB5AE16DCBCD3968A76061162,
	ExtensionDataSetter_BeginInvoke_mEC6C7BC0462B31CAE86B47A03959AEC34676792A,
	ExtensionDataSetter_EndInvoke_m3CC583BF62EAD752BE77B44756E219BFE19CF10D,
	ExtensionDataGetter__ctor_mD4E6092AD33162E3FAF8C58BD0CBA698CA2152B4,
	ExtensionDataGetter_Invoke_m4F90DCC828CF287792A0B884359B5AA86EE1F96D,
	ExtensionDataGetter_BeginInvoke_m02765E41942A104693DC26C6344F8E18AA3FC75C,
	ExtensionDataGetter_EndInvoke_mEA0F820536DC1D475F0F4932E773D132916BA8A6,
	JsonContract_get_UnderlyingType_m274B4718678409B2511BBE941FF2D647CA533DC3,
	JsonContract_set_UnderlyingType_m82F3DEB8DC8339C6CB32187651D87CE330DE13AE,
	JsonContract_get_CreatedType_m083775257FF46EBCD6F1CD5F2195F8A8E4F9ECBF,
	JsonContract_set_CreatedType_mCC0F045EE40A673F6567490F79A9BA9C57F5498A,
	JsonContract_get_IsReference_m85AD9382BF6556FBB63A383D1ECA6BC6C5C369A6,
	JsonContract_set_IsReference_m21803C0B2D772F7C3EED60332E2E2CA69D6D54F3,
	JsonContract_get_Converter_m92C6064189D577AA5D08771BE755B3BFBCB297A9,
	JsonContract_set_Converter_mA7452FEC775D9674F8163B299C151249DC7E544F,
	JsonContract_get_InternalConverter_mB36C4060D4251DE1F51662B767D2779C40C65BCA,
	JsonContract_set_InternalConverter_mAB2F562DCD689CC7E50B4BE84AEE73E2995D782F,
	JsonContract_get_OnDeserializedCallbacks_m51D3705D21A5D46F5C44CD2AEC69D35FB805E88B,
	JsonContract_get_OnDeserializingCallbacks_mA7864AB96D49B6FC9E2CFBA9EBEBD9B82D95C63E,
	JsonContract_get_OnSerializedCallbacks_m669138963D8D20031F8F005C94AEB4CD7592F7DD,
	JsonContract_get_OnSerializingCallbacks_m4EB4397D6E4F21716A1DDFB5F9BCD107EF04AB08,
	JsonContract_get_OnErrorCallbacks_m1B7A5E421C1FCB7CF207CBBD0C3BC5E0EB1058FF,
	JsonContract_get_DefaultCreator_m8271898AFA4F8DBDC5A018F79FD4EC5450839B81,
	JsonContract_set_DefaultCreator_m0D4E0BD379C8342A4BD6C6D73F19BE330526F825,
	JsonContract_get_DefaultCreatorNonPublic_mFEDABA69CB6CA3FDE1FB163D1192C74307F2A15F,
	JsonContract_set_DefaultCreatorNonPublic_mDCCE7A1E9DDED83BFFD22F33BC6E660F70CB4FEF,
	JsonContract__ctor_m735CB01EA1C75415DEC0BCD297F9622B94477F78,
	JsonContract_InvokeOnSerializing_mE71754D61053B520E71AB5333F329969370553FB,
	JsonContract_InvokeOnSerialized_m50B6E71E04D2AA8A1253102A0C7EDFB1B5123385,
	JsonContract_InvokeOnDeserializing_m963E996327E2AE515165D915543B20799FB865AA,
	JsonContract_InvokeOnDeserialized_mB59C124B20738ECDBA8CEE41C7B5E567BB945C4C,
	JsonContract_InvokeOnError_mEB2325BA5AFE7EEC24B6C84E6F14985A74F95D66,
	JsonContract_CreateSerializationCallback_mA43D137D94C5B97C73A7589A19C4873CB7E21128,
	JsonContract_CreateSerializationErrorCallback_m2D1541916958EDA3A52FACF50A026112EEDC94D6,
	U3CU3Ec__DisplayClass73_0__ctor_m327FFFA0BFB55C64621A1600416F6051D2CC8F2C,
	U3CU3Ec__DisplayClass73_0_U3CCreateSerializationCallbackU3Eb__0_mE55246604A2198788812A1B4BA3B191EB3103BD8,
	U3CU3Ec__DisplayClass74_0__ctor_m32174118CAC919A99277D1670F6F5A45B2A5DE11,
	U3CU3Ec__DisplayClass74_0_U3CCreateSerializationErrorCallbackU3Eb__0_m4F0A8CF50687635531818316C9BE6CF622827EF6,
	JsonDictionaryContract_get_DictionaryKeyResolver_m9FEC62269B460CBE840125B2C29F98F35FD18568,
	JsonDictionaryContract_set_DictionaryKeyResolver_m7DBA6B179284AF2B20F35EFD0A53D1C1A7A1338E,
	JsonDictionaryContract_get_DictionaryKeyType_m955BFAF792651E88B651BD2C619097726DCB76FD,
	JsonDictionaryContract_set_DictionaryKeyType_m6B1EDD598D5F7E159443731827F01450C44B11CB,
	JsonDictionaryContract_get_DictionaryValueType_m45B9DDEF68FCAAD1D2FD90391817AF79A2915922,
	JsonDictionaryContract_set_DictionaryValueType_m05D9BED20D4CA80764E423E027F66DB3A0818E18,
	JsonDictionaryContract_get_KeyContract_mC7BAD25BBB5DF24FA9FA1B704FB9153A1526EB8B,
	JsonDictionaryContract_set_KeyContract_m522C30EF20C09B50E617E24698E0BE1D84033EFC,
	JsonDictionaryContract_get_ShouldCreateWrapper_mD43CBCD1A26F23DFF9EF7F4069AA4BC99CE03BCC,
	JsonDictionaryContract_set_ShouldCreateWrapper_m81F5B13DB3FE9CC060B07F1943975289E930DBD0,
	JsonDictionaryContract_get_ParameterizedCreator_m709BFD1415EEBFC68DD858E1CF1A0213B63C0D1D,
	JsonDictionaryContract_get_OverrideCreator_mD200635EDEDDC59BAB34FA2372D037C2C7F07BD6,
	JsonDictionaryContract_set_OverrideCreator_m28EBE8929923BCACFB3A8322FF3A841F33368C52,
	JsonDictionaryContract_get_HasParameterizedCreator_m2AC31619D694866F47219F7228D781FDB7559361,
	JsonDictionaryContract_set_HasParameterizedCreator_m5C3555EBD3982F9FC072BDAF78014CC2C49F777D,
	JsonDictionaryContract_get_HasParameterizedCreatorInternal_m63D219D7A4FA312DAA9DC21DC5E1FB06D6E5DBE4,
	JsonDictionaryContract__ctor_m6D73B62F2292E1E876B950AE5AFD8D33E5D51366,
	JsonDictionaryContract_CreateWrapper_mDF0D8C2A477D8AF463BD24A2ABAE7B90611755BA,
	JsonDictionaryContract_CreateTemporaryDictionary_m0B22B816AF9C5B5CA42BC98A1EAEB486E95A40B0,
	JsonISerializableContract_set_ISerializableCreator_m5295CF43688CAD573D840026F52A80256EF73A24,
	JsonISerializableContract__ctor_m56BCC3490F830E1A520D20A155264E4D9DC676BB,
	JsonObjectContract_get_MemberSerialization_m927C496830ECEE969FEFCBD9015D2975B64EB7EA,
	JsonObjectContract_set_MemberSerialization_m80C86B38A9A42F0A2D096B74CFC52E8EE250175E,
	JsonObjectContract_get_ItemRequired_m75C6C4175B7C1473543857FFB985D37C39AD8C82,
	JsonObjectContract_set_ItemRequired_m322E068F6060AB63F5C6B2E61DC986925C674E07,
	JsonObjectContract_get_Properties_mD4B212D9A137993DE37F53402D769D0D905E3110,
	JsonObjectContract_set_Properties_mDC67F335196A048B9DF12F878BAAE0B20328314B,
	JsonObjectContract_get_CreatorParameters_m2537D42265D44FCD577D1FE3A3AFD91048356123,
	JsonObjectContract_set_OverrideConstructor_m235F3E2A09CB30F758F80DE69FF0A8449737C2C5,
	JsonObjectContract_set_ParametrizedConstructor_m569FE50A49972AC16601C46A9ACDAB0F83DD1242,
	JsonObjectContract_get_OverrideCreator_m9BE981868AC8BA4316074FC143FA8DC9F2911275,
	JsonObjectContract_get_ParameterizedCreator_m53CF73CCDCB1F71922CCABF43F8A95C1FDA3B41F,
	JsonObjectContract_get_ExtensionDataSetter_m2DFB59696B0D14849D0F4E443DD82AA5A76B243C,
	JsonObjectContract_set_ExtensionDataSetter_m39A79B22B522DB922D27C30377BD082401A787B6,
	JsonObjectContract_get_ExtensionDataGetter_m80664EEE79810B192D96E02FCD344B03EE7ACF35,
	JsonObjectContract_set_ExtensionDataGetter_m52A1E838E07D96D828C5BB724E77BE2F17313980,
	JsonObjectContract_set_ExtensionDataValueType_mB3DF262A7425DBC2F147CA65A6CC3BAE00CC7E0B,
	JsonObjectContract_get_HasRequiredOrDefaultValueProperties_mAF97B1C6E6C1BDA031FB5EC9C626C864ADBC44D5,
	JsonObjectContract__ctor_m75B16514965E3A3BF86D3BBB9F4656D424927116,
	JsonObjectContract_GetUninitializedObject_m9E5A10E243C300FCD8448D31E3B0B3B18F890AC4,
	JsonPrimitiveContract_get_TypeCode_mEED6CF3DA675CB2F244C3E02209F37FC8887C544,
	JsonPrimitiveContract_set_TypeCode_mD01C4FD88E81143FD4FE9446D08E10CBED5D16CB,
	JsonPrimitiveContract__ctor_m3E1FB6F5CF9442C0BDA4516FE390E1F27A37D5B4,
	JsonPrimitiveContract__cctor_m2349BE67983674123BCEC8BD6CDCEBF55C87E511,
	JsonProperty_get_PropertyContract_m1A07AE508ED64BF7DAFD6B768CAB11E8F07AC218,
	JsonProperty_set_PropertyContract_mA51FB41ACB2877BDA0005FFB84D6724AD6269B3D,
	JsonProperty_get_PropertyName_m72817401FC680F87EC4202862CD5B52CF6300C60,
	JsonProperty_set_PropertyName_m99CCA5636B8B8F8FE8F7100266FC4830FD8DA846,
	JsonProperty_get_DeclaringType_m0A64AF3ECCED18859E78E63E3A264AAB74586322,
	JsonProperty_set_DeclaringType_m4FA21E3ADA7905038A4B8410922A4D678DB627BD,
	JsonProperty_get_Order_mEF3E149DB2B59B8CA34317D3C09DFF703F31EF81,
	JsonProperty_set_Order_m360E9E66EB70C639663D3A29A37CD4FFB9217F56,
	JsonProperty_get_UnderlyingName_m8204307F227D4B3D9701133A47172F819B08219C,
	JsonProperty_set_UnderlyingName_mCD3B44976B4DFFD01B148B7D7A88AE92A71F6C2F,
	JsonProperty_get_ValueProvider_mB9CD0B24D58FCF1EE82DA3A3935973CD560C971B,
	JsonProperty_set_ValueProvider_m95563EC88EE32866DACCC6C2AE8F0FE656A5D847,
	JsonProperty_set_AttributeProvider_mC95FBFBE2AA39AD182245F28F860C0E77A2A7FD2,
	JsonProperty_get_PropertyType_mA61CDE8B995125100D36BA65D8112CA75961F019,
	JsonProperty_set_PropertyType_mC3FA4BCD6DD37EA6F7E8AB42312F70A76C294484,
	JsonProperty_get_Converter_mD78430E0A86FFCAD9077CA45A8515433C1A7ACAF,
	JsonProperty_set_Converter_m09958BDD926D75B7CCBA933BE95D63DF69DE2831,
	JsonProperty_get_MemberConverter_m4AAF8854081CD744699C3A0BAF9314E817FDDD43,
	JsonProperty_set_MemberConverter_m02E881B46A470612898913FEDB7568219E7254BB,
	JsonProperty_get_Ignored_m46548BB8982F27C3B6FA9874BB36BBBE93C5BE5C,
	JsonProperty_set_Ignored_m633FB01A6826E5E0A0AD5EADA49C0AAB4FC61943,
	JsonProperty_get_Readable_m7AEAE72E61CEE2D7204DF7DDE4BB46FD49ACF13A,
	JsonProperty_set_Readable_m200607F2CB3518230B19840B81318784938634C1,
	JsonProperty_get_Writable_mD0AD78FD1A02462591351FA144C3A3E3F3D71C04,
	JsonProperty_set_Writable_m1EAC194E3A9B4958ADBC14262F398F126E22669D,
	JsonProperty_get_HasMemberAttribute_m8D7C6BB1CB98AB72EFA491333E9BD9DD9A8EDD4B,
	JsonProperty_set_HasMemberAttribute_m39D8D4BE4DD54068759166CF360FF13F27479EC6,
	JsonProperty_get_DefaultValue_m022CCDE65C84390EDB18D613BB1D6639825FCD36,
	JsonProperty_set_DefaultValue_m83E7EE87272CAA2FAB10BE523817E5D6C248F64C,
	JsonProperty_GetResolvedDefaultValue_m2F663D0C4330BFEF8954915CD2804D47A20DE6A9,
	JsonProperty_get_Required_m047BD7FEDA7B029D9E4E1C7190CDD4BAA66F95AD,
	JsonProperty_get_IsReference_m153CA8FA5C547CC6B05661BE83264D4C965B9396,
	JsonProperty_set_IsReference_mB2D4152E154865A7D98A41608BB5DF115514B3C1,
	JsonProperty_get_NullValueHandling_m42B929FF14E1D403D8DCBEE4C71937234A81ACB1,
	JsonProperty_set_NullValueHandling_m0DFFF6AA9EB90290031B2A8AA34B5F4860650701,
	JsonProperty_get_DefaultValueHandling_m07D9A4D362F431188F9B92F069FA06522F8313AE,
	JsonProperty_set_DefaultValueHandling_mC9F3205408913D8B21FA525CCDAC7AD032E71EDA,
	JsonProperty_get_ReferenceLoopHandling_m36303DDEA2267BA0C5AEE6F04C31376769E24DA2,
	JsonProperty_set_ReferenceLoopHandling_m6CE09B4B487EDFDF5D4ADF5B9750278478C1728A,
	JsonProperty_get_ObjectCreationHandling_mFD14FC71400EAA2ABE2953F94D8ED12B9C5EA137,
	JsonProperty_set_ObjectCreationHandling_m049C8E802C5BED6F45FC9BC69F6FCF492F3AC05D,
	JsonProperty_get_TypeNameHandling_m8AEB537470EE0ED5BFE82E69FC9351EFFF39E20D,
	JsonProperty_set_TypeNameHandling_mFF34196E4C937D5CB8C7BFAC959C9EDBABABD42B,
	JsonProperty_get_ShouldSerialize_m3DEC6D4BDF9DD0467C9496E31D064F1D584131AD,
	JsonProperty_set_ShouldSerialize_m691B604D63F062A266B8AA0FA42CE22A52D1C80A,
	JsonProperty_get_ShouldDeserialize_m5E69820CB6EF8CBCA0B173519DB4B32C7D6B3F26,
	JsonProperty_get_GetIsSpecified_m2D32E7F92B46871C97992E41AB0BADCCDE3715A3,
	JsonProperty_set_GetIsSpecified_m69FEAC27F917FDA7B479399D6127C5A8BFDFD1A8,
	JsonProperty_get_SetIsSpecified_mB3A71DEAD82264AA63FB0A0E53E85DAB582AF220,
	JsonProperty_set_SetIsSpecified_m43AD00A6F292CE4CD5DD72CF33B5AEB186BC115E,
	JsonProperty_ToString_mE92D7E46DB796643D47E57BFDD6398730839873E,
	JsonProperty_get_ItemConverter_mF43A1CB4574AACC026916EB46DD3665428AD9E9E,
	JsonProperty_set_ItemConverter_mE132EF0F991D7A67FF1CC4B190F3A20680A74484,
	JsonProperty_get_ItemIsReference_m4FB1049E629A8E0C7532E7F42B5784838B641FE0,
	JsonProperty_set_ItemIsReference_m5520B121F62448B77C3005291868E4A6C363F04B,
	JsonProperty_get_ItemTypeNameHandling_mE9229CB08007B7C54F5FB9E1E100741102577526,
	JsonProperty_set_ItemTypeNameHandling_mB26022D26C0E5DF718210F7F91150A8AC42E79A6,
	JsonProperty_get_ItemReferenceLoopHandling_mCF84F2423F3441D4FF3A55CA0EB91A3C13F7C20E,
	JsonProperty_set_ItemReferenceLoopHandling_mA482C2F7321AB96C932D7FD5A3C4FBF6283E9B16,
	JsonProperty_WritePropertyName_mA063F926230E98D32E25F607C1844D680E902F7C,
	JsonProperty__ctor_m0C17B59D12EFA92F9504D5EAB99DCEF61D2A021D,
	JsonPropertyCollection__ctor_m10AF88EAE4E37DD81F6985739229547D87855F26,
	JsonPropertyCollection_GetKeyForItem_m74EE6C0E7D1BFEE99644E7BBA97E163E55384670,
	JsonPropertyCollection_AddProperty_m55D0AC4383A26BC34CAFE2577450F02260002BE2,
	JsonPropertyCollection_GetClosestMatchProperty_mC56AF3BDDFA034EBC09EDB347BB1A61B7C3E03B3,
	JsonPropertyCollection_TryGetValue_mA7A8A893D9A31EA6D12639E323991E183105B810,
	JsonPropertyCollection_GetProperty_mD5A03BC6B22DDFA6C4175238DCE4C697C0850F04,
	JsonSerializerInternalBase__ctor_mF661DD347384ACEA2E8D0F8772FDE3FCE318A040,
	JsonSerializerInternalBase_get_DefaultReferenceMappings_m0AD1C4B7991FB623BC3284D35984092D300EC513,
	JsonSerializerInternalBase_GetErrorContext_mD795A0D91081E591F9260CE33A3AE297FBD1FF7D,
	JsonSerializerInternalBase_ClearErrorContext_m0DBFE7B75E7D0828B2A411205B35C091608B3299,
	JsonSerializerInternalBase_IsErrorHandled_mC5DE21D3927A503792F8AE5962BC965BB4689E85,
	ReferenceEqualsEqualityComparer_System_Collections_Generic_IEqualityComparerU3CSystem_ObjectU3E_Equals_m9C19FB863E99FB0E8F684911A1A1B0AFAF67AA34,
	ReferenceEqualsEqualityComparer_System_Collections_Generic_IEqualityComparerU3CSystem_ObjectU3E_GetHashCode_m959D403ACF199A3EB9C7EA6769AE2A2B394FF44A,
	ReferenceEqualsEqualityComparer__ctor_m0D20F7AF62826C7CA9086E605EBD9B8585C07952,
	JsonSerializerInternalReader__ctor_mA15E63A6DBAEC12311813C5EA19737B00F87129B,
	JsonSerializerInternalReader_GetContractSafe_m133F75523E811E79248B64D1E180030793C00A8D,
	JsonSerializerInternalReader_Deserialize_m7A8F42D36257DAD366236258CFAD83426DE44E84,
	JsonSerializerInternalReader_GetInternalSerializer_m1916C24804A7B3BA115F09D72FDC7197634616CA,
	JsonSerializerInternalReader_CreateValueInternal_m3EC2690B80CB81A28115170993AFE08907AA16BB,
	JsonSerializerInternalReader_CoerceEmptyStringToNull_mFA8BAE9EEA76DABEF2FAAE26BB49FC8BD02A9C4D,
	JsonSerializerInternalReader_GetExpectedDescription_m7160A0626A80CF5F80E565D705F3495D0731D556,
	JsonSerializerInternalReader_GetConverter_m2F2488670D8CFEE6E8C15B3B594705361AFA8C2E,
	JsonSerializerInternalReader_CreateObject_m92A6187A99FAA790FDE4E5613CC79165CC1D0E42,
	JsonSerializerInternalReader_ReadMetadataProperties_m9BE7A38FF267C65A82D7B77C701534793F82D188,
	JsonSerializerInternalReader_ResolveTypeName_mFD4144F8CE8539087E30792ED22CECA87FC5BD1F,
	JsonSerializerInternalReader_EnsureArrayContract_m9BC600DA8AB0E77E7EEB608805D841C403D5509A,
	JsonSerializerInternalReader_CreateList_m205E8682EFD6B5E81D3694813CE4056F8F843A2F,
	JsonSerializerInternalReader_HasNoDefinedType_mE8C3693A8F7B03259665691EC4E35782A1BFDEA2,
	JsonSerializerInternalReader_EnsureType_m039985AE8A056134EE7998F2B369A212CBFDC7F3,
	JsonSerializerInternalReader_SetPropertyValue_mF9395418926887BB8E007F238CA4C017F5C7984C,
	JsonSerializerInternalReader_CalculatePropertyDetails_mE8FE25A1E556F355B0E3348120E106453B2F772A,
	JsonSerializerInternalReader_AddReference_m26BDB86BA9303743D16411E7A9B75DF9C8900D91,
	JsonSerializerInternalReader_HasFlag_mF3538606BFF79804E64F15A436CFD853676E21F0,
	JsonSerializerInternalReader_ShouldSetPropertyValue_mB87EAB4DA3B7A332CE88038FEA94A9EAEAD556BB,
	JsonSerializerInternalReader_CreateNewList_mD65C2185CA2E698B9A53FB00F367170E7F7FBE35,
	JsonSerializerInternalReader_CreateNewDictionary_m3395D04EDC8B10A5803D5AFED9E7EBAE5EC90E07,
	JsonSerializerInternalReader_OnDeserializing_m5A2F1A8A0126871AC41EB8767E18F38603004775,
	JsonSerializerInternalReader_OnDeserialized_m9603B44FD17BC2F0C15AF6A407097C7C9376D24A,
	JsonSerializerInternalReader_PopulateDictionary_mD14FE13DF213661A8FCBA06FB7C145E7336C09A5,
	JsonSerializerInternalReader_PopulateMultidimensionalArray_mC47FBA8A46AB0FF8C6489270C784C50BC25CF4EA,
	JsonSerializerInternalReader_ThrowUnexpectedEndException_m05CCBCD2F5FFC11D32C3E8CAE34B7231562700ED,
	JsonSerializerInternalReader_PopulateList_mFCFE2F29D66EF5B96160573D7E0565C8E77CB7CF,
	JsonSerializerInternalReader_CreateObjectUsingCreatorWithParameters_m732582D4C8F53A416C5F47CC5B90667E4C26C7CC,
	JsonSerializerInternalReader_DeserializeConvertable_mA54865AF80F0207BF6F4DCAA7282FAAE7C7A13EC,
	JsonSerializerInternalReader_ResolvePropertyAndCreatorValues_mD97877C5DDC4819C7473C73F4BA973A6F1982AE3,
	JsonSerializerInternalReader_ReadForType_m7A4CD8CDCE496D22DF7FFE49749F4DA2FAF33A88,
	JsonSerializerInternalReader_CreateNewObject_m3376CD7A093DA0440AABBC5AF7055A57558B60B3,
	JsonSerializerInternalReader_PopulateObject_m09807E53DD11A8886863F54EF38F276508E4CFDB,
	JsonSerializerInternalReader_ShouldDeserialize_m03544CC59E193E3328248C20ABB9E3C9E7F48548,
	JsonSerializerInternalReader_CheckPropertyName_m3651505C873DF94E56E5A6473AE4A5A322396D08,
	JsonSerializerInternalReader_SetExtensionData_mEE71B5B95C6D65AAB92A10253800EE7362E923EA,
	JsonSerializerInternalReader_ReadExtensionDataValue_m7C52479883C81B92FC7206283AE53E85327F3ED3,
	JsonSerializerInternalReader_EndProcessProperty_m5779521285FF3FCA14A3B8A145BE289EEEB81C18,
	JsonSerializerInternalReader_SetPropertyPresence_m64A239C9F207366C579F2779BEDEBA2A72FA176B,
	JsonSerializerInternalReader_HandleError_m8F99D22C1F0B18119C6957394310D4E535CDCCF2,
	CreatorPropertyContext__ctor_mB81D9D361C71DAE2AAB86DB1F791AD259D0674C4,
	U3CU3Ec__DisplayClass31_0__ctor_m5632FD9B61D2888DFADC0EF8B8A8B04D46FF7BB3,
	U3CU3Ec__DisplayClass31_0_U3CCreateObjectUsingCreatorWithParametersU3Eb__1_mE93AE83B0F3042FE741E836CC5C6E719D182FA77,
	U3CU3Ec__cctor_mB684917E51C88F503F61405714BD76633F355FBB,
	U3CU3Ec__ctor_mF4F6C6B34C47B9F6E948DBC0A421CE0D2D9FE54C,
	U3CU3Ec_U3CCreateObjectUsingCreatorWithParametersU3Eb__31_0_m77B222BDA92C5E53BB37296128EE435BACE70C5D,
	U3CU3Ec_U3CCreateObjectUsingCreatorWithParametersU3Eb__31_2_mEBDE616CFA8FDA5E6545636E1E71CC0F7E7E0D78,
	U3CU3Ec_U3CPopulateObjectU3Eb__36_0_m374EB5D2938C8599E51751DB03A1C23F7979B5E9,
	U3CU3Ec_U3CPopulateObjectU3Eb__36_1_mAB3EE0F153328A00A83058CBE6AE8B6423D0C320,
	JsonSerializerInternalWriter__ctor_m1AFA8C1FA24A1A4EA510C59AECE65B149EDE9355,
	JsonSerializerInternalWriter_Serialize_mF69E647FFD576F1A40BBE6193F383B0417BD5BE1,
	JsonSerializerInternalWriter_GetInternalSerializer_m9B37BA0257BD0F4863114C0A3E90F1483A182825,
	JsonSerializerInternalWriter_GetContractSafe_m8D7527C6F95E6E8F9BFB664501251637E30F2C48,
	JsonSerializerInternalWriter_SerializePrimitive_m3C715BAF1C55D696DEFB8BB6F52CB7979F9236FD,
	JsonSerializerInternalWriter_SerializeValue_m757192B4598E695F87E7733815B124717BE82F0F,
	JsonSerializerInternalWriter_ResolveIsReference_m43AC53037DDDB97F4263DB53CA3C711F8E31CB64,
	JsonSerializerInternalWriter_ShouldWriteReference_m3D5F3A3348DE09B87C10215A21EC7867C50A888B,
	JsonSerializerInternalWriter_ShouldWriteProperty_mD57E393611046E8B54B3BBF989C331A57FD94F23,
	JsonSerializerInternalWriter_CheckForCircularReference_m4967876D9EFED00AB5B1F1EFE0AE8E575F5ECC8A,
	JsonSerializerInternalWriter_WriteReference_mD3C2F169D5F8DBEE5A6E63DBB480D0723D33BC5D,
	JsonSerializerInternalWriter_GetReference_mD7EC662C19A3426BC564ED9890E9D2E83B1F2B30,
	JsonSerializerInternalWriter_TryConvertToString_mE7CF555EB2CAB24CA1D859F69BBC55EC84D21E7A,
	JsonSerializerInternalWriter_SerializeString_m1BC7D0E71EE24187445A74A216A5CC5CCF8719C9,
	JsonSerializerInternalWriter_OnSerializing_m350FCA9D440F26F8BCCA4C3EAC48B23C1DEC9994,
	JsonSerializerInternalWriter_OnSerialized_mA1CBF9C633BC2AC8C827E9AFB704D9141E299200,
	JsonSerializerInternalWriter_SerializeObject_m2EB8F81333F0A7A0BA067F716172C0D8F5B66FE9,
	JsonSerializerInternalWriter_CalculatePropertyValues_m4DEA924FF49865E23E6C2362B363B3CEA1320216,
	JsonSerializerInternalWriter_WriteObjectStart_m2D1D6B3368417AFAFABBA4D872AA211990826425,
	JsonSerializerInternalWriter_WriteReferenceIdProperty_mDEF0C77110DA0AB92C793BCEE4BC9C9A7A0F1E46,
	JsonSerializerInternalWriter_WriteTypeProperty_mEF26BE0513326E64C2AF3D8F82CB172E84A20C83,
	JsonSerializerInternalWriter_HasFlag_m0F6B733C1D24AA5FEFA1471226AD7AF0E3C83CC8,
	JsonSerializerInternalWriter_HasFlag_m55EE1C0E1D06C50B5B303146BAABF5D5B684D142,
	JsonSerializerInternalWriter_HasFlag_mCEF1AB8FC23B3609315E52BB4731C62CBECE70D6,
	JsonSerializerInternalWriter_SerializeConvertable_m8C0D75BF2DFBAABBE49ECC078D66300BA7FFE9DB,
	JsonSerializerInternalWriter_SerializeList_m42784DB5EF17DEBC92BBF7DD50122DAB8B75D89A,
	JsonSerializerInternalWriter_SerializeMultidimensionalArray_mF82AC318E144799FB7F4FE021F9379CC48527856,
	JsonSerializerInternalWriter_SerializeMultidimensionalArray_m14B956BC4A5CFA954F0A401F08347347EEFC24DB,
	JsonSerializerInternalWriter_WriteStartArray_m66A8198BE4036F4AC986ED13DEFB748FDF906E49,
	JsonSerializerInternalWriter_ShouldWriteType_m2516242599D93E812B249A35057B4F07A0370DD9,
	JsonSerializerInternalWriter_SerializeDictionary_m29272E678DE06CB92D9CB467988FE70AF003F558,
	JsonSerializerInternalWriter_GetPropertyName_mD212731827B88B0BAA4AD5C083D2BEC615B4BDE1,
	JsonSerializerInternalWriter_HandleError_m1365318B9672FFEA57192F3E59BAA6426004380C,
	JsonSerializerInternalWriter_ShouldSerialize_m5FC940308CCEF6A4D0C24B5B13AF595B3C206FF3,
	JsonSerializerInternalWriter_IsSpecified_mD5AFC0F88045079C05C3491DD01DAA2F9C19EB0A,
	JsonSerializerProxy_add_Error_m5332EF6713169F2612F0D6F53F759D6D79930E87,
	JsonSerializerProxy_remove_Error_m05F707DD2814E2AA3505E19E3C07167154F19D3B,
	JsonSerializerProxy_set_ReferenceResolver_m3183CBB2D4AB47F676D9A2B16C577EEF52407054,
	JsonSerializerProxy_get_TraceWriter_m4B48C90B910658F7A75CB33CF2C265CFC180FF5F,
	JsonSerializerProxy_set_TraceWriter_mB386E800DF7CE2C22EC2A3AA54348159C0ADCF66,
	JsonSerializerProxy_set_EqualityComparer_m3FC82F0A23DB0367B883DCA413B1225B25ACD785,
	JsonSerializerProxy_get_Converters_m7F576773F805FE428EBC087D0CA67151DACD479A,
	JsonSerializerProxy_set_DefaultValueHandling_m620D5C0D6D767919A76AC47EAD7B215A508C2F02,
	JsonSerializerProxy_get_ContractResolver_mBC99F18C5B6A2C28F36AEAED75770A84F626E4B4,
	JsonSerializerProxy_set_ContractResolver_m85BF647755B9EF0A359660D4E9AB2FA7156AA0BD,
	JsonSerializerProxy_set_MissingMemberHandling_mDC155116044AD028B55952DBBA1CC02655CF6A62,
	JsonSerializerProxy_set_NullValueHandling_m22CA9B7C8631C58D49BBD81D90316AF42C010558,
	JsonSerializerProxy_set_ObjectCreationHandling_m5AF86DDDAADE15712DCB515C93F2E6BCEC49F200,
	JsonSerializerProxy_set_ReferenceLoopHandling_m5FB2D1EDA234E5A7E6EA1F94F956D3407A98187B,
	JsonSerializerProxy_set_PreserveReferencesHandling_m182AB3B9E497A6344B6721414631778B03D5194C,
	JsonSerializerProxy_set_TypeNameHandling_mBB52993FB72C8EA27759485A6E978157FB857F16,
	JsonSerializerProxy_get_MetadataPropertyHandling_m6DC75A09F1045C6CC6AD959A4A8231D21A0978FC,
	JsonSerializerProxy_set_MetadataPropertyHandling_m32113B3765BAB3C3407E72B10DF7E29DB2A509FA,
	JsonSerializerProxy_set_TypeNameAssemblyFormat_mDD85866973259A1326A37896E25B8CF0535CBA85,
	JsonSerializerProxy_set_ConstructorHandling_m150E9008C4BBED96D5D9C758CECD8DE0AD48020F,
	JsonSerializerProxy_set_Binder_m4A3EA675D117467F4DD5C00CD223E09E54604AC5,
	JsonSerializerProxy_get_Context_mD33F8DBF63D2AC665AB7FB3EB83101E34D6BC346,
	JsonSerializerProxy_set_Context_m8ADC56B4CD9666B64420B45E4E7B0EBDC8D3831A,
	JsonSerializerProxy_get_Formatting_m3539A08D0819B39C52BB1C268B037A9FE5F90B5A,
	JsonSerializerProxy_get_CheckAdditionalContent_mF713CC40ACC4654A11BE2779372517EFCD87FA3D,
	JsonSerializerProxy_set_CheckAdditionalContent_m2C74C094FFE58CFDA4EEF0419859CB80969AD224,
	JsonSerializerProxy_GetInternalSerializer_mA9C47B00437D61801910CFA99C97BC0D3CCB9AAA,
	JsonSerializerProxy__ctor_m42CE32C8F24EB13CBC947AD94381CAC11CF0EBDE,
	JsonSerializerProxy__ctor_mEBEAF9B2186D7C7DAFAA0F4B2EB90769C3D1B646,
	JsonSerializerProxy_DeserializeInternal_m232ECAC23E34C1C3B2B0E562BC1AD3827EC4C16E,
	JsonSerializerProxy_SerializeInternal_m0189768E4B5AFC15EFD47159A4741F477684DD46,
	JsonStringContract__ctor_m895EB8BF3664D947CAAABBDEDF08B79B0C81E668,
	NULL,
	JsonTypeReflector_GetDataContractAttribute_m5845752C4A046C85DA96A5D9B4FEC89D35DAC246,
	JsonTypeReflector_GetDataMemberAttribute_mB14D9536F26AB591B1B71906ECBBC7EF9BA3150D,
	JsonTypeReflector_GetObjectMemberSerialization_mA9F7123352A69D75136EF08D4076B4F677319B77,
	JsonTypeReflector_GetJsonConverter_m698F0AA36908AA5EB808E9AAD99388170B4B8322,
	JsonTypeReflector_CreateJsonConverterInstance_m0A1C1696369A50698204A0C0B74C5021EECA6D0E,
	JsonTypeReflector_CreateNamingStrategyInstance_m983469EB051584F0BE993F3B435CE83DCB954749,
	JsonTypeReflector_GetContainerNamingStrategy_mC935BD957093DF747AC70883829ED9B12007BC92,
	JsonTypeReflector_GetCreator_mD27CA71228B825164E78F2A6FAE899257BE4122C,
	JsonTypeReflector_GetTypeConverter_m106FD3E440CAA5076DC226EA10B16305144A3577,
	JsonTypeReflector_GetAssociatedMetadataType_m9372B1DA0BDB89701CC390F29DA0FEECA4ACE778,
	JsonTypeReflector_GetAssociateMetadataTypeFromAttribute_mBFBBCF6E816B7C9CE33F1321EF47150381629594,
	NULL,
	NULL,
	NULL,
	JsonTypeReflector_get_FullyTrusted_mBED536FA8A1687A54B609F1FCE1D0A37336CCC25,
	JsonTypeReflector_get_ReflectionDelegateFactory_mD5A0D3AA68CA62CCD31B219ED5B1C03C63AAAEC6,
	JsonTypeReflector__cctor_mF726C9808106B6CE44C99C1DEFB538DDE3697C09,
	U3CU3Ec__DisplayClass20_0__ctor_m6FF8C47EB6FB31F8952DA33F7B70A293F502A0E2,
	U3CU3Ec__DisplayClass20_0_U3CGetCreatorU3Eb__0_m987F7CF1E29CCD749193506951C9C0348D193BCB,
	U3CU3Ec__cctor_m57D7B5CC6C5C25588D21AA16B15B972D7EA17B16,
	U3CU3Ec__ctor_m10DDD7E3D143A479BA4ABEA1D4361ACC61E136E5,
	U3CU3Ec_U3CGetCreatorU3Eb__20_1_mC6037338F8D1D9AFA8755A32D4D207646963D8E1,
	NamingStrategy_get_ProcessDictionaryKeys_mC1D4A8A7BDBC0305DBAE29FFEA3FFAB83177E249,
	NamingStrategy_get_OverrideSpecifiedNames_m45FEC93C0AC20A53AEC9AD731A4D9B276AF26AF4,
	NamingStrategy_GetPropertyName_m6949AB856F0450C007F63B63484FE2372142CF6E,
	NamingStrategy_GetDictionaryKey_m0F48B97AF21AA7A066CDB8905D1605455B4E907A,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	ReflectionAttributeProvider__ctor_m3A03DE577A627DFBF44D58AA17E191E2C0FD0E60,
	ReflectionValueProvider__ctor_m4CCF1B6E29FAC08128AD2D0E980F29107FD29C19,
	ReflectionValueProvider_SetValue_mF3A83C542EB0F0D53ADE0E384A043DA559182421,
	ReflectionValueProvider_GetValue_m26EB30AFB2BDE690A4955BBFDC482CB2E67B4DE7,
	TraceJsonReader__ctor_mDF664DDFC7FF1739F887F50E0CE389D056EC382A,
	TraceJsonReader_GetDeserializedJsonMessage_m0AE711BAFA1A2B758D450B50F5992D2DE714EED7,
	TraceJsonReader_Read_mB5B1144C2D86F9AA7E2BE7F332ACA7342D916838,
	TraceJsonReader_ReadAsInt32_mDD9B61D65D8F0BFDC4D785CBD169A5C0B0E20BD4,
	TraceJsonReader_ReadAsString_mDB0A396A74EE34E81D9510752177369AD220B95C,
	TraceJsonReader_ReadAsBytes_mF34242EE6F6C15B4BCACEA72F6EC654279B8E954,
	TraceJsonReader_ReadAsDecimal_m2866C8E8D0D8A1786B23940FFF3550809322D130,
	TraceJsonReader_ReadAsDouble_mD59C86D67B8FC8AD056DFC9A7A7E55B9A1DCFB74,
	TraceJsonReader_ReadAsBoolean_m189B58C223EC61B7A0114228F697D07C2B376AD6,
	TraceJsonReader_ReadAsDateTime_m31E9688C8F20CB5F1A3517A80EF27920679DFD1A,
	TraceJsonReader_ReadAsDateTimeOffset_mDAEE1C904D638231A033EAC8DE603B42779AA3FD,
	TraceJsonReader_get_Depth_mE3AFA404FCA65349C5C5DCBC71F0F3770177DA3C,
	TraceJsonReader_get_Path_m1F5468102FAA80E5B8DB599FD71A5B30F0E0CA60,
	TraceJsonReader_get_TokenType_m3934A485BDEC03D4E11CFD340077E6B0DADBF6BD,
	TraceJsonReader_get_Value_mB8C60D4BBD15E4ECDBDE49556E9E07F55047F93E,
	TraceJsonReader_get_ValueType_mD2045D7D802F3759CB8DE2FB293AB76D3E16EF12,
	TraceJsonReader_Close_mDB33FA7E4687365BDD88D0AFDEAD0253C1A3D1AF,
	TraceJsonReader_Newtonsoft_Json_IJsonLineInfo_HasLineInfo_m90913CA01160A53528846997BDA02BBFEF36123F,
	TraceJsonReader_Newtonsoft_Json_IJsonLineInfo_get_LineNumber_m3BF75683A19B5FC29E3D5290B5B3C1FA134C3148,
	TraceJsonReader_Newtonsoft_Json_IJsonLineInfo_get_LinePosition_m7B4B018D9ECFC2AD388057DD8F0215AB21183D06,
	TraceJsonWriter__ctor_m29B26F2DC7065ED209EB7F1BD7454D085A7C1C41,
	TraceJsonWriter_GetSerializedJsonMessage_m5B843782EA9D7D098F54283024609893F47AC118,
	TraceJsonWriter_WriteValue_m9BF104BFCC9361F1F1177B7DDDC55A5D0994AC9A,
	TraceJsonWriter_WriteValue_m3E4B078F2B00FD5250754A6A66DC5973AD6CA60C,
	TraceJsonWriter_WriteValue_mAE9F6E644A236BD55983E01C96D04F2E8A92F3B8,
	TraceJsonWriter_WriteValue_m6FE35B7C9F77BDD538F59AE2C597F27E1E155426,
	TraceJsonWriter_WriteValue_m9707DAACD55CB7D657F0971CB43B9228DC55609E,
	TraceJsonWriter_WriteValue_m91400888E8FFDE94292C9F7B1FACEC38A92627B0,
	TraceJsonWriter_WriteValue_m0D48CE333EDDA2EEF769CB10E783215A805ED1F2,
	TraceJsonWriter_WriteValue_m93AC35D2DC22F752D361B42AB92E09BE043FB55B,
	TraceJsonWriter_WriteValue_m06D2926D295E1814C93DCB9468ADF5064897438C,
	TraceJsonWriter_WriteUndefined_m1D80B8C61BA78FE70E157994F1F461A43C6DA9D5,
	TraceJsonWriter_WriteNull_m1C114CBB5198BAD1346622F30F2E06536E6A67E7,
	TraceJsonWriter_WriteValue_m9CFECDBD50358F59CC88998F625E88D08137E25B,
	TraceJsonWriter_WriteValue_m8F557D0F46C8D9DFE5D77540E300E6653DBC2E23,
	TraceJsonWriter_WriteValue_m86B387D4711067F1A4BA39F1A2F0C2CF1FC4CD8E,
	TraceJsonWriter_WriteValue_mC1359523457AF58B6E45E73EB4FD7DC6ADB9514F,
	TraceJsonWriter_WriteValue_mCF70906D73F2F874C153CCF43176C7F270DCD534,
	TraceJsonWriter_WriteValue_mAB836F999AC85984937844DA3DB99EAFB593D291,
	TraceJsonWriter_WriteValue_mD0DB38AA9308B0E181BC2AEE2EFF513A75E2F4FC,
	TraceJsonWriter_WriteValue_m348A56226E7608135E82F3FCAC6F91D578C11C86,
	TraceJsonWriter_WriteValue_m3C731AE9FD7B5898C197719457A3F87C549BF43D,
	TraceJsonWriter_WriteValue_mCE1C9ED077BDA0E5DEEFD3E4A106C5A57EA90850,
	TraceJsonWriter_WriteValue_m3B0429ABD3FCB2E96E4BC4C3205D947C6F15B861,
	TraceJsonWriter_WriteValue_m516ABC62B17BBF4E077B78D574738B3955C51B72,
	TraceJsonWriter_WriteComment_m7B6A62C629652A13C48DB7BF8F9E99FB0E2F18FE,
	TraceJsonWriter_WriteStartArray_m62E5F20C404EA5F6B7C2F8BE8BCEB8D5CF3607CF,
	TraceJsonWriter_WriteEndArray_m4292FCDB4F4E922D9199500BED9C07888680690C,
	TraceJsonWriter_WriteStartConstructor_m8C1F7EA7AF1EE915319DD74B1FD74165F925FE7F,
	TraceJsonWriter_WriteEndConstructor_m82D598D7C412DF807185B9FC4C712CD69C005D34,
	TraceJsonWriter_WritePropertyName_m8E987A764C0769862EEE598BFF9EB5B03B571B64,
	TraceJsonWriter_WritePropertyName_mBA2E40CB1DAA827635B2FCDA37A66919E1507835,
	TraceJsonWriter_WriteStartObject_m2EE8F71A1FD5BDDEEFCAE0E5C3689C5DC8B24A30,
	TraceJsonWriter_WriteEndObject_m7417900A02D479036FB4FE283727929A303AE56E,
	TraceJsonWriter_WriteRawValue_m02839B921BF871F752B464D19C65D92389B1C8B1,
	TraceJsonWriter_WriteRaw_mF5798374D6F0A1AFEB8E3AEBEBA41B7171393138,
	TraceJsonWriter_Close_mD31D297A991DB57260137EF39AD6C641E1F8C852,
	BinaryConverter_WriteJson_m38F6F9B3820776D5E3F0D1C3534E4C65E257746B,
	BinaryConverter_GetByteArray_m0DEF709EDA659437BAE64675B81F7949A3BD9D80,
	BinaryConverter_EnsureReflectionObject_mD05B38334EFC9C8EC364BE76705BD3C16031452F,
	BinaryConverter_ReadJson_m3458DDB255892438725E2070C0EFCA940B384B92,
	BinaryConverter_ReadByteArray_mD8A936BF1CE0542DEB8A2A9D434C8D2CE7609027,
	BinaryConverter_CanConvert_mA6F5A4F148236D9944C7ED8C30B12B1C0E69EB23,
	BinaryConverter__ctor_mFD2281076324F20D92F44E5A19650FB5518F9F19,
	KeyValuePairConverter_InitializeReflectionObject_m06AF5C794B04A48B532F925A5D6576D37908690A,
	KeyValuePairConverter_WriteJson_m56A13234A85C715D67F54DEAD005F187CD494204,
	KeyValuePairConverter_ReadJson_mF8F822C2D404938169B5C3B5701C5AC1D85CEC69,
	KeyValuePairConverter_CanConvert_m211BB20B62F84C81DB1704B890B31525BC899BB7,
	KeyValuePairConverter__ctor_m4B1F9194CB861A99BF850097D012624E5807505B,
	KeyValuePairConverter__cctor_mF4627C8F3CADAA2911CE4940B677D0B5D79259B6,
	RegexConverter_WriteJson_m2DFD8C928EE8DF8C5F5B2142C1BD4FD725A3A278,
	RegexConverter_WriteJson_m3232C1B85602852FB040265237936D883586D344,
	RegexConverter_ReadJson_m670966D979ED51B085DCEBDFED7B53697D3B075B,
	RegexConverter_ReadRegexString_m8416EBC385B8AD4B3DB08E1437E37DCAA9FA9734,
	RegexConverter_ReadRegexObject_m305F9A55E4024BD86481052D19B398005B06C745,
	RegexConverter_CanConvert_mB8EE7F3824E272C0D5119E0004256A78500A00E9,
	RegexConverter__ctor_mA8551DA4763F879B008CAA285672515BACAE949E,
};
extern void JsonPosition__ctor_m3C4DB92D84AE311EE082E3D1C35558CF05969C28_AdjustorThunk (void);
extern void JsonPosition_CalculateLength_mF22E55ABE3DE00539186EC9136AF9FC493E0CCBD_AdjustorThunk (void);
extern void JsonPosition_WriteTo_mEFF7AE829F518C06937587D13B92A88F35FCC73D_AdjustorThunk (void);
extern void TypeConvertKey_get_InitialType_m0F65A2432EB92CAA1377B113C1BAEF2C0283F634_AdjustorThunk (void);
extern void TypeConvertKey_get_TargetType_m8F2288FCB1DB8039CEE75810A72EC31B0085805D_AdjustorThunk (void);
extern void TypeConvertKey__ctor_mAE2DD8A2A907D194638D666DED0AAC9FF3AA9E48_AdjustorThunk (void);
extern void TypeConvertKey_GetHashCode_mC12EC356438B1840646DBA4ADEB7F6EB4AFDE894_AdjustorThunk (void);
extern void TypeConvertKey_Equals_m4DE5600EBFEAFAA076537C2075A96573AB87D2BE_AdjustorThunk (void);
extern void TypeConvertKey_Equals_mCCE90DDED2DF5DA439CDA3277A1BBB36E6E94C13_AdjustorThunk (void);
extern void DateTimeParser_Parse_m64820EE59B4BB49B549346281AD8B2EF330B3EDD_AdjustorThunk (void);
extern void DateTimeParser_ParseDate_m1A8649D480733E2FFD4F24827B2FC6C951C069E7_AdjustorThunk (void);
extern void DateTimeParser_ParseTimeAndZoneAndWhitespace_mD103AD070AF0E15C3783EA88A21A1C71732FEF70_AdjustorThunk (void);
extern void DateTimeParser_ParseTime_mB6558BEA3B81EB444F3FF78CD7288979EF071D20_AdjustorThunk (void);
extern void DateTimeParser_ParseZone_mC630CC51579EADDAA8D595F081F89E5FEB7977CE_AdjustorThunk (void);
extern void DateTimeParser_Parse4Digit_mFE4C2AE33BC9C3EACC29964487AE45CB86D66DDA_AdjustorThunk (void);
extern void DateTimeParser_Parse2Digit_m7D1B3E711C5BB1704A70ABB399E0711AF064234E_AdjustorThunk (void);
extern void DateTimeParser_ParseChar_m510297FF2814F39BC1DF2AFF91D2556AE81B7E5A_AdjustorThunk (void);
extern void StringBuffer_get_Position_m424CA0E4A0274918489C5986D189CB6DDEA6F11F_AdjustorThunk (void);
extern void StringBuffer_set_Position_mE931269D642EEE14E2D84EEE1F0706536D65401F_AdjustorThunk (void);
extern void StringBuffer_get_IsEmpty_m3B5208A8E035C3BF103F48C4BD666BA7D16C7682_AdjustorThunk (void);
extern void StringBuffer__ctor_m658802150D63B715C3470D6B2C0170551ECB7DD5_AdjustorThunk (void);
extern void StringBuffer__ctor_m80178B6D27B163F78337AC8C7BA4D6026696FA83_AdjustorThunk (void);
extern void StringBuffer_Append_m0FCC4FB58E15B226B908B153D32045BBFF1D76F0_AdjustorThunk (void);
extern void StringBuffer_Append_m603D9B2C24029588D05093561F805BC76C4DF0EA_AdjustorThunk (void);
extern void StringBuffer_Clear_m56DC59C94CAF5F3A5830C14E2E38F0C205F78610_AdjustorThunk (void);
extern void StringBuffer_EnsureSize_m76D11C8EED77ACD0299390A98CE072C5F2FE9EC0_AdjustorThunk (void);
extern void StringBuffer_ToString_m10B8FF428EAE38BBC65EEC7809ADC392532B5927_AdjustorThunk (void);
extern void StringBuffer_ToString_m88EAF73086605E7A0C898D962CA6B5CF8A6B0F07_AdjustorThunk (void);
extern void StringBuffer_get_InternalBuffer_mF41DB26CCDD88E4ACE7B65840ADB74BF5EEE748A_AdjustorThunk (void);
extern void StringReference_get_Item_m34C3ABEC482DD4EC5541D0A56EC05D05F669B0A0_AdjustorThunk (void);
extern void StringReference_get_Chars_m5EF63176FDB3554674E394261BE5786170C47024_AdjustorThunk (void);
extern void StringReference_get_StartIndex_mB09EFAA44487DB78CF15BDF61052689823A4C2C9_AdjustorThunk (void);
extern void StringReference_get_Length_m194CFDC809C99311F4040820C9F4A3B5E5F77EA4_AdjustorThunk (void);
extern void StringReference__ctor_m3AE890FED005EFC9FF5CDF3C7F740E743168B616_AdjustorThunk (void);
extern void StringReference_ToString_mFE493A141754B54CA6B7DC1903730740DAB2A881_AdjustorThunk (void);
extern void ResolverContractKey__ctor_m5C7C616DF53AB9D3EE81A19395431BA9B0A3EB9C_AdjustorThunk (void);
extern void ResolverContractKey_GetHashCode_mCF78DF3E9A1EB11DACA737FA5A306F172A8A0D6B_AdjustorThunk (void);
extern void ResolverContractKey_Equals_m7EADD44C28175D677F9881E4D0021CF8936E0732_AdjustorThunk (void);
extern void ResolverContractKey_Equals_m7F6E1B33EEBDE4971264A88E6662D9CDDE4CFC18_AdjustorThunk (void);
extern void TypeNameKey__ctor_mDC4275014619C46DE6BB5BE66BEEEA2962D1DDCD_AdjustorThunk (void);
extern void TypeNameKey_GetHashCode_m6F14E3CA5E0098A41949DDA45EC588069B2F7A4A_AdjustorThunk (void);
extern void TypeNameKey_Equals_m2508626D8FED8CA829246608EE62F8E2D34B5AA5_AdjustorThunk (void);
extern void TypeNameKey_Equals_m4114698DBBE4F208BBD13645CF9DF3500A5B2F6E_AdjustorThunk (void);
static Il2CppTokenAdjustorThunkPair s_adjustorThunks[43] = 
{
	{ 0x06000030, JsonPosition__ctor_m3C4DB92D84AE311EE082E3D1C35558CF05969C28_AdjustorThunk },
	{ 0x06000031, JsonPosition_CalculateLength_mF22E55ABE3DE00539186EC9136AF9FC493E0CCBD_AdjustorThunk },
	{ 0x06000032, JsonPosition_WriteTo_mEFF7AE829F518C06937587D13B92A88F35FCC73D_AdjustorThunk },
	{ 0x060001CD, TypeConvertKey_get_InitialType_m0F65A2432EB92CAA1377B113C1BAEF2C0283F634_AdjustorThunk },
	{ 0x060001CE, TypeConvertKey_get_TargetType_m8F2288FCB1DB8039CEE75810A72EC31B0085805D_AdjustorThunk },
	{ 0x060001CF, TypeConvertKey__ctor_mAE2DD8A2A907D194638D666DED0AAC9FF3AA9E48_AdjustorThunk },
	{ 0x060001D0, TypeConvertKey_GetHashCode_mC12EC356438B1840646DBA4ADEB7F6EB4AFDE894_AdjustorThunk },
	{ 0x060001D1, TypeConvertKey_Equals_m4DE5600EBFEAFAA076537C2075A96573AB87D2BE_AdjustorThunk },
	{ 0x060001D2, TypeConvertKey_Equals_mCCE90DDED2DF5DA439CDA3277A1BBB36E6E94C13_AdjustorThunk },
	{ 0x060001D6, DateTimeParser_Parse_m64820EE59B4BB49B549346281AD8B2EF330B3EDD_AdjustorThunk },
	{ 0x060001D7, DateTimeParser_ParseDate_m1A8649D480733E2FFD4F24827B2FC6C951C069E7_AdjustorThunk },
	{ 0x060001D8, DateTimeParser_ParseTimeAndZoneAndWhitespace_mD103AD070AF0E15C3783EA88A21A1C71732FEF70_AdjustorThunk },
	{ 0x060001D9, DateTimeParser_ParseTime_mB6558BEA3B81EB444F3FF78CD7288979EF071D20_AdjustorThunk },
	{ 0x060001DA, DateTimeParser_ParseZone_mC630CC51579EADDAA8D595F081F89E5FEB7977CE_AdjustorThunk },
	{ 0x060001DB, DateTimeParser_Parse4Digit_mFE4C2AE33BC9C3EACC29964487AE45CB86D66DDA_AdjustorThunk },
	{ 0x060001DC, DateTimeParser_Parse2Digit_m7D1B3E711C5BB1704A70ABB399E0711AF064234E_AdjustorThunk },
	{ 0x060001DD, DateTimeParser_ParseChar_m510297FF2814F39BC1DF2AFF91D2556AE81B7E5A_AdjustorThunk },
	{ 0x0600028F, StringBuffer_get_Position_m424CA0E4A0274918489C5986D189CB6DDEA6F11F_AdjustorThunk },
	{ 0x06000290, StringBuffer_set_Position_mE931269D642EEE14E2D84EEE1F0706536D65401F_AdjustorThunk },
	{ 0x06000291, StringBuffer_get_IsEmpty_m3B5208A8E035C3BF103F48C4BD666BA7D16C7682_AdjustorThunk },
	{ 0x06000292, StringBuffer__ctor_m658802150D63B715C3470D6B2C0170551ECB7DD5_AdjustorThunk },
	{ 0x06000293, StringBuffer__ctor_m80178B6D27B163F78337AC8C7BA4D6026696FA83_AdjustorThunk },
	{ 0x06000294, StringBuffer_Append_m0FCC4FB58E15B226B908B153D32045BBFF1D76F0_AdjustorThunk },
	{ 0x06000295, StringBuffer_Append_m603D9B2C24029588D05093561F805BC76C4DF0EA_AdjustorThunk },
	{ 0x06000296, StringBuffer_Clear_m56DC59C94CAF5F3A5830C14E2E38F0C205F78610_AdjustorThunk },
	{ 0x06000297, StringBuffer_EnsureSize_m76D11C8EED77ACD0299390A98CE072C5F2FE9EC0_AdjustorThunk },
	{ 0x06000298, StringBuffer_ToString_m10B8FF428EAE38BBC65EEC7809ADC392532B5927_AdjustorThunk },
	{ 0x06000299, StringBuffer_ToString_m88EAF73086605E7A0C898D962CA6B5CF8A6B0F07_AdjustorThunk },
	{ 0x0600029A, StringBuffer_get_InternalBuffer_mF41DB26CCDD88E4ACE7B65840ADB74BF5EEE748A_AdjustorThunk },
	{ 0x0600029B, StringReference_get_Item_m34C3ABEC482DD4EC5541D0A56EC05D05F669B0A0_AdjustorThunk },
	{ 0x0600029C, StringReference_get_Chars_m5EF63176FDB3554674E394261BE5786170C47024_AdjustorThunk },
	{ 0x0600029D, StringReference_get_StartIndex_mB09EFAA44487DB78CF15BDF61052689823A4C2C9_AdjustorThunk },
	{ 0x0600029E, StringReference_get_Length_m194CFDC809C99311F4040820C9F4A3B5E5F77EA4_AdjustorThunk },
	{ 0x0600029F, StringReference__ctor_m3AE890FED005EFC9FF5CDF3C7F740E743168B616_AdjustorThunk },
	{ 0x060002A0, StringReference_ToString_mFE493A141754B54CA6B7DC1903730740DAB2A881_AdjustorThunk },
	{ 0x060002C6, ResolverContractKey__ctor_m5C7C616DF53AB9D3EE81A19395431BA9B0A3EB9C_AdjustorThunk },
	{ 0x060002C7, ResolverContractKey_GetHashCode_mCF78DF3E9A1EB11DACA737FA5A306F172A8A0D6B_AdjustorThunk },
	{ 0x060002C8, ResolverContractKey_Equals_m7EADD44C28175D677F9881E4D0021CF8936E0732_AdjustorThunk },
	{ 0x060002C9, ResolverContractKey_Equals_m7F6E1B33EEBDE4971264A88E6662D9CDDE4CFC18_AdjustorThunk },
	{ 0x06000324, TypeNameKey__ctor_mDC4275014619C46DE6BB5BE66BEEEA2962D1DDCD_AdjustorThunk },
	{ 0x06000325, TypeNameKey_GetHashCode_m6F14E3CA5E0098A41949DDA45EC588069B2F7A4A_AdjustorThunk },
	{ 0x06000326, TypeNameKey_Equals_m2508626D8FED8CA829246608EE62F8E2D34B5AA5_AdjustorThunk },
	{ 0x06000327, TypeNameKey_Equals_m4114698DBBE4F208BBD13645CF9DF3500A5B2F6E_AdjustorThunk },
};
static const int32_t s_InvokerIndices[1255] = 
{
	-1,
	-1,
	3451,
	3397,
	3397,
	3414,
	3414,
	3414,
	3414,
	3414,
	2887,
	5208,
	5073,
	5062,
	4126,
	3842,
	4076,
	4654,
	5066,
	5056,
	5066,
	4674,
	4363,
	5066,
	4371,
	4371,
	-1,
	-1,
	4371,
	5226,
	1142,
	599,
	2545,
	3451,
	3451,
	3479,
	3414,
	3414,
	3479,
	3479,
	2887,
	1747,
	1755,
	3451,
	3451,
	3479,
	3397,
	2870,
	3397,
	2887,
	5108,
	4670,
	4371,
	5226,
	3414,
	3414,
	3414,
	3414,
	3414,
	3397,
	3451,
	2920,
	3451,
	3397,
	2870,
	3397,
	2870,
	3397,
	2870,
	3414,
	2887,
	3300,
	2772,
	3397,
	3414,
	3414,
	3397,
	3414,
	3414,
	2887,
	2263,
	3479,
	2870,
	3397,
	3397,
	3451,
	3300,
	1878,
	3414,
	3414,
	3414,
	3299,
	1877,
	3294,
	1873,
	3297,
	1876,
	3295,
	1874,
	3296,
	1875,
	3479,
	3414,
	3479,
	3479,
	2870,
	1627,
	1101,
	2920,
	3479,
	2870,
	3479,
	3479,
	2140,
	3479,
	2920,
	3479,
	3479,
	3451,
	3451,
	3397,
	2870,
	2870,
	2887,
	3479,
	1755,
	405,
	4679,
	4371,
	4111,
	3479,
	2887,
	1747,
	1755,
	4679,
	4371,
	4111,
	2887,
	2887,
	2887,
	2887,
	3414,
	2887,
	2887,
	2870,
	2870,
	2870,
	2870,
	2870,
	2870,
	2870,
	2870,
	2870,
	3397,
	2870,
	3414,
	3414,
	2887,
	3456,
	2925,
	3397,
	3451,
	2920,
	3451,
	3479,
	5208,
	5066,
	5208,
	5066,
	4862,
	-1,
	1315,
	1315,
	131,
	142,
	1142,
	1747,
	1142,
	3414,
	2300,
	4679,
	2887,
	3397,
	3397,
	3397,
	3397,
	3397,
	3414,
	3397,
	3397,
	3397,
	3397,
	3397,
	3414,
	3414,
	3414,
	3414,
	3414,
	3414,
	3456,
	5226,
	2887,
	3479,
	2870,
	1488,
	3935,
	3479,
	2179,
	1251,
	1376,
	1376,
	3451,
	3300,
	3295,
	3414,
	3414,
	2297,
	2296,
	3294,
	3479,
	2297,
	3296,
	3297,
	3299,
	3479,
	3479,
	3451,
	3479,
	2869,
	1084,
	3396,
	3479,
	3479,
	3451,
	3451,
	3451,
	2527,
	3479,
	3451,
	3479,
	2920,
	2575,
	3479,
	2870,
	2920,
	1169,
	2545,
	2545,
	2527,
	3479,
	3479,
	3479,
	3479,
	2297,
	2297,
	2297,
	3479,
	3451,
	3397,
	3397,
	3414,
	3396,
	2887,
	3479,
	3479,
	3479,
	2887,
	2870,
	2887,
	1753,
	3479,
	3479,
	3479,
	3479,
	3479,
	1743,
	3479,
	3479,
	2887,
	2887,
	1753,
	2870,
	2870,
	2871,
	2871,
	2922,
	2779,
	2846,
	2769,
	2920,
	2869,
	2869,
	2869,
	2920,
	2920,
	2844,
	2842,
	2887,
	2843,
	2862,
	2936,
	2887,
	2887,
	3479,
	2871,
	2871,
	5208,
	5226,
	3451,
	2920,
	3397,
	3397,
	3414,
	3414,
	3397,
	2870,
	3397,
	2870,
	3397,
	2870,
	3397,
	2870,
	3479,
	3397,
	2870,
	3414,
	2887,
	3414,
	2887,
	3479,
	3479,
	2870,
	3397,
	3397,
	3479,
	3479,
	3479,
	3479,
	3479,
	2887,
	3479,
	2887,
	1753,
	3479,
	1627,
	798,
	2887,
	2870,
	3479,
	2140,
	2870,
	2870,
	3479,
	3479,
	3479,
	2870,
	3479,
	3479,
	2887,
	2887,
	2887,
	2870,
	2870,
	2871,
	2871,
	2922,
	2846,
	2920,
	2869,
	2869,
	2869,
	2920,
	2920,
	2844,
	2842,
	2843,
	2862,
	2936,
	2772,
	2784,
	2773,
	2785,
	2779,
	2769,
	2762,
	2771,
	2783,
	2764,
	2763,
	2778,
	2767,
	2765,
	2766,
	2770,
	2780,
	2887,
	2887,
	2887,
	3479,
	2920,
	4504,
	4679,
	2870,
	2887,
	3479,
	1612,
	2870,
	3479,
	2887,
	3479,
	1755,
	1142,
	4371,
	4371,
	2887,
	1130,
	3479,
	1130,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	5111,
	4679,
	4371,
	-1,
	-1,
	4675,
	4513,
	4679,
	4370,
	3414,
	3414,
	2887,
	3397,
	2870,
	3479,
	4998,
	4611,
	5066,
	5111,
	5135,
	5077,
	4168,
	4013,
	4371,
	4371,
	5066,
	4755,
	5111,
	4000,
	4000,
	4755,
	4298,
	4993,
	5226,
	3414,
	3414,
	1747,
	3397,
	2545,
	2619,
	3479,
	2300,
	5226,
	1037,
	2528,
	2528,
	2472,
	2528,
	1370,
	1370,
	1372,
	5226,
	5132,
	4565,
	4929,
	4929,
	4622,
	4622,
	5008,
	4932,
	4436,
	4772,
	4931,
	3909,
	3894,
	4179,
	4168,
	4178,
	4436,
	3894,
	4772,
	4168,
	4436,
	3926,
	3673,
	4296,
	4217,
	4006,
	3927,
	4199,
	3414,
	5066,
	5066,
	5226,
	5226,
	3479,
	2300,
	2545,
	4675,
	4862,
	4366,
	5226,
	4661,
	4759,
	3606,
	4093,
	5108,
	5108,
	5108,
	5208,
	2300,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	3479,
	5226,
	3479,
	2300,
	2300,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	4995,
	4981,
	4737,
	-1,
	-1,
	-1,
	-1,
	4759,
	4371,
	5066,
	5226,
	3479,
	936,
	2300,
	1314,
	3479,
	4166,
	1132,
	-1,
	-1,
	-1,
	2300,
	-1,
	-1,
	-1,
	-1,
	-1,
	3479,
	3414,
	2887,
	3414,
	2887,
	2887,
	3479,
	3414,
	2887,
	3414,
	2887,
	3479,
	1315,
	2300,
	4679,
	4371,
	3479,
	2300,
	3479,
	2300,
	3479,
	1747,
	5226,
	5111,
	5066,
	5111,
	5066,
	4366,
	5066,
	4760,
	5066,
	4680,
	5111,
	5111,
	5066,
	4759,
	4759,
	4417,
	4759,
	4417,
	4417,
	5066,
	4497,
	5066,
	5111,
	5111,
	4679,
	4513,
	4760,
	4423,
	4675,
	4758,
	-1,
	-1,
	-1,
	4373,
	4497,
	4904,
	4679,
	4675,
	4511,
	4675,
	4604,
	4511,
	5066,
	5226,
	3479,
	2545,
	2300,
	2300,
	2545,
	3479,
	2545,
	2545,
	2545,
	3397,
	2870,
	3451,
	1743,
	2887,
	1742,
	775,
	2887,
	1743,
	3414,
	1303,
	3414,
	2008,
	3414,
	3397,
	3397,
	1130,
	3414,
	4052,
	4773,
	4773,
	4371,
	4111,
	3867,
	3700,
	4371,
	5063,
	4839,
	-1,
	5107,
	5107,
	4757,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	4998,
	5111,
	5111,
	5111,
	5111,
	5066,
	5111,
	5111,
	5111,
	5111,
	5111,
	4417,
	4759,
	4759,
	4862,
	-1,
	-1,
	1747,
	3397,
	2545,
	2572,
	3479,
	5208,
	3397,
	2870,
	3451,
	3451,
	3451,
	2920,
	3414,
	3479,
	2920,
	3414,
	2300,
	2300,
	2545,
	2300,
	2300,
	4862,
	2300,
	2300,
	1315,
	1315,
	2300,
	2300,
	2887,
	1747,
	232,
	5111,
	5111,
	2300,
	2300,
	2300,
	2300,
	2300,
	2300,
	2300,
	5111,
	5111,
	5111,
	3902,
	5066,
	1314,
	2300,
	1314,
	257,
	2300,
	1144,
	2300,
	2300,
	2300,
	5226,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	5226,
	3479,
	2545,
	2545,
	2300,
	2545,
	2545,
	2154,
	3479,
	3479,
	1142,
	3479,
	2300,
	3479,
	2300,
	3479,
	2545,
	3479,
	2545,
	2300,
	1315,
	1315,
	1142,
	1391,
	3479,
	5079,
	1315,
	3479,
	5226,
	1747,
	3397,
	2545,
	2621,
	781,
	3451,
	2920,
	3414,
	2887,
	2887,
	2887,
	2887,
	3451,
	2887,
	2887,
	1747,
	2300,
	1315,
	1315,
	1391,
	1142,
	3397,
	1100,
	1747,
	2300,
	3414,
	2887,
	3451,
	2920,
	3451,
	2920,
	3451,
	2920,
	3451,
	2920,
	3414,
	3414,
	2887,
	3451,
	2920,
	3451,
	2887,
	2300,
	3414,
	3414,
	2887,
	3414,
	3414,
	2887,
	3294,
	2762,
	3303,
	2776,
	3308,
	2782,
	2887,
	1745,
	1755,
	611,
	2887,
	1745,
	1159,
	336,
	2887,
	1745,
	1142,
	328,
	2887,
	1745,
	2300,
	942,
	2300,
	3414,
	2887,
	3414,
	2887,
	3294,
	2762,
	3414,
	2887,
	3414,
	2887,
	3414,
	3414,
	3414,
	3414,
	3414,
	3414,
	2887,
	3451,
	2920,
	2887,
	1755,
	1755,
	1755,
	1755,
	1159,
	5066,
	5066,
	3479,
	1755,
	3479,
	1159,
	3414,
	2887,
	3414,
	2887,
	3414,
	2887,
	3414,
	2887,
	3451,
	2920,
	3414,
	3414,
	2887,
	3451,
	2920,
	3451,
	2887,
	2300,
	3414,
	2887,
	2887,
	3397,
	2870,
	3304,
	2777,
	3414,
	2887,
	3414,
	2887,
	2887,
	3414,
	3414,
	3414,
	2887,
	3414,
	2887,
	2887,
	3451,
	2887,
	3414,
	3397,
	2870,
	2887,
	5226,
	3414,
	2887,
	3414,
	2887,
	3414,
	2887,
	3300,
	2772,
	3414,
	2887,
	3414,
	2887,
	2887,
	3414,
	2887,
	3414,
	2887,
	3414,
	2887,
	3451,
	2920,
	3451,
	2920,
	3451,
	2920,
	3451,
	2920,
	3414,
	2887,
	3414,
	3397,
	3294,
	2762,
	3301,
	2774,
	3298,
	2768,
	3303,
	2776,
	3302,
	2775,
	3308,
	2782,
	3414,
	2887,
	3414,
	3414,
	2887,
	3414,
	2887,
	3414,
	3414,
	2887,
	3294,
	2762,
	3308,
	2782,
	3303,
	2776,
	2887,
	3479,
	2887,
	2300,
	2887,
	2300,
	1386,
	1314,
	2887,
	3414,
	599,
	3479,
	211,
	1391,
	2154,
	3479,
	2887,
	2300,
	944,
	3414,
	112,
	4420,
	2300,
	599,
	112,
	32,
	132,
	942,
	196,
	2545,
	328,
	211,
	17,
	1142,
	1373,
	1391,
	939,
	939,
	1142,
	1142,
	328,
	328,
	781,
	328,
	328,
	599,
	599,
	1044,
	194,
	328,
	1043,
	1391,
	410,
	942,
	148,
	1142,
	1151,
	3479,
	3479,
	2545,
	5226,
	3479,
	2300,
	2300,
	2300,
	2154,
	2887,
	1142,
	3414,
	2300,
	261,
	261,
	444,
	357,
	1391,
	211,
	1747,
	1315,
	4417,
	1142,
	1142,
	1142,
	261,
	118,
	261,
	1142,
	1747,
	1373,
	1373,
	1373,
	261,
	261,
	261,
	259,
	211,
	350,
	261,
	596,
	1743,
	1043,
	1043,
	2887,
	2887,
	2887,
	3414,
	2887,
	2887,
	3414,
	2870,
	3414,
	2887,
	2870,
	2870,
	2870,
	2870,
	2870,
	2870,
	3397,
	2870,
	2870,
	2870,
	2887,
	3456,
	2925,
	3397,
	3451,
	2920,
	3414,
	2887,
	2887,
	1315,
	1142,
	2887,
	-1,
	5066,
	5066,
	4615,
	5066,
	4679,
	4679,
	5066,
	5066,
	5066,
	5066,
	5066,
	-1,
	-1,
	-1,
	5216,
	5208,
	5226,
	3479,
	2300,
	5226,
	3479,
	2300,
	3451,
	3451,
	1316,
	2300,
	2300,
	-1,
	-1,
	-1,
	-1,
	2887,
	2887,
	1747,
	2300,
	2887,
	3414,
	3451,
	3300,
	3414,
	3414,
	3297,
	3299,
	3294,
	3295,
	3296,
	3397,
	3414,
	3397,
	3414,
	3414,
	3479,
	3451,
	3397,
	3397,
	2887,
	3414,
	2844,
	2920,
	2920,
	2763,
	2869,
	2887,
	2842,
	2843,
	2846,
	3479,
	3479,
	2922,
	2862,
	2870,
	2871,
	2920,
	2869,
	2887,
	2936,
	2870,
	2871,
	2887,
	2869,
	2887,
	3479,
	3479,
	2887,
	3479,
	2887,
	1753,
	3479,
	3479,
	2887,
	2887,
	3479,
	1142,
	2300,
	2887,
	599,
	2300,
	2545,
	3479,
	5066,
	1142,
	599,
	2545,
	3479,
	5226,
	1142,
	1142,
	599,
	2300,
	1315,
	2545,
	3479,
};
static const Il2CppTokenRangePair s_rgctxIndices[36] = 
{
	{ 0x02000035, { 5, 9 } },
	{ 0x0200004C, { 58, 1 } },
	{ 0x0200004D, { 59, 1 } },
	{ 0x0200004E, { 60, 1 } },
	{ 0x0200004F, { 61, 1 } },
	{ 0x02000050, { 62, 1 } },
	{ 0x02000051, { 63, 1 } },
	{ 0x02000064, { 84, 1 } },
	{ 0x02000065, { 85, 7 } },
	{ 0x02000068, { 92, 7 } },
	{ 0x0200006C, { 99, 3 } },
	{ 0x0200006D, { 102, 8 } },
	{ 0x0600001B, { 0, 1 } },
	{ 0x0600001C, { 1, 2 } },
	{ 0x060000AB, { 3, 2 } },
	{ 0x060001A8, { 14, 1 } },
	{ 0x060001A9, { 15, 3 } },
	{ 0x060001AA, { 18, 2 } },
	{ 0x060001AE, { 20, 3 } },
	{ 0x060001AF, { 23, 3 } },
	{ 0x06000210, { 26, 6 } },
	{ 0x06000211, { 32, 6 } },
	{ 0x06000212, { 38, 5 } },
	{ 0x06000213, { 43, 5 } },
	{ 0x06000214, { 48, 5 } },
	{ 0x06000215, { 53, 5 } },
	{ 0x0600023B, { 64, 2 } },
	{ 0x0600023C, { 66, 2 } },
	{ 0x06000278, { 68, 1 } },
	{ 0x06000279, { 69, 2 } },
	{ 0x0600027A, { 71, 4 } },
	{ 0x060002AB, { 75, 9 } },
	{ 0x06000477, { 110, 2 } },
	{ 0x06000483, { 112, 2 } },
	{ 0x06000484, { 114, 2 } },
	{ 0x06000485, { 116, 3 } },
};
static const Il2CppRGCTXDefinition s_rgctxValues[119] = 
{
	{ (Il2CppRGCTXDataType)3, 25617 },
	{ (Il2CppRGCTXDataType)1, 186 },
	{ (Il2CppRGCTXDataType)2, 186 },
	{ (Il2CppRGCTXDataType)1, 195 },
	{ (Il2CppRGCTXDataType)2, 195 },
	{ (Il2CppRGCTXDataType)3, 2186 },
	{ (Il2CppRGCTXDataType)2, 1738 },
	{ (Il2CppRGCTXDataType)3, 3510 },
	{ (Il2CppRGCTXDataType)2, 1760 },
	{ (Il2CppRGCTXDataType)3, 3596 },
	{ (Il2CppRGCTXDataType)2, 2728 },
	{ (Il2CppRGCTXDataType)2, 913 },
	{ (Il2CppRGCTXDataType)2, 493 },
	{ (Il2CppRGCTXDataType)2, 2738 },
	{ (Il2CppRGCTXDataType)2, 2578 },
	{ (Il2CppRGCTXDataType)2, 2773 },
	{ (Il2CppRGCTXDataType)2, 2974 },
	{ (Il2CppRGCTXDataType)2, 2579 },
	{ (Il2CppRGCTXDataType)3, 25124 },
	{ (Il2CppRGCTXDataType)3, 24986 },
	{ (Il2CppRGCTXDataType)2, 2772 },
	{ (Il2CppRGCTXDataType)2, 2973 },
	{ (Il2CppRGCTXDataType)3, 9370 },
	{ (Il2CppRGCTXDataType)2, 68 },
	{ (Il2CppRGCTXDataType)3, 12912 },
	{ (Il2CppRGCTXDataType)3, 12911 },
	{ (Il2CppRGCTXDataType)2, 1227 },
	{ (Il2CppRGCTXDataType)3, 332 },
	{ (Il2CppRGCTXDataType)3, 333 },
	{ (Il2CppRGCTXDataType)2, 4127 },
	{ (Il2CppRGCTXDataType)3, 17251 },
	{ (Il2CppRGCTXDataType)3, 334 },
	{ (Il2CppRGCTXDataType)2, 1248 },
	{ (Il2CppRGCTXDataType)3, 442 },
	{ (Il2CppRGCTXDataType)3, 443 },
	{ (Il2CppRGCTXDataType)2, 2180 },
	{ (Il2CppRGCTXDataType)3, 9308 },
	{ (Il2CppRGCTXDataType)3, 444 },
	{ (Il2CppRGCTXDataType)2, 1257 },
	{ (Il2CppRGCTXDataType)3, 472 },
	{ (Il2CppRGCTXDataType)3, 473 },
	{ (Il2CppRGCTXDataType)2, 2251 },
	{ (Il2CppRGCTXDataType)3, 9377 },
	{ (Il2CppRGCTXDataType)2, 1261 },
	{ (Il2CppRGCTXDataType)3, 504 },
	{ (Il2CppRGCTXDataType)3, 505 },
	{ (Il2CppRGCTXDataType)2, 2250 },
	{ (Il2CppRGCTXDataType)3, 9376 },
	{ (Il2CppRGCTXDataType)2, 1263 },
	{ (Il2CppRGCTXDataType)3, 526 },
	{ (Il2CppRGCTXDataType)3, 527 },
	{ (Il2CppRGCTXDataType)2, 1409 },
	{ (Il2CppRGCTXDataType)3, 913 },
	{ (Il2CppRGCTXDataType)2, 1266 },
	{ (Il2CppRGCTXDataType)3, 550 },
	{ (Il2CppRGCTXDataType)3, 551 },
	{ (Il2CppRGCTXDataType)2, 1410 },
	{ (Il2CppRGCTXDataType)3, 914 },
	{ (Il2CppRGCTXDataType)2, 731 },
	{ (Il2CppRGCTXDataType)2, 732 },
	{ (Il2CppRGCTXDataType)2, 733 },
	{ (Il2CppRGCTXDataType)2, 734 },
	{ (Il2CppRGCTXDataType)2, 735 },
	{ (Il2CppRGCTXDataType)2, 736 },
	{ (Il2CppRGCTXDataType)3, 25929 },
	{ (Il2CppRGCTXDataType)3, 25928 },
	{ (Il2CppRGCTXDataType)3, 25935 },
	{ (Il2CppRGCTXDataType)3, 25934 },
	{ (Il2CppRGCTXDataType)3, 25944 },
	{ (Il2CppRGCTXDataType)3, 25946 },
	{ (Il2CppRGCTXDataType)3, 25176 },
	{ (Il2CppRGCTXDataType)1, 298 },
	{ (Il2CppRGCTXDataType)2, 5313 },
	{ (Il2CppRGCTXDataType)3, 25125 },
	{ (Il2CppRGCTXDataType)3, 25236 },
	{ (Il2CppRGCTXDataType)2, 1190 },
	{ (Il2CppRGCTXDataType)3, 114 },
	{ (Il2CppRGCTXDataType)3, 115 },
	{ (Il2CppRGCTXDataType)2, 2257 },
	{ (Il2CppRGCTXDataType)3, 9380 },
	{ (Il2CppRGCTXDataType)3, 25274 },
	{ (Il2CppRGCTXDataType)3, 25164 },
	{ (Il2CppRGCTXDataType)3, 25223 },
	{ (Il2CppRGCTXDataType)3, 116 },
	{ (Il2CppRGCTXDataType)3, 9413 },
	{ (Il2CppRGCTXDataType)2, 1746 },
	{ (Il2CppRGCTXDataType)3, 3586 },
	{ (Il2CppRGCTXDataType)3, 3588 },
	{ (Il2CppRGCTXDataType)3, 20643 },
	{ (Il2CppRGCTXDataType)3, 9393 },
	{ (Il2CppRGCTXDataType)3, 3589 },
	{ (Il2CppRGCTXDataType)3, 3587 },
	{ (Il2CppRGCTXDataType)2, 1583 },
	{ (Il2CppRGCTXDataType)3, 20645 },
	{ (Il2CppRGCTXDataType)3, 25653 },
	{ (Il2CppRGCTXDataType)2, 2430 },
	{ (Il2CppRGCTXDataType)3, 9494 },
	{ (Il2CppRGCTXDataType)2, 4645 },
	{ (Il2CppRGCTXDataType)3, 20644 },
	{ (Il2CppRGCTXDataType)2, 1278 },
	{ (Il2CppRGCTXDataType)3, 685 },
	{ (Il2CppRGCTXDataType)3, 6997 },
	{ (Il2CppRGCTXDataType)3, 686 },
	{ (Il2CppRGCTXDataType)2, 2927 },
	{ (Il2CppRGCTXDataType)2, 3078 },
	{ (Il2CppRGCTXDataType)3, 12448 },
	{ (Il2CppRGCTXDataType)2, 839 },
	{ (Il2CppRGCTXDataType)3, 12449 },
	{ (Il2CppRGCTXDataType)2, 1036 },
	{ (Il2CppRGCTXDataType)3, 687 },
	{ (Il2CppRGCTXDataType)3, 2299 },
	{ (Il2CppRGCTXDataType)2, 1582 },
	{ (Il2CppRGCTXDataType)3, 25943 },
	{ (Il2CppRGCTXDataType)2, 198 },
	{ (Il2CppRGCTXDataType)3, 25942 },
	{ (Il2CppRGCTXDataType)2, 197 },
	{ (Il2CppRGCTXDataType)3, 25652 },
	{ (Il2CppRGCTXDataType)3, 25651 },
	{ (Il2CppRGCTXDataType)3, 25941 },
};
extern const CustomAttributesCacheGenerator g_Newtonsoft_Json_AttributeGenerators[];
IL2CPP_EXTERN_C const Il2CppCodeGenModule g_Newtonsoft_Json_CodeGenModule;
const Il2CppCodeGenModule g_Newtonsoft_Json_CodeGenModule = 
{
	"Newtonsoft.Json.dll",
	1255,
	s_methodPointers,
	43,
	s_adjustorThunks,
	s_InvokerIndices,
	0,
	NULL,
	36,
	s_rgctxIndices,
	119,
	s_rgctxValues,
	NULL,
	g_Newtonsoft_Json_AttributeGenerators,
	NULL, // module initializer,
	NULL,
	NULL,
	NULL,
};
