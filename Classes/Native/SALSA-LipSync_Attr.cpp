﻿#include "pch-cpp.hpp"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include <limits>
#include <stdint.h>



// System.Char[]
struct CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34;
// UnityEngine.AddComponentMenu
struct AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100;
// System.Runtime.CompilerServices.CompilationRelaxationsAttribute
struct CompilationRelaxationsAttribute_t661FDDC06629BDA607A42BD660944F039FE03AFF;
// System.Runtime.CompilerServices.CompilerGeneratedAttribute
struct CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C;
// System.Diagnostics.DebuggableAttribute
struct DebuggableAttribute_tA8054EBD0FC7511695D494B690B5771658E3191B;
// System.Diagnostics.DebuggerHiddenAttribute
struct DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88;
// UnityEngine.HelpURLAttribute
struct HelpURLAttribute_t0924A6D83FABA7B77780F7F9BBCBCB9E0EA15023;
// UnityEngine.HideInInspector
struct HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA;
// UnityEngine.RangeAttribute
struct RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5;
// System.Runtime.CompilerServices.RuntimeCompatibilityAttribute
struct RuntimeCompatibilityAttribute_tFF99AB2963098F9CBCD47A20D9FD3D51C17C1C80;
// UnityEngine.SerializeField
struct SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25;
// System.String
struct String_t;



IL2CPP_EXTERN_C_BEGIN
IL2CPP_EXTERN_C_END

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Object


// System.Attribute
struct Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71  : public RuntimeObject
{
public:

public:
};


// System.String
struct String_t  : public RuntimeObject
{
public:
	// System.Int32 System.String::m_stringLength
	int32_t ___m_stringLength_0;
	// System.Char System.String::m_firstChar
	Il2CppChar ___m_firstChar_1;

public:
	inline static int32_t get_offset_of_m_stringLength_0() { return static_cast<int32_t>(offsetof(String_t, ___m_stringLength_0)); }
	inline int32_t get_m_stringLength_0() const { return ___m_stringLength_0; }
	inline int32_t* get_address_of_m_stringLength_0() { return &___m_stringLength_0; }
	inline void set_m_stringLength_0(int32_t value)
	{
		___m_stringLength_0 = value;
	}

	inline static int32_t get_offset_of_m_firstChar_1() { return static_cast<int32_t>(offsetof(String_t, ___m_firstChar_1)); }
	inline Il2CppChar get_m_firstChar_1() const { return ___m_firstChar_1; }
	inline Il2CppChar* get_address_of_m_firstChar_1() { return &___m_firstChar_1; }
	inline void set_m_firstChar_1(Il2CppChar value)
	{
		___m_firstChar_1 = value;
	}
};

struct String_t_StaticFields
{
public:
	// System.String System.String::Empty
	String_t* ___Empty_5;

public:
	inline static int32_t get_offset_of_Empty_5() { return static_cast<int32_t>(offsetof(String_t_StaticFields, ___Empty_5)); }
	inline String_t* get_Empty_5() const { return ___Empty_5; }
	inline String_t** get_address_of_Empty_5() { return &___Empty_5; }
	inline void set_Empty_5(String_t* value)
	{
		___Empty_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___Empty_5), (void*)value);
	}
};


// System.ValueType
struct ValueType_tDBF999C1B75C48C68621878250DBF6CDBCF51E52  : public RuntimeObject
{
public:

public:
};

// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_tDBF999C1B75C48C68621878250DBF6CDBCF51E52_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_tDBF999C1B75C48C68621878250DBF6CDBCF51E52_marshaled_com
{
};

// UnityEngine.AddComponentMenu
struct AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.String UnityEngine.AddComponentMenu::m_AddComponentMenu
	String_t* ___m_AddComponentMenu_0;
	// System.Int32 UnityEngine.AddComponentMenu::m_Ordering
	int32_t ___m_Ordering_1;

public:
	inline static int32_t get_offset_of_m_AddComponentMenu_0() { return static_cast<int32_t>(offsetof(AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100, ___m_AddComponentMenu_0)); }
	inline String_t* get_m_AddComponentMenu_0() const { return ___m_AddComponentMenu_0; }
	inline String_t** get_address_of_m_AddComponentMenu_0() { return &___m_AddComponentMenu_0; }
	inline void set_m_AddComponentMenu_0(String_t* value)
	{
		___m_AddComponentMenu_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_AddComponentMenu_0), (void*)value);
	}

	inline static int32_t get_offset_of_m_Ordering_1() { return static_cast<int32_t>(offsetof(AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100, ___m_Ordering_1)); }
	inline int32_t get_m_Ordering_1() const { return ___m_Ordering_1; }
	inline int32_t* get_address_of_m_Ordering_1() { return &___m_Ordering_1; }
	inline void set_m_Ordering_1(int32_t value)
	{
		___m_Ordering_1 = value;
	}
};


// System.Boolean
struct Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37 
{
public:
	// System.Boolean System.Boolean::m_value
	bool ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37, ___m_value_0)); }
	inline bool get_m_value_0() const { return ___m_value_0; }
	inline bool* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(bool value)
	{
		___m_value_0 = value;
	}
};

struct Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37_StaticFields
{
public:
	// System.String System.Boolean::TrueString
	String_t* ___TrueString_5;
	// System.String System.Boolean::FalseString
	String_t* ___FalseString_6;

public:
	inline static int32_t get_offset_of_TrueString_5() { return static_cast<int32_t>(offsetof(Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37_StaticFields, ___TrueString_5)); }
	inline String_t* get_TrueString_5() const { return ___TrueString_5; }
	inline String_t** get_address_of_TrueString_5() { return &___TrueString_5; }
	inline void set_TrueString_5(String_t* value)
	{
		___TrueString_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___TrueString_5), (void*)value);
	}

	inline static int32_t get_offset_of_FalseString_6() { return static_cast<int32_t>(offsetof(Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37_StaticFields, ___FalseString_6)); }
	inline String_t* get_FalseString_6() const { return ___FalseString_6; }
	inline String_t** get_address_of_FalseString_6() { return &___FalseString_6; }
	inline void set_FalseString_6(String_t* value)
	{
		___FalseString_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___FalseString_6), (void*)value);
	}
};


// System.Runtime.CompilerServices.CompilationRelaxationsAttribute
struct CompilationRelaxationsAttribute_t661FDDC06629BDA607A42BD660944F039FE03AFF  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.Int32 System.Runtime.CompilerServices.CompilationRelaxationsAttribute::m_relaxations
	int32_t ___m_relaxations_0;

public:
	inline static int32_t get_offset_of_m_relaxations_0() { return static_cast<int32_t>(offsetof(CompilationRelaxationsAttribute_t661FDDC06629BDA607A42BD660944F039FE03AFF, ___m_relaxations_0)); }
	inline int32_t get_m_relaxations_0() const { return ___m_relaxations_0; }
	inline int32_t* get_address_of_m_relaxations_0() { return &___m_relaxations_0; }
	inline void set_m_relaxations_0(int32_t value)
	{
		___m_relaxations_0 = value;
	}
};


// System.Runtime.CompilerServices.CompilerGeneratedAttribute
struct CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:

public:
};


// System.Diagnostics.DebuggerHiddenAttribute
struct DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:

public:
};


// System.Enum
struct Enum_t23B90B40F60E677A8025267341651C94AE079CDA  : public ValueType_tDBF999C1B75C48C68621878250DBF6CDBCF51E52
{
public:

public:
};

struct Enum_t23B90B40F60E677A8025267341651C94AE079CDA_StaticFields
{
public:
	// System.Char[] System.Enum::enumSeperatorCharArray
	CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34* ___enumSeperatorCharArray_0;

public:
	inline static int32_t get_offset_of_enumSeperatorCharArray_0() { return static_cast<int32_t>(offsetof(Enum_t23B90B40F60E677A8025267341651C94AE079CDA_StaticFields, ___enumSeperatorCharArray_0)); }
	inline CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34* get_enumSeperatorCharArray_0() const { return ___enumSeperatorCharArray_0; }
	inline CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34** get_address_of_enumSeperatorCharArray_0() { return &___enumSeperatorCharArray_0; }
	inline void set_enumSeperatorCharArray_0(CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34* value)
	{
		___enumSeperatorCharArray_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___enumSeperatorCharArray_0), (void*)value);
	}
};

// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t23B90B40F60E677A8025267341651C94AE079CDA_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t23B90B40F60E677A8025267341651C94AE079CDA_marshaled_com
{
};

// UnityEngine.HelpURLAttribute
struct HelpURLAttribute_t0924A6D83FABA7B77780F7F9BBCBCB9E0EA15023  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.String UnityEngine.HelpURLAttribute::m_Url
	String_t* ___m_Url_0;
	// System.Boolean UnityEngine.HelpURLAttribute::m_Dispatcher
	bool ___m_Dispatcher_1;
	// System.String UnityEngine.HelpURLAttribute::m_DispatchingFieldName
	String_t* ___m_DispatchingFieldName_2;

public:
	inline static int32_t get_offset_of_m_Url_0() { return static_cast<int32_t>(offsetof(HelpURLAttribute_t0924A6D83FABA7B77780F7F9BBCBCB9E0EA15023, ___m_Url_0)); }
	inline String_t* get_m_Url_0() const { return ___m_Url_0; }
	inline String_t** get_address_of_m_Url_0() { return &___m_Url_0; }
	inline void set_m_Url_0(String_t* value)
	{
		___m_Url_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Url_0), (void*)value);
	}

	inline static int32_t get_offset_of_m_Dispatcher_1() { return static_cast<int32_t>(offsetof(HelpURLAttribute_t0924A6D83FABA7B77780F7F9BBCBCB9E0EA15023, ___m_Dispatcher_1)); }
	inline bool get_m_Dispatcher_1() const { return ___m_Dispatcher_1; }
	inline bool* get_address_of_m_Dispatcher_1() { return &___m_Dispatcher_1; }
	inline void set_m_Dispatcher_1(bool value)
	{
		___m_Dispatcher_1 = value;
	}

	inline static int32_t get_offset_of_m_DispatchingFieldName_2() { return static_cast<int32_t>(offsetof(HelpURLAttribute_t0924A6D83FABA7B77780F7F9BBCBCB9E0EA15023, ___m_DispatchingFieldName_2)); }
	inline String_t* get_m_DispatchingFieldName_2() const { return ___m_DispatchingFieldName_2; }
	inline String_t** get_address_of_m_DispatchingFieldName_2() { return &___m_DispatchingFieldName_2; }
	inline void set_m_DispatchingFieldName_2(String_t* value)
	{
		___m_DispatchingFieldName_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_DispatchingFieldName_2), (void*)value);
	}
};


// UnityEngine.HideInInspector
struct HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:

public:
};


// UnityEngine.PropertyAttribute
struct PropertyAttribute_t4A352471DF625C56C811E27AC86B7E1CE6444052  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:

public:
};


// System.Runtime.CompilerServices.RuntimeCompatibilityAttribute
struct RuntimeCompatibilityAttribute_tFF99AB2963098F9CBCD47A20D9FD3D51C17C1C80  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.Boolean System.Runtime.CompilerServices.RuntimeCompatibilityAttribute::m_wrapNonExceptionThrows
	bool ___m_wrapNonExceptionThrows_0;

public:
	inline static int32_t get_offset_of_m_wrapNonExceptionThrows_0() { return static_cast<int32_t>(offsetof(RuntimeCompatibilityAttribute_tFF99AB2963098F9CBCD47A20D9FD3D51C17C1C80, ___m_wrapNonExceptionThrows_0)); }
	inline bool get_m_wrapNonExceptionThrows_0() const { return ___m_wrapNonExceptionThrows_0; }
	inline bool* get_address_of_m_wrapNonExceptionThrows_0() { return &___m_wrapNonExceptionThrows_0; }
	inline void set_m_wrapNonExceptionThrows_0(bool value)
	{
		___m_wrapNonExceptionThrows_0 = value;
	}
};


// UnityEngine.SerializeField
struct SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:

public:
};


// System.Void
struct Void_t700C6383A2A510C2CF4DD86DABD5CA9FF70ADAC5 
{
public:
	union
	{
		struct
		{
		};
		uint8_t Void_t700C6383A2A510C2CF4DD86DABD5CA9FF70ADAC5__padding[1];
	};

public:
};


// UnityEngine.RangeAttribute
struct RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5  : public PropertyAttribute_t4A352471DF625C56C811E27AC86B7E1CE6444052
{
public:
	// System.Single UnityEngine.RangeAttribute::min
	float ___min_0;
	// System.Single UnityEngine.RangeAttribute::max
	float ___max_1;

public:
	inline static int32_t get_offset_of_min_0() { return static_cast<int32_t>(offsetof(RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5, ___min_0)); }
	inline float get_min_0() const { return ___min_0; }
	inline float* get_address_of_min_0() { return &___min_0; }
	inline void set_min_0(float value)
	{
		___min_0 = value;
	}

	inline static int32_t get_offset_of_max_1() { return static_cast<int32_t>(offsetof(RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5, ___max_1)); }
	inline float get_max_1() const { return ___max_1; }
	inline float* get_address_of_max_1() { return &___max_1; }
	inline void set_max_1(float value)
	{
		___max_1 = value;
	}
};


// System.Diagnostics.DebuggableAttribute/DebuggingModes
struct DebuggingModes_t279D5B9C012ABA935887CB73C5A63A1F46AF08A8 
{
public:
	// System.Int32 System.Diagnostics.DebuggableAttribute/DebuggingModes::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(DebuggingModes_t279D5B9C012ABA935887CB73C5A63A1F46AF08A8, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// System.Diagnostics.DebuggableAttribute
struct DebuggableAttribute_tA8054EBD0FC7511695D494B690B5771658E3191B  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.Diagnostics.DebuggableAttribute/DebuggingModes System.Diagnostics.DebuggableAttribute::m_debuggingModes
	int32_t ___m_debuggingModes_0;

public:
	inline static int32_t get_offset_of_m_debuggingModes_0() { return static_cast<int32_t>(offsetof(DebuggableAttribute_tA8054EBD0FC7511695D494B690B5771658E3191B, ___m_debuggingModes_0)); }
	inline int32_t get_m_debuggingModes_0() const { return ___m_debuggingModes_0; }
	inline int32_t* get_address_of_m_debuggingModes_0() { return &___m_debuggingModes_0; }
	inline void set_m_debuggingModes_0(int32_t value)
	{
		___m_debuggingModes_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif



// System.Void System.Diagnostics.DebuggableAttribute::.ctor(System.Diagnostics.DebuggableAttribute/DebuggingModes)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void DebuggableAttribute__ctor_m7FF445C8435494A4847123A668D889E692E55550 (DebuggableAttribute_tA8054EBD0FC7511695D494B690B5771658E3191B * __this, int32_t ___modes0, const RuntimeMethod* method);
// System.Void System.Runtime.CompilerServices.CompilationRelaxationsAttribute::.ctor(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void CompilationRelaxationsAttribute__ctor_mAC3079EBC4EEAB474EED8208EF95DB39C922333B (CompilationRelaxationsAttribute_t661FDDC06629BDA607A42BD660944F039FE03AFF * __this, int32_t ___relaxations0, const RuntimeMethod* method);
// System.Void System.Runtime.CompilerServices.RuntimeCompatibilityAttribute::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void RuntimeCompatibilityAttribute__ctor_m551DDF1438CE97A984571949723F30F44CF7317C (RuntimeCompatibilityAttribute_tFF99AB2963098F9CBCD47A20D9FD3D51C17C1C80 * __this, const RuntimeMethod* method);
// System.Void System.Runtime.CompilerServices.RuntimeCompatibilityAttribute::set_WrapNonExceptionThrows(System.Boolean)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void RuntimeCompatibilityAttribute_set_WrapNonExceptionThrows_m8562196F90F3EBCEC23B5708EE0332842883C490_inline (RuntimeCompatibilityAttribute_tFF99AB2963098F9CBCD47A20D9FD3D51C17C1C80 * __this, bool ___value0, const RuntimeMethod* method);
// System.Void UnityEngine.SerializeField::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3 (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * __this, const RuntimeMethod* method);
// System.Void UnityEngine.HelpURLAttribute::.ctor(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void HelpURLAttribute__ctor_mCC837CE7900738F3152D585D454A34A15793C215 (HelpURLAttribute_t0924A6D83FABA7B77780F7F9BBCBCB9E0EA15023 * __this, String_t* ___url0, const RuntimeMethod* method);
// System.Void UnityEngine.AddComponentMenu::.ctor(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AddComponentMenu__ctor_m34CE7BDF93FA607429964AEF1D23436963EE8549 (AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 * __this, String_t* ___menuName0, const RuntimeMethod* method);
// System.Void UnityEngine.RangeAttribute::.ctor(System.Single,System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void RangeAttribute__ctor_mC74D39A9F20DD2A0D4174F05785ABE4F0DAEF000 (RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5 * __this, float ___min0, float ___max1, const RuntimeMethod* method);
// System.Void System.Runtime.CompilerServices.CompilerGeneratedAttribute::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35 (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * __this, const RuntimeMethod* method);
// System.Void System.Diagnostics.DebuggerHiddenAttribute::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3 (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * __this, const RuntimeMethod* method);
// System.Void UnityEngine.HideInInspector::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void HideInInspector__ctor_mE2B7FB1D206A74BA583C7812CDB4EBDD83EB66F9 (HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA * __this, const RuntimeMethod* method);
static void SALSAU2DLipSync_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		DebuggableAttribute_tA8054EBD0FC7511695D494B690B5771658E3191B * tmp = (DebuggableAttribute_tA8054EBD0FC7511695D494B690B5771658E3191B *)cache->attributes[0];
		DebuggableAttribute__ctor_m7FF445C8435494A4847123A668D889E692E55550(tmp, 2LL, NULL);
	}
	{
		CompilationRelaxationsAttribute_t661FDDC06629BDA607A42BD660944F039FE03AFF * tmp = (CompilationRelaxationsAttribute_t661FDDC06629BDA607A42BD660944F039FE03AFF *)cache->attributes[1];
		CompilationRelaxationsAttribute__ctor_mAC3079EBC4EEAB474EED8208EF95DB39C922333B(tmp, 8LL, NULL);
	}
	{
		RuntimeCompatibilityAttribute_tFF99AB2963098F9CBCD47A20D9FD3D51C17C1C80 * tmp = (RuntimeCompatibilityAttribute_tFF99AB2963098F9CBCD47A20D9FD3D51C17C1C80 *)cache->attributes[2];
		RuntimeCompatibilityAttribute__ctor_m551DDF1438CE97A984571949723F30F44CF7317C(tmp, NULL);
		RuntimeCompatibilityAttribute_set_WrapNonExceptionThrows_m8562196F90F3EBCEC23B5708EE0332842883C490_inline(tmp, true, NULL);
	}
}
static void EmoteExpression_tF6094EDCD5755325F24A8E62718855398682273B_CustomAttributesCacheGenerator_expData(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void Emoter_tFC793B4C007B66C28BCA6AAED0087BB02C77B3A7_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		HelpURLAttribute_t0924A6D83FABA7B77780F7F9BBCBCB9E0EA15023 * tmp = (HelpURLAttribute_t0924A6D83FABA7B77780F7F9BBCBCB9E0EA15023 *)cache->attributes[0];
		HelpURLAttribute__ctor_mCC837CE7900738F3152D585D454A34A15793C215(tmp, il2cpp_codegen_string_new_wrapper("\x68\x74\x74\x70\x73\x3A\x2F\x2F\x63\x72\x61\x7A\x79\x6D\x69\x6E\x6E\x6F\x77\x73\x74\x75\x64\x69\x6F\x2E\x63\x6F\x6D\x2F\x64\x6F\x63\x73\x2F\x73\x61\x6C\x73\x61\x2D\x6C\x69\x70\x2D\x73\x79\x6E\x63\x2F\x6D\x6F\x64\x75\x6C\x65\x73\x2F\x65\x6D\x6F\x74\x65\x72\x2F\x6F\x76\x65\x72\x76\x69\x65\x77\x2F"), NULL);
	}
	{
		AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 * tmp = (AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 *)cache->attributes[1];
		AddComponentMenu__ctor_m34CE7BDF93FA607429964AEF1D23436963EE8549(tmp, il2cpp_codegen_string_new_wrapper("\x43\x72\x61\x7A\x79\x20\x4D\x69\x6E\x6E\x6F\x77\x20\x53\x74\x75\x64\x69\x6F\x2F\x53\x41\x4C\x53\x41\x20\x4C\x69\x70\x53\x79\x6E\x63\x2F\x45\x6D\x6F\x74\x65\x52"), NULL);
	}
}
static void Emoter_tFC793B4C007B66C28BCA6AAED0087BB02C77B3A7_CustomAttributesCacheGenerator_lipsyncEmphasisChance(CustomAttributesCache* cache)
{
	{
		RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5 * tmp = (RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5 *)cache->attributes[0];
		RangeAttribute__ctor_mC74D39A9F20DD2A0D4174F05785ABE4F0DAEF000(tmp, 0.0f, 1.0f, NULL);
	}
}
static void Emoter_tFC793B4C007B66C28BCA6AAED0087BB02C77B3A7_CustomAttributesCacheGenerator_numRandomEmotesPerCycle(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void Emoter_tFC793B4C007B66C28BCA6AAED0087BB02C77B3A7_CustomAttributesCacheGenerator_numRandomEmphasizersPerCycle(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void Emoter_tFC793B4C007B66C28BCA6AAED0087BB02C77B3A7_CustomAttributesCacheGenerator_randomChance(CustomAttributesCache* cache)
{
	{
		RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5 * tmp = (RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5 *)cache->attributes[0];
		RangeAttribute__ctor_mC74D39A9F20DD2A0D4174F05785ABE4F0DAEF000(tmp, 0.0f, 1.0f, NULL);
	}
}
static void Emoter_tFC793B4C007B66C28BCA6AAED0087BB02C77B3A7_CustomAttributesCacheGenerator_randomFracBias(CustomAttributesCache* cache)
{
	{
		RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5 * tmp = (RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5 *)cache->attributes[0];
		RangeAttribute__ctor_mC74D39A9F20DD2A0D4174F05785ABE4F0DAEF000(tmp, 0.0f, 1.0f, NULL);
	}
}
static void U3CU3Ec_tB9501E27FA81BD46027CE788548D403ADEA2CE33_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ExpressionComponent_tBDE3C641F6D6E0CC47EBBC64234FB5E0731FA58D_CustomAttributesCacheGenerator_controller(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void Expression_t3BC18525F3B0285F91AB91E0AFAEEEE50337742D_CustomAttributesCacheGenerator_controllerVars(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void EventController_t31CB3636E0FBDC77D073403BE5888809141D18E4_CustomAttributesCacheGenerator_AnimationStarting(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void EventController_t31CB3636E0FBDC77D073403BE5888809141D18E4_CustomAttributesCacheGenerator_AnimationON(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void EventController_t31CB3636E0FBDC77D073403BE5888809141D18E4_CustomAttributesCacheGenerator_AnimationEnding(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void EventController_t31CB3636E0FBDC77D073403BE5888809141D18E4_CustomAttributesCacheGenerator_AnimationOFF(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void EventController_t31CB3636E0FBDC77D073403BE5888809141D18E4_CustomAttributesCacheGenerator_EventController_add_AnimationStarting_m908C545B38039320ED2221AD756581D403375232(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void EventController_t31CB3636E0FBDC77D073403BE5888809141D18E4_CustomAttributesCacheGenerator_EventController_remove_AnimationStarting_m704888C40478456DE937C3C15734769D53145BC7(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void EventController_t31CB3636E0FBDC77D073403BE5888809141D18E4_CustomAttributesCacheGenerator_EventController_add_AnimationON_m07523A57BF2CC2946FBFBD068471318F6D3FC3AA(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void EventController_t31CB3636E0FBDC77D073403BE5888809141D18E4_CustomAttributesCacheGenerator_EventController_remove_AnimationON_mD1156CA7CF26402A66E2CAF634A79180C2A0D59D(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void EventController_t31CB3636E0FBDC77D073403BE5888809141D18E4_CustomAttributesCacheGenerator_EventController_add_AnimationEnding_m9D5D7FC781857025A6E8AF4E3CF426AC55F01A61(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void EventController_t31CB3636E0FBDC77D073403BE5888809141D18E4_CustomAttributesCacheGenerator_EventController_remove_AnimationEnding_m8BE943425939845D6A70FB81E3753E1605A49B65(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void EventController_t31CB3636E0FBDC77D073403BE5888809141D18E4_CustomAttributesCacheGenerator_EventController_add_AnimationOFF_mB77661AE7AC1D3CB1537781A157FFAF8F3636931(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void EventController_t31CB3636E0FBDC77D073403BE5888809141D18E4_CustomAttributesCacheGenerator_EventController_remove_AnimationOFF_mC19633B977DD79655421316685D952ECB6BDDCD9(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void Eyes_t9D4DEC0898CBC8573E7CC918D328AE67C8A7D1A2_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		HelpURLAttribute_t0924A6D83FABA7B77780F7F9BBCBCB9E0EA15023 * tmp = (HelpURLAttribute_t0924A6D83FABA7B77780F7F9BBCBCB9E0EA15023 *)cache->attributes[0];
		HelpURLAttribute__ctor_mCC837CE7900738F3152D585D454A34A15793C215(tmp, il2cpp_codegen_string_new_wrapper("\x68\x74\x74\x70\x73\x3A\x2F\x2F\x63\x72\x61\x7A\x79\x6D\x69\x6E\x6E\x6F\x77\x73\x74\x75\x64\x69\x6F\x2E\x63\x6F\x6D\x2F\x64\x6F\x63\x73\x2F\x73\x61\x6C\x73\x61\x2D\x6C\x69\x70\x2D\x73\x79\x6E\x63\x2F\x6D\x6F\x64\x75\x6C\x65\x73\x2F\x65\x79\x65\x73\x2F\x6F\x76\x65\x72\x76\x69\x65\x77\x2F"), NULL);
	}
	{
		AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 * tmp = (AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 *)cache->attributes[1];
		AddComponentMenu__ctor_m34CE7BDF93FA607429964AEF1D23436963EE8549(tmp, il2cpp_codegen_string_new_wrapper("\x43\x72\x61\x7A\x79\x20\x4D\x69\x6E\x6E\x6F\x77\x20\x53\x74\x75\x64\x69\x6F\x2F\x53\x41\x4C\x53\x41\x20\x4C\x69\x70\x53\x79\x6E\x63\x2F\x45\x79\x65\x73"), NULL);
	}
}
static void Eyes_t9D4DEC0898CBC8573E7CC918D328AE67C8A7D1A2_CustomAttributesCacheGenerator_eyelidPercentEyes(CustomAttributesCache* cache)
{
	{
		RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5 * tmp = (RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5 *)cache->attributes[0];
		RangeAttribute__ctor_mC74D39A9F20DD2A0D4174F05785ABE4F0DAEF000(tmp, 0.0f, 1.0f, NULL);
	}
}
static void EyesExpression_t1F268E78F13B5C0B6D5B584077A784046486EAAD_CustomAttributesCacheGenerator_expData(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void LipsyncExpression_tAB7A8AF1E1C59432BC0E1E26945CD564CBF5040A_CustomAttributesCacheGenerator_expData(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void QueueProcessor_tEC2A0810C6DC63E05620372C02CDF8B75306CC45_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		HelpURLAttribute_t0924A6D83FABA7B77780F7F9BBCBCB9E0EA15023 * tmp = (HelpURLAttribute_t0924A6D83FABA7B77780F7F9BBCBCB9E0EA15023 *)cache->attributes[0];
		HelpURLAttribute__ctor_mCC837CE7900738F3152D585D454A34A15793C215(tmp, il2cpp_codegen_string_new_wrapper("\x68\x74\x74\x70\x73\x3A\x2F\x2F\x63\x72\x61\x7A\x79\x6D\x69\x6E\x6E\x6F\x77\x73\x74\x75\x64\x69\x6F\x2E\x63\x6F\x6D\x2F\x64\x6F\x63\x73\x2F\x73\x61\x6C\x73\x61\x2D\x6C\x69\x70\x2D\x73\x79\x6E\x63\x2F\x6D\x6F\x64\x75\x6C\x65\x73\x2F\x66\x75\x72\x74\x68\x65\x72\x2D\x72\x65\x61\x64\x69\x6E\x67\x2F\x71\x75\x65\x75\x65\x2D\x70\x72\x6F\x63\x65\x73\x73\x6F\x72\x2F"), NULL);
	}
}
static void QueueProcessor_tEC2A0810C6DC63E05620372C02CDF8B75306CC45_CustomAttributesCacheGenerator_ScaledTimeResumed(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void QueueProcessor_tEC2A0810C6DC63E05620372C02CDF8B75306CC45_CustomAttributesCacheGenerator_inspHeight(CustomAttributesCache* cache)
{
	{
		RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5 * tmp = (RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5 *)cache->attributes[0];
		RangeAttribute__ctor_mC74D39A9F20DD2A0D4174F05785ABE4F0DAEF000(tmp, 100.0f, 500.0f, NULL);
	}
}
static void QueueProcessor_tEC2A0810C6DC63E05620372C02CDF8B75306CC45_CustomAttributesCacheGenerator_QueueProcessor_add_ScaledTimeResumed_mA8B068661DFB759716F4E7C4CA9ED503D0EAFB36(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void QueueProcessor_tEC2A0810C6DC63E05620372C02CDF8B75306CC45_CustomAttributesCacheGenerator_QueueProcessor_remove_ScaledTimeResumed_m9A56C22AF3ED793523E0ED06F343C5931119F26D(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void Salsa_t74E5A82755E5E59F703B221CFB917DEB4B770D4B_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		HelpURLAttribute_t0924A6D83FABA7B77780F7F9BBCBCB9E0EA15023 * tmp = (HelpURLAttribute_t0924A6D83FABA7B77780F7F9BBCBCB9E0EA15023 *)cache->attributes[0];
		HelpURLAttribute__ctor_mCC837CE7900738F3152D585D454A34A15793C215(tmp, il2cpp_codegen_string_new_wrapper("\x68\x74\x74\x70\x73\x3A\x2F\x2F\x63\x72\x61\x7A\x79\x6D\x69\x6E\x6E\x6F\x77\x73\x74\x75\x64\x69\x6F\x2E\x63\x6F\x6D\x2F\x64\x6F\x63\x73\x2F\x73\x61\x6C\x73\x61\x2D\x6C\x69\x70\x2D\x73\x79\x6E\x63\x2F\x6D\x6F\x64\x75\x6C\x65\x73\x2F\x73\x61\x6C\x73\x61\x2F\x75\x73\x69\x6E\x67\x2F"), NULL);
	}
	{
		AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 * tmp = (AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 *)cache->attributes[1];
		AddComponentMenu__ctor_m34CE7BDF93FA607429964AEF1D23436963EE8549(tmp, il2cpp_codegen_string_new_wrapper("\x43\x72\x61\x7A\x79\x20\x4D\x69\x6E\x6E\x6F\x77\x20\x53\x74\x75\x64\x69\x6F\x2F\x53\x41\x4C\x53\x41\x20\x4C\x69\x70\x53\x79\x6E\x63\x2F\x53\x41\x4C\x53\x41"), NULL);
	}
}
static void Salsa_t74E5A82755E5E59F703B221CFB917DEB4B770D4B_CustomAttributesCacheGenerator_emphasizerTrigger(CustomAttributesCache* cache)
{
	{
		RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5 * tmp = (RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5 *)cache->attributes[0];
		RangeAttribute__ctor_mC74D39A9F20DD2A0D4174F05785ABE4F0DAEF000(tmp, 0.0f, 1.0f, NULL);
	}
}
static void Salsa_t74E5A82755E5E59F703B221CFB917DEB4B770D4B_CustomAttributesCacheGenerator_playheadBias(CustomAttributesCache* cache)
{
	{
		RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5 * tmp = (RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5 *)cache->attributes[0];
		RangeAttribute__ctor_mC74D39A9F20DD2A0D4174F05785ABE4F0DAEF000(tmp, -2048.0f, 9048.0f, NULL);
	}
}
static void Salsa_t74E5A82755E5E59F703B221CFB917DEB4B770D4B_CustomAttributesCacheGenerator_advDynPrimaryBias(CustomAttributesCache* cache)
{
	{
		RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5 * tmp = (RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5 *)cache->attributes[0];
		RangeAttribute__ctor_mC74D39A9F20DD2A0D4174F05785ABE4F0DAEF000(tmp, 0.0f, 1.0f, NULL);
	}
}
static void Salsa_t74E5A82755E5E59F703B221CFB917DEB4B770D4B_CustomAttributesCacheGenerator_advDynJitterAmount(CustomAttributesCache* cache)
{
	{
		RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5 * tmp = (RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5 *)cache->attributes[0];
		RangeAttribute__ctor_mC74D39A9F20DD2A0D4174F05785ABE4F0DAEF000(tmp, 0.0f, 1.0f, NULL);
	}
}
static void Salsa_t74E5A82755E5E59F703B221CFB917DEB4B770D4B_CustomAttributesCacheGenerator_advDynJitterProb(CustomAttributesCache* cache)
{
	{
		RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5 * tmp = (RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5 *)cache->attributes[0];
		RangeAttribute__ctor_mC74D39A9F20DD2A0D4174F05785ABE4F0DAEF000(tmp, 0.0f, 1.0f, NULL);
	}
}
static void Salsa_t74E5A82755E5E59F703B221CFB917DEB4B770D4B_CustomAttributesCacheGenerator_advDynSecondaryMix(CustomAttributesCache* cache)
{
	{
		RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5 * tmp = (RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5 *)cache->attributes[0];
		RangeAttribute__ctor_mC74D39A9F20DD2A0D4174F05785ABE4F0DAEF000(tmp, 0.0f, 1.0f, NULL);
	}
}
static void Salsa_t74E5A82755E5E59F703B221CFB917DEB4B770D4B_CustomAttributesCacheGenerator_advDynRollback(CustomAttributesCache* cache)
{
	{
		RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5 * tmp = (RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5 *)cache->attributes[0];
		RangeAttribute__ctor_mC74D39A9F20DD2A0D4174F05785ABE4F0DAEF000(tmp, 0.0f, 1.0f, NULL);
	}
}
static void Salsa_t74E5A82755E5E59F703B221CFB917DEB4B770D4B_CustomAttributesCacheGenerator_globalFrac(CustomAttributesCache* cache)
{
	{
		RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5 * tmp = (RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5 *)cache->attributes[0];
		RangeAttribute__ctor_mC74D39A9F20DD2A0D4174F05785ABE4F0DAEF000(tmp, 0.0f, 1.0f, NULL);
	}
}
static void Salsa_t74E5A82755E5E59F703B221CFB917DEB4B770D4B_CustomAttributesCacheGenerator_StartedSalsaing(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void Salsa_t74E5A82755E5E59F703B221CFB917DEB4B770D4B_CustomAttributesCacheGenerator_StoppedSalsaing(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void Salsa_t74E5A82755E5E59F703B221CFB917DEB4B770D4B_CustomAttributesCacheGenerator_VisemeTriggered(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void Salsa_t74E5A82755E5E59F703B221CFB917DEB4B770D4B_CustomAttributesCacheGenerator_Salsa_add_StartedSalsaing_mDFA1FF3D0181546C7DD01FAF22A3F7207C089C46(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void Salsa_t74E5A82755E5E59F703B221CFB917DEB4B770D4B_CustomAttributesCacheGenerator_Salsa_remove_StartedSalsaing_mD8CB3F704734628D926C9687057000079716A55B(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void Salsa_t74E5A82755E5E59F703B221CFB917DEB4B770D4B_CustomAttributesCacheGenerator_Salsa_add_StoppedSalsaing_m41629779A7EEE50D9E884F0D5FE0D78E2216B708(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void Salsa_t74E5A82755E5E59F703B221CFB917DEB4B770D4B_CustomAttributesCacheGenerator_Salsa_remove_StoppedSalsaing_m2C6E8AB8478DFEDFF2F43068690708DB82F747E5(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void Salsa_t74E5A82755E5E59F703B221CFB917DEB4B770D4B_CustomAttributesCacheGenerator_Salsa_add_VisemeTriggered_mE59483C9050A7539895DEA66032BF24F647D6F76(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void Salsa_t74E5A82755E5E59F703B221CFB917DEB4B770D4B_CustomAttributesCacheGenerator_Salsa_remove_VisemeTriggered_m041889BB2A473C46AFE1B8656B096F07D5FDE9EE(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CWaitForAudioU3Ed__137_t3FA2DAEDB59FA79BD458A60D2C189508A32DC7B9_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CWaitForAudioU3Ed__137_t3FA2DAEDB59FA79BD458A60D2C189508A32DC7B9_CustomAttributesCacheGenerator_U3CWaitForAudioU3Ed__137__ctor_m1D71147267D4201048A9C123160435F0D866A105(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CWaitForAudioU3Ed__137_t3FA2DAEDB59FA79BD458A60D2C189508A32DC7B9_CustomAttributesCacheGenerator_U3CWaitForAudioU3Ed__137_System_IDisposable_Dispose_m732FE97FB775FFC976EED92E599D9F129ECEA5B8(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CWaitForAudioU3Ed__137_t3FA2DAEDB59FA79BD458A60D2C189508A32DC7B9_CustomAttributesCacheGenerator_U3CWaitForAudioU3Ed__137_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mDD5B862935F825A39979DD881F5964EF35116076(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CWaitForAudioU3Ed__137_t3FA2DAEDB59FA79BD458A60D2C189508A32DC7B9_CustomAttributesCacheGenerator_U3CWaitForAudioU3Ed__137_System_Collections_IEnumerator_Reset_m4898E98876DCC0D2212C9EDB48BCA09F610BA98E(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CWaitForAudioU3Ed__137_t3FA2DAEDB59FA79BD458A60D2C189508A32DC7B9_CustomAttributesCacheGenerator_U3CWaitForAudioU3Ed__137_System_Collections_IEnumerator_get_Current_m0C034B4D68F886BE2323DE31E0008D5751073216(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CU3Ec_t5517ECA60FDBD8585D15B04231337343BF49ACE2_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CWaitForSalsaU3Ed__145_t6FA3AF78AF3C7192794681174044B1B3A8D9A324_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CWaitForSalsaU3Ed__145_t6FA3AF78AF3C7192794681174044B1B3A8D9A324_CustomAttributesCacheGenerator_U3CWaitForSalsaU3Ed__145__ctor_m126F324BEDE44B30B2CA4F9BBD042E94FD5A4ED4(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CWaitForSalsaU3Ed__145_t6FA3AF78AF3C7192794681174044B1B3A8D9A324_CustomAttributesCacheGenerator_U3CWaitForSalsaU3Ed__145_System_IDisposable_Dispose_m825925E43BB47FAC7DF2D600184612C0BD3B4B52(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CWaitForSalsaU3Ed__145_t6FA3AF78AF3C7192794681174044B1B3A8D9A324_CustomAttributesCacheGenerator_U3CWaitForSalsaU3Ed__145_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mA9437B3B4753AAEDB3B1B09479EB1CAF33381566(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CWaitForSalsaU3Ed__145_t6FA3AF78AF3C7192794681174044B1B3A8D9A324_CustomAttributesCacheGenerator_U3CWaitForSalsaU3Ed__145_System_Collections_IEnumerator_Reset_m41551491DB1233942B8AE989BF00395A93F3BBEE(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CWaitForSalsaU3Ed__145_t6FA3AF78AF3C7192794681174044B1B3A8D9A324_CustomAttributesCacheGenerator_U3CWaitForSalsaU3Ed__145_System_Collections_IEnumerator_get_Current_m7F7CA9D1302F69C6174C8DA613ACFD637FA05126(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void SalsaAdvancedDynamicsSilenceAnalyzer_tD480EF53C45D26A6AEEC1B0F43760E5CCD29F7B1_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 * tmp = (AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 *)cache->attributes[0];
		AddComponentMenu__ctor_m34CE7BDF93FA607429964AEF1D23436963EE8549(tmp, il2cpp_codegen_string_new_wrapper("\x43\x72\x61\x7A\x79\x20\x4D\x69\x6E\x6E\x6F\x77\x20\x53\x74\x75\x64\x69\x6F\x2F\x53\x41\x4C\x53\x41\x20\x4C\x69\x70\x53\x79\x6E\x63\x2F\x41\x64\x64\x2D\x6F\x6E\x73\x2F\x53\x69\x6C\x65\x6E\x63\x65\x20\x41\x6E\x61\x6C\x79\x7A\x65\x72"), NULL);
	}
	{
		HelpURLAttribute_t0924A6D83FABA7B77780F7F9BBCBCB9E0EA15023 * tmp = (HelpURLAttribute_t0924A6D83FABA7B77780F7F9BBCBCB9E0EA15023 *)cache->attributes[1];
		HelpURLAttribute__ctor_mCC837CE7900738F3152D585D454A34A15793C215(tmp, il2cpp_codegen_string_new_wrapper("\x68\x74\x74\x70\x73\x3A\x2F\x2F\x63\x72\x61\x7A\x79\x6D\x69\x6E\x6E\x6F\x77\x73\x74\x75\x64\x69\x6F\x2E\x63\x6F\x6D\x2F\x64\x6F\x63\x73\x2F\x73\x61\x6C\x73\x61\x2D\x6C\x69\x70\x2D\x73\x79\x6E\x63\x2F\x6D\x6F\x64\x75\x6C\x65\x73\x2F\x73\x61\x6C\x73\x61\x2F\x61\x64\x76\x61\x6E\x63\x65\x64\x2D\x64\x79\x6E\x61\x6D\x69\x63\x73\x2D\x73\x69\x6C\x65\x6E\x63\x65\x2D\x61\x6E\x61\x6C\x79\x7A\x65\x72\x2F"), NULL);
	}
}
static void SalsaAdvancedDynamicsSilenceAnalyzer_tD480EF53C45D26A6AEEC1B0F43760E5CCD29F7B1_CustomAttributesCacheGenerator_silenceThreshold(CustomAttributesCache* cache)
{
	{
		RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5 * tmp = (RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5 *)cache->attributes[0];
		RangeAttribute__ctor_mC74D39A9F20DD2A0D4174F05785ABE4F0DAEF000(tmp, 0.0f, 1.0f, NULL);
	}
}
static void SalsaAdvancedDynamicsSilenceAnalyzer_tD480EF53C45D26A6AEEC1B0F43760E5CCD29F7B1_CustomAttributesCacheGenerator_timingStartPoint(CustomAttributesCache* cache)
{
	{
		RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5 * tmp = (RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5 *)cache->attributes[0];
		RangeAttribute__ctor_mC74D39A9F20DD2A0D4174F05785ABE4F0DAEF000(tmp, 0.0f, 1.0f, NULL);
	}
}
static void SalsaAdvancedDynamicsSilenceAnalyzer_tD480EF53C45D26A6AEEC1B0F43760E5CCD29F7B1_CustomAttributesCacheGenerator_timingEndVariance(CustomAttributesCache* cache)
{
	{
		RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5 * tmp = (RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5 *)cache->attributes[0];
		RangeAttribute__ctor_mC74D39A9F20DD2A0D4174F05785ABE4F0DAEF000(tmp, 0.0f, 1.0f, NULL);
	}
}
static void SalsaAdvancedDynamicsSilenceAnalyzer_tD480EF53C45D26A6AEEC1B0F43760E5CCD29F7B1_CustomAttributesCacheGenerator_silenceSampleWeight(CustomAttributesCache* cache)
{
	{
		RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5 * tmp = (RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5 *)cache->attributes[0];
		RangeAttribute__ctor_mC74D39A9F20DD2A0D4174F05785ABE4F0DAEF000(tmp, 0.0f, 1.0f, NULL);
	}
}
static void UmaUepProxy_t260F487A4B72402F6BC32D27EF2B7D342CAFB1E9_CustomAttributesCacheGenerator_poses(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void UmaUepProxy_t260F487A4B72402F6BC32D27EF2B7D342CAFB1E9_CustomAttributesCacheGenerator_isPreviewing(CustomAttributesCache* cache)
{
	{
		HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA * tmp = (HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA *)cache->attributes[0];
		HideInInspector__ctor_mE2B7FB1D206A74BA583C7812CDB4EBDD83EB66F9(tmp, NULL);
	}
}
IL2CPP_EXTERN_C const CustomAttributesCacheGenerator g_SALSAU2DLipSync_AttributeGenerators[];
const CustomAttributesCacheGenerator g_SALSAU2DLipSync_AttributeGenerators[70] = 
{
	Emoter_tFC793B4C007B66C28BCA6AAED0087BB02C77B3A7_CustomAttributesCacheGenerator,
	U3CU3Ec_tB9501E27FA81BD46027CE788548D403ADEA2CE33_CustomAttributesCacheGenerator,
	Eyes_t9D4DEC0898CBC8573E7CC918D328AE67C8A7D1A2_CustomAttributesCacheGenerator,
	QueueProcessor_tEC2A0810C6DC63E05620372C02CDF8B75306CC45_CustomAttributesCacheGenerator,
	Salsa_t74E5A82755E5E59F703B221CFB917DEB4B770D4B_CustomAttributesCacheGenerator,
	U3CWaitForAudioU3Ed__137_t3FA2DAEDB59FA79BD458A60D2C189508A32DC7B9_CustomAttributesCacheGenerator,
	U3CU3Ec_t5517ECA60FDBD8585D15B04231337343BF49ACE2_CustomAttributesCacheGenerator,
	U3CWaitForSalsaU3Ed__145_t6FA3AF78AF3C7192794681174044B1B3A8D9A324_CustomAttributesCacheGenerator,
	SalsaAdvancedDynamicsSilenceAnalyzer_tD480EF53C45D26A6AEEC1B0F43760E5CCD29F7B1_CustomAttributesCacheGenerator,
	EmoteExpression_tF6094EDCD5755325F24A8E62718855398682273B_CustomAttributesCacheGenerator_expData,
	Emoter_tFC793B4C007B66C28BCA6AAED0087BB02C77B3A7_CustomAttributesCacheGenerator_lipsyncEmphasisChance,
	Emoter_tFC793B4C007B66C28BCA6AAED0087BB02C77B3A7_CustomAttributesCacheGenerator_numRandomEmotesPerCycle,
	Emoter_tFC793B4C007B66C28BCA6AAED0087BB02C77B3A7_CustomAttributesCacheGenerator_numRandomEmphasizersPerCycle,
	Emoter_tFC793B4C007B66C28BCA6AAED0087BB02C77B3A7_CustomAttributesCacheGenerator_randomChance,
	Emoter_tFC793B4C007B66C28BCA6AAED0087BB02C77B3A7_CustomAttributesCacheGenerator_randomFracBias,
	ExpressionComponent_tBDE3C641F6D6E0CC47EBBC64234FB5E0731FA58D_CustomAttributesCacheGenerator_controller,
	Expression_t3BC18525F3B0285F91AB91E0AFAEEEE50337742D_CustomAttributesCacheGenerator_controllerVars,
	EventController_t31CB3636E0FBDC77D073403BE5888809141D18E4_CustomAttributesCacheGenerator_AnimationStarting,
	EventController_t31CB3636E0FBDC77D073403BE5888809141D18E4_CustomAttributesCacheGenerator_AnimationON,
	EventController_t31CB3636E0FBDC77D073403BE5888809141D18E4_CustomAttributesCacheGenerator_AnimationEnding,
	EventController_t31CB3636E0FBDC77D073403BE5888809141D18E4_CustomAttributesCacheGenerator_AnimationOFF,
	Eyes_t9D4DEC0898CBC8573E7CC918D328AE67C8A7D1A2_CustomAttributesCacheGenerator_eyelidPercentEyes,
	EyesExpression_t1F268E78F13B5C0B6D5B584077A784046486EAAD_CustomAttributesCacheGenerator_expData,
	LipsyncExpression_tAB7A8AF1E1C59432BC0E1E26945CD564CBF5040A_CustomAttributesCacheGenerator_expData,
	QueueProcessor_tEC2A0810C6DC63E05620372C02CDF8B75306CC45_CustomAttributesCacheGenerator_ScaledTimeResumed,
	QueueProcessor_tEC2A0810C6DC63E05620372C02CDF8B75306CC45_CustomAttributesCacheGenerator_inspHeight,
	Salsa_t74E5A82755E5E59F703B221CFB917DEB4B770D4B_CustomAttributesCacheGenerator_emphasizerTrigger,
	Salsa_t74E5A82755E5E59F703B221CFB917DEB4B770D4B_CustomAttributesCacheGenerator_playheadBias,
	Salsa_t74E5A82755E5E59F703B221CFB917DEB4B770D4B_CustomAttributesCacheGenerator_advDynPrimaryBias,
	Salsa_t74E5A82755E5E59F703B221CFB917DEB4B770D4B_CustomAttributesCacheGenerator_advDynJitterAmount,
	Salsa_t74E5A82755E5E59F703B221CFB917DEB4B770D4B_CustomAttributesCacheGenerator_advDynJitterProb,
	Salsa_t74E5A82755E5E59F703B221CFB917DEB4B770D4B_CustomAttributesCacheGenerator_advDynSecondaryMix,
	Salsa_t74E5A82755E5E59F703B221CFB917DEB4B770D4B_CustomAttributesCacheGenerator_advDynRollback,
	Salsa_t74E5A82755E5E59F703B221CFB917DEB4B770D4B_CustomAttributesCacheGenerator_globalFrac,
	Salsa_t74E5A82755E5E59F703B221CFB917DEB4B770D4B_CustomAttributesCacheGenerator_StartedSalsaing,
	Salsa_t74E5A82755E5E59F703B221CFB917DEB4B770D4B_CustomAttributesCacheGenerator_StoppedSalsaing,
	Salsa_t74E5A82755E5E59F703B221CFB917DEB4B770D4B_CustomAttributesCacheGenerator_VisemeTriggered,
	SalsaAdvancedDynamicsSilenceAnalyzer_tD480EF53C45D26A6AEEC1B0F43760E5CCD29F7B1_CustomAttributesCacheGenerator_silenceThreshold,
	SalsaAdvancedDynamicsSilenceAnalyzer_tD480EF53C45D26A6AEEC1B0F43760E5CCD29F7B1_CustomAttributesCacheGenerator_timingStartPoint,
	SalsaAdvancedDynamicsSilenceAnalyzer_tD480EF53C45D26A6AEEC1B0F43760E5CCD29F7B1_CustomAttributesCacheGenerator_timingEndVariance,
	SalsaAdvancedDynamicsSilenceAnalyzer_tD480EF53C45D26A6AEEC1B0F43760E5CCD29F7B1_CustomAttributesCacheGenerator_silenceSampleWeight,
	UmaUepProxy_t260F487A4B72402F6BC32D27EF2B7D342CAFB1E9_CustomAttributesCacheGenerator_poses,
	UmaUepProxy_t260F487A4B72402F6BC32D27EF2B7D342CAFB1E9_CustomAttributesCacheGenerator_isPreviewing,
	EventController_t31CB3636E0FBDC77D073403BE5888809141D18E4_CustomAttributesCacheGenerator_EventController_add_AnimationStarting_m908C545B38039320ED2221AD756581D403375232,
	EventController_t31CB3636E0FBDC77D073403BE5888809141D18E4_CustomAttributesCacheGenerator_EventController_remove_AnimationStarting_m704888C40478456DE937C3C15734769D53145BC7,
	EventController_t31CB3636E0FBDC77D073403BE5888809141D18E4_CustomAttributesCacheGenerator_EventController_add_AnimationON_m07523A57BF2CC2946FBFBD068471318F6D3FC3AA,
	EventController_t31CB3636E0FBDC77D073403BE5888809141D18E4_CustomAttributesCacheGenerator_EventController_remove_AnimationON_mD1156CA7CF26402A66E2CAF634A79180C2A0D59D,
	EventController_t31CB3636E0FBDC77D073403BE5888809141D18E4_CustomAttributesCacheGenerator_EventController_add_AnimationEnding_m9D5D7FC781857025A6E8AF4E3CF426AC55F01A61,
	EventController_t31CB3636E0FBDC77D073403BE5888809141D18E4_CustomAttributesCacheGenerator_EventController_remove_AnimationEnding_m8BE943425939845D6A70FB81E3753E1605A49B65,
	EventController_t31CB3636E0FBDC77D073403BE5888809141D18E4_CustomAttributesCacheGenerator_EventController_add_AnimationOFF_mB77661AE7AC1D3CB1537781A157FFAF8F3636931,
	EventController_t31CB3636E0FBDC77D073403BE5888809141D18E4_CustomAttributesCacheGenerator_EventController_remove_AnimationOFF_mC19633B977DD79655421316685D952ECB6BDDCD9,
	QueueProcessor_tEC2A0810C6DC63E05620372C02CDF8B75306CC45_CustomAttributesCacheGenerator_QueueProcessor_add_ScaledTimeResumed_mA8B068661DFB759716F4E7C4CA9ED503D0EAFB36,
	QueueProcessor_tEC2A0810C6DC63E05620372C02CDF8B75306CC45_CustomAttributesCacheGenerator_QueueProcessor_remove_ScaledTimeResumed_m9A56C22AF3ED793523E0ED06F343C5931119F26D,
	Salsa_t74E5A82755E5E59F703B221CFB917DEB4B770D4B_CustomAttributesCacheGenerator_Salsa_add_StartedSalsaing_mDFA1FF3D0181546C7DD01FAF22A3F7207C089C46,
	Salsa_t74E5A82755E5E59F703B221CFB917DEB4B770D4B_CustomAttributesCacheGenerator_Salsa_remove_StartedSalsaing_mD8CB3F704734628D926C9687057000079716A55B,
	Salsa_t74E5A82755E5E59F703B221CFB917DEB4B770D4B_CustomAttributesCacheGenerator_Salsa_add_StoppedSalsaing_m41629779A7EEE50D9E884F0D5FE0D78E2216B708,
	Salsa_t74E5A82755E5E59F703B221CFB917DEB4B770D4B_CustomAttributesCacheGenerator_Salsa_remove_StoppedSalsaing_m2C6E8AB8478DFEDFF2F43068690708DB82F747E5,
	Salsa_t74E5A82755E5E59F703B221CFB917DEB4B770D4B_CustomAttributesCacheGenerator_Salsa_add_VisemeTriggered_mE59483C9050A7539895DEA66032BF24F647D6F76,
	Salsa_t74E5A82755E5E59F703B221CFB917DEB4B770D4B_CustomAttributesCacheGenerator_Salsa_remove_VisemeTriggered_m041889BB2A473C46AFE1B8656B096F07D5FDE9EE,
	U3CWaitForAudioU3Ed__137_t3FA2DAEDB59FA79BD458A60D2C189508A32DC7B9_CustomAttributesCacheGenerator_U3CWaitForAudioU3Ed__137__ctor_m1D71147267D4201048A9C123160435F0D866A105,
	U3CWaitForAudioU3Ed__137_t3FA2DAEDB59FA79BD458A60D2C189508A32DC7B9_CustomAttributesCacheGenerator_U3CWaitForAudioU3Ed__137_System_IDisposable_Dispose_m732FE97FB775FFC976EED92E599D9F129ECEA5B8,
	U3CWaitForAudioU3Ed__137_t3FA2DAEDB59FA79BD458A60D2C189508A32DC7B9_CustomAttributesCacheGenerator_U3CWaitForAudioU3Ed__137_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mDD5B862935F825A39979DD881F5964EF35116076,
	U3CWaitForAudioU3Ed__137_t3FA2DAEDB59FA79BD458A60D2C189508A32DC7B9_CustomAttributesCacheGenerator_U3CWaitForAudioU3Ed__137_System_Collections_IEnumerator_Reset_m4898E98876DCC0D2212C9EDB48BCA09F610BA98E,
	U3CWaitForAudioU3Ed__137_t3FA2DAEDB59FA79BD458A60D2C189508A32DC7B9_CustomAttributesCacheGenerator_U3CWaitForAudioU3Ed__137_System_Collections_IEnumerator_get_Current_m0C034B4D68F886BE2323DE31E0008D5751073216,
	U3CWaitForSalsaU3Ed__145_t6FA3AF78AF3C7192794681174044B1B3A8D9A324_CustomAttributesCacheGenerator_U3CWaitForSalsaU3Ed__145__ctor_m126F324BEDE44B30B2CA4F9BBD042E94FD5A4ED4,
	U3CWaitForSalsaU3Ed__145_t6FA3AF78AF3C7192794681174044B1B3A8D9A324_CustomAttributesCacheGenerator_U3CWaitForSalsaU3Ed__145_System_IDisposable_Dispose_m825925E43BB47FAC7DF2D600184612C0BD3B4B52,
	U3CWaitForSalsaU3Ed__145_t6FA3AF78AF3C7192794681174044B1B3A8D9A324_CustomAttributesCacheGenerator_U3CWaitForSalsaU3Ed__145_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mA9437B3B4753AAEDB3B1B09479EB1CAF33381566,
	U3CWaitForSalsaU3Ed__145_t6FA3AF78AF3C7192794681174044B1B3A8D9A324_CustomAttributesCacheGenerator_U3CWaitForSalsaU3Ed__145_System_Collections_IEnumerator_Reset_m41551491DB1233942B8AE989BF00395A93F3BBEE,
	U3CWaitForSalsaU3Ed__145_t6FA3AF78AF3C7192794681174044B1B3A8D9A324_CustomAttributesCacheGenerator_U3CWaitForSalsaU3Ed__145_System_Collections_IEnumerator_get_Current_m7F7CA9D1302F69C6174C8DA613ACFD637FA05126,
	SALSAU2DLipSync_CustomAttributesCacheGenerator,
};
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void RuntimeCompatibilityAttribute_set_WrapNonExceptionThrows_m8562196F90F3EBCEC23B5708EE0332842883C490_inline (RuntimeCompatibilityAttribute_tFF99AB2963098F9CBCD47A20D9FD3D51C17C1C80 * __this, bool ___value0, const RuntimeMethod* method)
{
	{
		bool L_0 = ___value0;
		__this->set_m_wrapNonExceptionThrows_0(L_0);
		return;
	}
}
