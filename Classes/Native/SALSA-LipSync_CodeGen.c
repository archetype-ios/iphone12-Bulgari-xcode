﻿#include "pch-c.h"
#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include "codegen/il2cpp-codegen-metadata.h"





// 0x00000001 System.Void CrazyMinnow.SALSA.AxisFixed::SetReferences(CrazyMinnow.SALSA.AxisOffset,CrazyMinnow.SALSA.AxisFixed,CrazyMinnow.SALSA.AxisOriginal)
extern void AxisFixed_SetReferences_mA1AF87FE77DA7BAA19F3E84877CE03393A7AFA6D (void);
// 0x00000002 System.Void CrazyMinnow.SALSA.AxisFixed::.ctor()
extern void AxisFixed__ctor_m9EDD7E85F785839E94E0635F1D04F10899774DD8 (void);
// 0x00000003 System.Void CrazyMinnow.SALSA.AxisOffset::SetReferences(CrazyMinnow.SALSA.AxisOffset,CrazyMinnow.SALSA.AxisFixed,CrazyMinnow.SALSA.AxisOriginal)
extern void AxisOffset_SetReferences_mC0F07F9313DBF74C95303E0B6D9C8E3DC0537DE3 (void);
// 0x00000004 System.Void CrazyMinnow.SALSA.AxisOffset::.ctor()
extern void AxisOffset__ctor_m7C804A2606B1EE7FCE0AFFF77226FD5B4B858D48 (void);
// 0x00000005 System.Void CrazyMinnow.SALSA.AxisOriginal::SetReferences(CrazyMinnow.SALSA.AxisOffset,CrazyMinnow.SALSA.AxisFixed,CrazyMinnow.SALSA.AxisOriginal)
extern void AxisOriginal_SetReferences_m2DF4E257F30E2EE1DEB86EC2041627CA13259791 (void);
// 0x00000006 System.Void CrazyMinnow.SALSA.AxisOriginal::.ctor()
extern void AxisOriginal__ctor_m37E43138619FDB7A40176F92125210C37C0699E5 (void);
// 0x00000007 System.Void CrazyMinnow.SALSA.EmoteExpression::.ctor(System.String,CrazyMinnow.SALSA.InspectorControllerHelperData,System.Boolean,System.Boolean,System.Boolean,System.Single,CrazyMinnow.SALSA.EmoteRepeater/StartDelay,System.Boolean,System.Single,System.Boolean)
extern void EmoteExpression__ctor_mA00EB7A6C51D83ADF2FDD181F79789DB2F91E428 (void);
// 0x00000008 System.Void CrazyMinnow.SALSA.EmoteRepeater::.ctor(CrazyMinnow.SALSA.EmoteExpression,System.Single,System.Boolean)
extern void EmoteRepeater__ctor_m569328BA0721FEB69E5410B89D2DD7471B280827 (void);
// 0x00000009 System.Void CrazyMinnow.SALSA.EmoteRepeater::SetTimeCheck(System.Boolean)
extern void EmoteRepeater_SetTimeCheck_mA17530000659BB6FB39AF9037DA783EE450C287A (void);
// 0x0000000A System.Int32 CrazyMinnow.SALSA.Emoter::get_InspectorBasedRandomEmotesCount()
extern void Emoter_get_InspectorBasedRandomEmotesCount_mA1B0753F78E64DF548213DE31661EBE488AB92F1 (void);
// 0x0000000B System.Int32 CrazyMinnow.SALSA.Emoter::get_NumRandomEmotesPerCycle()
extern void Emoter_get_NumRandomEmotesPerCycle_m6CE23A849C265EFC641C673A22FB98E6D01CA479 (void);
// 0x0000000C System.Void CrazyMinnow.SALSA.Emoter::set_NumRandomEmotesPerCycle(System.Int32)
extern void Emoter_set_NumRandomEmotesPerCycle_m23497E301C46D7981312390878455D050ADB33D2 (void);
// 0x0000000D System.Int32 CrazyMinnow.SALSA.Emoter::get_InspectorBasedRandomEmphasizerCount()
extern void Emoter_get_InspectorBasedRandomEmphasizerCount_m4D7B063998A09BC979912E3BA18EB04BF40F2815 (void);
// 0x0000000E System.Int32 CrazyMinnow.SALSA.Emoter::get_NumRandomEmphasizersPerCycle()
extern void Emoter_get_NumRandomEmphasizersPerCycle_m2FEBDC5E7E5C73DF31F8F1EF15721032ED4730B5 (void);
// 0x0000000F System.Void CrazyMinnow.SALSA.Emoter::set_NumRandomEmphasizersPerCycle(System.Int32)
extern void Emoter_set_NumRandomEmphasizersPerCycle_m777300414867EBABDA0F80705295C05C0854627B (void);
// 0x00000010 System.Void CrazyMinnow.SALSA.Emoter::Awake()
extern void Emoter_Awake_m3C66F2BD9E6F4C796BDEB5438613378F5204C143 (void);
// 0x00000011 System.Void CrazyMinnow.SALSA.Emoter::OnEnable()
extern void Emoter_OnEnable_m58223892E85015B5882DD883E189EF9CE940C150 (void);
// 0x00000012 System.Void CrazyMinnow.SALSA.Emoter::OnScaledTimeResumed(System.Object,CrazyMinnow.SALSA.QueueProcessor/QueueProcessorNotificationArgs)
extern void Emoter_OnScaledTimeResumed_mB640AB8E66D34CA7217635119893430386EEC016 (void);
// 0x00000013 System.Void CrazyMinnow.SALSA.Emoter::OnDisable()
extern void Emoter_OnDisable_mB32586FF30A5FDF364D01398F1E8AECAA9E2C00C (void);
// 0x00000014 System.Void CrazyMinnow.SALSA.Emoter::LateUpdate()
extern void Emoter_LateUpdate_mC8EEA91F196B69AB0809F3E04C8A2F0B1C8F204D (void);
// 0x00000015 System.Void CrazyMinnow.SALSA.Emoter::SetRandomTimerPulse()
extern void Emoter_SetRandomTimerPulse_mD44FB39E15F2517371BB3A0DB6FDFC2439966E35 (void);
// 0x00000016 System.Void CrazyMinnow.SALSA.Emoter::TriggerLipSyncEmphasis(System.Single)
extern void Emoter_TriggerLipSyncEmphasis_m2AF6DC943444A01EE9121338CE0AAFF203183442 (void);
// 0x00000017 System.Void CrazyMinnow.SALSA.Emoter::FireRandomEmphasizers(System.Single,System.Int32)
extern void Emoter_FireRandomEmphasizers_m206906B9C08CE902173A43E642CFCA78E11F49D7 (void);
// 0x00000018 System.Void CrazyMinnow.SALSA.Emoter::ResetEmoteHasFiredFlags()
extern void Emoter_ResetEmoteHasFiredFlags_m2D35CF4A7FD719472A95DB8356A65B0964758837 (void);
// 0x00000019 System.Int32 CrazyMinnow.SALSA.Emoter::GetRandomEmphasizerCount()
extern void Emoter_GetRandomEmphasizerCount_m7EBCA58369E184F9C9C1602F3E420A7468A7B877 (void);
// 0x0000001A System.Void CrazyMinnow.SALSA.Emoter::FireAlwaysEmphasizers(System.Single)
extern void Emoter_FireAlwaysEmphasizers_m207D58DB22E400EE60613F86F2B2E280144A2939 (void);
// 0x0000001B System.Void CrazyMinnow.SALSA.Emoter::ManualEmote(System.String,CrazyMinnow.SALSA.ExpressionComponent/ExpressionHandler,System.Single,System.Boolean,System.Single)
extern void Emoter_ManualEmote_m7D3DC72896252CCF559057814C7AA7CE226AFC75 (void);
// 0x0000001C System.Void CrazyMinnow.SALSA.Emoter::ManualEmote(System.Int32,CrazyMinnow.SALSA.ExpressionComponent/ExpressionHandler,System.Boolean,System.Single,System.Single)
extern void Emoter_ManualEmote_m76933BC028208443D1213C91FE322822BEFEFE31 (void);
// 0x0000001D CrazyMinnow.SALSA.EmoteExpression CrazyMinnow.SALSA.Emoter::FindEmote(System.String)
extern void Emoter_FindEmote_m727CB8392353E15DBCB836EC7C677F0CD3BC37A9 (void);
// 0x0000001E System.Void CrazyMinnow.SALSA.Emoter::RandomEmote(System.Single,System.Int32)
extern void Emoter_RandomEmote_m2C06DDBB897801F76E646C9388A84CD86ACED3CD (void);
// 0x0000001F System.Void CrazyMinnow.SALSA.Emoter::EmoteOneWay(CrazyMinnow.SALSA.EmoteExpression,System.Boolean)
extern void Emoter_EmoteOneWay_mF370D921EADCA07D9106C9A108870DBA76FD6A92 (void);
// 0x00000020 System.Void CrazyMinnow.SALSA.Emoter::EmoteOneWay(CrazyMinnow.SALSA.EmoteExpression,System.Single,System.Boolean)
extern void Emoter_EmoteOneWay_m17044882BB43C377DB119EA24476D0F5247BC576 (void);
// 0x00000021 System.Void CrazyMinnow.SALSA.Emoter::EmoteRoundTrip(CrazyMinnow.SALSA.EmoteExpression)
extern void Emoter_EmoteRoundTrip_m26BDE2CE60AC54F1B9E8D8759F577AE80B24CA78 (void);
// 0x00000022 System.Void CrazyMinnow.SALSA.Emoter::EmoteRoundTrip(CrazyMinnow.SALSA.EmoteExpression,System.Single)
extern void Emoter_EmoteRoundTrip_mD4D44FB40A4A346A3C4319E180FE311CA18B8A90 (void);
// 0x00000023 System.Void CrazyMinnow.SALSA.Emoter::EmoteRoundTrip(CrazyMinnow.SALSA.EmoteExpression,System.Single,System.Single)
extern void Emoter_EmoteRoundTrip_m22D154F3C18EAC52C67BDAFFA2F5CD36FF0C8B42 (void);
// 0x00000024 System.Void CrazyMinnow.SALSA.Emoter::EmoteComponentRoundTrip(CrazyMinnow.SALSA.Expression,System.Int32,System.Single,System.Single)
extern void Emoter_EmoteComponentRoundTrip_m542883E388B8F22B928571C3BED9333807844C4F (void);
// 0x00000025 System.Void CrazyMinnow.SALSA.Emoter::TurnOffAll()
extern void Emoter_TurnOffAll_mC9757302CA4AF5906F9F05893971399D7D297B64 (void);
// 0x00000026 System.Boolean CrazyMinnow.SALSA.Emoter::CheckConfigIsReady()
extern void Emoter_CheckConfigIsReady_mC206501783F08005B0AE9307C36FC5D006033A9A (void);
// 0x00000027 System.Void CrazyMinnow.SALSA.Emoter::UpdateExpressionControllers()
extern void Emoter_UpdateExpressionControllers_mBA11B75B1E1A3EAE1D71410194D65058EB1CAB5E (void);
// 0x00000028 System.Void CrazyMinnow.SALSA.Emoter::UpdateEmoteLists()
extern void Emoter_UpdateEmoteLists_mB5086ADAC01DC71D876A7C976623276D682962D2 (void);
// 0x00000029 System.Boolean CrazyMinnow.SALSA.Emoter::RemoveFromPool(System.String,CrazyMinnow.SALSA.Emoter/PoolType)
extern void Emoter_RemoveFromPool_mF2B279B17DF98823BC0E626421F3F63627FBDCB6 (void);
// 0x0000002A System.Boolean CrazyMinnow.SALSA.Emoter::AddToPool(System.String,CrazyMinnow.SALSA.Emoter/PoolType)
extern void Emoter_AddToPool_m95329AEDB9C3DE08D5857D7783F995C4154E07DE (void);
// 0x0000002B System.Void CrazyMinnow.SALSA.Emoter::OnApplicationQuit()
extern void Emoter_OnApplicationQuit_mFCB9D437A110E71C8EAD3B12ACA01B727B0D9AF4 (void);
// 0x0000002C System.Void CrazyMinnow.SALSA.Emoter::.ctor()
extern void Emoter__ctor_mC5684D634790CFDEF17158383B91BA37CD7B6E46 (void);
// 0x0000002D System.Void CrazyMinnow.SALSA.Emoter/<>c::.cctor()
extern void U3CU3Ec__cctor_mC8E50734A86874F1E6166EBFAD5F76E693F6D481 (void);
// 0x0000002E System.Void CrazyMinnow.SALSA.Emoter/<>c::.ctor()
extern void U3CU3Ec__ctor_mC86556408D9FF84FBE11593B94D80025F0A4046F (void);
// 0x0000002F System.Boolean CrazyMinnow.SALSA.Emoter/<>c::<get_InspectorBasedRandomEmotesCount>b__13_0(CrazyMinnow.SALSA.EmoteExpression)
extern void U3CU3Ec_U3Cget_InspectorBasedRandomEmotesCountU3Eb__13_0_m17D7AF97FAF3BEBAA1A0987CA9F10519C6885560 (void);
// 0x00000030 System.Boolean CrazyMinnow.SALSA.Emoter/<>c::<get_InspectorBasedRandomEmphasizerCount>b__18_0(CrazyMinnow.SALSA.EmoteExpression)
extern void U3CU3Ec_U3Cget_InspectorBasedRandomEmphasizerCountU3Eb__18_0_mADC4BAE9AC1E3BC6588D6715FBC80F5E926AE967 (void);
// 0x00000031 System.Void CrazyMinnow.SALSA.ExpressionComponent::.ctor()
extern void ExpressionComponent__ctor_m0B32F0AD40E2306FBE0D945B5EF58C139235B2CE (void);
// 0x00000032 System.Void CrazyMinnow.SALSA.ExpressionComponent::.ctor(CrazyMinnow.SALSA.ExpressionComponent)
extern void ExpressionComponent__ctor_m57770503C90E5E6C8EFBCD9A8FB8D9DF6216DDE1 (void);
// 0x00000033 System.Void CrazyMinnow.SALSA.ExpressionComponent::.ctor(System.String,System.Single,System.Single,System.Single,CrazyMinnow.SALSA.ExpressionComponent/ExpressionType,CrazyMinnow.SALSA.LerpEasings/EasingType,CrazyMinnow.SALSA.ExpressionComponent/ExpressionHandler)
extern void ExpressionComponent__ctor_m7032EAF5ECE9620D5148C63A8D5E760C97EDE866 (void);
// 0x00000034 CrazyMinnow.SALSA.ExpressionComponent/LipsyncControlType CrazyMinnow.SALSA.Expression::ConvertToLipsyncControlType(System.String)
extern void Expression_ConvertToLipsyncControlType_m643BB23E5EC7A3CBFF5AD42489730B5E8E2441B5 (void);
// 0x00000035 CrazyMinnow.SALSA.ExpressionComponent/EmoteControlType CrazyMinnow.SALSA.Expression::ConvertToEmoteControlType(System.String)
extern void Expression_ConvertToEmoteControlType_m8E1F3B93DE9E43C56515EF47555379E3F57B1B9A (void);
// 0x00000036 CrazyMinnow.SALSA.ExpressionComponent/EyesControlType CrazyMinnow.SALSA.Expression::ConvertToEyesControlType(System.String)
extern void Expression_ConvertToEyesControlType_mDF2C160C31E81D5720F6433331302BF201956844 (void);
// 0x00000037 System.Void CrazyMinnow.SALSA.Expression::.ctor()
extern void Expression__ctor_m33DDB757C5D5BACCCECE133F48DC7A06FDBCF33C (void);
// 0x00000038 System.Int32 CrazyMinnow.SALSA.IExpressionController::GetID()
// 0x00000039 System.Void CrazyMinnow.SALSA.IExpressionController::SetLerp(System.Single,System.Boolean)
// 0x0000003A System.Void CrazyMinnow.SALSA.IExpressionController::ProcessInfluence(System.Boolean,System.Single)
// 0x0000003B System.Boolean CrazyMinnow.SALSA.IExpressionController::HasDirtyInfluence()
// 0x0000003C System.Single CrazyMinnow.SALSA.IExpressionController::GetNewDelta(System.Boolean,System.Boolean)
// 0x0000003D CrazyMinnow.SALSA.ControllerData CrazyMinnow.SALSA.IExpressionController::GetPreviousDeltaData()
// 0x0000003E System.Void CrazyMinnow.SALSA.IExpressionController::SetPreviousDeltaData(CrazyMinnow.SALSA.ControllerData)
// 0x0000003F System.Void CrazyMinnow.SALSA.IExpressionController::SetFrac(System.Single)
// 0x00000040 System.Single CrazyMinnow.SALSA.IExpressionController::GetMinExtent()
// 0x00000041 System.Single CrazyMinnow.SALSA.IExpressionController::GetMaxExtent()
// 0x00000042 System.Boolean CrazyMinnow.SALSA.IExpressionController::HasInvalidController()
// 0x00000043 System.Void CrazyMinnow.SALSA.IExpressionController::SetTarget(CrazyMinnow.SALSA.TformBase,System.Single)
// 0x00000044 System.Void CrazyMinnow.SALSA.IExpressionController::SetDelta(System.Single)
// 0x00000045 System.Void CrazyMinnow.SALSA.ControllerData::.ctor(CrazyMinnow.SALSA.TformBase,System.Single)
extern void ControllerData__ctor_mB835E971272F264A7CD4B4E780D9CA773D82F561 (void);
// 0x00000046 System.Void CrazyMinnow.SALSA.TformBase::.ctor(UnityEngine.Vector3,UnityEngine.Quaternion,UnityEngine.Vector3)
extern void TformBase__ctor_mC920C8F643AF451EE372A801F9B47291653D3584 (void);
// 0x00000047 System.Void CrazyMinnow.SALSA.BoneController::.ctor(UnityEngine.Transform,CrazyMinnow.SALSA.TformBase,CrazyMinnow.SALSA.TformBase,System.Boolean,System.Boolean,System.Boolean,System.Boolean,System.Boolean,System.Single)
extern void BoneController__ctor_m4802A85329F431BE104174AD7997EC46C9D6B25F (void);
// 0x00000048 System.Void CrazyMinnow.SALSA.BoneController::GenerateOffset(CrazyMinnow.SALSA.TformBase,CrazyMinnow.SALSA.TformBase)
extern void BoneController_GenerateOffset_mA1BA79E626AA09C8EDA878AB22FFD1344C7BC50D (void);
// 0x00000049 System.Int32 CrazyMinnow.SALSA.BoneController::GetID()
extern void BoneController_GetID_m21EB8B9BA0CA5F25B64D9C164DE98C5545B30FB3 (void);
// 0x0000004A System.Void CrazyMinnow.SALSA.BoneController::SetLerp(System.Single,System.Boolean)
extern void BoneController_SetLerp_m1690CC12477A226AF889D77CEB3BBFA9549EDBE1 (void);
// 0x0000004B System.Void CrazyMinnow.SALSA.BoneController::ProcessInfluence(System.Boolean,System.Single)
extern void BoneController_ProcessInfluence_m9C5DF2BB28E15C39439315C7A6AC454D12075B18 (void);
// 0x0000004C System.Boolean CrazyMinnow.SALSA.BoneController::HasDirtyInfluence()
extern void BoneController_HasDirtyInfluence_m084CB3A7746ED0CFC82206B29E39B067DBCF5A47 (void);
// 0x0000004D System.Single CrazyMinnow.SALSA.BoneController::GetNewDelta(System.Boolean,System.Boolean)
extern void BoneController_GetNewDelta_m9AFE9FAB8DF8554EB671C9AB3B0707F3CB1CCFEE (void);
// 0x0000004E CrazyMinnow.SALSA.ControllerData CrazyMinnow.SALSA.BoneController::GetPreviousDeltaData()
extern void BoneController_GetPreviousDeltaData_m5B7CE3CE4E700186C3C810B78302BBA964819A33 (void);
// 0x0000004F System.Void CrazyMinnow.SALSA.BoneController::SetPreviousDeltaData(CrazyMinnow.SALSA.ControllerData)
extern void BoneController_SetPreviousDeltaData_m86FC66206BEE58F69646D3D96234EF310E32D7A2 (void);
// 0x00000050 System.Void CrazyMinnow.SALSA.BoneController::SetFrac(System.Single)
extern void BoneController_SetFrac_m972BEA80991BA4EE91AD361B1DEAAA5064337962 (void);
// 0x00000051 System.Boolean CrazyMinnow.SALSA.BoneController::Equals(CrazyMinnow.SALSA.IExpressionController)
extern void BoneController_Equals_mCF63666AE3A7009A5AC5F45E140CF99622AE0821 (void);
// 0x00000052 System.Single CrazyMinnow.SALSA.BoneController::GetMaxExtent()
extern void BoneController_GetMaxExtent_mC2015175CBD350766142BB31D04728E90D4F74F8 (void);
// 0x00000053 System.Single CrazyMinnow.SALSA.BoneController::GetMinExtent()
extern void BoneController_GetMinExtent_m73A1510D95784286524F2EEA572E8E5DBC3AF579 (void);
// 0x00000054 System.Boolean CrazyMinnow.SALSA.BoneController::HasInvalidController()
extern void BoneController_HasInvalidController_m644872D9FF94AE740A7902CEC251CD55C1AEFAAD (void);
// 0x00000055 System.Void CrazyMinnow.SALSA.BoneController::SetTarget(CrazyMinnow.SALSA.TformBase,System.Single)
extern void BoneController_SetTarget_m80D3D02CD9B03D5ECCC5E1D6EF5D5243CC1E1153 (void);
// 0x00000056 System.Void CrazyMinnow.SALSA.BoneController::SetDelta(System.Single)
extern void BoneController_SetDelta_m598EDA89BB0578584632616AFC5CEDB67A106B8F (void);
// 0x00000057 System.Void CrazyMinnow.SALSA.ShapeController::.ctor(UnityEngine.SkinnedMeshRenderer,System.Int32,System.Single,System.Single,System.Single)
extern void ShapeController__ctor_m5C516B1FDA7E963D9809304B01136E81F10AEE91 (void);
// 0x00000058 System.Int32 CrazyMinnow.SALSA.ShapeController::GetID()
extern void ShapeController_GetID_mDFA8098590D3011E8493A2EBA0A1FDF776FB6303 (void);
// 0x00000059 System.Void CrazyMinnow.SALSA.ShapeController::SetLerp(System.Single,System.Boolean)
extern void ShapeController_SetLerp_m080693B0DE6743BF8267ED3D90F0FA5DE1C18026 (void);
// 0x0000005A System.Void CrazyMinnow.SALSA.ShapeController::ProcessInfluence(System.Boolean,System.Single)
extern void ShapeController_ProcessInfluence_m5638176DFC79A0FA382B7347F79BA64909B7192F (void);
// 0x0000005B System.Boolean CrazyMinnow.SALSA.ShapeController::HasDirtyInfluence()
extern void ShapeController_HasDirtyInfluence_m2FDF7D73EF7953E903D3CDAAB020B5A4996BB626 (void);
// 0x0000005C System.Single CrazyMinnow.SALSA.ShapeController::GetNewDelta(System.Boolean,System.Boolean)
extern void ShapeController_GetNewDelta_m9929CA7D84EC8048B674CDF267448AABD432BD80 (void);
// 0x0000005D CrazyMinnow.SALSA.ControllerData CrazyMinnow.SALSA.ShapeController::GetPreviousDeltaData()
extern void ShapeController_GetPreviousDeltaData_m6D1EF2A9DD50ECB996CBFED6165480BC0CB82186 (void);
// 0x0000005E System.Void CrazyMinnow.SALSA.ShapeController::SetPreviousDeltaData(CrazyMinnow.SALSA.ControllerData)
extern void ShapeController_SetPreviousDeltaData_m4B658901A51432ECBBB22DC59514B50634891562 (void);
// 0x0000005F System.Void CrazyMinnow.SALSA.ShapeController::SetFrac(System.Single)
extern void ShapeController_SetFrac_m3D23B4BF79F3E93B5FAF68BD0953DBF1E1756293 (void);
// 0x00000060 System.Boolean CrazyMinnow.SALSA.ShapeController::Equals(CrazyMinnow.SALSA.IExpressionController)
extern void ShapeController_Equals_mD89DA06AAFD9E4F8E5D45A51CF9A9B1F15569159 (void);
// 0x00000061 System.Single CrazyMinnow.SALSA.ShapeController::GetMaxExtent()
extern void ShapeController_GetMaxExtent_mD01B9B398E93425A74DDC7F733457B552DF9D01E (void);
// 0x00000062 System.Single CrazyMinnow.SALSA.ShapeController::GetMinExtent()
extern void ShapeController_GetMinExtent_m2A2A05D778DC81DD4B937FC3822A121C08BAAA61 (void);
// 0x00000063 System.Boolean CrazyMinnow.SALSA.ShapeController::HasInvalidController()
extern void ShapeController_HasInvalidController_mBA82397201C0E16BBE994B745731E38FDE9C4FB6 (void);
// 0x00000064 System.Void CrazyMinnow.SALSA.ShapeController::SetTarget(CrazyMinnow.SALSA.TformBase,System.Single)
extern void ShapeController_SetTarget_m2F924C692C25FE98B76AA9D0E0C3846DA1587E5D (void);
// 0x00000065 System.Void CrazyMinnow.SALSA.ShapeController::SetDelta(System.Single)
extern void ShapeController_SetDelta_mC5A87E41260A9E72BBA4E52DE3B3A8E23AFF8A2E (void);
// 0x00000066 System.Void CrazyMinnow.SALSA.UmaController::.ctor(CrazyMinnow.SALSA.UmaUepProxy,System.Int32,System.Single,System.Single)
extern void UmaController__ctor_m887575118976652B2AAF7005B8E1E3E2FFAE097F (void);
// 0x00000067 System.Int32 CrazyMinnow.SALSA.UmaController::GetID()
extern void UmaController_GetID_mB29131FB7E3ED0E600E9EAE35BDA94D34AFC2201 (void);
// 0x00000068 System.Void CrazyMinnow.SALSA.UmaController::SetLerp(System.Single,System.Boolean)
extern void UmaController_SetLerp_m579D751C9AABB3F650FEBEB5E105C4A59F52535F (void);
// 0x00000069 System.Void CrazyMinnow.SALSA.UmaController::ProcessInfluence(System.Boolean,System.Single)
extern void UmaController_ProcessInfluence_mE932895AB0D126B4C4A9541FE8CC8CD22790389E (void);
// 0x0000006A System.Boolean CrazyMinnow.SALSA.UmaController::HasDirtyInfluence()
extern void UmaController_HasDirtyInfluence_m8088D3A308A9E35D0B3FB74F7CC06A7765F31845 (void);
// 0x0000006B System.Single CrazyMinnow.SALSA.UmaController::GetNewDelta(System.Boolean,System.Boolean)
extern void UmaController_GetNewDelta_m5E6421E7DF087D72A538D3E9191B5432F58E5539 (void);
// 0x0000006C CrazyMinnow.SALSA.ControllerData CrazyMinnow.SALSA.UmaController::GetPreviousDeltaData()
extern void UmaController_GetPreviousDeltaData_m3CF8425F2002261DC0DBADA420B2164C5B889F03 (void);
// 0x0000006D System.Void CrazyMinnow.SALSA.UmaController::SetPreviousDeltaData(CrazyMinnow.SALSA.ControllerData)
extern void UmaController_SetPreviousDeltaData_m96D1A0C9A3F0F32BD91411EEFCCF6F81E3BEBE65 (void);
// 0x0000006E System.Void CrazyMinnow.SALSA.UmaController::SetFrac(System.Single)
extern void UmaController_SetFrac_m9AB97E84F54D12731796BD9852D228478E2328AF (void);
// 0x0000006F System.Boolean CrazyMinnow.SALSA.UmaController::Equals(CrazyMinnow.SALSA.IExpressionController)
extern void UmaController_Equals_m5F5305F4E0F68ECE4FAB911BD1B1FB1DF31FCE42 (void);
// 0x00000070 System.Single CrazyMinnow.SALSA.UmaController::GetMaxExtent()
extern void UmaController_GetMaxExtent_m6DD957039D7245CF657BFB3E15CD3ECA6EBD264A (void);
// 0x00000071 System.Single CrazyMinnow.SALSA.UmaController::GetMinExtent()
extern void UmaController_GetMinExtent_m0BC6E812A9A24F1D0ADC33336C86D9544ECA1B0C (void);
// 0x00000072 System.Boolean CrazyMinnow.SALSA.UmaController::HasInvalidController()
extern void UmaController_HasInvalidController_m69A41E73CE5B858CFB821BF70F7D1319E35F1DA1 (void);
// 0x00000073 System.Void CrazyMinnow.SALSA.UmaController::SetTarget(CrazyMinnow.SALSA.TformBase,System.Single)
extern void UmaController_SetTarget_m1EEB092DBA483681F316B33CF1A1719F93C4CC48 (void);
// 0x00000074 System.Void CrazyMinnow.SALSA.UmaController::SetDelta(System.Single)
extern void UmaController_SetDelta_m9B24A1710E20CF21EEF5427C582A0344ACE6CD25 (void);
// 0x00000075 System.Boolean CrazyMinnow.SALSA.GenericController::HasDirtyInfluence()
extern void GenericController_HasDirtyInfluence_m079864657E37D96A79322D82E1F36578811B4ED4 (void);
// 0x00000076 System.Single CrazyMinnow.SALSA.GenericController::GetNewDelta(System.Boolean,System.Boolean)
extern void GenericController_GetNewDelta_mC00C87E55290BF051A671F625EE017FD3AB0195F (void);
// 0x00000077 CrazyMinnow.SALSA.ControllerData CrazyMinnow.SALSA.GenericController::GetPreviousDeltaData()
extern void GenericController_GetPreviousDeltaData_m57CFAF0A23638261DB1F813D41518A6B5A048717 (void);
// 0x00000078 System.Void CrazyMinnow.SALSA.GenericController::SetPreviousDeltaData(CrazyMinnow.SALSA.ControllerData)
extern void GenericController_SetPreviousDeltaData_mCC060E6AFD702D615616682A570E8CCD70C3D0B0 (void);
// 0x00000079 System.Int32 CrazyMinnow.SALSA.GenericController::GetID()
// 0x0000007A System.Void CrazyMinnow.SALSA.GenericController::SetLerp(System.Single,System.Boolean)
extern void GenericController_SetLerp_mDB610D6D0D5ADD38FC65C8EA6AD15376A6E59678 (void);
// 0x0000007B System.Void CrazyMinnow.SALSA.GenericController::ON_Start()
extern void GenericController_ON_Start_m8D47876103F95F12EB741CE004F6284EA5E0924C (void);
// 0x0000007C System.Void CrazyMinnow.SALSA.GenericController::ON_Finish()
extern void GenericController_ON_Finish_mBCC814CC09387D535389D810874BD4118B4849E1 (void);
// 0x0000007D System.Void CrazyMinnow.SALSA.GenericController::OFF_Start()
extern void GenericController_OFF_Start_mCA5796D003F0985904CA05ACB05BF4A4E960AB19 (void);
// 0x0000007E System.Void CrazyMinnow.SALSA.GenericController::OFF_Finish()
extern void GenericController_OFF_Finish_m18131A14D0D0437F39D3058DF87C5D0A53F7A3A9 (void);
// 0x0000007F System.Void CrazyMinnow.SALSA.GenericController::ProcessInfluence(System.Boolean,System.Single)
extern void GenericController_ProcessInfluence_mC24E007C2F97D78E902DFECAE9369AE4AA74C888 (void);
// 0x00000080 System.Void CrazyMinnow.SALSA.GenericController::SetFrac(System.Single)
extern void GenericController_SetFrac_m7721569EED180BA8676DE169C96607357A1C4BBE (void);
// 0x00000081 System.Single CrazyMinnow.SALSA.GenericController::GetMinExtent()
extern void GenericController_GetMinExtent_m9ABFF39C4AE09D6AB420DA13CFA3074D56EC5779 (void);
// 0x00000082 System.Single CrazyMinnow.SALSA.GenericController::GetMaxExtent()
extern void GenericController_GetMaxExtent_mC163A958FB128305B6AC906D1B43257D5BB964D0 (void);
// 0x00000083 System.Boolean CrazyMinnow.SALSA.GenericController::HasInvalidController()
// 0x00000084 System.Void CrazyMinnow.SALSA.GenericController::SetTarget(CrazyMinnow.SALSA.TformBase,System.Single)
extern void GenericController_SetTarget_m2D9CAF89ED6DBB6EEC8F7DA127927F05022B2513 (void);
// 0x00000085 System.Void CrazyMinnow.SALSA.GenericController::SetDelta(System.Single)
extern void GenericController_SetDelta_m3F7B864FB0E10EADDB00FFE8EA3B982423B339D3 (void);
// 0x00000086 System.Boolean CrazyMinnow.SALSA.GenericController::Equals(CrazyMinnow.SALSA.IExpressionController)
// 0x00000087 System.Void CrazyMinnow.SALSA.GenericController::.ctor()
extern void GenericController__ctor_mA3F94654E71FDC9D589C804CD2E9555D99F03750 (void);
// 0x00000088 System.Void CrazyMinnow.SALSA.AnimatorController::.ctor(UnityEngine.Animator,UnityEngine.AnimatorControllerParameter,System.Boolean)
extern void AnimatorController__ctor_mFF31AFAF4E7D0FBE9489A7F09E9CF2929790AD4F (void);
// 0x00000089 System.Int32 CrazyMinnow.SALSA.AnimatorController::GetID()
extern void AnimatorController_GetID_m8ABE08F0A02C2C166A84F81302631FAAEADAC855 (void);
// 0x0000008A System.Void CrazyMinnow.SALSA.AnimatorController::SetLerp(System.Single,System.Boolean)
extern void AnimatorController_SetLerp_m1CE2D284F6A4FB3319B52006F4905A27E734EA02 (void);
// 0x0000008B System.Void CrazyMinnow.SALSA.AnimatorController::ON_Start()
extern void AnimatorController_ON_Start_mE985DC17389F6D852130D8A256CD4F3C26860CC6 (void);
// 0x0000008C System.Void CrazyMinnow.SALSA.AnimatorController::ON_Finish()
extern void AnimatorController_ON_Finish_mFDBA370FA2C516BB2884A1E101EF0936DDF7F857 (void);
// 0x0000008D System.Void CrazyMinnow.SALSA.AnimatorController::OFF_Start()
extern void AnimatorController_OFF_Start_mAC7B69F57573C095CD56BB32BD3C0F798D8FCFAB (void);
// 0x0000008E System.Void CrazyMinnow.SALSA.AnimatorController::OFF_Finish()
extern void AnimatorController_OFF_Finish_mA2271E1D84C3DD04EA93AF7CF24F705A67172319 (void);
// 0x0000008F System.Boolean CrazyMinnow.SALSA.AnimatorController::Equals(CrazyMinnow.SALSA.IExpressionController)
extern void AnimatorController_Equals_mD30FF8309D86C9F34BDAA969110C50C800C2543B (void);
// 0x00000090 System.Boolean CrazyMinnow.SALSA.AnimatorController::HasInvalidController()
extern void AnimatorController_HasInvalidController_m04D41F07E970D5111CB97A41AFBD485FA7D84504 (void);
// 0x00000091 System.Void CrazyMinnow.SALSA.EventController::add_AnimationStarting(System.EventHandler`1<CrazyMinnow.SALSA.EventController/EventControllerNotificationArgs>)
extern void EventController_add_AnimationStarting_m908C545B38039320ED2221AD756581D403375232 (void);
// 0x00000092 System.Void CrazyMinnow.SALSA.EventController::remove_AnimationStarting(System.EventHandler`1<CrazyMinnow.SALSA.EventController/EventControllerNotificationArgs>)
extern void EventController_remove_AnimationStarting_m704888C40478456DE937C3C15734769D53145BC7 (void);
// 0x00000093 System.Void CrazyMinnow.SALSA.EventController::add_AnimationON(System.EventHandler`1<CrazyMinnow.SALSA.EventController/EventControllerNotificationArgs>)
extern void EventController_add_AnimationON_m07523A57BF2CC2946FBFBD068471318F6D3FC3AA (void);
// 0x00000094 System.Void CrazyMinnow.SALSA.EventController::remove_AnimationON(System.EventHandler`1<CrazyMinnow.SALSA.EventController/EventControllerNotificationArgs>)
extern void EventController_remove_AnimationON_mD1156CA7CF26402A66E2CAF634A79180C2A0D59D (void);
// 0x00000095 System.Void CrazyMinnow.SALSA.EventController::add_AnimationEnding(System.EventHandler`1<CrazyMinnow.SALSA.EventController/EventControllerNotificationArgs>)
extern void EventController_add_AnimationEnding_m9D5D7FC781857025A6E8AF4E3CF426AC55F01A61 (void);
// 0x00000096 System.Void CrazyMinnow.SALSA.EventController::remove_AnimationEnding(System.EventHandler`1<CrazyMinnow.SALSA.EventController/EventControllerNotificationArgs>)
extern void EventController_remove_AnimationEnding_m8BE943425939845D6A70FB81E3753E1605A49B65 (void);
// 0x00000097 System.Void CrazyMinnow.SALSA.EventController::add_AnimationOFF(System.EventHandler`1<CrazyMinnow.SALSA.EventController/EventControllerNotificationArgs>)
extern void EventController_add_AnimationOFF_mB77661AE7AC1D3CB1537781A157FFAF8F3636931 (void);
// 0x00000098 System.Void CrazyMinnow.SALSA.EventController::remove_AnimationOFF(System.EventHandler`1<CrazyMinnow.SALSA.EventController/EventControllerNotificationArgs>)
extern void EventController_remove_AnimationOFF_mC19633B977DD79655421316685D952ECB6BDDCD9 (void);
// 0x00000099 System.Void CrazyMinnow.SALSA.EventController::FireAnimationStartingEvent()
extern void EventController_FireAnimationStartingEvent_mD99B6C8F449C700516082713D58478234F13A953 (void);
// 0x0000009A System.Void CrazyMinnow.SALSA.EventController::FireAnimationOnEvent()
extern void EventController_FireAnimationOnEvent_mB528BBDD5ABFB9D0BEBFD25C1BF6641EDE7076A0 (void);
// 0x0000009B System.Void CrazyMinnow.SALSA.EventController::FireAnimationEndingEvent()
extern void EventController_FireAnimationEndingEvent_m92825B78794BDE4697FFA684FA7EDACA568294C4 (void);
// 0x0000009C System.Void CrazyMinnow.SALSA.EventController::FireAnimationOffEvent()
extern void EventController_FireAnimationOffEvent_m4F5785BBA6086C9672D9E3F2A4B9EC2F13EA19CF (void);
// 0x0000009D System.Void CrazyMinnow.SALSA.EventController::.ctor(System.String)
extern void EventController__ctor_m2CE057B924DCD61C81E9C00769BD366D1C352A7A (void);
// 0x0000009E System.Int32 CrazyMinnow.SALSA.EventController::GetID()
extern void EventController_GetID_mF4C30ED420B4756B2416A4AFFD1CB1B7AFDD1B99 (void);
// 0x0000009F System.Void CrazyMinnow.SALSA.EventController::ON_Start()
extern void EventController_ON_Start_m4F5DCCD87A6F979DC7F16B4046BC910498B7D569 (void);
// 0x000000A0 System.Void CrazyMinnow.SALSA.EventController::ON_Finish()
extern void EventController_ON_Finish_mC28BC393071762AA99AF749762818D67F8F7BE43 (void);
// 0x000000A1 System.Void CrazyMinnow.SALSA.EventController::OFF_Start()
extern void EventController_OFF_Start_m1476D461B058F42873B5D8084A535654BBDAFDC6 (void);
// 0x000000A2 System.Void CrazyMinnow.SALSA.EventController::OFF_Finish()
extern void EventController_OFF_Finish_m45DF12BB34C2CC80A8337F43220849B004649D08 (void);
// 0x000000A3 System.Boolean CrazyMinnow.SALSA.EventController::Equals(CrazyMinnow.SALSA.IExpressionController)
extern void EventController_Equals_mCED00DB4266BC38892E610D6BFF60EF328A13CA1 (void);
// 0x000000A4 System.Boolean CrazyMinnow.SALSA.EventController::HasInvalidController()
extern void EventController_HasInvalidController_mFCFB1E8C1440508C2945EAF6285432C97380E210 (void);
// 0x000000A5 System.Void CrazyMinnow.SALSA.EventController/EventControllerNotificationArgs::.ctor()
extern void EventControllerNotificationArgs__ctor_mF3C8C19D12FFA6B7D81CD0A6C444DF076779DEC5 (void);
// 0x000000A6 System.Void CrazyMinnow.SALSA.Switcher::.ctor()
extern void Switcher__ctor_m67537B3923AABCB49B621465F0620500B829567F (void);
// 0x000000A7 System.Int32 CrazyMinnow.SALSA.Switcher::GetID()
// 0x000000A8 System.Void CrazyMinnow.SALSA.Switcher::SetLerp(System.Single,System.Boolean)
extern void Switcher_SetLerp_m1318FEB344EEBA1FEF2CD825D83212B567F2FF6B (void);
// 0x000000A9 System.Void CrazyMinnow.SALSA.Switcher::ProcessInfluence(System.Boolean,System.Single)
extern void Switcher_ProcessInfluence_mFF68898EE2999B7FDCAEFB1E8017E464CEEF9D11 (void);
// 0x000000AA System.Boolean CrazyMinnow.SALSA.Switcher::HasDirtyInfluence()
extern void Switcher_HasDirtyInfluence_mD288F94E40BE07F2EF80263169701E6664353D7B (void);
// 0x000000AB CrazyMinnow.SALSA.ControllerData CrazyMinnow.SALSA.Switcher::GetPreviousDeltaData()
extern void Switcher_GetPreviousDeltaData_m8E6B55BDD377181D26D9446BBEC4844731CA9D70 (void);
// 0x000000AC System.Void CrazyMinnow.SALSA.Switcher::SetPreviousDeltaData(CrazyMinnow.SALSA.ControllerData)
extern void Switcher_SetPreviousDeltaData_m1E9D90083028820BD6C85EE9DC88BF3425014327 (void);
// 0x000000AD System.Void CrazyMinnow.SALSA.Switcher::SetFrac(System.Single)
extern void Switcher_SetFrac_mB016C8AE914081415ECFC0D1D9AABD1CF9DDD1BB (void);
// 0x000000AE System.Single CrazyMinnow.SALSA.Switcher::GetMinExtent()
extern void Switcher_GetMinExtent_m34DDBD1AFBDECB4CBD7640C993262FB45289D0E6 (void);
// 0x000000AF System.Single CrazyMinnow.SALSA.Switcher::GetMaxExtent()
extern void Switcher_GetMaxExtent_mFF6E8999AFAD58BD7F27A8ED24593BA6C75FA503 (void);
// 0x000000B0 System.Single CrazyMinnow.SALSA.Switcher::GetNewDelta(System.Boolean,System.Boolean)
extern void Switcher_GetNewDelta_mCF11D34091ADF0D0568D051C1F35E1A39853E8EC (void);
// 0x000000B1 System.Void CrazyMinnow.SALSA.Switcher::SetOn(System.Single)
// 0x000000B2 System.Void CrazyMinnow.SALSA.Switcher::SetOff()
// 0x000000B3 System.Boolean CrazyMinnow.SALSA.Switcher::HasInvalidController()
// 0x000000B4 System.Void CrazyMinnow.SALSA.Switcher::SetTarget(CrazyMinnow.SALSA.TformBase,System.Single)
extern void Switcher_SetTarget_m110B196470A83D4D9B225B6A994C7308A567E4D7 (void);
// 0x000000B5 System.Void CrazyMinnow.SALSA.Switcher::SetDelta(System.Single)
extern void Switcher_SetDelta_m804447511A190D9FBC259588FE0959046A677184 (void);
// 0x000000B6 System.Boolean CrazyMinnow.SALSA.Switcher::Equals(CrazyMinnow.SALSA.IExpressionController)
// 0x000000B7 System.Void CrazyMinnow.SALSA.SpriteController::.ctor(UnityEngine.SpriteRenderer,UnityEngine.Sprite[],CrazyMinnow.SALSA.Switcher/OnState,System.Boolean)
extern void SpriteController__ctor_mFE2740133E31B78FCAB0B36FFDE3146E636DC4B0 (void);
// 0x000000B8 System.Int32 CrazyMinnow.SALSA.SpriteController::GetID()
extern void SpriteController_GetID_m60261941DE8F388F8DEF3360748BAAD61EA355F0 (void);
// 0x000000B9 System.Void CrazyMinnow.SALSA.SpriteController::SetOn(System.Single)
extern void SpriteController_SetOn_m3E9C80B998537C136E4B096E1EAA83D9A222E24E (void);
// 0x000000BA System.Void CrazyMinnow.SALSA.SpriteController::SetOff()
extern void SpriteController_SetOff_m4650D36DAF1090197EF9AFCB94BAA2D935625A7F (void);
// 0x000000BB System.Boolean CrazyMinnow.SALSA.SpriteController::Equals(CrazyMinnow.SALSA.IExpressionController)
extern void SpriteController_Equals_m0EBE9A24BEA8D8BDEDF5162FB2A614179F75E673 (void);
// 0x000000BC System.Boolean CrazyMinnow.SALSA.SpriteController::HasInvalidController()
extern void SpriteController_HasInvalidController_m9887722B92879F379FF26962F2C42900D7E7D5DF (void);
// 0x000000BD System.Void CrazyMinnow.SALSA.UguiController::.ctor(UnityEngine.UI.Image,UnityEngine.Sprite[],CrazyMinnow.SALSA.Switcher/OnState,System.Boolean)
extern void UguiController__ctor_m16E0D998ACC0F04F200585B2078A74EFE25933F9 (void);
// 0x000000BE System.Int32 CrazyMinnow.SALSA.UguiController::GetID()
extern void UguiController_GetID_m0493D4C18EBCF9D9A4FB35C85CAD622016658B03 (void);
// 0x000000BF System.Void CrazyMinnow.SALSA.UguiController::SetOn(System.Single)
extern void UguiController_SetOn_mA7712BB1EB21451F47471F6242FDA45BCC5C379B (void);
// 0x000000C0 System.Void CrazyMinnow.SALSA.UguiController::SetOff()
extern void UguiController_SetOff_mFC2C292D5DB15F9AD379573657EC52CE93E0001C (void);
// 0x000000C1 System.Boolean CrazyMinnow.SALSA.UguiController::Equals(CrazyMinnow.SALSA.IExpressionController)
extern void UguiController_Equals_mB1F42A2B1E20E7989062079167FBBE1A51F04854 (void);
// 0x000000C2 System.Boolean CrazyMinnow.SALSA.UguiController::HasInvalidController()
extern void UguiController_HasInvalidController_mB2B3414B4922CAB36BBE3B7EC7657F8126021618 (void);
// 0x000000C3 System.Void CrazyMinnow.SALSA.TextureController::.ctor(UnityEngine.Renderer,System.Int32,UnityEngine.Texture[],CrazyMinnow.SALSA.Switcher/OnState,System.Boolean)
extern void TextureController__ctor_m69FAB6004583C9E38B95CB95105BE7D54F514CFA (void);
// 0x000000C4 System.Int32 CrazyMinnow.SALSA.TextureController::GetID()
extern void TextureController_GetID_m42D1846316CCE7C214B633B6E420FDE8F0E5258D (void);
// 0x000000C5 System.Void CrazyMinnow.SALSA.TextureController::SetOn(System.Single)
extern void TextureController_SetOn_m7CCB962FD67177494184100F47052FF8EE581641 (void);
// 0x000000C6 System.Void CrazyMinnow.SALSA.TextureController::SetOff()
extern void TextureController_SetOff_mC97B188462B2BEE5D5F164E9905CEB914E97CC7D (void);
// 0x000000C7 System.Boolean CrazyMinnow.SALSA.TextureController::Equals(CrazyMinnow.SALSA.IExpressionController)
extern void TextureController_Equals_mC123628BCB68D18201DBDB30511E9BFF8D53CACD (void);
// 0x000000C8 System.Boolean CrazyMinnow.SALSA.TextureController::HasInvalidController()
extern void TextureController_HasInvalidController_mFAF6B3A0DF33ADF58707E9A757D1C22C35CA01BF (void);
// 0x000000C9 System.Void CrazyMinnow.SALSA.MaterialController::.ctor(UnityEngine.Renderer,UnityEngine.Material[],CrazyMinnow.SALSA.Switcher/OnState,System.Boolean)
extern void MaterialController__ctor_m047FACC99D8708861861A4381686FF6FA9088A6A (void);
// 0x000000CA System.Int32 CrazyMinnow.SALSA.MaterialController::GetID()
extern void MaterialController_GetID_m2BF86662D4EFEADE69F0B912B7F5276A67C3AFB6 (void);
// 0x000000CB System.Void CrazyMinnow.SALSA.MaterialController::SetOn(System.Single)
extern void MaterialController_SetOn_m7902C995602DADC1C5A6C898D745E402BC5ADB63 (void);
// 0x000000CC System.Void CrazyMinnow.SALSA.MaterialController::SetOff()
extern void MaterialController_SetOff_mDF9CCDFBAEAE169083F7489CCA949841EE6E5952 (void);
// 0x000000CD System.Boolean CrazyMinnow.SALSA.MaterialController::HasInvalidController()
extern void MaterialController_HasInvalidController_mB9CF24A6279B884DB627C7D9B3E286124F549BB8 (void);
// 0x000000CE System.Boolean CrazyMinnow.SALSA.MaterialController::Equals(CrazyMinnow.SALSA.IExpressionController)
extern void MaterialController_Equals_m8422B4FFB7ABAE4BF41AAFB7E89DCF63EF369132 (void);
// 0x000000CF System.Void CrazyMinnow.SALSA.EyeGizmo::ToggleShowGizmo()
extern void EyeGizmo_ToggleShowGizmo_m91710B0B866100106F3F842D4C8C29DB8C549E72 (void);
// 0x000000D0 System.Void CrazyMinnow.SALSA.EyeGizmo::OnDrawGizmos()
extern void EyeGizmo_OnDrawGizmos_m641681A69C8F5EAD18083F4EF0E0D6CE799A2D39 (void);
// 0x000000D1 System.Void CrazyMinnow.SALSA.EyeGizmo::.ctor()
extern void EyeGizmo__ctor_mE66D9A977B5248E718FF50E8D249F6FD2E4C00E0 (void);
// 0x000000D2 System.Void CrazyMinnow.SALSA.Eyes::AddExpression(System.Collections.Generic.List`1<CrazyMinnow.SALSA.EyesExpression>&,CrazyMinnow.SALSA.ExpressionComponent/ExpressionType)
extern void Eyes_AddExpression_m5B81D49D79CE8AF5ECB99AF396A1705500CF3EAB (void);
// 0x000000D3 System.Void CrazyMinnow.SALSA.Eyes::AddComponent(System.Collections.Generic.List`1<CrazyMinnow.SALSA.EyesExpression>&,System.Int32,CrazyMinnow.SALSA.ExpressionComponent/ExpressionType,CrazyMinnow.SALSA.ExpressionComponent/ControlType,System.Boolean,System.Boolean,System.Boolean)
extern void Eyes_AddComponent_mC4F98F3DE6C289E8F129C86E8F615A8EFAAEDDD8 (void);
// 0x000000D4 System.Void CrazyMinnow.SALSA.Eyes::BuildHeadTemplate(CrazyMinnow.SALSA.Eyes/HeadTemplates)
extern void Eyes_BuildHeadTemplate_m114ED7E9D0D1848B3A61CA506FA0C10AEBA842AF (void);
// 0x000000D5 System.Void CrazyMinnow.SALSA.Eyes::BuildEyeTemplate(CrazyMinnow.SALSA.Eyes/EyeTemplates)
extern void Eyes_BuildEyeTemplate_m1B3FC48130BF6CC4CA43796688DDEB34B9A098E5 (void);
// 0x000000D6 System.Void CrazyMinnow.SALSA.Eyes::BuildEyelidTemplate(CrazyMinnow.SALSA.Eyes/EyelidTemplates)
extern void Eyes_BuildEyelidTemplate_m00A5637220944357FD195BEF8B65D2C8064E4679 (void);
// 0x000000D7 System.Void CrazyMinnow.SALSA.Eyes::BuildEyelidTemplate(CrazyMinnow.SALSA.Eyes/EyelidTemplates,CrazyMinnow.SALSA.Eyes/EyelidSelection)
extern void Eyes_BuildEyelidTemplate_m1310C11E6D8CEF449DA6EED2FBD12BE02EB28C3B (void);
// 0x000000D8 System.Void CrazyMinnow.SALSA.Eyes::BuildAnyBoneTemplate(System.Collections.Generic.List`1<CrazyMinnow.SALSA.EyesExpression>&,CrazyMinnow.SALSA.ExpressionComponent/ExpressionType)
extern void Eyes_BuildAnyBoneTemplate_m7A585B4A3708083700D23501ADC24DD067F0C9B7 (void);
// 0x000000D9 System.Void CrazyMinnow.SALSA.Eyes::AddBoneExpression(System.Collections.Generic.List`1<CrazyMinnow.SALSA.EyesExpression>&,CrazyMinnow.SALSA.ExpressionComponent/ExpressionType)
extern void Eyes_AddBoneExpression_m4EDFCD57EA3D6FF177355DC566634A507C408F11 (void);
// 0x000000DA System.Void CrazyMinnow.SALSA.Eyes::AddBoneComponent(System.Collections.Generic.List`1<CrazyMinnow.SALSA.EyesExpression>&,System.Int32,CrazyMinnow.SALSA.ExpressionComponent/ExpressionType)
extern void Eyes_AddBoneComponent_m38F759D1CFFAED5E0468D065DFA6658DE3C8736A (void);
// 0x000000DB System.Void CrazyMinnow.SALSA.Eyes::SetBonePosRot(System.Collections.Generic.List`1<CrazyMinnow.SALSA.EyesExpression>&,System.Boolean,System.Boolean,System.Boolean)
extern void Eyes_SetBonePosRot_mDD4B48E563AA361C0BE1EC777E21619753411072 (void);
// 0x000000DC System.Void CrazyMinnow.SALSA.Eyes::BuildEyeShapeTemplate()
extern void Eyes_BuildEyeShapeTemplate_mA69BF018C9ADEA7FBF570F6E78C298A2E52963E1 (void);
// 0x000000DD System.Void CrazyMinnow.SALSA.Eyes::AddEyeShapeExpression()
extern void Eyes_AddEyeShapeExpression_mF67E70BDAB79E911D6EE268C834759675B978B9D (void);
// 0x000000DE System.Void CrazyMinnow.SALSA.Eyes::AddShapeComponent(System.Collections.Generic.List`1<CrazyMinnow.SALSA.EyesExpression>&,System.Int32,CrazyMinnow.SALSA.ExpressionComponent/ExpressionType)
extern void Eyes_AddShapeComponent_m4A5C2819D770B30F1879C363F8CEBC18CAA5E6F8 (void);
// 0x000000DF System.Void CrazyMinnow.SALSA.Eyes::BuildEyeSectorTemplate(CrazyMinnow.SALSA.Eyes/SectorCount,CrazyMinnow.SALSA.ExpressionComponent/ControlType,System.Boolean)
extern void Eyes_BuildEyeSectorTemplate_m52C2B62172DFE1E0E9BDAC2578BA208695B59B1F (void);
// 0x000000E0 System.Void CrazyMinnow.SALSA.Eyes::AddEyeSectorExpression()
extern void Eyes_AddEyeSectorExpression_mB2D87E13382F0BF5F9104D48042A80A9FA58317A (void);
// 0x000000E1 System.Void CrazyMinnow.SALSA.Eyes::BuildEyelidShapeTemplate(CrazyMinnow.SALSA.Eyes/EyelidSelection)
extern void Eyes_BuildEyelidShapeTemplate_m3FB5CA846EFBF761037CE0E473F817B40D60696B (void);
// 0x000000E2 System.Void CrazyMinnow.SALSA.Eyes::AddEyelidShapeExpression(System.Collections.Generic.List`1<CrazyMinnow.SALSA.EyesExpression>&)
extern void Eyes_AddEyelidShapeExpression_m0E870A5E8350BCC7ABA1944A1D7DA0489D4DE373 (void);
// 0x000000E3 System.Void CrazyMinnow.SALSA.Eyes::BuildEyelidSwapTemplate(CrazyMinnow.SALSA.ExpressionComponent/ControlType)
extern void Eyes_BuildEyelidSwapTemplate_m52DF87D3731EFDFCB8E709786806F2ACB65CE5B0 (void);
// 0x000000E4 System.Void CrazyMinnow.SALSA.Eyes::AddEyelidSwapExpression(CrazyMinnow.SALSA.ExpressionComponent/ControlType)
extern void Eyes_AddEyelidSwapExpression_m727A64715BF763640F3C45C2AFB08730AD17FB18 (void);
// 0x000000E5 System.Void CrazyMinnow.SALSA.Eyes::AddEyelidSwapComponent(System.Int32,CrazyMinnow.SALSA.ExpressionComponent/ControlType)
extern void Eyes_AddEyelidSwapComponent_m55F3B26365C378687EA95BB9F4EFBDA74682452C (void);
// 0x000000E6 System.Void CrazyMinnow.SALSA.Eyes::BuildEyelidUMATemplate(CrazyMinnow.SALSA.Eyes/EyelidSelection)
extern void Eyes_BuildEyelidUMATemplate_m111F51DE6B90CBE84DDE8AADB663A3AB764852E2 (void);
// 0x000000E7 System.Void CrazyMinnow.SALSA.Eyes::AddEyelidUMAExpression()
extern void Eyes_AddEyelidUMAExpression_mCFACD6576A8C1B2797351EC4679F999B5B1C8D5E (void);
// 0x000000E8 System.Void CrazyMinnow.SALSA.Eyes::SetEyeAnimationTimingDurationOff(System.Single)
extern void Eyes_SetEyeAnimationTimingDurationOff_m7FD5FF33D07CA13EA68564EA83E5FC59EEC20EFA (void);
// 0x000000E9 System.String[] CrazyMinnow.SALSA.Eyes::GetExpressionNames(System.Collections.Generic.List`1<CrazyMinnow.SALSA.EyesExpression>&)
extern void Eyes_GetExpressionNames_m99A954AA5A726EB9B86AC6421C0D08BDA4F54D71 (void);
// 0x000000EA UnityEngine.Transform CrazyMinnow.SALSA.Eyes::CreateBoneAverageGizmo(System.Collections.Generic.List`1<CrazyMinnow.SALSA.ProcTrans>,System.String)
extern void Eyes_CreateBoneAverageGizmo_mF9B9CBF707AD86CD7D242FEC02AC98B30FDF618D (void);
// 0x000000EB UnityEngine.Transform CrazyMinnow.SALSA.Eyes::CreateBoneGizmo(UnityEngine.Transform,System.String)
extern void Eyes_CreateBoneGizmo_m32B8D8055A35A2D120D9C13A1CD633BD09A90CBF (void);
// 0x000000EC CrazyMinnow.SALSA.EyeGizmo CrazyMinnow.SALSA.Eyes::CreateEyeGizmo(System.String,UnityEngine.Transform)
extern void Eyes_CreateEyeGizmo_mFEEB2D58B46E54F33D235ADA6C3543B9AAE2F786 (void);
// 0x000000ED UnityEngine.Transform CrazyMinnow.SALSA.Eyes::CreateTrackingGizmo(UnityEngine.Transform,System.String,System.Single,System.Boolean,UnityEngine.Vector3)
extern void Eyes_CreateTrackingGizmo_m3FBBCF397E860A44B4DD9F96B8DC8D0FC73F2F88 (void);
// 0x000000EE System.Void CrazyMinnow.SALSA.Eyes::ShowGizmos(System.Boolean)
extern void Eyes_ShowGizmos_m96FDA5D2CE1D7CBD88D3005352FA017BF7632509 (void);
// 0x000000EF UnityEngine.Transform CrazyMinnow.SALSA.Eyes::FindTransform(UnityEngine.Transform,System.String)
extern void Eyes_FindTransform_m2275093CBDEB77AE9833C482E267E08BBA88DCD5 (void);
// 0x000000F0 UnityEngine.Transform CrazyMinnow.SALSA.Eyes::FindTransform(UnityEngine.Transform,System.String[])
extern void Eyes_FindTransform_mF266C8B915F64055510E796B10DBD292B035AE5C (void);
// 0x000000F1 System.Int32 CrazyMinnow.SALSA.Eyes::FindBlendIndex(UnityEngine.SkinnedMeshRenderer,System.String)
extern void Eyes_FindBlendIndex_m445FEAE0E6B275B4ED768BD29E0E8CB5B92A1F80 (void);
// 0x000000F2 System.Int32 CrazyMinnow.SALSA.Eyes::FindBlendIndex(UnityEngine.SkinnedMeshRenderer,System.String[])
extern void Eyes_FindBlendIndex_m87DDBDF74E19BAD7C34DE497B67DA73ABB92CE94 (void);
// 0x000000F3 CrazyMinnow.SALSA.ProcDirection CrazyMinnow.SALSA.Eyes::GetClampedRotation(UnityEngine.Transform,UnityEngine.Transform,UnityEngine.Vector3,CrazyMinnow.SALSA.Eyes/ClampAxes,System.Boolean,System.Boolean,System.Single,UnityEngine.Vector3)
extern void Eyes_GetClampedRotation_mC41DA289572EA0E4AC25AB0DF8FCA7EEA55FB932 (void);
// 0x000000F4 System.Single CrazyMinnow.SALSA.Eyes::ClampAngle(System.Single,System.Single)
extern void Eyes_ClampAngle_m2CA64670BF3384A92550B501F1658354F45451F3 (void);
// 0x000000F5 System.Single CrazyMinnow.SALSA.Eyes::GetSignedAngle(UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.Vector3)
extern void Eyes_GetSignedAngle_m9A187A9BD5F0D775D5450890FD277B6DA8D99B19 (void);
// 0x000000F6 UnityEngine.Vector3 CrazyMinnow.SALSA.Eyes::GenerateRandomSaccadePosition(UnityEngine.Transform,UnityEngine.Transform,UnityEngine.Vector2,System.Boolean)
extern void Eyes_GenerateRandomSaccadePosition_m256817CD6536C32EF229C5DE01336EFC56FB6907 (void);
// 0x000000F7 UnityEngine.Vector3 CrazyMinnow.SALSA.Eyes::GenerateRandomTargetPosition(UnityEngine.Transform,UnityEngine.Vector3,System.Single,System.Boolean,UnityEngine.Vector3)
extern void Eyes_GenerateRandomTargetPosition_mC02A64DCD992BC19B17F8AE5034DA0CB3ADEC8CC (void);
// 0x000000F8 System.Single CrazyMinnow.SALSA.Eyes::GenerateRandomDistanceExtent(System.Single,System.Single)
extern void Eyes_GenerateRandomDistanceExtent_mE0B108ABE1B7206312E940393A47BCC40D79B32D (void);
// 0x000000F9 UnityEngine.Vector3 CrazyMinnow.SALSA.Eyes::EyeRotToEyeShapes(CrazyMinnow.SALSA.ExpressionComponent/DirectionType,UnityEngine.Vector3)
extern void Eyes_EyeRotToEyeShapes_mF3183DDBA3990233CC0F108F16DB1C1EDBDA18BE (void);
// 0x000000FA UnityEngine.Vector3 CrazyMinnow.SALSA.Eyes::EyeRotToEyelidPos(CrazyMinnow.SALSA.EyesExpression,System.Int32,UnityEngine.Vector3,CrazyMinnow.SALSA.ExpressionComponent/DirectionType)
extern void Eyes_EyeRotToEyelidPos_m4C7434E92BC2D9234601B29365C69FD473F72E02 (void);
// 0x000000FB UnityEngine.Vector3 CrazyMinnow.SALSA.Eyes::EyeRotToEyelidScale(CrazyMinnow.SALSA.EyesExpression,System.Int32,UnityEngine.Vector3,CrazyMinnow.SALSA.ExpressionComponent/DirectionType)
extern void Eyes_EyeRotToEyelidScale_m99F4C5D7BCDEF0610EF5BC1499E65DF089F32E19 (void);
// 0x000000FC UnityEngine.Vector3 CrazyMinnow.SALSA.Eyes::EyePosToEyelidRot(CrazyMinnow.SALSA.EyesExpression,System.Int32)
extern void Eyes_EyePosToEyelidRot_m23A1384B086294769DCC3121151AE9BAD1B813B9 (void);
// 0x000000FD UnityEngine.Vector3 CrazyMinnow.SALSA.Eyes::EyeRotToEyelidShapes(CrazyMinnow.SALSA.EyesExpression,System.Int32,UnityEngine.Vector3,CrazyMinnow.SALSA.ExpressionComponent/DirectionType)
extern void Eyes_EyeRotToEyelidShapes_m1DADC3C5CD6E73C5A86CF8A2DBB2282EB39D2A11 (void);
// 0x000000FE UnityEngine.Vector3 CrazyMinnow.SALSA.Eyes::EyeRotToEyelidSwap(CrazyMinnow.SALSA.EyesExpression,System.Int32,UnityEngine.Vector3,CrazyMinnow.SALSA.ExpressionComponent/DirectionType)
extern void Eyes_EyeRotToEyelidSwap_m8B615D210B2501DF1D97B6CAF8497E91C6F81E90 (void);
// 0x000000FF UnityEngine.Vector3 CrazyMinnow.SALSA.Eyes::AnglesToDirVector(UnityEngine.Vector3,UnityEngine.Transform,UnityEngine.Transform)
extern void Eyes_AnglesToDirVector_mBA93C1759153EF2F378CB0834BCBD0BE9955AB86 (void);
// 0x00000100 System.Boolean CrazyMinnow.SALSA.Eyes::RemoveExpression(System.Collections.Generic.List`1<CrazyMinnow.SALSA.EyesExpression>&,System.Int32)
extern void Eyes_RemoveExpression_mEB8E35A22571933E6F6C8F87B2EA046FEE9FCE71 (void);
// 0x00000101 System.Boolean CrazyMinnow.SALSA.Eyes::RemoveComponent(System.Collections.Generic.List`1<CrazyMinnow.SALSA.EyesExpression>&,System.Int32,System.Int32)
extern void Eyes_RemoveComponent_mD7CCE3CB06373721AE527952D0EAE53C55B8829A (void);
// 0x00000102 System.Void CrazyMinnow.SALSA.Eyes::CopyBlinkToTrack()
extern void Eyes_CopyBlinkToTrack_m7841A2ACFCE1D883289607FB04EF02D76497EEC7 (void);
// 0x00000103 System.Boolean CrazyMinnow.SALSA.Eyes::AllBonesLinked(System.Collections.Generic.List`1<CrazyMinnow.SALSA.EyesExpression>&)
extern void Eyes_AllBonesLinked_m12B2B8A952C1E660C3CDC3C2A7EF0CD59E575300 (void);
// 0x00000104 UnityEngine.Transform CrazyMinnow.SALSA.Eyes::AddParent(UnityEngine.Transform)
extern void Eyes_AddParent_m7C4053602DCAECEDFFA8F6B01F5FB86CDEFDA7A8 (void);
// 0x00000105 CrazyMinnow.SALSA.AxisFixed CrazyMinnow.SALSA.Eyes::FixTransformAxis(System.Collections.Generic.List`1<CrazyMinnow.SALSA.EyesExpression>&,System.Int32,System.Int32,System.Boolean)
extern void Eyes_FixTransformAxis_mA1CA2EBB6112ED057834CE35C3E0162C98C31BE0 (void);
// 0x00000106 System.Void CrazyMinnow.SALSA.Eyes::FixAllTransformAxes(System.Collections.Generic.List`1<CrazyMinnow.SALSA.EyesExpression>&,System.Boolean)
extern void Eyes_FixAllTransformAxes_m063E1DC1B66B3131807D53760BA28AF516D8AFE3 (void);
// 0x00000107 System.Void CrazyMinnow.SALSA.Eyes::ParentRandomTargets(System.Boolean)
extern void Eyes_ParentRandomTargets_m5C7EFE50D8BE146E76CA73F395902FDA69776718 (void);
// 0x00000108 System.Void CrazyMinnow.SALSA.Eyes::EnableAll(System.Boolean)
extern void Eyes_EnableAll_m2E413D0C12BC92FC381B58EF2D847169E2A81FBE (void);
// 0x00000109 System.Void CrazyMinnow.SALSA.Eyes::EnableHead(System.Boolean)
extern void Eyes_EnableHead_m70E8046C65B449E9423DF93A9B44273624D9838C (void);
// 0x0000010A System.Void CrazyMinnow.SALSA.Eyes::EnableEye(System.Boolean)
extern void Eyes_EnableEye_m0DD53663FCAF2A8C318AB81AF6E679AFB7222240 (void);
// 0x0000010B System.Void CrazyMinnow.SALSA.Eyes::EnableEyelidBlink(System.Boolean)
extern void Eyes_EnableEyelidBlink_m78FA625F864C44623C17154BCB16AE3B5F3C67E4 (void);
// 0x0000010C System.Void CrazyMinnow.SALSA.Eyes::EnableEyelidTrack(System.Boolean)
extern void Eyes_EnableEyelidTrack_mA1233839C10252ADD43F25EA2F010D08CB132486 (void);
// 0x0000010D System.Void CrazyMinnow.SALSA.Eyes::SetForwardPosition(UnityEngine.Transform&,UnityEngine.Transform,System.Boolean,UnityEngine.Vector3)
extern void Eyes_SetForwardPosition_m80FBB8B818EEB4620313EE3446C2EB2996CA909D (void);
// 0x0000010E System.Void CrazyMinnow.SALSA.Eyes::LookForward()
extern void Eyes_LookForward_mF58378D8ADDCB202AC3347483FE2CB73CF51C30C (void);
// 0x0000010F System.Void CrazyMinnow.SALSA.Eyes::NewRandomHeadTarget()
extern void Eyes_NewRandomHeadTarget_m8E96F9524D4BB9273E8D5D4B889149C2643B606A (void);
// 0x00000110 System.Void CrazyMinnow.SALSA.Eyes::NewRandomEyeTarget()
extern void Eyes_NewRandomEyeTarget_mD736CDBE76499D2EF7F96C7E138639AD157A27DB (void);
// 0x00000111 System.Void CrazyMinnow.SALSA.Eyes::NewBlink(System.Single,System.Single,System.Single)
extern void Eyes_NewBlink_m52C53392F03C43D436708E43D561BD39ADAD83F8 (void);
// 0x00000112 System.Void CrazyMinnow.SALSA.Eyes::UpdateRuntimeExpressionControllers(System.Collections.Generic.List`1<CrazyMinnow.SALSA.EyesExpression>&)
extern void Eyes_UpdateRuntimeExpressionControllers_m9349227C9D6FE00FCFB47AABC3B32212615A3DC8 (void);
// 0x00000113 System.Boolean CrazyMinnow.SALSA.Eyes::GetFlipState()
extern void Eyes_GetFlipState_m988B82D7BF48E5525DD3E63D0A287B50713772B6 (void);
// 0x00000114 System.Single CrazyMinnow.SALSA.Eyes::GetDegree(UnityEngine.Transform,UnityEngine.Vector3)
extern void Eyes_GetDegree_m6C954CC64F0D5211FAE87354F20BE0017CD54569 (void);
// 0x00000115 CrazyMinnow.SALSA.ExpressionComponent/DirectionType CrazyMinnow.SALSA.Eyes::GetSectorFromDegree(System.Single,CrazyMinnow.SALSA.Eyes/SectorCount,System.Single)
extern void Eyes_GetSectorFromDegree_m8C0C107A445A06FA7FDDBB7A5A501777F9768E60 (void);
// 0x00000116 System.Single CrazyMinnow.SALSA.Eyes::GetProportionalMovementAmount(System.Single,System.Single,System.Single)
extern void Eyes_GetProportionalMovementAmount_m293C3E9FC73BD06066DEDCE1B4EF02D58813EDBD (void);
// 0x00000117 System.Int32 CrazyMinnow.SALSA.Eyes::GetSectorIndex(CrazyMinnow.SALSA.ExpressionComponent/DirectionType)
extern void Eyes_GetSectorIndex_m428CCCDEA23CCB1673712A436F23F172EBED9AB1 (void);
// 0x00000118 System.Void CrazyMinnow.SALSA.Eyes::ActivateSector(System.Int32,CrazyMinnow.SALSA.ExpressionComponent/DirectionType,System.Int32,CrazyMinnow.SALSA.ExpressionComponent/DirectionType)
extern void Eyes_ActivateSector_m9FACFD7372B0659F777041AEC648DCA3847AF5C2 (void);
// 0x00000119 System.Void CrazyMinnow.SALSA.Eyes::CaptureMin(System.Collections.Generic.List`1<CrazyMinnow.SALSA.EyesExpression>&,System.Int32,System.Int32)
extern void Eyes_CaptureMin_m43CED4D350DBB1498DD4EFDB510B2872BE814046 (void);
// 0x0000011A System.Void CrazyMinnow.SALSA.Eyes::CaptureMin(System.Collections.Generic.List`1<CrazyMinnow.SALSA.EyesExpression>&)
extern void Eyes_CaptureMin_mAFFCF2A242674A14E36B4CAD435E84AC530A3528 (void);
// 0x0000011B System.Void CrazyMinnow.SALSA.Eyes::CaptureMax(System.Collections.Generic.List`1<CrazyMinnow.SALSA.EyesExpression>&,System.Int32,System.Int32)
extern void Eyes_CaptureMax_mC8D1A55023F5C2D0BF01679B5F59F5469D8A28A7 (void);
// 0x0000011C System.Void CrazyMinnow.SALSA.Eyes::CaptureMax(System.Collections.Generic.List`1<CrazyMinnow.SALSA.EyesExpression>&)
extern void Eyes_CaptureMax_mC902522C1DCF6A51C67F4F259988440E604DBA5A (void);
// 0x0000011D System.Void CrazyMinnow.SALSA.Eyes::ReturnMin(System.Collections.Generic.List`1<CrazyMinnow.SALSA.EyesExpression>&,System.Int32,System.Int32)
extern void Eyes_ReturnMin_m805908A38546B8CBA8555371983EF61304C9F6A6 (void);
// 0x0000011E System.Void CrazyMinnow.SALSA.Eyes::ReturnMin(System.Collections.Generic.List`1<CrazyMinnow.SALSA.EyesExpression>&)
extern void Eyes_ReturnMin_mB96B8DA46FAAF908044977138E097C994BB27467 (void);
// 0x0000011F CrazyMinnow.SALSA.AxisOriginal CrazyMinnow.SALSA.Eyes::GetEyeAxisOriginal(System.Int32,System.Int32)
extern void Eyes_GetEyeAxisOriginal_m238E272AEAC7CAF2F12B2F3299594F36C370C9D9 (void);
// 0x00000120 System.Void CrazyMinnow.SALSA.Eyes::OverwriteAnimation()
extern void Eyes_OverwriteAnimation_m661AC9C67A7EBC618661AB0BE0C8548D7228F50A (void);
// 0x00000121 System.Void CrazyMinnow.SALSA.Eyes::DestroyProcTransOffsetGizmos(System.Collections.Generic.List`1<CrazyMinnow.SALSA.ProcTransOffset>&)
extern void Eyes_DestroyProcTransOffsetGizmos_mA82C2E84C9D1AEBC7C0E1E143A63212A7380172E (void);
// 0x00000122 System.Void CrazyMinnow.SALSA.Eyes::DestroyProcTransGizmos(System.Collections.Generic.List`1<CrazyMinnow.SALSA.ProcTrans>&)
extern void Eyes_DestroyProcTransGizmos_mC5B7DC181114697A257949EC68183B5881065985 (void);
// 0x00000123 System.Boolean CrazyMinnow.SALSA.Eyes::CheckConfigIsReady()
extern void Eyes_CheckConfigIsReady_m7A68134DC67EFADE3240B567202ED3DAE0D74FAE (void);
// 0x00000124 System.Void CrazyMinnow.SALSA.Eyes::Initialize()
extern void Eyes_Initialize_m779AB2DE24FA085098150D0CBA44D44A721213A9 (void);
// 0x00000125 System.Void CrazyMinnow.SALSA.Eyes::Awake()
extern void Eyes_Awake_m64A934856A9AC256248825C104439C23CB38AEDD (void);
// 0x00000126 System.Void CrazyMinnow.SALSA.Eyes::Start()
extern void Eyes_Start_m41DABC5B8EB61C4A5D623721A5018F66514315F2 (void);
// 0x00000127 System.Void CrazyMinnow.SALSA.Eyes::OnEnable()
extern void Eyes_OnEnable_m14CCE3CB6B4601AE04D540E19779787FF1D1AE8D (void);
// 0x00000128 System.Void CrazyMinnow.SALSA.Eyes::OnScaledTimeResumed(System.Object,CrazyMinnow.SALSA.QueueProcessor/QueueProcessorNotificationArgs)
extern void Eyes_OnScaledTimeResumed_mEAD4AA42329363C882FE40B504E3FB4E5F161495 (void);
// 0x00000129 System.Void CrazyMinnow.SALSA.Eyes::OnDisable()
extern void Eyes_OnDisable_m163B12181F0145A3B94E6B6000FE03F40BE1D4A2 (void);
// 0x0000012A System.Void CrazyMinnow.SALSA.Eyes::LateUpdate()
extern void Eyes_LateUpdate_m245547CCA575A7EBE3B253C9B3D791BE7371A3C3 (void);
// 0x0000012B System.Void CrazyMinnow.SALSA.Eyes::.ctor()
extern void Eyes__ctor_m796C24E7A7CC6E6C49C0F762297894BC0087D049 (void);
// 0x0000012C System.Void CrazyMinnow.SALSA.EyesExpression::.ctor(System.String,System.Boolean)
extern void EyesExpression__ctor_mB429B2E13FC29DAD70CCF61E40582BB9BA576FEB (void);
// 0x0000012D System.Void CrazyMinnow.SALSA.AnimCancel::.ctor()
extern void AnimCancel__ctor_m0F35FCAE32EB7A7698A00B85C708551A94115FA9 (void);
// 0x0000012E System.Void CrazyMinnow.SALSA.AnimCancel::Capture()
extern void AnimCancel_Capture_m04038D13868722E82AFF5C7AC8AC793B516A97BF (void);
// 0x0000012F System.Void CrazyMinnow.SALSA.AnimCancel::Overwrite()
extern void AnimCancel_Overwrite_m6C505E532107F4B5D51EEAED931F2FF457874C6A (void);
// 0x00000130 System.Void CrazyMinnow.SALSA.ProcTransOffset::.ctor()
extern void ProcTransOffset__ctor_mAB847A2EA52BB6816A2FC08AD4D1876EB91C4F86 (void);
// 0x00000131 System.Void CrazyMinnow.SALSA.ProcTrans::.ctor()
extern void ProcTrans__ctor_mA98D2DBB2D72FA6102BEEFF05FE62FE8B5C1CE1E (void);
// 0x00000132 System.Void CrazyMinnow.SALSA.ProcTForm::.ctor()
extern void ProcTForm__ctor_mC314D77BDD5990220D420024C4451C058EC440D6 (void);
// 0x00000133 System.Void CrazyMinnow.SALSA.ProcAxisOrig::.ctor()
extern void ProcAxisOrig__ctor_m1359EF04A2B22D78569EDE5D12A9F67D17775BDA (void);
// 0x00000134 System.Void CrazyMinnow.SALSA.ProcVecs::.ctor()
extern void ProcVecs__ctor_m262FA8C804E35E228FA8B6C64474080F3A7F40B8 (void);
// 0x00000135 System.Void CrazyMinnow.SALSA.ProcLLPair::.ctor(UnityEngine.Vector3,System.Int32)
extern void ProcLLPair__ctor_m150C12200B6ED2F62F542B40E9A0E42228AD0816 (void);
// 0x00000136 System.Void CrazyMinnow.SALSA.ProcLLPairs::.ctor()
extern void ProcLLPairs__ctor_m16963B5573A529317AFA4534427F8330D0AD8AAD (void);
// 0x00000137 System.Void CrazyMinnow.SALSA.ProcDirection::.ctor()
extern void ProcDirection__ctor_mAE04E1A07192555A8DFB719AE95CDA5D96BA48C6 (void);
// 0x00000138 System.Void CrazyMinnow.SALSA.ProcDirections::.ctor()
extern void ProcDirections__ctor_m52C91C467E9B99E8E51E5386AE41BC5BDB351401 (void);
// 0x00000139 System.Void CrazyMinnow.SALSA.InspectorControllerHelperData::.ctor()
extern void InspectorControllerHelperData__ctor_m4FDF274348A7A6722D23862A80B19EB5C54E3099 (void);
// 0x0000013A System.Void CrazyMinnow.SALSA.InspectorControllerHelperData::.ctor(CrazyMinnow.SALSA.InspectorControllerHelperData)
extern void InspectorControllerHelperData__ctor_m2BF06F1E42FAED1218484B07F283D64537D92915 (void);
// 0x0000013B System.Void CrazyMinnow.SALSA.InspectorControllerHelperData::StoreBoneBase()
extern void InspectorControllerHelperData_StoreBoneBase_mFB840295B298519E279DE9AAA32863B297EAC7CB (void);
// 0x0000013C System.Void CrazyMinnow.SALSA.InspectorControllerHelperData::StoreStartTform()
extern void InspectorControllerHelperData_StoreStartTform_m120D086FE77023DC57B52460C305ECB3512150E3 (void);
// 0x0000013D System.Void CrazyMinnow.SALSA.InspectorControllerHelperData::StoreEndTform()
extern void InspectorControllerHelperData_StoreEndTform_m4176D62949C93C9E831F37CF8EB9DF1BCB7B4ACB (void);
// 0x0000013E System.Void CrazyMinnow.SALSA.InspectorControllerHelperData::ResetBone(CrazyMinnow.SALSA.TformBase,System.Boolean)
extern void InspectorControllerHelperData_ResetBone_mF2258FDB275E314C8DEA5004D997AA16B255125A (void);
// 0x0000013F System.Void CrazyMinnow.SALSA.InspectorControllerHelperData::ClearStoredTforms()
extern void InspectorControllerHelperData_ClearStoredTforms_m16DC9C339386557C4A32015BA55A6B44319C6126 (void);
// 0x00000140 UnityEngine.Sprite[] CrazyMinnow.SALSA.InspectorControllerHelperData::GetSprites()
extern void InspectorControllerHelperData_GetSprites_mE5940092676969BC9F63D28C309882F3DB616F02 (void);
// 0x00000141 UnityEngine.Texture[] CrazyMinnow.SALSA.InspectorControllerHelperData::GetTextures()
extern void InspectorControllerHelperData_GetTextures_m19270F028D0A040262FA75BF0A59F94BF6910664 (void);
// 0x00000142 UnityEngine.Material[] CrazyMinnow.SALSA.InspectorControllerHelperData::GetMaterials()
extern void InspectorControllerHelperData_GetMaterials_m3F0B3A72C75F832BA0C57A2C9CF919BB3F3F146F (void);
// 0x00000143 System.Single CrazyMinnow.SALSA.LerpEasings::Ease(System.Single,CrazyMinnow.SALSA.LerpEasings/EasingType)
extern void LerpEasings_Ease_m84D5C1532DE05B873451DBCA2ACC4EE0A62F179B (void);
// 0x00000144 System.Single CrazyMinnow.SALSA.LerpEasings::CubicOut(System.Single)
extern void LerpEasings_CubicOut_m73A7B7334B6E7945D5E7FE0DC668D9A4A06E3793 (void);
// 0x00000145 System.Single CrazyMinnow.SALSA.LerpEasings::CubicIn(System.Single)
extern void LerpEasings_CubicIn_m09EB008C3C5EEF5C7FC9642A64A4B753EF9B497B (void);
// 0x00000146 System.Single CrazyMinnow.SALSA.LerpEasings::SquaredIn(System.Single)
extern void LerpEasings_SquaredIn_mD67BBAE3D01FE9A46CE29E5A1468312BDA40119E (void);
// 0x00000147 System.Single CrazyMinnow.SALSA.LerpEasings::CircleIn(System.Single)
extern void LerpEasings_CircleIn_m99711A2649BD04138260D14361E28617E87B5DA4 (void);
// 0x00000148 System.Single CrazyMinnow.SALSA.LerpEasings::CircleOut(System.Single)
extern void LerpEasings_CircleOut_m6B40E1FCF7E6F0E4924BDAF6235E4FB374F4FACF (void);
// 0x00000149 System.Single CrazyMinnow.SALSA.LerpEasings::CubicInOut(System.Single)
extern void LerpEasings_CubicInOut_m2E178C953FC92AF08DB2D5DF81706AD141AC7B64 (void);
// 0x0000014A System.Single CrazyMinnow.SALSA.LerpEasings::QuintOut(System.Single)
extern void LerpEasings_QuintOut_m3C49B643C7C59626EF34C870B0C4EE6FB8C245CB (void);
// 0x0000014B System.Single CrazyMinnow.SALSA.LerpEasings::QuarticOut(System.Single)
extern void LerpEasings_QuarticOut_mA3FDA2211D28B42EDAC6762C17CE97DA2BE1B996 (void);
// 0x0000014C System.Single CrazyMinnow.SALSA.LerpEasings::SinIn(System.Single)
extern void LerpEasings_SinIn_m7750F13627CBFDE514E61DA447E49E8F89F82B25 (void);
// 0x0000014D System.Single CrazyMinnow.SALSA.LerpEasings::SinOut(System.Single)
extern void LerpEasings_SinOut_m7BACAFF122B6494BB6BEEA267402698CA8C7D74B (void);
// 0x0000014E System.Void CrazyMinnow.SALSA.LipsyncExpression::.ctor(System.String,CrazyMinnow.SALSA.InspectorControllerHelperData,System.Single,System.Boolean)
extern void LipsyncExpression__ctor_m150DF066C6BC08A4567BA6040981F5CFAFAF9CF5 (void);
// 0x0000014F System.Void CrazyMinnow.SALSA.QueueExpressionComponent::.ctor(CrazyMinnow.SALSA.ExpressionComponent)
extern void QueueExpressionComponent__ctor_m2070728740E1660D6F0AE41D0BA7A08D86E005C7 (void);
// 0x00000150 System.Void CrazyMinnow.SALSA.QueueExpressionComponent::.ctor(CrazyMinnow.SALSA.QueueExpressionComponent)
extern void QueueExpressionComponent__ctor_m36912621696266F4638E6F38E2CFC386DDC0A76B (void);
// 0x00000151 System.Void CrazyMinnow.SALSA.QueueExpressionComponent::.ctor(CrazyMinnow.SALSA.IExpressionController,CrazyMinnow.SALSA.ExpressionComponent/ControlType,System.Single,System.Single,System.Single,System.Single,System.Boolean,CrazyMinnow.SALSA.ExpressionComponent/ExpressionType,CrazyMinnow.SALSA.LerpEasings/EasingType,CrazyMinnow.SALSA.ExpressionComponent/ExpressionHandler,System.Boolean,System.Boolean,System.String)
extern void QueueExpressionComponent__ctor_m2AE432C1E57A79D5F465EE30CFFDE1DEFDC6F976 (void);
// 0x00000152 System.Void CrazyMinnow.SALSA.QueueData::.ctor(CrazyMinnow.SALSA.ExpressionComponent,System.Single,System.Single,System.Boolean,System.Int32)
extern void QueueData__ctor_m869788F7850F5E9FA956C53E4F21FFB656011ACE (void);
// 0x00000153 System.Void CrazyMinnow.SALSA.QueueData::.ctor(CrazyMinnow.SALSA.QueueExpressionComponent,System.Single,System.Single,System.Boolean,System.Int32)
extern void QueueData__ctor_mE61A60FB8F9B593A1DC705557CE0616F3E1FD1F6 (void);
// 0x00000154 System.Void CrazyMinnow.SALSA.QueueData::.ctor(CrazyMinnow.SALSA.IExpressionController,CrazyMinnow.SALSA.ExpressionComponent/ControlType,System.Single,System.Single,System.Single,System.Single,System.Boolean,CrazyMinnow.SALSA.ExpressionComponent/ExpressionType,CrazyMinnow.SALSA.LerpEasings/EasingType,CrazyMinnow.SALSA.ExpressionComponent/ExpressionHandler,System.Boolean,System.Boolean,System.Single,System.Single,System.Boolean,System.Int32)
extern void QueueData__ctor_mBB7166CCFC77FD77222B1C14605C3F0B42311589 (void);
// 0x00000155 System.Void CrazyMinnow.SALSA.QueueProcessor::add_ScaledTimeResumed(System.EventHandler`1<CrazyMinnow.SALSA.QueueProcessor/QueueProcessorNotificationArgs>)
extern void QueueProcessor_add_ScaledTimeResumed_mA8B068661DFB759716F4E7C4CA9ED503D0EAFB36 (void);
// 0x00000156 System.Void CrazyMinnow.SALSA.QueueProcessor::remove_ScaledTimeResumed(System.EventHandler`1<CrazyMinnow.SALSA.QueueProcessor/QueueProcessorNotificationArgs>)
extern void QueueProcessor_remove_ScaledTimeResumed_m9A56C22AF3ED793523E0ED06F343C5931119F26D (void);
// 0x00000157 System.Void CrazyMinnow.SALSA.QueueProcessor::RespectScaledTime()
extern void QueueProcessor_RespectScaledTime_mCBEEA048DD821C227951223448E7679C609F2F81 (void);
// 0x00000158 System.Void CrazyMinnow.SALSA.QueueProcessor::Awake()
extern void QueueProcessor_Awake_mC3719F6B9359848699CD59AB82BBD763238DB356 (void);
// 0x00000159 System.Void CrazyMinnow.SALSA.QueueProcessor::InitializeQueueOrderOfOperation()
extern void QueueProcessor_InitializeQueueOrderOfOperation_m0B54061E1D2D33F2DF481BADA4A18C15EE6EB7EF (void);
// 0x0000015A System.Void CrazyMinnow.SALSA.QueueProcessor::Flush()
extern void QueueProcessor_Flush_m4579CB224E1439E95CF52F41C19B7E8B3C44CD88 (void);
// 0x0000015B System.Void CrazyMinnow.SALSA.QueueProcessor::ShutdownActiveQueue(System.Int32)
extern void QueueProcessor_ShutdownActiveQueue_m240528DFBDEABAF54E36697F89D6FA06321E12DE (void);
// 0x0000015C System.Void CrazyMinnow.SALSA.QueueProcessor::Register(CrazyMinnow.SALSA.ExpressionComponent,System.Boolean,System.Single)
extern void QueueProcessor_Register_m081FE8ACE20CE9648A1BB1D8E8901FDD592E9761 (void);
// 0x0000015D System.Void CrazyMinnow.SALSA.QueueProcessor::Register(CrazyMinnow.SALSA.IExpressionController,CrazyMinnow.SALSA.ExpressionComponent/ControlType,System.Single,System.Single,System.Single,System.Boolean,CrazyMinnow.SALSA.ExpressionComponent/ExpressionType,CrazyMinnow.SALSA.LerpEasings/EasingType,CrazyMinnow.SALSA.ExpressionComponent/ExpressionHandler,System.Boolean,System.Boolean,System.Boolean,System.Single)
extern void QueueProcessor_Register_mC982EAFA114261C912F7D4E972BA007FCB7F0E8D (void);
// 0x0000015E System.Void CrazyMinnow.SALSA.QueueProcessor::Register(CrazyMinnow.SALSA.IExpressionController,CrazyMinnow.SALSA.ExpressionComponent/ControlType,System.Single,System.Single,System.Single,System.Single,System.Boolean,CrazyMinnow.SALSA.ExpressionComponent/ExpressionType,CrazyMinnow.SALSA.LerpEasings/EasingType,CrazyMinnow.SALSA.ExpressionComponent/ExpressionHandler,System.Boolean,System.Boolean,System.Boolean,System.Single)
extern void QueueProcessor_Register_m5F2F5571F5A7F76DAAA220324254F13FB4C56D9A (void);
// 0x0000015F System.Void CrazyMinnow.SALSA.QueueProcessor::ConfirmRegistration(CrazyMinnow.SALSA.QueueData)
extern void QueueProcessor_ConfirmRegistration_mE5DC64971DA64E094A929224F14F6859050D9A08 (void);
// 0x00000160 CrazyMinnow.SALSA.QueueProcessor/QueueOoO CrazyMinnow.SALSA.QueueProcessor::GetRegistrationQueue(CrazyMinnow.SALSA.ExpressionComponent/ExpressionType)
extern void QueueProcessor_GetRegistrationQueue_m1935CA0004E0CB38DFEC2E86CD53C730A11E9845 (void);
// 0x00000161 System.Void CrazyMinnow.SALSA.QueueProcessor::LateUpdate()
extern void QueueProcessor_LateUpdate_mA0A29E1E453C9BC2B142ED237211910590275BAC (void);
// 0x00000162 System.Void CrazyMinnow.SALSA.QueueProcessor::ProcessQueue(System.Collections.Generic.List`1<CrazyMinnow.SALSA.QueueData>,System.Int32)
extern void QueueProcessor_ProcessQueue_mC6258813D82C24379B012F75321B620F570982BA (void);
// 0x00000163 System.Void CrazyMinnow.SALSA.QueueProcessor::.ctor()
extern void QueueProcessor__ctor_m98D9DEFA87E636FE6166A7632F0D9E4FB4A9204A (void);
// 0x00000164 System.Void CrazyMinnow.SALSA.QueueProcessor/QueueOoO::.ctor(System.String,System.Collections.Generic.List`1<CrazyMinnow.SALSA.QueueData>,System.Int32)
extern void QueueOoO__ctor_m97FB9C803279C26A8CCDA89F7C63BE985F8776EE (void);
// 0x00000165 System.Void CrazyMinnow.SALSA.QueueProcessor/QueueProcessorNotificationArgs::.ctor()
extern void QueueProcessorNotificationArgs__ctor_m76BD181B7709738AB2C7EB20C365912E60DD02F2 (void);
// 0x00000166 System.Single CrazyMinnow.SALSA.Salsa::get_CachedAnalysisValue()
extern void Salsa_get_CachedAnalysisValue_mCFA56E72A407395A6ABD7F8DBA3B7A78D46200F7 (void);
// 0x00000167 System.Boolean CrazyMinnow.SALSA.Salsa::get_IsSALSAing()
extern void Salsa_get_IsSALSAing_m08BB419CAD1A1852094610902B2A112DF2647F29 (void);
// 0x00000168 System.Int32 CrazyMinnow.SALSA.Salsa::get_TriggeredIndex()
extern void Salsa_get_TriggeredIndex_m5F6A4E11FA84288C6833C7B478E3C665EE6D1609 (void);
// 0x00000169 System.Void CrazyMinnow.SALSA.Salsa::add_StartedSalsaing(System.EventHandler`1<CrazyMinnow.SALSA.Salsa/SalsaNotificationArgs>)
extern void Salsa_add_StartedSalsaing_mDFA1FF3D0181546C7DD01FAF22A3F7207C089C46 (void);
// 0x0000016A System.Void CrazyMinnow.SALSA.Salsa::remove_StartedSalsaing(System.EventHandler`1<CrazyMinnow.SALSA.Salsa/SalsaNotificationArgs>)
extern void Salsa_remove_StartedSalsaing_mD8CB3F704734628D926C9687057000079716A55B (void);
// 0x0000016B System.Void CrazyMinnow.SALSA.Salsa::add_StoppedSalsaing(System.EventHandler`1<CrazyMinnow.SALSA.Salsa/SalsaNotificationArgs>)
extern void Salsa_add_StoppedSalsaing_m41629779A7EEE50D9E884F0D5FE0D78E2216B708 (void);
// 0x0000016C System.Void CrazyMinnow.SALSA.Salsa::remove_StoppedSalsaing(System.EventHandler`1<CrazyMinnow.SALSA.Salsa/SalsaNotificationArgs>)
extern void Salsa_remove_StoppedSalsaing_m2C6E8AB8478DFEDFF2F43068690708DB82F747E5 (void);
// 0x0000016D System.Void CrazyMinnow.SALSA.Salsa::add_VisemeTriggered(System.EventHandler`1<CrazyMinnow.SALSA.Salsa/SalsaNotificationArgs>)
extern void Salsa_add_VisemeTriggered_mE59483C9050A7539895DEA66032BF24F647D6F76 (void);
// 0x0000016E System.Void CrazyMinnow.SALSA.Salsa::remove_VisemeTriggered(System.EventHandler`1<CrazyMinnow.SALSA.Salsa/SalsaNotificationArgs>)
extern void Salsa_remove_VisemeTriggered_m041889BB2A473C46AFE1B8656B096F07D5FDE9EE (void);
// 0x0000016F System.Void CrazyMinnow.SALSA.Salsa::FireStartedSalsaingEvent()
extern void Salsa_FireStartedSalsaingEvent_m82BFC9C22FDC79087BD474AF0502FEEB21C8BED2 (void);
// 0x00000170 System.Void CrazyMinnow.SALSA.Salsa::FireStoppedSalsaingEvent()
extern void Salsa_FireStoppedSalsaingEvent_m59B795C0C737BE732FB36C731E8636EDFD7B4A3B (void);
// 0x00000171 System.Void CrazyMinnow.SALSA.Salsa::FireVisemeTriggeredEvent()
extern void Salsa_FireVisemeTriggeredEvent_mD904DE7DC81C552309FFF57EE402F478BC64228A (void);
// 0x00000172 System.Void CrazyMinnow.SALSA.Salsa::Awake()
extern void Salsa_Awake_m8750E72A3B002D8AF2FD18F1F1CF49952160E9EF (void);
// 0x00000173 System.Void CrazyMinnow.SALSA.Salsa::OnEnable()
extern void Salsa_OnEnable_m45467B631D3844264A2AB3DB1A0AD07C6A4BB045 (void);
// 0x00000174 System.Void CrazyMinnow.SALSA.Salsa::OnScaledTimeResumed(System.Object,CrazyMinnow.SALSA.QueueProcessor/QueueProcessorNotificationArgs)
extern void Salsa_OnScaledTimeResumed_m823E405261663E2A5BF8AB0D3D5BCF541FDDEF9E (void);
// 0x00000175 System.Void CrazyMinnow.SALSA.Salsa::OnDisable()
extern void Salsa_OnDisable_m7BD6142842F093F139832B87E65F923C80D2B76B (void);
// 0x00000176 System.Void CrazyMinnow.SALSA.Salsa::InitializeDelegates()
extern void Salsa_InitializeDelegates_mD76D22EB8C34D9FFE5ECA49E8461157BE73A4A83 (void);
// 0x00000177 System.Void CrazyMinnow.SALSA.Salsa::StartWaitForAudio()
extern void Salsa_StartWaitForAudio_m47C0E613C6AF55FD13E4BB95F6FE87F3D856E907 (void);
// 0x00000178 System.Boolean CrazyMinnow.SALSA.Salsa::IsWithinPulseCheckInterruptVariance(System.Single,System.Single)
extern void Salsa_IsWithinPulseCheckInterruptVariance_m6AE690752C299A71A552B0E5BB04616F664310B4 (void);
// 0x00000179 System.Void CrazyMinnow.SALSA.Salsa::LateUpdate()
extern void Salsa_LateUpdate_mDC7B07325DE883B78B67F3AA601324CCB8C820F2 (void);
// 0x0000017A System.Void CrazyMinnow.SALSA.Salsa::ProcessSilenceTrigger()
extern void Salsa_ProcessSilenceTrigger_m44C31D3AAAE6AD63F4AC3B283368F1B3BB92939B (void);
// 0x0000017B System.Void CrazyMinnow.SALSA.Salsa::ProcessAudioAnalysis()
extern void Salsa_ProcessAudioAnalysis_m437098DAA809BC6F1CDF04F8779480FBA6C7B1FF (void);
// 0x0000017C System.Int32 CrazyMinnow.SALSA.Salsa::SalsaLssGetTriggeredIndex()
extern void Salsa_SalsaLssGetTriggeredIndex_m4097023016DF1E96D9CCC1A54A52FE88F8AAC78C (void);
// 0x0000017D System.Void CrazyMinnow.SALSA.Salsa::ProcessEmphasizerEmote()
extern void Salsa_ProcessEmphasizerEmote_m1D5E011C67233585B4AFB4C08882E50489EF2095 (void);
// 0x0000017E System.Void CrazyMinnow.SALSA.Salsa::TurnPreviouslyTriggeredVisemeOff(System.Int32)
extern void Salsa_TurnPreviouslyTriggeredVisemeOff_mB6F0A8420E9FA89BFAC04AA5AA59C142637B7348 (void);
// 0x0000017F System.Void CrazyMinnow.SALSA.Salsa::TurnVisemComponentsOff(System.Int32)
extern void Salsa_TurnVisemComponentsOff_mB2F87AC392C36E5B62E0361E93D7BC36FB53EAA6 (void);
// 0x00000180 System.Int32 CrazyMinnow.SALSA.Salsa::GetSecondaryMixIndex(System.Int32)
extern void Salsa_GetSecondaryMixIndex_mC20D3171A9D5BD663B58209EF83B7457B37CB32F (void);
// 0x00000181 System.Void CrazyMinnow.SALSA.Salsa::RegisterComponentOn(CrazyMinnow.SALSA.ExpressionComponent,System.Single)
extern void Salsa_RegisterComponentOn_mA7AA78F8098FD37C71CA43CAB15A701DB1821481 (void);
// 0x00000182 System.Void CrazyMinnow.SALSA.Salsa::RegisterComponentOff(CrazyMinnow.SALSA.ExpressionComponent,System.Single)
extern void Salsa_RegisterComponentOff_m2BA3ADEE9DBDE488FD8CB28B1E1685CB48C9584D (void);
// 0x00000183 System.Single CrazyMinnow.SALSA.Salsa::CalcAdvancedDynamics(System.Int32)
extern void Salsa_CalcAdvancedDynamics_m3522A677DE2438E5463248944EE10F38AB96A26D (void);
// 0x00000184 System.Void CrazyMinnow.SALSA.Salsa::TurnVisemeOn(System.Int32,System.Single)
extern void Salsa_TurnVisemeOn_m7072EBD688E4CCA3E51CEF358349E89A004D90DE (void);
// 0x00000185 System.Void CrazyMinnow.SALSA.Salsa::TurnVisemeComponentsOn(System.Int32,System.Single)
extern void Salsa_TurnVisemeComponentsOn_m6270BDD6BFE8831D7800978F26AE72C16CA36A75 (void);
// 0x00000186 System.Single CrazyMinnow.SALSA.Salsa::AnalyzeAudio()
extern void Salsa_AnalyzeAudio_m1F47BA25642DDF4EF6F55A01CC42B1DEA4B47ACA (void);
// 0x00000187 System.Boolean CrazyMinnow.SALSA.Salsa::IsOffsetPointerOutOfBounds(System.Int32,System.Int32)
extern void Salsa_IsOffsetPointerOutOfBounds_m0D07F31F51E1288168FAE5B5175E95E283FF118C (void);
// 0x00000188 System.Int32 CrazyMinnow.SALSA.Salsa::CalcClipOffsetPointer(System.Int32)
extern void Salsa_CalcClipOffsetPointer_m3E8BAD6CDE9870C60BDE881ABA42D665894FCEEE (void);
// 0x00000189 System.Int32 CrazyMinnow.SALSA.Salsa::get_GetSalsaAudioPointer()
extern void Salsa_get_GetSalsaAudioPointer_mA37D6358FBE167D8B2AAFAC1DB41F1892B3C1298 (void);
// 0x0000018A System.Single CrazyMinnow.SALSA.Salsa::ScaleAudio(System.Single)
extern void Salsa_ScaleAudio_mFC2A90BECE72AED403F54B7E7862E7FA8F95616E (void);
// 0x0000018B System.Void CrazyMinnow.SALSA.Salsa::AdjustAnalysisSettings()
extern void Salsa_AdjustAnalysisSettings_m3093BFD52C850590F5D4E3680248AB93315D877D (void);
// 0x0000018C System.Void CrazyMinnow.SALSA.Salsa::SetPlayheadBias()
extern void Salsa_SetPlayheadBias_mA34239734DE7551D7C11713D5E1D704CEFA394BC (void);
// 0x0000018D System.Void CrazyMinnow.SALSA.Salsa::SetSampleSize()
extern void Salsa_SetSampleSize_m8CC64F2CC12C0C009EFE53B4CFF8342E52FE4CC2 (void);
// 0x0000018E System.Collections.IEnumerator CrazyMinnow.SALSA.Salsa::WaitForAudio()
extern void Salsa_WaitForAudio_m660A0F02F3A6233A91CC2D29F5FFAAD0A235E197 (void);
// 0x0000018F System.Void CrazyMinnow.SALSA.Salsa::UpdateExpressionControllers()
extern void Salsa_UpdateExpressionControllers_m0E1F6DD71EBF047651EA41A9FF52B244B1B8F844 (void);
// 0x00000190 System.Void CrazyMinnow.SALSA.Salsa::SetPersistence(System.Boolean)
extern void Salsa_SetPersistence_mB4B5713E91C1A4FCF8AD04320555588FE58C0125 (void);
// 0x00000191 System.Void CrazyMinnow.SALSA.Salsa::SortLipsyncExpressionsOnTrigger()
extern void Salsa_SortLipsyncExpressionsOnTrigger_m8BFD2E12817ED8E151761135A2B13B3C535A8293 (void);
// 0x00000192 System.Void CrazyMinnow.SALSA.Salsa::TurnOffAll()
extern void Salsa_TurnOffAll_m6FBDEED841BD8DE16D4372C41C766417513398BC (void);
// 0x00000193 System.Boolean CrazyMinnow.SALSA.Salsa::CheckConfigIsReady()
extern void Salsa_CheckConfigIsReady_m1F6631A85B7E08D672F38753987809DEA52AEB40 (void);
// 0x00000194 System.Void CrazyMinnow.SALSA.Salsa::DistributeTriggers(CrazyMinnow.SALSA.LerpEasings/EasingType)
extern void Salsa_DistributeTriggers_m6B4C3933FF8ED0FCE4C4039C614CB2D7E2EA506F (void);
// 0x00000195 System.Void CrazyMinnow.SALSA.Salsa::SalsaUndoRedoCallback()
extern void Salsa_SalsaUndoRedoCallback_m9E35205369478702613A00D62AF7D4D19CFA5F01 (void);
// 0x00000196 System.Collections.IEnumerator CrazyMinnow.SALSA.Salsa::WaitForSalsa()
extern void Salsa_WaitForSalsa_m2815A25D22FBA78003ACB8A97A718EBD803853EC (void);
// 0x00000197 System.Single CrazyMinnow.SALSA.Salsa::SalsaLssGetExternalAnalysisValue()
extern void Salsa_SalsaLssGetExternalAnalysisValue_m78B4642FEBEB85125DE4442A62319EE38F61BD65 (void);
// 0x00000198 System.Boolean CrazyMinnow.SALSA.Salsa::SalsaLssGetAudioClipData(System.Single[],System.Int32)
extern void Salsa_SalsaLssGetAudioClipData_m245A41E162AED46736AE145FB8987D9D1F18B465 (void);
// 0x00000199 System.Int32 CrazyMinnow.SALSA.Salsa::SalsaLssGetBufferPointer()
extern void Salsa_SalsaLssGetBufferPointer_m558B4CA2A493CEFEBE66458AA4F2AF3EB38E5979 (void);
// 0x0000019A System.Int32 CrazyMinnow.SALSA.Salsa::SalsaLssGetAudioClipSampleCount()
extern void Salsa_SalsaLssGetAudioClipSampleCount_m7E15A76B68C9161F693ACF81336222B7F3CEF485 (void);
// 0x0000019B System.Int32 CrazyMinnow.SALSA.Salsa::SalsaLssGetAudioClipFrequency()
extern void Salsa_SalsaLssGetAudioClipFrequency_mDE74E4402B69164D77E27E5B4313BE2B4C14368F (void);
// 0x0000019C System.Int32 CrazyMinnow.SALSA.Salsa::SalsaLssGetAudioClipChannels()
extern void Salsa_SalsaLssGetAudioClipChannels_mDF1EE8692AFD84D77FC340DC77E5B1BE6E22C691 (void);
// 0x0000019D System.Void CrazyMinnow.SALSA.Salsa::OnApplicationQuit()
extern void Salsa_OnApplicationQuit_m3C9F202B5B1184117E10739DE47BC397005AAD58 (void);
// 0x0000019E System.Void CrazyMinnow.SALSA.Salsa::.ctor()
extern void Salsa__ctor_m009A32DD10B776DB9C12DE3FA0BCD6E9472FADD6 (void);
// 0x0000019F System.Void CrazyMinnow.SALSA.Salsa/GetExternalAnalysis::.ctor(System.Object,System.IntPtr)
extern void GetExternalAnalysis__ctor_m77E5A87B31F1AFD205B33CAA8C313E9D00B76629 (void);
// 0x000001A0 System.Single CrazyMinnow.SALSA.Salsa/GetExternalAnalysis::Invoke()
extern void GetExternalAnalysis_Invoke_mA5D89EF5A44B81239F02BDCC0E0ACCBFCCCFF9EB (void);
// 0x000001A1 System.IAsyncResult CrazyMinnow.SALSA.Salsa/GetExternalAnalysis::BeginInvoke(System.AsyncCallback,System.Object)
extern void GetExternalAnalysis_BeginInvoke_mC80225B614EDF48C4AFCADF5B88A9FA0A1DE1D91 (void);
// 0x000001A2 System.Single CrazyMinnow.SALSA.Salsa/GetExternalAnalysis::EndInvoke(System.IAsyncResult)
extern void GetExternalAnalysis_EndInvoke_m625515D1CCD735D689A086A1FD624AA4A3D74EB1 (void);
// 0x000001A3 System.Void CrazyMinnow.SALSA.Salsa/AudioAnalyzer::.ctor(System.Object,System.IntPtr)
extern void AudioAnalyzer__ctor_mA07C80DF68DE3E1E836706824305E71EA5208E1B (void);
// 0x000001A4 System.Single CrazyMinnow.SALSA.Salsa/AudioAnalyzer::Invoke(System.Int32,System.Single[])
extern void AudioAnalyzer_Invoke_m38D61A2805EB6CD043D3D21F9FD50C12B5AC5A82 (void);
// 0x000001A5 System.IAsyncResult CrazyMinnow.SALSA.Salsa/AudioAnalyzer::BeginInvoke(System.Int32,System.Single[],System.AsyncCallback,System.Object)
extern void AudioAnalyzer_BeginInvoke_mFBCCDB67962BAC22DE886CA320A0A0493AD23DA7 (void);
// 0x000001A6 System.Single CrazyMinnow.SALSA.Salsa/AudioAnalyzer::EndInvoke(System.IAsyncResult)
extern void AudioAnalyzer_EndInvoke_m2877DBC8AC378C19AEB783E96D12FE83C8714B07 (void);
// 0x000001A7 System.Void CrazyMinnow.SALSA.Salsa/ClipChannels::.ctor(System.Object,System.IntPtr)
extern void ClipChannels__ctor_mBBA733579FA36D09ACFAF37155075A53152B86A4 (void);
// 0x000001A8 System.Int32 CrazyMinnow.SALSA.Salsa/ClipChannels::Invoke()
extern void ClipChannels_Invoke_m050CF7CE57A9379F76A87D40B8DCEE0659F48AC1 (void);
// 0x000001A9 System.IAsyncResult CrazyMinnow.SALSA.Salsa/ClipChannels::BeginInvoke(System.AsyncCallback,System.Object)
extern void ClipChannels_BeginInvoke_m86A353516E7B573EA1FF642FD369FF965E943246 (void);
// 0x000001AA System.Int32 CrazyMinnow.SALSA.Salsa/ClipChannels::EndInvoke(System.IAsyncResult)
extern void ClipChannels_EndInvoke_m1DFA9F8DBCCA50E6721DAA3E9E617EE853B187F4 (void);
// 0x000001AB System.Void CrazyMinnow.SALSA.Salsa/ClipFrequency::.ctor(System.Object,System.IntPtr)
extern void ClipFrequency__ctor_m88B0F7C910CF753F8719DA759C23784B775900FF (void);
// 0x000001AC System.Int32 CrazyMinnow.SALSA.Salsa/ClipFrequency::Invoke()
extern void ClipFrequency_Invoke_mCCE6328F0965DA5DDFE77AE95D47DB0123CB5B53 (void);
// 0x000001AD System.IAsyncResult CrazyMinnow.SALSA.Salsa/ClipFrequency::BeginInvoke(System.AsyncCallback,System.Object)
extern void ClipFrequency_BeginInvoke_mE935D16C5F50A1B4C62407449237211754A54E49 (void);
// 0x000001AE System.Int32 CrazyMinnow.SALSA.Salsa/ClipFrequency::EndInvoke(System.IAsyncResult)
extern void ClipFrequency_EndInvoke_mFD8EE66038891F92B7B11FB74C9717420C6125E1 (void);
// 0x000001AF System.Void CrazyMinnow.SALSA.Salsa/ClipSampleCount::.ctor(System.Object,System.IntPtr)
extern void ClipSampleCount__ctor_mC181076F208EFE6615AE08399EA3FC9BCA13BC9D (void);
// 0x000001B0 System.Int32 CrazyMinnow.SALSA.Salsa/ClipSampleCount::Invoke()
extern void ClipSampleCount_Invoke_mF0F112CD0A4C2CBA9D2F0E4D3B737CD741DEAF0E (void);
// 0x000001B1 System.IAsyncResult CrazyMinnow.SALSA.Salsa/ClipSampleCount::BeginInvoke(System.AsyncCallback,System.Object)
extern void ClipSampleCount_BeginInvoke_mFE42F71666C193EDC2B32FC47365DB74BB13B627 (void);
// 0x000001B2 System.Int32 CrazyMinnow.SALSA.Salsa/ClipSampleCount::EndInvoke(System.IAsyncResult)
extern void ClipSampleCount_EndInvoke_mBC1F9072A411A3B427C154EE4E75C1AD74EE3CBB (void);
// 0x000001B3 System.Void CrazyMinnow.SALSA.Salsa/GetClipData::.ctor(System.Object,System.IntPtr)
extern void GetClipData__ctor_m75A90A2C5C6F8486437FC10855BCDF618F724FE8 (void);
// 0x000001B4 System.Boolean CrazyMinnow.SALSA.Salsa/GetClipData::Invoke(System.Single[],System.Int32)
extern void GetClipData_Invoke_m967F4557D5DD4481C89F64C468BE81E864D62615 (void);
// 0x000001B5 System.IAsyncResult CrazyMinnow.SALSA.Salsa/GetClipData::BeginInvoke(System.Single[],System.Int32,System.AsyncCallback,System.Object)
extern void GetClipData_BeginInvoke_m1F8FF312AE282A8C79213E250A051FBFECBED033 (void);
// 0x000001B6 System.Boolean CrazyMinnow.SALSA.Salsa/GetClipData::EndInvoke(System.IAsyncResult)
extern void GetClipData_EndInvoke_m42CEFB12C80C6CA35188D2D37EDD69DCEB7023A8 (void);
// 0x000001B7 System.Void CrazyMinnow.SALSA.Salsa/ClipRecordHeadPointer::.ctor(System.Object,System.IntPtr)
extern void ClipRecordHeadPointer__ctor_m5711E8C2203B5A006592468D76EA426D502B9968 (void);
// 0x000001B8 System.Int32 CrazyMinnow.SALSA.Salsa/ClipRecordHeadPointer::Invoke()
extern void ClipRecordHeadPointer_Invoke_mF7799D5E209C42442281C4E5BCDE6241F52ADF71 (void);
// 0x000001B9 System.IAsyncResult CrazyMinnow.SALSA.Salsa/ClipRecordHeadPointer::BeginInvoke(System.AsyncCallback,System.Object)
extern void ClipRecordHeadPointer_BeginInvoke_m625E5A5287BFAEAA4C1CAB37C5B661CF7233DADF (void);
// 0x000001BA System.Int32 CrazyMinnow.SALSA.Salsa/ClipRecordHeadPointer::EndInvoke(System.IAsyncResult)
extern void ClipRecordHeadPointer_EndInvoke_m48D1619BEBF6C7FD049524DA06AD5269392CE928 (void);
// 0x000001BB System.Void CrazyMinnow.SALSA.Salsa/GetTriggerIndex::.ctor(System.Object,System.IntPtr)
extern void GetTriggerIndex__ctor_mBC2BFC1870A2CCE0A265BA6371BCB83E137D0818 (void);
// 0x000001BC System.Int32 CrazyMinnow.SALSA.Salsa/GetTriggerIndex::Invoke()
extern void GetTriggerIndex_Invoke_mD8807144445D0D977BEB9B5261E7BF05A6089EB7 (void);
// 0x000001BD System.IAsyncResult CrazyMinnow.SALSA.Salsa/GetTriggerIndex::BeginInvoke(System.AsyncCallback,System.Object)
extern void GetTriggerIndex_BeginInvoke_mE0D02680315FDCF678C5247D24E7B6C866B8D487 (void);
// 0x000001BE System.Int32 CrazyMinnow.SALSA.Salsa/GetTriggerIndex::EndInvoke(System.IAsyncResult)
extern void GetTriggerIndex_EndInvoke_m73A415F42BCD3120C03A79A0B258D25FE7BBA3C0 (void);
// 0x000001BF System.Void CrazyMinnow.SALSA.Salsa/SalsaNotificationArgs::.ctor()
extern void SalsaNotificationArgs__ctor_m86AA50938CAA960ECA10ADD8CBADD5AAD1033ABC (void);
// 0x000001C0 System.Void CrazyMinnow.SALSA.Salsa/<WaitForAudio>d__137::.ctor(System.Int32)
extern void U3CWaitForAudioU3Ed__137__ctor_m1D71147267D4201048A9C123160435F0D866A105 (void);
// 0x000001C1 System.Void CrazyMinnow.SALSA.Salsa/<WaitForAudio>d__137::System.IDisposable.Dispose()
extern void U3CWaitForAudioU3Ed__137_System_IDisposable_Dispose_m732FE97FB775FFC976EED92E599D9F129ECEA5B8 (void);
// 0x000001C2 System.Boolean CrazyMinnow.SALSA.Salsa/<WaitForAudio>d__137::MoveNext()
extern void U3CWaitForAudioU3Ed__137_MoveNext_m22D7E7B850710F0A2E4964D62FAFAE9375002378 (void);
// 0x000001C3 System.Object CrazyMinnow.SALSA.Salsa/<WaitForAudio>d__137::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CWaitForAudioU3Ed__137_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mDD5B862935F825A39979DD881F5964EF35116076 (void);
// 0x000001C4 System.Void CrazyMinnow.SALSA.Salsa/<WaitForAudio>d__137::System.Collections.IEnumerator.Reset()
extern void U3CWaitForAudioU3Ed__137_System_Collections_IEnumerator_Reset_m4898E98876DCC0D2212C9EDB48BCA09F610BA98E (void);
// 0x000001C5 System.Object CrazyMinnow.SALSA.Salsa/<WaitForAudio>d__137::System.Collections.IEnumerator.get_Current()
extern void U3CWaitForAudioU3Ed__137_System_Collections_IEnumerator_get_Current_m0C034B4D68F886BE2323DE31E0008D5751073216 (void);
// 0x000001C6 System.Void CrazyMinnow.SALSA.Salsa/<>c::.cctor()
extern void U3CU3Ec__cctor_mEAFA0C35857D4492DFFCC529CD4E9F0DCA397E0D (void);
// 0x000001C7 System.Void CrazyMinnow.SALSA.Salsa/<>c::.ctor()
extern void U3CU3Ec__ctor_mB4F3AFFBAF791CD59B6B424DBCA08A58D1AA67C7 (void);
// 0x000001C8 System.Int32 CrazyMinnow.SALSA.Salsa/<>c::<SortLipsyncExpressionsOnTrigger>b__140_0(CrazyMinnow.SALSA.LipsyncExpression,CrazyMinnow.SALSA.LipsyncExpression)
extern void U3CU3Ec_U3CSortLipsyncExpressionsOnTriggerU3Eb__140_0_mA45CA1357816997D7B9D7EDA3156378F99D0957D (void);
// 0x000001C9 System.Void CrazyMinnow.SALSA.Salsa/<WaitForSalsa>d__145::.ctor(System.Int32)
extern void U3CWaitForSalsaU3Ed__145__ctor_m126F324BEDE44B30B2CA4F9BBD042E94FD5A4ED4 (void);
// 0x000001CA System.Void CrazyMinnow.SALSA.Salsa/<WaitForSalsa>d__145::System.IDisposable.Dispose()
extern void U3CWaitForSalsaU3Ed__145_System_IDisposable_Dispose_m825925E43BB47FAC7DF2D600184612C0BD3B4B52 (void);
// 0x000001CB System.Boolean CrazyMinnow.SALSA.Salsa/<WaitForSalsa>d__145::MoveNext()
extern void U3CWaitForSalsaU3Ed__145_MoveNext_m2C238EEC8E15FB3B894A9A823C0D1EAC94E7F271 (void);
// 0x000001CC System.Object CrazyMinnow.SALSA.Salsa/<WaitForSalsa>d__145::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CWaitForSalsaU3Ed__145_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mA9437B3B4753AAEDB3B1B09479EB1CAF33381566 (void);
// 0x000001CD System.Void CrazyMinnow.SALSA.Salsa/<WaitForSalsa>d__145::System.Collections.IEnumerator.Reset()
extern void U3CWaitForSalsaU3Ed__145_System_Collections_IEnumerator_Reset_m41551491DB1233942B8AE989BF00395A93F3BBEE (void);
// 0x000001CE System.Object CrazyMinnow.SALSA.Salsa/<WaitForSalsa>d__145::System.Collections.IEnumerator.get_Current()
extern void U3CWaitForSalsaU3Ed__145_System_Collections_IEnumerator_get_Current_m7F7CA9D1302F69C6174C8DA613ACFD637FA05126 (void);
// 0x000001CF System.Void CrazyMinnow.SALSA.SalsaAdvancedDynamicsSilenceAnalyzer::Start()
extern void SalsaAdvancedDynamicsSilenceAnalyzer_Start_m895BC730482B1F8A3F38B52D9EB6C3178A0C97A6 (void);
// 0x000001D0 System.Void CrazyMinnow.SALSA.SalsaAdvancedDynamicsSilenceAnalyzer::LateUpdate()
extern void SalsaAdvancedDynamicsSilenceAnalyzer_LateUpdate_m82BE925F8C7AF794EC189F5F9588ABB6DD712ACB (void);
// 0x000001D1 System.Void CrazyMinnow.SALSA.SalsaAdvancedDynamicsSilenceAnalyzer::ProcessAudioStream()
extern void SalsaAdvancedDynamicsSilenceAnalyzer_ProcessAudioStream_m430CEF0591446308550122109CD79FBEB1593850 (void);
// 0x000001D2 System.Boolean CrazyMinnow.SALSA.SalsaAdvancedDynamicsSilenceAnalyzer::SalsaAdsaCheckForSilence(System.Single[])
extern void SalsaAdvancedDynamicsSilenceAnalyzer_SalsaAdsaCheckForSilence_m4DCFEF43414EB518FE260DB0837394CFE168128C (void);
// 0x000001D3 System.Void CrazyMinnow.SALSA.SalsaAdvancedDynamicsSilenceAnalyzer::.ctor()
extern void SalsaAdvancedDynamicsSilenceAnalyzer__ctor_mC3E304EEE4DF57BAA51C193C051A37638732E1A0 (void);
// 0x000001D4 System.Void CrazyMinnow.SALSA.SalsaAdvancedDynamicsSilenceAnalyzer/SilenceAnalyzer::.ctor(System.Object,System.IntPtr)
extern void SilenceAnalyzer__ctor_m809747F30F21CBCFBFC6A519F7FDEDBE865F5884 (void);
// 0x000001D5 System.Boolean CrazyMinnow.SALSA.SalsaAdvancedDynamicsSilenceAnalyzer/SilenceAnalyzer::Invoke(System.Single[])
extern void SilenceAnalyzer_Invoke_m08C4B76A403B7F286DD435A00EED1930F9C03C12 (void);
// 0x000001D6 System.IAsyncResult CrazyMinnow.SALSA.SalsaAdvancedDynamicsSilenceAnalyzer/SilenceAnalyzer::BeginInvoke(System.Single[],System.AsyncCallback,System.Object)
extern void SilenceAnalyzer_BeginInvoke_m609D69A846ADA8F58294A33C1653C8DC68EFE306 (void);
// 0x000001D7 System.Boolean CrazyMinnow.SALSA.SalsaAdvancedDynamicsSilenceAnalyzer/SilenceAnalyzer::EndInvoke(System.IAsyncResult)
extern void SilenceAnalyzer_EndInvoke_mB0A99D387B800D5D35BB26DCB9D7918F01F7CE5A (void);
// 0x000001D8 System.Single CrazyMinnow.SALSA.SalsaAudioAnalyzer::SalsaLssCalcSimpleAbs(System.Int32,System.Single[])
extern void SalsaAudioAnalyzer_SalsaLssCalcSimpleAbs_m6ECF700E71D9B9240F1B6E793B0B0C9AD696CE5A (void);
// 0x000001D9 System.Void CrazyMinnow.SALSA.SalsaAudioAnalyzer::.cctor()
extern void SalsaAudioAnalyzer__cctor_m12762790AAA5C6FF190066ADEE7732A53F34A82D (void);
// 0x000001DA System.Void CrazyMinnow.SALSA.SalsaSettings::.cctor()
extern void SalsaSettings__cctor_m84B7CF4CD83AB0492EB966E9755A5414296EF42A (void);
// 0x000001DB System.Void CrazyMinnow.SALSA.SalsaSettingsConfigurator::SetName(System.String)
extern void SalsaSettingsConfigurator_SetName_m63B08619CA283B2DB353BD1880B435FFE5D59D50 (void);
// 0x000001DC System.Void CrazyMinnow.SALSA.SalsaSettingsConfigurator::.ctor()
extern void SalsaSettingsConfigurator__ctor_m5151D118DC2F28A20997A2097FB2269CC4538D2F (void);
// 0x000001DD System.Single CrazyMinnow.SALSA.SalsaUtil::ScaleRange(System.Single,System.Single,System.Single,System.Single,System.Single)
extern void SalsaUtil_ScaleRange_mA5EE93D3BC6FE7D1E208ACB01BF4E06A14465B7A (void);
// 0x000001DE System.Single CrazyMinnow.SALSA.SalsaUtil::GetTime(System.Boolean)
extern void SalsaUtil_GetTime_mC2EF81107CA6C860B5B77F895C49CF57C6F6D718 (void);
// 0x000001DF System.Void CrazyMinnow.SALSA.SalsaUtil::RuntimeUpdateExpressionController(CrazyMinnow.SALSA.Expression)
extern void SalsaUtil_RuntimeUpdateExpressionController_m56D3D36D10A589EFB4AFF6A6D2AE3E9DE51B80B6 (void);
// 0x000001E0 System.Boolean CrazyMinnow.SALSA.SalsaUtil::CheckForInvalidComponent(UnityEngine.GameObject,System.String,CrazyMinnow.SALSA.Expression,System.Int32,System.Boolean)
extern void SalsaUtil_CheckForInvalidComponent_m2DA61F3A4CDFC45A831473F95160AC2885C7571C (void);
// 0x000001E1 System.Void CrazyMinnow.SALSA.SalsaUtil::.cctor()
extern void SalsaUtil__cctor_m2EF0F340254013366A29C4328097A8C7F217FAC9 (void);
// 0x000001E2 System.Void CrazyMinnow.SALSA.UmaUepEditorPreviewTwirler::.ctor()
extern void UmaUepEditorPreviewTwirler__ctor_mFE8A808BBEABF4911ACE50CEA402968CA7BC7F03 (void);
// 0x000001E3 System.Void CrazyMinnow.SALSA.UmaUepProxy::.ctor()
extern void UmaUepProxy__ctor_mF815CA6779BE0C1E39DB40AC7DE74F50550494F9 (void);
// 0x000001E4 CrazyMinnow.SALSA.UmaUepProxy/UepPose[] CrazyMinnow.SALSA.UmaUepProxy::get_Poses()
extern void UmaUepProxy_get_Poses_m8A22DDBEE10A74CD16AAF29574406F34493CFB8F (void);
// 0x000001E5 System.Void CrazyMinnow.SALSA.UmaUepProxy::InitializeUmaUepPoses(CrazyMinnow.SALSA.UmaUepProxy/UepPoseDef[])
extern void UmaUepProxy_InitializeUmaUepPoses_m66A94542595D50C31A6213AFAA6B92A39ED59E35 (void);
// 0x000001E6 System.Boolean CrazyMinnow.SALSA.UmaUepProxy::GetMode(System.Int32)
extern void UmaUepProxy_GetMode_m122E943E9C6D37C9EF68F72EAE5E134F5BB9DA41 (void);
// 0x000001E7 System.String[] CrazyMinnow.SALSA.UmaUepProxy::GetPoseNames()
extern void UmaUepProxy_GetPoseNames_m5CA2324D7C61A4B62972D54410321498531DD4A1 (void);
// 0x000001E8 System.Int32 CrazyMinnow.SALSA.UmaUepProxy::GetPoseIndex(System.String)
extern void UmaUepProxy_GetPoseIndex_mD6906C94C8EC9DCFE16D0BA4825502530028A1CD (void);
// 0x000001E9 System.Void CrazyMinnow.SALSA.UmaUepProxy::ClearDirty(System.Int32)
extern void UmaUepProxy_ClearDirty_m2D10F6A398C90FBC794736A003E4A5182B69BC4B (void);
// 0x000001EA System.Void CrazyMinnow.SALSA.UmaUepProxy::SetPose(System.Int32,System.Single)
extern void UmaUepProxy_SetPose_mD3C74328602C1BF06AD8203218FCF410C209C5C1 (void);
// 0x000001EB System.Void CrazyMinnow.SALSA.UmaUepProxy::LooseSetPose(System.Int32,System.Single)
extern void UmaUepProxy_LooseSetPose_m1991E3422754C45BE2535620D1DBCA5F6D1574BC (void);
// 0x000001EC System.Void CrazyMinnow.SALSA.UmaUepProxy::.cctor()
extern void UmaUepProxy__cctor_mE1CEDB088FD6008E99BA60FEF5B791FC4FE9F18C (void);
// 0x000001ED System.Void CrazyMinnow.SALSA.UmaUepProxy/UepPose::.ctor(System.String,System.Boolean,System.Single,System.Boolean)
extern void UepPose__ctor_m2A7D17A76D26AF2F5F5A262E2B97389770FE976C (void);
// 0x000001EE System.Void CrazyMinnow.SALSA.UmaUepProxy/UepPoseDef::.ctor(System.String,System.Boolean)
extern void UepPoseDef__ctor_m2409E7A2D4DE7FF4B5FE795C30F30A9B1D082617 (void);
static Il2CppMethodPointer s_methodPointers[494] = 
{
	AxisFixed_SetReferences_mA1AF87FE77DA7BAA19F3E84877CE03393A7AFA6D,
	AxisFixed__ctor_m9EDD7E85F785839E94E0635F1D04F10899774DD8,
	AxisOffset_SetReferences_mC0F07F9313DBF74C95303E0B6D9C8E3DC0537DE3,
	AxisOffset__ctor_m7C804A2606B1EE7FCE0AFFF77226FD5B4B858D48,
	AxisOriginal_SetReferences_m2DF4E257F30E2EE1DEB86EC2041627CA13259791,
	AxisOriginal__ctor_m37E43138619FDB7A40176F92125210C37C0699E5,
	EmoteExpression__ctor_mA00EB7A6C51D83ADF2FDD181F79789DB2F91E428,
	EmoteRepeater__ctor_m569328BA0721FEB69E5410B89D2DD7471B280827,
	EmoteRepeater_SetTimeCheck_mA17530000659BB6FB39AF9037DA783EE450C287A,
	Emoter_get_InspectorBasedRandomEmotesCount_mA1B0753F78E64DF548213DE31661EBE488AB92F1,
	Emoter_get_NumRandomEmotesPerCycle_m6CE23A849C265EFC641C673A22FB98E6D01CA479,
	Emoter_set_NumRandomEmotesPerCycle_m23497E301C46D7981312390878455D050ADB33D2,
	Emoter_get_InspectorBasedRandomEmphasizerCount_m4D7B063998A09BC979912E3BA18EB04BF40F2815,
	Emoter_get_NumRandomEmphasizersPerCycle_m2FEBDC5E7E5C73DF31F8F1EF15721032ED4730B5,
	Emoter_set_NumRandomEmphasizersPerCycle_m777300414867EBABDA0F80705295C05C0854627B,
	Emoter_Awake_m3C66F2BD9E6F4C796BDEB5438613378F5204C143,
	Emoter_OnEnable_m58223892E85015B5882DD883E189EF9CE940C150,
	Emoter_OnScaledTimeResumed_mB640AB8E66D34CA7217635119893430386EEC016,
	Emoter_OnDisable_mB32586FF30A5FDF364D01398F1E8AECAA9E2C00C,
	Emoter_LateUpdate_mC8EEA91F196B69AB0809F3E04C8A2F0B1C8F204D,
	Emoter_SetRandomTimerPulse_mD44FB39E15F2517371BB3A0DB6FDFC2439966E35,
	Emoter_TriggerLipSyncEmphasis_m2AF6DC943444A01EE9121338CE0AAFF203183442,
	Emoter_FireRandomEmphasizers_m206906B9C08CE902173A43E642CFCA78E11F49D7,
	Emoter_ResetEmoteHasFiredFlags_m2D35CF4A7FD719472A95DB8356A65B0964758837,
	Emoter_GetRandomEmphasizerCount_m7EBCA58369E184F9C9C1602F3E420A7468A7B877,
	Emoter_FireAlwaysEmphasizers_m207D58DB22E400EE60613F86F2B2E280144A2939,
	Emoter_ManualEmote_m7D3DC72896252CCF559057814C7AA7CE226AFC75,
	Emoter_ManualEmote_m76933BC028208443D1213C91FE322822BEFEFE31,
	Emoter_FindEmote_m727CB8392353E15DBCB836EC7C677F0CD3BC37A9,
	Emoter_RandomEmote_m2C06DDBB897801F76E646C9388A84CD86ACED3CD,
	Emoter_EmoteOneWay_mF370D921EADCA07D9106C9A108870DBA76FD6A92,
	Emoter_EmoteOneWay_m17044882BB43C377DB119EA24476D0F5247BC576,
	Emoter_EmoteRoundTrip_m26BDE2CE60AC54F1B9E8D8759F577AE80B24CA78,
	Emoter_EmoteRoundTrip_mD4D44FB40A4A346A3C4319E180FE311CA18B8A90,
	Emoter_EmoteRoundTrip_m22D154F3C18EAC52C67BDAFFA2F5CD36FF0C8B42,
	Emoter_EmoteComponentRoundTrip_m542883E388B8F22B928571C3BED9333807844C4F,
	Emoter_TurnOffAll_mC9757302CA4AF5906F9F05893971399D7D297B64,
	Emoter_CheckConfigIsReady_mC206501783F08005B0AE9307C36FC5D006033A9A,
	Emoter_UpdateExpressionControllers_mBA11B75B1E1A3EAE1D71410194D65058EB1CAB5E,
	Emoter_UpdateEmoteLists_mB5086ADAC01DC71D876A7C976623276D682962D2,
	Emoter_RemoveFromPool_mF2B279B17DF98823BC0E626421F3F63627FBDCB6,
	Emoter_AddToPool_m95329AEDB9C3DE08D5857D7783F995C4154E07DE,
	Emoter_OnApplicationQuit_mFCB9D437A110E71C8EAD3B12ACA01B727B0D9AF4,
	Emoter__ctor_mC5684D634790CFDEF17158383B91BA37CD7B6E46,
	U3CU3Ec__cctor_mC8E50734A86874F1E6166EBFAD5F76E693F6D481,
	U3CU3Ec__ctor_mC86556408D9FF84FBE11593B94D80025F0A4046F,
	U3CU3Ec_U3Cget_InspectorBasedRandomEmotesCountU3Eb__13_0_m17D7AF97FAF3BEBAA1A0987CA9F10519C6885560,
	U3CU3Ec_U3Cget_InspectorBasedRandomEmphasizerCountU3Eb__18_0_mADC4BAE9AC1E3BC6588D6715FBC80F5E926AE967,
	ExpressionComponent__ctor_m0B32F0AD40E2306FBE0D945B5EF58C139235B2CE,
	ExpressionComponent__ctor_m57770503C90E5E6C8EFBCD9A8FB8D9DF6216DDE1,
	ExpressionComponent__ctor_m7032EAF5ECE9620D5148C63A8D5E760C97EDE866,
	Expression_ConvertToLipsyncControlType_m643BB23E5EC7A3CBFF5AD42489730B5E8E2441B5,
	Expression_ConvertToEmoteControlType_m8E1F3B93DE9E43C56515EF47555379E3F57B1B9A,
	Expression_ConvertToEyesControlType_mDF2C160C31E81D5720F6433331302BF201956844,
	Expression__ctor_m33DDB757C5D5BACCCECE133F48DC7A06FDBCF33C,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	ControllerData__ctor_mB835E971272F264A7CD4B4E780D9CA773D82F561,
	TformBase__ctor_mC920C8F643AF451EE372A801F9B47291653D3584,
	BoneController__ctor_m4802A85329F431BE104174AD7997EC46C9D6B25F,
	BoneController_GenerateOffset_mA1BA79E626AA09C8EDA878AB22FFD1344C7BC50D,
	BoneController_GetID_m21EB8B9BA0CA5F25B64D9C164DE98C5545B30FB3,
	BoneController_SetLerp_m1690CC12477A226AF889D77CEB3BBFA9549EDBE1,
	BoneController_ProcessInfluence_m9C5DF2BB28E15C39439315C7A6AC454D12075B18,
	BoneController_HasDirtyInfluence_m084CB3A7746ED0CFC82206B29E39B067DBCF5A47,
	BoneController_GetNewDelta_m9AFE9FAB8DF8554EB671C9AB3B0707F3CB1CCFEE,
	BoneController_GetPreviousDeltaData_m5B7CE3CE4E700186C3C810B78302BBA964819A33,
	BoneController_SetPreviousDeltaData_m86FC66206BEE58F69646D3D96234EF310E32D7A2,
	BoneController_SetFrac_m972BEA80991BA4EE91AD361B1DEAAA5064337962,
	BoneController_Equals_mCF63666AE3A7009A5AC5F45E140CF99622AE0821,
	BoneController_GetMaxExtent_mC2015175CBD350766142BB31D04728E90D4F74F8,
	BoneController_GetMinExtent_m73A1510D95784286524F2EEA572E8E5DBC3AF579,
	BoneController_HasInvalidController_m644872D9FF94AE740A7902CEC251CD55C1AEFAAD,
	BoneController_SetTarget_m80D3D02CD9B03D5ECCC5E1D6EF5D5243CC1E1153,
	BoneController_SetDelta_m598EDA89BB0578584632616AFC5CEDB67A106B8F,
	ShapeController__ctor_m5C516B1FDA7E963D9809304B01136E81F10AEE91,
	ShapeController_GetID_mDFA8098590D3011E8493A2EBA0A1FDF776FB6303,
	ShapeController_SetLerp_m080693B0DE6743BF8267ED3D90F0FA5DE1C18026,
	ShapeController_ProcessInfluence_m5638176DFC79A0FA382B7347F79BA64909B7192F,
	ShapeController_HasDirtyInfluence_m2FDF7D73EF7953E903D3CDAAB020B5A4996BB626,
	ShapeController_GetNewDelta_m9929CA7D84EC8048B674CDF267448AABD432BD80,
	ShapeController_GetPreviousDeltaData_m6D1EF2A9DD50ECB996CBFED6165480BC0CB82186,
	ShapeController_SetPreviousDeltaData_m4B658901A51432ECBBB22DC59514B50634891562,
	ShapeController_SetFrac_m3D23B4BF79F3E93B5FAF68BD0953DBF1E1756293,
	ShapeController_Equals_mD89DA06AAFD9E4F8E5D45A51CF9A9B1F15569159,
	ShapeController_GetMaxExtent_mD01B9B398E93425A74DDC7F733457B552DF9D01E,
	ShapeController_GetMinExtent_m2A2A05D778DC81DD4B937FC3822A121C08BAAA61,
	ShapeController_HasInvalidController_mBA82397201C0E16BBE994B745731E38FDE9C4FB6,
	ShapeController_SetTarget_m2F924C692C25FE98B76AA9D0E0C3846DA1587E5D,
	ShapeController_SetDelta_mC5A87E41260A9E72BBA4E52DE3B3A8E23AFF8A2E,
	UmaController__ctor_m887575118976652B2AAF7005B8E1E3E2FFAE097F,
	UmaController_GetID_mB29131FB7E3ED0E600E9EAE35BDA94D34AFC2201,
	UmaController_SetLerp_m579D751C9AABB3F650FEBEB5E105C4A59F52535F,
	UmaController_ProcessInfluence_mE932895AB0D126B4C4A9541FE8CC8CD22790389E,
	UmaController_HasDirtyInfluence_m8088D3A308A9E35D0B3FB74F7CC06A7765F31845,
	UmaController_GetNewDelta_m5E6421E7DF087D72A538D3E9191B5432F58E5539,
	UmaController_GetPreviousDeltaData_m3CF8425F2002261DC0DBADA420B2164C5B889F03,
	UmaController_SetPreviousDeltaData_m96D1A0C9A3F0F32BD91411EEFCCF6F81E3BEBE65,
	UmaController_SetFrac_m9AB97E84F54D12731796BD9852D228478E2328AF,
	UmaController_Equals_m5F5305F4E0F68ECE4FAB911BD1B1FB1DF31FCE42,
	UmaController_GetMaxExtent_m6DD957039D7245CF657BFB3E15CD3ECA6EBD264A,
	UmaController_GetMinExtent_m0BC6E812A9A24F1D0ADC33336C86D9544ECA1B0C,
	UmaController_HasInvalidController_m69A41E73CE5B858CFB821BF70F7D1319E35F1DA1,
	UmaController_SetTarget_m1EEB092DBA483681F316B33CF1A1719F93C4CC48,
	UmaController_SetDelta_m9B24A1710E20CF21EEF5427C582A0344ACE6CD25,
	GenericController_HasDirtyInfluence_m079864657E37D96A79322D82E1F36578811B4ED4,
	GenericController_GetNewDelta_mC00C87E55290BF051A671F625EE017FD3AB0195F,
	GenericController_GetPreviousDeltaData_m57CFAF0A23638261DB1F813D41518A6B5A048717,
	GenericController_SetPreviousDeltaData_mCC060E6AFD702D615616682A570E8CCD70C3D0B0,
	NULL,
	GenericController_SetLerp_mDB610D6D0D5ADD38FC65C8EA6AD15376A6E59678,
	GenericController_ON_Start_m8D47876103F95F12EB741CE004F6284EA5E0924C,
	GenericController_ON_Finish_mBCC814CC09387D535389D810874BD4118B4849E1,
	GenericController_OFF_Start_mCA5796D003F0985904CA05ACB05BF4A4E960AB19,
	GenericController_OFF_Finish_m18131A14D0D0437F39D3058DF87C5D0A53F7A3A9,
	GenericController_ProcessInfluence_mC24E007C2F97D78E902DFECAE9369AE4AA74C888,
	GenericController_SetFrac_m7721569EED180BA8676DE169C96607357A1C4BBE,
	GenericController_GetMinExtent_m9ABFF39C4AE09D6AB420DA13CFA3074D56EC5779,
	GenericController_GetMaxExtent_mC163A958FB128305B6AC906D1B43257D5BB964D0,
	NULL,
	GenericController_SetTarget_m2D9CAF89ED6DBB6EEC8F7DA127927F05022B2513,
	GenericController_SetDelta_m3F7B864FB0E10EADDB00FFE8EA3B982423B339D3,
	NULL,
	GenericController__ctor_mA3F94654E71FDC9D589C804CD2E9555D99F03750,
	AnimatorController__ctor_mFF31AFAF4E7D0FBE9489A7F09E9CF2929790AD4F,
	AnimatorController_GetID_m8ABE08F0A02C2C166A84F81302631FAAEADAC855,
	AnimatorController_SetLerp_m1CE2D284F6A4FB3319B52006F4905A27E734EA02,
	AnimatorController_ON_Start_mE985DC17389F6D852130D8A256CD4F3C26860CC6,
	AnimatorController_ON_Finish_mFDBA370FA2C516BB2884A1E101EF0936DDF7F857,
	AnimatorController_OFF_Start_mAC7B69F57573C095CD56BB32BD3C0F798D8FCFAB,
	AnimatorController_OFF_Finish_mA2271E1D84C3DD04EA93AF7CF24F705A67172319,
	AnimatorController_Equals_mD30FF8309D86C9F34BDAA969110C50C800C2543B,
	AnimatorController_HasInvalidController_m04D41F07E970D5111CB97A41AFBD485FA7D84504,
	EventController_add_AnimationStarting_m908C545B38039320ED2221AD756581D403375232,
	EventController_remove_AnimationStarting_m704888C40478456DE937C3C15734769D53145BC7,
	EventController_add_AnimationON_m07523A57BF2CC2946FBFBD068471318F6D3FC3AA,
	EventController_remove_AnimationON_mD1156CA7CF26402A66E2CAF634A79180C2A0D59D,
	EventController_add_AnimationEnding_m9D5D7FC781857025A6E8AF4E3CF426AC55F01A61,
	EventController_remove_AnimationEnding_m8BE943425939845D6A70FB81E3753E1605A49B65,
	EventController_add_AnimationOFF_mB77661AE7AC1D3CB1537781A157FFAF8F3636931,
	EventController_remove_AnimationOFF_mC19633B977DD79655421316685D952ECB6BDDCD9,
	EventController_FireAnimationStartingEvent_mD99B6C8F449C700516082713D58478234F13A953,
	EventController_FireAnimationOnEvent_mB528BBDD5ABFB9D0BEBFD25C1BF6641EDE7076A0,
	EventController_FireAnimationEndingEvent_m92825B78794BDE4697FFA684FA7EDACA568294C4,
	EventController_FireAnimationOffEvent_m4F5785BBA6086C9672D9E3F2A4B9EC2F13EA19CF,
	EventController__ctor_m2CE057B924DCD61C81E9C00769BD366D1C352A7A,
	EventController_GetID_mF4C30ED420B4756B2416A4AFFD1CB1B7AFDD1B99,
	EventController_ON_Start_m4F5DCCD87A6F979DC7F16B4046BC910498B7D569,
	EventController_ON_Finish_mC28BC393071762AA99AF749762818D67F8F7BE43,
	EventController_OFF_Start_m1476D461B058F42873B5D8084A535654BBDAFDC6,
	EventController_OFF_Finish_m45DF12BB34C2CC80A8337F43220849B004649D08,
	EventController_Equals_mCED00DB4266BC38892E610D6BFF60EF328A13CA1,
	EventController_HasInvalidController_mFCFB1E8C1440508C2945EAF6285432C97380E210,
	EventControllerNotificationArgs__ctor_mF3C8C19D12FFA6B7D81CD0A6C444DF076779DEC5,
	Switcher__ctor_m67537B3923AABCB49B621465F0620500B829567F,
	NULL,
	Switcher_SetLerp_m1318FEB344EEBA1FEF2CD825D83212B567F2FF6B,
	Switcher_ProcessInfluence_mFF68898EE2999B7FDCAEFB1E8017E464CEEF9D11,
	Switcher_HasDirtyInfluence_mD288F94E40BE07F2EF80263169701E6664353D7B,
	Switcher_GetPreviousDeltaData_m8E6B55BDD377181D26D9446BBEC4844731CA9D70,
	Switcher_SetPreviousDeltaData_m1E9D90083028820BD6C85EE9DC88BF3425014327,
	Switcher_SetFrac_mB016C8AE914081415ECFC0D1D9AABD1CF9DDD1BB,
	Switcher_GetMinExtent_m34DDBD1AFBDECB4CBD7640C993262FB45289D0E6,
	Switcher_GetMaxExtent_mFF6E8999AFAD58BD7F27A8ED24593BA6C75FA503,
	Switcher_GetNewDelta_mCF11D34091ADF0D0568D051C1F35E1A39853E8EC,
	NULL,
	NULL,
	NULL,
	Switcher_SetTarget_m110B196470A83D4D9B225B6A994C7308A567E4D7,
	Switcher_SetDelta_m804447511A190D9FBC259588FE0959046A677184,
	NULL,
	SpriteController__ctor_mFE2740133E31B78FCAB0B36FFDE3146E636DC4B0,
	SpriteController_GetID_m60261941DE8F388F8DEF3360748BAAD61EA355F0,
	SpriteController_SetOn_m3E9C80B998537C136E4B096E1EAA83D9A222E24E,
	SpriteController_SetOff_m4650D36DAF1090197EF9AFCB94BAA2D935625A7F,
	SpriteController_Equals_m0EBE9A24BEA8D8BDEDF5162FB2A614179F75E673,
	SpriteController_HasInvalidController_m9887722B92879F379FF26962F2C42900D7E7D5DF,
	UguiController__ctor_m16E0D998ACC0F04F200585B2078A74EFE25933F9,
	UguiController_GetID_m0493D4C18EBCF9D9A4FB35C85CAD622016658B03,
	UguiController_SetOn_mA7712BB1EB21451F47471F6242FDA45BCC5C379B,
	UguiController_SetOff_mFC2C292D5DB15F9AD379573657EC52CE93E0001C,
	UguiController_Equals_mB1F42A2B1E20E7989062079167FBBE1A51F04854,
	UguiController_HasInvalidController_mB2B3414B4922CAB36BBE3B7EC7657F8126021618,
	TextureController__ctor_m69FAB6004583C9E38B95CB95105BE7D54F514CFA,
	TextureController_GetID_m42D1846316CCE7C214B633B6E420FDE8F0E5258D,
	TextureController_SetOn_m7CCB962FD67177494184100F47052FF8EE581641,
	TextureController_SetOff_mC97B188462B2BEE5D5F164E9905CEB914E97CC7D,
	TextureController_Equals_mC123628BCB68D18201DBDB30511E9BFF8D53CACD,
	TextureController_HasInvalidController_mFAF6B3A0DF33ADF58707E9A757D1C22C35CA01BF,
	MaterialController__ctor_m047FACC99D8708861861A4381686FF6FA9088A6A,
	MaterialController_GetID_m2BF86662D4EFEADE69F0B912B7F5276A67C3AFB6,
	MaterialController_SetOn_m7902C995602DADC1C5A6C898D745E402BC5ADB63,
	MaterialController_SetOff_mDF9CCDFBAEAE169083F7489CCA949841EE6E5952,
	MaterialController_HasInvalidController_mB9CF24A6279B884DB627C7D9B3E286124F549BB8,
	MaterialController_Equals_m8422B4FFB7ABAE4BF41AAFB7E89DCF63EF369132,
	EyeGizmo_ToggleShowGizmo_m91710B0B866100106F3F842D4C8C29DB8C549E72,
	EyeGizmo_OnDrawGizmos_m641681A69C8F5EAD18083F4EF0E0D6CE799A2D39,
	EyeGizmo__ctor_mE66D9A977B5248E718FF50E8D249F6FD2E4C00E0,
	Eyes_AddExpression_m5B81D49D79CE8AF5ECB99AF396A1705500CF3EAB,
	Eyes_AddComponent_mC4F98F3DE6C289E8F129C86E8F615A8EFAAEDDD8,
	Eyes_BuildHeadTemplate_m114ED7E9D0D1848B3A61CA506FA0C10AEBA842AF,
	Eyes_BuildEyeTemplate_m1B3FC48130BF6CC4CA43796688DDEB34B9A098E5,
	Eyes_BuildEyelidTemplate_m00A5637220944357FD195BEF8B65D2C8064E4679,
	Eyes_BuildEyelidTemplate_m1310C11E6D8CEF449DA6EED2FBD12BE02EB28C3B,
	Eyes_BuildAnyBoneTemplate_m7A585B4A3708083700D23501ADC24DD067F0C9B7,
	Eyes_AddBoneExpression_m4EDFCD57EA3D6FF177355DC566634A507C408F11,
	Eyes_AddBoneComponent_m38F759D1CFFAED5E0468D065DFA6658DE3C8736A,
	Eyes_SetBonePosRot_mDD4B48E563AA361C0BE1EC777E21619753411072,
	Eyes_BuildEyeShapeTemplate_mA69BF018C9ADEA7FBF570F6E78C298A2E52963E1,
	Eyes_AddEyeShapeExpression_mF67E70BDAB79E911D6EE268C834759675B978B9D,
	Eyes_AddShapeComponent_m4A5C2819D770B30F1879C363F8CEBC18CAA5E6F8,
	Eyes_BuildEyeSectorTemplate_m52C2B62172DFE1E0E9BDAC2578BA208695B59B1F,
	Eyes_AddEyeSectorExpression_mB2D87E13382F0BF5F9104D48042A80A9FA58317A,
	Eyes_BuildEyelidShapeTemplate_m3FB5CA846EFBF761037CE0E473F817B40D60696B,
	Eyes_AddEyelidShapeExpression_m0E870A5E8350BCC7ABA1944A1D7DA0489D4DE373,
	Eyes_BuildEyelidSwapTemplate_m52DF87D3731EFDFCB8E709786806F2ACB65CE5B0,
	Eyes_AddEyelidSwapExpression_m727A64715BF763640F3C45C2AFB08730AD17FB18,
	Eyes_AddEyelidSwapComponent_m55F3B26365C378687EA95BB9F4EFBDA74682452C,
	Eyes_BuildEyelidUMATemplate_m111F51DE6B90CBE84DDE8AADB663A3AB764852E2,
	Eyes_AddEyelidUMAExpression_mCFACD6576A8C1B2797351EC4679F999B5B1C8D5E,
	Eyes_SetEyeAnimationTimingDurationOff_m7FD5FF33D07CA13EA68564EA83E5FC59EEC20EFA,
	Eyes_GetExpressionNames_m99A954AA5A726EB9B86AC6421C0D08BDA4F54D71,
	Eyes_CreateBoneAverageGizmo_mF9B9CBF707AD86CD7D242FEC02AC98B30FDF618D,
	Eyes_CreateBoneGizmo_m32B8D8055A35A2D120D9C13A1CD633BD09A90CBF,
	Eyes_CreateEyeGizmo_mFEEB2D58B46E54F33D235ADA6C3543B9AAE2F786,
	Eyes_CreateTrackingGizmo_m3FBBCF397E860A44B4DD9F96B8DC8D0FC73F2F88,
	Eyes_ShowGizmos_m96FDA5D2CE1D7CBD88D3005352FA017BF7632509,
	Eyes_FindTransform_m2275093CBDEB77AE9833C482E267E08BBA88DCD5,
	Eyes_FindTransform_mF266C8B915F64055510E796B10DBD292B035AE5C,
	Eyes_FindBlendIndex_m445FEAE0E6B275B4ED768BD29E0E8CB5B92A1F80,
	Eyes_FindBlendIndex_m87DDBDF74E19BAD7C34DE497B67DA73ABB92CE94,
	Eyes_GetClampedRotation_mC41DA289572EA0E4AC25AB0DF8FCA7EEA55FB932,
	Eyes_ClampAngle_m2CA64670BF3384A92550B501F1658354F45451F3,
	Eyes_GetSignedAngle_m9A187A9BD5F0D775D5450890FD277B6DA8D99B19,
	Eyes_GenerateRandomSaccadePosition_m256817CD6536C32EF229C5DE01336EFC56FB6907,
	Eyes_GenerateRandomTargetPosition_mC02A64DCD992BC19B17F8AE5034DA0CB3ADEC8CC,
	Eyes_GenerateRandomDistanceExtent_mE0B108ABE1B7206312E940393A47BCC40D79B32D,
	Eyes_EyeRotToEyeShapes_mF3183DDBA3990233CC0F108F16DB1C1EDBDA18BE,
	Eyes_EyeRotToEyelidPos_m4C7434E92BC2D9234601B29365C69FD473F72E02,
	Eyes_EyeRotToEyelidScale_m99F4C5D7BCDEF0610EF5BC1499E65DF089F32E19,
	Eyes_EyePosToEyelidRot_m23A1384B086294769DCC3121151AE9BAD1B813B9,
	Eyes_EyeRotToEyelidShapes_m1DADC3C5CD6E73C5A86CF8A2DBB2282EB39D2A11,
	Eyes_EyeRotToEyelidSwap_m8B615D210B2501DF1D97B6CAF8497E91C6F81E90,
	Eyes_AnglesToDirVector_mBA93C1759153EF2F378CB0834BCBD0BE9955AB86,
	Eyes_RemoveExpression_mEB8E35A22571933E6F6C8F87B2EA046FEE9FCE71,
	Eyes_RemoveComponent_mD7CCE3CB06373721AE527952D0EAE53C55B8829A,
	Eyes_CopyBlinkToTrack_m7841A2ACFCE1D883289607FB04EF02D76497EEC7,
	Eyes_AllBonesLinked_m12B2B8A952C1E660C3CDC3C2A7EF0CD59E575300,
	Eyes_AddParent_m7C4053602DCAECEDFFA8F6B01F5FB86CDEFDA7A8,
	Eyes_FixTransformAxis_mA1CA2EBB6112ED057834CE35C3E0162C98C31BE0,
	Eyes_FixAllTransformAxes_m063E1DC1B66B3131807D53760BA28AF516D8AFE3,
	Eyes_ParentRandomTargets_m5C7EFE50D8BE146E76CA73F395902FDA69776718,
	Eyes_EnableAll_m2E413D0C12BC92FC381B58EF2D847169E2A81FBE,
	Eyes_EnableHead_m70E8046C65B449E9423DF93A9B44273624D9838C,
	Eyes_EnableEye_m0DD53663FCAF2A8C318AB81AF6E679AFB7222240,
	Eyes_EnableEyelidBlink_m78FA625F864C44623C17154BCB16AE3B5F3C67E4,
	Eyes_EnableEyelidTrack_mA1233839C10252ADD43F25EA2F010D08CB132486,
	Eyes_SetForwardPosition_m80FBB8B818EEB4620313EE3446C2EB2996CA909D,
	Eyes_LookForward_mF58378D8ADDCB202AC3347483FE2CB73CF51C30C,
	Eyes_NewRandomHeadTarget_m8E96F9524D4BB9273E8D5D4B889149C2643B606A,
	Eyes_NewRandomEyeTarget_mD736CDBE76499D2EF7F96C7E138639AD157A27DB,
	Eyes_NewBlink_m52C53392F03C43D436708E43D561BD39ADAD83F8,
	Eyes_UpdateRuntimeExpressionControllers_m9349227C9D6FE00FCFB47AABC3B32212615A3DC8,
	Eyes_GetFlipState_m988B82D7BF48E5525DD3E63D0A287B50713772B6,
	Eyes_GetDegree_m6C954CC64F0D5211FAE87354F20BE0017CD54569,
	Eyes_GetSectorFromDegree_m8C0C107A445A06FA7FDDBB7A5A501777F9768E60,
	Eyes_GetProportionalMovementAmount_m293C3E9FC73BD06066DEDCE1B4EF02D58813EDBD,
	Eyes_GetSectorIndex_m428CCCDEA23CCB1673712A436F23F172EBED9AB1,
	Eyes_ActivateSector_m9FACFD7372B0659F777041AEC648DCA3847AF5C2,
	Eyes_CaptureMin_m43CED4D350DBB1498DD4EFDB510B2872BE814046,
	Eyes_CaptureMin_mAFFCF2A242674A14E36B4CAD435E84AC530A3528,
	Eyes_CaptureMax_mC8D1A55023F5C2D0BF01679B5F59F5469D8A28A7,
	Eyes_CaptureMax_mC902522C1DCF6A51C67F4F259988440E604DBA5A,
	Eyes_ReturnMin_m805908A38546B8CBA8555371983EF61304C9F6A6,
	Eyes_ReturnMin_mB96B8DA46FAAF908044977138E097C994BB27467,
	Eyes_GetEyeAxisOriginal_m238E272AEAC7CAF2F12B2F3299594F36C370C9D9,
	Eyes_OverwriteAnimation_m661AC9C67A7EBC618661AB0BE0C8548D7228F50A,
	Eyes_DestroyProcTransOffsetGizmos_mA82C2E84C9D1AEBC7C0E1E143A63212A7380172E,
	Eyes_DestroyProcTransGizmos_mC5B7DC181114697A257949EC68183B5881065985,
	Eyes_CheckConfigIsReady_m7A68134DC67EFADE3240B567202ED3DAE0D74FAE,
	Eyes_Initialize_m779AB2DE24FA085098150D0CBA44D44A721213A9,
	Eyes_Awake_m64A934856A9AC256248825C104439C23CB38AEDD,
	Eyes_Start_m41DABC5B8EB61C4A5D623721A5018F66514315F2,
	Eyes_OnEnable_m14CCE3CB6B4601AE04D540E19779787FF1D1AE8D,
	Eyes_OnScaledTimeResumed_mEAD4AA42329363C882FE40B504E3FB4E5F161495,
	Eyes_OnDisable_m163B12181F0145A3B94E6B6000FE03F40BE1D4A2,
	Eyes_LateUpdate_m245547CCA575A7EBE3B253C9B3D791BE7371A3C3,
	Eyes__ctor_m796C24E7A7CC6E6C49C0F762297894BC0087D049,
	EyesExpression__ctor_mB429B2E13FC29DAD70CCF61E40582BB9BA576FEB,
	AnimCancel__ctor_m0F35FCAE32EB7A7698A00B85C708551A94115FA9,
	AnimCancel_Capture_m04038D13868722E82AFF5C7AC8AC793B516A97BF,
	AnimCancel_Overwrite_m6C505E532107F4B5D51EEAED931F2FF457874C6A,
	ProcTransOffset__ctor_mAB847A2EA52BB6816A2FC08AD4D1876EB91C4F86,
	ProcTrans__ctor_mA98D2DBB2D72FA6102BEEFF05FE62FE8B5C1CE1E,
	ProcTForm__ctor_mC314D77BDD5990220D420024C4451C058EC440D6,
	ProcAxisOrig__ctor_m1359EF04A2B22D78569EDE5D12A9F67D17775BDA,
	ProcVecs__ctor_m262FA8C804E35E228FA8B6C64474080F3A7F40B8,
	ProcLLPair__ctor_m150C12200B6ED2F62F542B40E9A0E42228AD0816,
	ProcLLPairs__ctor_m16963B5573A529317AFA4534427F8330D0AD8AAD,
	ProcDirection__ctor_mAE04E1A07192555A8DFB719AE95CDA5D96BA48C6,
	ProcDirections__ctor_m52C91C467E9B99E8E51E5386AE41BC5BDB351401,
	InspectorControllerHelperData__ctor_m4FDF274348A7A6722D23862A80B19EB5C54E3099,
	InspectorControllerHelperData__ctor_m2BF06F1E42FAED1218484B07F283D64537D92915,
	InspectorControllerHelperData_StoreBoneBase_mFB840295B298519E279DE9AAA32863B297EAC7CB,
	InspectorControllerHelperData_StoreStartTform_m120D086FE77023DC57B52460C305ECB3512150E3,
	InspectorControllerHelperData_StoreEndTform_m4176D62949C93C9E831F37CF8EB9DF1BCB7B4ACB,
	InspectorControllerHelperData_ResetBone_mF2258FDB275E314C8DEA5004D997AA16B255125A,
	InspectorControllerHelperData_ClearStoredTforms_m16DC9C339386557C4A32015BA55A6B44319C6126,
	InspectorControllerHelperData_GetSprites_mE5940092676969BC9F63D28C309882F3DB616F02,
	InspectorControllerHelperData_GetTextures_m19270F028D0A040262FA75BF0A59F94BF6910664,
	InspectorControllerHelperData_GetMaterials_m3F0B3A72C75F832BA0C57A2C9CF919BB3F3F146F,
	LerpEasings_Ease_m84D5C1532DE05B873451DBCA2ACC4EE0A62F179B,
	LerpEasings_CubicOut_m73A7B7334B6E7945D5E7FE0DC668D9A4A06E3793,
	LerpEasings_CubicIn_m09EB008C3C5EEF5C7FC9642A64A4B753EF9B497B,
	LerpEasings_SquaredIn_mD67BBAE3D01FE9A46CE29E5A1468312BDA40119E,
	LerpEasings_CircleIn_m99711A2649BD04138260D14361E28617E87B5DA4,
	LerpEasings_CircleOut_m6B40E1FCF7E6F0E4924BDAF6235E4FB374F4FACF,
	LerpEasings_CubicInOut_m2E178C953FC92AF08DB2D5DF81706AD141AC7B64,
	LerpEasings_QuintOut_m3C49B643C7C59626EF34C870B0C4EE6FB8C245CB,
	LerpEasings_QuarticOut_mA3FDA2211D28B42EDAC6762C17CE97DA2BE1B996,
	LerpEasings_SinIn_m7750F13627CBFDE514E61DA447E49E8F89F82B25,
	LerpEasings_SinOut_m7BACAFF122B6494BB6BEEA267402698CA8C7D74B,
	LipsyncExpression__ctor_m150DF066C6BC08A4567BA6040981F5CFAFAF9CF5,
	QueueExpressionComponent__ctor_m2070728740E1660D6F0AE41D0BA7A08D86E005C7,
	QueueExpressionComponent__ctor_m36912621696266F4638E6F38E2CFC386DDC0A76B,
	QueueExpressionComponent__ctor_m2AE432C1E57A79D5F465EE30CFFDE1DEFDC6F976,
	QueueData__ctor_m869788F7850F5E9FA956C53E4F21FFB656011ACE,
	QueueData__ctor_mE61A60FB8F9B593A1DC705557CE0616F3E1FD1F6,
	QueueData__ctor_mBB7166CCFC77FD77222B1C14605C3F0B42311589,
	QueueProcessor_add_ScaledTimeResumed_mA8B068661DFB759716F4E7C4CA9ED503D0EAFB36,
	QueueProcessor_remove_ScaledTimeResumed_m9A56C22AF3ED793523E0ED06F343C5931119F26D,
	QueueProcessor_RespectScaledTime_mCBEEA048DD821C227951223448E7679C609F2F81,
	QueueProcessor_Awake_mC3719F6B9359848699CD59AB82BBD763238DB356,
	QueueProcessor_InitializeQueueOrderOfOperation_m0B54061E1D2D33F2DF481BADA4A18C15EE6EB7EF,
	QueueProcessor_Flush_m4579CB224E1439E95CF52F41C19B7E8B3C44CD88,
	QueueProcessor_ShutdownActiveQueue_m240528DFBDEABAF54E36697F89D6FA06321E12DE,
	QueueProcessor_Register_m081FE8ACE20CE9648A1BB1D8E8901FDD592E9761,
	QueueProcessor_Register_mC982EAFA114261C912F7D4E972BA007FCB7F0E8D,
	QueueProcessor_Register_m5F2F5571F5A7F76DAAA220324254F13FB4C56D9A,
	QueueProcessor_ConfirmRegistration_mE5DC64971DA64E094A929224F14F6859050D9A08,
	QueueProcessor_GetRegistrationQueue_m1935CA0004E0CB38DFEC2E86CD53C730A11E9845,
	QueueProcessor_LateUpdate_mA0A29E1E453C9BC2B142ED237211910590275BAC,
	QueueProcessor_ProcessQueue_mC6258813D82C24379B012F75321B620F570982BA,
	QueueProcessor__ctor_m98D9DEFA87E636FE6166A7632F0D9E4FB4A9204A,
	QueueOoO__ctor_m97FB9C803279C26A8CCDA89F7C63BE985F8776EE,
	QueueProcessorNotificationArgs__ctor_m76BD181B7709738AB2C7EB20C365912E60DD02F2,
	Salsa_get_CachedAnalysisValue_mCFA56E72A407395A6ABD7F8DBA3B7A78D46200F7,
	Salsa_get_IsSALSAing_m08BB419CAD1A1852094610902B2A112DF2647F29,
	Salsa_get_TriggeredIndex_m5F6A4E11FA84288C6833C7B478E3C665EE6D1609,
	Salsa_add_StartedSalsaing_mDFA1FF3D0181546C7DD01FAF22A3F7207C089C46,
	Salsa_remove_StartedSalsaing_mD8CB3F704734628D926C9687057000079716A55B,
	Salsa_add_StoppedSalsaing_m41629779A7EEE50D9E884F0D5FE0D78E2216B708,
	Salsa_remove_StoppedSalsaing_m2C6E8AB8478DFEDFF2F43068690708DB82F747E5,
	Salsa_add_VisemeTriggered_mE59483C9050A7539895DEA66032BF24F647D6F76,
	Salsa_remove_VisemeTriggered_m041889BB2A473C46AFE1B8656B096F07D5FDE9EE,
	Salsa_FireStartedSalsaingEvent_m82BFC9C22FDC79087BD474AF0502FEEB21C8BED2,
	Salsa_FireStoppedSalsaingEvent_m59B795C0C737BE732FB36C731E8636EDFD7B4A3B,
	Salsa_FireVisemeTriggeredEvent_mD904DE7DC81C552309FFF57EE402F478BC64228A,
	Salsa_Awake_m8750E72A3B002D8AF2FD18F1F1CF49952160E9EF,
	Salsa_OnEnable_m45467B631D3844264A2AB3DB1A0AD07C6A4BB045,
	Salsa_OnScaledTimeResumed_m823E405261663E2A5BF8AB0D3D5BCF541FDDEF9E,
	Salsa_OnDisable_m7BD6142842F093F139832B87E65F923C80D2B76B,
	Salsa_InitializeDelegates_mD76D22EB8C34D9FFE5ECA49E8461157BE73A4A83,
	Salsa_StartWaitForAudio_m47C0E613C6AF55FD13E4BB95F6FE87F3D856E907,
	Salsa_IsWithinPulseCheckInterruptVariance_m6AE690752C299A71A552B0E5BB04616F664310B4,
	Salsa_LateUpdate_mDC7B07325DE883B78B67F3AA601324CCB8C820F2,
	Salsa_ProcessSilenceTrigger_m44C31D3AAAE6AD63F4AC3B283368F1B3BB92939B,
	Salsa_ProcessAudioAnalysis_m437098DAA809BC6F1CDF04F8779480FBA6C7B1FF,
	Salsa_SalsaLssGetTriggeredIndex_m4097023016DF1E96D9CCC1A54A52FE88F8AAC78C,
	Salsa_ProcessEmphasizerEmote_m1D5E011C67233585B4AFB4C08882E50489EF2095,
	Salsa_TurnPreviouslyTriggeredVisemeOff_mB6F0A8420E9FA89BFAC04AA5AA59C142637B7348,
	Salsa_TurnVisemComponentsOff_mB2F87AC392C36E5B62E0361E93D7BC36FB53EAA6,
	Salsa_GetSecondaryMixIndex_mC20D3171A9D5BD663B58209EF83B7457B37CB32F,
	Salsa_RegisterComponentOn_mA7AA78F8098FD37C71CA43CAB15A701DB1821481,
	Salsa_RegisterComponentOff_m2BA3ADEE9DBDE488FD8CB28B1E1685CB48C9584D,
	Salsa_CalcAdvancedDynamics_m3522A677DE2438E5463248944EE10F38AB96A26D,
	Salsa_TurnVisemeOn_m7072EBD688E4CCA3E51CEF358349E89A004D90DE,
	Salsa_TurnVisemeComponentsOn_m6270BDD6BFE8831D7800978F26AE72C16CA36A75,
	Salsa_AnalyzeAudio_m1F47BA25642DDF4EF6F55A01CC42B1DEA4B47ACA,
	Salsa_IsOffsetPointerOutOfBounds_m0D07F31F51E1288168FAE5B5175E95E283FF118C,
	Salsa_CalcClipOffsetPointer_m3E8BAD6CDE9870C60BDE881ABA42D665894FCEEE,
	Salsa_get_GetSalsaAudioPointer_mA37D6358FBE167D8B2AAFAC1DB41F1892B3C1298,
	Salsa_ScaleAudio_mFC2A90BECE72AED403F54B7E7862E7FA8F95616E,
	Salsa_AdjustAnalysisSettings_m3093BFD52C850590F5D4E3680248AB93315D877D,
	Salsa_SetPlayheadBias_mA34239734DE7551D7C11713D5E1D704CEFA394BC,
	Salsa_SetSampleSize_m8CC64F2CC12C0C009EFE53B4CFF8342E52FE4CC2,
	Salsa_WaitForAudio_m660A0F02F3A6233A91CC2D29F5FFAAD0A235E197,
	Salsa_UpdateExpressionControllers_m0E1F6DD71EBF047651EA41A9FF52B244B1B8F844,
	Salsa_SetPersistence_mB4B5713E91C1A4FCF8AD04320555588FE58C0125,
	Salsa_SortLipsyncExpressionsOnTrigger_m8BFD2E12817ED8E151761135A2B13B3C535A8293,
	Salsa_TurnOffAll_m6FBDEED841BD8DE16D4372C41C766417513398BC,
	Salsa_CheckConfigIsReady_m1F6631A85B7E08D672F38753987809DEA52AEB40,
	Salsa_DistributeTriggers_m6B4C3933FF8ED0FCE4C4039C614CB2D7E2EA506F,
	Salsa_SalsaUndoRedoCallback_m9E35205369478702613A00D62AF7D4D19CFA5F01,
	Salsa_WaitForSalsa_m2815A25D22FBA78003ACB8A97A718EBD803853EC,
	Salsa_SalsaLssGetExternalAnalysisValue_m78B4642FEBEB85125DE4442A62319EE38F61BD65,
	Salsa_SalsaLssGetAudioClipData_m245A41E162AED46736AE145FB8987D9D1F18B465,
	Salsa_SalsaLssGetBufferPointer_m558B4CA2A493CEFEBE66458AA4F2AF3EB38E5979,
	Salsa_SalsaLssGetAudioClipSampleCount_m7E15A76B68C9161F693ACF81336222B7F3CEF485,
	Salsa_SalsaLssGetAudioClipFrequency_mDE74E4402B69164D77E27E5B4313BE2B4C14368F,
	Salsa_SalsaLssGetAudioClipChannels_mDF1EE8692AFD84D77FC340DC77E5B1BE6E22C691,
	Salsa_OnApplicationQuit_m3C9F202B5B1184117E10739DE47BC397005AAD58,
	Salsa__ctor_m009A32DD10B776DB9C12DE3FA0BCD6E9472FADD6,
	GetExternalAnalysis__ctor_m77E5A87B31F1AFD205B33CAA8C313E9D00B76629,
	GetExternalAnalysis_Invoke_mA5D89EF5A44B81239F02BDCC0E0ACCBFCCCFF9EB,
	GetExternalAnalysis_BeginInvoke_mC80225B614EDF48C4AFCADF5B88A9FA0A1DE1D91,
	GetExternalAnalysis_EndInvoke_m625515D1CCD735D689A086A1FD624AA4A3D74EB1,
	AudioAnalyzer__ctor_mA07C80DF68DE3E1E836706824305E71EA5208E1B,
	AudioAnalyzer_Invoke_m38D61A2805EB6CD043D3D21F9FD50C12B5AC5A82,
	AudioAnalyzer_BeginInvoke_mFBCCDB67962BAC22DE886CA320A0A0493AD23DA7,
	AudioAnalyzer_EndInvoke_m2877DBC8AC378C19AEB783E96D12FE83C8714B07,
	ClipChannels__ctor_mBBA733579FA36D09ACFAF37155075A53152B86A4,
	ClipChannels_Invoke_m050CF7CE57A9379F76A87D40B8DCEE0659F48AC1,
	ClipChannels_BeginInvoke_m86A353516E7B573EA1FF642FD369FF965E943246,
	ClipChannels_EndInvoke_m1DFA9F8DBCCA50E6721DAA3E9E617EE853B187F4,
	ClipFrequency__ctor_m88B0F7C910CF753F8719DA759C23784B775900FF,
	ClipFrequency_Invoke_mCCE6328F0965DA5DDFE77AE95D47DB0123CB5B53,
	ClipFrequency_BeginInvoke_mE935D16C5F50A1B4C62407449237211754A54E49,
	ClipFrequency_EndInvoke_mFD8EE66038891F92B7B11FB74C9717420C6125E1,
	ClipSampleCount__ctor_mC181076F208EFE6615AE08399EA3FC9BCA13BC9D,
	ClipSampleCount_Invoke_mF0F112CD0A4C2CBA9D2F0E4D3B737CD741DEAF0E,
	ClipSampleCount_BeginInvoke_mFE42F71666C193EDC2B32FC47365DB74BB13B627,
	ClipSampleCount_EndInvoke_mBC1F9072A411A3B427C154EE4E75C1AD74EE3CBB,
	GetClipData__ctor_m75A90A2C5C6F8486437FC10855BCDF618F724FE8,
	GetClipData_Invoke_m967F4557D5DD4481C89F64C468BE81E864D62615,
	GetClipData_BeginInvoke_m1F8FF312AE282A8C79213E250A051FBFECBED033,
	GetClipData_EndInvoke_m42CEFB12C80C6CA35188D2D37EDD69DCEB7023A8,
	ClipRecordHeadPointer__ctor_m5711E8C2203B5A006592468D76EA426D502B9968,
	ClipRecordHeadPointer_Invoke_mF7799D5E209C42442281C4E5BCDE6241F52ADF71,
	ClipRecordHeadPointer_BeginInvoke_m625E5A5287BFAEAA4C1CAB37C5B661CF7233DADF,
	ClipRecordHeadPointer_EndInvoke_m48D1619BEBF6C7FD049524DA06AD5269392CE928,
	GetTriggerIndex__ctor_mBC2BFC1870A2CCE0A265BA6371BCB83E137D0818,
	GetTriggerIndex_Invoke_mD8807144445D0D977BEB9B5261E7BF05A6089EB7,
	GetTriggerIndex_BeginInvoke_mE0D02680315FDCF678C5247D24E7B6C866B8D487,
	GetTriggerIndex_EndInvoke_m73A415F42BCD3120C03A79A0B258D25FE7BBA3C0,
	SalsaNotificationArgs__ctor_m86AA50938CAA960ECA10ADD8CBADD5AAD1033ABC,
	U3CWaitForAudioU3Ed__137__ctor_m1D71147267D4201048A9C123160435F0D866A105,
	U3CWaitForAudioU3Ed__137_System_IDisposable_Dispose_m732FE97FB775FFC976EED92E599D9F129ECEA5B8,
	U3CWaitForAudioU3Ed__137_MoveNext_m22D7E7B850710F0A2E4964D62FAFAE9375002378,
	U3CWaitForAudioU3Ed__137_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mDD5B862935F825A39979DD881F5964EF35116076,
	U3CWaitForAudioU3Ed__137_System_Collections_IEnumerator_Reset_m4898E98876DCC0D2212C9EDB48BCA09F610BA98E,
	U3CWaitForAudioU3Ed__137_System_Collections_IEnumerator_get_Current_m0C034B4D68F886BE2323DE31E0008D5751073216,
	U3CU3Ec__cctor_mEAFA0C35857D4492DFFCC529CD4E9F0DCA397E0D,
	U3CU3Ec__ctor_mB4F3AFFBAF791CD59B6B424DBCA08A58D1AA67C7,
	U3CU3Ec_U3CSortLipsyncExpressionsOnTriggerU3Eb__140_0_mA45CA1357816997D7B9D7EDA3156378F99D0957D,
	U3CWaitForSalsaU3Ed__145__ctor_m126F324BEDE44B30B2CA4F9BBD042E94FD5A4ED4,
	U3CWaitForSalsaU3Ed__145_System_IDisposable_Dispose_m825925E43BB47FAC7DF2D600184612C0BD3B4B52,
	U3CWaitForSalsaU3Ed__145_MoveNext_m2C238EEC8E15FB3B894A9A823C0D1EAC94E7F271,
	U3CWaitForSalsaU3Ed__145_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mA9437B3B4753AAEDB3B1B09479EB1CAF33381566,
	U3CWaitForSalsaU3Ed__145_System_Collections_IEnumerator_Reset_m41551491DB1233942B8AE989BF00395A93F3BBEE,
	U3CWaitForSalsaU3Ed__145_System_Collections_IEnumerator_get_Current_m7F7CA9D1302F69C6174C8DA613ACFD637FA05126,
	SalsaAdvancedDynamicsSilenceAnalyzer_Start_m895BC730482B1F8A3F38B52D9EB6C3178A0C97A6,
	SalsaAdvancedDynamicsSilenceAnalyzer_LateUpdate_m82BE925F8C7AF794EC189F5F9588ABB6DD712ACB,
	SalsaAdvancedDynamicsSilenceAnalyzer_ProcessAudioStream_m430CEF0591446308550122109CD79FBEB1593850,
	SalsaAdvancedDynamicsSilenceAnalyzer_SalsaAdsaCheckForSilence_m4DCFEF43414EB518FE260DB0837394CFE168128C,
	SalsaAdvancedDynamicsSilenceAnalyzer__ctor_mC3E304EEE4DF57BAA51C193C051A37638732E1A0,
	SilenceAnalyzer__ctor_m809747F30F21CBCFBFC6A519F7FDEDBE865F5884,
	SilenceAnalyzer_Invoke_m08C4B76A403B7F286DD435A00EED1930F9C03C12,
	SilenceAnalyzer_BeginInvoke_m609D69A846ADA8F58294A33C1653C8DC68EFE306,
	SilenceAnalyzer_EndInvoke_mB0A99D387B800D5D35BB26DCB9D7918F01F7CE5A,
	SalsaAudioAnalyzer_SalsaLssCalcSimpleAbs_m6ECF700E71D9B9240F1B6E793B0B0C9AD696CE5A,
	SalsaAudioAnalyzer__cctor_m12762790AAA5C6FF190066ADEE7732A53F34A82D,
	SalsaSettings__cctor_m84B7CF4CD83AB0492EB966E9755A5414296EF42A,
	SalsaSettingsConfigurator_SetName_m63B08619CA283B2DB353BD1880B435FFE5D59D50,
	SalsaSettingsConfigurator__ctor_m5151D118DC2F28A20997A2097FB2269CC4538D2F,
	SalsaUtil_ScaleRange_mA5EE93D3BC6FE7D1E208ACB01BF4E06A14465B7A,
	SalsaUtil_GetTime_mC2EF81107CA6C860B5B77F895C49CF57C6F6D718,
	SalsaUtil_RuntimeUpdateExpressionController_m56D3D36D10A589EFB4AFF6A6D2AE3E9DE51B80B6,
	SalsaUtil_CheckForInvalidComponent_m2DA61F3A4CDFC45A831473F95160AC2885C7571C,
	SalsaUtil__cctor_m2EF0F340254013366A29C4328097A8C7F217FAC9,
	UmaUepEditorPreviewTwirler__ctor_mFE8A808BBEABF4911ACE50CEA402968CA7BC7F03,
	UmaUepProxy__ctor_mF815CA6779BE0C1E39DB40AC7DE74F50550494F9,
	UmaUepProxy_get_Poses_m8A22DDBEE10A74CD16AAF29574406F34493CFB8F,
	UmaUepProxy_InitializeUmaUepPoses_m66A94542595D50C31A6213AFAA6B92A39ED59E35,
	UmaUepProxy_GetMode_m122E943E9C6D37C9EF68F72EAE5E134F5BB9DA41,
	UmaUepProxy_GetPoseNames_m5CA2324D7C61A4B62972D54410321498531DD4A1,
	UmaUepProxy_GetPoseIndex_mD6906C94C8EC9DCFE16D0BA4825502530028A1CD,
	UmaUepProxy_ClearDirty_m2D10F6A398C90FBC794736A003E4A5182B69BC4B,
	UmaUepProxy_SetPose_mD3C74328602C1BF06AD8203218FCF410C209C5C1,
	UmaUepProxy_LooseSetPose_m1991E3422754C45BE2535620D1DBCA5F6D1574BC,
	UmaUepProxy__cctor_mE1CEDB088FD6008E99BA60FEF5B791FC4FE9F18C,
	UepPose__ctor_m2A7D17A76D26AF2F5F5A262E2B97389770FE976C,
	UepPoseDef__ctor_m2409E7A2D4DE7FF4B5FE795C30F30A9B1D082617,
};
extern void ControllerData__ctor_mB835E971272F264A7CD4B4E780D9CA773D82F561_AdjustorThunk (void);
extern void TformBase__ctor_mC920C8F643AF451EE372A801F9B47291653D3584_AdjustorThunk (void);
extern void QueueExpressionComponent__ctor_m2070728740E1660D6F0AE41D0BA7A08D86E005C7_AdjustorThunk (void);
extern void QueueExpressionComponent__ctor_m36912621696266F4638E6F38E2CFC386DDC0A76B_AdjustorThunk (void);
extern void QueueExpressionComponent__ctor_m2AE432C1E57A79D5F465EE30CFFDE1DEFDC6F976_AdjustorThunk (void);
extern void QueueData__ctor_m869788F7850F5E9FA956C53E4F21FFB656011ACE_AdjustorThunk (void);
extern void QueueData__ctor_mE61A60FB8F9B593A1DC705557CE0616F3E1FD1F6_AdjustorThunk (void);
extern void QueueData__ctor_mBB7166CCFC77FD77222B1C14605C3F0B42311589_AdjustorThunk (void);
static Il2CppTokenAdjustorThunkPair s_adjustorThunks[8] = 
{
	{ 0x06000045, ControllerData__ctor_mB835E971272F264A7CD4B4E780D9CA773D82F561_AdjustorThunk },
	{ 0x06000046, TformBase__ctor_mC920C8F643AF451EE372A801F9B47291653D3584_AdjustorThunk },
	{ 0x0600014F, QueueExpressionComponent__ctor_m2070728740E1660D6F0AE41D0BA7A08D86E005C7_AdjustorThunk },
	{ 0x06000150, QueueExpressionComponent__ctor_m36912621696266F4638E6F38E2CFC386DDC0A76B_AdjustorThunk },
	{ 0x06000151, QueueExpressionComponent__ctor_m2AE432C1E57A79D5F465EE30CFFDE1DEFDC6F976_AdjustorThunk },
	{ 0x06000152, QueueData__ctor_m869788F7850F5E9FA956C53E4F21FFB656011ACE_AdjustorThunk },
	{ 0x06000153, QueueData__ctor_mE61A60FB8F9B593A1DC705557CE0616F3E1FD1F6_AdjustorThunk },
	{ 0x06000154, QueueData__ctor_mBB7166CCFC77FD77222B1C14605C3F0B42311589_AdjustorThunk },
};
static const int32_t s_InvokerIndices[494] = 
{
	1142,
	3479,
	1142,
	3479,
	1142,
	3479,
	26,
	1157,
	2920,
	3397,
	3397,
	2870,
	3397,
	3397,
	2870,
	3479,
	3479,
	1747,
	3479,
	3479,
	3479,
	2922,
	1774,
	3479,
	3397,
	2922,
	393,
	371,
	2300,
	1774,
	1753,
	1157,
	2887,
	1754,
	1158,
	767,
	3479,
	3451,
	3479,
	3479,
	1388,
	1388,
	3479,
	3479,
	5226,
	3479,
	2545,
	2545,
	3479,
	2887,
	167,
	4998,
	4998,
	4998,
	3479,
	3397,
	1776,
	1770,
	3451,
	1452,
	3364,
	2836,
	2922,
	3453,
	3453,
	3451,
	1780,
	2922,
	1780,
	1183,
	50,
	1781,
	3397,
	1776,
	1770,
	3451,
	1452,
	3364,
	2836,
	2922,
	2545,
	3453,
	3453,
	3451,
	1780,
	2922,
	394,
	3397,
	1776,
	1770,
	3451,
	1452,
	3364,
	2836,
	2922,
	2545,
	3453,
	3453,
	3451,
	1780,
	2922,
	767,
	3397,
	1776,
	1770,
	3451,
	1452,
	3364,
	2836,
	2922,
	2545,
	3453,
	3453,
	3451,
	1780,
	2922,
	3451,
	1452,
	3364,
	2836,
	3397,
	1776,
	3479,
	3479,
	3479,
	3479,
	1770,
	2922,
	3453,
	3453,
	3451,
	1780,
	2922,
	2545,
	3479,
	1144,
	3397,
	1776,
	3479,
	3479,
	3479,
	3479,
	2545,
	3451,
	5159,
	5159,
	5159,
	5159,
	5159,
	5159,
	5159,
	5159,
	3479,
	3479,
	3479,
	3479,
	2887,
	3397,
	3479,
	3479,
	3479,
	3479,
	2545,
	3451,
	3479,
	3479,
	3397,
	1776,
	1770,
	3451,
	3364,
	2836,
	2922,
	3453,
	3453,
	1452,
	2922,
	3479,
	3451,
	1780,
	2922,
	2545,
	777,
	3397,
	2922,
	3479,
	2545,
	3451,
	777,
	3397,
	2922,
	3479,
	2545,
	3451,
	390,
	3397,
	2922,
	3479,
	2545,
	3451,
	777,
	3397,
	2922,
	3479,
	3451,
	2545,
	3479,
	3479,
	3479,
	1467,
	121,
	2870,
	2870,
	2870,
	1612,
	1467,
	1467,
	1072,
	716,
	3479,
	3479,
	1072,
	1096,
	3479,
	2870,
	2814,
	2870,
	2870,
	1612,
	2870,
	3479,
	2922,
	2286,
	1315,
	1315,
	1315,
	334,
	2920,
	4679,
	4679,
	4614,
	4614,
	59,
	1454,
	1061,
	702,
	359,
	1454,
	1459,
	701,
	701,
	1460,
	701,
	701,
	1067,
	1342,
	1005,
	3479,
	2472,
	2300,
	552,
	1470,
	2920,
	2920,
	2920,
	2920,
	2920,
	2920,
	715,
	3479,
	3479,
	3479,
	1179,
	2814,
	3451,
	1451,
	879,
	1060,
	2140,
	726,
	1072,
	2814,
	1072,
	2814,
	1072,
	2814,
	1303,
	3479,
	2814,
	2814,
	3451,
	3479,
	3479,
	3479,
	3479,
	1747,
	3479,
	3479,
	3479,
	1753,
	3479,
	3479,
	3479,
	3479,
	3479,
	3479,
	3479,
	3479,
	1789,
	3479,
	3479,
	3479,
	3479,
	2887,
	3479,
	3479,
	3479,
	1779,
	3479,
	3414,
	3414,
	3414,
	4792,
	5129,
	5129,
	5129,
	5129,
	5129,
	5129,
	5129,
	5129,
	5129,
	5129,
	788,
	2887,
	2903,
	6,
	432,
	436,
	3,
	5159,
	5159,
	3479,
	3479,
	3479,
	3479,
	2870,
	1155,
	5,
	4,
	2902,
	2297,
	3479,
	1743,
	3479,
	1140,
	3479,
	3453,
	3451,
	3397,
	5159,
	5159,
	5159,
	5159,
	5159,
	5159,
	3479,
	3479,
	3479,
	3479,
	3479,
	1747,
	3479,
	3479,
	3479,
	1412,
	3479,
	3479,
	3479,
	3397,
	3479,
	2870,
	2870,
	2140,
	1754,
	1754,
	2654,
	1653,
	1653,
	3453,
	1373,
	2140,
	3397,
	2657,
	3479,
	3479,
	3479,
	3414,
	3479,
	2920,
	3479,
	3479,
	3451,
	2870,
	3479,
	3414,
	3453,
	1388,
	3397,
	3397,
	3397,
	3397,
	3479,
	3479,
	1745,
	3453,
	1315,
	2655,
	1745,
	1446,
	572,
	2655,
	1745,
	3397,
	1315,
	2154,
	1745,
	3397,
	1315,
	2154,
	1745,
	3397,
	1315,
	2154,
	1745,
	1388,
	588,
	2545,
	1745,
	3397,
	1315,
	2154,
	1745,
	3397,
	1315,
	2154,
	3479,
	2870,
	3479,
	3451,
	3414,
	3479,
	3414,
	5226,
	3479,
	1238,
	2870,
	3479,
	3451,
	3414,
	3479,
	3414,
	3479,
	3479,
	3479,
	2545,
	3479,
	1745,
	2545,
	942,
	2545,
	4786,
	5226,
	5226,
	2887,
	3479,
	3913,
	5128,
	5159,
	3901,
	5226,
	3479,
	3479,
	3414,
	2887,
	2528,
	3414,
	2154,
	2870,
	1653,
	1653,
	5226,
	799,
	1753,
};
extern const CustomAttributesCacheGenerator g_SALSAU2DLipSync_AttributeGenerators[];
IL2CPP_EXTERN_C const Il2CppCodeGenModule g_SALSAU2DLipSync_CodeGenModule;
const Il2CppCodeGenModule g_SALSAU2DLipSync_CodeGenModule = 
{
	"SALSA-LipSync.dll",
	494,
	s_methodPointers,
	8,
	s_adjustorThunks,
	s_InvokerIndices,
	0,
	NULL,
	0,
	NULL,
	0,
	NULL,
	NULL,
	g_SALSAU2DLipSync_AttributeGenerators,
	NULL, // module initializer,
	NULL,
	NULL,
	NULL,
};
