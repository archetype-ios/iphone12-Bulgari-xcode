﻿#include "pch-cpp.hpp"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include <limits>
#include <stdint.h>



// System.Collections.Generic.List`1<CrazyMinnow.SALSA.Group>
struct List_1_t4CEBDA821397C4F46F5C0D752B030DA847F86F42;
// System.Collections.Generic.List`1<System.Int32>
struct List_1_t260B41F956D673396C33A4CF94E8D6C4389EACB7;
// System.Collections.Generic.List`1<System.Object>
struct List_1_t3F94120C77410A62EAE48421CF166B83AB95A2F5;
// System.Collections.Generic.List`1<CrazyMinnow.SALSA.Shape>
struct List_1_t18177CFBF082431F1077382C8DEF236A68EE2240;
// System.Boolean[]
struct BooleanU5BU5D_tEC7BAF93C44F875016DAADC8696EE3A465644D3C;
// System.Char[]
struct CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34;
// UnityEngine.GameObject[]
struct GameObjectU5BU5D_tA88FC1A1FC9D4D73D0B3984D4B0ECE88F4C47642;
// CrazyMinnow.SALSA.Group[]
struct GroupU5BU5D_t19548F734060123F227359DFEC233D92306A83FF;
// System.Int32[]
struct Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32;
// System.IntPtr[]
struct IntPtrU5BU5D_t27FC72B0409D75AAF33EC42498E8094E95FEE9A6;
// System.Object[]
struct ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE;
// CrazyMinnow.SALSA.RandomEyesCustomShape[]
struct RandomEyesCustomShapeU5BU5D_t8FB0C62923194ED52AE0B06A6249680C47CA152C;
// CrazyMinnow.SALSA.Shape[]
struct ShapeU5BU5D_tB93524008E1EA2809C6594B02B143C4822031504;
// System.Single[]
struct SingleU5BU5D_t47E8DBF5B597C122478D1FFBD9DD57399A0650FA;
// UnityEngine.SpriteRenderer[]
struct SpriteRendererU5BU5D_t45BF125944C2F3974C568E650FCCDB591E5ABFBF;
// System.Diagnostics.StackTrace[]
struct StackTraceU5BU5D_t4AD999C288CB6D1F38A299D12B1598D606588971;
// System.String[]
struct StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A;
// UnityEngine.Transform[]
struct TransformU5BU5D_t7821C0520CC567C0A069329C01AE9C058C7E3F1D;
// UnityEngine.Vector3[]
struct Vector3U5BU5D_t5FB88EAA33E46838BDC2ABDAEA3E8727491CB9E4;
// UnityEngine.AudioClip
struct AudioClip_t16D2E573E7CC1C5118D8EE0F0692D46866A1C0EE;
// UnityEngine.AudioSource
struct AudioSource_tC4BF65AF8CDCAA63724BB3CA59A7A29249269E6B;
// UnityEngine.Component
struct Component_t62FBC8D2420DA4BE9037AFE430740F6B3EECA684;
// UnityEngine.Coroutine
struct Coroutine_t899D5232EF542CB8BA70AF9ECEECA494FAA9CCB7;
// UnityEngine.GameObject
struct GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319;
// CrazyMinnow.SALSA.Group
struct Group_t0CC34C3639F4AF852CE020ABE05C91D971EBEDB1;
// System.Collections.IDictionary
struct IDictionary_t99871C56B8EC2452AC5C4CF3831695E617B89D3A;
// System.Collections.IEnumerator
struct IEnumerator_t5956F3AFB7ECF1117E3BC5890E7FC7B7F7A04105;
// UnityEngine.Mesh
struct Mesh_t2F5992DBA650D5862B43D3823ACD997132A57DA6;
// UnityEngine.MonoBehaviour
struct MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A;
// System.NotSupportedException
struct NotSupportedException_tB9D89F0E9470A2C423D239D7C68EE0CFD77F9339;
// UnityEngine.Object
struct Object_tF2F3778131EFF286AF62B7B013A170F95A91571A;
// CrazyMinnow.SALSA.RandomEyes2D
struct RandomEyes2D_t253864BE689DE7E477335D17ECD2EC77001B377F;
// CrazyMinnow.SALSA.RandomEyes2DGizmo
struct RandomEyes2DGizmo_t241646E16376D86639D345C821DF03D9EDFDDDDD;
// CrazyMinnow.SALSA.RandomEyes3D
struct RandomEyes3D_t6868157C4605BD0225E1B08301F110E72336CE45;
// CrazyMinnow.SALSA.RandomEyes3DGizmo
struct RandomEyes3DGizmo_t039AFDA0DE2968F2C3DFB63606D40A2C238839A1;
// CrazyMinnow.SALSA.RandomEyesBlendAmounts
struct RandomEyesBlendAmounts_t728404128F97998E0D58F3849065DCD32A181621;
// CrazyMinnow.SALSA.RandomEyesCustomShape
struct RandomEyesCustomShape_t10BCA0CEC18C2BBF6A8BD7BFAD2AF005D233C354;
// CrazyMinnow.SALSA.RandomEyesCustomShapeStatus
struct RandomEyesCustomShapeStatus_tAE795B1C1EF38DFDE0535D6A919E25A96283DD66;
// CrazyMinnow.SALSA.RandomEyesLookStatus
struct RandomEyesLookStatus_t2C78F0556D5424EFB610AEC14051A53E29757DC4;
// UnityEngine.Renderer
struct Renderer_t58147AB5B00224FE1460FD47542DC0DA7EC9378C;
// System.Runtime.Serialization.SafeSerializationManager
struct SafeSerializationManager_tDE44F029589A028F8A3053C5C06153FAB4AAE29F;
// CrazyMinnow.SALSA.Salsa2D
struct Salsa2D_t54C8B8FFE3B8149C565BD2A69D53629E65CFFB62;
// CrazyMinnow.SALSA.Salsa3D
struct Salsa3D_tACF83D529139263662FEAAC8F5B53EC43B140C9C;
// CrazyMinnow.SALSA.SalsaBlendAmounts
struct SalsaBlendAmounts_t8FBE1BDDD1306BB8232FCAAD43FC87042840D626;
// CrazyMinnow.SALSA.SalsaStatus
struct SalsaStatus_t67641B5EA3D03836981ED128DA718792E18105C1;
// CrazyMinnow.SALSA.SalsaUtility
struct SalsaUtility_t3157054D4AC9188D9E6BE3F1E4D00B71C49BB30B;
// CrazyMinnow.SALSA.Shape
struct Shape_t15D1659B3662125F0DBAA6FE8F716125CF62AF2F;
// UnityEngine.SkinnedMeshRenderer
struct SkinnedMeshRenderer_t126F4D6010E0F4B2685A7817B0A9171805D8F496;
// UnityEngine.Sprite
struct Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9;
// UnityEngine.SpriteRenderer
struct SpriteRenderer_t3F35AD5498243C170B46F5FFDB582AAEF78615EF;
// System.String
struct String_t;
// UnityEngine.Texture2D
struct Texture2D_t9B604D0D8E28032123641A7E7338FA872E2698BF;
// UnityEngine.Transform
struct Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1;
// System.Void
struct Void_t700C6383A2A510C2CF4DD86DABD5CA9FF70ADAC5;
// UnityEngine.WaitForSeconds
struct WaitForSeconds_t8F9189BE6E467C98C99177038881F8982E0E4013;
// UnityEngine.AudioClip/PCMReaderCallback
struct PCMReaderCallback_t9CA1437D36509A9FAC5EDD8FF2BC3259C24D0E0B;
// UnityEngine.AudioClip/PCMSetPositionCallback
struct PCMSetPositionCallback_tBDD99E7C0697687F1E7B06CDD5DE444A3709CF4C;
// CrazyMinnow.SALSA.RandomEyes2D/<BlinkEyes>d__2
struct U3CBlinkEyesU3Ed__2_t7DFD119FC328CD8363C654182C85B8B62BA190D1;
// CrazyMinnow.SALSA.RandomEyes2D/<TargetAffinityUpdate>d__0
struct U3CTargetAffinityUpdateU3Ed__0_tB9D47FFD16FA8A3CF1860EC8ADB05A787654DF78;
// CrazyMinnow.SALSA.RandomEyes3D/<BlinkEyes>d__2
struct U3CBlinkEyesU3Ed__2_t05DA28F00D8E1A9E64D0728065DB53DA96232044;
// CrazyMinnow.SALSA.RandomEyes3D/<DoBlink>d__c
struct U3CDoBlinkU3Ed__c_t8CF4EE9B667E494EE34FBB05C5E960320E5C8418;
// CrazyMinnow.SALSA.RandomEyes3D/<SetCustomShapeDuration>d__8
struct U3CSetCustomShapeDurationU3Ed__8_t1CD4174D8597ACF6A20E2146BB78651E84AE273B;
// CrazyMinnow.SALSA.RandomEyes3D/<SetCustomShapeDuration>d__a
struct U3CSetCustomShapeDurationU3Ed__a_tF3A147C5E712D0581C2B12133986348BFBB67140;
// CrazyMinnow.SALSA.RandomEyes3D/<SetCustomShapeOverrideDuration>d__4
struct U3CSetCustomShapeOverrideDurationU3Ed__4_tA42191F3DEC3EBAD53A277EAC5C346C81A3BC122;
// CrazyMinnow.SALSA.RandomEyes3D/<SetCustomShapeOverrideDuration>d__6
struct U3CSetCustomShapeOverrideDurationU3Ed__6_t3067670AF5E03DD7899B8F7492C6EF59E5B77DB0;
// CrazyMinnow.SALSA.RandomEyes3D/<TargetAffinityUpdate>d__0
struct U3CTargetAffinityUpdateU3Ed__0_tCF892F05CB3FC0D3FC4F3984F53D8C429D011013;
// CrazyMinnow.SALSA.Salsa2D/<UpdateSample>d__0
struct U3CUpdateSampleU3Ed__0_t610463941DB34736C5EE20E7CA02876D017BE95F;
// CrazyMinnow.SALSA.Salsa3D/<UpdateSample>d__0
struct U3CUpdateSampleU3Ed__0_tB4DEFA6499D020E6EC2C7109DBED4C19296282D3;

IL2CPP_EXTERN_C RuntimeClass* AudioClip_t16D2E573E7CC1C5118D8EE0F0692D46866A1C0EE_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* BooleanU5BU5D_tEC7BAF93C44F875016DAADC8696EE3A465644D3C_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* GameObjectU5BU5D_tA88FC1A1FC9D4D73D0B3984D4B0ECE88F4C47642_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Int32_tFDE5F8CD43D10453F6A2E0C77FE48C6CC7009046_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* List_1_t18177CFBF082431F1077382C8DEF236A68EE2240_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* List_1_t260B41F956D673396C33A4CF94E8D6C4389EACB7_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* List_1_t4CEBDA821397C4F46F5C0D752B030DA847F86F42_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* NotSupportedException_tB9D89F0E9470A2C423D239D7C68EE0CFD77F9339_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* RandomEyes2D_t253864BE689DE7E477335D17ECD2EC77001B377F_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* RandomEyes3D_t6868157C4605BD0225E1B08301F110E72336CE45_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* RandomEyesBlendAmounts_t728404128F97998E0D58F3849065DCD32A181621_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* RandomEyesCustomShapeStatus_tAE795B1C1EF38DFDE0535D6A919E25A96283DD66_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* RandomEyesCustomShapeU5BU5D_t8FB0C62923194ED52AE0B06A6249680C47CA152C_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* RandomEyesCustomShape_t10BCA0CEC18C2BBF6A8BD7BFAD2AF005D233C354_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Salsa2D_t54C8B8FFE3B8149C565BD2A69D53629E65CFFB62_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Salsa3D_tACF83D529139263662FEAAC8F5B53EC43B140C9C_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* SalsaBlendAmounts_t8FBE1BDDD1306BB8232FCAAD43FC87042840D626_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* SalsaStatus_t67641B5EA3D03836981ED128DA718792E18105C1_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* SingleU5BU5D_t47E8DBF5B597C122478D1FFBD9DD57399A0650FA_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* SpriteRendererU5BU5D_t45BF125944C2F3974C568E650FCCDB591E5ABFBF_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* SpriteRenderer_t3F35AD5498243C170B46F5FFDB582AAEF78615EF_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Texture2D_t9B604D0D8E28032123641A7E7338FA872E2698BF_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* U3CBlinkEyesU3Ed__2_t05DA28F00D8E1A9E64D0728065DB53DA96232044_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* U3CBlinkEyesU3Ed__2_t7DFD119FC328CD8363C654182C85B8B62BA190D1_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* U3CDoBlinkU3Ed__c_t8CF4EE9B667E494EE34FBB05C5E960320E5C8418_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* U3CSetCustomShapeDurationU3Ed__8_t1CD4174D8597ACF6A20E2146BB78651E84AE273B_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* U3CSetCustomShapeDurationU3Ed__a_tF3A147C5E712D0581C2B12133986348BFBB67140_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* U3CSetCustomShapeOverrideDurationU3Ed__4_tA42191F3DEC3EBAD53A277EAC5C346C81A3BC122_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* U3CSetCustomShapeOverrideDurationU3Ed__6_t3067670AF5E03DD7899B8F7492C6EF59E5B77DB0_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* U3CTargetAffinityUpdateU3Ed__0_tB9D47FFD16FA8A3CF1860EC8ADB05A787654DF78_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* U3CTargetAffinityUpdateU3Ed__0_tCF892F05CB3FC0D3FC4F3984F53D8C429D011013_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* U3CUpdateSampleU3Ed__0_t610463941DB34736C5EE20E7CA02876D017BE95F_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* U3CUpdateSampleU3Ed__0_tB4DEFA6499D020E6EC2C7109DBED4C19296282D3_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Vector3U5BU5D_t5FB88EAA33E46838BDC2ABDAEA3E8727491CB9E4_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* WaitForSeconds_t8F9189BE6E467C98C99177038881F8982E0E4013_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C String_t* _stringLiteral1D576BF6F6423EB3D2CD8843BB02C91A7E8D3143;
IL2CPP_EXTERN_C String_t* _stringLiteral2AF10F459E51945366EFDDFD0D2108598B13F7DD;
IL2CPP_EXTERN_C String_t* _stringLiteral461EE177B772C8076E2D62C04952F00C85951024;
IL2CPP_EXTERN_C String_t* _stringLiteral5153EC33000758EF3918055BED13B9869A6DB7B2;
IL2CPP_EXTERN_C String_t* _stringLiteral548D93DDB2AC6B24373148B19D9A625571AB2318;
IL2CPP_EXTERN_C String_t* _stringLiteral590F6531E6E208C59FB3221DF4DBCAC088455F77;
IL2CPP_EXTERN_C String_t* _stringLiteral63F369BAD251E7C89E2DB1884C363A04040FF39D;
IL2CPP_EXTERN_C String_t* _stringLiteral74ECD97912D81E0F9C90F35D74E271439C4F3190;
IL2CPP_EXTERN_C String_t* _stringLiteral7D43707E485AAE2574F2C02D133505293F0F5975;
IL2CPP_EXTERN_C String_t* _stringLiteral861234844093461379B1BEC77B56549309505C65;
IL2CPP_EXTERN_C String_t* _stringLiteral8739227E8E687EF781DA0D923452C2686CFF10A2;
IL2CPP_EXTERN_C String_t* _stringLiteral8B50F11C361CC9D4C94D35E40C9AE333832AC31B;
IL2CPP_EXTERN_C String_t* _stringLiteral9F4D5611E34F46999EFF8838045FEB99533BC13E;
IL2CPP_EXTERN_C String_t* _stringLiteralA33A5CAE02B786C2060461DF8C6764B4C05E9423;
IL2CPP_EXTERN_C String_t* _stringLiteralAE4B715AAAA58DCCE03CE0702B58CE77A631DE43;
IL2CPP_EXTERN_C String_t* _stringLiteralB0CAD8BCA47977D99C1B3C76C933E0FD4E6311FA;
IL2CPP_EXTERN_C String_t* _stringLiteralB1E5119D36EC43B340C0A0DDC99F1156546EA9DF;
IL2CPP_EXTERN_C String_t* _stringLiteralB9D3D73187778AF6D06AB846BD78D488ADBFB70E;
IL2CPP_EXTERN_C String_t* _stringLiteralC19BE6CBB5FEA10EDE721E3CF59D93ACFA10E599;
IL2CPP_EXTERN_C String_t* _stringLiteralC2D834C11C0F2F1BF471E704863B12385F9A7838;
IL2CPP_EXTERN_C String_t* _stringLiteralCCE432AEDAEF20FC0D1E205E083D0333967EE606;
IL2CPP_EXTERN_C String_t* _stringLiteralDA39A3EE5E6B4B0D3255BFEF95601890AFD80709;
IL2CPP_EXTERN_C String_t* _stringLiteralDE61B9AE03FF764AE619F4C4A8F720E843928341;
IL2CPP_EXTERN_C String_t* _stringLiteralFA880A6E9F663158BE6AFD9CB08CE9CD38B7D3AC;
IL2CPP_EXTERN_C String_t* _stringLiteralFCD4948C0020C53A3488DFC4A7D87786B7F6EC48;
IL2CPP_EXTERN_C String_t* _stringLiteralFE566FEFE77E0B16136ADCE410AD98FF054E2937;
IL2CPP_EXTERN_C const RuntimeMethod* Component_GetComponent_TisAudioSource_tC4BF65AF8CDCAA63724BB3CA59A7A29249269E6B_mCC3B7A679ECE504BFAF8C70C4EF527511F46902F_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Component_GetComponent_TisRandomEyes3DGizmo_t039AFDA0DE2968F2C3DFB63606D40A2C238839A1_mF418C2AB9AA5AE15ECFB9151E7F925BE3AA284A1_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Component_GetComponent_TisSkinnedMeshRenderer_t126F4D6010E0F4B2685A7817B0A9171805D8F496_m48EF3D17CF12700CC28C88CEFBB6741D6E1FFFE3_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Component_GetComponent_TisSpriteRenderer_t3F35AD5498243C170B46F5FFDB582AAEF78615EF_m7224DFA0D29BF2205FEA6E432D9BCB4133F6E491_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* GameObject_AddComponent_TisAudioSource_tC4BF65AF8CDCAA63724BB3CA59A7A29249269E6B_m74F4A6C820807E361696D4E8F71DC1E54BBE7F76_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* GameObject_AddComponent_TisRandomEyes2DGizmo_t241646E16376D86639D345C821DF03D9EDFDDDDD_m66CEF18356A343054A35723CCD97E26EA315A0DC_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* GameObject_AddComponent_TisRandomEyes3DGizmo_t039AFDA0DE2968F2C3DFB63606D40A2C238839A1_m2076D0783F9DCDF3BC97B514CDA27C93C23C5B0C_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* GameObject_GetComponentsInChildren_TisTransform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1_m1D81E170D9B0CD0720A6BCDD722BC8A0B4AA8F0E_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* List_1_Add_mEE653047BDB3486ACC2E16DC6C3422A0BA48F01F_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* List_1_RemoveAt_m4A6ABD183823501A4F9A6082D9EDC589029AD221_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* List_1__ctor_m1B9889869FAF2EC3320EFFDBB879511C4C48A20D_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* List_1__ctor_m45E78772E9157F6CD684A69AAB07CE4082FE5FFD_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* List_1__ctor_m8AF93F3DEF238332B1CE69F617C558510043D656_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* List_1_get_Count_m1A9C5C4BF43BFA79BD47ABB0D67817ACAC8C681F_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* List_1_get_Count_m7FA90926D9267868473EF90941F6BF794EC87FF2_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* List_1_get_Count_mCC4A67D6388864B347F3727B2A7172BB3E48AF13_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* List_1_get_Item_m6C9326C28DBBC58E66D0FDBC7DD547C22170CEB5_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* List_1_get_Item_m730FCAD2646FA94B07D1216A512B09AB9F0BBA5D_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* List_1_get_Item_mE46947FFA0AD2152DB5591A5A73B6338F9965EB0_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* U3CBlinkEyesU3Ed__2_System_Collections_IEnumerator_Reset_m88EBA7BE8E25F597E063CB1264198186B11DEBCB_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* U3CBlinkEyesU3Ed__2_System_Collections_IEnumerator_Reset_mD5DCBE6D12F53C662EF106666A000BEB03ED3558_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* U3CDoBlinkU3Ed__c_System_Collections_IEnumerator_Reset_m04161B4126710078525F0D3F99E46A39A538B5B6_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* U3CSetCustomShapeDurationU3Ed__8_System_Collections_IEnumerator_Reset_mA93C2810F2DFD6BB6409274C433C796616BD935F_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* U3CSetCustomShapeDurationU3Ed__a_System_Collections_IEnumerator_Reset_m6AD8EF97A2799112F845813343FD577CA1698CF7_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* U3CSetCustomShapeOverrideDurationU3Ed__4_System_Collections_IEnumerator_Reset_mAD496E65677155FF44B6B02F4850449E59AD4F75_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* U3CSetCustomShapeOverrideDurationU3Ed__6_System_Collections_IEnumerator_Reset_m248F5D80FBFAA6C850D4680977E43D02F8E5F7AC_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* U3CTargetAffinityUpdateU3Ed__0_System_Collections_IEnumerator_Reset_mBAE456A3C0C96A0C8B70B0D32F3D345A0C07CA1E_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* U3CTargetAffinityUpdateU3Ed__0_System_Collections_IEnumerator_Reset_mED26C7B767350312099E767FA28653FB03743451_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* U3CUpdateSampleU3Ed__0_System_Collections_IEnumerator_Reset_m482BF263E6A170335B23F21B5CA758CC19D9370F_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* U3CUpdateSampleU3Ed__0_System_Collections_IEnumerator_Reset_m721FCB06919D945E4656B278C22DB71EBAD5D67B_RuntimeMethod_var;
struct Exception_t_marshaled_com;
struct Exception_t_marshaled_pinvoke;

struct BooleanU5BU5D_tEC7BAF93C44F875016DAADC8696EE3A465644D3C;
struct GameObjectU5BU5D_tA88FC1A1FC9D4D73D0B3984D4B0ECE88F4C47642;
struct Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32;
struct ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE;
struct RandomEyesCustomShapeU5BU5D_t8FB0C62923194ED52AE0B06A6249680C47CA152C;
struct SingleU5BU5D_t47E8DBF5B597C122478D1FFBD9DD57399A0650FA;
struct SpriteRendererU5BU5D_t45BF125944C2F3974C568E650FCCDB591E5ABFBF;
struct StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A;
struct TransformU5BU5D_t7821C0520CC567C0A069329C01AE9C058C7E3F1D;
struct Vector3U5BU5D_t5FB88EAA33E46838BDC2ABDAEA3E8727491CB9E4;

IL2CPP_EXTERN_C_BEGIN
IL2CPP_EXTERN_C_END

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <Module>
struct U3CModuleU3E_t5ADB398A94A8AED49EADC326F1876DE8DF29BFEE 
{
public:

public:
};


// System.Object


// System.Collections.Generic.List`1<CrazyMinnow.SALSA.Group>
struct List_1_t4CEBDA821397C4F46F5C0D752B030DA847F86F42  : public RuntimeObject
{
public:
	// T[] System.Collections.Generic.List`1::_items
	GroupU5BU5D_t19548F734060123F227359DFEC233D92306A83FF* ____items_1;
	// System.Int32 System.Collections.Generic.List`1::_size
	int32_t ____size_2;
	// System.Int32 System.Collections.Generic.List`1::_version
	int32_t ____version_3;
	// System.Object System.Collections.Generic.List`1::_syncRoot
	RuntimeObject * ____syncRoot_4;

public:
	inline static int32_t get_offset_of__items_1() { return static_cast<int32_t>(offsetof(List_1_t4CEBDA821397C4F46F5C0D752B030DA847F86F42, ____items_1)); }
	inline GroupU5BU5D_t19548F734060123F227359DFEC233D92306A83FF* get__items_1() const { return ____items_1; }
	inline GroupU5BU5D_t19548F734060123F227359DFEC233D92306A83FF** get_address_of__items_1() { return &____items_1; }
	inline void set__items_1(GroupU5BU5D_t19548F734060123F227359DFEC233D92306A83FF* value)
	{
		____items_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____items_1), (void*)value);
	}

	inline static int32_t get_offset_of__size_2() { return static_cast<int32_t>(offsetof(List_1_t4CEBDA821397C4F46F5C0D752B030DA847F86F42, ____size_2)); }
	inline int32_t get__size_2() const { return ____size_2; }
	inline int32_t* get_address_of__size_2() { return &____size_2; }
	inline void set__size_2(int32_t value)
	{
		____size_2 = value;
	}

	inline static int32_t get_offset_of__version_3() { return static_cast<int32_t>(offsetof(List_1_t4CEBDA821397C4F46F5C0D752B030DA847F86F42, ____version_3)); }
	inline int32_t get__version_3() const { return ____version_3; }
	inline int32_t* get_address_of__version_3() { return &____version_3; }
	inline void set__version_3(int32_t value)
	{
		____version_3 = value;
	}

	inline static int32_t get_offset_of__syncRoot_4() { return static_cast<int32_t>(offsetof(List_1_t4CEBDA821397C4F46F5C0D752B030DA847F86F42, ____syncRoot_4)); }
	inline RuntimeObject * get__syncRoot_4() const { return ____syncRoot_4; }
	inline RuntimeObject ** get_address_of__syncRoot_4() { return &____syncRoot_4; }
	inline void set__syncRoot_4(RuntimeObject * value)
	{
		____syncRoot_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____syncRoot_4), (void*)value);
	}
};

struct List_1_t4CEBDA821397C4F46F5C0D752B030DA847F86F42_StaticFields
{
public:
	// T[] System.Collections.Generic.List`1::_emptyArray
	GroupU5BU5D_t19548F734060123F227359DFEC233D92306A83FF* ____emptyArray_5;

public:
	inline static int32_t get_offset_of__emptyArray_5() { return static_cast<int32_t>(offsetof(List_1_t4CEBDA821397C4F46F5C0D752B030DA847F86F42_StaticFields, ____emptyArray_5)); }
	inline GroupU5BU5D_t19548F734060123F227359DFEC233D92306A83FF* get__emptyArray_5() const { return ____emptyArray_5; }
	inline GroupU5BU5D_t19548F734060123F227359DFEC233D92306A83FF** get_address_of__emptyArray_5() { return &____emptyArray_5; }
	inline void set__emptyArray_5(GroupU5BU5D_t19548F734060123F227359DFEC233D92306A83FF* value)
	{
		____emptyArray_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____emptyArray_5), (void*)value);
	}
};


// System.Collections.Generic.List`1<System.Int32>
struct List_1_t260B41F956D673396C33A4CF94E8D6C4389EACB7  : public RuntimeObject
{
public:
	// T[] System.Collections.Generic.List`1::_items
	Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* ____items_1;
	// System.Int32 System.Collections.Generic.List`1::_size
	int32_t ____size_2;
	// System.Int32 System.Collections.Generic.List`1::_version
	int32_t ____version_3;
	// System.Object System.Collections.Generic.List`1::_syncRoot
	RuntimeObject * ____syncRoot_4;

public:
	inline static int32_t get_offset_of__items_1() { return static_cast<int32_t>(offsetof(List_1_t260B41F956D673396C33A4CF94E8D6C4389EACB7, ____items_1)); }
	inline Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* get__items_1() const { return ____items_1; }
	inline Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32** get_address_of__items_1() { return &____items_1; }
	inline void set__items_1(Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* value)
	{
		____items_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____items_1), (void*)value);
	}

	inline static int32_t get_offset_of__size_2() { return static_cast<int32_t>(offsetof(List_1_t260B41F956D673396C33A4CF94E8D6C4389EACB7, ____size_2)); }
	inline int32_t get__size_2() const { return ____size_2; }
	inline int32_t* get_address_of__size_2() { return &____size_2; }
	inline void set__size_2(int32_t value)
	{
		____size_2 = value;
	}

	inline static int32_t get_offset_of__version_3() { return static_cast<int32_t>(offsetof(List_1_t260B41F956D673396C33A4CF94E8D6C4389EACB7, ____version_3)); }
	inline int32_t get__version_3() const { return ____version_3; }
	inline int32_t* get_address_of__version_3() { return &____version_3; }
	inline void set__version_3(int32_t value)
	{
		____version_3 = value;
	}

	inline static int32_t get_offset_of__syncRoot_4() { return static_cast<int32_t>(offsetof(List_1_t260B41F956D673396C33A4CF94E8D6C4389EACB7, ____syncRoot_4)); }
	inline RuntimeObject * get__syncRoot_4() const { return ____syncRoot_4; }
	inline RuntimeObject ** get_address_of__syncRoot_4() { return &____syncRoot_4; }
	inline void set__syncRoot_4(RuntimeObject * value)
	{
		____syncRoot_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____syncRoot_4), (void*)value);
	}
};

struct List_1_t260B41F956D673396C33A4CF94E8D6C4389EACB7_StaticFields
{
public:
	// T[] System.Collections.Generic.List`1::_emptyArray
	Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* ____emptyArray_5;

public:
	inline static int32_t get_offset_of__emptyArray_5() { return static_cast<int32_t>(offsetof(List_1_t260B41F956D673396C33A4CF94E8D6C4389EACB7_StaticFields, ____emptyArray_5)); }
	inline Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* get__emptyArray_5() const { return ____emptyArray_5; }
	inline Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32** get_address_of__emptyArray_5() { return &____emptyArray_5; }
	inline void set__emptyArray_5(Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* value)
	{
		____emptyArray_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____emptyArray_5), (void*)value);
	}
};


// System.Collections.Generic.List`1<System.Object>
struct List_1_t3F94120C77410A62EAE48421CF166B83AB95A2F5  : public RuntimeObject
{
public:
	// T[] System.Collections.Generic.List`1::_items
	ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE* ____items_1;
	// System.Int32 System.Collections.Generic.List`1::_size
	int32_t ____size_2;
	// System.Int32 System.Collections.Generic.List`1::_version
	int32_t ____version_3;
	// System.Object System.Collections.Generic.List`1::_syncRoot
	RuntimeObject * ____syncRoot_4;

public:
	inline static int32_t get_offset_of__items_1() { return static_cast<int32_t>(offsetof(List_1_t3F94120C77410A62EAE48421CF166B83AB95A2F5, ____items_1)); }
	inline ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE* get__items_1() const { return ____items_1; }
	inline ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE** get_address_of__items_1() { return &____items_1; }
	inline void set__items_1(ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE* value)
	{
		____items_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____items_1), (void*)value);
	}

	inline static int32_t get_offset_of__size_2() { return static_cast<int32_t>(offsetof(List_1_t3F94120C77410A62EAE48421CF166B83AB95A2F5, ____size_2)); }
	inline int32_t get__size_2() const { return ____size_2; }
	inline int32_t* get_address_of__size_2() { return &____size_2; }
	inline void set__size_2(int32_t value)
	{
		____size_2 = value;
	}

	inline static int32_t get_offset_of__version_3() { return static_cast<int32_t>(offsetof(List_1_t3F94120C77410A62EAE48421CF166B83AB95A2F5, ____version_3)); }
	inline int32_t get__version_3() const { return ____version_3; }
	inline int32_t* get_address_of__version_3() { return &____version_3; }
	inline void set__version_3(int32_t value)
	{
		____version_3 = value;
	}

	inline static int32_t get_offset_of__syncRoot_4() { return static_cast<int32_t>(offsetof(List_1_t3F94120C77410A62EAE48421CF166B83AB95A2F5, ____syncRoot_4)); }
	inline RuntimeObject * get__syncRoot_4() const { return ____syncRoot_4; }
	inline RuntimeObject ** get_address_of__syncRoot_4() { return &____syncRoot_4; }
	inline void set__syncRoot_4(RuntimeObject * value)
	{
		____syncRoot_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____syncRoot_4), (void*)value);
	}
};

struct List_1_t3F94120C77410A62EAE48421CF166B83AB95A2F5_StaticFields
{
public:
	// T[] System.Collections.Generic.List`1::_emptyArray
	ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE* ____emptyArray_5;

public:
	inline static int32_t get_offset_of__emptyArray_5() { return static_cast<int32_t>(offsetof(List_1_t3F94120C77410A62EAE48421CF166B83AB95A2F5_StaticFields, ____emptyArray_5)); }
	inline ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE* get__emptyArray_5() const { return ____emptyArray_5; }
	inline ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE** get_address_of__emptyArray_5() { return &____emptyArray_5; }
	inline void set__emptyArray_5(ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE* value)
	{
		____emptyArray_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____emptyArray_5), (void*)value);
	}
};


// System.Collections.Generic.List`1<CrazyMinnow.SALSA.Shape>
struct List_1_t18177CFBF082431F1077382C8DEF236A68EE2240  : public RuntimeObject
{
public:
	// T[] System.Collections.Generic.List`1::_items
	ShapeU5BU5D_tB93524008E1EA2809C6594B02B143C4822031504* ____items_1;
	// System.Int32 System.Collections.Generic.List`1::_size
	int32_t ____size_2;
	// System.Int32 System.Collections.Generic.List`1::_version
	int32_t ____version_3;
	// System.Object System.Collections.Generic.List`1::_syncRoot
	RuntimeObject * ____syncRoot_4;

public:
	inline static int32_t get_offset_of__items_1() { return static_cast<int32_t>(offsetof(List_1_t18177CFBF082431F1077382C8DEF236A68EE2240, ____items_1)); }
	inline ShapeU5BU5D_tB93524008E1EA2809C6594B02B143C4822031504* get__items_1() const { return ____items_1; }
	inline ShapeU5BU5D_tB93524008E1EA2809C6594B02B143C4822031504** get_address_of__items_1() { return &____items_1; }
	inline void set__items_1(ShapeU5BU5D_tB93524008E1EA2809C6594B02B143C4822031504* value)
	{
		____items_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____items_1), (void*)value);
	}

	inline static int32_t get_offset_of__size_2() { return static_cast<int32_t>(offsetof(List_1_t18177CFBF082431F1077382C8DEF236A68EE2240, ____size_2)); }
	inline int32_t get__size_2() const { return ____size_2; }
	inline int32_t* get_address_of__size_2() { return &____size_2; }
	inline void set__size_2(int32_t value)
	{
		____size_2 = value;
	}

	inline static int32_t get_offset_of__version_3() { return static_cast<int32_t>(offsetof(List_1_t18177CFBF082431F1077382C8DEF236A68EE2240, ____version_3)); }
	inline int32_t get__version_3() const { return ____version_3; }
	inline int32_t* get_address_of__version_3() { return &____version_3; }
	inline void set__version_3(int32_t value)
	{
		____version_3 = value;
	}

	inline static int32_t get_offset_of__syncRoot_4() { return static_cast<int32_t>(offsetof(List_1_t18177CFBF082431F1077382C8DEF236A68EE2240, ____syncRoot_4)); }
	inline RuntimeObject * get__syncRoot_4() const { return ____syncRoot_4; }
	inline RuntimeObject ** get_address_of__syncRoot_4() { return &____syncRoot_4; }
	inline void set__syncRoot_4(RuntimeObject * value)
	{
		____syncRoot_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____syncRoot_4), (void*)value);
	}
};

struct List_1_t18177CFBF082431F1077382C8DEF236A68EE2240_StaticFields
{
public:
	// T[] System.Collections.Generic.List`1::_emptyArray
	ShapeU5BU5D_tB93524008E1EA2809C6594B02B143C4822031504* ____emptyArray_5;

public:
	inline static int32_t get_offset_of__emptyArray_5() { return static_cast<int32_t>(offsetof(List_1_t18177CFBF082431F1077382C8DEF236A68EE2240_StaticFields, ____emptyArray_5)); }
	inline ShapeU5BU5D_tB93524008E1EA2809C6594B02B143C4822031504* get__emptyArray_5() const { return ____emptyArray_5; }
	inline ShapeU5BU5D_tB93524008E1EA2809C6594B02B143C4822031504** get_address_of__emptyArray_5() { return &____emptyArray_5; }
	inline void set__emptyArray_5(ShapeU5BU5D_tB93524008E1EA2809C6594B02B143C4822031504* value)
	{
		____emptyArray_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____emptyArray_5), (void*)value);
	}
};

struct Il2CppArrayBounds;

// System.Array


// CrazyMinnow.SALSA.Group
struct Group_t0CC34C3639F4AF852CE020ABE05C91D971EBEDB1  : public RuntimeObject
{
public:
	// System.String CrazyMinnow.SALSA.Group::name
	String_t* ___name_0;
	// System.Boolean CrazyMinnow.SALSA.Group::preview
	bool ___preview_1;
	// System.Boolean CrazyMinnow.SALSA.Group::hide
	bool ___hide_2;
	// System.Collections.Generic.List`1<CrazyMinnow.SALSA.Shape> CrazyMinnow.SALSA.Group::shapes
	List_1_t18177CFBF082431F1077382C8DEF236A68EE2240 * ___shapes_3;

public:
	inline static int32_t get_offset_of_name_0() { return static_cast<int32_t>(offsetof(Group_t0CC34C3639F4AF852CE020ABE05C91D971EBEDB1, ___name_0)); }
	inline String_t* get_name_0() const { return ___name_0; }
	inline String_t** get_address_of_name_0() { return &___name_0; }
	inline void set_name_0(String_t* value)
	{
		___name_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___name_0), (void*)value);
	}

	inline static int32_t get_offset_of_preview_1() { return static_cast<int32_t>(offsetof(Group_t0CC34C3639F4AF852CE020ABE05C91D971EBEDB1, ___preview_1)); }
	inline bool get_preview_1() const { return ___preview_1; }
	inline bool* get_address_of_preview_1() { return &___preview_1; }
	inline void set_preview_1(bool value)
	{
		___preview_1 = value;
	}

	inline static int32_t get_offset_of_hide_2() { return static_cast<int32_t>(offsetof(Group_t0CC34C3639F4AF852CE020ABE05C91D971EBEDB1, ___hide_2)); }
	inline bool get_hide_2() const { return ___hide_2; }
	inline bool* get_address_of_hide_2() { return &___hide_2; }
	inline void set_hide_2(bool value)
	{
		___hide_2 = value;
	}

	inline static int32_t get_offset_of_shapes_3() { return static_cast<int32_t>(offsetof(Group_t0CC34C3639F4AF852CE020ABE05C91D971EBEDB1, ___shapes_3)); }
	inline List_1_t18177CFBF082431F1077382C8DEF236A68EE2240 * get_shapes_3() const { return ___shapes_3; }
	inline List_1_t18177CFBF082431F1077382C8DEF236A68EE2240 ** get_address_of_shapes_3() { return &___shapes_3; }
	inline void set_shapes_3(List_1_t18177CFBF082431F1077382C8DEF236A68EE2240 * value)
	{
		___shapes_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___shapes_3), (void*)value);
	}
};


// CrazyMinnow.SALSA.RandomEyesBlendAmounts
struct RandomEyesBlendAmounts_t728404128F97998E0D58F3849065DCD32A181621  : public RuntimeObject
{
public:
	// System.Single CrazyMinnow.SALSA.RandomEyesBlendAmounts::lookUp
	float ___lookUp_0;
	// System.Single CrazyMinnow.SALSA.RandomEyesBlendAmounts::lookDown
	float ___lookDown_1;
	// System.Single CrazyMinnow.SALSA.RandomEyesBlendAmounts::lookLeft
	float ___lookLeft_2;
	// System.Single CrazyMinnow.SALSA.RandomEyesBlendAmounts::lookRight
	float ___lookRight_3;
	// System.Single CrazyMinnow.SALSA.RandomEyesBlendAmounts::blink
	float ___blink_4;

public:
	inline static int32_t get_offset_of_lookUp_0() { return static_cast<int32_t>(offsetof(RandomEyesBlendAmounts_t728404128F97998E0D58F3849065DCD32A181621, ___lookUp_0)); }
	inline float get_lookUp_0() const { return ___lookUp_0; }
	inline float* get_address_of_lookUp_0() { return &___lookUp_0; }
	inline void set_lookUp_0(float value)
	{
		___lookUp_0 = value;
	}

	inline static int32_t get_offset_of_lookDown_1() { return static_cast<int32_t>(offsetof(RandomEyesBlendAmounts_t728404128F97998E0D58F3849065DCD32A181621, ___lookDown_1)); }
	inline float get_lookDown_1() const { return ___lookDown_1; }
	inline float* get_address_of_lookDown_1() { return &___lookDown_1; }
	inline void set_lookDown_1(float value)
	{
		___lookDown_1 = value;
	}

	inline static int32_t get_offset_of_lookLeft_2() { return static_cast<int32_t>(offsetof(RandomEyesBlendAmounts_t728404128F97998E0D58F3849065DCD32A181621, ___lookLeft_2)); }
	inline float get_lookLeft_2() const { return ___lookLeft_2; }
	inline float* get_address_of_lookLeft_2() { return &___lookLeft_2; }
	inline void set_lookLeft_2(float value)
	{
		___lookLeft_2 = value;
	}

	inline static int32_t get_offset_of_lookRight_3() { return static_cast<int32_t>(offsetof(RandomEyesBlendAmounts_t728404128F97998E0D58F3849065DCD32A181621, ___lookRight_3)); }
	inline float get_lookRight_3() const { return ___lookRight_3; }
	inline float* get_address_of_lookRight_3() { return &___lookRight_3; }
	inline void set_lookRight_3(float value)
	{
		___lookRight_3 = value;
	}

	inline static int32_t get_offset_of_blink_4() { return static_cast<int32_t>(offsetof(RandomEyesBlendAmounts_t728404128F97998E0D58F3849065DCD32A181621, ___blink_4)); }
	inline float get_blink_4() const { return ___blink_4; }
	inline float* get_address_of_blink_4() { return &___blink_4; }
	inline void set_blink_4(float value)
	{
		___blink_4 = value;
	}
};


// CrazyMinnow.SALSA.RandomEyesCustomShape
struct RandomEyesCustomShape_t10BCA0CEC18C2BBF6A8BD7BFAD2AF005D233C354  : public RuntimeObject
{
public:
	// System.Int32 CrazyMinnow.SALSA.RandomEyesCustomShape::shapeIndex
	int32_t ___shapeIndex_0;
	// System.String CrazyMinnow.SALSA.RandomEyesCustomShape::shapeName
	String_t* ___shapeName_1;
	// System.Single CrazyMinnow.SALSA.RandomEyesCustomShape::shapeValue
	float ___shapeValue_2;
	// System.Boolean CrazyMinnow.SALSA.RandomEyesCustomShape::overrideOn
	bool ___overrideOn_3;
	// System.Boolean CrazyMinnow.SALSA.RandomEyesCustomShape::notRandom
	bool ___notRandom_4;
	// System.Boolean CrazyMinnow.SALSA.RandomEyesCustomShape::isOn
	bool ___isOn_5;
	// System.Single CrazyMinnow.SALSA.RandomEyesCustomShape::blendSpeed
	float ___blendSpeed_6;
	// System.Single CrazyMinnow.SALSA.RandomEyesCustomShape::rangeOfMotion
	float ___rangeOfMotion_7;

public:
	inline static int32_t get_offset_of_shapeIndex_0() { return static_cast<int32_t>(offsetof(RandomEyesCustomShape_t10BCA0CEC18C2BBF6A8BD7BFAD2AF005D233C354, ___shapeIndex_0)); }
	inline int32_t get_shapeIndex_0() const { return ___shapeIndex_0; }
	inline int32_t* get_address_of_shapeIndex_0() { return &___shapeIndex_0; }
	inline void set_shapeIndex_0(int32_t value)
	{
		___shapeIndex_0 = value;
	}

	inline static int32_t get_offset_of_shapeName_1() { return static_cast<int32_t>(offsetof(RandomEyesCustomShape_t10BCA0CEC18C2BBF6A8BD7BFAD2AF005D233C354, ___shapeName_1)); }
	inline String_t* get_shapeName_1() const { return ___shapeName_1; }
	inline String_t** get_address_of_shapeName_1() { return &___shapeName_1; }
	inline void set_shapeName_1(String_t* value)
	{
		___shapeName_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___shapeName_1), (void*)value);
	}

	inline static int32_t get_offset_of_shapeValue_2() { return static_cast<int32_t>(offsetof(RandomEyesCustomShape_t10BCA0CEC18C2BBF6A8BD7BFAD2AF005D233C354, ___shapeValue_2)); }
	inline float get_shapeValue_2() const { return ___shapeValue_2; }
	inline float* get_address_of_shapeValue_2() { return &___shapeValue_2; }
	inline void set_shapeValue_2(float value)
	{
		___shapeValue_2 = value;
	}

	inline static int32_t get_offset_of_overrideOn_3() { return static_cast<int32_t>(offsetof(RandomEyesCustomShape_t10BCA0CEC18C2BBF6A8BD7BFAD2AF005D233C354, ___overrideOn_3)); }
	inline bool get_overrideOn_3() const { return ___overrideOn_3; }
	inline bool* get_address_of_overrideOn_3() { return &___overrideOn_3; }
	inline void set_overrideOn_3(bool value)
	{
		___overrideOn_3 = value;
	}

	inline static int32_t get_offset_of_notRandom_4() { return static_cast<int32_t>(offsetof(RandomEyesCustomShape_t10BCA0CEC18C2BBF6A8BD7BFAD2AF005D233C354, ___notRandom_4)); }
	inline bool get_notRandom_4() const { return ___notRandom_4; }
	inline bool* get_address_of_notRandom_4() { return &___notRandom_4; }
	inline void set_notRandom_4(bool value)
	{
		___notRandom_4 = value;
	}

	inline static int32_t get_offset_of_isOn_5() { return static_cast<int32_t>(offsetof(RandomEyesCustomShape_t10BCA0CEC18C2BBF6A8BD7BFAD2AF005D233C354, ___isOn_5)); }
	inline bool get_isOn_5() const { return ___isOn_5; }
	inline bool* get_address_of_isOn_5() { return &___isOn_5; }
	inline void set_isOn_5(bool value)
	{
		___isOn_5 = value;
	}

	inline static int32_t get_offset_of_blendSpeed_6() { return static_cast<int32_t>(offsetof(RandomEyesCustomShape_t10BCA0CEC18C2BBF6A8BD7BFAD2AF005D233C354, ___blendSpeed_6)); }
	inline float get_blendSpeed_6() const { return ___blendSpeed_6; }
	inline float* get_address_of_blendSpeed_6() { return &___blendSpeed_6; }
	inline void set_blendSpeed_6(float value)
	{
		___blendSpeed_6 = value;
	}

	inline static int32_t get_offset_of_rangeOfMotion_7() { return static_cast<int32_t>(offsetof(RandomEyesCustomShape_t10BCA0CEC18C2BBF6A8BD7BFAD2AF005D233C354, ___rangeOfMotion_7)); }
	inline float get_rangeOfMotion_7() const { return ___rangeOfMotion_7; }
	inline float* get_address_of_rangeOfMotion_7() { return &___rangeOfMotion_7; }
	inline void set_rangeOfMotion_7(float value)
	{
		___rangeOfMotion_7 = value;
	}
};


// CrazyMinnow.SALSA.RandomEyesLookStatus
struct RandomEyesLookStatus_t2C78F0556D5424EFB610AEC14051A53E29757DC4  : public RuntimeObject
{
public:
	// UnityEngine.Object CrazyMinnow.SALSA.RandomEyesLookStatus::<instance>k__BackingField
	Object_tF2F3778131EFF286AF62B7B013A170F95A91571A * ___U3CinstanceU3Ek__BackingField_0;
	// System.Single CrazyMinnow.SALSA.RandomEyesLookStatus::<blendSpeed>k__BackingField
	float ___U3CblendSpeedU3Ek__BackingField_1;
	// System.Single CrazyMinnow.SALSA.RandomEyesLookStatus::<rangeOfMotion>k__BackingField
	float ___U3CrangeOfMotionU3Ek__BackingField_2;

public:
	inline static int32_t get_offset_of_U3CinstanceU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(RandomEyesLookStatus_t2C78F0556D5424EFB610AEC14051A53E29757DC4, ___U3CinstanceU3Ek__BackingField_0)); }
	inline Object_tF2F3778131EFF286AF62B7B013A170F95A91571A * get_U3CinstanceU3Ek__BackingField_0() const { return ___U3CinstanceU3Ek__BackingField_0; }
	inline Object_tF2F3778131EFF286AF62B7B013A170F95A91571A ** get_address_of_U3CinstanceU3Ek__BackingField_0() { return &___U3CinstanceU3Ek__BackingField_0; }
	inline void set_U3CinstanceU3Ek__BackingField_0(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A * value)
	{
		___U3CinstanceU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CinstanceU3Ek__BackingField_0), (void*)value);
	}

	inline static int32_t get_offset_of_U3CblendSpeedU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(RandomEyesLookStatus_t2C78F0556D5424EFB610AEC14051A53E29757DC4, ___U3CblendSpeedU3Ek__BackingField_1)); }
	inline float get_U3CblendSpeedU3Ek__BackingField_1() const { return ___U3CblendSpeedU3Ek__BackingField_1; }
	inline float* get_address_of_U3CblendSpeedU3Ek__BackingField_1() { return &___U3CblendSpeedU3Ek__BackingField_1; }
	inline void set_U3CblendSpeedU3Ek__BackingField_1(float value)
	{
		___U3CblendSpeedU3Ek__BackingField_1 = value;
	}

	inline static int32_t get_offset_of_U3CrangeOfMotionU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(RandomEyesLookStatus_t2C78F0556D5424EFB610AEC14051A53E29757DC4, ___U3CrangeOfMotionU3Ek__BackingField_2)); }
	inline float get_U3CrangeOfMotionU3Ek__BackingField_2() const { return ___U3CrangeOfMotionU3Ek__BackingField_2; }
	inline float* get_address_of_U3CrangeOfMotionU3Ek__BackingField_2() { return &___U3CrangeOfMotionU3Ek__BackingField_2; }
	inline void set_U3CrangeOfMotionU3Ek__BackingField_2(float value)
	{
		___U3CrangeOfMotionU3Ek__BackingField_2 = value;
	}
};


// CrazyMinnow.SALSA.SalsaBlendAmounts
struct SalsaBlendAmounts_t8FBE1BDDD1306BB8232FCAAD43FC87042840D626  : public RuntimeObject
{
public:
	// System.Single CrazyMinnow.SALSA.SalsaBlendAmounts::saySmall
	float ___saySmall_0;
	// System.Single CrazyMinnow.SALSA.SalsaBlendAmounts::sayMedium
	float ___sayMedium_1;
	// System.Single CrazyMinnow.SALSA.SalsaBlendAmounts::sayLarge
	float ___sayLarge_2;

public:
	inline static int32_t get_offset_of_saySmall_0() { return static_cast<int32_t>(offsetof(SalsaBlendAmounts_t8FBE1BDDD1306BB8232FCAAD43FC87042840D626, ___saySmall_0)); }
	inline float get_saySmall_0() const { return ___saySmall_0; }
	inline float* get_address_of_saySmall_0() { return &___saySmall_0; }
	inline void set_saySmall_0(float value)
	{
		___saySmall_0 = value;
	}

	inline static int32_t get_offset_of_sayMedium_1() { return static_cast<int32_t>(offsetof(SalsaBlendAmounts_t8FBE1BDDD1306BB8232FCAAD43FC87042840D626, ___sayMedium_1)); }
	inline float get_sayMedium_1() const { return ___sayMedium_1; }
	inline float* get_address_of_sayMedium_1() { return &___sayMedium_1; }
	inline void set_sayMedium_1(float value)
	{
		___sayMedium_1 = value;
	}

	inline static int32_t get_offset_of_sayLarge_2() { return static_cast<int32_t>(offsetof(SalsaBlendAmounts_t8FBE1BDDD1306BB8232FCAAD43FC87042840D626, ___sayLarge_2)); }
	inline float get_sayLarge_2() const { return ___sayLarge_2; }
	inline float* get_address_of_sayLarge_2() { return &___sayLarge_2; }
	inline void set_sayLarge_2(float value)
	{
		___sayLarge_2 = value;
	}
};


// CrazyMinnow.SALSA.SalsaStatus
struct SalsaStatus_t67641B5EA3D03836981ED128DA718792E18105C1  : public RuntimeObject
{
public:
	// UnityEngine.Object CrazyMinnow.SALSA.SalsaStatus::<instance>k__BackingField
	Object_tF2F3778131EFF286AF62B7B013A170F95A91571A * ___U3CinstanceU3Ek__BackingField_0;
	// System.String CrazyMinnow.SALSA.SalsaStatus::<talkerName>k__BackingField
	String_t* ___U3CtalkerNameU3Ek__BackingField_1;
	// System.Boolean CrazyMinnow.SALSA.SalsaStatus::<isTalking>k__BackingField
	bool ___U3CisTalkingU3Ek__BackingField_2;
	// System.String CrazyMinnow.SALSA.SalsaStatus::<clipName>k__BackingField
	String_t* ___U3CclipNameU3Ek__BackingField_3;

public:
	inline static int32_t get_offset_of_U3CinstanceU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(SalsaStatus_t67641B5EA3D03836981ED128DA718792E18105C1, ___U3CinstanceU3Ek__BackingField_0)); }
	inline Object_tF2F3778131EFF286AF62B7B013A170F95A91571A * get_U3CinstanceU3Ek__BackingField_0() const { return ___U3CinstanceU3Ek__BackingField_0; }
	inline Object_tF2F3778131EFF286AF62B7B013A170F95A91571A ** get_address_of_U3CinstanceU3Ek__BackingField_0() { return &___U3CinstanceU3Ek__BackingField_0; }
	inline void set_U3CinstanceU3Ek__BackingField_0(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A * value)
	{
		___U3CinstanceU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CinstanceU3Ek__BackingField_0), (void*)value);
	}

	inline static int32_t get_offset_of_U3CtalkerNameU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(SalsaStatus_t67641B5EA3D03836981ED128DA718792E18105C1, ___U3CtalkerNameU3Ek__BackingField_1)); }
	inline String_t* get_U3CtalkerNameU3Ek__BackingField_1() const { return ___U3CtalkerNameU3Ek__BackingField_1; }
	inline String_t** get_address_of_U3CtalkerNameU3Ek__BackingField_1() { return &___U3CtalkerNameU3Ek__BackingField_1; }
	inline void set_U3CtalkerNameU3Ek__BackingField_1(String_t* value)
	{
		___U3CtalkerNameU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CtalkerNameU3Ek__BackingField_1), (void*)value);
	}

	inline static int32_t get_offset_of_U3CisTalkingU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(SalsaStatus_t67641B5EA3D03836981ED128DA718792E18105C1, ___U3CisTalkingU3Ek__BackingField_2)); }
	inline bool get_U3CisTalkingU3Ek__BackingField_2() const { return ___U3CisTalkingU3Ek__BackingField_2; }
	inline bool* get_address_of_U3CisTalkingU3Ek__BackingField_2() { return &___U3CisTalkingU3Ek__BackingField_2; }
	inline void set_U3CisTalkingU3Ek__BackingField_2(bool value)
	{
		___U3CisTalkingU3Ek__BackingField_2 = value;
	}

	inline static int32_t get_offset_of_U3CclipNameU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(SalsaStatus_t67641B5EA3D03836981ED128DA718792E18105C1, ___U3CclipNameU3Ek__BackingField_3)); }
	inline String_t* get_U3CclipNameU3Ek__BackingField_3() const { return ___U3CclipNameU3Ek__BackingField_3; }
	inline String_t** get_address_of_U3CclipNameU3Ek__BackingField_3() { return &___U3CclipNameU3Ek__BackingField_3; }
	inline void set_U3CclipNameU3Ek__BackingField_3(String_t* value)
	{
		___U3CclipNameU3Ek__BackingField_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CclipNameU3Ek__BackingField_3), (void*)value);
	}
};


// CrazyMinnow.SALSA.Shape
struct Shape_t15D1659B3662125F0DBAA6FE8F716125CF62AF2F  : public RuntimeObject
{
public:
	// System.Int32 CrazyMinnow.SALSA.Shape::index
	int32_t ___index_0;
	// System.String CrazyMinnow.SALSA.Shape::name
	String_t* ___name_1;
	// System.Single CrazyMinnow.SALSA.Shape::blendSpeed
	float ___blendSpeed_2;
	// System.Single CrazyMinnow.SALSA.Shape::rangeOfMotion
	float ___rangeOfMotion_3;

public:
	inline static int32_t get_offset_of_index_0() { return static_cast<int32_t>(offsetof(Shape_t15D1659B3662125F0DBAA6FE8F716125CF62AF2F, ___index_0)); }
	inline int32_t get_index_0() const { return ___index_0; }
	inline int32_t* get_address_of_index_0() { return &___index_0; }
	inline void set_index_0(int32_t value)
	{
		___index_0 = value;
	}

	inline static int32_t get_offset_of_name_1() { return static_cast<int32_t>(offsetof(Shape_t15D1659B3662125F0DBAA6FE8F716125CF62AF2F, ___name_1)); }
	inline String_t* get_name_1() const { return ___name_1; }
	inline String_t** get_address_of_name_1() { return &___name_1; }
	inline void set_name_1(String_t* value)
	{
		___name_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___name_1), (void*)value);
	}

	inline static int32_t get_offset_of_blendSpeed_2() { return static_cast<int32_t>(offsetof(Shape_t15D1659B3662125F0DBAA6FE8F716125CF62AF2F, ___blendSpeed_2)); }
	inline float get_blendSpeed_2() const { return ___blendSpeed_2; }
	inline float* get_address_of_blendSpeed_2() { return &___blendSpeed_2; }
	inline void set_blendSpeed_2(float value)
	{
		___blendSpeed_2 = value;
	}

	inline static int32_t get_offset_of_rangeOfMotion_3() { return static_cast<int32_t>(offsetof(Shape_t15D1659B3662125F0DBAA6FE8F716125CF62AF2F, ___rangeOfMotion_3)); }
	inline float get_rangeOfMotion_3() const { return ___rangeOfMotion_3; }
	inline float* get_address_of_rangeOfMotion_3() { return &___rangeOfMotion_3; }
	inline void set_rangeOfMotion_3(float value)
	{
		___rangeOfMotion_3 = value;
	}
};


// System.String
struct String_t  : public RuntimeObject
{
public:
	// System.Int32 System.String::m_stringLength
	int32_t ___m_stringLength_0;
	// System.Char System.String::m_firstChar
	Il2CppChar ___m_firstChar_1;

public:
	inline static int32_t get_offset_of_m_stringLength_0() { return static_cast<int32_t>(offsetof(String_t, ___m_stringLength_0)); }
	inline int32_t get_m_stringLength_0() const { return ___m_stringLength_0; }
	inline int32_t* get_address_of_m_stringLength_0() { return &___m_stringLength_0; }
	inline void set_m_stringLength_0(int32_t value)
	{
		___m_stringLength_0 = value;
	}

	inline static int32_t get_offset_of_m_firstChar_1() { return static_cast<int32_t>(offsetof(String_t, ___m_firstChar_1)); }
	inline Il2CppChar get_m_firstChar_1() const { return ___m_firstChar_1; }
	inline Il2CppChar* get_address_of_m_firstChar_1() { return &___m_firstChar_1; }
	inline void set_m_firstChar_1(Il2CppChar value)
	{
		___m_firstChar_1 = value;
	}
};

struct String_t_StaticFields
{
public:
	// System.String System.String::Empty
	String_t* ___Empty_5;

public:
	inline static int32_t get_offset_of_Empty_5() { return static_cast<int32_t>(offsetof(String_t_StaticFields, ___Empty_5)); }
	inline String_t* get_Empty_5() const { return ___Empty_5; }
	inline String_t** get_address_of_Empty_5() { return &___Empty_5; }
	inline void set_Empty_5(String_t* value)
	{
		___Empty_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___Empty_5), (void*)value);
	}
};


// System.ValueType
struct ValueType_tDBF999C1B75C48C68621878250DBF6CDBCF51E52  : public RuntimeObject
{
public:

public:
};

// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_tDBF999C1B75C48C68621878250DBF6CDBCF51E52_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_tDBF999C1B75C48C68621878250DBF6CDBCF51E52_marshaled_com
{
};

// UnityEngine.YieldInstruction
struct YieldInstruction_tB0B4E05316710E51ECCC1E57174C27FE6DEBBEAF  : public RuntimeObject
{
public:

public:
};

// Native definition for P/Invoke marshalling of UnityEngine.YieldInstruction
struct YieldInstruction_tB0B4E05316710E51ECCC1E57174C27FE6DEBBEAF_marshaled_pinvoke
{
};
// Native definition for COM marshalling of UnityEngine.YieldInstruction
struct YieldInstruction_tB0B4E05316710E51ECCC1E57174C27FE6DEBBEAF_marshaled_com
{
};

// CrazyMinnow.SALSA.RandomEyes2D/<BlinkEyes>d__2
struct U3CBlinkEyesU3Ed__2_t7DFD119FC328CD8363C654182C85B8B62BA190D1  : public RuntimeObject
{
public:
	// System.Object CrazyMinnow.SALSA.RandomEyes2D/<BlinkEyes>d__2::<>2__current
	RuntimeObject * ___U3CU3E2__current_0;
	// System.Int32 CrazyMinnow.SALSA.RandomEyes2D/<BlinkEyes>d__2::<>1__state
	int32_t ___U3CU3E1__state_1;
	// CrazyMinnow.SALSA.RandomEyes2D CrazyMinnow.SALSA.RandomEyes2D/<BlinkEyes>d__2::<>4__this
	RandomEyes2D_t253864BE689DE7E477335D17ECD2EC77001B377F * ___U3CU3E4__this_2;
	// System.Single CrazyMinnow.SALSA.RandomEyes2D/<BlinkEyes>d__2::duration
	float ___duration_3;

public:
	inline static int32_t get_offset_of_U3CU3E2__current_0() { return static_cast<int32_t>(offsetof(U3CBlinkEyesU3Ed__2_t7DFD119FC328CD8363C654182C85B8B62BA190D1, ___U3CU3E2__current_0)); }
	inline RuntimeObject * get_U3CU3E2__current_0() const { return ___U3CU3E2__current_0; }
	inline RuntimeObject ** get_address_of_U3CU3E2__current_0() { return &___U3CU3E2__current_0; }
	inline void set_U3CU3E2__current_0(RuntimeObject * value)
	{
		___U3CU3E2__current_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E2__current_0), (void*)value);
	}

	inline static int32_t get_offset_of_U3CU3E1__state_1() { return static_cast<int32_t>(offsetof(U3CBlinkEyesU3Ed__2_t7DFD119FC328CD8363C654182C85B8B62BA190D1, ___U3CU3E1__state_1)); }
	inline int32_t get_U3CU3E1__state_1() const { return ___U3CU3E1__state_1; }
	inline int32_t* get_address_of_U3CU3E1__state_1() { return &___U3CU3E1__state_1; }
	inline void set_U3CU3E1__state_1(int32_t value)
	{
		___U3CU3E1__state_1 = value;
	}

	inline static int32_t get_offset_of_U3CU3E4__this_2() { return static_cast<int32_t>(offsetof(U3CBlinkEyesU3Ed__2_t7DFD119FC328CD8363C654182C85B8B62BA190D1, ___U3CU3E4__this_2)); }
	inline RandomEyes2D_t253864BE689DE7E477335D17ECD2EC77001B377F * get_U3CU3E4__this_2() const { return ___U3CU3E4__this_2; }
	inline RandomEyes2D_t253864BE689DE7E477335D17ECD2EC77001B377F ** get_address_of_U3CU3E4__this_2() { return &___U3CU3E4__this_2; }
	inline void set_U3CU3E4__this_2(RandomEyes2D_t253864BE689DE7E477335D17ECD2EC77001B377F * value)
	{
		___U3CU3E4__this_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E4__this_2), (void*)value);
	}

	inline static int32_t get_offset_of_duration_3() { return static_cast<int32_t>(offsetof(U3CBlinkEyesU3Ed__2_t7DFD119FC328CD8363C654182C85B8B62BA190D1, ___duration_3)); }
	inline float get_duration_3() const { return ___duration_3; }
	inline float* get_address_of_duration_3() { return &___duration_3; }
	inline void set_duration_3(float value)
	{
		___duration_3 = value;
	}
};


// CrazyMinnow.SALSA.RandomEyes2D/<TargetAffinityUpdate>d__0
struct U3CTargetAffinityUpdateU3Ed__0_tB9D47FFD16FA8A3CF1860EC8ADB05A787654DF78  : public RuntimeObject
{
public:
	// System.Object CrazyMinnow.SALSA.RandomEyes2D/<TargetAffinityUpdate>d__0::<>2__current
	RuntimeObject * ___U3CU3E2__current_0;
	// System.Int32 CrazyMinnow.SALSA.RandomEyes2D/<TargetAffinityUpdate>d__0::<>1__state
	int32_t ___U3CU3E1__state_1;
	// CrazyMinnow.SALSA.RandomEyes2D CrazyMinnow.SALSA.RandomEyes2D/<TargetAffinityUpdate>d__0::<>4__this
	RandomEyes2D_t253864BE689DE7E477335D17ECD2EC77001B377F * ___U3CU3E4__this_2;

public:
	inline static int32_t get_offset_of_U3CU3E2__current_0() { return static_cast<int32_t>(offsetof(U3CTargetAffinityUpdateU3Ed__0_tB9D47FFD16FA8A3CF1860EC8ADB05A787654DF78, ___U3CU3E2__current_0)); }
	inline RuntimeObject * get_U3CU3E2__current_0() const { return ___U3CU3E2__current_0; }
	inline RuntimeObject ** get_address_of_U3CU3E2__current_0() { return &___U3CU3E2__current_0; }
	inline void set_U3CU3E2__current_0(RuntimeObject * value)
	{
		___U3CU3E2__current_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E2__current_0), (void*)value);
	}

	inline static int32_t get_offset_of_U3CU3E1__state_1() { return static_cast<int32_t>(offsetof(U3CTargetAffinityUpdateU3Ed__0_tB9D47FFD16FA8A3CF1860EC8ADB05A787654DF78, ___U3CU3E1__state_1)); }
	inline int32_t get_U3CU3E1__state_1() const { return ___U3CU3E1__state_1; }
	inline int32_t* get_address_of_U3CU3E1__state_1() { return &___U3CU3E1__state_1; }
	inline void set_U3CU3E1__state_1(int32_t value)
	{
		___U3CU3E1__state_1 = value;
	}

	inline static int32_t get_offset_of_U3CU3E4__this_2() { return static_cast<int32_t>(offsetof(U3CTargetAffinityUpdateU3Ed__0_tB9D47FFD16FA8A3CF1860EC8ADB05A787654DF78, ___U3CU3E4__this_2)); }
	inline RandomEyes2D_t253864BE689DE7E477335D17ECD2EC77001B377F * get_U3CU3E4__this_2() const { return ___U3CU3E4__this_2; }
	inline RandomEyes2D_t253864BE689DE7E477335D17ECD2EC77001B377F ** get_address_of_U3CU3E4__this_2() { return &___U3CU3E4__this_2; }
	inline void set_U3CU3E4__this_2(RandomEyes2D_t253864BE689DE7E477335D17ECD2EC77001B377F * value)
	{
		___U3CU3E4__this_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E4__this_2), (void*)value);
	}
};


// CrazyMinnow.SALSA.RandomEyes3D/<BlinkEyes>d__2
struct U3CBlinkEyesU3Ed__2_t05DA28F00D8E1A9E64D0728065DB53DA96232044  : public RuntimeObject
{
public:
	// System.Object CrazyMinnow.SALSA.RandomEyes3D/<BlinkEyes>d__2::<>2__current
	RuntimeObject * ___U3CU3E2__current_0;
	// System.Int32 CrazyMinnow.SALSA.RandomEyes3D/<BlinkEyes>d__2::<>1__state
	int32_t ___U3CU3E1__state_1;
	// CrazyMinnow.SALSA.RandomEyes3D CrazyMinnow.SALSA.RandomEyes3D/<BlinkEyes>d__2::<>4__this
	RandomEyes3D_t6868157C4605BD0225E1B08301F110E72336CE45 * ___U3CU3E4__this_2;
	// System.Single CrazyMinnow.SALSA.RandomEyes3D/<BlinkEyes>d__2::duration
	float ___duration_3;

public:
	inline static int32_t get_offset_of_U3CU3E2__current_0() { return static_cast<int32_t>(offsetof(U3CBlinkEyesU3Ed__2_t05DA28F00D8E1A9E64D0728065DB53DA96232044, ___U3CU3E2__current_0)); }
	inline RuntimeObject * get_U3CU3E2__current_0() const { return ___U3CU3E2__current_0; }
	inline RuntimeObject ** get_address_of_U3CU3E2__current_0() { return &___U3CU3E2__current_0; }
	inline void set_U3CU3E2__current_0(RuntimeObject * value)
	{
		___U3CU3E2__current_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E2__current_0), (void*)value);
	}

	inline static int32_t get_offset_of_U3CU3E1__state_1() { return static_cast<int32_t>(offsetof(U3CBlinkEyesU3Ed__2_t05DA28F00D8E1A9E64D0728065DB53DA96232044, ___U3CU3E1__state_1)); }
	inline int32_t get_U3CU3E1__state_1() const { return ___U3CU3E1__state_1; }
	inline int32_t* get_address_of_U3CU3E1__state_1() { return &___U3CU3E1__state_1; }
	inline void set_U3CU3E1__state_1(int32_t value)
	{
		___U3CU3E1__state_1 = value;
	}

	inline static int32_t get_offset_of_U3CU3E4__this_2() { return static_cast<int32_t>(offsetof(U3CBlinkEyesU3Ed__2_t05DA28F00D8E1A9E64D0728065DB53DA96232044, ___U3CU3E4__this_2)); }
	inline RandomEyes3D_t6868157C4605BD0225E1B08301F110E72336CE45 * get_U3CU3E4__this_2() const { return ___U3CU3E4__this_2; }
	inline RandomEyes3D_t6868157C4605BD0225E1B08301F110E72336CE45 ** get_address_of_U3CU3E4__this_2() { return &___U3CU3E4__this_2; }
	inline void set_U3CU3E4__this_2(RandomEyes3D_t6868157C4605BD0225E1B08301F110E72336CE45 * value)
	{
		___U3CU3E4__this_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E4__this_2), (void*)value);
	}

	inline static int32_t get_offset_of_duration_3() { return static_cast<int32_t>(offsetof(U3CBlinkEyesU3Ed__2_t05DA28F00D8E1A9E64D0728065DB53DA96232044, ___duration_3)); }
	inline float get_duration_3() const { return ___duration_3; }
	inline float* get_address_of_duration_3() { return &___duration_3; }
	inline void set_duration_3(float value)
	{
		___duration_3 = value;
	}
};


// CrazyMinnow.SALSA.RandomEyes3D/<DoBlink>d__c
struct U3CDoBlinkU3Ed__c_t8CF4EE9B667E494EE34FBB05C5E960320E5C8418  : public RuntimeObject
{
public:
	// System.Object CrazyMinnow.SALSA.RandomEyes3D/<DoBlink>d__c::<>2__current
	RuntimeObject * ___U3CU3E2__current_0;
	// System.Int32 CrazyMinnow.SALSA.RandomEyes3D/<DoBlink>d__c::<>1__state
	int32_t ___U3CU3E1__state_1;
	// CrazyMinnow.SALSA.RandomEyes3D CrazyMinnow.SALSA.RandomEyes3D/<DoBlink>d__c::<>4__this
	RandomEyes3D_t6868157C4605BD0225E1B08301F110E72336CE45 * ___U3CU3E4__this_2;
	// System.Single CrazyMinnow.SALSA.RandomEyes3D/<DoBlink>d__c::duration
	float ___duration_3;

public:
	inline static int32_t get_offset_of_U3CU3E2__current_0() { return static_cast<int32_t>(offsetof(U3CDoBlinkU3Ed__c_t8CF4EE9B667E494EE34FBB05C5E960320E5C8418, ___U3CU3E2__current_0)); }
	inline RuntimeObject * get_U3CU3E2__current_0() const { return ___U3CU3E2__current_0; }
	inline RuntimeObject ** get_address_of_U3CU3E2__current_0() { return &___U3CU3E2__current_0; }
	inline void set_U3CU3E2__current_0(RuntimeObject * value)
	{
		___U3CU3E2__current_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E2__current_0), (void*)value);
	}

	inline static int32_t get_offset_of_U3CU3E1__state_1() { return static_cast<int32_t>(offsetof(U3CDoBlinkU3Ed__c_t8CF4EE9B667E494EE34FBB05C5E960320E5C8418, ___U3CU3E1__state_1)); }
	inline int32_t get_U3CU3E1__state_1() const { return ___U3CU3E1__state_1; }
	inline int32_t* get_address_of_U3CU3E1__state_1() { return &___U3CU3E1__state_1; }
	inline void set_U3CU3E1__state_1(int32_t value)
	{
		___U3CU3E1__state_1 = value;
	}

	inline static int32_t get_offset_of_U3CU3E4__this_2() { return static_cast<int32_t>(offsetof(U3CDoBlinkU3Ed__c_t8CF4EE9B667E494EE34FBB05C5E960320E5C8418, ___U3CU3E4__this_2)); }
	inline RandomEyes3D_t6868157C4605BD0225E1B08301F110E72336CE45 * get_U3CU3E4__this_2() const { return ___U3CU3E4__this_2; }
	inline RandomEyes3D_t6868157C4605BD0225E1B08301F110E72336CE45 ** get_address_of_U3CU3E4__this_2() { return &___U3CU3E4__this_2; }
	inline void set_U3CU3E4__this_2(RandomEyes3D_t6868157C4605BD0225E1B08301F110E72336CE45 * value)
	{
		___U3CU3E4__this_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E4__this_2), (void*)value);
	}

	inline static int32_t get_offset_of_duration_3() { return static_cast<int32_t>(offsetof(U3CDoBlinkU3Ed__c_t8CF4EE9B667E494EE34FBB05C5E960320E5C8418, ___duration_3)); }
	inline float get_duration_3() const { return ___duration_3; }
	inline float* get_address_of_duration_3() { return &___duration_3; }
	inline void set_duration_3(float value)
	{
		___duration_3 = value;
	}
};


// CrazyMinnow.SALSA.RandomEyes3D/<SetCustomShapeDuration>d__8
struct U3CSetCustomShapeDurationU3Ed__8_t1CD4174D8597ACF6A20E2146BB78651E84AE273B  : public RuntimeObject
{
public:
	// System.Object CrazyMinnow.SALSA.RandomEyes3D/<SetCustomShapeDuration>d__8::<>2__current
	RuntimeObject * ___U3CU3E2__current_0;
	// System.Int32 CrazyMinnow.SALSA.RandomEyes3D/<SetCustomShapeDuration>d__8::<>1__state
	int32_t ___U3CU3E1__state_1;
	// CrazyMinnow.SALSA.RandomEyes3D CrazyMinnow.SALSA.RandomEyes3D/<SetCustomShapeDuration>d__8::<>4__this
	RandomEyes3D_t6868157C4605BD0225E1B08301F110E72336CE45 * ___U3CU3E4__this_2;
	// System.String CrazyMinnow.SALSA.RandomEyes3D/<SetCustomShapeDuration>d__8::shapeName
	String_t* ___shapeName_3;
	// System.Single CrazyMinnow.SALSA.RandomEyes3D/<SetCustomShapeDuration>d__8::duration
	float ___duration_4;

public:
	inline static int32_t get_offset_of_U3CU3E2__current_0() { return static_cast<int32_t>(offsetof(U3CSetCustomShapeDurationU3Ed__8_t1CD4174D8597ACF6A20E2146BB78651E84AE273B, ___U3CU3E2__current_0)); }
	inline RuntimeObject * get_U3CU3E2__current_0() const { return ___U3CU3E2__current_0; }
	inline RuntimeObject ** get_address_of_U3CU3E2__current_0() { return &___U3CU3E2__current_0; }
	inline void set_U3CU3E2__current_0(RuntimeObject * value)
	{
		___U3CU3E2__current_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E2__current_0), (void*)value);
	}

	inline static int32_t get_offset_of_U3CU3E1__state_1() { return static_cast<int32_t>(offsetof(U3CSetCustomShapeDurationU3Ed__8_t1CD4174D8597ACF6A20E2146BB78651E84AE273B, ___U3CU3E1__state_1)); }
	inline int32_t get_U3CU3E1__state_1() const { return ___U3CU3E1__state_1; }
	inline int32_t* get_address_of_U3CU3E1__state_1() { return &___U3CU3E1__state_1; }
	inline void set_U3CU3E1__state_1(int32_t value)
	{
		___U3CU3E1__state_1 = value;
	}

	inline static int32_t get_offset_of_U3CU3E4__this_2() { return static_cast<int32_t>(offsetof(U3CSetCustomShapeDurationU3Ed__8_t1CD4174D8597ACF6A20E2146BB78651E84AE273B, ___U3CU3E4__this_2)); }
	inline RandomEyes3D_t6868157C4605BD0225E1B08301F110E72336CE45 * get_U3CU3E4__this_2() const { return ___U3CU3E4__this_2; }
	inline RandomEyes3D_t6868157C4605BD0225E1B08301F110E72336CE45 ** get_address_of_U3CU3E4__this_2() { return &___U3CU3E4__this_2; }
	inline void set_U3CU3E4__this_2(RandomEyes3D_t6868157C4605BD0225E1B08301F110E72336CE45 * value)
	{
		___U3CU3E4__this_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E4__this_2), (void*)value);
	}

	inline static int32_t get_offset_of_shapeName_3() { return static_cast<int32_t>(offsetof(U3CSetCustomShapeDurationU3Ed__8_t1CD4174D8597ACF6A20E2146BB78651E84AE273B, ___shapeName_3)); }
	inline String_t* get_shapeName_3() const { return ___shapeName_3; }
	inline String_t** get_address_of_shapeName_3() { return &___shapeName_3; }
	inline void set_shapeName_3(String_t* value)
	{
		___shapeName_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___shapeName_3), (void*)value);
	}

	inline static int32_t get_offset_of_duration_4() { return static_cast<int32_t>(offsetof(U3CSetCustomShapeDurationU3Ed__8_t1CD4174D8597ACF6A20E2146BB78651E84AE273B, ___duration_4)); }
	inline float get_duration_4() const { return ___duration_4; }
	inline float* get_address_of_duration_4() { return &___duration_4; }
	inline void set_duration_4(float value)
	{
		___duration_4 = value;
	}
};


// CrazyMinnow.SALSA.RandomEyes3D/<SetCustomShapeDuration>d__a
struct U3CSetCustomShapeDurationU3Ed__a_tF3A147C5E712D0581C2B12133986348BFBB67140  : public RuntimeObject
{
public:
	// System.Object CrazyMinnow.SALSA.RandomEyes3D/<SetCustomShapeDuration>d__a::<>2__current
	RuntimeObject * ___U3CU3E2__current_0;
	// System.Int32 CrazyMinnow.SALSA.RandomEyes3D/<SetCustomShapeDuration>d__a::<>1__state
	int32_t ___U3CU3E1__state_1;
	// CrazyMinnow.SALSA.RandomEyes3D CrazyMinnow.SALSA.RandomEyes3D/<SetCustomShapeDuration>d__a::<>4__this
	RandomEyes3D_t6868157C4605BD0225E1B08301F110E72336CE45 * ___U3CU3E4__this_2;
	// System.Int32 CrazyMinnow.SALSA.RandomEyes3D/<SetCustomShapeDuration>d__a::shapeIndex
	int32_t ___shapeIndex_3;
	// System.Single CrazyMinnow.SALSA.RandomEyes3D/<SetCustomShapeDuration>d__a::duration
	float ___duration_4;

public:
	inline static int32_t get_offset_of_U3CU3E2__current_0() { return static_cast<int32_t>(offsetof(U3CSetCustomShapeDurationU3Ed__a_tF3A147C5E712D0581C2B12133986348BFBB67140, ___U3CU3E2__current_0)); }
	inline RuntimeObject * get_U3CU3E2__current_0() const { return ___U3CU3E2__current_0; }
	inline RuntimeObject ** get_address_of_U3CU3E2__current_0() { return &___U3CU3E2__current_0; }
	inline void set_U3CU3E2__current_0(RuntimeObject * value)
	{
		___U3CU3E2__current_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E2__current_0), (void*)value);
	}

	inline static int32_t get_offset_of_U3CU3E1__state_1() { return static_cast<int32_t>(offsetof(U3CSetCustomShapeDurationU3Ed__a_tF3A147C5E712D0581C2B12133986348BFBB67140, ___U3CU3E1__state_1)); }
	inline int32_t get_U3CU3E1__state_1() const { return ___U3CU3E1__state_1; }
	inline int32_t* get_address_of_U3CU3E1__state_1() { return &___U3CU3E1__state_1; }
	inline void set_U3CU3E1__state_1(int32_t value)
	{
		___U3CU3E1__state_1 = value;
	}

	inline static int32_t get_offset_of_U3CU3E4__this_2() { return static_cast<int32_t>(offsetof(U3CSetCustomShapeDurationU3Ed__a_tF3A147C5E712D0581C2B12133986348BFBB67140, ___U3CU3E4__this_2)); }
	inline RandomEyes3D_t6868157C4605BD0225E1B08301F110E72336CE45 * get_U3CU3E4__this_2() const { return ___U3CU3E4__this_2; }
	inline RandomEyes3D_t6868157C4605BD0225E1B08301F110E72336CE45 ** get_address_of_U3CU3E4__this_2() { return &___U3CU3E4__this_2; }
	inline void set_U3CU3E4__this_2(RandomEyes3D_t6868157C4605BD0225E1B08301F110E72336CE45 * value)
	{
		___U3CU3E4__this_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E4__this_2), (void*)value);
	}

	inline static int32_t get_offset_of_shapeIndex_3() { return static_cast<int32_t>(offsetof(U3CSetCustomShapeDurationU3Ed__a_tF3A147C5E712D0581C2B12133986348BFBB67140, ___shapeIndex_3)); }
	inline int32_t get_shapeIndex_3() const { return ___shapeIndex_3; }
	inline int32_t* get_address_of_shapeIndex_3() { return &___shapeIndex_3; }
	inline void set_shapeIndex_3(int32_t value)
	{
		___shapeIndex_3 = value;
	}

	inline static int32_t get_offset_of_duration_4() { return static_cast<int32_t>(offsetof(U3CSetCustomShapeDurationU3Ed__a_tF3A147C5E712D0581C2B12133986348BFBB67140, ___duration_4)); }
	inline float get_duration_4() const { return ___duration_4; }
	inline float* get_address_of_duration_4() { return &___duration_4; }
	inline void set_duration_4(float value)
	{
		___duration_4 = value;
	}
};


// CrazyMinnow.SALSA.RandomEyes3D/<SetCustomShapeOverrideDuration>d__4
struct U3CSetCustomShapeOverrideDurationU3Ed__4_tA42191F3DEC3EBAD53A277EAC5C346C81A3BC122  : public RuntimeObject
{
public:
	// System.Object CrazyMinnow.SALSA.RandomEyes3D/<SetCustomShapeOverrideDuration>d__4::<>2__current
	RuntimeObject * ___U3CU3E2__current_0;
	// System.Int32 CrazyMinnow.SALSA.RandomEyes3D/<SetCustomShapeOverrideDuration>d__4::<>1__state
	int32_t ___U3CU3E1__state_1;
	// CrazyMinnow.SALSA.RandomEyes3D CrazyMinnow.SALSA.RandomEyes3D/<SetCustomShapeOverrideDuration>d__4::<>4__this
	RandomEyes3D_t6868157C4605BD0225E1B08301F110E72336CE45 * ___U3CU3E4__this_2;
	// System.String CrazyMinnow.SALSA.RandomEyes3D/<SetCustomShapeOverrideDuration>d__4::shapeName
	String_t* ___shapeName_3;
	// System.Single CrazyMinnow.SALSA.RandomEyes3D/<SetCustomShapeOverrideDuration>d__4::duration
	float ___duration_4;

public:
	inline static int32_t get_offset_of_U3CU3E2__current_0() { return static_cast<int32_t>(offsetof(U3CSetCustomShapeOverrideDurationU3Ed__4_tA42191F3DEC3EBAD53A277EAC5C346C81A3BC122, ___U3CU3E2__current_0)); }
	inline RuntimeObject * get_U3CU3E2__current_0() const { return ___U3CU3E2__current_0; }
	inline RuntimeObject ** get_address_of_U3CU3E2__current_0() { return &___U3CU3E2__current_0; }
	inline void set_U3CU3E2__current_0(RuntimeObject * value)
	{
		___U3CU3E2__current_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E2__current_0), (void*)value);
	}

	inline static int32_t get_offset_of_U3CU3E1__state_1() { return static_cast<int32_t>(offsetof(U3CSetCustomShapeOverrideDurationU3Ed__4_tA42191F3DEC3EBAD53A277EAC5C346C81A3BC122, ___U3CU3E1__state_1)); }
	inline int32_t get_U3CU3E1__state_1() const { return ___U3CU3E1__state_1; }
	inline int32_t* get_address_of_U3CU3E1__state_1() { return &___U3CU3E1__state_1; }
	inline void set_U3CU3E1__state_1(int32_t value)
	{
		___U3CU3E1__state_1 = value;
	}

	inline static int32_t get_offset_of_U3CU3E4__this_2() { return static_cast<int32_t>(offsetof(U3CSetCustomShapeOverrideDurationU3Ed__4_tA42191F3DEC3EBAD53A277EAC5C346C81A3BC122, ___U3CU3E4__this_2)); }
	inline RandomEyes3D_t6868157C4605BD0225E1B08301F110E72336CE45 * get_U3CU3E4__this_2() const { return ___U3CU3E4__this_2; }
	inline RandomEyes3D_t6868157C4605BD0225E1B08301F110E72336CE45 ** get_address_of_U3CU3E4__this_2() { return &___U3CU3E4__this_2; }
	inline void set_U3CU3E4__this_2(RandomEyes3D_t6868157C4605BD0225E1B08301F110E72336CE45 * value)
	{
		___U3CU3E4__this_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E4__this_2), (void*)value);
	}

	inline static int32_t get_offset_of_shapeName_3() { return static_cast<int32_t>(offsetof(U3CSetCustomShapeOverrideDurationU3Ed__4_tA42191F3DEC3EBAD53A277EAC5C346C81A3BC122, ___shapeName_3)); }
	inline String_t* get_shapeName_3() const { return ___shapeName_3; }
	inline String_t** get_address_of_shapeName_3() { return &___shapeName_3; }
	inline void set_shapeName_3(String_t* value)
	{
		___shapeName_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___shapeName_3), (void*)value);
	}

	inline static int32_t get_offset_of_duration_4() { return static_cast<int32_t>(offsetof(U3CSetCustomShapeOverrideDurationU3Ed__4_tA42191F3DEC3EBAD53A277EAC5C346C81A3BC122, ___duration_4)); }
	inline float get_duration_4() const { return ___duration_4; }
	inline float* get_address_of_duration_4() { return &___duration_4; }
	inline void set_duration_4(float value)
	{
		___duration_4 = value;
	}
};


// CrazyMinnow.SALSA.RandomEyes3D/<SetCustomShapeOverrideDuration>d__6
struct U3CSetCustomShapeOverrideDurationU3Ed__6_t3067670AF5E03DD7899B8F7492C6EF59E5B77DB0  : public RuntimeObject
{
public:
	// System.Object CrazyMinnow.SALSA.RandomEyes3D/<SetCustomShapeOverrideDuration>d__6::<>2__current
	RuntimeObject * ___U3CU3E2__current_0;
	// System.Int32 CrazyMinnow.SALSA.RandomEyes3D/<SetCustomShapeOverrideDuration>d__6::<>1__state
	int32_t ___U3CU3E1__state_1;
	// CrazyMinnow.SALSA.RandomEyes3D CrazyMinnow.SALSA.RandomEyes3D/<SetCustomShapeOverrideDuration>d__6::<>4__this
	RandomEyes3D_t6868157C4605BD0225E1B08301F110E72336CE45 * ___U3CU3E4__this_2;
	// System.Int32 CrazyMinnow.SALSA.RandomEyes3D/<SetCustomShapeOverrideDuration>d__6::shapeIndex
	int32_t ___shapeIndex_3;
	// System.Single CrazyMinnow.SALSA.RandomEyes3D/<SetCustomShapeOverrideDuration>d__6::duration
	float ___duration_4;

public:
	inline static int32_t get_offset_of_U3CU3E2__current_0() { return static_cast<int32_t>(offsetof(U3CSetCustomShapeOverrideDurationU3Ed__6_t3067670AF5E03DD7899B8F7492C6EF59E5B77DB0, ___U3CU3E2__current_0)); }
	inline RuntimeObject * get_U3CU3E2__current_0() const { return ___U3CU3E2__current_0; }
	inline RuntimeObject ** get_address_of_U3CU3E2__current_0() { return &___U3CU3E2__current_0; }
	inline void set_U3CU3E2__current_0(RuntimeObject * value)
	{
		___U3CU3E2__current_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E2__current_0), (void*)value);
	}

	inline static int32_t get_offset_of_U3CU3E1__state_1() { return static_cast<int32_t>(offsetof(U3CSetCustomShapeOverrideDurationU3Ed__6_t3067670AF5E03DD7899B8F7492C6EF59E5B77DB0, ___U3CU3E1__state_1)); }
	inline int32_t get_U3CU3E1__state_1() const { return ___U3CU3E1__state_1; }
	inline int32_t* get_address_of_U3CU3E1__state_1() { return &___U3CU3E1__state_1; }
	inline void set_U3CU3E1__state_1(int32_t value)
	{
		___U3CU3E1__state_1 = value;
	}

	inline static int32_t get_offset_of_U3CU3E4__this_2() { return static_cast<int32_t>(offsetof(U3CSetCustomShapeOverrideDurationU3Ed__6_t3067670AF5E03DD7899B8F7492C6EF59E5B77DB0, ___U3CU3E4__this_2)); }
	inline RandomEyes3D_t6868157C4605BD0225E1B08301F110E72336CE45 * get_U3CU3E4__this_2() const { return ___U3CU3E4__this_2; }
	inline RandomEyes3D_t6868157C4605BD0225E1B08301F110E72336CE45 ** get_address_of_U3CU3E4__this_2() { return &___U3CU3E4__this_2; }
	inline void set_U3CU3E4__this_2(RandomEyes3D_t6868157C4605BD0225E1B08301F110E72336CE45 * value)
	{
		___U3CU3E4__this_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E4__this_2), (void*)value);
	}

	inline static int32_t get_offset_of_shapeIndex_3() { return static_cast<int32_t>(offsetof(U3CSetCustomShapeOverrideDurationU3Ed__6_t3067670AF5E03DD7899B8F7492C6EF59E5B77DB0, ___shapeIndex_3)); }
	inline int32_t get_shapeIndex_3() const { return ___shapeIndex_3; }
	inline int32_t* get_address_of_shapeIndex_3() { return &___shapeIndex_3; }
	inline void set_shapeIndex_3(int32_t value)
	{
		___shapeIndex_3 = value;
	}

	inline static int32_t get_offset_of_duration_4() { return static_cast<int32_t>(offsetof(U3CSetCustomShapeOverrideDurationU3Ed__6_t3067670AF5E03DD7899B8F7492C6EF59E5B77DB0, ___duration_4)); }
	inline float get_duration_4() const { return ___duration_4; }
	inline float* get_address_of_duration_4() { return &___duration_4; }
	inline void set_duration_4(float value)
	{
		___duration_4 = value;
	}
};


// CrazyMinnow.SALSA.RandomEyes3D/<TargetAffinityUpdate>d__0
struct U3CTargetAffinityUpdateU3Ed__0_tCF892F05CB3FC0D3FC4F3984F53D8C429D011013  : public RuntimeObject
{
public:
	// System.Object CrazyMinnow.SALSA.RandomEyes3D/<TargetAffinityUpdate>d__0::<>2__current
	RuntimeObject * ___U3CU3E2__current_0;
	// System.Int32 CrazyMinnow.SALSA.RandomEyes3D/<TargetAffinityUpdate>d__0::<>1__state
	int32_t ___U3CU3E1__state_1;
	// CrazyMinnow.SALSA.RandomEyes3D CrazyMinnow.SALSA.RandomEyes3D/<TargetAffinityUpdate>d__0::<>4__this
	RandomEyes3D_t6868157C4605BD0225E1B08301F110E72336CE45 * ___U3CU3E4__this_2;

public:
	inline static int32_t get_offset_of_U3CU3E2__current_0() { return static_cast<int32_t>(offsetof(U3CTargetAffinityUpdateU3Ed__0_tCF892F05CB3FC0D3FC4F3984F53D8C429D011013, ___U3CU3E2__current_0)); }
	inline RuntimeObject * get_U3CU3E2__current_0() const { return ___U3CU3E2__current_0; }
	inline RuntimeObject ** get_address_of_U3CU3E2__current_0() { return &___U3CU3E2__current_0; }
	inline void set_U3CU3E2__current_0(RuntimeObject * value)
	{
		___U3CU3E2__current_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E2__current_0), (void*)value);
	}

	inline static int32_t get_offset_of_U3CU3E1__state_1() { return static_cast<int32_t>(offsetof(U3CTargetAffinityUpdateU3Ed__0_tCF892F05CB3FC0D3FC4F3984F53D8C429D011013, ___U3CU3E1__state_1)); }
	inline int32_t get_U3CU3E1__state_1() const { return ___U3CU3E1__state_1; }
	inline int32_t* get_address_of_U3CU3E1__state_1() { return &___U3CU3E1__state_1; }
	inline void set_U3CU3E1__state_1(int32_t value)
	{
		___U3CU3E1__state_1 = value;
	}

	inline static int32_t get_offset_of_U3CU3E4__this_2() { return static_cast<int32_t>(offsetof(U3CTargetAffinityUpdateU3Ed__0_tCF892F05CB3FC0D3FC4F3984F53D8C429D011013, ___U3CU3E4__this_2)); }
	inline RandomEyes3D_t6868157C4605BD0225E1B08301F110E72336CE45 * get_U3CU3E4__this_2() const { return ___U3CU3E4__this_2; }
	inline RandomEyes3D_t6868157C4605BD0225E1B08301F110E72336CE45 ** get_address_of_U3CU3E4__this_2() { return &___U3CU3E4__this_2; }
	inline void set_U3CU3E4__this_2(RandomEyes3D_t6868157C4605BD0225E1B08301F110E72336CE45 * value)
	{
		___U3CU3E4__this_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E4__this_2), (void*)value);
	}
};


// CrazyMinnow.SALSA.Salsa2D/<UpdateSample>d__0
struct U3CUpdateSampleU3Ed__0_t610463941DB34736C5EE20E7CA02876D017BE95F  : public RuntimeObject
{
public:
	// System.Object CrazyMinnow.SALSA.Salsa2D/<UpdateSample>d__0::<>2__current
	RuntimeObject * ___U3CU3E2__current_0;
	// System.Int32 CrazyMinnow.SALSA.Salsa2D/<UpdateSample>d__0::<>1__state
	int32_t ___U3CU3E1__state_1;
	// CrazyMinnow.SALSA.Salsa2D CrazyMinnow.SALSA.Salsa2D/<UpdateSample>d__0::<>4__this
	Salsa2D_t54C8B8FFE3B8149C565BD2A69D53629E65CFFB62 * ___U3CU3E4__this_2;
	// System.Single CrazyMinnow.SALSA.Salsa2D/<UpdateSample>d__0::<addedVals>5__1
	float ___U3CaddedValsU3E5__1_3;

public:
	inline static int32_t get_offset_of_U3CU3E2__current_0() { return static_cast<int32_t>(offsetof(U3CUpdateSampleU3Ed__0_t610463941DB34736C5EE20E7CA02876D017BE95F, ___U3CU3E2__current_0)); }
	inline RuntimeObject * get_U3CU3E2__current_0() const { return ___U3CU3E2__current_0; }
	inline RuntimeObject ** get_address_of_U3CU3E2__current_0() { return &___U3CU3E2__current_0; }
	inline void set_U3CU3E2__current_0(RuntimeObject * value)
	{
		___U3CU3E2__current_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E2__current_0), (void*)value);
	}

	inline static int32_t get_offset_of_U3CU3E1__state_1() { return static_cast<int32_t>(offsetof(U3CUpdateSampleU3Ed__0_t610463941DB34736C5EE20E7CA02876D017BE95F, ___U3CU3E1__state_1)); }
	inline int32_t get_U3CU3E1__state_1() const { return ___U3CU3E1__state_1; }
	inline int32_t* get_address_of_U3CU3E1__state_1() { return &___U3CU3E1__state_1; }
	inline void set_U3CU3E1__state_1(int32_t value)
	{
		___U3CU3E1__state_1 = value;
	}

	inline static int32_t get_offset_of_U3CU3E4__this_2() { return static_cast<int32_t>(offsetof(U3CUpdateSampleU3Ed__0_t610463941DB34736C5EE20E7CA02876D017BE95F, ___U3CU3E4__this_2)); }
	inline Salsa2D_t54C8B8FFE3B8149C565BD2A69D53629E65CFFB62 * get_U3CU3E4__this_2() const { return ___U3CU3E4__this_2; }
	inline Salsa2D_t54C8B8FFE3B8149C565BD2A69D53629E65CFFB62 ** get_address_of_U3CU3E4__this_2() { return &___U3CU3E4__this_2; }
	inline void set_U3CU3E4__this_2(Salsa2D_t54C8B8FFE3B8149C565BD2A69D53629E65CFFB62 * value)
	{
		___U3CU3E4__this_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E4__this_2), (void*)value);
	}

	inline static int32_t get_offset_of_U3CaddedValsU3E5__1_3() { return static_cast<int32_t>(offsetof(U3CUpdateSampleU3Ed__0_t610463941DB34736C5EE20E7CA02876D017BE95F, ___U3CaddedValsU3E5__1_3)); }
	inline float get_U3CaddedValsU3E5__1_3() const { return ___U3CaddedValsU3E5__1_3; }
	inline float* get_address_of_U3CaddedValsU3E5__1_3() { return &___U3CaddedValsU3E5__1_3; }
	inline void set_U3CaddedValsU3E5__1_3(float value)
	{
		___U3CaddedValsU3E5__1_3 = value;
	}
};


// CrazyMinnow.SALSA.Salsa3D/<UpdateSample>d__0
struct U3CUpdateSampleU3Ed__0_tB4DEFA6499D020E6EC2C7109DBED4C19296282D3  : public RuntimeObject
{
public:
	// System.Object CrazyMinnow.SALSA.Salsa3D/<UpdateSample>d__0::<>2__current
	RuntimeObject * ___U3CU3E2__current_0;
	// System.Int32 CrazyMinnow.SALSA.Salsa3D/<UpdateSample>d__0::<>1__state
	int32_t ___U3CU3E1__state_1;
	// CrazyMinnow.SALSA.Salsa3D CrazyMinnow.SALSA.Salsa3D/<UpdateSample>d__0::<>4__this
	Salsa3D_tACF83D529139263662FEAAC8F5B53EC43B140C9C * ___U3CU3E4__this_2;
	// System.Single CrazyMinnow.SALSA.Salsa3D/<UpdateSample>d__0::<addedVals>5__1
	float ___U3CaddedValsU3E5__1_3;

public:
	inline static int32_t get_offset_of_U3CU3E2__current_0() { return static_cast<int32_t>(offsetof(U3CUpdateSampleU3Ed__0_tB4DEFA6499D020E6EC2C7109DBED4C19296282D3, ___U3CU3E2__current_0)); }
	inline RuntimeObject * get_U3CU3E2__current_0() const { return ___U3CU3E2__current_0; }
	inline RuntimeObject ** get_address_of_U3CU3E2__current_0() { return &___U3CU3E2__current_0; }
	inline void set_U3CU3E2__current_0(RuntimeObject * value)
	{
		___U3CU3E2__current_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E2__current_0), (void*)value);
	}

	inline static int32_t get_offset_of_U3CU3E1__state_1() { return static_cast<int32_t>(offsetof(U3CUpdateSampleU3Ed__0_tB4DEFA6499D020E6EC2C7109DBED4C19296282D3, ___U3CU3E1__state_1)); }
	inline int32_t get_U3CU3E1__state_1() const { return ___U3CU3E1__state_1; }
	inline int32_t* get_address_of_U3CU3E1__state_1() { return &___U3CU3E1__state_1; }
	inline void set_U3CU3E1__state_1(int32_t value)
	{
		___U3CU3E1__state_1 = value;
	}

	inline static int32_t get_offset_of_U3CU3E4__this_2() { return static_cast<int32_t>(offsetof(U3CUpdateSampleU3Ed__0_tB4DEFA6499D020E6EC2C7109DBED4C19296282D3, ___U3CU3E4__this_2)); }
	inline Salsa3D_tACF83D529139263662FEAAC8F5B53EC43B140C9C * get_U3CU3E4__this_2() const { return ___U3CU3E4__this_2; }
	inline Salsa3D_tACF83D529139263662FEAAC8F5B53EC43B140C9C ** get_address_of_U3CU3E4__this_2() { return &___U3CU3E4__this_2; }
	inline void set_U3CU3E4__this_2(Salsa3D_tACF83D529139263662FEAAC8F5B53EC43B140C9C * value)
	{
		___U3CU3E4__this_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E4__this_2), (void*)value);
	}

	inline static int32_t get_offset_of_U3CaddedValsU3E5__1_3() { return static_cast<int32_t>(offsetof(U3CUpdateSampleU3Ed__0_tB4DEFA6499D020E6EC2C7109DBED4C19296282D3, ___U3CaddedValsU3E5__1_3)); }
	inline float get_U3CaddedValsU3E5__1_3() const { return ___U3CaddedValsU3E5__1_3; }
	inline float* get_address_of_U3CaddedValsU3E5__1_3() { return &___U3CaddedValsU3E5__1_3; }
	inline void set_U3CaddedValsU3E5__1_3(float value)
	{
		___U3CaddedValsU3E5__1_3 = value;
	}
};


// System.Boolean
struct Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37 
{
public:
	// System.Boolean System.Boolean::m_value
	bool ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37, ___m_value_0)); }
	inline bool get_m_value_0() const { return ___m_value_0; }
	inline bool* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(bool value)
	{
		___m_value_0 = value;
	}
};

struct Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37_StaticFields
{
public:
	// System.String System.Boolean::TrueString
	String_t* ___TrueString_5;
	// System.String System.Boolean::FalseString
	String_t* ___FalseString_6;

public:
	inline static int32_t get_offset_of_TrueString_5() { return static_cast<int32_t>(offsetof(Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37_StaticFields, ___TrueString_5)); }
	inline String_t* get_TrueString_5() const { return ___TrueString_5; }
	inline String_t** get_address_of_TrueString_5() { return &___TrueString_5; }
	inline void set_TrueString_5(String_t* value)
	{
		___TrueString_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___TrueString_5), (void*)value);
	}

	inline static int32_t get_offset_of_FalseString_6() { return static_cast<int32_t>(offsetof(Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37_StaticFields, ___FalseString_6)); }
	inline String_t* get_FalseString_6() const { return ___FalseString_6; }
	inline String_t** get_address_of_FalseString_6() { return &___FalseString_6; }
	inline void set_FalseString_6(String_t* value)
	{
		___FalseString_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___FalseString_6), (void*)value);
	}
};


// System.Enum
struct Enum_t23B90B40F60E677A8025267341651C94AE079CDA  : public ValueType_tDBF999C1B75C48C68621878250DBF6CDBCF51E52
{
public:

public:
};

struct Enum_t23B90B40F60E677A8025267341651C94AE079CDA_StaticFields
{
public:
	// System.Char[] System.Enum::enumSeperatorCharArray
	CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34* ___enumSeperatorCharArray_0;

public:
	inline static int32_t get_offset_of_enumSeperatorCharArray_0() { return static_cast<int32_t>(offsetof(Enum_t23B90B40F60E677A8025267341651C94AE079CDA_StaticFields, ___enumSeperatorCharArray_0)); }
	inline CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34* get_enumSeperatorCharArray_0() const { return ___enumSeperatorCharArray_0; }
	inline CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34** get_address_of_enumSeperatorCharArray_0() { return &___enumSeperatorCharArray_0; }
	inline void set_enumSeperatorCharArray_0(CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34* value)
	{
		___enumSeperatorCharArray_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___enumSeperatorCharArray_0), (void*)value);
	}
};

// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t23B90B40F60E677A8025267341651C94AE079CDA_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t23B90B40F60E677A8025267341651C94AE079CDA_marshaled_com
{
};

// System.Int32
struct Int32_tFDE5F8CD43D10453F6A2E0C77FE48C6CC7009046 
{
public:
	// System.Int32 System.Int32::m_value
	int32_t ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Int32_tFDE5F8CD43D10453F6A2E0C77FE48C6CC7009046, ___m_value_0)); }
	inline int32_t get_m_value_0() const { return ___m_value_0; }
	inline int32_t* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(int32_t value)
	{
		___m_value_0 = value;
	}
};


// System.IntPtr
struct IntPtr_t 
{
public:
	// System.Void* System.IntPtr::m_value
	void* ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(IntPtr_t, ___m_value_0)); }
	inline void* get_m_value_0() const { return ___m_value_0; }
	inline void** get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(void* value)
	{
		___m_value_0 = value;
	}
};

struct IntPtr_t_StaticFields
{
public:
	// System.IntPtr System.IntPtr::Zero
	intptr_t ___Zero_1;

public:
	inline static int32_t get_offset_of_Zero_1() { return static_cast<int32_t>(offsetof(IntPtr_t_StaticFields, ___Zero_1)); }
	inline intptr_t get_Zero_1() const { return ___Zero_1; }
	inline intptr_t* get_address_of_Zero_1() { return &___Zero_1; }
	inline void set_Zero_1(intptr_t value)
	{
		___Zero_1 = value;
	}
};


// CrazyMinnow.SALSA.RandomEyesCustomShapeStatus
struct RandomEyesCustomShapeStatus_tAE795B1C1EF38DFDE0535D6A919E25A96283DD66  : public RandomEyesCustomShape_t10BCA0CEC18C2BBF6A8BD7BFAD2AF005D233C354
{
public:
	// UnityEngine.Object CrazyMinnow.SALSA.RandomEyesCustomShapeStatus::<instance>k__BackingField
	Object_tF2F3778131EFF286AF62B7B013A170F95A91571A * ___U3CinstanceU3Ek__BackingField_8;

public:
	inline static int32_t get_offset_of_U3CinstanceU3Ek__BackingField_8() { return static_cast<int32_t>(offsetof(RandomEyesCustomShapeStatus_tAE795B1C1EF38DFDE0535D6A919E25A96283DD66, ___U3CinstanceU3Ek__BackingField_8)); }
	inline Object_tF2F3778131EFF286AF62B7B013A170F95A91571A * get_U3CinstanceU3Ek__BackingField_8() const { return ___U3CinstanceU3Ek__BackingField_8; }
	inline Object_tF2F3778131EFF286AF62B7B013A170F95A91571A ** get_address_of_U3CinstanceU3Ek__BackingField_8() { return &___U3CinstanceU3Ek__BackingField_8; }
	inline void set_U3CinstanceU3Ek__BackingField_8(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A * value)
	{
		___U3CinstanceU3Ek__BackingField_8 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CinstanceU3Ek__BackingField_8), (void*)value);
	}
};


// System.Single
struct Single_tE07797BA3C98D4CA9B5A19413C19A76688AB899E 
{
public:
	// System.Single System.Single::m_value
	float ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Single_tE07797BA3C98D4CA9B5A19413C19A76688AB899E, ___m_value_0)); }
	inline float get_m_value_0() const { return ___m_value_0; }
	inline float* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(float value)
	{
		___m_value_0 = value;
	}
};


// UnityEngine.Vector2
struct Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 
{
public:
	// System.Single UnityEngine.Vector2::x
	float ___x_0;
	// System.Single UnityEngine.Vector2::y
	float ___y_1;

public:
	inline static int32_t get_offset_of_x_0() { return static_cast<int32_t>(offsetof(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9, ___x_0)); }
	inline float get_x_0() const { return ___x_0; }
	inline float* get_address_of_x_0() { return &___x_0; }
	inline void set_x_0(float value)
	{
		___x_0 = value;
	}

	inline static int32_t get_offset_of_y_1() { return static_cast<int32_t>(offsetof(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9, ___y_1)); }
	inline float get_y_1() const { return ___y_1; }
	inline float* get_address_of_y_1() { return &___y_1; }
	inline void set_y_1(float value)
	{
		___y_1 = value;
	}
};

struct Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9_StaticFields
{
public:
	// UnityEngine.Vector2 UnityEngine.Vector2::zeroVector
	Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ___zeroVector_2;
	// UnityEngine.Vector2 UnityEngine.Vector2::oneVector
	Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ___oneVector_3;
	// UnityEngine.Vector2 UnityEngine.Vector2::upVector
	Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ___upVector_4;
	// UnityEngine.Vector2 UnityEngine.Vector2::downVector
	Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ___downVector_5;
	// UnityEngine.Vector2 UnityEngine.Vector2::leftVector
	Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ___leftVector_6;
	// UnityEngine.Vector2 UnityEngine.Vector2::rightVector
	Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ___rightVector_7;
	// UnityEngine.Vector2 UnityEngine.Vector2::positiveInfinityVector
	Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ___positiveInfinityVector_8;
	// UnityEngine.Vector2 UnityEngine.Vector2::negativeInfinityVector
	Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ___negativeInfinityVector_9;

public:
	inline static int32_t get_offset_of_zeroVector_2() { return static_cast<int32_t>(offsetof(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9_StaticFields, ___zeroVector_2)); }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  get_zeroVector_2() const { return ___zeroVector_2; }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 * get_address_of_zeroVector_2() { return &___zeroVector_2; }
	inline void set_zeroVector_2(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  value)
	{
		___zeroVector_2 = value;
	}

	inline static int32_t get_offset_of_oneVector_3() { return static_cast<int32_t>(offsetof(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9_StaticFields, ___oneVector_3)); }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  get_oneVector_3() const { return ___oneVector_3; }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 * get_address_of_oneVector_3() { return &___oneVector_3; }
	inline void set_oneVector_3(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  value)
	{
		___oneVector_3 = value;
	}

	inline static int32_t get_offset_of_upVector_4() { return static_cast<int32_t>(offsetof(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9_StaticFields, ___upVector_4)); }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  get_upVector_4() const { return ___upVector_4; }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 * get_address_of_upVector_4() { return &___upVector_4; }
	inline void set_upVector_4(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  value)
	{
		___upVector_4 = value;
	}

	inline static int32_t get_offset_of_downVector_5() { return static_cast<int32_t>(offsetof(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9_StaticFields, ___downVector_5)); }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  get_downVector_5() const { return ___downVector_5; }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 * get_address_of_downVector_5() { return &___downVector_5; }
	inline void set_downVector_5(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  value)
	{
		___downVector_5 = value;
	}

	inline static int32_t get_offset_of_leftVector_6() { return static_cast<int32_t>(offsetof(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9_StaticFields, ___leftVector_6)); }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  get_leftVector_6() const { return ___leftVector_6; }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 * get_address_of_leftVector_6() { return &___leftVector_6; }
	inline void set_leftVector_6(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  value)
	{
		___leftVector_6 = value;
	}

	inline static int32_t get_offset_of_rightVector_7() { return static_cast<int32_t>(offsetof(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9_StaticFields, ___rightVector_7)); }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  get_rightVector_7() const { return ___rightVector_7; }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 * get_address_of_rightVector_7() { return &___rightVector_7; }
	inline void set_rightVector_7(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  value)
	{
		___rightVector_7 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_8() { return static_cast<int32_t>(offsetof(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9_StaticFields, ___positiveInfinityVector_8)); }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  get_positiveInfinityVector_8() const { return ___positiveInfinityVector_8; }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 * get_address_of_positiveInfinityVector_8() { return &___positiveInfinityVector_8; }
	inline void set_positiveInfinityVector_8(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  value)
	{
		___positiveInfinityVector_8 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_9() { return static_cast<int32_t>(offsetof(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9_StaticFields, ___negativeInfinityVector_9)); }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  get_negativeInfinityVector_9() const { return ___negativeInfinityVector_9; }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 * get_address_of_negativeInfinityVector_9() { return &___negativeInfinityVector_9; }
	inline void set_negativeInfinityVector_9(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  value)
	{
		___negativeInfinityVector_9 = value;
	}
};


// UnityEngine.Vector3
struct Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E 
{
public:
	// System.Single UnityEngine.Vector3::x
	float ___x_2;
	// System.Single UnityEngine.Vector3::y
	float ___y_3;
	// System.Single UnityEngine.Vector3::z
	float ___z_4;

public:
	inline static int32_t get_offset_of_x_2() { return static_cast<int32_t>(offsetof(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E, ___x_2)); }
	inline float get_x_2() const { return ___x_2; }
	inline float* get_address_of_x_2() { return &___x_2; }
	inline void set_x_2(float value)
	{
		___x_2 = value;
	}

	inline static int32_t get_offset_of_y_3() { return static_cast<int32_t>(offsetof(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E, ___y_3)); }
	inline float get_y_3() const { return ___y_3; }
	inline float* get_address_of_y_3() { return &___y_3; }
	inline void set_y_3(float value)
	{
		___y_3 = value;
	}

	inline static int32_t get_offset_of_z_4() { return static_cast<int32_t>(offsetof(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E, ___z_4)); }
	inline float get_z_4() const { return ___z_4; }
	inline float* get_address_of_z_4() { return &___z_4; }
	inline void set_z_4(float value)
	{
		___z_4 = value;
	}
};

struct Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E_StaticFields
{
public:
	// UnityEngine.Vector3 UnityEngine.Vector3::zeroVector
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___zeroVector_5;
	// UnityEngine.Vector3 UnityEngine.Vector3::oneVector
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___oneVector_6;
	// UnityEngine.Vector3 UnityEngine.Vector3::upVector
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___upVector_7;
	// UnityEngine.Vector3 UnityEngine.Vector3::downVector
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___downVector_8;
	// UnityEngine.Vector3 UnityEngine.Vector3::leftVector
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___leftVector_9;
	// UnityEngine.Vector3 UnityEngine.Vector3::rightVector
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___rightVector_10;
	// UnityEngine.Vector3 UnityEngine.Vector3::forwardVector
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___forwardVector_11;
	// UnityEngine.Vector3 UnityEngine.Vector3::backVector
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___backVector_12;
	// UnityEngine.Vector3 UnityEngine.Vector3::positiveInfinityVector
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___positiveInfinityVector_13;
	// UnityEngine.Vector3 UnityEngine.Vector3::negativeInfinityVector
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___negativeInfinityVector_14;

public:
	inline static int32_t get_offset_of_zeroVector_5() { return static_cast<int32_t>(offsetof(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E_StaticFields, ___zeroVector_5)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_zeroVector_5() const { return ___zeroVector_5; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_zeroVector_5() { return &___zeroVector_5; }
	inline void set_zeroVector_5(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___zeroVector_5 = value;
	}

	inline static int32_t get_offset_of_oneVector_6() { return static_cast<int32_t>(offsetof(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E_StaticFields, ___oneVector_6)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_oneVector_6() const { return ___oneVector_6; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_oneVector_6() { return &___oneVector_6; }
	inline void set_oneVector_6(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___oneVector_6 = value;
	}

	inline static int32_t get_offset_of_upVector_7() { return static_cast<int32_t>(offsetof(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E_StaticFields, ___upVector_7)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_upVector_7() const { return ___upVector_7; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_upVector_7() { return &___upVector_7; }
	inline void set_upVector_7(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___upVector_7 = value;
	}

	inline static int32_t get_offset_of_downVector_8() { return static_cast<int32_t>(offsetof(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E_StaticFields, ___downVector_8)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_downVector_8() const { return ___downVector_8; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_downVector_8() { return &___downVector_8; }
	inline void set_downVector_8(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___downVector_8 = value;
	}

	inline static int32_t get_offset_of_leftVector_9() { return static_cast<int32_t>(offsetof(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E_StaticFields, ___leftVector_9)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_leftVector_9() const { return ___leftVector_9; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_leftVector_9() { return &___leftVector_9; }
	inline void set_leftVector_9(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___leftVector_9 = value;
	}

	inline static int32_t get_offset_of_rightVector_10() { return static_cast<int32_t>(offsetof(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E_StaticFields, ___rightVector_10)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_rightVector_10() const { return ___rightVector_10; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_rightVector_10() { return &___rightVector_10; }
	inline void set_rightVector_10(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___rightVector_10 = value;
	}

	inline static int32_t get_offset_of_forwardVector_11() { return static_cast<int32_t>(offsetof(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E_StaticFields, ___forwardVector_11)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_forwardVector_11() const { return ___forwardVector_11; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_forwardVector_11() { return &___forwardVector_11; }
	inline void set_forwardVector_11(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___forwardVector_11 = value;
	}

	inline static int32_t get_offset_of_backVector_12() { return static_cast<int32_t>(offsetof(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E_StaticFields, ___backVector_12)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_backVector_12() const { return ___backVector_12; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_backVector_12() { return &___backVector_12; }
	inline void set_backVector_12(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___backVector_12 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_13() { return static_cast<int32_t>(offsetof(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E_StaticFields, ___positiveInfinityVector_13)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_positiveInfinityVector_13() const { return ___positiveInfinityVector_13; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_positiveInfinityVector_13() { return &___positiveInfinityVector_13; }
	inline void set_positiveInfinityVector_13(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___positiveInfinityVector_13 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_14() { return static_cast<int32_t>(offsetof(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E_StaticFields, ___negativeInfinityVector_14)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_negativeInfinityVector_14() const { return ___negativeInfinityVector_14; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_negativeInfinityVector_14() { return &___negativeInfinityVector_14; }
	inline void set_negativeInfinityVector_14(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___negativeInfinityVector_14 = value;
	}
};


// System.Void
struct Void_t700C6383A2A510C2CF4DD86DABD5CA9FF70ADAC5 
{
public:
	union
	{
		struct
		{
		};
		uint8_t Void_t700C6383A2A510C2CF4DD86DABD5CA9FF70ADAC5__padding[1];
	};

public:
};


// UnityEngine.WaitForSeconds
struct WaitForSeconds_t8F9189BE6E467C98C99177038881F8982E0E4013  : public YieldInstruction_tB0B4E05316710E51ECCC1E57174C27FE6DEBBEAF
{
public:
	// System.Single UnityEngine.WaitForSeconds::m_Seconds
	float ___m_Seconds_0;

public:
	inline static int32_t get_offset_of_m_Seconds_0() { return static_cast<int32_t>(offsetof(WaitForSeconds_t8F9189BE6E467C98C99177038881F8982E0E4013, ___m_Seconds_0)); }
	inline float get_m_Seconds_0() const { return ___m_Seconds_0; }
	inline float* get_address_of_m_Seconds_0() { return &___m_Seconds_0; }
	inline void set_m_Seconds_0(float value)
	{
		___m_Seconds_0 = value;
	}
};

// Native definition for P/Invoke marshalling of UnityEngine.WaitForSeconds
struct WaitForSeconds_t8F9189BE6E467C98C99177038881F8982E0E4013_marshaled_pinvoke : public YieldInstruction_tB0B4E05316710E51ECCC1E57174C27FE6DEBBEAF_marshaled_pinvoke
{
	float ___m_Seconds_0;
};
// Native definition for COM marshalling of UnityEngine.WaitForSeconds
struct WaitForSeconds_t8F9189BE6E467C98C99177038881F8982E0E4013_marshaled_com : public YieldInstruction_tB0B4E05316710E51ECCC1E57174C27FE6DEBBEAF_marshaled_com
{
	float ___m_Seconds_0;
};

// UnityEngine.Coroutine
struct Coroutine_t899D5232EF542CB8BA70AF9ECEECA494FAA9CCB7  : public YieldInstruction_tB0B4E05316710E51ECCC1E57174C27FE6DEBBEAF
{
public:
	// System.IntPtr UnityEngine.Coroutine::m_Ptr
	intptr_t ___m_Ptr_0;

public:
	inline static int32_t get_offset_of_m_Ptr_0() { return static_cast<int32_t>(offsetof(Coroutine_t899D5232EF542CB8BA70AF9ECEECA494FAA9CCB7, ___m_Ptr_0)); }
	inline intptr_t get_m_Ptr_0() const { return ___m_Ptr_0; }
	inline intptr_t* get_address_of_m_Ptr_0() { return &___m_Ptr_0; }
	inline void set_m_Ptr_0(intptr_t value)
	{
		___m_Ptr_0 = value;
	}
};

// Native definition for P/Invoke marshalling of UnityEngine.Coroutine
struct Coroutine_t899D5232EF542CB8BA70AF9ECEECA494FAA9CCB7_marshaled_pinvoke : public YieldInstruction_tB0B4E05316710E51ECCC1E57174C27FE6DEBBEAF_marshaled_pinvoke
{
	intptr_t ___m_Ptr_0;
};
// Native definition for COM marshalling of UnityEngine.Coroutine
struct Coroutine_t899D5232EF542CB8BA70AF9ECEECA494FAA9CCB7_marshaled_com : public YieldInstruction_tB0B4E05316710E51ECCC1E57174C27FE6DEBBEAF_marshaled_com
{
	intptr_t ___m_Ptr_0;
};

// System.Exception
struct Exception_t  : public RuntimeObject
{
public:
	// System.String System.Exception::_className
	String_t* ____className_1;
	// System.String System.Exception::_message
	String_t* ____message_2;
	// System.Collections.IDictionary System.Exception::_data
	RuntimeObject* ____data_3;
	// System.Exception System.Exception::_innerException
	Exception_t * ____innerException_4;
	// System.String System.Exception::_helpURL
	String_t* ____helpURL_5;
	// System.Object System.Exception::_stackTrace
	RuntimeObject * ____stackTrace_6;
	// System.String System.Exception::_stackTraceString
	String_t* ____stackTraceString_7;
	// System.String System.Exception::_remoteStackTraceString
	String_t* ____remoteStackTraceString_8;
	// System.Int32 System.Exception::_remoteStackIndex
	int32_t ____remoteStackIndex_9;
	// System.Object System.Exception::_dynamicMethods
	RuntimeObject * ____dynamicMethods_10;
	// System.Int32 System.Exception::_HResult
	int32_t ____HResult_11;
	// System.String System.Exception::_source
	String_t* ____source_12;
	// System.Runtime.Serialization.SafeSerializationManager System.Exception::_safeSerializationManager
	SafeSerializationManager_tDE44F029589A028F8A3053C5C06153FAB4AAE29F * ____safeSerializationManager_13;
	// System.Diagnostics.StackTrace[] System.Exception::captured_traces
	StackTraceU5BU5D_t4AD999C288CB6D1F38A299D12B1598D606588971* ___captured_traces_14;
	// System.IntPtr[] System.Exception::native_trace_ips
	IntPtrU5BU5D_t27FC72B0409D75AAF33EC42498E8094E95FEE9A6* ___native_trace_ips_15;

public:
	inline static int32_t get_offset_of__className_1() { return static_cast<int32_t>(offsetof(Exception_t, ____className_1)); }
	inline String_t* get__className_1() const { return ____className_1; }
	inline String_t** get_address_of__className_1() { return &____className_1; }
	inline void set__className_1(String_t* value)
	{
		____className_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____className_1), (void*)value);
	}

	inline static int32_t get_offset_of__message_2() { return static_cast<int32_t>(offsetof(Exception_t, ____message_2)); }
	inline String_t* get__message_2() const { return ____message_2; }
	inline String_t** get_address_of__message_2() { return &____message_2; }
	inline void set__message_2(String_t* value)
	{
		____message_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____message_2), (void*)value);
	}

	inline static int32_t get_offset_of__data_3() { return static_cast<int32_t>(offsetof(Exception_t, ____data_3)); }
	inline RuntimeObject* get__data_3() const { return ____data_3; }
	inline RuntimeObject** get_address_of__data_3() { return &____data_3; }
	inline void set__data_3(RuntimeObject* value)
	{
		____data_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____data_3), (void*)value);
	}

	inline static int32_t get_offset_of__innerException_4() { return static_cast<int32_t>(offsetof(Exception_t, ____innerException_4)); }
	inline Exception_t * get__innerException_4() const { return ____innerException_4; }
	inline Exception_t ** get_address_of__innerException_4() { return &____innerException_4; }
	inline void set__innerException_4(Exception_t * value)
	{
		____innerException_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____innerException_4), (void*)value);
	}

	inline static int32_t get_offset_of__helpURL_5() { return static_cast<int32_t>(offsetof(Exception_t, ____helpURL_5)); }
	inline String_t* get__helpURL_5() const { return ____helpURL_5; }
	inline String_t** get_address_of__helpURL_5() { return &____helpURL_5; }
	inline void set__helpURL_5(String_t* value)
	{
		____helpURL_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____helpURL_5), (void*)value);
	}

	inline static int32_t get_offset_of__stackTrace_6() { return static_cast<int32_t>(offsetof(Exception_t, ____stackTrace_6)); }
	inline RuntimeObject * get__stackTrace_6() const { return ____stackTrace_6; }
	inline RuntimeObject ** get_address_of__stackTrace_6() { return &____stackTrace_6; }
	inline void set__stackTrace_6(RuntimeObject * value)
	{
		____stackTrace_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____stackTrace_6), (void*)value);
	}

	inline static int32_t get_offset_of__stackTraceString_7() { return static_cast<int32_t>(offsetof(Exception_t, ____stackTraceString_7)); }
	inline String_t* get__stackTraceString_7() const { return ____stackTraceString_7; }
	inline String_t** get_address_of__stackTraceString_7() { return &____stackTraceString_7; }
	inline void set__stackTraceString_7(String_t* value)
	{
		____stackTraceString_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____stackTraceString_7), (void*)value);
	}

	inline static int32_t get_offset_of__remoteStackTraceString_8() { return static_cast<int32_t>(offsetof(Exception_t, ____remoteStackTraceString_8)); }
	inline String_t* get__remoteStackTraceString_8() const { return ____remoteStackTraceString_8; }
	inline String_t** get_address_of__remoteStackTraceString_8() { return &____remoteStackTraceString_8; }
	inline void set__remoteStackTraceString_8(String_t* value)
	{
		____remoteStackTraceString_8 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____remoteStackTraceString_8), (void*)value);
	}

	inline static int32_t get_offset_of__remoteStackIndex_9() { return static_cast<int32_t>(offsetof(Exception_t, ____remoteStackIndex_9)); }
	inline int32_t get__remoteStackIndex_9() const { return ____remoteStackIndex_9; }
	inline int32_t* get_address_of__remoteStackIndex_9() { return &____remoteStackIndex_9; }
	inline void set__remoteStackIndex_9(int32_t value)
	{
		____remoteStackIndex_9 = value;
	}

	inline static int32_t get_offset_of__dynamicMethods_10() { return static_cast<int32_t>(offsetof(Exception_t, ____dynamicMethods_10)); }
	inline RuntimeObject * get__dynamicMethods_10() const { return ____dynamicMethods_10; }
	inline RuntimeObject ** get_address_of__dynamicMethods_10() { return &____dynamicMethods_10; }
	inline void set__dynamicMethods_10(RuntimeObject * value)
	{
		____dynamicMethods_10 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____dynamicMethods_10), (void*)value);
	}

	inline static int32_t get_offset_of__HResult_11() { return static_cast<int32_t>(offsetof(Exception_t, ____HResult_11)); }
	inline int32_t get__HResult_11() const { return ____HResult_11; }
	inline int32_t* get_address_of__HResult_11() { return &____HResult_11; }
	inline void set__HResult_11(int32_t value)
	{
		____HResult_11 = value;
	}

	inline static int32_t get_offset_of__source_12() { return static_cast<int32_t>(offsetof(Exception_t, ____source_12)); }
	inline String_t* get__source_12() const { return ____source_12; }
	inline String_t** get_address_of__source_12() { return &____source_12; }
	inline void set__source_12(String_t* value)
	{
		____source_12 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____source_12), (void*)value);
	}

	inline static int32_t get_offset_of__safeSerializationManager_13() { return static_cast<int32_t>(offsetof(Exception_t, ____safeSerializationManager_13)); }
	inline SafeSerializationManager_tDE44F029589A028F8A3053C5C06153FAB4AAE29F * get__safeSerializationManager_13() const { return ____safeSerializationManager_13; }
	inline SafeSerializationManager_tDE44F029589A028F8A3053C5C06153FAB4AAE29F ** get_address_of__safeSerializationManager_13() { return &____safeSerializationManager_13; }
	inline void set__safeSerializationManager_13(SafeSerializationManager_tDE44F029589A028F8A3053C5C06153FAB4AAE29F * value)
	{
		____safeSerializationManager_13 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____safeSerializationManager_13), (void*)value);
	}

	inline static int32_t get_offset_of_captured_traces_14() { return static_cast<int32_t>(offsetof(Exception_t, ___captured_traces_14)); }
	inline StackTraceU5BU5D_t4AD999C288CB6D1F38A299D12B1598D606588971* get_captured_traces_14() const { return ___captured_traces_14; }
	inline StackTraceU5BU5D_t4AD999C288CB6D1F38A299D12B1598D606588971** get_address_of_captured_traces_14() { return &___captured_traces_14; }
	inline void set_captured_traces_14(StackTraceU5BU5D_t4AD999C288CB6D1F38A299D12B1598D606588971* value)
	{
		___captured_traces_14 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___captured_traces_14), (void*)value);
	}

	inline static int32_t get_offset_of_native_trace_ips_15() { return static_cast<int32_t>(offsetof(Exception_t, ___native_trace_ips_15)); }
	inline IntPtrU5BU5D_t27FC72B0409D75AAF33EC42498E8094E95FEE9A6* get_native_trace_ips_15() const { return ___native_trace_ips_15; }
	inline IntPtrU5BU5D_t27FC72B0409D75AAF33EC42498E8094E95FEE9A6** get_address_of_native_trace_ips_15() { return &___native_trace_ips_15; }
	inline void set_native_trace_ips_15(IntPtrU5BU5D_t27FC72B0409D75AAF33EC42498E8094E95FEE9A6* value)
	{
		___native_trace_ips_15 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___native_trace_ips_15), (void*)value);
	}
};

struct Exception_t_StaticFields
{
public:
	// System.Object System.Exception::s_EDILock
	RuntimeObject * ___s_EDILock_0;

public:
	inline static int32_t get_offset_of_s_EDILock_0() { return static_cast<int32_t>(offsetof(Exception_t_StaticFields, ___s_EDILock_0)); }
	inline RuntimeObject * get_s_EDILock_0() const { return ___s_EDILock_0; }
	inline RuntimeObject ** get_address_of_s_EDILock_0() { return &___s_EDILock_0; }
	inline void set_s_EDILock_0(RuntimeObject * value)
	{
		___s_EDILock_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___s_EDILock_0), (void*)value);
	}
};

// Native definition for P/Invoke marshalling of System.Exception
struct Exception_t_marshaled_pinvoke
{
	char* ____className_1;
	char* ____message_2;
	RuntimeObject* ____data_3;
	Exception_t_marshaled_pinvoke* ____innerException_4;
	char* ____helpURL_5;
	Il2CppIUnknown* ____stackTrace_6;
	char* ____stackTraceString_7;
	char* ____remoteStackTraceString_8;
	int32_t ____remoteStackIndex_9;
	Il2CppIUnknown* ____dynamicMethods_10;
	int32_t ____HResult_11;
	char* ____source_12;
	SafeSerializationManager_tDE44F029589A028F8A3053C5C06153FAB4AAE29F * ____safeSerializationManager_13;
	StackTraceU5BU5D_t4AD999C288CB6D1F38A299D12B1598D606588971* ___captured_traces_14;
	Il2CppSafeArray/*NONE*/* ___native_trace_ips_15;
};
// Native definition for COM marshalling of System.Exception
struct Exception_t_marshaled_com
{
	Il2CppChar* ____className_1;
	Il2CppChar* ____message_2;
	RuntimeObject* ____data_3;
	Exception_t_marshaled_com* ____innerException_4;
	Il2CppChar* ____helpURL_5;
	Il2CppIUnknown* ____stackTrace_6;
	Il2CppChar* ____stackTraceString_7;
	Il2CppChar* ____remoteStackTraceString_8;
	int32_t ____remoteStackIndex_9;
	Il2CppIUnknown* ____dynamicMethods_10;
	int32_t ____HResult_11;
	Il2CppChar* ____source_12;
	SafeSerializationManager_tDE44F029589A028F8A3053C5C06153FAB4AAE29F * ____safeSerializationManager_13;
	StackTraceU5BU5D_t4AD999C288CB6D1F38A299D12B1598D606588971* ___captured_traces_14;
	Il2CppSafeArray/*NONE*/* ___native_trace_ips_15;
};

// UnityEngine.FFTWindow
struct FFTWindow_t4BBC1F846B9A1AC7C119C720F9FD9C0E77DBDC71 
{
public:
	// System.Int32 UnityEngine.FFTWindow::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(FFTWindow_t4BBC1F846B9A1AC7C119C720F9FD9C0E77DBDC71, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// UnityEngine.Object
struct Object_tF2F3778131EFF286AF62B7B013A170F95A91571A  : public RuntimeObject
{
public:
	// System.IntPtr UnityEngine.Object::m_CachedPtr
	intptr_t ___m_CachedPtr_0;

public:
	inline static int32_t get_offset_of_m_CachedPtr_0() { return static_cast<int32_t>(offsetof(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A, ___m_CachedPtr_0)); }
	inline intptr_t get_m_CachedPtr_0() const { return ___m_CachedPtr_0; }
	inline intptr_t* get_address_of_m_CachedPtr_0() { return &___m_CachedPtr_0; }
	inline void set_m_CachedPtr_0(intptr_t value)
	{
		___m_CachedPtr_0 = value;
	}
};

struct Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_StaticFields
{
public:
	// System.Int32 UnityEngine.Object::OffsetOfInstanceIDInCPlusPlusObject
	int32_t ___OffsetOfInstanceIDInCPlusPlusObject_1;

public:
	inline static int32_t get_offset_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return static_cast<int32_t>(offsetof(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_StaticFields, ___OffsetOfInstanceIDInCPlusPlusObject_1)); }
	inline int32_t get_OffsetOfInstanceIDInCPlusPlusObject_1() const { return ___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline int32_t* get_address_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return &___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline void set_OffsetOfInstanceIDInCPlusPlusObject_1(int32_t value)
	{
		___OffsetOfInstanceIDInCPlusPlusObject_1 = value;
	}
};

// Native definition for P/Invoke marshalling of UnityEngine.Object
struct Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_marshaled_pinvoke
{
	intptr_t ___m_CachedPtr_0;
};
// Native definition for COM marshalling of UnityEngine.Object
struct Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_marshaled_com
{
	intptr_t ___m_CachedPtr_0;
};

// UnityEngine.SendMessageOptions
struct SendMessageOptions_t89E16D7B4FAECAF721478B06E56214F97438C61B 
{
public:
	// System.Int32 UnityEngine.SendMessageOptions::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(SendMessageOptions_t89E16D7B4FAECAF721478B06E56214F97438C61B, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// CrazyMinnow.SALSA.RandomEyes2D/SpriteRend
struct SpriteRend_t9C5C54BA8B49D2BC1AACA473C0648BB0399BBADD 
{
public:
	// System.Int32 CrazyMinnow.SALSA.RandomEyes2D/SpriteRend::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(SpriteRend_t9C5C54BA8B49D2BC1AACA473C0648BB0399BBADD, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// CrazyMinnow.SALSA.SalsaUtility/BlendDirection
struct BlendDirection_t2F990F1EFD13117CEC22103B33D3E463EB91D243 
{
public:
	// System.Int32 CrazyMinnow.SALSA.SalsaUtility/BlendDirection::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(BlendDirection_t2F990F1EFD13117CEC22103B33D3E463EB91D243, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// UnityEngine.AudioClip
struct AudioClip_t16D2E573E7CC1C5118D8EE0F0692D46866A1C0EE  : public Object_tF2F3778131EFF286AF62B7B013A170F95A91571A
{
public:
	// UnityEngine.AudioClip/PCMReaderCallback UnityEngine.AudioClip::m_PCMReaderCallback
	PCMReaderCallback_t9CA1437D36509A9FAC5EDD8FF2BC3259C24D0E0B * ___m_PCMReaderCallback_4;
	// UnityEngine.AudioClip/PCMSetPositionCallback UnityEngine.AudioClip::m_PCMSetPositionCallback
	PCMSetPositionCallback_tBDD99E7C0697687F1E7B06CDD5DE444A3709CF4C * ___m_PCMSetPositionCallback_5;

public:
	inline static int32_t get_offset_of_m_PCMReaderCallback_4() { return static_cast<int32_t>(offsetof(AudioClip_t16D2E573E7CC1C5118D8EE0F0692D46866A1C0EE, ___m_PCMReaderCallback_4)); }
	inline PCMReaderCallback_t9CA1437D36509A9FAC5EDD8FF2BC3259C24D0E0B * get_m_PCMReaderCallback_4() const { return ___m_PCMReaderCallback_4; }
	inline PCMReaderCallback_t9CA1437D36509A9FAC5EDD8FF2BC3259C24D0E0B ** get_address_of_m_PCMReaderCallback_4() { return &___m_PCMReaderCallback_4; }
	inline void set_m_PCMReaderCallback_4(PCMReaderCallback_t9CA1437D36509A9FAC5EDD8FF2BC3259C24D0E0B * value)
	{
		___m_PCMReaderCallback_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_PCMReaderCallback_4), (void*)value);
	}

	inline static int32_t get_offset_of_m_PCMSetPositionCallback_5() { return static_cast<int32_t>(offsetof(AudioClip_t16D2E573E7CC1C5118D8EE0F0692D46866A1C0EE, ___m_PCMSetPositionCallback_5)); }
	inline PCMSetPositionCallback_tBDD99E7C0697687F1E7B06CDD5DE444A3709CF4C * get_m_PCMSetPositionCallback_5() const { return ___m_PCMSetPositionCallback_5; }
	inline PCMSetPositionCallback_tBDD99E7C0697687F1E7B06CDD5DE444A3709CF4C ** get_address_of_m_PCMSetPositionCallback_5() { return &___m_PCMSetPositionCallback_5; }
	inline void set_m_PCMSetPositionCallback_5(PCMSetPositionCallback_tBDD99E7C0697687F1E7B06CDD5DE444A3709CF4C * value)
	{
		___m_PCMSetPositionCallback_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_PCMSetPositionCallback_5), (void*)value);
	}
};


// UnityEngine.Component
struct Component_t62FBC8D2420DA4BE9037AFE430740F6B3EECA684  : public Object_tF2F3778131EFF286AF62B7B013A170F95A91571A
{
public:

public:
};


// UnityEngine.GameObject
struct GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319  : public Object_tF2F3778131EFF286AF62B7B013A170F95A91571A
{
public:

public:
};


// UnityEngine.Mesh
struct Mesh_t2F5992DBA650D5862B43D3823ACD997132A57DA6  : public Object_tF2F3778131EFF286AF62B7B013A170F95A91571A
{
public:

public:
};


// CrazyMinnow.SALSA.SalsaUtility
struct SalsaUtility_t3157054D4AC9188D9E6BE3F1E4D00B71C49BB30B  : public RuntimeObject
{
public:
	// CrazyMinnow.SALSA.SalsaUtility/BlendDirection CrazyMinnow.SALSA.SalsaUtility::blendType
	int32_t ___blendType_0;

public:
	inline static int32_t get_offset_of_blendType_0() { return static_cast<int32_t>(offsetof(SalsaUtility_t3157054D4AC9188D9E6BE3F1E4D00B71C49BB30B, ___blendType_0)); }
	inline int32_t get_blendType_0() const { return ___blendType_0; }
	inline int32_t* get_address_of_blendType_0() { return &___blendType_0; }
	inline void set_blendType_0(int32_t value)
	{
		___blendType_0 = value;
	}
};


// UnityEngine.Sprite
struct Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9  : public Object_tF2F3778131EFF286AF62B7B013A170F95A91571A
{
public:

public:
};


// System.SystemException
struct SystemException_tC551B4D6EE3772B5F32C71EE8C719F4B43ECCC62  : public Exception_t
{
public:

public:
};


// UnityEngine.Texture
struct Texture_t9FE0218A1EEDF266E8C85879FE123265CACC95AE  : public Object_tF2F3778131EFF286AF62B7B013A170F95A91571A
{
public:

public:
};

struct Texture_t9FE0218A1EEDF266E8C85879FE123265CACC95AE_StaticFields
{
public:
	// System.Int32 UnityEngine.Texture::GenerateAllMips
	int32_t ___GenerateAllMips_4;

public:
	inline static int32_t get_offset_of_GenerateAllMips_4() { return static_cast<int32_t>(offsetof(Texture_t9FE0218A1EEDF266E8C85879FE123265CACC95AE_StaticFields, ___GenerateAllMips_4)); }
	inline int32_t get_GenerateAllMips_4() const { return ___GenerateAllMips_4; }
	inline int32_t* get_address_of_GenerateAllMips_4() { return &___GenerateAllMips_4; }
	inline void set_GenerateAllMips_4(int32_t value)
	{
		___GenerateAllMips_4 = value;
	}
};


// UnityEngine.Behaviour
struct Behaviour_t1A3DDDCF73B4627928FBFE02ED52B7251777DBD9  : public Component_t62FBC8D2420DA4BE9037AFE430740F6B3EECA684
{
public:

public:
};


// System.NotSupportedException
struct NotSupportedException_tB9D89F0E9470A2C423D239D7C68EE0CFD77F9339  : public SystemException_tC551B4D6EE3772B5F32C71EE8C719F4B43ECCC62
{
public:

public:
};


// UnityEngine.Renderer
struct Renderer_t58147AB5B00224FE1460FD47542DC0DA7EC9378C  : public Component_t62FBC8D2420DA4BE9037AFE430740F6B3EECA684
{
public:

public:
};


// UnityEngine.Texture2D
struct Texture2D_t9B604D0D8E28032123641A7E7338FA872E2698BF  : public Texture_t9FE0218A1EEDF266E8C85879FE123265CACC95AE
{
public:

public:
};


// UnityEngine.Transform
struct Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1  : public Component_t62FBC8D2420DA4BE9037AFE430740F6B3EECA684
{
public:

public:
};


// UnityEngine.AudioBehaviour
struct AudioBehaviour_tB44966D47AD43C50C7294AEE9B57574E55AACA4A  : public Behaviour_t1A3DDDCF73B4627928FBFE02ED52B7251777DBD9
{
public:

public:
};


// UnityEngine.MonoBehaviour
struct MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A  : public Behaviour_t1A3DDDCF73B4627928FBFE02ED52B7251777DBD9
{
public:

public:
};


// UnityEngine.SkinnedMeshRenderer
struct SkinnedMeshRenderer_t126F4D6010E0F4B2685A7817B0A9171805D8F496  : public Renderer_t58147AB5B00224FE1460FD47542DC0DA7EC9378C
{
public:

public:
};


// UnityEngine.SpriteRenderer
struct SpriteRenderer_t3F35AD5498243C170B46F5FFDB582AAEF78615EF  : public Renderer_t58147AB5B00224FE1460FD47542DC0DA7EC9378C
{
public:

public:
};


// UnityEngine.AudioSource
struct AudioSource_tC4BF65AF8CDCAA63724BB3CA59A7A29249269E6B  : public AudioBehaviour_tB44966D47AD43C50C7294AEE9B57574E55AACA4A
{
public:

public:
};


// CrazyMinnow.SALSA.RandomEyes2D
struct RandomEyes2D_t253864BE689DE7E477335D17ECD2EC77001B377F  : public MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A
{
public:
	// UnityEngine.SpriteRenderer CrazyMinnow.SALSA.RandomEyes2D::character
	SpriteRenderer_t3F35AD5498243C170B46F5FFDB582AAEF78615EF * ___character_5;
	// System.Int32 CrazyMinnow.SALSA.RandomEyes2D::characterLayer
	int32_t ___characterLayer_6;
	// System.Int32 CrazyMinnow.SALSA.RandomEyes2D::eyeCount
	int32_t ___eyeCount_7;
	// UnityEngine.SpriteRenderer[] CrazyMinnow.SALSA.RandomEyes2D::eyes
	SpriteRendererU5BU5D_t45BF125944C2F3974C568E650FCCDB591E5ABFBF* ___eyes_8;
	// System.Int32 CrazyMinnow.SALSA.RandomEyes2D::eyeLayer
	int32_t ___eyeLayer_9;
	// System.Int32 CrazyMinnow.SALSA.RandomEyes2D::eyeLidCount
	int32_t ___eyeLidCount_10;
	// UnityEngine.SpriteRenderer[] CrazyMinnow.SALSA.RandomEyes2D::eyeLids
	SpriteRendererU5BU5D_t45BF125944C2F3974C568E650FCCDB591E5ABFBF* ___eyeLids_11;
	// System.Int32 CrazyMinnow.SALSA.RandomEyes2D::eyeLidLayer
	int32_t ___eyeLidLayer_12;
	// UnityEngine.Vector3 CrazyMinnow.SALSA.RandomEyes2D::currentLookPos
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___currentLookPos_13;
	// UnityEngine.Vector3 CrazyMinnow.SALSA.RandomEyes2D::lookRandPosition
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___lookRandPosition_14;
	// System.Single CrazyMinnow.SALSA.RandomEyes2D::rangeOfMotion
	float ___rangeOfMotion_15;
	// System.Boolean CrazyMinnow.SALSA.RandomEyes2D::moveProportional
	bool ___moveProportional_16;
	// UnityEngine.GameObject CrazyMinnow.SALSA.RandomEyes2D::lookTarget
	GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * ___lookTarget_17;
	// UnityEngine.GameObject CrazyMinnow.SALSA.RandomEyes2D::lookTargetTracking
	GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * ___lookTargetTracking_18;
	// System.Boolean CrazyMinnow.SALSA.RandomEyes2D::targetAffinity
	bool ___targetAffinity_19;
	// System.Single CrazyMinnow.SALSA.RandomEyes2D::targetAffinityPercentage
	float ___targetAffinityPercentage_20;
	// System.Boolean CrazyMinnow.SALSA.RandomEyes2D::randomEyes
	bool ___randomEyes_21;
	// System.Single CrazyMinnow.SALSA.RandomEyes2D::maxRandomInterval
	float ___maxRandomInterval_22;
	// System.Single CrazyMinnow.SALSA.RandomEyes2D::blendSpeed
	float ___blendSpeed_23;
	// System.Boolean CrazyMinnow.SALSA.RandomEyes2D::randomBlink
	bool ___randomBlink_24;
	// System.Single CrazyMinnow.SALSA.RandomEyes2D::maxBlinkInterval
	float ___maxBlinkInterval_25;
	// System.Single CrazyMinnow.SALSA.RandomEyes2D::blinkDuration
	float ___blinkDuration_26;
	// System.Boolean CrazyMinnow.SALSA.RandomEyes2D::expandEyes
	bool ___expandEyes_27;
	// System.Boolean CrazyMinnow.SALSA.RandomEyes2D::expandEyelids
	bool ___expandEyelids_28;
	// System.Boolean CrazyMinnow.SALSA.RandomEyes2D::showBroadcast
	bool ___showBroadcast_29;
	// UnityEngine.Vector3[] CrazyMinnow.SALSA.RandomEyes2D::trackDirection
	Vector3U5BU5D_t5FB88EAA33E46838BDC2ABDAEA3E8727491CB9E4* ___trackDirection_30;
	// UnityEngine.GameObject[] CrazyMinnow.SALSA.RandomEyes2D::randomEyes2DGizmos
	GameObjectU5BU5D_tA88FC1A1FC9D4D73D0B3984D4B0ECE88F4C47642* ___randomEyes2DGizmos_31;
	// UnityEngine.Vector2 CrazyMinnow.SALSA.RandomEyes2D::targetAffinityTimerRange
	Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ___targetAffinityTimerRange_32;
	// System.Single CrazyMinnow.SALSA.RandomEyes2D::targetAffinityTimer
	float ___targetAffinityTimer_33;
	// System.Boolean CrazyMinnow.SALSA.RandomEyes2D::hasAffinity
	bool ___hasAffinity_34;
	// System.Single CrazyMinnow.SALSA.RandomEyes2D::randomTimer
	float ___randomTimer_35;
	// System.Single CrazyMinnow.SALSA.RandomEyes2D::blinkTimer
	float ___blinkTimer_36;
	// System.Int32 CrazyMinnow.SALSA.RandomEyes2D::largestEyeIndex
	int32_t ___largestEyeIndex_37;
	// UnityEngine.SpriteRenderer CrazyMinnow.SALSA.RandomEyes2D::largestEye
	SpriteRenderer_t3F35AD5498243C170B46F5FFDB582AAEF78615EF * ___largestEye_38;

public:
	inline static int32_t get_offset_of_character_5() { return static_cast<int32_t>(offsetof(RandomEyes2D_t253864BE689DE7E477335D17ECD2EC77001B377F, ___character_5)); }
	inline SpriteRenderer_t3F35AD5498243C170B46F5FFDB582AAEF78615EF * get_character_5() const { return ___character_5; }
	inline SpriteRenderer_t3F35AD5498243C170B46F5FFDB582AAEF78615EF ** get_address_of_character_5() { return &___character_5; }
	inline void set_character_5(SpriteRenderer_t3F35AD5498243C170B46F5FFDB582AAEF78615EF * value)
	{
		___character_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___character_5), (void*)value);
	}

	inline static int32_t get_offset_of_characterLayer_6() { return static_cast<int32_t>(offsetof(RandomEyes2D_t253864BE689DE7E477335D17ECD2EC77001B377F, ___characterLayer_6)); }
	inline int32_t get_characterLayer_6() const { return ___characterLayer_6; }
	inline int32_t* get_address_of_characterLayer_6() { return &___characterLayer_6; }
	inline void set_characterLayer_6(int32_t value)
	{
		___characterLayer_6 = value;
	}

	inline static int32_t get_offset_of_eyeCount_7() { return static_cast<int32_t>(offsetof(RandomEyes2D_t253864BE689DE7E477335D17ECD2EC77001B377F, ___eyeCount_7)); }
	inline int32_t get_eyeCount_7() const { return ___eyeCount_7; }
	inline int32_t* get_address_of_eyeCount_7() { return &___eyeCount_7; }
	inline void set_eyeCount_7(int32_t value)
	{
		___eyeCount_7 = value;
	}

	inline static int32_t get_offset_of_eyes_8() { return static_cast<int32_t>(offsetof(RandomEyes2D_t253864BE689DE7E477335D17ECD2EC77001B377F, ___eyes_8)); }
	inline SpriteRendererU5BU5D_t45BF125944C2F3974C568E650FCCDB591E5ABFBF* get_eyes_8() const { return ___eyes_8; }
	inline SpriteRendererU5BU5D_t45BF125944C2F3974C568E650FCCDB591E5ABFBF** get_address_of_eyes_8() { return &___eyes_8; }
	inline void set_eyes_8(SpriteRendererU5BU5D_t45BF125944C2F3974C568E650FCCDB591E5ABFBF* value)
	{
		___eyes_8 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___eyes_8), (void*)value);
	}

	inline static int32_t get_offset_of_eyeLayer_9() { return static_cast<int32_t>(offsetof(RandomEyes2D_t253864BE689DE7E477335D17ECD2EC77001B377F, ___eyeLayer_9)); }
	inline int32_t get_eyeLayer_9() const { return ___eyeLayer_9; }
	inline int32_t* get_address_of_eyeLayer_9() { return &___eyeLayer_9; }
	inline void set_eyeLayer_9(int32_t value)
	{
		___eyeLayer_9 = value;
	}

	inline static int32_t get_offset_of_eyeLidCount_10() { return static_cast<int32_t>(offsetof(RandomEyes2D_t253864BE689DE7E477335D17ECD2EC77001B377F, ___eyeLidCount_10)); }
	inline int32_t get_eyeLidCount_10() const { return ___eyeLidCount_10; }
	inline int32_t* get_address_of_eyeLidCount_10() { return &___eyeLidCount_10; }
	inline void set_eyeLidCount_10(int32_t value)
	{
		___eyeLidCount_10 = value;
	}

	inline static int32_t get_offset_of_eyeLids_11() { return static_cast<int32_t>(offsetof(RandomEyes2D_t253864BE689DE7E477335D17ECD2EC77001B377F, ___eyeLids_11)); }
	inline SpriteRendererU5BU5D_t45BF125944C2F3974C568E650FCCDB591E5ABFBF* get_eyeLids_11() const { return ___eyeLids_11; }
	inline SpriteRendererU5BU5D_t45BF125944C2F3974C568E650FCCDB591E5ABFBF** get_address_of_eyeLids_11() { return &___eyeLids_11; }
	inline void set_eyeLids_11(SpriteRendererU5BU5D_t45BF125944C2F3974C568E650FCCDB591E5ABFBF* value)
	{
		___eyeLids_11 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___eyeLids_11), (void*)value);
	}

	inline static int32_t get_offset_of_eyeLidLayer_12() { return static_cast<int32_t>(offsetof(RandomEyes2D_t253864BE689DE7E477335D17ECD2EC77001B377F, ___eyeLidLayer_12)); }
	inline int32_t get_eyeLidLayer_12() const { return ___eyeLidLayer_12; }
	inline int32_t* get_address_of_eyeLidLayer_12() { return &___eyeLidLayer_12; }
	inline void set_eyeLidLayer_12(int32_t value)
	{
		___eyeLidLayer_12 = value;
	}

	inline static int32_t get_offset_of_currentLookPos_13() { return static_cast<int32_t>(offsetof(RandomEyes2D_t253864BE689DE7E477335D17ECD2EC77001B377F, ___currentLookPos_13)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_currentLookPos_13() const { return ___currentLookPos_13; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_currentLookPos_13() { return &___currentLookPos_13; }
	inline void set_currentLookPos_13(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___currentLookPos_13 = value;
	}

	inline static int32_t get_offset_of_lookRandPosition_14() { return static_cast<int32_t>(offsetof(RandomEyes2D_t253864BE689DE7E477335D17ECD2EC77001B377F, ___lookRandPosition_14)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_lookRandPosition_14() const { return ___lookRandPosition_14; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_lookRandPosition_14() { return &___lookRandPosition_14; }
	inline void set_lookRandPosition_14(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___lookRandPosition_14 = value;
	}

	inline static int32_t get_offset_of_rangeOfMotion_15() { return static_cast<int32_t>(offsetof(RandomEyes2D_t253864BE689DE7E477335D17ECD2EC77001B377F, ___rangeOfMotion_15)); }
	inline float get_rangeOfMotion_15() const { return ___rangeOfMotion_15; }
	inline float* get_address_of_rangeOfMotion_15() { return &___rangeOfMotion_15; }
	inline void set_rangeOfMotion_15(float value)
	{
		___rangeOfMotion_15 = value;
	}

	inline static int32_t get_offset_of_moveProportional_16() { return static_cast<int32_t>(offsetof(RandomEyes2D_t253864BE689DE7E477335D17ECD2EC77001B377F, ___moveProportional_16)); }
	inline bool get_moveProportional_16() const { return ___moveProportional_16; }
	inline bool* get_address_of_moveProportional_16() { return &___moveProportional_16; }
	inline void set_moveProportional_16(bool value)
	{
		___moveProportional_16 = value;
	}

	inline static int32_t get_offset_of_lookTarget_17() { return static_cast<int32_t>(offsetof(RandomEyes2D_t253864BE689DE7E477335D17ECD2EC77001B377F, ___lookTarget_17)); }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * get_lookTarget_17() const { return ___lookTarget_17; }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 ** get_address_of_lookTarget_17() { return &___lookTarget_17; }
	inline void set_lookTarget_17(GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * value)
	{
		___lookTarget_17 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___lookTarget_17), (void*)value);
	}

	inline static int32_t get_offset_of_lookTargetTracking_18() { return static_cast<int32_t>(offsetof(RandomEyes2D_t253864BE689DE7E477335D17ECD2EC77001B377F, ___lookTargetTracking_18)); }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * get_lookTargetTracking_18() const { return ___lookTargetTracking_18; }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 ** get_address_of_lookTargetTracking_18() { return &___lookTargetTracking_18; }
	inline void set_lookTargetTracking_18(GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * value)
	{
		___lookTargetTracking_18 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___lookTargetTracking_18), (void*)value);
	}

	inline static int32_t get_offset_of_targetAffinity_19() { return static_cast<int32_t>(offsetof(RandomEyes2D_t253864BE689DE7E477335D17ECD2EC77001B377F, ___targetAffinity_19)); }
	inline bool get_targetAffinity_19() const { return ___targetAffinity_19; }
	inline bool* get_address_of_targetAffinity_19() { return &___targetAffinity_19; }
	inline void set_targetAffinity_19(bool value)
	{
		___targetAffinity_19 = value;
	}

	inline static int32_t get_offset_of_targetAffinityPercentage_20() { return static_cast<int32_t>(offsetof(RandomEyes2D_t253864BE689DE7E477335D17ECD2EC77001B377F, ___targetAffinityPercentage_20)); }
	inline float get_targetAffinityPercentage_20() const { return ___targetAffinityPercentage_20; }
	inline float* get_address_of_targetAffinityPercentage_20() { return &___targetAffinityPercentage_20; }
	inline void set_targetAffinityPercentage_20(float value)
	{
		___targetAffinityPercentage_20 = value;
	}

	inline static int32_t get_offset_of_randomEyes_21() { return static_cast<int32_t>(offsetof(RandomEyes2D_t253864BE689DE7E477335D17ECD2EC77001B377F, ___randomEyes_21)); }
	inline bool get_randomEyes_21() const { return ___randomEyes_21; }
	inline bool* get_address_of_randomEyes_21() { return &___randomEyes_21; }
	inline void set_randomEyes_21(bool value)
	{
		___randomEyes_21 = value;
	}

	inline static int32_t get_offset_of_maxRandomInterval_22() { return static_cast<int32_t>(offsetof(RandomEyes2D_t253864BE689DE7E477335D17ECD2EC77001B377F, ___maxRandomInterval_22)); }
	inline float get_maxRandomInterval_22() const { return ___maxRandomInterval_22; }
	inline float* get_address_of_maxRandomInterval_22() { return &___maxRandomInterval_22; }
	inline void set_maxRandomInterval_22(float value)
	{
		___maxRandomInterval_22 = value;
	}

	inline static int32_t get_offset_of_blendSpeed_23() { return static_cast<int32_t>(offsetof(RandomEyes2D_t253864BE689DE7E477335D17ECD2EC77001B377F, ___blendSpeed_23)); }
	inline float get_blendSpeed_23() const { return ___blendSpeed_23; }
	inline float* get_address_of_blendSpeed_23() { return &___blendSpeed_23; }
	inline void set_blendSpeed_23(float value)
	{
		___blendSpeed_23 = value;
	}

	inline static int32_t get_offset_of_randomBlink_24() { return static_cast<int32_t>(offsetof(RandomEyes2D_t253864BE689DE7E477335D17ECD2EC77001B377F, ___randomBlink_24)); }
	inline bool get_randomBlink_24() const { return ___randomBlink_24; }
	inline bool* get_address_of_randomBlink_24() { return &___randomBlink_24; }
	inline void set_randomBlink_24(bool value)
	{
		___randomBlink_24 = value;
	}

	inline static int32_t get_offset_of_maxBlinkInterval_25() { return static_cast<int32_t>(offsetof(RandomEyes2D_t253864BE689DE7E477335D17ECD2EC77001B377F, ___maxBlinkInterval_25)); }
	inline float get_maxBlinkInterval_25() const { return ___maxBlinkInterval_25; }
	inline float* get_address_of_maxBlinkInterval_25() { return &___maxBlinkInterval_25; }
	inline void set_maxBlinkInterval_25(float value)
	{
		___maxBlinkInterval_25 = value;
	}

	inline static int32_t get_offset_of_blinkDuration_26() { return static_cast<int32_t>(offsetof(RandomEyes2D_t253864BE689DE7E477335D17ECD2EC77001B377F, ___blinkDuration_26)); }
	inline float get_blinkDuration_26() const { return ___blinkDuration_26; }
	inline float* get_address_of_blinkDuration_26() { return &___blinkDuration_26; }
	inline void set_blinkDuration_26(float value)
	{
		___blinkDuration_26 = value;
	}

	inline static int32_t get_offset_of_expandEyes_27() { return static_cast<int32_t>(offsetof(RandomEyes2D_t253864BE689DE7E477335D17ECD2EC77001B377F, ___expandEyes_27)); }
	inline bool get_expandEyes_27() const { return ___expandEyes_27; }
	inline bool* get_address_of_expandEyes_27() { return &___expandEyes_27; }
	inline void set_expandEyes_27(bool value)
	{
		___expandEyes_27 = value;
	}

	inline static int32_t get_offset_of_expandEyelids_28() { return static_cast<int32_t>(offsetof(RandomEyes2D_t253864BE689DE7E477335D17ECD2EC77001B377F, ___expandEyelids_28)); }
	inline bool get_expandEyelids_28() const { return ___expandEyelids_28; }
	inline bool* get_address_of_expandEyelids_28() { return &___expandEyelids_28; }
	inline void set_expandEyelids_28(bool value)
	{
		___expandEyelids_28 = value;
	}

	inline static int32_t get_offset_of_showBroadcast_29() { return static_cast<int32_t>(offsetof(RandomEyes2D_t253864BE689DE7E477335D17ECD2EC77001B377F, ___showBroadcast_29)); }
	inline bool get_showBroadcast_29() const { return ___showBroadcast_29; }
	inline bool* get_address_of_showBroadcast_29() { return &___showBroadcast_29; }
	inline void set_showBroadcast_29(bool value)
	{
		___showBroadcast_29 = value;
	}

	inline static int32_t get_offset_of_trackDirection_30() { return static_cast<int32_t>(offsetof(RandomEyes2D_t253864BE689DE7E477335D17ECD2EC77001B377F, ___trackDirection_30)); }
	inline Vector3U5BU5D_t5FB88EAA33E46838BDC2ABDAEA3E8727491CB9E4* get_trackDirection_30() const { return ___trackDirection_30; }
	inline Vector3U5BU5D_t5FB88EAA33E46838BDC2ABDAEA3E8727491CB9E4** get_address_of_trackDirection_30() { return &___trackDirection_30; }
	inline void set_trackDirection_30(Vector3U5BU5D_t5FB88EAA33E46838BDC2ABDAEA3E8727491CB9E4* value)
	{
		___trackDirection_30 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___trackDirection_30), (void*)value);
	}

	inline static int32_t get_offset_of_randomEyes2DGizmos_31() { return static_cast<int32_t>(offsetof(RandomEyes2D_t253864BE689DE7E477335D17ECD2EC77001B377F, ___randomEyes2DGizmos_31)); }
	inline GameObjectU5BU5D_tA88FC1A1FC9D4D73D0B3984D4B0ECE88F4C47642* get_randomEyes2DGizmos_31() const { return ___randomEyes2DGizmos_31; }
	inline GameObjectU5BU5D_tA88FC1A1FC9D4D73D0B3984D4B0ECE88F4C47642** get_address_of_randomEyes2DGizmos_31() { return &___randomEyes2DGizmos_31; }
	inline void set_randomEyes2DGizmos_31(GameObjectU5BU5D_tA88FC1A1FC9D4D73D0B3984D4B0ECE88F4C47642* value)
	{
		___randomEyes2DGizmos_31 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___randomEyes2DGizmos_31), (void*)value);
	}

	inline static int32_t get_offset_of_targetAffinityTimerRange_32() { return static_cast<int32_t>(offsetof(RandomEyes2D_t253864BE689DE7E477335D17ECD2EC77001B377F, ___targetAffinityTimerRange_32)); }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  get_targetAffinityTimerRange_32() const { return ___targetAffinityTimerRange_32; }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 * get_address_of_targetAffinityTimerRange_32() { return &___targetAffinityTimerRange_32; }
	inline void set_targetAffinityTimerRange_32(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  value)
	{
		___targetAffinityTimerRange_32 = value;
	}

	inline static int32_t get_offset_of_targetAffinityTimer_33() { return static_cast<int32_t>(offsetof(RandomEyes2D_t253864BE689DE7E477335D17ECD2EC77001B377F, ___targetAffinityTimer_33)); }
	inline float get_targetAffinityTimer_33() const { return ___targetAffinityTimer_33; }
	inline float* get_address_of_targetAffinityTimer_33() { return &___targetAffinityTimer_33; }
	inline void set_targetAffinityTimer_33(float value)
	{
		___targetAffinityTimer_33 = value;
	}

	inline static int32_t get_offset_of_hasAffinity_34() { return static_cast<int32_t>(offsetof(RandomEyes2D_t253864BE689DE7E477335D17ECD2EC77001B377F, ___hasAffinity_34)); }
	inline bool get_hasAffinity_34() const { return ___hasAffinity_34; }
	inline bool* get_address_of_hasAffinity_34() { return &___hasAffinity_34; }
	inline void set_hasAffinity_34(bool value)
	{
		___hasAffinity_34 = value;
	}

	inline static int32_t get_offset_of_randomTimer_35() { return static_cast<int32_t>(offsetof(RandomEyes2D_t253864BE689DE7E477335D17ECD2EC77001B377F, ___randomTimer_35)); }
	inline float get_randomTimer_35() const { return ___randomTimer_35; }
	inline float* get_address_of_randomTimer_35() { return &___randomTimer_35; }
	inline void set_randomTimer_35(float value)
	{
		___randomTimer_35 = value;
	}

	inline static int32_t get_offset_of_blinkTimer_36() { return static_cast<int32_t>(offsetof(RandomEyes2D_t253864BE689DE7E477335D17ECD2EC77001B377F, ___blinkTimer_36)); }
	inline float get_blinkTimer_36() const { return ___blinkTimer_36; }
	inline float* get_address_of_blinkTimer_36() { return &___blinkTimer_36; }
	inline void set_blinkTimer_36(float value)
	{
		___blinkTimer_36 = value;
	}

	inline static int32_t get_offset_of_largestEyeIndex_37() { return static_cast<int32_t>(offsetof(RandomEyes2D_t253864BE689DE7E477335D17ECD2EC77001B377F, ___largestEyeIndex_37)); }
	inline int32_t get_largestEyeIndex_37() const { return ___largestEyeIndex_37; }
	inline int32_t* get_address_of_largestEyeIndex_37() { return &___largestEyeIndex_37; }
	inline void set_largestEyeIndex_37(int32_t value)
	{
		___largestEyeIndex_37 = value;
	}

	inline static int32_t get_offset_of_largestEye_38() { return static_cast<int32_t>(offsetof(RandomEyes2D_t253864BE689DE7E477335D17ECD2EC77001B377F, ___largestEye_38)); }
	inline SpriteRenderer_t3F35AD5498243C170B46F5FFDB582AAEF78615EF * get_largestEye_38() const { return ___largestEye_38; }
	inline SpriteRenderer_t3F35AD5498243C170B46F5FFDB582AAEF78615EF ** get_address_of_largestEye_38() { return &___largestEye_38; }
	inline void set_largestEye_38(SpriteRenderer_t3F35AD5498243C170B46F5FFDB582AAEF78615EF * value)
	{
		___largestEye_38 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___largestEye_38), (void*)value);
	}
};

struct RandomEyes2D_t253864BE689DE7E477335D17ECD2EC77001B377F_StaticFields
{
public:
	// CrazyMinnow.SALSA.RandomEyes2D CrazyMinnow.SALSA.RandomEyes2D::instance
	RandomEyes2D_t253864BE689DE7E477335D17ECD2EC77001B377F * ___instance_4;

public:
	inline static int32_t get_offset_of_instance_4() { return static_cast<int32_t>(offsetof(RandomEyes2D_t253864BE689DE7E477335D17ECD2EC77001B377F_StaticFields, ___instance_4)); }
	inline RandomEyes2D_t253864BE689DE7E477335D17ECD2EC77001B377F * get_instance_4() const { return ___instance_4; }
	inline RandomEyes2D_t253864BE689DE7E477335D17ECD2EC77001B377F ** get_address_of_instance_4() { return &___instance_4; }
	inline void set_instance_4(RandomEyes2D_t253864BE689DE7E477335D17ECD2EC77001B377F * value)
	{
		___instance_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___instance_4), (void*)value);
	}
};


// CrazyMinnow.SALSA.RandomEyes2DGizmo
struct RandomEyes2DGizmo_t241646E16376D86639D345C821DF03D9EDFDDDDD  : public MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A
{
public:

public:
};

struct RandomEyes2DGizmo_t241646E16376D86639D345C821DF03D9EDFDDDDD_StaticFields
{
public:
	// CrazyMinnow.SALSA.RandomEyes3DGizmo CrazyMinnow.SALSA.RandomEyes2DGizmo::instance
	RandomEyes3DGizmo_t039AFDA0DE2968F2C3DFB63606D40A2C238839A1 * ___instance_4;

public:
	inline static int32_t get_offset_of_instance_4() { return static_cast<int32_t>(offsetof(RandomEyes2DGizmo_t241646E16376D86639D345C821DF03D9EDFDDDDD_StaticFields, ___instance_4)); }
	inline RandomEyes3DGizmo_t039AFDA0DE2968F2C3DFB63606D40A2C238839A1 * get_instance_4() const { return ___instance_4; }
	inline RandomEyes3DGizmo_t039AFDA0DE2968F2C3DFB63606D40A2C238839A1 ** get_address_of_instance_4() { return &___instance_4; }
	inline void set_instance_4(RandomEyes3DGizmo_t039AFDA0DE2968F2C3DFB63606D40A2C238839A1 * value)
	{
		___instance_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___instance_4), (void*)value);
	}
};


// CrazyMinnow.SALSA.RandomEyes3D
struct RandomEyes3D_t6868157C4605BD0225E1B08301F110E72336CE45  : public MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A
{
public:
	// CrazyMinnow.SALSA.Salsa3D CrazyMinnow.SALSA.RandomEyes3D::salsa3D
	Salsa3D_tACF83D529139263662FEAAC8F5B53EC43B140C9C * ___salsa3D_5;
	// UnityEngine.SkinnedMeshRenderer CrazyMinnow.SALSA.RandomEyes3D::skinnedMeshRenderer
	SkinnedMeshRenderer_t126F4D6010E0F4B2685A7817B0A9171805D8F496 * ___skinnedMeshRenderer_6;
	// UnityEngine.SkinnedMeshRenderer CrazyMinnow.SALSA.RandomEyes3D::prevSkinnedMeshRenderer
	SkinnedMeshRenderer_t126F4D6010E0F4B2685A7817B0A9171805D8F496 * ___prevSkinnedMeshRenderer_7;
	// System.Boolean CrazyMinnow.SALSA.RandomEyes3D::getSkndMshRndr
	bool ___getSkndMshRndr_8;
	// System.String[] CrazyMinnow.SALSA.RandomEyes3D::blendShapes
	StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* ___blendShapes_9;
	// System.Int32 CrazyMinnow.SALSA.RandomEyes3D::lookUpIndex
	int32_t ___lookUpIndex_10;
	// System.Int32 CrazyMinnow.SALSA.RandomEyes3D::lookDownIndex
	int32_t ___lookDownIndex_11;
	// System.Int32 CrazyMinnow.SALSA.RandomEyes3D::lookLeftIndex
	int32_t ___lookLeftIndex_12;
	// System.Int32 CrazyMinnow.SALSA.RandomEyes3D::lookRightIndex
	int32_t ___lookRightIndex_13;
	// System.Int32 CrazyMinnow.SALSA.RandomEyes3D::blinkIndex
	int32_t ___blinkIndex_14;
	// UnityEngine.Vector3 CrazyMinnow.SALSA.RandomEyes3D::lookRandPosition
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___lookRandPosition_15;
	// CrazyMinnow.SALSA.RandomEyesBlendAmounts CrazyMinnow.SALSA.RandomEyes3D::lookAmount
	RandomEyesBlendAmounts_t728404128F97998E0D58F3849065DCD32A181621 * ___lookAmount_16;
	// System.Boolean CrazyMinnow.SALSA.RandomEyes3D::randomEyes
	bool ___randomEyes_17;
	// System.Single CrazyMinnow.SALSA.RandomEyes3D::maxRandomInterval
	float ___maxRandomInterval_18;
	// System.Single CrazyMinnow.SALSA.RandomEyes3D::blendSpeed
	float ___blendSpeed_19;
	// System.Single CrazyMinnow.SALSA.RandomEyes3D::rangeOfMotion
	float ___rangeOfMotion_20;
	// System.Single CrazyMinnow.SALSA.RandomEyes3D::blinkDuration
	float ___blinkDuration_21;
	// System.Single CrazyMinnow.SALSA.RandomEyes3D::blinkSpeed
	float ___blinkSpeed_22;
	// System.Single CrazyMinnow.SALSA.RandomEyes3D::openMax
	float ___openMax_23;
	// System.Single CrazyMinnow.SALSA.RandomEyes3D::closeMax
	float ___closeMax_24;
	// UnityEngine.GameObject CrazyMinnow.SALSA.RandomEyes3D::eyePosition
	GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * ___eyePosition_25;
	// UnityEngine.GameObject CrazyMinnow.SALSA.RandomEyes3D::eyePosVerify
	GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * ___eyePosVerify_26;
	// UnityEngine.GameObject CrazyMinnow.SALSA.RandomEyes3D::lookTarget
	GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * ___lookTarget_27;
	// UnityEngine.GameObject CrazyMinnow.SALSA.RandomEyes3D::lookTargetTracking
	GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * ___lookTargetTracking_28;
	// System.Boolean CrazyMinnow.SALSA.RandomEyes3D::targetAffinity
	bool ___targetAffinity_29;
	// System.Single CrazyMinnow.SALSA.RandomEyes3D::targetAffinityPercentage
	float ___targetAffinityPercentage_30;
	// System.Boolean CrazyMinnow.SALSA.RandomEyes3D::invertTracking
	bool ___invertTracking_31;
	// System.Boolean CrazyMinnow.SALSA.RandomEyes3D::trackBehind
	bool ___trackBehind_32;
	// System.Boolean CrazyMinnow.SALSA.RandomEyes3D::randomBlink
	bool ___randomBlink_33;
	// System.Single CrazyMinnow.SALSA.RandomEyes3D::maxBlinkInterval
	float ___maxBlinkInterval_34;
	// System.Boolean CrazyMinnow.SALSA.RandomEyes3D::broadcastNeeded
	bool ___broadcastNeeded_35;
	// System.Boolean CrazyMinnow.SALSA.RandomEyes3D::broadcastCS
	bool ___broadcastCS_36;
	// System.Int32 CrazyMinnow.SALSA.RandomEyes3D::broadcastCSReceiversCount
	int32_t ___broadcastCSReceiversCount_37;
	// System.Int32 CrazyMinnow.SALSA.RandomEyes3D::prevBroadcastCSReceiversCount
	int32_t ___prevBroadcastCSReceiversCount_38;
	// System.Boolean CrazyMinnow.SALSA.RandomEyes3D::expandBroadcastCS
	bool ___expandBroadcastCS_39;
	// UnityEngine.GameObject[] CrazyMinnow.SALSA.RandomEyes3D::broadcastCSReceivers
	GameObjectU5BU5D_tA88FC1A1FC9D4D73D0B3984D4B0ECE88F4C47642* ___broadcastCSReceivers_40;
	// System.Boolean CrazyMinnow.SALSA.RandomEyes3D::propagateToChildrenCS
	bool ___propagateToChildrenCS_41;
	// System.Int32 CrazyMinnow.SALSA.RandomEyes3D::selectedCustomShape
	int32_t ___selectedCustomShape_42;
	// System.Int32 CrazyMinnow.SALSA.RandomEyes3D::noneShapeIndex
	int32_t ___noneShapeIndex_43;
	// System.Boolean CrazyMinnow.SALSA.RandomEyes3D::excludeEyeShapes
	bool ___excludeEyeShapes_44;
	// System.String[] CrazyMinnow.SALSA.RandomEyes3D::dynamicCustomShapes
	StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* ___dynamicCustomShapes_45;
	// System.Int32 CrazyMinnow.SALSA.RandomEyes3D::customShapeCount
	int32_t ___customShapeCount_46;
	// System.Boolean CrazyMinnow.SALSA.RandomEyes3D::expandCustomShapes
	bool ___expandCustomShapes_47;
	// CrazyMinnow.SALSA.RandomEyesCustomShape[] CrazyMinnow.SALSA.RandomEyes3D::customShapes
	RandomEyesCustomShapeU5BU5D_t8FB0C62923194ED52AE0B06A6249680C47CA152C* ___customShapes_48;
	// System.Boolean CrazyMinnow.SALSA.RandomEyes3D::randomCustomShapes
	bool ___randomCustomShapes_49;
	// System.Single CrazyMinnow.SALSA.RandomEyes3D::maxCustomShapeInterval
	float ___maxCustomShapeInterval_50;
	// System.Single CrazyMinnow.SALSA.RandomEyes3D::randomCSBlendSpeedMin
	float ___randomCSBlendSpeedMin_51;
	// System.Single CrazyMinnow.SALSA.RandomEyes3D::randomCSBlendSpeedMax
	float ___randomCSBlendSpeedMax_52;
	// System.Single CrazyMinnow.SALSA.RandomEyes3D::randomCSRangeOfMotionMin
	float ___randomCSRangeOfMotionMin_53;
	// System.Single CrazyMinnow.SALSA.RandomEyes3D::randomCSRangeOfMotionMax
	float ___randomCSRangeOfMotionMax_54;
	// System.Single CrazyMinnow.SALSA.RandomEyes3D::randomCSDurationMin
	float ___randomCSDurationMin_55;
	// System.Single CrazyMinnow.SALSA.RandomEyes3D::randomCSDurationMax
	float ___randomCSDurationMax_56;
	// System.Boolean CrazyMinnow.SALSA.RandomEyes3D::csNotRandomToggle
	bool ___csNotRandomToggle_57;
	// System.Boolean CrazyMinnow.SALSA.RandomEyes3D::useCustomShapesOnly
	bool ___useCustomShapesOnly_58;
	// System.Boolean CrazyMinnow.SALSA.RandomEyes3D::showEyeShapes
	bool ___showEyeShapes_59;
	// System.Boolean CrazyMinnow.SALSA.RandomEyes3D::showTracking
	bool ___showTracking_60;
	// System.Boolean CrazyMinnow.SALSA.RandomEyes3D::showEyeProps
	bool ___showEyeProps_61;
	// System.Boolean CrazyMinnow.SALSA.RandomEyes3D::showCustom
	bool ___showCustom_62;
	// System.Boolean CrazyMinnow.SALSA.RandomEyes3D::showBroadcast
	bool ___showBroadcast_63;
	// System.Boolean CrazyMinnow.SALSA.RandomEyes3D::showBroadcastCS
	bool ___showBroadcastCS_64;
	// System.Boolean CrazyMinnow.SALSA.RandomEyes3D::expandShapeGroups
	bool ___expandShapeGroups_65;
	// System.Collections.Generic.List`1<CrazyMinnow.SALSA.Group> CrazyMinnow.SALSA.RandomEyes3D::groups
	List_1_t4CEBDA821397C4F46F5C0D752B030DA847F86F42 * ___groups_66;
	// System.Int32 CrazyMinnow.SALSA.RandomEyes3D::shapeGroupCount
	int32_t ___shapeGroupCount_67;
	// UnityEngine.Vector3 CrazyMinnow.SALSA.RandomEyes3D::lookDirection
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___lookDirection_68;
	// System.Single CrazyMinnow.SALSA.RandomEyes3D::randomLookRange
	float ___randomLookRange_69;
	// System.Single CrazyMinnow.SALSA.RandomEyes3D::currentUpVal
	float ___currentUpVal_70;
	// System.Single CrazyMinnow.SALSA.RandomEyes3D::currentRightVal
	float ___currentRightVal_71;
	// System.Single CrazyMinnow.SALSA.RandomEyes3D::currentLeftVal
	float ___currentLeftVal_72;
	// System.Single CrazyMinnow.SALSA.RandomEyes3D::currentDownVal
	float ___currentDownVal_73;
	// UnityEngine.Vector2 CrazyMinnow.SALSA.RandomEyes3D::targetAffinityTimerRange
	Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ___targetAffinityTimerRange_74;
	// System.Single CrazyMinnow.SALSA.RandomEyes3D::targetAffinityTimer
	float ___targetAffinityTimer_75;
	// System.Boolean CrazyMinnow.SALSA.RandomEyes3D::hasAffinity
	bool ___hasAffinity_76;
	// System.Int32 CrazyMinnow.SALSA.RandomEyes3D::randomShapeIndex
	int32_t ___randomShapeIndex_77;
	// System.Single CrazyMinnow.SALSA.RandomEyes3D::randomTimer
	float ___randomTimer_78;
	// System.Single CrazyMinnow.SALSA.RandomEyes3D::blinkTimer
	float ___blinkTimer_79;
	// System.Boolean CrazyMinnow.SALSA.RandomEyes3D::isBlinking
	bool ___isBlinking_80;
	// System.Boolean CrazyMinnow.SALSA.RandomEyes3D::inBlinkSequence
	bool ___inBlinkSequence_81;
	// System.Single CrazyMinnow.SALSA.RandomEyes3D::currentCloseAmount
	float ___currentCloseAmount_82;
	// CrazyMinnow.SALSA.RandomEyesCustomShapeStatus CrazyMinnow.SALSA.RandomEyes3D::customShape
	RandomEyesCustomShapeStatus_tAE795B1C1EF38DFDE0535D6A919E25A96283DD66 * ___customShape_83;
	// System.Single CrazyMinnow.SALSA.RandomEyes3D::customShapeTimer
	float ___customShapeTimer_84;
	// System.Boolean CrazyMinnow.SALSA.RandomEyes3D::prevRandomCustomShapes
	bool ___prevRandomCustomShapes_85;
	// System.String CrazyMinnow.SALSA.RandomEyes3D::randomShape
	String_t* ___randomShape_86;
	// System.Int32 CrazyMinnow.SALSA.RandomEyes3D::prevCustomShapeCount
	int32_t ___prevCustomShapeCount_87;
	// System.Boolean[] CrazyMinnow.SALSA.RandomEyes3D::prevIsOn
	BooleanU5BU5D_tEC7BAF93C44F875016DAADC8696EE3A465644D3C* ___prevIsOn_88;

public:
	inline static int32_t get_offset_of_salsa3D_5() { return static_cast<int32_t>(offsetof(RandomEyes3D_t6868157C4605BD0225E1B08301F110E72336CE45, ___salsa3D_5)); }
	inline Salsa3D_tACF83D529139263662FEAAC8F5B53EC43B140C9C * get_salsa3D_5() const { return ___salsa3D_5; }
	inline Salsa3D_tACF83D529139263662FEAAC8F5B53EC43B140C9C ** get_address_of_salsa3D_5() { return &___salsa3D_5; }
	inline void set_salsa3D_5(Salsa3D_tACF83D529139263662FEAAC8F5B53EC43B140C9C * value)
	{
		___salsa3D_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___salsa3D_5), (void*)value);
	}

	inline static int32_t get_offset_of_skinnedMeshRenderer_6() { return static_cast<int32_t>(offsetof(RandomEyes3D_t6868157C4605BD0225E1B08301F110E72336CE45, ___skinnedMeshRenderer_6)); }
	inline SkinnedMeshRenderer_t126F4D6010E0F4B2685A7817B0A9171805D8F496 * get_skinnedMeshRenderer_6() const { return ___skinnedMeshRenderer_6; }
	inline SkinnedMeshRenderer_t126F4D6010E0F4B2685A7817B0A9171805D8F496 ** get_address_of_skinnedMeshRenderer_6() { return &___skinnedMeshRenderer_6; }
	inline void set_skinnedMeshRenderer_6(SkinnedMeshRenderer_t126F4D6010E0F4B2685A7817B0A9171805D8F496 * value)
	{
		___skinnedMeshRenderer_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___skinnedMeshRenderer_6), (void*)value);
	}

	inline static int32_t get_offset_of_prevSkinnedMeshRenderer_7() { return static_cast<int32_t>(offsetof(RandomEyes3D_t6868157C4605BD0225E1B08301F110E72336CE45, ___prevSkinnedMeshRenderer_7)); }
	inline SkinnedMeshRenderer_t126F4D6010E0F4B2685A7817B0A9171805D8F496 * get_prevSkinnedMeshRenderer_7() const { return ___prevSkinnedMeshRenderer_7; }
	inline SkinnedMeshRenderer_t126F4D6010E0F4B2685A7817B0A9171805D8F496 ** get_address_of_prevSkinnedMeshRenderer_7() { return &___prevSkinnedMeshRenderer_7; }
	inline void set_prevSkinnedMeshRenderer_7(SkinnedMeshRenderer_t126F4D6010E0F4B2685A7817B0A9171805D8F496 * value)
	{
		___prevSkinnedMeshRenderer_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___prevSkinnedMeshRenderer_7), (void*)value);
	}

	inline static int32_t get_offset_of_getSkndMshRndr_8() { return static_cast<int32_t>(offsetof(RandomEyes3D_t6868157C4605BD0225E1B08301F110E72336CE45, ___getSkndMshRndr_8)); }
	inline bool get_getSkndMshRndr_8() const { return ___getSkndMshRndr_8; }
	inline bool* get_address_of_getSkndMshRndr_8() { return &___getSkndMshRndr_8; }
	inline void set_getSkndMshRndr_8(bool value)
	{
		___getSkndMshRndr_8 = value;
	}

	inline static int32_t get_offset_of_blendShapes_9() { return static_cast<int32_t>(offsetof(RandomEyes3D_t6868157C4605BD0225E1B08301F110E72336CE45, ___blendShapes_9)); }
	inline StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* get_blendShapes_9() const { return ___blendShapes_9; }
	inline StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A** get_address_of_blendShapes_9() { return &___blendShapes_9; }
	inline void set_blendShapes_9(StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* value)
	{
		___blendShapes_9 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___blendShapes_9), (void*)value);
	}

	inline static int32_t get_offset_of_lookUpIndex_10() { return static_cast<int32_t>(offsetof(RandomEyes3D_t6868157C4605BD0225E1B08301F110E72336CE45, ___lookUpIndex_10)); }
	inline int32_t get_lookUpIndex_10() const { return ___lookUpIndex_10; }
	inline int32_t* get_address_of_lookUpIndex_10() { return &___lookUpIndex_10; }
	inline void set_lookUpIndex_10(int32_t value)
	{
		___lookUpIndex_10 = value;
	}

	inline static int32_t get_offset_of_lookDownIndex_11() { return static_cast<int32_t>(offsetof(RandomEyes3D_t6868157C4605BD0225E1B08301F110E72336CE45, ___lookDownIndex_11)); }
	inline int32_t get_lookDownIndex_11() const { return ___lookDownIndex_11; }
	inline int32_t* get_address_of_lookDownIndex_11() { return &___lookDownIndex_11; }
	inline void set_lookDownIndex_11(int32_t value)
	{
		___lookDownIndex_11 = value;
	}

	inline static int32_t get_offset_of_lookLeftIndex_12() { return static_cast<int32_t>(offsetof(RandomEyes3D_t6868157C4605BD0225E1B08301F110E72336CE45, ___lookLeftIndex_12)); }
	inline int32_t get_lookLeftIndex_12() const { return ___lookLeftIndex_12; }
	inline int32_t* get_address_of_lookLeftIndex_12() { return &___lookLeftIndex_12; }
	inline void set_lookLeftIndex_12(int32_t value)
	{
		___lookLeftIndex_12 = value;
	}

	inline static int32_t get_offset_of_lookRightIndex_13() { return static_cast<int32_t>(offsetof(RandomEyes3D_t6868157C4605BD0225E1B08301F110E72336CE45, ___lookRightIndex_13)); }
	inline int32_t get_lookRightIndex_13() const { return ___lookRightIndex_13; }
	inline int32_t* get_address_of_lookRightIndex_13() { return &___lookRightIndex_13; }
	inline void set_lookRightIndex_13(int32_t value)
	{
		___lookRightIndex_13 = value;
	}

	inline static int32_t get_offset_of_blinkIndex_14() { return static_cast<int32_t>(offsetof(RandomEyes3D_t6868157C4605BD0225E1B08301F110E72336CE45, ___blinkIndex_14)); }
	inline int32_t get_blinkIndex_14() const { return ___blinkIndex_14; }
	inline int32_t* get_address_of_blinkIndex_14() { return &___blinkIndex_14; }
	inline void set_blinkIndex_14(int32_t value)
	{
		___blinkIndex_14 = value;
	}

	inline static int32_t get_offset_of_lookRandPosition_15() { return static_cast<int32_t>(offsetof(RandomEyes3D_t6868157C4605BD0225E1B08301F110E72336CE45, ___lookRandPosition_15)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_lookRandPosition_15() const { return ___lookRandPosition_15; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_lookRandPosition_15() { return &___lookRandPosition_15; }
	inline void set_lookRandPosition_15(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___lookRandPosition_15 = value;
	}

	inline static int32_t get_offset_of_lookAmount_16() { return static_cast<int32_t>(offsetof(RandomEyes3D_t6868157C4605BD0225E1B08301F110E72336CE45, ___lookAmount_16)); }
	inline RandomEyesBlendAmounts_t728404128F97998E0D58F3849065DCD32A181621 * get_lookAmount_16() const { return ___lookAmount_16; }
	inline RandomEyesBlendAmounts_t728404128F97998E0D58F3849065DCD32A181621 ** get_address_of_lookAmount_16() { return &___lookAmount_16; }
	inline void set_lookAmount_16(RandomEyesBlendAmounts_t728404128F97998E0D58F3849065DCD32A181621 * value)
	{
		___lookAmount_16 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___lookAmount_16), (void*)value);
	}

	inline static int32_t get_offset_of_randomEyes_17() { return static_cast<int32_t>(offsetof(RandomEyes3D_t6868157C4605BD0225E1B08301F110E72336CE45, ___randomEyes_17)); }
	inline bool get_randomEyes_17() const { return ___randomEyes_17; }
	inline bool* get_address_of_randomEyes_17() { return &___randomEyes_17; }
	inline void set_randomEyes_17(bool value)
	{
		___randomEyes_17 = value;
	}

	inline static int32_t get_offset_of_maxRandomInterval_18() { return static_cast<int32_t>(offsetof(RandomEyes3D_t6868157C4605BD0225E1B08301F110E72336CE45, ___maxRandomInterval_18)); }
	inline float get_maxRandomInterval_18() const { return ___maxRandomInterval_18; }
	inline float* get_address_of_maxRandomInterval_18() { return &___maxRandomInterval_18; }
	inline void set_maxRandomInterval_18(float value)
	{
		___maxRandomInterval_18 = value;
	}

	inline static int32_t get_offset_of_blendSpeed_19() { return static_cast<int32_t>(offsetof(RandomEyes3D_t6868157C4605BD0225E1B08301F110E72336CE45, ___blendSpeed_19)); }
	inline float get_blendSpeed_19() const { return ___blendSpeed_19; }
	inline float* get_address_of_blendSpeed_19() { return &___blendSpeed_19; }
	inline void set_blendSpeed_19(float value)
	{
		___blendSpeed_19 = value;
	}

	inline static int32_t get_offset_of_rangeOfMotion_20() { return static_cast<int32_t>(offsetof(RandomEyes3D_t6868157C4605BD0225E1B08301F110E72336CE45, ___rangeOfMotion_20)); }
	inline float get_rangeOfMotion_20() const { return ___rangeOfMotion_20; }
	inline float* get_address_of_rangeOfMotion_20() { return &___rangeOfMotion_20; }
	inline void set_rangeOfMotion_20(float value)
	{
		___rangeOfMotion_20 = value;
	}

	inline static int32_t get_offset_of_blinkDuration_21() { return static_cast<int32_t>(offsetof(RandomEyes3D_t6868157C4605BD0225E1B08301F110E72336CE45, ___blinkDuration_21)); }
	inline float get_blinkDuration_21() const { return ___blinkDuration_21; }
	inline float* get_address_of_blinkDuration_21() { return &___blinkDuration_21; }
	inline void set_blinkDuration_21(float value)
	{
		___blinkDuration_21 = value;
	}

	inline static int32_t get_offset_of_blinkSpeed_22() { return static_cast<int32_t>(offsetof(RandomEyes3D_t6868157C4605BD0225E1B08301F110E72336CE45, ___blinkSpeed_22)); }
	inline float get_blinkSpeed_22() const { return ___blinkSpeed_22; }
	inline float* get_address_of_blinkSpeed_22() { return &___blinkSpeed_22; }
	inline void set_blinkSpeed_22(float value)
	{
		___blinkSpeed_22 = value;
	}

	inline static int32_t get_offset_of_openMax_23() { return static_cast<int32_t>(offsetof(RandomEyes3D_t6868157C4605BD0225E1B08301F110E72336CE45, ___openMax_23)); }
	inline float get_openMax_23() const { return ___openMax_23; }
	inline float* get_address_of_openMax_23() { return &___openMax_23; }
	inline void set_openMax_23(float value)
	{
		___openMax_23 = value;
	}

	inline static int32_t get_offset_of_closeMax_24() { return static_cast<int32_t>(offsetof(RandomEyes3D_t6868157C4605BD0225E1B08301F110E72336CE45, ___closeMax_24)); }
	inline float get_closeMax_24() const { return ___closeMax_24; }
	inline float* get_address_of_closeMax_24() { return &___closeMax_24; }
	inline void set_closeMax_24(float value)
	{
		___closeMax_24 = value;
	}

	inline static int32_t get_offset_of_eyePosition_25() { return static_cast<int32_t>(offsetof(RandomEyes3D_t6868157C4605BD0225E1B08301F110E72336CE45, ___eyePosition_25)); }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * get_eyePosition_25() const { return ___eyePosition_25; }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 ** get_address_of_eyePosition_25() { return &___eyePosition_25; }
	inline void set_eyePosition_25(GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * value)
	{
		___eyePosition_25 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___eyePosition_25), (void*)value);
	}

	inline static int32_t get_offset_of_eyePosVerify_26() { return static_cast<int32_t>(offsetof(RandomEyes3D_t6868157C4605BD0225E1B08301F110E72336CE45, ___eyePosVerify_26)); }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * get_eyePosVerify_26() const { return ___eyePosVerify_26; }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 ** get_address_of_eyePosVerify_26() { return &___eyePosVerify_26; }
	inline void set_eyePosVerify_26(GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * value)
	{
		___eyePosVerify_26 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___eyePosVerify_26), (void*)value);
	}

	inline static int32_t get_offset_of_lookTarget_27() { return static_cast<int32_t>(offsetof(RandomEyes3D_t6868157C4605BD0225E1B08301F110E72336CE45, ___lookTarget_27)); }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * get_lookTarget_27() const { return ___lookTarget_27; }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 ** get_address_of_lookTarget_27() { return &___lookTarget_27; }
	inline void set_lookTarget_27(GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * value)
	{
		___lookTarget_27 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___lookTarget_27), (void*)value);
	}

	inline static int32_t get_offset_of_lookTargetTracking_28() { return static_cast<int32_t>(offsetof(RandomEyes3D_t6868157C4605BD0225E1B08301F110E72336CE45, ___lookTargetTracking_28)); }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * get_lookTargetTracking_28() const { return ___lookTargetTracking_28; }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 ** get_address_of_lookTargetTracking_28() { return &___lookTargetTracking_28; }
	inline void set_lookTargetTracking_28(GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * value)
	{
		___lookTargetTracking_28 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___lookTargetTracking_28), (void*)value);
	}

	inline static int32_t get_offset_of_targetAffinity_29() { return static_cast<int32_t>(offsetof(RandomEyes3D_t6868157C4605BD0225E1B08301F110E72336CE45, ___targetAffinity_29)); }
	inline bool get_targetAffinity_29() const { return ___targetAffinity_29; }
	inline bool* get_address_of_targetAffinity_29() { return &___targetAffinity_29; }
	inline void set_targetAffinity_29(bool value)
	{
		___targetAffinity_29 = value;
	}

	inline static int32_t get_offset_of_targetAffinityPercentage_30() { return static_cast<int32_t>(offsetof(RandomEyes3D_t6868157C4605BD0225E1B08301F110E72336CE45, ___targetAffinityPercentage_30)); }
	inline float get_targetAffinityPercentage_30() const { return ___targetAffinityPercentage_30; }
	inline float* get_address_of_targetAffinityPercentage_30() { return &___targetAffinityPercentage_30; }
	inline void set_targetAffinityPercentage_30(float value)
	{
		___targetAffinityPercentage_30 = value;
	}

	inline static int32_t get_offset_of_invertTracking_31() { return static_cast<int32_t>(offsetof(RandomEyes3D_t6868157C4605BD0225E1B08301F110E72336CE45, ___invertTracking_31)); }
	inline bool get_invertTracking_31() const { return ___invertTracking_31; }
	inline bool* get_address_of_invertTracking_31() { return &___invertTracking_31; }
	inline void set_invertTracking_31(bool value)
	{
		___invertTracking_31 = value;
	}

	inline static int32_t get_offset_of_trackBehind_32() { return static_cast<int32_t>(offsetof(RandomEyes3D_t6868157C4605BD0225E1B08301F110E72336CE45, ___trackBehind_32)); }
	inline bool get_trackBehind_32() const { return ___trackBehind_32; }
	inline bool* get_address_of_trackBehind_32() { return &___trackBehind_32; }
	inline void set_trackBehind_32(bool value)
	{
		___trackBehind_32 = value;
	}

	inline static int32_t get_offset_of_randomBlink_33() { return static_cast<int32_t>(offsetof(RandomEyes3D_t6868157C4605BD0225E1B08301F110E72336CE45, ___randomBlink_33)); }
	inline bool get_randomBlink_33() const { return ___randomBlink_33; }
	inline bool* get_address_of_randomBlink_33() { return &___randomBlink_33; }
	inline void set_randomBlink_33(bool value)
	{
		___randomBlink_33 = value;
	}

	inline static int32_t get_offset_of_maxBlinkInterval_34() { return static_cast<int32_t>(offsetof(RandomEyes3D_t6868157C4605BD0225E1B08301F110E72336CE45, ___maxBlinkInterval_34)); }
	inline float get_maxBlinkInterval_34() const { return ___maxBlinkInterval_34; }
	inline float* get_address_of_maxBlinkInterval_34() { return &___maxBlinkInterval_34; }
	inline void set_maxBlinkInterval_34(float value)
	{
		___maxBlinkInterval_34 = value;
	}

	inline static int32_t get_offset_of_broadcastNeeded_35() { return static_cast<int32_t>(offsetof(RandomEyes3D_t6868157C4605BD0225E1B08301F110E72336CE45, ___broadcastNeeded_35)); }
	inline bool get_broadcastNeeded_35() const { return ___broadcastNeeded_35; }
	inline bool* get_address_of_broadcastNeeded_35() { return &___broadcastNeeded_35; }
	inline void set_broadcastNeeded_35(bool value)
	{
		___broadcastNeeded_35 = value;
	}

	inline static int32_t get_offset_of_broadcastCS_36() { return static_cast<int32_t>(offsetof(RandomEyes3D_t6868157C4605BD0225E1B08301F110E72336CE45, ___broadcastCS_36)); }
	inline bool get_broadcastCS_36() const { return ___broadcastCS_36; }
	inline bool* get_address_of_broadcastCS_36() { return &___broadcastCS_36; }
	inline void set_broadcastCS_36(bool value)
	{
		___broadcastCS_36 = value;
	}

	inline static int32_t get_offset_of_broadcastCSReceiversCount_37() { return static_cast<int32_t>(offsetof(RandomEyes3D_t6868157C4605BD0225E1B08301F110E72336CE45, ___broadcastCSReceiversCount_37)); }
	inline int32_t get_broadcastCSReceiversCount_37() const { return ___broadcastCSReceiversCount_37; }
	inline int32_t* get_address_of_broadcastCSReceiversCount_37() { return &___broadcastCSReceiversCount_37; }
	inline void set_broadcastCSReceiversCount_37(int32_t value)
	{
		___broadcastCSReceiversCount_37 = value;
	}

	inline static int32_t get_offset_of_prevBroadcastCSReceiversCount_38() { return static_cast<int32_t>(offsetof(RandomEyes3D_t6868157C4605BD0225E1B08301F110E72336CE45, ___prevBroadcastCSReceiversCount_38)); }
	inline int32_t get_prevBroadcastCSReceiversCount_38() const { return ___prevBroadcastCSReceiversCount_38; }
	inline int32_t* get_address_of_prevBroadcastCSReceiversCount_38() { return &___prevBroadcastCSReceiversCount_38; }
	inline void set_prevBroadcastCSReceiversCount_38(int32_t value)
	{
		___prevBroadcastCSReceiversCount_38 = value;
	}

	inline static int32_t get_offset_of_expandBroadcastCS_39() { return static_cast<int32_t>(offsetof(RandomEyes3D_t6868157C4605BD0225E1B08301F110E72336CE45, ___expandBroadcastCS_39)); }
	inline bool get_expandBroadcastCS_39() const { return ___expandBroadcastCS_39; }
	inline bool* get_address_of_expandBroadcastCS_39() { return &___expandBroadcastCS_39; }
	inline void set_expandBroadcastCS_39(bool value)
	{
		___expandBroadcastCS_39 = value;
	}

	inline static int32_t get_offset_of_broadcastCSReceivers_40() { return static_cast<int32_t>(offsetof(RandomEyes3D_t6868157C4605BD0225E1B08301F110E72336CE45, ___broadcastCSReceivers_40)); }
	inline GameObjectU5BU5D_tA88FC1A1FC9D4D73D0B3984D4B0ECE88F4C47642* get_broadcastCSReceivers_40() const { return ___broadcastCSReceivers_40; }
	inline GameObjectU5BU5D_tA88FC1A1FC9D4D73D0B3984D4B0ECE88F4C47642** get_address_of_broadcastCSReceivers_40() { return &___broadcastCSReceivers_40; }
	inline void set_broadcastCSReceivers_40(GameObjectU5BU5D_tA88FC1A1FC9D4D73D0B3984D4B0ECE88F4C47642* value)
	{
		___broadcastCSReceivers_40 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___broadcastCSReceivers_40), (void*)value);
	}

	inline static int32_t get_offset_of_propagateToChildrenCS_41() { return static_cast<int32_t>(offsetof(RandomEyes3D_t6868157C4605BD0225E1B08301F110E72336CE45, ___propagateToChildrenCS_41)); }
	inline bool get_propagateToChildrenCS_41() const { return ___propagateToChildrenCS_41; }
	inline bool* get_address_of_propagateToChildrenCS_41() { return &___propagateToChildrenCS_41; }
	inline void set_propagateToChildrenCS_41(bool value)
	{
		___propagateToChildrenCS_41 = value;
	}

	inline static int32_t get_offset_of_selectedCustomShape_42() { return static_cast<int32_t>(offsetof(RandomEyes3D_t6868157C4605BD0225E1B08301F110E72336CE45, ___selectedCustomShape_42)); }
	inline int32_t get_selectedCustomShape_42() const { return ___selectedCustomShape_42; }
	inline int32_t* get_address_of_selectedCustomShape_42() { return &___selectedCustomShape_42; }
	inline void set_selectedCustomShape_42(int32_t value)
	{
		___selectedCustomShape_42 = value;
	}

	inline static int32_t get_offset_of_noneShapeIndex_43() { return static_cast<int32_t>(offsetof(RandomEyes3D_t6868157C4605BD0225E1B08301F110E72336CE45, ___noneShapeIndex_43)); }
	inline int32_t get_noneShapeIndex_43() const { return ___noneShapeIndex_43; }
	inline int32_t* get_address_of_noneShapeIndex_43() { return &___noneShapeIndex_43; }
	inline void set_noneShapeIndex_43(int32_t value)
	{
		___noneShapeIndex_43 = value;
	}

	inline static int32_t get_offset_of_excludeEyeShapes_44() { return static_cast<int32_t>(offsetof(RandomEyes3D_t6868157C4605BD0225E1B08301F110E72336CE45, ___excludeEyeShapes_44)); }
	inline bool get_excludeEyeShapes_44() const { return ___excludeEyeShapes_44; }
	inline bool* get_address_of_excludeEyeShapes_44() { return &___excludeEyeShapes_44; }
	inline void set_excludeEyeShapes_44(bool value)
	{
		___excludeEyeShapes_44 = value;
	}

	inline static int32_t get_offset_of_dynamicCustomShapes_45() { return static_cast<int32_t>(offsetof(RandomEyes3D_t6868157C4605BD0225E1B08301F110E72336CE45, ___dynamicCustomShapes_45)); }
	inline StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* get_dynamicCustomShapes_45() const { return ___dynamicCustomShapes_45; }
	inline StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A** get_address_of_dynamicCustomShapes_45() { return &___dynamicCustomShapes_45; }
	inline void set_dynamicCustomShapes_45(StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* value)
	{
		___dynamicCustomShapes_45 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___dynamicCustomShapes_45), (void*)value);
	}

	inline static int32_t get_offset_of_customShapeCount_46() { return static_cast<int32_t>(offsetof(RandomEyes3D_t6868157C4605BD0225E1B08301F110E72336CE45, ___customShapeCount_46)); }
	inline int32_t get_customShapeCount_46() const { return ___customShapeCount_46; }
	inline int32_t* get_address_of_customShapeCount_46() { return &___customShapeCount_46; }
	inline void set_customShapeCount_46(int32_t value)
	{
		___customShapeCount_46 = value;
	}

	inline static int32_t get_offset_of_expandCustomShapes_47() { return static_cast<int32_t>(offsetof(RandomEyes3D_t6868157C4605BD0225E1B08301F110E72336CE45, ___expandCustomShapes_47)); }
	inline bool get_expandCustomShapes_47() const { return ___expandCustomShapes_47; }
	inline bool* get_address_of_expandCustomShapes_47() { return &___expandCustomShapes_47; }
	inline void set_expandCustomShapes_47(bool value)
	{
		___expandCustomShapes_47 = value;
	}

	inline static int32_t get_offset_of_customShapes_48() { return static_cast<int32_t>(offsetof(RandomEyes3D_t6868157C4605BD0225E1B08301F110E72336CE45, ___customShapes_48)); }
	inline RandomEyesCustomShapeU5BU5D_t8FB0C62923194ED52AE0B06A6249680C47CA152C* get_customShapes_48() const { return ___customShapes_48; }
	inline RandomEyesCustomShapeU5BU5D_t8FB0C62923194ED52AE0B06A6249680C47CA152C** get_address_of_customShapes_48() { return &___customShapes_48; }
	inline void set_customShapes_48(RandomEyesCustomShapeU5BU5D_t8FB0C62923194ED52AE0B06A6249680C47CA152C* value)
	{
		___customShapes_48 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___customShapes_48), (void*)value);
	}

	inline static int32_t get_offset_of_randomCustomShapes_49() { return static_cast<int32_t>(offsetof(RandomEyes3D_t6868157C4605BD0225E1B08301F110E72336CE45, ___randomCustomShapes_49)); }
	inline bool get_randomCustomShapes_49() const { return ___randomCustomShapes_49; }
	inline bool* get_address_of_randomCustomShapes_49() { return &___randomCustomShapes_49; }
	inline void set_randomCustomShapes_49(bool value)
	{
		___randomCustomShapes_49 = value;
	}

	inline static int32_t get_offset_of_maxCustomShapeInterval_50() { return static_cast<int32_t>(offsetof(RandomEyes3D_t6868157C4605BD0225E1B08301F110E72336CE45, ___maxCustomShapeInterval_50)); }
	inline float get_maxCustomShapeInterval_50() const { return ___maxCustomShapeInterval_50; }
	inline float* get_address_of_maxCustomShapeInterval_50() { return &___maxCustomShapeInterval_50; }
	inline void set_maxCustomShapeInterval_50(float value)
	{
		___maxCustomShapeInterval_50 = value;
	}

	inline static int32_t get_offset_of_randomCSBlendSpeedMin_51() { return static_cast<int32_t>(offsetof(RandomEyes3D_t6868157C4605BD0225E1B08301F110E72336CE45, ___randomCSBlendSpeedMin_51)); }
	inline float get_randomCSBlendSpeedMin_51() const { return ___randomCSBlendSpeedMin_51; }
	inline float* get_address_of_randomCSBlendSpeedMin_51() { return &___randomCSBlendSpeedMin_51; }
	inline void set_randomCSBlendSpeedMin_51(float value)
	{
		___randomCSBlendSpeedMin_51 = value;
	}

	inline static int32_t get_offset_of_randomCSBlendSpeedMax_52() { return static_cast<int32_t>(offsetof(RandomEyes3D_t6868157C4605BD0225E1B08301F110E72336CE45, ___randomCSBlendSpeedMax_52)); }
	inline float get_randomCSBlendSpeedMax_52() const { return ___randomCSBlendSpeedMax_52; }
	inline float* get_address_of_randomCSBlendSpeedMax_52() { return &___randomCSBlendSpeedMax_52; }
	inline void set_randomCSBlendSpeedMax_52(float value)
	{
		___randomCSBlendSpeedMax_52 = value;
	}

	inline static int32_t get_offset_of_randomCSRangeOfMotionMin_53() { return static_cast<int32_t>(offsetof(RandomEyes3D_t6868157C4605BD0225E1B08301F110E72336CE45, ___randomCSRangeOfMotionMin_53)); }
	inline float get_randomCSRangeOfMotionMin_53() const { return ___randomCSRangeOfMotionMin_53; }
	inline float* get_address_of_randomCSRangeOfMotionMin_53() { return &___randomCSRangeOfMotionMin_53; }
	inline void set_randomCSRangeOfMotionMin_53(float value)
	{
		___randomCSRangeOfMotionMin_53 = value;
	}

	inline static int32_t get_offset_of_randomCSRangeOfMotionMax_54() { return static_cast<int32_t>(offsetof(RandomEyes3D_t6868157C4605BD0225E1B08301F110E72336CE45, ___randomCSRangeOfMotionMax_54)); }
	inline float get_randomCSRangeOfMotionMax_54() const { return ___randomCSRangeOfMotionMax_54; }
	inline float* get_address_of_randomCSRangeOfMotionMax_54() { return &___randomCSRangeOfMotionMax_54; }
	inline void set_randomCSRangeOfMotionMax_54(float value)
	{
		___randomCSRangeOfMotionMax_54 = value;
	}

	inline static int32_t get_offset_of_randomCSDurationMin_55() { return static_cast<int32_t>(offsetof(RandomEyes3D_t6868157C4605BD0225E1B08301F110E72336CE45, ___randomCSDurationMin_55)); }
	inline float get_randomCSDurationMin_55() const { return ___randomCSDurationMin_55; }
	inline float* get_address_of_randomCSDurationMin_55() { return &___randomCSDurationMin_55; }
	inline void set_randomCSDurationMin_55(float value)
	{
		___randomCSDurationMin_55 = value;
	}

	inline static int32_t get_offset_of_randomCSDurationMax_56() { return static_cast<int32_t>(offsetof(RandomEyes3D_t6868157C4605BD0225E1B08301F110E72336CE45, ___randomCSDurationMax_56)); }
	inline float get_randomCSDurationMax_56() const { return ___randomCSDurationMax_56; }
	inline float* get_address_of_randomCSDurationMax_56() { return &___randomCSDurationMax_56; }
	inline void set_randomCSDurationMax_56(float value)
	{
		___randomCSDurationMax_56 = value;
	}

	inline static int32_t get_offset_of_csNotRandomToggle_57() { return static_cast<int32_t>(offsetof(RandomEyes3D_t6868157C4605BD0225E1B08301F110E72336CE45, ___csNotRandomToggle_57)); }
	inline bool get_csNotRandomToggle_57() const { return ___csNotRandomToggle_57; }
	inline bool* get_address_of_csNotRandomToggle_57() { return &___csNotRandomToggle_57; }
	inline void set_csNotRandomToggle_57(bool value)
	{
		___csNotRandomToggle_57 = value;
	}

	inline static int32_t get_offset_of_useCustomShapesOnly_58() { return static_cast<int32_t>(offsetof(RandomEyes3D_t6868157C4605BD0225E1B08301F110E72336CE45, ___useCustomShapesOnly_58)); }
	inline bool get_useCustomShapesOnly_58() const { return ___useCustomShapesOnly_58; }
	inline bool* get_address_of_useCustomShapesOnly_58() { return &___useCustomShapesOnly_58; }
	inline void set_useCustomShapesOnly_58(bool value)
	{
		___useCustomShapesOnly_58 = value;
	}

	inline static int32_t get_offset_of_showEyeShapes_59() { return static_cast<int32_t>(offsetof(RandomEyes3D_t6868157C4605BD0225E1B08301F110E72336CE45, ___showEyeShapes_59)); }
	inline bool get_showEyeShapes_59() const { return ___showEyeShapes_59; }
	inline bool* get_address_of_showEyeShapes_59() { return &___showEyeShapes_59; }
	inline void set_showEyeShapes_59(bool value)
	{
		___showEyeShapes_59 = value;
	}

	inline static int32_t get_offset_of_showTracking_60() { return static_cast<int32_t>(offsetof(RandomEyes3D_t6868157C4605BD0225E1B08301F110E72336CE45, ___showTracking_60)); }
	inline bool get_showTracking_60() const { return ___showTracking_60; }
	inline bool* get_address_of_showTracking_60() { return &___showTracking_60; }
	inline void set_showTracking_60(bool value)
	{
		___showTracking_60 = value;
	}

	inline static int32_t get_offset_of_showEyeProps_61() { return static_cast<int32_t>(offsetof(RandomEyes3D_t6868157C4605BD0225E1B08301F110E72336CE45, ___showEyeProps_61)); }
	inline bool get_showEyeProps_61() const { return ___showEyeProps_61; }
	inline bool* get_address_of_showEyeProps_61() { return &___showEyeProps_61; }
	inline void set_showEyeProps_61(bool value)
	{
		___showEyeProps_61 = value;
	}

	inline static int32_t get_offset_of_showCustom_62() { return static_cast<int32_t>(offsetof(RandomEyes3D_t6868157C4605BD0225E1B08301F110E72336CE45, ___showCustom_62)); }
	inline bool get_showCustom_62() const { return ___showCustom_62; }
	inline bool* get_address_of_showCustom_62() { return &___showCustom_62; }
	inline void set_showCustom_62(bool value)
	{
		___showCustom_62 = value;
	}

	inline static int32_t get_offset_of_showBroadcast_63() { return static_cast<int32_t>(offsetof(RandomEyes3D_t6868157C4605BD0225E1B08301F110E72336CE45, ___showBroadcast_63)); }
	inline bool get_showBroadcast_63() const { return ___showBroadcast_63; }
	inline bool* get_address_of_showBroadcast_63() { return &___showBroadcast_63; }
	inline void set_showBroadcast_63(bool value)
	{
		___showBroadcast_63 = value;
	}

	inline static int32_t get_offset_of_showBroadcastCS_64() { return static_cast<int32_t>(offsetof(RandomEyes3D_t6868157C4605BD0225E1B08301F110E72336CE45, ___showBroadcastCS_64)); }
	inline bool get_showBroadcastCS_64() const { return ___showBroadcastCS_64; }
	inline bool* get_address_of_showBroadcastCS_64() { return &___showBroadcastCS_64; }
	inline void set_showBroadcastCS_64(bool value)
	{
		___showBroadcastCS_64 = value;
	}

	inline static int32_t get_offset_of_expandShapeGroups_65() { return static_cast<int32_t>(offsetof(RandomEyes3D_t6868157C4605BD0225E1B08301F110E72336CE45, ___expandShapeGroups_65)); }
	inline bool get_expandShapeGroups_65() const { return ___expandShapeGroups_65; }
	inline bool* get_address_of_expandShapeGroups_65() { return &___expandShapeGroups_65; }
	inline void set_expandShapeGroups_65(bool value)
	{
		___expandShapeGroups_65 = value;
	}

	inline static int32_t get_offset_of_groups_66() { return static_cast<int32_t>(offsetof(RandomEyes3D_t6868157C4605BD0225E1B08301F110E72336CE45, ___groups_66)); }
	inline List_1_t4CEBDA821397C4F46F5C0D752B030DA847F86F42 * get_groups_66() const { return ___groups_66; }
	inline List_1_t4CEBDA821397C4F46F5C0D752B030DA847F86F42 ** get_address_of_groups_66() { return &___groups_66; }
	inline void set_groups_66(List_1_t4CEBDA821397C4F46F5C0D752B030DA847F86F42 * value)
	{
		___groups_66 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___groups_66), (void*)value);
	}

	inline static int32_t get_offset_of_shapeGroupCount_67() { return static_cast<int32_t>(offsetof(RandomEyes3D_t6868157C4605BD0225E1B08301F110E72336CE45, ___shapeGroupCount_67)); }
	inline int32_t get_shapeGroupCount_67() const { return ___shapeGroupCount_67; }
	inline int32_t* get_address_of_shapeGroupCount_67() { return &___shapeGroupCount_67; }
	inline void set_shapeGroupCount_67(int32_t value)
	{
		___shapeGroupCount_67 = value;
	}

	inline static int32_t get_offset_of_lookDirection_68() { return static_cast<int32_t>(offsetof(RandomEyes3D_t6868157C4605BD0225E1B08301F110E72336CE45, ___lookDirection_68)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_lookDirection_68() const { return ___lookDirection_68; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_lookDirection_68() { return &___lookDirection_68; }
	inline void set_lookDirection_68(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___lookDirection_68 = value;
	}

	inline static int32_t get_offset_of_randomLookRange_69() { return static_cast<int32_t>(offsetof(RandomEyes3D_t6868157C4605BD0225E1B08301F110E72336CE45, ___randomLookRange_69)); }
	inline float get_randomLookRange_69() const { return ___randomLookRange_69; }
	inline float* get_address_of_randomLookRange_69() { return &___randomLookRange_69; }
	inline void set_randomLookRange_69(float value)
	{
		___randomLookRange_69 = value;
	}

	inline static int32_t get_offset_of_currentUpVal_70() { return static_cast<int32_t>(offsetof(RandomEyes3D_t6868157C4605BD0225E1B08301F110E72336CE45, ___currentUpVal_70)); }
	inline float get_currentUpVal_70() const { return ___currentUpVal_70; }
	inline float* get_address_of_currentUpVal_70() { return &___currentUpVal_70; }
	inline void set_currentUpVal_70(float value)
	{
		___currentUpVal_70 = value;
	}

	inline static int32_t get_offset_of_currentRightVal_71() { return static_cast<int32_t>(offsetof(RandomEyes3D_t6868157C4605BD0225E1B08301F110E72336CE45, ___currentRightVal_71)); }
	inline float get_currentRightVal_71() const { return ___currentRightVal_71; }
	inline float* get_address_of_currentRightVal_71() { return &___currentRightVal_71; }
	inline void set_currentRightVal_71(float value)
	{
		___currentRightVal_71 = value;
	}

	inline static int32_t get_offset_of_currentLeftVal_72() { return static_cast<int32_t>(offsetof(RandomEyes3D_t6868157C4605BD0225E1B08301F110E72336CE45, ___currentLeftVal_72)); }
	inline float get_currentLeftVal_72() const { return ___currentLeftVal_72; }
	inline float* get_address_of_currentLeftVal_72() { return &___currentLeftVal_72; }
	inline void set_currentLeftVal_72(float value)
	{
		___currentLeftVal_72 = value;
	}

	inline static int32_t get_offset_of_currentDownVal_73() { return static_cast<int32_t>(offsetof(RandomEyes3D_t6868157C4605BD0225E1B08301F110E72336CE45, ___currentDownVal_73)); }
	inline float get_currentDownVal_73() const { return ___currentDownVal_73; }
	inline float* get_address_of_currentDownVal_73() { return &___currentDownVal_73; }
	inline void set_currentDownVal_73(float value)
	{
		___currentDownVal_73 = value;
	}

	inline static int32_t get_offset_of_targetAffinityTimerRange_74() { return static_cast<int32_t>(offsetof(RandomEyes3D_t6868157C4605BD0225E1B08301F110E72336CE45, ___targetAffinityTimerRange_74)); }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  get_targetAffinityTimerRange_74() const { return ___targetAffinityTimerRange_74; }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 * get_address_of_targetAffinityTimerRange_74() { return &___targetAffinityTimerRange_74; }
	inline void set_targetAffinityTimerRange_74(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  value)
	{
		___targetAffinityTimerRange_74 = value;
	}

	inline static int32_t get_offset_of_targetAffinityTimer_75() { return static_cast<int32_t>(offsetof(RandomEyes3D_t6868157C4605BD0225E1B08301F110E72336CE45, ___targetAffinityTimer_75)); }
	inline float get_targetAffinityTimer_75() const { return ___targetAffinityTimer_75; }
	inline float* get_address_of_targetAffinityTimer_75() { return &___targetAffinityTimer_75; }
	inline void set_targetAffinityTimer_75(float value)
	{
		___targetAffinityTimer_75 = value;
	}

	inline static int32_t get_offset_of_hasAffinity_76() { return static_cast<int32_t>(offsetof(RandomEyes3D_t6868157C4605BD0225E1B08301F110E72336CE45, ___hasAffinity_76)); }
	inline bool get_hasAffinity_76() const { return ___hasAffinity_76; }
	inline bool* get_address_of_hasAffinity_76() { return &___hasAffinity_76; }
	inline void set_hasAffinity_76(bool value)
	{
		___hasAffinity_76 = value;
	}

	inline static int32_t get_offset_of_randomShapeIndex_77() { return static_cast<int32_t>(offsetof(RandomEyes3D_t6868157C4605BD0225E1B08301F110E72336CE45, ___randomShapeIndex_77)); }
	inline int32_t get_randomShapeIndex_77() const { return ___randomShapeIndex_77; }
	inline int32_t* get_address_of_randomShapeIndex_77() { return &___randomShapeIndex_77; }
	inline void set_randomShapeIndex_77(int32_t value)
	{
		___randomShapeIndex_77 = value;
	}

	inline static int32_t get_offset_of_randomTimer_78() { return static_cast<int32_t>(offsetof(RandomEyes3D_t6868157C4605BD0225E1B08301F110E72336CE45, ___randomTimer_78)); }
	inline float get_randomTimer_78() const { return ___randomTimer_78; }
	inline float* get_address_of_randomTimer_78() { return &___randomTimer_78; }
	inline void set_randomTimer_78(float value)
	{
		___randomTimer_78 = value;
	}

	inline static int32_t get_offset_of_blinkTimer_79() { return static_cast<int32_t>(offsetof(RandomEyes3D_t6868157C4605BD0225E1B08301F110E72336CE45, ___blinkTimer_79)); }
	inline float get_blinkTimer_79() const { return ___blinkTimer_79; }
	inline float* get_address_of_blinkTimer_79() { return &___blinkTimer_79; }
	inline void set_blinkTimer_79(float value)
	{
		___blinkTimer_79 = value;
	}

	inline static int32_t get_offset_of_isBlinking_80() { return static_cast<int32_t>(offsetof(RandomEyes3D_t6868157C4605BD0225E1B08301F110E72336CE45, ___isBlinking_80)); }
	inline bool get_isBlinking_80() const { return ___isBlinking_80; }
	inline bool* get_address_of_isBlinking_80() { return &___isBlinking_80; }
	inline void set_isBlinking_80(bool value)
	{
		___isBlinking_80 = value;
	}

	inline static int32_t get_offset_of_inBlinkSequence_81() { return static_cast<int32_t>(offsetof(RandomEyes3D_t6868157C4605BD0225E1B08301F110E72336CE45, ___inBlinkSequence_81)); }
	inline bool get_inBlinkSequence_81() const { return ___inBlinkSequence_81; }
	inline bool* get_address_of_inBlinkSequence_81() { return &___inBlinkSequence_81; }
	inline void set_inBlinkSequence_81(bool value)
	{
		___inBlinkSequence_81 = value;
	}

	inline static int32_t get_offset_of_currentCloseAmount_82() { return static_cast<int32_t>(offsetof(RandomEyes3D_t6868157C4605BD0225E1B08301F110E72336CE45, ___currentCloseAmount_82)); }
	inline float get_currentCloseAmount_82() const { return ___currentCloseAmount_82; }
	inline float* get_address_of_currentCloseAmount_82() { return &___currentCloseAmount_82; }
	inline void set_currentCloseAmount_82(float value)
	{
		___currentCloseAmount_82 = value;
	}

	inline static int32_t get_offset_of_customShape_83() { return static_cast<int32_t>(offsetof(RandomEyes3D_t6868157C4605BD0225E1B08301F110E72336CE45, ___customShape_83)); }
	inline RandomEyesCustomShapeStatus_tAE795B1C1EF38DFDE0535D6A919E25A96283DD66 * get_customShape_83() const { return ___customShape_83; }
	inline RandomEyesCustomShapeStatus_tAE795B1C1EF38DFDE0535D6A919E25A96283DD66 ** get_address_of_customShape_83() { return &___customShape_83; }
	inline void set_customShape_83(RandomEyesCustomShapeStatus_tAE795B1C1EF38DFDE0535D6A919E25A96283DD66 * value)
	{
		___customShape_83 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___customShape_83), (void*)value);
	}

	inline static int32_t get_offset_of_customShapeTimer_84() { return static_cast<int32_t>(offsetof(RandomEyes3D_t6868157C4605BD0225E1B08301F110E72336CE45, ___customShapeTimer_84)); }
	inline float get_customShapeTimer_84() const { return ___customShapeTimer_84; }
	inline float* get_address_of_customShapeTimer_84() { return &___customShapeTimer_84; }
	inline void set_customShapeTimer_84(float value)
	{
		___customShapeTimer_84 = value;
	}

	inline static int32_t get_offset_of_prevRandomCustomShapes_85() { return static_cast<int32_t>(offsetof(RandomEyes3D_t6868157C4605BD0225E1B08301F110E72336CE45, ___prevRandomCustomShapes_85)); }
	inline bool get_prevRandomCustomShapes_85() const { return ___prevRandomCustomShapes_85; }
	inline bool* get_address_of_prevRandomCustomShapes_85() { return &___prevRandomCustomShapes_85; }
	inline void set_prevRandomCustomShapes_85(bool value)
	{
		___prevRandomCustomShapes_85 = value;
	}

	inline static int32_t get_offset_of_randomShape_86() { return static_cast<int32_t>(offsetof(RandomEyes3D_t6868157C4605BD0225E1B08301F110E72336CE45, ___randomShape_86)); }
	inline String_t* get_randomShape_86() const { return ___randomShape_86; }
	inline String_t** get_address_of_randomShape_86() { return &___randomShape_86; }
	inline void set_randomShape_86(String_t* value)
	{
		___randomShape_86 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___randomShape_86), (void*)value);
	}

	inline static int32_t get_offset_of_prevCustomShapeCount_87() { return static_cast<int32_t>(offsetof(RandomEyes3D_t6868157C4605BD0225E1B08301F110E72336CE45, ___prevCustomShapeCount_87)); }
	inline int32_t get_prevCustomShapeCount_87() const { return ___prevCustomShapeCount_87; }
	inline int32_t* get_address_of_prevCustomShapeCount_87() { return &___prevCustomShapeCount_87; }
	inline void set_prevCustomShapeCount_87(int32_t value)
	{
		___prevCustomShapeCount_87 = value;
	}

	inline static int32_t get_offset_of_prevIsOn_88() { return static_cast<int32_t>(offsetof(RandomEyes3D_t6868157C4605BD0225E1B08301F110E72336CE45, ___prevIsOn_88)); }
	inline BooleanU5BU5D_tEC7BAF93C44F875016DAADC8696EE3A465644D3C* get_prevIsOn_88() const { return ___prevIsOn_88; }
	inline BooleanU5BU5D_tEC7BAF93C44F875016DAADC8696EE3A465644D3C** get_address_of_prevIsOn_88() { return &___prevIsOn_88; }
	inline void set_prevIsOn_88(BooleanU5BU5D_tEC7BAF93C44F875016DAADC8696EE3A465644D3C* value)
	{
		___prevIsOn_88 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___prevIsOn_88), (void*)value);
	}
};

struct RandomEyes3D_t6868157C4605BD0225E1B08301F110E72336CE45_StaticFields
{
public:
	// CrazyMinnow.SALSA.RandomEyes3D CrazyMinnow.SALSA.RandomEyes3D::instance
	RandomEyes3D_t6868157C4605BD0225E1B08301F110E72336CE45 * ___instance_4;

public:
	inline static int32_t get_offset_of_instance_4() { return static_cast<int32_t>(offsetof(RandomEyes3D_t6868157C4605BD0225E1B08301F110E72336CE45_StaticFields, ___instance_4)); }
	inline RandomEyes3D_t6868157C4605BD0225E1B08301F110E72336CE45 * get_instance_4() const { return ___instance_4; }
	inline RandomEyes3D_t6868157C4605BD0225E1B08301F110E72336CE45 ** get_address_of_instance_4() { return &___instance_4; }
	inline void set_instance_4(RandomEyes3D_t6868157C4605BD0225E1B08301F110E72336CE45 * value)
	{
		___instance_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___instance_4), (void*)value);
	}
};


// CrazyMinnow.SALSA.RandomEyes3DGizmo
struct RandomEyes3DGizmo_t039AFDA0DE2968F2C3DFB63606D40A2C238839A1  : public MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A
{
public:
	// System.Boolean CrazyMinnow.SALSA.RandomEyes3DGizmo::showGizmo
	bool ___showGizmo_5;

public:
	inline static int32_t get_offset_of_showGizmo_5() { return static_cast<int32_t>(offsetof(RandomEyes3DGizmo_t039AFDA0DE2968F2C3DFB63606D40A2C238839A1, ___showGizmo_5)); }
	inline bool get_showGizmo_5() const { return ___showGizmo_5; }
	inline bool* get_address_of_showGizmo_5() { return &___showGizmo_5; }
	inline void set_showGizmo_5(bool value)
	{
		___showGizmo_5 = value;
	}
};

struct RandomEyes3DGizmo_t039AFDA0DE2968F2C3DFB63606D40A2C238839A1_StaticFields
{
public:
	// CrazyMinnow.SALSA.RandomEyes3DGizmo CrazyMinnow.SALSA.RandomEyes3DGizmo::instance
	RandomEyes3DGizmo_t039AFDA0DE2968F2C3DFB63606D40A2C238839A1 * ___instance_4;

public:
	inline static int32_t get_offset_of_instance_4() { return static_cast<int32_t>(offsetof(RandomEyes3DGizmo_t039AFDA0DE2968F2C3DFB63606D40A2C238839A1_StaticFields, ___instance_4)); }
	inline RandomEyes3DGizmo_t039AFDA0DE2968F2C3DFB63606D40A2C238839A1 * get_instance_4() const { return ___instance_4; }
	inline RandomEyes3DGizmo_t039AFDA0DE2968F2C3DFB63606D40A2C238839A1 ** get_address_of_instance_4() { return &___instance_4; }
	inline void set_instance_4(RandomEyes3DGizmo_t039AFDA0DE2968F2C3DFB63606D40A2C238839A1 * value)
	{
		___instance_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___instance_4), (void*)value);
	}
};


// CrazyMinnow.SALSA.Salsa2D
struct Salsa2D_t54C8B8FFE3B8149C565BD2A69D53629E65CFFB62  : public MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A
{
public:
	// System.Int32 CrazyMinnow.SALSA.Salsa2D::mouthLayer
	int32_t ___mouthLayer_5;
	// UnityEngine.SpriteRenderer CrazyMinnow.SALSA.Salsa2D::spriteRenderer
	SpriteRenderer_t3F35AD5498243C170B46F5FFDB582AAEF78615EF * ___spriteRenderer_6;
	// UnityEngine.SpriteRenderer CrazyMinnow.SALSA.Salsa2D::prevSpriteRenderer
	SpriteRenderer_t3F35AD5498243C170B46F5FFDB582AAEF78615EF * ___prevSpriteRenderer_7;
	// System.Boolean CrazyMinnow.SALSA.Salsa2D::getSpriteRenderer
	bool ___getSpriteRenderer_8;
	// UnityEngine.AudioSource CrazyMinnow.SALSA.Salsa2D::audioSrc
	AudioSource_tC4BF65AF8CDCAA63724BB3CA59A7A29249269E6B * ___audioSrc_9;
	// UnityEngine.AudioClip CrazyMinnow.SALSA.Salsa2D::audioClip
	AudioClip_t16D2E573E7CC1C5118D8EE0F0692D46866A1C0EE * ___audioClip_10;
	// UnityEngine.Sprite CrazyMinnow.SALSA.Salsa2D::sayRestSprite
	Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 * ___sayRestSprite_11;
	// UnityEngine.Sprite CrazyMinnow.SALSA.Salsa2D::saySmallSprite
	Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 * ___saySmallSprite_12;
	// UnityEngine.Sprite CrazyMinnow.SALSA.Salsa2D::sayMediumSprite
	Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 * ___sayMediumSprite_13;
	// UnityEngine.Sprite CrazyMinnow.SALSA.Salsa2D::sayLargeSprite
	Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 * ___sayLargeSprite_14;
	// System.Single CrazyMinnow.SALSA.Salsa2D::saySmallTrigger
	float ___saySmallTrigger_15;
	// System.Single CrazyMinnow.SALSA.Salsa2D::sayMediumTrigger
	float ___sayMediumTrigger_16;
	// System.Single CrazyMinnow.SALSA.Salsa2D::sayLargeTrigger
	float ___sayLargeTrigger_17;
	// System.Single CrazyMinnow.SALSA.Salsa2D::audioUpdateDelay
	float ___audioUpdateDelay_18;
	// System.Boolean CrazyMinnow.SALSA.Salsa2D::isTalking
	bool ___isTalking_19;
	// System.Boolean CrazyMinnow.SALSA.Salsa2D::prevIsTalking
	bool ___prevIsTalking_20;
	// System.Boolean CrazyMinnow.SALSA.Salsa2D::broadcast
	bool ___broadcast_21;
	// System.Int32 CrazyMinnow.SALSA.Salsa2D::broadcastReceiversCount
	int32_t ___broadcastReceiversCount_22;
	// System.Boolean CrazyMinnow.SALSA.Salsa2D::expandBroadcast
	bool ___expandBroadcast_23;
	// UnityEngine.GameObject[] CrazyMinnow.SALSA.Salsa2D::broadcastReceivers
	GameObjectU5BU5D_tA88FC1A1FC9D4D73D0B3984D4B0ECE88F4C47642* ___broadcastReceivers_24;
	// System.Boolean CrazyMinnow.SALSA.Salsa2D::propagateToChildren
	bool ___propagateToChildren_25;
	// System.Single CrazyMinnow.SALSA.Salsa2D::average
	float ___average_26;
	// System.String CrazyMinnow.SALSA.Salsa2D::say
	String_t* ___say_27;
	// System.Int32 CrazyMinnow.SALSA.Salsa2D::sayIndex
	int32_t ___sayIndex_28;
	// UnityEngine.WaitForSeconds CrazyMinnow.SALSA.Salsa2D::updateSampleDelay
	WaitForSeconds_t8F9189BE6E467C98C99177038881F8982E0E4013 * ___updateSampleDelay_29;
	// System.Single CrazyMinnow.SALSA.Salsa2D::prevAudioUpdateDelay
	float ___prevAudioUpdateDelay_30;
	// System.Single[] CrazyMinnow.SALSA.Salsa2D::sample
	SingleU5BU5D_t47E8DBF5B597C122478D1FFBD9DD57399A0650FA* ___sample_31;
	// System.Single CrazyMinnow.SALSA.Salsa2D::currentRestValue
	float ___currentRestValue_32;
	// System.Single CrazyMinnow.SALSA.Salsa2D::currentSmallValue
	float ___currentSmallValue_33;
	// System.Single CrazyMinnow.SALSA.Salsa2D::currentMediumValue
	float ___currentMediumValue_34;
	// System.Single CrazyMinnow.SALSA.Salsa2D::currentBigValue
	float ___currentBigValue_35;
	// System.Int32 CrazyMinnow.SALSA.Salsa2D::sampleSize
	int32_t ___sampleSize_36;
	// System.Boolean CrazyMinnow.SALSA.Salsa2D::writeAverage
	bool ___writeAverage_37;

public:
	inline static int32_t get_offset_of_mouthLayer_5() { return static_cast<int32_t>(offsetof(Salsa2D_t54C8B8FFE3B8149C565BD2A69D53629E65CFFB62, ___mouthLayer_5)); }
	inline int32_t get_mouthLayer_5() const { return ___mouthLayer_5; }
	inline int32_t* get_address_of_mouthLayer_5() { return &___mouthLayer_5; }
	inline void set_mouthLayer_5(int32_t value)
	{
		___mouthLayer_5 = value;
	}

	inline static int32_t get_offset_of_spriteRenderer_6() { return static_cast<int32_t>(offsetof(Salsa2D_t54C8B8FFE3B8149C565BD2A69D53629E65CFFB62, ___spriteRenderer_6)); }
	inline SpriteRenderer_t3F35AD5498243C170B46F5FFDB582AAEF78615EF * get_spriteRenderer_6() const { return ___spriteRenderer_6; }
	inline SpriteRenderer_t3F35AD5498243C170B46F5FFDB582AAEF78615EF ** get_address_of_spriteRenderer_6() { return &___spriteRenderer_6; }
	inline void set_spriteRenderer_6(SpriteRenderer_t3F35AD5498243C170B46F5FFDB582AAEF78615EF * value)
	{
		___spriteRenderer_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___spriteRenderer_6), (void*)value);
	}

	inline static int32_t get_offset_of_prevSpriteRenderer_7() { return static_cast<int32_t>(offsetof(Salsa2D_t54C8B8FFE3B8149C565BD2A69D53629E65CFFB62, ___prevSpriteRenderer_7)); }
	inline SpriteRenderer_t3F35AD5498243C170B46F5FFDB582AAEF78615EF * get_prevSpriteRenderer_7() const { return ___prevSpriteRenderer_7; }
	inline SpriteRenderer_t3F35AD5498243C170B46F5FFDB582AAEF78615EF ** get_address_of_prevSpriteRenderer_7() { return &___prevSpriteRenderer_7; }
	inline void set_prevSpriteRenderer_7(SpriteRenderer_t3F35AD5498243C170B46F5FFDB582AAEF78615EF * value)
	{
		___prevSpriteRenderer_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___prevSpriteRenderer_7), (void*)value);
	}

	inline static int32_t get_offset_of_getSpriteRenderer_8() { return static_cast<int32_t>(offsetof(Salsa2D_t54C8B8FFE3B8149C565BD2A69D53629E65CFFB62, ___getSpriteRenderer_8)); }
	inline bool get_getSpriteRenderer_8() const { return ___getSpriteRenderer_8; }
	inline bool* get_address_of_getSpriteRenderer_8() { return &___getSpriteRenderer_8; }
	inline void set_getSpriteRenderer_8(bool value)
	{
		___getSpriteRenderer_8 = value;
	}

	inline static int32_t get_offset_of_audioSrc_9() { return static_cast<int32_t>(offsetof(Salsa2D_t54C8B8FFE3B8149C565BD2A69D53629E65CFFB62, ___audioSrc_9)); }
	inline AudioSource_tC4BF65AF8CDCAA63724BB3CA59A7A29249269E6B * get_audioSrc_9() const { return ___audioSrc_9; }
	inline AudioSource_tC4BF65AF8CDCAA63724BB3CA59A7A29249269E6B ** get_address_of_audioSrc_9() { return &___audioSrc_9; }
	inline void set_audioSrc_9(AudioSource_tC4BF65AF8CDCAA63724BB3CA59A7A29249269E6B * value)
	{
		___audioSrc_9 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___audioSrc_9), (void*)value);
	}

	inline static int32_t get_offset_of_audioClip_10() { return static_cast<int32_t>(offsetof(Salsa2D_t54C8B8FFE3B8149C565BD2A69D53629E65CFFB62, ___audioClip_10)); }
	inline AudioClip_t16D2E573E7CC1C5118D8EE0F0692D46866A1C0EE * get_audioClip_10() const { return ___audioClip_10; }
	inline AudioClip_t16D2E573E7CC1C5118D8EE0F0692D46866A1C0EE ** get_address_of_audioClip_10() { return &___audioClip_10; }
	inline void set_audioClip_10(AudioClip_t16D2E573E7CC1C5118D8EE0F0692D46866A1C0EE * value)
	{
		___audioClip_10 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___audioClip_10), (void*)value);
	}

	inline static int32_t get_offset_of_sayRestSprite_11() { return static_cast<int32_t>(offsetof(Salsa2D_t54C8B8FFE3B8149C565BD2A69D53629E65CFFB62, ___sayRestSprite_11)); }
	inline Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 * get_sayRestSprite_11() const { return ___sayRestSprite_11; }
	inline Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 ** get_address_of_sayRestSprite_11() { return &___sayRestSprite_11; }
	inline void set_sayRestSprite_11(Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 * value)
	{
		___sayRestSprite_11 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___sayRestSprite_11), (void*)value);
	}

	inline static int32_t get_offset_of_saySmallSprite_12() { return static_cast<int32_t>(offsetof(Salsa2D_t54C8B8FFE3B8149C565BD2A69D53629E65CFFB62, ___saySmallSprite_12)); }
	inline Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 * get_saySmallSprite_12() const { return ___saySmallSprite_12; }
	inline Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 ** get_address_of_saySmallSprite_12() { return &___saySmallSprite_12; }
	inline void set_saySmallSprite_12(Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 * value)
	{
		___saySmallSprite_12 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___saySmallSprite_12), (void*)value);
	}

	inline static int32_t get_offset_of_sayMediumSprite_13() { return static_cast<int32_t>(offsetof(Salsa2D_t54C8B8FFE3B8149C565BD2A69D53629E65CFFB62, ___sayMediumSprite_13)); }
	inline Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 * get_sayMediumSprite_13() const { return ___sayMediumSprite_13; }
	inline Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 ** get_address_of_sayMediumSprite_13() { return &___sayMediumSprite_13; }
	inline void set_sayMediumSprite_13(Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 * value)
	{
		___sayMediumSprite_13 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___sayMediumSprite_13), (void*)value);
	}

	inline static int32_t get_offset_of_sayLargeSprite_14() { return static_cast<int32_t>(offsetof(Salsa2D_t54C8B8FFE3B8149C565BD2A69D53629E65CFFB62, ___sayLargeSprite_14)); }
	inline Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 * get_sayLargeSprite_14() const { return ___sayLargeSprite_14; }
	inline Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 ** get_address_of_sayLargeSprite_14() { return &___sayLargeSprite_14; }
	inline void set_sayLargeSprite_14(Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 * value)
	{
		___sayLargeSprite_14 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___sayLargeSprite_14), (void*)value);
	}

	inline static int32_t get_offset_of_saySmallTrigger_15() { return static_cast<int32_t>(offsetof(Salsa2D_t54C8B8FFE3B8149C565BD2A69D53629E65CFFB62, ___saySmallTrigger_15)); }
	inline float get_saySmallTrigger_15() const { return ___saySmallTrigger_15; }
	inline float* get_address_of_saySmallTrigger_15() { return &___saySmallTrigger_15; }
	inline void set_saySmallTrigger_15(float value)
	{
		___saySmallTrigger_15 = value;
	}

	inline static int32_t get_offset_of_sayMediumTrigger_16() { return static_cast<int32_t>(offsetof(Salsa2D_t54C8B8FFE3B8149C565BD2A69D53629E65CFFB62, ___sayMediumTrigger_16)); }
	inline float get_sayMediumTrigger_16() const { return ___sayMediumTrigger_16; }
	inline float* get_address_of_sayMediumTrigger_16() { return &___sayMediumTrigger_16; }
	inline void set_sayMediumTrigger_16(float value)
	{
		___sayMediumTrigger_16 = value;
	}

	inline static int32_t get_offset_of_sayLargeTrigger_17() { return static_cast<int32_t>(offsetof(Salsa2D_t54C8B8FFE3B8149C565BD2A69D53629E65CFFB62, ___sayLargeTrigger_17)); }
	inline float get_sayLargeTrigger_17() const { return ___sayLargeTrigger_17; }
	inline float* get_address_of_sayLargeTrigger_17() { return &___sayLargeTrigger_17; }
	inline void set_sayLargeTrigger_17(float value)
	{
		___sayLargeTrigger_17 = value;
	}

	inline static int32_t get_offset_of_audioUpdateDelay_18() { return static_cast<int32_t>(offsetof(Salsa2D_t54C8B8FFE3B8149C565BD2A69D53629E65CFFB62, ___audioUpdateDelay_18)); }
	inline float get_audioUpdateDelay_18() const { return ___audioUpdateDelay_18; }
	inline float* get_address_of_audioUpdateDelay_18() { return &___audioUpdateDelay_18; }
	inline void set_audioUpdateDelay_18(float value)
	{
		___audioUpdateDelay_18 = value;
	}

	inline static int32_t get_offset_of_isTalking_19() { return static_cast<int32_t>(offsetof(Salsa2D_t54C8B8FFE3B8149C565BD2A69D53629E65CFFB62, ___isTalking_19)); }
	inline bool get_isTalking_19() const { return ___isTalking_19; }
	inline bool* get_address_of_isTalking_19() { return &___isTalking_19; }
	inline void set_isTalking_19(bool value)
	{
		___isTalking_19 = value;
	}

	inline static int32_t get_offset_of_prevIsTalking_20() { return static_cast<int32_t>(offsetof(Salsa2D_t54C8B8FFE3B8149C565BD2A69D53629E65CFFB62, ___prevIsTalking_20)); }
	inline bool get_prevIsTalking_20() const { return ___prevIsTalking_20; }
	inline bool* get_address_of_prevIsTalking_20() { return &___prevIsTalking_20; }
	inline void set_prevIsTalking_20(bool value)
	{
		___prevIsTalking_20 = value;
	}

	inline static int32_t get_offset_of_broadcast_21() { return static_cast<int32_t>(offsetof(Salsa2D_t54C8B8FFE3B8149C565BD2A69D53629E65CFFB62, ___broadcast_21)); }
	inline bool get_broadcast_21() const { return ___broadcast_21; }
	inline bool* get_address_of_broadcast_21() { return &___broadcast_21; }
	inline void set_broadcast_21(bool value)
	{
		___broadcast_21 = value;
	}

	inline static int32_t get_offset_of_broadcastReceiversCount_22() { return static_cast<int32_t>(offsetof(Salsa2D_t54C8B8FFE3B8149C565BD2A69D53629E65CFFB62, ___broadcastReceiversCount_22)); }
	inline int32_t get_broadcastReceiversCount_22() const { return ___broadcastReceiversCount_22; }
	inline int32_t* get_address_of_broadcastReceiversCount_22() { return &___broadcastReceiversCount_22; }
	inline void set_broadcastReceiversCount_22(int32_t value)
	{
		___broadcastReceiversCount_22 = value;
	}

	inline static int32_t get_offset_of_expandBroadcast_23() { return static_cast<int32_t>(offsetof(Salsa2D_t54C8B8FFE3B8149C565BD2A69D53629E65CFFB62, ___expandBroadcast_23)); }
	inline bool get_expandBroadcast_23() const { return ___expandBroadcast_23; }
	inline bool* get_address_of_expandBroadcast_23() { return &___expandBroadcast_23; }
	inline void set_expandBroadcast_23(bool value)
	{
		___expandBroadcast_23 = value;
	}

	inline static int32_t get_offset_of_broadcastReceivers_24() { return static_cast<int32_t>(offsetof(Salsa2D_t54C8B8FFE3B8149C565BD2A69D53629E65CFFB62, ___broadcastReceivers_24)); }
	inline GameObjectU5BU5D_tA88FC1A1FC9D4D73D0B3984D4B0ECE88F4C47642* get_broadcastReceivers_24() const { return ___broadcastReceivers_24; }
	inline GameObjectU5BU5D_tA88FC1A1FC9D4D73D0B3984D4B0ECE88F4C47642** get_address_of_broadcastReceivers_24() { return &___broadcastReceivers_24; }
	inline void set_broadcastReceivers_24(GameObjectU5BU5D_tA88FC1A1FC9D4D73D0B3984D4B0ECE88F4C47642* value)
	{
		___broadcastReceivers_24 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___broadcastReceivers_24), (void*)value);
	}

	inline static int32_t get_offset_of_propagateToChildren_25() { return static_cast<int32_t>(offsetof(Salsa2D_t54C8B8FFE3B8149C565BD2A69D53629E65CFFB62, ___propagateToChildren_25)); }
	inline bool get_propagateToChildren_25() const { return ___propagateToChildren_25; }
	inline bool* get_address_of_propagateToChildren_25() { return &___propagateToChildren_25; }
	inline void set_propagateToChildren_25(bool value)
	{
		___propagateToChildren_25 = value;
	}

	inline static int32_t get_offset_of_average_26() { return static_cast<int32_t>(offsetof(Salsa2D_t54C8B8FFE3B8149C565BD2A69D53629E65CFFB62, ___average_26)); }
	inline float get_average_26() const { return ___average_26; }
	inline float* get_address_of_average_26() { return &___average_26; }
	inline void set_average_26(float value)
	{
		___average_26 = value;
	}

	inline static int32_t get_offset_of_say_27() { return static_cast<int32_t>(offsetof(Salsa2D_t54C8B8FFE3B8149C565BD2A69D53629E65CFFB62, ___say_27)); }
	inline String_t* get_say_27() const { return ___say_27; }
	inline String_t** get_address_of_say_27() { return &___say_27; }
	inline void set_say_27(String_t* value)
	{
		___say_27 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___say_27), (void*)value);
	}

	inline static int32_t get_offset_of_sayIndex_28() { return static_cast<int32_t>(offsetof(Salsa2D_t54C8B8FFE3B8149C565BD2A69D53629E65CFFB62, ___sayIndex_28)); }
	inline int32_t get_sayIndex_28() const { return ___sayIndex_28; }
	inline int32_t* get_address_of_sayIndex_28() { return &___sayIndex_28; }
	inline void set_sayIndex_28(int32_t value)
	{
		___sayIndex_28 = value;
	}

	inline static int32_t get_offset_of_updateSampleDelay_29() { return static_cast<int32_t>(offsetof(Salsa2D_t54C8B8FFE3B8149C565BD2A69D53629E65CFFB62, ___updateSampleDelay_29)); }
	inline WaitForSeconds_t8F9189BE6E467C98C99177038881F8982E0E4013 * get_updateSampleDelay_29() const { return ___updateSampleDelay_29; }
	inline WaitForSeconds_t8F9189BE6E467C98C99177038881F8982E0E4013 ** get_address_of_updateSampleDelay_29() { return &___updateSampleDelay_29; }
	inline void set_updateSampleDelay_29(WaitForSeconds_t8F9189BE6E467C98C99177038881F8982E0E4013 * value)
	{
		___updateSampleDelay_29 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___updateSampleDelay_29), (void*)value);
	}

	inline static int32_t get_offset_of_prevAudioUpdateDelay_30() { return static_cast<int32_t>(offsetof(Salsa2D_t54C8B8FFE3B8149C565BD2A69D53629E65CFFB62, ___prevAudioUpdateDelay_30)); }
	inline float get_prevAudioUpdateDelay_30() const { return ___prevAudioUpdateDelay_30; }
	inline float* get_address_of_prevAudioUpdateDelay_30() { return &___prevAudioUpdateDelay_30; }
	inline void set_prevAudioUpdateDelay_30(float value)
	{
		___prevAudioUpdateDelay_30 = value;
	}

	inline static int32_t get_offset_of_sample_31() { return static_cast<int32_t>(offsetof(Salsa2D_t54C8B8FFE3B8149C565BD2A69D53629E65CFFB62, ___sample_31)); }
	inline SingleU5BU5D_t47E8DBF5B597C122478D1FFBD9DD57399A0650FA* get_sample_31() const { return ___sample_31; }
	inline SingleU5BU5D_t47E8DBF5B597C122478D1FFBD9DD57399A0650FA** get_address_of_sample_31() { return &___sample_31; }
	inline void set_sample_31(SingleU5BU5D_t47E8DBF5B597C122478D1FFBD9DD57399A0650FA* value)
	{
		___sample_31 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___sample_31), (void*)value);
	}

	inline static int32_t get_offset_of_currentRestValue_32() { return static_cast<int32_t>(offsetof(Salsa2D_t54C8B8FFE3B8149C565BD2A69D53629E65CFFB62, ___currentRestValue_32)); }
	inline float get_currentRestValue_32() const { return ___currentRestValue_32; }
	inline float* get_address_of_currentRestValue_32() { return &___currentRestValue_32; }
	inline void set_currentRestValue_32(float value)
	{
		___currentRestValue_32 = value;
	}

	inline static int32_t get_offset_of_currentSmallValue_33() { return static_cast<int32_t>(offsetof(Salsa2D_t54C8B8FFE3B8149C565BD2A69D53629E65CFFB62, ___currentSmallValue_33)); }
	inline float get_currentSmallValue_33() const { return ___currentSmallValue_33; }
	inline float* get_address_of_currentSmallValue_33() { return &___currentSmallValue_33; }
	inline void set_currentSmallValue_33(float value)
	{
		___currentSmallValue_33 = value;
	}

	inline static int32_t get_offset_of_currentMediumValue_34() { return static_cast<int32_t>(offsetof(Salsa2D_t54C8B8FFE3B8149C565BD2A69D53629E65CFFB62, ___currentMediumValue_34)); }
	inline float get_currentMediumValue_34() const { return ___currentMediumValue_34; }
	inline float* get_address_of_currentMediumValue_34() { return &___currentMediumValue_34; }
	inline void set_currentMediumValue_34(float value)
	{
		___currentMediumValue_34 = value;
	}

	inline static int32_t get_offset_of_currentBigValue_35() { return static_cast<int32_t>(offsetof(Salsa2D_t54C8B8FFE3B8149C565BD2A69D53629E65CFFB62, ___currentBigValue_35)); }
	inline float get_currentBigValue_35() const { return ___currentBigValue_35; }
	inline float* get_address_of_currentBigValue_35() { return &___currentBigValue_35; }
	inline void set_currentBigValue_35(float value)
	{
		___currentBigValue_35 = value;
	}

	inline static int32_t get_offset_of_sampleSize_36() { return static_cast<int32_t>(offsetof(Salsa2D_t54C8B8FFE3B8149C565BD2A69D53629E65CFFB62, ___sampleSize_36)); }
	inline int32_t get_sampleSize_36() const { return ___sampleSize_36; }
	inline int32_t* get_address_of_sampleSize_36() { return &___sampleSize_36; }
	inline void set_sampleSize_36(int32_t value)
	{
		___sampleSize_36 = value;
	}

	inline static int32_t get_offset_of_writeAverage_37() { return static_cast<int32_t>(offsetof(Salsa2D_t54C8B8FFE3B8149C565BD2A69D53629E65CFFB62, ___writeAverage_37)); }
	inline bool get_writeAverage_37() const { return ___writeAverage_37; }
	inline bool* get_address_of_writeAverage_37() { return &___writeAverage_37; }
	inline void set_writeAverage_37(bool value)
	{
		___writeAverage_37 = value;
	}
};

struct Salsa2D_t54C8B8FFE3B8149C565BD2A69D53629E65CFFB62_StaticFields
{
public:
	// CrazyMinnow.SALSA.Salsa2D CrazyMinnow.SALSA.Salsa2D::instance
	Salsa2D_t54C8B8FFE3B8149C565BD2A69D53629E65CFFB62 * ___instance_4;

public:
	inline static int32_t get_offset_of_instance_4() { return static_cast<int32_t>(offsetof(Salsa2D_t54C8B8FFE3B8149C565BD2A69D53629E65CFFB62_StaticFields, ___instance_4)); }
	inline Salsa2D_t54C8B8FFE3B8149C565BD2A69D53629E65CFFB62 * get_instance_4() const { return ___instance_4; }
	inline Salsa2D_t54C8B8FFE3B8149C565BD2A69D53629E65CFFB62 ** get_address_of_instance_4() { return &___instance_4; }
	inline void set_instance_4(Salsa2D_t54C8B8FFE3B8149C565BD2A69D53629E65CFFB62 * value)
	{
		___instance_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___instance_4), (void*)value);
	}
};


// CrazyMinnow.SALSA.Salsa3D
struct Salsa3D_tACF83D529139263662FEAAC8F5B53EC43B140C9C  : public MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A
{
public:
	// UnityEngine.SkinnedMeshRenderer CrazyMinnow.SALSA.Salsa3D::skinnedMeshRenderer
	SkinnedMeshRenderer_t126F4D6010E0F4B2685A7817B0A9171805D8F496 * ___skinnedMeshRenderer_5;
	// UnityEngine.SkinnedMeshRenderer CrazyMinnow.SALSA.Salsa3D::prevSkinnedMeshRenderer
	SkinnedMeshRenderer_t126F4D6010E0F4B2685A7817B0A9171805D8F496 * ___prevSkinnedMeshRenderer_6;
	// System.Boolean CrazyMinnow.SALSA.Salsa3D::getSkndMshRndr
	bool ___getSkndMshRndr_7;
	// UnityEngine.AudioSource CrazyMinnow.SALSA.Salsa3D::audioSrc
	AudioSource_tC4BF65AF8CDCAA63724BB3CA59A7A29249269E6B * ___audioSrc_8;
	// UnityEngine.AudioClip CrazyMinnow.SALSA.Salsa3D::audioClip
	AudioClip_t16D2E573E7CC1C5118D8EE0F0692D46866A1C0EE * ___audioClip_9;
	// System.String[] CrazyMinnow.SALSA.Salsa3D::blendShapes
	StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* ___blendShapes_10;
	// System.Int32 CrazyMinnow.SALSA.Salsa3D::saySmallIndex
	int32_t ___saySmallIndex_11;
	// System.Int32 CrazyMinnow.SALSA.Salsa3D::sayMediumIndex
	int32_t ___sayMediumIndex_12;
	// System.Int32 CrazyMinnow.SALSA.Salsa3D::sayLargeIndex
	int32_t ___sayLargeIndex_13;
	// System.Single CrazyMinnow.SALSA.Salsa3D::saySmallTrigger
	float ___saySmallTrigger_14;
	// System.Single CrazyMinnow.SALSA.Salsa3D::sayMediumTrigger
	float ___sayMediumTrigger_15;
	// System.Single CrazyMinnow.SALSA.Salsa3D::sayLargeTrigger
	float ___sayLargeTrigger_16;
	// System.Single CrazyMinnow.SALSA.Salsa3D::audioUpdateDelay
	float ___audioUpdateDelay_17;
	// System.Single CrazyMinnow.SALSA.Salsa3D::blendSpeed
	float ___blendSpeed_18;
	// System.Single CrazyMinnow.SALSA.Salsa3D::rangeOfMotion
	float ___rangeOfMotion_19;
	// System.Boolean CrazyMinnow.SALSA.Salsa3D::isTalking
	bool ___isTalking_20;
	// System.Boolean CrazyMinnow.SALSA.Salsa3D::prevIsTalking
	bool ___prevIsTalking_21;
	// System.Boolean CrazyMinnow.SALSA.Salsa3D::broadcast
	bool ___broadcast_22;
	// System.Int32 CrazyMinnow.SALSA.Salsa3D::broadcastReceiversCount
	int32_t ___broadcastReceiversCount_23;
	// System.Boolean CrazyMinnow.SALSA.Salsa3D::expandBroadcast
	bool ___expandBroadcast_24;
	// UnityEngine.GameObject[] CrazyMinnow.SALSA.Salsa3D::broadcastReceivers
	GameObjectU5BU5D_tA88FC1A1FC9D4D73D0B3984D4B0ECE88F4C47642* ___broadcastReceivers_25;
	// System.Boolean CrazyMinnow.SALSA.Salsa3D::propagateToChildren
	bool ___propagateToChildren_26;
	// System.Single CrazyMinnow.SALSA.Salsa3D::average
	float ___average_27;
	// System.String CrazyMinnow.SALSA.Salsa3D::say
	String_t* ___say_28;
	// System.Int32 CrazyMinnow.SALSA.Salsa3D::sayIndex
	int32_t ___sayIndex_29;
	// CrazyMinnow.SALSA.SalsaBlendAmounts CrazyMinnow.SALSA.Salsa3D::sayAmount
	SalsaBlendAmounts_t8FBE1BDDD1306BB8232FCAAD43FC87042840D626 * ___sayAmount_30;
	// UnityEngine.WaitForSeconds CrazyMinnow.SALSA.Salsa3D::updateSampleDelay
	WaitForSeconds_t8F9189BE6E467C98C99177038881F8982E0E4013 * ___updateSampleDelay_31;
	// System.Single CrazyMinnow.SALSA.Salsa3D::prevAudioUpdateDelay
	float ___prevAudioUpdateDelay_32;
	// System.Single[] CrazyMinnow.SALSA.Salsa3D::sample
	SingleU5BU5D_t47E8DBF5B597C122478D1FFBD9DD57399A0650FA* ___sample_33;
	// System.Single CrazyMinnow.SALSA.Salsa3D::saySmallValue
	float ___saySmallValue_34;
	// System.Single CrazyMinnow.SALSA.Salsa3D::sayMediumValue
	float ___sayMediumValue_35;
	// System.Single CrazyMinnow.SALSA.Salsa3D::sayLargeValue
	float ___sayLargeValue_36;
	// System.Int32 CrazyMinnow.SALSA.Salsa3D::sampleSize
	int32_t ___sampleSize_37;
	// System.Boolean CrazyMinnow.SALSA.Salsa3D::writeAverage
	bool ___writeAverage_38;
	// System.Boolean CrazyMinnow.SALSA.Salsa3D::lockShapes
	bool ___lockShapes_39;

public:
	inline static int32_t get_offset_of_skinnedMeshRenderer_5() { return static_cast<int32_t>(offsetof(Salsa3D_tACF83D529139263662FEAAC8F5B53EC43B140C9C, ___skinnedMeshRenderer_5)); }
	inline SkinnedMeshRenderer_t126F4D6010E0F4B2685A7817B0A9171805D8F496 * get_skinnedMeshRenderer_5() const { return ___skinnedMeshRenderer_5; }
	inline SkinnedMeshRenderer_t126F4D6010E0F4B2685A7817B0A9171805D8F496 ** get_address_of_skinnedMeshRenderer_5() { return &___skinnedMeshRenderer_5; }
	inline void set_skinnedMeshRenderer_5(SkinnedMeshRenderer_t126F4D6010E0F4B2685A7817B0A9171805D8F496 * value)
	{
		___skinnedMeshRenderer_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___skinnedMeshRenderer_5), (void*)value);
	}

	inline static int32_t get_offset_of_prevSkinnedMeshRenderer_6() { return static_cast<int32_t>(offsetof(Salsa3D_tACF83D529139263662FEAAC8F5B53EC43B140C9C, ___prevSkinnedMeshRenderer_6)); }
	inline SkinnedMeshRenderer_t126F4D6010E0F4B2685A7817B0A9171805D8F496 * get_prevSkinnedMeshRenderer_6() const { return ___prevSkinnedMeshRenderer_6; }
	inline SkinnedMeshRenderer_t126F4D6010E0F4B2685A7817B0A9171805D8F496 ** get_address_of_prevSkinnedMeshRenderer_6() { return &___prevSkinnedMeshRenderer_6; }
	inline void set_prevSkinnedMeshRenderer_6(SkinnedMeshRenderer_t126F4D6010E0F4B2685A7817B0A9171805D8F496 * value)
	{
		___prevSkinnedMeshRenderer_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___prevSkinnedMeshRenderer_6), (void*)value);
	}

	inline static int32_t get_offset_of_getSkndMshRndr_7() { return static_cast<int32_t>(offsetof(Salsa3D_tACF83D529139263662FEAAC8F5B53EC43B140C9C, ___getSkndMshRndr_7)); }
	inline bool get_getSkndMshRndr_7() const { return ___getSkndMshRndr_7; }
	inline bool* get_address_of_getSkndMshRndr_7() { return &___getSkndMshRndr_7; }
	inline void set_getSkndMshRndr_7(bool value)
	{
		___getSkndMshRndr_7 = value;
	}

	inline static int32_t get_offset_of_audioSrc_8() { return static_cast<int32_t>(offsetof(Salsa3D_tACF83D529139263662FEAAC8F5B53EC43B140C9C, ___audioSrc_8)); }
	inline AudioSource_tC4BF65AF8CDCAA63724BB3CA59A7A29249269E6B * get_audioSrc_8() const { return ___audioSrc_8; }
	inline AudioSource_tC4BF65AF8CDCAA63724BB3CA59A7A29249269E6B ** get_address_of_audioSrc_8() { return &___audioSrc_8; }
	inline void set_audioSrc_8(AudioSource_tC4BF65AF8CDCAA63724BB3CA59A7A29249269E6B * value)
	{
		___audioSrc_8 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___audioSrc_8), (void*)value);
	}

	inline static int32_t get_offset_of_audioClip_9() { return static_cast<int32_t>(offsetof(Salsa3D_tACF83D529139263662FEAAC8F5B53EC43B140C9C, ___audioClip_9)); }
	inline AudioClip_t16D2E573E7CC1C5118D8EE0F0692D46866A1C0EE * get_audioClip_9() const { return ___audioClip_9; }
	inline AudioClip_t16D2E573E7CC1C5118D8EE0F0692D46866A1C0EE ** get_address_of_audioClip_9() { return &___audioClip_9; }
	inline void set_audioClip_9(AudioClip_t16D2E573E7CC1C5118D8EE0F0692D46866A1C0EE * value)
	{
		___audioClip_9 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___audioClip_9), (void*)value);
	}

	inline static int32_t get_offset_of_blendShapes_10() { return static_cast<int32_t>(offsetof(Salsa3D_tACF83D529139263662FEAAC8F5B53EC43B140C9C, ___blendShapes_10)); }
	inline StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* get_blendShapes_10() const { return ___blendShapes_10; }
	inline StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A** get_address_of_blendShapes_10() { return &___blendShapes_10; }
	inline void set_blendShapes_10(StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* value)
	{
		___blendShapes_10 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___blendShapes_10), (void*)value);
	}

	inline static int32_t get_offset_of_saySmallIndex_11() { return static_cast<int32_t>(offsetof(Salsa3D_tACF83D529139263662FEAAC8F5B53EC43B140C9C, ___saySmallIndex_11)); }
	inline int32_t get_saySmallIndex_11() const { return ___saySmallIndex_11; }
	inline int32_t* get_address_of_saySmallIndex_11() { return &___saySmallIndex_11; }
	inline void set_saySmallIndex_11(int32_t value)
	{
		___saySmallIndex_11 = value;
	}

	inline static int32_t get_offset_of_sayMediumIndex_12() { return static_cast<int32_t>(offsetof(Salsa3D_tACF83D529139263662FEAAC8F5B53EC43B140C9C, ___sayMediumIndex_12)); }
	inline int32_t get_sayMediumIndex_12() const { return ___sayMediumIndex_12; }
	inline int32_t* get_address_of_sayMediumIndex_12() { return &___sayMediumIndex_12; }
	inline void set_sayMediumIndex_12(int32_t value)
	{
		___sayMediumIndex_12 = value;
	}

	inline static int32_t get_offset_of_sayLargeIndex_13() { return static_cast<int32_t>(offsetof(Salsa3D_tACF83D529139263662FEAAC8F5B53EC43B140C9C, ___sayLargeIndex_13)); }
	inline int32_t get_sayLargeIndex_13() const { return ___sayLargeIndex_13; }
	inline int32_t* get_address_of_sayLargeIndex_13() { return &___sayLargeIndex_13; }
	inline void set_sayLargeIndex_13(int32_t value)
	{
		___sayLargeIndex_13 = value;
	}

	inline static int32_t get_offset_of_saySmallTrigger_14() { return static_cast<int32_t>(offsetof(Salsa3D_tACF83D529139263662FEAAC8F5B53EC43B140C9C, ___saySmallTrigger_14)); }
	inline float get_saySmallTrigger_14() const { return ___saySmallTrigger_14; }
	inline float* get_address_of_saySmallTrigger_14() { return &___saySmallTrigger_14; }
	inline void set_saySmallTrigger_14(float value)
	{
		___saySmallTrigger_14 = value;
	}

	inline static int32_t get_offset_of_sayMediumTrigger_15() { return static_cast<int32_t>(offsetof(Salsa3D_tACF83D529139263662FEAAC8F5B53EC43B140C9C, ___sayMediumTrigger_15)); }
	inline float get_sayMediumTrigger_15() const { return ___sayMediumTrigger_15; }
	inline float* get_address_of_sayMediumTrigger_15() { return &___sayMediumTrigger_15; }
	inline void set_sayMediumTrigger_15(float value)
	{
		___sayMediumTrigger_15 = value;
	}

	inline static int32_t get_offset_of_sayLargeTrigger_16() { return static_cast<int32_t>(offsetof(Salsa3D_tACF83D529139263662FEAAC8F5B53EC43B140C9C, ___sayLargeTrigger_16)); }
	inline float get_sayLargeTrigger_16() const { return ___sayLargeTrigger_16; }
	inline float* get_address_of_sayLargeTrigger_16() { return &___sayLargeTrigger_16; }
	inline void set_sayLargeTrigger_16(float value)
	{
		___sayLargeTrigger_16 = value;
	}

	inline static int32_t get_offset_of_audioUpdateDelay_17() { return static_cast<int32_t>(offsetof(Salsa3D_tACF83D529139263662FEAAC8F5B53EC43B140C9C, ___audioUpdateDelay_17)); }
	inline float get_audioUpdateDelay_17() const { return ___audioUpdateDelay_17; }
	inline float* get_address_of_audioUpdateDelay_17() { return &___audioUpdateDelay_17; }
	inline void set_audioUpdateDelay_17(float value)
	{
		___audioUpdateDelay_17 = value;
	}

	inline static int32_t get_offset_of_blendSpeed_18() { return static_cast<int32_t>(offsetof(Salsa3D_tACF83D529139263662FEAAC8F5B53EC43B140C9C, ___blendSpeed_18)); }
	inline float get_blendSpeed_18() const { return ___blendSpeed_18; }
	inline float* get_address_of_blendSpeed_18() { return &___blendSpeed_18; }
	inline void set_blendSpeed_18(float value)
	{
		___blendSpeed_18 = value;
	}

	inline static int32_t get_offset_of_rangeOfMotion_19() { return static_cast<int32_t>(offsetof(Salsa3D_tACF83D529139263662FEAAC8F5B53EC43B140C9C, ___rangeOfMotion_19)); }
	inline float get_rangeOfMotion_19() const { return ___rangeOfMotion_19; }
	inline float* get_address_of_rangeOfMotion_19() { return &___rangeOfMotion_19; }
	inline void set_rangeOfMotion_19(float value)
	{
		___rangeOfMotion_19 = value;
	}

	inline static int32_t get_offset_of_isTalking_20() { return static_cast<int32_t>(offsetof(Salsa3D_tACF83D529139263662FEAAC8F5B53EC43B140C9C, ___isTalking_20)); }
	inline bool get_isTalking_20() const { return ___isTalking_20; }
	inline bool* get_address_of_isTalking_20() { return &___isTalking_20; }
	inline void set_isTalking_20(bool value)
	{
		___isTalking_20 = value;
	}

	inline static int32_t get_offset_of_prevIsTalking_21() { return static_cast<int32_t>(offsetof(Salsa3D_tACF83D529139263662FEAAC8F5B53EC43B140C9C, ___prevIsTalking_21)); }
	inline bool get_prevIsTalking_21() const { return ___prevIsTalking_21; }
	inline bool* get_address_of_prevIsTalking_21() { return &___prevIsTalking_21; }
	inline void set_prevIsTalking_21(bool value)
	{
		___prevIsTalking_21 = value;
	}

	inline static int32_t get_offset_of_broadcast_22() { return static_cast<int32_t>(offsetof(Salsa3D_tACF83D529139263662FEAAC8F5B53EC43B140C9C, ___broadcast_22)); }
	inline bool get_broadcast_22() const { return ___broadcast_22; }
	inline bool* get_address_of_broadcast_22() { return &___broadcast_22; }
	inline void set_broadcast_22(bool value)
	{
		___broadcast_22 = value;
	}

	inline static int32_t get_offset_of_broadcastReceiversCount_23() { return static_cast<int32_t>(offsetof(Salsa3D_tACF83D529139263662FEAAC8F5B53EC43B140C9C, ___broadcastReceiversCount_23)); }
	inline int32_t get_broadcastReceiversCount_23() const { return ___broadcastReceiversCount_23; }
	inline int32_t* get_address_of_broadcastReceiversCount_23() { return &___broadcastReceiversCount_23; }
	inline void set_broadcastReceiversCount_23(int32_t value)
	{
		___broadcastReceiversCount_23 = value;
	}

	inline static int32_t get_offset_of_expandBroadcast_24() { return static_cast<int32_t>(offsetof(Salsa3D_tACF83D529139263662FEAAC8F5B53EC43B140C9C, ___expandBroadcast_24)); }
	inline bool get_expandBroadcast_24() const { return ___expandBroadcast_24; }
	inline bool* get_address_of_expandBroadcast_24() { return &___expandBroadcast_24; }
	inline void set_expandBroadcast_24(bool value)
	{
		___expandBroadcast_24 = value;
	}

	inline static int32_t get_offset_of_broadcastReceivers_25() { return static_cast<int32_t>(offsetof(Salsa3D_tACF83D529139263662FEAAC8F5B53EC43B140C9C, ___broadcastReceivers_25)); }
	inline GameObjectU5BU5D_tA88FC1A1FC9D4D73D0B3984D4B0ECE88F4C47642* get_broadcastReceivers_25() const { return ___broadcastReceivers_25; }
	inline GameObjectU5BU5D_tA88FC1A1FC9D4D73D0B3984D4B0ECE88F4C47642** get_address_of_broadcastReceivers_25() { return &___broadcastReceivers_25; }
	inline void set_broadcastReceivers_25(GameObjectU5BU5D_tA88FC1A1FC9D4D73D0B3984D4B0ECE88F4C47642* value)
	{
		___broadcastReceivers_25 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___broadcastReceivers_25), (void*)value);
	}

	inline static int32_t get_offset_of_propagateToChildren_26() { return static_cast<int32_t>(offsetof(Salsa3D_tACF83D529139263662FEAAC8F5B53EC43B140C9C, ___propagateToChildren_26)); }
	inline bool get_propagateToChildren_26() const { return ___propagateToChildren_26; }
	inline bool* get_address_of_propagateToChildren_26() { return &___propagateToChildren_26; }
	inline void set_propagateToChildren_26(bool value)
	{
		___propagateToChildren_26 = value;
	}

	inline static int32_t get_offset_of_average_27() { return static_cast<int32_t>(offsetof(Salsa3D_tACF83D529139263662FEAAC8F5B53EC43B140C9C, ___average_27)); }
	inline float get_average_27() const { return ___average_27; }
	inline float* get_address_of_average_27() { return &___average_27; }
	inline void set_average_27(float value)
	{
		___average_27 = value;
	}

	inline static int32_t get_offset_of_say_28() { return static_cast<int32_t>(offsetof(Salsa3D_tACF83D529139263662FEAAC8F5B53EC43B140C9C, ___say_28)); }
	inline String_t* get_say_28() const { return ___say_28; }
	inline String_t** get_address_of_say_28() { return &___say_28; }
	inline void set_say_28(String_t* value)
	{
		___say_28 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___say_28), (void*)value);
	}

	inline static int32_t get_offset_of_sayIndex_29() { return static_cast<int32_t>(offsetof(Salsa3D_tACF83D529139263662FEAAC8F5B53EC43B140C9C, ___sayIndex_29)); }
	inline int32_t get_sayIndex_29() const { return ___sayIndex_29; }
	inline int32_t* get_address_of_sayIndex_29() { return &___sayIndex_29; }
	inline void set_sayIndex_29(int32_t value)
	{
		___sayIndex_29 = value;
	}

	inline static int32_t get_offset_of_sayAmount_30() { return static_cast<int32_t>(offsetof(Salsa3D_tACF83D529139263662FEAAC8F5B53EC43B140C9C, ___sayAmount_30)); }
	inline SalsaBlendAmounts_t8FBE1BDDD1306BB8232FCAAD43FC87042840D626 * get_sayAmount_30() const { return ___sayAmount_30; }
	inline SalsaBlendAmounts_t8FBE1BDDD1306BB8232FCAAD43FC87042840D626 ** get_address_of_sayAmount_30() { return &___sayAmount_30; }
	inline void set_sayAmount_30(SalsaBlendAmounts_t8FBE1BDDD1306BB8232FCAAD43FC87042840D626 * value)
	{
		___sayAmount_30 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___sayAmount_30), (void*)value);
	}

	inline static int32_t get_offset_of_updateSampleDelay_31() { return static_cast<int32_t>(offsetof(Salsa3D_tACF83D529139263662FEAAC8F5B53EC43B140C9C, ___updateSampleDelay_31)); }
	inline WaitForSeconds_t8F9189BE6E467C98C99177038881F8982E0E4013 * get_updateSampleDelay_31() const { return ___updateSampleDelay_31; }
	inline WaitForSeconds_t8F9189BE6E467C98C99177038881F8982E0E4013 ** get_address_of_updateSampleDelay_31() { return &___updateSampleDelay_31; }
	inline void set_updateSampleDelay_31(WaitForSeconds_t8F9189BE6E467C98C99177038881F8982E0E4013 * value)
	{
		___updateSampleDelay_31 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___updateSampleDelay_31), (void*)value);
	}

	inline static int32_t get_offset_of_prevAudioUpdateDelay_32() { return static_cast<int32_t>(offsetof(Salsa3D_tACF83D529139263662FEAAC8F5B53EC43B140C9C, ___prevAudioUpdateDelay_32)); }
	inline float get_prevAudioUpdateDelay_32() const { return ___prevAudioUpdateDelay_32; }
	inline float* get_address_of_prevAudioUpdateDelay_32() { return &___prevAudioUpdateDelay_32; }
	inline void set_prevAudioUpdateDelay_32(float value)
	{
		___prevAudioUpdateDelay_32 = value;
	}

	inline static int32_t get_offset_of_sample_33() { return static_cast<int32_t>(offsetof(Salsa3D_tACF83D529139263662FEAAC8F5B53EC43B140C9C, ___sample_33)); }
	inline SingleU5BU5D_t47E8DBF5B597C122478D1FFBD9DD57399A0650FA* get_sample_33() const { return ___sample_33; }
	inline SingleU5BU5D_t47E8DBF5B597C122478D1FFBD9DD57399A0650FA** get_address_of_sample_33() { return &___sample_33; }
	inline void set_sample_33(SingleU5BU5D_t47E8DBF5B597C122478D1FFBD9DD57399A0650FA* value)
	{
		___sample_33 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___sample_33), (void*)value);
	}

	inline static int32_t get_offset_of_saySmallValue_34() { return static_cast<int32_t>(offsetof(Salsa3D_tACF83D529139263662FEAAC8F5B53EC43B140C9C, ___saySmallValue_34)); }
	inline float get_saySmallValue_34() const { return ___saySmallValue_34; }
	inline float* get_address_of_saySmallValue_34() { return &___saySmallValue_34; }
	inline void set_saySmallValue_34(float value)
	{
		___saySmallValue_34 = value;
	}

	inline static int32_t get_offset_of_sayMediumValue_35() { return static_cast<int32_t>(offsetof(Salsa3D_tACF83D529139263662FEAAC8F5B53EC43B140C9C, ___sayMediumValue_35)); }
	inline float get_sayMediumValue_35() const { return ___sayMediumValue_35; }
	inline float* get_address_of_sayMediumValue_35() { return &___sayMediumValue_35; }
	inline void set_sayMediumValue_35(float value)
	{
		___sayMediumValue_35 = value;
	}

	inline static int32_t get_offset_of_sayLargeValue_36() { return static_cast<int32_t>(offsetof(Salsa3D_tACF83D529139263662FEAAC8F5B53EC43B140C9C, ___sayLargeValue_36)); }
	inline float get_sayLargeValue_36() const { return ___sayLargeValue_36; }
	inline float* get_address_of_sayLargeValue_36() { return &___sayLargeValue_36; }
	inline void set_sayLargeValue_36(float value)
	{
		___sayLargeValue_36 = value;
	}

	inline static int32_t get_offset_of_sampleSize_37() { return static_cast<int32_t>(offsetof(Salsa3D_tACF83D529139263662FEAAC8F5B53EC43B140C9C, ___sampleSize_37)); }
	inline int32_t get_sampleSize_37() const { return ___sampleSize_37; }
	inline int32_t* get_address_of_sampleSize_37() { return &___sampleSize_37; }
	inline void set_sampleSize_37(int32_t value)
	{
		___sampleSize_37 = value;
	}

	inline static int32_t get_offset_of_writeAverage_38() { return static_cast<int32_t>(offsetof(Salsa3D_tACF83D529139263662FEAAC8F5B53EC43B140C9C, ___writeAverage_38)); }
	inline bool get_writeAverage_38() const { return ___writeAverage_38; }
	inline bool* get_address_of_writeAverage_38() { return &___writeAverage_38; }
	inline void set_writeAverage_38(bool value)
	{
		___writeAverage_38 = value;
	}

	inline static int32_t get_offset_of_lockShapes_39() { return static_cast<int32_t>(offsetof(Salsa3D_tACF83D529139263662FEAAC8F5B53EC43B140C9C, ___lockShapes_39)); }
	inline bool get_lockShapes_39() const { return ___lockShapes_39; }
	inline bool* get_address_of_lockShapes_39() { return &___lockShapes_39; }
	inline void set_lockShapes_39(bool value)
	{
		___lockShapes_39 = value;
	}
};

struct Salsa3D_tACF83D529139263662FEAAC8F5B53EC43B140C9C_StaticFields
{
public:
	// CrazyMinnow.SALSA.Salsa3D CrazyMinnow.SALSA.Salsa3D::instance
	Salsa3D_tACF83D529139263662FEAAC8F5B53EC43B140C9C * ___instance_4;

public:
	inline static int32_t get_offset_of_instance_4() { return static_cast<int32_t>(offsetof(Salsa3D_tACF83D529139263662FEAAC8F5B53EC43B140C9C_StaticFields, ___instance_4)); }
	inline Salsa3D_tACF83D529139263662FEAAC8F5B53EC43B140C9C * get_instance_4() const { return ___instance_4; }
	inline Salsa3D_tACF83D529139263662FEAAC8F5B53EC43B140C9C ** get_address_of_instance_4() { return &___instance_4; }
	inline void set_instance_4(Salsa3D_tACF83D529139263662FEAAC8F5B53EC43B140C9C * value)
	{
		___instance_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___instance_4), (void*)value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// UnityEngine.SpriteRenderer[]
struct SpriteRendererU5BU5D_t45BF125944C2F3974C568E650FCCDB591E5ABFBF  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) SpriteRenderer_t3F35AD5498243C170B46F5FFDB582AAEF78615EF * m_Items[1];

public:
	inline SpriteRenderer_t3F35AD5498243C170B46F5FFDB582AAEF78615EF * GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline SpriteRenderer_t3F35AD5498243C170B46F5FFDB582AAEF78615EF ** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, SpriteRenderer_t3F35AD5498243C170B46F5FFDB582AAEF78615EF * value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier((void**)m_Items + index, (void*)value);
	}
	inline SpriteRenderer_t3F35AD5498243C170B46F5FFDB582AAEF78615EF * GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline SpriteRenderer_t3F35AD5498243C170B46F5FFDB582AAEF78615EF ** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, SpriteRenderer_t3F35AD5498243C170B46F5FFDB582AAEF78615EF * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier((void**)m_Items + index, (void*)value);
	}
};
// UnityEngine.Vector3[]
struct Vector3U5BU5D_t5FB88EAA33E46838BDC2ABDAEA3E8727491CB9E4  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  m_Items[1];

public:
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
	}
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		m_Items[index] = value;
	}
};
// UnityEngine.GameObject[]
struct GameObjectU5BU5D_tA88FC1A1FC9D4D73D0B3984D4B0ECE88F4C47642  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * m_Items[1];

public:
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 ** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier((void**)m_Items + index, (void*)value);
	}
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 ** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier((void**)m_Items + index, (void*)value);
	}
};
// System.Boolean[]
struct BooleanU5BU5D_tEC7BAF93C44F875016DAADC8696EE3A465644D3C  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) bool m_Items[1];

public:
	inline bool GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline bool* GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, bool value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
	}
	inline bool GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline bool* GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, bool value)
	{
		m_Items[index] = value;
	}
};
// CrazyMinnow.SALSA.RandomEyesCustomShape[]
struct RandomEyesCustomShapeU5BU5D_t8FB0C62923194ED52AE0B06A6249680C47CA152C  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) RandomEyesCustomShape_t10BCA0CEC18C2BBF6A8BD7BFAD2AF005D233C354 * m_Items[1];

public:
	inline RandomEyesCustomShape_t10BCA0CEC18C2BBF6A8BD7BFAD2AF005D233C354 * GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline RandomEyesCustomShape_t10BCA0CEC18C2BBF6A8BD7BFAD2AF005D233C354 ** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, RandomEyesCustomShape_t10BCA0CEC18C2BBF6A8BD7BFAD2AF005D233C354 * value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier((void**)m_Items + index, (void*)value);
	}
	inline RandomEyesCustomShape_t10BCA0CEC18C2BBF6A8BD7BFAD2AF005D233C354 * GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline RandomEyesCustomShape_t10BCA0CEC18C2BBF6A8BD7BFAD2AF005D233C354 ** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, RandomEyesCustomShape_t10BCA0CEC18C2BBF6A8BD7BFAD2AF005D233C354 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier((void**)m_Items + index, (void*)value);
	}
};
// System.String[]
struct StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) String_t* m_Items[1];

public:
	inline String_t* GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline String_t** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, String_t* value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier((void**)m_Items + index, (void*)value);
	}
	inline String_t* GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline String_t** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, String_t* value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier((void**)m_Items + index, (void*)value);
	}
};
// UnityEngine.Transform[]
struct TransformU5BU5D_t7821C0520CC567C0A069329C01AE9C058C7E3F1D  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * m_Items[1];

public:
	inline Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 ** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier((void**)m_Items + index, (void*)value);
	}
	inline Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 ** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier((void**)m_Items + index, (void*)value);
	}
};
// System.Single[]
struct SingleU5BU5D_t47E8DBF5B597C122478D1FFBD9DD57399A0650FA  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) float m_Items[1];

public:
	inline float GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline float* GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, float value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
	}
	inline float GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline float* GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, float value)
	{
		m_Items[index] = value;
	}
};
// System.Object[]
struct ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) RuntimeObject * m_Items[1];

public:
	inline RuntimeObject * GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline RuntimeObject ** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, RuntimeObject * value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier((void**)m_Items + index, (void*)value);
	}
	inline RuntimeObject * GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline RuntimeObject ** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, RuntimeObject * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier((void**)m_Items + index, (void*)value);
	}
};
// System.Int32[]
struct Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) int32_t m_Items[1];

public:
	inline int32_t GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline int32_t* GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, int32_t value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
	}
	inline int32_t GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline int32_t* GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, int32_t value)
	{
		m_Items[index] = value;
	}
};


// System.Void System.Collections.Generic.List`1<System.Object>::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void List_1__ctor_m0F0E00088CF56FEACC9E32D8B7D91B93D91DAA3B_gshared (List_1_t3F94120C77410A62EAE48421CF166B83AB95A2F5 * __this, const RuntimeMethod* method);
// !!0 UnityEngine.Component::GetComponent<System.Object>()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject * Component_GetComponent_TisRuntimeObject_m69D9C576D6DD024C709E29EEADBC8041299A3AA7_gshared (Component_t62FBC8D2420DA4BE9037AFE430740F6B3EECA684 * __this, const RuntimeMethod* method);
// !!0 UnityEngine.GameObject::AddComponent<System.Object>()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject * GameObject_AddComponent_TisRuntimeObject_mBDBD6EC58A4409E35E4C5D08757C36E4938256B1_gshared (GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * __this, const RuntimeMethod* method);
// !0 System.Collections.Generic.List`1<System.Object>::get_Item(System.Int32)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR RuntimeObject * List_1_get_Item_mF00B574E58FB078BB753B05A3B86DD0A7A266B63_gshared_inline (List_1_t3F94120C77410A62EAE48421CF166B83AB95A2F5 * __this, int32_t ___index0, const RuntimeMethod* method);
// System.Int32 System.Collections.Generic.List`1<System.Object>::get_Count()
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR int32_t List_1_get_Count_m5D847939ABB9A78203B062CAFFE975792174D00F_gshared_inline (List_1_t3F94120C77410A62EAE48421CF166B83AB95A2F5 * __this, const RuntimeMethod* method);
// System.Void System.Collections.Generic.List`1<System.Int32>::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void List_1__ctor_m45E78772E9157F6CD684A69AAB07CE4082FE5FFD_gshared (List_1_t260B41F956D673396C33A4CF94E8D6C4389EACB7 * __this, const RuntimeMethod* method);
// System.Void System.Collections.Generic.List`1<System.Int32>::Add(!0)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void List_1_Add_mEE653047BDB3486ACC2E16DC6C3422A0BA48F01F_gshared (List_1_t260B41F956D673396C33A4CF94E8D6C4389EACB7 * __this, int32_t ___item0, const RuntimeMethod* method);
// System.Int32 System.Collections.Generic.List`1<System.Int32>::get_Count()
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR int32_t List_1_get_Count_m7FA90926D9267868473EF90941F6BF794EC87FF2_gshared_inline (List_1_t260B41F956D673396C33A4CF94E8D6C4389EACB7 * __this, const RuntimeMethod* method);
// !0 System.Collections.Generic.List`1<System.Int32>::get_Item(System.Int32)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR int32_t List_1_get_Item_m730FCAD2646FA94B07D1216A512B09AB9F0BBA5D_gshared_inline (List_1_t260B41F956D673396C33A4CF94E8D6C4389EACB7 * __this, int32_t ___index0, const RuntimeMethod* method);
// System.Void System.Collections.Generic.List`1<System.Int32>::RemoveAt(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void List_1_RemoveAt_m4A6ABD183823501A4F9A6082D9EDC589029AD221_gshared (List_1_t260B41F956D673396C33A4CF94E8D6C4389EACB7 * __this, int32_t ___index0, const RuntimeMethod* method);
// !!0[] UnityEngine.GameObject::GetComponentsInChildren<System.Object>()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE* GameObject_GetComponentsInChildren_TisRuntimeObject_m6662AE3C936281A25097CCBD9098A9F85C69279A_gshared (GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * __this, const RuntimeMethod* method);

// System.Void System.Collections.Generic.List`1<CrazyMinnow.SALSA.Shape>::.ctor()
inline void List_1__ctor_m8AF93F3DEF238332B1CE69F617C558510043D656 (List_1_t18177CFBF082431F1077382C8DEF236A68EE2240 * __this, const RuntimeMethod* method)
{
	((  void (*) (List_1_t18177CFBF082431F1077382C8DEF236A68EE2240 *, const RuntimeMethod*))List_1__ctor_m0F0E00088CF56FEACC9E32D8B7D91B93D91DAA3B_gshared)(__this, method);
}
// System.Void System.Object::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Object__ctor_m88880E0413421D13FD95325EDCE231707CE1F405 (RuntimeObject * __this, const RuntimeMethod* method);
// System.Single UnityEngine.Random::Range(System.Single,System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float Random_Range_mC15372D42A9ABDCAC3DE82E114D60A40C9C311D2 (float ___minInclusive0, float ___maxInclusive1, const RuntimeMethod* method);
// System.Collections.IEnumerator CrazyMinnow.SALSA.RandomEyes2D::TargetAffinityUpdate(System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* RandomEyes2D_TargetAffinityUpdate_m105F6F7B0C6788A396093FE3D0431CD3331FCCB0 (RandomEyes2D_t253864BE689DE7E477335D17ECD2EC77001B377F * __this, float ___updateDelay0, const RuntimeMethod* method);
// UnityEngine.Coroutine UnityEngine.MonoBehaviour::StartCoroutine(System.Collections.IEnumerator)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Coroutine_t899D5232EF542CB8BA70AF9ECEECA494FAA9CCB7 * MonoBehaviour_StartCoroutine_m3E33706D38B23CDD179E99BAD61E32303E9CC719 (MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A * __this, RuntimeObject* ___routine0, const RuntimeMethod* method);
// System.Boolean UnityEngine.Object::op_Implicit(UnityEngine.Object)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool Object_op_Implicit_mC8214E4F028CC2F036CC82BDB81D102A02893499 (Object_tF2F3778131EFF286AF62B7B013A170F95A91571A * ___exists0, const RuntimeMethod* method);
// !!0 UnityEngine.Component::GetComponent<UnityEngine.SpriteRenderer>()
inline SpriteRenderer_t3F35AD5498243C170B46F5FFDB582AAEF78615EF * Component_GetComponent_TisSpriteRenderer_t3F35AD5498243C170B46F5FFDB582AAEF78615EF_m7224DFA0D29BF2205FEA6E432D9BCB4133F6E491 (Component_t62FBC8D2420DA4BE9037AFE430740F6B3EECA684 * __this, const RuntimeMethod* method)
{
	return ((  SpriteRenderer_t3F35AD5498243C170B46F5FFDB582AAEF78615EF * (*) (Component_t62FBC8D2420DA4BE9037AFE430740F6B3EECA684 *, const RuntimeMethod*))Component_GetComponent_TisRuntimeObject_m69D9C576D6DD024C709E29EEADBC8041299A3AA7_gshared)(__this, method);
}
// System.Boolean UnityEngine.Object::op_Inequality(UnityEngine.Object,UnityEngine.Object)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool Object_op_Inequality_mE1F187520BD83FB7D86A6D850710C4D42B864E90 (Object_tF2F3778131EFF286AF62B7B013A170F95A91571A * ___x0, Object_tF2F3778131EFF286AF62B7B013A170F95A91571A * ___y1, const RuntimeMethod* method);
// UnityEngine.Transform UnityEngine.Component::get_transform()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * Component_get_transform_mE8496EBC45BEB1BADB5F314960F1DF1C952FA11F (Component_t62FBC8D2420DA4BE9037AFE430740F6B3EECA684 * __this, const RuntimeMethod* method);
// UnityEngine.Vector3 UnityEngine.Transform::get_position()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  Transform_get_position_m40A8A9895568D56FFC687B57F30E8D53CB5EA341 (Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * __this, const RuntimeMethod* method);
// System.String System.String::Concat(System.Object,System.Object)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* String_Concat_m4D0DDA7FEDB75304E5FDAF8489A0478EE58A45F2 (RuntimeObject * ___arg00, RuntimeObject * ___arg11, const RuntimeMethod* method);
// System.Void UnityEngine.GameObject::.ctor(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void GameObject__ctor_mDF8BF31EAE3E03F24421531B25FB4BEDB7C87144 (GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * __this, String_t* ___name0, const RuntimeMethod* method);
// !!0 UnityEngine.GameObject::AddComponent<CrazyMinnow.SALSA.RandomEyes2DGizmo>()
inline RandomEyes2DGizmo_t241646E16376D86639D345C821DF03D9EDFDDDDD * GameObject_AddComponent_TisRandomEyes2DGizmo_t241646E16376D86639D345C821DF03D9EDFDDDDD_m66CEF18356A343054A35723CCD97E26EA315A0DC (GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * __this, const RuntimeMethod* method)
{
	return ((  RandomEyes2DGizmo_t241646E16376D86639D345C821DF03D9EDFDDDDD * (*) (GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 *, const RuntimeMethod*))GameObject_AddComponent_TisRuntimeObject_mBDBD6EC58A4409E35E4C5D08757C36E4938256B1_gshared)(__this, method);
}
// UnityEngine.Transform UnityEngine.GameObject::get_transform()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * GameObject_get_transform_m16A80BB92B6C8C5AB696E447014D45EDF1E4DE34 (GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * __this, const RuntimeMethod* method);
// System.Void UnityEngine.Transform::set_position(UnityEngine.Vector3)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Transform_set_position_mB169E52D57EEAC1E3F22C5395968714E4F00AC91 (Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * __this, Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___value0, const RuntimeMethod* method);
// System.Void UnityEngine.Transform::set_parent(UnityEngine.Transform)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Transform_set_parent_mEAE304E1A804E8B83054CEECB5BF1E517196EC13 (Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * __this, Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * ___value0, const RuntimeMethod* method);
// System.Void UnityEngine.Renderer::set_enabled(System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Renderer_set_enabled_mFFBA418C428C1B2B151C77B879DD10C393D9D95B (Renderer_t58147AB5B00224FE1460FD47542DC0DA7EC9378C * __this, bool ___value0, const RuntimeMethod* method);
// System.Boolean UnityEngine.Object::op_Equality(UnityEngine.Object,UnityEngine.Object)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool Object_op_Equality_mEE9EC7EB5C7DC3E95B94AB904E1986FC4D566D54 (Object_tF2F3778131EFF286AF62B7B013A170F95A91571A * ___x0, Object_tF2F3778131EFF286AF62B7B013A170F95A91571A * ___y1, const RuntimeMethod* method);
// UnityEngine.Vector3 UnityEngine.Transform::get_localScale()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  Transform_get_localScale_mD9DF6CA81108C2A6002B5EA2BE25A6CD2723D046 (Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * __this, const RuntimeMethod* method);
// System.Single UnityEngine.Vector3::get_magnitude()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float Vector3_get_magnitude_mDDD40612220D8104E77E993E18A101A69A944991 (Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * __this, const RuntimeMethod* method);
// System.Single UnityEngine.Time::get_time()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float Time_get_time_m1A186074B1FCD448AB13A4B9D715AB9ED0B40844 (const RuntimeMethod* method);
// System.Void CrazyMinnow.SALSA.RandomEyes2D::Blink(System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void RandomEyes2D_Blink_m2105B90CDDB68CD9D6F42752C3B7BAB853123B43 (RandomEyes2D_t253864BE689DE7E477335D17ECD2EC77001B377F * __this, float ___duration0, const RuntimeMethod* method);
// System.Void CrazyMinnow.SALSA.RandomEyes2D::LookTracking(UnityEngine.Vector3)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void RandomEyes2D_LookTracking_m4C39BFE72EEA5E5F4E707B3BD925D259FCF6B77C (RandomEyes2D_t253864BE689DE7E477335D17ECD2EC77001B377F * __this, Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___lookDir0, const RuntimeMethod* method);
// System.Void UnityEngine.Vector3::.ctor(System.Single,System.Single,System.Single)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void Vector3__ctor_m57495F692C6CE1CEF278CAD9A98221165D37E636_inline (Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * __this, float ___x0, float ___y1, float ___z2, const RuntimeMethod* method);
// System.Int32 UnityEngine.Random::Range(System.Int32,System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t Random_Range_m4B3A0037ACA057F33C94508F908546B9317D996A (int32_t ___minInclusive0, int32_t ___maxExclusive1, const RuntimeMethod* method);
// UnityEngine.Vector3 UnityEngine.Vector3::op_Subtraction(UnityEngine.Vector3,UnityEngine.Vector3)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  Vector3_op_Subtraction_m2725C96965D5C0B1F9715797E51762B13A5FED58_inline (Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___a0, Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___b1, const RuntimeMethod* method);
// System.Single CrazyMinnow.SALSA.RandomEyes2D::ProportionalMovement(System.Single,System.Single,System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float RandomEyes2D_ProportionalMovement_m4EC2F23BA31627C8C60313AA4695FA4B11A1651B (RandomEyes2D_t253864BE689DE7E477335D17ECD2EC77001B377F * __this, float ___currentEyeScale0, float ___largestEyeScale1, float ___amount2, const RuntimeMethod* method);
// UnityEngine.Vector3 UnityEngine.Vector3::ClampMagnitude(UnityEngine.Vector3,System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  Vector3_ClampMagnitude_mF85598307D6CF3B4E5BEEB218CEDDCE39CDF3336 (Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___vector0, float ___maxLength1, const RuntimeMethod* method);
// UnityEngine.Vector3 UnityEngine.Vector3::op_Addition(UnityEngine.Vector3,UnityEngine.Vector3)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  Vector3_op_Addition_mEE4F672B923CCB184C39AABCA33443DB218E50E0_inline (Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___a0, Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___b1, const RuntimeMethod* method);
// UnityEngine.Vector3 CrazyMinnow.SALSA.SalsaUtility::Lerp2DEyeTracking(UnityEngine.Vector3,UnityEngine.Vector3,System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  SalsaUtility_Lerp2DEyeTracking_m49C8BAED6D6F4ABD41691661601291A0F20A800D (Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___currentPosition0, Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___newPosition1, float ___blendSpeed2, const RuntimeMethod* method);
// System.Void CrazyMinnow.SALSA.RandomEyes2D/<TargetAffinityUpdate>d__0::.ctor(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CTargetAffinityUpdateU3Ed__0__ctor_m11474126BC59372CD200224F8F86C2D80D12BD0C (U3CTargetAffinityUpdateU3Ed__0_tB9D47FFD16FA8A3CF1860EC8ADB05A787654DF78 * __this, int32_t ___U3CU3E1__state0, const RuntimeMethod* method);
// System.Collections.IEnumerator CrazyMinnow.SALSA.RandomEyes2D::BlinkEyes(System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* RandomEyes2D_BlinkEyes_mA9926DC8191C467D2188EC8D8F3AFFBB7AF92C24 (RandomEyes2D_t253864BE689DE7E477335D17ECD2EC77001B377F * __this, float ___duration0, const RuntimeMethod* method);
// System.Void CrazyMinnow.SALSA.RandomEyes2D/<BlinkEyes>d__2::.ctor(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CBlinkEyesU3Ed__2__ctor_m794D2B438E817C03906B33BA6CEA16630030D920 (U3CBlinkEyesU3Ed__2_t7DFD119FC328CD8363C654182C85B8B62BA190D1 * __this, int32_t ___U3CU3E1__state0, const RuntimeMethod* method);
// System.Single CrazyMinnow.SALSA.SalsaUtility::ClampValue(System.Single,System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float SalsaUtility_ClampValue_m167E1BE774D495060E1FAA2503DD01D2C00EE208 (float ___currentVal0, float ___rangeOfMotion1, const RuntimeMethod* method);
// System.Void UnityEngine.Renderer::set_sortingOrder(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Renderer_set_sortingOrder_mAABE4F8F9B158068C8A1582ACE0BFEA3CF499139 (Renderer_t58147AB5B00224FE1460FD47542DC0DA7EC9378C * __this, int32_t ___value0, const RuntimeMethod* method);
// System.Single UnityEngine.Mathf::Clamp(System.Single,System.Single,System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float Mathf_Clamp_m2416F3B785C8F135863E3D17E5B0CB4174797B87 (float ___value0, float ___min1, float ___max2, const RuntimeMethod* method);
// System.Void UnityEngine.SpriteRenderer::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void SpriteRenderer__ctor_m5724D3A61E7A1DDF3EFB2A8B47A8A1348CB188D3 (SpriteRenderer_t3F35AD5498243C170B46F5FFDB582AAEF78615EF * __this, const RuntimeMethod* method);
// System.Void UnityEngine.Vector2::.ctor(System.Single,System.Single)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void Vector2__ctor_m9F1F2D5EB5D1FF7091BB527AC8A72CBB309D115E_inline (Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 * __this, float ___x0, float ___y1, const RuntimeMethod* method);
// System.Void UnityEngine.MonoBehaviour::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void MonoBehaviour__ctor_mC0995D847F6A95B1A553652636C38A2AA8B13BED (MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A * __this, const RuntimeMethod* method);
// !!0 UnityEngine.Component::GetComponent<UnityEngine.SkinnedMeshRenderer>()
inline SkinnedMeshRenderer_t126F4D6010E0F4B2685A7817B0A9171805D8F496 * Component_GetComponent_TisSkinnedMeshRenderer_t126F4D6010E0F4B2685A7817B0A9171805D8F496_m48EF3D17CF12700CC28C88CEFBB6741D6E1FFFE3 (Component_t62FBC8D2420DA4BE9037AFE430740F6B3EECA684 * __this, const RuntimeMethod* method)
{
	return ((  SkinnedMeshRenderer_t126F4D6010E0F4B2685A7817B0A9171805D8F496 * (*) (Component_t62FBC8D2420DA4BE9037AFE430740F6B3EECA684 *, const RuntimeMethod*))Component_GetComponent_TisRuntimeObject_m69D9C576D6DD024C709E29EEADBC8041299A3AA7_gshared)(__this, method);
}
// System.Collections.IEnumerator CrazyMinnow.SALSA.RandomEyes3D::TargetAffinityUpdate()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* RandomEyes3D_TargetAffinityUpdate_mF7439C88C6DF7E156DBF3E40D67C57EC64EF3E77 (RandomEyes3D_t6868157C4605BD0225E1B08301F110E72336CE45 * __this, const RuntimeMethod* method);
// System.Single UnityEngine.SkinnedMeshRenderer::GetBlendShapeWeight(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float SkinnedMeshRenderer_GetBlendShapeWeight_m3F662DD48CC110C8429E53FD2A2E33DE601AA792 (SkinnedMeshRenderer_t126F4D6010E0F4B2685A7817B0A9171805D8F496 * __this, int32_t ___index0, const RuntimeMethod* method);
// System.Void CrazyMinnow.SALSA.RandomEyes3D::Blink(System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void RandomEyes3D_Blink_mCCC507CFFA092075E2BFAF242D716F81A073771C (RandomEyes3D_t6868157C4605BD0225E1B08301F110E72336CE45 * __this, float ___duration0, const RuntimeMethod* method);
// UnityEngine.Vector3 UnityEngine.Transform::InverseTransformPoint(UnityEngine.Vector3)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  Transform_InverseTransformPoint_m476ABC8F3F14824D7D82FE2C54CEE5A151A669B8 (Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * __this, Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___position0, const RuntimeMethod* method);
// UnityEngine.Vector3 UnityEngine.Vector3::get_normalized()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  Vector3_get_normalized_m2FA6DF38F97BDA4CCBDAE12B9FE913A241DAC8D5 (Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * __this, const RuntimeMethod* method);
// System.Void CrazyMinnow.SALSA.RandomEyes3D::LookTracking(UnityEngine.Vector3)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void RandomEyes3D_LookTracking_m7F29611D970378663A41B66374456DDAEDCC3205 (RandomEyes3D_t6868157C4605BD0225E1B08301F110E72336CE45 * __this, Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___lookDir0, const RuntimeMethod* method);
// System.Void CrazyMinnow.SALSA.RandomEyesCustomShapeStatus::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void RandomEyesCustomShapeStatus__ctor_m97BF52FFB152BF60CB27311F72EA547C783657C0 (RandomEyesCustomShapeStatus_tAE795B1C1EF38DFDE0535D6A919E25A96283DD66 * __this, const RuntimeMethod* method);
// System.Void CrazyMinnow.SALSA.RandomEyesCustomShapeStatus::set_instance(UnityEngine.Object)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void RandomEyesCustomShapeStatus_set_instance_m4F5644F9DB6E38215FA12B1892E396CA0D8281B1_inline (RandomEyesCustomShapeStatus_tAE795B1C1EF38DFDE0535D6A919E25A96283DD66 * __this, Object_tF2F3778131EFF286AF62B7B013A170F95A91571A * ___value0, const RuntimeMethod* method);
// System.Void CrazyMinnow.SALSA.RandomEyes3D::CustomShapeChanged(CrazyMinnow.SALSA.RandomEyesCustomShapeStatus)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void RandomEyes3D_CustomShapeChanged_m380A0E30479AFAD7D59CA1222BB6E77DCD1FF068 (RandomEyes3D_t6868157C4605BD0225E1B08301F110E72336CE45 * __this, RandomEyesCustomShapeStatus_tAE795B1C1EF38DFDE0535D6A919E25A96283DD66 * ___customShape0, const RuntimeMethod* method);
// System.Boolean System.String::op_Equality(System.String,System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool String_op_Equality_m2B91EE68355F142F67095973D32EB5828B7B73CB (String_t* ___a0, String_t* ___b1, const RuntimeMethod* method);
// System.Void UnityEngine.SkinnedMeshRenderer::SetBlendShapeWeight(System.Int32,System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void SkinnedMeshRenderer_SetBlendShapeWeight_mF546F3567C5039C217AD1E32157B992B4124B5FD (SkinnedMeshRenderer_t126F4D6010E0F4B2685A7817B0A9171805D8F496 * __this, int32_t ___index0, float ___value1, const RuntimeMethod* method);
// System.Single CrazyMinnow.SALSA.SalsaUtility::LerpRangeOfMotion(System.Single,System.Single,System.Single,CrazyMinnow.SALSA.SalsaUtility/BlendDirection)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float SalsaUtility_LerpRangeOfMotion_mDA97D8D31F6D62217220938CD785C48A75AEB246 (float ___currentVal0, float ___blendSpeed1, float ___rangeOfMotion2, int32_t ___blendDirection3, const RuntimeMethod* method);
// System.Boolean System.String::op_Inequality(System.String,System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool String_op_Inequality_mDDA2DDED3E7EF042987EB7180EE3E88105F0AAE2 (String_t* ___a0, String_t* ___b1, const RuntimeMethod* method);
// System.Void CrazyMinnow.SALSA.RandomEyes3D::SetCustomShapeBlendSpeed(System.String,System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void RandomEyes3D_SetCustomShapeBlendSpeed_mE6C02207767F40A39806CEEFB76C1E90FC6B5693 (RandomEyes3D_t6868157C4605BD0225E1B08301F110E72336CE45 * __this, String_t* ___shapeName0, float ___blend1, const RuntimeMethod* method);
// System.Void CrazyMinnow.SALSA.RandomEyes3D::SetCustomShapeRangeOfMotion(System.String,System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void RandomEyes3D_SetCustomShapeRangeOfMotion_m203068B98D9D262BB4D6B2C8AEECE0875FF4AA14 (RandomEyes3D_t6868157C4605BD0225E1B08301F110E72336CE45 * __this, String_t* ___shapeName0, float ___range1, const RuntimeMethod* method);
// System.Void CrazyMinnow.SALSA.RandomEyes3D::SetCustomShapeOverride(System.String,System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void RandomEyes3D_SetCustomShapeOverride_mA4614D96A02DF336129C3255D4866EC9499AC131 (RandomEyes3D_t6868157C4605BD0225E1B08301F110E72336CE45 * __this, String_t* ___shapeName0, float ___duration1, const RuntimeMethod* method);
// System.Single UnityEngine.Time::get_deltaTime()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float Time_get_deltaTime_mCC15F147DA67F38C74CE408FB5D7FF4A87DA2290 (const RuntimeMethod* method);
// System.Single UnityEngine.Mathf::Lerp(System.Single,System.Single,System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float Mathf_Lerp_m8A2A50B945F42D579EDF44D5EE79E85A4DA59616 (float ___a0, float ___b1, float ___t2, const RuntimeMethod* method);
// System.Single CrazyMinnow.SALSA.SalsaUtility::Lerp3DEyeTracking(System.Single,System.Single,System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float SalsaUtility_Lerp3DEyeTracking_m914ED29E6BF956DD834E8CF6B8C5B20591A22C19 (float ___currentVal0, float ___targetVal1, float ___blendSpeed2, const RuntimeMethod* method);
// System.Void CrazyMinnow.SALSA.RandomEyes3D/<TargetAffinityUpdate>d__0::.ctor(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CTargetAffinityUpdateU3Ed__0__ctor_mCAFDF47555144A310E4F743A29A8C68AAAA8FDFB (U3CTargetAffinityUpdateU3Ed__0_tCF892F05CB3FC0D3FC4F3984F53D8C429D011013 * __this, int32_t ___U3CU3E1__state0, const RuntimeMethod* method);
// System.Void CrazyMinnow.SALSA.RandomEyes3D/<BlinkEyes>d__2::.ctor(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CBlinkEyesU3Ed__2__ctor_m1C070BB710ACF1198445B49A02B0EE406A56F50C (U3CBlinkEyesU3Ed__2_t05DA28F00D8E1A9E64D0728065DB53DA96232044 * __this, int32_t ___U3CU3E1__state0, const RuntimeMethod* method);
// System.Void CrazyMinnow.SALSA.RandomEyes3D/<SetCustomShapeOverrideDuration>d__4::.ctor(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CSetCustomShapeOverrideDurationU3Ed__4__ctor_mB66C5B43F2C15F540F85F1845A71DB41035E6FAD (U3CSetCustomShapeOverrideDurationU3Ed__4_tA42191F3DEC3EBAD53A277EAC5C346C81A3BC122 * __this, int32_t ___U3CU3E1__state0, const RuntimeMethod* method);
// System.Void CrazyMinnow.SALSA.RandomEyes3D/<SetCustomShapeOverrideDuration>d__6::.ctor(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CSetCustomShapeOverrideDurationU3Ed__6__ctor_m90B95A85DE3597BE73729C4BD7FF6D9756F8D6EB (U3CSetCustomShapeOverrideDurationU3Ed__6_t3067670AF5E03DD7899B8F7492C6EF59E5B77DB0 * __this, int32_t ___U3CU3E1__state0, const RuntimeMethod* method);
// System.Void CrazyMinnow.SALSA.RandomEyes3D/<SetCustomShapeDuration>d__8::.ctor(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CSetCustomShapeDurationU3Ed__8__ctor_mF6CD6A348AAE88971E18E1E4E94D43FF9A964EB3 (U3CSetCustomShapeDurationU3Ed__8_t1CD4174D8597ACF6A20E2146BB78651E84AE273B * __this, int32_t ___U3CU3E1__state0, const RuntimeMethod* method);
// System.Void CrazyMinnow.SALSA.RandomEyes3D/<SetCustomShapeDuration>d__a::.ctor(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CSetCustomShapeDurationU3Ed__a__ctor_m22ECCEFDEC5DB0D1080451AE79B9E22EC5A618EB (U3CSetCustomShapeDurationU3Ed__a_tF3A147C5E712D0581C2B12133986348BFBB67140 * __this, int32_t ___U3CU3E1__state0, const RuntimeMethod* method);
// System.Void UnityEngine.GameObject::BroadcastMessage(System.String,System.Object,UnityEngine.SendMessageOptions)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void GameObject_BroadcastMessage_m2B5D6163ABB0ED80A381A41DC84ED48CC10212AD (GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * __this, String_t* ___methodName0, RuntimeObject * ___parameter1, int32_t ___options2, const RuntimeMethod* method);
// System.Void UnityEngine.GameObject::SendMessage(System.String,System.Object,UnityEngine.SendMessageOptions)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void GameObject_SendMessage_mD49CCADA51268480B585733DD7C6540CCCC6EF5C (GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * __this, String_t* ___methodName0, RuntimeObject * ___value1, int32_t ___options2, const RuntimeMethod* method);
// System.Collections.IEnumerator CrazyMinnow.SALSA.RandomEyes3D::DoBlink(System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* RandomEyes3D_DoBlink_m82BC786A5A89543456B234AAD2FC795DD86698FC (RandomEyes3D_t6868157C4605BD0225E1B08301F110E72336CE45 * __this, float ___duration0, const RuntimeMethod* method);
// System.Void CrazyMinnow.SALSA.RandomEyes3D/<DoBlink>d__c::.ctor(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CDoBlinkU3Ed__c__ctor_m9D9176B98A436D63C7C71A253F9242C3ACD37F00 (U3CDoBlinkU3Ed__c_t8CF4EE9B667E494EE34FBB05C5E960320E5C8418 * __this, int32_t ___U3CU3E1__state0, const RuntimeMethod* method);
// System.Collections.IEnumerator CrazyMinnow.SALSA.RandomEyes3D::SetCustomShapeOverrideDuration(System.String,System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* RandomEyes3D_SetCustomShapeOverrideDuration_m0A72CCB00C2DB1FBFFE2A7CBB1B85685B0EA52CC (RandomEyes3D_t6868157C4605BD0225E1B08301F110E72336CE45 * __this, String_t* ___shapeName0, float ___duration1, const RuntimeMethod* method);
// System.Collections.IEnumerator CrazyMinnow.SALSA.RandomEyes3D::SetCustomShapeOverrideDuration(System.Int32,System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* RandomEyes3D_SetCustomShapeOverrideDuration_m07722506C2734D00B002B64D6BD1FE3EE9908F69 (RandomEyes3D_t6868157C4605BD0225E1B08301F110E72336CE45 * __this, int32_t ___shapeIndex0, float ___duration1, const RuntimeMethod* method);
// UnityEngine.Mesh UnityEngine.SkinnedMeshRenderer::get_sharedMesh()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Mesh_t2F5992DBA650D5862B43D3823ACD997132A57DA6 * SkinnedMeshRenderer_get_sharedMesh_mFD55E307943C1C4B2E2E8632F15B41CCBD8D91F2 (SkinnedMeshRenderer_t126F4D6010E0F4B2685A7817B0A9171805D8F496 * __this, const RuntimeMethod* method);
// System.String UnityEngine.Mesh::GetBlendShapeName(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* Mesh_GetBlendShapeName_mF28E636D7F66608E6FD9152ACD9547B78C895000 (Mesh_t2F5992DBA650D5862B43D3823ACD997132A57DA6 * __this, int32_t ___shapeIndex0, const RuntimeMethod* method);
// System.Collections.IEnumerator CrazyMinnow.SALSA.RandomEyes3D::SetCustomShapeDuration(System.String,System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* RandomEyes3D_SetCustomShapeDuration_mC9EFA2C619003EB530936AF8AC02019814A65099 (RandomEyes3D_t6868157C4605BD0225E1B08301F110E72336CE45 * __this, String_t* ___shapeName0, float ___duration1, const RuntimeMethod* method);
// System.Collections.IEnumerator CrazyMinnow.SALSA.RandomEyes3D::SetCustomShapeDuration(System.Int32,System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* RandomEyes3D_SetCustomShapeDuration_m3B9A380330E625EA5EB3E911590D6CEF499B6F53 (RandomEyes3D_t6868157C4605BD0225E1B08301F110E72336CE45 * __this, int32_t ___shapeIndex0, float ___duration1, const RuntimeMethod* method);
// !0 System.Collections.Generic.List`1<CrazyMinnow.SALSA.Group>::get_Item(System.Int32)
inline Group_t0CC34C3639F4AF852CE020ABE05C91D971EBEDB1 * List_1_get_Item_m6C9326C28DBBC58E66D0FDBC7DD547C22170CEB5_inline (List_1_t4CEBDA821397C4F46F5C0D752B030DA847F86F42 * __this, int32_t ___index0, const RuntimeMethod* method)
{
	return ((  Group_t0CC34C3639F4AF852CE020ABE05C91D971EBEDB1 * (*) (List_1_t4CEBDA821397C4F46F5C0D752B030DA847F86F42 *, int32_t, const RuntimeMethod*))List_1_get_Item_mF00B574E58FB078BB753B05A3B86DD0A7A266B63_gshared_inline)(__this, ___index0, method);
}
// !0 System.Collections.Generic.List`1<CrazyMinnow.SALSA.Shape>::get_Item(System.Int32)
inline Shape_t15D1659B3662125F0DBAA6FE8F716125CF62AF2F * List_1_get_Item_mE46947FFA0AD2152DB5591A5A73B6338F9965EB0_inline (List_1_t18177CFBF082431F1077382C8DEF236A68EE2240 * __this, int32_t ___index0, const RuntimeMethod* method)
{
	return ((  Shape_t15D1659B3662125F0DBAA6FE8F716125CF62AF2F * (*) (List_1_t18177CFBF082431F1077382C8DEF236A68EE2240 *, int32_t, const RuntimeMethod*))List_1_get_Item_mF00B574E58FB078BB753B05A3B86DD0A7A266B63_gshared_inline)(__this, ___index0, method);
}
// System.Void CrazyMinnow.SALSA.RandomEyes3D::SetCustomShapeOverride(System.String,System.Single,System.Single,System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void RandomEyes3D_SetCustomShapeOverride_mAC24BBA3E20FC41A96F383097AD90C35E0EA7066 (RandomEyes3D_t6868157C4605BD0225E1B08301F110E72336CE45 * __this, String_t* ___shapeName0, float ___blendSpeed1, float ___rangeOfMotion2, float ___duration3, const RuntimeMethod* method);
// System.Int32 System.Collections.Generic.List`1<CrazyMinnow.SALSA.Shape>::get_Count()
inline int32_t List_1_get_Count_mCC4A67D6388864B347F3727B2A7172BB3E48AF13_inline (List_1_t18177CFBF082431F1077382C8DEF236A68EE2240 * __this, const RuntimeMethod* method)
{
	return ((  int32_t (*) (List_1_t18177CFBF082431F1077382C8DEF236A68EE2240 *, const RuntimeMethod*))List_1_get_Count_m5D847939ABB9A78203B062CAFFE975792174D00F_gshared_inline)(__this, method);
}
// System.Int32 System.Collections.Generic.List`1<CrazyMinnow.SALSA.Group>::get_Count()
inline int32_t List_1_get_Count_m1A9C5C4BF43BFA79BD47ABB0D67817ACAC8C681F_inline (List_1_t4CEBDA821397C4F46F5C0D752B030DA847F86F42 * __this, const RuntimeMethod* method)
{
	return ((  int32_t (*) (List_1_t4CEBDA821397C4F46F5C0D752B030DA847F86F42 *, const RuntimeMethod*))List_1_get_Count_m5D847939ABB9A78203B062CAFFE975792174D00F_gshared_inline)(__this, method);
}
// System.Void CrazyMinnow.SALSA.RandomEyes3D::SetCustomShapeOverride(System.String,System.Single,System.Single,System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void RandomEyes3D_SetCustomShapeOverride_m9C8E2FDBF2AC9F768DADF1EF2853823C01937A52 (RandomEyes3D_t6868157C4605BD0225E1B08301F110E72336CE45 * __this, String_t* ___shapeName0, float ___blendSpeed1, float ___rangeOfMotion2, bool ___overrideOn3, const RuntimeMethod* method);
// System.Int32 UnityEngine.Mesh::get_blendShapeCount()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t Mesh_get_blendShapeCount_mC80CCEA555E9E5609E3497EECF2B03F9B822CB77 (Mesh_t2F5992DBA650D5862B43D3823ACD997132A57DA6 * __this, const RuntimeMethod* method);
// System.Void CrazyMinnow.SALSA.RandomEyes3D::GetBlendShapes()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void RandomEyes3D_GetBlendShapes_mF9F69C143A9AF121E534B05AC0F6EAD505765562 (RandomEyes3D_t6868157C4605BD0225E1B08301F110E72336CE45 * __this, const RuntimeMethod* method);
// System.Int32 System.String::get_Length()
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR int32_t String_get_Length_m129FC0ADA02FECBED3C0B1A809AE84A5AEE1CF09_inline (String_t* __this, const RuntimeMethod* method);
// System.String System.String::ToLower()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* String_ToLower_m7875A49FE166D0A68F3F6B6E70C0C056EBEFD31D (String_t* __this, const RuntimeMethod* method);
// System.String System.String::Substring(System.Int32,System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* String_Substring_m7A39A2AC0893AE940CF4CEC841326D56FFB9D86B (String_t* __this, int32_t ___startIndex0, int32_t ___length1, const RuntimeMethod* method);
// System.Void CrazyMinnow.SALSA.RandomEyes3D::SetCustomShapeExcludeEyes(System.Boolean)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void RandomEyes3D_SetCustomShapeExcludeEyes_m2907E3D9B857200BD4368EB62CCC3A0B4D51C436_inline (RandomEyes3D_t6868157C4605BD0225E1B08301F110E72336CE45 * __this, bool ___status0, const RuntimeMethod* method);
// System.Void CrazyMinnow.SALSA.RandomEyes3D::SetCustomShapeExcludeMouth(CrazyMinnow.SALSA.Salsa3D)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void RandomEyes3D_SetCustomShapeExcludeMouth_m3063985221F372102DC1B3854A31D75F95655684_inline (RandomEyes3D_t6868157C4605BD0225E1B08301F110E72336CE45 * __this, Salsa3D_tACF83D529139263662FEAAC8F5B53EC43B140C9C * ___salsa3D0, const RuntimeMethod* method);
// System.Void System.Collections.Generic.List`1<System.Int32>::.ctor()
inline void List_1__ctor_m45E78772E9157F6CD684A69AAB07CE4082FE5FFD (List_1_t260B41F956D673396C33A4CF94E8D6C4389EACB7 * __this, const RuntimeMethod* method)
{
	((  void (*) (List_1_t260B41F956D673396C33A4CF94E8D6C4389EACB7 *, const RuntimeMethod*))List_1__ctor_m45E78772E9157F6CD684A69AAB07CE4082FE5FFD_gshared)(__this, method);
}
// System.Void System.Collections.Generic.List`1<System.Int32>::Add(!0)
inline void List_1_Add_mEE653047BDB3486ACC2E16DC6C3422A0BA48F01F (List_1_t260B41F956D673396C33A4CF94E8D6C4389EACB7 * __this, int32_t ___item0, const RuntimeMethod* method)
{
	((  void (*) (List_1_t260B41F956D673396C33A4CF94E8D6C4389EACB7 *, int32_t, const RuntimeMethod*))List_1_Add_mEE653047BDB3486ACC2E16DC6C3422A0BA48F01F_gshared)(__this, ___item0, method);
}
// System.Int32 System.Collections.Generic.List`1<System.Int32>::get_Count()
inline int32_t List_1_get_Count_m7FA90926D9267868473EF90941F6BF794EC87FF2_inline (List_1_t260B41F956D673396C33A4CF94E8D6C4389EACB7 * __this, const RuntimeMethod* method)
{
	return ((  int32_t (*) (List_1_t260B41F956D673396C33A4CF94E8D6C4389EACB7 *, const RuntimeMethod*))List_1_get_Count_m7FA90926D9267868473EF90941F6BF794EC87FF2_gshared_inline)(__this, method);
}
// !0 System.Collections.Generic.List`1<System.Int32>::get_Item(System.Int32)
inline int32_t List_1_get_Item_m730FCAD2646FA94B07D1216A512B09AB9F0BBA5D_inline (List_1_t260B41F956D673396C33A4CF94E8D6C4389EACB7 * __this, int32_t ___index0, const RuntimeMethod* method)
{
	return ((  int32_t (*) (List_1_t260B41F956D673396C33A4CF94E8D6C4389EACB7 *, int32_t, const RuntimeMethod*))List_1_get_Item_m730FCAD2646FA94B07D1216A512B09AB9F0BBA5D_gshared_inline)(__this, ___index0, method);
}
// System.Void System.Collections.Generic.List`1<System.Int32>::RemoveAt(System.Int32)
inline void List_1_RemoveAt_m4A6ABD183823501A4F9A6082D9EDC589029AD221 (List_1_t260B41F956D673396C33A4CF94E8D6C4389EACB7 * __this, int32_t ___index0, const RuntimeMethod* method)
{
	((  void (*) (List_1_t260B41F956D673396C33A4CF94E8D6C4389EACB7 *, int32_t, const RuntimeMethod*))List_1_RemoveAt_m4A6ABD183823501A4F9A6082D9EDC589029AD221_gshared)(__this, ___index0, method);
}
// System.Void CrazyMinnow.SALSA.RandomEyesCustomShape::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void RandomEyesCustomShape__ctor_m55E79792EBDFA8E4E88E7396AB6E31327D5D0DBF (RandomEyesCustomShape_t10BCA0CEC18C2BBF6A8BD7BFAD2AF005D233C354 * __this, const RuntimeMethod* method);
// System.Int32 CrazyMinnow.SALSA.RandomEyes3D::RebuildCurrentCustomShapeList()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t RandomEyes3D_RebuildCurrentCustomShapeList_m645926658AF05E208117F2A4D0EAA998DBD27348 (RandomEyes3D_t6868157C4605BD0225E1B08301F110E72336CE45 * __this, const RuntimeMethod* method);
// UnityEngine.GameObject UnityEngine.Component::get_gameObject()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * Component_get_gameObject_m55DC35B149AFB9157582755383BA954655FE0C5B (Component_t62FBC8D2420DA4BE9037AFE430740F6B3EECA684 * __this, const RuntimeMethod* method);
// !!0[] UnityEngine.GameObject::GetComponentsInChildren<UnityEngine.Transform>()
inline TransformU5BU5D_t7821C0520CC567C0A069329C01AE9C058C7E3F1D* GameObject_GetComponentsInChildren_TisTransform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1_m1D81E170D9B0CD0720A6BCDD722BC8A0B4AA8F0E (GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * __this, const RuntimeMethod* method)
{
	return ((  TransformU5BU5D_t7821C0520CC567C0A069329C01AE9C058C7E3F1D* (*) (GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 *, const RuntimeMethod*))GameObject_GetComponentsInChildren_TisRuntimeObject_m6662AE3C936281A25097CCBD9098A9F85C69279A_gshared)(__this, method);
}
// !!0 UnityEngine.Component::GetComponent<CrazyMinnow.SALSA.RandomEyes3DGizmo>()
inline RandomEyes3DGizmo_t039AFDA0DE2968F2C3DFB63606D40A2C238839A1 * Component_GetComponent_TisRandomEyes3DGizmo_t039AFDA0DE2968F2C3DFB63606D40A2C238839A1_mF418C2AB9AA5AE15ECFB9151E7F925BE3AA284A1 (Component_t62FBC8D2420DA4BE9037AFE430740F6B3EECA684 * __this, const RuntimeMethod* method)
{
	return ((  RandomEyes3DGizmo_t039AFDA0DE2968F2C3DFB63606D40A2C238839A1 * (*) (Component_t62FBC8D2420DA4BE9037AFE430740F6B3EECA684 *, const RuntimeMethod*))Component_GetComponent_TisRuntimeObject_m69D9C576D6DD024C709E29EEADBC8041299A3AA7_gshared)(__this, method);
}
// System.Void UnityEngine.Object::DestroyImmediate(UnityEngine.Object)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Object_DestroyImmediate_mCCED69F4D4C9A4FA3AC30A142CF3D7F085F7C422 (Object_tF2F3778131EFF286AF62B7B013A170F95A91571A * ___obj0, const RuntimeMethod* method);
// System.String UnityEngine.Object::get_name()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* Object_get_name_m0C7BC870ED2F0DC5A2FB09628136CD7D1CB82CFB (Object_tF2F3778131EFF286AF62B7B013A170F95A91571A * __this, const RuntimeMethod* method);
// System.Boolean System.String::Contains(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool String_Contains_mA26BDCCE8F191E8965EB8EEFC18BB4D0F85A075A (String_t* __this, String_t* ___value0, const RuntimeMethod* method);
// System.Int32 UnityEngine.Object::GetInstanceID()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t Object_GetInstanceID_m7CF962BC1DB5C03F3522F88728CB2F514582B501 (Object_tF2F3778131EFF286AF62B7B013A170F95A91571A * __this, const RuntimeMethod* method);
// UnityEngine.Vector3 UnityEngine.Vector3::op_Multiply(UnityEngine.Vector3,System.Single)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  Vector3_op_Multiply_m9EA3D18290418D7B410C7D11C4788C13BFD2C30A_inline (Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___a0, float ___d1, const RuntimeMethod* method);
// !!0 UnityEngine.GameObject::AddComponent<CrazyMinnow.SALSA.RandomEyes3DGizmo>()
inline RandomEyes3DGizmo_t039AFDA0DE2968F2C3DFB63606D40A2C238839A1 * GameObject_AddComponent_TisRandomEyes3DGizmo_t039AFDA0DE2968F2C3DFB63606D40A2C238839A1_m2076D0783F9DCDF3BC97B514CDA27C93C23C5B0C (GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * __this, const RuntimeMethod* method)
{
	return ((  RandomEyes3DGizmo_t039AFDA0DE2968F2C3DFB63606D40A2C238839A1 * (*) (GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 *, const RuntimeMethod*))GameObject_AddComponent_TisRuntimeObject_mBDBD6EC58A4409E35E4C5D08757C36E4938256B1_gshared)(__this, method);
}
// System.Void CrazyMinnow.SALSA.RandomEyesBlendAmounts::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void RandomEyesBlendAmounts__ctor_m20EA40B8CB17580CC2E6FB16FF225D40D1CA54FE (RandomEyesBlendAmounts_t728404128F97998E0D58F3849065DCD32A181621 * __this, const RuntimeMethod* method);
// System.Void System.Collections.Generic.List`1<CrazyMinnow.SALSA.Group>::.ctor()
inline void List_1__ctor_m1B9889869FAF2EC3320EFFDBB879511C4C48A20D (List_1_t4CEBDA821397C4F46F5C0D752B030DA847F86F42 * __this, const RuntimeMethod* method)
{
	((  void (*) (List_1_t4CEBDA821397C4F46F5C0D752B030DA847F86F42 *, const RuntimeMethod*))List_1__ctor_m0F0E00088CF56FEACC9E32D8B7D91B93D91DAA3B_gshared)(__this, method);
}
// System.Void UnityEngine.Gizmos::DrawIcon(UnityEngine.Vector3,System.String,System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Gizmos_DrawIcon_m7591EA380992EF450530CC74B1A7183BF15BF5C3 (Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___center0, String_t* ___name1, bool ___allowScaling2, const RuntimeMethod* method);
// UnityEngine.AudioSource CrazyMinnow.SALSA.Salsa2D::FindAudioSource()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR AudioSource_tC4BF65AF8CDCAA63724BB3CA59A7A29249269E6B * Salsa2D_FindAudioSource_m4F909704BCA8FAD1285CB91F23783882DFD618C4 (Salsa2D_t54C8B8FFE3B8149C565BD2A69D53629E65CFFB62 * __this, const RuntimeMethod* method);
// System.Void CrazyMinnow.SALSA.Salsa2D::SetOrderInLayer(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Salsa2D_SetOrderInLayer_m2F1C9424774C0A54279E6AC5848890A0C365E834 (Salsa2D_t54C8B8FFE3B8149C565BD2A69D53629E65CFFB62 * __this, int32_t ___order0, const RuntimeMethod* method);
// UnityEngine.AudioClip UnityEngine.AudioSource::get_clip()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR AudioClip_t16D2E573E7CC1C5118D8EE0F0692D46866A1C0EE * AudioSource_get_clip_mE4454E38D2C0A4C8CC780A435FC1DBD4D47D16DC (AudioSource_tC4BF65AF8CDCAA63724BB3CA59A7A29249269E6B * __this, const RuntimeMethod* method);
// System.Void UnityEngine.AudioSource::set_clip(UnityEngine.AudioClip)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AudioSource_set_clip_mD1F50F7BA6EA3AF25B4922473352C5180CFF7B2B (AudioSource_tC4BF65AF8CDCAA63724BB3CA59A7A29249269E6B * __this, AudioClip_t16D2E573E7CC1C5118D8EE0F0692D46866A1C0EE * ___value0, const RuntimeMethod* method);
// System.Boolean UnityEngine.AudioSource::get_playOnAwake()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool AudioSource_get_playOnAwake_mDB89801F304962AA2577AB653A7BA3F38F8FFF18 (AudioSource_tC4BF65AF8CDCAA63724BB3CA59A7A29249269E6B * __this, const RuntimeMethod* method);
// System.Void UnityEngine.AudioSource::Play()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AudioSource_Play_mED16664B8F8F3E4D68785C8C00FC96C4DF053AE1 (AudioSource_tC4BF65AF8CDCAA63724BB3CA59A7A29249269E6B * __this, const RuntimeMethod* method);
// System.Void UnityEngine.WaitForSeconds::.ctor(System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void WaitForSeconds__ctor_mD298C4CB9532BBBDE172FC40F3397E30504038D4 (WaitForSeconds_t8F9189BE6E467C98C99177038881F8982E0E4013 * __this, float ___seconds0, const RuntimeMethod* method);
// System.Collections.IEnumerator CrazyMinnow.SALSA.Salsa2D::UpdateSample()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* Salsa2D_UpdateSample_m3ECFBCCA2C397C22A1B8715F2F43223C469F7563 (Salsa2D_t54C8B8FFE3B8149C565BD2A69D53629E65CFFB62 * __this, const RuntimeMethod* method);
// !!0 UnityEngine.Component::GetComponent<UnityEngine.AudioSource>()
inline AudioSource_tC4BF65AF8CDCAA63724BB3CA59A7A29249269E6B * Component_GetComponent_TisAudioSource_tC4BF65AF8CDCAA63724BB3CA59A7A29249269E6B_mCC3B7A679ECE504BFAF8C70C4EF527511F46902F (Component_t62FBC8D2420DA4BE9037AFE430740F6B3EECA684 * __this, const RuntimeMethod* method)
{
	return ((  AudioSource_tC4BF65AF8CDCAA63724BB3CA59A7A29249269E6B * (*) (Component_t62FBC8D2420DA4BE9037AFE430740F6B3EECA684 *, const RuntimeMethod*))Component_GetComponent_TisRuntimeObject_m69D9C576D6DD024C709E29EEADBC8041299A3AA7_gshared)(__this, method);
}
// System.Void CrazyMinnow.SALSA.Salsa2D::TalkStatusChanged()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Salsa2D_TalkStatusChanged_m7EF42D46D528EA86DB9E59DF5FD9C105387EA97C (Salsa2D_t54C8B8FFE3B8149C565BD2A69D53629E65CFFB62 * __this, const RuntimeMethod* method);
// System.Boolean UnityEngine.AudioSource::get_isPlaying()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool AudioSource_get_isPlaying_mEA69477C77D542971F7B454946EF25DFBE0AF6A8 (AudioSource_tC4BF65AF8CDCAA63724BB3CA59A7A29249269E6B * __this, const RuntimeMethod* method);
// System.Void CrazyMinnow.SALSA.SalsaStatus::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void SalsaStatus__ctor_m60EB1381FDD2EA05C23A1AD9A6005AF854E5E89D (SalsaStatus_t67641B5EA3D03836981ED128DA718792E18105C1 * __this, const RuntimeMethod* method);
// System.Void CrazyMinnow.SALSA.SalsaStatus::set_instance(UnityEngine.Object)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void SalsaStatus_set_instance_mB67A73851E6FDD5B713B5F85017446B672B32F63_inline (SalsaStatus_t67641B5EA3D03836981ED128DA718792E18105C1 * __this, Object_tF2F3778131EFF286AF62B7B013A170F95A91571A * ___value0, const RuntimeMethod* method);
// System.Void CrazyMinnow.SALSA.SalsaStatus::set_talkerName(System.String)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void SalsaStatus_set_talkerName_m88E92FFF1840D7288C4474685B82F562E5A6C5EE_inline (SalsaStatus_t67641B5EA3D03836981ED128DA718792E18105C1 * __this, String_t* ___value0, const RuntimeMethod* method);
// System.Void CrazyMinnow.SALSA.SalsaStatus::set_isTalking(System.Boolean)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void SalsaStatus_set_isTalking_m621FF64AC69982D2A47B901157DB29C7F3EC6725_inline (SalsaStatus_t67641B5EA3D03836981ED128DA718792E18105C1 * __this, bool ___value0, const RuntimeMethod* method);
// System.Void CrazyMinnow.SALSA.SalsaStatus::set_clipName(System.String)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void SalsaStatus_set_clipName_m564A868F0584865EBA31D5F8DD38FE39B698F72A_inline (SalsaStatus_t67641B5EA3D03836981ED128DA718792E18105C1 * __this, String_t* ___value0, const RuntimeMethod* method);
// System.Void UnityEngine.SpriteRenderer::set_sprite(UnityEngine.Sprite)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void SpriteRenderer_set_sprite_mBCFFBF3F10C068FD1174C4506DF73E204303FC1A (SpriteRenderer_t3F35AD5498243C170B46F5FFDB582AAEF78615EF * __this, Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 * ___value0, const RuntimeMethod* method);
// System.Void CrazyMinnow.SALSA.Salsa2D/<UpdateSample>d__0::.ctor(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CUpdateSampleU3Ed__0__ctor_m455E0D010D62E290992788EA19E42269D2B13A77 (U3CUpdateSampleU3Ed__0_t610463941DB34736C5EE20E7CA02876D017BE95F * __this, int32_t ___U3CU3E1__state0, const RuntimeMethod* method);
// System.Void UnityEngine.AudioSource::Pause()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AudioSource_Pause_mC4F9932A77B6AA2CC3FB720721B7837CF57B675D (AudioSource_tC4BF65AF8CDCAA63724BB3CA59A7A29249269E6B * __this, const RuntimeMethod* method);
// System.Void UnityEngine.AudioSource::Stop()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AudioSource_Stop_mADA564D223832A64F8CF3EFBDEB534C0D658810F (AudioSource_tC4BF65AF8CDCAA63724BB3CA59A7A29249269E6B * __this, const RuntimeMethod* method);
// UnityEngine.Object UnityEngine.Resources::Load(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Object_tF2F3778131EFF286AF62B7B013A170F95A91571A * Resources_Load_m011631B3740AFD38D496838F10D3DA635A061120 (String_t* ___path0, const RuntimeMethod* method);
// !!0 UnityEngine.GameObject::AddComponent<UnityEngine.AudioSource>()
inline AudioSource_tC4BF65AF8CDCAA63724BB3CA59A7A29249269E6B * GameObject_AddComponent_TisAudioSource_tC4BF65AF8CDCAA63724BB3CA59A7A29249269E6B_m74F4A6C820807E361696D4E8F71DC1E54BBE7F76 (GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * __this, const RuntimeMethod* method)
{
	return ((  AudioSource_tC4BF65AF8CDCAA63724BB3CA59A7A29249269E6B * (*) (GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 *, const RuntimeMethod*))GameObject_AddComponent_TisRuntimeObject_mBDBD6EC58A4409E35E4C5D08757C36E4938256B1_gshared)(__this, method);
}
// UnityEngine.AudioSource CrazyMinnow.SALSA.Salsa3D::FindAudioSource()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR AudioSource_tC4BF65AF8CDCAA63724BB3CA59A7A29249269E6B * Salsa3D_FindAudioSource_mDE3A59BD7075854BE6C62C69780834597C8F2281 (Salsa3D_tACF83D529139263662FEAAC8F5B53EC43B140C9C * __this, const RuntimeMethod* method);
// System.Collections.IEnumerator CrazyMinnow.SALSA.Salsa3D::UpdateSample()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* Salsa3D_UpdateSample_mB63EC23E06A628ADCF06EEFCE0735FF9D2E0D727 (Salsa3D_tACF83D529139263662FEAAC8F5B53EC43B140C9C * __this, const RuntimeMethod* method);
// System.Void CrazyMinnow.SALSA.Salsa3D::TalkStatusChanged()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Salsa3D_TalkStatusChanged_m4C6E88F7F47206891F1C4227E68A3896C39254B5 (Salsa3D_tACF83D529139263662FEAAC8F5B53EC43B140C9C * __this, const RuntimeMethod* method);
// System.Void CrazyMinnow.SALSA.Salsa3D/<UpdateSample>d__0::.ctor(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CUpdateSampleU3Ed__0__ctor_m199B19B5576CA91594C9D8529188676BA2B73670 (U3CUpdateSampleU3Ed__0_tB4DEFA6499D020E6EC2C7109DBED4C19296282D3 * __this, int32_t ___U3CU3E1__state0, const RuntimeMethod* method);
// System.Void CrazyMinnow.SALSA.Salsa3D::GetBlendShapes()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Salsa3D_GetBlendShapes_mAD39B313314A8243FE2172912EC2DD385D1EA751 (Salsa3D_tACF83D529139263662FEAAC8F5B53EC43B140C9C * __this, const RuntimeMethod* method);
// System.Void CrazyMinnow.SALSA.SalsaBlendAmounts::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void SalsaBlendAmounts__ctor_mA2E926D5E2C432845C8162FAEC2012E860EB94E2 (SalsaBlendAmounts_t8FBE1BDDD1306BB8232FCAAD43FC87042840D626 * __this, const RuntimeMethod* method);
// UnityEngine.Vector3 UnityEngine.Vector3::Lerp(UnityEngine.Vector3,UnityEngine.Vector3,System.Single)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  Vector3_Lerp_m8E095584FFA10CF1D3EABCD04F4C83FB82EC5524_inline (Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___a0, Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___b1, float ___t2, const RuntimeMethod* method);
// System.Void System.NotSupportedException::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void NotSupportedException__ctor_m3EA81A5B209A87C3ADA47443F2AFFF735E5256EE (NotSupportedException_tB9D89F0E9470A2C423D239D7C68EE0CFD77F9339 * __this, const RuntimeMethod* method);
// System.Single UnityEngine.Random::get_value()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float Random_get_value_m9AEBC7DF0BB6C57C928B0798349A7D3C0B3FB872 (const RuntimeMethod* method);
// System.Void CrazyMinnow.SALSA.RandomEyes2D::SetLookTargetTracking(UnityEngine.GameObject)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void RandomEyes2D_SetLookTargetTracking_m3B736925D0B69176837D0D89485EB6748354ECE9 (RandomEyes2D_t253864BE689DE7E477335D17ECD2EC77001B377F * __this, GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * ___obj0, const RuntimeMethod* method);
// System.Void CrazyMinnow.SALSA.RandomEyes3D::SetCustomShape(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void RandomEyes3D_SetCustomShape_m0B61CBCBC3389E335E869448A557D96549915D36 (RandomEyes3D_t6868157C4605BD0225E1B08301F110E72336CE45 * __this, String_t* ___shapeName0, const RuntimeMethod* method);
// System.Void CrazyMinnow.SALSA.RandomEyes3D::SetCustomShape(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void RandomEyes3D_SetCustomShape_m46A96B9C7709FD6B8790B63AF40F71C00927A4A2 (RandomEyes3D_t6868157C4605BD0225E1B08301F110E72336CE45 * __this, int32_t ___shapeIndex0, const RuntimeMethod* method);
// System.Void CrazyMinnow.SALSA.RandomEyes3D::SetCustomShapeOverride(System.String,System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void RandomEyes3D_SetCustomShapeOverride_m23761F4B53F0E1A29A36DFFF91801AD211032AC1 (RandomEyes3D_t6868157C4605BD0225E1B08301F110E72336CE45 * __this, String_t* ___shapeName0, bool ___overrideOn1, const RuntimeMethod* method);
// System.Void CrazyMinnow.SALSA.RandomEyes3D::SetCustomShapeOverride(System.Int32,System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void RandomEyes3D_SetCustomShapeOverride_m98357978832E89B5E5B1FD37F2702911980DCD0A (RandomEyes3D_t6868157C4605BD0225E1B08301F110E72336CE45 * __this, int32_t ___shapeIndex0, bool ___overrideOn1, const RuntimeMethod* method);
// System.Void CrazyMinnow.SALSA.RandomEyes3D::SetLookTargetTracking(UnityEngine.GameObject)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void RandomEyes3D_SetLookTargetTracking_m8DC063699D644B556133DD4013E7BB99C95572EC (RandomEyes3D_t6868157C4605BD0225E1B08301F110E72336CE45 * __this, GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * ___obj0, const RuntimeMethod* method);
// System.Void UnityEngine.AudioSource::GetSpectrumData(System.Single[],System.Int32,UnityEngine.FFTWindow)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AudioSource_GetSpectrumData_m534E9BDB8C2924A118A711A0F741581DCEB27266 (AudioSource_tC4BF65AF8CDCAA63724BB3CA59A7A29249269E6B * __this, SingleU5BU5D_t47E8DBF5B597C122478D1FFBD9DD57399A0650FA* ___samples0, int32_t ___channel1, int32_t ___window2, const RuntimeMethod* method);
// System.Void System.Array::Clear(System.Array,System.Int32,System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Array_Clear_mEB42D172C5E0825D340F6209F28578BDDDDCE34F (RuntimeArray * ___array0, int32_t ___index1, int32_t ___length2, const RuntimeMethod* method);
// System.Void CrazyMinnow.SALSA.Salsa2D::UpdateShape()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Salsa2D_UpdateShape_m81111EF6B2731082FA420939A62870B3B59C3AEF (Salsa2D_t54C8B8FFE3B8149C565BD2A69D53629E65CFFB62 * __this, const RuntimeMethod* method);
// System.Single UnityEngine.Mathf::Clamp01(System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float Mathf_Clamp01_m2296D75F0F1292D5C8181C57007A1CA45F440C4C (float ___value0, const RuntimeMethod* method);
// System.Void System.ThrowHelper::ThrowArgumentOutOfRangeException()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ThrowHelper_ThrowArgumentOutOfRangeException_m4841366ABC2B2AFA37C10900551D7E07522C0929 (const RuntimeMethod* method);
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void CrazyMinnow.SALSA.Group::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Group__ctor_mB95A1CB37E84D974BE25697B57B07FBA0BA41A45 (Group_t0CC34C3639F4AF852CE020ABE05C91D971EBEDB1 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1__ctor_m8AF93F3DEF238332B1CE69F617C558510043D656_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_t18177CFBF082431F1077382C8DEF236A68EE2240_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralDA39A3EE5E6B4B0D3255BFEF95601890AFD80709);
		s_Il2CppMethodInitialized = true;
	}
	{
		__this->set_name_0(_stringLiteralDA39A3EE5E6B4B0D3255BFEF95601890AFD80709);
		List_1_t18177CFBF082431F1077382C8DEF236A68EE2240 * L_0 = (List_1_t18177CFBF082431F1077382C8DEF236A68EE2240 *)il2cpp_codegen_object_new(List_1_t18177CFBF082431F1077382C8DEF236A68EE2240_il2cpp_TypeInfo_var);
		List_1__ctor_m8AF93F3DEF238332B1CE69F617C558510043D656(L_0, /*hidden argument*/List_1__ctor_m8AF93F3DEF238332B1CE69F617C558510043D656_RuntimeMethod_var);
		__this->set_shapes_3(L_0);
		Object__ctor_m88880E0413421D13FD95325EDCE231707CE1F405(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void CrazyMinnow.SALSA.RandomEyes2D::Start()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void RandomEyes2D_Start_m5605EE5FB0D11EE87C18900DC7DBF7C52BF1B0F2 (RandomEyes2D_t253864BE689DE7E477335D17ECD2EC77001B377F * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Component_GetComponent_TisSpriteRenderer_t3F35AD5498243C170B46F5FFDB582AAEF78615EF_m7224DFA0D29BF2205FEA6E432D9BCB4133F6E491_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&GameObjectU5BU5D_tA88FC1A1FC9D4D73D0B3984D4B0ECE88F4C47642_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&GameObject_AddComponent_TisRandomEyes2DGizmo_t241646E16376D86639D345C821DF03D9EDFDDDDD_m66CEF18356A343054A35723CCD97E26EA315A0DC_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Int32_tFDE5F8CD43D10453F6A2E0C77FE48C6CC7009046_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&RandomEyes2D_t253864BE689DE7E477335D17ECD2EC77001B377F_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Vector3U5BU5D_t5FB88EAA33E46838BDC2ABDAEA3E8727491CB9E4_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral9F4D5611E34F46999EFF8838045FEB99533BC13E);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * V_2 = NULL;
	int32_t V_3 = 0;
	int32_t V_4 = 0;
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  V_5;
	memset((&V_5), 0, sizeof(V_5));
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  V_6;
	memset((&V_6), 0, sizeof(V_6));
	{
		((RandomEyes2D_t253864BE689DE7E477335D17ECD2EC77001B377F_StaticFields*)il2cpp_codegen_static_fields_for(RandomEyes2D_t253864BE689DE7E477335D17ECD2EC77001B377F_il2cpp_TypeInfo_var))->set_instance_4(__this);
		Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 * L_0 = __this->get_address_of_targetAffinityTimerRange_32();
		float L_1 = L_0->get_x_0();
		Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 * L_2 = __this->get_address_of_targetAffinityTimerRange_32();
		float L_3 = L_2->get_y_1();
		float L_4;
		L_4 = Random_Range_mC15372D42A9ABDCAC3DE82E114D60A40C9C311D2(L_1, L_3, /*hidden argument*/NULL);
		__this->set_targetAffinityTimer_33(L_4);
		float L_5 = __this->get_targetAffinityTimer_33();
		RuntimeObject* L_6;
		L_6 = RandomEyes2D_TargetAffinityUpdate_m105F6F7B0C6788A396093FE3D0431CD3331FCCB0(__this, L_5, /*hidden argument*/NULL);
		Coroutine_t899D5232EF542CB8BA70AF9ECEECA494FAA9CCB7 * L_7;
		L_7 = MonoBehaviour_StartCoroutine_m3E33706D38B23CDD179E99BAD61E32303E9CC719(__this, L_6, /*hidden argument*/NULL);
		SpriteRenderer_t3F35AD5498243C170B46F5FFDB582AAEF78615EF * L_8 = __this->get_character_5();
		IL2CPP_RUNTIME_CLASS_INIT(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		bool L_9;
		L_9 = Object_op_Implicit_mC8214E4F028CC2F036CC82BDB81D102A02893499(L_8, /*hidden argument*/NULL);
		if (L_9)
		{
			goto IL_0053;
		}
	}
	{
		SpriteRenderer_t3F35AD5498243C170B46F5FFDB582AAEF78615EF * L_10;
		L_10 = Component_GetComponent_TisSpriteRenderer_t3F35AD5498243C170B46F5FFDB582AAEF78615EF_m7224DFA0D29BF2205FEA6E432D9BCB4133F6E491(__this, /*hidden argument*/Component_GetComponent_TisSpriteRenderer_t3F35AD5498243C170B46F5FFDB582AAEF78615EF_m7224DFA0D29BF2205FEA6E432D9BCB4133F6E491_RuntimeMethod_var);
		__this->set_character_5(L_10);
	}

IL_0053:
	{
		SpriteRendererU5BU5D_t45BF125944C2F3974C568E650FCCDB591E5ABFBF* L_11 = __this->get_eyes_8();
		NullCheck(L_11);
		Vector3U5BU5D_t5FB88EAA33E46838BDC2ABDAEA3E8727491CB9E4* L_12 = (Vector3U5BU5D_t5FB88EAA33E46838BDC2ABDAEA3E8727491CB9E4*)(Vector3U5BU5D_t5FB88EAA33E46838BDC2ABDAEA3E8727491CB9E4*)SZArrayNew(Vector3U5BU5D_t5FB88EAA33E46838BDC2ABDAEA3E8727491CB9E4_il2cpp_TypeInfo_var, (uint32_t)((int32_t)((int32_t)(((RuntimeArray*)L_11)->max_length))));
		__this->set_trackDirection_30(L_12);
		V_0 = 0;
		goto IL_00a1;
	}

IL_006a:
	{
		SpriteRendererU5BU5D_t45BF125944C2F3974C568E650FCCDB591E5ABFBF* L_13 = __this->get_eyes_8();
		int32_t L_14 = V_0;
		NullCheck(L_13);
		int32_t L_15 = L_14;
		SpriteRenderer_t3F35AD5498243C170B46F5FFDB582AAEF78615EF * L_16 = (L_13)->GetAt(static_cast<il2cpp_array_size_t>(L_15));
		IL2CPP_RUNTIME_CLASS_INIT(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		bool L_17;
		L_17 = Object_op_Inequality_mE1F187520BD83FB7D86A6D850710C4D42B864E90(L_16, (Object_tF2F3778131EFF286AF62B7B013A170F95A91571A *)NULL, /*hidden argument*/NULL);
		if (!L_17)
		{
			goto IL_009d;
		}
	}
	{
		Vector3U5BU5D_t5FB88EAA33E46838BDC2ABDAEA3E8727491CB9E4* L_18 = __this->get_trackDirection_30();
		int32_t L_19 = V_0;
		NullCheck(L_18);
		SpriteRendererU5BU5D_t45BF125944C2F3974C568E650FCCDB591E5ABFBF* L_20 = __this->get_eyes_8();
		int32_t L_21 = V_0;
		NullCheck(L_20);
		int32_t L_22 = L_21;
		SpriteRenderer_t3F35AD5498243C170B46F5FFDB582AAEF78615EF * L_23 = (L_20)->GetAt(static_cast<il2cpp_array_size_t>(L_22));
		NullCheck(L_23);
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_24;
		L_24 = Component_get_transform_mE8496EBC45BEB1BADB5F314960F1DF1C952FA11F(L_23, /*hidden argument*/NULL);
		NullCheck(L_24);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_25;
		L_25 = Transform_get_position_m40A8A9895568D56FFC687B57F30E8D53CB5EA341(L_24, /*hidden argument*/NULL);
		*(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E *)((L_18)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_19))) = L_25;
	}

IL_009d:
	{
		int32_t L_26 = V_0;
		V_0 = ((int32_t)il2cpp_codegen_add((int32_t)L_26, (int32_t)1));
	}

IL_00a1:
	{
		int32_t L_27 = V_0;
		SpriteRendererU5BU5D_t45BF125944C2F3974C568E650FCCDB591E5ABFBF* L_28 = __this->get_eyes_8();
		NullCheck(L_28);
		if ((((int32_t)L_27) < ((int32_t)((int32_t)((int32_t)(((RuntimeArray*)L_28)->max_length))))))
		{
			goto IL_006a;
		}
	}
	{
		GameObjectU5BU5D_tA88FC1A1FC9D4D73D0B3984D4B0ECE88F4C47642* L_29 = __this->get_randomEyes2DGizmos_31();
		if (L_29)
		{
			goto IL_0141;
		}
	}
	{
		SpriteRendererU5BU5D_t45BF125944C2F3974C568E650FCCDB591E5ABFBF* L_30 = __this->get_eyes_8();
		NullCheck(L_30);
		GameObjectU5BU5D_tA88FC1A1FC9D4D73D0B3984D4B0ECE88F4C47642* L_31 = (GameObjectU5BU5D_tA88FC1A1FC9D4D73D0B3984D4B0ECE88F4C47642*)(GameObjectU5BU5D_tA88FC1A1FC9D4D73D0B3984D4B0ECE88F4C47642*)SZArrayNew(GameObjectU5BU5D_tA88FC1A1FC9D4D73D0B3984D4B0ECE88F4C47642_il2cpp_TypeInfo_var, (uint32_t)((int32_t)((int32_t)(((RuntimeArray*)L_30)->max_length))));
		__this->set_randomEyes2DGizmos_31(L_31);
		V_1 = 0;
		goto IL_0136;
	}

IL_00ce:
	{
		int32_t L_32 = V_1;
		int32_t L_33 = L_32;
		RuntimeObject * L_34 = Box(Int32_tFDE5F8CD43D10453F6A2E0C77FE48C6CC7009046_il2cpp_TypeInfo_var, &L_33);
		String_t* L_35;
		L_35 = String_Concat_m4D0DDA7FEDB75304E5FDAF8489A0478EE58A45F2(_stringLiteral9F4D5611E34F46999EFF8838045FEB99533BC13E, L_34, /*hidden argument*/NULL);
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_36 = (GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 *)il2cpp_codegen_object_new(GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319_il2cpp_TypeInfo_var);
		GameObject__ctor_mDF8BF31EAE3E03F24421531B25FB4BEDB7C87144(L_36, L_35, /*hidden argument*/NULL);
		V_2 = L_36;
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_37 = V_2;
		NullCheck(L_37);
		RandomEyes2DGizmo_t241646E16376D86639D345C821DF03D9EDFDDDDD * L_38;
		L_38 = GameObject_AddComponent_TisRandomEyes2DGizmo_t241646E16376D86639D345C821DF03D9EDFDDDDD_m66CEF18356A343054A35723CCD97E26EA315A0DC(L_37, /*hidden argument*/GameObject_AddComponent_TisRandomEyes2DGizmo_t241646E16376D86639D345C821DF03D9EDFDDDDD_m66CEF18356A343054A35723CCD97E26EA315A0DC_RuntimeMethod_var);
		SpriteRendererU5BU5D_t45BF125944C2F3974C568E650FCCDB591E5ABFBF* L_39 = __this->get_eyes_8();
		int32_t L_40 = V_1;
		NullCheck(L_39);
		int32_t L_41 = L_40;
		SpriteRenderer_t3F35AD5498243C170B46F5FFDB582AAEF78615EF * L_42 = (L_39)->GetAt(static_cast<il2cpp_array_size_t>(L_41));
		IL2CPP_RUNTIME_CLASS_INIT(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		bool L_43;
		L_43 = Object_op_Inequality_mE1F187520BD83FB7D86A6D850710C4D42B864E90(L_42, (Object_tF2F3778131EFF286AF62B7B013A170F95A91571A *)NULL, /*hidden argument*/NULL);
		if (!L_43)
		{
			goto IL_0118;
		}
	}
	{
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_44 = V_2;
		NullCheck(L_44);
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_45;
		L_45 = GameObject_get_transform_m16A80BB92B6C8C5AB696E447014D45EDF1E4DE34(L_44, /*hidden argument*/NULL);
		SpriteRendererU5BU5D_t45BF125944C2F3974C568E650FCCDB591E5ABFBF* L_46 = __this->get_eyes_8();
		int32_t L_47 = V_1;
		NullCheck(L_46);
		int32_t L_48 = L_47;
		SpriteRenderer_t3F35AD5498243C170B46F5FFDB582AAEF78615EF * L_49 = (L_46)->GetAt(static_cast<il2cpp_array_size_t>(L_48));
		NullCheck(L_49);
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_50;
		L_50 = Component_get_transform_mE8496EBC45BEB1BADB5F314960F1DF1C952FA11F(L_49, /*hidden argument*/NULL);
		NullCheck(L_50);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_51;
		L_51 = Transform_get_position_m40A8A9895568D56FFC687B57F30E8D53CB5EA341(L_50, /*hidden argument*/NULL);
		NullCheck(L_45);
		Transform_set_position_mB169E52D57EEAC1E3F22C5395968714E4F00AC91(L_45, L_51, /*hidden argument*/NULL);
	}

IL_0118:
	{
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_52 = V_2;
		NullCheck(L_52);
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_53;
		L_53 = GameObject_get_transform_m16A80BB92B6C8C5AB696E447014D45EDF1E4DE34(L_52, /*hidden argument*/NULL);
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_54;
		L_54 = Component_get_transform_mE8496EBC45BEB1BADB5F314960F1DF1C952FA11F(__this, /*hidden argument*/NULL);
		NullCheck(L_53);
		Transform_set_parent_mEAE304E1A804E8B83054CEECB5BF1E517196EC13(L_53, L_54, /*hidden argument*/NULL);
		GameObjectU5BU5D_tA88FC1A1FC9D4D73D0B3984D4B0ECE88F4C47642* L_55 = __this->get_randomEyes2DGizmos_31();
		int32_t L_56 = V_1;
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_57 = V_2;
		NullCheck(L_55);
		ArrayElementTypeCheck (L_55, L_57);
		(L_55)->SetAt(static_cast<il2cpp_array_size_t>(L_56), (GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 *)L_57);
		int32_t L_58 = V_1;
		V_1 = ((int32_t)il2cpp_codegen_add((int32_t)L_58, (int32_t)1));
	}

IL_0136:
	{
		int32_t L_59 = V_1;
		SpriteRendererU5BU5D_t45BF125944C2F3974C568E650FCCDB591E5ABFBF* L_60 = __this->get_eyes_8();
		NullCheck(L_60);
		if ((((int32_t)L_59) < ((int32_t)((int32_t)((int32_t)(((RuntimeArray*)L_60)->max_length))))))
		{
			goto IL_00ce;
		}
	}

IL_0141:
	{
		V_3 = 0;
		goto IL_0167;
	}

IL_0145:
	{
		SpriteRendererU5BU5D_t45BF125944C2F3974C568E650FCCDB591E5ABFBF* L_61 = __this->get_eyeLids_11();
		int32_t L_62 = V_3;
		NullCheck(L_61);
		int32_t L_63 = L_62;
		SpriteRenderer_t3F35AD5498243C170B46F5FFDB582AAEF78615EF * L_64 = (L_61)->GetAt(static_cast<il2cpp_array_size_t>(L_63));
		IL2CPP_RUNTIME_CLASS_INIT(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		bool L_65;
		L_65 = Object_op_Inequality_mE1F187520BD83FB7D86A6D850710C4D42B864E90(L_64, (Object_tF2F3778131EFF286AF62B7B013A170F95A91571A *)NULL, /*hidden argument*/NULL);
		if (!L_65)
		{
			goto IL_0163;
		}
	}
	{
		SpriteRendererU5BU5D_t45BF125944C2F3974C568E650FCCDB591E5ABFBF* L_66 = __this->get_eyeLids_11();
		int32_t L_67 = V_3;
		NullCheck(L_66);
		int32_t L_68 = L_67;
		SpriteRenderer_t3F35AD5498243C170B46F5FFDB582AAEF78615EF * L_69 = (L_66)->GetAt(static_cast<il2cpp_array_size_t>(L_68));
		NullCheck(L_69);
		Renderer_set_enabled_mFFBA418C428C1B2B151C77B879DD10C393D9D95B(L_69, (bool)0, /*hidden argument*/NULL);
	}

IL_0163:
	{
		int32_t L_70 = V_3;
		V_3 = ((int32_t)il2cpp_codegen_add((int32_t)L_70, (int32_t)1));
	}

IL_0167:
	{
		int32_t L_71 = V_3;
		SpriteRendererU5BU5D_t45BF125944C2F3974C568E650FCCDB591E5ABFBF* L_72 = __this->get_eyeLids_11();
		NullCheck(L_72);
		if ((((int32_t)L_71) < ((int32_t)((int32_t)((int32_t)(((RuntimeArray*)L_72)->max_length))))))
		{
			goto IL_0145;
		}
	}
	{
		SpriteRendererU5BU5D_t45BF125944C2F3974C568E650FCCDB591E5ABFBF* L_73 = __this->get_eyes_8();
		if (!L_73)
		{
			goto IL_0238;
		}
	}
	{
		SpriteRenderer_t3F35AD5498243C170B46F5FFDB582AAEF78615EF * L_74 = __this->get_largestEye_38();
		IL2CPP_RUNTIME_CLASS_INIT(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		bool L_75;
		L_75 = Object_op_Equality_mEE9EC7EB5C7DC3E95B94AB904E1986FC4D566D54(L_74, (Object_tF2F3778131EFF286AF62B7B013A170F95A91571A *)NULL, /*hidden argument*/NULL);
		if (!L_75)
		{
			goto IL_01b4;
		}
	}
	{
		SpriteRendererU5BU5D_t45BF125944C2F3974C568E650FCCDB591E5ABFBF* L_76 = __this->get_eyes_8();
		NullCheck(L_76);
		if ((((int32_t)((int32_t)((int32_t)(((RuntimeArray*)L_76)->max_length)))) <= ((int32_t)0)))
		{
			goto IL_01b4;
		}
	}
	{
		SpriteRendererU5BU5D_t45BF125944C2F3974C568E650FCCDB591E5ABFBF* L_77 = __this->get_eyes_8();
		NullCheck(L_77);
		int32_t L_78 = 0;
		SpriteRenderer_t3F35AD5498243C170B46F5FFDB582AAEF78615EF * L_79 = (L_77)->GetAt(static_cast<il2cpp_array_size_t>(L_78));
		IL2CPP_RUNTIME_CLASS_INIT(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		bool L_80;
		L_80 = Object_op_Inequality_mE1F187520BD83FB7D86A6D850710C4D42B864E90(L_79, (Object_tF2F3778131EFF286AF62B7B013A170F95A91571A *)NULL, /*hidden argument*/NULL);
		if (!L_80)
		{
			goto IL_01b4;
		}
	}
	{
		SpriteRendererU5BU5D_t45BF125944C2F3974C568E650FCCDB591E5ABFBF* L_81 = __this->get_eyes_8();
		NullCheck(L_81);
		int32_t L_82 = 0;
		SpriteRenderer_t3F35AD5498243C170B46F5FFDB582AAEF78615EF * L_83 = (L_81)->GetAt(static_cast<il2cpp_array_size_t>(L_82));
		__this->set_largestEye_38(L_83);
	}

IL_01b4:
	{
		V_4 = 0;
		goto IL_022c;
	}

IL_01b9:
	{
		SpriteRenderer_t3F35AD5498243C170B46F5FFDB582AAEF78615EF * L_84 = __this->get_largestEye_38();
		IL2CPP_RUNTIME_CLASS_INIT(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		bool L_85;
		L_85 = Object_op_Inequality_mE1F187520BD83FB7D86A6D850710C4D42B864E90(L_84, (Object_tF2F3778131EFF286AF62B7B013A170F95A91571A *)NULL, /*hidden argument*/NULL);
		if (!L_85)
		{
			goto IL_0226;
		}
	}
	{
		SpriteRendererU5BU5D_t45BF125944C2F3974C568E650FCCDB591E5ABFBF* L_86 = __this->get_eyes_8();
		int32_t L_87 = V_4;
		NullCheck(L_86);
		int32_t L_88 = L_87;
		SpriteRenderer_t3F35AD5498243C170B46F5FFDB582AAEF78615EF * L_89 = (L_86)->GetAt(static_cast<il2cpp_array_size_t>(L_88));
		IL2CPP_RUNTIME_CLASS_INIT(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		bool L_90;
		L_90 = Object_op_Inequality_mE1F187520BD83FB7D86A6D850710C4D42B864E90(L_89, (Object_tF2F3778131EFF286AF62B7B013A170F95A91571A *)NULL, /*hidden argument*/NULL);
		if (!L_90)
		{
			goto IL_0226;
		}
	}
	{
		SpriteRendererU5BU5D_t45BF125944C2F3974C568E650FCCDB591E5ABFBF* L_91 = __this->get_eyes_8();
		int32_t L_92 = V_4;
		NullCheck(L_91);
		int32_t L_93 = L_92;
		SpriteRenderer_t3F35AD5498243C170B46F5FFDB582AAEF78615EF * L_94 = (L_91)->GetAt(static_cast<il2cpp_array_size_t>(L_93));
		NullCheck(L_94);
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_95;
		L_95 = Component_get_transform_mE8496EBC45BEB1BADB5F314960F1DF1C952FA11F(L_94, /*hidden argument*/NULL);
		NullCheck(L_95);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_96;
		L_96 = Transform_get_localScale_mD9DF6CA81108C2A6002B5EA2BE25A6CD2723D046(L_95, /*hidden argument*/NULL);
		V_5 = L_96;
		float L_97;
		L_97 = Vector3_get_magnitude_mDDD40612220D8104E77E993E18A101A69A944991((Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E *)(&V_5), /*hidden argument*/NULL);
		SpriteRenderer_t3F35AD5498243C170B46F5FFDB582AAEF78615EF * L_98 = __this->get_largestEye_38();
		NullCheck(L_98);
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_99;
		L_99 = Component_get_transform_mE8496EBC45BEB1BADB5F314960F1DF1C952FA11F(L_98, /*hidden argument*/NULL);
		NullCheck(L_99);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_100;
		L_100 = Transform_get_localScale_mD9DF6CA81108C2A6002B5EA2BE25A6CD2723D046(L_99, /*hidden argument*/NULL);
		V_6 = L_100;
		float L_101;
		L_101 = Vector3_get_magnitude_mDDD40612220D8104E77E993E18A101A69A944991((Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E *)(&V_6), /*hidden argument*/NULL);
		if ((!(((float)L_97) > ((float)L_101))))
		{
			goto IL_0226;
		}
	}
	{
		SpriteRendererU5BU5D_t45BF125944C2F3974C568E650FCCDB591E5ABFBF* L_102 = __this->get_eyes_8();
		int32_t L_103 = V_4;
		NullCheck(L_102);
		int32_t L_104 = L_103;
		SpriteRenderer_t3F35AD5498243C170B46F5FFDB582AAEF78615EF * L_105 = (L_102)->GetAt(static_cast<il2cpp_array_size_t>(L_104));
		__this->set_largestEye_38(L_105);
		int32_t L_106 = V_4;
		__this->set_largestEyeIndex_37(L_106);
	}

IL_0226:
	{
		int32_t L_107 = V_4;
		V_4 = ((int32_t)il2cpp_codegen_add((int32_t)L_107, (int32_t)1));
	}

IL_022c:
	{
		int32_t L_108 = V_4;
		SpriteRendererU5BU5D_t45BF125944C2F3974C568E650FCCDB591E5ABFBF* L_109 = __this->get_eyes_8();
		NullCheck(L_109);
		if ((((int32_t)L_108) < ((int32_t)((int32_t)((int32_t)(((RuntimeArray*)L_109)->max_length))))))
		{
			goto IL_01b9;
		}
	}

IL_0238:
	{
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_110 = __this->get_lookTarget_17();
		IL2CPP_RUNTIME_CLASS_INIT(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		bool L_111;
		L_111 = Object_op_Implicit_mC8214E4F028CC2F036CC82BDB81D102A02893499(L_110, /*hidden argument*/NULL);
		if (L_111)
		{
			goto IL_025f;
		}
	}
	{
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_112 = __this->get_lookTarget_17();
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_113 = __this->get_lookTargetTracking_18();
		IL2CPP_RUNTIME_CLASS_INIT(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		bool L_114;
		L_114 = Object_op_Inequality_mE1F187520BD83FB7D86A6D850710C4D42B864E90(L_112, L_113, /*hidden argument*/NULL);
		if (!L_114)
		{
			goto IL_025f;
		}
	}
	{
		__this->set_lookTargetTracking_18((GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 *)NULL);
	}

IL_025f:
	{
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_115 = __this->get_lookTarget_17();
		IL2CPP_RUNTIME_CLASS_INIT(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		bool L_116;
		L_116 = Object_op_Implicit_mC8214E4F028CC2F036CC82BDB81D102A02893499(L_115, /*hidden argument*/NULL);
		if (!L_116)
		{
			goto IL_028b;
		}
	}
	{
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_117 = __this->get_lookTarget_17();
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_118 = __this->get_lookTargetTracking_18();
		IL2CPP_RUNTIME_CLASS_INIT(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		bool L_119;
		L_119 = Object_op_Inequality_mE1F187520BD83FB7D86A6D850710C4D42B864E90(L_117, L_118, /*hidden argument*/NULL);
		if (!L_119)
		{
			goto IL_028b;
		}
	}
	{
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_120 = __this->get_lookTarget_17();
		__this->set_lookTargetTracking_18(L_120);
	}

IL_028b:
	{
		float L_121;
		L_121 = Time_get_time_m1A186074B1FCD448AB13A4B9D715AB9ED0B40844(/*hidden argument*/NULL);
		float L_122 = __this->get_maxBlinkInterval_25();
		float L_123;
		L_123 = Random_Range_mC15372D42A9ABDCAC3DE82E114D60A40C9C311D2((0.25f), L_122, /*hidden argument*/NULL);
		__this->set_blinkTimer_36(((float)il2cpp_codegen_add((float)L_121, (float)L_123)));
		float L_124;
		L_124 = Time_get_time_m1A186074B1FCD448AB13A4B9D715AB9ED0B40844(/*hidden argument*/NULL);
		float L_125 = __this->get_maxRandomInterval_22();
		float L_126;
		L_126 = Random_Range_mC15372D42A9ABDCAC3DE82E114D60A40C9C311D2((0.25f), L_125, /*hidden argument*/NULL);
		__this->set_randomTimer_35(((float)il2cpp_codegen_add((float)L_124, (float)L_126)));
		return;
	}
}
// System.Void CrazyMinnow.SALSA.RandomEyes2D::OnEnable()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void RandomEyes2D_OnEnable_mF60FB619533D13BC163E2C09AB09BEEABFCE6225 (RandomEyes2D_t253864BE689DE7E477335D17ECD2EC77001B377F * __this, const RuntimeMethod* method)
{
	{
		Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 * L_0 = __this->get_address_of_targetAffinityTimerRange_32();
		float L_1 = L_0->get_x_0();
		Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 * L_2 = __this->get_address_of_targetAffinityTimerRange_32();
		float L_3 = L_2->get_y_1();
		float L_4;
		L_4 = Random_Range_mC15372D42A9ABDCAC3DE82E114D60A40C9C311D2(L_1, L_3, /*hidden argument*/NULL);
		__this->set_targetAffinityTimer_33(L_4);
		float L_5 = __this->get_targetAffinityTimer_33();
		RuntimeObject* L_6;
		L_6 = RandomEyes2D_TargetAffinityUpdate_m105F6F7B0C6788A396093FE3D0431CD3331FCCB0(__this, L_5, /*hidden argument*/NULL);
		Coroutine_t899D5232EF542CB8BA70AF9ECEECA494FAA9CCB7 * L_7;
		L_7 = MonoBehaviour_StartCoroutine_m3E33706D38B23CDD179E99BAD61E32303E9CC719(__this, L_6, /*hidden argument*/NULL);
		float L_8;
		L_8 = Time_get_time_m1A186074B1FCD448AB13A4B9D715AB9ED0B40844(/*hidden argument*/NULL);
		float L_9 = __this->get_maxBlinkInterval_25();
		float L_10;
		L_10 = Random_Range_mC15372D42A9ABDCAC3DE82E114D60A40C9C311D2((0.25f), L_9, /*hidden argument*/NULL);
		__this->set_blinkTimer_36(((float)il2cpp_codegen_add((float)L_8, (float)L_10)));
		float L_11;
		L_11 = Time_get_time_m1A186074B1FCD448AB13A4B9D715AB9ED0B40844(/*hidden argument*/NULL);
		float L_12 = __this->get_maxRandomInterval_22();
		float L_13;
		L_13 = Random_Range_mC15372D42A9ABDCAC3DE82E114D60A40C9C311D2((0.25f), L_12, /*hidden argument*/NULL);
		__this->set_randomTimer_35(((float)il2cpp_codegen_add((float)L_11, (float)L_13)));
		return;
	}
}
// System.Void CrazyMinnow.SALSA.RandomEyes2D::LateUpdate()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void RandomEyes2D_LateUpdate_m2257F2110275E2A1F2F8D9703A8EF26BD611ECFA (RandomEyes2D_t253864BE689DE7E477335D17ECD2EC77001B377F * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_0 = __this->get_lookTarget_17();
		IL2CPP_RUNTIME_CLASS_INIT(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		bool L_1;
		L_1 = Object_op_Implicit_mC8214E4F028CC2F036CC82BDB81D102A02893499(L_0, /*hidden argument*/NULL);
		if (L_1)
		{
			goto IL_0027;
		}
	}
	{
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_2 = __this->get_lookTarget_17();
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_3 = __this->get_lookTargetTracking_18();
		IL2CPP_RUNTIME_CLASS_INIT(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		bool L_4;
		L_4 = Object_op_Inequality_mE1F187520BD83FB7D86A6D850710C4D42B864E90(L_2, L_3, /*hidden argument*/NULL);
		if (!L_4)
		{
			goto IL_0027;
		}
	}
	{
		__this->set_lookTargetTracking_18((GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 *)NULL);
	}

IL_0027:
	{
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_5 = __this->get_lookTarget_17();
		IL2CPP_RUNTIME_CLASS_INIT(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		bool L_6;
		L_6 = Object_op_Implicit_mC8214E4F028CC2F036CC82BDB81D102A02893499(L_5, /*hidden argument*/NULL);
		if (!L_6)
		{
			goto IL_005b;
		}
	}
	{
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_7 = __this->get_lookTarget_17();
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_8 = __this->get_lookTargetTracking_18();
		IL2CPP_RUNTIME_CLASS_INIT(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		bool L_9;
		L_9 = Object_op_Inequality_mE1F187520BD83FB7D86A6D850710C4D42B864E90(L_7, L_8, /*hidden argument*/NULL);
		if (!L_9)
		{
			goto IL_005b;
		}
	}
	{
		bool L_10 = __this->get_targetAffinity_19();
		if (L_10)
		{
			goto IL_005b;
		}
	}
	{
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_11 = __this->get_lookTarget_17();
		__this->set_lookTargetTracking_18(L_11);
	}

IL_005b:
	{
		bool L_12 = __this->get_randomBlink_24();
		if (!L_12)
		{
			goto IL_0098;
		}
	}
	{
		float L_13;
		L_13 = Time_get_time_m1A186074B1FCD448AB13A4B9D715AB9ED0B40844(/*hidden argument*/NULL);
		float L_14 = __this->get_blinkTimer_36();
		if ((!(((float)L_13) > ((float)L_14))))
		{
			goto IL_0098;
		}
	}
	{
		float L_15;
		L_15 = Time_get_time_m1A186074B1FCD448AB13A4B9D715AB9ED0B40844(/*hidden argument*/NULL);
		float L_16 = __this->get_maxBlinkInterval_25();
		float L_17;
		L_17 = Random_Range_mC15372D42A9ABDCAC3DE82E114D60A40C9C311D2((0.25f), L_16, /*hidden argument*/NULL);
		__this->set_blinkTimer_36(((float)il2cpp_codegen_add((float)L_15, (float)L_17)));
		float L_18 = __this->get_blinkDuration_26();
		RandomEyes2D_Blink_m2105B90CDDB68CD9D6F42752C3B7BAB853123B43(__this, L_18, /*hidden argument*/NULL);
	}

IL_0098:
	{
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_19 = __this->get_lookTargetTracking_18();
		IL2CPP_RUNTIME_CLASS_INIT(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		bool L_20;
		L_20 = Object_op_Implicit_mC8214E4F028CC2F036CC82BDB81D102A02893499(L_19, /*hidden argument*/NULL);
		if (!L_20)
		{
			goto IL_00ad;
		}
	}
	{
		bool L_21 = __this->get_targetAffinity_19();
		if (!L_21)
		{
			goto IL_00ca;
		}
	}

IL_00ad:
	{
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_22 = __this->get_lookTargetTracking_18();
		IL2CPP_RUNTIME_CLASS_INIT(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		bool L_23;
		L_23 = Object_op_Implicit_mC8214E4F028CC2F036CC82BDB81D102A02893499(L_22, /*hidden argument*/NULL);
		if (!L_23)
		{
			goto IL_00e1;
		}
	}
	{
		bool L_24 = __this->get_targetAffinity_19();
		if (!L_24)
		{
			goto IL_00e1;
		}
	}
	{
		bool L_25 = __this->get_hasAffinity_34();
		if (!L_25)
		{
			goto IL_00e1;
		}
	}

IL_00ca:
	{
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_26 = __this->get_lookTarget_17();
		NullCheck(L_26);
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_27;
		L_27 = GameObject_get_transform_m16A80BB92B6C8C5AB696E447014D45EDF1E4DE34(L_26, /*hidden argument*/NULL);
		NullCheck(L_27);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_28;
		L_28 = Transform_get_position_m40A8A9895568D56FFC687B57F30E8D53CB5EA341(L_27, /*hidden argument*/NULL);
		RandomEyes2D_LookTracking_m4C39BFE72EEA5E5F4E707B3BD925D259FCF6B77C(__this, L_28, /*hidden argument*/NULL);
		return;
	}

IL_00e1:
	{
		bool L_29 = __this->get_randomEyes_21();
		if (L_29)
		{
			goto IL_0103;
		}
	}
	{
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_30;
		memset((&L_30), 0, sizeof(L_30));
		Vector3__ctor_m57495F692C6CE1CEF278CAD9A98221165D37E636_inline((&L_30), (0.0f), (0.0f), (100.0f), /*hidden argument*/NULL);
		RandomEyes2D_LookTracking_m4C39BFE72EEA5E5F4E707B3BD925D259FCF6B77C(__this, L_30, /*hidden argument*/NULL);
	}

IL_0103:
	{
		bool L_31 = __this->get_randomEyes_21();
		if (!L_31)
		{
			goto IL_017e;
		}
	}
	{
		float L_32;
		L_32 = Time_get_time_m1A186074B1FCD448AB13A4B9D715AB9ED0B40844(/*hidden argument*/NULL);
		float L_33 = __this->get_randomTimer_35();
		if ((!(((float)L_32) > ((float)L_33))))
		{
			goto IL_0172;
		}
	}
	{
		float L_34;
		L_34 = Time_get_time_m1A186074B1FCD448AB13A4B9D715AB9ED0B40844(/*hidden argument*/NULL);
		float L_35 = __this->get_maxRandomInterval_22();
		float L_36;
		L_36 = Random_Range_mC15372D42A9ABDCAC3DE82E114D60A40C9C311D2((0.25f), L_35, /*hidden argument*/NULL);
		__this->set_randomTimer_35(((float)il2cpp_codegen_add((float)L_34, (float)L_36)));
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_37;
		memset((&L_37), 0, sizeof(L_37));
		Vector3__ctor_m57495F692C6CE1CEF278CAD9A98221165D37E636_inline((&L_37), (0.0f), (0.0f), (0.0f), /*hidden argument*/NULL);
		__this->set_lookRandPosition_14(L_37);
		int32_t L_38;
		L_38 = Random_Range_m4B3A0037ACA057F33C94508F908546B9317D996A(((int32_t)-100), ((int32_t)100), /*hidden argument*/NULL);
		int32_t L_39;
		L_39 = Random_Range_m4B3A0037ACA057F33C94508F908546B9317D996A(((int32_t)-100), ((int32_t)100), /*hidden argument*/NULL);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_40;
		memset((&L_40), 0, sizeof(L_40));
		Vector3__ctor_m57495F692C6CE1CEF278CAD9A98221165D37E636_inline((&L_40), ((float)((float)L_38)), ((float)((float)L_39)), (0.0f), /*hidden argument*/NULL);
		__this->set_lookRandPosition_14(L_40);
	}

IL_0172:
	{
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_41 = __this->get_lookRandPosition_14();
		RandomEyes2D_LookTracking_m4C39BFE72EEA5E5F4E707B3BD925D259FCF6B77C(__this, L_41, /*hidden argument*/NULL);
	}

IL_017e:
	{
		return;
	}
}
// System.Void CrazyMinnow.SALSA.RandomEyes2D::LookTracking(UnityEngine.Vector3)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void RandomEyes2D_LookTracking_m4C39BFE72EEA5E5F4E707B3BD925D259FCF6B77C (RandomEyes2D_t253864BE689DE7E477335D17ECD2EC77001B377F * __this, Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___lookDir0, const RuntimeMethod* method)
{
	int32_t V_0 = 0;
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  V_1;
	memset((&V_1), 0, sizeof(V_1));
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  V_2;
	memset((&V_2), 0, sizeof(V_2));
	{
		V_0 = 0;
		goto IL_0197;
	}

IL_0007:
	{
		int32_t L_0 = V_0;
		int32_t L_1 = __this->get_largestEyeIndex_37();
		if ((((int32_t)L_0) == ((int32_t)L_1)))
		{
			goto IL_00e6;
		}
	}
	{
		bool L_2 = __this->get_moveProportional_16();
		if (!L_2)
		{
			goto IL_00e6;
		}
	}
	{
		SpriteRendererU5BU5D_t45BF125944C2F3974C568E650FCCDB591E5ABFBF* L_3 = __this->get_eyes_8();
		int32_t L_4 = __this->get_largestEyeIndex_37();
		NullCheck(L_3);
		int32_t L_5 = L_4;
		SpriteRenderer_t3F35AD5498243C170B46F5FFDB582AAEF78615EF * L_6 = (L_3)->GetAt(static_cast<il2cpp_array_size_t>(L_5));
		NullCheck(L_6);
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_7;
		L_7 = Component_get_transform_mE8496EBC45BEB1BADB5F314960F1DF1C952FA11F(L_6, /*hidden argument*/NULL);
		NullCheck(L_7);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_8;
		L_8 = Transform_get_localScale_mD9DF6CA81108C2A6002B5EA2BE25A6CD2723D046(L_7, /*hidden argument*/NULL);
		V_1 = L_8;
		float L_9;
		L_9 = Vector3_get_magnitude_mDDD40612220D8104E77E993E18A101A69A944991((Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E *)(&V_1), /*hidden argument*/NULL);
		SpriteRendererU5BU5D_t45BF125944C2F3974C568E650FCCDB591E5ABFBF* L_10 = __this->get_eyes_8();
		int32_t L_11 = V_0;
		NullCheck(L_10);
		int32_t L_12 = L_11;
		SpriteRenderer_t3F35AD5498243C170B46F5FFDB582AAEF78615EF * L_13 = (L_10)->GetAt(static_cast<il2cpp_array_size_t>(L_12));
		NullCheck(L_13);
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_14;
		L_14 = Component_get_transform_mE8496EBC45BEB1BADB5F314960F1DF1C952FA11F(L_13, /*hidden argument*/NULL);
		NullCheck(L_14);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_15;
		L_15 = Transform_get_localScale_mD9DF6CA81108C2A6002B5EA2BE25A6CD2723D046(L_14, /*hidden argument*/NULL);
		V_2 = L_15;
		float L_16;
		L_16 = Vector3_get_magnitude_mDDD40612220D8104E77E993E18A101A69A944991((Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E *)(&V_2), /*hidden argument*/NULL);
		if ((!(((float)L_9) > ((float)L_16))))
		{
			goto IL_00e6;
		}
	}
	{
		Vector3U5BU5D_t5FB88EAA33E46838BDC2ABDAEA3E8727491CB9E4* L_17 = __this->get_trackDirection_30();
		int32_t L_18 = V_0;
		NullCheck(L_17);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_19 = ___lookDir0;
		GameObjectU5BU5D_tA88FC1A1FC9D4D73D0B3984D4B0ECE88F4C47642* L_20 = __this->get_randomEyes2DGizmos_31();
		int32_t L_21 = V_0;
		NullCheck(L_20);
		int32_t L_22 = L_21;
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_23 = (L_20)->GetAt(static_cast<il2cpp_array_size_t>(L_22));
		NullCheck(L_23);
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_24;
		L_24 = GameObject_get_transform_m16A80BB92B6C8C5AB696E447014D45EDF1E4DE34(L_23, /*hidden argument*/NULL);
		NullCheck(L_24);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_25;
		L_25 = Transform_get_position_m40A8A9895568D56FFC687B57F30E8D53CB5EA341(L_24, /*hidden argument*/NULL);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_26;
		L_26 = Vector3_op_Subtraction_m2725C96965D5C0B1F9715797E51762B13A5FED58_inline(L_19, L_25, /*hidden argument*/NULL);
		*(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E *)((L_17)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_18))) = L_26;
		Vector3U5BU5D_t5FB88EAA33E46838BDC2ABDAEA3E8727491CB9E4* L_27 = __this->get_trackDirection_30();
		int32_t L_28 = V_0;
		NullCheck(L_27);
		Vector3U5BU5D_t5FB88EAA33E46838BDC2ABDAEA3E8727491CB9E4* L_29 = __this->get_trackDirection_30();
		int32_t L_30 = V_0;
		NullCheck(L_29);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_31 = (*(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E *)((L_29)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_30))));
		SpriteRendererU5BU5D_t45BF125944C2F3974C568E650FCCDB591E5ABFBF* L_32 = __this->get_eyes_8();
		int32_t L_33 = V_0;
		NullCheck(L_32);
		int32_t L_34 = L_33;
		SpriteRenderer_t3F35AD5498243C170B46F5FFDB582AAEF78615EF * L_35 = (L_32)->GetAt(static_cast<il2cpp_array_size_t>(L_34));
		NullCheck(L_35);
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_36;
		L_36 = Component_get_transform_mE8496EBC45BEB1BADB5F314960F1DF1C952FA11F(L_35, /*hidden argument*/NULL);
		NullCheck(L_36);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_37;
		L_37 = Transform_get_localScale_mD9DF6CA81108C2A6002B5EA2BE25A6CD2723D046(L_36, /*hidden argument*/NULL);
		float L_38 = L_37.get_x_2();
		SpriteRenderer_t3F35AD5498243C170B46F5FFDB582AAEF78615EF * L_39 = __this->get_largestEye_38();
		NullCheck(L_39);
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_40;
		L_40 = Component_get_transform_mE8496EBC45BEB1BADB5F314960F1DF1C952FA11F(L_39, /*hidden argument*/NULL);
		NullCheck(L_40);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_41;
		L_41 = Transform_get_localScale_mD9DF6CA81108C2A6002B5EA2BE25A6CD2723D046(L_40, /*hidden argument*/NULL);
		float L_42 = L_41.get_x_2();
		float L_43 = __this->get_rangeOfMotion_15();
		float L_44;
		L_44 = RandomEyes2D_ProportionalMovement_m4EC2F23BA31627C8C60313AA4695FA4B11A1651B(__this, L_38, L_42, L_43, /*hidden argument*/NULL);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_45;
		L_45 = Vector3_ClampMagnitude_mF85598307D6CF3B4E5BEEB218CEDDCE39CDF3336(L_31, L_44, /*hidden argument*/NULL);
		*(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E *)((L_27)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_28))) = L_45;
		goto IL_013c;
	}

IL_00e6:
	{
		Vector3U5BU5D_t5FB88EAA33E46838BDC2ABDAEA3E8727491CB9E4* L_46 = __this->get_trackDirection_30();
		int32_t L_47 = V_0;
		NullCheck(L_46);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_48 = ___lookDir0;
		GameObjectU5BU5D_tA88FC1A1FC9D4D73D0B3984D4B0ECE88F4C47642* L_49 = __this->get_randomEyes2DGizmos_31();
		int32_t L_50 = V_0;
		NullCheck(L_49);
		int32_t L_51 = L_50;
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_52 = (L_49)->GetAt(static_cast<il2cpp_array_size_t>(L_51));
		NullCheck(L_52);
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_53;
		L_53 = GameObject_get_transform_m16A80BB92B6C8C5AB696E447014D45EDF1E4DE34(L_52, /*hidden argument*/NULL);
		NullCheck(L_53);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_54;
		L_54 = Transform_get_position_m40A8A9895568D56FFC687B57F30E8D53CB5EA341(L_53, /*hidden argument*/NULL);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_55;
		L_55 = Vector3_op_Subtraction_m2725C96965D5C0B1F9715797E51762B13A5FED58_inline(L_48, L_54, /*hidden argument*/NULL);
		*(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E *)((L_46)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_47))) = L_55;
		Vector3U5BU5D_t5FB88EAA33E46838BDC2ABDAEA3E8727491CB9E4* L_56 = __this->get_trackDirection_30();
		int32_t L_57 = V_0;
		NullCheck(L_56);
		Vector3U5BU5D_t5FB88EAA33E46838BDC2ABDAEA3E8727491CB9E4* L_58 = __this->get_trackDirection_30();
		int32_t L_59 = V_0;
		NullCheck(L_58);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_60 = (*(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E *)((L_58)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_59))));
		float L_61 = __this->get_rangeOfMotion_15();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_62;
		L_62 = Vector3_ClampMagnitude_mF85598307D6CF3B4E5BEEB218CEDDCE39CDF3336(L_60, L_61, /*hidden argument*/NULL);
		*(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E *)((L_56)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_57))) = L_62;
	}

IL_013c:
	{
		SpriteRendererU5BU5D_t45BF125944C2F3974C568E650FCCDB591E5ABFBF* L_63 = __this->get_eyes_8();
		int32_t L_64 = V_0;
		NullCheck(L_63);
		int32_t L_65 = L_64;
		SpriteRenderer_t3F35AD5498243C170B46F5FFDB582AAEF78615EF * L_66 = (L_63)->GetAt(static_cast<il2cpp_array_size_t>(L_65));
		NullCheck(L_66);
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_67;
		L_67 = Component_get_transform_mE8496EBC45BEB1BADB5F314960F1DF1C952FA11F(L_66, /*hidden argument*/NULL);
		SpriteRendererU5BU5D_t45BF125944C2F3974C568E650FCCDB591E5ABFBF* L_68 = __this->get_eyes_8();
		int32_t L_69 = V_0;
		NullCheck(L_68);
		int32_t L_70 = L_69;
		SpriteRenderer_t3F35AD5498243C170B46F5FFDB582AAEF78615EF * L_71 = (L_68)->GetAt(static_cast<il2cpp_array_size_t>(L_70));
		NullCheck(L_71);
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_72;
		L_72 = Component_get_transform_mE8496EBC45BEB1BADB5F314960F1DF1C952FA11F(L_71, /*hidden argument*/NULL);
		NullCheck(L_72);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_73;
		L_73 = Transform_get_position_m40A8A9895568D56FFC687B57F30E8D53CB5EA341(L_72, /*hidden argument*/NULL);
		GameObjectU5BU5D_tA88FC1A1FC9D4D73D0B3984D4B0ECE88F4C47642* L_74 = __this->get_randomEyes2DGizmos_31();
		int32_t L_75 = V_0;
		NullCheck(L_74);
		int32_t L_76 = L_75;
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_77 = (L_74)->GetAt(static_cast<il2cpp_array_size_t>(L_76));
		NullCheck(L_77);
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_78;
		L_78 = GameObject_get_transform_m16A80BB92B6C8C5AB696E447014D45EDF1E4DE34(L_77, /*hidden argument*/NULL);
		NullCheck(L_78);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_79;
		L_79 = Transform_get_position_m40A8A9895568D56FFC687B57F30E8D53CB5EA341(L_78, /*hidden argument*/NULL);
		Vector3U5BU5D_t5FB88EAA33E46838BDC2ABDAEA3E8727491CB9E4* L_80 = __this->get_trackDirection_30();
		int32_t L_81 = V_0;
		NullCheck(L_80);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_82 = (*(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E *)((L_80)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_81))));
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_83;
		L_83 = Vector3_op_Addition_mEE4F672B923CCB184C39AABCA33443DB218E50E0_inline(L_79, L_82, /*hidden argument*/NULL);
		float L_84 = __this->get_blendSpeed_23();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_85;
		L_85 = SalsaUtility_Lerp2DEyeTracking_m49C8BAED6D6F4ABD41691661601291A0F20A800D(L_73, L_83, L_84, /*hidden argument*/NULL);
		NullCheck(L_67);
		Transform_set_position_mB169E52D57EEAC1E3F22C5395968714E4F00AC91(L_67, L_85, /*hidden argument*/NULL);
		int32_t L_86 = V_0;
		V_0 = ((int32_t)il2cpp_codegen_add((int32_t)L_86, (int32_t)1));
	}

IL_0197:
	{
		int32_t L_87 = V_0;
		SpriteRendererU5BU5D_t45BF125944C2F3974C568E650FCCDB591E5ABFBF* L_88 = __this->get_eyes_8();
		NullCheck(L_88);
		if ((((int32_t)L_87) < ((int32_t)((int32_t)((int32_t)(((RuntimeArray*)L_88)->max_length))))))
		{
			goto IL_0007;
		}
	}
	{
		return;
	}
}
// System.Single CrazyMinnow.SALSA.RandomEyes2D::ProportionalMovement(System.Single,System.Single,System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float RandomEyes2D_ProportionalMovement_m4EC2F23BA31627C8C60313AA4695FA4B11A1651B (RandomEyes2D_t253864BE689DE7E477335D17ECD2EC77001B377F * __this, float ___currentEyeScale0, float ___largestEyeScale1, float ___amount2, const RuntimeMethod* method)
{
	{
		float L_0 = ___currentEyeScale0;
		float L_1 = ___largestEyeScale1;
		float L_2 = ___amount2;
		return ((float)il2cpp_codegen_multiply((float)((float)((float)((float)il2cpp_codegen_multiply((float)((float)((float)L_0/(float)L_1)), (float)(100.0f)))/(float)(100.0f))), (float)L_2));
	}
}
// System.Collections.IEnumerator CrazyMinnow.SALSA.RandomEyes2D::TargetAffinityUpdate(System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* RandomEyes2D_TargetAffinityUpdate_m105F6F7B0C6788A396093FE3D0431CD3331FCCB0 (RandomEyes2D_t253864BE689DE7E477335D17ECD2EC77001B377F * __this, float ___updateDelay0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CTargetAffinityUpdateU3Ed__0_tB9D47FFD16FA8A3CF1860EC8ADB05A787654DF78_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	U3CTargetAffinityUpdateU3Ed__0_tB9D47FFD16FA8A3CF1860EC8ADB05A787654DF78 * V_0 = NULL;
	{
		U3CTargetAffinityUpdateU3Ed__0_tB9D47FFD16FA8A3CF1860EC8ADB05A787654DF78 * L_0 = (U3CTargetAffinityUpdateU3Ed__0_tB9D47FFD16FA8A3CF1860EC8ADB05A787654DF78 *)il2cpp_codegen_object_new(U3CTargetAffinityUpdateU3Ed__0_tB9D47FFD16FA8A3CF1860EC8ADB05A787654DF78_il2cpp_TypeInfo_var);
		U3CTargetAffinityUpdateU3Ed__0__ctor_m11474126BC59372CD200224F8F86C2D80D12BD0C(L_0, 0, /*hidden argument*/NULL);
		V_0 = L_0;
		U3CTargetAffinityUpdateU3Ed__0_tB9D47FFD16FA8A3CF1860EC8ADB05A787654DF78 * L_1 = V_0;
		NullCheck(L_1);
		L_1->set_U3CU3E4__this_2(__this);
		U3CTargetAffinityUpdateU3Ed__0_tB9D47FFD16FA8A3CF1860EC8ADB05A787654DF78 * L_2 = V_0;
		return L_2;
	}
}
// System.Void CrazyMinnow.SALSA.RandomEyes2D::Blink(System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void RandomEyes2D_Blink_m2105B90CDDB68CD9D6F42752C3B7BAB853123B43 (RandomEyes2D_t253864BE689DE7E477335D17ECD2EC77001B377F * __this, float ___duration0, const RuntimeMethod* method)
{
	{
		float L_0 = ___duration0;
		RuntimeObject* L_1;
		L_1 = RandomEyes2D_BlinkEyes_mA9926DC8191C467D2188EC8D8F3AFFBB7AF92C24(__this, L_0, /*hidden argument*/NULL);
		Coroutine_t899D5232EF542CB8BA70AF9ECEECA494FAA9CCB7 * L_2;
		L_2 = MonoBehaviour_StartCoroutine_m3E33706D38B23CDD179E99BAD61E32303E9CC719(__this, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Collections.IEnumerator CrazyMinnow.SALSA.RandomEyes2D::BlinkEyes(System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* RandomEyes2D_BlinkEyes_mA9926DC8191C467D2188EC8D8F3AFFBB7AF92C24 (RandomEyes2D_t253864BE689DE7E477335D17ECD2EC77001B377F * __this, float ___duration0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CBlinkEyesU3Ed__2_t7DFD119FC328CD8363C654182C85B8B62BA190D1_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	U3CBlinkEyesU3Ed__2_t7DFD119FC328CD8363C654182C85B8B62BA190D1 * V_0 = NULL;
	{
		U3CBlinkEyesU3Ed__2_t7DFD119FC328CD8363C654182C85B8B62BA190D1 * L_0 = (U3CBlinkEyesU3Ed__2_t7DFD119FC328CD8363C654182C85B8B62BA190D1 *)il2cpp_codegen_object_new(U3CBlinkEyesU3Ed__2_t7DFD119FC328CD8363C654182C85B8B62BA190D1_il2cpp_TypeInfo_var);
		U3CBlinkEyesU3Ed__2__ctor_m794D2B438E817C03906B33BA6CEA16630030D920(L_0, 0, /*hidden argument*/NULL);
		V_0 = L_0;
		U3CBlinkEyesU3Ed__2_t7DFD119FC328CD8363C654182C85B8B62BA190D1 * L_1 = V_0;
		NullCheck(L_1);
		L_1->set_U3CU3E4__this_2(__this);
		U3CBlinkEyesU3Ed__2_t7DFD119FC328CD8363C654182C85B8B62BA190D1 * L_2 = V_0;
		float L_3 = ___duration0;
		NullCheck(L_2);
		L_2->set_duration_3(L_3);
		U3CBlinkEyesU3Ed__2_t7DFD119FC328CD8363C654182C85B8B62BA190D1 * L_4 = V_0;
		return L_4;
	}
}
// System.Void CrazyMinnow.SALSA.RandomEyes2D::SetRangeOfMotion(System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void RandomEyes2D_SetRangeOfMotion_mB76F28D1AF16BF195B3E33D9C11C8DB09D4A6E95 (RandomEyes2D_t253864BE689DE7E477335D17ECD2EC77001B377F * __this, float ___range0, const RuntimeMethod* method)
{
	{
		float L_0 = ___range0;
		float L_1;
		L_1 = SalsaUtility_ClampValue_m167E1BE774D495060E1FAA2503DD01D2C00EE208(L_0, (50.0f), /*hidden argument*/NULL);
		__this->set_rangeOfMotion_15(L_1);
		return;
	}
}
// System.Void CrazyMinnow.SALSA.RandomEyes2D::SetBlendSpeed(System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void RandomEyes2D_SetBlendSpeed_m1EB9A46A16069C006ADA386330F1FD54602CF1ED (RandomEyes2D_t253864BE689DE7E477335D17ECD2EC77001B377F * __this, float ___speed0, const RuntimeMethod* method)
{
	{
		float L_0 = ___speed0;
		float L_1;
		L_1 = SalsaUtility_ClampValue_m167E1BE774D495060E1FAA2503DD01D2C00EE208(L_0, (100.0f), /*hidden argument*/NULL);
		__this->set_blendSpeed_23(L_1);
		return;
	}
}
// System.Void CrazyMinnow.SALSA.RandomEyes2D::SetOrderInLayer(CrazyMinnow.SALSA.RandomEyes2D/SpriteRend,System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void RandomEyes2D_SetOrderInLayer_m09E8144307532A7FA8F0946D35CFBA7350D0D285 (RandomEyes2D_t253864BE689DE7E477335D17ECD2EC77001B377F * __this, int32_t ___item0, int32_t ___order1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Component_GetComponent_TisSpriteRenderer_t3F35AD5498243C170B46F5FFDB582AAEF78615EF_m7224DFA0D29BF2205FEA6E432D9BCB4133F6E491_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	SpriteRenderer_t3F35AD5498243C170B46F5FFDB582AAEF78615EF * V_0 = NULL;
	int32_t V_1 = 0;
	int32_t V_2 = 0;
	int32_t V_3 = 0;
	{
		int32_t L_0 = ___item0;
		V_3 = L_0;
		int32_t L_1 = V_3;
		switch (L_1)
		{
			case 0:
			{
				goto IL_0015;
			}
			case 1:
			{
				goto IL_0039;
			}
			case 2:
			{
				goto IL_008f;
			}
		}
	}
	{
		return;
	}

IL_0015:
	{
		int32_t L_2 = ___order1;
		__this->set_characterLayer_6(L_2);
		SpriteRenderer_t3F35AD5498243C170B46F5FFDB582AAEF78615EF * L_3 = __this->get_character_5();
		IL2CPP_RUNTIME_CLASS_INIT(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		bool L_4;
		L_4 = Object_op_Implicit_mC8214E4F028CC2F036CC82BDB81D102A02893499(L_3, /*hidden argument*/NULL);
		if (!L_4)
		{
			goto IL_00d6;
		}
	}
	{
		SpriteRenderer_t3F35AD5498243C170B46F5FFDB582AAEF78615EF * L_5 = __this->get_character_5();
		int32_t L_6 = ___order1;
		NullCheck(L_5);
		Renderer_set_sortingOrder_mAABE4F8F9B158068C8A1582ACE0BFEA3CF499139(L_5, L_6, /*hidden argument*/NULL);
		return;
	}

IL_0039:
	{
		SpriteRendererU5BU5D_t45BF125944C2F3974C568E650FCCDB591E5ABFBF* L_7 = __this->get_eyes_8();
		NullCheck(L_7);
		if ((((int32_t)((int32_t)((int32_t)(((RuntimeArray*)L_7)->max_length)))) <= ((int32_t)0)))
		{
			goto IL_00d6;
		}
	}
	{
		V_1 = 0;
		goto IL_0083;
	}

IL_004b:
	{
		int32_t L_8 = ___order1;
		__this->set_eyeLayer_9(L_8);
		SpriteRendererU5BU5D_t45BF125944C2F3974C568E650FCCDB591E5ABFBF* L_9 = __this->get_eyes_8();
		int32_t L_10 = V_1;
		NullCheck(L_9);
		int32_t L_11 = L_10;
		SpriteRenderer_t3F35AD5498243C170B46F5FFDB582AAEF78615EF * L_12 = (L_9)->GetAt(static_cast<il2cpp_array_size_t>(L_11));
		IL2CPP_RUNTIME_CLASS_INIT(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		bool L_13;
		L_13 = Object_op_Inequality_mE1F187520BD83FB7D86A6D850710C4D42B864E90(L_12, (Object_tF2F3778131EFF286AF62B7B013A170F95A91571A *)NULL, /*hidden argument*/NULL);
		if (!L_13)
		{
			goto IL_007f;
		}
	}
	{
		SpriteRendererU5BU5D_t45BF125944C2F3974C568E650FCCDB591E5ABFBF* L_14 = __this->get_eyes_8();
		int32_t L_15 = V_1;
		NullCheck(L_14);
		int32_t L_16 = L_15;
		SpriteRenderer_t3F35AD5498243C170B46F5FFDB582AAEF78615EF * L_17 = (L_14)->GetAt(static_cast<il2cpp_array_size_t>(L_16));
		NullCheck(L_17);
		SpriteRenderer_t3F35AD5498243C170B46F5FFDB582AAEF78615EF * L_18;
		L_18 = Component_GetComponent_TisSpriteRenderer_t3F35AD5498243C170B46F5FFDB582AAEF78615EF_m7224DFA0D29BF2205FEA6E432D9BCB4133F6E491(L_17, /*hidden argument*/Component_GetComponent_TisSpriteRenderer_t3F35AD5498243C170B46F5FFDB582AAEF78615EF_m7224DFA0D29BF2205FEA6E432D9BCB4133F6E491_RuntimeMethod_var);
		V_0 = L_18;
		SpriteRenderer_t3F35AD5498243C170B46F5FFDB582AAEF78615EF * L_19 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		bool L_20;
		L_20 = Object_op_Implicit_mC8214E4F028CC2F036CC82BDB81D102A02893499(L_19, /*hidden argument*/NULL);
		if (!L_20)
		{
			goto IL_007f;
		}
	}
	{
		SpriteRenderer_t3F35AD5498243C170B46F5FFDB582AAEF78615EF * L_21 = V_0;
		int32_t L_22 = ___order1;
		NullCheck(L_21);
		Renderer_set_sortingOrder_mAABE4F8F9B158068C8A1582ACE0BFEA3CF499139(L_21, L_22, /*hidden argument*/NULL);
	}

IL_007f:
	{
		int32_t L_23 = V_1;
		V_1 = ((int32_t)il2cpp_codegen_add((int32_t)L_23, (int32_t)1));
	}

IL_0083:
	{
		int32_t L_24 = V_1;
		SpriteRendererU5BU5D_t45BF125944C2F3974C568E650FCCDB591E5ABFBF* L_25 = __this->get_eyes_8();
		NullCheck(L_25);
		if ((((int32_t)L_24) < ((int32_t)((int32_t)((int32_t)(((RuntimeArray*)L_25)->max_length))))))
		{
			goto IL_004b;
		}
	}
	{
		return;
	}

IL_008f:
	{
		V_2 = 0;
		goto IL_00cb;
	}

IL_0093:
	{
		int32_t L_26 = ___order1;
		__this->set_eyeLidLayer_12(L_26);
		SpriteRendererU5BU5D_t45BF125944C2F3974C568E650FCCDB591E5ABFBF* L_27 = __this->get_eyeLids_11();
		int32_t L_28 = V_2;
		NullCheck(L_27);
		int32_t L_29 = L_28;
		SpriteRenderer_t3F35AD5498243C170B46F5FFDB582AAEF78615EF * L_30 = (L_27)->GetAt(static_cast<il2cpp_array_size_t>(L_29));
		IL2CPP_RUNTIME_CLASS_INIT(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		bool L_31;
		L_31 = Object_op_Inequality_mE1F187520BD83FB7D86A6D850710C4D42B864E90(L_30, (Object_tF2F3778131EFF286AF62B7B013A170F95A91571A *)NULL, /*hidden argument*/NULL);
		if (!L_31)
		{
			goto IL_00c7;
		}
	}
	{
		SpriteRendererU5BU5D_t45BF125944C2F3974C568E650FCCDB591E5ABFBF* L_32 = __this->get_eyeLids_11();
		int32_t L_33 = V_2;
		NullCheck(L_32);
		int32_t L_34 = L_33;
		SpriteRenderer_t3F35AD5498243C170B46F5FFDB582AAEF78615EF * L_35 = (L_32)->GetAt(static_cast<il2cpp_array_size_t>(L_34));
		NullCheck(L_35);
		SpriteRenderer_t3F35AD5498243C170B46F5FFDB582AAEF78615EF * L_36;
		L_36 = Component_GetComponent_TisSpriteRenderer_t3F35AD5498243C170B46F5FFDB582AAEF78615EF_m7224DFA0D29BF2205FEA6E432D9BCB4133F6E491(L_35, /*hidden argument*/Component_GetComponent_TisSpriteRenderer_t3F35AD5498243C170B46F5FFDB582AAEF78615EF_m7224DFA0D29BF2205FEA6E432D9BCB4133F6E491_RuntimeMethod_var);
		V_0 = L_36;
		SpriteRenderer_t3F35AD5498243C170B46F5FFDB582AAEF78615EF * L_37 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		bool L_38;
		L_38 = Object_op_Implicit_mC8214E4F028CC2F036CC82BDB81D102A02893499(L_37, /*hidden argument*/NULL);
		if (!L_38)
		{
			goto IL_00c7;
		}
	}
	{
		SpriteRenderer_t3F35AD5498243C170B46F5FFDB582AAEF78615EF * L_39 = V_0;
		int32_t L_40 = ___order1;
		NullCheck(L_39);
		Renderer_set_sortingOrder_mAABE4F8F9B158068C8A1582ACE0BFEA3CF499139(L_39, L_40, /*hidden argument*/NULL);
	}

IL_00c7:
	{
		int32_t L_41 = V_2;
		V_2 = ((int32_t)il2cpp_codegen_add((int32_t)L_41, (int32_t)1));
	}

IL_00cb:
	{
		int32_t L_42 = V_2;
		SpriteRendererU5BU5D_t45BF125944C2F3974C568E650FCCDB591E5ABFBF* L_43 = __this->get_eyeLids_11();
		NullCheck(L_43);
		if ((((int32_t)L_42) < ((int32_t)((int32_t)((int32_t)(((RuntimeArray*)L_43)->max_length))))))
		{
			goto IL_0093;
		}
	}

IL_00d6:
	{
		return;
	}
}
// System.Void CrazyMinnow.SALSA.RandomEyes2D::SetLookTargetTracking(UnityEngine.GameObject)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void RandomEyes2D_SetLookTargetTracking_m3B736925D0B69176837D0D89485EB6748354ECE9 (RandomEyes2D_t253864BE689DE7E477335D17ECD2EC77001B377F * __this, GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * ___obj0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_0 = ___obj0;
		IL2CPP_RUNTIME_CLASS_INIT(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		bool L_1;
		L_1 = Object_op_Implicit_mC8214E4F028CC2F036CC82BDB81D102A02893499(L_0, /*hidden argument*/NULL);
		if (L_1)
		{
			goto IL_001d;
		}
	}
	{
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_2 = ___obj0;
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_3 = __this->get_lookTargetTracking_18();
		IL2CPP_RUNTIME_CLASS_INIT(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		bool L_4;
		L_4 = Object_op_Inequality_mE1F187520BD83FB7D86A6D850710C4D42B864E90(L_2, L_3, /*hidden argument*/NULL);
		if (!L_4)
		{
			goto IL_001d;
		}
	}
	{
		__this->set_lookTargetTracking_18((GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 *)NULL);
	}

IL_001d:
	{
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_5 = ___obj0;
		IL2CPP_RUNTIME_CLASS_INIT(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		bool L_6;
		L_6 = Object_op_Implicit_mC8214E4F028CC2F036CC82BDB81D102A02893499(L_5, /*hidden argument*/NULL);
		if (!L_6)
		{
			goto IL_0047;
		}
	}
	{
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_7 = ___obj0;
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_8 = __this->get_lookTargetTracking_18();
		IL2CPP_RUNTIME_CLASS_INIT(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		bool L_9;
		L_9 = Object_op_Inequality_mE1F187520BD83FB7D86A6D850710C4D42B864E90(L_7, L_8, /*hidden argument*/NULL);
		if (!L_9)
		{
			goto IL_0047;
		}
	}
	{
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_10 = __this->get_lookTarget_17();
		IL2CPP_RUNTIME_CLASS_INIT(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		bool L_11;
		L_11 = Object_op_Implicit_mC8214E4F028CC2F036CC82BDB81D102A02893499(L_10, /*hidden argument*/NULL);
		if (!L_11)
		{
			goto IL_0047;
		}
	}
	{
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_12 = ___obj0;
		__this->set_lookTargetTracking_18(L_12);
	}

IL_0047:
	{
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_13 = __this->get_lookTarget_17();
		IL2CPP_RUNTIME_CLASS_INIT(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		bool L_14;
		L_14 = Object_op_Implicit_mC8214E4F028CC2F036CC82BDB81D102A02893499(L_13, /*hidden argument*/NULL);
		if (L_14)
		{
			goto IL_0068;
		}
	}
	{
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_15 = __this->get_lookTargetTracking_18();
		IL2CPP_RUNTIME_CLASS_INIT(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		bool L_16;
		L_16 = Object_op_Implicit_mC8214E4F028CC2F036CC82BDB81D102A02893499(L_15, /*hidden argument*/NULL);
		if (!L_16)
		{
			goto IL_0068;
		}
	}
	{
		__this->set_lookTargetTracking_18((GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 *)NULL);
	}

IL_0068:
	{
		return;
	}
}
// System.Void CrazyMinnow.SALSA.RandomEyes2D::SetLookTarget(UnityEngine.GameObject)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void RandomEyes2D_SetLookTarget_mBFA754D52C5FB53AEED97FA40A287CEC35A2C520 (RandomEyes2D_t253864BE689DE7E477335D17ECD2EC77001B377F * __this, GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * ___obj0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_0 = ___obj0;
		IL2CPP_RUNTIME_CLASS_INIT(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		bool L_1;
		L_1 = Object_op_Implicit_mC8214E4F028CC2F036CC82BDB81D102A02893499(L_0, /*hidden argument*/NULL);
		if (L_1)
		{
			goto IL_001c;
		}
	}
	{
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_2 = __this->get_lookTarget_17();
		IL2CPP_RUNTIME_CLASS_INIT(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		bool L_3;
		L_3 = Object_op_Implicit_mC8214E4F028CC2F036CC82BDB81D102A02893499(L_2, /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_001c;
		}
	}
	{
		__this->set_lookTarget_17((GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 *)NULL);
	}

IL_001c:
	{
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_4 = ___obj0;
		IL2CPP_RUNTIME_CLASS_INIT(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		bool L_5;
		L_5 = Object_op_Implicit_mC8214E4F028CC2F036CC82BDB81D102A02893499(L_4, /*hidden argument*/NULL);
		if (!L_5)
		{
			goto IL_0039;
		}
	}
	{
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_6 = ___obj0;
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_7 = __this->get_lookTarget_17();
		IL2CPP_RUNTIME_CLASS_INIT(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		bool L_8;
		L_8 = Object_op_Inequality_mE1F187520BD83FB7D86A6D850710C4D42B864E90(L_6, L_7, /*hidden argument*/NULL);
		if (!L_8)
		{
			goto IL_0039;
		}
	}
	{
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_9 = ___obj0;
		__this->set_lookTarget_17(L_9);
	}

IL_0039:
	{
		return;
	}
}
// System.Void CrazyMinnow.SALSA.RandomEyes2D::SetTargetAffinity(System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void RandomEyes2D_SetTargetAffinity_m01D20750B2E1E15CA5326459B402A45B00F94F5C (RandomEyes2D_t253864BE689DE7E477335D17ECD2EC77001B377F * __this, bool ___status0, const RuntimeMethod* method)
{
	{
		bool L_0 = ___status0;
		__this->set_targetAffinity_19(L_0);
		return;
	}
}
// System.Void CrazyMinnow.SALSA.RandomEyes2D::SetAffinityPercentage(System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void RandomEyes2D_SetAffinityPercentage_mCE3C8F69ABB166059CDCA2D0D2CFC935A2B5E694 (RandomEyes2D_t253864BE689DE7E477335D17ECD2EC77001B377F * __this, float ___percent0, const RuntimeMethod* method)
{
	{
		float L_0 = ___percent0;
		float L_1;
		L_1 = Mathf_Clamp_m2416F3B785C8F135863E3D17E5B0CB4174797B87(L_0, (0.0f), (1.0f), /*hidden argument*/NULL);
		__this->set_targetAffinityPercentage_20(L_1);
		return;
	}
}
// System.Void CrazyMinnow.SALSA.RandomEyes2D::SetRandomEyes(System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void RandomEyes2D_SetRandomEyes_m422A7C480E27AC8B5D3F6DB4F19CFFAD52E74E8B (RandomEyes2D_t253864BE689DE7E477335D17ECD2EC77001B377F * __this, bool ___status0, const RuntimeMethod* method)
{
	{
		bool L_0 = ___status0;
		__this->set_randomEyes_21(L_0);
		float L_1;
		L_1 = Time_get_time_m1A186074B1FCD448AB13A4B9D715AB9ED0B40844(/*hidden argument*/NULL);
		__this->set_blinkTimer_36(L_1);
		float L_2;
		L_2 = Time_get_time_m1A186074B1FCD448AB13A4B9D715AB9ED0B40844(/*hidden argument*/NULL);
		__this->set_randomTimer_35(L_2);
		return;
	}
}
// System.Void CrazyMinnow.SALSA.RandomEyes2D::SetBlink(System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void RandomEyes2D_SetBlink_mFBF167619C503F29BE10F9050A91177B3B6DE8B9 (RandomEyes2D_t253864BE689DE7E477335D17ECD2EC77001B377F * __this, bool ___status0, const RuntimeMethod* method)
{
	{
		bool L_0 = ___status0;
		__this->set_randomBlink_24(L_0);
		return;
	}
}
// System.Void CrazyMinnow.SALSA.RandomEyes2D::SetBlinkDuration(System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void RandomEyes2D_SetBlinkDuration_m4A0D9E4B1A05916535B39E2217ADEA2EF39D33A7 (RandomEyes2D_t253864BE689DE7E477335D17ECD2EC77001B377F * __this, float ___duration0, const RuntimeMethod* method)
{
	{
		float L_0 = ___duration0;
		__this->set_blinkDuration_26(L_0);
		return;
	}
}
// System.Void CrazyMinnow.SALSA.RandomEyes2D::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void RandomEyes2D__ctor_m03558EFA8B83C41A3C01B3250F2DA35FE7DC2FC0 (RandomEyes2D_t253864BE689DE7E477335D17ECD2EC77001B377F * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&SpriteRendererU5BU5D_t45BF125944C2F3974C568E650FCCDB591E5ABFBF_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&SpriteRenderer_t3F35AD5498243C170B46F5FFDB582AAEF78615EF_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		SpriteRenderer_t3F35AD5498243C170B46F5FFDB582AAEF78615EF * L_0 = (SpriteRenderer_t3F35AD5498243C170B46F5FFDB582AAEF78615EF *)il2cpp_codegen_object_new(SpriteRenderer_t3F35AD5498243C170B46F5FFDB582AAEF78615EF_il2cpp_TypeInfo_var);
		SpriteRenderer__ctor_m5724D3A61E7A1DDF3EFB2A8B47A8A1348CB188D3(L_0, /*hidden argument*/NULL);
		__this->set_character_5(L_0);
		SpriteRendererU5BU5D_t45BF125944C2F3974C568E650FCCDB591E5ABFBF* L_1 = (SpriteRendererU5BU5D_t45BF125944C2F3974C568E650FCCDB591E5ABFBF*)(SpriteRendererU5BU5D_t45BF125944C2F3974C568E650FCCDB591E5ABFBF*)SZArrayNew(SpriteRendererU5BU5D_t45BF125944C2F3974C568E650FCCDB591E5ABFBF_il2cpp_TypeInfo_var, (uint32_t)0);
		__this->set_eyes_8(L_1);
		__this->set_eyeLayer_9(1);
		SpriteRendererU5BU5D_t45BF125944C2F3974C568E650FCCDB591E5ABFBF* L_2 = (SpriteRendererU5BU5D_t45BF125944C2F3974C568E650FCCDB591E5ABFBF*)(SpriteRendererU5BU5D_t45BF125944C2F3974C568E650FCCDB591E5ABFBF*)SZArrayNew(SpriteRendererU5BU5D_t45BF125944C2F3974C568E650FCCDB591E5ABFBF_il2cpp_TypeInfo_var, (uint32_t)0);
		__this->set_eyeLids_11(L_2);
		__this->set_eyeLidLayer_12(2);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_3;
		memset((&L_3), 0, sizeof(L_3));
		Vector3__ctor_m57495F692C6CE1CEF278CAD9A98221165D37E636_inline((&L_3), (0.0f), (0.0f), (0.0f), /*hidden argument*/NULL);
		__this->set_lookRandPosition_14(L_3);
		__this->set_rangeOfMotion_15((0.25f));
		__this->set_moveProportional_16((bool)1);
		__this->set_targetAffinityPercentage_20((0.699999988f));
		__this->set_randomEyes_21((bool)1);
		__this->set_maxRandomInterval_22((3.0f));
		__this->set_blendSpeed_23((7.0f));
		__this->set_randomBlink_24((bool)1);
		__this->set_maxBlinkInterval_25((5.0f));
		__this->set_blinkDuration_26((0.0500000007f));
		__this->set_expandEyes_27((bool)1);
		__this->set_expandEyelids_28((bool)1);
		__this->set_showBroadcast_29((bool)1);
		Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  L_4;
		memset((&L_4), 0, sizeof(L_4));
		Vector2__ctor_m9F1F2D5EB5D1FF7091BB527AC8A72CBB309D115E_inline((&L_4), (1.0f), (5.0f), /*hidden argument*/NULL);
		__this->set_targetAffinityTimerRange_32(L_4);
		MonoBehaviour__ctor_mC0995D847F6A95B1A553652636C38A2AA8B13BED(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void CrazyMinnow.SALSA.RandomEyes2DGizmo::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void RandomEyes2DGizmo__ctor_m1B5A6AB31BA24749312A096CBA2D9BE9AF973E9A (RandomEyes2DGizmo_t241646E16376D86639D345C821DF03D9EDFDDDDD * __this, const RuntimeMethod* method)
{
	{
		MonoBehaviour__ctor_mC0995D847F6A95B1A553652636C38A2AA8B13BED(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void CrazyMinnow.SALSA.RandomEyes3D::Reset()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void RandomEyes3D_Reset_m7D9EB1391455DBDBD702534215BD097180AE6FC3 (RandomEyes3D_t6868157C4605BD0225E1B08301F110E72336CE45 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Component_GetComponent_TisSkinnedMeshRenderer_t126F4D6010E0F4B2685A7817B0A9171805D8F496_m48EF3D17CF12700CC28C88CEFBB6741D6E1FFFE3_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		SkinnedMeshRenderer_t126F4D6010E0F4B2685A7817B0A9171805D8F496 * L_0 = __this->get_skinnedMeshRenderer_6();
		IL2CPP_RUNTIME_CLASS_INIT(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		bool L_1;
		L_1 = Object_op_Implicit_mC8214E4F028CC2F036CC82BDB81D102A02893499(L_0, /*hidden argument*/NULL);
		if (L_1)
		{
			goto IL_0019;
		}
	}
	{
		SkinnedMeshRenderer_t126F4D6010E0F4B2685A7817B0A9171805D8F496 * L_2;
		L_2 = Component_GetComponent_TisSkinnedMeshRenderer_t126F4D6010E0F4B2685A7817B0A9171805D8F496_m48EF3D17CF12700CC28C88CEFBB6741D6E1FFFE3(__this, /*hidden argument*/Component_GetComponent_TisSkinnedMeshRenderer_t126F4D6010E0F4B2685A7817B0A9171805D8F496_m48EF3D17CF12700CC28C88CEFBB6741D6E1FFFE3_RuntimeMethod_var);
		__this->set_skinnedMeshRenderer_6(L_2);
	}

IL_0019:
	{
		return;
	}
}
// System.Void CrazyMinnow.SALSA.RandomEyes3D::Start()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void RandomEyes3D_Start_m29BD63BB4AB6AB55143C09550A5FE0476EDF538C (RandomEyes3D_t6868157C4605BD0225E1B08301F110E72336CE45 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&BooleanU5BU5D_tEC7BAF93C44F875016DAADC8696EE3A465644D3C_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&RandomEyes3D_t6868157C4605BD0225E1B08301F110E72336CE45_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	{
		((RandomEyes3D_t6868157C4605BD0225E1B08301F110E72336CE45_StaticFields*)il2cpp_codegen_static_fields_for(RandomEyes3D_t6868157C4605BD0225E1B08301F110E72336CE45_il2cpp_TypeInfo_var))->set_instance_4(__this);
		Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 * L_0 = __this->get_address_of_targetAffinityTimerRange_74();
		float L_1 = L_0->get_x_0();
		Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 * L_2 = __this->get_address_of_targetAffinityTimerRange_74();
		float L_3 = L_2->get_y_1();
		float L_4;
		L_4 = Random_Range_mC15372D42A9ABDCAC3DE82E114D60A40C9C311D2(L_1, L_3, /*hidden argument*/NULL);
		__this->set_targetAffinityTimer_75(L_4);
		RuntimeObject* L_5;
		L_5 = RandomEyes3D_TargetAffinityUpdate_mF7439C88C6DF7E156DBF3E40D67C57EC64EF3E77(__this, /*hidden argument*/NULL);
		Coroutine_t899D5232EF542CB8BA70AF9ECEECA494FAA9CCB7 * L_6;
		L_6 = MonoBehaviour_StartCoroutine_m3E33706D38B23CDD179E99BAD61E32303E9CC719(__this, L_5, /*hidden argument*/NULL);
		float L_7;
		L_7 = Time_get_time_m1A186074B1FCD448AB13A4B9D715AB9ED0B40844(/*hidden argument*/NULL);
		float L_8 = __this->get_maxBlinkInterval_34();
		float L_9;
		L_9 = Random_Range_mC15372D42A9ABDCAC3DE82E114D60A40C9C311D2((1.5f), L_8, /*hidden argument*/NULL);
		__this->set_blinkTimer_79(((float)il2cpp_codegen_add((float)L_7, (float)L_9)));
		float L_10;
		L_10 = Time_get_time_m1A186074B1FCD448AB13A4B9D715AB9ED0B40844(/*hidden argument*/NULL);
		float L_11 = __this->get_maxRandomInterval_18();
		float L_12;
		L_12 = Random_Range_mC15372D42A9ABDCAC3DE82E114D60A40C9C311D2((0.25f), L_11, /*hidden argument*/NULL);
		__this->set_randomTimer_78(((float)il2cpp_codegen_add((float)L_10, (float)L_12)));
		float L_13;
		L_13 = Time_get_time_m1A186074B1FCD448AB13A4B9D715AB9ED0B40844(/*hidden argument*/NULL);
		float L_14 = __this->get_maxCustomShapeInterval_50();
		float L_15;
		L_15 = Random_Range_mC15372D42A9ABDCAC3DE82E114D60A40C9C311D2((0.25f), L_14, /*hidden argument*/NULL);
		__this->set_customShapeTimer_84(((float)il2cpp_codegen_add((float)L_13, (float)L_15)));
		int32_t L_16 = __this->get_customShapeCount_46();
		__this->set_prevCustomShapeCount_87(L_16);
		bool L_17 = __this->get_randomCustomShapes_49();
		__this->set_prevRandomCustomShapes_85(L_17);
		int32_t L_18 = __this->get_customShapeCount_46();
		BooleanU5BU5D_tEC7BAF93C44F875016DAADC8696EE3A465644D3C* L_19 = (BooleanU5BU5D_tEC7BAF93C44F875016DAADC8696EE3A465644D3C*)(BooleanU5BU5D_tEC7BAF93C44F875016DAADC8696EE3A465644D3C*)SZArrayNew(BooleanU5BU5D_tEC7BAF93C44F875016DAADC8696EE3A465644D3C_il2cpp_TypeInfo_var, (uint32_t)L_18);
		__this->set_prevIsOn_88(L_19);
		RandomEyesCustomShapeU5BU5D_t8FB0C62923194ED52AE0B06A6249680C47CA152C* L_20 = __this->get_customShapes_48();
		if (!L_20)
		{
			goto IL_0106;
		}
	}
	{
		RandomEyesCustomShapeU5BU5D_t8FB0C62923194ED52AE0B06A6249680C47CA152C* L_21 = __this->get_customShapes_48();
		NullCheck(L_21);
		BooleanU5BU5D_tEC7BAF93C44F875016DAADC8696EE3A465644D3C* L_22 = __this->get_prevIsOn_88();
		NullCheck(L_22);
		if ((((int32_t)((int32_t)((int32_t)(((RuntimeArray*)L_21)->max_length)))) <= ((int32_t)((int32_t)((int32_t)(((RuntimeArray*)L_22)->max_length))))))
		{
			goto IL_00de;
		}
	}
	{
		RandomEyesCustomShapeU5BU5D_t8FB0C62923194ED52AE0B06A6249680C47CA152C* L_23 = __this->get_customShapes_48();
		NullCheck(L_23);
		BooleanU5BU5D_tEC7BAF93C44F875016DAADC8696EE3A465644D3C* L_24 = (BooleanU5BU5D_tEC7BAF93C44F875016DAADC8696EE3A465644D3C*)(BooleanU5BU5D_tEC7BAF93C44F875016DAADC8696EE3A465644D3C*)SZArrayNew(BooleanU5BU5D_tEC7BAF93C44F875016DAADC8696EE3A465644D3C_il2cpp_TypeInfo_var, (uint32_t)((int32_t)((int32_t)(((RuntimeArray*)L_23)->max_length))));
		__this->set_prevIsOn_88(L_24);
	}

IL_00de:
	{
		V_0 = 0;
		goto IL_00fb;
	}

IL_00e2:
	{
		BooleanU5BU5D_tEC7BAF93C44F875016DAADC8696EE3A465644D3C* L_25 = __this->get_prevIsOn_88();
		int32_t L_26 = V_0;
		RandomEyesCustomShapeU5BU5D_t8FB0C62923194ED52AE0B06A6249680C47CA152C* L_27 = __this->get_customShapes_48();
		int32_t L_28 = V_0;
		NullCheck(L_27);
		int32_t L_29 = L_28;
		RandomEyesCustomShape_t10BCA0CEC18C2BBF6A8BD7BFAD2AF005D233C354 * L_30 = (L_27)->GetAt(static_cast<il2cpp_array_size_t>(L_29));
		NullCheck(L_30);
		bool L_31 = L_30->get_isOn_5();
		NullCheck(L_25);
		(L_25)->SetAt(static_cast<il2cpp_array_size_t>(L_26), (bool)L_31);
		int32_t L_32 = V_0;
		V_0 = ((int32_t)il2cpp_codegen_add((int32_t)L_32, (int32_t)1));
	}

IL_00fb:
	{
		int32_t L_33 = V_0;
		RandomEyesCustomShapeU5BU5D_t8FB0C62923194ED52AE0B06A6249680C47CA152C* L_34 = __this->get_customShapes_48();
		NullCheck(L_34);
		if ((((int32_t)L_33) < ((int32_t)((int32_t)((int32_t)(((RuntimeArray*)L_34)->max_length))))))
		{
			goto IL_00e2;
		}
	}

IL_0106:
	{
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_35 = __this->get_lookTarget_27();
		IL2CPP_RUNTIME_CLASS_INIT(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		bool L_36;
		L_36 = Object_op_Implicit_mC8214E4F028CC2F036CC82BDB81D102A02893499(L_35, /*hidden argument*/NULL);
		if (L_36)
		{
			goto IL_012d;
		}
	}
	{
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_37 = __this->get_lookTarget_27();
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_38 = __this->get_lookTargetTracking_28();
		IL2CPP_RUNTIME_CLASS_INIT(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		bool L_39;
		L_39 = Object_op_Inequality_mE1F187520BD83FB7D86A6D850710C4D42B864E90(L_37, L_38, /*hidden argument*/NULL);
		if (!L_39)
		{
			goto IL_012d;
		}
	}
	{
		__this->set_lookTargetTracking_28((GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 *)NULL);
	}

IL_012d:
	{
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_40 = __this->get_lookTarget_27();
		IL2CPP_RUNTIME_CLASS_INIT(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		bool L_41;
		L_41 = Object_op_Implicit_mC8214E4F028CC2F036CC82BDB81D102A02893499(L_40, /*hidden argument*/NULL);
		if (!L_41)
		{
			goto IL_0159;
		}
	}
	{
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_42 = __this->get_lookTarget_27();
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_43 = __this->get_lookTargetTracking_28();
		IL2CPP_RUNTIME_CLASS_INIT(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		bool L_44;
		L_44 = Object_op_Inequality_mE1F187520BD83FB7D86A6D850710C4D42B864E90(L_42, L_43, /*hidden argument*/NULL);
		if (!L_44)
		{
			goto IL_0159;
		}
	}
	{
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_45 = __this->get_lookTarget_27();
		__this->set_lookTargetTracking_28(L_45);
	}

IL_0159:
	{
		SkinnedMeshRenderer_t126F4D6010E0F4B2685A7817B0A9171805D8F496 * L_46 = __this->get_skinnedMeshRenderer_6();
		IL2CPP_RUNTIME_CLASS_INIT(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		bool L_47;
		L_47 = Object_op_Implicit_mC8214E4F028CC2F036CC82BDB81D102A02893499(L_46, /*hidden argument*/NULL);
		if (!L_47)
		{
			goto IL_017d;
		}
	}
	{
		SkinnedMeshRenderer_t126F4D6010E0F4B2685A7817B0A9171805D8F496 * L_48 = __this->get_skinnedMeshRenderer_6();
		int32_t L_49 = __this->get_blinkIndex_14();
		NullCheck(L_48);
		float L_50;
		L_50 = SkinnedMeshRenderer_GetBlendShapeWeight_m3F662DD48CC110C8429E53FD2A2E33DE601AA792(L_48, L_49, /*hidden argument*/NULL);
		__this->set_currentCloseAmount_82(L_50);
	}

IL_017d:
	{
		return;
	}
}
// System.Void CrazyMinnow.SALSA.RandomEyes3D::OnEnable()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void RandomEyes3D_OnEnable_mE7DAF2DEF3455C3BB7ED38E8DEC2EEE9C1D204B2 (RandomEyes3D_t6868157C4605BD0225E1B08301F110E72336CE45 * __this, const RuntimeMethod* method)
{
	{
		Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 * L_0 = __this->get_address_of_targetAffinityTimerRange_74();
		float L_1 = L_0->get_x_0();
		Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 * L_2 = __this->get_address_of_targetAffinityTimerRange_74();
		float L_3 = L_2->get_y_1();
		float L_4;
		L_4 = Random_Range_mC15372D42A9ABDCAC3DE82E114D60A40C9C311D2(L_1, L_3, /*hidden argument*/NULL);
		__this->set_targetAffinityTimer_75(L_4);
		RuntimeObject* L_5;
		L_5 = RandomEyes3D_TargetAffinityUpdate_mF7439C88C6DF7E156DBF3E40D67C57EC64EF3E77(__this, /*hidden argument*/NULL);
		Coroutine_t899D5232EF542CB8BA70AF9ECEECA494FAA9CCB7 * L_6;
		L_6 = MonoBehaviour_StartCoroutine_m3E33706D38B23CDD179E99BAD61E32303E9CC719(__this, L_5, /*hidden argument*/NULL);
		float L_7;
		L_7 = Time_get_time_m1A186074B1FCD448AB13A4B9D715AB9ED0B40844(/*hidden argument*/NULL);
		float L_8 = __this->get_maxBlinkInterval_34();
		float L_9;
		L_9 = Random_Range_mC15372D42A9ABDCAC3DE82E114D60A40C9C311D2((1.5f), L_8, /*hidden argument*/NULL);
		__this->set_blinkTimer_79(((float)il2cpp_codegen_add((float)L_7, (float)L_9)));
		float L_10;
		L_10 = Time_get_time_m1A186074B1FCD448AB13A4B9D715AB9ED0B40844(/*hidden argument*/NULL);
		float L_11 = __this->get_maxRandomInterval_18();
		float L_12;
		L_12 = Random_Range_mC15372D42A9ABDCAC3DE82E114D60A40C9C311D2((0.25f), L_11, /*hidden argument*/NULL);
		__this->set_randomTimer_78(((float)il2cpp_codegen_add((float)L_10, (float)L_12)));
		float L_13;
		L_13 = Time_get_time_m1A186074B1FCD448AB13A4B9D715AB9ED0B40844(/*hidden argument*/NULL);
		float L_14 = __this->get_maxCustomShapeInterval_50();
		float L_15;
		L_15 = Random_Range_mC15372D42A9ABDCAC3DE82E114D60A40C9C311D2((0.25f), L_14, /*hidden argument*/NULL);
		__this->set_customShapeTimer_84(((float)il2cpp_codegen_add((float)L_13, (float)L_15)));
		int32_t L_16 = __this->get_customShapeCount_46();
		__this->set_prevCustomShapeCount_87(L_16);
		bool L_17 = __this->get_randomCustomShapes_49();
		__this->set_prevRandomCustomShapes_85(L_17);
		return;
	}
}
// System.Void CrazyMinnow.SALSA.RandomEyes3D::LateUpdate()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void RandomEyes3D_LateUpdate_m87C843FC895A9CA260ADEABEDC307D030EAA5AE9 (RandomEyes3D_t6868157C4605BD0225E1B08301F110E72336CE45 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&BooleanU5BU5D_tEC7BAF93C44F875016DAADC8696EE3A465644D3C_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&RandomEyesCustomShapeStatus_tAE795B1C1EF38DFDE0535D6A919E25A96283DD66_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral548D93DDB2AC6B24373148B19D9A625571AB2318);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralDA39A3EE5E6B4B0D3255BFEF95601890AFD80709);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	int32_t V_2 = 0;
	int32_t V_3 = 0;
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  V_4;
	memset((&V_4), 0, sizeof(V_4));
	{
		__this->set_randomShapeIndex_77((-1));
		bool L_0 = __this->get_useCustomShapesOnly_58();
		if (L_0)
		{
			goto IL_028a;
		}
	}
	{
		bool L_1 = __this->get_randomBlink_33();
		if (!L_1)
		{
			goto IL_004f;
		}
	}
	{
		float L_2;
		L_2 = Time_get_time_m1A186074B1FCD448AB13A4B9D715AB9ED0B40844(/*hidden argument*/NULL);
		float L_3 = __this->get_blinkTimer_79();
		if ((!(((float)L_2) > ((float)L_3))))
		{
			goto IL_004f;
		}
	}
	{
		float L_4;
		L_4 = Time_get_time_m1A186074B1FCD448AB13A4B9D715AB9ED0B40844(/*hidden argument*/NULL);
		float L_5 = __this->get_maxBlinkInterval_34();
		float L_6;
		L_6 = Random_Range_mC15372D42A9ABDCAC3DE82E114D60A40C9C311D2((1.5f), L_5, /*hidden argument*/NULL);
		__this->set_blinkTimer_79(((float)il2cpp_codegen_add((float)L_4, (float)L_6)));
		float L_7 = __this->get_blinkDuration_21();
		RandomEyes3D_Blink_mCCC507CFFA092075E2BFAF242D716F81A073771C(__this, L_7, /*hidden argument*/NULL);
	}

IL_004f:
	{
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_8 = __this->get_lookTarget_27();
		IL2CPP_RUNTIME_CLASS_INIT(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		bool L_9;
		L_9 = Object_op_Implicit_mC8214E4F028CC2F036CC82BDB81D102A02893499(L_8, /*hidden argument*/NULL);
		if (L_9)
		{
			goto IL_0076;
		}
	}
	{
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_10 = __this->get_lookTarget_27();
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_11 = __this->get_lookTargetTracking_28();
		IL2CPP_RUNTIME_CLASS_INIT(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		bool L_12;
		L_12 = Object_op_Inequality_mE1F187520BD83FB7D86A6D850710C4D42B864E90(L_10, L_11, /*hidden argument*/NULL);
		if (!L_12)
		{
			goto IL_0076;
		}
	}
	{
		__this->set_lookTargetTracking_28((GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 *)NULL);
	}

IL_0076:
	{
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_13 = __this->get_lookTarget_27();
		IL2CPP_RUNTIME_CLASS_INIT(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		bool L_14;
		L_14 = Object_op_Implicit_mC8214E4F028CC2F036CC82BDB81D102A02893499(L_13, /*hidden argument*/NULL);
		if (!L_14)
		{
			goto IL_00aa;
		}
	}
	{
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_15 = __this->get_lookTarget_27();
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_16 = __this->get_lookTargetTracking_28();
		IL2CPP_RUNTIME_CLASS_INIT(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		bool L_17;
		L_17 = Object_op_Inequality_mE1F187520BD83FB7D86A6D850710C4D42B864E90(L_15, L_16, /*hidden argument*/NULL);
		if (!L_17)
		{
			goto IL_00aa;
		}
	}
	{
		bool L_18 = __this->get_targetAffinity_29();
		if (L_18)
		{
			goto IL_00aa;
		}
	}
	{
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_19 = __this->get_lookTarget_27();
		__this->set_lookTargetTracking_28(L_19);
	}

IL_00aa:
	{
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_20 = __this->get_lookTargetTracking_28();
		IL2CPP_RUNTIME_CLASS_INIT(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		bool L_21;
		L_21 = Object_op_Implicit_mC8214E4F028CC2F036CC82BDB81D102A02893499(L_20, /*hidden argument*/NULL);
		if (!L_21)
		{
			goto IL_00e6;
		}
	}
	{
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_22 = __this->get_eyePosition_25();
		NullCheck(L_22);
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_23;
		L_23 = GameObject_get_transform_m16A80BB92B6C8C5AB696E447014D45EDF1E4DE34(L_22, /*hidden argument*/NULL);
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_24 = __this->get_lookTargetTracking_28();
		NullCheck(L_24);
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_25;
		L_25 = GameObject_get_transform_m16A80BB92B6C8C5AB696E447014D45EDF1E4DE34(L_24, /*hidden argument*/NULL);
		NullCheck(L_25);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_26;
		L_26 = Transform_get_position_m40A8A9895568D56FFC687B57F30E8D53CB5EA341(L_25, /*hidden argument*/NULL);
		NullCheck(L_23);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_27;
		L_27 = Transform_InverseTransformPoint_m476ABC8F3F14824D7D82FE2C54CEE5A151A669B8(L_23, L_26, /*hidden argument*/NULL);
		V_4 = L_27;
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_28;
		L_28 = Vector3_get_normalized_m2FA6DF38F97BDA4CCBDAE12B9FE913A241DAC8D5((Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E *)(&V_4), /*hidden argument*/NULL);
		__this->set_lookDirection_68(L_28);
	}

IL_00e6:
	{
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_29 = __this->get_lookTargetTracking_28();
		IL2CPP_RUNTIME_CLASS_INIT(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		bool L_30;
		L_30 = Object_op_Implicit_mC8214E4F028CC2F036CC82BDB81D102A02893499(L_29, /*hidden argument*/NULL);
		if (!L_30)
		{
			goto IL_0106;
		}
	}
	{
		bool L_31 = __this->get_targetAffinity_29();
		if (L_31)
		{
			goto IL_0106;
		}
	}
	{
		bool L_32 = __this->get_trackBehind_32();
		if (L_32)
		{
			goto IL_0191;
		}
	}

IL_0106:
	{
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_33 = __this->get_lookTargetTracking_28();
		IL2CPP_RUNTIME_CLASS_INIT(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		bool L_34;
		L_34 = Object_op_Implicit_mC8214E4F028CC2F036CC82BDB81D102A02893499(L_33, /*hidden argument*/NULL);
		if (!L_34)
		{
			goto IL_013d;
		}
	}
	{
		bool L_35 = __this->get_targetAffinity_29();
		if (L_35)
		{
			goto IL_013d;
		}
	}
	{
		bool L_36 = __this->get_trackBehind_32();
		if (L_36)
		{
			goto IL_013d;
		}
	}
	{
		bool L_37 = __this->get_invertTracking_31();
		if (!L_37)
		{
			goto IL_013d;
		}
	}
	{
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * L_38 = __this->get_address_of_lookDirection_68();
		float L_39 = L_38->get_z_4();
		if ((((float)L_39) < ((float)(0.0f))))
		{
			goto IL_0191;
		}
	}

IL_013d:
	{
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_40 = __this->get_lookTargetTracking_28();
		IL2CPP_RUNTIME_CLASS_INIT(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		bool L_41;
		L_41 = Object_op_Implicit_mC8214E4F028CC2F036CC82BDB81D102A02893499(L_40, /*hidden argument*/NULL);
		if (!L_41)
		{
			goto IL_0174;
		}
	}
	{
		bool L_42 = __this->get_targetAffinity_29();
		if (L_42)
		{
			goto IL_0174;
		}
	}
	{
		bool L_43 = __this->get_trackBehind_32();
		if (L_43)
		{
			goto IL_0174;
		}
	}
	{
		bool L_44 = __this->get_invertTracking_31();
		if (L_44)
		{
			goto IL_0174;
		}
	}
	{
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * L_45 = __this->get_address_of_lookDirection_68();
		float L_46 = L_45->get_z_4();
		if ((((float)L_46) > ((float)(0.0f))))
		{
			goto IL_0191;
		}
	}

IL_0174:
	{
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_47 = __this->get_lookTargetTracking_28();
		IL2CPP_RUNTIME_CLASS_INIT(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		bool L_48;
		L_48 = Object_op_Implicit_mC8214E4F028CC2F036CC82BDB81D102A02893499(L_47, /*hidden argument*/NULL);
		if (!L_48)
		{
			goto IL_01a2;
		}
	}
	{
		bool L_49 = __this->get_targetAffinity_29();
		if (!L_49)
		{
			goto IL_01a2;
		}
	}
	{
		bool L_50 = __this->get_hasAffinity_76();
		if (!L_50)
		{
			goto IL_01a2;
		}
	}

IL_0191:
	{
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_51 = __this->get_lookDirection_68();
		RandomEyes3D_LookTracking_m7F29611D970378663A41B66374456DDAEDCC3205(__this, L_51, /*hidden argument*/NULL);
		goto IL_028a;
	}

IL_01a2:
	{
		bool L_52 = __this->get_randomEyes_17();
		if (L_52)
		{
			goto IL_01c4;
		}
	}
	{
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_53;
		memset((&L_53), 0, sizeof(L_53));
		Vector3__ctor_m57495F692C6CE1CEF278CAD9A98221165D37E636_inline((&L_53), (0.0f), (0.0f), (0.0f), /*hidden argument*/NULL);
		RandomEyes3D_LookTracking_m7F29611D970378663A41B66374456DDAEDCC3205(__this, L_53, /*hidden argument*/NULL);
	}

IL_01c4:
	{
		bool L_54 = __this->get_randomEyes_17();
		if (!L_54)
		{
			goto IL_028a;
		}
	}
	{
		bool L_55 = __this->get_isBlinking_80();
		if (L_55)
		{
			goto IL_028a;
		}
	}
	{
		float L_56;
		L_56 = Time_get_time_m1A186074B1FCD448AB13A4B9D715AB9ED0B40844(/*hidden argument*/NULL);
		float L_57 = __this->get_randomTimer_78();
		if ((!(((float)L_56) > ((float)L_57))))
		{
			goto IL_027e;
		}
	}
	{
		float L_58;
		L_58 = Time_get_time_m1A186074B1FCD448AB13A4B9D715AB9ED0B40844(/*hidden argument*/NULL);
		float L_59 = __this->get_maxRandomInterval_18();
		float L_60;
		L_60 = Random_Range_mC15372D42A9ABDCAC3DE82E114D60A40C9C311D2((0.25f), L_59, /*hidden argument*/NULL);
		__this->set_randomTimer_78(((float)il2cpp_codegen_add((float)L_58, (float)L_60)));
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_61;
		memset((&L_61), 0, sizeof(L_61));
		Vector3__ctor_m57495F692C6CE1CEF278CAD9A98221165D37E636_inline((&L_61), (0.0f), (0.0f), (0.0f), /*hidden argument*/NULL);
		__this->set_lookRandPosition_15(L_61);
		float L_62 = __this->get_rangeOfMotion_20();
		if ((!(((float)L_62) > ((float)(0.0f)))))
		{
			goto IL_023f;
		}
	}
	{
		float L_63 = __this->get_rangeOfMotion_20();
		__this->set_randomLookRange_69(((float)((float)L_63/(float)(100.0f))));
	}

IL_023f:
	{
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_64 = __this->get_lookRandPosition_15();
		float L_65 = __this->get_randomLookRange_69();
		float L_66 = __this->get_randomLookRange_69();
		float L_67;
		L_67 = Random_Range_mC15372D42A9ABDCAC3DE82E114D60A40C9C311D2(((-L_65)), L_66, /*hidden argument*/NULL);
		float L_68 = __this->get_randomLookRange_69();
		float L_69 = __this->get_randomLookRange_69();
		float L_70;
		L_70 = Random_Range_mC15372D42A9ABDCAC3DE82E114D60A40C9C311D2(((-L_68)), L_69, /*hidden argument*/NULL);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_71;
		memset((&L_71), 0, sizeof(L_71));
		Vector3__ctor_m57495F692C6CE1CEF278CAD9A98221165D37E636_inline((&L_71), L_67, L_70, (0.200000003f), /*hidden argument*/NULL);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_72;
		L_72 = Vector3_op_Addition_mEE4F672B923CCB184C39AABCA33443DB218E50E0_inline(L_64, L_71, /*hidden argument*/NULL);
		__this->set_lookRandPosition_15(L_72);
	}

IL_027e:
	{
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_73 = __this->get_lookRandPosition_15();
		RandomEyes3D_LookTracking_m7F29611D970378663A41B66374456DDAEDCC3205(__this, L_73, /*hidden argument*/NULL);
	}

IL_028a:
	{
		int32_t L_74 = __this->get_prevCustomShapeCount_87();
		int32_t L_75 = __this->get_customShapeCount_46();
		if ((((int32_t)L_74) == ((int32_t)L_75)))
		{
			goto IL_02d9;
		}
	}
	{
		int32_t L_76 = __this->get_customShapeCount_46();
		BooleanU5BU5D_tEC7BAF93C44F875016DAADC8696EE3A465644D3C* L_77 = (BooleanU5BU5D_tEC7BAF93C44F875016DAADC8696EE3A465644D3C*)(BooleanU5BU5D_tEC7BAF93C44F875016DAADC8696EE3A465644D3C*)SZArrayNew(BooleanU5BU5D_tEC7BAF93C44F875016DAADC8696EE3A465644D3C_il2cpp_TypeInfo_var, (uint32_t)L_76);
		__this->set_prevIsOn_88(L_77);
		RandomEyesCustomShapeU5BU5D_t8FB0C62923194ED52AE0B06A6249680C47CA152C* L_78 = __this->get_customShapes_48();
		if (!L_78)
		{
			goto IL_02d9;
		}
	}
	{
		V_0 = 0;
		goto IL_02ce;
	}

IL_02b5:
	{
		BooleanU5BU5D_tEC7BAF93C44F875016DAADC8696EE3A465644D3C* L_79 = __this->get_prevIsOn_88();
		int32_t L_80 = V_0;
		RandomEyesCustomShapeU5BU5D_t8FB0C62923194ED52AE0B06A6249680C47CA152C* L_81 = __this->get_customShapes_48();
		int32_t L_82 = V_0;
		NullCheck(L_81);
		int32_t L_83 = L_82;
		RandomEyesCustomShape_t10BCA0CEC18C2BBF6A8BD7BFAD2AF005D233C354 * L_84 = (L_81)->GetAt(static_cast<il2cpp_array_size_t>(L_83));
		NullCheck(L_84);
		bool L_85 = L_84->get_isOn_5();
		NullCheck(L_79);
		(L_79)->SetAt(static_cast<il2cpp_array_size_t>(L_80), (bool)L_85);
		int32_t L_86 = V_0;
		V_0 = ((int32_t)il2cpp_codegen_add((int32_t)L_86, (int32_t)1));
	}

IL_02ce:
	{
		int32_t L_87 = V_0;
		RandomEyesCustomShapeU5BU5D_t8FB0C62923194ED52AE0B06A6249680C47CA152C* L_88 = __this->get_customShapes_48();
		NullCheck(L_88);
		if ((((int32_t)L_87) < ((int32_t)((int32_t)((int32_t)(((RuntimeArray*)L_88)->max_length))))))
		{
			goto IL_02b5;
		}
	}

IL_02d9:
	{
		RandomEyesCustomShapeU5BU5D_t8FB0C62923194ED52AE0B06A6249680C47CA152C* L_89 = __this->get_customShapes_48();
		if (!L_89)
		{
			goto IL_0884;
		}
	}
	{
		V_1 = 0;
		goto IL_03dc;
	}

IL_02eb:
	{
		bool L_90 = __this->get_broadcastCS_36();
		if (!L_90)
		{
			goto IL_03d8;
		}
	}
	{
		BooleanU5BU5D_tEC7BAF93C44F875016DAADC8696EE3A465644D3C* L_91 = __this->get_prevIsOn_88();
		int32_t L_92 = V_1;
		NullCheck(L_91);
		int32_t L_93 = L_92;
		int8_t L_94 = (int8_t)(L_91)->GetAt(static_cast<il2cpp_array_size_t>(L_93));
		RandomEyesCustomShapeU5BU5D_t8FB0C62923194ED52AE0B06A6249680C47CA152C* L_95 = __this->get_customShapes_48();
		int32_t L_96 = V_1;
		NullCheck(L_95);
		int32_t L_97 = L_96;
		RandomEyesCustomShape_t10BCA0CEC18C2BBF6A8BD7BFAD2AF005D233C354 * L_98 = (L_95)->GetAt(static_cast<il2cpp_array_size_t>(L_97));
		NullCheck(L_98);
		bool L_99 = L_98->get_isOn_5();
		if ((((int32_t)L_94) == ((int32_t)L_99)))
		{
			goto IL_03d8;
		}
	}
	{
		RandomEyesCustomShapeStatus_tAE795B1C1EF38DFDE0535D6A919E25A96283DD66 * L_100 = (RandomEyesCustomShapeStatus_tAE795B1C1EF38DFDE0535D6A919E25A96283DD66 *)il2cpp_codegen_object_new(RandomEyesCustomShapeStatus_tAE795B1C1EF38DFDE0535D6A919E25A96283DD66_il2cpp_TypeInfo_var);
		RandomEyesCustomShapeStatus__ctor_m97BF52FFB152BF60CB27311F72EA547C783657C0(L_100, /*hidden argument*/NULL);
		__this->set_customShape_83(L_100);
		RandomEyesCustomShapeStatus_tAE795B1C1EF38DFDE0535D6A919E25A96283DD66 * L_101 = __this->get_customShape_83();
		NullCheck(L_101);
		RandomEyesCustomShapeStatus_set_instance_m4F5644F9DB6E38215FA12B1892E396CA0D8281B1_inline(L_101, __this, /*hidden argument*/NULL);
		RandomEyesCustomShapeStatus_tAE795B1C1EF38DFDE0535D6A919E25A96283DD66 * L_102 = __this->get_customShape_83();
		RandomEyesCustomShapeU5BU5D_t8FB0C62923194ED52AE0B06A6249680C47CA152C* L_103 = __this->get_customShapes_48();
		int32_t L_104 = V_1;
		NullCheck(L_103);
		int32_t L_105 = L_104;
		RandomEyesCustomShape_t10BCA0CEC18C2BBF6A8BD7BFAD2AF005D233C354 * L_106 = (L_103)->GetAt(static_cast<il2cpp_array_size_t>(L_105));
		NullCheck(L_106);
		int32_t L_107 = L_106->get_shapeIndex_0();
		NullCheck(L_102);
		((RandomEyesCustomShape_t10BCA0CEC18C2BBF6A8BD7BFAD2AF005D233C354 *)L_102)->set_shapeIndex_0(L_107);
		RandomEyesCustomShapeStatus_tAE795B1C1EF38DFDE0535D6A919E25A96283DD66 * L_108 = __this->get_customShape_83();
		RandomEyesCustomShapeU5BU5D_t8FB0C62923194ED52AE0B06A6249680C47CA152C* L_109 = __this->get_customShapes_48();
		int32_t L_110 = V_1;
		NullCheck(L_109);
		int32_t L_111 = L_110;
		RandomEyesCustomShape_t10BCA0CEC18C2BBF6A8BD7BFAD2AF005D233C354 * L_112 = (L_109)->GetAt(static_cast<il2cpp_array_size_t>(L_111));
		NullCheck(L_112);
		String_t* L_113 = L_112->get_shapeName_1();
		NullCheck(L_108);
		((RandomEyesCustomShape_t10BCA0CEC18C2BBF6A8BD7BFAD2AF005D233C354 *)L_108)->set_shapeName_1(L_113);
		RandomEyesCustomShapeStatus_tAE795B1C1EF38DFDE0535D6A919E25A96283DD66 * L_114 = __this->get_customShape_83();
		RandomEyesCustomShapeU5BU5D_t8FB0C62923194ED52AE0B06A6249680C47CA152C* L_115 = __this->get_customShapes_48();
		int32_t L_116 = V_1;
		NullCheck(L_115);
		int32_t L_117 = L_116;
		RandomEyesCustomShape_t10BCA0CEC18C2BBF6A8BD7BFAD2AF005D233C354 * L_118 = (L_115)->GetAt(static_cast<il2cpp_array_size_t>(L_117));
		NullCheck(L_118);
		bool L_119 = L_118->get_overrideOn_3();
		NullCheck(L_114);
		((RandomEyesCustomShape_t10BCA0CEC18C2BBF6A8BD7BFAD2AF005D233C354 *)L_114)->set_overrideOn_3(L_119);
		RandomEyesCustomShapeStatus_tAE795B1C1EF38DFDE0535D6A919E25A96283DD66 * L_120 = __this->get_customShape_83();
		RandomEyesCustomShapeU5BU5D_t8FB0C62923194ED52AE0B06A6249680C47CA152C* L_121 = __this->get_customShapes_48();
		int32_t L_122 = V_1;
		NullCheck(L_121);
		int32_t L_123 = L_122;
		RandomEyesCustomShape_t10BCA0CEC18C2BBF6A8BD7BFAD2AF005D233C354 * L_124 = (L_121)->GetAt(static_cast<il2cpp_array_size_t>(L_123));
		NullCheck(L_124);
		bool L_125 = L_124->get_isOn_5();
		NullCheck(L_120);
		((RandomEyesCustomShape_t10BCA0CEC18C2BBF6A8BD7BFAD2AF005D233C354 *)L_120)->set_isOn_5(L_125);
		RandomEyesCustomShapeStatus_tAE795B1C1EF38DFDE0535D6A919E25A96283DD66 * L_126 = __this->get_customShape_83();
		RandomEyesCustomShapeU5BU5D_t8FB0C62923194ED52AE0B06A6249680C47CA152C* L_127 = __this->get_customShapes_48();
		int32_t L_128 = V_1;
		NullCheck(L_127);
		int32_t L_129 = L_128;
		RandomEyesCustomShape_t10BCA0CEC18C2BBF6A8BD7BFAD2AF005D233C354 * L_130 = (L_127)->GetAt(static_cast<il2cpp_array_size_t>(L_129));
		NullCheck(L_130);
		float L_131 = L_130->get_blendSpeed_6();
		NullCheck(L_126);
		((RandomEyesCustomShape_t10BCA0CEC18C2BBF6A8BD7BFAD2AF005D233C354 *)L_126)->set_blendSpeed_6(L_131);
		RandomEyesCustomShapeStatus_tAE795B1C1EF38DFDE0535D6A919E25A96283DD66 * L_132 = __this->get_customShape_83();
		RandomEyesCustomShapeU5BU5D_t8FB0C62923194ED52AE0B06A6249680C47CA152C* L_133 = __this->get_customShapes_48();
		int32_t L_134 = V_1;
		NullCheck(L_133);
		int32_t L_135 = L_134;
		RandomEyesCustomShape_t10BCA0CEC18C2BBF6A8BD7BFAD2AF005D233C354 * L_136 = (L_133)->GetAt(static_cast<il2cpp_array_size_t>(L_135));
		NullCheck(L_136);
		float L_137 = L_136->get_rangeOfMotion_7();
		NullCheck(L_132);
		((RandomEyesCustomShape_t10BCA0CEC18C2BBF6A8BD7BFAD2AF005D233C354 *)L_132)->set_rangeOfMotion_7(L_137);
		RandomEyesCustomShapeStatus_tAE795B1C1EF38DFDE0535D6A919E25A96283DD66 * L_138 = __this->get_customShape_83();
		RandomEyes3D_CustomShapeChanged_m380A0E30479AFAD7D59CA1222BB6E77DCD1FF068(__this, L_138, /*hidden argument*/NULL);
		BooleanU5BU5D_tEC7BAF93C44F875016DAADC8696EE3A465644D3C* L_139 = __this->get_prevIsOn_88();
		int32_t L_140 = V_1;
		RandomEyesCustomShapeU5BU5D_t8FB0C62923194ED52AE0B06A6249680C47CA152C* L_141 = __this->get_customShapes_48();
		int32_t L_142 = V_1;
		NullCheck(L_141);
		int32_t L_143 = L_142;
		RandomEyesCustomShape_t10BCA0CEC18C2BBF6A8BD7BFAD2AF005D233C354 * L_144 = (L_141)->GetAt(static_cast<il2cpp_array_size_t>(L_143));
		NullCheck(L_144);
		bool L_145 = L_144->get_isOn_5();
		NullCheck(L_139);
		(L_139)->SetAt(static_cast<il2cpp_array_size_t>(L_140), (bool)L_145);
	}

IL_03d8:
	{
		int32_t L_146 = V_1;
		V_1 = ((int32_t)il2cpp_codegen_add((int32_t)L_146, (int32_t)1));
	}

IL_03dc:
	{
		int32_t L_147 = V_1;
		RandomEyesCustomShapeU5BU5D_t8FB0C62923194ED52AE0B06A6249680C47CA152C* L_148 = __this->get_customShapes_48();
		NullCheck(L_148);
		if ((((int32_t)L_147) < ((int32_t)((int32_t)((int32_t)(((RuntimeArray*)L_148)->max_length))))))
		{
			goto IL_02eb;
		}
	}
	{
		RandomEyesCustomShapeU5BU5D_t8FB0C62923194ED52AE0B06A6249680C47CA152C* L_149 = __this->get_customShapes_48();
		NullCheck(L_149);
		if ((((int32_t)((int32_t)((int32_t)(((RuntimeArray*)L_149)->max_length)))) <= ((int32_t)0)))
		{
			goto IL_0758;
		}
	}
	{
		RandomEyesCustomShapeU5BU5D_t8FB0C62923194ED52AE0B06A6249680C47CA152C* L_150 = __this->get_customShapes_48();
		NullCheck(L_150);
		int32_t L_151 = __this->get_customShapeCount_46();
		if ((((int32_t)((int32_t)((int32_t)(((RuntimeArray*)L_150)->max_length)))) <= ((int32_t)L_151)))
		{
			goto IL_0416;
		}
	}
	{
		RandomEyesCustomShapeU5BU5D_t8FB0C62923194ED52AE0B06A6249680C47CA152C* L_152 = __this->get_customShapes_48();
		NullCheck(L_152);
		__this->set_customShapeCount_46(((int32_t)((int32_t)(((RuntimeArray*)L_152)->max_length))));
	}

IL_0416:
	{
		StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* L_153 = __this->get_dynamicCustomShapes_45();
		if (!L_153)
		{
			goto IL_0758;
		}
	}
	{
		StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* L_154 = __this->get_dynamicCustomShapes_45();
		NullCheck(L_154);
		if ((((int32_t)((int32_t)((int32_t)(((RuntimeArray*)L_154)->max_length)))) <= ((int32_t)0)))
		{
			goto IL_0758;
		}
	}
	{
		int32_t L_155 = __this->get_selectedCustomShape_42();
		StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* L_156 = __this->get_dynamicCustomShapes_45();
		NullCheck(L_156);
		if ((((int32_t)L_155) >= ((int32_t)((int32_t)((int32_t)(((RuntimeArray*)L_156)->max_length))))))
		{
			goto IL_0758;
		}
	}
	{
		StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* L_157 = __this->get_dynamicCustomShapes_45();
		int32_t L_158 = __this->get_selectedCustomShape_42();
		NullCheck(L_157);
		int32_t L_159 = L_158;
		String_t* L_160 = (L_157)->GetAt(static_cast<il2cpp_array_size_t>(L_159));
		bool L_161;
		L_161 = String_op_Equality_m2B91EE68355F142F67095973D32EB5828B7B73CB(L_160, _stringLiteral548D93DDB2AC6B24373148B19D9A625571AB2318, /*hidden argument*/NULL);
		if (!L_161)
		{
			goto IL_05ca;
		}
	}
	{
		V_2 = 0;
		goto IL_05b7;
	}

IL_0465:
	{
		RandomEyesCustomShapeU5BU5D_t8FB0C62923194ED52AE0B06A6249680C47CA152C* L_162 = __this->get_customShapes_48();
		int32_t L_163 = V_2;
		NullCheck(L_162);
		int32_t L_164 = L_163;
		RandomEyesCustomShape_t10BCA0CEC18C2BBF6A8BD7BFAD2AF005D233C354 * L_165 = (L_162)->GetAt(static_cast<il2cpp_array_size_t>(L_164));
		NullCheck(L_165);
		bool L_166 = L_165->get_overrideOn_3();
		if (L_166)
		{
			goto IL_051d;
		}
	}
	{
		RandomEyesCustomShapeU5BU5D_t8FB0C62923194ED52AE0B06A6249680C47CA152C* L_167 = __this->get_customShapes_48();
		int32_t L_168 = V_2;
		NullCheck(L_167);
		int32_t L_169 = L_168;
		RandomEyesCustomShape_t10BCA0CEC18C2BBF6A8BD7BFAD2AF005D233C354 * L_170 = (L_167)->GetAt(static_cast<il2cpp_array_size_t>(L_169));
		NullCheck(L_170);
		float L_171 = L_170->get_shapeValue_2();
		if ((!(((float)L_171) > ((float)(0.0f)))))
		{
			goto IL_05b3;
		}
	}
	{
		RandomEyesCustomShapeU5BU5D_t8FB0C62923194ED52AE0B06A6249680C47CA152C* L_172 = __this->get_customShapes_48();
		int32_t L_173 = V_2;
		NullCheck(L_172);
		int32_t L_174 = L_173;
		RandomEyesCustomShape_t10BCA0CEC18C2BBF6A8BD7BFAD2AF005D233C354 * L_175 = (L_172)->GetAt(static_cast<il2cpp_array_size_t>(L_174));
		RandomEyesCustomShape_t10BCA0CEC18C2BBF6A8BD7BFAD2AF005D233C354 * L_176 = L_175;
		NullCheck(L_176);
		float L_177 = L_176->get_shapeValue_2();
		RandomEyesCustomShapeU5BU5D_t8FB0C62923194ED52AE0B06A6249680C47CA152C* L_178 = __this->get_customShapes_48();
		int32_t L_179 = V_2;
		NullCheck(L_178);
		int32_t L_180 = L_179;
		RandomEyesCustomShape_t10BCA0CEC18C2BBF6A8BD7BFAD2AF005D233C354 * L_181 = (L_178)->GetAt(static_cast<il2cpp_array_size_t>(L_180));
		NullCheck(L_181);
		float L_182 = L_181->get_blendSpeed_6();
		NullCheck(L_176);
		L_176->set_shapeValue_2(((float)il2cpp_codegen_subtract((float)L_177, (float)L_182)));
		RandomEyesCustomShapeU5BU5D_t8FB0C62923194ED52AE0B06A6249680C47CA152C* L_183 = __this->get_customShapes_48();
		int32_t L_184 = V_2;
		NullCheck(L_183);
		int32_t L_185 = L_184;
		RandomEyesCustomShape_t10BCA0CEC18C2BBF6A8BD7BFAD2AF005D233C354 * L_186 = (L_183)->GetAt(static_cast<il2cpp_array_size_t>(L_185));
		NullCheck(L_186);
		float L_187 = L_186->get_shapeValue_2();
		if ((!(((float)L_187) < ((float)(0.0f)))))
		{
			goto IL_04e3;
		}
	}
	{
		RandomEyesCustomShapeU5BU5D_t8FB0C62923194ED52AE0B06A6249680C47CA152C* L_188 = __this->get_customShapes_48();
		int32_t L_189 = V_2;
		NullCheck(L_188);
		int32_t L_190 = L_189;
		RandomEyesCustomShape_t10BCA0CEC18C2BBF6A8BD7BFAD2AF005D233C354 * L_191 = (L_188)->GetAt(static_cast<il2cpp_array_size_t>(L_190));
		NullCheck(L_191);
		L_191->set_shapeValue_2((0.0f));
		RandomEyesCustomShapeU5BU5D_t8FB0C62923194ED52AE0B06A6249680C47CA152C* L_192 = __this->get_customShapes_48();
		int32_t L_193 = V_2;
		NullCheck(L_192);
		int32_t L_194 = L_193;
		RandomEyesCustomShape_t10BCA0CEC18C2BBF6A8BD7BFAD2AF005D233C354 * L_195 = (L_192)->GetAt(static_cast<il2cpp_array_size_t>(L_194));
		NullCheck(L_195);
		L_195->set_isOn_5((bool)0);
	}

IL_04e3:
	{
		SkinnedMeshRenderer_t126F4D6010E0F4B2685A7817B0A9171805D8F496 * L_196 = __this->get_skinnedMeshRenderer_6();
		IL2CPP_RUNTIME_CLASS_INIT(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		bool L_197;
		L_197 = Object_op_Implicit_mC8214E4F028CC2F036CC82BDB81D102A02893499(L_196, /*hidden argument*/NULL);
		if (!L_197)
		{
			goto IL_05b3;
		}
	}
	{
		SkinnedMeshRenderer_t126F4D6010E0F4B2685A7817B0A9171805D8F496 * L_198 = __this->get_skinnedMeshRenderer_6();
		RandomEyesCustomShapeU5BU5D_t8FB0C62923194ED52AE0B06A6249680C47CA152C* L_199 = __this->get_customShapes_48();
		int32_t L_200 = V_2;
		NullCheck(L_199);
		int32_t L_201 = L_200;
		RandomEyesCustomShape_t10BCA0CEC18C2BBF6A8BD7BFAD2AF005D233C354 * L_202 = (L_199)->GetAt(static_cast<il2cpp_array_size_t>(L_201));
		NullCheck(L_202);
		int32_t L_203 = L_202->get_shapeIndex_0();
		RandomEyesCustomShapeU5BU5D_t8FB0C62923194ED52AE0B06A6249680C47CA152C* L_204 = __this->get_customShapes_48();
		int32_t L_205 = V_2;
		NullCheck(L_204);
		int32_t L_206 = L_205;
		RandomEyesCustomShape_t10BCA0CEC18C2BBF6A8BD7BFAD2AF005D233C354 * L_207 = (L_204)->GetAt(static_cast<il2cpp_array_size_t>(L_206));
		NullCheck(L_207);
		float L_208 = L_207->get_shapeValue_2();
		NullCheck(L_198);
		SkinnedMeshRenderer_SetBlendShapeWeight_mF546F3567C5039C217AD1E32157B992B4124B5FD(L_198, L_203, L_208, /*hidden argument*/NULL);
		goto IL_05b3;
	}

IL_051d:
	{
		RandomEyesCustomShapeU5BU5D_t8FB0C62923194ED52AE0B06A6249680C47CA152C* L_209 = __this->get_customShapes_48();
		int32_t L_210 = V_2;
		NullCheck(L_209);
		int32_t L_211 = L_210;
		RandomEyesCustomShape_t10BCA0CEC18C2BBF6A8BD7BFAD2AF005D233C354 * L_212 = (L_209)->GetAt(static_cast<il2cpp_array_size_t>(L_211));
		RandomEyesCustomShapeU5BU5D_t8FB0C62923194ED52AE0B06A6249680C47CA152C* L_213 = __this->get_customShapes_48();
		int32_t L_214 = V_2;
		NullCheck(L_213);
		int32_t L_215 = L_214;
		RandomEyesCustomShape_t10BCA0CEC18C2BBF6A8BD7BFAD2AF005D233C354 * L_216 = (L_213)->GetAt(static_cast<il2cpp_array_size_t>(L_215));
		NullCheck(L_216);
		float L_217 = L_216->get_shapeValue_2();
		RandomEyesCustomShapeU5BU5D_t8FB0C62923194ED52AE0B06A6249680C47CA152C* L_218 = __this->get_customShapes_48();
		int32_t L_219 = V_2;
		NullCheck(L_218);
		int32_t L_220 = L_219;
		RandomEyesCustomShape_t10BCA0CEC18C2BBF6A8BD7BFAD2AF005D233C354 * L_221 = (L_218)->GetAt(static_cast<il2cpp_array_size_t>(L_220));
		NullCheck(L_221);
		float L_222 = L_221->get_blendSpeed_6();
		RandomEyesCustomShapeU5BU5D_t8FB0C62923194ED52AE0B06A6249680C47CA152C* L_223 = __this->get_customShapes_48();
		int32_t L_224 = V_2;
		NullCheck(L_223);
		int32_t L_225 = L_224;
		RandomEyesCustomShape_t10BCA0CEC18C2BBF6A8BD7BFAD2AF005D233C354 * L_226 = (L_223)->GetAt(static_cast<il2cpp_array_size_t>(L_225));
		NullCheck(L_226);
		float L_227 = L_226->get_rangeOfMotion_7();
		float L_228;
		L_228 = SalsaUtility_LerpRangeOfMotion_mDA97D8D31F6D62217220938CD785C48A75AEB246(L_217, L_222, L_227, 0, /*hidden argument*/NULL);
		NullCheck(L_212);
		L_212->set_shapeValue_2(L_228);
		SkinnedMeshRenderer_t126F4D6010E0F4B2685A7817B0A9171805D8F496 * L_229 = __this->get_skinnedMeshRenderer_6();
		IL2CPP_RUNTIME_CLASS_INIT(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		bool L_230;
		L_230 = Object_op_Implicit_mC8214E4F028CC2F036CC82BDB81D102A02893499(L_229, /*hidden argument*/NULL);
		if (!L_230)
		{
			goto IL_0589;
		}
	}
	{
		SkinnedMeshRenderer_t126F4D6010E0F4B2685A7817B0A9171805D8F496 * L_231 = __this->get_skinnedMeshRenderer_6();
		RandomEyesCustomShapeU5BU5D_t8FB0C62923194ED52AE0B06A6249680C47CA152C* L_232 = __this->get_customShapes_48();
		int32_t L_233 = V_2;
		NullCheck(L_232);
		int32_t L_234 = L_233;
		RandomEyesCustomShape_t10BCA0CEC18C2BBF6A8BD7BFAD2AF005D233C354 * L_235 = (L_232)->GetAt(static_cast<il2cpp_array_size_t>(L_234));
		NullCheck(L_235);
		int32_t L_236 = L_235->get_shapeIndex_0();
		RandomEyesCustomShapeU5BU5D_t8FB0C62923194ED52AE0B06A6249680C47CA152C* L_237 = __this->get_customShapes_48();
		int32_t L_238 = V_2;
		NullCheck(L_237);
		int32_t L_239 = L_238;
		RandomEyesCustomShape_t10BCA0CEC18C2BBF6A8BD7BFAD2AF005D233C354 * L_240 = (L_237)->GetAt(static_cast<il2cpp_array_size_t>(L_239));
		NullCheck(L_240);
		float L_241 = L_240->get_shapeValue_2();
		NullCheck(L_231);
		SkinnedMeshRenderer_SetBlendShapeWeight_mF546F3567C5039C217AD1E32157B992B4124B5FD(L_231, L_236, L_241, /*hidden argument*/NULL);
	}

IL_0589:
	{
		RandomEyesCustomShapeU5BU5D_t8FB0C62923194ED52AE0B06A6249680C47CA152C* L_242 = __this->get_customShapes_48();
		int32_t L_243 = V_2;
		NullCheck(L_242);
		int32_t L_244 = L_243;
		RandomEyesCustomShape_t10BCA0CEC18C2BBF6A8BD7BFAD2AF005D233C354 * L_245 = (L_242)->GetAt(static_cast<il2cpp_array_size_t>(L_244));
		NullCheck(L_245);
		float L_246 = L_245->get_shapeValue_2();
		RandomEyesCustomShapeU5BU5D_t8FB0C62923194ED52AE0B06A6249680C47CA152C* L_247 = __this->get_customShapes_48();
		int32_t L_248 = V_2;
		NullCheck(L_247);
		int32_t L_249 = L_248;
		RandomEyesCustomShape_t10BCA0CEC18C2BBF6A8BD7BFAD2AF005D233C354 * L_250 = (L_247)->GetAt(static_cast<il2cpp_array_size_t>(L_249));
		NullCheck(L_250);
		float L_251 = L_250->get_rangeOfMotion_7();
		if ((!(((float)L_246) == ((float)L_251))))
		{
			goto IL_05b3;
		}
	}
	{
		RandomEyesCustomShapeU5BU5D_t8FB0C62923194ED52AE0B06A6249680C47CA152C* L_252 = __this->get_customShapes_48();
		int32_t L_253 = V_2;
		NullCheck(L_252);
		int32_t L_254 = L_253;
		RandomEyesCustomShape_t10BCA0CEC18C2BBF6A8BD7BFAD2AF005D233C354 * L_255 = (L_252)->GetAt(static_cast<il2cpp_array_size_t>(L_254));
		NullCheck(L_255);
		L_255->set_isOn_5((bool)1);
	}

IL_05b3:
	{
		int32_t L_256 = V_2;
		V_2 = ((int32_t)il2cpp_codegen_add((int32_t)L_256, (int32_t)1));
	}

IL_05b7:
	{
		int32_t L_257 = V_2;
		RandomEyesCustomShapeU5BU5D_t8FB0C62923194ED52AE0B06A6249680C47CA152C* L_258 = __this->get_customShapes_48();
		NullCheck(L_258);
		if ((((int32_t)L_257) < ((int32_t)((int32_t)((int32_t)(((RuntimeArray*)L_258)->max_length))))))
		{
			goto IL_0465;
		}
	}
	{
		goto IL_0758;
	}

IL_05ca:
	{
		V_3 = 0;
		goto IL_074a;
	}

IL_05d1:
	{
		RandomEyesCustomShapeU5BU5D_t8FB0C62923194ED52AE0B06A6249680C47CA152C* L_259 = __this->get_customShapes_48();
		int32_t L_260 = V_3;
		NullCheck(L_259);
		int32_t L_261 = L_260;
		RandomEyesCustomShape_t10BCA0CEC18C2BBF6A8BD7BFAD2AF005D233C354 * L_262 = (L_259)->GetAt(static_cast<il2cpp_array_size_t>(L_261));
		NullCheck(L_262);
		bool L_263 = L_262->get_overrideOn_3();
		if (L_263)
		{
			goto IL_067d;
		}
	}
	{
		int32_t L_264 = V_3;
		int32_t L_265 = __this->get_selectedCustomShape_42();
		if ((((int32_t)L_264) == ((int32_t)L_265)))
		{
			goto IL_067d;
		}
	}
	{
		RandomEyesCustomShapeU5BU5D_t8FB0C62923194ED52AE0B06A6249680C47CA152C* L_266 = __this->get_customShapes_48();
		int32_t L_267 = V_3;
		NullCheck(L_266);
		int32_t L_268 = L_267;
		RandomEyesCustomShape_t10BCA0CEC18C2BBF6A8BD7BFAD2AF005D233C354 * L_269 = (L_266)->GetAt(static_cast<il2cpp_array_size_t>(L_268));
		NullCheck(L_269);
		float L_270 = L_269->get_shapeValue_2();
		if ((!(((float)L_270) > ((float)(0.0f)))))
		{
			goto IL_067d;
		}
	}
	{
		RandomEyesCustomShapeU5BU5D_t8FB0C62923194ED52AE0B06A6249680C47CA152C* L_271 = __this->get_customShapes_48();
		int32_t L_272 = V_3;
		NullCheck(L_271);
		int32_t L_273 = L_272;
		RandomEyesCustomShape_t10BCA0CEC18C2BBF6A8BD7BFAD2AF005D233C354 * L_274 = (L_271)->GetAt(static_cast<il2cpp_array_size_t>(L_273));
		RandomEyesCustomShape_t10BCA0CEC18C2BBF6A8BD7BFAD2AF005D233C354 * L_275 = L_274;
		NullCheck(L_275);
		float L_276 = L_275->get_shapeValue_2();
		RandomEyesCustomShapeU5BU5D_t8FB0C62923194ED52AE0B06A6249680C47CA152C* L_277 = __this->get_customShapes_48();
		int32_t L_278 = V_3;
		NullCheck(L_277);
		int32_t L_279 = L_278;
		RandomEyesCustomShape_t10BCA0CEC18C2BBF6A8BD7BFAD2AF005D233C354 * L_280 = (L_277)->GetAt(static_cast<il2cpp_array_size_t>(L_279));
		NullCheck(L_280);
		float L_281 = L_280->get_blendSpeed_6();
		NullCheck(L_275);
		L_275->set_shapeValue_2(((float)il2cpp_codegen_subtract((float)L_276, (float)L_281)));
		RandomEyesCustomShapeU5BU5D_t8FB0C62923194ED52AE0B06A6249680C47CA152C* L_282 = __this->get_customShapes_48();
		int32_t L_283 = V_3;
		NullCheck(L_282);
		int32_t L_284 = L_283;
		RandomEyesCustomShape_t10BCA0CEC18C2BBF6A8BD7BFAD2AF005D233C354 * L_285 = (L_282)->GetAt(static_cast<il2cpp_array_size_t>(L_284));
		NullCheck(L_285);
		float L_286 = L_285->get_shapeValue_2();
		if ((!(((float)L_286) < ((float)(0.0f)))))
		{
			goto IL_0658;
		}
	}
	{
		RandomEyesCustomShapeU5BU5D_t8FB0C62923194ED52AE0B06A6249680C47CA152C* L_287 = __this->get_customShapes_48();
		int32_t L_288 = V_3;
		NullCheck(L_287);
		int32_t L_289 = L_288;
		RandomEyesCustomShape_t10BCA0CEC18C2BBF6A8BD7BFAD2AF005D233C354 * L_290 = (L_287)->GetAt(static_cast<il2cpp_array_size_t>(L_289));
		NullCheck(L_290);
		L_290->set_shapeValue_2((0.0f));
		RandomEyesCustomShapeU5BU5D_t8FB0C62923194ED52AE0B06A6249680C47CA152C* L_291 = __this->get_customShapes_48();
		int32_t L_292 = V_3;
		NullCheck(L_291);
		int32_t L_293 = L_292;
		RandomEyesCustomShape_t10BCA0CEC18C2BBF6A8BD7BFAD2AF005D233C354 * L_294 = (L_291)->GetAt(static_cast<il2cpp_array_size_t>(L_293));
		NullCheck(L_294);
		L_294->set_isOn_5((bool)0);
	}

IL_0658:
	{
		SkinnedMeshRenderer_t126F4D6010E0F4B2685A7817B0A9171805D8F496 * L_295 = __this->get_skinnedMeshRenderer_6();
		RandomEyesCustomShapeU5BU5D_t8FB0C62923194ED52AE0B06A6249680C47CA152C* L_296 = __this->get_customShapes_48();
		int32_t L_297 = V_3;
		NullCheck(L_296);
		int32_t L_298 = L_297;
		RandomEyesCustomShape_t10BCA0CEC18C2BBF6A8BD7BFAD2AF005D233C354 * L_299 = (L_296)->GetAt(static_cast<il2cpp_array_size_t>(L_298));
		NullCheck(L_299);
		int32_t L_300 = L_299->get_shapeIndex_0();
		RandomEyesCustomShapeU5BU5D_t8FB0C62923194ED52AE0B06A6249680C47CA152C* L_301 = __this->get_customShapes_48();
		int32_t L_302 = V_3;
		NullCheck(L_301);
		int32_t L_303 = L_302;
		RandomEyesCustomShape_t10BCA0CEC18C2BBF6A8BD7BFAD2AF005D233C354 * L_304 = (L_301)->GetAt(static_cast<il2cpp_array_size_t>(L_303));
		NullCheck(L_304);
		float L_305 = L_304->get_shapeValue_2();
		NullCheck(L_295);
		SkinnedMeshRenderer_SetBlendShapeWeight_mF546F3567C5039C217AD1E32157B992B4124B5FD(L_295, L_300, L_305, /*hidden argument*/NULL);
	}

IL_067d:
	{
		RandomEyesCustomShapeU5BU5D_t8FB0C62923194ED52AE0B06A6249680C47CA152C* L_306 = __this->get_customShapes_48();
		int32_t L_307 = V_3;
		NullCheck(L_306);
		int32_t L_308 = L_307;
		RandomEyesCustomShape_t10BCA0CEC18C2BBF6A8BD7BFAD2AF005D233C354 * L_309 = (L_306)->GetAt(static_cast<il2cpp_array_size_t>(L_308));
		NullCheck(L_309);
		bool L_310 = L_309->get_overrideOn_3();
		if (L_310)
		{
			goto IL_06b0;
		}
	}
	{
		StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* L_311 = __this->get_dynamicCustomShapes_45();
		int32_t L_312 = __this->get_selectedCustomShape_42();
		NullCheck(L_311);
		int32_t L_313 = L_312;
		String_t* L_314 = (L_311)->GetAt(static_cast<il2cpp_array_size_t>(L_313));
		RandomEyesCustomShapeU5BU5D_t8FB0C62923194ED52AE0B06A6249680C47CA152C* L_315 = __this->get_customShapes_48();
		int32_t L_316 = V_3;
		NullCheck(L_315);
		int32_t L_317 = L_316;
		RandomEyesCustomShape_t10BCA0CEC18C2BBF6A8BD7BFAD2AF005D233C354 * L_318 = (L_315)->GetAt(static_cast<il2cpp_array_size_t>(L_317));
		NullCheck(L_318);
		String_t* L_319 = L_318->get_shapeName_1();
		bool L_320;
		L_320 = String_op_Equality_m2B91EE68355F142F67095973D32EB5828B7B73CB(L_314, L_319, /*hidden argument*/NULL);
		if (!L_320)
		{
			goto IL_0746;
		}
	}

IL_06b0:
	{
		RandomEyesCustomShapeU5BU5D_t8FB0C62923194ED52AE0B06A6249680C47CA152C* L_321 = __this->get_customShapes_48();
		int32_t L_322 = V_3;
		NullCheck(L_321);
		int32_t L_323 = L_322;
		RandomEyesCustomShape_t10BCA0CEC18C2BBF6A8BD7BFAD2AF005D233C354 * L_324 = (L_321)->GetAt(static_cast<il2cpp_array_size_t>(L_323));
		RandomEyesCustomShapeU5BU5D_t8FB0C62923194ED52AE0B06A6249680C47CA152C* L_325 = __this->get_customShapes_48();
		int32_t L_326 = V_3;
		NullCheck(L_325);
		int32_t L_327 = L_326;
		RandomEyesCustomShape_t10BCA0CEC18C2BBF6A8BD7BFAD2AF005D233C354 * L_328 = (L_325)->GetAt(static_cast<il2cpp_array_size_t>(L_327));
		NullCheck(L_328);
		float L_329 = L_328->get_shapeValue_2();
		RandomEyesCustomShapeU5BU5D_t8FB0C62923194ED52AE0B06A6249680C47CA152C* L_330 = __this->get_customShapes_48();
		int32_t L_331 = V_3;
		NullCheck(L_330);
		int32_t L_332 = L_331;
		RandomEyesCustomShape_t10BCA0CEC18C2BBF6A8BD7BFAD2AF005D233C354 * L_333 = (L_330)->GetAt(static_cast<il2cpp_array_size_t>(L_332));
		NullCheck(L_333);
		float L_334 = L_333->get_blendSpeed_6();
		RandomEyesCustomShapeU5BU5D_t8FB0C62923194ED52AE0B06A6249680C47CA152C* L_335 = __this->get_customShapes_48();
		int32_t L_336 = V_3;
		NullCheck(L_335);
		int32_t L_337 = L_336;
		RandomEyesCustomShape_t10BCA0CEC18C2BBF6A8BD7BFAD2AF005D233C354 * L_338 = (L_335)->GetAt(static_cast<il2cpp_array_size_t>(L_337));
		NullCheck(L_338);
		float L_339 = L_338->get_rangeOfMotion_7();
		float L_340;
		L_340 = SalsaUtility_LerpRangeOfMotion_mDA97D8D31F6D62217220938CD785C48A75AEB246(L_329, L_334, L_339, 0, /*hidden argument*/NULL);
		NullCheck(L_324);
		L_324->set_shapeValue_2(L_340);
		SkinnedMeshRenderer_t126F4D6010E0F4B2685A7817B0A9171805D8F496 * L_341 = __this->get_skinnedMeshRenderer_6();
		IL2CPP_RUNTIME_CLASS_INIT(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		bool L_342;
		L_342 = Object_op_Implicit_mC8214E4F028CC2F036CC82BDB81D102A02893499(L_341, /*hidden argument*/NULL);
		if (!L_342)
		{
			goto IL_071c;
		}
	}
	{
		SkinnedMeshRenderer_t126F4D6010E0F4B2685A7817B0A9171805D8F496 * L_343 = __this->get_skinnedMeshRenderer_6();
		RandomEyesCustomShapeU5BU5D_t8FB0C62923194ED52AE0B06A6249680C47CA152C* L_344 = __this->get_customShapes_48();
		int32_t L_345 = V_3;
		NullCheck(L_344);
		int32_t L_346 = L_345;
		RandomEyesCustomShape_t10BCA0CEC18C2BBF6A8BD7BFAD2AF005D233C354 * L_347 = (L_344)->GetAt(static_cast<il2cpp_array_size_t>(L_346));
		NullCheck(L_347);
		int32_t L_348 = L_347->get_shapeIndex_0();
		RandomEyesCustomShapeU5BU5D_t8FB0C62923194ED52AE0B06A6249680C47CA152C* L_349 = __this->get_customShapes_48();
		int32_t L_350 = V_3;
		NullCheck(L_349);
		int32_t L_351 = L_350;
		RandomEyesCustomShape_t10BCA0CEC18C2BBF6A8BD7BFAD2AF005D233C354 * L_352 = (L_349)->GetAt(static_cast<il2cpp_array_size_t>(L_351));
		NullCheck(L_352);
		float L_353 = L_352->get_shapeValue_2();
		NullCheck(L_343);
		SkinnedMeshRenderer_SetBlendShapeWeight_mF546F3567C5039C217AD1E32157B992B4124B5FD(L_343, L_348, L_353, /*hidden argument*/NULL);
	}

IL_071c:
	{
		RandomEyesCustomShapeU5BU5D_t8FB0C62923194ED52AE0B06A6249680C47CA152C* L_354 = __this->get_customShapes_48();
		int32_t L_355 = V_3;
		NullCheck(L_354);
		int32_t L_356 = L_355;
		RandomEyesCustomShape_t10BCA0CEC18C2BBF6A8BD7BFAD2AF005D233C354 * L_357 = (L_354)->GetAt(static_cast<il2cpp_array_size_t>(L_356));
		NullCheck(L_357);
		float L_358 = L_357->get_shapeValue_2();
		RandomEyesCustomShapeU5BU5D_t8FB0C62923194ED52AE0B06A6249680C47CA152C* L_359 = __this->get_customShapes_48();
		int32_t L_360 = V_3;
		NullCheck(L_359);
		int32_t L_361 = L_360;
		RandomEyesCustomShape_t10BCA0CEC18C2BBF6A8BD7BFAD2AF005D233C354 * L_362 = (L_359)->GetAt(static_cast<il2cpp_array_size_t>(L_361));
		NullCheck(L_362);
		float L_363 = L_362->get_rangeOfMotion_7();
		if ((!(((float)L_358) == ((float)L_363))))
		{
			goto IL_0746;
		}
	}
	{
		RandomEyesCustomShapeU5BU5D_t8FB0C62923194ED52AE0B06A6249680C47CA152C* L_364 = __this->get_customShapes_48();
		int32_t L_365 = V_3;
		NullCheck(L_364);
		int32_t L_366 = L_365;
		RandomEyesCustomShape_t10BCA0CEC18C2BBF6A8BD7BFAD2AF005D233C354 * L_367 = (L_364)->GetAt(static_cast<il2cpp_array_size_t>(L_366));
		NullCheck(L_367);
		L_367->set_isOn_5((bool)1);
	}

IL_0746:
	{
		int32_t L_368 = V_3;
		V_3 = ((int32_t)il2cpp_codegen_add((int32_t)L_368, (int32_t)1));
	}

IL_074a:
	{
		int32_t L_369 = V_3;
		RandomEyesCustomShapeU5BU5D_t8FB0C62923194ED52AE0B06A6249680C47CA152C* L_370 = __this->get_customShapes_48();
		NullCheck(L_370);
		if ((((int32_t)L_369) < ((int32_t)((int32_t)((int32_t)(((RuntimeArray*)L_370)->max_length))))))
		{
			goto IL_05d1;
		}
	}

IL_0758:
	{
		bool L_371 = __this->get_randomCustomShapes_49();
		RandomEyesCustomShapeU5BU5D_t8FB0C62923194ED52AE0B06A6249680C47CA152C* L_372 = __this->get_customShapes_48();
		NullCheck(L_372);
		if (!((int32_t)((int32_t)L_371&(int32_t)((((int32_t)((int32_t)((int32_t)(((RuntimeArray*)L_372)->max_length)))) > ((int32_t)0))? 1 : 0))))
		{
			goto IL_0884;
		}
	}
	{
		bool L_373 = __this->get_prevRandomCustomShapes_85();
		bool L_374 = __this->get_randomCustomShapes_49();
		if ((((int32_t)L_373) == ((int32_t)L_374)))
		{
			goto IL_0794;
		}
	}
	{
		float L_375;
		L_375 = Time_get_time_m1A186074B1FCD448AB13A4B9D715AB9ED0B40844(/*hidden argument*/NULL);
		__this->set_customShapeTimer_84(L_375);
		bool L_376 = __this->get_randomCustomShapes_49();
		__this->set_prevRandomCustomShapes_85(L_376);
	}

IL_0794:
	{
		float L_377;
		L_377 = Time_get_time_m1A186074B1FCD448AB13A4B9D715AB9ED0B40844(/*hidden argument*/NULL);
		float L_378 = __this->get_customShapeTimer_84();
		if ((!(((float)L_377) > ((float)L_378))))
		{
			goto IL_0884;
		}
	}
	{
		RandomEyesCustomShapeU5BU5D_t8FB0C62923194ED52AE0B06A6249680C47CA152C* L_379 = __this->get_customShapes_48();
		NullCheck(L_379);
		int32_t L_380;
		L_380 = Random_Range_m4B3A0037ACA057F33C94508F908546B9317D996A(0, ((int32_t)((int32_t)(((RuntimeArray*)L_379)->max_length))), /*hidden argument*/NULL);
		__this->set_randomShapeIndex_77(L_380);
		RandomEyesCustomShapeU5BU5D_t8FB0C62923194ED52AE0B06A6249680C47CA152C* L_381 = __this->get_customShapes_48();
		int32_t L_382 = __this->get_randomShapeIndex_77();
		NullCheck(L_381);
		int32_t L_383 = L_382;
		RandomEyesCustomShape_t10BCA0CEC18C2BBF6A8BD7BFAD2AF005D233C354 * L_384 = (L_381)->GetAt(static_cast<il2cpp_array_size_t>(L_383));
		NullCheck(L_384);
		String_t* L_385 = L_384->get_shapeName_1();
		__this->set_randomShape_86(L_385);
		String_t* L_386 = __this->get_randomShape_86();
		bool L_387;
		L_387 = String_op_Inequality_mDDA2DDED3E7EF042987EB7180EE3E88105F0AAE2(L_386, _stringLiteralDA39A3EE5E6B4B0D3255BFEF95601890AFD80709, /*hidden argument*/NULL);
		if (!L_387)
		{
			goto IL_0884;
		}
	}
	{
		RandomEyesCustomShapeU5BU5D_t8FB0C62923194ED52AE0B06A6249680C47CA152C* L_388 = __this->get_customShapes_48();
		int32_t L_389 = __this->get_randomShapeIndex_77();
		NullCheck(L_388);
		int32_t L_390 = L_389;
		RandomEyesCustomShape_t10BCA0CEC18C2BBF6A8BD7BFAD2AF005D233C354 * L_391 = (L_388)->GetAt(static_cast<il2cpp_array_size_t>(L_390));
		NullCheck(L_391);
		bool L_392 = L_391->get_overrideOn_3();
		if (L_392)
		{
			goto IL_0884;
		}
	}
	{
		RandomEyesCustomShapeU5BU5D_t8FB0C62923194ED52AE0B06A6249680C47CA152C* L_393 = __this->get_customShapes_48();
		int32_t L_394 = __this->get_randomShapeIndex_77();
		NullCheck(L_393);
		int32_t L_395 = L_394;
		RandomEyesCustomShape_t10BCA0CEC18C2BBF6A8BD7BFAD2AF005D233C354 * L_396 = (L_393)->GetAt(static_cast<il2cpp_array_size_t>(L_395));
		NullCheck(L_396);
		bool L_397 = L_396->get_notRandom_4();
		if (L_397)
		{
			goto IL_0884;
		}
	}
	{
		String_t* L_398 = __this->get_randomShape_86();
		float L_399 = __this->get_randomCSBlendSpeedMin_51();
		float L_400 = __this->get_randomCSBlendSpeedMax_52();
		float L_401;
		L_401 = Random_Range_mC15372D42A9ABDCAC3DE82E114D60A40C9C311D2(L_399, L_400, /*hidden argument*/NULL);
		RandomEyes3D_SetCustomShapeBlendSpeed_mE6C02207767F40A39806CEEFB76C1E90FC6B5693(__this, L_398, L_401, /*hidden argument*/NULL);
		String_t* L_402 = __this->get_randomShape_86();
		float L_403 = __this->get_randomCSRangeOfMotionMin_53();
		float L_404 = __this->get_randomCSRangeOfMotionMax_54();
		float L_405;
		L_405 = Random_Range_mC15372D42A9ABDCAC3DE82E114D60A40C9C311D2(L_403, L_404, /*hidden argument*/NULL);
		RandomEyes3D_SetCustomShapeRangeOfMotion_m203068B98D9D262BB4D6B2C8AEECE0875FF4AA14(__this, L_402, L_405, /*hidden argument*/NULL);
		String_t* L_406 = __this->get_randomShape_86();
		float L_407 = __this->get_randomCSDurationMin_55();
		float L_408 = __this->get_randomCSDurationMax_56();
		float L_409;
		L_409 = Random_Range_mC15372D42A9ABDCAC3DE82E114D60A40C9C311D2(L_407, L_408, /*hidden argument*/NULL);
		RandomEyes3D_SetCustomShapeOverride_mA4614D96A02DF336129C3255D4866EC9499AC131(__this, L_406, L_409, /*hidden argument*/NULL);
		float L_410 = __this->get_customShapeTimer_84();
		float L_411 = __this->get_maxCustomShapeInterval_50();
		float L_412;
		L_412 = Random_Range_mC15372D42A9ABDCAC3DE82E114D60A40C9C311D2((0.25f), L_411, /*hidden argument*/NULL);
		__this->set_customShapeTimer_84(((float)il2cpp_codegen_add((float)L_410, (float)L_412)));
	}

IL_0884:
	{
		int32_t L_413 = __this->get_customShapeCount_46();
		__this->set_prevCustomShapeCount_87(L_413);
		bool L_414 = __this->get_randomCustomShapes_49();
		__this->set_prevRandomCustomShapes_85(L_414);
		return;
	}
}
// System.Void CrazyMinnow.SALSA.RandomEyes3D::LookTracking(UnityEngine.Vector3)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void RandomEyes3D_LookTracking_m7F29611D970378663A41B66374456DDAEDCC3205 (RandomEyes3D_t6868157C4605BD0225E1B08301F110E72336CE45 * __this, Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___lookDir0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		__this->set_currentUpVal_70((0.0f));
		__this->set_currentDownVal_73((0.0f));
		__this->set_currentLeftVal_72((0.0f));
		__this->set_currentRightVal_71((0.0f));
		float L_0 = (&___lookDir0)->get_y_3();
		if ((!(((float)L_0) > ((float)(0.0f)))))
		{
			goto IL_00ae;
		}
	}
	{
		float L_1 = (&___lookDir0)->get_y_3();
		float L_2;
		L_2 = fabsf(((float)il2cpp_codegen_add((float)((float)((float)((float)il2cpp_codegen_multiply((float)(-100.0f), (float)((float)il2cpp_codegen_subtract((float)L_1, (float)(0.0f)))))/(float)(1.0f))), (float)(0.0f))));
		float L_3;
		L_3 = fabsf(L_2);
		__this->set_currentUpVal_70(L_3);
		RandomEyesBlendAmounts_t728404128F97998E0D58F3849065DCD32A181621 * L_4 = __this->get_lookAmount_16();
		RandomEyesBlendAmounts_t728404128F97998E0D58F3849065DCD32A181621 * L_5 = __this->get_lookAmount_16();
		NullCheck(L_5);
		float L_6 = L_5->get_lookUp_0();
		float L_7 = __this->get_currentUpVal_70();
		float L_8;
		L_8 = Time_get_deltaTime_mCC15F147DA67F38C74CE408FB5D7FF4A87DA2290(/*hidden argument*/NULL);
		float L_9 = __this->get_blendSpeed_19();
		float L_10;
		L_10 = Mathf_Lerp_m8A2A50B945F42D579EDF44D5EE79E85A4DA59616(L_6, L_7, ((float)il2cpp_codegen_multiply((float)L_8, (float)L_9)), /*hidden argument*/NULL);
		NullCheck(L_4);
		L_4->set_lookUp_0(L_10);
		RandomEyesBlendAmounts_t728404128F97998E0D58F3849065DCD32A181621 * L_11 = __this->get_lookAmount_16();
		RandomEyesBlendAmounts_t728404128F97998E0D58F3849065DCD32A181621 * L_12 = L_11;
		NullCheck(L_12);
		float L_13 = L_12->get_lookDown_1();
		float L_14 = __this->get_blendSpeed_19();
		NullCheck(L_12);
		L_12->set_lookDown_1(((float)il2cpp_codegen_subtract((float)L_13, (float)L_14)));
	}

IL_00ae:
	{
		float L_15 = (&___lookDir0)->get_y_3();
		if ((!(((float)L_15) < ((float)(0.0f)))))
		{
			goto IL_0130;
		}
	}
	{
		float L_16 = (&___lookDir0)->get_y_3();
		float L_17;
		L_17 = fabsf(((float)il2cpp_codegen_add((float)((float)((float)((float)il2cpp_codegen_multiply((float)(-100.0f), (float)((float)il2cpp_codegen_subtract((float)L_16, (float)(0.0f)))))/(float)(1.0f))), (float)(0.0f))));
		float L_18;
		L_18 = fabsf(L_17);
		__this->set_currentDownVal_73(L_18);
		RandomEyesBlendAmounts_t728404128F97998E0D58F3849065DCD32A181621 * L_19 = __this->get_lookAmount_16();
		RandomEyesBlendAmounts_t728404128F97998E0D58F3849065DCD32A181621 * L_20 = __this->get_lookAmount_16();
		NullCheck(L_20);
		float L_21 = L_20->get_lookDown_1();
		float L_22 = __this->get_currentDownVal_73();
		float L_23;
		L_23 = Time_get_deltaTime_mCC15F147DA67F38C74CE408FB5D7FF4A87DA2290(/*hidden argument*/NULL);
		float L_24 = __this->get_blendSpeed_19();
		float L_25;
		L_25 = Mathf_Lerp_m8A2A50B945F42D579EDF44D5EE79E85A4DA59616(L_21, L_22, ((float)il2cpp_codegen_multiply((float)L_23, (float)L_24)), /*hidden argument*/NULL);
		NullCheck(L_19);
		L_19->set_lookDown_1(L_25);
		RandomEyesBlendAmounts_t728404128F97998E0D58F3849065DCD32A181621 * L_26 = __this->get_lookAmount_16();
		RandomEyesBlendAmounts_t728404128F97998E0D58F3849065DCD32A181621 * L_27 = L_26;
		NullCheck(L_27);
		float L_28 = L_27->get_lookUp_0();
		float L_29 = __this->get_blendSpeed_19();
		NullCheck(L_27);
		L_27->set_lookUp_0(((float)il2cpp_codegen_subtract((float)L_28, (float)L_29)));
	}

IL_0130:
	{
		bool L_30 = __this->get_invertTracking_31();
		if (!L_30)
		{
			goto IL_0227;
		}
	}
	{
		float L_31 = (&___lookDir0)->get_x_2();
		if ((!(((float)L_31) < ((float)(0.0f)))))
		{
			goto IL_01ad;
		}
	}
	{
		RandomEyesBlendAmounts_t728404128F97998E0D58F3849065DCD32A181621 * L_32 = __this->get_lookAmount_16();
		float L_33 = (&___lookDir0)->get_x_2();
		float L_34;
		L_34 = fabsf(((float)il2cpp_codegen_add((float)((float)((float)((float)il2cpp_codegen_multiply((float)(-100.0f), (float)((float)il2cpp_codegen_subtract((float)L_33, (float)(0.0f)))))/(float)(1.0f))), (float)(0.0f))));
		NullCheck(L_32);
		L_32->set_lookRight_3(L_34);
		float L_35 = __this->get_currentRightVal_71();
		RandomEyesBlendAmounts_t728404128F97998E0D58F3849065DCD32A181621 * L_36 = __this->get_lookAmount_16();
		NullCheck(L_36);
		float L_37 = L_36->get_lookRight_3();
		float L_38 = __this->get_blendSpeed_19();
		float L_39;
		L_39 = SalsaUtility_Lerp3DEyeTracking_m914ED29E6BF956DD834E8CF6B8C5B20591A22C19(L_35, L_37, L_38, /*hidden argument*/NULL);
		__this->set_currentRightVal_71(L_39);
		float L_40 = __this->get_currentLeftVal_72();
		float L_41 = __this->get_blendSpeed_19();
		__this->set_currentLeftVal_72(((float)il2cpp_codegen_subtract((float)L_40, (float)L_41)));
	}

IL_01ad:
	{
		float L_42 = (&___lookDir0)->get_x_2();
		if ((!(((float)L_42) > ((float)(0.0f)))))
		{
			goto IL_032b;
		}
	}
	{
		RandomEyesBlendAmounts_t728404128F97998E0D58F3849065DCD32A181621 * L_43 = __this->get_lookAmount_16();
		float L_44 = (&___lookDir0)->get_x_2();
		float L_45;
		L_45 = fabsf(((float)il2cpp_codegen_add((float)((float)((float)((float)il2cpp_codegen_multiply((float)(-100.0f), (float)((float)il2cpp_codegen_subtract((float)L_44, (float)(0.0f)))))/(float)(1.0f))), (float)(0.0f))));
		NullCheck(L_43);
		L_43->set_lookLeft_2(L_45);
		float L_46 = __this->get_currentLeftVal_72();
		RandomEyesBlendAmounts_t728404128F97998E0D58F3849065DCD32A181621 * L_47 = __this->get_lookAmount_16();
		NullCheck(L_47);
		float L_48 = L_47->get_lookLeft_2();
		float L_49 = __this->get_blendSpeed_19();
		float L_50;
		L_50 = SalsaUtility_Lerp3DEyeTracking_m914ED29E6BF956DD834E8CF6B8C5B20591A22C19(L_46, L_48, L_49, /*hidden argument*/NULL);
		__this->set_currentLeftVal_72(L_50);
		float L_51 = __this->get_currentRightVal_71();
		float L_52 = __this->get_blendSpeed_19();
		__this->set_currentRightVal_71(((float)il2cpp_codegen_subtract((float)L_51, (float)L_52)));
		goto IL_032b;
	}

IL_0227:
	{
		float L_53 = (&___lookDir0)->get_x_2();
		if ((!(((float)L_53) > ((float)(0.0f)))))
		{
			goto IL_02a9;
		}
	}
	{
		float L_54 = (&___lookDir0)->get_x_2();
		float L_55;
		L_55 = fabsf(((float)il2cpp_codegen_add((float)((float)((float)((float)il2cpp_codegen_multiply((float)(-100.0f), (float)((float)il2cpp_codegen_subtract((float)L_54, (float)(0.0f)))))/(float)(1.0f))), (float)(0.0f))));
		float L_56;
		L_56 = fabsf(L_55);
		__this->set_currentRightVal_71(L_56);
		RandomEyesBlendAmounts_t728404128F97998E0D58F3849065DCD32A181621 * L_57 = __this->get_lookAmount_16();
		RandomEyesBlendAmounts_t728404128F97998E0D58F3849065DCD32A181621 * L_58 = __this->get_lookAmount_16();
		NullCheck(L_58);
		float L_59 = L_58->get_lookRight_3();
		float L_60 = __this->get_currentRightVal_71();
		float L_61;
		L_61 = Time_get_deltaTime_mCC15F147DA67F38C74CE408FB5D7FF4A87DA2290(/*hidden argument*/NULL);
		float L_62 = __this->get_blendSpeed_19();
		float L_63;
		L_63 = Mathf_Lerp_m8A2A50B945F42D579EDF44D5EE79E85A4DA59616(L_59, L_60, ((float)il2cpp_codegen_multiply((float)L_61, (float)L_62)), /*hidden argument*/NULL);
		NullCheck(L_57);
		L_57->set_lookRight_3(L_63);
		RandomEyesBlendAmounts_t728404128F97998E0D58F3849065DCD32A181621 * L_64 = __this->get_lookAmount_16();
		RandomEyesBlendAmounts_t728404128F97998E0D58F3849065DCD32A181621 * L_65 = L_64;
		NullCheck(L_65);
		float L_66 = L_65->get_lookLeft_2();
		float L_67 = __this->get_blendSpeed_19();
		NullCheck(L_65);
		L_65->set_lookLeft_2(((float)il2cpp_codegen_subtract((float)L_66, (float)L_67)));
	}

IL_02a9:
	{
		float L_68 = (&___lookDir0)->get_x_2();
		if ((!(((float)L_68) < ((float)(0.0f)))))
		{
			goto IL_032b;
		}
	}
	{
		float L_69 = (&___lookDir0)->get_x_2();
		float L_70;
		L_70 = fabsf(((float)il2cpp_codegen_add((float)((float)((float)((float)il2cpp_codegen_multiply((float)(-100.0f), (float)((float)il2cpp_codegen_subtract((float)L_69, (float)(0.0f)))))/(float)(1.0f))), (float)(0.0f))));
		float L_71;
		L_71 = fabsf(L_70);
		__this->set_currentLeftVal_72(L_71);
		RandomEyesBlendAmounts_t728404128F97998E0D58F3849065DCD32A181621 * L_72 = __this->get_lookAmount_16();
		RandomEyesBlendAmounts_t728404128F97998E0D58F3849065DCD32A181621 * L_73 = __this->get_lookAmount_16();
		NullCheck(L_73);
		float L_74 = L_73->get_lookLeft_2();
		float L_75 = __this->get_currentLeftVal_72();
		float L_76;
		L_76 = Time_get_deltaTime_mCC15F147DA67F38C74CE408FB5D7FF4A87DA2290(/*hidden argument*/NULL);
		float L_77 = __this->get_blendSpeed_19();
		float L_78;
		L_78 = Mathf_Lerp_m8A2A50B945F42D579EDF44D5EE79E85A4DA59616(L_74, L_75, ((float)il2cpp_codegen_multiply((float)L_76, (float)L_77)), /*hidden argument*/NULL);
		NullCheck(L_72);
		L_72->set_lookLeft_2(L_78);
		RandomEyesBlendAmounts_t728404128F97998E0D58F3849065DCD32A181621 * L_79 = __this->get_lookAmount_16();
		RandomEyesBlendAmounts_t728404128F97998E0D58F3849065DCD32A181621 * L_80 = L_79;
		NullCheck(L_80);
		float L_81 = L_80->get_lookRight_3();
		float L_82 = __this->get_blendSpeed_19();
		NullCheck(L_80);
		L_80->set_lookRight_3(((float)il2cpp_codegen_subtract((float)L_81, (float)L_82)));
	}

IL_032b:
	{
		float L_83 = (&___lookDir0)->get_x_2();
		if ((!(((float)L_83) == ((float)(0.0f)))))
		{
			goto IL_03fd;
		}
	}
	{
		float L_84 = (&___lookDir0)->get_y_3();
		if ((!(((float)L_84) == ((float)(0.0f)))))
		{
			goto IL_03fd;
		}
	}
	{
		RandomEyesBlendAmounts_t728404128F97998E0D58F3849065DCD32A181621 * L_85 = __this->get_lookAmount_16();
		RandomEyesBlendAmounts_t728404128F97998E0D58F3849065DCD32A181621 * L_86 = __this->get_lookAmount_16();
		NullCheck(L_86);
		float L_87 = L_86->get_lookUp_0();
		float L_88;
		L_88 = Time_get_deltaTime_mCC15F147DA67F38C74CE408FB5D7FF4A87DA2290(/*hidden argument*/NULL);
		float L_89 = __this->get_blendSpeed_19();
		float L_90;
		L_90 = Mathf_Lerp_m8A2A50B945F42D579EDF44D5EE79E85A4DA59616(L_87, (0.0f), ((float)il2cpp_codegen_multiply((float)L_88, (float)L_89)), /*hidden argument*/NULL);
		NullCheck(L_85);
		L_85->set_lookUp_0(L_90);
		RandomEyesBlendAmounts_t728404128F97998E0D58F3849065DCD32A181621 * L_91 = __this->get_lookAmount_16();
		RandomEyesBlendAmounts_t728404128F97998E0D58F3849065DCD32A181621 * L_92 = __this->get_lookAmount_16();
		NullCheck(L_92);
		float L_93 = L_92->get_lookDown_1();
		float L_94;
		L_94 = Time_get_deltaTime_mCC15F147DA67F38C74CE408FB5D7FF4A87DA2290(/*hidden argument*/NULL);
		float L_95 = __this->get_blendSpeed_19();
		float L_96;
		L_96 = Mathf_Lerp_m8A2A50B945F42D579EDF44D5EE79E85A4DA59616(L_93, (0.0f), ((float)il2cpp_codegen_multiply((float)L_94, (float)L_95)), /*hidden argument*/NULL);
		NullCheck(L_91);
		L_91->set_lookDown_1(L_96);
		RandomEyesBlendAmounts_t728404128F97998E0D58F3849065DCD32A181621 * L_97 = __this->get_lookAmount_16();
		RandomEyesBlendAmounts_t728404128F97998E0D58F3849065DCD32A181621 * L_98 = __this->get_lookAmount_16();
		NullCheck(L_98);
		float L_99 = L_98->get_lookLeft_2();
		float L_100;
		L_100 = Time_get_deltaTime_mCC15F147DA67F38C74CE408FB5D7FF4A87DA2290(/*hidden argument*/NULL);
		float L_101 = __this->get_blendSpeed_19();
		float L_102;
		L_102 = Mathf_Lerp_m8A2A50B945F42D579EDF44D5EE79E85A4DA59616(L_99, (0.0f), ((float)il2cpp_codegen_multiply((float)L_100, (float)L_101)), /*hidden argument*/NULL);
		NullCheck(L_97);
		L_97->set_lookLeft_2(L_102);
		RandomEyesBlendAmounts_t728404128F97998E0D58F3849065DCD32A181621 * L_103 = __this->get_lookAmount_16();
		RandomEyesBlendAmounts_t728404128F97998E0D58F3849065DCD32A181621 * L_104 = __this->get_lookAmount_16();
		NullCheck(L_104);
		float L_105 = L_104->get_lookRight_3();
		float L_106;
		L_106 = Time_get_deltaTime_mCC15F147DA67F38C74CE408FB5D7FF4A87DA2290(/*hidden argument*/NULL);
		float L_107 = __this->get_blendSpeed_19();
		float L_108;
		L_108 = Mathf_Lerp_m8A2A50B945F42D579EDF44D5EE79E85A4DA59616(L_105, (0.0f), ((float)il2cpp_codegen_multiply((float)L_106, (float)L_107)), /*hidden argument*/NULL);
		NullCheck(L_103);
		L_103->set_lookRight_3(L_108);
	}

IL_03fd:
	{
		RandomEyesBlendAmounts_t728404128F97998E0D58F3849065DCD32A181621 * L_109 = __this->get_lookAmount_16();
		RandomEyesBlendAmounts_t728404128F97998E0D58F3849065DCD32A181621 * L_110 = __this->get_lookAmount_16();
		NullCheck(L_110);
		float L_111 = L_110->get_lookUp_0();
		float L_112 = __this->get_rangeOfMotion_20();
		float L_113;
		L_113 = Mathf_Clamp_m2416F3B785C8F135863E3D17E5B0CB4174797B87(L_111, (0.0f), L_112, /*hidden argument*/NULL);
		NullCheck(L_109);
		L_109->set_lookUp_0(L_113);
		RandomEyesBlendAmounts_t728404128F97998E0D58F3849065DCD32A181621 * L_114 = __this->get_lookAmount_16();
		RandomEyesBlendAmounts_t728404128F97998E0D58F3849065DCD32A181621 * L_115 = __this->get_lookAmount_16();
		NullCheck(L_115);
		float L_116 = L_115->get_lookDown_1();
		float L_117 = __this->get_rangeOfMotion_20();
		float L_118;
		L_118 = Mathf_Clamp_m2416F3B785C8F135863E3D17E5B0CB4174797B87(L_116, (0.0f), L_117, /*hidden argument*/NULL);
		NullCheck(L_114);
		L_114->set_lookDown_1(L_118);
		RandomEyesBlendAmounts_t728404128F97998E0D58F3849065DCD32A181621 * L_119 = __this->get_lookAmount_16();
		RandomEyesBlendAmounts_t728404128F97998E0D58F3849065DCD32A181621 * L_120 = __this->get_lookAmount_16();
		NullCheck(L_120);
		float L_121 = L_120->get_lookRight_3();
		float L_122 = __this->get_rangeOfMotion_20();
		float L_123;
		L_123 = Mathf_Clamp_m2416F3B785C8F135863E3D17E5B0CB4174797B87(L_121, (0.0f), L_122, /*hidden argument*/NULL);
		NullCheck(L_119);
		L_119->set_lookRight_3(L_123);
		RandomEyesBlendAmounts_t728404128F97998E0D58F3849065DCD32A181621 * L_124 = __this->get_lookAmount_16();
		RandomEyesBlendAmounts_t728404128F97998E0D58F3849065DCD32A181621 * L_125 = __this->get_lookAmount_16();
		NullCheck(L_125);
		float L_126 = L_125->get_lookLeft_2();
		float L_127 = __this->get_rangeOfMotion_20();
		float L_128;
		L_128 = Mathf_Clamp_m2416F3B785C8F135863E3D17E5B0CB4174797B87(L_126, (0.0f), L_127, /*hidden argument*/NULL);
		NullCheck(L_124);
		L_124->set_lookLeft_2(L_128);
		SkinnedMeshRenderer_t126F4D6010E0F4B2685A7817B0A9171805D8F496 * L_129 = __this->get_skinnedMeshRenderer_6();
		IL2CPP_RUNTIME_CLASS_INIT(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		bool L_130;
		L_130 = Object_op_Implicit_mC8214E4F028CC2F036CC82BDB81D102A02893499(L_129, /*hidden argument*/NULL);
		if (!L_130)
		{
			goto IL_0512;
		}
	}
	{
		SkinnedMeshRenderer_t126F4D6010E0F4B2685A7817B0A9171805D8F496 * L_131 = __this->get_skinnedMeshRenderer_6();
		int32_t L_132 = __this->get_lookUpIndex_10();
		RandomEyesBlendAmounts_t728404128F97998E0D58F3849065DCD32A181621 * L_133 = __this->get_lookAmount_16();
		NullCheck(L_133);
		float L_134 = L_133->get_lookUp_0();
		NullCheck(L_131);
		SkinnedMeshRenderer_SetBlendShapeWeight_mF546F3567C5039C217AD1E32157B992B4124B5FD(L_131, L_132, L_134, /*hidden argument*/NULL);
		SkinnedMeshRenderer_t126F4D6010E0F4B2685A7817B0A9171805D8F496 * L_135 = __this->get_skinnedMeshRenderer_6();
		int32_t L_136 = __this->get_lookDownIndex_11();
		RandomEyesBlendAmounts_t728404128F97998E0D58F3849065DCD32A181621 * L_137 = __this->get_lookAmount_16();
		NullCheck(L_137);
		float L_138 = L_137->get_lookDown_1();
		NullCheck(L_135);
		SkinnedMeshRenderer_SetBlendShapeWeight_mF546F3567C5039C217AD1E32157B992B4124B5FD(L_135, L_136, L_138, /*hidden argument*/NULL);
		SkinnedMeshRenderer_t126F4D6010E0F4B2685A7817B0A9171805D8F496 * L_139 = __this->get_skinnedMeshRenderer_6();
		int32_t L_140 = __this->get_lookRightIndex_13();
		RandomEyesBlendAmounts_t728404128F97998E0D58F3849065DCD32A181621 * L_141 = __this->get_lookAmount_16();
		NullCheck(L_141);
		float L_142 = L_141->get_lookRight_3();
		NullCheck(L_139);
		SkinnedMeshRenderer_SetBlendShapeWeight_mF546F3567C5039C217AD1E32157B992B4124B5FD(L_139, L_140, L_142, /*hidden argument*/NULL);
		SkinnedMeshRenderer_t126F4D6010E0F4B2685A7817B0A9171805D8F496 * L_143 = __this->get_skinnedMeshRenderer_6();
		int32_t L_144 = __this->get_lookLeftIndex_12();
		RandomEyesBlendAmounts_t728404128F97998E0D58F3849065DCD32A181621 * L_145 = __this->get_lookAmount_16();
		NullCheck(L_145);
		float L_146 = L_145->get_lookLeft_2();
		NullCheck(L_143);
		SkinnedMeshRenderer_SetBlendShapeWeight_mF546F3567C5039C217AD1E32157B992B4124B5FD(L_143, L_144, L_146, /*hidden argument*/NULL);
	}

IL_0512:
	{
		return;
	}
}
// System.Collections.IEnumerator CrazyMinnow.SALSA.RandomEyes3D::TargetAffinityUpdate()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* RandomEyes3D_TargetAffinityUpdate_mF7439C88C6DF7E156DBF3E40D67C57EC64EF3E77 (RandomEyes3D_t6868157C4605BD0225E1B08301F110E72336CE45 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CTargetAffinityUpdateU3Ed__0_tCF892F05CB3FC0D3FC4F3984F53D8C429D011013_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	U3CTargetAffinityUpdateU3Ed__0_tCF892F05CB3FC0D3FC4F3984F53D8C429D011013 * V_0 = NULL;
	{
		U3CTargetAffinityUpdateU3Ed__0_tCF892F05CB3FC0D3FC4F3984F53D8C429D011013 * L_0 = (U3CTargetAffinityUpdateU3Ed__0_tCF892F05CB3FC0D3FC4F3984F53D8C429D011013 *)il2cpp_codegen_object_new(U3CTargetAffinityUpdateU3Ed__0_tCF892F05CB3FC0D3FC4F3984F53D8C429D011013_il2cpp_TypeInfo_var);
		U3CTargetAffinityUpdateU3Ed__0__ctor_mCAFDF47555144A310E4F743A29A8C68AAAA8FDFB(L_0, 0, /*hidden argument*/NULL);
		V_0 = L_0;
		U3CTargetAffinityUpdateU3Ed__0_tCF892F05CB3FC0D3FC4F3984F53D8C429D011013 * L_1 = V_0;
		NullCheck(L_1);
		L_1->set_U3CU3E4__this_2(__this);
		U3CTargetAffinityUpdateU3Ed__0_tCF892F05CB3FC0D3FC4F3984F53D8C429D011013 * L_2 = V_0;
		return L_2;
	}
}
// System.Void CrazyMinnow.SALSA.RandomEyes3D::DisableAllCustomShapes()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void RandomEyes3D_DisableAllCustomShapes_mDA8F4E0E351329FAD3ACC8BD9E13D3D70D0C44CD (RandomEyes3D_t6868157C4605BD0225E1B08301F110E72336CE45 * __this, const RuntimeMethod* method)
{
	int32_t V_0 = 0;
	{
		RandomEyesCustomShapeU5BU5D_t8FB0C62923194ED52AE0B06A6249680C47CA152C* L_0 = __this->get_customShapes_48();
		if (!L_0)
		{
			goto IL_0029;
		}
	}
	{
		V_0 = 0;
		goto IL_001e;
	}

IL_000c:
	{
		RandomEyesCustomShapeU5BU5D_t8FB0C62923194ED52AE0B06A6249680C47CA152C* L_1 = __this->get_customShapes_48();
		int32_t L_2 = V_0;
		NullCheck(L_1);
		int32_t L_3 = L_2;
		RandomEyesCustomShape_t10BCA0CEC18C2BBF6A8BD7BFAD2AF005D233C354 * L_4 = (L_1)->GetAt(static_cast<il2cpp_array_size_t>(L_3));
		NullCheck(L_4);
		L_4->set_overrideOn_3((bool)0);
		int32_t L_5 = V_0;
		V_0 = ((int32_t)il2cpp_codegen_add((int32_t)L_5, (int32_t)1));
	}

IL_001e:
	{
		int32_t L_6 = V_0;
		RandomEyesCustomShapeU5BU5D_t8FB0C62923194ED52AE0B06A6249680C47CA152C* L_7 = __this->get_customShapes_48();
		NullCheck(L_7);
		if ((((int32_t)L_6) < ((int32_t)((int32_t)((int32_t)(((RuntimeArray*)L_7)->max_length))))))
		{
			goto IL_000c;
		}
	}

IL_0029:
	{
		return;
	}
}
// System.Collections.IEnumerator CrazyMinnow.SALSA.RandomEyes3D::BlinkEyes(System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* RandomEyes3D_BlinkEyes_mDFDFEB7B2811CA1009B0A4D55E6E71CD4433ED6A (RandomEyes3D_t6868157C4605BD0225E1B08301F110E72336CE45 * __this, float ___duration0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CBlinkEyesU3Ed__2_t05DA28F00D8E1A9E64D0728065DB53DA96232044_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	U3CBlinkEyesU3Ed__2_t05DA28F00D8E1A9E64D0728065DB53DA96232044 * V_0 = NULL;
	{
		U3CBlinkEyesU3Ed__2_t05DA28F00D8E1A9E64D0728065DB53DA96232044 * L_0 = (U3CBlinkEyesU3Ed__2_t05DA28F00D8E1A9E64D0728065DB53DA96232044 *)il2cpp_codegen_object_new(U3CBlinkEyesU3Ed__2_t05DA28F00D8E1A9E64D0728065DB53DA96232044_il2cpp_TypeInfo_var);
		U3CBlinkEyesU3Ed__2__ctor_m1C070BB710ACF1198445B49A02B0EE406A56F50C(L_0, 0, /*hidden argument*/NULL);
		V_0 = L_0;
		U3CBlinkEyesU3Ed__2_t05DA28F00D8E1A9E64D0728065DB53DA96232044 * L_1 = V_0;
		NullCheck(L_1);
		L_1->set_U3CU3E4__this_2(__this);
		U3CBlinkEyesU3Ed__2_t05DA28F00D8E1A9E64D0728065DB53DA96232044 * L_2 = V_0;
		float L_3 = ___duration0;
		NullCheck(L_2);
		L_2->set_duration_3(L_3);
		U3CBlinkEyesU3Ed__2_t05DA28F00D8E1A9E64D0728065DB53DA96232044 * L_4 = V_0;
		return L_4;
	}
}
// System.Void CrazyMinnow.SALSA.RandomEyes3D::SetCustomShapesAllNotRandom(System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void RandomEyes3D_SetCustomShapesAllNotRandom_m6E09D075DA414527EC49BE94F6FE650C144D8587 (RandomEyes3D_t6868157C4605BD0225E1B08301F110E72336CE45 * __this, bool ___status0, const RuntimeMethod* method)
{
	int32_t V_0 = 0;
	{
		V_0 = 0;
		goto IL_0016;
	}

IL_0004:
	{
		RandomEyesCustomShapeU5BU5D_t8FB0C62923194ED52AE0B06A6249680C47CA152C* L_0 = __this->get_customShapes_48();
		int32_t L_1 = V_0;
		NullCheck(L_0);
		int32_t L_2 = L_1;
		RandomEyesCustomShape_t10BCA0CEC18C2BBF6A8BD7BFAD2AF005D233C354 * L_3 = (L_0)->GetAt(static_cast<il2cpp_array_size_t>(L_2));
		bool L_4 = ___status0;
		NullCheck(L_3);
		L_3->set_notRandom_4(L_4);
		int32_t L_5 = V_0;
		V_0 = ((int32_t)il2cpp_codegen_add((int32_t)L_5, (int32_t)1));
	}

IL_0016:
	{
		int32_t L_6 = V_0;
		RandomEyesCustomShapeU5BU5D_t8FB0C62923194ED52AE0B06A6249680C47CA152C* L_7 = __this->get_customShapes_48();
		NullCheck(L_7);
		if ((((int32_t)L_6) < ((int32_t)((int32_t)((int32_t)(((RuntimeArray*)L_7)->max_length))))))
		{
			goto IL_0004;
		}
	}
	{
		bool L_8 = ___status0;
		__this->set_csNotRandomToggle_57(L_8);
		return;
	}
}
// System.Collections.IEnumerator CrazyMinnow.SALSA.RandomEyes3D::SetCustomShapeOverrideDuration(System.String,System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* RandomEyes3D_SetCustomShapeOverrideDuration_m0A72CCB00C2DB1FBFFE2A7CBB1B85685B0EA52CC (RandomEyes3D_t6868157C4605BD0225E1B08301F110E72336CE45 * __this, String_t* ___shapeName0, float ___duration1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CSetCustomShapeOverrideDurationU3Ed__4_tA42191F3DEC3EBAD53A277EAC5C346C81A3BC122_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	U3CSetCustomShapeOverrideDurationU3Ed__4_tA42191F3DEC3EBAD53A277EAC5C346C81A3BC122 * V_0 = NULL;
	{
		U3CSetCustomShapeOverrideDurationU3Ed__4_tA42191F3DEC3EBAD53A277EAC5C346C81A3BC122 * L_0 = (U3CSetCustomShapeOverrideDurationU3Ed__4_tA42191F3DEC3EBAD53A277EAC5C346C81A3BC122 *)il2cpp_codegen_object_new(U3CSetCustomShapeOverrideDurationU3Ed__4_tA42191F3DEC3EBAD53A277EAC5C346C81A3BC122_il2cpp_TypeInfo_var);
		U3CSetCustomShapeOverrideDurationU3Ed__4__ctor_mB66C5B43F2C15F540F85F1845A71DB41035E6FAD(L_0, 0, /*hidden argument*/NULL);
		V_0 = L_0;
		U3CSetCustomShapeOverrideDurationU3Ed__4_tA42191F3DEC3EBAD53A277EAC5C346C81A3BC122 * L_1 = V_0;
		NullCheck(L_1);
		L_1->set_U3CU3E4__this_2(__this);
		U3CSetCustomShapeOverrideDurationU3Ed__4_tA42191F3DEC3EBAD53A277EAC5C346C81A3BC122 * L_2 = V_0;
		String_t* L_3 = ___shapeName0;
		NullCheck(L_2);
		L_2->set_shapeName_3(L_3);
		U3CSetCustomShapeOverrideDurationU3Ed__4_tA42191F3DEC3EBAD53A277EAC5C346C81A3BC122 * L_4 = V_0;
		float L_5 = ___duration1;
		NullCheck(L_4);
		L_4->set_duration_4(L_5);
		U3CSetCustomShapeOverrideDurationU3Ed__4_tA42191F3DEC3EBAD53A277EAC5C346C81A3BC122 * L_6 = V_0;
		return L_6;
	}
}
// System.Collections.IEnumerator CrazyMinnow.SALSA.RandomEyes3D::SetCustomShapeOverrideDuration(System.Int32,System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* RandomEyes3D_SetCustomShapeOverrideDuration_m07722506C2734D00B002B64D6BD1FE3EE9908F69 (RandomEyes3D_t6868157C4605BD0225E1B08301F110E72336CE45 * __this, int32_t ___shapeIndex0, float ___duration1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CSetCustomShapeOverrideDurationU3Ed__6_t3067670AF5E03DD7899B8F7492C6EF59E5B77DB0_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	U3CSetCustomShapeOverrideDurationU3Ed__6_t3067670AF5E03DD7899B8F7492C6EF59E5B77DB0 * V_0 = NULL;
	{
		U3CSetCustomShapeOverrideDurationU3Ed__6_t3067670AF5E03DD7899B8F7492C6EF59E5B77DB0 * L_0 = (U3CSetCustomShapeOverrideDurationU3Ed__6_t3067670AF5E03DD7899B8F7492C6EF59E5B77DB0 *)il2cpp_codegen_object_new(U3CSetCustomShapeOverrideDurationU3Ed__6_t3067670AF5E03DD7899B8F7492C6EF59E5B77DB0_il2cpp_TypeInfo_var);
		U3CSetCustomShapeOverrideDurationU3Ed__6__ctor_m90B95A85DE3597BE73729C4BD7FF6D9756F8D6EB(L_0, 0, /*hidden argument*/NULL);
		V_0 = L_0;
		U3CSetCustomShapeOverrideDurationU3Ed__6_t3067670AF5E03DD7899B8F7492C6EF59E5B77DB0 * L_1 = V_0;
		NullCheck(L_1);
		L_1->set_U3CU3E4__this_2(__this);
		U3CSetCustomShapeOverrideDurationU3Ed__6_t3067670AF5E03DD7899B8F7492C6EF59E5B77DB0 * L_2 = V_0;
		int32_t L_3 = ___shapeIndex0;
		NullCheck(L_2);
		L_2->set_shapeIndex_3(L_3);
		U3CSetCustomShapeOverrideDurationU3Ed__6_t3067670AF5E03DD7899B8F7492C6EF59E5B77DB0 * L_4 = V_0;
		float L_5 = ___duration1;
		NullCheck(L_4);
		L_4->set_duration_4(L_5);
		U3CSetCustomShapeOverrideDurationU3Ed__6_t3067670AF5E03DD7899B8F7492C6EF59E5B77DB0 * L_6 = V_0;
		return L_6;
	}
}
// System.Collections.IEnumerator CrazyMinnow.SALSA.RandomEyes3D::SetCustomShapeDuration(System.String,System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* RandomEyes3D_SetCustomShapeDuration_mC9EFA2C619003EB530936AF8AC02019814A65099 (RandomEyes3D_t6868157C4605BD0225E1B08301F110E72336CE45 * __this, String_t* ___shapeName0, float ___duration1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CSetCustomShapeDurationU3Ed__8_t1CD4174D8597ACF6A20E2146BB78651E84AE273B_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	U3CSetCustomShapeDurationU3Ed__8_t1CD4174D8597ACF6A20E2146BB78651E84AE273B * V_0 = NULL;
	{
		U3CSetCustomShapeDurationU3Ed__8_t1CD4174D8597ACF6A20E2146BB78651E84AE273B * L_0 = (U3CSetCustomShapeDurationU3Ed__8_t1CD4174D8597ACF6A20E2146BB78651E84AE273B *)il2cpp_codegen_object_new(U3CSetCustomShapeDurationU3Ed__8_t1CD4174D8597ACF6A20E2146BB78651E84AE273B_il2cpp_TypeInfo_var);
		U3CSetCustomShapeDurationU3Ed__8__ctor_mF6CD6A348AAE88971E18E1E4E94D43FF9A964EB3(L_0, 0, /*hidden argument*/NULL);
		V_0 = L_0;
		U3CSetCustomShapeDurationU3Ed__8_t1CD4174D8597ACF6A20E2146BB78651E84AE273B * L_1 = V_0;
		NullCheck(L_1);
		L_1->set_U3CU3E4__this_2(__this);
		U3CSetCustomShapeDurationU3Ed__8_t1CD4174D8597ACF6A20E2146BB78651E84AE273B * L_2 = V_0;
		String_t* L_3 = ___shapeName0;
		NullCheck(L_2);
		L_2->set_shapeName_3(L_3);
		U3CSetCustomShapeDurationU3Ed__8_t1CD4174D8597ACF6A20E2146BB78651E84AE273B * L_4 = V_0;
		float L_5 = ___duration1;
		NullCheck(L_4);
		L_4->set_duration_4(L_5);
		U3CSetCustomShapeDurationU3Ed__8_t1CD4174D8597ACF6A20E2146BB78651E84AE273B * L_6 = V_0;
		return L_6;
	}
}
// System.Collections.IEnumerator CrazyMinnow.SALSA.RandomEyes3D::SetCustomShapeDuration(System.Int32,System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* RandomEyes3D_SetCustomShapeDuration_m3B9A380330E625EA5EB3E911590D6CEF499B6F53 (RandomEyes3D_t6868157C4605BD0225E1B08301F110E72336CE45 * __this, int32_t ___shapeIndex0, float ___duration1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CSetCustomShapeDurationU3Ed__a_tF3A147C5E712D0581C2B12133986348BFBB67140_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	U3CSetCustomShapeDurationU3Ed__a_tF3A147C5E712D0581C2B12133986348BFBB67140 * V_0 = NULL;
	{
		U3CSetCustomShapeDurationU3Ed__a_tF3A147C5E712D0581C2B12133986348BFBB67140 * L_0 = (U3CSetCustomShapeDurationU3Ed__a_tF3A147C5E712D0581C2B12133986348BFBB67140 *)il2cpp_codegen_object_new(U3CSetCustomShapeDurationU3Ed__a_tF3A147C5E712D0581C2B12133986348BFBB67140_il2cpp_TypeInfo_var);
		U3CSetCustomShapeDurationU3Ed__a__ctor_m22ECCEFDEC5DB0D1080451AE79B9E22EC5A618EB(L_0, 0, /*hidden argument*/NULL);
		V_0 = L_0;
		U3CSetCustomShapeDurationU3Ed__a_tF3A147C5E712D0581C2B12133986348BFBB67140 * L_1 = V_0;
		NullCheck(L_1);
		L_1->set_U3CU3E4__this_2(__this);
		U3CSetCustomShapeDurationU3Ed__a_tF3A147C5E712D0581C2B12133986348BFBB67140 * L_2 = V_0;
		int32_t L_3 = ___shapeIndex0;
		NullCheck(L_2);
		L_2->set_shapeIndex_3(L_3);
		U3CSetCustomShapeDurationU3Ed__a_tF3A147C5E712D0581C2B12133986348BFBB67140 * L_4 = V_0;
		float L_5 = ___duration1;
		NullCheck(L_4);
		L_4->set_duration_4(L_5);
		U3CSetCustomShapeDurationU3Ed__a_tF3A147C5E712D0581C2B12133986348BFBB67140 * L_6 = V_0;
		return L_6;
	}
}
// System.Void CrazyMinnow.SALSA.RandomEyes3D::CustomShapeChanged(CrazyMinnow.SALSA.RandomEyesCustomShapeStatus)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void RandomEyes3D_CustomShapeChanged_m380A0E30479AFAD7D59CA1222BB6E77DCD1FF068 (RandomEyes3D_t6868157C4605BD0225E1B08301F110E72336CE45 * __this, RandomEyesCustomShapeStatus_tAE795B1C1EF38DFDE0535D6A919E25A96283DD66 * ___customShape0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralDE61B9AE03FF764AE619F4C4A8F720E843928341);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	{
		GameObjectU5BU5D_tA88FC1A1FC9D4D73D0B3984D4B0ECE88F4C47642* L_0 = __this->get_broadcastCSReceivers_40();
		NullCheck(L_0);
		if ((((int32_t)((int32_t)((int32_t)(((RuntimeArray*)L_0)->max_length)))) <= ((int32_t)0)))
		{
			goto IL_0062;
		}
	}
	{
		bool L_1 = __this->get_propagateToChildrenCS_41();
		if (!L_1)
		{
			goto IL_003b;
		}
	}
	{
		V_0 = 0;
		goto IL_002f;
	}

IL_0017:
	{
		GameObjectU5BU5D_tA88FC1A1FC9D4D73D0B3984D4B0ECE88F4C47642* L_2 = __this->get_broadcastCSReceivers_40();
		int32_t L_3 = V_0;
		NullCheck(L_2);
		int32_t L_4 = L_3;
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_5 = (L_2)->GetAt(static_cast<il2cpp_array_size_t>(L_4));
		RandomEyesCustomShapeStatus_tAE795B1C1EF38DFDE0535D6A919E25A96283DD66 * L_6 = ___customShape0;
		NullCheck(L_5);
		GameObject_BroadcastMessage_m2B5D6163ABB0ED80A381A41DC84ED48CC10212AD(L_5, _stringLiteralDE61B9AE03FF764AE619F4C4A8F720E843928341, L_6, 1, /*hidden argument*/NULL);
		int32_t L_7 = V_0;
		V_0 = ((int32_t)il2cpp_codegen_add((int32_t)L_7, (int32_t)1));
	}

IL_002f:
	{
		int32_t L_8 = V_0;
		GameObjectU5BU5D_tA88FC1A1FC9D4D73D0B3984D4B0ECE88F4C47642* L_9 = __this->get_broadcastCSReceivers_40();
		NullCheck(L_9);
		if ((((int32_t)L_8) < ((int32_t)((int32_t)((int32_t)(((RuntimeArray*)L_9)->max_length))))))
		{
			goto IL_0017;
		}
	}
	{
		return;
	}

IL_003b:
	{
		V_1 = 0;
		goto IL_0057;
	}

IL_003f:
	{
		GameObjectU5BU5D_tA88FC1A1FC9D4D73D0B3984D4B0ECE88F4C47642* L_10 = __this->get_broadcastCSReceivers_40();
		int32_t L_11 = V_1;
		NullCheck(L_10);
		int32_t L_12 = L_11;
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_13 = (L_10)->GetAt(static_cast<il2cpp_array_size_t>(L_12));
		RandomEyesCustomShapeStatus_tAE795B1C1EF38DFDE0535D6A919E25A96283DD66 * L_14 = ___customShape0;
		NullCheck(L_13);
		GameObject_SendMessage_mD49CCADA51268480B585733DD7C6540CCCC6EF5C(L_13, _stringLiteralDE61B9AE03FF764AE619F4C4A8F720E843928341, L_14, 1, /*hidden argument*/NULL);
		int32_t L_15 = V_1;
		V_1 = ((int32_t)il2cpp_codegen_add((int32_t)L_15, (int32_t)1));
	}

IL_0057:
	{
		int32_t L_16 = V_1;
		GameObjectU5BU5D_tA88FC1A1FC9D4D73D0B3984D4B0ECE88F4C47642* L_17 = __this->get_broadcastCSReceivers_40();
		NullCheck(L_17);
		if ((((int32_t)L_16) < ((int32_t)((int32_t)((int32_t)(((RuntimeArray*)L_17)->max_length))))))
		{
			goto IL_003f;
		}
	}

IL_0062:
	{
		return;
	}
}
// System.Void CrazyMinnow.SALSA.RandomEyes3D::SetCustomShapeExcludeEyes(System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void RandomEyes3D_SetCustomShapeExcludeEyes_m2907E3D9B857200BD4368EB62CCC3A0B4D51C436 (RandomEyes3D_t6868157C4605BD0225E1B08301F110E72336CE45 * __this, bool ___status0, const RuntimeMethod* method)
{
	{
		bool L_0 = ___status0;
		__this->set_excludeEyeShapes_44(L_0);
		return;
	}
}
// System.Void CrazyMinnow.SALSA.RandomEyes3D::SetCustomShapeExcludeMouth(CrazyMinnow.SALSA.Salsa3D)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void RandomEyes3D_SetCustomShapeExcludeMouth_m3063985221F372102DC1B3854A31D75F95655684 (RandomEyes3D_t6868157C4605BD0225E1B08301F110E72336CE45 * __this, Salsa3D_tACF83D529139263662FEAAC8F5B53EC43B140C9C * ___salsa3D0, const RuntimeMethod* method)
{
	{
		Salsa3D_tACF83D529139263662FEAAC8F5B53EC43B140C9C * L_0 = ___salsa3D0;
		__this->set_salsa3D_5(L_0);
		return;
	}
}
// System.Void CrazyMinnow.SALSA.RandomEyes3D::SetRangeOfMotion(System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void RandomEyes3D_SetRangeOfMotion_m284CEE173D95F6A663474206D141963CDDAFAB3C (RandomEyes3D_t6868157C4605BD0225E1B08301F110E72336CE45 * __this, float ___range0, const RuntimeMethod* method)
{
	{
		float L_0 = ___range0;
		float L_1;
		L_1 = SalsaUtility_ClampValue_m167E1BE774D495060E1FAA2503DD01D2C00EE208(L_0, (100.0f), /*hidden argument*/NULL);
		__this->set_rangeOfMotion_20(L_1);
		return;
	}
}
// System.Void CrazyMinnow.SALSA.RandomEyes3D::SetBlendSpeed(System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void RandomEyes3D_SetBlendSpeed_m6FAD94D8045D839F3E0B9D49FE88591FD72610E9 (RandomEyes3D_t6868157C4605BD0225E1B08301F110E72336CE45 * __this, float ___speed0, const RuntimeMethod* method)
{
	{
		float L_0 = ___speed0;
		float L_1;
		L_1 = SalsaUtility_ClampValue_m167E1BE774D495060E1FAA2503DD01D2C00EE208(L_0, (100.0f), /*hidden argument*/NULL);
		__this->set_blendSpeed_19(L_1);
		return;
	}
}
// System.Void CrazyMinnow.SALSA.RandomEyes3D::Blink(System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void RandomEyes3D_Blink_mCCC507CFFA092075E2BFAF242D716F81A073771C (RandomEyes3D_t6868157C4605BD0225E1B08301F110E72336CE45 * __this, float ___duration0, const RuntimeMethod* method)
{
	{
		bool L_0 = __this->get_inBlinkSequence_81();
		if (L_0)
		{
			goto IL_0016;
		}
	}
	{
		float L_1 = ___duration0;
		RuntimeObject* L_2;
		L_2 = RandomEyes3D_DoBlink_m82BC786A5A89543456B234AAD2FC795DD86698FC(__this, L_1, /*hidden argument*/NULL);
		Coroutine_t899D5232EF542CB8BA70AF9ECEECA494FAA9CCB7 * L_3;
		L_3 = MonoBehaviour_StartCoroutine_m3E33706D38B23CDD179E99BAD61E32303E9CC719(__this, L_2, /*hidden argument*/NULL);
	}

IL_0016:
	{
		return;
	}
}
// System.Collections.IEnumerator CrazyMinnow.SALSA.RandomEyes3D::DoBlink(System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* RandomEyes3D_DoBlink_m82BC786A5A89543456B234AAD2FC795DD86698FC (RandomEyes3D_t6868157C4605BD0225E1B08301F110E72336CE45 * __this, float ___duration0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CDoBlinkU3Ed__c_t8CF4EE9B667E494EE34FBB05C5E960320E5C8418_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	U3CDoBlinkU3Ed__c_t8CF4EE9B667E494EE34FBB05C5E960320E5C8418 * V_0 = NULL;
	{
		U3CDoBlinkU3Ed__c_t8CF4EE9B667E494EE34FBB05C5E960320E5C8418 * L_0 = (U3CDoBlinkU3Ed__c_t8CF4EE9B667E494EE34FBB05C5E960320E5C8418 *)il2cpp_codegen_object_new(U3CDoBlinkU3Ed__c_t8CF4EE9B667E494EE34FBB05C5E960320E5C8418_il2cpp_TypeInfo_var);
		U3CDoBlinkU3Ed__c__ctor_m9D9176B98A436D63C7C71A253F9242C3ACD37F00(L_0, 0, /*hidden argument*/NULL);
		V_0 = L_0;
		U3CDoBlinkU3Ed__c_t8CF4EE9B667E494EE34FBB05C5E960320E5C8418 * L_1 = V_0;
		NullCheck(L_1);
		L_1->set_U3CU3E4__this_2(__this);
		U3CDoBlinkU3Ed__c_t8CF4EE9B667E494EE34FBB05C5E960320E5C8418 * L_2 = V_0;
		float L_3 = ___duration0;
		NullCheck(L_2);
		L_2->set_duration_3(L_3);
		U3CDoBlinkU3Ed__c_t8CF4EE9B667E494EE34FBB05C5E960320E5C8418 * L_4 = V_0;
		return L_4;
	}
}
// System.Void CrazyMinnow.SALSA.RandomEyes3D::SetLookTargetTracking(UnityEngine.GameObject)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void RandomEyes3D_SetLookTargetTracking_m8DC063699D644B556133DD4013E7BB99C95572EC (RandomEyes3D_t6868157C4605BD0225E1B08301F110E72336CE45 * __this, GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * ___obj0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_0 = ___obj0;
		IL2CPP_RUNTIME_CLASS_INIT(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		bool L_1;
		L_1 = Object_op_Implicit_mC8214E4F028CC2F036CC82BDB81D102A02893499(L_0, /*hidden argument*/NULL);
		if (L_1)
		{
			goto IL_001d;
		}
	}
	{
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_2 = ___obj0;
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_3 = __this->get_lookTargetTracking_28();
		IL2CPP_RUNTIME_CLASS_INIT(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		bool L_4;
		L_4 = Object_op_Inequality_mE1F187520BD83FB7D86A6D850710C4D42B864E90(L_2, L_3, /*hidden argument*/NULL);
		if (!L_4)
		{
			goto IL_001d;
		}
	}
	{
		__this->set_lookTargetTracking_28((GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 *)NULL);
	}

IL_001d:
	{
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_5 = ___obj0;
		IL2CPP_RUNTIME_CLASS_INIT(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		bool L_6;
		L_6 = Object_op_Implicit_mC8214E4F028CC2F036CC82BDB81D102A02893499(L_5, /*hidden argument*/NULL);
		if (!L_6)
		{
			goto IL_0047;
		}
	}
	{
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_7 = ___obj0;
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_8 = __this->get_lookTargetTracking_28();
		IL2CPP_RUNTIME_CLASS_INIT(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		bool L_9;
		L_9 = Object_op_Inequality_mE1F187520BD83FB7D86A6D850710C4D42B864E90(L_7, L_8, /*hidden argument*/NULL);
		if (!L_9)
		{
			goto IL_0047;
		}
	}
	{
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_10 = __this->get_lookTarget_27();
		IL2CPP_RUNTIME_CLASS_INIT(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		bool L_11;
		L_11 = Object_op_Implicit_mC8214E4F028CC2F036CC82BDB81D102A02893499(L_10, /*hidden argument*/NULL);
		if (!L_11)
		{
			goto IL_0047;
		}
	}
	{
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_12 = ___obj0;
		__this->set_lookTargetTracking_28(L_12);
	}

IL_0047:
	{
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_13 = __this->get_lookTarget_27();
		IL2CPP_RUNTIME_CLASS_INIT(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		bool L_14;
		L_14 = Object_op_Implicit_mC8214E4F028CC2F036CC82BDB81D102A02893499(L_13, /*hidden argument*/NULL);
		if (L_14)
		{
			goto IL_0068;
		}
	}
	{
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_15 = __this->get_lookTargetTracking_28();
		IL2CPP_RUNTIME_CLASS_INIT(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		bool L_16;
		L_16 = Object_op_Implicit_mC8214E4F028CC2F036CC82BDB81D102A02893499(L_15, /*hidden argument*/NULL);
		if (!L_16)
		{
			goto IL_0068;
		}
	}
	{
		__this->set_lookTargetTracking_28((GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 *)NULL);
	}

IL_0068:
	{
		return;
	}
}
// System.Void CrazyMinnow.SALSA.RandomEyes3D::SetLookTarget(UnityEngine.GameObject)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void RandomEyes3D_SetLookTarget_m37D2E133D824526AC0FC64BBA65B40865592B4B8 (RandomEyes3D_t6868157C4605BD0225E1B08301F110E72336CE45 * __this, GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * ___obj0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_0 = ___obj0;
		IL2CPP_RUNTIME_CLASS_INIT(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		bool L_1;
		L_1 = Object_op_Implicit_mC8214E4F028CC2F036CC82BDB81D102A02893499(L_0, /*hidden argument*/NULL);
		if (L_1)
		{
			goto IL_001c;
		}
	}
	{
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_2 = __this->get_lookTarget_27();
		IL2CPP_RUNTIME_CLASS_INIT(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		bool L_3;
		L_3 = Object_op_Implicit_mC8214E4F028CC2F036CC82BDB81D102A02893499(L_2, /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_001c;
		}
	}
	{
		__this->set_lookTarget_27((GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 *)NULL);
	}

IL_001c:
	{
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_4 = ___obj0;
		IL2CPP_RUNTIME_CLASS_INIT(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		bool L_5;
		L_5 = Object_op_Implicit_mC8214E4F028CC2F036CC82BDB81D102A02893499(L_4, /*hidden argument*/NULL);
		if (!L_5)
		{
			goto IL_0039;
		}
	}
	{
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_6 = ___obj0;
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_7 = __this->get_lookTarget_27();
		IL2CPP_RUNTIME_CLASS_INIT(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		bool L_8;
		L_8 = Object_op_Inequality_mE1F187520BD83FB7D86A6D850710C4D42B864E90(L_6, L_7, /*hidden argument*/NULL);
		if (!L_8)
		{
			goto IL_0039;
		}
	}
	{
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_9 = ___obj0;
		__this->set_lookTarget_27(L_9);
	}

IL_0039:
	{
		return;
	}
}
// System.Void CrazyMinnow.SALSA.RandomEyes3D::SetTargetAffinity(System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void RandomEyes3D_SetTargetAffinity_m2946B7CB73E8C8C336CD612B46DB2289F5896D69 (RandomEyes3D_t6868157C4605BD0225E1B08301F110E72336CE45 * __this, bool ___status0, const RuntimeMethod* method)
{
	{
		bool L_0 = ___status0;
		__this->set_targetAffinity_29(L_0);
		return;
	}
}
// System.Void CrazyMinnow.SALSA.RandomEyes3D::SetAffinityPercentage(System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void RandomEyes3D_SetAffinityPercentage_mB5F8839B49735773F154CECB55A4A945127EBABC (RandomEyes3D_t6868157C4605BD0225E1B08301F110E72336CE45 * __this, float ___percent0, const RuntimeMethod* method)
{
	{
		float L_0 = ___percent0;
		float L_1;
		L_1 = Mathf_Clamp_m2416F3B785C8F135863E3D17E5B0CB4174797B87(L_0, (0.0f), (1.0f), /*hidden argument*/NULL);
		__this->set_targetAffinityPercentage_30(L_1);
		return;
	}
}
// System.Void CrazyMinnow.SALSA.RandomEyes3D::SetRandomEyes(System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void RandomEyes3D_SetRandomEyes_mD372850D2928E9D46B8E385E7F5C0C738CD4A618 (RandomEyes3D_t6868157C4605BD0225E1B08301F110E72336CE45 * __this, bool ___status0, const RuntimeMethod* method)
{
	{
		bool L_0 = ___status0;
		__this->set_randomEyes_17(L_0);
		float L_1;
		L_1 = Time_get_time_m1A186074B1FCD448AB13A4B9D715AB9ED0B40844(/*hidden argument*/NULL);
		__this->set_blinkTimer_79(L_1);
		float L_2;
		L_2 = Time_get_time_m1A186074B1FCD448AB13A4B9D715AB9ED0B40844(/*hidden argument*/NULL);
		__this->set_randomTimer_78(L_2);
		return;
	}
}
// System.Void CrazyMinnow.SALSA.RandomEyes3D::SetBlink(System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void RandomEyes3D_SetBlink_mC54B3B3E6FEC4B3C0F1558051F70124EE7EE1EFA (RandomEyes3D_t6868157C4605BD0225E1B08301F110E72336CE45 * __this, bool ___status0, const RuntimeMethod* method)
{
	{
		bool L_0 = ___status0;
		__this->set_randomBlink_33(L_0);
		return;
	}
}
// System.Void CrazyMinnow.SALSA.RandomEyes3D::SetBlinkDuration(System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void RandomEyes3D_SetBlinkDuration_m58E1BE83D084596998E0876B051021D7701ECBEE (RandomEyes3D_t6868157C4605BD0225E1B08301F110E72336CE45 * __this, float ___duration0, const RuntimeMethod* method)
{
	{
		float L_0 = ___duration0;
		__this->set_blinkDuration_21(L_0);
		return;
	}
}
// System.Void CrazyMinnow.SALSA.RandomEyes3D::SetBlinkSpeed(System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void RandomEyes3D_SetBlinkSpeed_m154A39F053782E03ECB5B5DF26CB4E956D9C112A (RandomEyes3D_t6868157C4605BD0225E1B08301F110E72336CE45 * __this, float ___speed0, const RuntimeMethod* method)
{
	{
		float L_0 = ___speed0;
		__this->set_blinkSpeed_22(L_0);
		return;
	}
}
// System.Void CrazyMinnow.SALSA.RandomEyes3D::SetOpenMax(System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void RandomEyes3D_SetOpenMax_m2277B51661364A14724610455A8E8464576FA51E (RandomEyes3D_t6868157C4605BD0225E1B08301F110E72336CE45 * __this, float ___max0, const RuntimeMethod* method)
{
	{
		float L_0 = ___max0;
		__this->set_openMax_23(L_0);
		return;
	}
}
// System.Void CrazyMinnow.SALSA.RandomEyes3D::SetCloseMax(System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void RandomEyes3D_SetCloseMax_mB3143FFFD37CDB7D07A4DBF7306A27119607BF41 (RandomEyes3D_t6868157C4605BD0225E1B08301F110E72336CE45 * __this, float ___max0, const RuntimeMethod* method)
{
	{
		float L_0 = ___max0;
		__this->set_closeMax_24(L_0);
		return;
	}
}
// System.Void CrazyMinnow.SALSA.RandomEyes3D::SetCustomShapeRandom(System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void RandomEyes3D_SetCustomShapeRandom_m55793E8EE1AB7B795CA82B0B2A71148A7072B39F (RandomEyes3D_t6868157C4605BD0225E1B08301F110E72336CE45 * __this, bool ___status0, const RuntimeMethod* method)
{
	{
		bool L_0 = ___status0;
		__this->set_randomCustomShapes_49(L_0);
		float L_1;
		L_1 = Time_get_time_m1A186074B1FCD448AB13A4B9D715AB9ED0B40844(/*hidden argument*/NULL);
		__this->set_customShapeTimer_84(L_1);
		return;
	}
}
// System.Void CrazyMinnow.SALSA.RandomEyes3D::SetCustomShapeOverride(System.String,System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void RandomEyes3D_SetCustomShapeOverride_m23761F4B53F0E1A29A36DFFF91801AD211032AC1 (RandomEyes3D_t6868157C4605BD0225E1B08301F110E72336CE45 * __this, String_t* ___shapeName0, bool ___overrideOn1, const RuntimeMethod* method)
{
	int32_t V_0 = 0;
	{
		V_0 = 0;
		goto IL_002b;
	}

IL_0004:
	{
		RandomEyesCustomShapeU5BU5D_t8FB0C62923194ED52AE0B06A6249680C47CA152C* L_0 = __this->get_customShapes_48();
		int32_t L_1 = V_0;
		NullCheck(L_0);
		int32_t L_2 = L_1;
		RandomEyesCustomShape_t10BCA0CEC18C2BBF6A8BD7BFAD2AF005D233C354 * L_3 = (L_0)->GetAt(static_cast<il2cpp_array_size_t>(L_2));
		NullCheck(L_3);
		String_t* L_4 = L_3->get_shapeName_1();
		String_t* L_5 = ___shapeName0;
		bool L_6;
		L_6 = String_op_Equality_m2B91EE68355F142F67095973D32EB5828B7B73CB(L_4, L_5, /*hidden argument*/NULL);
		if (!L_6)
		{
			goto IL_0027;
		}
	}
	{
		RandomEyesCustomShapeU5BU5D_t8FB0C62923194ED52AE0B06A6249680C47CA152C* L_7 = __this->get_customShapes_48();
		int32_t L_8 = V_0;
		NullCheck(L_7);
		int32_t L_9 = L_8;
		RandomEyesCustomShape_t10BCA0CEC18C2BBF6A8BD7BFAD2AF005D233C354 * L_10 = (L_7)->GetAt(static_cast<il2cpp_array_size_t>(L_9));
		bool L_11 = ___overrideOn1;
		NullCheck(L_10);
		L_10->set_overrideOn_3(L_11);
	}

IL_0027:
	{
		int32_t L_12 = V_0;
		V_0 = ((int32_t)il2cpp_codegen_add((int32_t)L_12, (int32_t)1));
	}

IL_002b:
	{
		int32_t L_13 = V_0;
		RandomEyesCustomShapeU5BU5D_t8FB0C62923194ED52AE0B06A6249680C47CA152C* L_14 = __this->get_customShapes_48();
		NullCheck(L_14);
		if ((((int32_t)L_13) < ((int32_t)((int32_t)((int32_t)(((RuntimeArray*)L_14)->max_length))))))
		{
			goto IL_0004;
		}
	}
	{
		return;
	}
}
// System.Void CrazyMinnow.SALSA.RandomEyes3D::SetCustomShapeOverride(System.Int32,System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void RandomEyes3D_SetCustomShapeOverride_m98357978832E89B5E5B1FD37F2702911980DCD0A (RandomEyes3D_t6868157C4605BD0225E1B08301F110E72336CE45 * __this, int32_t ___shapeIndex0, bool ___overrideOn1, const RuntimeMethod* method)
{
	int32_t V_0 = 0;
	{
		V_0 = 0;
		goto IL_0026;
	}

IL_0004:
	{
		RandomEyesCustomShapeU5BU5D_t8FB0C62923194ED52AE0B06A6249680C47CA152C* L_0 = __this->get_customShapes_48();
		int32_t L_1 = V_0;
		NullCheck(L_0);
		int32_t L_2 = L_1;
		RandomEyesCustomShape_t10BCA0CEC18C2BBF6A8BD7BFAD2AF005D233C354 * L_3 = (L_0)->GetAt(static_cast<il2cpp_array_size_t>(L_2));
		NullCheck(L_3);
		int32_t L_4 = L_3->get_shapeIndex_0();
		int32_t L_5 = ___shapeIndex0;
		if ((!(((uint32_t)L_4) == ((uint32_t)L_5))))
		{
			goto IL_0022;
		}
	}
	{
		RandomEyesCustomShapeU5BU5D_t8FB0C62923194ED52AE0B06A6249680C47CA152C* L_6 = __this->get_customShapes_48();
		int32_t L_7 = V_0;
		NullCheck(L_6);
		int32_t L_8 = L_7;
		RandomEyesCustomShape_t10BCA0CEC18C2BBF6A8BD7BFAD2AF005D233C354 * L_9 = (L_6)->GetAt(static_cast<il2cpp_array_size_t>(L_8));
		bool L_10 = ___overrideOn1;
		NullCheck(L_9);
		L_9->set_overrideOn_3(L_10);
	}

IL_0022:
	{
		int32_t L_11 = V_0;
		V_0 = ((int32_t)il2cpp_codegen_add((int32_t)L_11, (int32_t)1));
	}

IL_0026:
	{
		int32_t L_12 = V_0;
		RandomEyesCustomShapeU5BU5D_t8FB0C62923194ED52AE0B06A6249680C47CA152C* L_13 = __this->get_customShapes_48();
		NullCheck(L_13);
		if ((((int32_t)L_12) < ((int32_t)((int32_t)((int32_t)(((RuntimeArray*)L_13)->max_length))))))
		{
			goto IL_0004;
		}
	}
	{
		return;
	}
}
// System.Void CrazyMinnow.SALSA.RandomEyes3D::SetCustomShapeOverride(System.String,System.Single,System.Single,System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void RandomEyes3D_SetCustomShapeOverride_m9C8E2FDBF2AC9F768DADF1EF2853823C01937A52 (RandomEyes3D_t6868157C4605BD0225E1B08301F110E72336CE45 * __this, String_t* ___shapeName0, float ___blendSpeed1, float ___rangeOfMotion2, bool ___overrideOn3, const RuntimeMethod* method)
{
	int32_t V_0 = 0;
	{
		V_0 = 0;
		goto IL_0048;
	}

IL_0004:
	{
		RandomEyesCustomShapeU5BU5D_t8FB0C62923194ED52AE0B06A6249680C47CA152C* L_0 = __this->get_customShapes_48();
		int32_t L_1 = V_0;
		NullCheck(L_0);
		int32_t L_2 = L_1;
		RandomEyesCustomShape_t10BCA0CEC18C2BBF6A8BD7BFAD2AF005D233C354 * L_3 = (L_0)->GetAt(static_cast<il2cpp_array_size_t>(L_2));
		NullCheck(L_3);
		String_t* L_4 = L_3->get_shapeName_1();
		String_t* L_5 = ___shapeName0;
		bool L_6;
		L_6 = String_op_Equality_m2B91EE68355F142F67095973D32EB5828B7B73CB(L_4, L_5, /*hidden argument*/NULL);
		if (!L_6)
		{
			goto IL_0044;
		}
	}
	{
		RandomEyesCustomShapeU5BU5D_t8FB0C62923194ED52AE0B06A6249680C47CA152C* L_7 = __this->get_customShapes_48();
		int32_t L_8 = V_0;
		NullCheck(L_7);
		int32_t L_9 = L_8;
		RandomEyesCustomShape_t10BCA0CEC18C2BBF6A8BD7BFAD2AF005D233C354 * L_10 = (L_7)->GetAt(static_cast<il2cpp_array_size_t>(L_9));
		float L_11 = ___blendSpeed1;
		NullCheck(L_10);
		L_10->set_blendSpeed_6(L_11);
		RandomEyesCustomShapeU5BU5D_t8FB0C62923194ED52AE0B06A6249680C47CA152C* L_12 = __this->get_customShapes_48();
		int32_t L_13 = V_0;
		NullCheck(L_12);
		int32_t L_14 = L_13;
		RandomEyesCustomShape_t10BCA0CEC18C2BBF6A8BD7BFAD2AF005D233C354 * L_15 = (L_12)->GetAt(static_cast<il2cpp_array_size_t>(L_14));
		float L_16 = ___rangeOfMotion2;
		NullCheck(L_15);
		L_15->set_rangeOfMotion_7(L_16);
		RandomEyesCustomShapeU5BU5D_t8FB0C62923194ED52AE0B06A6249680C47CA152C* L_17 = __this->get_customShapes_48();
		int32_t L_18 = V_0;
		NullCheck(L_17);
		int32_t L_19 = L_18;
		RandomEyesCustomShape_t10BCA0CEC18C2BBF6A8BD7BFAD2AF005D233C354 * L_20 = (L_17)->GetAt(static_cast<il2cpp_array_size_t>(L_19));
		bool L_21 = ___overrideOn3;
		NullCheck(L_20);
		L_20->set_overrideOn_3(L_21);
	}

IL_0044:
	{
		int32_t L_22 = V_0;
		V_0 = ((int32_t)il2cpp_codegen_add((int32_t)L_22, (int32_t)1));
	}

IL_0048:
	{
		int32_t L_23 = V_0;
		RandomEyesCustomShapeU5BU5D_t8FB0C62923194ED52AE0B06A6249680C47CA152C* L_24 = __this->get_customShapes_48();
		NullCheck(L_24);
		if ((((int32_t)L_23) < ((int32_t)((int32_t)((int32_t)(((RuntimeArray*)L_24)->max_length))))))
		{
			goto IL_0004;
		}
	}
	{
		return;
	}
}
// System.Void CrazyMinnow.SALSA.RandomEyes3D::SetCustomShapeOverride(System.Int32,System.Single,System.Single,System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void RandomEyes3D_SetCustomShapeOverride_m5C3C269C3763197AC91B6053CA0F7EAF1E5CF4F3 (RandomEyes3D_t6868157C4605BD0225E1B08301F110E72336CE45 * __this, int32_t ___shapeIndex0, float ___blendSpeed1, float ___rangeOfMotion2, bool ___overrideOn3, const RuntimeMethod* method)
{
	int32_t V_0 = 0;
	{
		V_0 = 0;
		goto IL_0043;
	}

IL_0004:
	{
		RandomEyesCustomShapeU5BU5D_t8FB0C62923194ED52AE0B06A6249680C47CA152C* L_0 = __this->get_customShapes_48();
		int32_t L_1 = V_0;
		NullCheck(L_0);
		int32_t L_2 = L_1;
		RandomEyesCustomShape_t10BCA0CEC18C2BBF6A8BD7BFAD2AF005D233C354 * L_3 = (L_0)->GetAt(static_cast<il2cpp_array_size_t>(L_2));
		NullCheck(L_3);
		int32_t L_4 = L_3->get_shapeIndex_0();
		int32_t L_5 = ___shapeIndex0;
		if ((!(((uint32_t)L_4) == ((uint32_t)L_5))))
		{
			goto IL_003f;
		}
	}
	{
		RandomEyesCustomShapeU5BU5D_t8FB0C62923194ED52AE0B06A6249680C47CA152C* L_6 = __this->get_customShapes_48();
		int32_t L_7 = V_0;
		NullCheck(L_6);
		int32_t L_8 = L_7;
		RandomEyesCustomShape_t10BCA0CEC18C2BBF6A8BD7BFAD2AF005D233C354 * L_9 = (L_6)->GetAt(static_cast<il2cpp_array_size_t>(L_8));
		float L_10 = ___blendSpeed1;
		NullCheck(L_9);
		L_9->set_blendSpeed_6(L_10);
		RandomEyesCustomShapeU5BU5D_t8FB0C62923194ED52AE0B06A6249680C47CA152C* L_11 = __this->get_customShapes_48();
		int32_t L_12 = V_0;
		NullCheck(L_11);
		int32_t L_13 = L_12;
		RandomEyesCustomShape_t10BCA0CEC18C2BBF6A8BD7BFAD2AF005D233C354 * L_14 = (L_11)->GetAt(static_cast<il2cpp_array_size_t>(L_13));
		float L_15 = ___rangeOfMotion2;
		NullCheck(L_14);
		L_14->set_rangeOfMotion_7(L_15);
		RandomEyesCustomShapeU5BU5D_t8FB0C62923194ED52AE0B06A6249680C47CA152C* L_16 = __this->get_customShapes_48();
		int32_t L_17 = V_0;
		NullCheck(L_16);
		int32_t L_18 = L_17;
		RandomEyesCustomShape_t10BCA0CEC18C2BBF6A8BD7BFAD2AF005D233C354 * L_19 = (L_16)->GetAt(static_cast<il2cpp_array_size_t>(L_18));
		bool L_20 = ___overrideOn3;
		NullCheck(L_19);
		L_19->set_overrideOn_3(L_20);
	}

IL_003f:
	{
		int32_t L_21 = V_0;
		V_0 = ((int32_t)il2cpp_codegen_add((int32_t)L_21, (int32_t)1));
	}

IL_0043:
	{
		int32_t L_22 = V_0;
		RandomEyesCustomShapeU5BU5D_t8FB0C62923194ED52AE0B06A6249680C47CA152C* L_23 = __this->get_customShapes_48();
		NullCheck(L_23);
		if ((((int32_t)L_22) < ((int32_t)((int32_t)((int32_t)(((RuntimeArray*)L_23)->max_length))))))
		{
			goto IL_0004;
		}
	}
	{
		return;
	}
}
// System.Void CrazyMinnow.SALSA.RandomEyes3D::SetCustomShapeOverride(System.String,System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void RandomEyes3D_SetCustomShapeOverride_mA4614D96A02DF336129C3255D4866EC9499AC131 (RandomEyes3D_t6868157C4605BD0225E1B08301F110E72336CE45 * __this, String_t* ___shapeName0, float ___duration1, const RuntimeMethod* method)
{
	int32_t V_0 = 0;
	{
		V_0 = 0;
		goto IL_002c;
	}

IL_0004:
	{
		RandomEyesCustomShapeU5BU5D_t8FB0C62923194ED52AE0B06A6249680C47CA152C* L_0 = __this->get_customShapes_48();
		int32_t L_1 = V_0;
		NullCheck(L_0);
		int32_t L_2 = L_1;
		RandomEyesCustomShape_t10BCA0CEC18C2BBF6A8BD7BFAD2AF005D233C354 * L_3 = (L_0)->GetAt(static_cast<il2cpp_array_size_t>(L_2));
		NullCheck(L_3);
		String_t* L_4 = L_3->get_shapeName_1();
		String_t* L_5 = ___shapeName0;
		bool L_6;
		L_6 = String_op_Equality_m2B91EE68355F142F67095973D32EB5828B7B73CB(L_4, L_5, /*hidden argument*/NULL);
		if (!L_6)
		{
			goto IL_0028;
		}
	}
	{
		String_t* L_7 = ___shapeName0;
		float L_8 = ___duration1;
		RuntimeObject* L_9;
		L_9 = RandomEyes3D_SetCustomShapeOverrideDuration_m0A72CCB00C2DB1FBFFE2A7CBB1B85685B0EA52CC(__this, L_7, L_8, /*hidden argument*/NULL);
		Coroutine_t899D5232EF542CB8BA70AF9ECEECA494FAA9CCB7 * L_10;
		L_10 = MonoBehaviour_StartCoroutine_m3E33706D38B23CDD179E99BAD61E32303E9CC719(__this, L_9, /*hidden argument*/NULL);
	}

IL_0028:
	{
		int32_t L_11 = V_0;
		V_0 = ((int32_t)il2cpp_codegen_add((int32_t)L_11, (int32_t)1));
	}

IL_002c:
	{
		int32_t L_12 = V_0;
		RandomEyesCustomShapeU5BU5D_t8FB0C62923194ED52AE0B06A6249680C47CA152C* L_13 = __this->get_customShapes_48();
		NullCheck(L_13);
		if ((((int32_t)L_12) < ((int32_t)((int32_t)((int32_t)(((RuntimeArray*)L_13)->max_length))))))
		{
			goto IL_0004;
		}
	}
	{
		return;
	}
}
// System.Void CrazyMinnow.SALSA.RandomEyes3D::SetCustomShapeOverride(System.Int32,System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void RandomEyes3D_SetCustomShapeOverride_mB89253EB0B19EEA2BEDCFE5D5628A7C30F4EAD8E (RandomEyes3D_t6868157C4605BD0225E1B08301F110E72336CE45 * __this, int32_t ___shapeIndex0, float ___duration1, const RuntimeMethod* method)
{
	int32_t V_0 = 0;
	{
		V_0 = 0;
		goto IL_0027;
	}

IL_0004:
	{
		RandomEyesCustomShapeU5BU5D_t8FB0C62923194ED52AE0B06A6249680C47CA152C* L_0 = __this->get_customShapes_48();
		int32_t L_1 = V_0;
		NullCheck(L_0);
		int32_t L_2 = L_1;
		RandomEyesCustomShape_t10BCA0CEC18C2BBF6A8BD7BFAD2AF005D233C354 * L_3 = (L_0)->GetAt(static_cast<il2cpp_array_size_t>(L_2));
		NullCheck(L_3);
		int32_t L_4 = L_3->get_shapeIndex_0();
		int32_t L_5 = ___shapeIndex0;
		if ((!(((uint32_t)L_4) == ((uint32_t)L_5))))
		{
			goto IL_0023;
		}
	}
	{
		int32_t L_6 = ___shapeIndex0;
		float L_7 = ___duration1;
		RuntimeObject* L_8;
		L_8 = RandomEyes3D_SetCustomShapeOverrideDuration_m07722506C2734D00B002B64D6BD1FE3EE9908F69(__this, L_6, L_7, /*hidden argument*/NULL);
		Coroutine_t899D5232EF542CB8BA70AF9ECEECA494FAA9CCB7 * L_9;
		L_9 = MonoBehaviour_StartCoroutine_m3E33706D38B23CDD179E99BAD61E32303E9CC719(__this, L_8, /*hidden argument*/NULL);
	}

IL_0023:
	{
		int32_t L_10 = V_0;
		V_0 = ((int32_t)il2cpp_codegen_add((int32_t)L_10, (int32_t)1));
	}

IL_0027:
	{
		int32_t L_11 = V_0;
		RandomEyesCustomShapeU5BU5D_t8FB0C62923194ED52AE0B06A6249680C47CA152C* L_12 = __this->get_customShapes_48();
		NullCheck(L_12);
		if ((((int32_t)L_11) < ((int32_t)((int32_t)((int32_t)(((RuntimeArray*)L_12)->max_length))))))
		{
			goto IL_0004;
		}
	}
	{
		return;
	}
}
// System.Void CrazyMinnow.SALSA.RandomEyes3D::SetCustomShapeOverride(System.String,System.Single,System.Single,System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void RandomEyes3D_SetCustomShapeOverride_mAC24BBA3E20FC41A96F383097AD90C35E0EA7066 (RandomEyes3D_t6868157C4605BD0225E1B08301F110E72336CE45 * __this, String_t* ___shapeName0, float ___blendSpeed1, float ___rangeOfMotion2, float ___duration3, const RuntimeMethod* method)
{
	int32_t V_0 = 0;
	{
		V_0 = 0;
		goto IL_0049;
	}

IL_0004:
	{
		RandomEyesCustomShapeU5BU5D_t8FB0C62923194ED52AE0B06A6249680C47CA152C* L_0 = __this->get_customShapes_48();
		int32_t L_1 = V_0;
		NullCheck(L_0);
		int32_t L_2 = L_1;
		RandomEyesCustomShape_t10BCA0CEC18C2BBF6A8BD7BFAD2AF005D233C354 * L_3 = (L_0)->GetAt(static_cast<il2cpp_array_size_t>(L_2));
		NullCheck(L_3);
		String_t* L_4 = L_3->get_shapeName_1();
		String_t* L_5 = ___shapeName0;
		bool L_6;
		L_6 = String_op_Equality_m2B91EE68355F142F67095973D32EB5828B7B73CB(L_4, L_5, /*hidden argument*/NULL);
		if (!L_6)
		{
			goto IL_0045;
		}
	}
	{
		RandomEyesCustomShapeU5BU5D_t8FB0C62923194ED52AE0B06A6249680C47CA152C* L_7 = __this->get_customShapes_48();
		int32_t L_8 = V_0;
		NullCheck(L_7);
		int32_t L_9 = L_8;
		RandomEyesCustomShape_t10BCA0CEC18C2BBF6A8BD7BFAD2AF005D233C354 * L_10 = (L_7)->GetAt(static_cast<il2cpp_array_size_t>(L_9));
		float L_11 = ___blendSpeed1;
		NullCheck(L_10);
		L_10->set_blendSpeed_6(L_11);
		RandomEyesCustomShapeU5BU5D_t8FB0C62923194ED52AE0B06A6249680C47CA152C* L_12 = __this->get_customShapes_48();
		int32_t L_13 = V_0;
		NullCheck(L_12);
		int32_t L_14 = L_13;
		RandomEyesCustomShape_t10BCA0CEC18C2BBF6A8BD7BFAD2AF005D233C354 * L_15 = (L_12)->GetAt(static_cast<il2cpp_array_size_t>(L_14));
		float L_16 = ___rangeOfMotion2;
		NullCheck(L_15);
		L_15->set_rangeOfMotion_7(L_16);
		String_t* L_17 = ___shapeName0;
		float L_18 = ___duration3;
		RuntimeObject* L_19;
		L_19 = RandomEyes3D_SetCustomShapeOverrideDuration_m0A72CCB00C2DB1FBFFE2A7CBB1B85685B0EA52CC(__this, L_17, L_18, /*hidden argument*/NULL);
		Coroutine_t899D5232EF542CB8BA70AF9ECEECA494FAA9CCB7 * L_20;
		L_20 = MonoBehaviour_StartCoroutine_m3E33706D38B23CDD179E99BAD61E32303E9CC719(__this, L_19, /*hidden argument*/NULL);
	}

IL_0045:
	{
		int32_t L_21 = V_0;
		V_0 = ((int32_t)il2cpp_codegen_add((int32_t)L_21, (int32_t)1));
	}

IL_0049:
	{
		int32_t L_22 = V_0;
		RandomEyesCustomShapeU5BU5D_t8FB0C62923194ED52AE0B06A6249680C47CA152C* L_23 = __this->get_customShapes_48();
		NullCheck(L_23);
		if ((((int32_t)L_22) < ((int32_t)((int32_t)((int32_t)(((RuntimeArray*)L_23)->max_length))))))
		{
			goto IL_0004;
		}
	}
	{
		return;
	}
}
// System.Void CrazyMinnow.SALSA.RandomEyes3D::SetCustomShapeOverride(System.Int32,System.Single,System.Single,System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void RandomEyes3D_SetCustomShapeOverride_m05639C6FFFB925FE08A91D34C92678F1C851758A (RandomEyes3D_t6868157C4605BD0225E1B08301F110E72336CE45 * __this, int32_t ___shapeIndex0, float ___blendSpeed1, float ___rangeOfMotion2, float ___duration3, const RuntimeMethod* method)
{
	int32_t V_0 = 0;
	{
		V_0 = 0;
		goto IL_0044;
	}

IL_0004:
	{
		RandomEyesCustomShapeU5BU5D_t8FB0C62923194ED52AE0B06A6249680C47CA152C* L_0 = __this->get_customShapes_48();
		int32_t L_1 = V_0;
		NullCheck(L_0);
		int32_t L_2 = L_1;
		RandomEyesCustomShape_t10BCA0CEC18C2BBF6A8BD7BFAD2AF005D233C354 * L_3 = (L_0)->GetAt(static_cast<il2cpp_array_size_t>(L_2));
		NullCheck(L_3);
		int32_t L_4 = L_3->get_shapeIndex_0();
		int32_t L_5 = ___shapeIndex0;
		if ((!(((uint32_t)L_4) == ((uint32_t)L_5))))
		{
			goto IL_0040;
		}
	}
	{
		RandomEyesCustomShapeU5BU5D_t8FB0C62923194ED52AE0B06A6249680C47CA152C* L_6 = __this->get_customShapes_48();
		int32_t L_7 = V_0;
		NullCheck(L_6);
		int32_t L_8 = L_7;
		RandomEyesCustomShape_t10BCA0CEC18C2BBF6A8BD7BFAD2AF005D233C354 * L_9 = (L_6)->GetAt(static_cast<il2cpp_array_size_t>(L_8));
		float L_10 = ___blendSpeed1;
		NullCheck(L_9);
		L_9->set_blendSpeed_6(L_10);
		RandomEyesCustomShapeU5BU5D_t8FB0C62923194ED52AE0B06A6249680C47CA152C* L_11 = __this->get_customShapes_48();
		int32_t L_12 = V_0;
		NullCheck(L_11);
		int32_t L_13 = L_12;
		RandomEyesCustomShape_t10BCA0CEC18C2BBF6A8BD7BFAD2AF005D233C354 * L_14 = (L_11)->GetAt(static_cast<il2cpp_array_size_t>(L_13));
		float L_15 = ___rangeOfMotion2;
		NullCheck(L_14);
		L_14->set_rangeOfMotion_7(L_15);
		int32_t L_16 = ___shapeIndex0;
		float L_17 = ___duration3;
		RuntimeObject* L_18;
		L_18 = RandomEyes3D_SetCustomShapeOverrideDuration_m07722506C2734D00B002B64D6BD1FE3EE9908F69(__this, L_16, L_17, /*hidden argument*/NULL);
		Coroutine_t899D5232EF542CB8BA70AF9ECEECA494FAA9CCB7 * L_19;
		L_19 = MonoBehaviour_StartCoroutine_m3E33706D38B23CDD179E99BAD61E32303E9CC719(__this, L_18, /*hidden argument*/NULL);
	}

IL_0040:
	{
		int32_t L_20 = V_0;
		V_0 = ((int32_t)il2cpp_codegen_add((int32_t)L_20, (int32_t)1));
	}

IL_0044:
	{
		int32_t L_21 = V_0;
		RandomEyesCustomShapeU5BU5D_t8FB0C62923194ED52AE0B06A6249680C47CA152C* L_22 = __this->get_customShapes_48();
		NullCheck(L_22);
		if ((((int32_t)L_21) < ((int32_t)((int32_t)((int32_t)(((RuntimeArray*)L_22)->max_length))))))
		{
			goto IL_0004;
		}
	}
	{
		return;
	}
}
// System.Void CrazyMinnow.SALSA.RandomEyes3D::SetCustomShapeBlendSpeed(System.String,System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void RandomEyes3D_SetCustomShapeBlendSpeed_mE6C02207767F40A39806CEEFB76C1E90FC6B5693 (RandomEyes3D_t6868157C4605BD0225E1B08301F110E72336CE45 * __this, String_t* ___shapeName0, float ___blend1, const RuntimeMethod* method)
{
	int32_t V_0 = 0;
	{
		V_0 = 0;
		goto IL_002b;
	}

IL_0004:
	{
		RandomEyesCustomShapeU5BU5D_t8FB0C62923194ED52AE0B06A6249680C47CA152C* L_0 = __this->get_customShapes_48();
		int32_t L_1 = V_0;
		NullCheck(L_0);
		int32_t L_2 = L_1;
		RandomEyesCustomShape_t10BCA0CEC18C2BBF6A8BD7BFAD2AF005D233C354 * L_3 = (L_0)->GetAt(static_cast<il2cpp_array_size_t>(L_2));
		NullCheck(L_3);
		String_t* L_4 = L_3->get_shapeName_1();
		String_t* L_5 = ___shapeName0;
		bool L_6;
		L_6 = String_op_Equality_m2B91EE68355F142F67095973D32EB5828B7B73CB(L_4, L_5, /*hidden argument*/NULL);
		if (!L_6)
		{
			goto IL_0027;
		}
	}
	{
		RandomEyesCustomShapeU5BU5D_t8FB0C62923194ED52AE0B06A6249680C47CA152C* L_7 = __this->get_customShapes_48();
		int32_t L_8 = V_0;
		NullCheck(L_7);
		int32_t L_9 = L_8;
		RandomEyesCustomShape_t10BCA0CEC18C2BBF6A8BD7BFAD2AF005D233C354 * L_10 = (L_7)->GetAt(static_cast<il2cpp_array_size_t>(L_9));
		float L_11 = ___blend1;
		NullCheck(L_10);
		L_10->set_blendSpeed_6(L_11);
	}

IL_0027:
	{
		int32_t L_12 = V_0;
		V_0 = ((int32_t)il2cpp_codegen_add((int32_t)L_12, (int32_t)1));
	}

IL_002b:
	{
		int32_t L_13 = V_0;
		RandomEyesCustomShapeU5BU5D_t8FB0C62923194ED52AE0B06A6249680C47CA152C* L_14 = __this->get_customShapes_48();
		NullCheck(L_14);
		if ((((int32_t)L_13) < ((int32_t)((int32_t)((int32_t)(((RuntimeArray*)L_14)->max_length))))))
		{
			goto IL_0004;
		}
	}
	{
		return;
	}
}
// System.Void CrazyMinnow.SALSA.RandomEyes3D::SetCustomShapeBlendSpeed(System.Int32,System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void RandomEyes3D_SetCustomShapeBlendSpeed_mE243E3BC51581AC08B9F57ADA68C3E7798D18B09 (RandomEyes3D_t6868157C4605BD0225E1B08301F110E72336CE45 * __this, int32_t ___shapeIndex0, float ___blend1, const RuntimeMethod* method)
{
	int32_t V_0 = 0;
	{
		V_0 = 0;
		goto IL_0026;
	}

IL_0004:
	{
		RandomEyesCustomShapeU5BU5D_t8FB0C62923194ED52AE0B06A6249680C47CA152C* L_0 = __this->get_customShapes_48();
		int32_t L_1 = V_0;
		NullCheck(L_0);
		int32_t L_2 = L_1;
		RandomEyesCustomShape_t10BCA0CEC18C2BBF6A8BD7BFAD2AF005D233C354 * L_3 = (L_0)->GetAt(static_cast<il2cpp_array_size_t>(L_2));
		NullCheck(L_3);
		int32_t L_4 = L_3->get_shapeIndex_0();
		int32_t L_5 = ___shapeIndex0;
		if ((!(((uint32_t)L_4) == ((uint32_t)L_5))))
		{
			goto IL_0022;
		}
	}
	{
		RandomEyesCustomShapeU5BU5D_t8FB0C62923194ED52AE0B06A6249680C47CA152C* L_6 = __this->get_customShapes_48();
		int32_t L_7 = V_0;
		NullCheck(L_6);
		int32_t L_8 = L_7;
		RandomEyesCustomShape_t10BCA0CEC18C2BBF6A8BD7BFAD2AF005D233C354 * L_9 = (L_6)->GetAt(static_cast<il2cpp_array_size_t>(L_8));
		float L_10 = ___blend1;
		NullCheck(L_9);
		L_9->set_blendSpeed_6(L_10);
	}

IL_0022:
	{
		int32_t L_11 = V_0;
		V_0 = ((int32_t)il2cpp_codegen_add((int32_t)L_11, (int32_t)1));
	}

IL_0026:
	{
		int32_t L_12 = V_0;
		RandomEyesCustomShapeU5BU5D_t8FB0C62923194ED52AE0B06A6249680C47CA152C* L_13 = __this->get_customShapes_48();
		NullCheck(L_13);
		if ((((int32_t)L_12) < ((int32_t)((int32_t)((int32_t)(((RuntimeArray*)L_13)->max_length))))))
		{
			goto IL_0004;
		}
	}
	{
		return;
	}
}
// System.Void CrazyMinnow.SALSA.RandomEyes3D::SetCustomShapeRangeOfMotion(System.String,System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void RandomEyes3D_SetCustomShapeRangeOfMotion_m203068B98D9D262BB4D6B2C8AEECE0875FF4AA14 (RandomEyes3D_t6868157C4605BD0225E1B08301F110E72336CE45 * __this, String_t* ___shapeName0, float ___range1, const RuntimeMethod* method)
{
	int32_t V_0 = 0;
	{
		V_0 = 0;
		goto IL_002b;
	}

IL_0004:
	{
		RandomEyesCustomShapeU5BU5D_t8FB0C62923194ED52AE0B06A6249680C47CA152C* L_0 = __this->get_customShapes_48();
		int32_t L_1 = V_0;
		NullCheck(L_0);
		int32_t L_2 = L_1;
		RandomEyesCustomShape_t10BCA0CEC18C2BBF6A8BD7BFAD2AF005D233C354 * L_3 = (L_0)->GetAt(static_cast<il2cpp_array_size_t>(L_2));
		NullCheck(L_3);
		String_t* L_4 = L_3->get_shapeName_1();
		String_t* L_5 = ___shapeName0;
		bool L_6;
		L_6 = String_op_Equality_m2B91EE68355F142F67095973D32EB5828B7B73CB(L_4, L_5, /*hidden argument*/NULL);
		if (!L_6)
		{
			goto IL_0027;
		}
	}
	{
		RandomEyesCustomShapeU5BU5D_t8FB0C62923194ED52AE0B06A6249680C47CA152C* L_7 = __this->get_customShapes_48();
		int32_t L_8 = V_0;
		NullCheck(L_7);
		int32_t L_9 = L_8;
		RandomEyesCustomShape_t10BCA0CEC18C2BBF6A8BD7BFAD2AF005D233C354 * L_10 = (L_7)->GetAt(static_cast<il2cpp_array_size_t>(L_9));
		float L_11 = ___range1;
		NullCheck(L_10);
		L_10->set_rangeOfMotion_7(L_11);
	}

IL_0027:
	{
		int32_t L_12 = V_0;
		V_0 = ((int32_t)il2cpp_codegen_add((int32_t)L_12, (int32_t)1));
	}

IL_002b:
	{
		int32_t L_13 = V_0;
		RandomEyesCustomShapeU5BU5D_t8FB0C62923194ED52AE0B06A6249680C47CA152C* L_14 = __this->get_customShapes_48();
		NullCheck(L_14);
		if ((((int32_t)L_13) < ((int32_t)((int32_t)((int32_t)(((RuntimeArray*)L_14)->max_length))))))
		{
			goto IL_0004;
		}
	}
	{
		return;
	}
}
// System.Void CrazyMinnow.SALSA.RandomEyes3D::SetCustomShapeRangeOfMotion(System.Int32,System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void RandomEyes3D_SetCustomShapeRangeOfMotion_m74054909B1069B8527597D1285B516B980955B67 (RandomEyes3D_t6868157C4605BD0225E1B08301F110E72336CE45 * __this, int32_t ___shapeIndex0, float ___range1, const RuntimeMethod* method)
{
	int32_t V_0 = 0;
	{
		V_0 = 0;
		goto IL_0026;
	}

IL_0004:
	{
		RandomEyesCustomShapeU5BU5D_t8FB0C62923194ED52AE0B06A6249680C47CA152C* L_0 = __this->get_customShapes_48();
		int32_t L_1 = V_0;
		NullCheck(L_0);
		int32_t L_2 = L_1;
		RandomEyesCustomShape_t10BCA0CEC18C2BBF6A8BD7BFAD2AF005D233C354 * L_3 = (L_0)->GetAt(static_cast<il2cpp_array_size_t>(L_2));
		NullCheck(L_3);
		int32_t L_4 = L_3->get_shapeIndex_0();
		int32_t L_5 = ___shapeIndex0;
		if ((!(((uint32_t)L_4) == ((uint32_t)L_5))))
		{
			goto IL_0022;
		}
	}
	{
		RandomEyesCustomShapeU5BU5D_t8FB0C62923194ED52AE0B06A6249680C47CA152C* L_6 = __this->get_customShapes_48();
		int32_t L_7 = V_0;
		NullCheck(L_6);
		int32_t L_8 = L_7;
		RandomEyesCustomShape_t10BCA0CEC18C2BBF6A8BD7BFAD2AF005D233C354 * L_9 = (L_6)->GetAt(static_cast<il2cpp_array_size_t>(L_8));
		float L_10 = ___range1;
		NullCheck(L_9);
		L_9->set_rangeOfMotion_7(L_10);
	}

IL_0022:
	{
		int32_t L_11 = V_0;
		V_0 = ((int32_t)il2cpp_codegen_add((int32_t)L_11, (int32_t)1));
	}

IL_0026:
	{
		int32_t L_12 = V_0;
		RandomEyesCustomShapeU5BU5D_t8FB0C62923194ED52AE0B06A6249680C47CA152C* L_13 = __this->get_customShapes_48();
		NullCheck(L_13);
		if ((((int32_t)L_12) < ((int32_t)((int32_t)((int32_t)(((RuntimeArray*)L_13)->max_length))))))
		{
			goto IL_0004;
		}
	}
	{
		return;
	}
}
// System.Void CrazyMinnow.SALSA.RandomEyes3D::SetCustomShape(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void RandomEyes3D_SetCustomShape_m0B61CBCBC3389E335E869448A557D96549915D36 (RandomEyes3D_t6868157C4605BD0225E1B08301F110E72336CE45 * __this, String_t* ___shapeName0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral548D93DDB2AC6B24373148B19D9A625571AB2318);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralDA39A3EE5E6B4B0D3255BFEF95601890AFD80709);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	{
		V_0 = 0;
		goto IL_004a;
	}

IL_0004:
	{
		StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* L_0 = __this->get_dynamicCustomShapes_45();
		int32_t L_1 = V_0;
		NullCheck(L_0);
		int32_t L_2 = L_1;
		String_t* L_3 = (L_0)->GetAt(static_cast<il2cpp_array_size_t>(L_2));
		String_t* L_4 = ___shapeName0;
		bool L_5;
		L_5 = String_op_Equality_m2B91EE68355F142F67095973D32EB5828B7B73CB(L_3, L_4, /*hidden argument*/NULL);
		if (!L_5)
		{
			goto IL_001b;
		}
	}
	{
		int32_t L_6 = V_0;
		__this->set_selectedCustomShape_42(L_6);
	}

IL_001b:
	{
		String_t* L_7 = ___shapeName0;
		if (!L_7)
		{
			goto IL_002b;
		}
	}
	{
		String_t* L_8 = ___shapeName0;
		bool L_9;
		L_9 = String_op_Equality_m2B91EE68355F142F67095973D32EB5828B7B73CB(L_8, _stringLiteralDA39A3EE5E6B4B0D3255BFEF95601890AFD80709, /*hidden argument*/NULL);
		if (!L_9)
		{
			goto IL_0046;
		}
	}

IL_002b:
	{
		StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* L_10 = __this->get_dynamicCustomShapes_45();
		int32_t L_11 = V_0;
		NullCheck(L_10);
		int32_t L_12 = L_11;
		String_t* L_13 = (L_10)->GetAt(static_cast<il2cpp_array_size_t>(L_12));
		bool L_14;
		L_14 = String_op_Equality_m2B91EE68355F142F67095973D32EB5828B7B73CB(L_13, _stringLiteral548D93DDB2AC6B24373148B19D9A625571AB2318, /*hidden argument*/NULL);
		if (!L_14)
		{
			goto IL_0046;
		}
	}
	{
		int32_t L_15 = V_0;
		__this->set_selectedCustomShape_42(L_15);
	}

IL_0046:
	{
		int32_t L_16 = V_0;
		V_0 = ((int32_t)il2cpp_codegen_add((int32_t)L_16, (int32_t)1));
	}

IL_004a:
	{
		int32_t L_17 = V_0;
		StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* L_18 = __this->get_dynamicCustomShapes_45();
		NullCheck(L_18);
		if ((((int32_t)L_17) < ((int32_t)((int32_t)((int32_t)(((RuntimeArray*)L_18)->max_length))))))
		{
			goto IL_0004;
		}
	}
	{
		return;
	}
}
// System.Void CrazyMinnow.SALSA.RandomEyes3D::SetCustomShape(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void RandomEyes3D_SetCustomShape_m46A96B9C7709FD6B8790B63AF40F71C00927A4A2 (RandomEyes3D_t6868157C4605BD0225E1B08301F110E72336CE45 * __this, int32_t ___shapeIndex0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral548D93DDB2AC6B24373148B19D9A625571AB2318);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralDA39A3EE5E6B4B0D3255BFEF95601890AFD80709);
		s_Il2CppMethodInitialized = true;
	}
	String_t* V_0 = NULL;
	int32_t V_1 = 0;
	{
		SkinnedMeshRenderer_t126F4D6010E0F4B2685A7817B0A9171805D8F496 * L_0 = __this->get_skinnedMeshRenderer_6();
		IL2CPP_RUNTIME_CLASS_INIT(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		bool L_1;
		L_1 = Object_op_Implicit_mC8214E4F028CC2F036CC82BDB81D102A02893499(L_0, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0072;
		}
	}
	{
		V_0 = _stringLiteralDA39A3EE5E6B4B0D3255BFEF95601890AFD80709;
		int32_t L_2 = ___shapeIndex0;
		if ((((int32_t)L_2) <= ((int32_t)(-1))))
		{
			goto IL_0029;
		}
	}
	{
		SkinnedMeshRenderer_t126F4D6010E0F4B2685A7817B0A9171805D8F496 * L_3 = __this->get_skinnedMeshRenderer_6();
		NullCheck(L_3);
		Mesh_t2F5992DBA650D5862B43D3823ACD997132A57DA6 * L_4;
		L_4 = SkinnedMeshRenderer_get_sharedMesh_mFD55E307943C1C4B2E2E8632F15B41CCBD8D91F2(L_3, /*hidden argument*/NULL);
		int32_t L_5 = ___shapeIndex0;
		NullCheck(L_4);
		String_t* L_6;
		L_6 = Mesh_GetBlendShapeName_mF28E636D7F66608E6FD9152ACD9547B78C895000(L_4, L_5, /*hidden argument*/NULL);
		V_0 = L_6;
	}

IL_0029:
	{
		V_1 = 0;
		goto IL_0067;
	}

IL_002d:
	{
		StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* L_7 = __this->get_dynamicCustomShapes_45();
		int32_t L_8 = V_1;
		NullCheck(L_7);
		int32_t L_9 = L_8;
		String_t* L_10 = (L_7)->GetAt(static_cast<il2cpp_array_size_t>(L_9));
		String_t* L_11 = V_0;
		bool L_12;
		L_12 = String_op_Equality_m2B91EE68355F142F67095973D32EB5828B7B73CB(L_10, L_11, /*hidden argument*/NULL);
		if (!L_12)
		{
			goto IL_0044;
		}
	}
	{
		int32_t L_13 = V_1;
		__this->set_selectedCustomShape_42(L_13);
	}

IL_0044:
	{
		int32_t L_14 = ___shapeIndex0;
		if ((!(((uint32_t)L_14) == ((uint32_t)(-1)))))
		{
			goto IL_0063;
		}
	}
	{
		StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* L_15 = __this->get_dynamicCustomShapes_45();
		int32_t L_16 = V_1;
		NullCheck(L_15);
		int32_t L_17 = L_16;
		String_t* L_18 = (L_15)->GetAt(static_cast<il2cpp_array_size_t>(L_17));
		bool L_19;
		L_19 = String_op_Equality_m2B91EE68355F142F67095973D32EB5828B7B73CB(L_18, _stringLiteral548D93DDB2AC6B24373148B19D9A625571AB2318, /*hidden argument*/NULL);
		if (!L_19)
		{
			goto IL_0063;
		}
	}
	{
		int32_t L_20 = V_1;
		__this->set_selectedCustomShape_42(L_20);
	}

IL_0063:
	{
		int32_t L_21 = V_1;
		V_1 = ((int32_t)il2cpp_codegen_add((int32_t)L_21, (int32_t)1));
	}

IL_0067:
	{
		int32_t L_22 = V_1;
		StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* L_23 = __this->get_dynamicCustomShapes_45();
		NullCheck(L_23);
		if ((((int32_t)L_22) < ((int32_t)((int32_t)((int32_t)(((RuntimeArray*)L_23)->max_length))))))
		{
			goto IL_002d;
		}
	}

IL_0072:
	{
		return;
	}
}
// System.Void CrazyMinnow.SALSA.RandomEyes3D::SetCustomShape(System.String,System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void RandomEyes3D_SetCustomShape_m00C34809EE3C77CE110E8F6CCFECA9F0ECE2DB11 (RandomEyes3D_t6868157C4605BD0225E1B08301F110E72336CE45 * __this, String_t* ___shapeName0, float ___duration1, const RuntimeMethod* method)
{
	{
		String_t* L_0 = ___shapeName0;
		float L_1 = ___duration1;
		RuntimeObject* L_2;
		L_2 = RandomEyes3D_SetCustomShapeDuration_mC9EFA2C619003EB530936AF8AC02019814A65099(__this, L_0, L_1, /*hidden argument*/NULL);
		Coroutine_t899D5232EF542CB8BA70AF9ECEECA494FAA9CCB7 * L_3;
		L_3 = MonoBehaviour_StartCoroutine_m3E33706D38B23CDD179E99BAD61E32303E9CC719(__this, L_2, /*hidden argument*/NULL);
		return;
	}
}
// System.Void CrazyMinnow.SALSA.RandomEyes3D::SetCustomShape(System.Int32,System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void RandomEyes3D_SetCustomShape_mC41F70839B2458EDFC63410F9C534DC53A775177 (RandomEyes3D_t6868157C4605BD0225E1B08301F110E72336CE45 * __this, int32_t ___shapeIndex0, float ___duration1, const RuntimeMethod* method)
{
	{
		int32_t L_0 = ___shapeIndex0;
		float L_1 = ___duration1;
		RuntimeObject* L_2;
		L_2 = RandomEyes3D_SetCustomShapeDuration_m3B9A380330E625EA5EB3E911590D6CEF499B6F53(__this, L_0, L_1, /*hidden argument*/NULL);
		Coroutine_t899D5232EF542CB8BA70AF9ECEECA494FAA9CCB7 * L_3;
		L_3 = MonoBehaviour_StartCoroutine_m3E33706D38B23CDD179E99BAD61E32303E9CC719(__this, L_2, /*hidden argument*/NULL);
		return;
	}
}
// System.Void CrazyMinnow.SALSA.RandomEyes3D::SetGroup(System.String,System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void RandomEyes3D_SetGroup_m36FB61B99AB52F36F2A82B7D61E2D71BB781FCC4 (RandomEyes3D_t6868157C4605BD0225E1B08301F110E72336CE45 * __this, String_t* ___groupName0, float ___duration1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_get_Count_m1A9C5C4BF43BFA79BD47ABB0D67817ACAC8C681F_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_get_Count_mCC4A67D6388864B347F3727B2A7172BB3E48AF13_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_get_Item_m6C9326C28DBBC58E66D0FDBC7DD547C22170CEB5_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_get_Item_mE46947FFA0AD2152DB5591A5A73B6338F9965EB0_RuntimeMethod_var);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	{
		V_0 = 0;
		goto IL_00a0;
	}

IL_0007:
	{
		List_1_t4CEBDA821397C4F46F5C0D752B030DA847F86F42 * L_0 = __this->get_groups_66();
		int32_t L_1 = V_0;
		NullCheck(L_0);
		Group_t0CC34C3639F4AF852CE020ABE05C91D971EBEDB1 * L_2;
		L_2 = List_1_get_Item_m6C9326C28DBBC58E66D0FDBC7DD547C22170CEB5_inline(L_0, L_1, /*hidden argument*/List_1_get_Item_m6C9326C28DBBC58E66D0FDBC7DD547C22170CEB5_RuntimeMethod_var);
		NullCheck(L_2);
		String_t* L_3 = L_2->get_name_0();
		String_t* L_4 = ___groupName0;
		bool L_5;
		L_5 = String_op_Equality_m2B91EE68355F142F67095973D32EB5828B7B73CB(L_3, L_4, /*hidden argument*/NULL);
		if (!L_5)
		{
			goto IL_009c;
		}
	}
	{
		V_1 = 0;
		goto IL_0083;
	}

IL_0024:
	{
		List_1_t4CEBDA821397C4F46F5C0D752B030DA847F86F42 * L_6 = __this->get_groups_66();
		int32_t L_7 = V_0;
		NullCheck(L_6);
		Group_t0CC34C3639F4AF852CE020ABE05C91D971EBEDB1 * L_8;
		L_8 = List_1_get_Item_m6C9326C28DBBC58E66D0FDBC7DD547C22170CEB5_inline(L_6, L_7, /*hidden argument*/List_1_get_Item_m6C9326C28DBBC58E66D0FDBC7DD547C22170CEB5_RuntimeMethod_var);
		NullCheck(L_8);
		List_1_t18177CFBF082431F1077382C8DEF236A68EE2240 * L_9 = L_8->get_shapes_3();
		int32_t L_10 = V_1;
		NullCheck(L_9);
		Shape_t15D1659B3662125F0DBAA6FE8F716125CF62AF2F * L_11;
		L_11 = List_1_get_Item_mE46947FFA0AD2152DB5591A5A73B6338F9965EB0_inline(L_9, L_10, /*hidden argument*/List_1_get_Item_mE46947FFA0AD2152DB5591A5A73B6338F9965EB0_RuntimeMethod_var);
		NullCheck(L_11);
		String_t* L_12 = L_11->get_name_1();
		List_1_t4CEBDA821397C4F46F5C0D752B030DA847F86F42 * L_13 = __this->get_groups_66();
		int32_t L_14 = V_0;
		NullCheck(L_13);
		Group_t0CC34C3639F4AF852CE020ABE05C91D971EBEDB1 * L_15;
		L_15 = List_1_get_Item_m6C9326C28DBBC58E66D0FDBC7DD547C22170CEB5_inline(L_13, L_14, /*hidden argument*/List_1_get_Item_m6C9326C28DBBC58E66D0FDBC7DD547C22170CEB5_RuntimeMethod_var);
		NullCheck(L_15);
		List_1_t18177CFBF082431F1077382C8DEF236A68EE2240 * L_16 = L_15->get_shapes_3();
		int32_t L_17 = V_1;
		NullCheck(L_16);
		Shape_t15D1659B3662125F0DBAA6FE8F716125CF62AF2F * L_18;
		L_18 = List_1_get_Item_mE46947FFA0AD2152DB5591A5A73B6338F9965EB0_inline(L_16, L_17, /*hidden argument*/List_1_get_Item_mE46947FFA0AD2152DB5591A5A73B6338F9965EB0_RuntimeMethod_var);
		NullCheck(L_18);
		float L_19 = L_18->get_blendSpeed_2();
		List_1_t4CEBDA821397C4F46F5C0D752B030DA847F86F42 * L_20 = __this->get_groups_66();
		int32_t L_21 = V_0;
		NullCheck(L_20);
		Group_t0CC34C3639F4AF852CE020ABE05C91D971EBEDB1 * L_22;
		L_22 = List_1_get_Item_m6C9326C28DBBC58E66D0FDBC7DD547C22170CEB5_inline(L_20, L_21, /*hidden argument*/List_1_get_Item_m6C9326C28DBBC58E66D0FDBC7DD547C22170CEB5_RuntimeMethod_var);
		NullCheck(L_22);
		List_1_t18177CFBF082431F1077382C8DEF236A68EE2240 * L_23 = L_22->get_shapes_3();
		int32_t L_24 = V_1;
		NullCheck(L_23);
		Shape_t15D1659B3662125F0DBAA6FE8F716125CF62AF2F * L_25;
		L_25 = List_1_get_Item_mE46947FFA0AD2152DB5591A5A73B6338F9965EB0_inline(L_23, L_24, /*hidden argument*/List_1_get_Item_mE46947FFA0AD2152DB5591A5A73B6338F9965EB0_RuntimeMethod_var);
		NullCheck(L_25);
		float L_26 = L_25->get_rangeOfMotion_3();
		float L_27 = ___duration1;
		RandomEyes3D_SetCustomShapeOverride_mAC24BBA3E20FC41A96F383097AD90C35E0EA7066(__this, L_12, L_19, L_26, L_27, /*hidden argument*/NULL);
		int32_t L_28 = V_1;
		V_1 = ((int32_t)il2cpp_codegen_add((int32_t)L_28, (int32_t)1));
	}

IL_0083:
	{
		int32_t L_29 = V_1;
		List_1_t4CEBDA821397C4F46F5C0D752B030DA847F86F42 * L_30 = __this->get_groups_66();
		int32_t L_31 = V_0;
		NullCheck(L_30);
		Group_t0CC34C3639F4AF852CE020ABE05C91D971EBEDB1 * L_32;
		L_32 = List_1_get_Item_m6C9326C28DBBC58E66D0FDBC7DD547C22170CEB5_inline(L_30, L_31, /*hidden argument*/List_1_get_Item_m6C9326C28DBBC58E66D0FDBC7DD547C22170CEB5_RuntimeMethod_var);
		NullCheck(L_32);
		List_1_t18177CFBF082431F1077382C8DEF236A68EE2240 * L_33 = L_32->get_shapes_3();
		NullCheck(L_33);
		int32_t L_34;
		L_34 = List_1_get_Count_mCC4A67D6388864B347F3727B2A7172BB3E48AF13_inline(L_33, /*hidden argument*/List_1_get_Count_mCC4A67D6388864B347F3727B2A7172BB3E48AF13_RuntimeMethod_var);
		if ((((int32_t)L_29) < ((int32_t)L_34)))
		{
			goto IL_0024;
		}
	}

IL_009c:
	{
		int32_t L_35 = V_0;
		V_0 = ((int32_t)il2cpp_codegen_add((int32_t)L_35, (int32_t)1));
	}

IL_00a0:
	{
		int32_t L_36 = V_0;
		List_1_t4CEBDA821397C4F46F5C0D752B030DA847F86F42 * L_37 = __this->get_groups_66();
		NullCheck(L_37);
		int32_t L_38;
		L_38 = List_1_get_Count_m1A9C5C4BF43BFA79BD47ABB0D67817ACAC8C681F_inline(L_37, /*hidden argument*/List_1_get_Count_m1A9C5C4BF43BFA79BD47ABB0D67817ACAC8C681F_RuntimeMethod_var);
		if ((((int32_t)L_36) < ((int32_t)L_38)))
		{
			goto IL_0007;
		}
	}
	{
		return;
	}
}
// System.Void CrazyMinnow.SALSA.RandomEyes3D::SetGroup(System.String,System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void RandomEyes3D_SetGroup_mED0933BCC89904D06A34464FFF8CF78FFD3B3B1E (RandomEyes3D_t6868157C4605BD0225E1B08301F110E72336CE45 * __this, String_t* ___groupName0, bool ___status1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_get_Count_m1A9C5C4BF43BFA79BD47ABB0D67817ACAC8C681F_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_get_Count_mCC4A67D6388864B347F3727B2A7172BB3E48AF13_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_get_Item_m6C9326C28DBBC58E66D0FDBC7DD547C22170CEB5_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_get_Item_mE46947FFA0AD2152DB5591A5A73B6338F9965EB0_RuntimeMethod_var);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	{
		V_0 = 0;
		goto IL_00a0;
	}

IL_0007:
	{
		List_1_t4CEBDA821397C4F46F5C0D752B030DA847F86F42 * L_0 = __this->get_groups_66();
		int32_t L_1 = V_0;
		NullCheck(L_0);
		Group_t0CC34C3639F4AF852CE020ABE05C91D971EBEDB1 * L_2;
		L_2 = List_1_get_Item_m6C9326C28DBBC58E66D0FDBC7DD547C22170CEB5_inline(L_0, L_1, /*hidden argument*/List_1_get_Item_m6C9326C28DBBC58E66D0FDBC7DD547C22170CEB5_RuntimeMethod_var);
		NullCheck(L_2);
		String_t* L_3 = L_2->get_name_0();
		String_t* L_4 = ___groupName0;
		bool L_5;
		L_5 = String_op_Equality_m2B91EE68355F142F67095973D32EB5828B7B73CB(L_3, L_4, /*hidden argument*/NULL);
		if (!L_5)
		{
			goto IL_009c;
		}
	}
	{
		V_1 = 0;
		goto IL_0083;
	}

IL_0024:
	{
		List_1_t4CEBDA821397C4F46F5C0D752B030DA847F86F42 * L_6 = __this->get_groups_66();
		int32_t L_7 = V_0;
		NullCheck(L_6);
		Group_t0CC34C3639F4AF852CE020ABE05C91D971EBEDB1 * L_8;
		L_8 = List_1_get_Item_m6C9326C28DBBC58E66D0FDBC7DD547C22170CEB5_inline(L_6, L_7, /*hidden argument*/List_1_get_Item_m6C9326C28DBBC58E66D0FDBC7DD547C22170CEB5_RuntimeMethod_var);
		NullCheck(L_8);
		List_1_t18177CFBF082431F1077382C8DEF236A68EE2240 * L_9 = L_8->get_shapes_3();
		int32_t L_10 = V_1;
		NullCheck(L_9);
		Shape_t15D1659B3662125F0DBAA6FE8F716125CF62AF2F * L_11;
		L_11 = List_1_get_Item_mE46947FFA0AD2152DB5591A5A73B6338F9965EB0_inline(L_9, L_10, /*hidden argument*/List_1_get_Item_mE46947FFA0AD2152DB5591A5A73B6338F9965EB0_RuntimeMethod_var);
		NullCheck(L_11);
		String_t* L_12 = L_11->get_name_1();
		List_1_t4CEBDA821397C4F46F5C0D752B030DA847F86F42 * L_13 = __this->get_groups_66();
		int32_t L_14 = V_0;
		NullCheck(L_13);
		Group_t0CC34C3639F4AF852CE020ABE05C91D971EBEDB1 * L_15;
		L_15 = List_1_get_Item_m6C9326C28DBBC58E66D0FDBC7DD547C22170CEB5_inline(L_13, L_14, /*hidden argument*/List_1_get_Item_m6C9326C28DBBC58E66D0FDBC7DD547C22170CEB5_RuntimeMethod_var);
		NullCheck(L_15);
		List_1_t18177CFBF082431F1077382C8DEF236A68EE2240 * L_16 = L_15->get_shapes_3();
		int32_t L_17 = V_1;
		NullCheck(L_16);
		Shape_t15D1659B3662125F0DBAA6FE8F716125CF62AF2F * L_18;
		L_18 = List_1_get_Item_mE46947FFA0AD2152DB5591A5A73B6338F9965EB0_inline(L_16, L_17, /*hidden argument*/List_1_get_Item_mE46947FFA0AD2152DB5591A5A73B6338F9965EB0_RuntimeMethod_var);
		NullCheck(L_18);
		float L_19 = L_18->get_blendSpeed_2();
		List_1_t4CEBDA821397C4F46F5C0D752B030DA847F86F42 * L_20 = __this->get_groups_66();
		int32_t L_21 = V_0;
		NullCheck(L_20);
		Group_t0CC34C3639F4AF852CE020ABE05C91D971EBEDB1 * L_22;
		L_22 = List_1_get_Item_m6C9326C28DBBC58E66D0FDBC7DD547C22170CEB5_inline(L_20, L_21, /*hidden argument*/List_1_get_Item_m6C9326C28DBBC58E66D0FDBC7DD547C22170CEB5_RuntimeMethod_var);
		NullCheck(L_22);
		List_1_t18177CFBF082431F1077382C8DEF236A68EE2240 * L_23 = L_22->get_shapes_3();
		int32_t L_24 = V_1;
		NullCheck(L_23);
		Shape_t15D1659B3662125F0DBAA6FE8F716125CF62AF2F * L_25;
		L_25 = List_1_get_Item_mE46947FFA0AD2152DB5591A5A73B6338F9965EB0_inline(L_23, L_24, /*hidden argument*/List_1_get_Item_mE46947FFA0AD2152DB5591A5A73B6338F9965EB0_RuntimeMethod_var);
		NullCheck(L_25);
		float L_26 = L_25->get_rangeOfMotion_3();
		bool L_27 = ___status1;
		RandomEyes3D_SetCustomShapeOverride_m9C8E2FDBF2AC9F768DADF1EF2853823C01937A52(__this, L_12, L_19, L_26, L_27, /*hidden argument*/NULL);
		int32_t L_28 = V_1;
		V_1 = ((int32_t)il2cpp_codegen_add((int32_t)L_28, (int32_t)1));
	}

IL_0083:
	{
		int32_t L_29 = V_1;
		List_1_t4CEBDA821397C4F46F5C0D752B030DA847F86F42 * L_30 = __this->get_groups_66();
		int32_t L_31 = V_0;
		NullCheck(L_30);
		Group_t0CC34C3639F4AF852CE020ABE05C91D971EBEDB1 * L_32;
		L_32 = List_1_get_Item_m6C9326C28DBBC58E66D0FDBC7DD547C22170CEB5_inline(L_30, L_31, /*hidden argument*/List_1_get_Item_m6C9326C28DBBC58E66D0FDBC7DD547C22170CEB5_RuntimeMethod_var);
		NullCheck(L_32);
		List_1_t18177CFBF082431F1077382C8DEF236A68EE2240 * L_33 = L_32->get_shapes_3();
		NullCheck(L_33);
		int32_t L_34;
		L_34 = List_1_get_Count_mCC4A67D6388864B347F3727B2A7172BB3E48AF13_inline(L_33, /*hidden argument*/List_1_get_Count_mCC4A67D6388864B347F3727B2A7172BB3E48AF13_RuntimeMethod_var);
		if ((((int32_t)L_29) < ((int32_t)L_34)))
		{
			goto IL_0024;
		}
	}

IL_009c:
	{
		int32_t L_35 = V_0;
		V_0 = ((int32_t)il2cpp_codegen_add((int32_t)L_35, (int32_t)1));
	}

IL_00a0:
	{
		int32_t L_36 = V_0;
		List_1_t4CEBDA821397C4F46F5C0D752B030DA847F86F42 * L_37 = __this->get_groups_66();
		NullCheck(L_37);
		int32_t L_38;
		L_38 = List_1_get_Count_m1A9C5C4BF43BFA79BD47ABB0D67817ACAC8C681F_inline(L_37, /*hidden argument*/List_1_get_Count_m1A9C5C4BF43BFA79BD47ABB0D67817ACAC8C681F_RuntimeMethod_var);
		if ((((int32_t)L_36) < ((int32_t)L_38)))
		{
			goto IL_0007;
		}
	}
	{
		return;
	}
}
// System.Void CrazyMinnow.SALSA.RandomEyes3D::GetBlendShapes()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void RandomEyes3D_GetBlendShapes_mF9F69C143A9AF121E534B05AC0F6EAD505765562 (RandomEyes3D_t6868157C4605BD0225E1B08301F110E72336CE45 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	{
		SkinnedMeshRenderer_t126F4D6010E0F4B2685A7817B0A9171805D8F496 * L_0 = __this->get_skinnedMeshRenderer_6();
		IL2CPP_RUNTIME_CLASS_INIT(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		bool L_1;
		L_1 = Object_op_Implicit_mC8214E4F028CC2F036CC82BDB81D102A02893499(L_0, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0054;
		}
	}
	{
		SkinnedMeshRenderer_t126F4D6010E0F4B2685A7817B0A9171805D8F496 * L_2 = __this->get_skinnedMeshRenderer_6();
		NullCheck(L_2);
		Mesh_t2F5992DBA650D5862B43D3823ACD997132A57DA6 * L_3;
		L_3 = SkinnedMeshRenderer_get_sharedMesh_mFD55E307943C1C4B2E2E8632F15B41CCBD8D91F2(L_2, /*hidden argument*/NULL);
		NullCheck(L_3);
		int32_t L_4;
		L_4 = Mesh_get_blendShapeCount_mC80CCEA555E9E5609E3497EECF2B03F9B822CB77(L_3, /*hidden argument*/NULL);
		StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* L_5 = (StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A*)(StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A*)SZArrayNew(StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A_il2cpp_TypeInfo_var, (uint32_t)L_4);
		__this->set_blendShapes_9(L_5);
		V_0 = 0;
		goto IL_0049;
	}

IL_002c:
	{
		StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* L_6 = __this->get_blendShapes_9();
		int32_t L_7 = V_0;
		SkinnedMeshRenderer_t126F4D6010E0F4B2685A7817B0A9171805D8F496 * L_8 = __this->get_skinnedMeshRenderer_6();
		NullCheck(L_8);
		Mesh_t2F5992DBA650D5862B43D3823ACD997132A57DA6 * L_9;
		L_9 = SkinnedMeshRenderer_get_sharedMesh_mFD55E307943C1C4B2E2E8632F15B41CCBD8D91F2(L_8, /*hidden argument*/NULL);
		int32_t L_10 = V_0;
		NullCheck(L_9);
		String_t* L_11;
		L_11 = Mesh_GetBlendShapeName_mF28E636D7F66608E6FD9152ACD9547B78C895000(L_9, L_10, /*hidden argument*/NULL);
		NullCheck(L_6);
		ArrayElementTypeCheck (L_6, L_11);
		(L_6)->SetAt(static_cast<il2cpp_array_size_t>(L_7), (String_t*)L_11);
		int32_t L_12 = V_0;
		V_0 = ((int32_t)il2cpp_codegen_add((int32_t)L_12, (int32_t)1));
	}

IL_0049:
	{
		int32_t L_13 = V_0;
		StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* L_14 = __this->get_blendShapes_9();
		NullCheck(L_14);
		if ((((int32_t)L_13) < ((int32_t)((int32_t)((int32_t)(((RuntimeArray*)L_14)->max_length))))))
		{
			goto IL_002c;
		}
	}

IL_0054:
	{
		return;
	}
}
// System.Void CrazyMinnow.SALSA.RandomEyes3D::AutoLinkEyes()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void RandomEyes3D_AutoLinkEyes_m7AFABE4BEF07CBD9C860F545823CC8856A3B08A4 (RandomEyes3D_t6868157C4605BD0225E1B08301F110E72336CE45 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral2AF10F459E51945366EFDDFD0D2108598B13F7DD);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral63F369BAD251E7C89E2DB1884C363A04040FF39D);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral8B50F11C361CC9D4C94D35E40C9AE333832AC31B);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralC2D834C11C0F2F1BF471E704863B12385F9A7838);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralFCD4948C0020C53A3488DFC4A7D87786B7F6EC48);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	{
		SkinnedMeshRenderer_t126F4D6010E0F4B2685A7817B0A9171805D8F496 * L_0 = __this->get_skinnedMeshRenderer_6();
		IL2CPP_RUNTIME_CLASS_INIT(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		bool L_1;
		L_1 = Object_op_Implicit_mC8214E4F028CC2F036CC82BDB81D102A02893499(L_0, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_011e;
		}
	}
	{
		StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* L_2 = __this->get_blendShapes_9();
		if (L_2)
		{
			goto IL_001e;
		}
	}
	{
		RandomEyes3D_GetBlendShapes_mF9F69C143A9AF121E534B05AC0F6EAD505765562(__this, /*hidden argument*/NULL);
	}

IL_001e:
	{
		StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* L_3 = __this->get_blendShapes_9();
		if (!L_3)
		{
			goto IL_0036;
		}
	}
	{
		StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* L_4 = __this->get_blendShapes_9();
		NullCheck(L_4);
		if (((int32_t)((int32_t)(((RuntimeArray*)L_4)->max_length))))
		{
			goto IL_0036;
		}
	}
	{
		RandomEyes3D_GetBlendShapes_mF9F69C143A9AF121E534B05AC0F6EAD505765562(__this, /*hidden argument*/NULL);
	}

IL_0036:
	{
		V_0 = 0;
		goto IL_0110;
	}

IL_003d:
	{
		StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* L_5 = __this->get_blendShapes_9();
		int32_t L_6 = V_0;
		NullCheck(L_5);
		int32_t L_7 = L_6;
		String_t* L_8 = (L_5)->GetAt(static_cast<il2cpp_array_size_t>(L_7));
		NullCheck(L_8);
		int32_t L_9;
		L_9 = String_get_Length_m129FC0ADA02FECBED3C0B1A809AE84A5AEE1CF09_inline(L_8, /*hidden argument*/NULL);
		if ((((int32_t)L_9) < ((int32_t)5)))
		{
			goto IL_010c;
		}
	}
	{
		StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* L_10 = __this->get_blendShapes_9();
		int32_t L_11 = V_0;
		NullCheck(L_10);
		int32_t L_12 = L_11;
		String_t* L_13 = (L_10)->GetAt(static_cast<il2cpp_array_size_t>(L_12));
		NullCheck(L_13);
		String_t* L_14;
		L_14 = String_ToLower_m7875A49FE166D0A68F3F6B6E70C0C056EBEFD31D(L_13, /*hidden argument*/NULL);
		NullCheck(L_14);
		String_t* L_15;
		L_15 = String_Substring_m7A39A2AC0893AE940CF4CEC841326D56FFB9D86B(L_14, 0, 5, /*hidden argument*/NULL);
		bool L_16;
		L_16 = String_op_Equality_m2B91EE68355F142F67095973D32EB5828B7B73CB(L_15, _stringLiteralFCD4948C0020C53A3488DFC4A7D87786B7F6EC48, /*hidden argument*/NULL);
		if (!L_16)
		{
			goto IL_0077;
		}
	}
	{
		int32_t L_17 = V_0;
		__this->set_lookUpIndex_10(L_17);
	}

IL_0077:
	{
		StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* L_18 = __this->get_blendShapes_9();
		int32_t L_19 = V_0;
		NullCheck(L_18);
		int32_t L_20 = L_19;
		String_t* L_21 = (L_18)->GetAt(static_cast<il2cpp_array_size_t>(L_20));
		NullCheck(L_21);
		String_t* L_22;
		L_22 = String_ToLower_m7875A49FE166D0A68F3F6B6E70C0C056EBEFD31D(L_21, /*hidden argument*/NULL);
		NullCheck(L_22);
		String_t* L_23;
		L_23 = String_Substring_m7A39A2AC0893AE940CF4CEC841326D56FFB9D86B(L_22, 0, 5, /*hidden argument*/NULL);
		bool L_24;
		L_24 = String_op_Equality_m2B91EE68355F142F67095973D32EB5828B7B73CB(L_23, _stringLiteralC2D834C11C0F2F1BF471E704863B12385F9A7838, /*hidden argument*/NULL);
		if (!L_24)
		{
			goto IL_009e;
		}
	}
	{
		int32_t L_25 = V_0;
		__this->set_lookDownIndex_11(L_25);
	}

IL_009e:
	{
		StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* L_26 = __this->get_blendShapes_9();
		int32_t L_27 = V_0;
		NullCheck(L_26);
		int32_t L_28 = L_27;
		String_t* L_29 = (L_26)->GetAt(static_cast<il2cpp_array_size_t>(L_28));
		NullCheck(L_29);
		String_t* L_30;
		L_30 = String_ToLower_m7875A49FE166D0A68F3F6B6E70C0C056EBEFD31D(L_29, /*hidden argument*/NULL);
		NullCheck(L_30);
		String_t* L_31;
		L_31 = String_Substring_m7A39A2AC0893AE940CF4CEC841326D56FFB9D86B(L_30, 0, 5, /*hidden argument*/NULL);
		bool L_32;
		L_32 = String_op_Equality_m2B91EE68355F142F67095973D32EB5828B7B73CB(L_31, _stringLiteral8B50F11C361CC9D4C94D35E40C9AE333832AC31B, /*hidden argument*/NULL);
		if (!L_32)
		{
			goto IL_00c5;
		}
	}
	{
		int32_t L_33 = V_0;
		__this->set_lookLeftIndex_12(L_33);
	}

IL_00c5:
	{
		StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* L_34 = __this->get_blendShapes_9();
		int32_t L_35 = V_0;
		NullCheck(L_34);
		int32_t L_36 = L_35;
		String_t* L_37 = (L_34)->GetAt(static_cast<il2cpp_array_size_t>(L_36));
		NullCheck(L_37);
		String_t* L_38;
		L_38 = String_ToLower_m7875A49FE166D0A68F3F6B6E70C0C056EBEFD31D(L_37, /*hidden argument*/NULL);
		NullCheck(L_38);
		String_t* L_39;
		L_39 = String_Substring_m7A39A2AC0893AE940CF4CEC841326D56FFB9D86B(L_38, 0, 5, /*hidden argument*/NULL);
		bool L_40;
		L_40 = String_op_Equality_m2B91EE68355F142F67095973D32EB5828B7B73CB(L_39, _stringLiteral63F369BAD251E7C89E2DB1884C363A04040FF39D, /*hidden argument*/NULL);
		if (!L_40)
		{
			goto IL_00ec;
		}
	}
	{
		int32_t L_41 = V_0;
		__this->set_lookRightIndex_13(L_41);
	}

IL_00ec:
	{
		StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* L_42 = __this->get_blendShapes_9();
		int32_t L_43 = V_0;
		NullCheck(L_42);
		int32_t L_44 = L_43;
		String_t* L_45 = (L_42)->GetAt(static_cast<il2cpp_array_size_t>(L_44));
		NullCheck(L_45);
		String_t* L_46;
		L_46 = String_ToLower_m7875A49FE166D0A68F3F6B6E70C0C056EBEFD31D(L_45, /*hidden argument*/NULL);
		bool L_47;
		L_47 = String_op_Equality_m2B91EE68355F142F67095973D32EB5828B7B73CB(L_46, _stringLiteral2AF10F459E51945366EFDDFD0D2108598B13F7DD, /*hidden argument*/NULL);
		if (!L_47)
		{
			goto IL_010c;
		}
	}
	{
		int32_t L_48 = V_0;
		__this->set_blinkIndex_14(L_48);
	}

IL_010c:
	{
		int32_t L_49 = V_0;
		V_0 = ((int32_t)il2cpp_codegen_add((int32_t)L_49, (int32_t)1));
	}

IL_0110:
	{
		int32_t L_50 = V_0;
		StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* L_51 = __this->get_blendShapes_9();
		NullCheck(L_51);
		if ((((int32_t)L_50) < ((int32_t)((int32_t)((int32_t)(((RuntimeArray*)L_51)->max_length))))))
		{
			goto IL_003d;
		}
	}

IL_011e:
	{
		return;
	}
}
// System.Void CrazyMinnow.SALSA.RandomEyes3D::AutoLinkCustomShapes(System.Boolean,CrazyMinnow.SALSA.Salsa3D)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void RandomEyes3D_AutoLinkCustomShapes_m9D8A263AA469EDC6301A778EFCAF60B3BFD34359 (RandomEyes3D_t6868157C4605BD0225E1B08301F110E72336CE45 * __this, bool ___excludeEyeShapes0, Salsa3D_tACF83D529139263662FEAAC8F5B53EC43B140C9C * ___excludeSalsaShapes1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_Add_mEE653047BDB3486ACC2E16DC6C3422A0BA48F01F_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_RemoveAt_m4A6ABD183823501A4F9A6082D9EDC589029AD221_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1__ctor_m45E78772E9157F6CD684A69AAB07CE4082FE5FFD_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_get_Count_m7FA90926D9267868473EF90941F6BF794EC87FF2_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_get_Item_m730FCAD2646FA94B07D1216A512B09AB9F0BBA5D_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_t260B41F956D673396C33A4CF94E8D6C4389EACB7_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&RandomEyesCustomShapeU5BU5D_t8FB0C62923194ED52AE0B06A6249680C47CA152C_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&RandomEyesCustomShape_t10BCA0CEC18C2BBF6A8BD7BFAD2AF005D233C354_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	List_1_t260B41F956D673396C33A4CF94E8D6C4389EACB7 * V_0 = NULL;
	int32_t V_1 = 0;
	int32_t V_2 = 0;
	int32_t V_3 = 0;
	int32_t V_4 = 0;
	int32_t V_5 = 0;
	int32_t V_6 = 0;
	{
		bool L_0 = ___excludeEyeShapes0;
		RandomEyes3D_SetCustomShapeExcludeEyes_m2907E3D9B857200BD4368EB62CCC3A0B4D51C436_inline(__this, L_0, /*hidden argument*/NULL);
		Salsa3D_tACF83D529139263662FEAAC8F5B53EC43B140C9C * L_1 = ___excludeSalsaShapes1;
		RandomEyes3D_SetCustomShapeExcludeMouth_m3063985221F372102DC1B3854A31D75F95655684_inline(__this, L_1, /*hidden argument*/NULL);
		List_1_t260B41F956D673396C33A4CF94E8D6C4389EACB7 * L_2 = (List_1_t260B41F956D673396C33A4CF94E8D6C4389EACB7 *)il2cpp_codegen_object_new(List_1_t260B41F956D673396C33A4CF94E8D6C4389EACB7_il2cpp_TypeInfo_var);
		List_1__ctor_m45E78772E9157F6CD684A69AAB07CE4082FE5FFD(L_2, /*hidden argument*/List_1__ctor_m45E78772E9157F6CD684A69AAB07CE4082FE5FFD_RuntimeMethod_var);
		V_0 = L_2;
		bool L_3 = __this->get_excludeEyeShapes_44();
		if (L_3)
		{
			goto IL_002c;
		}
	}
	{
		Salsa3D_tACF83D529139263662FEAAC8F5B53EC43B140C9C * L_4 = __this->get_salsa3D_5();
		IL2CPP_RUNTIME_CLASS_INIT(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		bool L_5;
		L_5 = Object_op_Implicit_mC8214E4F028CC2F036CC82BDB81D102A02893499(L_4, /*hidden argument*/NULL);
		if (!L_5)
		{
			goto IL_02bb;
		}
	}

IL_002c:
	{
		SkinnedMeshRenderer_t126F4D6010E0F4B2685A7817B0A9171805D8F496 * L_6 = __this->get_skinnedMeshRenderer_6();
		IL2CPP_RUNTIME_CLASS_INIT(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		bool L_7;
		L_7 = Object_op_Implicit_mC8214E4F028CC2F036CC82BDB81D102A02893499(L_6, /*hidden argument*/NULL);
		if (!L_7)
		{
			goto IL_005b;
		}
	}
	{
		V_1 = 0;
		goto IL_0048;
	}

IL_003d:
	{
		List_1_t260B41F956D673396C33A4CF94E8D6C4389EACB7 * L_8 = V_0;
		int32_t L_9 = V_1;
		NullCheck(L_8);
		List_1_Add_mEE653047BDB3486ACC2E16DC6C3422A0BA48F01F(L_8, L_9, /*hidden argument*/List_1_Add_mEE653047BDB3486ACC2E16DC6C3422A0BA48F01F_RuntimeMethod_var);
		int32_t L_10 = V_1;
		V_1 = ((int32_t)il2cpp_codegen_add((int32_t)L_10, (int32_t)1));
	}

IL_0048:
	{
		int32_t L_11 = V_1;
		SkinnedMeshRenderer_t126F4D6010E0F4B2685A7817B0A9171805D8F496 * L_12 = __this->get_skinnedMeshRenderer_6();
		NullCheck(L_12);
		Mesh_t2F5992DBA650D5862B43D3823ACD997132A57DA6 * L_13;
		L_13 = SkinnedMeshRenderer_get_sharedMesh_mFD55E307943C1C4B2E2E8632F15B41CCBD8D91F2(L_12, /*hidden argument*/NULL);
		NullCheck(L_13);
		int32_t L_14;
		L_14 = Mesh_get_blendShapeCount_mC80CCEA555E9E5609E3497EECF2B03F9B822CB77(L_13, /*hidden argument*/NULL);
		if ((((int32_t)L_11) < ((int32_t)L_14)))
		{
			goto IL_003d;
		}
	}

IL_005b:
	{
		bool L_15 = __this->get_excludeEyeShapes_44();
		if (!L_15)
		{
			goto IL_013b;
		}
	}
	{
		SkinnedMeshRenderer_t126F4D6010E0F4B2685A7817B0A9171805D8F496 * L_16 = __this->get_skinnedMeshRenderer_6();
		IL2CPP_RUNTIME_CLASS_INIT(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		bool L_17;
		L_17 = Object_op_Implicit_mC8214E4F028CC2F036CC82BDB81D102A02893499(L_16, /*hidden argument*/NULL);
		if (!L_17)
		{
			goto IL_013b;
		}
	}
	{
		int32_t L_18 = __this->get_lookUpIndex_10();
		int32_t L_19 = __this->get_lookDownIndex_11();
		if ((((int32_t)L_18) == ((int32_t)L_19)))
		{
			goto IL_013b;
		}
	}
	{
		int32_t L_20 = __this->get_lookUpIndex_10();
		int32_t L_21 = __this->get_lookLeftIndex_12();
		if ((((int32_t)L_20) == ((int32_t)L_21)))
		{
			goto IL_013b;
		}
	}
	{
		int32_t L_22 = __this->get_lookUpIndex_10();
		int32_t L_23 = __this->get_lookRightIndex_13();
		if ((((int32_t)L_22) == ((int32_t)L_23)))
		{
			goto IL_013b;
		}
	}
	{
		int32_t L_24 = __this->get_lookUpIndex_10();
		int32_t L_25 = __this->get_blinkIndex_14();
		if ((((int32_t)L_24) == ((int32_t)L_25)))
		{
			goto IL_013b;
		}
	}
	{
		List_1_t260B41F956D673396C33A4CF94E8D6C4389EACB7 * L_26 = V_0;
		NullCheck(L_26);
		int32_t L_27;
		L_27 = List_1_get_Count_m7FA90926D9267868473EF90941F6BF794EC87FF2_inline(L_26, /*hidden argument*/List_1_get_Count_m7FA90926D9267868473EF90941F6BF794EC87FF2_RuntimeMethod_var);
		V_2 = ((int32_t)il2cpp_codegen_subtract((int32_t)L_27, (int32_t)1));
		goto IL_0137;
	}

IL_00c5:
	{
		int32_t L_28 = V_2;
		int32_t L_29 = __this->get_lookUpIndex_10();
		if ((!(((uint32_t)L_28) == ((uint32_t)L_29))))
		{
			goto IL_00db;
		}
	}
	{
		List_1_t260B41F956D673396C33A4CF94E8D6C4389EACB7 * L_30 = V_0;
		List_1_t260B41F956D673396C33A4CF94E8D6C4389EACB7 * L_31 = V_0;
		int32_t L_32 = V_2;
		NullCheck(L_31);
		int32_t L_33;
		L_33 = List_1_get_Item_m730FCAD2646FA94B07D1216A512B09AB9F0BBA5D_inline(L_31, L_32, /*hidden argument*/List_1_get_Item_m730FCAD2646FA94B07D1216A512B09AB9F0BBA5D_RuntimeMethod_var);
		NullCheck(L_30);
		List_1_RemoveAt_m4A6ABD183823501A4F9A6082D9EDC589029AD221(L_30, L_33, /*hidden argument*/List_1_RemoveAt_m4A6ABD183823501A4F9A6082D9EDC589029AD221_RuntimeMethod_var);
	}

IL_00db:
	{
		int32_t L_34 = V_2;
		int32_t L_35 = __this->get_lookDownIndex_11();
		if ((!(((uint32_t)L_34) == ((uint32_t)L_35))))
		{
			goto IL_00f1;
		}
	}
	{
		List_1_t260B41F956D673396C33A4CF94E8D6C4389EACB7 * L_36 = V_0;
		List_1_t260B41F956D673396C33A4CF94E8D6C4389EACB7 * L_37 = V_0;
		int32_t L_38 = V_2;
		NullCheck(L_37);
		int32_t L_39;
		L_39 = List_1_get_Item_m730FCAD2646FA94B07D1216A512B09AB9F0BBA5D_inline(L_37, L_38, /*hidden argument*/List_1_get_Item_m730FCAD2646FA94B07D1216A512B09AB9F0BBA5D_RuntimeMethod_var);
		NullCheck(L_36);
		List_1_RemoveAt_m4A6ABD183823501A4F9A6082D9EDC589029AD221(L_36, L_39, /*hidden argument*/List_1_RemoveAt_m4A6ABD183823501A4F9A6082D9EDC589029AD221_RuntimeMethod_var);
	}

IL_00f1:
	{
		int32_t L_40 = V_2;
		int32_t L_41 = __this->get_lookLeftIndex_12();
		if ((!(((uint32_t)L_40) == ((uint32_t)L_41))))
		{
			goto IL_0107;
		}
	}
	{
		List_1_t260B41F956D673396C33A4CF94E8D6C4389EACB7 * L_42 = V_0;
		List_1_t260B41F956D673396C33A4CF94E8D6C4389EACB7 * L_43 = V_0;
		int32_t L_44 = V_2;
		NullCheck(L_43);
		int32_t L_45;
		L_45 = List_1_get_Item_m730FCAD2646FA94B07D1216A512B09AB9F0BBA5D_inline(L_43, L_44, /*hidden argument*/List_1_get_Item_m730FCAD2646FA94B07D1216A512B09AB9F0BBA5D_RuntimeMethod_var);
		NullCheck(L_42);
		List_1_RemoveAt_m4A6ABD183823501A4F9A6082D9EDC589029AD221(L_42, L_45, /*hidden argument*/List_1_RemoveAt_m4A6ABD183823501A4F9A6082D9EDC589029AD221_RuntimeMethod_var);
	}

IL_0107:
	{
		int32_t L_46 = V_2;
		int32_t L_47 = __this->get_lookRightIndex_13();
		if ((!(((uint32_t)L_46) == ((uint32_t)L_47))))
		{
			goto IL_011d;
		}
	}
	{
		List_1_t260B41F956D673396C33A4CF94E8D6C4389EACB7 * L_48 = V_0;
		List_1_t260B41F956D673396C33A4CF94E8D6C4389EACB7 * L_49 = V_0;
		int32_t L_50 = V_2;
		NullCheck(L_49);
		int32_t L_51;
		L_51 = List_1_get_Item_m730FCAD2646FA94B07D1216A512B09AB9F0BBA5D_inline(L_49, L_50, /*hidden argument*/List_1_get_Item_m730FCAD2646FA94B07D1216A512B09AB9F0BBA5D_RuntimeMethod_var);
		NullCheck(L_48);
		List_1_RemoveAt_m4A6ABD183823501A4F9A6082D9EDC589029AD221(L_48, L_51, /*hidden argument*/List_1_RemoveAt_m4A6ABD183823501A4F9A6082D9EDC589029AD221_RuntimeMethod_var);
	}

IL_011d:
	{
		int32_t L_52 = V_2;
		int32_t L_53 = __this->get_blinkIndex_14();
		if ((!(((uint32_t)L_52) == ((uint32_t)L_53))))
		{
			goto IL_0133;
		}
	}
	{
		List_1_t260B41F956D673396C33A4CF94E8D6C4389EACB7 * L_54 = V_0;
		List_1_t260B41F956D673396C33A4CF94E8D6C4389EACB7 * L_55 = V_0;
		int32_t L_56 = V_2;
		NullCheck(L_55);
		int32_t L_57;
		L_57 = List_1_get_Item_m730FCAD2646FA94B07D1216A512B09AB9F0BBA5D_inline(L_55, L_56, /*hidden argument*/List_1_get_Item_m730FCAD2646FA94B07D1216A512B09AB9F0BBA5D_RuntimeMethod_var);
		NullCheck(L_54);
		List_1_RemoveAt_m4A6ABD183823501A4F9A6082D9EDC589029AD221(L_54, L_57, /*hidden argument*/List_1_RemoveAt_m4A6ABD183823501A4F9A6082D9EDC589029AD221_RuntimeMethod_var);
	}

IL_0133:
	{
		int32_t L_58 = V_2;
		V_2 = ((int32_t)il2cpp_codegen_subtract((int32_t)L_58, (int32_t)1));
	}

IL_0137:
	{
		int32_t L_59 = V_2;
		if ((((int32_t)L_59) >= ((int32_t)0)))
		{
			goto IL_00c5;
		}
	}

IL_013b:
	{
		Salsa3D_tACF83D529139263662FEAAC8F5B53EC43B140C9C * L_60 = __this->get_salsa3D_5();
		IL2CPP_RUNTIME_CLASS_INIT(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		bool L_61;
		L_61 = Object_op_Implicit_mC8214E4F028CC2F036CC82BDB81D102A02893499(L_60, /*hidden argument*/NULL);
		if (!L_61)
		{
			goto IL_020f;
		}
	}
	{
		Salsa3D_tACF83D529139263662FEAAC8F5B53EC43B140C9C * L_62 = __this->get_salsa3D_5();
		NullCheck(L_62);
		SkinnedMeshRenderer_t126F4D6010E0F4B2685A7817B0A9171805D8F496 * L_63 = L_62->get_skinnedMeshRenderer_5();
		IL2CPP_RUNTIME_CLASS_INIT(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		bool L_64;
		L_64 = Object_op_Implicit_mC8214E4F028CC2F036CC82BDB81D102A02893499(L_63, /*hidden argument*/NULL);
		if (!L_64)
		{
			goto IL_020f;
		}
	}
	{
		SkinnedMeshRenderer_t126F4D6010E0F4B2685A7817B0A9171805D8F496 * L_65 = __this->get_skinnedMeshRenderer_6();
		Salsa3D_tACF83D529139263662FEAAC8F5B53EC43B140C9C * L_66 = __this->get_salsa3D_5();
		NullCheck(L_66);
		SkinnedMeshRenderer_t126F4D6010E0F4B2685A7817B0A9171805D8F496 * L_67 = L_66->get_skinnedMeshRenderer_5();
		IL2CPP_RUNTIME_CLASS_INIT(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		bool L_68;
		L_68 = Object_op_Equality_mEE9EC7EB5C7DC3E95B94AB904E1986FC4D566D54(L_65, L_67, /*hidden argument*/NULL);
		if (!L_68)
		{
			goto IL_020f;
		}
	}
	{
		Salsa3D_tACF83D529139263662FEAAC8F5B53EC43B140C9C * L_69 = __this->get_salsa3D_5();
		NullCheck(L_69);
		int32_t L_70 = L_69->get_saySmallIndex_11();
		Salsa3D_tACF83D529139263662FEAAC8F5B53EC43B140C9C * L_71 = __this->get_salsa3D_5();
		NullCheck(L_71);
		int32_t L_72 = L_71->get_sayMediumIndex_12();
		if ((((int32_t)L_70) == ((int32_t)L_72)))
		{
			goto IL_020f;
		}
	}
	{
		Salsa3D_tACF83D529139263662FEAAC8F5B53EC43B140C9C * L_73 = __this->get_salsa3D_5();
		NullCheck(L_73);
		int32_t L_74 = L_73->get_saySmallIndex_11();
		Salsa3D_tACF83D529139263662FEAAC8F5B53EC43B140C9C * L_75 = __this->get_salsa3D_5();
		NullCheck(L_75);
		int32_t L_76 = L_75->get_sayLargeIndex_13();
		if ((((int32_t)L_74) == ((int32_t)L_76)))
		{
			goto IL_020f;
		}
	}
	{
		List_1_t260B41F956D673396C33A4CF94E8D6C4389EACB7 * L_77 = V_0;
		NullCheck(L_77);
		int32_t L_78;
		L_78 = List_1_get_Count_m7FA90926D9267868473EF90941F6BF794EC87FF2_inline(L_77, /*hidden argument*/List_1_get_Count_m7FA90926D9267868473EF90941F6BF794EC87FF2_RuntimeMethod_var);
		V_3 = ((int32_t)il2cpp_codegen_subtract((int32_t)L_78, (int32_t)1));
		goto IL_020b;
	}

IL_01b6:
	{
		int32_t L_79 = V_3;
		Salsa3D_tACF83D529139263662FEAAC8F5B53EC43B140C9C * L_80 = __this->get_salsa3D_5();
		NullCheck(L_80);
		int32_t L_81 = L_80->get_saySmallIndex_11();
		if ((!(((uint32_t)L_79) == ((uint32_t)L_81))))
		{
			goto IL_01d1;
		}
	}
	{
		List_1_t260B41F956D673396C33A4CF94E8D6C4389EACB7 * L_82 = V_0;
		List_1_t260B41F956D673396C33A4CF94E8D6C4389EACB7 * L_83 = V_0;
		int32_t L_84 = V_3;
		NullCheck(L_83);
		int32_t L_85;
		L_85 = List_1_get_Item_m730FCAD2646FA94B07D1216A512B09AB9F0BBA5D_inline(L_83, L_84, /*hidden argument*/List_1_get_Item_m730FCAD2646FA94B07D1216A512B09AB9F0BBA5D_RuntimeMethod_var);
		NullCheck(L_82);
		List_1_RemoveAt_m4A6ABD183823501A4F9A6082D9EDC589029AD221(L_82, L_85, /*hidden argument*/List_1_RemoveAt_m4A6ABD183823501A4F9A6082D9EDC589029AD221_RuntimeMethod_var);
	}

IL_01d1:
	{
		int32_t L_86 = V_3;
		Salsa3D_tACF83D529139263662FEAAC8F5B53EC43B140C9C * L_87 = __this->get_salsa3D_5();
		NullCheck(L_87);
		int32_t L_88 = L_87->get_sayMediumIndex_12();
		if ((!(((uint32_t)L_86) == ((uint32_t)L_88))))
		{
			goto IL_01ec;
		}
	}
	{
		List_1_t260B41F956D673396C33A4CF94E8D6C4389EACB7 * L_89 = V_0;
		List_1_t260B41F956D673396C33A4CF94E8D6C4389EACB7 * L_90 = V_0;
		int32_t L_91 = V_3;
		NullCheck(L_90);
		int32_t L_92;
		L_92 = List_1_get_Item_m730FCAD2646FA94B07D1216A512B09AB9F0BBA5D_inline(L_90, L_91, /*hidden argument*/List_1_get_Item_m730FCAD2646FA94B07D1216A512B09AB9F0BBA5D_RuntimeMethod_var);
		NullCheck(L_89);
		List_1_RemoveAt_m4A6ABD183823501A4F9A6082D9EDC589029AD221(L_89, L_92, /*hidden argument*/List_1_RemoveAt_m4A6ABD183823501A4F9A6082D9EDC589029AD221_RuntimeMethod_var);
	}

IL_01ec:
	{
		int32_t L_93 = V_3;
		Salsa3D_tACF83D529139263662FEAAC8F5B53EC43B140C9C * L_94 = __this->get_salsa3D_5();
		NullCheck(L_94);
		int32_t L_95 = L_94->get_sayLargeIndex_13();
		if ((!(((uint32_t)L_93) == ((uint32_t)L_95))))
		{
			goto IL_0207;
		}
	}
	{
		List_1_t260B41F956D673396C33A4CF94E8D6C4389EACB7 * L_96 = V_0;
		List_1_t260B41F956D673396C33A4CF94E8D6C4389EACB7 * L_97 = V_0;
		int32_t L_98 = V_3;
		NullCheck(L_97);
		int32_t L_99;
		L_99 = List_1_get_Item_m730FCAD2646FA94B07D1216A512B09AB9F0BBA5D_inline(L_97, L_98, /*hidden argument*/List_1_get_Item_m730FCAD2646FA94B07D1216A512B09AB9F0BBA5D_RuntimeMethod_var);
		NullCheck(L_96);
		List_1_RemoveAt_m4A6ABD183823501A4F9A6082D9EDC589029AD221(L_96, L_99, /*hidden argument*/List_1_RemoveAt_m4A6ABD183823501A4F9A6082D9EDC589029AD221_RuntimeMethod_var);
	}

IL_0207:
	{
		int32_t L_100 = V_3;
		V_3 = ((int32_t)il2cpp_codegen_subtract((int32_t)L_100, (int32_t)1));
	}

IL_020b:
	{
		int32_t L_101 = V_3;
		if ((((int32_t)L_101) >= ((int32_t)0)))
		{
			goto IL_01b6;
		}
	}

IL_020f:
	{
		List_1_t260B41F956D673396C33A4CF94E8D6C4389EACB7 * L_102 = V_0;
		NullCheck(L_102);
		int32_t L_103;
		L_103 = List_1_get_Count_m7FA90926D9267868473EF90941F6BF794EC87FF2_inline(L_102, /*hidden argument*/List_1_get_Count_m7FA90926D9267868473EF90941F6BF794EC87FF2_RuntimeMethod_var);
		__this->set_customShapeCount_46(L_103);
		int32_t L_104 = __this->get_customShapeCount_46();
		RandomEyesCustomShapeU5BU5D_t8FB0C62923194ED52AE0B06A6249680C47CA152C* L_105 = (RandomEyesCustomShapeU5BU5D_t8FB0C62923194ED52AE0B06A6249680C47CA152C*)(RandomEyesCustomShapeU5BU5D_t8FB0C62923194ED52AE0B06A6249680C47CA152C*)SZArrayNew(RandomEyesCustomShapeU5BU5D_t8FB0C62923194ED52AE0B06A6249680C47CA152C_il2cpp_TypeInfo_var, (uint32_t)L_104);
		__this->set_customShapes_48(L_105);
		V_4 = 0;
		goto IL_02a3;
	}

IL_0231:
	{
		V_5 = 0;
		goto IL_0293;
	}

IL_0236:
	{
		RandomEyesCustomShapeU5BU5D_t8FB0C62923194ED52AE0B06A6249680C47CA152C* L_106 = __this->get_customShapes_48();
		int32_t L_107 = V_5;
		RandomEyesCustomShape_t10BCA0CEC18C2BBF6A8BD7BFAD2AF005D233C354 * L_108 = (RandomEyesCustomShape_t10BCA0CEC18C2BBF6A8BD7BFAD2AF005D233C354 *)il2cpp_codegen_object_new(RandomEyesCustomShape_t10BCA0CEC18C2BBF6A8BD7BFAD2AF005D233C354_il2cpp_TypeInfo_var);
		RandomEyesCustomShape__ctor_m55E79792EBDFA8E4E88E7396AB6E31327D5D0DBF(L_108, /*hidden argument*/NULL);
		NullCheck(L_106);
		ArrayElementTypeCheck (L_106, L_108);
		(L_106)->SetAt(static_cast<il2cpp_array_size_t>(L_107), (RandomEyesCustomShape_t10BCA0CEC18C2BBF6A8BD7BFAD2AF005D233C354 *)L_108);
		RandomEyesCustomShapeU5BU5D_t8FB0C62923194ED52AE0B06A6249680C47CA152C* L_109 = __this->get_customShapes_48();
		int32_t L_110 = V_5;
		NullCheck(L_109);
		int32_t L_111 = L_110;
		RandomEyesCustomShape_t10BCA0CEC18C2BBF6A8BD7BFAD2AF005D233C354 * L_112 = (L_109)->GetAt(static_cast<il2cpp_array_size_t>(L_111));
		List_1_t260B41F956D673396C33A4CF94E8D6C4389EACB7 * L_113 = V_0;
		int32_t L_114 = V_5;
		NullCheck(L_113);
		int32_t L_115;
		L_115 = List_1_get_Item_m730FCAD2646FA94B07D1216A512B09AB9F0BBA5D_inline(L_113, L_114, /*hidden argument*/List_1_get_Item_m730FCAD2646FA94B07D1216A512B09AB9F0BBA5D_RuntimeMethod_var);
		NullCheck(L_112);
		L_112->set_shapeIndex_0(L_115);
		SkinnedMeshRenderer_t126F4D6010E0F4B2685A7817B0A9171805D8F496 * L_116 = __this->get_skinnedMeshRenderer_6();
		IL2CPP_RUNTIME_CLASS_INIT(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		bool L_117;
		L_117 = Object_op_Implicit_mC8214E4F028CC2F036CC82BDB81D102A02893499(L_116, /*hidden argument*/NULL);
		if (!L_117)
		{
			goto IL_028d;
		}
	}
	{
		RandomEyesCustomShapeU5BU5D_t8FB0C62923194ED52AE0B06A6249680C47CA152C* L_118 = __this->get_customShapes_48();
		int32_t L_119 = V_5;
		NullCheck(L_118);
		int32_t L_120 = L_119;
		RandomEyesCustomShape_t10BCA0CEC18C2BBF6A8BD7BFAD2AF005D233C354 * L_121 = (L_118)->GetAt(static_cast<il2cpp_array_size_t>(L_120));
		SkinnedMeshRenderer_t126F4D6010E0F4B2685A7817B0A9171805D8F496 * L_122 = __this->get_skinnedMeshRenderer_6();
		NullCheck(L_122);
		Mesh_t2F5992DBA650D5862B43D3823ACD997132A57DA6 * L_123;
		L_123 = SkinnedMeshRenderer_get_sharedMesh_mFD55E307943C1C4B2E2E8632F15B41CCBD8D91F2(L_122, /*hidden argument*/NULL);
		List_1_t260B41F956D673396C33A4CF94E8D6C4389EACB7 * L_124 = V_0;
		int32_t L_125 = V_5;
		NullCheck(L_124);
		int32_t L_126;
		L_126 = List_1_get_Item_m730FCAD2646FA94B07D1216A512B09AB9F0BBA5D_inline(L_124, L_125, /*hidden argument*/List_1_get_Item_m730FCAD2646FA94B07D1216A512B09AB9F0BBA5D_RuntimeMethod_var);
		NullCheck(L_123);
		String_t* L_127;
		L_127 = Mesh_GetBlendShapeName_mF28E636D7F66608E6FD9152ACD9547B78C895000(L_123, L_126, /*hidden argument*/NULL);
		NullCheck(L_121);
		L_121->set_shapeName_1(L_127);
	}

IL_028d:
	{
		int32_t L_128 = V_5;
		V_5 = ((int32_t)il2cpp_codegen_add((int32_t)L_128, (int32_t)1));
	}

IL_0293:
	{
		int32_t L_129 = V_5;
		List_1_t260B41F956D673396C33A4CF94E8D6C4389EACB7 * L_130 = V_0;
		NullCheck(L_130);
		int32_t L_131;
		L_131 = List_1_get_Count_m7FA90926D9267868473EF90941F6BF794EC87FF2_inline(L_130, /*hidden argument*/List_1_get_Count_m7FA90926D9267868473EF90941F6BF794EC87FF2_RuntimeMethod_var);
		if ((((int32_t)L_129) < ((int32_t)L_131)))
		{
			goto IL_0236;
		}
	}
	{
		int32_t L_132 = V_4;
		V_4 = ((int32_t)il2cpp_codegen_add((int32_t)L_132, (int32_t)1));
	}

IL_02a3:
	{
		int32_t L_133 = V_4;
		List_1_t260B41F956D673396C33A4CF94E8D6C4389EACB7 * L_134 = V_0;
		NullCheck(L_134);
		int32_t L_135;
		L_135 = List_1_get_Count_m7FA90926D9267868473EF90941F6BF794EC87FF2_inline(L_134, /*hidden argument*/List_1_get_Count_m7FA90926D9267868473EF90941F6BF794EC87FF2_RuntimeMethod_var);
		if ((((int32_t)L_133) < ((int32_t)L_135)))
		{
			goto IL_0231;
		}
	}
	{
		RandomEyesCustomShapeU5BU5D_t8FB0C62923194ED52AE0B06A6249680C47CA152C* L_136 = __this->get_customShapes_48();
		NullCheck(L_136);
		__this->set_selectedCustomShape_42(((int32_t)((int32_t)(((RuntimeArray*)L_136)->max_length))));
	}

IL_02bb:
	{
		SkinnedMeshRenderer_t126F4D6010E0F4B2685A7817B0A9171805D8F496 * L_137 = __this->get_skinnedMeshRenderer_6();
		IL2CPP_RUNTIME_CLASS_INIT(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		bool L_138;
		L_138 = Object_op_Implicit_mC8214E4F028CC2F036CC82BDB81D102A02893499(L_137, /*hidden argument*/NULL);
		if (!L_138)
		{
			goto IL_0378;
		}
	}
	{
		bool L_139 = __this->get_excludeEyeShapes_44();
		if (L_139)
		{
			goto IL_0378;
		}
	}
	{
		Salsa3D_tACF83D529139263662FEAAC8F5B53EC43B140C9C * L_140 = __this->get_salsa3D_5();
		IL2CPP_RUNTIME_CLASS_INIT(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		bool L_141;
		L_141 = Object_op_Implicit_mC8214E4F028CC2F036CC82BDB81D102A02893499(L_140, /*hidden argument*/NULL);
		if (L_141)
		{
			goto IL_0378;
		}
	}
	{
		SkinnedMeshRenderer_t126F4D6010E0F4B2685A7817B0A9171805D8F496 * L_142 = __this->get_skinnedMeshRenderer_6();
		NullCheck(L_142);
		Mesh_t2F5992DBA650D5862B43D3823ACD997132A57DA6 * L_143;
		L_143 = SkinnedMeshRenderer_get_sharedMesh_mFD55E307943C1C4B2E2E8632F15B41CCBD8D91F2(L_142, /*hidden argument*/NULL);
		NullCheck(L_143);
		int32_t L_144;
		L_144 = Mesh_get_blendShapeCount_mC80CCEA555E9E5609E3497EECF2B03F9B822CB77(L_143, /*hidden argument*/NULL);
		__this->set_customShapeCount_46(L_144);
		int32_t L_145 = __this->get_customShapeCount_46();
		RandomEyesCustomShapeU5BU5D_t8FB0C62923194ED52AE0B06A6249680C47CA152C* L_146 = (RandomEyesCustomShapeU5BU5D_t8FB0C62923194ED52AE0B06A6249680C47CA152C*)(RandomEyesCustomShapeU5BU5D_t8FB0C62923194ED52AE0B06A6249680C47CA152C*)SZArrayNew(RandomEyesCustomShapeU5BU5D_t8FB0C62923194ED52AE0B06A6249680C47CA152C_il2cpp_TypeInfo_var, (uint32_t)L_145);
		__this->set_customShapes_48(L_146);
		V_6 = 0;
		goto IL_0356;
	}

IL_0312:
	{
		RandomEyesCustomShapeU5BU5D_t8FB0C62923194ED52AE0B06A6249680C47CA152C* L_147 = __this->get_customShapes_48();
		int32_t L_148 = V_6;
		RandomEyesCustomShape_t10BCA0CEC18C2BBF6A8BD7BFAD2AF005D233C354 * L_149 = (RandomEyesCustomShape_t10BCA0CEC18C2BBF6A8BD7BFAD2AF005D233C354 *)il2cpp_codegen_object_new(RandomEyesCustomShape_t10BCA0CEC18C2BBF6A8BD7BFAD2AF005D233C354_il2cpp_TypeInfo_var);
		RandomEyesCustomShape__ctor_m55E79792EBDFA8E4E88E7396AB6E31327D5D0DBF(L_149, /*hidden argument*/NULL);
		NullCheck(L_147);
		ArrayElementTypeCheck (L_147, L_149);
		(L_147)->SetAt(static_cast<il2cpp_array_size_t>(L_148), (RandomEyesCustomShape_t10BCA0CEC18C2BBF6A8BD7BFAD2AF005D233C354 *)L_149);
		RandomEyesCustomShapeU5BU5D_t8FB0C62923194ED52AE0B06A6249680C47CA152C* L_150 = __this->get_customShapes_48();
		int32_t L_151 = V_6;
		NullCheck(L_150);
		int32_t L_152 = L_151;
		RandomEyesCustomShape_t10BCA0CEC18C2BBF6A8BD7BFAD2AF005D233C354 * L_153 = (L_150)->GetAt(static_cast<il2cpp_array_size_t>(L_152));
		int32_t L_154 = V_6;
		NullCheck(L_153);
		L_153->set_shapeIndex_0(L_154);
		RandomEyesCustomShapeU5BU5D_t8FB0C62923194ED52AE0B06A6249680C47CA152C* L_155 = __this->get_customShapes_48();
		int32_t L_156 = V_6;
		NullCheck(L_155);
		int32_t L_157 = L_156;
		RandomEyesCustomShape_t10BCA0CEC18C2BBF6A8BD7BFAD2AF005D233C354 * L_158 = (L_155)->GetAt(static_cast<il2cpp_array_size_t>(L_157));
		SkinnedMeshRenderer_t126F4D6010E0F4B2685A7817B0A9171805D8F496 * L_159 = __this->get_skinnedMeshRenderer_6();
		NullCheck(L_159);
		Mesh_t2F5992DBA650D5862B43D3823ACD997132A57DA6 * L_160;
		L_160 = SkinnedMeshRenderer_get_sharedMesh_mFD55E307943C1C4B2E2E8632F15B41CCBD8D91F2(L_159, /*hidden argument*/NULL);
		int32_t L_161 = V_6;
		NullCheck(L_160);
		String_t* L_162;
		L_162 = Mesh_GetBlendShapeName_mF28E636D7F66608E6FD9152ACD9547B78C895000(L_160, L_161, /*hidden argument*/NULL);
		NullCheck(L_158);
		L_158->set_shapeName_1(L_162);
		int32_t L_163 = V_6;
		V_6 = ((int32_t)il2cpp_codegen_add((int32_t)L_163, (int32_t)1));
	}

IL_0356:
	{
		int32_t L_164 = V_6;
		SkinnedMeshRenderer_t126F4D6010E0F4B2685A7817B0A9171805D8F496 * L_165 = __this->get_skinnedMeshRenderer_6();
		NullCheck(L_165);
		Mesh_t2F5992DBA650D5862B43D3823ACD997132A57DA6 * L_166;
		L_166 = SkinnedMeshRenderer_get_sharedMesh_mFD55E307943C1C4B2E2E8632F15B41CCBD8D91F2(L_165, /*hidden argument*/NULL);
		NullCheck(L_166);
		int32_t L_167;
		L_167 = Mesh_get_blendShapeCount_mC80CCEA555E9E5609E3497EECF2B03F9B822CB77(L_166, /*hidden argument*/NULL);
		if ((((int32_t)L_164) < ((int32_t)L_167)))
		{
			goto IL_0312;
		}
	}
	{
		RandomEyesCustomShapeU5BU5D_t8FB0C62923194ED52AE0B06A6249680C47CA152C* L_168 = __this->get_customShapes_48();
		NullCheck(L_168);
		__this->set_selectedCustomShape_42(((int32_t)((int32_t)(((RuntimeArray*)L_168)->max_length))));
	}

IL_0378:
	{
		int32_t L_169;
		L_169 = RandomEyes3D_RebuildCurrentCustomShapeList_m645926658AF05E208117F2A4D0EAA998DBD27348(__this, /*hidden argument*/NULL);
		__this->set_noneShapeIndex_43(L_169);
		int32_t L_170 = __this->get_noneShapeIndex_43();
		__this->set_selectedCustomShape_42(L_170);
		return;
	}
}
// System.Int32 CrazyMinnow.SALSA.RandomEyes3D::RebuildCurrentCustomShapeList()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t RandomEyes3D_RebuildCurrentCustomShapeList_m645926658AF05E208117F2A4D0EAA998DBD27348 (RandomEyes3D_t6868157C4605BD0225E1B08301F110E72336CE45 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral548D93DDB2AC6B24373148B19D9A625571AB2318);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	{
		V_0 = 0;
		int32_t L_0 = __this->get_customShapeCount_46();
		StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* L_1 = (StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A*)(StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A*)SZArrayNew(StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A_il2cpp_TypeInfo_var, (uint32_t)((int32_t)il2cpp_codegen_add((int32_t)L_0, (int32_t)1)));
		__this->set_dynamicCustomShapes_45(L_1);
		V_1 = 0;
		goto IL_006f;
	}

IL_0019:
	{
		RandomEyesCustomShapeU5BU5D_t8FB0C62923194ED52AE0B06A6249680C47CA152C* L_2 = __this->get_customShapes_48();
		if (!L_2)
		{
			goto IL_005e;
		}
	}
	{
		int32_t L_3 = V_1;
		RandomEyesCustomShapeU5BU5D_t8FB0C62923194ED52AE0B06A6249680C47CA152C* L_4 = __this->get_customShapes_48();
		NullCheck(L_4);
		if ((((int32_t)L_3) >= ((int32_t)((int32_t)((int32_t)(((RuntimeArray*)L_4)->max_length))))))
		{
			goto IL_004d;
		}
	}
	{
		RandomEyesCustomShapeU5BU5D_t8FB0C62923194ED52AE0B06A6249680C47CA152C* L_5 = __this->get_customShapes_48();
		int32_t L_6 = V_1;
		NullCheck(L_5);
		int32_t L_7 = L_6;
		RandomEyesCustomShape_t10BCA0CEC18C2BBF6A8BD7BFAD2AF005D233C354 * L_8 = (L_5)->GetAt(static_cast<il2cpp_array_size_t>(L_7));
		if (!L_8)
		{
			goto IL_006b;
		}
	}
	{
		StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* L_9 = __this->get_dynamicCustomShapes_45();
		int32_t L_10 = V_1;
		RandomEyesCustomShapeU5BU5D_t8FB0C62923194ED52AE0B06A6249680C47CA152C* L_11 = __this->get_customShapes_48();
		int32_t L_12 = V_1;
		NullCheck(L_11);
		int32_t L_13 = L_12;
		RandomEyesCustomShape_t10BCA0CEC18C2BBF6A8BD7BFAD2AF005D233C354 * L_14 = (L_11)->GetAt(static_cast<il2cpp_array_size_t>(L_13));
		NullCheck(L_14);
		String_t* L_15 = L_14->get_shapeName_1();
		NullCheck(L_9);
		ArrayElementTypeCheck (L_9, L_15);
		(L_9)->SetAt(static_cast<il2cpp_array_size_t>(L_10), (String_t*)L_15);
		goto IL_006b;
	}

IL_004d:
	{
		StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* L_16 = __this->get_dynamicCustomShapes_45();
		int32_t L_17 = V_1;
		NullCheck(L_16);
		ArrayElementTypeCheck (L_16, _stringLiteral548D93DDB2AC6B24373148B19D9A625571AB2318);
		(L_16)->SetAt(static_cast<il2cpp_array_size_t>(L_17), (String_t*)_stringLiteral548D93DDB2AC6B24373148B19D9A625571AB2318);
		int32_t L_18 = V_1;
		V_0 = L_18;
		goto IL_006b;
	}

IL_005e:
	{
		StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* L_19 = __this->get_dynamicCustomShapes_45();
		NullCheck(L_19);
		ArrayElementTypeCheck (L_19, _stringLiteral548D93DDB2AC6B24373148B19D9A625571AB2318);
		(L_19)->SetAt(static_cast<il2cpp_array_size_t>(0), (String_t*)_stringLiteral548D93DDB2AC6B24373148B19D9A625571AB2318);
	}

IL_006b:
	{
		int32_t L_20 = V_1;
		V_1 = ((int32_t)il2cpp_codegen_add((int32_t)L_20, (int32_t)1));
	}

IL_006f:
	{
		int32_t L_21 = V_1;
		StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* L_22 = __this->get_dynamicCustomShapes_45();
		NullCheck(L_22);
		if ((((int32_t)L_21) < ((int32_t)((int32_t)((int32_t)(((RuntimeArray*)L_22)->max_length))))))
		{
			goto IL_0019;
		}
	}
	{
		int32_t L_23 = V_0;
		return L_23;
	}
}
// System.Void CrazyMinnow.SALSA.RandomEyes3D::FindOrCreateEyePositionGizmo()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void RandomEyes3D_FindOrCreateEyePositionGizmo_m62C69039D32AC352E5A274B9C560E2625DE8BE74 (RandomEyes3D_t6868157C4605BD0225E1B08301F110E72336CE45 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Component_GetComponent_TisRandomEyes3DGizmo_t039AFDA0DE2968F2C3DFB63606D40A2C238839A1_mF418C2AB9AA5AE15ECFB9151E7F925BE3AA284A1_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Component_GetComponent_TisSkinnedMeshRenderer_t126F4D6010E0F4B2685A7817B0A9171805D8F496_m48EF3D17CF12700CC28C88CEFBB6741D6E1FFFE3_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&GameObject_AddComponent_TisRandomEyes3DGizmo_t039AFDA0DE2968F2C3DFB63606D40A2C238839A1_m2076D0783F9DCDF3BC97B514CDA27C93C23C5B0C_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&GameObject_GetComponentsInChildren_TisTransform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1_m1D81E170D9B0CD0720A6BCDD722BC8A0B4AA8F0E_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral461EE177B772C8076E2D62C04952F00C85951024);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral74ECD97912D81E0F9C90F35D74E271439C4F3190);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral7D43707E485AAE2574F2C02D133505293F0F5975);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral8739227E8E687EF781DA0D923452C2686CFF10A2);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralA33A5CAE02B786C2060461DF8C6764B4C05E9423);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralB0CAD8BCA47977D99C1B3C76C933E0FD4E6311FA);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralB1E5119D36EC43B340C0A0DDC99F1156546EA9DF);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralB9D3D73187778AF6D06AB846BD78D488ADBFB70E);
		s_Il2CppMethodInitialized = true;
	}
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  V_0;
	memset((&V_0), 0, sizeof(V_0));
	GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * V_1 = NULL;
	GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * V_2 = NULL;
	GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * V_3 = NULL;
	TransformU5BU5D_t7821C0520CC567C0A069329C01AE9C058C7E3F1D* V_4 = NULL;
	Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * V_5 = NULL;
	Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * V_6 = NULL;
	TransformU5BU5D_t7821C0520CC567C0A069329C01AE9C058C7E3F1D* V_7 = NULL;
	int32_t V_8 = 0;
	TransformU5BU5D_t7821C0520CC567C0A069329C01AE9C058C7E3F1D* V_9 = NULL;
	int32_t V_10 = 0;
	{
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_0;
		L_0 = Component_get_gameObject_m55DC35B149AFB9157582755383BA954655FE0C5B(__this, /*hidden argument*/NULL);
		NullCheck(L_0);
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_1;
		L_1 = GameObject_get_transform_m16A80BB92B6C8C5AB696E447014D45EDF1E4DE34(L_0, /*hidden argument*/NULL);
		NullCheck(L_1);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_2;
		L_2 = Transform_get_position_m40A8A9895568D56FFC687B57F30E8D53CB5EA341(L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		V_1 = (GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 *)NULL;
		V_2 = (GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 *)NULL;
		V_3 = (GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 *)NULL;
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_3 = __this->get_eyePosition_25();
		IL2CPP_RUNTIME_CLASS_INIT(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		bool L_4;
		L_4 = Object_op_Implicit_mC8214E4F028CC2F036CC82BDB81D102A02893499(L_3, /*hidden argument*/NULL);
		if (L_4)
		{
			goto IL_02c0;
		}
	}
	{
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_5;
		L_5 = Component_get_gameObject_m55DC35B149AFB9157582755383BA954655FE0C5B(__this, /*hidden argument*/NULL);
		NullCheck(L_5);
		TransformU5BU5D_t7821C0520CC567C0A069329C01AE9C058C7E3F1D* L_6;
		L_6 = GameObject_GetComponentsInChildren_TisTransform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1_m1D81E170D9B0CD0720A6BCDD722BC8A0B4AA8F0E(L_5, /*hidden argument*/GameObject_GetComponentsInChildren_TisTransform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1_m1D81E170D9B0CD0720A6BCDD722BC8A0B4AA8F0E_RuntimeMethod_var);
		V_4 = L_6;
		TransformU5BU5D_t7821C0520CC567C0A069329C01AE9C058C7E3F1D* L_7 = V_4;
		V_7 = L_7;
		V_8 = 0;
		goto IL_0081;
	}

IL_003d:
	{
		TransformU5BU5D_t7821C0520CC567C0A069329C01AE9C058C7E3F1D* L_8 = V_7;
		int32_t L_9 = V_8;
		NullCheck(L_8);
		int32_t L_10 = L_9;
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_11 = (L_8)->GetAt(static_cast<il2cpp_array_size_t>(L_10));
		V_5 = L_11;
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_12 = V_5;
		NullCheck(L_12);
		RandomEyes3DGizmo_t039AFDA0DE2968F2C3DFB63606D40A2C238839A1 * L_13;
		L_13 = Component_GetComponent_TisRandomEyes3DGizmo_t039AFDA0DE2968F2C3DFB63606D40A2C238839A1_mF418C2AB9AA5AE15ECFB9151E7F925BE3AA284A1(L_12, /*hidden argument*/Component_GetComponent_TisRandomEyes3DGizmo_t039AFDA0DE2968F2C3DFB63606D40A2C238839A1_mF418C2AB9AA5AE15ECFB9151E7F925BE3AA284A1_RuntimeMethod_var);
		IL2CPP_RUNTIME_CLASS_INIT(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		bool L_14;
		L_14 = Object_op_Inequality_mE1F187520BD83FB7D86A6D850710C4D42B864E90(L_13, (Object_tF2F3778131EFF286AF62B7B013A170F95A91571A *)NULL, /*hidden argument*/NULL);
		if (!L_14)
		{
			goto IL_007b;
		}
	}
	{
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_15 = __this->get_eyePosition_25();
		IL2CPP_RUNTIME_CLASS_INIT(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		bool L_16;
		L_16 = Object_op_Implicit_mC8214E4F028CC2F036CC82BDB81D102A02893499(L_15, /*hidden argument*/NULL);
		if (L_16)
		{
			goto IL_006f;
		}
	}
	{
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_17 = V_5;
		NullCheck(L_17);
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_18;
		L_18 = Component_get_gameObject_m55DC35B149AFB9157582755383BA954655FE0C5B(L_17, /*hidden argument*/NULL);
		__this->set_eyePosition_25(L_18);
		goto IL_007b;
	}

IL_006f:
	{
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_19 = V_5;
		NullCheck(L_19);
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_20;
		L_20 = Component_get_gameObject_m55DC35B149AFB9157582755383BA954655FE0C5B(L_19, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		Object_DestroyImmediate_mCCED69F4D4C9A4FA3AC30A142CF3D7F085F7C422(L_20, /*hidden argument*/NULL);
	}

IL_007b:
	{
		int32_t L_21 = V_8;
		V_8 = ((int32_t)il2cpp_codegen_add((int32_t)L_21, (int32_t)1));
	}

IL_0081:
	{
		int32_t L_22 = V_8;
		TransformU5BU5D_t7821C0520CC567C0A069329C01AE9C058C7E3F1D* L_23 = V_7;
		NullCheck(L_23);
		if ((((int32_t)L_22) < ((int32_t)((int32_t)((int32_t)(((RuntimeArray*)L_23)->max_length))))))
		{
			goto IL_003d;
		}
	}
	{
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_24 = __this->get_eyePosition_25();
		IL2CPP_RUNTIME_CLASS_INIT(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		bool L_25;
		L_25 = Object_op_Implicit_mC8214E4F028CC2F036CC82BDB81D102A02893499(L_24, /*hidden argument*/NULL);
		if (L_25)
		{
			goto IL_02c0;
		}
	}
	{
		TransformU5BU5D_t7821C0520CC567C0A069329C01AE9C058C7E3F1D* L_26 = V_4;
		V_9 = L_26;
		V_10 = 0;
		goto IL_01d9;
	}

IL_00a5:
	{
		TransformU5BU5D_t7821C0520CC567C0A069329C01AE9C058C7E3F1D* L_27 = V_9;
		int32_t L_28 = V_10;
		NullCheck(L_27);
		int32_t L_29 = L_28;
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_30 = (L_27)->GetAt(static_cast<il2cpp_array_size_t>(L_29));
		V_6 = L_30;
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_31 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		bool L_32;
		L_32 = Object_op_Implicit_mC8214E4F028CC2F036CC82BDB81D102A02893499(L_31, /*hidden argument*/NULL);
		if (L_32)
		{
			goto IL_00e3;
		}
	}
	{
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_33 = V_6;
		NullCheck(L_33);
		SkinnedMeshRenderer_t126F4D6010E0F4B2685A7817B0A9171805D8F496 * L_34;
		L_34 = Component_GetComponent_TisSkinnedMeshRenderer_t126F4D6010E0F4B2685A7817B0A9171805D8F496_m48EF3D17CF12700CC28C88CEFBB6741D6E1FFFE3(L_33, /*hidden argument*/Component_GetComponent_TisSkinnedMeshRenderer_t126F4D6010E0F4B2685A7817B0A9171805D8F496_m48EF3D17CF12700CC28C88CEFBB6741D6E1FFFE3_RuntimeMethod_var);
		IL2CPP_RUNTIME_CLASS_INIT(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		bool L_35;
		L_35 = Object_op_Equality_mEE9EC7EB5C7DC3E95B94AB904E1986FC4D566D54(L_34, (Object_tF2F3778131EFF286AF62B7B013A170F95A91571A *)NULL, /*hidden argument*/NULL);
		if (!L_35)
		{
			goto IL_00e3;
		}
	}
	{
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_36 = V_6;
		NullCheck(L_36);
		String_t* L_37;
		L_37 = Object_get_name_m0C7BC870ED2F0DC5A2FB09628136CD7D1CB82CFB(L_36, /*hidden argument*/NULL);
		NullCheck(L_37);
		String_t* L_38;
		L_38 = String_ToLower_m7875A49FE166D0A68F3F6B6E70C0C056EBEFD31D(L_37, /*hidden argument*/NULL);
		NullCheck(L_38);
		bool L_39;
		L_39 = String_Contains_mA26BDCCE8F191E8965EB8EEFC18BB4D0F85A075A(L_38, _stringLiteral461EE177B772C8076E2D62C04952F00C85951024, /*hidden argument*/NULL);
		if (!L_39)
		{
			goto IL_00e3;
		}
	}
	{
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_40 = V_6;
		NullCheck(L_40);
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_41;
		L_41 = Component_get_gameObject_m55DC35B149AFB9157582755383BA954655FE0C5B(L_40, /*hidden argument*/NULL);
		V_1 = L_41;
	}

IL_00e3:
	{
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_42 = V_6;
		NullCheck(L_42);
		String_t* L_43;
		L_43 = Object_get_name_m0C7BC870ED2F0DC5A2FB09628136CD7D1CB82CFB(L_42, /*hidden argument*/NULL);
		NullCheck(L_43);
		String_t* L_44;
		L_44 = String_ToLower_m7875A49FE166D0A68F3F6B6E70C0C056EBEFD31D(L_43, /*hidden argument*/NULL);
		NullCheck(L_44);
		bool L_45;
		L_45 = String_Contains_mA26BDCCE8F191E8965EB8EEFC18BB4D0F85A075A(L_44, _stringLiteral74ECD97912D81E0F9C90F35D74E271439C4F3190, /*hidden argument*/NULL);
		if (!L_45)
		{
			goto IL_01d3;
		}
	}
	{
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_46 = V_6;
		NullCheck(L_46);
		String_t* L_47;
		L_47 = Object_get_name_m0C7BC870ED2F0DC5A2FB09628136CD7D1CB82CFB(L_46, /*hidden argument*/NULL);
		NullCheck(L_47);
		String_t* L_48;
		L_48 = String_ToLower_m7875A49FE166D0A68F3F6B6E70C0C056EBEFD31D(L_47, /*hidden argument*/NULL);
		NullCheck(L_48);
		bool L_49;
		L_49 = String_Contains_mA26BDCCE8F191E8965EB8EEFC18BB4D0F85A075A(L_48, _stringLiteralB0CAD8BCA47977D99C1B3C76C933E0FD4E6311FA, /*hidden argument*/NULL);
		if (L_49)
		{
			goto IL_01d3;
		}
	}
	{
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_50 = V_2;
		IL2CPP_RUNTIME_CLASS_INIT(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		bool L_51;
		L_51 = Object_op_Implicit_mC8214E4F028CC2F036CC82BDB81D102A02893499(L_50, /*hidden argument*/NULL);
		if (L_51)
		{
			goto IL_0168;
		}
	}
	{
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_52 = V_6;
		NullCheck(L_52);
		SkinnedMeshRenderer_t126F4D6010E0F4B2685A7817B0A9171805D8F496 * L_53;
		L_53 = Component_GetComponent_TisSkinnedMeshRenderer_t126F4D6010E0F4B2685A7817B0A9171805D8F496_m48EF3D17CF12700CC28C88CEFBB6741D6E1FFFE3(L_52, /*hidden argument*/Component_GetComponent_TisSkinnedMeshRenderer_t126F4D6010E0F4B2685A7817B0A9171805D8F496_m48EF3D17CF12700CC28C88CEFBB6741D6E1FFFE3_RuntimeMethod_var);
		IL2CPP_RUNTIME_CLASS_INIT(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		bool L_54;
		L_54 = Object_op_Equality_mEE9EC7EB5C7DC3E95B94AB904E1986FC4D566D54(L_53, (Object_tF2F3778131EFF286AF62B7B013A170F95A91571A *)NULL, /*hidden argument*/NULL);
		if (!L_54)
		{
			goto IL_0168;
		}
	}
	{
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_55 = V_6;
		NullCheck(L_55);
		String_t* L_56;
		L_56 = Object_get_name_m0C7BC870ED2F0DC5A2FB09628136CD7D1CB82CFB(L_55, /*hidden argument*/NULL);
		NullCheck(L_56);
		String_t* L_57;
		L_57 = String_ToLower_m7875A49FE166D0A68F3F6B6E70C0C056EBEFD31D(L_56, /*hidden argument*/NULL);
		NullCheck(L_57);
		bool L_58;
		L_58 = String_Contains_mA26BDCCE8F191E8965EB8EEFC18BB4D0F85A075A(L_57, _stringLiteral8739227E8E687EF781DA0D923452C2686CFF10A2, /*hidden argument*/NULL);
		if (L_58)
		{
			goto IL_0160;
		}
	}
	{
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_59 = V_6;
		NullCheck(L_59);
		String_t* L_60;
		L_60 = Object_get_name_m0C7BC870ED2F0DC5A2FB09628136CD7D1CB82CFB(L_59, /*hidden argument*/NULL);
		NullCheck(L_60);
		String_t* L_61;
		L_61 = String_ToLower_m7875A49FE166D0A68F3F6B6E70C0C056EBEFD31D(L_60, /*hidden argument*/NULL);
		NullCheck(L_61);
		bool L_62;
		L_62 = String_Contains_mA26BDCCE8F191E8965EB8EEFC18BB4D0F85A075A(L_61, _stringLiteralB9D3D73187778AF6D06AB846BD78D488ADBFB70E, /*hidden argument*/NULL);
		if (!L_62)
		{
			goto IL_0168;
		}
	}

IL_0160:
	{
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_63 = V_6;
		NullCheck(L_63);
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_64;
		L_64 = Component_get_gameObject_m55DC35B149AFB9157582755383BA954655FE0C5B(L_63, /*hidden argument*/NULL);
		V_2 = L_64;
	}

IL_0168:
	{
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_65 = V_3;
		IL2CPP_RUNTIME_CLASS_INIT(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		bool L_66;
		L_66 = Object_op_Implicit_mC8214E4F028CC2F036CC82BDB81D102A02893499(L_65, /*hidden argument*/NULL);
		if (L_66)
		{
			goto IL_01d3;
		}
	}
	{
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_67 = V_6;
		NullCheck(L_67);
		SkinnedMeshRenderer_t126F4D6010E0F4B2685A7817B0A9171805D8F496 * L_68;
		L_68 = Component_GetComponent_TisSkinnedMeshRenderer_t126F4D6010E0F4B2685A7817B0A9171805D8F496_m48EF3D17CF12700CC28C88CEFBB6741D6E1FFFE3(L_67, /*hidden argument*/Component_GetComponent_TisSkinnedMeshRenderer_t126F4D6010E0F4B2685A7817B0A9171805D8F496_m48EF3D17CF12700CC28C88CEFBB6741D6E1FFFE3_RuntimeMethod_var);
		IL2CPP_RUNTIME_CLASS_INIT(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		bool L_69;
		L_69 = Object_op_Equality_mEE9EC7EB5C7DC3E95B94AB904E1986FC4D566D54(L_68, (Object_tF2F3778131EFF286AF62B7B013A170F95A91571A *)NULL, /*hidden argument*/NULL);
		if (!L_69)
		{
			goto IL_01d3;
		}
	}
	{
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_70 = V_6;
		NullCheck(L_70);
		String_t* L_71;
		L_71 = Object_get_name_m0C7BC870ED2F0DC5A2FB09628136CD7D1CB82CFB(L_70, /*hidden argument*/NULL);
		NullCheck(L_71);
		String_t* L_72;
		L_72 = String_ToLower_m7875A49FE166D0A68F3F6B6E70C0C056EBEFD31D(L_71, /*hidden argument*/NULL);
		NullCheck(L_72);
		bool L_73;
		L_73 = String_Contains_mA26BDCCE8F191E8965EB8EEFC18BB4D0F85A075A(L_72, _stringLiteralB1E5119D36EC43B340C0A0DDC99F1156546EA9DF, /*hidden argument*/NULL);
		if (L_73)
		{
			goto IL_01af;
		}
	}
	{
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_74 = V_6;
		NullCheck(L_74);
		String_t* L_75;
		L_75 = Object_get_name_m0C7BC870ED2F0DC5A2FB09628136CD7D1CB82CFB(L_74, /*hidden argument*/NULL);
		NullCheck(L_75);
		String_t* L_76;
		L_76 = String_ToLower_m7875A49FE166D0A68F3F6B6E70C0C056EBEFD31D(L_75, /*hidden argument*/NULL);
		NullCheck(L_76);
		bool L_77;
		L_77 = String_Contains_mA26BDCCE8F191E8965EB8EEFC18BB4D0F85A075A(L_76, _stringLiteralA33A5CAE02B786C2060461DF8C6764B4C05E9423, /*hidden argument*/NULL);
		if (!L_77)
		{
			goto IL_01d3;
		}
	}

IL_01af:
	{
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_78 = V_2;
		IL2CPP_RUNTIME_CLASS_INIT(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		bool L_79;
		L_79 = Object_op_Implicit_mC8214E4F028CC2F036CC82BDB81D102A02893499(L_78, /*hidden argument*/NULL);
		if (!L_79)
		{
			goto IL_01d3;
		}
	}
	{
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_80 = V_2;
		NullCheck(L_80);
		int32_t L_81;
		L_81 = Object_GetInstanceID_m7CF962BC1DB5C03F3522F88728CB2F514582B501(L_80, /*hidden argument*/NULL);
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_82 = V_6;
		NullCheck(L_82);
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_83;
		L_83 = Component_get_gameObject_m55DC35B149AFB9157582755383BA954655FE0C5B(L_82, /*hidden argument*/NULL);
		NullCheck(L_83);
		int32_t L_84;
		L_84 = Object_GetInstanceID_m7CF962BC1DB5C03F3522F88728CB2F514582B501(L_83, /*hidden argument*/NULL);
		if ((((int32_t)L_81) == ((int32_t)L_84)))
		{
			goto IL_01d3;
		}
	}
	{
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_85 = V_6;
		NullCheck(L_85);
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_86;
		L_86 = Component_get_gameObject_m55DC35B149AFB9157582755383BA954655FE0C5B(L_85, /*hidden argument*/NULL);
		V_3 = L_86;
	}

IL_01d3:
	{
		int32_t L_87 = V_10;
		V_10 = ((int32_t)il2cpp_codegen_add((int32_t)L_87, (int32_t)1));
	}

IL_01d9:
	{
		int32_t L_88 = V_10;
		TransformU5BU5D_t7821C0520CC567C0A069329C01AE9C058C7E3F1D* L_89 = V_9;
		NullCheck(L_89);
		if ((((int32_t)L_88) < ((int32_t)((int32_t)((int32_t)(((RuntimeArray*)L_89)->max_length))))))
		{
			goto IL_00a5;
		}
	}
	{
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_90 = V_2;
		IL2CPP_RUNTIME_CLASS_INIT(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		bool L_91;
		L_91 = Object_op_Implicit_mC8214E4F028CC2F036CC82BDB81D102A02893499(L_90, /*hidden argument*/NULL);
		if (!L_91)
		{
			goto IL_022a;
		}
	}
	{
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_92 = V_3;
		IL2CPP_RUNTIME_CLASS_INIT(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		bool L_93;
		L_93 = Object_op_Implicit_mC8214E4F028CC2F036CC82BDB81D102A02893499(L_92, /*hidden argument*/NULL);
		if (!L_93)
		{
			goto IL_022a;
		}
	}
	{
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_94 = V_2;
		NullCheck(L_94);
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_95;
		L_95 = GameObject_get_transform_m16A80BB92B6C8C5AB696E447014D45EDF1E4DE34(L_94, /*hidden argument*/NULL);
		NullCheck(L_95);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_96;
		L_96 = Transform_get_position_m40A8A9895568D56FFC687B57F30E8D53CB5EA341(L_95, /*hidden argument*/NULL);
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_97 = V_3;
		NullCheck(L_97);
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_98;
		L_98 = GameObject_get_transform_m16A80BB92B6C8C5AB696E447014D45EDF1E4DE34(L_97, /*hidden argument*/NULL);
		NullCheck(L_98);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_99;
		L_99 = Transform_get_position_m40A8A9895568D56FFC687B57F30E8D53CB5EA341(L_98, /*hidden argument*/NULL);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_100;
		L_100 = Vector3_op_Subtraction_m2725C96965D5C0B1F9715797E51762B13A5FED58_inline(L_96, L_99, /*hidden argument*/NULL);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_101;
		L_101 = Vector3_op_Multiply_m9EA3D18290418D7B410C7D11C4788C13BFD2C30A_inline(L_100, (0.5f), /*hidden argument*/NULL);
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_102 = V_3;
		NullCheck(L_102);
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_103;
		L_103 = GameObject_get_transform_m16A80BB92B6C8C5AB696E447014D45EDF1E4DE34(L_102, /*hidden argument*/NULL);
		NullCheck(L_103);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_104;
		L_104 = Transform_get_position_m40A8A9895568D56FFC687B57F30E8D53CB5EA341(L_103, /*hidden argument*/NULL);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_105;
		L_105 = Vector3_op_Addition_mEE4F672B923CCB184C39AABCA33443DB218E50E0_inline(L_101, L_104, /*hidden argument*/NULL);
		V_0 = L_105;
	}

IL_022a:
	{
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_106 = V_2;
		IL2CPP_RUNTIME_CLASS_INIT(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		bool L_107;
		L_107 = Object_op_Implicit_mC8214E4F028CC2F036CC82BDB81D102A02893499(L_106, /*hidden argument*/NULL);
		if (L_107)
		{
			goto IL_024e;
		}
	}
	{
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_108 = V_3;
		IL2CPP_RUNTIME_CLASS_INIT(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		bool L_109;
		L_109 = Object_op_Implicit_mC8214E4F028CC2F036CC82BDB81D102A02893499(L_108, /*hidden argument*/NULL);
		if (L_109)
		{
			goto IL_024e;
		}
	}
	{
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_110 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		bool L_111;
		L_111 = Object_op_Implicit_mC8214E4F028CC2F036CC82BDB81D102A02893499(L_110, /*hidden argument*/NULL);
		if (!L_111)
		{
			goto IL_024e;
		}
	}
	{
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_112 = V_1;
		NullCheck(L_112);
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_113;
		L_113 = GameObject_get_transform_m16A80BB92B6C8C5AB696E447014D45EDF1E4DE34(L_112, /*hidden argument*/NULL);
		NullCheck(L_113);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_114;
		L_114 = Transform_get_position_m40A8A9895568D56FFC687B57F30E8D53CB5EA341(L_113, /*hidden argument*/NULL);
		V_0 = L_114;
	}

IL_024e:
	{
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_115 = (GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 *)il2cpp_codegen_object_new(GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319_il2cpp_TypeInfo_var);
		GameObject__ctor_mDF8BF31EAE3E03F24421531B25FB4BEDB7C87144(L_115, _stringLiteral7D43707E485AAE2574F2C02D133505293F0F5975, /*hidden argument*/NULL);
		__this->set_eyePosition_25(L_115);
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_116 = __this->get_eyePosition_25();
		NullCheck(L_116);
		RandomEyes3DGizmo_t039AFDA0DE2968F2C3DFB63606D40A2C238839A1 * L_117;
		L_117 = GameObject_AddComponent_TisRandomEyes3DGizmo_t039AFDA0DE2968F2C3DFB63606D40A2C238839A1_m2076D0783F9DCDF3BC97B514CDA27C93C23C5B0C(L_116, /*hidden argument*/GameObject_AddComponent_TisRandomEyes3DGizmo_t039AFDA0DE2968F2C3DFB63606D40A2C238839A1_m2076D0783F9DCDF3BC97B514CDA27C93C23C5B0C_RuntimeMethod_var);
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_118 = __this->get_eyePosition_25();
		__this->set_eyePosVerify_26(L_118);
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_119 = __this->get_eyePosition_25();
		NullCheck(L_119);
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_120;
		L_120 = GameObject_get_transform_m16A80BB92B6C8C5AB696E447014D45EDF1E4DE34(L_119, /*hidden argument*/NULL);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_121 = V_0;
		NullCheck(L_120);
		Transform_set_position_mB169E52D57EEAC1E3F22C5395968714E4F00AC91(L_120, L_121, /*hidden argument*/NULL);
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_122 = __this->get_eyePosition_25();
		NullCheck(L_122);
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_123;
		L_123 = GameObject_get_transform_m16A80BB92B6C8C5AB696E447014D45EDF1E4DE34(L_122, /*hidden argument*/NULL);
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_124;
		L_124 = Component_get_gameObject_m55DC35B149AFB9157582755383BA954655FE0C5B(__this, /*hidden argument*/NULL);
		NullCheck(L_124);
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_125;
		L_125 = GameObject_get_transform_m16A80BB92B6C8C5AB696E447014D45EDF1E4DE34(L_124, /*hidden argument*/NULL);
		NullCheck(L_123);
		Transform_set_parent_mEAE304E1A804E8B83054CEECB5BF1E517196EC13(L_123, L_125, /*hidden argument*/NULL);
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_126 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		bool L_127;
		L_127 = Object_op_Implicit_mC8214E4F028CC2F036CC82BDB81D102A02893499(L_126, /*hidden argument*/NULL);
		if (!L_127)
		{
			goto IL_02c0;
		}
	}
	{
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_128 = __this->get_eyePosition_25();
		NullCheck(L_128);
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_129;
		L_129 = GameObject_get_transform_m16A80BB92B6C8C5AB696E447014D45EDF1E4DE34(L_128, /*hidden argument*/NULL);
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_130 = V_1;
		NullCheck(L_130);
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_131;
		L_131 = GameObject_get_transform_m16A80BB92B6C8C5AB696E447014D45EDF1E4DE34(L_130, /*hidden argument*/NULL);
		NullCheck(L_129);
		Transform_set_parent_mEAE304E1A804E8B83054CEECB5BF1E517196EC13(L_129, L_131, /*hidden argument*/NULL);
	}

IL_02c0:
	{
		return;
	}
}
// System.Void CrazyMinnow.SALSA.RandomEyes3D::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void RandomEyes3D__ctor_mBCB53DB419FB67074996B3785C1F705D12CA80EF (RandomEyes3D_t6868157C4605BD0225E1B08301F110E72336CE45 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1__ctor_m1B9889869FAF2EC3320EFFDBB879511C4C48A20D_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_t4CEBDA821397C4F46F5C0D752B030DA847F86F42_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&RandomEyesBlendAmounts_t728404128F97998E0D58F3849065DCD32A181621_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralDA39A3EE5E6B4B0D3255BFEF95601890AFD80709);
		s_Il2CppMethodInitialized = true;
	}
	{
		StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* L_0 = (StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A*)(StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A*)SZArrayNew(StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A_il2cpp_TypeInfo_var, (uint32_t)0);
		__this->set_blendShapes_9(L_0);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_1;
		memset((&L_1), 0, sizeof(L_1));
		Vector3__ctor_m57495F692C6CE1CEF278CAD9A98221165D37E636_inline((&L_1), (0.0f), (0.0f), (0.0f), /*hidden argument*/NULL);
		__this->set_lookRandPosition_15(L_1);
		RandomEyesBlendAmounts_t728404128F97998E0D58F3849065DCD32A181621 * L_2 = (RandomEyesBlendAmounts_t728404128F97998E0D58F3849065DCD32A181621 *)il2cpp_codegen_object_new(RandomEyesBlendAmounts_t728404128F97998E0D58F3849065DCD32A181621_il2cpp_TypeInfo_var);
		RandomEyesBlendAmounts__ctor_m20EA40B8CB17580CC2E6FB16FF225D40D1CA54FE(L_2, /*hidden argument*/NULL);
		__this->set_lookAmount_16(L_2);
		__this->set_randomEyes_17((bool)1);
		__this->set_maxRandomInterval_18((3.0f));
		__this->set_blendSpeed_19((15.0f));
		__this->set_rangeOfMotion_20((50.0f));
		__this->set_blinkDuration_21((0.0500000007f));
		__this->set_blinkSpeed_22((50.0f));
		__this->set_openMax_23((5.0f));
		__this->set_closeMax_24((100.0f));
		__this->set_targetAffinityPercentage_30((0.5f));
		__this->set_trackBehind_32((bool)1);
		__this->set_randomBlink_33((bool)1);
		__this->set_maxBlinkInterval_34((5.0f));
		__this->set_excludeEyeShapes_44((bool)1);
		StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* L_3 = (StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A*)(StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A*)SZArrayNew(StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A_il2cpp_TypeInfo_var, (uint32_t)0);
		__this->set_dynamicCustomShapes_45(L_3);
		__this->set_randomCustomShapes_49((bool)1);
		__this->set_maxCustomShapeInterval_50((3.0f));
		__this->set_randomCSBlendSpeedMin_51((0.75f));
		__this->set_randomCSBlendSpeedMax_52((2.0f));
		__this->set_randomCSRangeOfMotionMin_53((10.0f));
		__this->set_randomCSRangeOfMotionMax_54((40.0f));
		__this->set_randomCSDurationMin_55((0.25f));
		__this->set_randomCSDurationMax_56((2.5f));
		__this->set_showEyeShapes_59((bool)1);
		__this->set_showTracking_60((bool)1);
		__this->set_showEyeProps_61((bool)1);
		__this->set_showCustom_62((bool)1);
		__this->set_showBroadcast_63((bool)1);
		__this->set_showBroadcastCS_64((bool)1);
		List_1_t4CEBDA821397C4F46F5C0D752B030DA847F86F42 * L_4 = (List_1_t4CEBDA821397C4F46F5C0D752B030DA847F86F42 *)il2cpp_codegen_object_new(List_1_t4CEBDA821397C4F46F5C0D752B030DA847F86F42_il2cpp_TypeInfo_var);
		List_1__ctor_m1B9889869FAF2EC3320EFFDBB879511C4C48A20D(L_4, /*hidden argument*/List_1__ctor_m1B9889869FAF2EC3320EFFDBB879511C4C48A20D_RuntimeMethod_var);
		__this->set_groups_66(L_4);
		__this->set_randomLookRange_69((0.300000012f));
		Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  L_5;
		memset((&L_5), 0, sizeof(L_5));
		Vector2__ctor_m9F1F2D5EB5D1FF7091BB527AC8A72CBB309D115E_inline((&L_5), (1.0f), (5.0f), /*hidden argument*/NULL);
		__this->set_targetAffinityTimerRange_74(L_5);
		__this->set_randomShape_86(_stringLiteralDA39A3EE5E6B4B0D3255BFEF95601890AFD80709);
		MonoBehaviour__ctor_mC0995D847F6A95B1A553652636C38A2AA8B13BED(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void CrazyMinnow.SALSA.RandomEyes3DGizmo::OnDrawGizmos()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void RandomEyes3DGizmo_OnDrawGizmos_m3E702ED02773C0CB2431D737D0193D15D6AFD8EC (RandomEyes3DGizmo_t039AFDA0DE2968F2C3DFB63606D40A2C238839A1 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralCCE432AEDAEF20FC0D1E205E083D0333967EE606);
		s_Il2CppMethodInitialized = true;
	}
	{
		bool L_0 = __this->get_showGizmo_5();
		if (!L_0)
		{
			goto IL_001e;
		}
	}
	{
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_1;
		L_1 = Component_get_transform_mE8496EBC45BEB1BADB5F314960F1DF1C952FA11F(__this, /*hidden argument*/NULL);
		NullCheck(L_1);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_2;
		L_2 = Transform_get_position_m40A8A9895568D56FFC687B57F30E8D53CB5EA341(L_1, /*hidden argument*/NULL);
		Gizmos_DrawIcon_m7591EA380992EF450530CC74B1A7183BF15BF5C3(L_2, _stringLiteralCCE432AEDAEF20FC0D1E205E083D0333967EE606, (bool)1, /*hidden argument*/NULL);
	}

IL_001e:
	{
		return;
	}
}
// System.Void CrazyMinnow.SALSA.RandomEyes3DGizmo::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void RandomEyes3DGizmo__ctor_m31CB5D4DA8919EA40F7E37474FA16BDBB467DAEC (RandomEyes3DGizmo_t039AFDA0DE2968F2C3DFB63606D40A2C238839A1 * __this, const RuntimeMethod* method)
{
	{
		__this->set_showGizmo_5((bool)1);
		MonoBehaviour__ctor_mC0995D847F6A95B1A553652636C38A2AA8B13BED(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void CrazyMinnow.SALSA.RandomEyesBlendAmounts::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void RandomEyesBlendAmounts__ctor_m20EA40B8CB17580CC2E6FB16FF225D40D1CA54FE (RandomEyesBlendAmounts_t728404128F97998E0D58F3849065DCD32A181621 * __this, const RuntimeMethod* method)
{
	{
		Object__ctor_m88880E0413421D13FD95325EDCE231707CE1F405(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void CrazyMinnow.SALSA.RandomEyesCustomShape::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void RandomEyesCustomShape__ctor_m55E79792EBDFA8E4E88E7396AB6E31327D5D0DBF (RandomEyesCustomShape_t10BCA0CEC18C2BBF6A8BD7BFAD2AF005D233C354 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralDA39A3EE5E6B4B0D3255BFEF95601890AFD80709);
		s_Il2CppMethodInitialized = true;
	}
	{
		__this->set_shapeName_1(_stringLiteralDA39A3EE5E6B4B0D3255BFEF95601890AFD80709);
		__this->set_blendSpeed_6((5.0f));
		__this->set_rangeOfMotion_7((100.0f));
		Object__ctor_m88880E0413421D13FD95325EDCE231707CE1F405(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// UnityEngine.Object CrazyMinnow.SALSA.RandomEyesCustomShapeStatus::get_instance()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Object_tF2F3778131EFF286AF62B7B013A170F95A91571A * RandomEyesCustomShapeStatus_get_instance_m3E3018C5AB25E4037D1E5DD7F84F42337B99229F (RandomEyesCustomShapeStatus_tAE795B1C1EF38DFDE0535D6A919E25A96283DD66 * __this, const RuntimeMethod* method)
{
	{
		Object_tF2F3778131EFF286AF62B7B013A170F95A91571A * L_0 = __this->get_U3CinstanceU3Ek__BackingField_8();
		return L_0;
	}
}
// System.Void CrazyMinnow.SALSA.RandomEyesCustomShapeStatus::set_instance(UnityEngine.Object)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void RandomEyesCustomShapeStatus_set_instance_m4F5644F9DB6E38215FA12B1892E396CA0D8281B1 (RandomEyesCustomShapeStatus_tAE795B1C1EF38DFDE0535D6A919E25A96283DD66 * __this, Object_tF2F3778131EFF286AF62B7B013A170F95A91571A * ___value0, const RuntimeMethod* method)
{
	{
		Object_tF2F3778131EFF286AF62B7B013A170F95A91571A * L_0 = ___value0;
		__this->set_U3CinstanceU3Ek__BackingField_8(L_0);
		return;
	}
}
// System.Void CrazyMinnow.SALSA.RandomEyesCustomShapeStatus::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void RandomEyesCustomShapeStatus__ctor_m97BF52FFB152BF60CB27311F72EA547C783657C0 (RandomEyesCustomShapeStatus_tAE795B1C1EF38DFDE0535D6A919E25A96283DD66 * __this, const RuntimeMethod* method)
{
	{
		RandomEyesCustomShape__ctor_m55E79792EBDFA8E4E88E7396AB6E31327D5D0DBF(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// UnityEngine.Object CrazyMinnow.SALSA.RandomEyesLookStatus::get_instance()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Object_tF2F3778131EFF286AF62B7B013A170F95A91571A * RandomEyesLookStatus_get_instance_m701539465A1941EB294C4D49503D98D0AD731E34 (RandomEyesLookStatus_t2C78F0556D5424EFB610AEC14051A53E29757DC4 * __this, const RuntimeMethod* method)
{
	{
		Object_tF2F3778131EFF286AF62B7B013A170F95A91571A * L_0 = __this->get_U3CinstanceU3Ek__BackingField_0();
		return L_0;
	}
}
// System.Void CrazyMinnow.SALSA.RandomEyesLookStatus::set_instance(UnityEngine.Object)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void RandomEyesLookStatus_set_instance_m68125F9E62FDE656F139985323B8500DA3E5AE8D (RandomEyesLookStatus_t2C78F0556D5424EFB610AEC14051A53E29757DC4 * __this, Object_tF2F3778131EFF286AF62B7B013A170F95A91571A * ___value0, const RuntimeMethod* method)
{
	{
		Object_tF2F3778131EFF286AF62B7B013A170F95A91571A * L_0 = ___value0;
		__this->set_U3CinstanceU3Ek__BackingField_0(L_0);
		return;
	}
}
// System.Single CrazyMinnow.SALSA.RandomEyesLookStatus::get_blendSpeed()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float RandomEyesLookStatus_get_blendSpeed_m1AC267734DB387C3AC1040BA8519978D3E62F34D (RandomEyesLookStatus_t2C78F0556D5424EFB610AEC14051A53E29757DC4 * __this, const RuntimeMethod* method)
{
	{
		float L_0 = __this->get_U3CblendSpeedU3Ek__BackingField_1();
		return L_0;
	}
}
// System.Void CrazyMinnow.SALSA.RandomEyesLookStatus::set_blendSpeed(System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void RandomEyesLookStatus_set_blendSpeed_m8D54B2B8FB2CB13A97161B6C5A480FDFD5D258EE (RandomEyesLookStatus_t2C78F0556D5424EFB610AEC14051A53E29757DC4 * __this, float ___value0, const RuntimeMethod* method)
{
	{
		float L_0 = ___value0;
		__this->set_U3CblendSpeedU3Ek__BackingField_1(L_0);
		return;
	}
}
// System.Single CrazyMinnow.SALSA.RandomEyesLookStatus::get_rangeOfMotion()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float RandomEyesLookStatus_get_rangeOfMotion_m96D975B67348C3BB84FC651115084D7957685029 (RandomEyesLookStatus_t2C78F0556D5424EFB610AEC14051A53E29757DC4 * __this, const RuntimeMethod* method)
{
	{
		float L_0 = __this->get_U3CrangeOfMotionU3Ek__BackingField_2();
		return L_0;
	}
}
// System.Void CrazyMinnow.SALSA.RandomEyesLookStatus::set_rangeOfMotion(System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void RandomEyesLookStatus_set_rangeOfMotion_m8E01B94D5F3932364DD566B7676D60B1939B09C9 (RandomEyesLookStatus_t2C78F0556D5424EFB610AEC14051A53E29757DC4 * __this, float ___value0, const RuntimeMethod* method)
{
	{
		float L_0 = ___value0;
		__this->set_U3CrangeOfMotionU3Ek__BackingField_2(L_0);
		return;
	}
}
// System.Void CrazyMinnow.SALSA.RandomEyesLookStatus::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void RandomEyesLookStatus__ctor_m91B47B57FA3F7484BB01B458547D71931629555A (RandomEyesLookStatus_t2C78F0556D5424EFB610AEC14051A53E29757DC4 * __this, const RuntimeMethod* method)
{
	{
		Object__ctor_m88880E0413421D13FD95325EDCE231707CE1F405(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void CrazyMinnow.SALSA.Salsa2D::Reset()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Salsa2D_Reset_mF278D642863E4E5FAC9DDA581B0CFB88B9A6C595 (Salsa2D_t54C8B8FFE3B8149C565BD2A69D53629E65CFFB62 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Component_GetComponent_TisSpriteRenderer_t3F35AD5498243C170B46F5FFDB582AAEF78615EF_m7224DFA0D29BF2205FEA6E432D9BCB4133F6E491_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		SpriteRenderer_t3F35AD5498243C170B46F5FFDB582AAEF78615EF * L_0 = __this->get_spriteRenderer_6();
		IL2CPP_RUNTIME_CLASS_INIT(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		bool L_1;
		L_1 = Object_op_Implicit_mC8214E4F028CC2F036CC82BDB81D102A02893499(L_0, /*hidden argument*/NULL);
		if (L_1)
		{
			goto IL_0019;
		}
	}
	{
		SpriteRenderer_t3F35AD5498243C170B46F5FFDB582AAEF78615EF * L_2;
		L_2 = Component_GetComponent_TisSpriteRenderer_t3F35AD5498243C170B46F5FFDB582AAEF78615EF_m7224DFA0D29BF2205FEA6E432D9BCB4133F6E491(__this, /*hidden argument*/Component_GetComponent_TisSpriteRenderer_t3F35AD5498243C170B46F5FFDB582AAEF78615EF_m7224DFA0D29BF2205FEA6E432D9BCB4133F6E491_RuntimeMethod_var);
		__this->set_spriteRenderer_6(L_2);
	}

IL_0019:
	{
		AudioSource_tC4BF65AF8CDCAA63724BB3CA59A7A29249269E6B * L_3 = __this->get_audioSrc_9();
		IL2CPP_RUNTIME_CLASS_INIT(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		bool L_4;
		L_4 = Object_op_Implicit_mC8214E4F028CC2F036CC82BDB81D102A02893499(L_3, /*hidden argument*/NULL);
		if (L_4)
		{
			goto IL_0032;
		}
	}
	{
		AudioSource_tC4BF65AF8CDCAA63724BB3CA59A7A29249269E6B * L_5;
		L_5 = Salsa2D_FindAudioSource_m4F909704BCA8FAD1285CB91F23783882DFD618C4(__this, /*hidden argument*/NULL);
		__this->set_audioSrc_9(L_5);
	}

IL_0032:
	{
		int32_t L_6 = __this->get_mouthLayer_5();
		Salsa2D_SetOrderInLayer_m2F1C9424774C0A54279E6AC5848890A0C365E834(__this, L_6, /*hidden argument*/NULL);
		return;
	}
}
// System.Void CrazyMinnow.SALSA.Salsa2D::Awake()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Salsa2D_Awake_m9C980FB4ACD64F6B9CD5E64F3D5781CEAF296448 (Salsa2D_t54C8B8FFE3B8149C565BD2A69D53629E65CFFB62 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Salsa2D_t54C8B8FFE3B8149C565BD2A69D53629E65CFFB62_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&SingleU5BU5D_t47E8DBF5B597C122478D1FFBD9DD57399A0650FA_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&WaitForSeconds_t8F9189BE6E467C98C99177038881F8982E0E4013_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		((Salsa2D_t54C8B8FFE3B8149C565BD2A69D53629E65CFFB62_StaticFields*)il2cpp_codegen_static_fields_for(Salsa2D_t54C8B8FFE3B8149C565BD2A69D53629E65CFFB62_il2cpp_TypeInfo_var))->set_instance_4(__this);
		bool L_0 = __this->get_isTalking_19();
		__this->set_prevIsTalking_20(L_0);
		AudioSource_tC4BF65AF8CDCAA63724BB3CA59A7A29249269E6B * L_1 = __this->get_audioSrc_9();
		IL2CPP_RUNTIME_CLASS_INIT(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		bool L_2;
		L_2 = Object_op_Implicit_mC8214E4F028CC2F036CC82BDB81D102A02893499(L_1, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_009b;
		}
	}
	{
		AudioSource_tC4BF65AF8CDCAA63724BB3CA59A7A29249269E6B * L_3 = __this->get_audioSrc_9();
		NullCheck(L_3);
		AudioClip_t16D2E573E7CC1C5118D8EE0F0692D46866A1C0EE * L_4;
		L_4 = AudioSource_get_clip_mE4454E38D2C0A4C8CC780A435FC1DBD4D47D16DC(L_3, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		bool L_5;
		L_5 = Object_op_Equality_mEE9EC7EB5C7DC3E95B94AB904E1986FC4D566D54(L_4, (Object_tF2F3778131EFF286AF62B7B013A170F95A91571A *)NULL, /*hidden argument*/NULL);
		if (!L_5)
		{
			goto IL_0051;
		}
	}
	{
		AudioClip_t16D2E573E7CC1C5118D8EE0F0692D46866A1C0EE * L_6 = __this->get_audioClip_10();
		IL2CPP_RUNTIME_CLASS_INIT(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		bool L_7;
		L_7 = Object_op_Inequality_mE1F187520BD83FB7D86A6D850710C4D42B864E90(L_6, (Object_tF2F3778131EFF286AF62B7B013A170F95A91571A *)NULL, /*hidden argument*/NULL);
		if (!L_7)
		{
			goto IL_0051;
		}
	}
	{
		AudioSource_tC4BF65AF8CDCAA63724BB3CA59A7A29249269E6B * L_8 = __this->get_audioSrc_9();
		AudioClip_t16D2E573E7CC1C5118D8EE0F0692D46866A1C0EE * L_9 = __this->get_audioClip_10();
		NullCheck(L_8);
		AudioSource_set_clip_mD1F50F7BA6EA3AF25B4922473352C5180CFF7B2B(L_8, L_9, /*hidden argument*/NULL);
	}

IL_0051:
	{
		AudioSource_tC4BF65AF8CDCAA63724BB3CA59A7A29249269E6B * L_10 = __this->get_audioSrc_9();
		NullCheck(L_10);
		AudioClip_t16D2E573E7CC1C5118D8EE0F0692D46866A1C0EE * L_11;
		L_11 = AudioSource_get_clip_mE4454E38D2C0A4C8CC780A435FC1DBD4D47D16DC(L_10, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		bool L_12;
		L_12 = Object_op_Inequality_mE1F187520BD83FB7D86A6D850710C4D42B864E90(L_11, (Object_tF2F3778131EFF286AF62B7B013A170F95A91571A *)NULL, /*hidden argument*/NULL);
		if (!L_12)
		{
			goto IL_0083;
		}
	}
	{
		AudioClip_t16D2E573E7CC1C5118D8EE0F0692D46866A1C0EE * L_13 = __this->get_audioClip_10();
		IL2CPP_RUNTIME_CLASS_INIT(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		bool L_14;
		L_14 = Object_op_Equality_mEE9EC7EB5C7DC3E95B94AB904E1986FC4D566D54(L_13, (Object_tF2F3778131EFF286AF62B7B013A170F95A91571A *)NULL, /*hidden argument*/NULL);
		if (!L_14)
		{
			goto IL_0083;
		}
	}
	{
		AudioSource_tC4BF65AF8CDCAA63724BB3CA59A7A29249269E6B * L_15 = __this->get_audioSrc_9();
		NullCheck(L_15);
		AudioClip_t16D2E573E7CC1C5118D8EE0F0692D46866A1C0EE * L_16;
		L_16 = AudioSource_get_clip_mE4454E38D2C0A4C8CC780A435FC1DBD4D47D16DC(L_15, /*hidden argument*/NULL);
		__this->set_audioClip_10(L_16);
	}

IL_0083:
	{
		AudioSource_tC4BF65AF8CDCAA63724BB3CA59A7A29249269E6B * L_17 = __this->get_audioSrc_9();
		NullCheck(L_17);
		bool L_18;
		L_18 = AudioSource_get_playOnAwake_mDB89801F304962AA2577AB653A7BA3F38F8FFF18(L_17, /*hidden argument*/NULL);
		if (!L_18)
		{
			goto IL_009b;
		}
	}
	{
		AudioSource_tC4BF65AF8CDCAA63724BB3CA59A7A29249269E6B * L_19 = __this->get_audioSrc_9();
		NullCheck(L_19);
		AudioSource_Play_mED16664B8F8F3E4D68785C8C00FC96C4DF053AE1(L_19, /*hidden argument*/NULL);
	}

IL_009b:
	{
		int32_t L_20 = __this->get_sampleSize_36();
		SingleU5BU5D_t47E8DBF5B597C122478D1FFBD9DD57399A0650FA* L_21 = (SingleU5BU5D_t47E8DBF5B597C122478D1FFBD9DD57399A0650FA*)(SingleU5BU5D_t47E8DBF5B597C122478D1FFBD9DD57399A0650FA*)SZArrayNew(SingleU5BU5D_t47E8DBF5B597C122478D1FFBD9DD57399A0650FA_il2cpp_TypeInfo_var, (uint32_t)L_20);
		__this->set_sample_31(L_21);
		float L_22 = __this->get_audioUpdateDelay_18();
		WaitForSeconds_t8F9189BE6E467C98C99177038881F8982E0E4013 * L_23 = (WaitForSeconds_t8F9189BE6E467C98C99177038881F8982E0E4013 *)il2cpp_codegen_object_new(WaitForSeconds_t8F9189BE6E467C98C99177038881F8982E0E4013_il2cpp_TypeInfo_var);
		WaitForSeconds__ctor_mD298C4CB9532BBBDE172FC40F3397E30504038D4(L_23, L_22, /*hidden argument*/NULL);
		__this->set_updateSampleDelay_29(L_23);
		RuntimeObject* L_24;
		L_24 = Salsa2D_UpdateSample_m3ECFBCCA2C397C22A1B8715F2F43223C469F7563(__this, /*hidden argument*/NULL);
		Coroutine_t899D5232EF542CB8BA70AF9ECEECA494FAA9CCB7 * L_25;
		L_25 = MonoBehaviour_StartCoroutine_m3E33706D38B23CDD179E99BAD61E32303E9CC719(__this, L_24, /*hidden argument*/NULL);
		return;
	}
}
// System.Void CrazyMinnow.SALSA.Salsa2D::OnEnable()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Salsa2D_OnEnable_mEAB9811C8A6709133703DC6BA21718A4CCFF5BE8 (Salsa2D_t54C8B8FFE3B8149C565BD2A69D53629E65CFFB62 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Component_GetComponent_TisAudioSource_tC4BF65AF8CDCAA63724BB3CA59A7A29249269E6B_mCC3B7A679ECE504BFAF8C70C4EF527511F46902F_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		AudioSource_tC4BF65AF8CDCAA63724BB3CA59A7A29249269E6B * L_0 = __this->get_audioSrc_9();
		IL2CPP_RUNTIME_CLASS_INIT(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		bool L_1;
		L_1 = Object_op_Implicit_mC8214E4F028CC2F036CC82BDB81D102A02893499(L_0, /*hidden argument*/NULL);
		if (L_1)
		{
			goto IL_0019;
		}
	}
	{
		AudioSource_tC4BF65AF8CDCAA63724BB3CA59A7A29249269E6B * L_2;
		L_2 = Component_GetComponent_TisAudioSource_tC4BF65AF8CDCAA63724BB3CA59A7A29249269E6B_mCC3B7A679ECE504BFAF8C70C4EF527511F46902F(__this, /*hidden argument*/Component_GetComponent_TisAudioSource_tC4BF65AF8CDCAA63724BB3CA59A7A29249269E6B_mCC3B7A679ECE504BFAF8C70C4EF527511F46902F_RuntimeMethod_var);
		__this->set_audioSrc_9(L_2);
	}

IL_0019:
	{
		int32_t L_3 = __this->get_mouthLayer_5();
		Salsa2D_SetOrderInLayer_m2F1C9424774C0A54279E6AC5848890A0C365E834(__this, L_3, /*hidden argument*/NULL);
		RuntimeObject* L_4;
		L_4 = Salsa2D_UpdateSample_m3ECFBCCA2C397C22A1B8715F2F43223C469F7563(__this, /*hidden argument*/NULL);
		Coroutine_t899D5232EF542CB8BA70AF9ECEECA494FAA9CCB7 * L_5;
		L_5 = MonoBehaviour_StartCoroutine_m3E33706D38B23CDD179E99BAD61E32303E9CC719(__this, L_4, /*hidden argument*/NULL);
		return;
	}
}
// System.Void CrazyMinnow.SALSA.Salsa2D::LateUpdate()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Salsa2D_LateUpdate_m3D28055ADA1D7F1AAB095581F94054E16BF874BC (Salsa2D_t54C8B8FFE3B8149C565BD2A69D53629E65CFFB62 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&WaitForSeconds_t8F9189BE6E467C98C99177038881F8982E0E4013_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		bool L_0 = __this->get_prevIsTalking_20();
		bool L_1 = __this->get_isTalking_19();
		if ((((int32_t)L_0) == ((int32_t)L_1)))
		{
			goto IL_0028;
		}
	}
	{
		bool L_2 = __this->get_isTalking_19();
		__this->set_prevIsTalking_20(L_2);
		bool L_3 = __this->get_broadcast_21();
		if (!L_3)
		{
			goto IL_0028;
		}
	}
	{
		Salsa2D_TalkStatusChanged_m7EF42D46D528EA86DB9E59DF5FD9C105387EA97C(__this, /*hidden argument*/NULL);
	}

IL_0028:
	{
		AudioSource_tC4BF65AF8CDCAA63724BB3CA59A7A29249269E6B * L_4 = __this->get_audioSrc_9();
		IL2CPP_RUNTIME_CLASS_INIT(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		bool L_5;
		L_5 = Object_op_Implicit_mC8214E4F028CC2F036CC82BDB81D102A02893499(L_4, /*hidden argument*/NULL);
		if (!L_5)
		{
			goto IL_0052;
		}
	}
	{
		AudioSource_tC4BF65AF8CDCAA63724BB3CA59A7A29249269E6B * L_6 = __this->get_audioSrc_9();
		NullCheck(L_6);
		bool L_7;
		L_7 = AudioSource_get_isPlaying_mEA69477C77D542971F7B454946EF25DFBE0AF6A8(L_6, /*hidden argument*/NULL);
		if (!L_7)
		{
			goto IL_004b;
		}
	}
	{
		__this->set_isTalking_19((bool)1);
		goto IL_0052;
	}

IL_004b:
	{
		__this->set_isTalking_19((bool)0);
	}

IL_0052:
	{
		float L_8 = __this->get_prevAudioUpdateDelay_30();
		float L_9 = __this->get_audioUpdateDelay_18();
		if ((((float)L_8) == ((float)L_9)))
		{
			goto IL_007d;
		}
	}
	{
		float L_10 = __this->get_audioUpdateDelay_18();
		WaitForSeconds_t8F9189BE6E467C98C99177038881F8982E0E4013 * L_11 = (WaitForSeconds_t8F9189BE6E467C98C99177038881F8982E0E4013 *)il2cpp_codegen_object_new(WaitForSeconds_t8F9189BE6E467C98C99177038881F8982E0E4013_il2cpp_TypeInfo_var);
		WaitForSeconds__ctor_mD298C4CB9532BBBDE172FC40F3397E30504038D4(L_11, L_10, /*hidden argument*/NULL);
		__this->set_updateSampleDelay_29(L_11);
		float L_12 = __this->get_audioUpdateDelay_18();
		__this->set_prevAudioUpdateDelay_30(L_12);
	}

IL_007d:
	{
		return;
	}
}
// System.Void CrazyMinnow.SALSA.Salsa2D::TalkStatusChanged()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Salsa2D_TalkStatusChanged_m7EF42D46D528EA86DB9E59DF5FD9C105387EA97C (Salsa2D_t54C8B8FFE3B8149C565BD2A69D53629E65CFFB62 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Salsa2D_t54C8B8FFE3B8149C565BD2A69D53629E65CFFB62_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&SalsaStatus_t67641B5EA3D03836981ED128DA718792E18105C1_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral861234844093461379B1BEC77B56549309505C65);
		s_Il2CppMethodInitialized = true;
	}
	SalsaStatus_t67641B5EA3D03836981ED128DA718792E18105C1 * V_0 = NULL;
	int32_t V_1 = 0;
	int32_t V_2 = 0;
	{
		SalsaStatus_t67641B5EA3D03836981ED128DA718792E18105C1 * L_0 = (SalsaStatus_t67641B5EA3D03836981ED128DA718792E18105C1 *)il2cpp_codegen_object_new(SalsaStatus_t67641B5EA3D03836981ED128DA718792E18105C1_il2cpp_TypeInfo_var);
		SalsaStatus__ctor_m60EB1381FDD2EA05C23A1AD9A6005AF854E5E89D(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		SalsaStatus_t67641B5EA3D03836981ED128DA718792E18105C1 * L_1 = V_0;
		Salsa2D_t54C8B8FFE3B8149C565BD2A69D53629E65CFFB62 * L_2 = ((Salsa2D_t54C8B8FFE3B8149C565BD2A69D53629E65CFFB62_StaticFields*)il2cpp_codegen_static_fields_for(Salsa2D_t54C8B8FFE3B8149C565BD2A69D53629E65CFFB62_il2cpp_TypeInfo_var))->get_instance_4();
		NullCheck(L_1);
		SalsaStatus_set_instance_mB67A73851E6FDD5B713B5F85017446B672B32F63_inline(L_1, L_2, /*hidden argument*/NULL);
		SalsaStatus_t67641B5EA3D03836981ED128DA718792E18105C1 * L_3 = V_0;
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_4;
		L_4 = Component_get_gameObject_m55DC35B149AFB9157582755383BA954655FE0C5B(__this, /*hidden argument*/NULL);
		NullCheck(L_4);
		String_t* L_5;
		L_5 = Object_get_name_m0C7BC870ED2F0DC5A2FB09628136CD7D1CB82CFB(L_4, /*hidden argument*/NULL);
		NullCheck(L_3);
		SalsaStatus_set_talkerName_m88E92FFF1840D7288C4474685B82F562E5A6C5EE_inline(L_3, L_5, /*hidden argument*/NULL);
		SalsaStatus_t67641B5EA3D03836981ED128DA718792E18105C1 * L_6 = V_0;
		bool L_7 = __this->get_isTalking_19();
		NullCheck(L_6);
		SalsaStatus_set_isTalking_m621FF64AC69982D2A47B901157DB29C7F3EC6725_inline(L_6, L_7, /*hidden argument*/NULL);
		SalsaStatus_t67641B5EA3D03836981ED128DA718792E18105C1 * L_8 = V_0;
		AudioSource_tC4BF65AF8CDCAA63724BB3CA59A7A29249269E6B * L_9 = __this->get_audioSrc_9();
		NullCheck(L_9);
		AudioClip_t16D2E573E7CC1C5118D8EE0F0692D46866A1C0EE * L_10;
		L_10 = AudioSource_get_clip_mE4454E38D2C0A4C8CC780A435FC1DBD4D47D16DC(L_9, /*hidden argument*/NULL);
		NullCheck(L_10);
		String_t* L_11;
		L_11 = Object_get_name_m0C7BC870ED2F0DC5A2FB09628136CD7D1CB82CFB(L_10, /*hidden argument*/NULL);
		NullCheck(L_8);
		SalsaStatus_set_clipName_m564A868F0584865EBA31D5F8DD38FE39B698F72A_inline(L_8, L_11, /*hidden argument*/NULL);
		GameObjectU5BU5D_tA88FC1A1FC9D4D73D0B3984D4B0ECE88F4C47642* L_12 = __this->get_broadcastReceivers_24();
		NullCheck(L_12);
		if ((((int32_t)((int32_t)((int32_t)(((RuntimeArray*)L_12)->max_length)))) <= ((int32_t)0)))
		{
			goto IL_00a6;
		}
	}
	{
		bool L_13 = __this->get_propagateToChildren_25();
		if (!L_13)
		{
			goto IL_007f;
		}
	}
	{
		V_1 = 0;
		goto IL_0073;
	}

IL_005b:
	{
		GameObjectU5BU5D_tA88FC1A1FC9D4D73D0B3984D4B0ECE88F4C47642* L_14 = __this->get_broadcastReceivers_24();
		int32_t L_15 = V_1;
		NullCheck(L_14);
		int32_t L_16 = L_15;
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_17 = (L_14)->GetAt(static_cast<il2cpp_array_size_t>(L_16));
		SalsaStatus_t67641B5EA3D03836981ED128DA718792E18105C1 * L_18 = V_0;
		NullCheck(L_17);
		GameObject_BroadcastMessage_m2B5D6163ABB0ED80A381A41DC84ED48CC10212AD(L_17, _stringLiteral861234844093461379B1BEC77B56549309505C65, L_18, 1, /*hidden argument*/NULL);
		int32_t L_19 = V_1;
		V_1 = ((int32_t)il2cpp_codegen_add((int32_t)L_19, (int32_t)1));
	}

IL_0073:
	{
		int32_t L_20 = V_1;
		GameObjectU5BU5D_tA88FC1A1FC9D4D73D0B3984D4B0ECE88F4C47642* L_21 = __this->get_broadcastReceivers_24();
		NullCheck(L_21);
		if ((((int32_t)L_20) < ((int32_t)((int32_t)((int32_t)(((RuntimeArray*)L_21)->max_length))))))
		{
			goto IL_005b;
		}
	}
	{
		return;
	}

IL_007f:
	{
		V_2 = 0;
		goto IL_009b;
	}

IL_0083:
	{
		GameObjectU5BU5D_tA88FC1A1FC9D4D73D0B3984D4B0ECE88F4C47642* L_22 = __this->get_broadcastReceivers_24();
		int32_t L_23 = V_2;
		NullCheck(L_22);
		int32_t L_24 = L_23;
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_25 = (L_22)->GetAt(static_cast<il2cpp_array_size_t>(L_24));
		SalsaStatus_t67641B5EA3D03836981ED128DA718792E18105C1 * L_26 = V_0;
		NullCheck(L_25);
		GameObject_SendMessage_mD49CCADA51268480B585733DD7C6540CCCC6EF5C(L_25, _stringLiteral861234844093461379B1BEC77B56549309505C65, L_26, 1, /*hidden argument*/NULL);
		int32_t L_27 = V_2;
		V_2 = ((int32_t)il2cpp_codegen_add((int32_t)L_27, (int32_t)1));
	}

IL_009b:
	{
		int32_t L_28 = V_2;
		GameObjectU5BU5D_tA88FC1A1FC9D4D73D0B3984D4B0ECE88F4C47642* L_29 = __this->get_broadcastReceivers_24();
		NullCheck(L_29);
		if ((((int32_t)L_28) < ((int32_t)((int32_t)((int32_t)(((RuntimeArray*)L_29)->max_length))))))
		{
			goto IL_0083;
		}
	}

IL_00a6:
	{
		return;
	}
}
// System.Void CrazyMinnow.SALSA.Salsa2D::UpdateShape()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Salsa2D_UpdateShape_m81111EF6B2731082FA420939A62870B3B59C3AEF (Salsa2D_t54C8B8FFE3B8149C565BD2A69D53629E65CFFB62 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral1D576BF6F6423EB3D2CD8843BB02C91A7E8D3143);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralAE4B715AAAA58DCCE03CE0702B58CE77A631DE43);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralDA39A3EE5E6B4B0D3255BFEF95601890AFD80709);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralFA880A6E9F663158BE6AFD9CB08CE9CD38B7D3AC);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralFE566FEFE77E0B16136ADCE410AD98FF054E2937);
		s_Il2CppMethodInitialized = true;
	}
	{
		__this->set_say_27(_stringLiteralDA39A3EE5E6B4B0D3255BFEF95601890AFD80709);
		float L_0 = __this->get_average_26();
		float L_1 = __this->get_saySmallTrigger_15();
		if ((!(((float)L_0) < ((float)L_1))))
		{
			goto IL_003d;
		}
	}
	{
		__this->set_say_27(_stringLiteralAE4B715AAAA58DCCE03CE0702B58CE77A631DE43);
		__this->set_sayIndex_28(0);
		SpriteRenderer_t3F35AD5498243C170B46F5FFDB582AAEF78615EF * L_2 = __this->get_spriteRenderer_6();
		Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 * L_3 = __this->get_sayRestSprite_11();
		NullCheck(L_2);
		SpriteRenderer_set_sprite_mBCFFBF3F10C068FD1174C4506DF73E204303FC1A(L_2, L_3, /*hidden argument*/NULL);
		return;
	}

IL_003d:
	{
		float L_4 = __this->get_average_26();
		float L_5 = __this->get_sayMediumTrigger_16();
		if ((!(((float)L_4) < ((float)L_5))))
		{
			goto IL_006f;
		}
	}
	{
		__this->set_say_27(_stringLiteralFA880A6E9F663158BE6AFD9CB08CE9CD38B7D3AC);
		__this->set_sayIndex_28(1);
		SpriteRenderer_t3F35AD5498243C170B46F5FFDB582AAEF78615EF * L_6 = __this->get_spriteRenderer_6();
		Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 * L_7 = __this->get_saySmallSprite_12();
		NullCheck(L_6);
		SpriteRenderer_set_sprite_mBCFFBF3F10C068FD1174C4506DF73E204303FC1A(L_6, L_7, /*hidden argument*/NULL);
		return;
	}

IL_006f:
	{
		float L_8 = __this->get_average_26();
		float L_9 = __this->get_sayLargeTrigger_17();
		if ((!(((float)L_8) < ((float)L_9))))
		{
			goto IL_00a1;
		}
	}
	{
		__this->set_say_27(_stringLiteralFE566FEFE77E0B16136ADCE410AD98FF054E2937);
		__this->set_sayIndex_28(2);
		SpriteRenderer_t3F35AD5498243C170B46F5FFDB582AAEF78615EF * L_10 = __this->get_spriteRenderer_6();
		Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 * L_11 = __this->get_sayMediumSprite_13();
		NullCheck(L_10);
		SpriteRenderer_set_sprite_mBCFFBF3F10C068FD1174C4506DF73E204303FC1A(L_10, L_11, /*hidden argument*/NULL);
		return;
	}

IL_00a1:
	{
		__this->set_say_27(_stringLiteral1D576BF6F6423EB3D2CD8843BB02C91A7E8D3143);
		__this->set_sayIndex_28(3);
		SpriteRenderer_t3F35AD5498243C170B46F5FFDB582AAEF78615EF * L_12 = __this->get_spriteRenderer_6();
		Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 * L_13 = __this->get_sayLargeSprite_14();
		NullCheck(L_12);
		SpriteRenderer_set_sprite_mBCFFBF3F10C068FD1174C4506DF73E204303FC1A(L_12, L_13, /*hidden argument*/NULL);
		return;
	}
}
// System.Collections.IEnumerator CrazyMinnow.SALSA.Salsa2D::UpdateSample()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* Salsa2D_UpdateSample_m3ECFBCCA2C397C22A1B8715F2F43223C469F7563 (Salsa2D_t54C8B8FFE3B8149C565BD2A69D53629E65CFFB62 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CUpdateSampleU3Ed__0_t610463941DB34736C5EE20E7CA02876D017BE95F_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	U3CUpdateSampleU3Ed__0_t610463941DB34736C5EE20E7CA02876D017BE95F * V_0 = NULL;
	{
		U3CUpdateSampleU3Ed__0_t610463941DB34736C5EE20E7CA02876D017BE95F * L_0 = (U3CUpdateSampleU3Ed__0_t610463941DB34736C5EE20E7CA02876D017BE95F *)il2cpp_codegen_object_new(U3CUpdateSampleU3Ed__0_t610463941DB34736C5EE20E7CA02876D017BE95F_il2cpp_TypeInfo_var);
		U3CUpdateSampleU3Ed__0__ctor_m455E0D010D62E290992788EA19E42269D2B13A77(L_0, 0, /*hidden argument*/NULL);
		V_0 = L_0;
		U3CUpdateSampleU3Ed__0_t610463941DB34736C5EE20E7CA02876D017BE95F * L_1 = V_0;
		NullCheck(L_1);
		L_1->set_U3CU3E4__this_2(__this);
		U3CUpdateSampleU3Ed__0_t610463941DB34736C5EE20E7CA02876D017BE95F * L_2 = V_0;
		return L_2;
	}
}
// System.Void CrazyMinnow.SALSA.Salsa2D::ClampValues()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Salsa2D_ClampValues_mF88513A62C56A731A344DD11BDBB0024C8742455 (Salsa2D_t54C8B8FFE3B8149C565BD2A69D53629E65CFFB62 * __this, const RuntimeMethod* method)
{
	{
		float L_0 = __this->get_currentRestValue_32();
		float L_1;
		L_1 = Mathf_Clamp_m2416F3B785C8F135863E3D17E5B0CB4174797B87(L_0, (0.0f), (100.0f), /*hidden argument*/NULL);
		__this->set_currentRestValue_32(L_1);
		float L_2 = __this->get_currentSmallValue_33();
		float L_3;
		L_3 = Mathf_Clamp_m2416F3B785C8F135863E3D17E5B0CB4174797B87(L_2, (0.0f), (100.0f), /*hidden argument*/NULL);
		__this->set_currentSmallValue_33(L_3);
		float L_4 = __this->get_currentMediumValue_34();
		float L_5;
		L_5 = Mathf_Clamp_m2416F3B785C8F135863E3D17E5B0CB4174797B87(L_4, (0.0f), (100.0f), /*hidden argument*/NULL);
		__this->set_currentMediumValue_34(L_5);
		float L_6 = __this->get_currentBigValue_35();
		float L_7;
		L_7 = Mathf_Clamp_m2416F3B785C8F135863E3D17E5B0CB4174797B87(L_6, (0.0f), (100.0f), /*hidden argument*/NULL);
		__this->set_currentBigValue_35(L_7);
		return;
	}
}
// System.Void CrazyMinnow.SALSA.Salsa2D::Play()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Salsa2D_Play_m35EA602BD85046F81EE5B10AECED91841066A1F8 (Salsa2D_t54C8B8FFE3B8149C565BD2A69D53629E65CFFB62 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		AudioSource_tC4BF65AF8CDCAA63724BB3CA59A7A29249269E6B * L_0 = __this->get_audioSrc_9();
		IL2CPP_RUNTIME_CLASS_INIT(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		bool L_1;
		L_1 = Object_op_Implicit_mC8214E4F028CC2F036CC82BDB81D102A02893499(L_0, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0018;
		}
	}
	{
		AudioSource_tC4BF65AF8CDCAA63724BB3CA59A7A29249269E6B * L_2 = __this->get_audioSrc_9();
		NullCheck(L_2);
		AudioSource_Play_mED16664B8F8F3E4D68785C8C00FC96C4DF053AE1(L_2, /*hidden argument*/NULL);
	}

IL_0018:
	{
		return;
	}
}
// System.Void CrazyMinnow.SALSA.Salsa2D::Pause()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Salsa2D_Pause_m42C21144E3C154C1B2EECEF63D3E4601EA50522C (Salsa2D_t54C8B8FFE3B8149C565BD2A69D53629E65CFFB62 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		AudioSource_tC4BF65AF8CDCAA63724BB3CA59A7A29249269E6B * L_0 = __this->get_audioSrc_9();
		IL2CPP_RUNTIME_CLASS_INIT(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		bool L_1;
		L_1 = Object_op_Implicit_mC8214E4F028CC2F036CC82BDB81D102A02893499(L_0, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0018;
		}
	}
	{
		AudioSource_tC4BF65AF8CDCAA63724BB3CA59A7A29249269E6B * L_2 = __this->get_audioSrc_9();
		NullCheck(L_2);
		AudioSource_Pause_mC4F9932A77B6AA2CC3FB720721B7837CF57B675D(L_2, /*hidden argument*/NULL);
	}

IL_0018:
	{
		return;
	}
}
// System.Void CrazyMinnow.SALSA.Salsa2D::Stop()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Salsa2D_Stop_mBC22BAF8743751DB23FB66929BD7E648B39BD653 (Salsa2D_t54C8B8FFE3B8149C565BD2A69D53629E65CFFB62 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		AudioSource_tC4BF65AF8CDCAA63724BB3CA59A7A29249269E6B * L_0 = __this->get_audioSrc_9();
		IL2CPP_RUNTIME_CLASS_INIT(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		bool L_1;
		L_1 = Object_op_Implicit_mC8214E4F028CC2F036CC82BDB81D102A02893499(L_0, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0018;
		}
	}
	{
		AudioSource_tC4BF65AF8CDCAA63724BB3CA59A7A29249269E6B * L_2 = __this->get_audioSrc_9();
		NullCheck(L_2);
		AudioSource_Stop_mADA564D223832A64F8CF3EFBDEB534C0D658810F(L_2, /*hidden argument*/NULL);
	}

IL_0018:
	{
		return;
	}
}
// System.Void CrazyMinnow.SALSA.Salsa2D::SetAudioClip(UnityEngine.AudioClip)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Salsa2D_SetAudioClip_m030D997129C505E6F5D787B66B15A032C2F4EC8B (Salsa2D_t54C8B8FFE3B8149C565BD2A69D53629E65CFFB62 * __this, AudioClip_t16D2E573E7CC1C5118D8EE0F0692D46866A1C0EE * ___audioClip0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		AudioClip_t16D2E573E7CC1C5118D8EE0F0692D46866A1C0EE * L_0 = ___audioClip0;
		__this->set_audioClip_10(L_0);
		AudioSource_tC4BF65AF8CDCAA63724BB3CA59A7A29249269E6B * L_1 = __this->get_audioSrc_9();
		IL2CPP_RUNTIME_CLASS_INIT(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		bool L_2;
		L_2 = Object_op_Implicit_mC8214E4F028CC2F036CC82BDB81D102A02893499(L_1, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_0020;
		}
	}
	{
		AudioSource_tC4BF65AF8CDCAA63724BB3CA59A7A29249269E6B * L_3 = __this->get_audioSrc_9();
		AudioClip_t16D2E573E7CC1C5118D8EE0F0692D46866A1C0EE * L_4 = ___audioClip0;
		NullCheck(L_3);
		AudioSource_set_clip_mD1F50F7BA6EA3AF25B4922473352C5180CFF7B2B(L_3, L_4, /*hidden argument*/NULL);
	}

IL_0020:
	{
		return;
	}
}
// System.Void CrazyMinnow.SALSA.Salsa2D::LoadAudioClip(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Salsa2D_LoadAudioClip_m00E486A9634C76CC9833BE9F8D88D3600F9326F6 (Salsa2D_t54C8B8FFE3B8149C565BD2A69D53629E65CFFB62 * __this, String_t* ___resourceClipName0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&AudioClip_t16D2E573E7CC1C5118D8EE0F0692D46866A1C0EE_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	Object_tF2F3778131EFF286AF62B7B013A170F95A91571A * V_0 = NULL;
	AudioClip_t16D2E573E7CC1C5118D8EE0F0692D46866A1C0EE * V_1 = NULL;
	{
		String_t* L_0 = ___resourceClipName0;
		NullCheck(L_0);
		int32_t L_1;
		L_1 = String_get_Length_m129FC0ADA02FECBED3C0B1A809AE84A5AEE1CF09_inline(L_0, /*hidden argument*/NULL);
		if ((((int32_t)L_1) <= ((int32_t)0)))
		{
			goto IL_0054;
		}
	}
	{
		String_t* L_2 = ___resourceClipName0;
		Object_tF2F3778131EFF286AF62B7B013A170F95A91571A * L_3;
		L_3 = Resources_Load_m011631B3740AFD38D496838F10D3DA635A061120(L_2, /*hidden argument*/NULL);
		V_0 = ((AudioClip_t16D2E573E7CC1C5118D8EE0F0692D46866A1C0EE *)IsInstSealed((RuntimeObject*)L_3, AudioClip_t16D2E573E7CC1C5118D8EE0F0692D46866A1C0EE_il2cpp_TypeInfo_var));
		Object_tF2F3778131EFF286AF62B7B013A170F95A91571A * L_4 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		bool L_5;
		L_5 = Object_op_Inequality_mE1F187520BD83FB7D86A6D850710C4D42B864E90(L_4, (Object_tF2F3778131EFF286AF62B7B013A170F95A91571A *)NULL, /*hidden argument*/NULL);
		if (!L_5)
		{
			goto IL_0074;
		}
	}
	{
		Object_tF2F3778131EFF286AF62B7B013A170F95A91571A * L_6 = V_0;
		V_1 = ((AudioClip_t16D2E573E7CC1C5118D8EE0F0692D46866A1C0EE *)CastclassSealed((RuntimeObject*)L_6, AudioClip_t16D2E573E7CC1C5118D8EE0F0692D46866A1C0EE_il2cpp_TypeInfo_var));
		AudioClip_t16D2E573E7CC1C5118D8EE0F0692D46866A1C0EE * L_7 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		bool L_8;
		L_8 = Object_op_Inequality_mE1F187520BD83FB7D86A6D850710C4D42B864E90(L_7, (Object_tF2F3778131EFF286AF62B7B013A170F95A91571A *)NULL, /*hidden argument*/NULL);
		if (!L_8)
		{
			goto IL_0035;
		}
	}
	{
		AudioClip_t16D2E573E7CC1C5118D8EE0F0692D46866A1C0EE * L_9 = V_1;
		__this->set_audioClip_10(L_9);
	}

IL_0035:
	{
		AudioSource_tC4BF65AF8CDCAA63724BB3CA59A7A29249269E6B * L_10 = __this->get_audioSrc_9();
		IL2CPP_RUNTIME_CLASS_INIT(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		bool L_11;
		L_11 = Object_op_Implicit_mC8214E4F028CC2F036CC82BDB81D102A02893499(L_10, /*hidden argument*/NULL);
		if (!L_11)
		{
			goto IL_0074;
		}
	}
	{
		AudioSource_tC4BF65AF8CDCAA63724BB3CA59A7A29249269E6B * L_12 = __this->get_audioSrc_9();
		AudioClip_t16D2E573E7CC1C5118D8EE0F0692D46866A1C0EE * L_13 = __this->get_audioClip_10();
		NullCheck(L_12);
		AudioSource_set_clip_mD1F50F7BA6EA3AF25B4922473352C5180CFF7B2B(L_12, L_13, /*hidden argument*/NULL);
		return;
	}

IL_0054:
	{
		__this->set_audioClip_10((AudioClip_t16D2E573E7CC1C5118D8EE0F0692D46866A1C0EE *)NULL);
		AudioSource_tC4BF65AF8CDCAA63724BB3CA59A7A29249269E6B * L_14 = __this->get_audioSrc_9();
		IL2CPP_RUNTIME_CLASS_INIT(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		bool L_15;
		L_15 = Object_op_Implicit_mC8214E4F028CC2F036CC82BDB81D102A02893499(L_14, /*hidden argument*/NULL);
		if (!L_15)
		{
			goto IL_0074;
		}
	}
	{
		AudioSource_tC4BF65AF8CDCAA63724BB3CA59A7A29249269E6B * L_16 = __this->get_audioSrc_9();
		NullCheck(L_16);
		AudioSource_set_clip_mD1F50F7BA6EA3AF25B4922473352C5180CFF7B2B(L_16, (AudioClip_t16D2E573E7CC1C5118D8EE0F0692D46866A1C0EE *)NULL, /*hidden argument*/NULL);
	}

IL_0074:
	{
		return;
	}
}
// UnityEngine.SpriteRenderer CrazyMinnow.SALSA.Salsa2D::FindSpriteRenderer()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR SpriteRenderer_t3F35AD5498243C170B46F5FFDB582AAEF78615EF * Salsa2D_FindSpriteRenderer_m568528C27E3DA94591E872864EFF993F32B11D0E (Salsa2D_t54C8B8FFE3B8149C565BD2A69D53629E65CFFB62 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Component_GetComponent_TisSpriteRenderer_t3F35AD5498243C170B46F5FFDB582AAEF78615EF_m7224DFA0D29BF2205FEA6E432D9BCB4133F6E491_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		SpriteRenderer_t3F35AD5498243C170B46F5FFDB582AAEF78615EF * L_0 = __this->get_spriteRenderer_6();
		IL2CPP_RUNTIME_CLASS_INIT(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		bool L_1;
		L_1 = Object_op_Implicit_mC8214E4F028CC2F036CC82BDB81D102A02893499(L_0, /*hidden argument*/NULL);
		if (L_1)
		{
			goto IL_0019;
		}
	}
	{
		SpriteRenderer_t3F35AD5498243C170B46F5FFDB582AAEF78615EF * L_2;
		L_2 = Component_GetComponent_TisSpriteRenderer_t3F35AD5498243C170B46F5FFDB582AAEF78615EF_m7224DFA0D29BF2205FEA6E432D9BCB4133F6E491(__this, /*hidden argument*/Component_GetComponent_TisSpriteRenderer_t3F35AD5498243C170B46F5FFDB582AAEF78615EF_m7224DFA0D29BF2205FEA6E432D9BCB4133F6E491_RuntimeMethod_var);
		__this->set_spriteRenderer_6(L_2);
	}

IL_0019:
	{
		SpriteRenderer_t3F35AD5498243C170B46F5FFDB582AAEF78615EF * L_3 = __this->get_spriteRenderer_6();
		return L_3;
	}
}
// UnityEngine.AudioSource CrazyMinnow.SALSA.Salsa2D::FindAudioSource()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR AudioSource_tC4BF65AF8CDCAA63724BB3CA59A7A29249269E6B * Salsa2D_FindAudioSource_m4F909704BCA8FAD1285CB91F23783882DFD618C4 (Salsa2D_t54C8B8FFE3B8149C565BD2A69D53629E65CFFB62 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Component_GetComponent_TisAudioSource_tC4BF65AF8CDCAA63724BB3CA59A7A29249269E6B_mCC3B7A679ECE504BFAF8C70C4EF527511F46902F_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&GameObject_AddComponent_TisAudioSource_tC4BF65AF8CDCAA63724BB3CA59A7A29249269E6B_m74F4A6C820807E361696D4E8F71DC1E54BBE7F76_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	AudioSource_tC4BF65AF8CDCAA63724BB3CA59A7A29249269E6B * V_0 = NULL;
	{
		AudioSource_tC4BF65AF8CDCAA63724BB3CA59A7A29249269E6B * L_0 = __this->get_audioSrc_9();
		V_0 = L_0;
		AudioSource_tC4BF65AF8CDCAA63724BB3CA59A7A29249269E6B * L_1 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		bool L_2;
		L_2 = Object_op_Implicit_mC8214E4F028CC2F036CC82BDB81D102A02893499(L_1, /*hidden argument*/NULL);
		if (L_2)
		{
			goto IL_002a;
		}
	}
	{
		AudioSource_tC4BF65AF8CDCAA63724BB3CA59A7A29249269E6B * L_3;
		L_3 = Component_GetComponent_TisAudioSource_tC4BF65AF8CDCAA63724BB3CA59A7A29249269E6B_mCC3B7A679ECE504BFAF8C70C4EF527511F46902F(__this, /*hidden argument*/Component_GetComponent_TisAudioSource_tC4BF65AF8CDCAA63724BB3CA59A7A29249269E6B_mCC3B7A679ECE504BFAF8C70C4EF527511F46902F_RuntimeMethod_var);
		V_0 = L_3;
		AudioSource_tC4BF65AF8CDCAA63724BB3CA59A7A29249269E6B * L_4 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		bool L_5;
		L_5 = Object_op_Implicit_mC8214E4F028CC2F036CC82BDB81D102A02893499(L_4, /*hidden argument*/NULL);
		if (L_5)
		{
			goto IL_002a;
		}
	}
	{
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_6;
		L_6 = Component_get_gameObject_m55DC35B149AFB9157582755383BA954655FE0C5B(__this, /*hidden argument*/NULL);
		NullCheck(L_6);
		AudioSource_tC4BF65AF8CDCAA63724BB3CA59A7A29249269E6B * L_7;
		L_7 = GameObject_AddComponent_TisAudioSource_tC4BF65AF8CDCAA63724BB3CA59A7A29249269E6B_m74F4A6C820807E361696D4E8F71DC1E54BBE7F76(L_6, /*hidden argument*/GameObject_AddComponent_TisAudioSource_tC4BF65AF8CDCAA63724BB3CA59A7A29249269E6B_m74F4A6C820807E361696D4E8F71DC1E54BBE7F76_RuntimeMethod_var);
		V_0 = L_7;
	}

IL_002a:
	{
		AudioSource_tC4BF65AF8CDCAA63724BB3CA59A7A29249269E6B * L_8 = V_0;
		return L_8;
	}
}
// System.Void CrazyMinnow.SALSA.Salsa2D::SetOrderInLayer(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Salsa2D_SetOrderInLayer_m2F1C9424774C0A54279E6AC5848890A0C365E834 (Salsa2D_t54C8B8FFE3B8149C565BD2A69D53629E65CFFB62 * __this, int32_t ___order0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		int32_t L_0 = ___order0;
		__this->set_mouthLayer_5(L_0);
		SpriteRenderer_t3F35AD5498243C170B46F5FFDB582AAEF78615EF * L_1 = __this->get_spriteRenderer_6();
		IL2CPP_RUNTIME_CLASS_INIT(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		bool L_2;
		L_2 = Object_op_Implicit_mC8214E4F028CC2F036CC82BDB81D102A02893499(L_1, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_0025;
		}
	}
	{
		SpriteRenderer_t3F35AD5498243C170B46F5FFDB582AAEF78615EF * L_3 = __this->get_spriteRenderer_6();
		int32_t L_4 = __this->get_mouthLayer_5();
		NullCheck(L_3);
		Renderer_set_sortingOrder_mAABE4F8F9B158068C8A1582ACE0BFEA3CF499139(L_3, L_4, /*hidden argument*/NULL);
	}

IL_0025:
	{
		return;
	}
}
// System.Void CrazyMinnow.SALSA.Salsa2D::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Salsa2D__ctor_mCF46C454E09DDAE70FF51E82150EED81A9FC8094 (Salsa2D_t54C8B8FFE3B8149C565BD2A69D53629E65CFFB62 * __this, const RuntimeMethod* method)
{
	{
		__this->set_mouthLayer_5(3);
		__this->set_saySmallTrigger_15((0.000500000024f));
		__this->set_sayMediumTrigger_16((0.00300000003f));
		__this->set_sayLargeTrigger_17((0.00549999997f));
		__this->set_audioUpdateDelay_18((0.0799999982f));
		__this->set_broadcastReceiversCount_22(1);
		__this->set_sampleSize_36(((int32_t)64));
		__this->set_writeAverage_37((bool)1);
		MonoBehaviour__ctor_mC0995D847F6A95B1A553652636C38A2AA8B13BED(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void CrazyMinnow.SALSA.Salsa3D::Reset()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Salsa3D_Reset_m53CBF6B12A477D9B56DDDB9486EF2B5C887DED58 (Salsa3D_tACF83D529139263662FEAAC8F5B53EC43B140C9C * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Component_GetComponent_TisSkinnedMeshRenderer_t126F4D6010E0F4B2685A7817B0A9171805D8F496_m48EF3D17CF12700CC28C88CEFBB6741D6E1FFFE3_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		SkinnedMeshRenderer_t126F4D6010E0F4B2685A7817B0A9171805D8F496 * L_0 = __this->get_skinnedMeshRenderer_5();
		IL2CPP_RUNTIME_CLASS_INIT(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		bool L_1;
		L_1 = Object_op_Implicit_mC8214E4F028CC2F036CC82BDB81D102A02893499(L_0, /*hidden argument*/NULL);
		if (L_1)
		{
			goto IL_0019;
		}
	}
	{
		SkinnedMeshRenderer_t126F4D6010E0F4B2685A7817B0A9171805D8F496 * L_2;
		L_2 = Component_GetComponent_TisSkinnedMeshRenderer_t126F4D6010E0F4B2685A7817B0A9171805D8F496_m48EF3D17CF12700CC28C88CEFBB6741D6E1FFFE3(__this, /*hidden argument*/Component_GetComponent_TisSkinnedMeshRenderer_t126F4D6010E0F4B2685A7817B0A9171805D8F496_m48EF3D17CF12700CC28C88CEFBB6741D6E1FFFE3_RuntimeMethod_var);
		__this->set_skinnedMeshRenderer_5(L_2);
	}

IL_0019:
	{
		AudioSource_tC4BF65AF8CDCAA63724BB3CA59A7A29249269E6B * L_3 = __this->get_audioSrc_8();
		IL2CPP_RUNTIME_CLASS_INIT(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		bool L_4;
		L_4 = Object_op_Implicit_mC8214E4F028CC2F036CC82BDB81D102A02893499(L_3, /*hidden argument*/NULL);
		if (L_4)
		{
			goto IL_0032;
		}
	}
	{
		AudioSource_tC4BF65AF8CDCAA63724BB3CA59A7A29249269E6B * L_5;
		L_5 = Salsa3D_FindAudioSource_mDE3A59BD7075854BE6C62C69780834597C8F2281(__this, /*hidden argument*/NULL);
		__this->set_audioSrc_8(L_5);
	}

IL_0032:
	{
		return;
	}
}
// System.Void CrazyMinnow.SALSA.Salsa3D::Awake()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Salsa3D_Awake_m532A8AE4ACDD0CCD856228FF388FD17D07F65E58 (Salsa3D_tACF83D529139263662FEAAC8F5B53EC43B140C9C * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Salsa3D_tACF83D529139263662FEAAC8F5B53EC43B140C9C_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&SingleU5BU5D_t47E8DBF5B597C122478D1FFBD9DD57399A0650FA_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&WaitForSeconds_t8F9189BE6E467C98C99177038881F8982E0E4013_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		((Salsa3D_tACF83D529139263662FEAAC8F5B53EC43B140C9C_StaticFields*)il2cpp_codegen_static_fields_for(Salsa3D_tACF83D529139263662FEAAC8F5B53EC43B140C9C_il2cpp_TypeInfo_var))->set_instance_4(__this);
		bool L_0 = __this->get_isTalking_20();
		__this->set_prevIsTalking_21(L_0);
		AudioSource_tC4BF65AF8CDCAA63724BB3CA59A7A29249269E6B * L_1 = __this->get_audioSrc_8();
		IL2CPP_RUNTIME_CLASS_INIT(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		bool L_2;
		L_2 = Object_op_Implicit_mC8214E4F028CC2F036CC82BDB81D102A02893499(L_1, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_009b;
		}
	}
	{
		AudioSource_tC4BF65AF8CDCAA63724BB3CA59A7A29249269E6B * L_3 = __this->get_audioSrc_8();
		NullCheck(L_3);
		AudioClip_t16D2E573E7CC1C5118D8EE0F0692D46866A1C0EE * L_4;
		L_4 = AudioSource_get_clip_mE4454E38D2C0A4C8CC780A435FC1DBD4D47D16DC(L_3, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		bool L_5;
		L_5 = Object_op_Equality_mEE9EC7EB5C7DC3E95B94AB904E1986FC4D566D54(L_4, (Object_tF2F3778131EFF286AF62B7B013A170F95A91571A *)NULL, /*hidden argument*/NULL);
		if (!L_5)
		{
			goto IL_0051;
		}
	}
	{
		AudioClip_t16D2E573E7CC1C5118D8EE0F0692D46866A1C0EE * L_6 = __this->get_audioClip_9();
		IL2CPP_RUNTIME_CLASS_INIT(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		bool L_7;
		L_7 = Object_op_Inequality_mE1F187520BD83FB7D86A6D850710C4D42B864E90(L_6, (Object_tF2F3778131EFF286AF62B7B013A170F95A91571A *)NULL, /*hidden argument*/NULL);
		if (!L_7)
		{
			goto IL_0051;
		}
	}
	{
		AudioSource_tC4BF65AF8CDCAA63724BB3CA59A7A29249269E6B * L_8 = __this->get_audioSrc_8();
		AudioClip_t16D2E573E7CC1C5118D8EE0F0692D46866A1C0EE * L_9 = __this->get_audioClip_9();
		NullCheck(L_8);
		AudioSource_set_clip_mD1F50F7BA6EA3AF25B4922473352C5180CFF7B2B(L_8, L_9, /*hidden argument*/NULL);
	}

IL_0051:
	{
		AudioSource_tC4BF65AF8CDCAA63724BB3CA59A7A29249269E6B * L_10 = __this->get_audioSrc_8();
		NullCheck(L_10);
		AudioClip_t16D2E573E7CC1C5118D8EE0F0692D46866A1C0EE * L_11;
		L_11 = AudioSource_get_clip_mE4454E38D2C0A4C8CC780A435FC1DBD4D47D16DC(L_10, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		bool L_12;
		L_12 = Object_op_Inequality_mE1F187520BD83FB7D86A6D850710C4D42B864E90(L_11, (Object_tF2F3778131EFF286AF62B7B013A170F95A91571A *)NULL, /*hidden argument*/NULL);
		if (!L_12)
		{
			goto IL_0083;
		}
	}
	{
		AudioClip_t16D2E573E7CC1C5118D8EE0F0692D46866A1C0EE * L_13 = __this->get_audioClip_9();
		IL2CPP_RUNTIME_CLASS_INIT(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		bool L_14;
		L_14 = Object_op_Equality_mEE9EC7EB5C7DC3E95B94AB904E1986FC4D566D54(L_13, (Object_tF2F3778131EFF286AF62B7B013A170F95A91571A *)NULL, /*hidden argument*/NULL);
		if (!L_14)
		{
			goto IL_0083;
		}
	}
	{
		AudioSource_tC4BF65AF8CDCAA63724BB3CA59A7A29249269E6B * L_15 = __this->get_audioSrc_8();
		NullCheck(L_15);
		AudioClip_t16D2E573E7CC1C5118D8EE0F0692D46866A1C0EE * L_16;
		L_16 = AudioSource_get_clip_mE4454E38D2C0A4C8CC780A435FC1DBD4D47D16DC(L_15, /*hidden argument*/NULL);
		__this->set_audioClip_9(L_16);
	}

IL_0083:
	{
		AudioSource_tC4BF65AF8CDCAA63724BB3CA59A7A29249269E6B * L_17 = __this->get_audioSrc_8();
		NullCheck(L_17);
		bool L_18;
		L_18 = AudioSource_get_playOnAwake_mDB89801F304962AA2577AB653A7BA3F38F8FFF18(L_17, /*hidden argument*/NULL);
		if (!L_18)
		{
			goto IL_009b;
		}
	}
	{
		AudioSource_tC4BF65AF8CDCAA63724BB3CA59A7A29249269E6B * L_19 = __this->get_audioSrc_8();
		NullCheck(L_19);
		AudioSource_Play_mED16664B8F8F3E4D68785C8C00FC96C4DF053AE1(L_19, /*hidden argument*/NULL);
	}

IL_009b:
	{
		int32_t L_20 = __this->get_sampleSize_37();
		SingleU5BU5D_t47E8DBF5B597C122478D1FFBD9DD57399A0650FA* L_21 = (SingleU5BU5D_t47E8DBF5B597C122478D1FFBD9DD57399A0650FA*)(SingleU5BU5D_t47E8DBF5B597C122478D1FFBD9DD57399A0650FA*)SZArrayNew(SingleU5BU5D_t47E8DBF5B597C122478D1FFBD9DD57399A0650FA_il2cpp_TypeInfo_var, (uint32_t)L_20);
		__this->set_sample_33(L_21);
		float L_22 = __this->get_audioUpdateDelay_17();
		WaitForSeconds_t8F9189BE6E467C98C99177038881F8982E0E4013 * L_23 = (WaitForSeconds_t8F9189BE6E467C98C99177038881F8982E0E4013 *)il2cpp_codegen_object_new(WaitForSeconds_t8F9189BE6E467C98C99177038881F8982E0E4013_il2cpp_TypeInfo_var);
		WaitForSeconds__ctor_mD298C4CB9532BBBDE172FC40F3397E30504038D4(L_23, L_22, /*hidden argument*/NULL);
		__this->set_updateSampleDelay_31(L_23);
		RuntimeObject* L_24;
		L_24 = Salsa3D_UpdateSample_mB63EC23E06A628ADCF06EEFCE0735FF9D2E0D727(__this, /*hidden argument*/NULL);
		Coroutine_t899D5232EF542CB8BA70AF9ECEECA494FAA9CCB7 * L_25;
		L_25 = MonoBehaviour_StartCoroutine_m3E33706D38B23CDD179E99BAD61E32303E9CC719(__this, L_24, /*hidden argument*/NULL);
		return;
	}
}
// System.Void CrazyMinnow.SALSA.Salsa3D::OnEnable()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Salsa3D_OnEnable_m85CD4CE8B73639E5D58DD48BEF309F80C27A555F (Salsa3D_tACF83D529139263662FEAAC8F5B53EC43B140C9C * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		AudioSource_tC4BF65AF8CDCAA63724BB3CA59A7A29249269E6B * L_0 = __this->get_audioSrc_8();
		IL2CPP_RUNTIME_CLASS_INIT(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		bool L_1;
		L_1 = Object_op_Implicit_mC8214E4F028CC2F036CC82BDB81D102A02893499(L_0, /*hidden argument*/NULL);
		if (L_1)
		{
			goto IL_0019;
		}
	}
	{
		AudioSource_tC4BF65AF8CDCAA63724BB3CA59A7A29249269E6B * L_2;
		L_2 = Salsa3D_FindAudioSource_mDE3A59BD7075854BE6C62C69780834597C8F2281(__this, /*hidden argument*/NULL);
		__this->set_audioSrc_8(L_2);
	}

IL_0019:
	{
		RuntimeObject* L_3;
		L_3 = Salsa3D_UpdateSample_mB63EC23E06A628ADCF06EEFCE0735FF9D2E0D727(__this, /*hidden argument*/NULL);
		Coroutine_t899D5232EF542CB8BA70AF9ECEECA494FAA9CCB7 * L_4;
		L_4 = MonoBehaviour_StartCoroutine_m3E33706D38B23CDD179E99BAD61E32303E9CC719(__this, L_3, /*hidden argument*/NULL);
		return;
	}
}
// System.Void CrazyMinnow.SALSA.Salsa3D::LateUpdate()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Salsa3D_LateUpdate_mA3E1A807016185BCE0DDDD8E2136A84F89267577 (Salsa3D_tACF83D529139263662FEAAC8F5B53EC43B140C9C * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&WaitForSeconds_t8F9189BE6E467C98C99177038881F8982E0E4013_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral1D576BF6F6423EB3D2CD8843BB02C91A7E8D3143);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralAE4B715AAAA58DCCE03CE0702B58CE77A631DE43);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralFA880A6E9F663158BE6AFD9CB08CE9CD38B7D3AC);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralFE566FEFE77E0B16136ADCE410AD98FF054E2937);
		s_Il2CppMethodInitialized = true;
	}
	{
		bool L_0 = __this->get_prevIsTalking_21();
		bool L_1 = __this->get_isTalking_20();
		if ((((int32_t)L_0) == ((int32_t)L_1)))
		{
			goto IL_0028;
		}
	}
	{
		bool L_2 = __this->get_isTalking_20();
		__this->set_prevIsTalking_21(L_2);
		bool L_3 = __this->get_broadcast_22();
		if (!L_3)
		{
			goto IL_0028;
		}
	}
	{
		Salsa3D_TalkStatusChanged_m4C6E88F7F47206891F1C4227E68A3896C39254B5(__this, /*hidden argument*/NULL);
	}

IL_0028:
	{
		float L_4 = __this->get_average_27();
		float L_5 = __this->get_saySmallTrigger_14();
		if ((!(((float)L_4) < ((float)L_5))))
		{
			goto IL_00a7;
		}
	}
	{
		__this->set_say_28(_stringLiteralAE4B715AAAA58DCCE03CE0702B58CE77A631DE43);
		__this->set_sayIndex_29(0);
		float L_6 = __this->get_saySmallValue_34();
		float L_7 = __this->get_blendSpeed_18();
		float L_8 = __this->get_rangeOfMotion_19();
		float L_9;
		L_9 = SalsaUtility_LerpRangeOfMotion_mDA97D8D31F6D62217220938CD785C48A75AEB246(L_6, L_7, L_8, 1, /*hidden argument*/NULL);
		__this->set_saySmallValue_34(L_9);
		float L_10 = __this->get_sayMediumValue_35();
		float L_11 = __this->get_blendSpeed_18();
		float L_12 = __this->get_rangeOfMotion_19();
		float L_13;
		L_13 = SalsaUtility_LerpRangeOfMotion_mDA97D8D31F6D62217220938CD785C48A75AEB246(L_10, L_11, L_12, 1, /*hidden argument*/NULL);
		__this->set_sayMediumValue_35(L_13);
		float L_14 = __this->get_sayLargeValue_36();
		float L_15 = __this->get_blendSpeed_18();
		float L_16 = __this->get_rangeOfMotion_19();
		float L_17;
		L_17 = SalsaUtility_LerpRangeOfMotion_mDA97D8D31F6D62217220938CD785C48A75AEB246(L_14, L_15, L_16, 1, /*hidden argument*/NULL);
		__this->set_sayLargeValue_36(L_17);
		goto IL_020e;
	}

IL_00a7:
	{
		float L_18 = __this->get_average_27();
		float L_19 = __this->get_sayMediumTrigger_15();
		if ((!(((float)L_18) < ((float)L_19))))
		{
			goto IL_0126;
		}
	}
	{
		__this->set_say_28(_stringLiteralFA880A6E9F663158BE6AFD9CB08CE9CD38B7D3AC);
		__this->set_sayIndex_29(1);
		float L_20 = __this->get_saySmallValue_34();
		float L_21 = __this->get_blendSpeed_18();
		float L_22 = __this->get_rangeOfMotion_19();
		float L_23;
		L_23 = SalsaUtility_LerpRangeOfMotion_mDA97D8D31F6D62217220938CD785C48A75AEB246(L_20, L_21, L_22, 0, /*hidden argument*/NULL);
		__this->set_saySmallValue_34(L_23);
		float L_24 = __this->get_sayMediumValue_35();
		float L_25 = __this->get_blendSpeed_18();
		float L_26 = __this->get_rangeOfMotion_19();
		float L_27;
		L_27 = SalsaUtility_LerpRangeOfMotion_mDA97D8D31F6D62217220938CD785C48A75AEB246(L_24, L_25, L_26, 1, /*hidden argument*/NULL);
		__this->set_sayMediumValue_35(L_27);
		float L_28 = __this->get_sayLargeValue_36();
		float L_29 = __this->get_blendSpeed_18();
		float L_30 = __this->get_rangeOfMotion_19();
		float L_31;
		L_31 = SalsaUtility_LerpRangeOfMotion_mDA97D8D31F6D62217220938CD785C48A75AEB246(L_28, L_29, L_30, 1, /*hidden argument*/NULL);
		__this->set_sayLargeValue_36(L_31);
		goto IL_020e;
	}

IL_0126:
	{
		float L_32 = __this->get_average_27();
		float L_33 = __this->get_sayLargeTrigger_16();
		if ((!(((float)L_32) < ((float)L_33))))
		{
			goto IL_01a2;
		}
	}
	{
		__this->set_say_28(_stringLiteralFE566FEFE77E0B16136ADCE410AD98FF054E2937);
		__this->set_sayIndex_29(2);
		float L_34 = __this->get_saySmallValue_34();
		float L_35 = __this->get_blendSpeed_18();
		float L_36 = __this->get_rangeOfMotion_19();
		float L_37;
		L_37 = SalsaUtility_LerpRangeOfMotion_mDA97D8D31F6D62217220938CD785C48A75AEB246(L_34, L_35, L_36, 1, /*hidden argument*/NULL);
		__this->set_saySmallValue_34(L_37);
		float L_38 = __this->get_sayMediumValue_35();
		float L_39 = __this->get_blendSpeed_18();
		float L_40 = __this->get_rangeOfMotion_19();
		float L_41;
		L_41 = SalsaUtility_LerpRangeOfMotion_mDA97D8D31F6D62217220938CD785C48A75AEB246(L_38, L_39, L_40, 0, /*hidden argument*/NULL);
		__this->set_sayMediumValue_35(L_41);
		float L_42 = __this->get_sayLargeValue_36();
		float L_43 = __this->get_blendSpeed_18();
		float L_44 = __this->get_rangeOfMotion_19();
		float L_45;
		L_45 = SalsaUtility_LerpRangeOfMotion_mDA97D8D31F6D62217220938CD785C48A75AEB246(L_42, L_43, L_44, 1, /*hidden argument*/NULL);
		__this->set_sayLargeValue_36(L_45);
		goto IL_020e;
	}

IL_01a2:
	{
		__this->set_say_28(_stringLiteral1D576BF6F6423EB3D2CD8843BB02C91A7E8D3143);
		__this->set_sayIndex_29(3);
		float L_46 = __this->get_saySmallValue_34();
		float L_47 = __this->get_blendSpeed_18();
		float L_48 = __this->get_rangeOfMotion_19();
		float L_49;
		L_49 = SalsaUtility_LerpRangeOfMotion_mDA97D8D31F6D62217220938CD785C48A75AEB246(L_46, L_47, L_48, 1, /*hidden argument*/NULL);
		__this->set_saySmallValue_34(L_49);
		float L_50 = __this->get_sayMediumValue_35();
		float L_51 = __this->get_blendSpeed_18();
		float L_52 = __this->get_rangeOfMotion_19();
		float L_53;
		L_53 = SalsaUtility_LerpRangeOfMotion_mDA97D8D31F6D62217220938CD785C48A75AEB246(L_50, L_51, L_52, 1, /*hidden argument*/NULL);
		__this->set_sayMediumValue_35(L_53);
		float L_54 = __this->get_sayLargeValue_36();
		float L_55 = __this->get_blendSpeed_18();
		float L_56 = __this->get_rangeOfMotion_19();
		float L_57;
		L_57 = SalsaUtility_LerpRangeOfMotion_mDA97D8D31F6D62217220938CD785C48A75AEB246(L_54, L_55, L_56, 0, /*hidden argument*/NULL);
		__this->set_sayLargeValue_36(L_57);
	}

IL_020e:
	{
		SalsaBlendAmounts_t8FBE1BDDD1306BB8232FCAAD43FC87042840D626 * L_58 = __this->get_sayAmount_30();
		float L_59 = __this->get_saySmallValue_34();
		NullCheck(L_58);
		L_58->set_saySmall_0(L_59);
		SalsaBlendAmounts_t8FBE1BDDD1306BB8232FCAAD43FC87042840D626 * L_60 = __this->get_sayAmount_30();
		float L_61 = __this->get_sayMediumValue_35();
		NullCheck(L_60);
		L_60->set_sayMedium_1(L_61);
		SalsaBlendAmounts_t8FBE1BDDD1306BB8232FCAAD43FC87042840D626 * L_62 = __this->get_sayAmount_30();
		float L_63 = __this->get_sayLargeValue_36();
		NullCheck(L_62);
		L_62->set_sayLarge_2(L_63);
		SkinnedMeshRenderer_t126F4D6010E0F4B2685A7817B0A9171805D8F496 * L_64 = __this->get_skinnedMeshRenderer_5();
		IL2CPP_RUNTIME_CLASS_INIT(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		bool L_65;
		L_65 = Object_op_Implicit_mC8214E4F028CC2F036CC82BDB81D102A02893499(L_64, /*hidden argument*/NULL);
		if (!L_65)
		{
			goto IL_02aa;
		}
	}
	{
		bool L_66 = __this->get_lockShapes_39();
		if (!L_66)
		{
			goto IL_02aa;
		}
	}
	{
		SkinnedMeshRenderer_t126F4D6010E0F4B2685A7817B0A9171805D8F496 * L_67 = __this->get_skinnedMeshRenderer_5();
		int32_t L_68 = __this->get_saySmallIndex_11();
		SalsaBlendAmounts_t8FBE1BDDD1306BB8232FCAAD43FC87042840D626 * L_69 = __this->get_sayAmount_30();
		NullCheck(L_69);
		float L_70 = L_69->get_saySmall_0();
		NullCheck(L_67);
		SkinnedMeshRenderer_SetBlendShapeWeight_mF546F3567C5039C217AD1E32157B992B4124B5FD(L_67, L_68, L_70, /*hidden argument*/NULL);
		SkinnedMeshRenderer_t126F4D6010E0F4B2685A7817B0A9171805D8F496 * L_71 = __this->get_skinnedMeshRenderer_5();
		int32_t L_72 = __this->get_sayMediumIndex_12();
		SalsaBlendAmounts_t8FBE1BDDD1306BB8232FCAAD43FC87042840D626 * L_73 = __this->get_sayAmount_30();
		NullCheck(L_73);
		float L_74 = L_73->get_sayMedium_1();
		NullCheck(L_71);
		SkinnedMeshRenderer_SetBlendShapeWeight_mF546F3567C5039C217AD1E32157B992B4124B5FD(L_71, L_72, L_74, /*hidden argument*/NULL);
		SkinnedMeshRenderer_t126F4D6010E0F4B2685A7817B0A9171805D8F496 * L_75 = __this->get_skinnedMeshRenderer_5();
		int32_t L_76 = __this->get_sayLargeIndex_13();
		SalsaBlendAmounts_t8FBE1BDDD1306BB8232FCAAD43FC87042840D626 * L_77 = __this->get_sayAmount_30();
		NullCheck(L_77);
		float L_78 = L_77->get_sayLarge_2();
		NullCheck(L_75);
		SkinnedMeshRenderer_SetBlendShapeWeight_mF546F3567C5039C217AD1E32157B992B4124B5FD(L_75, L_76, L_78, /*hidden argument*/NULL);
	}

IL_02aa:
	{
		AudioSource_tC4BF65AF8CDCAA63724BB3CA59A7A29249269E6B * L_79 = __this->get_audioSrc_8();
		IL2CPP_RUNTIME_CLASS_INIT(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		bool L_80;
		L_80 = Object_op_Implicit_mC8214E4F028CC2F036CC82BDB81D102A02893499(L_79, /*hidden argument*/NULL);
		if (!L_80)
		{
			goto IL_02d4;
		}
	}
	{
		AudioSource_tC4BF65AF8CDCAA63724BB3CA59A7A29249269E6B * L_81 = __this->get_audioSrc_8();
		NullCheck(L_81);
		bool L_82;
		L_82 = AudioSource_get_isPlaying_mEA69477C77D542971F7B454946EF25DFBE0AF6A8(L_81, /*hidden argument*/NULL);
		if (!L_82)
		{
			goto IL_02cd;
		}
	}
	{
		__this->set_isTalking_20((bool)1);
		goto IL_02d4;
	}

IL_02cd:
	{
		__this->set_isTalking_20((bool)0);
	}

IL_02d4:
	{
		bool L_83 = __this->get_isTalking_20();
		if (!L_83)
		{
			goto IL_02e5;
		}
	}
	{
		__this->set_lockShapes_39((bool)1);
		goto IL_0329;
	}

IL_02e5:
	{
		__this->set_lockShapes_39((bool)0);
		SalsaBlendAmounts_t8FBE1BDDD1306BB8232FCAAD43FC87042840D626 * L_84 = __this->get_sayAmount_30();
		NullCheck(L_84);
		float L_85 = L_84->get_saySmall_0();
		if ((((float)L_85) > ((float)(0.0f))))
		{
			goto IL_0322;
		}
	}
	{
		SalsaBlendAmounts_t8FBE1BDDD1306BB8232FCAAD43FC87042840D626 * L_86 = __this->get_sayAmount_30();
		NullCheck(L_86);
		float L_87 = L_86->get_sayMedium_1();
		if ((((float)L_87) > ((float)(0.0f))))
		{
			goto IL_0322;
		}
	}
	{
		SalsaBlendAmounts_t8FBE1BDDD1306BB8232FCAAD43FC87042840D626 * L_88 = __this->get_sayAmount_30();
		NullCheck(L_88);
		float L_89 = L_88->get_sayLarge_2();
		if ((!(((float)L_89) > ((float)(0.0f)))))
		{
			goto IL_0329;
		}
	}

IL_0322:
	{
		__this->set_lockShapes_39((bool)1);
	}

IL_0329:
	{
		float L_90 = __this->get_prevAudioUpdateDelay_32();
		float L_91 = __this->get_audioUpdateDelay_17();
		if ((((float)L_90) == ((float)L_91)))
		{
			goto IL_0354;
		}
	}
	{
		float L_92 = __this->get_audioUpdateDelay_17();
		WaitForSeconds_t8F9189BE6E467C98C99177038881F8982E0E4013 * L_93 = (WaitForSeconds_t8F9189BE6E467C98C99177038881F8982E0E4013 *)il2cpp_codegen_object_new(WaitForSeconds_t8F9189BE6E467C98C99177038881F8982E0E4013_il2cpp_TypeInfo_var);
		WaitForSeconds__ctor_mD298C4CB9532BBBDE172FC40F3397E30504038D4(L_93, L_92, /*hidden argument*/NULL);
		__this->set_updateSampleDelay_31(L_93);
		float L_94 = __this->get_audioUpdateDelay_17();
		__this->set_prevAudioUpdateDelay_32(L_94);
	}

IL_0354:
	{
		return;
	}
}
// System.Void CrazyMinnow.SALSA.Salsa3D::TalkStatusChanged()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Salsa3D_TalkStatusChanged_m4C6E88F7F47206891F1C4227E68A3896C39254B5 (Salsa3D_tACF83D529139263662FEAAC8F5B53EC43B140C9C * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Salsa3D_tACF83D529139263662FEAAC8F5B53EC43B140C9C_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&SalsaStatus_t67641B5EA3D03836981ED128DA718792E18105C1_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral861234844093461379B1BEC77B56549309505C65);
		s_Il2CppMethodInitialized = true;
	}
	SalsaStatus_t67641B5EA3D03836981ED128DA718792E18105C1 * V_0 = NULL;
	int32_t V_1 = 0;
	int32_t V_2 = 0;
	{
		SalsaStatus_t67641B5EA3D03836981ED128DA718792E18105C1 * L_0 = (SalsaStatus_t67641B5EA3D03836981ED128DA718792E18105C1 *)il2cpp_codegen_object_new(SalsaStatus_t67641B5EA3D03836981ED128DA718792E18105C1_il2cpp_TypeInfo_var);
		SalsaStatus__ctor_m60EB1381FDD2EA05C23A1AD9A6005AF854E5E89D(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		SalsaStatus_t67641B5EA3D03836981ED128DA718792E18105C1 * L_1 = V_0;
		Salsa3D_tACF83D529139263662FEAAC8F5B53EC43B140C9C * L_2 = ((Salsa3D_tACF83D529139263662FEAAC8F5B53EC43B140C9C_StaticFields*)il2cpp_codegen_static_fields_for(Salsa3D_tACF83D529139263662FEAAC8F5B53EC43B140C9C_il2cpp_TypeInfo_var))->get_instance_4();
		NullCheck(L_1);
		SalsaStatus_set_instance_mB67A73851E6FDD5B713B5F85017446B672B32F63_inline(L_1, L_2, /*hidden argument*/NULL);
		SalsaStatus_t67641B5EA3D03836981ED128DA718792E18105C1 * L_3 = V_0;
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_4;
		L_4 = Component_get_gameObject_m55DC35B149AFB9157582755383BA954655FE0C5B(__this, /*hidden argument*/NULL);
		NullCheck(L_4);
		String_t* L_5;
		L_5 = Object_get_name_m0C7BC870ED2F0DC5A2FB09628136CD7D1CB82CFB(L_4, /*hidden argument*/NULL);
		NullCheck(L_3);
		SalsaStatus_set_talkerName_m88E92FFF1840D7288C4474685B82F562E5A6C5EE_inline(L_3, L_5, /*hidden argument*/NULL);
		SalsaStatus_t67641B5EA3D03836981ED128DA718792E18105C1 * L_6 = V_0;
		bool L_7 = __this->get_isTalking_20();
		NullCheck(L_6);
		SalsaStatus_set_isTalking_m621FF64AC69982D2A47B901157DB29C7F3EC6725_inline(L_6, L_7, /*hidden argument*/NULL);
		SalsaStatus_t67641B5EA3D03836981ED128DA718792E18105C1 * L_8 = V_0;
		AudioSource_tC4BF65AF8CDCAA63724BB3CA59A7A29249269E6B * L_9 = __this->get_audioSrc_8();
		NullCheck(L_9);
		AudioClip_t16D2E573E7CC1C5118D8EE0F0692D46866A1C0EE * L_10;
		L_10 = AudioSource_get_clip_mE4454E38D2C0A4C8CC780A435FC1DBD4D47D16DC(L_9, /*hidden argument*/NULL);
		NullCheck(L_10);
		String_t* L_11;
		L_11 = Object_get_name_m0C7BC870ED2F0DC5A2FB09628136CD7D1CB82CFB(L_10, /*hidden argument*/NULL);
		NullCheck(L_8);
		SalsaStatus_set_clipName_m564A868F0584865EBA31D5F8DD38FE39B698F72A_inline(L_8, L_11, /*hidden argument*/NULL);
		GameObjectU5BU5D_tA88FC1A1FC9D4D73D0B3984D4B0ECE88F4C47642* L_12 = __this->get_broadcastReceivers_25();
		NullCheck(L_12);
		if ((((int32_t)((int32_t)((int32_t)(((RuntimeArray*)L_12)->max_length)))) <= ((int32_t)0)))
		{
			goto IL_00c4;
		}
	}
	{
		GameObjectU5BU5D_tA88FC1A1FC9D4D73D0B3984D4B0ECE88F4C47642* L_13 = __this->get_broadcastReceivers_25();
		NullCheck(L_13);
		int32_t L_14 = __this->get_broadcastReceiversCount_23();
		if ((((int32_t)((int32_t)((int32_t)(((RuntimeArray*)L_13)->max_length)))) <= ((int32_t)L_14)))
		{
			goto IL_006d;
		}
	}
	{
		GameObjectU5BU5D_tA88FC1A1FC9D4D73D0B3984D4B0ECE88F4C47642* L_15 = __this->get_broadcastReceivers_25();
		NullCheck(L_15);
		__this->set_broadcastReceiversCount_23(((int32_t)((int32_t)(((RuntimeArray*)L_15)->max_length))));
	}

IL_006d:
	{
		bool L_16 = __this->get_propagateToChildren_26();
		if (!L_16)
		{
			goto IL_009d;
		}
	}
	{
		V_1 = 0;
		goto IL_0091;
	}

IL_0079:
	{
		GameObjectU5BU5D_tA88FC1A1FC9D4D73D0B3984D4B0ECE88F4C47642* L_17 = __this->get_broadcastReceivers_25();
		int32_t L_18 = V_1;
		NullCheck(L_17);
		int32_t L_19 = L_18;
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_20 = (L_17)->GetAt(static_cast<il2cpp_array_size_t>(L_19));
		SalsaStatus_t67641B5EA3D03836981ED128DA718792E18105C1 * L_21 = V_0;
		NullCheck(L_20);
		GameObject_BroadcastMessage_m2B5D6163ABB0ED80A381A41DC84ED48CC10212AD(L_20, _stringLiteral861234844093461379B1BEC77B56549309505C65, L_21, 1, /*hidden argument*/NULL);
		int32_t L_22 = V_1;
		V_1 = ((int32_t)il2cpp_codegen_add((int32_t)L_22, (int32_t)1));
	}

IL_0091:
	{
		int32_t L_23 = V_1;
		GameObjectU5BU5D_tA88FC1A1FC9D4D73D0B3984D4B0ECE88F4C47642* L_24 = __this->get_broadcastReceivers_25();
		NullCheck(L_24);
		if ((((int32_t)L_23) < ((int32_t)((int32_t)((int32_t)(((RuntimeArray*)L_24)->max_length))))))
		{
			goto IL_0079;
		}
	}
	{
		return;
	}

IL_009d:
	{
		V_2 = 0;
		goto IL_00b9;
	}

IL_00a1:
	{
		GameObjectU5BU5D_tA88FC1A1FC9D4D73D0B3984D4B0ECE88F4C47642* L_25 = __this->get_broadcastReceivers_25();
		int32_t L_26 = V_2;
		NullCheck(L_25);
		int32_t L_27 = L_26;
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_28 = (L_25)->GetAt(static_cast<il2cpp_array_size_t>(L_27));
		SalsaStatus_t67641B5EA3D03836981ED128DA718792E18105C1 * L_29 = V_0;
		NullCheck(L_28);
		GameObject_SendMessage_mD49CCADA51268480B585733DD7C6540CCCC6EF5C(L_28, _stringLiteral861234844093461379B1BEC77B56549309505C65, L_29, 1, /*hidden argument*/NULL);
		int32_t L_30 = V_2;
		V_2 = ((int32_t)il2cpp_codegen_add((int32_t)L_30, (int32_t)1));
	}

IL_00b9:
	{
		int32_t L_31 = V_2;
		GameObjectU5BU5D_tA88FC1A1FC9D4D73D0B3984D4B0ECE88F4C47642* L_32 = __this->get_broadcastReceivers_25();
		NullCheck(L_32);
		if ((((int32_t)L_31) < ((int32_t)((int32_t)((int32_t)(((RuntimeArray*)L_32)->max_length))))))
		{
			goto IL_00a1;
		}
	}

IL_00c4:
	{
		return;
	}
}
// System.Collections.IEnumerator CrazyMinnow.SALSA.Salsa3D::UpdateSample()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* Salsa3D_UpdateSample_mB63EC23E06A628ADCF06EEFCE0735FF9D2E0D727 (Salsa3D_tACF83D529139263662FEAAC8F5B53EC43B140C9C * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CUpdateSampleU3Ed__0_tB4DEFA6499D020E6EC2C7109DBED4C19296282D3_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	U3CUpdateSampleU3Ed__0_tB4DEFA6499D020E6EC2C7109DBED4C19296282D3 * V_0 = NULL;
	{
		U3CUpdateSampleU3Ed__0_tB4DEFA6499D020E6EC2C7109DBED4C19296282D3 * L_0 = (U3CUpdateSampleU3Ed__0_tB4DEFA6499D020E6EC2C7109DBED4C19296282D3 *)il2cpp_codegen_object_new(U3CUpdateSampleU3Ed__0_tB4DEFA6499D020E6EC2C7109DBED4C19296282D3_il2cpp_TypeInfo_var);
		U3CUpdateSampleU3Ed__0__ctor_m199B19B5576CA91594C9D8529188676BA2B73670(L_0, 0, /*hidden argument*/NULL);
		V_0 = L_0;
		U3CUpdateSampleU3Ed__0_tB4DEFA6499D020E6EC2C7109DBED4C19296282D3 * L_1 = V_0;
		NullCheck(L_1);
		L_1->set_U3CU3E4__this_2(__this);
		U3CUpdateSampleU3Ed__0_tB4DEFA6499D020E6EC2C7109DBED4C19296282D3 * L_2 = V_0;
		return L_2;
	}
}
// System.Void CrazyMinnow.SALSA.Salsa3D::GetBlendShapes()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Salsa3D_GetBlendShapes_mAD39B313314A8243FE2172912EC2DD385D1EA751 (Salsa3D_tACF83D529139263662FEAAC8F5B53EC43B140C9C * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	{
		SkinnedMeshRenderer_t126F4D6010E0F4B2685A7817B0A9171805D8F496 * L_0 = __this->get_skinnedMeshRenderer_5();
		IL2CPP_RUNTIME_CLASS_INIT(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		bool L_1;
		L_1 = Object_op_Implicit_mC8214E4F028CC2F036CC82BDB81D102A02893499(L_0, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0054;
		}
	}
	{
		SkinnedMeshRenderer_t126F4D6010E0F4B2685A7817B0A9171805D8F496 * L_2 = __this->get_skinnedMeshRenderer_5();
		NullCheck(L_2);
		Mesh_t2F5992DBA650D5862B43D3823ACD997132A57DA6 * L_3;
		L_3 = SkinnedMeshRenderer_get_sharedMesh_mFD55E307943C1C4B2E2E8632F15B41CCBD8D91F2(L_2, /*hidden argument*/NULL);
		NullCheck(L_3);
		int32_t L_4;
		L_4 = Mesh_get_blendShapeCount_mC80CCEA555E9E5609E3497EECF2B03F9B822CB77(L_3, /*hidden argument*/NULL);
		StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* L_5 = (StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A*)(StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A*)SZArrayNew(StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A_il2cpp_TypeInfo_var, (uint32_t)L_4);
		__this->set_blendShapes_10(L_5);
		V_0 = 0;
		goto IL_0049;
	}

IL_002c:
	{
		StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* L_6 = __this->get_blendShapes_10();
		int32_t L_7 = V_0;
		SkinnedMeshRenderer_t126F4D6010E0F4B2685A7817B0A9171805D8F496 * L_8 = __this->get_skinnedMeshRenderer_5();
		NullCheck(L_8);
		Mesh_t2F5992DBA650D5862B43D3823ACD997132A57DA6 * L_9;
		L_9 = SkinnedMeshRenderer_get_sharedMesh_mFD55E307943C1C4B2E2E8632F15B41CCBD8D91F2(L_8, /*hidden argument*/NULL);
		int32_t L_10 = V_0;
		NullCheck(L_9);
		String_t* L_11;
		L_11 = Mesh_GetBlendShapeName_mF28E636D7F66608E6FD9152ACD9547B78C895000(L_9, L_10, /*hidden argument*/NULL);
		NullCheck(L_6);
		ArrayElementTypeCheck (L_6, L_11);
		(L_6)->SetAt(static_cast<il2cpp_array_size_t>(L_7), (String_t*)L_11);
		int32_t L_12 = V_0;
		V_0 = ((int32_t)il2cpp_codegen_add((int32_t)L_12, (int32_t)1));
	}

IL_0049:
	{
		int32_t L_13 = V_0;
		StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* L_14 = __this->get_blendShapes_10();
		NullCheck(L_14);
		if ((((int32_t)L_13) < ((int32_t)((int32_t)((int32_t)(((RuntimeArray*)L_14)->max_length))))))
		{
			goto IL_002c;
		}
	}

IL_0054:
	{
		return;
	}
}
// System.Void CrazyMinnow.SALSA.Salsa3D::AutoLinkMouth()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Salsa3D_AutoLinkMouth_mC22B8B11A8FD3FB251B7D3550C76312CB7CC3E32 (Salsa3D_tACF83D529139263662FEAAC8F5B53EC43B140C9C * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral5153EC33000758EF3918055BED13B9869A6DB7B2);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral590F6531E6E208C59FB3221DF4DBCAC088455F77);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralC19BE6CBB5FEA10EDE721E3CF59D93ACFA10E599);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	{
		SkinnedMeshRenderer_t126F4D6010E0F4B2685A7817B0A9171805D8F496 * L_0 = __this->get_skinnedMeshRenderer_5();
		IL2CPP_RUNTIME_CLASS_INIT(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		bool L_1;
		L_1 = Object_op_Implicit_mC8214E4F028CC2F036CC82BDB81D102A02893499(L_0, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_00bc;
		}
	}
	{
		StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* L_2 = __this->get_blendShapes_10();
		if (L_2)
		{
			goto IL_001e;
		}
	}
	{
		Salsa3D_GetBlendShapes_mAD39B313314A8243FE2172912EC2DD385D1EA751(__this, /*hidden argument*/NULL);
	}

IL_001e:
	{
		V_0 = 0;
		goto IL_00ae;
	}

IL_0025:
	{
		StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* L_3 = __this->get_blendShapes_10();
		int32_t L_4 = V_0;
		NullCheck(L_3);
		int32_t L_5 = L_4;
		String_t* L_6 = (L_3)->GetAt(static_cast<il2cpp_array_size_t>(L_5));
		NullCheck(L_6);
		int32_t L_7;
		L_7 = String_get_Length_m129FC0ADA02FECBED3C0B1A809AE84A5AEE1CF09_inline(L_6, /*hidden argument*/NULL);
		if ((((int32_t)L_7) < ((int32_t)4)))
		{
			goto IL_00aa;
		}
	}
	{
		StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* L_8 = __this->get_blendShapes_10();
		int32_t L_9 = V_0;
		NullCheck(L_8);
		int32_t L_10 = L_9;
		String_t* L_11 = (L_8)->GetAt(static_cast<il2cpp_array_size_t>(L_10));
		NullCheck(L_11);
		String_t* L_12;
		L_12 = String_ToLower_m7875A49FE166D0A68F3F6B6E70C0C056EBEFD31D(L_11, /*hidden argument*/NULL);
		NullCheck(L_12);
		String_t* L_13;
		L_13 = String_Substring_m7A39A2AC0893AE940CF4CEC841326D56FFB9D86B(L_12, 0, 4, /*hidden argument*/NULL);
		bool L_14;
		L_14 = String_op_Equality_m2B91EE68355F142F67095973D32EB5828B7B73CB(L_13, _stringLiteral5153EC33000758EF3918055BED13B9869A6DB7B2, /*hidden argument*/NULL);
		if (!L_14)
		{
			goto IL_005c;
		}
	}
	{
		int32_t L_15 = V_0;
		__this->set_saySmallIndex_11(L_15);
	}

IL_005c:
	{
		StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* L_16 = __this->get_blendShapes_10();
		int32_t L_17 = V_0;
		NullCheck(L_16);
		int32_t L_18 = L_17;
		String_t* L_19 = (L_16)->GetAt(static_cast<il2cpp_array_size_t>(L_18));
		NullCheck(L_19);
		String_t* L_20;
		L_20 = String_ToLower_m7875A49FE166D0A68F3F6B6E70C0C056EBEFD31D(L_19, /*hidden argument*/NULL);
		NullCheck(L_20);
		String_t* L_21;
		L_21 = String_Substring_m7A39A2AC0893AE940CF4CEC841326D56FFB9D86B(L_20, 0, 4, /*hidden argument*/NULL);
		bool L_22;
		L_22 = String_op_Equality_m2B91EE68355F142F67095973D32EB5828B7B73CB(L_21, _stringLiteral590F6531E6E208C59FB3221DF4DBCAC088455F77, /*hidden argument*/NULL);
		if (!L_22)
		{
			goto IL_0083;
		}
	}
	{
		int32_t L_23 = V_0;
		__this->set_sayMediumIndex_12(L_23);
	}

IL_0083:
	{
		StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* L_24 = __this->get_blendShapes_10();
		int32_t L_25 = V_0;
		NullCheck(L_24);
		int32_t L_26 = L_25;
		String_t* L_27 = (L_24)->GetAt(static_cast<il2cpp_array_size_t>(L_26));
		NullCheck(L_27);
		String_t* L_28;
		L_28 = String_ToLower_m7875A49FE166D0A68F3F6B6E70C0C056EBEFD31D(L_27, /*hidden argument*/NULL);
		NullCheck(L_28);
		String_t* L_29;
		L_29 = String_Substring_m7A39A2AC0893AE940CF4CEC841326D56FFB9D86B(L_28, 0, 4, /*hidden argument*/NULL);
		bool L_30;
		L_30 = String_op_Equality_m2B91EE68355F142F67095973D32EB5828B7B73CB(L_29, _stringLiteralC19BE6CBB5FEA10EDE721E3CF59D93ACFA10E599, /*hidden argument*/NULL);
		if (!L_30)
		{
			goto IL_00aa;
		}
	}
	{
		int32_t L_31 = V_0;
		__this->set_sayLargeIndex_13(L_31);
	}

IL_00aa:
	{
		int32_t L_32 = V_0;
		V_0 = ((int32_t)il2cpp_codegen_add((int32_t)L_32, (int32_t)1));
	}

IL_00ae:
	{
		int32_t L_33 = V_0;
		StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* L_34 = __this->get_blendShapes_10();
		NullCheck(L_34);
		if ((((int32_t)L_33) < ((int32_t)((int32_t)((int32_t)(((RuntimeArray*)L_34)->max_length))))))
		{
			goto IL_0025;
		}
	}

IL_00bc:
	{
		return;
	}
}
// System.Void CrazyMinnow.SALSA.Salsa3D::Play()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Salsa3D_Play_mEA71E242A27A0C1921CAAA55522985950120F751 (Salsa3D_tACF83D529139263662FEAAC8F5B53EC43B140C9C * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		AudioSource_tC4BF65AF8CDCAA63724BB3CA59A7A29249269E6B * L_0 = __this->get_audioSrc_8();
		IL2CPP_RUNTIME_CLASS_INIT(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		bool L_1;
		L_1 = Object_op_Implicit_mC8214E4F028CC2F036CC82BDB81D102A02893499(L_0, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0018;
		}
	}
	{
		AudioSource_tC4BF65AF8CDCAA63724BB3CA59A7A29249269E6B * L_2 = __this->get_audioSrc_8();
		NullCheck(L_2);
		AudioSource_Play_mED16664B8F8F3E4D68785C8C00FC96C4DF053AE1(L_2, /*hidden argument*/NULL);
	}

IL_0018:
	{
		return;
	}
}
// System.Void CrazyMinnow.SALSA.Salsa3D::Pause()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Salsa3D_Pause_m4507436881B8E2F41E18AEB1B8A5C4E8A35DB804 (Salsa3D_tACF83D529139263662FEAAC8F5B53EC43B140C9C * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		AudioSource_tC4BF65AF8CDCAA63724BB3CA59A7A29249269E6B * L_0 = __this->get_audioSrc_8();
		IL2CPP_RUNTIME_CLASS_INIT(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		bool L_1;
		L_1 = Object_op_Implicit_mC8214E4F028CC2F036CC82BDB81D102A02893499(L_0, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0018;
		}
	}
	{
		AudioSource_tC4BF65AF8CDCAA63724BB3CA59A7A29249269E6B * L_2 = __this->get_audioSrc_8();
		NullCheck(L_2);
		AudioSource_Pause_mC4F9932A77B6AA2CC3FB720721B7837CF57B675D(L_2, /*hidden argument*/NULL);
	}

IL_0018:
	{
		return;
	}
}
// System.Void CrazyMinnow.SALSA.Salsa3D::Stop()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Salsa3D_Stop_m4362CFD77DCD332D5CFB4224D4E6ACF6B2FB2967 (Salsa3D_tACF83D529139263662FEAAC8F5B53EC43B140C9C * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		AudioSource_tC4BF65AF8CDCAA63724BB3CA59A7A29249269E6B * L_0 = __this->get_audioSrc_8();
		IL2CPP_RUNTIME_CLASS_INIT(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		bool L_1;
		L_1 = Object_op_Implicit_mC8214E4F028CC2F036CC82BDB81D102A02893499(L_0, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0018;
		}
	}
	{
		AudioSource_tC4BF65AF8CDCAA63724BB3CA59A7A29249269E6B * L_2 = __this->get_audioSrc_8();
		NullCheck(L_2);
		AudioSource_Stop_mADA564D223832A64F8CF3EFBDEB534C0D658810F(L_2, /*hidden argument*/NULL);
	}

IL_0018:
	{
		return;
	}
}
// System.Void CrazyMinnow.SALSA.Salsa3D::SetAudioClip(UnityEngine.AudioClip)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Salsa3D_SetAudioClip_m7E1EAB203B768C23F2B00544C93C92B989703665 (Salsa3D_tACF83D529139263662FEAAC8F5B53EC43B140C9C * __this, AudioClip_t16D2E573E7CC1C5118D8EE0F0692D46866A1C0EE * ___audioClip0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		AudioClip_t16D2E573E7CC1C5118D8EE0F0692D46866A1C0EE * L_0 = ___audioClip0;
		__this->set_audioClip_9(L_0);
		AudioSource_tC4BF65AF8CDCAA63724BB3CA59A7A29249269E6B * L_1 = __this->get_audioSrc_8();
		IL2CPP_RUNTIME_CLASS_INIT(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		bool L_2;
		L_2 = Object_op_Implicit_mC8214E4F028CC2F036CC82BDB81D102A02893499(L_1, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_0025;
		}
	}
	{
		AudioSource_tC4BF65AF8CDCAA63724BB3CA59A7A29249269E6B * L_3 = __this->get_audioSrc_8();
		AudioClip_t16D2E573E7CC1C5118D8EE0F0692D46866A1C0EE * L_4 = __this->get_audioClip_9();
		NullCheck(L_3);
		AudioSource_set_clip_mD1F50F7BA6EA3AF25B4922473352C5180CFF7B2B(L_3, L_4, /*hidden argument*/NULL);
	}

IL_0025:
	{
		return;
	}
}
// System.Void CrazyMinnow.SALSA.Salsa3D::LoadAudioClip(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Salsa3D_LoadAudioClip_m6D5FDC03511EDFE828A82FE84E85E888F0FF9398 (Salsa3D_tACF83D529139263662FEAAC8F5B53EC43B140C9C * __this, String_t* ___resourceClipName0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&AudioClip_t16D2E573E7CC1C5118D8EE0F0692D46866A1C0EE_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	Object_tF2F3778131EFF286AF62B7B013A170F95A91571A * V_0 = NULL;
	AudioClip_t16D2E573E7CC1C5118D8EE0F0692D46866A1C0EE * V_1 = NULL;
	{
		String_t* L_0 = ___resourceClipName0;
		NullCheck(L_0);
		int32_t L_1;
		L_1 = String_get_Length_m129FC0ADA02FECBED3C0B1A809AE84A5AEE1CF09_inline(L_0, /*hidden argument*/NULL);
		if ((((int32_t)L_1) <= ((int32_t)0)))
		{
			goto IL_0054;
		}
	}
	{
		String_t* L_2 = ___resourceClipName0;
		Object_tF2F3778131EFF286AF62B7B013A170F95A91571A * L_3;
		L_3 = Resources_Load_m011631B3740AFD38D496838F10D3DA635A061120(L_2, /*hidden argument*/NULL);
		V_0 = ((AudioClip_t16D2E573E7CC1C5118D8EE0F0692D46866A1C0EE *)IsInstSealed((RuntimeObject*)L_3, AudioClip_t16D2E573E7CC1C5118D8EE0F0692D46866A1C0EE_il2cpp_TypeInfo_var));
		Object_tF2F3778131EFF286AF62B7B013A170F95A91571A * L_4 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		bool L_5;
		L_5 = Object_op_Inequality_mE1F187520BD83FB7D86A6D850710C4D42B864E90(L_4, (Object_tF2F3778131EFF286AF62B7B013A170F95A91571A *)NULL, /*hidden argument*/NULL);
		if (!L_5)
		{
			goto IL_0074;
		}
	}
	{
		Object_tF2F3778131EFF286AF62B7B013A170F95A91571A * L_6 = V_0;
		V_1 = ((AudioClip_t16D2E573E7CC1C5118D8EE0F0692D46866A1C0EE *)CastclassSealed((RuntimeObject*)L_6, AudioClip_t16D2E573E7CC1C5118D8EE0F0692D46866A1C0EE_il2cpp_TypeInfo_var));
		AudioClip_t16D2E573E7CC1C5118D8EE0F0692D46866A1C0EE * L_7 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		bool L_8;
		L_8 = Object_op_Inequality_mE1F187520BD83FB7D86A6D850710C4D42B864E90(L_7, (Object_tF2F3778131EFF286AF62B7B013A170F95A91571A *)NULL, /*hidden argument*/NULL);
		if (!L_8)
		{
			goto IL_0035;
		}
	}
	{
		AudioClip_t16D2E573E7CC1C5118D8EE0F0692D46866A1C0EE * L_9 = V_1;
		__this->set_audioClip_9(L_9);
	}

IL_0035:
	{
		AudioSource_tC4BF65AF8CDCAA63724BB3CA59A7A29249269E6B * L_10 = __this->get_audioSrc_8();
		IL2CPP_RUNTIME_CLASS_INIT(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		bool L_11;
		L_11 = Object_op_Implicit_mC8214E4F028CC2F036CC82BDB81D102A02893499(L_10, /*hidden argument*/NULL);
		if (!L_11)
		{
			goto IL_0074;
		}
	}
	{
		AudioSource_tC4BF65AF8CDCAA63724BB3CA59A7A29249269E6B * L_12 = __this->get_audioSrc_8();
		AudioClip_t16D2E573E7CC1C5118D8EE0F0692D46866A1C0EE * L_13 = __this->get_audioClip_9();
		NullCheck(L_12);
		AudioSource_set_clip_mD1F50F7BA6EA3AF25B4922473352C5180CFF7B2B(L_12, L_13, /*hidden argument*/NULL);
		return;
	}

IL_0054:
	{
		__this->set_audioClip_9((AudioClip_t16D2E573E7CC1C5118D8EE0F0692D46866A1C0EE *)NULL);
		AudioSource_tC4BF65AF8CDCAA63724BB3CA59A7A29249269E6B * L_14 = __this->get_audioSrc_8();
		IL2CPP_RUNTIME_CLASS_INIT(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		bool L_15;
		L_15 = Object_op_Implicit_mC8214E4F028CC2F036CC82BDB81D102A02893499(L_14, /*hidden argument*/NULL);
		if (!L_15)
		{
			goto IL_0074;
		}
	}
	{
		AudioSource_tC4BF65AF8CDCAA63724BB3CA59A7A29249269E6B * L_16 = __this->get_audioSrc_8();
		NullCheck(L_16);
		AudioSource_set_clip_mD1F50F7BA6EA3AF25B4922473352C5180CFF7B2B(L_16, (AudioClip_t16D2E573E7CC1C5118D8EE0F0692D46866A1C0EE *)NULL, /*hidden argument*/NULL);
	}

IL_0074:
	{
		return;
	}
}
// UnityEngine.AudioSource CrazyMinnow.SALSA.Salsa3D::FindAudioSource()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR AudioSource_tC4BF65AF8CDCAA63724BB3CA59A7A29249269E6B * Salsa3D_FindAudioSource_mDE3A59BD7075854BE6C62C69780834597C8F2281 (Salsa3D_tACF83D529139263662FEAAC8F5B53EC43B140C9C * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Component_GetComponent_TisAudioSource_tC4BF65AF8CDCAA63724BB3CA59A7A29249269E6B_mCC3B7A679ECE504BFAF8C70C4EF527511F46902F_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&GameObject_AddComponent_TisAudioSource_tC4BF65AF8CDCAA63724BB3CA59A7A29249269E6B_m74F4A6C820807E361696D4E8F71DC1E54BBE7F76_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	AudioSource_tC4BF65AF8CDCAA63724BB3CA59A7A29249269E6B * V_0 = NULL;
	{
		AudioSource_tC4BF65AF8CDCAA63724BB3CA59A7A29249269E6B * L_0 = __this->get_audioSrc_8();
		V_0 = L_0;
		AudioSource_tC4BF65AF8CDCAA63724BB3CA59A7A29249269E6B * L_1 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		bool L_2;
		L_2 = Object_op_Implicit_mC8214E4F028CC2F036CC82BDB81D102A02893499(L_1, /*hidden argument*/NULL);
		if (L_2)
		{
			goto IL_002a;
		}
	}
	{
		AudioSource_tC4BF65AF8CDCAA63724BB3CA59A7A29249269E6B * L_3;
		L_3 = Component_GetComponent_TisAudioSource_tC4BF65AF8CDCAA63724BB3CA59A7A29249269E6B_mCC3B7A679ECE504BFAF8C70C4EF527511F46902F(__this, /*hidden argument*/Component_GetComponent_TisAudioSource_tC4BF65AF8CDCAA63724BB3CA59A7A29249269E6B_mCC3B7A679ECE504BFAF8C70C4EF527511F46902F_RuntimeMethod_var);
		V_0 = L_3;
		AudioSource_tC4BF65AF8CDCAA63724BB3CA59A7A29249269E6B * L_4 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		bool L_5;
		L_5 = Object_op_Implicit_mC8214E4F028CC2F036CC82BDB81D102A02893499(L_4, /*hidden argument*/NULL);
		if (L_5)
		{
			goto IL_002a;
		}
	}
	{
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_6;
		L_6 = Component_get_gameObject_m55DC35B149AFB9157582755383BA954655FE0C5B(__this, /*hidden argument*/NULL);
		NullCheck(L_6);
		AudioSource_tC4BF65AF8CDCAA63724BB3CA59A7A29249269E6B * L_7;
		L_7 = GameObject_AddComponent_TisAudioSource_tC4BF65AF8CDCAA63724BB3CA59A7A29249269E6B_m74F4A6C820807E361696D4E8F71DC1E54BBE7F76(L_6, /*hidden argument*/GameObject_AddComponent_TisAudioSource_tC4BF65AF8CDCAA63724BB3CA59A7A29249269E6B_m74F4A6C820807E361696D4E8F71DC1E54BBE7F76_RuntimeMethod_var);
		V_0 = L_7;
	}

IL_002a:
	{
		AudioSource_tC4BF65AF8CDCAA63724BB3CA59A7A29249269E6B * L_8 = V_0;
		return L_8;
	}
}
// System.Void CrazyMinnow.SALSA.Salsa3D::SetRangeOfMotion(System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Salsa3D_SetRangeOfMotion_m02D66529248011B5E0C665270F3D5F950748F64D (Salsa3D_tACF83D529139263662FEAAC8F5B53EC43B140C9C * __this, float ___amount0, const RuntimeMethod* method)
{
	{
		float L_0 = ___amount0;
		if ((((float)L_0) >= ((float)(0.0f))))
		{
			goto IL_0010;
		}
	}
	{
		float L_1 = ___amount0;
		if ((!(((float)L_1) <= ((float)(100.0f)))))
		{
			goto IL_0017;
		}
	}

IL_0010:
	{
		float L_2 = ___amount0;
		__this->set_rangeOfMotion_19(L_2);
	}

IL_0017:
	{
		return;
	}
}
// System.Void CrazyMinnow.SALSA.Salsa3D::SetBlendSpeed(System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Salsa3D_SetBlendSpeed_m579082426D279F7A6C139F3672C03779C29977B6 (Salsa3D_tACF83D529139263662FEAAC8F5B53EC43B140C9C * __this, float ___blendSpeed0, const RuntimeMethod* method)
{
	{
		float L_0 = ___blendSpeed0;
		if ((((float)L_0) >= ((float)(0.0f))))
		{
			goto IL_0010;
		}
	}
	{
		float L_1 = ___blendSpeed0;
		if ((!(((float)L_1) <= ((float)(100.0f)))))
		{
			goto IL_0017;
		}
	}

IL_0010:
	{
		float L_2 = ___blendSpeed0;
		__this->set_blendSpeed_18(L_2);
	}

IL_0017:
	{
		return;
	}
}
// System.Void CrazyMinnow.SALSA.Salsa3D::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Salsa3D__ctor_m2FC9AF05F5A114DECB757F015343373CBCF4358B (Salsa3D_tACF83D529139263662FEAAC8F5B53EC43B140C9C * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&SalsaBlendAmounts_t8FBE1BDDD1306BB8232FCAAD43FC87042840D626_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		__this->set_saySmallTrigger_14((0.000500000024f));
		__this->set_sayMediumTrigger_15((0.00300000003f));
		__this->set_sayLargeTrigger_16((0.00549999997f));
		__this->set_audioUpdateDelay_17((0.0799999982f));
		__this->set_blendSpeed_18((10.0f));
		__this->set_rangeOfMotion_19((100.0f));
		__this->set_broadcastReceiversCount_23(1);
		SalsaBlendAmounts_t8FBE1BDDD1306BB8232FCAAD43FC87042840D626 * L_0 = (SalsaBlendAmounts_t8FBE1BDDD1306BB8232FCAAD43FC87042840D626 *)il2cpp_codegen_object_new(SalsaBlendAmounts_t8FBE1BDDD1306BB8232FCAAD43FC87042840D626_il2cpp_TypeInfo_var);
		SalsaBlendAmounts__ctor_mA2E926D5E2C432845C8162FAEC2012E860EB94E2(L_0, /*hidden argument*/NULL);
		__this->set_sayAmount_30(L_0);
		__this->set_sampleSize_37(((int32_t)64));
		__this->set_writeAverage_38((bool)1);
		MonoBehaviour__ctor_mC0995D847F6A95B1A553652636C38A2AA8B13BED(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void CrazyMinnow.SALSA.SalsaBlendAmounts::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void SalsaBlendAmounts__ctor_mA2E926D5E2C432845C8162FAEC2012E860EB94E2 (SalsaBlendAmounts_t8FBE1BDDD1306BB8232FCAAD43FC87042840D626 * __this, const RuntimeMethod* method)
{
	{
		Object__ctor_m88880E0413421D13FD95325EDCE231707CE1F405(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// UnityEngine.Object CrazyMinnow.SALSA.SalsaStatus::get_instance()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Object_tF2F3778131EFF286AF62B7B013A170F95A91571A * SalsaStatus_get_instance_mB63DF2D8EF456D6A9E976AEB43B6C03A77BE4C29 (SalsaStatus_t67641B5EA3D03836981ED128DA718792E18105C1 * __this, const RuntimeMethod* method)
{
	{
		Object_tF2F3778131EFF286AF62B7B013A170F95A91571A * L_0 = __this->get_U3CinstanceU3Ek__BackingField_0();
		return L_0;
	}
}
// System.Void CrazyMinnow.SALSA.SalsaStatus::set_instance(UnityEngine.Object)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void SalsaStatus_set_instance_mB67A73851E6FDD5B713B5F85017446B672B32F63 (SalsaStatus_t67641B5EA3D03836981ED128DA718792E18105C1 * __this, Object_tF2F3778131EFF286AF62B7B013A170F95A91571A * ___value0, const RuntimeMethod* method)
{
	{
		Object_tF2F3778131EFF286AF62B7B013A170F95A91571A * L_0 = ___value0;
		__this->set_U3CinstanceU3Ek__BackingField_0(L_0);
		return;
	}
}
// System.String CrazyMinnow.SALSA.SalsaStatus::get_talkerName()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* SalsaStatus_get_talkerName_mAE5109304DDE680F4BB760BDC4A60BE9AA29EDD5 (SalsaStatus_t67641B5EA3D03836981ED128DA718792E18105C1 * __this, const RuntimeMethod* method)
{
	{
		String_t* L_0 = __this->get_U3CtalkerNameU3Ek__BackingField_1();
		return L_0;
	}
}
// System.Void CrazyMinnow.SALSA.SalsaStatus::set_talkerName(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void SalsaStatus_set_talkerName_m88E92FFF1840D7288C4474685B82F562E5A6C5EE (SalsaStatus_t67641B5EA3D03836981ED128DA718792E18105C1 * __this, String_t* ___value0, const RuntimeMethod* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set_U3CtalkerNameU3Ek__BackingField_1(L_0);
		return;
	}
}
// System.Boolean CrazyMinnow.SALSA.SalsaStatus::get_isTalking()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool SalsaStatus_get_isTalking_m5E400E0CDFCB8B3241BE2255144A88E0E0881234 (SalsaStatus_t67641B5EA3D03836981ED128DA718792E18105C1 * __this, const RuntimeMethod* method)
{
	{
		bool L_0 = __this->get_U3CisTalkingU3Ek__BackingField_2();
		return L_0;
	}
}
// System.Void CrazyMinnow.SALSA.SalsaStatus::set_isTalking(System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void SalsaStatus_set_isTalking_m621FF64AC69982D2A47B901157DB29C7F3EC6725 (SalsaStatus_t67641B5EA3D03836981ED128DA718792E18105C1 * __this, bool ___value0, const RuntimeMethod* method)
{
	{
		bool L_0 = ___value0;
		__this->set_U3CisTalkingU3Ek__BackingField_2(L_0);
		return;
	}
}
// System.String CrazyMinnow.SALSA.SalsaStatus::get_clipName()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* SalsaStatus_get_clipName_m1D43753B3C3BED26B88D82DBF7F14991D56E2B92 (SalsaStatus_t67641B5EA3D03836981ED128DA718792E18105C1 * __this, const RuntimeMethod* method)
{
	{
		String_t* L_0 = __this->get_U3CclipNameU3Ek__BackingField_3();
		return L_0;
	}
}
// System.Void CrazyMinnow.SALSA.SalsaStatus::set_clipName(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void SalsaStatus_set_clipName_m564A868F0584865EBA31D5F8DD38FE39B698F72A (SalsaStatus_t67641B5EA3D03836981ED128DA718792E18105C1 * __this, String_t* ___value0, const RuntimeMethod* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set_U3CclipNameU3Ek__BackingField_3(L_0);
		return;
	}
}
// System.Void CrazyMinnow.SALSA.SalsaStatus::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void SalsaStatus__ctor_m60EB1381FDD2EA05C23A1AD9A6005AF854E5E89D (SalsaStatus_t67641B5EA3D03836981ED128DA718792E18105C1 * __this, const RuntimeMethod* method)
{
	{
		Object__ctor_m88880E0413421D13FD95325EDCE231707CE1F405(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// UnityEngine.Texture2D CrazyMinnow.SALSA.SalsaUtility::LoadResource(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Texture2D_t9B604D0D8E28032123641A7E7338FA872E2698BF * SalsaUtility_LoadResource_m83BADDA1A9109E56D955FFA4143062817C97B1B2 (String_t* ___resourceName0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Texture2D_t9B604D0D8E28032123641A7E7338FA872E2698BF_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	Texture2D_t9B604D0D8E28032123641A7E7338FA872E2698BF * V_0 = NULL;
	{
		String_t* L_0 = ___resourceName0;
		Object_tF2F3778131EFF286AF62B7B013A170F95A91571A * L_1;
		L_1 = Resources_Load_m011631B3740AFD38D496838F10D3DA635A061120(L_0, /*hidden argument*/NULL);
		V_0 = ((Texture2D_t9B604D0D8E28032123641A7E7338FA872E2698BF *)CastclassSealed((RuntimeObject*)L_1, Texture2D_t9B604D0D8E28032123641A7E7338FA872E2698BF_il2cpp_TypeInfo_var));
		Texture2D_t9B604D0D8E28032123641A7E7338FA872E2698BF * L_2 = V_0;
		return L_2;
	}
}
// System.Single CrazyMinnow.SALSA.SalsaUtility::LerpRangeOfMotion(System.Single,System.Single,System.Single,CrazyMinnow.SALSA.SalsaUtility/BlendDirection)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float SalsaUtility_LerpRangeOfMotion_mDA97D8D31F6D62217220938CD785C48A75AEB246 (float ___currentVal0, float ___blendSpeed1, float ___rangeOfMotion2, int32_t ___blendDirection3, const RuntimeMethod* method)
{
	float V_0 = 0.0f;
	float V_1 = 0.0f;
	float V_2 = 0.0f;
	int32_t V_3 = 0;
	{
		float L_0 = ___currentVal0;
		V_0 = L_0;
		float L_1 = ___blendSpeed1;
		V_1 = L_1;
		float L_2 = ___rangeOfMotion2;
		V_2 = L_2;
		int32_t L_3 = ___blendDirection3;
		V_3 = L_3;
		float L_4 = V_0;
		float L_5;
		L_5 = SalsaUtility_ClampValue_m167E1BE774D495060E1FAA2503DD01D2C00EE208(L_4, (100.0f), /*hidden argument*/NULL);
		V_0 = L_5;
		float L_6 = V_0;
		float L_7 = V_2;
		if ((!(((float)L_6) > ((float)L_7))))
		{
			goto IL_001e;
		}
	}
	{
		float L_8 = V_0;
		float L_9 = V_1;
		V_0 = ((float)il2cpp_codegen_subtract((float)L_8, (float)L_9));
		goto IL_0033;
	}

IL_001e:
	{
		int32_t L_10 = V_3;
		if (L_10)
		{
			goto IL_0027;
		}
	}
	{
		float L_11 = V_0;
		float L_12 = V_1;
		V_0 = ((float)il2cpp_codegen_add((float)L_11, (float)L_12));
		goto IL_002b;
	}

IL_0027:
	{
		float L_13 = V_0;
		float L_14 = V_1;
		V_0 = ((float)il2cpp_codegen_subtract((float)L_13, (float)L_14));
	}

IL_002b:
	{
		float L_15 = V_0;
		float L_16 = V_2;
		float L_17;
		L_17 = SalsaUtility_ClampValue_m167E1BE774D495060E1FAA2503DD01D2C00EE208(L_15, L_16, /*hidden argument*/NULL);
		V_0 = L_17;
	}

IL_0033:
	{
		float L_18 = V_0;
		return L_18;
	}
}
// System.Single CrazyMinnow.SALSA.SalsaUtility::Lerp3DEyeTracking(System.Single,System.Single,System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float SalsaUtility_Lerp3DEyeTracking_m914ED29E6BF956DD834E8CF6B8C5B20591A22C19 (float ___currentVal0, float ___targetVal1, float ___blendSpeed2, const RuntimeMethod* method)
{
	float V_0 = 0.0f;
	float V_1 = 0.0f;
	float V_2 = 0.0f;
	{
		float L_0 = ___currentVal0;
		V_0 = L_0;
		float L_1 = ___blendSpeed2;
		V_1 = L_1;
		float L_2 = ___targetVal1;
		V_2 = L_2;
		float L_3 = V_0;
		float L_4;
		L_4 = SalsaUtility_ClampValue_m167E1BE774D495060E1FAA2503DD01D2C00EE208(L_3, (100.0f), /*hidden argument*/NULL);
		V_0 = L_4;
		float L_5 = ___targetVal1;
		float L_6 = V_0;
		if ((!(((float)L_5) > ((float)L_6))))
		{
			goto IL_001a;
		}
	}
	{
		float L_7 = V_0;
		float L_8 = V_1;
		V_0 = ((float)il2cpp_codegen_add((float)L_7, (float)L_8));
	}

IL_001a:
	{
		float L_9 = ___targetVal1;
		float L_10 = V_0;
		if ((!(((float)L_9) < ((float)L_10))))
		{
			goto IL_0022;
		}
	}
	{
		float L_11 = V_0;
		float L_12 = V_1;
		V_0 = ((float)il2cpp_codegen_subtract((float)L_11, (float)L_12));
	}

IL_0022:
	{
		float L_13 = V_0;
		float L_14 = V_2;
		float L_15;
		L_15 = SalsaUtility_ClampValue_m167E1BE774D495060E1FAA2503DD01D2C00EE208(L_13, L_14, /*hidden argument*/NULL);
		V_0 = L_15;
		float L_16 = V_0;
		return L_16;
	}
}
// UnityEngine.Vector3 CrazyMinnow.SALSA.SalsaUtility::Lerp2DEyeTracking(UnityEngine.Vector3,UnityEngine.Vector3,System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  SalsaUtility_Lerp2DEyeTracking_m49C8BAED6D6F4ABD41691661601291A0F20A800D (Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___currentPosition0, Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___newPosition1, float ___blendSpeed2, const RuntimeMethod* method)
{
	{
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_0 = ___currentPosition0;
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_1 = ___newPosition1;
		float L_2 = ___blendSpeed2;
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_3;
		L_3 = Vector3_Lerp_m8E095584FFA10CF1D3EABCD04F4C83FB82EC5524_inline(L_0, L_1, ((float)((float)L_2/(float)(20.0f))), /*hidden argument*/NULL);
		return L_3;
	}
}
// System.Single CrazyMinnow.SALSA.SalsaUtility::ClampValue(System.Single,System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float SalsaUtility_ClampValue_m167E1BE774D495060E1FAA2503DD01D2C00EE208 (float ___currentVal0, float ___rangeOfMotion1, const RuntimeMethod* method)
{
	float V_0 = 0.0f;
	float V_1 = 0.0f;
	{
		float L_0 = ___currentVal0;
		V_0 = L_0;
		float L_1 = ___rangeOfMotion1;
		V_1 = L_1;
		float L_2 = V_0;
		float L_3 = V_1;
		float L_4;
		L_4 = Mathf_Clamp_m2416F3B785C8F135863E3D17E5B0CB4174797B87(L_2, (0.0f), L_3, /*hidden argument*/NULL);
		V_0 = L_4;
		float L_5 = V_0;
		return L_5;
	}
}
// System.Single CrazyMinnow.SALSA.SalsaUtility::ClampValueMinMax(System.Single,System.Single,System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float SalsaUtility_ClampValueMinMax_mDC645C582746F5E4B0C6D90541D284BF6F9A73FB (float ___currentVal0, float ___min1, float ___max2, const RuntimeMethod* method)
{
	float V_0 = 0.0f;
	float V_1 = 0.0f;
	float V_2 = 0.0f;
	{
		float L_0 = ___currentVal0;
		V_0 = L_0;
		float L_1 = ___min1;
		V_1 = L_1;
		float L_2 = ___max2;
		V_2 = L_2;
		float L_3 = V_0;
		float L_4 = V_1;
		float L_5 = V_2;
		float L_6;
		L_6 = Mathf_Clamp_m2416F3B785C8F135863E3D17E5B0CB4174797B87(L_3, L_4, L_5, /*hidden argument*/NULL);
		V_0 = L_6;
		float L_7 = V_0;
		return L_7;
	}
}
// System.Single CrazyMinnow.SALSA.SalsaUtility::ScaleRange(System.Single,System.Single,System.Single,System.Single,System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float SalsaUtility_ScaleRange_m58E61AAAA506DE027672DBC1B89A1E874AE4E8A7 (float ___val0, float ___inMin1, float ___inMax2, float ___outMin3, float ___outMax4, const RuntimeMethod* method)
{
	float V_0 = 0.0f;
	{
		float L_0 = ___val0;
		float L_1 = ___inMin1;
		float L_2 = ___inMax2;
		float L_3;
		L_3 = Mathf_Clamp_m2416F3B785C8F135863E3D17E5B0CB4174797B87(L_0, L_1, L_2, /*hidden argument*/NULL);
		___val0 = L_3;
		float L_4 = ___val0;
		float L_5 = ___inMin1;
		float L_6 = ___outMax4;
		float L_7 = ___outMin3;
		float L_8 = ___inMax2;
		float L_9 = ___inMin1;
		float L_10 = ___outMin3;
		V_0 = ((float)il2cpp_codegen_add((float)((float)((float)((float)il2cpp_codegen_multiply((float)((float)il2cpp_codegen_subtract((float)L_4, (float)L_5)), (float)((float)il2cpp_codegen_subtract((float)L_6, (float)L_7))))/(float)((float)il2cpp_codegen_subtract((float)L_8, (float)L_9)))), (float)L_10));
		float L_11 = V_0;
		float L_12;
		L_12 = fabsf(L_11);
		float L_13 = ___outMin3;
		float L_14 = ___outMax4;
		float L_15;
		L_15 = Mathf_Clamp_m2416F3B785C8F135863E3D17E5B0CB4174797B87(L_12, L_13, L_14, /*hidden argument*/NULL);
		return L_15;
	}
}
// System.Void CrazyMinnow.SALSA.SalsaUtility::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void SalsaUtility__ctor_m32C37E3AD85E505EF76625DD2918D8CFAB1B6986 (SalsaUtility_t3157054D4AC9188D9E6BE3F1E4D00B71C49BB30B * __this, const RuntimeMethod* method)
{
	{
		Object__ctor_m88880E0413421D13FD95325EDCE231707CE1F405(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void CrazyMinnow.SALSA.Shape::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Shape__ctor_mF26431536C0DA3EA973AED80958736B9238D7ADA (Shape_t15D1659B3662125F0DBAA6FE8F716125CF62AF2F * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralDA39A3EE5E6B4B0D3255BFEF95601890AFD80709);
		s_Il2CppMethodInitialized = true;
	}
	{
		__this->set_name_1(_stringLiteralDA39A3EE5E6B4B0D3255BFEF95601890AFD80709);
		__this->set_blendSpeed_2((5.0f));
		Object__ctor_m88880E0413421D13FD95325EDCE231707CE1F405(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void CrazyMinnow.SALSA.Shape::.ctor(System.Int32,System.String,System.Single,System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Shape__ctor_m8FEA41876A6510E06F593EAC4A0915629895D292 (Shape_t15D1659B3662125F0DBAA6FE8F716125CF62AF2F * __this, int32_t ___index0, String_t* ___name1, float ___blendSpeed2, float ___rangeOfMotion3, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralDA39A3EE5E6B4B0D3255BFEF95601890AFD80709);
		s_Il2CppMethodInitialized = true;
	}
	{
		__this->set_name_1(_stringLiteralDA39A3EE5E6B4B0D3255BFEF95601890AFD80709);
		__this->set_blendSpeed_2((5.0f));
		Object__ctor_m88880E0413421D13FD95325EDCE231707CE1F405(__this, /*hidden argument*/NULL);
		int32_t L_0 = ___index0;
		__this->set_index_0(L_0);
		String_t* L_1 = ___name1;
		__this->set_name_1(L_1);
		float L_2 = ___blendSpeed2;
		__this->set_blendSpeed_2(L_2);
		float L_3 = ___rangeOfMotion3;
		__this->set_rangeOfMotion_3(L_3);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Boolean CrazyMinnow.SALSA.RandomEyes2D/<BlinkEyes>d__2::MoveNext()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool U3CBlinkEyesU3Ed__2_MoveNext_m89F92B1E6FB7BEE6A9A504CC3431FF670C19B14B (U3CBlinkEyesU3Ed__2_t7DFD119FC328CD8363C654182C85B8B62BA190D1 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&WaitForSeconds_t8F9189BE6E467C98C99177038881F8982E0E4013_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	int32_t V_2 = 0;
	{
		int32_t L_0 = __this->get_U3CU3E1__state_1();
		V_2 = L_0;
		int32_t L_1 = V_2;
		switch (L_1)
		{
			case 0:
			{
				goto IL_001a;
			}
			case 1:
			{
				goto IL_007b;
			}
		}
	}
	{
		goto IL_00c2;
	}

IL_001a:
	{
		__this->set_U3CU3E1__state_1((-1));
		V_0 = 0;
		goto IL_0051;
	}

IL_0025:
	{
		RandomEyes2D_t253864BE689DE7E477335D17ECD2EC77001B377F * L_2 = __this->get_U3CU3E4__this_2();
		NullCheck(L_2);
		SpriteRendererU5BU5D_t45BF125944C2F3974C568E650FCCDB591E5ABFBF* L_3 = L_2->get_eyeLids_11();
		int32_t L_4 = V_0;
		NullCheck(L_3);
		int32_t L_5 = L_4;
		SpriteRenderer_t3F35AD5498243C170B46F5FFDB582AAEF78615EF * L_6 = (L_3)->GetAt(static_cast<il2cpp_array_size_t>(L_5));
		IL2CPP_RUNTIME_CLASS_INIT(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		bool L_7;
		L_7 = Object_op_Inequality_mE1F187520BD83FB7D86A6D850710C4D42B864E90(L_6, (Object_tF2F3778131EFF286AF62B7B013A170F95A91571A *)NULL, /*hidden argument*/NULL);
		if (!L_7)
		{
			goto IL_004d;
		}
	}
	{
		RandomEyes2D_t253864BE689DE7E477335D17ECD2EC77001B377F * L_8 = __this->get_U3CU3E4__this_2();
		NullCheck(L_8);
		SpriteRendererU5BU5D_t45BF125944C2F3974C568E650FCCDB591E5ABFBF* L_9 = L_8->get_eyeLids_11();
		int32_t L_10 = V_0;
		NullCheck(L_9);
		int32_t L_11 = L_10;
		SpriteRenderer_t3F35AD5498243C170B46F5FFDB582AAEF78615EF * L_12 = (L_9)->GetAt(static_cast<il2cpp_array_size_t>(L_11));
		NullCheck(L_12);
		Renderer_set_enabled_mFFBA418C428C1B2B151C77B879DD10C393D9D95B(L_12, (bool)1, /*hidden argument*/NULL);
	}

IL_004d:
	{
		int32_t L_13 = V_0;
		V_0 = ((int32_t)il2cpp_codegen_add((int32_t)L_13, (int32_t)1));
	}

IL_0051:
	{
		int32_t L_14 = V_0;
		RandomEyes2D_t253864BE689DE7E477335D17ECD2EC77001B377F * L_15 = __this->get_U3CU3E4__this_2();
		NullCheck(L_15);
		SpriteRendererU5BU5D_t45BF125944C2F3974C568E650FCCDB591E5ABFBF* L_16 = L_15->get_eyeLids_11();
		NullCheck(L_16);
		if ((((int32_t)L_14) < ((int32_t)((int32_t)((int32_t)(((RuntimeArray*)L_16)->max_length))))))
		{
			goto IL_0025;
		}
	}
	{
		float L_17 = __this->get_duration_3();
		WaitForSeconds_t8F9189BE6E467C98C99177038881F8982E0E4013 * L_18 = (WaitForSeconds_t8F9189BE6E467C98C99177038881F8982E0E4013 *)il2cpp_codegen_object_new(WaitForSeconds_t8F9189BE6E467C98C99177038881F8982E0E4013_il2cpp_TypeInfo_var);
		WaitForSeconds__ctor_mD298C4CB9532BBBDE172FC40F3397E30504038D4(L_18, L_17, /*hidden argument*/NULL);
		__this->set_U3CU3E2__current_0(L_18);
		__this->set_U3CU3E1__state_1(1);
		return (bool)1;
	}

IL_007b:
	{
		__this->set_U3CU3E1__state_1((-1));
		V_1 = 0;
		goto IL_00b2;
	}

IL_0086:
	{
		RandomEyes2D_t253864BE689DE7E477335D17ECD2EC77001B377F * L_19 = __this->get_U3CU3E4__this_2();
		NullCheck(L_19);
		SpriteRendererU5BU5D_t45BF125944C2F3974C568E650FCCDB591E5ABFBF* L_20 = L_19->get_eyeLids_11();
		int32_t L_21 = V_1;
		NullCheck(L_20);
		int32_t L_22 = L_21;
		SpriteRenderer_t3F35AD5498243C170B46F5FFDB582AAEF78615EF * L_23 = (L_20)->GetAt(static_cast<il2cpp_array_size_t>(L_22));
		IL2CPP_RUNTIME_CLASS_INIT(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		bool L_24;
		L_24 = Object_op_Inequality_mE1F187520BD83FB7D86A6D850710C4D42B864E90(L_23, (Object_tF2F3778131EFF286AF62B7B013A170F95A91571A *)NULL, /*hidden argument*/NULL);
		if (!L_24)
		{
			goto IL_00ae;
		}
	}
	{
		RandomEyes2D_t253864BE689DE7E477335D17ECD2EC77001B377F * L_25 = __this->get_U3CU3E4__this_2();
		NullCheck(L_25);
		SpriteRendererU5BU5D_t45BF125944C2F3974C568E650FCCDB591E5ABFBF* L_26 = L_25->get_eyeLids_11();
		int32_t L_27 = V_1;
		NullCheck(L_26);
		int32_t L_28 = L_27;
		SpriteRenderer_t3F35AD5498243C170B46F5FFDB582AAEF78615EF * L_29 = (L_26)->GetAt(static_cast<il2cpp_array_size_t>(L_28));
		NullCheck(L_29);
		Renderer_set_enabled_mFFBA418C428C1B2B151C77B879DD10C393D9D95B(L_29, (bool)0, /*hidden argument*/NULL);
	}

IL_00ae:
	{
		int32_t L_30 = V_1;
		V_1 = ((int32_t)il2cpp_codegen_add((int32_t)L_30, (int32_t)1));
	}

IL_00b2:
	{
		int32_t L_31 = V_1;
		RandomEyes2D_t253864BE689DE7E477335D17ECD2EC77001B377F * L_32 = __this->get_U3CU3E4__this_2();
		NullCheck(L_32);
		SpriteRendererU5BU5D_t45BF125944C2F3974C568E650FCCDB591E5ABFBF* L_33 = L_32->get_eyeLids_11();
		NullCheck(L_33);
		if ((((int32_t)L_31) < ((int32_t)((int32_t)((int32_t)(((RuntimeArray*)L_33)->max_length))))))
		{
			goto IL_0086;
		}
	}

IL_00c2:
	{
		return (bool)0;
	}
}
// System.Object CrazyMinnow.SALSA.RandomEyes2D/<BlinkEyes>d__2::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject * U3CBlinkEyesU3Ed__2_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m81D3332B40A695A9AA1FDE3E8B85B2139144A02C (U3CBlinkEyesU3Ed__2_t7DFD119FC328CD8363C654182C85B8B62BA190D1 * __this, const RuntimeMethod* method)
{
	{
		RuntimeObject * L_0 = __this->get_U3CU3E2__current_0();
		return L_0;
	}
}
// System.Void CrazyMinnow.SALSA.RandomEyes2D/<BlinkEyes>d__2::System.Collections.IEnumerator.Reset()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CBlinkEyesU3Ed__2_System_Collections_IEnumerator_Reset_m88EBA7BE8E25F597E063CB1264198186B11DEBCB (U3CBlinkEyesU3Ed__2_t7DFD119FC328CD8363C654182C85B8B62BA190D1 * __this, const RuntimeMethod* method)
{
	{
		NotSupportedException_tB9D89F0E9470A2C423D239D7C68EE0CFD77F9339 * L_0 = (NotSupportedException_tB9D89F0E9470A2C423D239D7C68EE0CFD77F9339 *)il2cpp_codegen_object_new(((RuntimeClass*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&NotSupportedException_tB9D89F0E9470A2C423D239D7C68EE0CFD77F9339_il2cpp_TypeInfo_var)));
		NotSupportedException__ctor_m3EA81A5B209A87C3ADA47443F2AFFF735E5256EE(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0, ((RuntimeMethod*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&U3CBlinkEyesU3Ed__2_System_Collections_IEnumerator_Reset_m88EBA7BE8E25F597E063CB1264198186B11DEBCB_RuntimeMethod_var)));
	}
}
// System.Void CrazyMinnow.SALSA.RandomEyes2D/<BlinkEyes>d__2::System.IDisposable.Dispose()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CBlinkEyesU3Ed__2_System_IDisposable_Dispose_m617DA6BE6487FDE9E084BA9A3155DED4DD7FA55A (U3CBlinkEyesU3Ed__2_t7DFD119FC328CD8363C654182C85B8B62BA190D1 * __this, const RuntimeMethod* method)
{
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	{
		return;
	}
}
// System.Object CrazyMinnow.SALSA.RandomEyes2D/<BlinkEyes>d__2::System.Collections.IEnumerator.get_Current()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject * U3CBlinkEyesU3Ed__2_System_Collections_IEnumerator_get_Current_mA4EBBC50009E04F0472056FDA043C8E273572F7A (U3CBlinkEyesU3Ed__2_t7DFD119FC328CD8363C654182C85B8B62BA190D1 * __this, const RuntimeMethod* method)
{
	{
		RuntimeObject * L_0 = __this->get_U3CU3E2__current_0();
		return L_0;
	}
}
// System.Void CrazyMinnow.SALSA.RandomEyes2D/<BlinkEyes>d__2::.ctor(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CBlinkEyesU3Ed__2__ctor_m794D2B438E817C03906B33BA6CEA16630030D920 (U3CBlinkEyesU3Ed__2_t7DFD119FC328CD8363C654182C85B8B62BA190D1 * __this, int32_t ___U3CU3E1__state0, const RuntimeMethod* method)
{
	{
		Object__ctor_m88880E0413421D13FD95325EDCE231707CE1F405(__this, /*hidden argument*/NULL);
		int32_t L_0 = ___U3CU3E1__state0;
		__this->set_U3CU3E1__state_1(L_0);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Boolean CrazyMinnow.SALSA.RandomEyes2D/<TargetAffinityUpdate>d__0::MoveNext()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool U3CTargetAffinityUpdateU3Ed__0_MoveNext_mB4D2EE738CF3817FB48D0C654ADEB4A568B8D13C (U3CTargetAffinityUpdateU3Ed__0_tB9D47FFD16FA8A3CF1860EC8ADB05A787654DF78 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&WaitForSeconds_t8F9189BE6E467C98C99177038881F8982E0E4013_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	{
		int32_t L_0 = __this->get_U3CU3E1__state_1();
		V_0 = L_0;
		int32_t L_1 = V_0;
		switch (L_1)
		{
			case 0:
			{
				goto IL_001a;
			}
			case 1:
			{
				goto IL_00c6;
			}
		}
	}
	{
		goto IL_0102;
	}

IL_001a:
	{
		__this->set_U3CU3E1__state_1((-1));
	}

IL_0021:
	{
		RandomEyes2D_t253864BE689DE7E477335D17ECD2EC77001B377F * L_2 = __this->get_U3CU3E4__this_2();
		NullCheck(L_2);
		bool L_3 = L_2->get_targetAffinity_19();
		if (L_3)
		{
			goto IL_003a;
		}
	}
	{
		RandomEyes2D_t253864BE689DE7E477335D17ECD2EC77001B377F * L_4 = __this->get_U3CU3E4__this_2();
		NullCheck(L_4);
		L_4->set_hasAffinity_34((bool)0);
	}

IL_003a:
	{
		RandomEyes2D_t253864BE689DE7E477335D17ECD2EC77001B377F * L_5 = __this->get_U3CU3E4__this_2();
		NullCheck(L_5);
		bool L_6 = L_5->get_targetAffinity_19();
		if (!L_6)
		{
			goto IL_00a7;
		}
	}
	{
		RandomEyes2D_t253864BE689DE7E477335D17ECD2EC77001B377F * L_7 = __this->get_U3CU3E4__this_2();
		NullCheck(L_7);
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_8 = L_7->get_lookTarget_17();
		IL2CPP_RUNTIME_CLASS_INIT(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		bool L_9;
		L_9 = Object_op_Implicit_mC8214E4F028CC2F036CC82BDB81D102A02893499(L_8, /*hidden argument*/NULL);
		if (!L_9)
		{
			goto IL_00a7;
		}
	}
	{
		float L_10;
		L_10 = Random_get_value_m9AEBC7DF0BB6C57C928B0798349A7D3C0B3FB872(/*hidden argument*/NULL);
		RandomEyes2D_t253864BE689DE7E477335D17ECD2EC77001B377F * L_11 = __this->get_U3CU3E4__this_2();
		NullCheck(L_11);
		float L_12 = L_11->get_targetAffinityPercentage_20();
		if ((!(((float)L_10) <= ((float)L_12))))
		{
			goto IL_008f;
		}
	}
	{
		RandomEyes2D_t253864BE689DE7E477335D17ECD2EC77001B377F * L_13 = __this->get_U3CU3E4__this_2();
		NullCheck(L_13);
		L_13->set_hasAffinity_34((bool)1);
		RandomEyes2D_t253864BE689DE7E477335D17ECD2EC77001B377F * L_14 = __this->get_U3CU3E4__this_2();
		RandomEyes2D_t253864BE689DE7E477335D17ECD2EC77001B377F * L_15 = __this->get_U3CU3E4__this_2();
		NullCheck(L_15);
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_16 = L_15->get_lookTarget_17();
		NullCheck(L_14);
		RandomEyes2D_SetLookTargetTracking_m3B736925D0B69176837D0D89485EB6748354ECE9(L_14, L_16, /*hidden argument*/NULL);
		goto IL_00a7;
	}

IL_008f:
	{
		RandomEyes2D_t253864BE689DE7E477335D17ECD2EC77001B377F * L_17 = __this->get_U3CU3E4__this_2();
		NullCheck(L_17);
		L_17->set_hasAffinity_34((bool)0);
		RandomEyes2D_t253864BE689DE7E477335D17ECD2EC77001B377F * L_18 = __this->get_U3CU3E4__this_2();
		NullCheck(L_18);
		RandomEyes2D_SetLookTargetTracking_m3B736925D0B69176837D0D89485EB6748354ECE9(L_18, (GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 *)NULL, /*hidden argument*/NULL);
	}

IL_00a7:
	{
		RandomEyes2D_t253864BE689DE7E477335D17ECD2EC77001B377F * L_19 = __this->get_U3CU3E4__this_2();
		NullCheck(L_19);
		float L_20 = L_19->get_targetAffinityTimer_33();
		WaitForSeconds_t8F9189BE6E467C98C99177038881F8982E0E4013 * L_21 = (WaitForSeconds_t8F9189BE6E467C98C99177038881F8982E0E4013 *)il2cpp_codegen_object_new(WaitForSeconds_t8F9189BE6E467C98C99177038881F8982E0E4013_il2cpp_TypeInfo_var);
		WaitForSeconds__ctor_mD298C4CB9532BBBDE172FC40F3397E30504038D4(L_21, L_20, /*hidden argument*/NULL);
		__this->set_U3CU3E2__current_0(L_21);
		__this->set_U3CU3E1__state_1(1);
		return (bool)1;
	}

IL_00c6:
	{
		__this->set_U3CU3E1__state_1((-1));
		RandomEyes2D_t253864BE689DE7E477335D17ECD2EC77001B377F * L_22 = __this->get_U3CU3E4__this_2();
		RandomEyes2D_t253864BE689DE7E477335D17ECD2EC77001B377F * L_23 = __this->get_U3CU3E4__this_2();
		NullCheck(L_23);
		Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 * L_24 = L_23->get_address_of_targetAffinityTimerRange_32();
		float L_25 = L_24->get_x_0();
		RandomEyes2D_t253864BE689DE7E477335D17ECD2EC77001B377F * L_26 = __this->get_U3CU3E4__this_2();
		NullCheck(L_26);
		Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 * L_27 = L_26->get_address_of_targetAffinityTimerRange_32();
		float L_28 = L_27->get_y_1();
		float L_29;
		L_29 = Random_Range_mC15372D42A9ABDCAC3DE82E114D60A40C9C311D2(L_25, L_28, /*hidden argument*/NULL);
		NullCheck(L_22);
		L_22->set_targetAffinityTimer_33(L_29);
		goto IL_0021;
	}

IL_0102:
	{
		return (bool)0;
	}
}
// System.Object CrazyMinnow.SALSA.RandomEyes2D/<TargetAffinityUpdate>d__0::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject * U3CTargetAffinityUpdateU3Ed__0_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m8D0D3B955C4C2F3FCBBAD3D5FA4A2EE44ADA142D (U3CTargetAffinityUpdateU3Ed__0_tB9D47FFD16FA8A3CF1860EC8ADB05A787654DF78 * __this, const RuntimeMethod* method)
{
	{
		RuntimeObject * L_0 = __this->get_U3CU3E2__current_0();
		return L_0;
	}
}
// System.Void CrazyMinnow.SALSA.RandomEyes2D/<TargetAffinityUpdate>d__0::System.Collections.IEnumerator.Reset()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CTargetAffinityUpdateU3Ed__0_System_Collections_IEnumerator_Reset_mED26C7B767350312099E767FA28653FB03743451 (U3CTargetAffinityUpdateU3Ed__0_tB9D47FFD16FA8A3CF1860EC8ADB05A787654DF78 * __this, const RuntimeMethod* method)
{
	{
		NotSupportedException_tB9D89F0E9470A2C423D239D7C68EE0CFD77F9339 * L_0 = (NotSupportedException_tB9D89F0E9470A2C423D239D7C68EE0CFD77F9339 *)il2cpp_codegen_object_new(((RuntimeClass*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&NotSupportedException_tB9D89F0E9470A2C423D239D7C68EE0CFD77F9339_il2cpp_TypeInfo_var)));
		NotSupportedException__ctor_m3EA81A5B209A87C3ADA47443F2AFFF735E5256EE(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0, ((RuntimeMethod*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&U3CTargetAffinityUpdateU3Ed__0_System_Collections_IEnumerator_Reset_mED26C7B767350312099E767FA28653FB03743451_RuntimeMethod_var)));
	}
}
// System.Void CrazyMinnow.SALSA.RandomEyes2D/<TargetAffinityUpdate>d__0::System.IDisposable.Dispose()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CTargetAffinityUpdateU3Ed__0_System_IDisposable_Dispose_m9C4EEA564DC977DB7BB9E05AFF7F66C3627BD119 (U3CTargetAffinityUpdateU3Ed__0_tB9D47FFD16FA8A3CF1860EC8ADB05A787654DF78 * __this, const RuntimeMethod* method)
{
	{
		return;
	}
}
// System.Object CrazyMinnow.SALSA.RandomEyes2D/<TargetAffinityUpdate>d__0::System.Collections.IEnumerator.get_Current()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject * U3CTargetAffinityUpdateU3Ed__0_System_Collections_IEnumerator_get_Current_m681473C79048E8CF76C118B34813BB9D67BB552A (U3CTargetAffinityUpdateU3Ed__0_tB9D47FFD16FA8A3CF1860EC8ADB05A787654DF78 * __this, const RuntimeMethod* method)
{
	{
		RuntimeObject * L_0 = __this->get_U3CU3E2__current_0();
		return L_0;
	}
}
// System.Void CrazyMinnow.SALSA.RandomEyes2D/<TargetAffinityUpdate>d__0::.ctor(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CTargetAffinityUpdateU3Ed__0__ctor_m11474126BC59372CD200224F8F86C2D80D12BD0C (U3CTargetAffinityUpdateU3Ed__0_tB9D47FFD16FA8A3CF1860EC8ADB05A787654DF78 * __this, int32_t ___U3CU3E1__state0, const RuntimeMethod* method)
{
	{
		Object__ctor_m88880E0413421D13FD95325EDCE231707CE1F405(__this, /*hidden argument*/NULL);
		int32_t L_0 = ___U3CU3E1__state0;
		__this->set_U3CU3E1__state_1(L_0);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Boolean CrazyMinnow.SALSA.RandomEyes3D/<BlinkEyes>d__2::MoveNext()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool U3CBlinkEyesU3Ed__2_MoveNext_mD6E6B0ECBC82B2E9C31F22D8F7DEB26604A96FDE (U3CBlinkEyesU3Ed__2_t05DA28F00D8E1A9E64D0728065DB53DA96232044 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&WaitForSeconds_t8F9189BE6E467C98C99177038881F8982E0E4013_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	{
		int32_t L_0 = __this->get_U3CU3E1__state_1();
		V_0 = L_0;
		int32_t L_1 = V_0;
		switch (L_1)
		{
			case 0:
			{
				goto IL_0017;
			}
			case 1:
			{
				goto IL_0038;
			}
		}
	}
	{
		goto IL_004b;
	}

IL_0017:
	{
		__this->set_U3CU3E1__state_1((-1));
		float L_2 = __this->get_duration_3();
		WaitForSeconds_t8F9189BE6E467C98C99177038881F8982E0E4013 * L_3 = (WaitForSeconds_t8F9189BE6E467C98C99177038881F8982E0E4013 *)il2cpp_codegen_object_new(WaitForSeconds_t8F9189BE6E467C98C99177038881F8982E0E4013_il2cpp_TypeInfo_var);
		WaitForSeconds__ctor_mD298C4CB9532BBBDE172FC40F3397E30504038D4(L_3, L_2, /*hidden argument*/NULL);
		__this->set_U3CU3E2__current_0(L_3);
		__this->set_U3CU3E1__state_1(1);
		return (bool)1;
	}

IL_0038:
	{
		__this->set_U3CU3E1__state_1((-1));
		RandomEyes3D_t6868157C4605BD0225E1B08301F110E72336CE45 * L_4 = __this->get_U3CU3E4__this_2();
		NullCheck(L_4);
		L_4->set_isBlinking_80((bool)0);
	}

IL_004b:
	{
		return (bool)0;
	}
}
// System.Object CrazyMinnow.SALSA.RandomEyes3D/<BlinkEyes>d__2::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject * U3CBlinkEyesU3Ed__2_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mB5FD936BCB45081E3D0AD94D05AB1EA32D510B74 (U3CBlinkEyesU3Ed__2_t05DA28F00D8E1A9E64D0728065DB53DA96232044 * __this, const RuntimeMethod* method)
{
	{
		RuntimeObject * L_0 = __this->get_U3CU3E2__current_0();
		return L_0;
	}
}
// System.Void CrazyMinnow.SALSA.RandomEyes3D/<BlinkEyes>d__2::System.Collections.IEnumerator.Reset()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CBlinkEyesU3Ed__2_System_Collections_IEnumerator_Reset_mD5DCBE6D12F53C662EF106666A000BEB03ED3558 (U3CBlinkEyesU3Ed__2_t05DA28F00D8E1A9E64D0728065DB53DA96232044 * __this, const RuntimeMethod* method)
{
	{
		NotSupportedException_tB9D89F0E9470A2C423D239D7C68EE0CFD77F9339 * L_0 = (NotSupportedException_tB9D89F0E9470A2C423D239D7C68EE0CFD77F9339 *)il2cpp_codegen_object_new(((RuntimeClass*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&NotSupportedException_tB9D89F0E9470A2C423D239D7C68EE0CFD77F9339_il2cpp_TypeInfo_var)));
		NotSupportedException__ctor_m3EA81A5B209A87C3ADA47443F2AFFF735E5256EE(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0, ((RuntimeMethod*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&U3CBlinkEyesU3Ed__2_System_Collections_IEnumerator_Reset_mD5DCBE6D12F53C662EF106666A000BEB03ED3558_RuntimeMethod_var)));
	}
}
// System.Void CrazyMinnow.SALSA.RandomEyes3D/<BlinkEyes>d__2::System.IDisposable.Dispose()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CBlinkEyesU3Ed__2_System_IDisposable_Dispose_m2DBE82FEB6DF1FEDBCBE44AA8BBA27D6A63250BC (U3CBlinkEyesU3Ed__2_t05DA28F00D8E1A9E64D0728065DB53DA96232044 * __this, const RuntimeMethod* method)
{
	{
		return;
	}
}
// System.Object CrazyMinnow.SALSA.RandomEyes3D/<BlinkEyes>d__2::System.Collections.IEnumerator.get_Current()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject * U3CBlinkEyesU3Ed__2_System_Collections_IEnumerator_get_Current_mF267239843A192233380FE5A3DA1DA92BA248B7E (U3CBlinkEyesU3Ed__2_t05DA28F00D8E1A9E64D0728065DB53DA96232044 * __this, const RuntimeMethod* method)
{
	{
		RuntimeObject * L_0 = __this->get_U3CU3E2__current_0();
		return L_0;
	}
}
// System.Void CrazyMinnow.SALSA.RandomEyes3D/<BlinkEyes>d__2::.ctor(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CBlinkEyesU3Ed__2__ctor_m1C070BB710ACF1198445B49A02B0EE406A56F50C (U3CBlinkEyesU3Ed__2_t05DA28F00D8E1A9E64D0728065DB53DA96232044 * __this, int32_t ___U3CU3E1__state0, const RuntimeMethod* method)
{
	{
		Object__ctor_m88880E0413421D13FD95325EDCE231707CE1F405(__this, /*hidden argument*/NULL);
		int32_t L_0 = ___U3CU3E1__state0;
		__this->set_U3CU3E1__state_1(L_0);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Boolean CrazyMinnow.SALSA.RandomEyes3D/<DoBlink>d__c::MoveNext()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool U3CDoBlinkU3Ed__c_MoveNext_m6FE3368A1FA11FF8B8A8DF812B8ED7EC26F6EB6E (U3CDoBlinkU3Ed__c_t8CF4EE9B667E494EE34FBB05C5E960320E5C8418 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&WaitForSeconds_t8F9189BE6E467C98C99177038881F8982E0E4013_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	{
		int32_t L_0 = __this->get_U3CU3E1__state_1();
		V_0 = L_0;
		int32_t L_1 = V_0;
		switch (L_1)
		{
			case 0:
			{
				goto IL_0022;
			}
			case 1:
			{
				goto IL_00ea;
			}
			case 2:
			{
				goto IL_0126;
			}
			case 3:
			{
				goto IL_01e2;
			}
		}
	}
	{
		goto IL_0210;
	}

IL_0022:
	{
		__this->set_U3CU3E1__state_1((-1));
		RandomEyes3D_t6868157C4605BD0225E1B08301F110E72336CE45 * L_2 = __this->get_U3CU3E4__this_2();
		NullCheck(L_2);
		L_2->set_inBlinkSequence_81((bool)1);
		goto IL_00f1;
	}

IL_003a:
	{
		RandomEyes3D_t6868157C4605BD0225E1B08301F110E72336CE45 * L_3 = __this->get_U3CU3E4__this_2();
		RandomEyes3D_t6868157C4605BD0225E1B08301F110E72336CE45 * L_4 = L_3;
		NullCheck(L_4);
		float L_5 = L_4->get_currentCloseAmount_82();
		RandomEyes3D_t6868157C4605BD0225E1B08301F110E72336CE45 * L_6 = __this->get_U3CU3E4__this_2();
		NullCheck(L_6);
		float L_7 = L_6->get_blinkSpeed_22();
		NullCheck(L_4);
		L_4->set_currentCloseAmount_82(((float)il2cpp_codegen_add((float)L_5, (float)L_7)));
		RandomEyes3D_t6868157C4605BD0225E1B08301F110E72336CE45 * L_8 = __this->get_U3CU3E4__this_2();
		NullCheck(L_8);
		RandomEyesBlendAmounts_t728404128F97998E0D58F3849065DCD32A181621 * L_9 = L_8->get_lookAmount_16();
		RandomEyes3D_t6868157C4605BD0225E1B08301F110E72336CE45 * L_10 = __this->get_U3CU3E4__this_2();
		NullCheck(L_10);
		float L_11 = L_10->get_currentCloseAmount_82();
		NullCheck(L_9);
		L_9->set_blink_4(L_11);
		RandomEyes3D_t6868157C4605BD0225E1B08301F110E72336CE45 * L_12 = __this->get_U3CU3E4__this_2();
		NullCheck(L_12);
		float L_13 = L_12->get_currentCloseAmount_82();
		RandomEyes3D_t6868157C4605BD0225E1B08301F110E72336CE45 * L_14 = __this->get_U3CU3E4__this_2();
		NullCheck(L_14);
		float L_15 = L_14->get_openMax_23();
		RandomEyes3D_t6868157C4605BD0225E1B08301F110E72336CE45 * L_16 = __this->get_U3CU3E4__this_2();
		NullCheck(L_16);
		float L_17 = L_16->get_closeMax_24();
		float L_18;
		L_18 = Mathf_Clamp_m2416F3B785C8F135863E3D17E5B0CB4174797B87(L_13, L_15, L_17, /*hidden argument*/NULL);
		RandomEyes3D_t6868157C4605BD0225E1B08301F110E72336CE45 * L_19 = __this->get_U3CU3E4__this_2();
		NullCheck(L_19);
		SkinnedMeshRenderer_t126F4D6010E0F4B2685A7817B0A9171805D8F496 * L_20 = L_19->get_skinnedMeshRenderer_6();
		IL2CPP_RUNTIME_CLASS_INIT(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		bool L_21;
		L_21 = Object_op_Implicit_mC8214E4F028CC2F036CC82BDB81D102A02893499(L_20, /*hidden argument*/NULL);
		if (!L_21)
		{
			goto IL_00d1;
		}
	}
	{
		RandomEyes3D_t6868157C4605BD0225E1B08301F110E72336CE45 * L_22 = __this->get_U3CU3E4__this_2();
		NullCheck(L_22);
		SkinnedMeshRenderer_t126F4D6010E0F4B2685A7817B0A9171805D8F496 * L_23 = L_22->get_skinnedMeshRenderer_6();
		RandomEyes3D_t6868157C4605BD0225E1B08301F110E72336CE45 * L_24 = __this->get_U3CU3E4__this_2();
		NullCheck(L_24);
		int32_t L_25 = L_24->get_blinkIndex_14();
		RandomEyes3D_t6868157C4605BD0225E1B08301F110E72336CE45 * L_26 = __this->get_U3CU3E4__this_2();
		NullCheck(L_26);
		float L_27 = L_26->get_currentCloseAmount_82();
		NullCheck(L_23);
		SkinnedMeshRenderer_SetBlendShapeWeight_mF546F3567C5039C217AD1E32157B992B4124B5FD(L_23, L_25, L_27, /*hidden argument*/NULL);
	}

IL_00d1:
	{
		float L_28;
		L_28 = Time_get_deltaTime_mCC15F147DA67F38C74CE408FB5D7FF4A87DA2290(/*hidden argument*/NULL);
		WaitForSeconds_t8F9189BE6E467C98C99177038881F8982E0E4013 * L_29 = (WaitForSeconds_t8F9189BE6E467C98C99177038881F8982E0E4013 *)il2cpp_codegen_object_new(WaitForSeconds_t8F9189BE6E467C98C99177038881F8982E0E4013_il2cpp_TypeInfo_var);
		WaitForSeconds__ctor_mD298C4CB9532BBBDE172FC40F3397E30504038D4(L_29, L_28, /*hidden argument*/NULL);
		__this->set_U3CU3E2__current_0(L_29);
		__this->set_U3CU3E1__state_1(1);
		return (bool)1;
	}

IL_00ea:
	{
		__this->set_U3CU3E1__state_1((-1));
	}

IL_00f1:
	{
		RandomEyes3D_t6868157C4605BD0225E1B08301F110E72336CE45 * L_30 = __this->get_U3CU3E4__this_2();
		NullCheck(L_30);
		float L_31 = L_30->get_currentCloseAmount_82();
		RandomEyes3D_t6868157C4605BD0225E1B08301F110E72336CE45 * L_32 = __this->get_U3CU3E4__this_2();
		NullCheck(L_32);
		float L_33 = L_32->get_closeMax_24();
		if ((((float)L_31) < ((float)L_33)))
		{
			goto IL_003a;
		}
	}
	{
		float L_34 = __this->get_duration_3();
		WaitForSeconds_t8F9189BE6E467C98C99177038881F8982E0E4013 * L_35 = (WaitForSeconds_t8F9189BE6E467C98C99177038881F8982E0E4013 *)il2cpp_codegen_object_new(WaitForSeconds_t8F9189BE6E467C98C99177038881F8982E0E4013_il2cpp_TypeInfo_var);
		WaitForSeconds__ctor_mD298C4CB9532BBBDE172FC40F3397E30504038D4(L_35, L_34, /*hidden argument*/NULL);
		__this->set_U3CU3E2__current_0(L_35);
		__this->set_U3CU3E1__state_1(2);
		return (bool)1;
	}

IL_0126:
	{
		__this->set_U3CU3E1__state_1((-1));
		goto IL_01e9;
	}

IL_0132:
	{
		RandomEyes3D_t6868157C4605BD0225E1B08301F110E72336CE45 * L_36 = __this->get_U3CU3E4__this_2();
		RandomEyes3D_t6868157C4605BD0225E1B08301F110E72336CE45 * L_37 = L_36;
		NullCheck(L_37);
		float L_38 = L_37->get_currentCloseAmount_82();
		RandomEyes3D_t6868157C4605BD0225E1B08301F110E72336CE45 * L_39 = __this->get_U3CU3E4__this_2();
		NullCheck(L_39);
		float L_40 = L_39->get_blinkSpeed_22();
		NullCheck(L_37);
		L_37->set_currentCloseAmount_82(((float)il2cpp_codegen_subtract((float)L_38, (float)L_40)));
		RandomEyes3D_t6868157C4605BD0225E1B08301F110E72336CE45 * L_41 = __this->get_U3CU3E4__this_2();
		NullCheck(L_41);
		RandomEyesBlendAmounts_t728404128F97998E0D58F3849065DCD32A181621 * L_42 = L_41->get_lookAmount_16();
		RandomEyes3D_t6868157C4605BD0225E1B08301F110E72336CE45 * L_43 = __this->get_U3CU3E4__this_2();
		NullCheck(L_43);
		float L_44 = L_43->get_currentCloseAmount_82();
		NullCheck(L_42);
		L_42->set_blink_4(L_44);
		RandomEyes3D_t6868157C4605BD0225E1B08301F110E72336CE45 * L_45 = __this->get_U3CU3E4__this_2();
		NullCheck(L_45);
		float L_46 = L_45->get_currentCloseAmount_82();
		RandomEyes3D_t6868157C4605BD0225E1B08301F110E72336CE45 * L_47 = __this->get_U3CU3E4__this_2();
		NullCheck(L_47);
		float L_48 = L_47->get_openMax_23();
		RandomEyes3D_t6868157C4605BD0225E1B08301F110E72336CE45 * L_49 = __this->get_U3CU3E4__this_2();
		NullCheck(L_49);
		float L_50 = L_49->get_closeMax_24();
		float L_51;
		L_51 = Mathf_Clamp_m2416F3B785C8F135863E3D17E5B0CB4174797B87(L_46, L_48, L_50, /*hidden argument*/NULL);
		RandomEyes3D_t6868157C4605BD0225E1B08301F110E72336CE45 * L_52 = __this->get_U3CU3E4__this_2();
		NullCheck(L_52);
		SkinnedMeshRenderer_t126F4D6010E0F4B2685A7817B0A9171805D8F496 * L_53 = L_52->get_skinnedMeshRenderer_6();
		IL2CPP_RUNTIME_CLASS_INIT(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		bool L_54;
		L_54 = Object_op_Implicit_mC8214E4F028CC2F036CC82BDB81D102A02893499(L_53, /*hidden argument*/NULL);
		if (!L_54)
		{
			goto IL_01c9;
		}
	}
	{
		RandomEyes3D_t6868157C4605BD0225E1B08301F110E72336CE45 * L_55 = __this->get_U3CU3E4__this_2();
		NullCheck(L_55);
		SkinnedMeshRenderer_t126F4D6010E0F4B2685A7817B0A9171805D8F496 * L_56 = L_55->get_skinnedMeshRenderer_6();
		RandomEyes3D_t6868157C4605BD0225E1B08301F110E72336CE45 * L_57 = __this->get_U3CU3E4__this_2();
		NullCheck(L_57);
		int32_t L_58 = L_57->get_blinkIndex_14();
		RandomEyes3D_t6868157C4605BD0225E1B08301F110E72336CE45 * L_59 = __this->get_U3CU3E4__this_2();
		NullCheck(L_59);
		float L_60 = L_59->get_currentCloseAmount_82();
		NullCheck(L_56);
		SkinnedMeshRenderer_SetBlendShapeWeight_mF546F3567C5039C217AD1E32157B992B4124B5FD(L_56, L_58, L_60, /*hidden argument*/NULL);
	}

IL_01c9:
	{
		float L_61;
		L_61 = Time_get_deltaTime_mCC15F147DA67F38C74CE408FB5D7FF4A87DA2290(/*hidden argument*/NULL);
		WaitForSeconds_t8F9189BE6E467C98C99177038881F8982E0E4013 * L_62 = (WaitForSeconds_t8F9189BE6E467C98C99177038881F8982E0E4013 *)il2cpp_codegen_object_new(WaitForSeconds_t8F9189BE6E467C98C99177038881F8982E0E4013_il2cpp_TypeInfo_var);
		WaitForSeconds__ctor_mD298C4CB9532BBBDE172FC40F3397E30504038D4(L_62, L_61, /*hidden argument*/NULL);
		__this->set_U3CU3E2__current_0(L_62);
		__this->set_U3CU3E1__state_1(3);
		return (bool)1;
	}

IL_01e2:
	{
		__this->set_U3CU3E1__state_1((-1));
	}

IL_01e9:
	{
		RandomEyes3D_t6868157C4605BD0225E1B08301F110E72336CE45 * L_63 = __this->get_U3CU3E4__this_2();
		NullCheck(L_63);
		float L_64 = L_63->get_currentCloseAmount_82();
		RandomEyes3D_t6868157C4605BD0225E1B08301F110E72336CE45 * L_65 = __this->get_U3CU3E4__this_2();
		NullCheck(L_65);
		float L_66 = L_65->get_openMax_23();
		if ((((float)L_64) > ((float)L_66)))
		{
			goto IL_0132;
		}
	}
	{
		RandomEyes3D_t6868157C4605BD0225E1B08301F110E72336CE45 * L_67 = __this->get_U3CU3E4__this_2();
		NullCheck(L_67);
		L_67->set_inBlinkSequence_81((bool)0);
	}

IL_0210:
	{
		return (bool)0;
	}
}
// System.Object CrazyMinnow.SALSA.RandomEyes3D/<DoBlink>d__c::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject * U3CDoBlinkU3Ed__c_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m401CF5F735EAD27CA82731FA9AAC660CEB51F4CA (U3CDoBlinkU3Ed__c_t8CF4EE9B667E494EE34FBB05C5E960320E5C8418 * __this, const RuntimeMethod* method)
{
	{
		RuntimeObject * L_0 = __this->get_U3CU3E2__current_0();
		return L_0;
	}
}
// System.Void CrazyMinnow.SALSA.RandomEyes3D/<DoBlink>d__c::System.Collections.IEnumerator.Reset()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CDoBlinkU3Ed__c_System_Collections_IEnumerator_Reset_m04161B4126710078525F0D3F99E46A39A538B5B6 (U3CDoBlinkU3Ed__c_t8CF4EE9B667E494EE34FBB05C5E960320E5C8418 * __this, const RuntimeMethod* method)
{
	{
		NotSupportedException_tB9D89F0E9470A2C423D239D7C68EE0CFD77F9339 * L_0 = (NotSupportedException_tB9D89F0E9470A2C423D239D7C68EE0CFD77F9339 *)il2cpp_codegen_object_new(((RuntimeClass*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&NotSupportedException_tB9D89F0E9470A2C423D239D7C68EE0CFD77F9339_il2cpp_TypeInfo_var)));
		NotSupportedException__ctor_m3EA81A5B209A87C3ADA47443F2AFFF735E5256EE(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0, ((RuntimeMethod*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&U3CDoBlinkU3Ed__c_System_Collections_IEnumerator_Reset_m04161B4126710078525F0D3F99E46A39A538B5B6_RuntimeMethod_var)));
	}
}
// System.Void CrazyMinnow.SALSA.RandomEyes3D/<DoBlink>d__c::System.IDisposable.Dispose()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CDoBlinkU3Ed__c_System_IDisposable_Dispose_m700124204B78C1A7F4A5BD798ECE2D22492C7FB6 (U3CDoBlinkU3Ed__c_t8CF4EE9B667E494EE34FBB05C5E960320E5C8418 * __this, const RuntimeMethod* method)
{
	{
		return;
	}
}
// System.Object CrazyMinnow.SALSA.RandomEyes3D/<DoBlink>d__c::System.Collections.IEnumerator.get_Current()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject * U3CDoBlinkU3Ed__c_System_Collections_IEnumerator_get_Current_mA6C9A533B91FE9022C5929D1F59EA6849129B59D (U3CDoBlinkU3Ed__c_t8CF4EE9B667E494EE34FBB05C5E960320E5C8418 * __this, const RuntimeMethod* method)
{
	{
		RuntimeObject * L_0 = __this->get_U3CU3E2__current_0();
		return L_0;
	}
}
// System.Void CrazyMinnow.SALSA.RandomEyes3D/<DoBlink>d__c::.ctor(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CDoBlinkU3Ed__c__ctor_m9D9176B98A436D63C7C71A253F9242C3ACD37F00 (U3CDoBlinkU3Ed__c_t8CF4EE9B667E494EE34FBB05C5E960320E5C8418 * __this, int32_t ___U3CU3E1__state0, const RuntimeMethod* method)
{
	{
		Object__ctor_m88880E0413421D13FD95325EDCE231707CE1F405(__this, /*hidden argument*/NULL);
		int32_t L_0 = ___U3CU3E1__state0;
		__this->set_U3CU3E1__state_1(L_0);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Boolean CrazyMinnow.SALSA.RandomEyes3D/<SetCustomShapeDuration>d__8::MoveNext()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool U3CSetCustomShapeDurationU3Ed__8_MoveNext_mF5A8B9DF3C76678C8ED39451F03A21D4500F39BF (U3CSetCustomShapeDurationU3Ed__8_t1CD4174D8597ACF6A20E2146BB78651E84AE273B * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&WaitForSeconds_t8F9189BE6E467C98C99177038881F8982E0E4013_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	{
		int32_t L_0 = __this->get_U3CU3E1__state_1();
		V_0 = L_0;
		int32_t L_1 = V_0;
		switch (L_1)
		{
			case 0:
			{
				goto IL_0017;
			}
			case 1:
			{
				goto IL_0049;
			}
		}
	}
	{
		goto IL_005c;
	}

IL_0017:
	{
		__this->set_U3CU3E1__state_1((-1));
		RandomEyes3D_t6868157C4605BD0225E1B08301F110E72336CE45 * L_2 = __this->get_U3CU3E4__this_2();
		String_t* L_3 = __this->get_shapeName_3();
		NullCheck(L_2);
		RandomEyes3D_SetCustomShape_m0B61CBCBC3389E335E869448A557D96549915D36(L_2, L_3, /*hidden argument*/NULL);
		float L_4 = __this->get_duration_4();
		WaitForSeconds_t8F9189BE6E467C98C99177038881F8982E0E4013 * L_5 = (WaitForSeconds_t8F9189BE6E467C98C99177038881F8982E0E4013 *)il2cpp_codegen_object_new(WaitForSeconds_t8F9189BE6E467C98C99177038881F8982E0E4013_il2cpp_TypeInfo_var);
		WaitForSeconds__ctor_mD298C4CB9532BBBDE172FC40F3397E30504038D4(L_5, L_4, /*hidden argument*/NULL);
		__this->set_U3CU3E2__current_0(L_5);
		__this->set_U3CU3E1__state_1(1);
		return (bool)1;
	}

IL_0049:
	{
		__this->set_U3CU3E1__state_1((-1));
		RandomEyes3D_t6868157C4605BD0225E1B08301F110E72336CE45 * L_6 = __this->get_U3CU3E4__this_2();
		NullCheck(L_6);
		RandomEyes3D_SetCustomShape_m0B61CBCBC3389E335E869448A557D96549915D36(L_6, (String_t*)NULL, /*hidden argument*/NULL);
	}

IL_005c:
	{
		return (bool)0;
	}
}
// System.Object CrazyMinnow.SALSA.RandomEyes3D/<SetCustomShapeDuration>d__8::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject * U3CSetCustomShapeDurationU3Ed__8_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m1DD68496D286084B27BA2709A378BF2620CF9534 (U3CSetCustomShapeDurationU3Ed__8_t1CD4174D8597ACF6A20E2146BB78651E84AE273B * __this, const RuntimeMethod* method)
{
	{
		RuntimeObject * L_0 = __this->get_U3CU3E2__current_0();
		return L_0;
	}
}
// System.Void CrazyMinnow.SALSA.RandomEyes3D/<SetCustomShapeDuration>d__8::System.Collections.IEnumerator.Reset()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CSetCustomShapeDurationU3Ed__8_System_Collections_IEnumerator_Reset_mA93C2810F2DFD6BB6409274C433C796616BD935F (U3CSetCustomShapeDurationU3Ed__8_t1CD4174D8597ACF6A20E2146BB78651E84AE273B * __this, const RuntimeMethod* method)
{
	{
		NotSupportedException_tB9D89F0E9470A2C423D239D7C68EE0CFD77F9339 * L_0 = (NotSupportedException_tB9D89F0E9470A2C423D239D7C68EE0CFD77F9339 *)il2cpp_codegen_object_new(((RuntimeClass*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&NotSupportedException_tB9D89F0E9470A2C423D239D7C68EE0CFD77F9339_il2cpp_TypeInfo_var)));
		NotSupportedException__ctor_m3EA81A5B209A87C3ADA47443F2AFFF735E5256EE(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0, ((RuntimeMethod*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&U3CSetCustomShapeDurationU3Ed__8_System_Collections_IEnumerator_Reset_mA93C2810F2DFD6BB6409274C433C796616BD935F_RuntimeMethod_var)));
	}
}
// System.Void CrazyMinnow.SALSA.RandomEyes3D/<SetCustomShapeDuration>d__8::System.IDisposable.Dispose()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CSetCustomShapeDurationU3Ed__8_System_IDisposable_Dispose_m0EB432743B895C26FA9451C2F70D14108A12DBBE (U3CSetCustomShapeDurationU3Ed__8_t1CD4174D8597ACF6A20E2146BB78651E84AE273B * __this, const RuntimeMethod* method)
{
	{
		return;
	}
}
// System.Object CrazyMinnow.SALSA.RandomEyes3D/<SetCustomShapeDuration>d__8::System.Collections.IEnumerator.get_Current()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject * U3CSetCustomShapeDurationU3Ed__8_System_Collections_IEnumerator_get_Current_m655AA5A40FE04228B897FF3815520CE581E9C3DC (U3CSetCustomShapeDurationU3Ed__8_t1CD4174D8597ACF6A20E2146BB78651E84AE273B * __this, const RuntimeMethod* method)
{
	{
		RuntimeObject * L_0 = __this->get_U3CU3E2__current_0();
		return L_0;
	}
}
// System.Void CrazyMinnow.SALSA.RandomEyes3D/<SetCustomShapeDuration>d__8::.ctor(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CSetCustomShapeDurationU3Ed__8__ctor_mF6CD6A348AAE88971E18E1E4E94D43FF9A964EB3 (U3CSetCustomShapeDurationU3Ed__8_t1CD4174D8597ACF6A20E2146BB78651E84AE273B * __this, int32_t ___U3CU3E1__state0, const RuntimeMethod* method)
{
	{
		Object__ctor_m88880E0413421D13FD95325EDCE231707CE1F405(__this, /*hidden argument*/NULL);
		int32_t L_0 = ___U3CU3E1__state0;
		__this->set_U3CU3E1__state_1(L_0);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Boolean CrazyMinnow.SALSA.RandomEyes3D/<SetCustomShapeDuration>d__a::MoveNext()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool U3CSetCustomShapeDurationU3Ed__a_MoveNext_m80EA5F24116A83527DAA55FD2D166CFF55C1457C (U3CSetCustomShapeDurationU3Ed__a_tF3A147C5E712D0581C2B12133986348BFBB67140 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&WaitForSeconds_t8F9189BE6E467C98C99177038881F8982E0E4013_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	{
		int32_t L_0 = __this->get_U3CU3E1__state_1();
		V_0 = L_0;
		int32_t L_1 = V_0;
		switch (L_1)
		{
			case 0:
			{
				goto IL_0017;
			}
			case 1:
			{
				goto IL_0049;
			}
		}
	}
	{
		goto IL_005c;
	}

IL_0017:
	{
		__this->set_U3CU3E1__state_1((-1));
		RandomEyes3D_t6868157C4605BD0225E1B08301F110E72336CE45 * L_2 = __this->get_U3CU3E4__this_2();
		int32_t L_3 = __this->get_shapeIndex_3();
		NullCheck(L_2);
		RandomEyes3D_SetCustomShape_m46A96B9C7709FD6B8790B63AF40F71C00927A4A2(L_2, L_3, /*hidden argument*/NULL);
		float L_4 = __this->get_duration_4();
		WaitForSeconds_t8F9189BE6E467C98C99177038881F8982E0E4013 * L_5 = (WaitForSeconds_t8F9189BE6E467C98C99177038881F8982E0E4013 *)il2cpp_codegen_object_new(WaitForSeconds_t8F9189BE6E467C98C99177038881F8982E0E4013_il2cpp_TypeInfo_var);
		WaitForSeconds__ctor_mD298C4CB9532BBBDE172FC40F3397E30504038D4(L_5, L_4, /*hidden argument*/NULL);
		__this->set_U3CU3E2__current_0(L_5);
		__this->set_U3CU3E1__state_1(1);
		return (bool)1;
	}

IL_0049:
	{
		__this->set_U3CU3E1__state_1((-1));
		RandomEyes3D_t6868157C4605BD0225E1B08301F110E72336CE45 * L_6 = __this->get_U3CU3E4__this_2();
		NullCheck(L_6);
		RandomEyes3D_SetCustomShape_m0B61CBCBC3389E335E869448A557D96549915D36(L_6, (String_t*)NULL, /*hidden argument*/NULL);
	}

IL_005c:
	{
		return (bool)0;
	}
}
// System.Object CrazyMinnow.SALSA.RandomEyes3D/<SetCustomShapeDuration>d__a::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject * U3CSetCustomShapeDurationU3Ed__a_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m29AD9BF27A7FF383D8C75DEA1AD987DCB65A24E5 (U3CSetCustomShapeDurationU3Ed__a_tF3A147C5E712D0581C2B12133986348BFBB67140 * __this, const RuntimeMethod* method)
{
	{
		RuntimeObject * L_0 = __this->get_U3CU3E2__current_0();
		return L_0;
	}
}
// System.Void CrazyMinnow.SALSA.RandomEyes3D/<SetCustomShapeDuration>d__a::System.Collections.IEnumerator.Reset()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CSetCustomShapeDurationU3Ed__a_System_Collections_IEnumerator_Reset_m6AD8EF97A2799112F845813343FD577CA1698CF7 (U3CSetCustomShapeDurationU3Ed__a_tF3A147C5E712D0581C2B12133986348BFBB67140 * __this, const RuntimeMethod* method)
{
	{
		NotSupportedException_tB9D89F0E9470A2C423D239D7C68EE0CFD77F9339 * L_0 = (NotSupportedException_tB9D89F0E9470A2C423D239D7C68EE0CFD77F9339 *)il2cpp_codegen_object_new(((RuntimeClass*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&NotSupportedException_tB9D89F0E9470A2C423D239D7C68EE0CFD77F9339_il2cpp_TypeInfo_var)));
		NotSupportedException__ctor_m3EA81A5B209A87C3ADA47443F2AFFF735E5256EE(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0, ((RuntimeMethod*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&U3CSetCustomShapeDurationU3Ed__a_System_Collections_IEnumerator_Reset_m6AD8EF97A2799112F845813343FD577CA1698CF7_RuntimeMethod_var)));
	}
}
// System.Void CrazyMinnow.SALSA.RandomEyes3D/<SetCustomShapeDuration>d__a::System.IDisposable.Dispose()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CSetCustomShapeDurationU3Ed__a_System_IDisposable_Dispose_m9D8F38C3E7B44E6DE7815CC7D32D11D27EEF8D03 (U3CSetCustomShapeDurationU3Ed__a_tF3A147C5E712D0581C2B12133986348BFBB67140 * __this, const RuntimeMethod* method)
{
	{
		return;
	}
}
// System.Object CrazyMinnow.SALSA.RandomEyes3D/<SetCustomShapeDuration>d__a::System.Collections.IEnumerator.get_Current()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject * U3CSetCustomShapeDurationU3Ed__a_System_Collections_IEnumerator_get_Current_m3FCCF65FB5B696321DFF75279636D1A8666D7DF7 (U3CSetCustomShapeDurationU3Ed__a_tF3A147C5E712D0581C2B12133986348BFBB67140 * __this, const RuntimeMethod* method)
{
	{
		RuntimeObject * L_0 = __this->get_U3CU3E2__current_0();
		return L_0;
	}
}
// System.Void CrazyMinnow.SALSA.RandomEyes3D/<SetCustomShapeDuration>d__a::.ctor(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CSetCustomShapeDurationU3Ed__a__ctor_m22ECCEFDEC5DB0D1080451AE79B9E22EC5A618EB (U3CSetCustomShapeDurationU3Ed__a_tF3A147C5E712D0581C2B12133986348BFBB67140 * __this, int32_t ___U3CU3E1__state0, const RuntimeMethod* method)
{
	{
		Object__ctor_m88880E0413421D13FD95325EDCE231707CE1F405(__this, /*hidden argument*/NULL);
		int32_t L_0 = ___U3CU3E1__state0;
		__this->set_U3CU3E1__state_1(L_0);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Boolean CrazyMinnow.SALSA.RandomEyes3D/<SetCustomShapeOverrideDuration>d__4::MoveNext()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool U3CSetCustomShapeOverrideDurationU3Ed__4_MoveNext_mF967AD900DA0BB3DACEB9E8EC01074D0C512490F (U3CSetCustomShapeOverrideDurationU3Ed__4_tA42191F3DEC3EBAD53A277EAC5C346C81A3BC122 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&WaitForSeconds_t8F9189BE6E467C98C99177038881F8982E0E4013_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	{
		int32_t L_0 = __this->get_U3CU3E1__state_1();
		V_0 = L_0;
		int32_t L_1 = V_0;
		switch (L_1)
		{
			case 0:
			{
				goto IL_0017;
			}
			case 1:
			{
				goto IL_004a;
			}
		}
	}
	{
		goto IL_0063;
	}

IL_0017:
	{
		__this->set_U3CU3E1__state_1((-1));
		RandomEyes3D_t6868157C4605BD0225E1B08301F110E72336CE45 * L_2 = __this->get_U3CU3E4__this_2();
		String_t* L_3 = __this->get_shapeName_3();
		NullCheck(L_2);
		RandomEyes3D_SetCustomShapeOverride_m23761F4B53F0E1A29A36DFFF91801AD211032AC1(L_2, L_3, (bool)1, /*hidden argument*/NULL);
		float L_4 = __this->get_duration_4();
		WaitForSeconds_t8F9189BE6E467C98C99177038881F8982E0E4013 * L_5 = (WaitForSeconds_t8F9189BE6E467C98C99177038881F8982E0E4013 *)il2cpp_codegen_object_new(WaitForSeconds_t8F9189BE6E467C98C99177038881F8982E0E4013_il2cpp_TypeInfo_var);
		WaitForSeconds__ctor_mD298C4CB9532BBBDE172FC40F3397E30504038D4(L_5, L_4, /*hidden argument*/NULL);
		__this->set_U3CU3E2__current_0(L_5);
		__this->set_U3CU3E1__state_1(1);
		return (bool)1;
	}

IL_004a:
	{
		__this->set_U3CU3E1__state_1((-1));
		RandomEyes3D_t6868157C4605BD0225E1B08301F110E72336CE45 * L_6 = __this->get_U3CU3E4__this_2();
		String_t* L_7 = __this->get_shapeName_3();
		NullCheck(L_6);
		RandomEyes3D_SetCustomShapeOverride_m23761F4B53F0E1A29A36DFFF91801AD211032AC1(L_6, L_7, (bool)0, /*hidden argument*/NULL);
	}

IL_0063:
	{
		return (bool)0;
	}
}
// System.Object CrazyMinnow.SALSA.RandomEyes3D/<SetCustomShapeOverrideDuration>d__4::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject * U3CSetCustomShapeOverrideDurationU3Ed__4_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m0DB8C66E0B7DC354D7A4096727A9F719FA17B455 (U3CSetCustomShapeOverrideDurationU3Ed__4_tA42191F3DEC3EBAD53A277EAC5C346C81A3BC122 * __this, const RuntimeMethod* method)
{
	{
		RuntimeObject * L_0 = __this->get_U3CU3E2__current_0();
		return L_0;
	}
}
// System.Void CrazyMinnow.SALSA.RandomEyes3D/<SetCustomShapeOverrideDuration>d__4::System.Collections.IEnumerator.Reset()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CSetCustomShapeOverrideDurationU3Ed__4_System_Collections_IEnumerator_Reset_mAD496E65677155FF44B6B02F4850449E59AD4F75 (U3CSetCustomShapeOverrideDurationU3Ed__4_tA42191F3DEC3EBAD53A277EAC5C346C81A3BC122 * __this, const RuntimeMethod* method)
{
	{
		NotSupportedException_tB9D89F0E9470A2C423D239D7C68EE0CFD77F9339 * L_0 = (NotSupportedException_tB9D89F0E9470A2C423D239D7C68EE0CFD77F9339 *)il2cpp_codegen_object_new(((RuntimeClass*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&NotSupportedException_tB9D89F0E9470A2C423D239D7C68EE0CFD77F9339_il2cpp_TypeInfo_var)));
		NotSupportedException__ctor_m3EA81A5B209A87C3ADA47443F2AFFF735E5256EE(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0, ((RuntimeMethod*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&U3CSetCustomShapeOverrideDurationU3Ed__4_System_Collections_IEnumerator_Reset_mAD496E65677155FF44B6B02F4850449E59AD4F75_RuntimeMethod_var)));
	}
}
// System.Void CrazyMinnow.SALSA.RandomEyes3D/<SetCustomShapeOverrideDuration>d__4::System.IDisposable.Dispose()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CSetCustomShapeOverrideDurationU3Ed__4_System_IDisposable_Dispose_m8201BD769FB409291915C1E2559C9DF0CE067234 (U3CSetCustomShapeOverrideDurationU3Ed__4_tA42191F3DEC3EBAD53A277EAC5C346C81A3BC122 * __this, const RuntimeMethod* method)
{
	{
		return;
	}
}
// System.Object CrazyMinnow.SALSA.RandomEyes3D/<SetCustomShapeOverrideDuration>d__4::System.Collections.IEnumerator.get_Current()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject * U3CSetCustomShapeOverrideDurationU3Ed__4_System_Collections_IEnumerator_get_Current_mD22F300FB60FBC84ACA05B07F1CC27E4ECFFCE7C (U3CSetCustomShapeOverrideDurationU3Ed__4_tA42191F3DEC3EBAD53A277EAC5C346C81A3BC122 * __this, const RuntimeMethod* method)
{
	{
		RuntimeObject * L_0 = __this->get_U3CU3E2__current_0();
		return L_0;
	}
}
// System.Void CrazyMinnow.SALSA.RandomEyes3D/<SetCustomShapeOverrideDuration>d__4::.ctor(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CSetCustomShapeOverrideDurationU3Ed__4__ctor_mB66C5B43F2C15F540F85F1845A71DB41035E6FAD (U3CSetCustomShapeOverrideDurationU3Ed__4_tA42191F3DEC3EBAD53A277EAC5C346C81A3BC122 * __this, int32_t ___U3CU3E1__state0, const RuntimeMethod* method)
{
	{
		Object__ctor_m88880E0413421D13FD95325EDCE231707CE1F405(__this, /*hidden argument*/NULL);
		int32_t L_0 = ___U3CU3E1__state0;
		__this->set_U3CU3E1__state_1(L_0);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Boolean CrazyMinnow.SALSA.RandomEyes3D/<SetCustomShapeOverrideDuration>d__6::MoveNext()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool U3CSetCustomShapeOverrideDurationU3Ed__6_MoveNext_m63E68EDC3982D66FE7182E3E87F877F2DD3E4ABF (U3CSetCustomShapeOverrideDurationU3Ed__6_t3067670AF5E03DD7899B8F7492C6EF59E5B77DB0 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&WaitForSeconds_t8F9189BE6E467C98C99177038881F8982E0E4013_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	{
		int32_t L_0 = __this->get_U3CU3E1__state_1();
		V_0 = L_0;
		int32_t L_1 = V_0;
		switch (L_1)
		{
			case 0:
			{
				goto IL_0017;
			}
			case 1:
			{
				goto IL_004a;
			}
		}
	}
	{
		goto IL_0063;
	}

IL_0017:
	{
		__this->set_U3CU3E1__state_1((-1));
		RandomEyes3D_t6868157C4605BD0225E1B08301F110E72336CE45 * L_2 = __this->get_U3CU3E4__this_2();
		int32_t L_3 = __this->get_shapeIndex_3();
		NullCheck(L_2);
		RandomEyes3D_SetCustomShapeOverride_m98357978832E89B5E5B1FD37F2702911980DCD0A(L_2, L_3, (bool)1, /*hidden argument*/NULL);
		float L_4 = __this->get_duration_4();
		WaitForSeconds_t8F9189BE6E467C98C99177038881F8982E0E4013 * L_5 = (WaitForSeconds_t8F9189BE6E467C98C99177038881F8982E0E4013 *)il2cpp_codegen_object_new(WaitForSeconds_t8F9189BE6E467C98C99177038881F8982E0E4013_il2cpp_TypeInfo_var);
		WaitForSeconds__ctor_mD298C4CB9532BBBDE172FC40F3397E30504038D4(L_5, L_4, /*hidden argument*/NULL);
		__this->set_U3CU3E2__current_0(L_5);
		__this->set_U3CU3E1__state_1(1);
		return (bool)1;
	}

IL_004a:
	{
		__this->set_U3CU3E1__state_1((-1));
		RandomEyes3D_t6868157C4605BD0225E1B08301F110E72336CE45 * L_6 = __this->get_U3CU3E4__this_2();
		int32_t L_7 = __this->get_shapeIndex_3();
		NullCheck(L_6);
		RandomEyes3D_SetCustomShapeOverride_m98357978832E89B5E5B1FD37F2702911980DCD0A(L_6, L_7, (bool)0, /*hidden argument*/NULL);
	}

IL_0063:
	{
		return (bool)0;
	}
}
// System.Object CrazyMinnow.SALSA.RandomEyes3D/<SetCustomShapeOverrideDuration>d__6::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject * U3CSetCustomShapeOverrideDurationU3Ed__6_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mC45AF0F2DFB5CC2AB9A7B2674BE4416D56C54330 (U3CSetCustomShapeOverrideDurationU3Ed__6_t3067670AF5E03DD7899B8F7492C6EF59E5B77DB0 * __this, const RuntimeMethod* method)
{
	{
		RuntimeObject * L_0 = __this->get_U3CU3E2__current_0();
		return L_0;
	}
}
// System.Void CrazyMinnow.SALSA.RandomEyes3D/<SetCustomShapeOverrideDuration>d__6::System.Collections.IEnumerator.Reset()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CSetCustomShapeOverrideDurationU3Ed__6_System_Collections_IEnumerator_Reset_m248F5D80FBFAA6C850D4680977E43D02F8E5F7AC (U3CSetCustomShapeOverrideDurationU3Ed__6_t3067670AF5E03DD7899B8F7492C6EF59E5B77DB0 * __this, const RuntimeMethod* method)
{
	{
		NotSupportedException_tB9D89F0E9470A2C423D239D7C68EE0CFD77F9339 * L_0 = (NotSupportedException_tB9D89F0E9470A2C423D239D7C68EE0CFD77F9339 *)il2cpp_codegen_object_new(((RuntimeClass*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&NotSupportedException_tB9D89F0E9470A2C423D239D7C68EE0CFD77F9339_il2cpp_TypeInfo_var)));
		NotSupportedException__ctor_m3EA81A5B209A87C3ADA47443F2AFFF735E5256EE(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0, ((RuntimeMethod*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&U3CSetCustomShapeOverrideDurationU3Ed__6_System_Collections_IEnumerator_Reset_m248F5D80FBFAA6C850D4680977E43D02F8E5F7AC_RuntimeMethod_var)));
	}
}
// System.Void CrazyMinnow.SALSA.RandomEyes3D/<SetCustomShapeOverrideDuration>d__6::System.IDisposable.Dispose()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CSetCustomShapeOverrideDurationU3Ed__6_System_IDisposable_Dispose_mE23ACB37AE186DEDA95535FB3720E8359D308E07 (U3CSetCustomShapeOverrideDurationU3Ed__6_t3067670AF5E03DD7899B8F7492C6EF59E5B77DB0 * __this, const RuntimeMethod* method)
{
	{
		return;
	}
}
// System.Object CrazyMinnow.SALSA.RandomEyes3D/<SetCustomShapeOverrideDuration>d__6::System.Collections.IEnumerator.get_Current()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject * U3CSetCustomShapeOverrideDurationU3Ed__6_System_Collections_IEnumerator_get_Current_mFD60724EFEB1942BDF96085D0C38219DC9796378 (U3CSetCustomShapeOverrideDurationU3Ed__6_t3067670AF5E03DD7899B8F7492C6EF59E5B77DB0 * __this, const RuntimeMethod* method)
{
	{
		RuntimeObject * L_0 = __this->get_U3CU3E2__current_0();
		return L_0;
	}
}
// System.Void CrazyMinnow.SALSA.RandomEyes3D/<SetCustomShapeOverrideDuration>d__6::.ctor(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CSetCustomShapeOverrideDurationU3Ed__6__ctor_m90B95A85DE3597BE73729C4BD7FF6D9756F8D6EB (U3CSetCustomShapeOverrideDurationU3Ed__6_t3067670AF5E03DD7899B8F7492C6EF59E5B77DB0 * __this, int32_t ___U3CU3E1__state0, const RuntimeMethod* method)
{
	{
		Object__ctor_m88880E0413421D13FD95325EDCE231707CE1F405(__this, /*hidden argument*/NULL);
		int32_t L_0 = ___U3CU3E1__state0;
		__this->set_U3CU3E1__state_1(L_0);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Boolean CrazyMinnow.SALSA.RandomEyes3D/<TargetAffinityUpdate>d__0::MoveNext()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool U3CTargetAffinityUpdateU3Ed__0_MoveNext_m6C8737D2049E5CA630C97961F714DDF5D1F5B9B5 (U3CTargetAffinityUpdateU3Ed__0_tCF892F05CB3FC0D3FC4F3984F53D8C429D011013 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&WaitForSeconds_t8F9189BE6E467C98C99177038881F8982E0E4013_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	{
		int32_t L_0 = __this->get_U3CU3E1__state_1();
		V_0 = L_0;
		int32_t L_1 = V_0;
		switch (L_1)
		{
			case 0:
			{
				goto IL_001a;
			}
			case 1:
			{
				goto IL_01b0;
			}
		}
	}
	{
		goto IL_01ec;
	}

IL_001a:
	{
		__this->set_U3CU3E1__state_1((-1));
	}

IL_0021:
	{
		RandomEyes3D_t6868157C4605BD0225E1B08301F110E72336CE45 * L_2 = __this->get_U3CU3E4__this_2();
		NullCheck(L_2);
		bool L_3 = L_2->get_targetAffinity_29();
		if (L_3)
		{
			goto IL_003a;
		}
	}
	{
		RandomEyes3D_t6868157C4605BD0225E1B08301F110E72336CE45 * L_4 = __this->get_U3CU3E4__this_2();
		NullCheck(L_4);
		L_4->set_hasAffinity_76((bool)0);
	}

IL_003a:
	{
		RandomEyes3D_t6868157C4605BD0225E1B08301F110E72336CE45 * L_5 = __this->get_U3CU3E4__this_2();
		NullCheck(L_5);
		bool L_6 = L_5->get_targetAffinity_29();
		if (!L_6)
		{
			goto IL_00a7;
		}
	}
	{
		RandomEyes3D_t6868157C4605BD0225E1B08301F110E72336CE45 * L_7 = __this->get_U3CU3E4__this_2();
		NullCheck(L_7);
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_8 = L_7->get_lookTarget_27();
		IL2CPP_RUNTIME_CLASS_INIT(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		bool L_9;
		L_9 = Object_op_Implicit_mC8214E4F028CC2F036CC82BDB81D102A02893499(L_8, /*hidden argument*/NULL);
		if (!L_9)
		{
			goto IL_00a7;
		}
	}
	{
		float L_10;
		L_10 = Random_get_value_m9AEBC7DF0BB6C57C928B0798349A7D3C0B3FB872(/*hidden argument*/NULL);
		RandomEyes3D_t6868157C4605BD0225E1B08301F110E72336CE45 * L_11 = __this->get_U3CU3E4__this_2();
		NullCheck(L_11);
		float L_12 = L_11->get_targetAffinityPercentage_30();
		if ((!(((float)L_10) <= ((float)L_12))))
		{
			goto IL_008f;
		}
	}
	{
		RandomEyes3D_t6868157C4605BD0225E1B08301F110E72336CE45 * L_13 = __this->get_U3CU3E4__this_2();
		NullCheck(L_13);
		L_13->set_hasAffinity_76((bool)1);
		RandomEyes3D_t6868157C4605BD0225E1B08301F110E72336CE45 * L_14 = __this->get_U3CU3E4__this_2();
		RandomEyes3D_t6868157C4605BD0225E1B08301F110E72336CE45 * L_15 = __this->get_U3CU3E4__this_2();
		NullCheck(L_15);
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_16 = L_15->get_lookTarget_27();
		NullCheck(L_14);
		RandomEyes3D_SetLookTargetTracking_m8DC063699D644B556133DD4013E7BB99C95572EC(L_14, L_16, /*hidden argument*/NULL);
		goto IL_00a7;
	}

IL_008f:
	{
		RandomEyes3D_t6868157C4605BD0225E1B08301F110E72336CE45 * L_17 = __this->get_U3CU3E4__this_2();
		NullCheck(L_17);
		L_17->set_hasAffinity_76((bool)0);
		RandomEyes3D_t6868157C4605BD0225E1B08301F110E72336CE45 * L_18 = __this->get_U3CU3E4__this_2();
		NullCheck(L_18);
		RandomEyes3D_SetLookTargetTracking_m8DC063699D644B556133DD4013E7BB99C95572EC(L_18, (GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 *)NULL, /*hidden argument*/NULL);
	}

IL_00a7:
	{
		RandomEyes3D_t6868157C4605BD0225E1B08301F110E72336CE45 * L_19 = __this->get_U3CU3E4__this_2();
		NullCheck(L_19);
		bool L_20 = L_19->get_hasAffinity_76();
		if (!L_20)
		{
			goto IL_0191;
		}
	}
	{
		RandomEyes3D_t6868157C4605BD0225E1B08301F110E72336CE45 * L_21 = __this->get_U3CU3E4__this_2();
		NullCheck(L_21);
		bool L_22 = L_21->get_trackBehind_32();
		if (!L_22)
		{
			goto IL_00df;
		}
	}
	{
		RandomEyes3D_t6868157C4605BD0225E1B08301F110E72336CE45 * L_23 = __this->get_U3CU3E4__this_2();
		RandomEyes3D_t6868157C4605BD0225E1B08301F110E72336CE45 * L_24 = __this->get_U3CU3E4__this_2();
		NullCheck(L_24);
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_25 = L_24->get_lookTargetTracking_28();
		NullCheck(L_23);
		RandomEyes3D_SetLookTargetTracking_m8DC063699D644B556133DD4013E7BB99C95572EC(L_23, L_25, /*hidden argument*/NULL);
		goto IL_0191;
	}

IL_00df:
	{
		RandomEyes3D_t6868157C4605BD0225E1B08301F110E72336CE45 * L_26 = __this->get_U3CU3E4__this_2();
		NullCheck(L_26);
		bool L_27 = L_26->get_invertTracking_31();
		if (L_27)
		{
			goto IL_0103;
		}
	}
	{
		RandomEyes3D_t6868157C4605BD0225E1B08301F110E72336CE45 * L_28 = __this->get_U3CU3E4__this_2();
		NullCheck(L_28);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * L_29 = L_28->get_address_of_lookDirection_68();
		float L_30 = L_29->get_z_4();
		if ((((float)L_30) > ((float)(0.0f))))
		{
			goto IL_0127;
		}
	}

IL_0103:
	{
		RandomEyes3D_t6868157C4605BD0225E1B08301F110E72336CE45 * L_31 = __this->get_U3CU3E4__this_2();
		NullCheck(L_31);
		bool L_32 = L_31->get_invertTracking_31();
		if (!L_32)
		{
			goto IL_013d;
		}
	}
	{
		RandomEyes3D_t6868157C4605BD0225E1B08301F110E72336CE45 * L_33 = __this->get_U3CU3E4__this_2();
		NullCheck(L_33);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * L_34 = L_33->get_address_of_lookDirection_68();
		float L_35 = L_34->get_z_4();
		if ((!(((float)L_35) < ((float)(0.0f)))))
		{
			goto IL_013d;
		}
	}

IL_0127:
	{
		RandomEyes3D_t6868157C4605BD0225E1B08301F110E72336CE45 * L_36 = __this->get_U3CU3E4__this_2();
		RandomEyes3D_t6868157C4605BD0225E1B08301F110E72336CE45 * L_37 = __this->get_U3CU3E4__this_2();
		NullCheck(L_37);
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_38 = L_37->get_lookTargetTracking_28();
		NullCheck(L_36);
		RandomEyes3D_SetLookTargetTracking_m8DC063699D644B556133DD4013E7BB99C95572EC(L_36, L_38, /*hidden argument*/NULL);
	}

IL_013d:
	{
		RandomEyes3D_t6868157C4605BD0225E1B08301F110E72336CE45 * L_39 = __this->get_U3CU3E4__this_2();
		NullCheck(L_39);
		bool L_40 = L_39->get_invertTracking_31();
		if (L_40)
		{
			goto IL_0161;
		}
	}
	{
		RandomEyes3D_t6868157C4605BD0225E1B08301F110E72336CE45 * L_41 = __this->get_U3CU3E4__this_2();
		NullCheck(L_41);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * L_42 = L_41->get_address_of_lookDirection_68();
		float L_43 = L_42->get_z_4();
		if ((((float)L_43) < ((float)(0.0f))))
		{
			goto IL_0185;
		}
	}

IL_0161:
	{
		RandomEyes3D_t6868157C4605BD0225E1B08301F110E72336CE45 * L_44 = __this->get_U3CU3E4__this_2();
		NullCheck(L_44);
		bool L_45 = L_44->get_invertTracking_31();
		if (!L_45)
		{
			goto IL_0191;
		}
	}
	{
		RandomEyes3D_t6868157C4605BD0225E1B08301F110E72336CE45 * L_46 = __this->get_U3CU3E4__this_2();
		NullCheck(L_46);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * L_47 = L_46->get_address_of_lookDirection_68();
		float L_48 = L_47->get_z_4();
		if ((!(((float)L_48) > ((float)(0.0f)))))
		{
			goto IL_0191;
		}
	}

IL_0185:
	{
		RandomEyes3D_t6868157C4605BD0225E1B08301F110E72336CE45 * L_49 = __this->get_U3CU3E4__this_2();
		NullCheck(L_49);
		RandomEyes3D_SetLookTargetTracking_m8DC063699D644B556133DD4013E7BB99C95572EC(L_49, (GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 *)NULL, /*hidden argument*/NULL);
	}

IL_0191:
	{
		RandomEyes3D_t6868157C4605BD0225E1B08301F110E72336CE45 * L_50 = __this->get_U3CU3E4__this_2();
		NullCheck(L_50);
		float L_51 = L_50->get_targetAffinityTimer_75();
		WaitForSeconds_t8F9189BE6E467C98C99177038881F8982E0E4013 * L_52 = (WaitForSeconds_t8F9189BE6E467C98C99177038881F8982E0E4013 *)il2cpp_codegen_object_new(WaitForSeconds_t8F9189BE6E467C98C99177038881F8982E0E4013_il2cpp_TypeInfo_var);
		WaitForSeconds__ctor_mD298C4CB9532BBBDE172FC40F3397E30504038D4(L_52, L_51, /*hidden argument*/NULL);
		__this->set_U3CU3E2__current_0(L_52);
		__this->set_U3CU3E1__state_1(1);
		return (bool)1;
	}

IL_01b0:
	{
		__this->set_U3CU3E1__state_1((-1));
		RandomEyes3D_t6868157C4605BD0225E1B08301F110E72336CE45 * L_53 = __this->get_U3CU3E4__this_2();
		RandomEyes3D_t6868157C4605BD0225E1B08301F110E72336CE45 * L_54 = __this->get_U3CU3E4__this_2();
		NullCheck(L_54);
		Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 * L_55 = L_54->get_address_of_targetAffinityTimerRange_74();
		float L_56 = L_55->get_x_0();
		RandomEyes3D_t6868157C4605BD0225E1B08301F110E72336CE45 * L_57 = __this->get_U3CU3E4__this_2();
		NullCheck(L_57);
		Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 * L_58 = L_57->get_address_of_targetAffinityTimerRange_74();
		float L_59 = L_58->get_y_1();
		float L_60;
		L_60 = Random_Range_mC15372D42A9ABDCAC3DE82E114D60A40C9C311D2(L_56, L_59, /*hidden argument*/NULL);
		NullCheck(L_53);
		L_53->set_targetAffinityTimer_75(L_60);
		goto IL_0021;
	}

IL_01ec:
	{
		return (bool)0;
	}
}
// System.Object CrazyMinnow.SALSA.RandomEyes3D/<TargetAffinityUpdate>d__0::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject * U3CTargetAffinityUpdateU3Ed__0_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m2327362FC616DD432555C7E25749B63137D99477 (U3CTargetAffinityUpdateU3Ed__0_tCF892F05CB3FC0D3FC4F3984F53D8C429D011013 * __this, const RuntimeMethod* method)
{
	{
		RuntimeObject * L_0 = __this->get_U3CU3E2__current_0();
		return L_0;
	}
}
// System.Void CrazyMinnow.SALSA.RandomEyes3D/<TargetAffinityUpdate>d__0::System.Collections.IEnumerator.Reset()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CTargetAffinityUpdateU3Ed__0_System_Collections_IEnumerator_Reset_mBAE456A3C0C96A0C8B70B0D32F3D345A0C07CA1E (U3CTargetAffinityUpdateU3Ed__0_tCF892F05CB3FC0D3FC4F3984F53D8C429D011013 * __this, const RuntimeMethod* method)
{
	{
		NotSupportedException_tB9D89F0E9470A2C423D239D7C68EE0CFD77F9339 * L_0 = (NotSupportedException_tB9D89F0E9470A2C423D239D7C68EE0CFD77F9339 *)il2cpp_codegen_object_new(((RuntimeClass*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&NotSupportedException_tB9D89F0E9470A2C423D239D7C68EE0CFD77F9339_il2cpp_TypeInfo_var)));
		NotSupportedException__ctor_m3EA81A5B209A87C3ADA47443F2AFFF735E5256EE(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0, ((RuntimeMethod*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&U3CTargetAffinityUpdateU3Ed__0_System_Collections_IEnumerator_Reset_mBAE456A3C0C96A0C8B70B0D32F3D345A0C07CA1E_RuntimeMethod_var)));
	}
}
// System.Void CrazyMinnow.SALSA.RandomEyes3D/<TargetAffinityUpdate>d__0::System.IDisposable.Dispose()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CTargetAffinityUpdateU3Ed__0_System_IDisposable_Dispose_m6DB637D4B9B6B5D7278321DA86C71BB5BBE1951F (U3CTargetAffinityUpdateU3Ed__0_tCF892F05CB3FC0D3FC4F3984F53D8C429D011013 * __this, const RuntimeMethod* method)
{
	{
		return;
	}
}
// System.Object CrazyMinnow.SALSA.RandomEyes3D/<TargetAffinityUpdate>d__0::System.Collections.IEnumerator.get_Current()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject * U3CTargetAffinityUpdateU3Ed__0_System_Collections_IEnumerator_get_Current_m348B9921D9DFEA1779B61D9EA42F31C06F6A00BB (U3CTargetAffinityUpdateU3Ed__0_tCF892F05CB3FC0D3FC4F3984F53D8C429D011013 * __this, const RuntimeMethod* method)
{
	{
		RuntimeObject * L_0 = __this->get_U3CU3E2__current_0();
		return L_0;
	}
}
// System.Void CrazyMinnow.SALSA.RandomEyes3D/<TargetAffinityUpdate>d__0::.ctor(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CTargetAffinityUpdateU3Ed__0__ctor_mCAFDF47555144A310E4F743A29A8C68AAAA8FDFB (U3CTargetAffinityUpdateU3Ed__0_tCF892F05CB3FC0D3FC4F3984F53D8C429D011013 * __this, int32_t ___U3CU3E1__state0, const RuntimeMethod* method)
{
	{
		Object__ctor_m88880E0413421D13FD95325EDCE231707CE1F405(__this, /*hidden argument*/NULL);
		int32_t L_0 = ___U3CU3E1__state0;
		__this->set_U3CU3E1__state_1(L_0);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Boolean CrazyMinnow.SALSA.Salsa2D/<UpdateSample>d__0::MoveNext()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool U3CUpdateSampleU3Ed__0_MoveNext_m96AE4867D7BA9B7FB3F37F0FF06599F22D0780EB (U3CUpdateSampleU3Ed__0_t610463941DB34736C5EE20E7CA02876D017BE95F * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	{
		int32_t L_0 = __this->get_U3CU3E1__state_1();
		V_1 = L_0;
		int32_t L_1 = V_1;
		switch (L_1)
		{
			case 0:
			{
				goto IL_001a;
			}
			case 1:
			{
				goto IL_0174;
			}
		}
	}
	{
		goto IL_018b;
	}

IL_001a:
	{
		__this->set_U3CU3E1__state_1((-1));
	}

IL_0021:
	{
		__this->set_U3CaddedValsU3E5__1_3((0.0f));
		Salsa2D_t54C8B8FFE3B8149C565BD2A69D53629E65CFFB62 * L_2 = __this->get_U3CU3E4__this_2();
		NullCheck(L_2);
		AudioSource_tC4BF65AF8CDCAA63724BB3CA59A7A29249269E6B * L_3 = L_2->get_audioSrc_9();
		IL2CPP_RUNTIME_CLASS_INIT(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		bool L_4;
		L_4 = Object_op_Implicit_mC8214E4F028CC2F036CC82BDB81D102A02893499(L_3, /*hidden argument*/NULL);
		if (!L_4)
		{
			goto IL_008d;
		}
	}
	{
		Salsa2D_t54C8B8FFE3B8149C565BD2A69D53629E65CFFB62 * L_5 = __this->get_U3CU3E4__this_2();
		NullCheck(L_5);
		AudioSource_tC4BF65AF8CDCAA63724BB3CA59A7A29249269E6B * L_6 = L_5->get_audioSrc_9();
		NullCheck(L_6);
		bool L_7;
		L_7 = AudioSource_get_isPlaying_mEA69477C77D542971F7B454946EF25DFBE0AF6A8(L_6, /*hidden argument*/NULL);
		if (!L_7)
		{
			goto IL_006f;
		}
	}
	{
		Salsa2D_t54C8B8FFE3B8149C565BD2A69D53629E65CFFB62 * L_8 = __this->get_U3CU3E4__this_2();
		NullCheck(L_8);
		AudioSource_tC4BF65AF8CDCAA63724BB3CA59A7A29249269E6B * L_9 = L_8->get_audioSrc_9();
		Salsa2D_t54C8B8FFE3B8149C565BD2A69D53629E65CFFB62 * L_10 = __this->get_U3CU3E4__this_2();
		NullCheck(L_10);
		SingleU5BU5D_t47E8DBF5B597C122478D1FFBD9DD57399A0650FA* L_11 = L_10->get_sample_31();
		NullCheck(L_9);
		AudioSource_GetSpectrumData_m534E9BDB8C2924A118A711A0F741581DCEB27266(L_9, L_11, 0, 5, /*hidden argument*/NULL);
		goto IL_008d;
	}

IL_006f:
	{
		Salsa2D_t54C8B8FFE3B8149C565BD2A69D53629E65CFFB62 * L_12 = __this->get_U3CU3E4__this_2();
		NullCheck(L_12);
		SingleU5BU5D_t47E8DBF5B597C122478D1FFBD9DD57399A0650FA* L_13 = L_12->get_sample_31();
		Salsa2D_t54C8B8FFE3B8149C565BD2A69D53629E65CFFB62 * L_14 = __this->get_U3CU3E4__this_2();
		NullCheck(L_14);
		SingleU5BU5D_t47E8DBF5B597C122478D1FFBD9DD57399A0650FA* L_15 = L_14->get_sample_31();
		NullCheck(L_15);
		Array_Clear_mEB42D172C5E0825D340F6209F28578BDDDDCE34F((RuntimeArray *)(RuntimeArray *)L_13, 0, ((int32_t)((int32_t)(((RuntimeArray*)L_15)->max_length))), /*hidden argument*/NULL);
	}

IL_008d:
	{
		V_0 = 0;
		goto IL_00af;
	}

IL_0091:
	{
		float L_16 = __this->get_U3CaddedValsU3E5__1_3();
		Salsa2D_t54C8B8FFE3B8149C565BD2A69D53629E65CFFB62 * L_17 = __this->get_U3CU3E4__this_2();
		NullCheck(L_17);
		SingleU5BU5D_t47E8DBF5B597C122478D1FFBD9DD57399A0650FA* L_18 = L_17->get_sample_31();
		int32_t L_19 = V_0;
		NullCheck(L_18);
		int32_t L_20 = L_19;
		float L_21 = (L_18)->GetAt(static_cast<il2cpp_array_size_t>(L_20));
		__this->set_U3CaddedValsU3E5__1_3(((float)il2cpp_codegen_add((float)L_16, (float)L_21)));
		int32_t L_22 = V_0;
		V_0 = ((int32_t)il2cpp_codegen_add((int32_t)L_22, (int32_t)1));
	}

IL_00af:
	{
		int32_t L_23 = V_0;
		Salsa2D_t54C8B8FFE3B8149C565BD2A69D53629E65CFFB62 * L_24 = __this->get_U3CU3E4__this_2();
		NullCheck(L_24);
		SingleU5BU5D_t47E8DBF5B597C122478D1FFBD9DD57399A0650FA* L_25 = L_24->get_sample_31();
		NullCheck(L_25);
		if ((((int32_t)L_23) < ((int32_t)((int32_t)((int32_t)(((RuntimeArray*)L_25)->max_length))))))
		{
			goto IL_0091;
		}
	}
	{
		Salsa2D_t54C8B8FFE3B8149C565BD2A69D53629E65CFFB62 * L_26 = __this->get_U3CU3E4__this_2();
		NullCheck(L_26);
		AudioSource_tC4BF65AF8CDCAA63724BB3CA59A7A29249269E6B * L_27 = L_26->get_audioSrc_9();
		IL2CPP_RUNTIME_CLASS_INIT(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		bool L_28;
		L_28 = Object_op_Implicit_mC8214E4F028CC2F036CC82BDB81D102A02893499(L_27, /*hidden argument*/NULL);
		if (!L_28)
		{
			goto IL_011f;
		}
	}
	{
		Salsa2D_t54C8B8FFE3B8149C565BD2A69D53629E65CFFB62 * L_29 = __this->get_U3CU3E4__this_2();
		NullCheck(L_29);
		AudioSource_tC4BF65AF8CDCAA63724BB3CA59A7A29249269E6B * L_30 = L_29->get_audioSrc_9();
		NullCheck(L_30);
		bool L_31;
		L_31 = AudioSource_get_isPlaying_mEA69477C77D542971F7B454946EF25DFBE0AF6A8(L_30, /*hidden argument*/NULL);
		if (L_31)
		{
			goto IL_0101;
		}
	}
	{
		Salsa2D_t54C8B8FFE3B8149C565BD2A69D53629E65CFFB62 * L_32 = __this->get_U3CU3E4__this_2();
		NullCheck(L_32);
		float L_33 = L_32->get_average_26();
		if ((!(((float)L_33) == ((float)(0.0f)))))
		{
			goto IL_0101;
		}
	}
	{
		Salsa2D_t54C8B8FFE3B8149C565BD2A69D53629E65CFFB62 * L_34 = __this->get_U3CU3E4__this_2();
		NullCheck(L_34);
		L_34->set_writeAverage_37((bool)0);
	}

IL_0101:
	{
		Salsa2D_t54C8B8FFE3B8149C565BD2A69D53629E65CFFB62 * L_35 = __this->get_U3CU3E4__this_2();
		NullCheck(L_35);
		AudioSource_tC4BF65AF8CDCAA63724BB3CA59A7A29249269E6B * L_36 = L_35->get_audioSrc_9();
		NullCheck(L_36);
		bool L_37;
		L_37 = AudioSource_get_isPlaying_mEA69477C77D542971F7B454946EF25DFBE0AF6A8(L_36, /*hidden argument*/NULL);
		if (!L_37)
		{
			goto IL_011f;
		}
	}
	{
		Salsa2D_t54C8B8FFE3B8149C565BD2A69D53629E65CFFB62 * L_38 = __this->get_U3CU3E4__this_2();
		NullCheck(L_38);
		L_38->set_writeAverage_37((bool)1);
	}

IL_011f:
	{
		Salsa2D_t54C8B8FFE3B8149C565BD2A69D53629E65CFFB62 * L_39 = __this->get_U3CU3E4__this_2();
		NullCheck(L_39);
		bool L_40 = L_39->get_writeAverage_37();
		if (!L_40)
		{
			goto IL_015a;
		}
	}
	{
		Salsa2D_t54C8B8FFE3B8149C565BD2A69D53629E65CFFB62 * L_41 = __this->get_U3CU3E4__this_2();
		NullCheck(L_41);
		L_41->set_average_26((0.0f));
		Salsa2D_t54C8B8FFE3B8149C565BD2A69D53629E65CFFB62 * L_42 = __this->get_U3CU3E4__this_2();
		float L_43 = __this->get_U3CaddedValsU3E5__1_3();
		Salsa2D_t54C8B8FFE3B8149C565BD2A69D53629E65CFFB62 * L_44 = __this->get_U3CU3E4__this_2();
		NullCheck(L_44);
		int32_t L_45 = L_44->get_sampleSize_36();
		NullCheck(L_42);
		L_42->set_average_26(((float)((float)L_43/(float)((float)((float)L_45)))));
	}

IL_015a:
	{
		Salsa2D_t54C8B8FFE3B8149C565BD2A69D53629E65CFFB62 * L_46 = __this->get_U3CU3E4__this_2();
		NullCheck(L_46);
		WaitForSeconds_t8F9189BE6E467C98C99177038881F8982E0E4013 * L_47 = L_46->get_updateSampleDelay_29();
		__this->set_U3CU3E2__current_0(L_47);
		__this->set_U3CU3E1__state_1(1);
		return (bool)1;
	}

IL_0174:
	{
		__this->set_U3CU3E1__state_1((-1));
		Salsa2D_t54C8B8FFE3B8149C565BD2A69D53629E65CFFB62 * L_48 = __this->get_U3CU3E4__this_2();
		NullCheck(L_48);
		Salsa2D_UpdateShape_m81111EF6B2731082FA420939A62870B3B59C3AEF(L_48, /*hidden argument*/NULL);
		goto IL_0021;
	}

IL_018b:
	{
		return (bool)0;
	}
}
// System.Object CrazyMinnow.SALSA.Salsa2D/<UpdateSample>d__0::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject * U3CUpdateSampleU3Ed__0_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m1DE327FA8E703A2453E6E02DE2D379EF29B0353A (U3CUpdateSampleU3Ed__0_t610463941DB34736C5EE20E7CA02876D017BE95F * __this, const RuntimeMethod* method)
{
	{
		RuntimeObject * L_0 = __this->get_U3CU3E2__current_0();
		return L_0;
	}
}
// System.Void CrazyMinnow.SALSA.Salsa2D/<UpdateSample>d__0::System.Collections.IEnumerator.Reset()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CUpdateSampleU3Ed__0_System_Collections_IEnumerator_Reset_m721FCB06919D945E4656B278C22DB71EBAD5D67B (U3CUpdateSampleU3Ed__0_t610463941DB34736C5EE20E7CA02876D017BE95F * __this, const RuntimeMethod* method)
{
	{
		NotSupportedException_tB9D89F0E9470A2C423D239D7C68EE0CFD77F9339 * L_0 = (NotSupportedException_tB9D89F0E9470A2C423D239D7C68EE0CFD77F9339 *)il2cpp_codegen_object_new(((RuntimeClass*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&NotSupportedException_tB9D89F0E9470A2C423D239D7C68EE0CFD77F9339_il2cpp_TypeInfo_var)));
		NotSupportedException__ctor_m3EA81A5B209A87C3ADA47443F2AFFF735E5256EE(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0, ((RuntimeMethod*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&U3CUpdateSampleU3Ed__0_System_Collections_IEnumerator_Reset_m721FCB06919D945E4656B278C22DB71EBAD5D67B_RuntimeMethod_var)));
	}
}
// System.Void CrazyMinnow.SALSA.Salsa2D/<UpdateSample>d__0::System.IDisposable.Dispose()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CUpdateSampleU3Ed__0_System_IDisposable_Dispose_m279F715E7CC834B20BE21E737211C44321A8B7DF (U3CUpdateSampleU3Ed__0_t610463941DB34736C5EE20E7CA02876D017BE95F * __this, const RuntimeMethod* method)
{
	int32_t V_0 = 0;
	{
		return;
	}
}
// System.Object CrazyMinnow.SALSA.Salsa2D/<UpdateSample>d__0::System.Collections.IEnumerator.get_Current()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject * U3CUpdateSampleU3Ed__0_System_Collections_IEnumerator_get_Current_mB214E3D8FF3AD5D7552BD9302AB3D5D5B40C79CB (U3CUpdateSampleU3Ed__0_t610463941DB34736C5EE20E7CA02876D017BE95F * __this, const RuntimeMethod* method)
{
	{
		RuntimeObject * L_0 = __this->get_U3CU3E2__current_0();
		return L_0;
	}
}
// System.Void CrazyMinnow.SALSA.Salsa2D/<UpdateSample>d__0::.ctor(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CUpdateSampleU3Ed__0__ctor_m455E0D010D62E290992788EA19E42269D2B13A77 (U3CUpdateSampleU3Ed__0_t610463941DB34736C5EE20E7CA02876D017BE95F * __this, int32_t ___U3CU3E1__state0, const RuntimeMethod* method)
{
	{
		Object__ctor_m88880E0413421D13FD95325EDCE231707CE1F405(__this, /*hidden argument*/NULL);
		int32_t L_0 = ___U3CU3E1__state0;
		__this->set_U3CU3E1__state_1(L_0);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Boolean CrazyMinnow.SALSA.Salsa3D/<UpdateSample>d__0::MoveNext()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool U3CUpdateSampleU3Ed__0_MoveNext_m07D140947B25F0D0BCB80CA6BFD59BD28A7F3F30 (U3CUpdateSampleU3Ed__0_tB4DEFA6499D020E6EC2C7109DBED4C19296282D3 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	{
		int32_t L_0 = __this->get_U3CU3E1__state_1();
		V_1 = L_0;
		int32_t L_1 = V_1;
		switch (L_1)
		{
			case 0:
			{
				goto IL_001a;
			}
			case 1:
			{
				goto IL_0174;
			}
		}
	}
	{
		goto IL_0180;
	}

IL_001a:
	{
		__this->set_U3CU3E1__state_1((-1));
	}

IL_0021:
	{
		__this->set_U3CaddedValsU3E5__1_3((0.0f));
		Salsa3D_tACF83D529139263662FEAAC8F5B53EC43B140C9C * L_2 = __this->get_U3CU3E4__this_2();
		NullCheck(L_2);
		AudioSource_tC4BF65AF8CDCAA63724BB3CA59A7A29249269E6B * L_3 = L_2->get_audioSrc_8();
		IL2CPP_RUNTIME_CLASS_INIT(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		bool L_4;
		L_4 = Object_op_Implicit_mC8214E4F028CC2F036CC82BDB81D102A02893499(L_3, /*hidden argument*/NULL);
		if (!L_4)
		{
			goto IL_008d;
		}
	}
	{
		Salsa3D_tACF83D529139263662FEAAC8F5B53EC43B140C9C * L_5 = __this->get_U3CU3E4__this_2();
		NullCheck(L_5);
		AudioSource_tC4BF65AF8CDCAA63724BB3CA59A7A29249269E6B * L_6 = L_5->get_audioSrc_8();
		NullCheck(L_6);
		bool L_7;
		L_7 = AudioSource_get_isPlaying_mEA69477C77D542971F7B454946EF25DFBE0AF6A8(L_6, /*hidden argument*/NULL);
		if (!L_7)
		{
			goto IL_006f;
		}
	}
	{
		Salsa3D_tACF83D529139263662FEAAC8F5B53EC43B140C9C * L_8 = __this->get_U3CU3E4__this_2();
		NullCheck(L_8);
		AudioSource_tC4BF65AF8CDCAA63724BB3CA59A7A29249269E6B * L_9 = L_8->get_audioSrc_8();
		Salsa3D_tACF83D529139263662FEAAC8F5B53EC43B140C9C * L_10 = __this->get_U3CU3E4__this_2();
		NullCheck(L_10);
		SingleU5BU5D_t47E8DBF5B597C122478D1FFBD9DD57399A0650FA* L_11 = L_10->get_sample_33();
		NullCheck(L_9);
		AudioSource_GetSpectrumData_m534E9BDB8C2924A118A711A0F741581DCEB27266(L_9, L_11, 0, 5, /*hidden argument*/NULL);
		goto IL_008d;
	}

IL_006f:
	{
		Salsa3D_tACF83D529139263662FEAAC8F5B53EC43B140C9C * L_12 = __this->get_U3CU3E4__this_2();
		NullCheck(L_12);
		SingleU5BU5D_t47E8DBF5B597C122478D1FFBD9DD57399A0650FA* L_13 = L_12->get_sample_33();
		Salsa3D_tACF83D529139263662FEAAC8F5B53EC43B140C9C * L_14 = __this->get_U3CU3E4__this_2();
		NullCheck(L_14);
		SingleU5BU5D_t47E8DBF5B597C122478D1FFBD9DD57399A0650FA* L_15 = L_14->get_sample_33();
		NullCheck(L_15);
		Array_Clear_mEB42D172C5E0825D340F6209F28578BDDDDCE34F((RuntimeArray *)(RuntimeArray *)L_13, 0, ((int32_t)((int32_t)(((RuntimeArray*)L_15)->max_length))), /*hidden argument*/NULL);
	}

IL_008d:
	{
		V_0 = 0;
		goto IL_00af;
	}

IL_0091:
	{
		float L_16 = __this->get_U3CaddedValsU3E5__1_3();
		Salsa3D_tACF83D529139263662FEAAC8F5B53EC43B140C9C * L_17 = __this->get_U3CU3E4__this_2();
		NullCheck(L_17);
		SingleU5BU5D_t47E8DBF5B597C122478D1FFBD9DD57399A0650FA* L_18 = L_17->get_sample_33();
		int32_t L_19 = V_0;
		NullCheck(L_18);
		int32_t L_20 = L_19;
		float L_21 = (L_18)->GetAt(static_cast<il2cpp_array_size_t>(L_20));
		__this->set_U3CaddedValsU3E5__1_3(((float)il2cpp_codegen_add((float)L_16, (float)L_21)));
		int32_t L_22 = V_0;
		V_0 = ((int32_t)il2cpp_codegen_add((int32_t)L_22, (int32_t)1));
	}

IL_00af:
	{
		int32_t L_23 = V_0;
		Salsa3D_tACF83D529139263662FEAAC8F5B53EC43B140C9C * L_24 = __this->get_U3CU3E4__this_2();
		NullCheck(L_24);
		SingleU5BU5D_t47E8DBF5B597C122478D1FFBD9DD57399A0650FA* L_25 = L_24->get_sample_33();
		NullCheck(L_25);
		if ((((int32_t)L_23) < ((int32_t)((int32_t)((int32_t)(((RuntimeArray*)L_25)->max_length))))))
		{
			goto IL_0091;
		}
	}
	{
		Salsa3D_tACF83D529139263662FEAAC8F5B53EC43B140C9C * L_26 = __this->get_U3CU3E4__this_2();
		NullCheck(L_26);
		AudioSource_tC4BF65AF8CDCAA63724BB3CA59A7A29249269E6B * L_27 = L_26->get_audioSrc_8();
		IL2CPP_RUNTIME_CLASS_INIT(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		bool L_28;
		L_28 = Object_op_Implicit_mC8214E4F028CC2F036CC82BDB81D102A02893499(L_27, /*hidden argument*/NULL);
		if (!L_28)
		{
			goto IL_011f;
		}
	}
	{
		Salsa3D_tACF83D529139263662FEAAC8F5B53EC43B140C9C * L_29 = __this->get_U3CU3E4__this_2();
		NullCheck(L_29);
		AudioSource_tC4BF65AF8CDCAA63724BB3CA59A7A29249269E6B * L_30 = L_29->get_audioSrc_8();
		NullCheck(L_30);
		bool L_31;
		L_31 = AudioSource_get_isPlaying_mEA69477C77D542971F7B454946EF25DFBE0AF6A8(L_30, /*hidden argument*/NULL);
		if (L_31)
		{
			goto IL_0101;
		}
	}
	{
		Salsa3D_tACF83D529139263662FEAAC8F5B53EC43B140C9C * L_32 = __this->get_U3CU3E4__this_2();
		NullCheck(L_32);
		float L_33 = L_32->get_average_27();
		if ((!(((float)L_33) == ((float)(0.0f)))))
		{
			goto IL_0101;
		}
	}
	{
		Salsa3D_tACF83D529139263662FEAAC8F5B53EC43B140C9C * L_34 = __this->get_U3CU3E4__this_2();
		NullCheck(L_34);
		L_34->set_writeAverage_38((bool)0);
	}

IL_0101:
	{
		Salsa3D_tACF83D529139263662FEAAC8F5B53EC43B140C9C * L_35 = __this->get_U3CU3E4__this_2();
		NullCheck(L_35);
		AudioSource_tC4BF65AF8CDCAA63724BB3CA59A7A29249269E6B * L_36 = L_35->get_audioSrc_8();
		NullCheck(L_36);
		bool L_37;
		L_37 = AudioSource_get_isPlaying_mEA69477C77D542971F7B454946EF25DFBE0AF6A8(L_36, /*hidden argument*/NULL);
		if (!L_37)
		{
			goto IL_011f;
		}
	}
	{
		Salsa3D_tACF83D529139263662FEAAC8F5B53EC43B140C9C * L_38 = __this->get_U3CU3E4__this_2();
		NullCheck(L_38);
		L_38->set_writeAverage_38((bool)1);
	}

IL_011f:
	{
		Salsa3D_tACF83D529139263662FEAAC8F5B53EC43B140C9C * L_39 = __this->get_U3CU3E4__this_2();
		NullCheck(L_39);
		bool L_40 = L_39->get_writeAverage_38();
		if (!L_40)
		{
			goto IL_015a;
		}
	}
	{
		Salsa3D_tACF83D529139263662FEAAC8F5B53EC43B140C9C * L_41 = __this->get_U3CU3E4__this_2();
		NullCheck(L_41);
		L_41->set_average_27((0.0f));
		Salsa3D_tACF83D529139263662FEAAC8F5B53EC43B140C9C * L_42 = __this->get_U3CU3E4__this_2();
		float L_43 = __this->get_U3CaddedValsU3E5__1_3();
		Salsa3D_tACF83D529139263662FEAAC8F5B53EC43B140C9C * L_44 = __this->get_U3CU3E4__this_2();
		NullCheck(L_44);
		int32_t L_45 = L_44->get_sampleSize_37();
		NullCheck(L_42);
		L_42->set_average_27(((float)((float)L_43/(float)((float)((float)L_45)))));
	}

IL_015a:
	{
		Salsa3D_tACF83D529139263662FEAAC8F5B53EC43B140C9C * L_46 = __this->get_U3CU3E4__this_2();
		NullCheck(L_46);
		WaitForSeconds_t8F9189BE6E467C98C99177038881F8982E0E4013 * L_47 = L_46->get_updateSampleDelay_31();
		__this->set_U3CU3E2__current_0(L_47);
		__this->set_U3CU3E1__state_1(1);
		return (bool)1;
	}

IL_0174:
	{
		__this->set_U3CU3E1__state_1((-1));
		goto IL_0021;
	}

IL_0180:
	{
		return (bool)0;
	}
}
// System.Object CrazyMinnow.SALSA.Salsa3D/<UpdateSample>d__0::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject * U3CUpdateSampleU3Ed__0_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m7D4F83F4B85BEAC2F01C41CF80ACA0A2F23EAAB5 (U3CUpdateSampleU3Ed__0_tB4DEFA6499D020E6EC2C7109DBED4C19296282D3 * __this, const RuntimeMethod* method)
{
	{
		RuntimeObject * L_0 = __this->get_U3CU3E2__current_0();
		return L_0;
	}
}
// System.Void CrazyMinnow.SALSA.Salsa3D/<UpdateSample>d__0::System.Collections.IEnumerator.Reset()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CUpdateSampleU3Ed__0_System_Collections_IEnumerator_Reset_m482BF263E6A170335B23F21B5CA758CC19D9370F (U3CUpdateSampleU3Ed__0_tB4DEFA6499D020E6EC2C7109DBED4C19296282D3 * __this, const RuntimeMethod* method)
{
	{
		NotSupportedException_tB9D89F0E9470A2C423D239D7C68EE0CFD77F9339 * L_0 = (NotSupportedException_tB9D89F0E9470A2C423D239D7C68EE0CFD77F9339 *)il2cpp_codegen_object_new(((RuntimeClass*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&NotSupportedException_tB9D89F0E9470A2C423D239D7C68EE0CFD77F9339_il2cpp_TypeInfo_var)));
		NotSupportedException__ctor_m3EA81A5B209A87C3ADA47443F2AFFF735E5256EE(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0, ((RuntimeMethod*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&U3CUpdateSampleU3Ed__0_System_Collections_IEnumerator_Reset_m482BF263E6A170335B23F21B5CA758CC19D9370F_RuntimeMethod_var)));
	}
}
// System.Void CrazyMinnow.SALSA.Salsa3D/<UpdateSample>d__0::System.IDisposable.Dispose()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CUpdateSampleU3Ed__0_System_IDisposable_Dispose_mA953A0205C9175B7DD1F260B4D142FAE87F508CA (U3CUpdateSampleU3Ed__0_tB4DEFA6499D020E6EC2C7109DBED4C19296282D3 * __this, const RuntimeMethod* method)
{
	int32_t V_0 = 0;
	{
		return;
	}
}
// System.Object CrazyMinnow.SALSA.Salsa3D/<UpdateSample>d__0::System.Collections.IEnumerator.get_Current()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject * U3CUpdateSampleU3Ed__0_System_Collections_IEnumerator_get_Current_m3B132E8B0A5214B5E01262ADBFC7FD269CC51AA2 (U3CUpdateSampleU3Ed__0_tB4DEFA6499D020E6EC2C7109DBED4C19296282D3 * __this, const RuntimeMethod* method)
{
	{
		RuntimeObject * L_0 = __this->get_U3CU3E2__current_0();
		return L_0;
	}
}
// System.Void CrazyMinnow.SALSA.Salsa3D/<UpdateSample>d__0::.ctor(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CUpdateSampleU3Ed__0__ctor_m199B19B5576CA91594C9D8529188676BA2B73670 (U3CUpdateSampleU3Ed__0_tB4DEFA6499D020E6EC2C7109DBED4C19296282D3 * __this, int32_t ___U3CU3E1__state0, const RuntimeMethod* method)
{
	{
		Object__ctor_m88880E0413421D13FD95325EDCE231707CE1F405(__this, /*hidden argument*/NULL);
		int32_t L_0 = ___U3CU3E1__state0;
		__this->set_U3CU3E1__state_1(L_0);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void Vector3__ctor_m57495F692C6CE1CEF278CAD9A98221165D37E636_inline (Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * __this, float ___x0, float ___y1, float ___z2, const RuntimeMethod* method)
{
	{
		float L_0 = ___x0;
		__this->set_x_2(L_0);
		float L_1 = ___y1;
		__this->set_y_3(L_1);
		float L_2 = ___z2;
		__this->set_z_4(L_2);
		return;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  Vector3_op_Subtraction_m2725C96965D5C0B1F9715797E51762B13A5FED58_inline (Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___a0, Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___b1, const RuntimeMethod* method)
{
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  V_0;
	memset((&V_0), 0, sizeof(V_0));
	{
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_0 = ___a0;
		float L_1 = L_0.get_x_2();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_2 = ___b1;
		float L_3 = L_2.get_x_2();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_4 = ___a0;
		float L_5 = L_4.get_y_3();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_6 = ___b1;
		float L_7 = L_6.get_y_3();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_8 = ___a0;
		float L_9 = L_8.get_z_4();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_10 = ___b1;
		float L_11 = L_10.get_z_4();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_12;
		memset((&L_12), 0, sizeof(L_12));
		Vector3__ctor_m57495F692C6CE1CEF278CAD9A98221165D37E636_inline((&L_12), ((float)il2cpp_codegen_subtract((float)L_1, (float)L_3)), ((float)il2cpp_codegen_subtract((float)L_5, (float)L_7)), ((float)il2cpp_codegen_subtract((float)L_9, (float)L_11)), /*hidden argument*/NULL);
		V_0 = L_12;
		goto IL_0030;
	}

IL_0030:
	{
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_13 = V_0;
		return L_13;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  Vector3_op_Addition_mEE4F672B923CCB184C39AABCA33443DB218E50E0_inline (Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___a0, Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___b1, const RuntimeMethod* method)
{
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  V_0;
	memset((&V_0), 0, sizeof(V_0));
	{
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_0 = ___a0;
		float L_1 = L_0.get_x_2();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_2 = ___b1;
		float L_3 = L_2.get_x_2();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_4 = ___a0;
		float L_5 = L_4.get_y_3();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_6 = ___b1;
		float L_7 = L_6.get_y_3();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_8 = ___a0;
		float L_9 = L_8.get_z_4();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_10 = ___b1;
		float L_11 = L_10.get_z_4();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_12;
		memset((&L_12), 0, sizeof(L_12));
		Vector3__ctor_m57495F692C6CE1CEF278CAD9A98221165D37E636_inline((&L_12), ((float)il2cpp_codegen_add((float)L_1, (float)L_3)), ((float)il2cpp_codegen_add((float)L_5, (float)L_7)), ((float)il2cpp_codegen_add((float)L_9, (float)L_11)), /*hidden argument*/NULL);
		V_0 = L_12;
		goto IL_0030;
	}

IL_0030:
	{
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_13 = V_0;
		return L_13;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void Vector2__ctor_m9F1F2D5EB5D1FF7091BB527AC8A72CBB309D115E_inline (Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 * __this, float ___x0, float ___y1, const RuntimeMethod* method)
{
	{
		float L_0 = ___x0;
		__this->set_x_0(L_0);
		float L_1 = ___y1;
		__this->set_y_1(L_1);
		return;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void RandomEyesCustomShapeStatus_set_instance_m4F5644F9DB6E38215FA12B1892E396CA0D8281B1_inline (RandomEyesCustomShapeStatus_tAE795B1C1EF38DFDE0535D6A919E25A96283DD66 * __this, Object_tF2F3778131EFF286AF62B7B013A170F95A91571A * ___value0, const RuntimeMethod* method)
{
	{
		Object_tF2F3778131EFF286AF62B7B013A170F95A91571A * L_0 = ___value0;
		__this->set_U3CinstanceU3Ek__BackingField_8(L_0);
		return;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR int32_t String_get_Length_m129FC0ADA02FECBED3C0B1A809AE84A5AEE1CF09_inline (String_t* __this, const RuntimeMethod* method)
{
	{
		int32_t L_0 = __this->get_m_stringLength_0();
		return L_0;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void RandomEyes3D_SetCustomShapeExcludeEyes_m2907E3D9B857200BD4368EB62CCC3A0B4D51C436_inline (RandomEyes3D_t6868157C4605BD0225E1B08301F110E72336CE45 * __this, bool ___status0, const RuntimeMethod* method)
{
	{
		bool L_0 = ___status0;
		__this->set_excludeEyeShapes_44(L_0);
		return;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void RandomEyes3D_SetCustomShapeExcludeMouth_m3063985221F372102DC1B3854A31D75F95655684_inline (RandomEyes3D_t6868157C4605BD0225E1B08301F110E72336CE45 * __this, Salsa3D_tACF83D529139263662FEAAC8F5B53EC43B140C9C * ___salsa3D0, const RuntimeMethod* method)
{
	{
		Salsa3D_tACF83D529139263662FEAAC8F5B53EC43B140C9C * L_0 = ___salsa3D0;
		__this->set_salsa3D_5(L_0);
		return;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  Vector3_op_Multiply_m9EA3D18290418D7B410C7D11C4788C13BFD2C30A_inline (Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___a0, float ___d1, const RuntimeMethod* method)
{
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  V_0;
	memset((&V_0), 0, sizeof(V_0));
	{
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_0 = ___a0;
		float L_1 = L_0.get_x_2();
		float L_2 = ___d1;
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_3 = ___a0;
		float L_4 = L_3.get_y_3();
		float L_5 = ___d1;
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_6 = ___a0;
		float L_7 = L_6.get_z_4();
		float L_8 = ___d1;
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_9;
		memset((&L_9), 0, sizeof(L_9));
		Vector3__ctor_m57495F692C6CE1CEF278CAD9A98221165D37E636_inline((&L_9), ((float)il2cpp_codegen_multiply((float)L_1, (float)L_2)), ((float)il2cpp_codegen_multiply((float)L_4, (float)L_5)), ((float)il2cpp_codegen_multiply((float)L_7, (float)L_8)), /*hidden argument*/NULL);
		V_0 = L_9;
		goto IL_0021;
	}

IL_0021:
	{
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_10 = V_0;
		return L_10;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void SalsaStatus_set_instance_mB67A73851E6FDD5B713B5F85017446B672B32F63_inline (SalsaStatus_t67641B5EA3D03836981ED128DA718792E18105C1 * __this, Object_tF2F3778131EFF286AF62B7B013A170F95A91571A * ___value0, const RuntimeMethod* method)
{
	{
		Object_tF2F3778131EFF286AF62B7B013A170F95A91571A * L_0 = ___value0;
		__this->set_U3CinstanceU3Ek__BackingField_0(L_0);
		return;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void SalsaStatus_set_talkerName_m88E92FFF1840D7288C4474685B82F562E5A6C5EE_inline (SalsaStatus_t67641B5EA3D03836981ED128DA718792E18105C1 * __this, String_t* ___value0, const RuntimeMethod* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set_U3CtalkerNameU3Ek__BackingField_1(L_0);
		return;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void SalsaStatus_set_isTalking_m621FF64AC69982D2A47B901157DB29C7F3EC6725_inline (SalsaStatus_t67641B5EA3D03836981ED128DA718792E18105C1 * __this, bool ___value0, const RuntimeMethod* method)
{
	{
		bool L_0 = ___value0;
		__this->set_U3CisTalkingU3Ek__BackingField_2(L_0);
		return;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void SalsaStatus_set_clipName_m564A868F0584865EBA31D5F8DD38FE39B698F72A_inline (SalsaStatus_t67641B5EA3D03836981ED128DA718792E18105C1 * __this, String_t* ___value0, const RuntimeMethod* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set_U3CclipNameU3Ek__BackingField_3(L_0);
		return;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  Vector3_Lerp_m8E095584FFA10CF1D3EABCD04F4C83FB82EC5524_inline (Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___a0, Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___b1, float ___t2, const RuntimeMethod* method)
{
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  V_0;
	memset((&V_0), 0, sizeof(V_0));
	{
		float L_0 = ___t2;
		float L_1;
		L_1 = Mathf_Clamp01_m2296D75F0F1292D5C8181C57007A1CA45F440C4C(L_0, /*hidden argument*/NULL);
		___t2 = L_1;
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_2 = ___a0;
		float L_3 = L_2.get_x_2();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_4 = ___b1;
		float L_5 = L_4.get_x_2();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_6 = ___a0;
		float L_7 = L_6.get_x_2();
		float L_8 = ___t2;
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_9 = ___a0;
		float L_10 = L_9.get_y_3();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_11 = ___b1;
		float L_12 = L_11.get_y_3();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_13 = ___a0;
		float L_14 = L_13.get_y_3();
		float L_15 = ___t2;
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_16 = ___a0;
		float L_17 = L_16.get_z_4();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_18 = ___b1;
		float L_19 = L_18.get_z_4();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_20 = ___a0;
		float L_21 = L_20.get_z_4();
		float L_22 = ___t2;
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_23;
		memset((&L_23), 0, sizeof(L_23));
		Vector3__ctor_m57495F692C6CE1CEF278CAD9A98221165D37E636_inline((&L_23), ((float)il2cpp_codegen_add((float)L_3, (float)((float)il2cpp_codegen_multiply((float)((float)il2cpp_codegen_subtract((float)L_5, (float)L_7)), (float)L_8)))), ((float)il2cpp_codegen_add((float)L_10, (float)((float)il2cpp_codegen_multiply((float)((float)il2cpp_codegen_subtract((float)L_12, (float)L_14)), (float)L_15)))), ((float)il2cpp_codegen_add((float)L_17, (float)((float)il2cpp_codegen_multiply((float)((float)il2cpp_codegen_subtract((float)L_19, (float)L_21)), (float)L_22)))), /*hidden argument*/NULL);
		V_0 = L_23;
		goto IL_0053;
	}

IL_0053:
	{
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_24 = V_0;
		return L_24;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR RuntimeObject * List_1_get_Item_mF00B574E58FB078BB753B05A3B86DD0A7A266B63_gshared_inline (List_1_t3F94120C77410A62EAE48421CF166B83AB95A2F5 * __this, int32_t ___index0, const RuntimeMethod* method)
{
	{
		int32_t L_0 = ___index0;
		int32_t L_1 = (int32_t)__this->get__size_2();
		if ((!(((uint32_t)L_0) >= ((uint32_t)L_1))))
		{
			goto IL_000e;
		}
	}
	{
		ThrowHelper_ThrowArgumentOutOfRangeException_m4841366ABC2B2AFA37C10900551D7E07522C0929(/*hidden argument*/NULL);
	}

IL_000e:
	{
		ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE* L_2 = (ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE*)__this->get__items_1();
		int32_t L_3 = ___index0;
		RuntimeObject * L_4;
		L_4 = IL2CPP_ARRAY_UNSAFE_LOAD((ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE*)L_2, (int32_t)L_3);
		return (RuntimeObject *)L_4;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR int32_t List_1_get_Count_m5D847939ABB9A78203B062CAFFE975792174D00F_gshared_inline (List_1_t3F94120C77410A62EAE48421CF166B83AB95A2F5 * __this, const RuntimeMethod* method)
{
	{
		int32_t L_0 = (int32_t)__this->get__size_2();
		return (int32_t)L_0;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR int32_t List_1_get_Count_m7FA90926D9267868473EF90941F6BF794EC87FF2_gshared_inline (List_1_t260B41F956D673396C33A4CF94E8D6C4389EACB7 * __this, const RuntimeMethod* method)
{
	{
		int32_t L_0 = (int32_t)__this->get__size_2();
		return (int32_t)L_0;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR int32_t List_1_get_Item_m730FCAD2646FA94B07D1216A512B09AB9F0BBA5D_gshared_inline (List_1_t260B41F956D673396C33A4CF94E8D6C4389EACB7 * __this, int32_t ___index0, const RuntimeMethod* method)
{
	{
		int32_t L_0 = ___index0;
		int32_t L_1 = (int32_t)__this->get__size_2();
		if ((!(((uint32_t)L_0) >= ((uint32_t)L_1))))
		{
			goto IL_000e;
		}
	}
	{
		ThrowHelper_ThrowArgumentOutOfRangeException_m4841366ABC2B2AFA37C10900551D7E07522C0929(/*hidden argument*/NULL);
	}

IL_000e:
	{
		Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* L_2 = (Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32*)__this->get__items_1();
		int32_t L_3 = ___index0;
		int32_t L_4;
		L_4 = IL2CPP_ARRAY_UNSAFE_LOAD((Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32*)L_2, (int32_t)L_3);
		return (int32_t)L_4;
	}
}
