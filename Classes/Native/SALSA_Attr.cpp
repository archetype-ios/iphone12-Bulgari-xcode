﻿#include "pch-cpp.hpp"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include <limits>
#include <stdint.h>



// System.Char[]
struct CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34;
// System.Type[]
struct TypeU5BU5D_t85B10489E46F06CEC7C4B1CCBD0E01FAB6649755;
// UnityEngine.AddComponentMenu
struct AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100;
// System.Reflection.Binder
struct Binder_t2BEE27FD84737D1E79BC47FD67F6D3DD2F2DDA30;
// System.Runtime.CompilerServices.CompilationRelaxationsAttribute
struct CompilationRelaxationsAttribute_t661FDDC06629BDA607A42BD660944F039FE03AFF;
// System.Runtime.CompilerServices.CompilerGeneratedAttribute
struct CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C;
// System.Diagnostics.DebuggerHiddenAttribute
struct DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88;
// System.Reflection.MemberFilter
struct MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81;
// UnityEngine.RangeAttribute
struct RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5;
// UnityEngine.RequireComponent
struct RequireComponent_tEDA546F9722B8874DA9658BDAB821BA49647FC91;
// System.Runtime.CompilerServices.RuntimeCompatibilityAttribute
struct RuntimeCompatibilityAttribute_tFF99AB2963098F9CBCD47A20D9FD3D51C17C1C80;
// System.String
struct String_t;
// System.Type
struct Type_t;
// System.Void
struct Void_t700C6383A2A510C2CF4DD86DABD5CA9FF70ADAC5;

IL2CPP_EXTERN_C const RuntimeType* SpriteRenderer_t3F35AD5498243C170B46F5FFDB582AAEF78615EF_0_0_0_var;


IL2CPP_EXTERN_C_BEGIN
IL2CPP_EXTERN_C_END

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Object


// System.Attribute
struct Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71  : public RuntimeObject
{
public:

public:
};


// System.Reflection.MemberInfo
struct MemberInfo_t  : public RuntimeObject
{
public:

public:
};


// System.String
struct String_t  : public RuntimeObject
{
public:
	// System.Int32 System.String::m_stringLength
	int32_t ___m_stringLength_0;
	// System.Char System.String::m_firstChar
	Il2CppChar ___m_firstChar_1;

public:
	inline static int32_t get_offset_of_m_stringLength_0() { return static_cast<int32_t>(offsetof(String_t, ___m_stringLength_0)); }
	inline int32_t get_m_stringLength_0() const { return ___m_stringLength_0; }
	inline int32_t* get_address_of_m_stringLength_0() { return &___m_stringLength_0; }
	inline void set_m_stringLength_0(int32_t value)
	{
		___m_stringLength_0 = value;
	}

	inline static int32_t get_offset_of_m_firstChar_1() { return static_cast<int32_t>(offsetof(String_t, ___m_firstChar_1)); }
	inline Il2CppChar get_m_firstChar_1() const { return ___m_firstChar_1; }
	inline Il2CppChar* get_address_of_m_firstChar_1() { return &___m_firstChar_1; }
	inline void set_m_firstChar_1(Il2CppChar value)
	{
		___m_firstChar_1 = value;
	}
};

struct String_t_StaticFields
{
public:
	// System.String System.String::Empty
	String_t* ___Empty_5;

public:
	inline static int32_t get_offset_of_Empty_5() { return static_cast<int32_t>(offsetof(String_t_StaticFields, ___Empty_5)); }
	inline String_t* get_Empty_5() const { return ___Empty_5; }
	inline String_t** get_address_of_Empty_5() { return &___Empty_5; }
	inline void set_Empty_5(String_t* value)
	{
		___Empty_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___Empty_5), (void*)value);
	}
};


// System.ValueType
struct ValueType_tDBF999C1B75C48C68621878250DBF6CDBCF51E52  : public RuntimeObject
{
public:

public:
};

// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_tDBF999C1B75C48C68621878250DBF6CDBCF51E52_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_tDBF999C1B75C48C68621878250DBF6CDBCF51E52_marshaled_com
{
};

// UnityEngine.AddComponentMenu
struct AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.String UnityEngine.AddComponentMenu::m_AddComponentMenu
	String_t* ___m_AddComponentMenu_0;
	// System.Int32 UnityEngine.AddComponentMenu::m_Ordering
	int32_t ___m_Ordering_1;

public:
	inline static int32_t get_offset_of_m_AddComponentMenu_0() { return static_cast<int32_t>(offsetof(AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100, ___m_AddComponentMenu_0)); }
	inline String_t* get_m_AddComponentMenu_0() const { return ___m_AddComponentMenu_0; }
	inline String_t** get_address_of_m_AddComponentMenu_0() { return &___m_AddComponentMenu_0; }
	inline void set_m_AddComponentMenu_0(String_t* value)
	{
		___m_AddComponentMenu_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_AddComponentMenu_0), (void*)value);
	}

	inline static int32_t get_offset_of_m_Ordering_1() { return static_cast<int32_t>(offsetof(AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100, ___m_Ordering_1)); }
	inline int32_t get_m_Ordering_1() const { return ___m_Ordering_1; }
	inline int32_t* get_address_of_m_Ordering_1() { return &___m_Ordering_1; }
	inline void set_m_Ordering_1(int32_t value)
	{
		___m_Ordering_1 = value;
	}
};


// System.Boolean
struct Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37 
{
public:
	// System.Boolean System.Boolean::m_value
	bool ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37, ___m_value_0)); }
	inline bool get_m_value_0() const { return ___m_value_0; }
	inline bool* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(bool value)
	{
		___m_value_0 = value;
	}
};

struct Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37_StaticFields
{
public:
	// System.String System.Boolean::TrueString
	String_t* ___TrueString_5;
	// System.String System.Boolean::FalseString
	String_t* ___FalseString_6;

public:
	inline static int32_t get_offset_of_TrueString_5() { return static_cast<int32_t>(offsetof(Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37_StaticFields, ___TrueString_5)); }
	inline String_t* get_TrueString_5() const { return ___TrueString_5; }
	inline String_t** get_address_of_TrueString_5() { return &___TrueString_5; }
	inline void set_TrueString_5(String_t* value)
	{
		___TrueString_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___TrueString_5), (void*)value);
	}

	inline static int32_t get_offset_of_FalseString_6() { return static_cast<int32_t>(offsetof(Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37_StaticFields, ___FalseString_6)); }
	inline String_t* get_FalseString_6() const { return ___FalseString_6; }
	inline String_t** get_address_of_FalseString_6() { return &___FalseString_6; }
	inline void set_FalseString_6(String_t* value)
	{
		___FalseString_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___FalseString_6), (void*)value);
	}
};


// System.Runtime.CompilerServices.CompilationRelaxationsAttribute
struct CompilationRelaxationsAttribute_t661FDDC06629BDA607A42BD660944F039FE03AFF  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.Int32 System.Runtime.CompilerServices.CompilationRelaxationsAttribute::m_relaxations
	int32_t ___m_relaxations_0;

public:
	inline static int32_t get_offset_of_m_relaxations_0() { return static_cast<int32_t>(offsetof(CompilationRelaxationsAttribute_t661FDDC06629BDA607A42BD660944F039FE03AFF, ___m_relaxations_0)); }
	inline int32_t get_m_relaxations_0() const { return ___m_relaxations_0; }
	inline int32_t* get_address_of_m_relaxations_0() { return &___m_relaxations_0; }
	inline void set_m_relaxations_0(int32_t value)
	{
		___m_relaxations_0 = value;
	}
};


// System.Runtime.CompilerServices.CompilerGeneratedAttribute
struct CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:

public:
};


// System.Diagnostics.DebuggerHiddenAttribute
struct DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:

public:
};


// System.Enum
struct Enum_t23B90B40F60E677A8025267341651C94AE079CDA  : public ValueType_tDBF999C1B75C48C68621878250DBF6CDBCF51E52
{
public:

public:
};

struct Enum_t23B90B40F60E677A8025267341651C94AE079CDA_StaticFields
{
public:
	// System.Char[] System.Enum::enumSeperatorCharArray
	CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34* ___enumSeperatorCharArray_0;

public:
	inline static int32_t get_offset_of_enumSeperatorCharArray_0() { return static_cast<int32_t>(offsetof(Enum_t23B90B40F60E677A8025267341651C94AE079CDA_StaticFields, ___enumSeperatorCharArray_0)); }
	inline CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34* get_enumSeperatorCharArray_0() const { return ___enumSeperatorCharArray_0; }
	inline CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34** get_address_of_enumSeperatorCharArray_0() { return &___enumSeperatorCharArray_0; }
	inline void set_enumSeperatorCharArray_0(CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34* value)
	{
		___enumSeperatorCharArray_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___enumSeperatorCharArray_0), (void*)value);
	}
};

// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t23B90B40F60E677A8025267341651C94AE079CDA_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t23B90B40F60E677A8025267341651C94AE079CDA_marshaled_com
{
};

// System.IntPtr
struct IntPtr_t 
{
public:
	// System.Void* System.IntPtr::m_value
	void* ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(IntPtr_t, ___m_value_0)); }
	inline void* get_m_value_0() const { return ___m_value_0; }
	inline void** get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(void* value)
	{
		___m_value_0 = value;
	}
};

struct IntPtr_t_StaticFields
{
public:
	// System.IntPtr System.IntPtr::Zero
	intptr_t ___Zero_1;

public:
	inline static int32_t get_offset_of_Zero_1() { return static_cast<int32_t>(offsetof(IntPtr_t_StaticFields, ___Zero_1)); }
	inline intptr_t get_Zero_1() const { return ___Zero_1; }
	inline intptr_t* get_address_of_Zero_1() { return &___Zero_1; }
	inline void set_Zero_1(intptr_t value)
	{
		___Zero_1 = value;
	}
};


// UnityEngine.PropertyAttribute
struct PropertyAttribute_t4A352471DF625C56C811E27AC86B7E1CE6444052  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:

public:
};


// UnityEngine.RequireComponent
struct RequireComponent_tEDA546F9722B8874DA9658BDAB821BA49647FC91  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.Type UnityEngine.RequireComponent::m_Type0
	Type_t * ___m_Type0_0;
	// System.Type UnityEngine.RequireComponent::m_Type1
	Type_t * ___m_Type1_1;
	// System.Type UnityEngine.RequireComponent::m_Type2
	Type_t * ___m_Type2_2;

public:
	inline static int32_t get_offset_of_m_Type0_0() { return static_cast<int32_t>(offsetof(RequireComponent_tEDA546F9722B8874DA9658BDAB821BA49647FC91, ___m_Type0_0)); }
	inline Type_t * get_m_Type0_0() const { return ___m_Type0_0; }
	inline Type_t ** get_address_of_m_Type0_0() { return &___m_Type0_0; }
	inline void set_m_Type0_0(Type_t * value)
	{
		___m_Type0_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Type0_0), (void*)value);
	}

	inline static int32_t get_offset_of_m_Type1_1() { return static_cast<int32_t>(offsetof(RequireComponent_tEDA546F9722B8874DA9658BDAB821BA49647FC91, ___m_Type1_1)); }
	inline Type_t * get_m_Type1_1() const { return ___m_Type1_1; }
	inline Type_t ** get_address_of_m_Type1_1() { return &___m_Type1_1; }
	inline void set_m_Type1_1(Type_t * value)
	{
		___m_Type1_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Type1_1), (void*)value);
	}

	inline static int32_t get_offset_of_m_Type2_2() { return static_cast<int32_t>(offsetof(RequireComponent_tEDA546F9722B8874DA9658BDAB821BA49647FC91, ___m_Type2_2)); }
	inline Type_t * get_m_Type2_2() const { return ___m_Type2_2; }
	inline Type_t ** get_address_of_m_Type2_2() { return &___m_Type2_2; }
	inline void set_m_Type2_2(Type_t * value)
	{
		___m_Type2_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Type2_2), (void*)value);
	}
};


// System.Runtime.CompilerServices.RuntimeCompatibilityAttribute
struct RuntimeCompatibilityAttribute_tFF99AB2963098F9CBCD47A20D9FD3D51C17C1C80  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.Boolean System.Runtime.CompilerServices.RuntimeCompatibilityAttribute::m_wrapNonExceptionThrows
	bool ___m_wrapNonExceptionThrows_0;

public:
	inline static int32_t get_offset_of_m_wrapNonExceptionThrows_0() { return static_cast<int32_t>(offsetof(RuntimeCompatibilityAttribute_tFF99AB2963098F9CBCD47A20D9FD3D51C17C1C80, ___m_wrapNonExceptionThrows_0)); }
	inline bool get_m_wrapNonExceptionThrows_0() const { return ___m_wrapNonExceptionThrows_0; }
	inline bool* get_address_of_m_wrapNonExceptionThrows_0() { return &___m_wrapNonExceptionThrows_0; }
	inline void set_m_wrapNonExceptionThrows_0(bool value)
	{
		___m_wrapNonExceptionThrows_0 = value;
	}
};


// System.Void
struct Void_t700C6383A2A510C2CF4DD86DABD5CA9FF70ADAC5 
{
public:
	union
	{
		struct
		{
		};
		uint8_t Void_t700C6383A2A510C2CF4DD86DABD5CA9FF70ADAC5__padding[1];
	};

public:
};


// System.Reflection.BindingFlags
struct BindingFlags_tAAAB07D9AC588F0D55D844E51D7035E96DF94733 
{
public:
	// System.Int32 System.Reflection.BindingFlags::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(BindingFlags_tAAAB07D9AC588F0D55D844E51D7035E96DF94733, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// UnityEngine.RangeAttribute
struct RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5  : public PropertyAttribute_t4A352471DF625C56C811E27AC86B7E1CE6444052
{
public:
	// System.Single UnityEngine.RangeAttribute::min
	float ___min_0;
	// System.Single UnityEngine.RangeAttribute::max
	float ___max_1;

public:
	inline static int32_t get_offset_of_min_0() { return static_cast<int32_t>(offsetof(RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5, ___min_0)); }
	inline float get_min_0() const { return ___min_0; }
	inline float* get_address_of_min_0() { return &___min_0; }
	inline void set_min_0(float value)
	{
		___min_0 = value;
	}

	inline static int32_t get_offset_of_max_1() { return static_cast<int32_t>(offsetof(RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5, ___max_1)); }
	inline float get_max_1() const { return ___max_1; }
	inline float* get_address_of_max_1() { return &___max_1; }
	inline void set_max_1(float value)
	{
		___max_1 = value;
	}
};


// System.RuntimeTypeHandle
struct RuntimeTypeHandle_tC33965ADA3E041E0C94AF05E5CB527B56482CEF9 
{
public:
	// System.IntPtr System.RuntimeTypeHandle::value
	intptr_t ___value_0;

public:
	inline static int32_t get_offset_of_value_0() { return static_cast<int32_t>(offsetof(RuntimeTypeHandle_tC33965ADA3E041E0C94AF05E5CB527B56482CEF9, ___value_0)); }
	inline intptr_t get_value_0() const { return ___value_0; }
	inline intptr_t* get_address_of_value_0() { return &___value_0; }
	inline void set_value_0(intptr_t value)
	{
		___value_0 = value;
	}
};


// System.Type
struct Type_t  : public MemberInfo_t
{
public:
	// System.RuntimeTypeHandle System.Type::_impl
	RuntimeTypeHandle_tC33965ADA3E041E0C94AF05E5CB527B56482CEF9  ____impl_9;

public:
	inline static int32_t get_offset_of__impl_9() { return static_cast<int32_t>(offsetof(Type_t, ____impl_9)); }
	inline RuntimeTypeHandle_tC33965ADA3E041E0C94AF05E5CB527B56482CEF9  get__impl_9() const { return ____impl_9; }
	inline RuntimeTypeHandle_tC33965ADA3E041E0C94AF05E5CB527B56482CEF9 * get_address_of__impl_9() { return &____impl_9; }
	inline void set__impl_9(RuntimeTypeHandle_tC33965ADA3E041E0C94AF05E5CB527B56482CEF9  value)
	{
		____impl_9 = value;
	}
};

struct Type_t_StaticFields
{
public:
	// System.Reflection.MemberFilter System.Type::FilterAttribute
	MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * ___FilterAttribute_0;
	// System.Reflection.MemberFilter System.Type::FilterName
	MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * ___FilterName_1;
	// System.Reflection.MemberFilter System.Type::FilterNameIgnoreCase
	MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * ___FilterNameIgnoreCase_2;
	// System.Object System.Type::Missing
	RuntimeObject * ___Missing_3;
	// System.Char System.Type::Delimiter
	Il2CppChar ___Delimiter_4;
	// System.Type[] System.Type::EmptyTypes
	TypeU5BU5D_t85B10489E46F06CEC7C4B1CCBD0E01FAB6649755* ___EmptyTypes_5;
	// System.Reflection.Binder System.Type::defaultBinder
	Binder_t2BEE27FD84737D1E79BC47FD67F6D3DD2F2DDA30 * ___defaultBinder_6;

public:
	inline static int32_t get_offset_of_FilterAttribute_0() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___FilterAttribute_0)); }
	inline MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * get_FilterAttribute_0() const { return ___FilterAttribute_0; }
	inline MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 ** get_address_of_FilterAttribute_0() { return &___FilterAttribute_0; }
	inline void set_FilterAttribute_0(MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * value)
	{
		___FilterAttribute_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___FilterAttribute_0), (void*)value);
	}

	inline static int32_t get_offset_of_FilterName_1() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___FilterName_1)); }
	inline MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * get_FilterName_1() const { return ___FilterName_1; }
	inline MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 ** get_address_of_FilterName_1() { return &___FilterName_1; }
	inline void set_FilterName_1(MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * value)
	{
		___FilterName_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___FilterName_1), (void*)value);
	}

	inline static int32_t get_offset_of_FilterNameIgnoreCase_2() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___FilterNameIgnoreCase_2)); }
	inline MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * get_FilterNameIgnoreCase_2() const { return ___FilterNameIgnoreCase_2; }
	inline MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 ** get_address_of_FilterNameIgnoreCase_2() { return &___FilterNameIgnoreCase_2; }
	inline void set_FilterNameIgnoreCase_2(MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * value)
	{
		___FilterNameIgnoreCase_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___FilterNameIgnoreCase_2), (void*)value);
	}

	inline static int32_t get_offset_of_Missing_3() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___Missing_3)); }
	inline RuntimeObject * get_Missing_3() const { return ___Missing_3; }
	inline RuntimeObject ** get_address_of_Missing_3() { return &___Missing_3; }
	inline void set_Missing_3(RuntimeObject * value)
	{
		___Missing_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___Missing_3), (void*)value);
	}

	inline static int32_t get_offset_of_Delimiter_4() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___Delimiter_4)); }
	inline Il2CppChar get_Delimiter_4() const { return ___Delimiter_4; }
	inline Il2CppChar* get_address_of_Delimiter_4() { return &___Delimiter_4; }
	inline void set_Delimiter_4(Il2CppChar value)
	{
		___Delimiter_4 = value;
	}

	inline static int32_t get_offset_of_EmptyTypes_5() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___EmptyTypes_5)); }
	inline TypeU5BU5D_t85B10489E46F06CEC7C4B1CCBD0E01FAB6649755* get_EmptyTypes_5() const { return ___EmptyTypes_5; }
	inline TypeU5BU5D_t85B10489E46F06CEC7C4B1CCBD0E01FAB6649755** get_address_of_EmptyTypes_5() { return &___EmptyTypes_5; }
	inline void set_EmptyTypes_5(TypeU5BU5D_t85B10489E46F06CEC7C4B1CCBD0E01FAB6649755* value)
	{
		___EmptyTypes_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___EmptyTypes_5), (void*)value);
	}

	inline static int32_t get_offset_of_defaultBinder_6() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___defaultBinder_6)); }
	inline Binder_t2BEE27FD84737D1E79BC47FD67F6D3DD2F2DDA30 * get_defaultBinder_6() const { return ___defaultBinder_6; }
	inline Binder_t2BEE27FD84737D1E79BC47FD67F6D3DD2F2DDA30 ** get_address_of_defaultBinder_6() { return &___defaultBinder_6; }
	inline void set_defaultBinder_6(Binder_t2BEE27FD84737D1E79BC47FD67F6D3DD2F2DDA30 * value)
	{
		___defaultBinder_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___defaultBinder_6), (void*)value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif



// System.Void System.Runtime.CompilerServices.RuntimeCompatibilityAttribute::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void RuntimeCompatibilityAttribute__ctor_m551DDF1438CE97A984571949723F30F44CF7317C (RuntimeCompatibilityAttribute_tFF99AB2963098F9CBCD47A20D9FD3D51C17C1C80 * __this, const RuntimeMethod* method);
// System.Void System.Runtime.CompilerServices.RuntimeCompatibilityAttribute::set_WrapNonExceptionThrows(System.Boolean)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void RuntimeCompatibilityAttribute_set_WrapNonExceptionThrows_m8562196F90F3EBCEC23B5708EE0332842883C490_inline (RuntimeCompatibilityAttribute_tFF99AB2963098F9CBCD47A20D9FD3D51C17C1C80 * __this, bool ___value0, const RuntimeMethod* method);
// System.Void System.Runtime.CompilerServices.CompilationRelaxationsAttribute::.ctor(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void CompilationRelaxationsAttribute__ctor_mAC3079EBC4EEAB474EED8208EF95DB39C922333B (CompilationRelaxationsAttribute_t661FDDC06629BDA607A42BD660944F039FE03AFF * __this, int32_t ___relaxations0, const RuntimeMethod* method);
// System.Void UnityEngine.AddComponentMenu::.ctor(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AddComponentMenu__ctor_m34CE7BDF93FA607429964AEF1D23436963EE8549 (AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 * __this, String_t* ___menuName0, const RuntimeMethod* method);
// System.Void UnityEngine.RangeAttribute::.ctor(System.Single,System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void RangeAttribute__ctor_mC74D39A9F20DD2A0D4174F05785ABE4F0DAEF000 (RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5 * __this, float ___min0, float ___max1, const RuntimeMethod* method);
// System.Void System.Runtime.CompilerServices.CompilerGeneratedAttribute::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35 (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * __this, const RuntimeMethod* method);
// System.Void System.Diagnostics.DebuggerHiddenAttribute::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3 (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * __this, const RuntimeMethod* method);
// System.Void UnityEngine.RequireComponent::.ctor(System.Type)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void RequireComponent__ctor_m5EC89D3D22D7D880E1B88A5C9FADF1FBDC713EE4 (RequireComponent_tEDA546F9722B8874DA9658BDAB821BA49647FC91 * __this, Type_t * ___requiredComponent0, const RuntimeMethod* method);
static void SALSA_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		RuntimeCompatibilityAttribute_tFF99AB2963098F9CBCD47A20D9FD3D51C17C1C80 * tmp = (RuntimeCompatibilityAttribute_tFF99AB2963098F9CBCD47A20D9FD3D51C17C1C80 *)cache->attributes[0];
		RuntimeCompatibilityAttribute__ctor_m551DDF1438CE97A984571949723F30F44CF7317C(tmp, NULL);
		RuntimeCompatibilityAttribute_set_WrapNonExceptionThrows_m8562196F90F3EBCEC23B5708EE0332842883C490_inline(tmp, true, NULL);
	}
	{
		CompilationRelaxationsAttribute_t661FDDC06629BDA607A42BD660944F039FE03AFF * tmp = (CompilationRelaxationsAttribute_t661FDDC06629BDA607A42BD660944F039FE03AFF *)cache->attributes[1];
		CompilationRelaxationsAttribute__ctor_mAC3079EBC4EEAB474EED8208EF95DB39C922333B(tmp, 8LL, NULL);
	}
}
static void RandomEyes2D_t253864BE689DE7E477335D17ECD2EC77001B377F_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 * tmp = (AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 *)cache->attributes[0];
		AddComponentMenu__ctor_m34CE7BDF93FA607429964AEF1D23436963EE8549(tmp, il2cpp_codegen_string_new_wrapper("\x43\x72\x61\x7A\x79\x20\x4D\x69\x6E\x6E\x6F\x77\x20\x53\x74\x75\x64\x69\x6F\x2F\x52\x61\x6E\x64\x6F\x6D\x45\x79\x65\x73\x32\x44"), NULL);
	}
}
static void RandomEyes2D_t253864BE689DE7E477335D17ECD2EC77001B377F_CustomAttributesCacheGenerator_rangeOfMotion(CustomAttributesCache* cache)
{
	{
		RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5 * tmp = (RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5 *)cache->attributes[0];
		RangeAttribute__ctor_mC74D39A9F20DD2A0D4174F05785ABE4F0DAEF000(tmp, 0.0f, 50.0f, NULL);
	}
}
static void RandomEyes2D_t253864BE689DE7E477335D17ECD2EC77001B377F_CustomAttributesCacheGenerator_targetAffinityPercentage(CustomAttributesCache* cache)
{
	{
		RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5 * tmp = (RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5 *)cache->attributes[0];
		RangeAttribute__ctor_mC74D39A9F20DD2A0D4174F05785ABE4F0DAEF000(tmp, 0.0f, 1.0f, NULL);
	}
}
static void RandomEyes2D_t253864BE689DE7E477335D17ECD2EC77001B377F_CustomAttributesCacheGenerator_blendSpeed(CustomAttributesCache* cache)
{
	{
		RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5 * tmp = (RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5 *)cache->attributes[0];
		RangeAttribute__ctor_mC74D39A9F20DD2A0D4174F05785ABE4F0DAEF000(tmp, 0.0f, 100.0f, NULL);
	}
}
static void U3CTargetAffinityUpdateU3Ed__0_tB9D47FFD16FA8A3CF1860EC8ADB05A787654DF78_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CTargetAffinityUpdateU3Ed__0_tB9D47FFD16FA8A3CF1860EC8ADB05A787654DF78_CustomAttributesCacheGenerator_U3CTargetAffinityUpdateU3Ed__0_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m8D0D3B955C4C2F3FCBBAD3D5FA4A2EE44ADA142D(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CTargetAffinityUpdateU3Ed__0_tB9D47FFD16FA8A3CF1860EC8ADB05A787654DF78_CustomAttributesCacheGenerator_U3CTargetAffinityUpdateU3Ed__0_System_Collections_IEnumerator_Reset_mED26C7B767350312099E767FA28653FB03743451(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CTargetAffinityUpdateU3Ed__0_tB9D47FFD16FA8A3CF1860EC8ADB05A787654DF78_CustomAttributesCacheGenerator_U3CTargetAffinityUpdateU3Ed__0_System_Collections_IEnumerator_get_Current_m681473C79048E8CF76C118B34813BB9D67BB552A(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CTargetAffinityUpdateU3Ed__0_tB9D47FFD16FA8A3CF1860EC8ADB05A787654DF78_CustomAttributesCacheGenerator_U3CTargetAffinityUpdateU3Ed__0__ctor_m11474126BC59372CD200224F8F86C2D80D12BD0C(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CBlinkEyesU3Ed__2_t7DFD119FC328CD8363C654182C85B8B62BA190D1_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CBlinkEyesU3Ed__2_t7DFD119FC328CD8363C654182C85B8B62BA190D1_CustomAttributesCacheGenerator_U3CBlinkEyesU3Ed__2_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m81D3332B40A695A9AA1FDE3E8B85B2139144A02C(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CBlinkEyesU3Ed__2_t7DFD119FC328CD8363C654182C85B8B62BA190D1_CustomAttributesCacheGenerator_U3CBlinkEyesU3Ed__2_System_Collections_IEnumerator_Reset_m88EBA7BE8E25F597E063CB1264198186B11DEBCB(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CBlinkEyesU3Ed__2_t7DFD119FC328CD8363C654182C85B8B62BA190D1_CustomAttributesCacheGenerator_U3CBlinkEyesU3Ed__2_System_Collections_IEnumerator_get_Current_mA4EBBC50009E04F0472056FDA043C8E273572F7A(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CBlinkEyesU3Ed__2_t7DFD119FC328CD8363C654182C85B8B62BA190D1_CustomAttributesCacheGenerator_U3CBlinkEyesU3Ed__2__ctor_m794D2B438E817C03906B33BA6CEA16630030D920(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void RandomEyes3D_t6868157C4605BD0225E1B08301F110E72336CE45_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 * tmp = (AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 *)cache->attributes[0];
		AddComponentMenu__ctor_m34CE7BDF93FA607429964AEF1D23436963EE8549(tmp, il2cpp_codegen_string_new_wrapper("\x43\x72\x61\x7A\x79\x20\x4D\x69\x6E\x6E\x6F\x77\x20\x53\x74\x75\x64\x69\x6F\x2F\x52\x61\x6E\x64\x6F\x6D\x45\x79\x65\x73\x33\x44"), NULL);
	}
}
static void RandomEyes3D_t6868157C4605BD0225E1B08301F110E72336CE45_CustomAttributesCacheGenerator_blendSpeed(CustomAttributesCache* cache)
{
	{
		RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5 * tmp = (RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5 *)cache->attributes[0];
		RangeAttribute__ctor_mC74D39A9F20DD2A0D4174F05785ABE4F0DAEF000(tmp, 0.0f, 100.0f, NULL);
	}
}
static void RandomEyes3D_t6868157C4605BD0225E1B08301F110E72336CE45_CustomAttributesCacheGenerator_rangeOfMotion(CustomAttributesCache* cache)
{
	{
		RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5 * tmp = (RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5 *)cache->attributes[0];
		RangeAttribute__ctor_mC74D39A9F20DD2A0D4174F05785ABE4F0DAEF000(tmp, 0.0f, 100.0f, NULL);
	}
}
static void RandomEyes3D_t6868157C4605BD0225E1B08301F110E72336CE45_CustomAttributesCacheGenerator_blinkSpeed(CustomAttributesCache* cache)
{
	{
		RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5 * tmp = (RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5 *)cache->attributes[0];
		RangeAttribute__ctor_mC74D39A9F20DD2A0D4174F05785ABE4F0DAEF000(tmp, 0.0f, 100.0f, NULL);
	}
}
static void RandomEyes3D_t6868157C4605BD0225E1B08301F110E72336CE45_CustomAttributesCacheGenerator_targetAffinityPercentage(CustomAttributesCache* cache)
{
	{
		RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5 * tmp = (RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5 *)cache->attributes[0];
		RangeAttribute__ctor_mC74D39A9F20DD2A0D4174F05785ABE4F0DAEF000(tmp, 0.0f, 1.0f, NULL);
	}
}
static void U3CTargetAffinityUpdateU3Ed__0_tCF892F05CB3FC0D3FC4F3984F53D8C429D011013_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CTargetAffinityUpdateU3Ed__0_tCF892F05CB3FC0D3FC4F3984F53D8C429D011013_CustomAttributesCacheGenerator_U3CTargetAffinityUpdateU3Ed__0_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m2327362FC616DD432555C7E25749B63137D99477(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CTargetAffinityUpdateU3Ed__0_tCF892F05CB3FC0D3FC4F3984F53D8C429D011013_CustomAttributesCacheGenerator_U3CTargetAffinityUpdateU3Ed__0_System_Collections_IEnumerator_Reset_mBAE456A3C0C96A0C8B70B0D32F3D345A0C07CA1E(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CTargetAffinityUpdateU3Ed__0_tCF892F05CB3FC0D3FC4F3984F53D8C429D011013_CustomAttributesCacheGenerator_U3CTargetAffinityUpdateU3Ed__0_System_Collections_IEnumerator_get_Current_m348B9921D9DFEA1779B61D9EA42F31C06F6A00BB(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CTargetAffinityUpdateU3Ed__0_tCF892F05CB3FC0D3FC4F3984F53D8C429D011013_CustomAttributesCacheGenerator_U3CTargetAffinityUpdateU3Ed__0__ctor_mCAFDF47555144A310E4F743A29A8C68AAAA8FDFB(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CBlinkEyesU3Ed__2_t05DA28F00D8E1A9E64D0728065DB53DA96232044_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CBlinkEyesU3Ed__2_t05DA28F00D8E1A9E64D0728065DB53DA96232044_CustomAttributesCacheGenerator_U3CBlinkEyesU3Ed__2_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mB5FD936BCB45081E3D0AD94D05AB1EA32D510B74(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CBlinkEyesU3Ed__2_t05DA28F00D8E1A9E64D0728065DB53DA96232044_CustomAttributesCacheGenerator_U3CBlinkEyesU3Ed__2_System_Collections_IEnumerator_Reset_mD5DCBE6D12F53C662EF106666A000BEB03ED3558(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CBlinkEyesU3Ed__2_t05DA28F00D8E1A9E64D0728065DB53DA96232044_CustomAttributesCacheGenerator_U3CBlinkEyesU3Ed__2_System_Collections_IEnumerator_get_Current_mF267239843A192233380FE5A3DA1DA92BA248B7E(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CBlinkEyesU3Ed__2_t05DA28F00D8E1A9E64D0728065DB53DA96232044_CustomAttributesCacheGenerator_U3CBlinkEyesU3Ed__2__ctor_m1C070BB710ACF1198445B49A02B0EE406A56F50C(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CSetCustomShapeOverrideDurationU3Ed__4_tA42191F3DEC3EBAD53A277EAC5C346C81A3BC122_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CSetCustomShapeOverrideDurationU3Ed__4_tA42191F3DEC3EBAD53A277EAC5C346C81A3BC122_CustomAttributesCacheGenerator_U3CSetCustomShapeOverrideDurationU3Ed__4_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m0DB8C66E0B7DC354D7A4096727A9F719FA17B455(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CSetCustomShapeOverrideDurationU3Ed__4_tA42191F3DEC3EBAD53A277EAC5C346C81A3BC122_CustomAttributesCacheGenerator_U3CSetCustomShapeOverrideDurationU3Ed__4_System_Collections_IEnumerator_Reset_mAD496E65677155FF44B6B02F4850449E59AD4F75(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CSetCustomShapeOverrideDurationU3Ed__4_tA42191F3DEC3EBAD53A277EAC5C346C81A3BC122_CustomAttributesCacheGenerator_U3CSetCustomShapeOverrideDurationU3Ed__4_System_Collections_IEnumerator_get_Current_mD22F300FB60FBC84ACA05B07F1CC27E4ECFFCE7C(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CSetCustomShapeOverrideDurationU3Ed__4_tA42191F3DEC3EBAD53A277EAC5C346C81A3BC122_CustomAttributesCacheGenerator_U3CSetCustomShapeOverrideDurationU3Ed__4__ctor_mB66C5B43F2C15F540F85F1845A71DB41035E6FAD(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CSetCustomShapeOverrideDurationU3Ed__6_t3067670AF5E03DD7899B8F7492C6EF59E5B77DB0_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CSetCustomShapeOverrideDurationU3Ed__6_t3067670AF5E03DD7899B8F7492C6EF59E5B77DB0_CustomAttributesCacheGenerator_U3CSetCustomShapeOverrideDurationU3Ed__6_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mC45AF0F2DFB5CC2AB9A7B2674BE4416D56C54330(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CSetCustomShapeOverrideDurationU3Ed__6_t3067670AF5E03DD7899B8F7492C6EF59E5B77DB0_CustomAttributesCacheGenerator_U3CSetCustomShapeOverrideDurationU3Ed__6_System_Collections_IEnumerator_Reset_m248F5D80FBFAA6C850D4680977E43D02F8E5F7AC(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CSetCustomShapeOverrideDurationU3Ed__6_t3067670AF5E03DD7899B8F7492C6EF59E5B77DB0_CustomAttributesCacheGenerator_U3CSetCustomShapeOverrideDurationU3Ed__6_System_Collections_IEnumerator_get_Current_mFD60724EFEB1942BDF96085D0C38219DC9796378(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CSetCustomShapeOverrideDurationU3Ed__6_t3067670AF5E03DD7899B8F7492C6EF59E5B77DB0_CustomAttributesCacheGenerator_U3CSetCustomShapeOverrideDurationU3Ed__6__ctor_m90B95A85DE3597BE73729C4BD7FF6D9756F8D6EB(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CSetCustomShapeDurationU3Ed__8_t1CD4174D8597ACF6A20E2146BB78651E84AE273B_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CSetCustomShapeDurationU3Ed__8_t1CD4174D8597ACF6A20E2146BB78651E84AE273B_CustomAttributesCacheGenerator_U3CSetCustomShapeDurationU3Ed__8_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m1DD68496D286084B27BA2709A378BF2620CF9534(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CSetCustomShapeDurationU3Ed__8_t1CD4174D8597ACF6A20E2146BB78651E84AE273B_CustomAttributesCacheGenerator_U3CSetCustomShapeDurationU3Ed__8_System_Collections_IEnumerator_Reset_mA93C2810F2DFD6BB6409274C433C796616BD935F(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CSetCustomShapeDurationU3Ed__8_t1CD4174D8597ACF6A20E2146BB78651E84AE273B_CustomAttributesCacheGenerator_U3CSetCustomShapeDurationU3Ed__8_System_Collections_IEnumerator_get_Current_m655AA5A40FE04228B897FF3815520CE581E9C3DC(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CSetCustomShapeDurationU3Ed__8_t1CD4174D8597ACF6A20E2146BB78651E84AE273B_CustomAttributesCacheGenerator_U3CSetCustomShapeDurationU3Ed__8__ctor_mF6CD6A348AAE88971E18E1E4E94D43FF9A964EB3(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CSetCustomShapeDurationU3Ed__a_tF3A147C5E712D0581C2B12133986348BFBB67140_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CSetCustomShapeDurationU3Ed__a_tF3A147C5E712D0581C2B12133986348BFBB67140_CustomAttributesCacheGenerator_U3CSetCustomShapeDurationU3Ed__a_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m29AD9BF27A7FF383D8C75DEA1AD987DCB65A24E5(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CSetCustomShapeDurationU3Ed__a_tF3A147C5E712D0581C2B12133986348BFBB67140_CustomAttributesCacheGenerator_U3CSetCustomShapeDurationU3Ed__a_System_Collections_IEnumerator_Reset_m6AD8EF97A2799112F845813343FD577CA1698CF7(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CSetCustomShapeDurationU3Ed__a_tF3A147C5E712D0581C2B12133986348BFBB67140_CustomAttributesCacheGenerator_U3CSetCustomShapeDurationU3Ed__a_System_Collections_IEnumerator_get_Current_m3FCCF65FB5B696321DFF75279636D1A8666D7DF7(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CSetCustomShapeDurationU3Ed__a_tF3A147C5E712D0581C2B12133986348BFBB67140_CustomAttributesCacheGenerator_U3CSetCustomShapeDurationU3Ed__a__ctor_m22ECCEFDEC5DB0D1080451AE79B9E22EC5A618EB(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CDoBlinkU3Ed__c_t8CF4EE9B667E494EE34FBB05C5E960320E5C8418_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CDoBlinkU3Ed__c_t8CF4EE9B667E494EE34FBB05C5E960320E5C8418_CustomAttributesCacheGenerator_U3CDoBlinkU3Ed__c_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m401CF5F735EAD27CA82731FA9AAC660CEB51F4CA(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CDoBlinkU3Ed__c_t8CF4EE9B667E494EE34FBB05C5E960320E5C8418_CustomAttributesCacheGenerator_U3CDoBlinkU3Ed__c_System_Collections_IEnumerator_Reset_m04161B4126710078525F0D3F99E46A39A538B5B6(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CDoBlinkU3Ed__c_t8CF4EE9B667E494EE34FBB05C5E960320E5C8418_CustomAttributesCacheGenerator_U3CDoBlinkU3Ed__c_System_Collections_IEnumerator_get_Current_mA6C9A533B91FE9022C5929D1F59EA6849129B59D(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CDoBlinkU3Ed__c_t8CF4EE9B667E494EE34FBB05C5E960320E5C8418_CustomAttributesCacheGenerator_U3CDoBlinkU3Ed__c__ctor_m9D9176B98A436D63C7C71A253F9242C3ACD37F00(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void Salsa2D_t54C8B8FFE3B8149C565BD2A69D53629E65CFFB62_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&SpriteRenderer_t3F35AD5498243C170B46F5FFDB582AAEF78615EF_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		RequireComponent_tEDA546F9722B8874DA9658BDAB821BA49647FC91 * tmp = (RequireComponent_tEDA546F9722B8874DA9658BDAB821BA49647FC91 *)cache->attributes[0];
		RequireComponent__ctor_m5EC89D3D22D7D880E1B88A5C9FADF1FBDC713EE4(tmp, il2cpp_codegen_type_get_object(SpriteRenderer_t3F35AD5498243C170B46F5FFDB582AAEF78615EF_0_0_0_var), NULL);
	}
	{
		AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 * tmp = (AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 *)cache->attributes[1];
		AddComponentMenu__ctor_m34CE7BDF93FA607429964AEF1D23436963EE8549(tmp, il2cpp_codegen_string_new_wrapper("\x43\x72\x61\x7A\x79\x20\x4D\x69\x6E\x6E\x6F\x77\x20\x53\x74\x75\x64\x69\x6F\x2F\x53\x61\x6C\x73\x61\x32\x44"), NULL);
	}
}
static void Salsa2D_t54C8B8FFE3B8149C565BD2A69D53629E65CFFB62_CustomAttributesCacheGenerator_saySmallTrigger(CustomAttributesCache* cache)
{
	{
		RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5 * tmp = (RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5 *)cache->attributes[0];
		RangeAttribute__ctor_mC74D39A9F20DD2A0D4174F05785ABE4F0DAEF000(tmp, 9.99999975E-05f, 0.0120000001f, NULL);
	}
}
static void Salsa2D_t54C8B8FFE3B8149C565BD2A69D53629E65CFFB62_CustomAttributesCacheGenerator_sayMediumTrigger(CustomAttributesCache* cache)
{
	{
		RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5 * tmp = (RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5 *)cache->attributes[0];
		RangeAttribute__ctor_mC74D39A9F20DD2A0D4174F05785ABE4F0DAEF000(tmp, 9.99999975E-05f, 0.0120000001f, NULL);
	}
}
static void Salsa2D_t54C8B8FFE3B8149C565BD2A69D53629E65CFFB62_CustomAttributesCacheGenerator_sayLargeTrigger(CustomAttributesCache* cache)
{
	{
		RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5 * tmp = (RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5 *)cache->attributes[0];
		RangeAttribute__ctor_mC74D39A9F20DD2A0D4174F05785ABE4F0DAEF000(tmp, 9.99999975E-05f, 0.0120000001f, NULL);
	}
}
static void Salsa2D_t54C8B8FFE3B8149C565BD2A69D53629E65CFFB62_CustomAttributesCacheGenerator_audioUpdateDelay(CustomAttributesCache* cache)
{
	{
		RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5 * tmp = (RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5 *)cache->attributes[0];
		RangeAttribute__ctor_mC74D39A9F20DD2A0D4174F05785ABE4F0DAEF000(tmp, 0.100000001f, 1.0f, NULL);
	}
}
static void U3CUpdateSampleU3Ed__0_t610463941DB34736C5EE20E7CA02876D017BE95F_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CUpdateSampleU3Ed__0_t610463941DB34736C5EE20E7CA02876D017BE95F_CustomAttributesCacheGenerator_U3CUpdateSampleU3Ed__0_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m1DE327FA8E703A2453E6E02DE2D379EF29B0353A(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CUpdateSampleU3Ed__0_t610463941DB34736C5EE20E7CA02876D017BE95F_CustomAttributesCacheGenerator_U3CUpdateSampleU3Ed__0_System_Collections_IEnumerator_Reset_m721FCB06919D945E4656B278C22DB71EBAD5D67B(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CUpdateSampleU3Ed__0_t610463941DB34736C5EE20E7CA02876D017BE95F_CustomAttributesCacheGenerator_U3CUpdateSampleU3Ed__0_System_Collections_IEnumerator_get_Current_mB214E3D8FF3AD5D7552BD9302AB3D5D5B40C79CB(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CUpdateSampleU3Ed__0_t610463941DB34736C5EE20E7CA02876D017BE95F_CustomAttributesCacheGenerator_U3CUpdateSampleU3Ed__0__ctor_m455E0D010D62E290992788EA19E42269D2B13A77(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void Salsa3D_tACF83D529139263662FEAAC8F5B53EC43B140C9C_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 * tmp = (AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 *)cache->attributes[0];
		AddComponentMenu__ctor_m34CE7BDF93FA607429964AEF1D23436963EE8549(tmp, il2cpp_codegen_string_new_wrapper("\x43\x72\x61\x7A\x79\x20\x4D\x69\x6E\x6E\x6F\x77\x20\x53\x74\x75\x64\x69\x6F\x2F\x53\x61\x6C\x73\x61\x33\x44"), NULL);
	}
}
static void Salsa3D_tACF83D529139263662FEAAC8F5B53EC43B140C9C_CustomAttributesCacheGenerator_saySmallTrigger(CustomAttributesCache* cache)
{
	{
		RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5 * tmp = (RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5 *)cache->attributes[0];
		RangeAttribute__ctor_mC74D39A9F20DD2A0D4174F05785ABE4F0DAEF000(tmp, 9.99999975E-05f, 0.0120000001f, NULL);
	}
}
static void Salsa3D_tACF83D529139263662FEAAC8F5B53EC43B140C9C_CustomAttributesCacheGenerator_sayMediumTrigger(CustomAttributesCache* cache)
{
	{
		RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5 * tmp = (RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5 *)cache->attributes[0];
		RangeAttribute__ctor_mC74D39A9F20DD2A0D4174F05785ABE4F0DAEF000(tmp, 9.99999975E-05f, 0.0120000001f, NULL);
	}
}
static void Salsa3D_tACF83D529139263662FEAAC8F5B53EC43B140C9C_CustomAttributesCacheGenerator_sayLargeTrigger(CustomAttributesCache* cache)
{
	{
		RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5 * tmp = (RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5 *)cache->attributes[0];
		RangeAttribute__ctor_mC74D39A9F20DD2A0D4174F05785ABE4F0DAEF000(tmp, 9.99999975E-05f, 0.0120000001f, NULL);
	}
}
static void Salsa3D_tACF83D529139263662FEAAC8F5B53EC43B140C9C_CustomAttributesCacheGenerator_audioUpdateDelay(CustomAttributesCache* cache)
{
	{
		RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5 * tmp = (RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5 *)cache->attributes[0];
		RangeAttribute__ctor_mC74D39A9F20DD2A0D4174F05785ABE4F0DAEF000(tmp, 0.00999999978f, 1.0f, NULL);
	}
}
static void Salsa3D_tACF83D529139263662FEAAC8F5B53EC43B140C9C_CustomAttributesCacheGenerator_blendSpeed(CustomAttributesCache* cache)
{
	{
		RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5 * tmp = (RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5 *)cache->attributes[0];
		RangeAttribute__ctor_mC74D39A9F20DD2A0D4174F05785ABE4F0DAEF000(tmp, 0.0f, 100.0f, NULL);
	}
}
static void Salsa3D_tACF83D529139263662FEAAC8F5B53EC43B140C9C_CustomAttributesCacheGenerator_rangeOfMotion(CustomAttributesCache* cache)
{
	{
		RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5 * tmp = (RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5 *)cache->attributes[0];
		RangeAttribute__ctor_mC74D39A9F20DD2A0D4174F05785ABE4F0DAEF000(tmp, 0.0f, 100.0f, NULL);
	}
}
static void U3CUpdateSampleU3Ed__0_tB4DEFA6499D020E6EC2C7109DBED4C19296282D3_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CUpdateSampleU3Ed__0_tB4DEFA6499D020E6EC2C7109DBED4C19296282D3_CustomAttributesCacheGenerator_U3CUpdateSampleU3Ed__0_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m7D4F83F4B85BEAC2F01C41CF80ACA0A2F23EAAB5(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CUpdateSampleU3Ed__0_tB4DEFA6499D020E6EC2C7109DBED4C19296282D3_CustomAttributesCacheGenerator_U3CUpdateSampleU3Ed__0_System_Collections_IEnumerator_Reset_m482BF263E6A170335B23F21B5CA758CC19D9370F(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CUpdateSampleU3Ed__0_tB4DEFA6499D020E6EC2C7109DBED4C19296282D3_CustomAttributesCacheGenerator_U3CUpdateSampleU3Ed__0_System_Collections_IEnumerator_get_Current_m3B132E8B0A5214B5E01262ADBFC7FD269CC51AA2(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CUpdateSampleU3Ed__0_tB4DEFA6499D020E6EC2C7109DBED4C19296282D3_CustomAttributesCacheGenerator_U3CUpdateSampleU3Ed__0__ctor_m199B19B5576CA91594C9D8529188676BA2B73670(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void SalsaStatus_t67641B5EA3D03836981ED128DA718792E18105C1_CustomAttributesCacheGenerator_U3CinstanceU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void SalsaStatus_t67641B5EA3D03836981ED128DA718792E18105C1_CustomAttributesCacheGenerator_U3CtalkerNameU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void SalsaStatus_t67641B5EA3D03836981ED128DA718792E18105C1_CustomAttributesCacheGenerator_U3CisTalkingU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void SalsaStatus_t67641B5EA3D03836981ED128DA718792E18105C1_CustomAttributesCacheGenerator_U3CclipNameU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void SalsaStatus_t67641B5EA3D03836981ED128DA718792E18105C1_CustomAttributesCacheGenerator_SalsaStatus_get_instance_mB63DF2D8EF456D6A9E976AEB43B6C03A77BE4C29(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void SalsaStatus_t67641B5EA3D03836981ED128DA718792E18105C1_CustomAttributesCacheGenerator_SalsaStatus_set_instance_mB67A73851E6FDD5B713B5F85017446B672B32F63(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void SalsaStatus_t67641B5EA3D03836981ED128DA718792E18105C1_CustomAttributesCacheGenerator_SalsaStatus_get_talkerName_mAE5109304DDE680F4BB760BDC4A60BE9AA29EDD5(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void SalsaStatus_t67641B5EA3D03836981ED128DA718792E18105C1_CustomAttributesCacheGenerator_SalsaStatus_set_talkerName_m88E92FFF1840D7288C4474685B82F562E5A6C5EE(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void SalsaStatus_t67641B5EA3D03836981ED128DA718792E18105C1_CustomAttributesCacheGenerator_SalsaStatus_get_isTalking_m5E400E0CDFCB8B3241BE2255144A88E0E0881234(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void SalsaStatus_t67641B5EA3D03836981ED128DA718792E18105C1_CustomAttributesCacheGenerator_SalsaStatus_set_isTalking_m621FF64AC69982D2A47B901157DB29C7F3EC6725(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void SalsaStatus_t67641B5EA3D03836981ED128DA718792E18105C1_CustomAttributesCacheGenerator_SalsaStatus_get_clipName_m1D43753B3C3BED26B88D82DBF7F14991D56E2B92(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void SalsaStatus_t67641B5EA3D03836981ED128DA718792E18105C1_CustomAttributesCacheGenerator_SalsaStatus_set_clipName_m564A868F0584865EBA31D5F8DD38FE39B698F72A(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void RandomEyesCustomShape_t10BCA0CEC18C2BBF6A8BD7BFAD2AF005D233C354_CustomAttributesCacheGenerator_rangeOfMotion(CustomAttributesCache* cache)
{
	{
		RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5 * tmp = (RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5 *)cache->attributes[0];
		RangeAttribute__ctor_mC74D39A9F20DD2A0D4174F05785ABE4F0DAEF000(tmp, 0.0f, 100.0f, NULL);
	}
}
static void RandomEyesCustomShapeStatus_tAE795B1C1EF38DFDE0535D6A919E25A96283DD66_CustomAttributesCacheGenerator_U3CinstanceU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void RandomEyesCustomShapeStatus_tAE795B1C1EF38DFDE0535D6A919E25A96283DD66_CustomAttributesCacheGenerator_RandomEyesCustomShapeStatus_get_instance_m3E3018C5AB25E4037D1E5DD7F84F42337B99229F(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void RandomEyesCustomShapeStatus_tAE795B1C1EF38DFDE0535D6A919E25A96283DD66_CustomAttributesCacheGenerator_RandomEyesCustomShapeStatus_set_instance_m4F5644F9DB6E38215FA12B1892E396CA0D8281B1(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void RandomEyesLookStatus_t2C78F0556D5424EFB610AEC14051A53E29757DC4_CustomAttributesCacheGenerator_U3CinstanceU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void RandomEyesLookStatus_t2C78F0556D5424EFB610AEC14051A53E29757DC4_CustomAttributesCacheGenerator_U3CblendSpeedU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void RandomEyesLookStatus_t2C78F0556D5424EFB610AEC14051A53E29757DC4_CustomAttributesCacheGenerator_U3CrangeOfMotionU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void RandomEyesLookStatus_t2C78F0556D5424EFB610AEC14051A53E29757DC4_CustomAttributesCacheGenerator_RandomEyesLookStatus_get_instance_m701539465A1941EB294C4D49503D98D0AD731E34(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void RandomEyesLookStatus_t2C78F0556D5424EFB610AEC14051A53E29757DC4_CustomAttributesCacheGenerator_RandomEyesLookStatus_set_instance_m68125F9E62FDE656F139985323B8500DA3E5AE8D(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void RandomEyesLookStatus_t2C78F0556D5424EFB610AEC14051A53E29757DC4_CustomAttributesCacheGenerator_RandomEyesLookStatus_get_blendSpeed_m1AC267734DB387C3AC1040BA8519978D3E62F34D(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void RandomEyesLookStatus_t2C78F0556D5424EFB610AEC14051A53E29757DC4_CustomAttributesCacheGenerator_RandomEyesLookStatus_set_blendSpeed_m8D54B2B8FB2CB13A97161B6C5A480FDFD5D258EE(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void RandomEyesLookStatus_t2C78F0556D5424EFB610AEC14051A53E29757DC4_CustomAttributesCacheGenerator_RandomEyesLookStatus_get_rangeOfMotion_m96D975B67348C3BB84FC651115084D7957685029(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void RandomEyesLookStatus_t2C78F0556D5424EFB610AEC14051A53E29757DC4_CustomAttributesCacheGenerator_RandomEyesLookStatus_set_rangeOfMotion_m8E01B94D5F3932364DD566B7676D60B1939B09C9(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
IL2CPP_EXTERN_C const CustomAttributesCacheGenerator g_SALSA_AttributeGenerators[];
const CustomAttributesCacheGenerator g_SALSA_AttributeGenerators[102] = 
{
	RandomEyes2D_t253864BE689DE7E477335D17ECD2EC77001B377F_CustomAttributesCacheGenerator,
	U3CTargetAffinityUpdateU3Ed__0_tB9D47FFD16FA8A3CF1860EC8ADB05A787654DF78_CustomAttributesCacheGenerator,
	U3CBlinkEyesU3Ed__2_t7DFD119FC328CD8363C654182C85B8B62BA190D1_CustomAttributesCacheGenerator,
	RandomEyes3D_t6868157C4605BD0225E1B08301F110E72336CE45_CustomAttributesCacheGenerator,
	U3CTargetAffinityUpdateU3Ed__0_tCF892F05CB3FC0D3FC4F3984F53D8C429D011013_CustomAttributesCacheGenerator,
	U3CBlinkEyesU3Ed__2_t05DA28F00D8E1A9E64D0728065DB53DA96232044_CustomAttributesCacheGenerator,
	U3CSetCustomShapeOverrideDurationU3Ed__4_tA42191F3DEC3EBAD53A277EAC5C346C81A3BC122_CustomAttributesCacheGenerator,
	U3CSetCustomShapeOverrideDurationU3Ed__6_t3067670AF5E03DD7899B8F7492C6EF59E5B77DB0_CustomAttributesCacheGenerator,
	U3CSetCustomShapeDurationU3Ed__8_t1CD4174D8597ACF6A20E2146BB78651E84AE273B_CustomAttributesCacheGenerator,
	U3CSetCustomShapeDurationU3Ed__a_tF3A147C5E712D0581C2B12133986348BFBB67140_CustomAttributesCacheGenerator,
	U3CDoBlinkU3Ed__c_t8CF4EE9B667E494EE34FBB05C5E960320E5C8418_CustomAttributesCacheGenerator,
	Salsa2D_t54C8B8FFE3B8149C565BD2A69D53629E65CFFB62_CustomAttributesCacheGenerator,
	U3CUpdateSampleU3Ed__0_t610463941DB34736C5EE20E7CA02876D017BE95F_CustomAttributesCacheGenerator,
	Salsa3D_tACF83D529139263662FEAAC8F5B53EC43B140C9C_CustomAttributesCacheGenerator,
	U3CUpdateSampleU3Ed__0_tB4DEFA6499D020E6EC2C7109DBED4C19296282D3_CustomAttributesCacheGenerator,
	RandomEyes2D_t253864BE689DE7E477335D17ECD2EC77001B377F_CustomAttributesCacheGenerator_rangeOfMotion,
	RandomEyes2D_t253864BE689DE7E477335D17ECD2EC77001B377F_CustomAttributesCacheGenerator_targetAffinityPercentage,
	RandomEyes2D_t253864BE689DE7E477335D17ECD2EC77001B377F_CustomAttributesCacheGenerator_blendSpeed,
	RandomEyes3D_t6868157C4605BD0225E1B08301F110E72336CE45_CustomAttributesCacheGenerator_blendSpeed,
	RandomEyes3D_t6868157C4605BD0225E1B08301F110E72336CE45_CustomAttributesCacheGenerator_rangeOfMotion,
	RandomEyes3D_t6868157C4605BD0225E1B08301F110E72336CE45_CustomAttributesCacheGenerator_blinkSpeed,
	RandomEyes3D_t6868157C4605BD0225E1B08301F110E72336CE45_CustomAttributesCacheGenerator_targetAffinityPercentage,
	Salsa2D_t54C8B8FFE3B8149C565BD2A69D53629E65CFFB62_CustomAttributesCacheGenerator_saySmallTrigger,
	Salsa2D_t54C8B8FFE3B8149C565BD2A69D53629E65CFFB62_CustomAttributesCacheGenerator_sayMediumTrigger,
	Salsa2D_t54C8B8FFE3B8149C565BD2A69D53629E65CFFB62_CustomAttributesCacheGenerator_sayLargeTrigger,
	Salsa2D_t54C8B8FFE3B8149C565BD2A69D53629E65CFFB62_CustomAttributesCacheGenerator_audioUpdateDelay,
	Salsa3D_tACF83D529139263662FEAAC8F5B53EC43B140C9C_CustomAttributesCacheGenerator_saySmallTrigger,
	Salsa3D_tACF83D529139263662FEAAC8F5B53EC43B140C9C_CustomAttributesCacheGenerator_sayMediumTrigger,
	Salsa3D_tACF83D529139263662FEAAC8F5B53EC43B140C9C_CustomAttributesCacheGenerator_sayLargeTrigger,
	Salsa3D_tACF83D529139263662FEAAC8F5B53EC43B140C9C_CustomAttributesCacheGenerator_audioUpdateDelay,
	Salsa3D_tACF83D529139263662FEAAC8F5B53EC43B140C9C_CustomAttributesCacheGenerator_blendSpeed,
	Salsa3D_tACF83D529139263662FEAAC8F5B53EC43B140C9C_CustomAttributesCacheGenerator_rangeOfMotion,
	SalsaStatus_t67641B5EA3D03836981ED128DA718792E18105C1_CustomAttributesCacheGenerator_U3CinstanceU3Ek__BackingField,
	SalsaStatus_t67641B5EA3D03836981ED128DA718792E18105C1_CustomAttributesCacheGenerator_U3CtalkerNameU3Ek__BackingField,
	SalsaStatus_t67641B5EA3D03836981ED128DA718792E18105C1_CustomAttributesCacheGenerator_U3CisTalkingU3Ek__BackingField,
	SalsaStatus_t67641B5EA3D03836981ED128DA718792E18105C1_CustomAttributesCacheGenerator_U3CclipNameU3Ek__BackingField,
	RandomEyesCustomShape_t10BCA0CEC18C2BBF6A8BD7BFAD2AF005D233C354_CustomAttributesCacheGenerator_rangeOfMotion,
	RandomEyesCustomShapeStatus_tAE795B1C1EF38DFDE0535D6A919E25A96283DD66_CustomAttributesCacheGenerator_U3CinstanceU3Ek__BackingField,
	RandomEyesLookStatus_t2C78F0556D5424EFB610AEC14051A53E29757DC4_CustomAttributesCacheGenerator_U3CinstanceU3Ek__BackingField,
	RandomEyesLookStatus_t2C78F0556D5424EFB610AEC14051A53E29757DC4_CustomAttributesCacheGenerator_U3CblendSpeedU3Ek__BackingField,
	RandomEyesLookStatus_t2C78F0556D5424EFB610AEC14051A53E29757DC4_CustomAttributesCacheGenerator_U3CrangeOfMotionU3Ek__BackingField,
	U3CTargetAffinityUpdateU3Ed__0_tB9D47FFD16FA8A3CF1860EC8ADB05A787654DF78_CustomAttributesCacheGenerator_U3CTargetAffinityUpdateU3Ed__0_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m8D0D3B955C4C2F3FCBBAD3D5FA4A2EE44ADA142D,
	U3CTargetAffinityUpdateU3Ed__0_tB9D47FFD16FA8A3CF1860EC8ADB05A787654DF78_CustomAttributesCacheGenerator_U3CTargetAffinityUpdateU3Ed__0_System_Collections_IEnumerator_Reset_mED26C7B767350312099E767FA28653FB03743451,
	U3CTargetAffinityUpdateU3Ed__0_tB9D47FFD16FA8A3CF1860EC8ADB05A787654DF78_CustomAttributesCacheGenerator_U3CTargetAffinityUpdateU3Ed__0_System_Collections_IEnumerator_get_Current_m681473C79048E8CF76C118B34813BB9D67BB552A,
	U3CTargetAffinityUpdateU3Ed__0_tB9D47FFD16FA8A3CF1860EC8ADB05A787654DF78_CustomAttributesCacheGenerator_U3CTargetAffinityUpdateU3Ed__0__ctor_m11474126BC59372CD200224F8F86C2D80D12BD0C,
	U3CBlinkEyesU3Ed__2_t7DFD119FC328CD8363C654182C85B8B62BA190D1_CustomAttributesCacheGenerator_U3CBlinkEyesU3Ed__2_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m81D3332B40A695A9AA1FDE3E8B85B2139144A02C,
	U3CBlinkEyesU3Ed__2_t7DFD119FC328CD8363C654182C85B8B62BA190D1_CustomAttributesCacheGenerator_U3CBlinkEyesU3Ed__2_System_Collections_IEnumerator_Reset_m88EBA7BE8E25F597E063CB1264198186B11DEBCB,
	U3CBlinkEyesU3Ed__2_t7DFD119FC328CD8363C654182C85B8B62BA190D1_CustomAttributesCacheGenerator_U3CBlinkEyesU3Ed__2_System_Collections_IEnumerator_get_Current_mA4EBBC50009E04F0472056FDA043C8E273572F7A,
	U3CBlinkEyesU3Ed__2_t7DFD119FC328CD8363C654182C85B8B62BA190D1_CustomAttributesCacheGenerator_U3CBlinkEyesU3Ed__2__ctor_m794D2B438E817C03906B33BA6CEA16630030D920,
	U3CTargetAffinityUpdateU3Ed__0_tCF892F05CB3FC0D3FC4F3984F53D8C429D011013_CustomAttributesCacheGenerator_U3CTargetAffinityUpdateU3Ed__0_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m2327362FC616DD432555C7E25749B63137D99477,
	U3CTargetAffinityUpdateU3Ed__0_tCF892F05CB3FC0D3FC4F3984F53D8C429D011013_CustomAttributesCacheGenerator_U3CTargetAffinityUpdateU3Ed__0_System_Collections_IEnumerator_Reset_mBAE456A3C0C96A0C8B70B0D32F3D345A0C07CA1E,
	U3CTargetAffinityUpdateU3Ed__0_tCF892F05CB3FC0D3FC4F3984F53D8C429D011013_CustomAttributesCacheGenerator_U3CTargetAffinityUpdateU3Ed__0_System_Collections_IEnumerator_get_Current_m348B9921D9DFEA1779B61D9EA42F31C06F6A00BB,
	U3CTargetAffinityUpdateU3Ed__0_tCF892F05CB3FC0D3FC4F3984F53D8C429D011013_CustomAttributesCacheGenerator_U3CTargetAffinityUpdateU3Ed__0__ctor_mCAFDF47555144A310E4F743A29A8C68AAAA8FDFB,
	U3CBlinkEyesU3Ed__2_t05DA28F00D8E1A9E64D0728065DB53DA96232044_CustomAttributesCacheGenerator_U3CBlinkEyesU3Ed__2_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mB5FD936BCB45081E3D0AD94D05AB1EA32D510B74,
	U3CBlinkEyesU3Ed__2_t05DA28F00D8E1A9E64D0728065DB53DA96232044_CustomAttributesCacheGenerator_U3CBlinkEyesU3Ed__2_System_Collections_IEnumerator_Reset_mD5DCBE6D12F53C662EF106666A000BEB03ED3558,
	U3CBlinkEyesU3Ed__2_t05DA28F00D8E1A9E64D0728065DB53DA96232044_CustomAttributesCacheGenerator_U3CBlinkEyesU3Ed__2_System_Collections_IEnumerator_get_Current_mF267239843A192233380FE5A3DA1DA92BA248B7E,
	U3CBlinkEyesU3Ed__2_t05DA28F00D8E1A9E64D0728065DB53DA96232044_CustomAttributesCacheGenerator_U3CBlinkEyesU3Ed__2__ctor_m1C070BB710ACF1198445B49A02B0EE406A56F50C,
	U3CSetCustomShapeOverrideDurationU3Ed__4_tA42191F3DEC3EBAD53A277EAC5C346C81A3BC122_CustomAttributesCacheGenerator_U3CSetCustomShapeOverrideDurationU3Ed__4_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m0DB8C66E0B7DC354D7A4096727A9F719FA17B455,
	U3CSetCustomShapeOverrideDurationU3Ed__4_tA42191F3DEC3EBAD53A277EAC5C346C81A3BC122_CustomAttributesCacheGenerator_U3CSetCustomShapeOverrideDurationU3Ed__4_System_Collections_IEnumerator_Reset_mAD496E65677155FF44B6B02F4850449E59AD4F75,
	U3CSetCustomShapeOverrideDurationU3Ed__4_tA42191F3DEC3EBAD53A277EAC5C346C81A3BC122_CustomAttributesCacheGenerator_U3CSetCustomShapeOverrideDurationU3Ed__4_System_Collections_IEnumerator_get_Current_mD22F300FB60FBC84ACA05B07F1CC27E4ECFFCE7C,
	U3CSetCustomShapeOverrideDurationU3Ed__4_tA42191F3DEC3EBAD53A277EAC5C346C81A3BC122_CustomAttributesCacheGenerator_U3CSetCustomShapeOverrideDurationU3Ed__4__ctor_mB66C5B43F2C15F540F85F1845A71DB41035E6FAD,
	U3CSetCustomShapeOverrideDurationU3Ed__6_t3067670AF5E03DD7899B8F7492C6EF59E5B77DB0_CustomAttributesCacheGenerator_U3CSetCustomShapeOverrideDurationU3Ed__6_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mC45AF0F2DFB5CC2AB9A7B2674BE4416D56C54330,
	U3CSetCustomShapeOverrideDurationU3Ed__6_t3067670AF5E03DD7899B8F7492C6EF59E5B77DB0_CustomAttributesCacheGenerator_U3CSetCustomShapeOverrideDurationU3Ed__6_System_Collections_IEnumerator_Reset_m248F5D80FBFAA6C850D4680977E43D02F8E5F7AC,
	U3CSetCustomShapeOverrideDurationU3Ed__6_t3067670AF5E03DD7899B8F7492C6EF59E5B77DB0_CustomAttributesCacheGenerator_U3CSetCustomShapeOverrideDurationU3Ed__6_System_Collections_IEnumerator_get_Current_mFD60724EFEB1942BDF96085D0C38219DC9796378,
	U3CSetCustomShapeOverrideDurationU3Ed__6_t3067670AF5E03DD7899B8F7492C6EF59E5B77DB0_CustomAttributesCacheGenerator_U3CSetCustomShapeOverrideDurationU3Ed__6__ctor_m90B95A85DE3597BE73729C4BD7FF6D9756F8D6EB,
	U3CSetCustomShapeDurationU3Ed__8_t1CD4174D8597ACF6A20E2146BB78651E84AE273B_CustomAttributesCacheGenerator_U3CSetCustomShapeDurationU3Ed__8_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m1DD68496D286084B27BA2709A378BF2620CF9534,
	U3CSetCustomShapeDurationU3Ed__8_t1CD4174D8597ACF6A20E2146BB78651E84AE273B_CustomAttributesCacheGenerator_U3CSetCustomShapeDurationU3Ed__8_System_Collections_IEnumerator_Reset_mA93C2810F2DFD6BB6409274C433C796616BD935F,
	U3CSetCustomShapeDurationU3Ed__8_t1CD4174D8597ACF6A20E2146BB78651E84AE273B_CustomAttributesCacheGenerator_U3CSetCustomShapeDurationU3Ed__8_System_Collections_IEnumerator_get_Current_m655AA5A40FE04228B897FF3815520CE581E9C3DC,
	U3CSetCustomShapeDurationU3Ed__8_t1CD4174D8597ACF6A20E2146BB78651E84AE273B_CustomAttributesCacheGenerator_U3CSetCustomShapeDurationU3Ed__8__ctor_mF6CD6A348AAE88971E18E1E4E94D43FF9A964EB3,
	U3CSetCustomShapeDurationU3Ed__a_tF3A147C5E712D0581C2B12133986348BFBB67140_CustomAttributesCacheGenerator_U3CSetCustomShapeDurationU3Ed__a_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m29AD9BF27A7FF383D8C75DEA1AD987DCB65A24E5,
	U3CSetCustomShapeDurationU3Ed__a_tF3A147C5E712D0581C2B12133986348BFBB67140_CustomAttributesCacheGenerator_U3CSetCustomShapeDurationU3Ed__a_System_Collections_IEnumerator_Reset_m6AD8EF97A2799112F845813343FD577CA1698CF7,
	U3CSetCustomShapeDurationU3Ed__a_tF3A147C5E712D0581C2B12133986348BFBB67140_CustomAttributesCacheGenerator_U3CSetCustomShapeDurationU3Ed__a_System_Collections_IEnumerator_get_Current_m3FCCF65FB5B696321DFF75279636D1A8666D7DF7,
	U3CSetCustomShapeDurationU3Ed__a_tF3A147C5E712D0581C2B12133986348BFBB67140_CustomAttributesCacheGenerator_U3CSetCustomShapeDurationU3Ed__a__ctor_m22ECCEFDEC5DB0D1080451AE79B9E22EC5A618EB,
	U3CDoBlinkU3Ed__c_t8CF4EE9B667E494EE34FBB05C5E960320E5C8418_CustomAttributesCacheGenerator_U3CDoBlinkU3Ed__c_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m401CF5F735EAD27CA82731FA9AAC660CEB51F4CA,
	U3CDoBlinkU3Ed__c_t8CF4EE9B667E494EE34FBB05C5E960320E5C8418_CustomAttributesCacheGenerator_U3CDoBlinkU3Ed__c_System_Collections_IEnumerator_Reset_m04161B4126710078525F0D3F99E46A39A538B5B6,
	U3CDoBlinkU3Ed__c_t8CF4EE9B667E494EE34FBB05C5E960320E5C8418_CustomAttributesCacheGenerator_U3CDoBlinkU3Ed__c_System_Collections_IEnumerator_get_Current_mA6C9A533B91FE9022C5929D1F59EA6849129B59D,
	U3CDoBlinkU3Ed__c_t8CF4EE9B667E494EE34FBB05C5E960320E5C8418_CustomAttributesCacheGenerator_U3CDoBlinkU3Ed__c__ctor_m9D9176B98A436D63C7C71A253F9242C3ACD37F00,
	U3CUpdateSampleU3Ed__0_t610463941DB34736C5EE20E7CA02876D017BE95F_CustomAttributesCacheGenerator_U3CUpdateSampleU3Ed__0_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m1DE327FA8E703A2453E6E02DE2D379EF29B0353A,
	U3CUpdateSampleU3Ed__0_t610463941DB34736C5EE20E7CA02876D017BE95F_CustomAttributesCacheGenerator_U3CUpdateSampleU3Ed__0_System_Collections_IEnumerator_Reset_m721FCB06919D945E4656B278C22DB71EBAD5D67B,
	U3CUpdateSampleU3Ed__0_t610463941DB34736C5EE20E7CA02876D017BE95F_CustomAttributesCacheGenerator_U3CUpdateSampleU3Ed__0_System_Collections_IEnumerator_get_Current_mB214E3D8FF3AD5D7552BD9302AB3D5D5B40C79CB,
	U3CUpdateSampleU3Ed__0_t610463941DB34736C5EE20E7CA02876D017BE95F_CustomAttributesCacheGenerator_U3CUpdateSampleU3Ed__0__ctor_m455E0D010D62E290992788EA19E42269D2B13A77,
	U3CUpdateSampleU3Ed__0_tB4DEFA6499D020E6EC2C7109DBED4C19296282D3_CustomAttributesCacheGenerator_U3CUpdateSampleU3Ed__0_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m7D4F83F4B85BEAC2F01C41CF80ACA0A2F23EAAB5,
	U3CUpdateSampleU3Ed__0_tB4DEFA6499D020E6EC2C7109DBED4C19296282D3_CustomAttributesCacheGenerator_U3CUpdateSampleU3Ed__0_System_Collections_IEnumerator_Reset_m482BF263E6A170335B23F21B5CA758CC19D9370F,
	U3CUpdateSampleU3Ed__0_tB4DEFA6499D020E6EC2C7109DBED4C19296282D3_CustomAttributesCacheGenerator_U3CUpdateSampleU3Ed__0_System_Collections_IEnumerator_get_Current_m3B132E8B0A5214B5E01262ADBFC7FD269CC51AA2,
	U3CUpdateSampleU3Ed__0_tB4DEFA6499D020E6EC2C7109DBED4C19296282D3_CustomAttributesCacheGenerator_U3CUpdateSampleU3Ed__0__ctor_m199B19B5576CA91594C9D8529188676BA2B73670,
	SalsaStatus_t67641B5EA3D03836981ED128DA718792E18105C1_CustomAttributesCacheGenerator_SalsaStatus_get_instance_mB63DF2D8EF456D6A9E976AEB43B6C03A77BE4C29,
	SalsaStatus_t67641B5EA3D03836981ED128DA718792E18105C1_CustomAttributesCacheGenerator_SalsaStatus_set_instance_mB67A73851E6FDD5B713B5F85017446B672B32F63,
	SalsaStatus_t67641B5EA3D03836981ED128DA718792E18105C1_CustomAttributesCacheGenerator_SalsaStatus_get_talkerName_mAE5109304DDE680F4BB760BDC4A60BE9AA29EDD5,
	SalsaStatus_t67641B5EA3D03836981ED128DA718792E18105C1_CustomAttributesCacheGenerator_SalsaStatus_set_talkerName_m88E92FFF1840D7288C4474685B82F562E5A6C5EE,
	SalsaStatus_t67641B5EA3D03836981ED128DA718792E18105C1_CustomAttributesCacheGenerator_SalsaStatus_get_isTalking_m5E400E0CDFCB8B3241BE2255144A88E0E0881234,
	SalsaStatus_t67641B5EA3D03836981ED128DA718792E18105C1_CustomAttributesCacheGenerator_SalsaStatus_set_isTalking_m621FF64AC69982D2A47B901157DB29C7F3EC6725,
	SalsaStatus_t67641B5EA3D03836981ED128DA718792E18105C1_CustomAttributesCacheGenerator_SalsaStatus_get_clipName_m1D43753B3C3BED26B88D82DBF7F14991D56E2B92,
	SalsaStatus_t67641B5EA3D03836981ED128DA718792E18105C1_CustomAttributesCacheGenerator_SalsaStatus_set_clipName_m564A868F0584865EBA31D5F8DD38FE39B698F72A,
	RandomEyesCustomShapeStatus_tAE795B1C1EF38DFDE0535D6A919E25A96283DD66_CustomAttributesCacheGenerator_RandomEyesCustomShapeStatus_get_instance_m3E3018C5AB25E4037D1E5DD7F84F42337B99229F,
	RandomEyesCustomShapeStatus_tAE795B1C1EF38DFDE0535D6A919E25A96283DD66_CustomAttributesCacheGenerator_RandomEyesCustomShapeStatus_set_instance_m4F5644F9DB6E38215FA12B1892E396CA0D8281B1,
	RandomEyesLookStatus_t2C78F0556D5424EFB610AEC14051A53E29757DC4_CustomAttributesCacheGenerator_RandomEyesLookStatus_get_instance_m701539465A1941EB294C4D49503D98D0AD731E34,
	RandomEyesLookStatus_t2C78F0556D5424EFB610AEC14051A53E29757DC4_CustomAttributesCacheGenerator_RandomEyesLookStatus_set_instance_m68125F9E62FDE656F139985323B8500DA3E5AE8D,
	RandomEyesLookStatus_t2C78F0556D5424EFB610AEC14051A53E29757DC4_CustomAttributesCacheGenerator_RandomEyesLookStatus_get_blendSpeed_m1AC267734DB387C3AC1040BA8519978D3E62F34D,
	RandomEyesLookStatus_t2C78F0556D5424EFB610AEC14051A53E29757DC4_CustomAttributesCacheGenerator_RandomEyesLookStatus_set_blendSpeed_m8D54B2B8FB2CB13A97161B6C5A480FDFD5D258EE,
	RandomEyesLookStatus_t2C78F0556D5424EFB610AEC14051A53E29757DC4_CustomAttributesCacheGenerator_RandomEyesLookStatus_get_rangeOfMotion_m96D975B67348C3BB84FC651115084D7957685029,
	RandomEyesLookStatus_t2C78F0556D5424EFB610AEC14051A53E29757DC4_CustomAttributesCacheGenerator_RandomEyesLookStatus_set_rangeOfMotion_m8E01B94D5F3932364DD566B7676D60B1939B09C9,
	SALSA_CustomAttributesCacheGenerator,
};
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void RuntimeCompatibilityAttribute_set_WrapNonExceptionThrows_m8562196F90F3EBCEC23B5708EE0332842883C490_inline (RuntimeCompatibilityAttribute_tFF99AB2963098F9CBCD47A20D9FD3D51C17C1C80 * __this, bool ___value0, const RuntimeMethod* method)
{
	{
		bool L_0 = ___value0;
		__this->set_m_wrapNonExceptionThrows_0(L_0);
		return;
	}
}
