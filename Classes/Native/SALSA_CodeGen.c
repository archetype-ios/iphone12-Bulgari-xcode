﻿#include "pch-c.h"
#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include "codegen/il2cpp-codegen-metadata.h"





// 0x00000001 System.Void CrazyMinnow.SALSA.RandomEyes2D::Start()
extern void RandomEyes2D_Start_m5605EE5FB0D11EE87C18900DC7DBF7C52BF1B0F2 (void);
// 0x00000002 System.Void CrazyMinnow.SALSA.RandomEyes2D::OnEnable()
extern void RandomEyes2D_OnEnable_mF60FB619533D13BC163E2C09AB09BEEABFCE6225 (void);
// 0x00000003 System.Void CrazyMinnow.SALSA.RandomEyes2D::LateUpdate()
extern void RandomEyes2D_LateUpdate_m2257F2110275E2A1F2F8D9703A8EF26BD611ECFA (void);
// 0x00000004 System.Void CrazyMinnow.SALSA.RandomEyes2D::LookTracking(UnityEngine.Vector3)
extern void RandomEyes2D_LookTracking_m4C39BFE72EEA5E5F4E707B3BD925D259FCF6B77C (void);
// 0x00000005 System.Single CrazyMinnow.SALSA.RandomEyes2D::ProportionalMovement(System.Single,System.Single,System.Single)
extern void RandomEyes2D_ProportionalMovement_m4EC2F23BA31627C8C60313AA4695FA4B11A1651B (void);
// 0x00000006 System.Collections.IEnumerator CrazyMinnow.SALSA.RandomEyes2D::TargetAffinityUpdate(System.Single)
extern void RandomEyes2D_TargetAffinityUpdate_m105F6F7B0C6788A396093FE3D0431CD3331FCCB0 (void);
// 0x00000007 System.Void CrazyMinnow.SALSA.RandomEyes2D::Blink(System.Single)
extern void RandomEyes2D_Blink_m2105B90CDDB68CD9D6F42752C3B7BAB853123B43 (void);
// 0x00000008 System.Collections.IEnumerator CrazyMinnow.SALSA.RandomEyes2D::BlinkEyes(System.Single)
extern void RandomEyes2D_BlinkEyes_mA9926DC8191C467D2188EC8D8F3AFFBB7AF92C24 (void);
// 0x00000009 System.Void CrazyMinnow.SALSA.RandomEyes2D::SetRangeOfMotion(System.Single)
extern void RandomEyes2D_SetRangeOfMotion_mB76F28D1AF16BF195B3E33D9C11C8DB09D4A6E95 (void);
// 0x0000000A System.Void CrazyMinnow.SALSA.RandomEyes2D::SetBlendSpeed(System.Single)
extern void RandomEyes2D_SetBlendSpeed_m1EB9A46A16069C006ADA386330F1FD54602CF1ED (void);
// 0x0000000B System.Void CrazyMinnow.SALSA.RandomEyes2D::SetOrderInLayer(CrazyMinnow.SALSA.RandomEyes2D/SpriteRend,System.Int32)
extern void RandomEyes2D_SetOrderInLayer_m09E8144307532A7FA8F0946D35CFBA7350D0D285 (void);
// 0x0000000C System.Void CrazyMinnow.SALSA.RandomEyes2D::SetLookTargetTracking(UnityEngine.GameObject)
extern void RandomEyes2D_SetLookTargetTracking_m3B736925D0B69176837D0D89485EB6748354ECE9 (void);
// 0x0000000D System.Void CrazyMinnow.SALSA.RandomEyes2D::SetLookTarget(UnityEngine.GameObject)
extern void RandomEyes2D_SetLookTarget_mBFA754D52C5FB53AEED97FA40A287CEC35A2C520 (void);
// 0x0000000E System.Void CrazyMinnow.SALSA.RandomEyes2D::SetTargetAffinity(System.Boolean)
extern void RandomEyes2D_SetTargetAffinity_m01D20750B2E1E15CA5326459B402A45B00F94F5C (void);
// 0x0000000F System.Void CrazyMinnow.SALSA.RandomEyes2D::SetAffinityPercentage(System.Single)
extern void RandomEyes2D_SetAffinityPercentage_mCE3C8F69ABB166059CDCA2D0D2CFC935A2B5E694 (void);
// 0x00000010 System.Void CrazyMinnow.SALSA.RandomEyes2D::SetRandomEyes(System.Boolean)
extern void RandomEyes2D_SetRandomEyes_m422A7C480E27AC8B5D3F6DB4F19CFFAD52E74E8B (void);
// 0x00000011 System.Void CrazyMinnow.SALSA.RandomEyes2D::SetBlink(System.Boolean)
extern void RandomEyes2D_SetBlink_mFBF167619C503F29BE10F9050A91177B3B6DE8B9 (void);
// 0x00000012 System.Void CrazyMinnow.SALSA.RandomEyes2D::SetBlinkDuration(System.Single)
extern void RandomEyes2D_SetBlinkDuration_m4A0D9E4B1A05916535B39E2217ADEA2EF39D33A7 (void);
// 0x00000013 System.Void CrazyMinnow.SALSA.RandomEyes2D::.ctor()
extern void RandomEyes2D__ctor_m03558EFA8B83C41A3C01B3250F2DA35FE7DC2FC0 (void);
// 0x00000014 System.Boolean CrazyMinnow.SALSA.RandomEyes2D/<TargetAffinityUpdate>d__0::MoveNext()
extern void U3CTargetAffinityUpdateU3Ed__0_MoveNext_mB4D2EE738CF3817FB48D0C654ADEB4A568B8D13C (void);
// 0x00000015 System.Object CrazyMinnow.SALSA.RandomEyes2D/<TargetAffinityUpdate>d__0::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CTargetAffinityUpdateU3Ed__0_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m8D0D3B955C4C2F3FCBBAD3D5FA4A2EE44ADA142D (void);
// 0x00000016 System.Void CrazyMinnow.SALSA.RandomEyes2D/<TargetAffinityUpdate>d__0::System.Collections.IEnumerator.Reset()
extern void U3CTargetAffinityUpdateU3Ed__0_System_Collections_IEnumerator_Reset_mED26C7B767350312099E767FA28653FB03743451 (void);
// 0x00000017 System.Void CrazyMinnow.SALSA.RandomEyes2D/<TargetAffinityUpdate>d__0::System.IDisposable.Dispose()
extern void U3CTargetAffinityUpdateU3Ed__0_System_IDisposable_Dispose_m9C4EEA564DC977DB7BB9E05AFF7F66C3627BD119 (void);
// 0x00000018 System.Object CrazyMinnow.SALSA.RandomEyes2D/<TargetAffinityUpdate>d__0::System.Collections.IEnumerator.get_Current()
extern void U3CTargetAffinityUpdateU3Ed__0_System_Collections_IEnumerator_get_Current_m681473C79048E8CF76C118B34813BB9D67BB552A (void);
// 0x00000019 System.Void CrazyMinnow.SALSA.RandomEyes2D/<TargetAffinityUpdate>d__0::.ctor(System.Int32)
extern void U3CTargetAffinityUpdateU3Ed__0__ctor_m11474126BC59372CD200224F8F86C2D80D12BD0C (void);
// 0x0000001A System.Boolean CrazyMinnow.SALSA.RandomEyes2D/<BlinkEyes>d__2::MoveNext()
extern void U3CBlinkEyesU3Ed__2_MoveNext_m89F92B1E6FB7BEE6A9A504CC3431FF670C19B14B (void);
// 0x0000001B System.Object CrazyMinnow.SALSA.RandomEyes2D/<BlinkEyes>d__2::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CBlinkEyesU3Ed__2_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m81D3332B40A695A9AA1FDE3E8B85B2139144A02C (void);
// 0x0000001C System.Void CrazyMinnow.SALSA.RandomEyes2D/<BlinkEyes>d__2::System.Collections.IEnumerator.Reset()
extern void U3CBlinkEyesU3Ed__2_System_Collections_IEnumerator_Reset_m88EBA7BE8E25F597E063CB1264198186B11DEBCB (void);
// 0x0000001D System.Void CrazyMinnow.SALSA.RandomEyes2D/<BlinkEyes>d__2::System.IDisposable.Dispose()
extern void U3CBlinkEyesU3Ed__2_System_IDisposable_Dispose_m617DA6BE6487FDE9E084BA9A3155DED4DD7FA55A (void);
// 0x0000001E System.Object CrazyMinnow.SALSA.RandomEyes2D/<BlinkEyes>d__2::System.Collections.IEnumerator.get_Current()
extern void U3CBlinkEyesU3Ed__2_System_Collections_IEnumerator_get_Current_mA4EBBC50009E04F0472056FDA043C8E273572F7A (void);
// 0x0000001F System.Void CrazyMinnow.SALSA.RandomEyes2D/<BlinkEyes>d__2::.ctor(System.Int32)
extern void U3CBlinkEyesU3Ed__2__ctor_m794D2B438E817C03906B33BA6CEA16630030D920 (void);
// 0x00000020 System.Void CrazyMinnow.SALSA.RandomEyes3D::Reset()
extern void RandomEyes3D_Reset_m7D9EB1391455DBDBD702534215BD097180AE6FC3 (void);
// 0x00000021 System.Void CrazyMinnow.SALSA.RandomEyes3D::Start()
extern void RandomEyes3D_Start_m29BD63BB4AB6AB55143C09550A5FE0476EDF538C (void);
// 0x00000022 System.Void CrazyMinnow.SALSA.RandomEyes3D::OnEnable()
extern void RandomEyes3D_OnEnable_mE7DAF2DEF3455C3BB7ED38E8DEC2EEE9C1D204B2 (void);
// 0x00000023 System.Void CrazyMinnow.SALSA.RandomEyes3D::LateUpdate()
extern void RandomEyes3D_LateUpdate_m87C843FC895A9CA260ADEABEDC307D030EAA5AE9 (void);
// 0x00000024 System.Void CrazyMinnow.SALSA.RandomEyes3D::LookTracking(UnityEngine.Vector3)
extern void RandomEyes3D_LookTracking_m7F29611D970378663A41B66374456DDAEDCC3205 (void);
// 0x00000025 System.Collections.IEnumerator CrazyMinnow.SALSA.RandomEyes3D::TargetAffinityUpdate()
extern void RandomEyes3D_TargetAffinityUpdate_mF7439C88C6DF7E156DBF3E40D67C57EC64EF3E77 (void);
// 0x00000026 System.Void CrazyMinnow.SALSA.RandomEyes3D::DisableAllCustomShapes()
extern void RandomEyes3D_DisableAllCustomShapes_mDA8F4E0E351329FAD3ACC8BD9E13D3D70D0C44CD (void);
// 0x00000027 System.Collections.IEnumerator CrazyMinnow.SALSA.RandomEyes3D::BlinkEyes(System.Single)
extern void RandomEyes3D_BlinkEyes_mDFDFEB7B2811CA1009B0A4D55E6E71CD4433ED6A (void);
// 0x00000028 System.Void CrazyMinnow.SALSA.RandomEyes3D::SetCustomShapesAllNotRandom(System.Boolean)
extern void RandomEyes3D_SetCustomShapesAllNotRandom_m6E09D075DA414527EC49BE94F6FE650C144D8587 (void);
// 0x00000029 System.Collections.IEnumerator CrazyMinnow.SALSA.RandomEyes3D::SetCustomShapeOverrideDuration(System.String,System.Single)
extern void RandomEyes3D_SetCustomShapeOverrideDuration_m0A72CCB00C2DB1FBFFE2A7CBB1B85685B0EA52CC (void);
// 0x0000002A System.Collections.IEnumerator CrazyMinnow.SALSA.RandomEyes3D::SetCustomShapeOverrideDuration(System.Int32,System.Single)
extern void RandomEyes3D_SetCustomShapeOverrideDuration_m07722506C2734D00B002B64D6BD1FE3EE9908F69 (void);
// 0x0000002B System.Collections.IEnumerator CrazyMinnow.SALSA.RandomEyes3D::SetCustomShapeDuration(System.String,System.Single)
extern void RandomEyes3D_SetCustomShapeDuration_mC9EFA2C619003EB530936AF8AC02019814A65099 (void);
// 0x0000002C System.Collections.IEnumerator CrazyMinnow.SALSA.RandomEyes3D::SetCustomShapeDuration(System.Int32,System.Single)
extern void RandomEyes3D_SetCustomShapeDuration_m3B9A380330E625EA5EB3E911590D6CEF499B6F53 (void);
// 0x0000002D System.Void CrazyMinnow.SALSA.RandomEyes3D::CustomShapeChanged(CrazyMinnow.SALSA.RandomEyesCustomShapeStatus)
extern void RandomEyes3D_CustomShapeChanged_m380A0E30479AFAD7D59CA1222BB6E77DCD1FF068 (void);
// 0x0000002E System.Void CrazyMinnow.SALSA.RandomEyes3D::SetCustomShapeExcludeEyes(System.Boolean)
extern void RandomEyes3D_SetCustomShapeExcludeEyes_m2907E3D9B857200BD4368EB62CCC3A0B4D51C436 (void);
// 0x0000002F System.Void CrazyMinnow.SALSA.RandomEyes3D::SetCustomShapeExcludeMouth(CrazyMinnow.SALSA.Salsa3D)
extern void RandomEyes3D_SetCustomShapeExcludeMouth_m3063985221F372102DC1B3854A31D75F95655684 (void);
// 0x00000030 System.Void CrazyMinnow.SALSA.RandomEyes3D::SetRangeOfMotion(System.Single)
extern void RandomEyes3D_SetRangeOfMotion_m284CEE173D95F6A663474206D141963CDDAFAB3C (void);
// 0x00000031 System.Void CrazyMinnow.SALSA.RandomEyes3D::SetBlendSpeed(System.Single)
extern void RandomEyes3D_SetBlendSpeed_m6FAD94D8045D839F3E0B9D49FE88591FD72610E9 (void);
// 0x00000032 System.Void CrazyMinnow.SALSA.RandomEyes3D::Blink(System.Single)
extern void RandomEyes3D_Blink_mCCC507CFFA092075E2BFAF242D716F81A073771C (void);
// 0x00000033 System.Collections.IEnumerator CrazyMinnow.SALSA.RandomEyes3D::DoBlink(System.Single)
extern void RandomEyes3D_DoBlink_m82BC786A5A89543456B234AAD2FC795DD86698FC (void);
// 0x00000034 System.Void CrazyMinnow.SALSA.RandomEyes3D::SetLookTargetTracking(UnityEngine.GameObject)
extern void RandomEyes3D_SetLookTargetTracking_m8DC063699D644B556133DD4013E7BB99C95572EC (void);
// 0x00000035 System.Void CrazyMinnow.SALSA.RandomEyes3D::SetLookTarget(UnityEngine.GameObject)
extern void RandomEyes3D_SetLookTarget_m37D2E133D824526AC0FC64BBA65B40865592B4B8 (void);
// 0x00000036 System.Void CrazyMinnow.SALSA.RandomEyes3D::SetTargetAffinity(System.Boolean)
extern void RandomEyes3D_SetTargetAffinity_m2946B7CB73E8C8C336CD612B46DB2289F5896D69 (void);
// 0x00000037 System.Void CrazyMinnow.SALSA.RandomEyes3D::SetAffinityPercentage(System.Single)
extern void RandomEyes3D_SetAffinityPercentage_mB5F8839B49735773F154CECB55A4A945127EBABC (void);
// 0x00000038 System.Void CrazyMinnow.SALSA.RandomEyes3D::SetRandomEyes(System.Boolean)
extern void RandomEyes3D_SetRandomEyes_mD372850D2928E9D46B8E385E7F5C0C738CD4A618 (void);
// 0x00000039 System.Void CrazyMinnow.SALSA.RandomEyes3D::SetBlink(System.Boolean)
extern void RandomEyes3D_SetBlink_mC54B3B3E6FEC4B3C0F1558051F70124EE7EE1EFA (void);
// 0x0000003A System.Void CrazyMinnow.SALSA.RandomEyes3D::SetBlinkDuration(System.Single)
extern void RandomEyes3D_SetBlinkDuration_m58E1BE83D084596998E0876B051021D7701ECBEE (void);
// 0x0000003B System.Void CrazyMinnow.SALSA.RandomEyes3D::SetBlinkSpeed(System.Single)
extern void RandomEyes3D_SetBlinkSpeed_m154A39F053782E03ECB5B5DF26CB4E956D9C112A (void);
// 0x0000003C System.Void CrazyMinnow.SALSA.RandomEyes3D::SetOpenMax(System.Single)
extern void RandomEyes3D_SetOpenMax_m2277B51661364A14724610455A8E8464576FA51E (void);
// 0x0000003D System.Void CrazyMinnow.SALSA.RandomEyes3D::SetCloseMax(System.Single)
extern void RandomEyes3D_SetCloseMax_mB3143FFFD37CDB7D07A4DBF7306A27119607BF41 (void);
// 0x0000003E System.Void CrazyMinnow.SALSA.RandomEyes3D::SetCustomShapeRandom(System.Boolean)
extern void RandomEyes3D_SetCustomShapeRandom_m55793E8EE1AB7B795CA82B0B2A71148A7072B39F (void);
// 0x0000003F System.Void CrazyMinnow.SALSA.RandomEyes3D::SetCustomShapeOverride(System.String,System.Boolean)
extern void RandomEyes3D_SetCustomShapeOverride_m23761F4B53F0E1A29A36DFFF91801AD211032AC1 (void);
// 0x00000040 System.Void CrazyMinnow.SALSA.RandomEyes3D::SetCustomShapeOverride(System.Int32,System.Boolean)
extern void RandomEyes3D_SetCustomShapeOverride_m98357978832E89B5E5B1FD37F2702911980DCD0A (void);
// 0x00000041 System.Void CrazyMinnow.SALSA.RandomEyes3D::SetCustomShapeOverride(System.String,System.Single,System.Single,System.Boolean)
extern void RandomEyes3D_SetCustomShapeOverride_m9C8E2FDBF2AC9F768DADF1EF2853823C01937A52 (void);
// 0x00000042 System.Void CrazyMinnow.SALSA.RandomEyes3D::SetCustomShapeOverride(System.Int32,System.Single,System.Single,System.Boolean)
extern void RandomEyes3D_SetCustomShapeOverride_m5C3C269C3763197AC91B6053CA0F7EAF1E5CF4F3 (void);
// 0x00000043 System.Void CrazyMinnow.SALSA.RandomEyes3D::SetCustomShapeOverride(System.String,System.Single)
extern void RandomEyes3D_SetCustomShapeOverride_mA4614D96A02DF336129C3255D4866EC9499AC131 (void);
// 0x00000044 System.Void CrazyMinnow.SALSA.RandomEyes3D::SetCustomShapeOverride(System.Int32,System.Single)
extern void RandomEyes3D_SetCustomShapeOverride_mB89253EB0B19EEA2BEDCFE5D5628A7C30F4EAD8E (void);
// 0x00000045 System.Void CrazyMinnow.SALSA.RandomEyes3D::SetCustomShapeOverride(System.String,System.Single,System.Single,System.Single)
extern void RandomEyes3D_SetCustomShapeOverride_mAC24BBA3E20FC41A96F383097AD90C35E0EA7066 (void);
// 0x00000046 System.Void CrazyMinnow.SALSA.RandomEyes3D::SetCustomShapeOverride(System.Int32,System.Single,System.Single,System.Single)
extern void RandomEyes3D_SetCustomShapeOverride_m05639C6FFFB925FE08A91D34C92678F1C851758A (void);
// 0x00000047 System.Void CrazyMinnow.SALSA.RandomEyes3D::SetCustomShapeBlendSpeed(System.String,System.Single)
extern void RandomEyes3D_SetCustomShapeBlendSpeed_mE6C02207767F40A39806CEEFB76C1E90FC6B5693 (void);
// 0x00000048 System.Void CrazyMinnow.SALSA.RandomEyes3D::SetCustomShapeBlendSpeed(System.Int32,System.Single)
extern void RandomEyes3D_SetCustomShapeBlendSpeed_mE243E3BC51581AC08B9F57ADA68C3E7798D18B09 (void);
// 0x00000049 System.Void CrazyMinnow.SALSA.RandomEyes3D::SetCustomShapeRangeOfMotion(System.String,System.Single)
extern void RandomEyes3D_SetCustomShapeRangeOfMotion_m203068B98D9D262BB4D6B2C8AEECE0875FF4AA14 (void);
// 0x0000004A System.Void CrazyMinnow.SALSA.RandomEyes3D::SetCustomShapeRangeOfMotion(System.Int32,System.Single)
extern void RandomEyes3D_SetCustomShapeRangeOfMotion_m74054909B1069B8527597D1285B516B980955B67 (void);
// 0x0000004B System.Void CrazyMinnow.SALSA.RandomEyes3D::SetCustomShape(System.String)
extern void RandomEyes3D_SetCustomShape_m0B61CBCBC3389E335E869448A557D96549915D36 (void);
// 0x0000004C System.Void CrazyMinnow.SALSA.RandomEyes3D::SetCustomShape(System.Int32)
extern void RandomEyes3D_SetCustomShape_m46A96B9C7709FD6B8790B63AF40F71C00927A4A2 (void);
// 0x0000004D System.Void CrazyMinnow.SALSA.RandomEyes3D::SetCustomShape(System.String,System.Single)
extern void RandomEyes3D_SetCustomShape_m00C34809EE3C77CE110E8F6CCFECA9F0ECE2DB11 (void);
// 0x0000004E System.Void CrazyMinnow.SALSA.RandomEyes3D::SetCustomShape(System.Int32,System.Single)
extern void RandomEyes3D_SetCustomShape_mC41F70839B2458EDFC63410F9C534DC53A775177 (void);
// 0x0000004F System.Void CrazyMinnow.SALSA.RandomEyes3D::SetGroup(System.String,System.Single)
extern void RandomEyes3D_SetGroup_m36FB61B99AB52F36F2A82B7D61E2D71BB781FCC4 (void);
// 0x00000050 System.Void CrazyMinnow.SALSA.RandomEyes3D::SetGroup(System.String,System.Boolean)
extern void RandomEyes3D_SetGroup_mED0933BCC89904D06A34464FFF8CF78FFD3B3B1E (void);
// 0x00000051 System.Void CrazyMinnow.SALSA.RandomEyes3D::GetBlendShapes()
extern void RandomEyes3D_GetBlendShapes_mF9F69C143A9AF121E534B05AC0F6EAD505765562 (void);
// 0x00000052 System.Void CrazyMinnow.SALSA.RandomEyes3D::AutoLinkEyes()
extern void RandomEyes3D_AutoLinkEyes_m7AFABE4BEF07CBD9C860F545823CC8856A3B08A4 (void);
// 0x00000053 System.Void CrazyMinnow.SALSA.RandomEyes3D::AutoLinkCustomShapes(System.Boolean,CrazyMinnow.SALSA.Salsa3D)
extern void RandomEyes3D_AutoLinkCustomShapes_m9D8A263AA469EDC6301A778EFCAF60B3BFD34359 (void);
// 0x00000054 System.Int32 CrazyMinnow.SALSA.RandomEyes3D::RebuildCurrentCustomShapeList()
extern void RandomEyes3D_RebuildCurrentCustomShapeList_m645926658AF05E208117F2A4D0EAA998DBD27348 (void);
// 0x00000055 System.Void CrazyMinnow.SALSA.RandomEyes3D::FindOrCreateEyePositionGizmo()
extern void RandomEyes3D_FindOrCreateEyePositionGizmo_m62C69039D32AC352E5A274B9C560E2625DE8BE74 (void);
// 0x00000056 System.Void CrazyMinnow.SALSA.RandomEyes3D::.ctor()
extern void RandomEyes3D__ctor_mBCB53DB419FB67074996B3785C1F705D12CA80EF (void);
// 0x00000057 System.Boolean CrazyMinnow.SALSA.RandomEyes3D/<TargetAffinityUpdate>d__0::MoveNext()
extern void U3CTargetAffinityUpdateU3Ed__0_MoveNext_m6C8737D2049E5CA630C97961F714DDF5D1F5B9B5 (void);
// 0x00000058 System.Object CrazyMinnow.SALSA.RandomEyes3D/<TargetAffinityUpdate>d__0::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CTargetAffinityUpdateU3Ed__0_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m2327362FC616DD432555C7E25749B63137D99477 (void);
// 0x00000059 System.Void CrazyMinnow.SALSA.RandomEyes3D/<TargetAffinityUpdate>d__0::System.Collections.IEnumerator.Reset()
extern void U3CTargetAffinityUpdateU3Ed__0_System_Collections_IEnumerator_Reset_mBAE456A3C0C96A0C8B70B0D32F3D345A0C07CA1E (void);
// 0x0000005A System.Void CrazyMinnow.SALSA.RandomEyes3D/<TargetAffinityUpdate>d__0::System.IDisposable.Dispose()
extern void U3CTargetAffinityUpdateU3Ed__0_System_IDisposable_Dispose_m6DB637D4B9B6B5D7278321DA86C71BB5BBE1951F (void);
// 0x0000005B System.Object CrazyMinnow.SALSA.RandomEyes3D/<TargetAffinityUpdate>d__0::System.Collections.IEnumerator.get_Current()
extern void U3CTargetAffinityUpdateU3Ed__0_System_Collections_IEnumerator_get_Current_m348B9921D9DFEA1779B61D9EA42F31C06F6A00BB (void);
// 0x0000005C System.Void CrazyMinnow.SALSA.RandomEyes3D/<TargetAffinityUpdate>d__0::.ctor(System.Int32)
extern void U3CTargetAffinityUpdateU3Ed__0__ctor_mCAFDF47555144A310E4F743A29A8C68AAAA8FDFB (void);
// 0x0000005D System.Boolean CrazyMinnow.SALSA.RandomEyes3D/<BlinkEyes>d__2::MoveNext()
extern void U3CBlinkEyesU3Ed__2_MoveNext_mD6E6B0ECBC82B2E9C31F22D8F7DEB26604A96FDE (void);
// 0x0000005E System.Object CrazyMinnow.SALSA.RandomEyes3D/<BlinkEyes>d__2::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CBlinkEyesU3Ed__2_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mB5FD936BCB45081E3D0AD94D05AB1EA32D510B74 (void);
// 0x0000005F System.Void CrazyMinnow.SALSA.RandomEyes3D/<BlinkEyes>d__2::System.Collections.IEnumerator.Reset()
extern void U3CBlinkEyesU3Ed__2_System_Collections_IEnumerator_Reset_mD5DCBE6D12F53C662EF106666A000BEB03ED3558 (void);
// 0x00000060 System.Void CrazyMinnow.SALSA.RandomEyes3D/<BlinkEyes>d__2::System.IDisposable.Dispose()
extern void U3CBlinkEyesU3Ed__2_System_IDisposable_Dispose_m2DBE82FEB6DF1FEDBCBE44AA8BBA27D6A63250BC (void);
// 0x00000061 System.Object CrazyMinnow.SALSA.RandomEyes3D/<BlinkEyes>d__2::System.Collections.IEnumerator.get_Current()
extern void U3CBlinkEyesU3Ed__2_System_Collections_IEnumerator_get_Current_mF267239843A192233380FE5A3DA1DA92BA248B7E (void);
// 0x00000062 System.Void CrazyMinnow.SALSA.RandomEyes3D/<BlinkEyes>d__2::.ctor(System.Int32)
extern void U3CBlinkEyesU3Ed__2__ctor_m1C070BB710ACF1198445B49A02B0EE406A56F50C (void);
// 0x00000063 System.Boolean CrazyMinnow.SALSA.RandomEyes3D/<SetCustomShapeOverrideDuration>d__4::MoveNext()
extern void U3CSetCustomShapeOverrideDurationU3Ed__4_MoveNext_mF967AD900DA0BB3DACEB9E8EC01074D0C512490F (void);
// 0x00000064 System.Object CrazyMinnow.SALSA.RandomEyes3D/<SetCustomShapeOverrideDuration>d__4::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CSetCustomShapeOverrideDurationU3Ed__4_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m0DB8C66E0B7DC354D7A4096727A9F719FA17B455 (void);
// 0x00000065 System.Void CrazyMinnow.SALSA.RandomEyes3D/<SetCustomShapeOverrideDuration>d__4::System.Collections.IEnumerator.Reset()
extern void U3CSetCustomShapeOverrideDurationU3Ed__4_System_Collections_IEnumerator_Reset_mAD496E65677155FF44B6B02F4850449E59AD4F75 (void);
// 0x00000066 System.Void CrazyMinnow.SALSA.RandomEyes3D/<SetCustomShapeOverrideDuration>d__4::System.IDisposable.Dispose()
extern void U3CSetCustomShapeOverrideDurationU3Ed__4_System_IDisposable_Dispose_m8201BD769FB409291915C1E2559C9DF0CE067234 (void);
// 0x00000067 System.Object CrazyMinnow.SALSA.RandomEyes3D/<SetCustomShapeOverrideDuration>d__4::System.Collections.IEnumerator.get_Current()
extern void U3CSetCustomShapeOverrideDurationU3Ed__4_System_Collections_IEnumerator_get_Current_mD22F300FB60FBC84ACA05B07F1CC27E4ECFFCE7C (void);
// 0x00000068 System.Void CrazyMinnow.SALSA.RandomEyes3D/<SetCustomShapeOverrideDuration>d__4::.ctor(System.Int32)
extern void U3CSetCustomShapeOverrideDurationU3Ed__4__ctor_mB66C5B43F2C15F540F85F1845A71DB41035E6FAD (void);
// 0x00000069 System.Boolean CrazyMinnow.SALSA.RandomEyes3D/<SetCustomShapeOverrideDuration>d__6::MoveNext()
extern void U3CSetCustomShapeOverrideDurationU3Ed__6_MoveNext_m63E68EDC3982D66FE7182E3E87F877F2DD3E4ABF (void);
// 0x0000006A System.Object CrazyMinnow.SALSA.RandomEyes3D/<SetCustomShapeOverrideDuration>d__6::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CSetCustomShapeOverrideDurationU3Ed__6_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mC45AF0F2DFB5CC2AB9A7B2674BE4416D56C54330 (void);
// 0x0000006B System.Void CrazyMinnow.SALSA.RandomEyes3D/<SetCustomShapeOverrideDuration>d__6::System.Collections.IEnumerator.Reset()
extern void U3CSetCustomShapeOverrideDurationU3Ed__6_System_Collections_IEnumerator_Reset_m248F5D80FBFAA6C850D4680977E43D02F8E5F7AC (void);
// 0x0000006C System.Void CrazyMinnow.SALSA.RandomEyes3D/<SetCustomShapeOverrideDuration>d__6::System.IDisposable.Dispose()
extern void U3CSetCustomShapeOverrideDurationU3Ed__6_System_IDisposable_Dispose_mE23ACB37AE186DEDA95535FB3720E8359D308E07 (void);
// 0x0000006D System.Object CrazyMinnow.SALSA.RandomEyes3D/<SetCustomShapeOverrideDuration>d__6::System.Collections.IEnumerator.get_Current()
extern void U3CSetCustomShapeOverrideDurationU3Ed__6_System_Collections_IEnumerator_get_Current_mFD60724EFEB1942BDF96085D0C38219DC9796378 (void);
// 0x0000006E System.Void CrazyMinnow.SALSA.RandomEyes3D/<SetCustomShapeOverrideDuration>d__6::.ctor(System.Int32)
extern void U3CSetCustomShapeOverrideDurationU3Ed__6__ctor_m90B95A85DE3597BE73729C4BD7FF6D9756F8D6EB (void);
// 0x0000006F System.Boolean CrazyMinnow.SALSA.RandomEyes3D/<SetCustomShapeDuration>d__8::MoveNext()
extern void U3CSetCustomShapeDurationU3Ed__8_MoveNext_mF5A8B9DF3C76678C8ED39451F03A21D4500F39BF (void);
// 0x00000070 System.Object CrazyMinnow.SALSA.RandomEyes3D/<SetCustomShapeDuration>d__8::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CSetCustomShapeDurationU3Ed__8_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m1DD68496D286084B27BA2709A378BF2620CF9534 (void);
// 0x00000071 System.Void CrazyMinnow.SALSA.RandomEyes3D/<SetCustomShapeDuration>d__8::System.Collections.IEnumerator.Reset()
extern void U3CSetCustomShapeDurationU3Ed__8_System_Collections_IEnumerator_Reset_mA93C2810F2DFD6BB6409274C433C796616BD935F (void);
// 0x00000072 System.Void CrazyMinnow.SALSA.RandomEyes3D/<SetCustomShapeDuration>d__8::System.IDisposable.Dispose()
extern void U3CSetCustomShapeDurationU3Ed__8_System_IDisposable_Dispose_m0EB432743B895C26FA9451C2F70D14108A12DBBE (void);
// 0x00000073 System.Object CrazyMinnow.SALSA.RandomEyes3D/<SetCustomShapeDuration>d__8::System.Collections.IEnumerator.get_Current()
extern void U3CSetCustomShapeDurationU3Ed__8_System_Collections_IEnumerator_get_Current_m655AA5A40FE04228B897FF3815520CE581E9C3DC (void);
// 0x00000074 System.Void CrazyMinnow.SALSA.RandomEyes3D/<SetCustomShapeDuration>d__8::.ctor(System.Int32)
extern void U3CSetCustomShapeDurationU3Ed__8__ctor_mF6CD6A348AAE88971E18E1E4E94D43FF9A964EB3 (void);
// 0x00000075 System.Boolean CrazyMinnow.SALSA.RandomEyes3D/<SetCustomShapeDuration>d__a::MoveNext()
extern void U3CSetCustomShapeDurationU3Ed__a_MoveNext_m80EA5F24116A83527DAA55FD2D166CFF55C1457C (void);
// 0x00000076 System.Object CrazyMinnow.SALSA.RandomEyes3D/<SetCustomShapeDuration>d__a::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CSetCustomShapeDurationU3Ed__a_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m29AD9BF27A7FF383D8C75DEA1AD987DCB65A24E5 (void);
// 0x00000077 System.Void CrazyMinnow.SALSA.RandomEyes3D/<SetCustomShapeDuration>d__a::System.Collections.IEnumerator.Reset()
extern void U3CSetCustomShapeDurationU3Ed__a_System_Collections_IEnumerator_Reset_m6AD8EF97A2799112F845813343FD577CA1698CF7 (void);
// 0x00000078 System.Void CrazyMinnow.SALSA.RandomEyes3D/<SetCustomShapeDuration>d__a::System.IDisposable.Dispose()
extern void U3CSetCustomShapeDurationU3Ed__a_System_IDisposable_Dispose_m9D8F38C3E7B44E6DE7815CC7D32D11D27EEF8D03 (void);
// 0x00000079 System.Object CrazyMinnow.SALSA.RandomEyes3D/<SetCustomShapeDuration>d__a::System.Collections.IEnumerator.get_Current()
extern void U3CSetCustomShapeDurationU3Ed__a_System_Collections_IEnumerator_get_Current_m3FCCF65FB5B696321DFF75279636D1A8666D7DF7 (void);
// 0x0000007A System.Void CrazyMinnow.SALSA.RandomEyes3D/<SetCustomShapeDuration>d__a::.ctor(System.Int32)
extern void U3CSetCustomShapeDurationU3Ed__a__ctor_m22ECCEFDEC5DB0D1080451AE79B9E22EC5A618EB (void);
// 0x0000007B System.Boolean CrazyMinnow.SALSA.RandomEyes3D/<DoBlink>d__c::MoveNext()
extern void U3CDoBlinkU3Ed__c_MoveNext_m6FE3368A1FA11FF8B8A8DF812B8ED7EC26F6EB6E (void);
// 0x0000007C System.Object CrazyMinnow.SALSA.RandomEyes3D/<DoBlink>d__c::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CDoBlinkU3Ed__c_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m401CF5F735EAD27CA82731FA9AAC660CEB51F4CA (void);
// 0x0000007D System.Void CrazyMinnow.SALSA.RandomEyes3D/<DoBlink>d__c::System.Collections.IEnumerator.Reset()
extern void U3CDoBlinkU3Ed__c_System_Collections_IEnumerator_Reset_m04161B4126710078525F0D3F99E46A39A538B5B6 (void);
// 0x0000007E System.Void CrazyMinnow.SALSA.RandomEyes3D/<DoBlink>d__c::System.IDisposable.Dispose()
extern void U3CDoBlinkU3Ed__c_System_IDisposable_Dispose_m700124204B78C1A7F4A5BD798ECE2D22492C7FB6 (void);
// 0x0000007F System.Object CrazyMinnow.SALSA.RandomEyes3D/<DoBlink>d__c::System.Collections.IEnumerator.get_Current()
extern void U3CDoBlinkU3Ed__c_System_Collections_IEnumerator_get_Current_mA6C9A533B91FE9022C5929D1F59EA6849129B59D (void);
// 0x00000080 System.Void CrazyMinnow.SALSA.RandomEyes3D/<DoBlink>d__c::.ctor(System.Int32)
extern void U3CDoBlinkU3Ed__c__ctor_m9D9176B98A436D63C7C71A253F9242C3ACD37F00 (void);
// 0x00000081 System.Void CrazyMinnow.SALSA.RandomEyes3DGizmo::OnDrawGizmos()
extern void RandomEyes3DGizmo_OnDrawGizmos_m3E702ED02773C0CB2431D737D0193D15D6AFD8EC (void);
// 0x00000082 System.Void CrazyMinnow.SALSA.RandomEyes3DGizmo::.ctor()
extern void RandomEyes3DGizmo__ctor_m31CB5D4DA8919EA40F7E37474FA16BDBB467DAEC (void);
// 0x00000083 System.Void CrazyMinnow.SALSA.Salsa2D::Reset()
extern void Salsa2D_Reset_mF278D642863E4E5FAC9DDA581B0CFB88B9A6C595 (void);
// 0x00000084 System.Void CrazyMinnow.SALSA.Salsa2D::Awake()
extern void Salsa2D_Awake_m9C980FB4ACD64F6B9CD5E64F3D5781CEAF296448 (void);
// 0x00000085 System.Void CrazyMinnow.SALSA.Salsa2D::OnEnable()
extern void Salsa2D_OnEnable_mEAB9811C8A6709133703DC6BA21718A4CCFF5BE8 (void);
// 0x00000086 System.Void CrazyMinnow.SALSA.Salsa2D::LateUpdate()
extern void Salsa2D_LateUpdate_m3D28055ADA1D7F1AAB095581F94054E16BF874BC (void);
// 0x00000087 System.Void CrazyMinnow.SALSA.Salsa2D::TalkStatusChanged()
extern void Salsa2D_TalkStatusChanged_m7EF42D46D528EA86DB9E59DF5FD9C105387EA97C (void);
// 0x00000088 System.Void CrazyMinnow.SALSA.Salsa2D::UpdateShape()
extern void Salsa2D_UpdateShape_m81111EF6B2731082FA420939A62870B3B59C3AEF (void);
// 0x00000089 System.Collections.IEnumerator CrazyMinnow.SALSA.Salsa2D::UpdateSample()
extern void Salsa2D_UpdateSample_m3ECFBCCA2C397C22A1B8715F2F43223C469F7563 (void);
// 0x0000008A System.Void CrazyMinnow.SALSA.Salsa2D::ClampValues()
extern void Salsa2D_ClampValues_mF88513A62C56A731A344DD11BDBB0024C8742455 (void);
// 0x0000008B System.Void CrazyMinnow.SALSA.Salsa2D::Play()
extern void Salsa2D_Play_m35EA602BD85046F81EE5B10AECED91841066A1F8 (void);
// 0x0000008C System.Void CrazyMinnow.SALSA.Salsa2D::Pause()
extern void Salsa2D_Pause_m42C21144E3C154C1B2EECEF63D3E4601EA50522C (void);
// 0x0000008D System.Void CrazyMinnow.SALSA.Salsa2D::Stop()
extern void Salsa2D_Stop_mBC22BAF8743751DB23FB66929BD7E648B39BD653 (void);
// 0x0000008E System.Void CrazyMinnow.SALSA.Salsa2D::SetAudioClip(UnityEngine.AudioClip)
extern void Salsa2D_SetAudioClip_m030D997129C505E6F5D787B66B15A032C2F4EC8B (void);
// 0x0000008F System.Void CrazyMinnow.SALSA.Salsa2D::LoadAudioClip(System.String)
extern void Salsa2D_LoadAudioClip_m00E486A9634C76CC9833BE9F8D88D3600F9326F6 (void);
// 0x00000090 UnityEngine.SpriteRenderer CrazyMinnow.SALSA.Salsa2D::FindSpriteRenderer()
extern void Salsa2D_FindSpriteRenderer_m568528C27E3DA94591E872864EFF993F32B11D0E (void);
// 0x00000091 UnityEngine.AudioSource CrazyMinnow.SALSA.Salsa2D::FindAudioSource()
extern void Salsa2D_FindAudioSource_m4F909704BCA8FAD1285CB91F23783882DFD618C4 (void);
// 0x00000092 System.Void CrazyMinnow.SALSA.Salsa2D::SetOrderInLayer(System.Int32)
extern void Salsa2D_SetOrderInLayer_m2F1C9424774C0A54279E6AC5848890A0C365E834 (void);
// 0x00000093 System.Void CrazyMinnow.SALSA.Salsa2D::.ctor()
extern void Salsa2D__ctor_mCF46C454E09DDAE70FF51E82150EED81A9FC8094 (void);
// 0x00000094 System.Boolean CrazyMinnow.SALSA.Salsa2D/<UpdateSample>d__0::MoveNext()
extern void U3CUpdateSampleU3Ed__0_MoveNext_m96AE4867D7BA9B7FB3F37F0FF06599F22D0780EB (void);
// 0x00000095 System.Object CrazyMinnow.SALSA.Salsa2D/<UpdateSample>d__0::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CUpdateSampleU3Ed__0_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m1DE327FA8E703A2453E6E02DE2D379EF29B0353A (void);
// 0x00000096 System.Void CrazyMinnow.SALSA.Salsa2D/<UpdateSample>d__0::System.Collections.IEnumerator.Reset()
extern void U3CUpdateSampleU3Ed__0_System_Collections_IEnumerator_Reset_m721FCB06919D945E4656B278C22DB71EBAD5D67B (void);
// 0x00000097 System.Void CrazyMinnow.SALSA.Salsa2D/<UpdateSample>d__0::System.IDisposable.Dispose()
extern void U3CUpdateSampleU3Ed__0_System_IDisposable_Dispose_m279F715E7CC834B20BE21E737211C44321A8B7DF (void);
// 0x00000098 System.Object CrazyMinnow.SALSA.Salsa2D/<UpdateSample>d__0::System.Collections.IEnumerator.get_Current()
extern void U3CUpdateSampleU3Ed__0_System_Collections_IEnumerator_get_Current_mB214E3D8FF3AD5D7552BD9302AB3D5D5B40C79CB (void);
// 0x00000099 System.Void CrazyMinnow.SALSA.Salsa2D/<UpdateSample>d__0::.ctor(System.Int32)
extern void U3CUpdateSampleU3Ed__0__ctor_m455E0D010D62E290992788EA19E42269D2B13A77 (void);
// 0x0000009A System.Void CrazyMinnow.SALSA.Salsa3D::Reset()
extern void Salsa3D_Reset_m53CBF6B12A477D9B56DDDB9486EF2B5C887DED58 (void);
// 0x0000009B System.Void CrazyMinnow.SALSA.Salsa3D::Awake()
extern void Salsa3D_Awake_m532A8AE4ACDD0CCD856228FF388FD17D07F65E58 (void);
// 0x0000009C System.Void CrazyMinnow.SALSA.Salsa3D::OnEnable()
extern void Salsa3D_OnEnable_m85CD4CE8B73639E5D58DD48BEF309F80C27A555F (void);
// 0x0000009D System.Void CrazyMinnow.SALSA.Salsa3D::LateUpdate()
extern void Salsa3D_LateUpdate_mA3E1A807016185BCE0DDDD8E2136A84F89267577 (void);
// 0x0000009E System.Void CrazyMinnow.SALSA.Salsa3D::TalkStatusChanged()
extern void Salsa3D_TalkStatusChanged_m4C6E88F7F47206891F1C4227E68A3896C39254B5 (void);
// 0x0000009F System.Collections.IEnumerator CrazyMinnow.SALSA.Salsa3D::UpdateSample()
extern void Salsa3D_UpdateSample_mB63EC23E06A628ADCF06EEFCE0735FF9D2E0D727 (void);
// 0x000000A0 System.Void CrazyMinnow.SALSA.Salsa3D::GetBlendShapes()
extern void Salsa3D_GetBlendShapes_mAD39B313314A8243FE2172912EC2DD385D1EA751 (void);
// 0x000000A1 System.Void CrazyMinnow.SALSA.Salsa3D::AutoLinkMouth()
extern void Salsa3D_AutoLinkMouth_mC22B8B11A8FD3FB251B7D3550C76312CB7CC3E32 (void);
// 0x000000A2 System.Void CrazyMinnow.SALSA.Salsa3D::Play()
extern void Salsa3D_Play_mEA71E242A27A0C1921CAAA55522985950120F751 (void);
// 0x000000A3 System.Void CrazyMinnow.SALSA.Salsa3D::Pause()
extern void Salsa3D_Pause_m4507436881B8E2F41E18AEB1B8A5C4E8A35DB804 (void);
// 0x000000A4 System.Void CrazyMinnow.SALSA.Salsa3D::Stop()
extern void Salsa3D_Stop_m4362CFD77DCD332D5CFB4224D4E6ACF6B2FB2967 (void);
// 0x000000A5 System.Void CrazyMinnow.SALSA.Salsa3D::SetAudioClip(UnityEngine.AudioClip)
extern void Salsa3D_SetAudioClip_m7E1EAB203B768C23F2B00544C93C92B989703665 (void);
// 0x000000A6 System.Void CrazyMinnow.SALSA.Salsa3D::LoadAudioClip(System.String)
extern void Salsa3D_LoadAudioClip_m6D5FDC03511EDFE828A82FE84E85E888F0FF9398 (void);
// 0x000000A7 UnityEngine.AudioSource CrazyMinnow.SALSA.Salsa3D::FindAudioSource()
extern void Salsa3D_FindAudioSource_mDE3A59BD7075854BE6C62C69780834597C8F2281 (void);
// 0x000000A8 System.Void CrazyMinnow.SALSA.Salsa3D::SetRangeOfMotion(System.Single)
extern void Salsa3D_SetRangeOfMotion_m02D66529248011B5E0C665270F3D5F950748F64D (void);
// 0x000000A9 System.Void CrazyMinnow.SALSA.Salsa3D::SetBlendSpeed(System.Single)
extern void Salsa3D_SetBlendSpeed_m579082426D279F7A6C139F3672C03779C29977B6 (void);
// 0x000000AA System.Void CrazyMinnow.SALSA.Salsa3D::.ctor()
extern void Salsa3D__ctor_m2FC9AF05F5A114DECB757F015343373CBCF4358B (void);
// 0x000000AB System.Boolean CrazyMinnow.SALSA.Salsa3D/<UpdateSample>d__0::MoveNext()
extern void U3CUpdateSampleU3Ed__0_MoveNext_m07D140947B25F0D0BCB80CA6BFD59BD28A7F3F30 (void);
// 0x000000AC System.Object CrazyMinnow.SALSA.Salsa3D/<UpdateSample>d__0::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CUpdateSampleU3Ed__0_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m7D4F83F4B85BEAC2F01C41CF80ACA0A2F23EAAB5 (void);
// 0x000000AD System.Void CrazyMinnow.SALSA.Salsa3D/<UpdateSample>d__0::System.Collections.IEnumerator.Reset()
extern void U3CUpdateSampleU3Ed__0_System_Collections_IEnumerator_Reset_m482BF263E6A170335B23F21B5CA758CC19D9370F (void);
// 0x000000AE System.Void CrazyMinnow.SALSA.Salsa3D/<UpdateSample>d__0::System.IDisposable.Dispose()
extern void U3CUpdateSampleU3Ed__0_System_IDisposable_Dispose_mA953A0205C9175B7DD1F260B4D142FAE87F508CA (void);
// 0x000000AF System.Object CrazyMinnow.SALSA.Salsa3D/<UpdateSample>d__0::System.Collections.IEnumerator.get_Current()
extern void U3CUpdateSampleU3Ed__0_System_Collections_IEnumerator_get_Current_m3B132E8B0A5214B5E01262ADBFC7FD269CC51AA2 (void);
// 0x000000B0 System.Void CrazyMinnow.SALSA.Salsa3D/<UpdateSample>d__0::.ctor(System.Int32)
extern void U3CUpdateSampleU3Ed__0__ctor_m199B19B5576CA91594C9D8529188676BA2B73670 (void);
// 0x000000B1 UnityEngine.Object CrazyMinnow.SALSA.SalsaStatus::get_instance()
extern void SalsaStatus_get_instance_mB63DF2D8EF456D6A9E976AEB43B6C03A77BE4C29 (void);
// 0x000000B2 System.Void CrazyMinnow.SALSA.SalsaStatus::set_instance(UnityEngine.Object)
extern void SalsaStatus_set_instance_mB67A73851E6FDD5B713B5F85017446B672B32F63 (void);
// 0x000000B3 System.String CrazyMinnow.SALSA.SalsaStatus::get_talkerName()
extern void SalsaStatus_get_talkerName_mAE5109304DDE680F4BB760BDC4A60BE9AA29EDD5 (void);
// 0x000000B4 System.Void CrazyMinnow.SALSA.SalsaStatus::set_talkerName(System.String)
extern void SalsaStatus_set_talkerName_m88E92FFF1840D7288C4474685B82F562E5A6C5EE (void);
// 0x000000B5 System.Boolean CrazyMinnow.SALSA.SalsaStatus::get_isTalking()
extern void SalsaStatus_get_isTalking_m5E400E0CDFCB8B3241BE2255144A88E0E0881234 (void);
// 0x000000B6 System.Void CrazyMinnow.SALSA.SalsaStatus::set_isTalking(System.Boolean)
extern void SalsaStatus_set_isTalking_m621FF64AC69982D2A47B901157DB29C7F3EC6725 (void);
// 0x000000B7 System.String CrazyMinnow.SALSA.SalsaStatus::get_clipName()
extern void SalsaStatus_get_clipName_m1D43753B3C3BED26B88D82DBF7F14991D56E2B92 (void);
// 0x000000B8 System.Void CrazyMinnow.SALSA.SalsaStatus::set_clipName(System.String)
extern void SalsaStatus_set_clipName_m564A868F0584865EBA31D5F8DD38FE39B698F72A (void);
// 0x000000B9 System.Void CrazyMinnow.SALSA.SalsaStatus::.ctor()
extern void SalsaStatus__ctor_m60EB1381FDD2EA05C23A1AD9A6005AF854E5E89D (void);
// 0x000000BA UnityEngine.Texture2D CrazyMinnow.SALSA.SalsaUtility::LoadResource(System.String)
extern void SalsaUtility_LoadResource_m83BADDA1A9109E56D955FFA4143062817C97B1B2 (void);
// 0x000000BB System.Single CrazyMinnow.SALSA.SalsaUtility::LerpRangeOfMotion(System.Single,System.Single,System.Single,CrazyMinnow.SALSA.SalsaUtility/BlendDirection)
extern void SalsaUtility_LerpRangeOfMotion_mDA97D8D31F6D62217220938CD785C48A75AEB246 (void);
// 0x000000BC System.Single CrazyMinnow.SALSA.SalsaUtility::Lerp3DEyeTracking(System.Single,System.Single,System.Single)
extern void SalsaUtility_Lerp3DEyeTracking_m914ED29E6BF956DD834E8CF6B8C5B20591A22C19 (void);
// 0x000000BD UnityEngine.Vector3 CrazyMinnow.SALSA.SalsaUtility::Lerp2DEyeTracking(UnityEngine.Vector3,UnityEngine.Vector3,System.Single)
extern void SalsaUtility_Lerp2DEyeTracking_m49C8BAED6D6F4ABD41691661601291A0F20A800D (void);
// 0x000000BE System.Single CrazyMinnow.SALSA.SalsaUtility::ClampValue(System.Single,System.Single)
extern void SalsaUtility_ClampValue_m167E1BE774D495060E1FAA2503DD01D2C00EE208 (void);
// 0x000000BF System.Single CrazyMinnow.SALSA.SalsaUtility::ClampValueMinMax(System.Single,System.Single,System.Single)
extern void SalsaUtility_ClampValueMinMax_mDC645C582746F5E4B0C6D90541D284BF6F9A73FB (void);
// 0x000000C0 System.Single CrazyMinnow.SALSA.SalsaUtility::ScaleRange(System.Single,System.Single,System.Single,System.Single,System.Single)
extern void SalsaUtility_ScaleRange_m58E61AAAA506DE027672DBC1B89A1E874AE4E8A7 (void);
// 0x000000C1 System.Void CrazyMinnow.SALSA.SalsaUtility::.ctor()
extern void SalsaUtility__ctor_m32C37E3AD85E505EF76625DD2918D8CFAB1B6986 (void);
// 0x000000C2 System.Void CrazyMinnow.SALSA.SalsaBlendAmounts::.ctor()
extern void SalsaBlendAmounts__ctor_mA2E926D5E2C432845C8162FAEC2012E860EB94E2 (void);
// 0x000000C3 System.Void CrazyMinnow.SALSA.RandomEyesBlendAmounts::.ctor()
extern void RandomEyesBlendAmounts__ctor_m20EA40B8CB17580CC2E6FB16FF225D40D1CA54FE (void);
// 0x000000C4 System.Void CrazyMinnow.SALSA.Shape::.ctor()
extern void Shape__ctor_mF26431536C0DA3EA973AED80958736B9238D7ADA (void);
// 0x000000C5 System.Void CrazyMinnow.SALSA.Shape::.ctor(System.Int32,System.String,System.Single,System.Single)
extern void Shape__ctor_m8FEA41876A6510E06F593EAC4A0915629895D292 (void);
// 0x000000C6 System.Void CrazyMinnow.SALSA.Group::.ctor()
extern void Group__ctor_mB95A1CB37E84D974BE25697B57B07FBA0BA41A45 (void);
// 0x000000C7 System.Void CrazyMinnow.SALSA.RandomEyes2DGizmo::.ctor()
extern void RandomEyes2DGizmo__ctor_m1B5A6AB31BA24749312A096CBA2D9BE9AF973E9A (void);
// 0x000000C8 System.Void CrazyMinnow.SALSA.RandomEyesCustomShape::.ctor()
extern void RandomEyesCustomShape__ctor_m55E79792EBDFA8E4E88E7396AB6E31327D5D0DBF (void);
// 0x000000C9 UnityEngine.Object CrazyMinnow.SALSA.RandomEyesCustomShapeStatus::get_instance()
extern void RandomEyesCustomShapeStatus_get_instance_m3E3018C5AB25E4037D1E5DD7F84F42337B99229F (void);
// 0x000000CA System.Void CrazyMinnow.SALSA.RandomEyesCustomShapeStatus::set_instance(UnityEngine.Object)
extern void RandomEyesCustomShapeStatus_set_instance_m4F5644F9DB6E38215FA12B1892E396CA0D8281B1 (void);
// 0x000000CB System.Void CrazyMinnow.SALSA.RandomEyesCustomShapeStatus::.ctor()
extern void RandomEyesCustomShapeStatus__ctor_m97BF52FFB152BF60CB27311F72EA547C783657C0 (void);
// 0x000000CC UnityEngine.Object CrazyMinnow.SALSA.RandomEyesLookStatus::get_instance()
extern void RandomEyesLookStatus_get_instance_m701539465A1941EB294C4D49503D98D0AD731E34 (void);
// 0x000000CD System.Void CrazyMinnow.SALSA.RandomEyesLookStatus::set_instance(UnityEngine.Object)
extern void RandomEyesLookStatus_set_instance_m68125F9E62FDE656F139985323B8500DA3E5AE8D (void);
// 0x000000CE System.Single CrazyMinnow.SALSA.RandomEyesLookStatus::get_blendSpeed()
extern void RandomEyesLookStatus_get_blendSpeed_m1AC267734DB387C3AC1040BA8519978D3E62F34D (void);
// 0x000000CF System.Void CrazyMinnow.SALSA.RandomEyesLookStatus::set_blendSpeed(System.Single)
extern void RandomEyesLookStatus_set_blendSpeed_m8D54B2B8FB2CB13A97161B6C5A480FDFD5D258EE (void);
// 0x000000D0 System.Single CrazyMinnow.SALSA.RandomEyesLookStatus::get_rangeOfMotion()
extern void RandomEyesLookStatus_get_rangeOfMotion_m96D975B67348C3BB84FC651115084D7957685029 (void);
// 0x000000D1 System.Void CrazyMinnow.SALSA.RandomEyesLookStatus::set_rangeOfMotion(System.Single)
extern void RandomEyesLookStatus_set_rangeOfMotion_m8E01B94D5F3932364DD566B7676D60B1939B09C9 (void);
// 0x000000D2 System.Void CrazyMinnow.SALSA.RandomEyesLookStatus::.ctor()
extern void RandomEyesLookStatus__ctor_m91B47B57FA3F7484BB01B458547D71931629555A (void);
static Il2CppMethodPointer s_methodPointers[210] = 
{
	RandomEyes2D_Start_m5605EE5FB0D11EE87C18900DC7DBF7C52BF1B0F2,
	RandomEyes2D_OnEnable_mF60FB619533D13BC163E2C09AB09BEEABFCE6225,
	RandomEyes2D_LateUpdate_m2257F2110275E2A1F2F8D9703A8EF26BD611ECFA,
	RandomEyes2D_LookTracking_m4C39BFE72EEA5E5F4E707B3BD925D259FCF6B77C,
	RandomEyes2D_ProportionalMovement_m4EC2F23BA31627C8C60313AA4695FA4B11A1651B,
	RandomEyes2D_TargetAffinityUpdate_m105F6F7B0C6788A396093FE3D0431CD3331FCCB0,
	RandomEyes2D_Blink_m2105B90CDDB68CD9D6F42752C3B7BAB853123B43,
	RandomEyes2D_BlinkEyes_mA9926DC8191C467D2188EC8D8F3AFFBB7AF92C24,
	RandomEyes2D_SetRangeOfMotion_mB76F28D1AF16BF195B3E33D9C11C8DB09D4A6E95,
	RandomEyes2D_SetBlendSpeed_m1EB9A46A16069C006ADA386330F1FD54602CF1ED,
	RandomEyes2D_SetOrderInLayer_m09E8144307532A7FA8F0946D35CFBA7350D0D285,
	RandomEyes2D_SetLookTargetTracking_m3B736925D0B69176837D0D89485EB6748354ECE9,
	RandomEyes2D_SetLookTarget_mBFA754D52C5FB53AEED97FA40A287CEC35A2C520,
	RandomEyes2D_SetTargetAffinity_m01D20750B2E1E15CA5326459B402A45B00F94F5C,
	RandomEyes2D_SetAffinityPercentage_mCE3C8F69ABB166059CDCA2D0D2CFC935A2B5E694,
	RandomEyes2D_SetRandomEyes_m422A7C480E27AC8B5D3F6DB4F19CFFAD52E74E8B,
	RandomEyes2D_SetBlink_mFBF167619C503F29BE10F9050A91177B3B6DE8B9,
	RandomEyes2D_SetBlinkDuration_m4A0D9E4B1A05916535B39E2217ADEA2EF39D33A7,
	RandomEyes2D__ctor_m03558EFA8B83C41A3C01B3250F2DA35FE7DC2FC0,
	U3CTargetAffinityUpdateU3Ed__0_MoveNext_mB4D2EE738CF3817FB48D0C654ADEB4A568B8D13C,
	U3CTargetAffinityUpdateU3Ed__0_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m8D0D3B955C4C2F3FCBBAD3D5FA4A2EE44ADA142D,
	U3CTargetAffinityUpdateU3Ed__0_System_Collections_IEnumerator_Reset_mED26C7B767350312099E767FA28653FB03743451,
	U3CTargetAffinityUpdateU3Ed__0_System_IDisposable_Dispose_m9C4EEA564DC977DB7BB9E05AFF7F66C3627BD119,
	U3CTargetAffinityUpdateU3Ed__0_System_Collections_IEnumerator_get_Current_m681473C79048E8CF76C118B34813BB9D67BB552A,
	U3CTargetAffinityUpdateU3Ed__0__ctor_m11474126BC59372CD200224F8F86C2D80D12BD0C,
	U3CBlinkEyesU3Ed__2_MoveNext_m89F92B1E6FB7BEE6A9A504CC3431FF670C19B14B,
	U3CBlinkEyesU3Ed__2_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m81D3332B40A695A9AA1FDE3E8B85B2139144A02C,
	U3CBlinkEyesU3Ed__2_System_Collections_IEnumerator_Reset_m88EBA7BE8E25F597E063CB1264198186B11DEBCB,
	U3CBlinkEyesU3Ed__2_System_IDisposable_Dispose_m617DA6BE6487FDE9E084BA9A3155DED4DD7FA55A,
	U3CBlinkEyesU3Ed__2_System_Collections_IEnumerator_get_Current_mA4EBBC50009E04F0472056FDA043C8E273572F7A,
	U3CBlinkEyesU3Ed__2__ctor_m794D2B438E817C03906B33BA6CEA16630030D920,
	RandomEyes3D_Reset_m7D9EB1391455DBDBD702534215BD097180AE6FC3,
	RandomEyes3D_Start_m29BD63BB4AB6AB55143C09550A5FE0476EDF538C,
	RandomEyes3D_OnEnable_mE7DAF2DEF3455C3BB7ED38E8DEC2EEE9C1D204B2,
	RandomEyes3D_LateUpdate_m87C843FC895A9CA260ADEABEDC307D030EAA5AE9,
	RandomEyes3D_LookTracking_m7F29611D970378663A41B66374456DDAEDCC3205,
	RandomEyes3D_TargetAffinityUpdate_mF7439C88C6DF7E156DBF3E40D67C57EC64EF3E77,
	RandomEyes3D_DisableAllCustomShapes_mDA8F4E0E351329FAD3ACC8BD9E13D3D70D0C44CD,
	RandomEyes3D_BlinkEyes_mDFDFEB7B2811CA1009B0A4D55E6E71CD4433ED6A,
	RandomEyes3D_SetCustomShapesAllNotRandom_m6E09D075DA414527EC49BE94F6FE650C144D8587,
	RandomEyes3D_SetCustomShapeOverrideDuration_m0A72CCB00C2DB1FBFFE2A7CBB1B85685B0EA52CC,
	RandomEyes3D_SetCustomShapeOverrideDuration_m07722506C2734D00B002B64D6BD1FE3EE9908F69,
	RandomEyes3D_SetCustomShapeDuration_mC9EFA2C619003EB530936AF8AC02019814A65099,
	RandomEyes3D_SetCustomShapeDuration_m3B9A380330E625EA5EB3E911590D6CEF499B6F53,
	RandomEyes3D_CustomShapeChanged_m380A0E30479AFAD7D59CA1222BB6E77DCD1FF068,
	RandomEyes3D_SetCustomShapeExcludeEyes_m2907E3D9B857200BD4368EB62CCC3A0B4D51C436,
	RandomEyes3D_SetCustomShapeExcludeMouth_m3063985221F372102DC1B3854A31D75F95655684,
	RandomEyes3D_SetRangeOfMotion_m284CEE173D95F6A663474206D141963CDDAFAB3C,
	RandomEyes3D_SetBlendSpeed_m6FAD94D8045D839F3E0B9D49FE88591FD72610E9,
	RandomEyes3D_Blink_mCCC507CFFA092075E2BFAF242D716F81A073771C,
	RandomEyes3D_DoBlink_m82BC786A5A89543456B234AAD2FC795DD86698FC,
	RandomEyes3D_SetLookTargetTracking_m8DC063699D644B556133DD4013E7BB99C95572EC,
	RandomEyes3D_SetLookTarget_m37D2E133D824526AC0FC64BBA65B40865592B4B8,
	RandomEyes3D_SetTargetAffinity_m2946B7CB73E8C8C336CD612B46DB2289F5896D69,
	RandomEyes3D_SetAffinityPercentage_mB5F8839B49735773F154CECB55A4A945127EBABC,
	RandomEyes3D_SetRandomEyes_mD372850D2928E9D46B8E385E7F5C0C738CD4A618,
	RandomEyes3D_SetBlink_mC54B3B3E6FEC4B3C0F1558051F70124EE7EE1EFA,
	RandomEyes3D_SetBlinkDuration_m58E1BE83D084596998E0876B051021D7701ECBEE,
	RandomEyes3D_SetBlinkSpeed_m154A39F053782E03ECB5B5DF26CB4E956D9C112A,
	RandomEyes3D_SetOpenMax_m2277B51661364A14724610455A8E8464576FA51E,
	RandomEyes3D_SetCloseMax_mB3143FFFD37CDB7D07A4DBF7306A27119607BF41,
	RandomEyes3D_SetCustomShapeRandom_m55793E8EE1AB7B795CA82B0B2A71148A7072B39F,
	RandomEyes3D_SetCustomShapeOverride_m23761F4B53F0E1A29A36DFFF91801AD211032AC1,
	RandomEyes3D_SetCustomShapeOverride_m98357978832E89B5E5B1FD37F2702911980DCD0A,
	RandomEyes3D_SetCustomShapeOverride_m9C8E2FDBF2AC9F768DADF1EF2853823C01937A52,
	RandomEyes3D_SetCustomShapeOverride_m5C3C269C3763197AC91B6053CA0F7EAF1E5CF4F3,
	RandomEyes3D_SetCustomShapeOverride_mA4614D96A02DF336129C3255D4866EC9499AC131,
	RandomEyes3D_SetCustomShapeOverride_mB89253EB0B19EEA2BEDCFE5D5628A7C30F4EAD8E,
	RandomEyes3D_SetCustomShapeOverride_mAC24BBA3E20FC41A96F383097AD90C35E0EA7066,
	RandomEyes3D_SetCustomShapeOverride_m05639C6FFFB925FE08A91D34C92678F1C851758A,
	RandomEyes3D_SetCustomShapeBlendSpeed_mE6C02207767F40A39806CEEFB76C1E90FC6B5693,
	RandomEyes3D_SetCustomShapeBlendSpeed_mE243E3BC51581AC08B9F57ADA68C3E7798D18B09,
	RandomEyes3D_SetCustomShapeRangeOfMotion_m203068B98D9D262BB4D6B2C8AEECE0875FF4AA14,
	RandomEyes3D_SetCustomShapeRangeOfMotion_m74054909B1069B8527597D1285B516B980955B67,
	RandomEyes3D_SetCustomShape_m0B61CBCBC3389E335E869448A557D96549915D36,
	RandomEyes3D_SetCustomShape_m46A96B9C7709FD6B8790B63AF40F71C00927A4A2,
	RandomEyes3D_SetCustomShape_m00C34809EE3C77CE110E8F6CCFECA9F0ECE2DB11,
	RandomEyes3D_SetCustomShape_mC41F70839B2458EDFC63410F9C534DC53A775177,
	RandomEyes3D_SetGroup_m36FB61B99AB52F36F2A82B7D61E2D71BB781FCC4,
	RandomEyes3D_SetGroup_mED0933BCC89904D06A34464FFF8CF78FFD3B3B1E,
	RandomEyes3D_GetBlendShapes_mF9F69C143A9AF121E534B05AC0F6EAD505765562,
	RandomEyes3D_AutoLinkEyes_m7AFABE4BEF07CBD9C860F545823CC8856A3B08A4,
	RandomEyes3D_AutoLinkCustomShapes_m9D8A263AA469EDC6301A778EFCAF60B3BFD34359,
	RandomEyes3D_RebuildCurrentCustomShapeList_m645926658AF05E208117F2A4D0EAA998DBD27348,
	RandomEyes3D_FindOrCreateEyePositionGizmo_m62C69039D32AC352E5A274B9C560E2625DE8BE74,
	RandomEyes3D__ctor_mBCB53DB419FB67074996B3785C1F705D12CA80EF,
	U3CTargetAffinityUpdateU3Ed__0_MoveNext_m6C8737D2049E5CA630C97961F714DDF5D1F5B9B5,
	U3CTargetAffinityUpdateU3Ed__0_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m2327362FC616DD432555C7E25749B63137D99477,
	U3CTargetAffinityUpdateU3Ed__0_System_Collections_IEnumerator_Reset_mBAE456A3C0C96A0C8B70B0D32F3D345A0C07CA1E,
	U3CTargetAffinityUpdateU3Ed__0_System_IDisposable_Dispose_m6DB637D4B9B6B5D7278321DA86C71BB5BBE1951F,
	U3CTargetAffinityUpdateU3Ed__0_System_Collections_IEnumerator_get_Current_m348B9921D9DFEA1779B61D9EA42F31C06F6A00BB,
	U3CTargetAffinityUpdateU3Ed__0__ctor_mCAFDF47555144A310E4F743A29A8C68AAAA8FDFB,
	U3CBlinkEyesU3Ed__2_MoveNext_mD6E6B0ECBC82B2E9C31F22D8F7DEB26604A96FDE,
	U3CBlinkEyesU3Ed__2_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mB5FD936BCB45081E3D0AD94D05AB1EA32D510B74,
	U3CBlinkEyesU3Ed__2_System_Collections_IEnumerator_Reset_mD5DCBE6D12F53C662EF106666A000BEB03ED3558,
	U3CBlinkEyesU3Ed__2_System_IDisposable_Dispose_m2DBE82FEB6DF1FEDBCBE44AA8BBA27D6A63250BC,
	U3CBlinkEyesU3Ed__2_System_Collections_IEnumerator_get_Current_mF267239843A192233380FE5A3DA1DA92BA248B7E,
	U3CBlinkEyesU3Ed__2__ctor_m1C070BB710ACF1198445B49A02B0EE406A56F50C,
	U3CSetCustomShapeOverrideDurationU3Ed__4_MoveNext_mF967AD900DA0BB3DACEB9E8EC01074D0C512490F,
	U3CSetCustomShapeOverrideDurationU3Ed__4_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m0DB8C66E0B7DC354D7A4096727A9F719FA17B455,
	U3CSetCustomShapeOverrideDurationU3Ed__4_System_Collections_IEnumerator_Reset_mAD496E65677155FF44B6B02F4850449E59AD4F75,
	U3CSetCustomShapeOverrideDurationU3Ed__4_System_IDisposable_Dispose_m8201BD769FB409291915C1E2559C9DF0CE067234,
	U3CSetCustomShapeOverrideDurationU3Ed__4_System_Collections_IEnumerator_get_Current_mD22F300FB60FBC84ACA05B07F1CC27E4ECFFCE7C,
	U3CSetCustomShapeOverrideDurationU3Ed__4__ctor_mB66C5B43F2C15F540F85F1845A71DB41035E6FAD,
	U3CSetCustomShapeOverrideDurationU3Ed__6_MoveNext_m63E68EDC3982D66FE7182E3E87F877F2DD3E4ABF,
	U3CSetCustomShapeOverrideDurationU3Ed__6_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mC45AF0F2DFB5CC2AB9A7B2674BE4416D56C54330,
	U3CSetCustomShapeOverrideDurationU3Ed__6_System_Collections_IEnumerator_Reset_m248F5D80FBFAA6C850D4680977E43D02F8E5F7AC,
	U3CSetCustomShapeOverrideDurationU3Ed__6_System_IDisposable_Dispose_mE23ACB37AE186DEDA95535FB3720E8359D308E07,
	U3CSetCustomShapeOverrideDurationU3Ed__6_System_Collections_IEnumerator_get_Current_mFD60724EFEB1942BDF96085D0C38219DC9796378,
	U3CSetCustomShapeOverrideDurationU3Ed__6__ctor_m90B95A85DE3597BE73729C4BD7FF6D9756F8D6EB,
	U3CSetCustomShapeDurationU3Ed__8_MoveNext_mF5A8B9DF3C76678C8ED39451F03A21D4500F39BF,
	U3CSetCustomShapeDurationU3Ed__8_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m1DD68496D286084B27BA2709A378BF2620CF9534,
	U3CSetCustomShapeDurationU3Ed__8_System_Collections_IEnumerator_Reset_mA93C2810F2DFD6BB6409274C433C796616BD935F,
	U3CSetCustomShapeDurationU3Ed__8_System_IDisposable_Dispose_m0EB432743B895C26FA9451C2F70D14108A12DBBE,
	U3CSetCustomShapeDurationU3Ed__8_System_Collections_IEnumerator_get_Current_m655AA5A40FE04228B897FF3815520CE581E9C3DC,
	U3CSetCustomShapeDurationU3Ed__8__ctor_mF6CD6A348AAE88971E18E1E4E94D43FF9A964EB3,
	U3CSetCustomShapeDurationU3Ed__a_MoveNext_m80EA5F24116A83527DAA55FD2D166CFF55C1457C,
	U3CSetCustomShapeDurationU3Ed__a_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m29AD9BF27A7FF383D8C75DEA1AD987DCB65A24E5,
	U3CSetCustomShapeDurationU3Ed__a_System_Collections_IEnumerator_Reset_m6AD8EF97A2799112F845813343FD577CA1698CF7,
	U3CSetCustomShapeDurationU3Ed__a_System_IDisposable_Dispose_m9D8F38C3E7B44E6DE7815CC7D32D11D27EEF8D03,
	U3CSetCustomShapeDurationU3Ed__a_System_Collections_IEnumerator_get_Current_m3FCCF65FB5B696321DFF75279636D1A8666D7DF7,
	U3CSetCustomShapeDurationU3Ed__a__ctor_m22ECCEFDEC5DB0D1080451AE79B9E22EC5A618EB,
	U3CDoBlinkU3Ed__c_MoveNext_m6FE3368A1FA11FF8B8A8DF812B8ED7EC26F6EB6E,
	U3CDoBlinkU3Ed__c_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m401CF5F735EAD27CA82731FA9AAC660CEB51F4CA,
	U3CDoBlinkU3Ed__c_System_Collections_IEnumerator_Reset_m04161B4126710078525F0D3F99E46A39A538B5B6,
	U3CDoBlinkU3Ed__c_System_IDisposable_Dispose_m700124204B78C1A7F4A5BD798ECE2D22492C7FB6,
	U3CDoBlinkU3Ed__c_System_Collections_IEnumerator_get_Current_mA6C9A533B91FE9022C5929D1F59EA6849129B59D,
	U3CDoBlinkU3Ed__c__ctor_m9D9176B98A436D63C7C71A253F9242C3ACD37F00,
	RandomEyes3DGizmo_OnDrawGizmos_m3E702ED02773C0CB2431D737D0193D15D6AFD8EC,
	RandomEyes3DGizmo__ctor_m31CB5D4DA8919EA40F7E37474FA16BDBB467DAEC,
	Salsa2D_Reset_mF278D642863E4E5FAC9DDA581B0CFB88B9A6C595,
	Salsa2D_Awake_m9C980FB4ACD64F6B9CD5E64F3D5781CEAF296448,
	Salsa2D_OnEnable_mEAB9811C8A6709133703DC6BA21718A4CCFF5BE8,
	Salsa2D_LateUpdate_m3D28055ADA1D7F1AAB095581F94054E16BF874BC,
	Salsa2D_TalkStatusChanged_m7EF42D46D528EA86DB9E59DF5FD9C105387EA97C,
	Salsa2D_UpdateShape_m81111EF6B2731082FA420939A62870B3B59C3AEF,
	Salsa2D_UpdateSample_m3ECFBCCA2C397C22A1B8715F2F43223C469F7563,
	Salsa2D_ClampValues_mF88513A62C56A731A344DD11BDBB0024C8742455,
	Salsa2D_Play_m35EA602BD85046F81EE5B10AECED91841066A1F8,
	Salsa2D_Pause_m42C21144E3C154C1B2EECEF63D3E4601EA50522C,
	Salsa2D_Stop_mBC22BAF8743751DB23FB66929BD7E648B39BD653,
	Salsa2D_SetAudioClip_m030D997129C505E6F5D787B66B15A032C2F4EC8B,
	Salsa2D_LoadAudioClip_m00E486A9634C76CC9833BE9F8D88D3600F9326F6,
	Salsa2D_FindSpriteRenderer_m568528C27E3DA94591E872864EFF993F32B11D0E,
	Salsa2D_FindAudioSource_m4F909704BCA8FAD1285CB91F23783882DFD618C4,
	Salsa2D_SetOrderInLayer_m2F1C9424774C0A54279E6AC5848890A0C365E834,
	Salsa2D__ctor_mCF46C454E09DDAE70FF51E82150EED81A9FC8094,
	U3CUpdateSampleU3Ed__0_MoveNext_m96AE4867D7BA9B7FB3F37F0FF06599F22D0780EB,
	U3CUpdateSampleU3Ed__0_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m1DE327FA8E703A2453E6E02DE2D379EF29B0353A,
	U3CUpdateSampleU3Ed__0_System_Collections_IEnumerator_Reset_m721FCB06919D945E4656B278C22DB71EBAD5D67B,
	U3CUpdateSampleU3Ed__0_System_IDisposable_Dispose_m279F715E7CC834B20BE21E737211C44321A8B7DF,
	U3CUpdateSampleU3Ed__0_System_Collections_IEnumerator_get_Current_mB214E3D8FF3AD5D7552BD9302AB3D5D5B40C79CB,
	U3CUpdateSampleU3Ed__0__ctor_m455E0D010D62E290992788EA19E42269D2B13A77,
	Salsa3D_Reset_m53CBF6B12A477D9B56DDDB9486EF2B5C887DED58,
	Salsa3D_Awake_m532A8AE4ACDD0CCD856228FF388FD17D07F65E58,
	Salsa3D_OnEnable_m85CD4CE8B73639E5D58DD48BEF309F80C27A555F,
	Salsa3D_LateUpdate_mA3E1A807016185BCE0DDDD8E2136A84F89267577,
	Salsa3D_TalkStatusChanged_m4C6E88F7F47206891F1C4227E68A3896C39254B5,
	Salsa3D_UpdateSample_mB63EC23E06A628ADCF06EEFCE0735FF9D2E0D727,
	Salsa3D_GetBlendShapes_mAD39B313314A8243FE2172912EC2DD385D1EA751,
	Salsa3D_AutoLinkMouth_mC22B8B11A8FD3FB251B7D3550C76312CB7CC3E32,
	Salsa3D_Play_mEA71E242A27A0C1921CAAA55522985950120F751,
	Salsa3D_Pause_m4507436881B8E2F41E18AEB1B8A5C4E8A35DB804,
	Salsa3D_Stop_m4362CFD77DCD332D5CFB4224D4E6ACF6B2FB2967,
	Salsa3D_SetAudioClip_m7E1EAB203B768C23F2B00544C93C92B989703665,
	Salsa3D_LoadAudioClip_m6D5FDC03511EDFE828A82FE84E85E888F0FF9398,
	Salsa3D_FindAudioSource_mDE3A59BD7075854BE6C62C69780834597C8F2281,
	Salsa3D_SetRangeOfMotion_m02D66529248011B5E0C665270F3D5F950748F64D,
	Salsa3D_SetBlendSpeed_m579082426D279F7A6C139F3672C03779C29977B6,
	Salsa3D__ctor_m2FC9AF05F5A114DECB757F015343373CBCF4358B,
	U3CUpdateSampleU3Ed__0_MoveNext_m07D140947B25F0D0BCB80CA6BFD59BD28A7F3F30,
	U3CUpdateSampleU3Ed__0_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m7D4F83F4B85BEAC2F01C41CF80ACA0A2F23EAAB5,
	U3CUpdateSampleU3Ed__0_System_Collections_IEnumerator_Reset_m482BF263E6A170335B23F21B5CA758CC19D9370F,
	U3CUpdateSampleU3Ed__0_System_IDisposable_Dispose_mA953A0205C9175B7DD1F260B4D142FAE87F508CA,
	U3CUpdateSampleU3Ed__0_System_Collections_IEnumerator_get_Current_m3B132E8B0A5214B5E01262ADBFC7FD269CC51AA2,
	U3CUpdateSampleU3Ed__0__ctor_m199B19B5576CA91594C9D8529188676BA2B73670,
	SalsaStatus_get_instance_mB63DF2D8EF456D6A9E976AEB43B6C03A77BE4C29,
	SalsaStatus_set_instance_mB67A73851E6FDD5B713B5F85017446B672B32F63,
	SalsaStatus_get_talkerName_mAE5109304DDE680F4BB760BDC4A60BE9AA29EDD5,
	SalsaStatus_set_talkerName_m88E92FFF1840D7288C4474685B82F562E5A6C5EE,
	SalsaStatus_get_isTalking_m5E400E0CDFCB8B3241BE2255144A88E0E0881234,
	SalsaStatus_set_isTalking_m621FF64AC69982D2A47B901157DB29C7F3EC6725,
	SalsaStatus_get_clipName_m1D43753B3C3BED26B88D82DBF7F14991D56E2B92,
	SalsaStatus_set_clipName_m564A868F0584865EBA31D5F8DD38FE39B698F72A,
	SalsaStatus__ctor_m60EB1381FDD2EA05C23A1AD9A6005AF854E5E89D,
	SalsaUtility_LoadResource_m83BADDA1A9109E56D955FFA4143062817C97B1B2,
	SalsaUtility_LerpRangeOfMotion_mDA97D8D31F6D62217220938CD785C48A75AEB246,
	SalsaUtility_Lerp3DEyeTracking_m914ED29E6BF956DD834E8CF6B8C5B20591A22C19,
	SalsaUtility_Lerp2DEyeTracking_m49C8BAED6D6F4ABD41691661601291A0F20A800D,
	SalsaUtility_ClampValue_m167E1BE774D495060E1FAA2503DD01D2C00EE208,
	SalsaUtility_ClampValueMinMax_mDC645C582746F5E4B0C6D90541D284BF6F9A73FB,
	SalsaUtility_ScaleRange_m58E61AAAA506DE027672DBC1B89A1E874AE4E8A7,
	SalsaUtility__ctor_m32C37E3AD85E505EF76625DD2918D8CFAB1B6986,
	SalsaBlendAmounts__ctor_mA2E926D5E2C432845C8162FAEC2012E860EB94E2,
	RandomEyesBlendAmounts__ctor_m20EA40B8CB17580CC2E6FB16FF225D40D1CA54FE,
	Shape__ctor_mF26431536C0DA3EA973AED80958736B9238D7ADA,
	Shape__ctor_m8FEA41876A6510E06F593EAC4A0915629895D292,
	Group__ctor_mB95A1CB37E84D974BE25697B57B07FBA0BA41A45,
	RandomEyes2DGizmo__ctor_m1B5A6AB31BA24749312A096CBA2D9BE9AF973E9A,
	RandomEyesCustomShape__ctor_m55E79792EBDFA8E4E88E7396AB6E31327D5D0DBF,
	RandomEyesCustomShapeStatus_get_instance_m3E3018C5AB25E4037D1E5DD7F84F42337B99229F,
	RandomEyesCustomShapeStatus_set_instance_m4F5644F9DB6E38215FA12B1892E396CA0D8281B1,
	RandomEyesCustomShapeStatus__ctor_m97BF52FFB152BF60CB27311F72EA547C783657C0,
	RandomEyesLookStatus_get_instance_m701539465A1941EB294C4D49503D98D0AD731E34,
	RandomEyesLookStatus_set_instance_m68125F9E62FDE656F139985323B8500DA3E5AE8D,
	RandomEyesLookStatus_get_blendSpeed_m1AC267734DB387C3AC1040BA8519978D3E62F34D,
	RandomEyesLookStatus_set_blendSpeed_m8D54B2B8FB2CB13A97161B6C5A480FDFD5D258EE,
	RandomEyesLookStatus_get_rangeOfMotion_m96D975B67348C3BB84FC651115084D7957685029,
	RandomEyesLookStatus_set_rangeOfMotion_m8E01B94D5F3932364DD566B7676D60B1939B09C9,
	RandomEyesLookStatus__ctor_m91B47B57FA3F7484BB01B458547D71931629555A,
};
static const int32_t s_InvokerIndices[210] = 
{
	3479,
	3479,
	3479,
	2944,
	1060,
	2303,
	2922,
	2303,
	2922,
	2922,
	1612,
	2887,
	2887,
	2920,
	2922,
	2920,
	2920,
	2922,
	3479,
	3451,
	3414,
	3479,
	3479,
	3414,
	2870,
	3451,
	3414,
	3479,
	3479,
	3414,
	2870,
	3479,
	3479,
	3479,
	3479,
	2944,
	3414,
	3479,
	2303,
	2920,
	1317,
	1306,
	1317,
	1306,
	2887,
	2920,
	2887,
	2922,
	2922,
	2922,
	2303,
	2887,
	2887,
	2920,
	2922,
	2920,
	2920,
	2922,
	2922,
	2922,
	2922,
	2920,
	1753,
	1652,
	801,
	742,
	1754,
	1653,
	802,
	743,
	1754,
	1653,
	1754,
	1653,
	2887,
	2870,
	1754,
	1653,
	1754,
	1753,
	3479,
	3479,
	1768,
	3397,
	3479,
	3479,
	3451,
	3414,
	3479,
	3479,
	3414,
	2870,
	3451,
	3414,
	3479,
	3479,
	3414,
	2870,
	3451,
	3414,
	3479,
	3479,
	3414,
	2870,
	3451,
	3414,
	3479,
	3479,
	3414,
	2870,
	3451,
	3414,
	3479,
	3479,
	3414,
	2870,
	3451,
	3414,
	3479,
	3479,
	3414,
	2870,
	3451,
	3414,
	3479,
	3479,
	3414,
	2870,
	3479,
	3479,
	3479,
	3479,
	3479,
	3479,
	3479,
	3479,
	3414,
	3479,
	3479,
	3479,
	3479,
	2887,
	2887,
	3414,
	3414,
	2870,
	3479,
	3451,
	3414,
	3479,
	3479,
	3414,
	2870,
	3479,
	3479,
	3479,
	3479,
	3479,
	3414,
	3479,
	3479,
	3479,
	3479,
	3479,
	2887,
	2887,
	3414,
	2922,
	2922,
	3479,
	3451,
	3414,
	3479,
	3479,
	3414,
	2870,
	3414,
	2887,
	3414,
	2887,
	3451,
	2920,
	3414,
	2887,
	3479,
	5066,
	4189,
	4446,
	4451,
	4793,
	4446,
	3913,
	3479,
	3479,
	3479,
	3479,
	739,
	3479,
	3479,
	3479,
	3414,
	2887,
	3479,
	3414,
	2887,
	3453,
	2922,
	3453,
	2922,
	3479,
};
extern const CustomAttributesCacheGenerator g_SALSA_AttributeGenerators[];
IL2CPP_EXTERN_C const Il2CppCodeGenModule g_SALSA_CodeGenModule;
const Il2CppCodeGenModule g_SALSA_CodeGenModule = 
{
	"SALSA.dll",
	210,
	s_methodPointers,
	0,
	NULL,
	s_InvokerIndices,
	0,
	NULL,
	0,
	NULL,
	0,
	NULL,
	NULL,
	g_SALSA_AttributeGenerators,
	NULL, // module initializer,
	NULL,
	NULL,
	NULL,
};
