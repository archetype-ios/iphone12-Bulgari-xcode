﻿#include "pch-c.h"
#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include "codegen/il2cpp-codegen-metadata.h"





// 0x00000001 System.String SR::GetString(System.String)
extern void SR_GetString_mD7FC73A3473F4F165E55F8B4A7088F2E9F9CC412 (void);
// 0x00000002 System.Void System.Threading.ReaderWriterCount::.ctor()
extern void ReaderWriterCount__ctor_m0C083844BE7A2C4172B9684CF08A786C1E51874B (void);
// 0x00000003 System.Void System.Threading.ReaderWriterLockSlim::InitializeThreadCounts()
extern void ReaderWriterLockSlim_InitializeThreadCounts_m256062BF3249D0A11A4A3F9BD625198B719C98AD (void);
// 0x00000004 System.Void System.Threading.ReaderWriterLockSlim::.ctor(System.Threading.LockRecursionPolicy)
extern void ReaderWriterLockSlim__ctor_m76201E97862C090097E879E3F10FAA8CD0ADF1A1 (void);
// 0x00000005 System.Boolean System.Threading.ReaderWriterLockSlim::IsRWEntryEmpty(System.Threading.ReaderWriterCount)
extern void ReaderWriterLockSlim_IsRWEntryEmpty_mC8DAC7CFE64A30203FF50C4823149F25FB15D116 (void);
// 0x00000006 System.Boolean System.Threading.ReaderWriterLockSlim::IsRwHashEntryChanged(System.Threading.ReaderWriterCount)
extern void ReaderWriterLockSlim_IsRwHashEntryChanged_m3EDD34CB05C022D9A2852EA9BA753642D20A4617 (void);
// 0x00000007 System.Threading.ReaderWriterCount System.Threading.ReaderWriterLockSlim::GetThreadRWCount(System.Boolean)
extern void ReaderWriterLockSlim_GetThreadRWCount_m8CCEECA7603C844BACCAB04BB083976E0A85AD5A (void);
// 0x00000008 System.Void System.Threading.ReaderWriterLockSlim::EnterReadLock()
extern void ReaderWriterLockSlim_EnterReadLock_m2BE91DD529FF6A951B0A9BF4E99CDD57E7907FEF (void);
// 0x00000009 System.Boolean System.Threading.ReaderWriterLockSlim::TryEnterReadLock(System.Int32)
extern void ReaderWriterLockSlim_TryEnterReadLock_m52AE956B043DA2B69FAB59B8D5229F6A6A03A51B (void);
// 0x0000000A System.Boolean System.Threading.ReaderWriterLockSlim::TryEnterReadLock(System.Threading.ReaderWriterLockSlim/TimeoutTracker)
extern void ReaderWriterLockSlim_TryEnterReadLock_mA2D7C92AC16257ACBCA744B16B6C47485C6F969A (void);
// 0x0000000B System.Boolean System.Threading.ReaderWriterLockSlim::TryEnterReadLockCore(System.Threading.ReaderWriterLockSlim/TimeoutTracker)
extern void ReaderWriterLockSlim_TryEnterReadLockCore_mC6BC860B780596E9593D2748682CB7296EE9D1F7 (void);
// 0x0000000C System.Void System.Threading.ReaderWriterLockSlim::EnterWriteLock()
extern void ReaderWriterLockSlim_EnterWriteLock_m6906FE7058BCD5857E7036D763FA14A94D395386 (void);
// 0x0000000D System.Boolean System.Threading.ReaderWriterLockSlim::TryEnterWriteLock(System.Int32)
extern void ReaderWriterLockSlim_TryEnterWriteLock_mE8293BAF7A39EDB8E5C3C9F39BC6B404BC83C38F (void);
// 0x0000000E System.Boolean System.Threading.ReaderWriterLockSlim::TryEnterWriteLock(System.Threading.ReaderWriterLockSlim/TimeoutTracker)
extern void ReaderWriterLockSlim_TryEnterWriteLock_m8DC7B1653133775A79E6FDBE072DF63C8E6822C7 (void);
// 0x0000000F System.Boolean System.Threading.ReaderWriterLockSlim::TryEnterWriteLockCore(System.Threading.ReaderWriterLockSlim/TimeoutTracker)
extern void ReaderWriterLockSlim_TryEnterWriteLockCore_m92031DB0DAF78611332535E47CCC146C90B9706F (void);
// 0x00000010 System.Void System.Threading.ReaderWriterLockSlim::EnterUpgradeableReadLock()
extern void ReaderWriterLockSlim_EnterUpgradeableReadLock_m49E33B0F164253250D2CE7D4F369365D4A278319 (void);
// 0x00000011 System.Boolean System.Threading.ReaderWriterLockSlim::TryEnterUpgradeableReadLock(System.Int32)
extern void ReaderWriterLockSlim_TryEnterUpgradeableReadLock_mF0F2532E93D3821AD35647FE1DA56C2FEF01FDAA (void);
// 0x00000012 System.Boolean System.Threading.ReaderWriterLockSlim::TryEnterUpgradeableReadLock(System.Threading.ReaderWriterLockSlim/TimeoutTracker)
extern void ReaderWriterLockSlim_TryEnterUpgradeableReadLock_mE362E867B7E04114EF8BECDC6FF15D65C7D01C13 (void);
// 0x00000013 System.Boolean System.Threading.ReaderWriterLockSlim::TryEnterUpgradeableReadLockCore(System.Threading.ReaderWriterLockSlim/TimeoutTracker)
extern void ReaderWriterLockSlim_TryEnterUpgradeableReadLockCore_mC3C1C2217C8FA13B0FA3C6C29C12052859A1B49B (void);
// 0x00000014 System.Void System.Threading.ReaderWriterLockSlim::ExitReadLock()
extern void ReaderWriterLockSlim_ExitReadLock_m912A6E70DAE228A948E8EB78C1C2EE49D78E9ADF (void);
// 0x00000015 System.Void System.Threading.ReaderWriterLockSlim::ExitWriteLock()
extern void ReaderWriterLockSlim_ExitWriteLock_m2A49859BD012D6C7FCC58A7C37C3702B8C761BE2 (void);
// 0x00000016 System.Void System.Threading.ReaderWriterLockSlim::ExitUpgradeableReadLock()
extern void ReaderWriterLockSlim_ExitUpgradeableReadLock_m870A35845A911065A8897A68EB1F9C28C08E38DE (void);
// 0x00000017 System.Void System.Threading.ReaderWriterLockSlim::LazyCreateEvent(System.Threading.EventWaitHandle&,System.Boolean)
extern void ReaderWriterLockSlim_LazyCreateEvent_m02E55386C7E31D1CB3FC38C5D2E1EB89EB617644 (void);
// 0x00000018 System.Boolean System.Threading.ReaderWriterLockSlim::WaitOnEvent(System.Threading.EventWaitHandle,System.UInt32&,System.Threading.ReaderWriterLockSlim/TimeoutTracker,System.Boolean)
extern void ReaderWriterLockSlim_WaitOnEvent_m6C6EB73C0C8D57FF14A6F1EBE8A6756407903FF1 (void);
// 0x00000019 System.Void System.Threading.ReaderWriterLockSlim::ExitAndWakeUpAppropriateWaiters()
extern void ReaderWriterLockSlim_ExitAndWakeUpAppropriateWaiters_m203B2A315D7812546CE39C84789145491C7729C5 (void);
// 0x0000001A System.Void System.Threading.ReaderWriterLockSlim::ExitAndWakeUpAppropriateWaitersPreferringWriters()
extern void ReaderWriterLockSlim_ExitAndWakeUpAppropriateWaitersPreferringWriters_mDE8BD2C760A17C2045AE43994C0D7E85AB316D5D (void);
// 0x0000001B System.Void System.Threading.ReaderWriterLockSlim::ExitAndWakeUpAppropriateReadWaiters()
extern void ReaderWriterLockSlim_ExitAndWakeUpAppropriateReadWaiters_mA0AA94088046F5DFAA395A193C3C45D3D2086389 (void);
// 0x0000001C System.Boolean System.Threading.ReaderWriterLockSlim::IsWriterAcquired()
extern void ReaderWriterLockSlim_IsWriterAcquired_m9F14403D5E6BA8D9840274FFA9CA59E03FE9ADD9 (void);
// 0x0000001D System.Void System.Threading.ReaderWriterLockSlim::SetWriterAcquired()
extern void ReaderWriterLockSlim_SetWriterAcquired_m24C2FFF43CE66EAF4E5434B345B1858EE92E39E2 (void);
// 0x0000001E System.Void System.Threading.ReaderWriterLockSlim::ClearWriterAcquired()
extern void ReaderWriterLockSlim_ClearWriterAcquired_m81A620F80C7AFCF5E4AC1DAB2F2A5B262E42AFD8 (void);
// 0x0000001F System.Void System.Threading.ReaderWriterLockSlim::SetWritersWaiting()
extern void ReaderWriterLockSlim_SetWritersWaiting_m0E15480965179DBE2CF352BD140188DB5359DF5C (void);
// 0x00000020 System.Void System.Threading.ReaderWriterLockSlim::ClearWritersWaiting()
extern void ReaderWriterLockSlim_ClearWritersWaiting_m97456CAD2756BAD28B6C0EF9B42E60BE7A35D669 (void);
// 0x00000021 System.Void System.Threading.ReaderWriterLockSlim::SetUpgraderWaiting()
extern void ReaderWriterLockSlim_SetUpgraderWaiting_mF6E393FE0C46184DF6A1B1B8F2EFBD40A307827F (void);
// 0x00000022 System.Void System.Threading.ReaderWriterLockSlim::ClearUpgraderWaiting()
extern void ReaderWriterLockSlim_ClearUpgraderWaiting_m85BBABDCA1A01D201683926A892CD4F75EAAA44A (void);
// 0x00000023 System.UInt32 System.Threading.ReaderWriterLockSlim::GetNumReaders()
extern void ReaderWriterLockSlim_GetNumReaders_mC51603FD596E2B077422FA9331023A5D87DD3D2D (void);
// 0x00000024 System.Void System.Threading.ReaderWriterLockSlim::EnterMyLock()
extern void ReaderWriterLockSlim_EnterMyLock_mCE063DE6AD5C084F0EC81D24610EF4AD5A2F0E2A (void);
// 0x00000025 System.Void System.Threading.ReaderWriterLockSlim::EnterMyLockSpin()
extern void ReaderWriterLockSlim_EnterMyLockSpin_mC63264D1309E935E22E6E2BFCAD04EAB8A19AE3C (void);
// 0x00000026 System.Void System.Threading.ReaderWriterLockSlim::ExitMyLock()
extern void ReaderWriterLockSlim_ExitMyLock_m4D09639AB6AA1E1FEF2BAEA70F3368B1DB88C6A8 (void);
// 0x00000027 System.Void System.Threading.ReaderWriterLockSlim::SpinWait(System.Int32)
extern void ReaderWriterLockSlim_SpinWait_m4B2AD68AFAF0D0D8C4FACC6EE090CAA5AE7A8B00 (void);
// 0x00000028 System.Void System.Threading.ReaderWriterLockSlim::Dispose()
extern void ReaderWriterLockSlim_Dispose_m342639E5C3D64460ADC87ED495F4FD43B8C57A7C (void);
// 0x00000029 System.Void System.Threading.ReaderWriterLockSlim::Dispose(System.Boolean)
extern void ReaderWriterLockSlim_Dispose_mDA9910A42284308F6EA8F8A294FF52AC4E05699E (void);
// 0x0000002A System.Boolean System.Threading.ReaderWriterLockSlim::get_IsReadLockHeld()
extern void ReaderWriterLockSlim_get_IsReadLockHeld_m7DCD5FD5F491F1F19B202A8CE4C9609E6FD54B78 (void);
// 0x0000002B System.Boolean System.Threading.ReaderWriterLockSlim::get_IsUpgradeableReadLockHeld()
extern void ReaderWriterLockSlim_get_IsUpgradeableReadLockHeld_mCD549D784C4E29D93D7D1DC7F87D07BBD2B054E4 (void);
// 0x0000002C System.Boolean System.Threading.ReaderWriterLockSlim::get_IsWriteLockHeld()
extern void ReaderWriterLockSlim_get_IsWriteLockHeld_m76BAF63331AE3EA08A46001A77C3B8ED95E6FE25 (void);
// 0x0000002D System.Int32 System.Threading.ReaderWriterLockSlim::get_RecursiveReadCount()
extern void ReaderWriterLockSlim_get_RecursiveReadCount_m4F403802E0535224A75B6ACB914795E3255EBE6C (void);
// 0x0000002E System.Int32 System.Threading.ReaderWriterLockSlim::get_RecursiveUpgradeCount()
extern void ReaderWriterLockSlim_get_RecursiveUpgradeCount_m57C446384AA3302B1D8551FDF094E0614B43D507 (void);
// 0x0000002F System.Int32 System.Threading.ReaderWriterLockSlim::get_RecursiveWriteCount()
extern void ReaderWriterLockSlim_get_RecursiveWriteCount_mB157295491E285DF2269906A16A90F972474367A (void);
// 0x00000030 System.Int32 System.Threading.ReaderWriterLockSlim::get_WaitingReadCount()
extern void ReaderWriterLockSlim_get_WaitingReadCount_m7ABA9E4099E3A845BA69B2DE31F1620468A96168 (void);
// 0x00000031 System.Int32 System.Threading.ReaderWriterLockSlim::get_WaitingUpgradeCount()
extern void ReaderWriterLockSlim_get_WaitingUpgradeCount_m6427A931CE9CD0A0A759ECE9FB89A2953D53F679 (void);
// 0x00000032 System.Int32 System.Threading.ReaderWriterLockSlim::get_WaitingWriteCount()
extern void ReaderWriterLockSlim_get_WaitingWriteCount_m4C34DCD34734878CF89D94EFAED6772AC5C803EE (void);
// 0x00000033 System.Void System.Threading.ReaderWriterLockSlim/TimeoutTracker::.ctor(System.Int32)
extern void TimeoutTracker__ctor_mD01DAC1C1322B0E6DB8D847D6BB02DF0DEC14553 (void);
// 0x00000034 System.Int32 System.Threading.ReaderWriterLockSlim/TimeoutTracker::get_RemainingMilliseconds()
extern void TimeoutTracker_get_RemainingMilliseconds_m2DF873714961B9BED784F45364703221E4259F04 (void);
// 0x00000035 System.Boolean System.Threading.ReaderWriterLockSlim/TimeoutTracker::get_IsExpired()
extern void TimeoutTracker_get_IsExpired_mE4ED4EDAA3A57C06198F451E21D347640DB1EA38 (void);
// 0x00000036 System.Void System.Security.Cryptography.AesManaged::.ctor()
extern void AesManaged__ctor_m79644F6BCD0E8C2D8BAF1B1E22E90D3C364F5C57 (void);
// 0x00000037 System.Int32 System.Security.Cryptography.AesManaged::get_FeedbackSize()
extern void AesManaged_get_FeedbackSize_mCFE4C56DFF81F5E616CE535AB7D9E37DC1B7A937 (void);
// 0x00000038 System.Void System.Security.Cryptography.AesManaged::set_FeedbackSize(System.Int32)
extern void AesManaged_set_FeedbackSize_mD898DD8078EA74C68D8DA8D461E79ABE7BF0BDE2 (void);
// 0x00000039 System.Byte[] System.Security.Cryptography.AesManaged::get_IV()
extern void AesManaged_get_IV_mB1D7896A5F5E71B8B7938A5DF3A743FC2E444018 (void);
// 0x0000003A System.Void System.Security.Cryptography.AesManaged::set_IV(System.Byte[])
extern void AesManaged_set_IV_m1DBDC4FDAE66A5F2FA99AA4A4E76769BB8897D1E (void);
// 0x0000003B System.Byte[] System.Security.Cryptography.AesManaged::get_Key()
extern void AesManaged_get_Key_m4CC3B2D28A918B935AD42F3F8D54E93A6CB2FA31 (void);
// 0x0000003C System.Void System.Security.Cryptography.AesManaged::set_Key(System.Byte[])
extern void AesManaged_set_Key_m35D61E5FD8942054840B1F24E685E91E3E6CA6E1 (void);
// 0x0000003D System.Int32 System.Security.Cryptography.AesManaged::get_KeySize()
extern void AesManaged_get_KeySize_mBE6EA533BD5978099974A74FF3DE3ECB8B173CD6 (void);
// 0x0000003E System.Void System.Security.Cryptography.AesManaged::set_KeySize(System.Int32)
extern void AesManaged_set_KeySize_m2003A2B9200003C23B544F56E949A0630AA87F93 (void);
// 0x0000003F System.Security.Cryptography.CipherMode System.Security.Cryptography.AesManaged::get_Mode()
extern void AesManaged_get_Mode_mF9D7222B2AB685AC46F4564B6F2247114244AEF6 (void);
// 0x00000040 System.Void System.Security.Cryptography.AesManaged::set_Mode(System.Security.Cryptography.CipherMode)
extern void AesManaged_set_Mode_mA5CF4C1F3B41503C6E09373ADB0B8983A6F61460 (void);
// 0x00000041 System.Security.Cryptography.PaddingMode System.Security.Cryptography.AesManaged::get_Padding()
extern void AesManaged_get_Padding_mD81B3F96D3421F6CD2189A01D65736A9098ACD45 (void);
// 0x00000042 System.Void System.Security.Cryptography.AesManaged::set_Padding(System.Security.Cryptography.PaddingMode)
extern void AesManaged_set_Padding_m6B07EC4A0F1F451417DC0AC64E9D637D7916866B (void);
// 0x00000043 System.Security.Cryptography.ICryptoTransform System.Security.Cryptography.AesManaged::CreateDecryptor()
extern void AesManaged_CreateDecryptor_m41AE4428FE60C9FD485640F3A09F1BF345452A3C (void);
// 0x00000044 System.Security.Cryptography.ICryptoTransform System.Security.Cryptography.AesManaged::CreateDecryptor(System.Byte[],System.Byte[])
extern void AesManaged_CreateDecryptor_m7240F8C38B99CE73159DE7455046E951C4900268 (void);
// 0x00000045 System.Security.Cryptography.ICryptoTransform System.Security.Cryptography.AesManaged::CreateEncryptor()
extern void AesManaged_CreateEncryptor_mB2BBCAB8753A59FFB572091D2EF80F287CD951BF (void);
// 0x00000046 System.Security.Cryptography.ICryptoTransform System.Security.Cryptography.AesManaged::CreateEncryptor(System.Byte[],System.Byte[])
extern void AesManaged_CreateEncryptor_m1E4EB80DE75FCF9E940228E1D7664C0EA1378153 (void);
// 0x00000047 System.Void System.Security.Cryptography.AesManaged::Dispose(System.Boolean)
extern void AesManaged_Dispose_mB0D969841D51825F37095A93E73A50C15C1A1477 (void);
// 0x00000048 System.Void System.Security.Cryptography.AesManaged::GenerateIV()
extern void AesManaged_GenerateIV_mBB19651CC37782273A882055D4E63370268F2D91 (void);
// 0x00000049 System.Void System.Security.Cryptography.AesManaged::GenerateKey()
extern void AesManaged_GenerateKey_mF6673B955AE82377595277C6B78C7DA8A16F480E (void);
// 0x0000004A System.Void System.Security.Cryptography.AesCryptoServiceProvider::.ctor()
extern void AesCryptoServiceProvider__ctor_mA9857852BC34D8AB0F463C1AF1837CBBD9102265 (void);
// 0x0000004B System.Void System.Security.Cryptography.AesCryptoServiceProvider::GenerateIV()
extern void AesCryptoServiceProvider_GenerateIV_m18539D5136BA9A2FC71F439150D16E35AD3BF5C4 (void);
// 0x0000004C System.Void System.Security.Cryptography.AesCryptoServiceProvider::GenerateKey()
extern void AesCryptoServiceProvider_GenerateKey_m574F877FD23D1F07033FC035E89BE232303F3502 (void);
// 0x0000004D System.Security.Cryptography.ICryptoTransform System.Security.Cryptography.AesCryptoServiceProvider::CreateDecryptor(System.Byte[],System.Byte[])
extern void AesCryptoServiceProvider_CreateDecryptor_mAB5FB857F549A86D986461C8665BE6B2393305D1 (void);
// 0x0000004E System.Security.Cryptography.ICryptoTransform System.Security.Cryptography.AesCryptoServiceProvider::CreateEncryptor(System.Byte[],System.Byte[])
extern void AesCryptoServiceProvider_CreateEncryptor_m6BF20D5D8424DB627CD3010D9E4C8555C6BD0465 (void);
// 0x0000004F System.Byte[] System.Security.Cryptography.AesCryptoServiceProvider::get_IV()
extern void AesCryptoServiceProvider_get_IV_m6A46F1C255ABE41F98BEE8C0C37D6AFBB9F29D34 (void);
// 0x00000050 System.Void System.Security.Cryptography.AesCryptoServiceProvider::set_IV(System.Byte[])
extern void AesCryptoServiceProvider_set_IV_mCB88C0F651B17F3EC7575F16E14C9E3BD2DB24DB (void);
// 0x00000051 System.Byte[] System.Security.Cryptography.AesCryptoServiceProvider::get_Key()
extern void AesCryptoServiceProvider_get_Key_mAC979BC922E8F1F15B36220E77972AC9CE5D5252 (void);
// 0x00000052 System.Void System.Security.Cryptography.AesCryptoServiceProvider::set_Key(System.Byte[])
extern void AesCryptoServiceProvider_set_Key_m65785032C270005BC120157A0C9D019F6F6BC96F (void);
// 0x00000053 System.Int32 System.Security.Cryptography.AesCryptoServiceProvider::get_KeySize()
extern void AesCryptoServiceProvider_get_KeySize_m3081171DF6C11CA55ECEBA29B9559D18E78D8058 (void);
// 0x00000054 System.Void System.Security.Cryptography.AesCryptoServiceProvider::set_KeySize(System.Int32)
extern void AesCryptoServiceProvider_set_KeySize_mA994D2D3098216C0B8C4F02C0F0A0F63D4256218 (void);
// 0x00000055 System.Int32 System.Security.Cryptography.AesCryptoServiceProvider::get_FeedbackSize()
extern void AesCryptoServiceProvider_get_FeedbackSize_m9DC2E1C3E84CC674ADB2D7E6B06066F333BEC89D (void);
// 0x00000056 System.Void System.Security.Cryptography.AesCryptoServiceProvider::set_FeedbackSize(System.Int32)
extern void AesCryptoServiceProvider_set_FeedbackSize_m5B367A05D9F985C7C83425637637B840858C255D (void);
// 0x00000057 System.Security.Cryptography.CipherMode System.Security.Cryptography.AesCryptoServiceProvider::get_Mode()
extern void AesCryptoServiceProvider_get_Mode_m3E1CBFD4D7CE748F3AB615EB88DE1A5D7238285D (void);
// 0x00000058 System.Void System.Security.Cryptography.AesCryptoServiceProvider::set_Mode(System.Security.Cryptography.CipherMode)
extern void AesCryptoServiceProvider_set_Mode_mFE7044929761BABE312D1146B0ED51B331E35D63 (void);
// 0x00000059 System.Security.Cryptography.PaddingMode System.Security.Cryptography.AesCryptoServiceProvider::get_Padding()
extern void AesCryptoServiceProvider_get_Padding_m89D49B05949BA2C6C557EFA5211B4934D279C7AD (void);
// 0x0000005A System.Void System.Security.Cryptography.AesCryptoServiceProvider::set_Padding(System.Security.Cryptography.PaddingMode)
extern void AesCryptoServiceProvider_set_Padding_mD3353CD8F4B931AA00203000140520775643F96E (void);
// 0x0000005B System.Security.Cryptography.ICryptoTransform System.Security.Cryptography.AesCryptoServiceProvider::CreateDecryptor()
extern void AesCryptoServiceProvider_CreateDecryptor_mB1F90A7339DA65542795E17DF9C37810BD088DDF (void);
// 0x0000005C System.Security.Cryptography.ICryptoTransform System.Security.Cryptography.AesCryptoServiceProvider::CreateEncryptor()
extern void AesCryptoServiceProvider_CreateEncryptor_m9555DFFCA344DF06C8B88DDE2EB987B3958EC6BB (void);
// 0x0000005D System.Void System.Security.Cryptography.AesCryptoServiceProvider::Dispose(System.Boolean)
extern void AesCryptoServiceProvider_Dispose_m7123198904819E2BF2B1398E20047B316C3D7D1E (void);
// 0x0000005E System.Void System.Security.Cryptography.AesTransform::.ctor(System.Security.Cryptography.Aes,System.Boolean,System.Byte[],System.Byte[])
extern void AesTransform__ctor_m3903A599E8B2C3F7AB3B70E1258980151D639598 (void);
// 0x0000005F System.Void System.Security.Cryptography.AesTransform::ECB(System.Byte[],System.Byte[])
extern void AesTransform_ECB_m2E2F4E2B307B0D34FEADF38684007E622FCEDFD1 (void);
// 0x00000060 System.UInt32 System.Security.Cryptography.AesTransform::SubByte(System.UInt32)
extern void AesTransform_SubByte_m2D77D545ABD3D84C04741B80ABB74BEFE8C55679 (void);
// 0x00000061 System.Void System.Security.Cryptography.AesTransform::Encrypt128(System.Byte[],System.Byte[],System.UInt32[])
extern void AesTransform_Encrypt128_m57DA74A7E05818DFD92F2614F8F65B0D1E696129 (void);
// 0x00000062 System.Void System.Security.Cryptography.AesTransform::Decrypt128(System.Byte[],System.Byte[],System.UInt32[])
extern void AesTransform_Decrypt128_m075F7BA40A4CFECA6F6A379065B731586EDDB23A (void);
// 0x00000063 System.Void System.Security.Cryptography.AesTransform::.cctor()
extern void AesTransform__cctor_mAC6D46ED54345C2D23DFCA026C69029757222CFD (void);
// 0x00000064 System.Exception System.Linq.Error::ArgumentNull(System.String)
extern void Error_ArgumentNull_m0EDA0D46D72CA692518E3E2EB75B48044D8FD41E (void);
// 0x00000065 System.Exception System.Linq.Error::ArgumentOutOfRange(System.String)
extern void Error_ArgumentOutOfRange_m2EFB999454161A6B48F8DAC3753FDC190538F0F2 (void);
// 0x00000066 System.Exception System.Linq.Error::MoreThanOneElement()
extern void Error_MoreThanOneElement_mDB56C9FA4C344A86553D6AFB66D10B290302C53A (void);
// 0x00000067 System.Exception System.Linq.Error::MoreThanOneMatch()
extern void Error_MoreThanOneMatch_m4C4756AF34A76EF12F3B2B6D8C78DE547F0FBCF8 (void);
// 0x00000068 System.Exception System.Linq.Error::NoElements()
extern void Error_NoElements_mB89E91246572F009281D79730950808F17C3F353 (void);
// 0x00000069 System.Exception System.Linq.Error::NotSupported()
extern void Error_NotSupported_m51A0560ABF374B66CF6D1208DAF27C4CBAD9AABA (void);
// 0x0000006A System.Collections.Generic.IEnumerable`1<TSource> System.Linq.Enumerable::Where(System.Collections.Generic.IEnumerable`1<TSource>,System.Func`2<TSource,System.Boolean>)
// 0x0000006B System.Collections.Generic.IEnumerable`1<TResult> System.Linq.Enumerable::Select(System.Collections.Generic.IEnumerable`1<TSource>,System.Func`2<TSource,TResult>)
// 0x0000006C System.Func`2<TSource,System.Boolean> System.Linq.Enumerable::CombinePredicates(System.Func`2<TSource,System.Boolean>,System.Func`2<TSource,System.Boolean>)
// 0x0000006D System.Func`2<TSource,TResult> System.Linq.Enumerable::CombineSelectors(System.Func`2<TSource,TMiddle>,System.Func`2<TMiddle,TResult>)
// 0x0000006E System.Collections.Generic.IEnumerable`1<TResult> System.Linq.Enumerable::SelectMany(System.Collections.Generic.IEnumerable`1<TSource>,System.Func`2<TSource,System.Collections.Generic.IEnumerable`1<TResult>>)
// 0x0000006F System.Collections.Generic.IEnumerable`1<TResult> System.Linq.Enumerable::SelectManyIterator(System.Collections.Generic.IEnumerable`1<TSource>,System.Func`2<TSource,System.Collections.Generic.IEnumerable`1<TResult>>)
// 0x00000070 System.Linq.IOrderedEnumerable`1<TSource> System.Linq.Enumerable::OrderBy(System.Collections.Generic.IEnumerable`1<TSource>,System.Func`2<TSource,TKey>)
// 0x00000071 System.Linq.IOrderedEnumerable`1<TSource> System.Linq.Enumerable::ThenBy(System.Linq.IOrderedEnumerable`1<TSource>,System.Func`2<TSource,TKey>)
// 0x00000072 System.Collections.Generic.IEnumerable`1<System.Linq.IGrouping`2<TKey,TSource>> System.Linq.Enumerable::GroupBy(System.Collections.Generic.IEnumerable`1<TSource>,System.Func`2<TSource,TKey>)
// 0x00000073 System.Collections.Generic.IEnumerable`1<TSource> System.Linq.Enumerable::Concat(System.Collections.Generic.IEnumerable`1<TSource>,System.Collections.Generic.IEnumerable`1<TSource>)
// 0x00000074 System.Collections.Generic.IEnumerable`1<TSource> System.Linq.Enumerable::ConcatIterator(System.Collections.Generic.IEnumerable`1<TSource>,System.Collections.Generic.IEnumerable`1<TSource>)
// 0x00000075 System.Collections.Generic.IEnumerable`1<TSource> System.Linq.Enumerable::Union(System.Collections.Generic.IEnumerable`1<TSource>,System.Collections.Generic.IEnumerable`1<TSource>)
// 0x00000076 System.Collections.Generic.IEnumerable`1<TSource> System.Linq.Enumerable::UnionIterator(System.Collections.Generic.IEnumerable`1<TSource>,System.Collections.Generic.IEnumerable`1<TSource>,System.Collections.Generic.IEqualityComparer`1<TSource>)
// 0x00000077 TSource[] System.Linq.Enumerable::ToArray(System.Collections.Generic.IEnumerable`1<TSource>)
// 0x00000078 System.Collections.Generic.List`1<TSource> System.Linq.Enumerable::ToList(System.Collections.Generic.IEnumerable`1<TSource>)
// 0x00000079 System.Collections.Generic.Dictionary`2<TKey,TElement> System.Linq.Enumerable::ToDictionary(System.Collections.Generic.IEnumerable`1<TSource>,System.Func`2<TSource,TKey>,System.Func`2<TSource,TElement>)
// 0x0000007A System.Collections.Generic.Dictionary`2<TKey,TElement> System.Linq.Enumerable::ToDictionary(System.Collections.Generic.IEnumerable`1<TSource>,System.Func`2<TSource,TKey>,System.Func`2<TSource,TElement>,System.Collections.Generic.IEqualityComparer`1<TKey>)
// 0x0000007B System.Collections.Generic.IEnumerable`1<TResult> System.Linq.Enumerable::Cast(System.Collections.IEnumerable)
// 0x0000007C System.Collections.Generic.IEnumerable`1<TResult> System.Linq.Enumerable::CastIterator(System.Collections.IEnumerable)
// 0x0000007D TSource System.Linq.Enumerable::First(System.Collections.Generic.IEnumerable`1<TSource>)
// 0x0000007E TSource System.Linq.Enumerable::FirstOrDefault(System.Collections.Generic.IEnumerable`1<TSource>)
// 0x0000007F TSource System.Linq.Enumerable::Last(System.Collections.Generic.IEnumerable`1<TSource>)
// 0x00000080 TSource System.Linq.Enumerable::LastOrDefault(System.Collections.Generic.IEnumerable`1<TSource>,System.Func`2<TSource,System.Boolean>)
// 0x00000081 TSource System.Linq.Enumerable::Single(System.Collections.Generic.IEnumerable`1<TSource>)
// 0x00000082 TSource System.Linq.Enumerable::SingleOrDefault(System.Collections.Generic.IEnumerable`1<TSource>)
// 0x00000083 TSource System.Linq.Enumerable::SingleOrDefault(System.Collections.Generic.IEnumerable`1<TSource>,System.Func`2<TSource,System.Boolean>)
// 0x00000084 System.Collections.Generic.IEnumerable`1<TResult> System.Linq.Enumerable::Repeat(TResult,System.Int32)
// 0x00000085 System.Collections.Generic.IEnumerable`1<TResult> System.Linq.Enumerable::RepeatIterator(TResult,System.Int32)
// 0x00000086 System.Collections.Generic.IEnumerable`1<TResult> System.Linq.Enumerable::Empty()
// 0x00000087 System.Boolean System.Linq.Enumerable::Any(System.Collections.Generic.IEnumerable`1<TSource>)
// 0x00000088 System.Boolean System.Linq.Enumerable::Any(System.Collections.Generic.IEnumerable`1<TSource>,System.Func`2<TSource,System.Boolean>)
// 0x00000089 System.Boolean System.Linq.Enumerable::All(System.Collections.Generic.IEnumerable`1<TSource>,System.Func`2<TSource,System.Boolean>)
// 0x0000008A System.Int32 System.Linq.Enumerable::Count(System.Collections.Generic.IEnumerable`1<TSource>)
// 0x0000008B System.Boolean System.Linq.Enumerable::Contains(System.Collections.Generic.IEnumerable`1<TSource>,TSource)
// 0x0000008C System.Boolean System.Linq.Enumerable::Contains(System.Collections.Generic.IEnumerable`1<TSource>,TSource,System.Collections.Generic.IEqualityComparer`1<TSource>)
// 0x0000008D TAccumulate System.Linq.Enumerable::Aggregate(System.Collections.Generic.IEnumerable`1<TSource>,TAccumulate,System.Func`3<TAccumulate,TSource,TAccumulate>)
// 0x0000008E System.Int64 System.Linq.Enumerable::Sum(System.Collections.Generic.IEnumerable`1<System.Int64>)
extern void Enumerable_Sum_m5A80A2FF702FA59CBEDB5AAEDB98FE91BF8B557A (void);
// 0x0000008F System.Int64 System.Linq.Enumerable::Sum(System.Collections.Generic.IEnumerable`1<TSource>,System.Func`2<TSource,System.Int64>)
// 0x00000090 System.Single System.Linq.Enumerable::Max(System.Collections.Generic.IEnumerable`1<System.Single>)
extern void Enumerable_Max_m2E60496646FFAAB20A13DEE9F52EC21F0054B72B (void);
// 0x00000091 System.Single System.Linq.Enumerable::Average(System.Collections.Generic.IEnumerable`1<System.Single>)
extern void Enumerable_Average_mCC66070D3714E5082DD32AAB83A2E6F158241BB6 (void);
// 0x00000092 System.Void System.Linq.Enumerable/Iterator`1::.ctor()
// 0x00000093 TSource System.Linq.Enumerable/Iterator`1::get_Current()
// 0x00000094 System.Linq.Enumerable/Iterator`1<TSource> System.Linq.Enumerable/Iterator`1::Clone()
// 0x00000095 System.Void System.Linq.Enumerable/Iterator`1::Dispose()
// 0x00000096 System.Collections.Generic.IEnumerator`1<TSource> System.Linq.Enumerable/Iterator`1::GetEnumerator()
// 0x00000097 System.Boolean System.Linq.Enumerable/Iterator`1::MoveNext()
// 0x00000098 System.Collections.Generic.IEnumerable`1<TResult> System.Linq.Enumerable/Iterator`1::Select(System.Func`2<TSource,TResult>)
// 0x00000099 System.Collections.Generic.IEnumerable`1<TSource> System.Linq.Enumerable/Iterator`1::Where(System.Func`2<TSource,System.Boolean>)
// 0x0000009A System.Object System.Linq.Enumerable/Iterator`1::System.Collections.IEnumerator.get_Current()
// 0x0000009B System.Collections.IEnumerator System.Linq.Enumerable/Iterator`1::System.Collections.IEnumerable.GetEnumerator()
// 0x0000009C System.Void System.Linq.Enumerable/Iterator`1::System.Collections.IEnumerator.Reset()
// 0x0000009D System.Void System.Linq.Enumerable/WhereEnumerableIterator`1::.ctor(System.Collections.Generic.IEnumerable`1<TSource>,System.Func`2<TSource,System.Boolean>)
// 0x0000009E System.Linq.Enumerable/Iterator`1<TSource> System.Linq.Enumerable/WhereEnumerableIterator`1::Clone()
// 0x0000009F System.Void System.Linq.Enumerable/WhereEnumerableIterator`1::Dispose()
// 0x000000A0 System.Boolean System.Linq.Enumerable/WhereEnumerableIterator`1::MoveNext()
// 0x000000A1 System.Collections.Generic.IEnumerable`1<TResult> System.Linq.Enumerable/WhereEnumerableIterator`1::Select(System.Func`2<TSource,TResult>)
// 0x000000A2 System.Collections.Generic.IEnumerable`1<TSource> System.Linq.Enumerable/WhereEnumerableIterator`1::Where(System.Func`2<TSource,System.Boolean>)
// 0x000000A3 System.Void System.Linq.Enumerable/WhereArrayIterator`1::.ctor(TSource[],System.Func`2<TSource,System.Boolean>)
// 0x000000A4 System.Linq.Enumerable/Iterator`1<TSource> System.Linq.Enumerable/WhereArrayIterator`1::Clone()
// 0x000000A5 System.Boolean System.Linq.Enumerable/WhereArrayIterator`1::MoveNext()
// 0x000000A6 System.Collections.Generic.IEnumerable`1<TResult> System.Linq.Enumerable/WhereArrayIterator`1::Select(System.Func`2<TSource,TResult>)
// 0x000000A7 System.Collections.Generic.IEnumerable`1<TSource> System.Linq.Enumerable/WhereArrayIterator`1::Where(System.Func`2<TSource,System.Boolean>)
// 0x000000A8 System.Void System.Linq.Enumerable/WhereListIterator`1::.ctor(System.Collections.Generic.List`1<TSource>,System.Func`2<TSource,System.Boolean>)
// 0x000000A9 System.Linq.Enumerable/Iterator`1<TSource> System.Linq.Enumerable/WhereListIterator`1::Clone()
// 0x000000AA System.Boolean System.Linq.Enumerable/WhereListIterator`1::MoveNext()
// 0x000000AB System.Collections.Generic.IEnumerable`1<TResult> System.Linq.Enumerable/WhereListIterator`1::Select(System.Func`2<TSource,TResult>)
// 0x000000AC System.Collections.Generic.IEnumerable`1<TSource> System.Linq.Enumerable/WhereListIterator`1::Where(System.Func`2<TSource,System.Boolean>)
// 0x000000AD System.Void System.Linq.Enumerable/WhereSelectEnumerableIterator`2::.ctor(System.Collections.Generic.IEnumerable`1<TSource>,System.Func`2<TSource,System.Boolean>,System.Func`2<TSource,TResult>)
// 0x000000AE System.Linq.Enumerable/Iterator`1<TResult> System.Linq.Enumerable/WhereSelectEnumerableIterator`2::Clone()
// 0x000000AF System.Void System.Linq.Enumerable/WhereSelectEnumerableIterator`2::Dispose()
// 0x000000B0 System.Boolean System.Linq.Enumerable/WhereSelectEnumerableIterator`2::MoveNext()
// 0x000000B1 System.Collections.Generic.IEnumerable`1<TResult2> System.Linq.Enumerable/WhereSelectEnumerableIterator`2::Select(System.Func`2<TResult,TResult2>)
// 0x000000B2 System.Collections.Generic.IEnumerable`1<TResult> System.Linq.Enumerable/WhereSelectEnumerableIterator`2::Where(System.Func`2<TResult,System.Boolean>)
// 0x000000B3 System.Void System.Linq.Enumerable/WhereSelectArrayIterator`2::.ctor(TSource[],System.Func`2<TSource,System.Boolean>,System.Func`2<TSource,TResult>)
// 0x000000B4 System.Linq.Enumerable/Iterator`1<TResult> System.Linq.Enumerable/WhereSelectArrayIterator`2::Clone()
// 0x000000B5 System.Boolean System.Linq.Enumerable/WhereSelectArrayIterator`2::MoveNext()
// 0x000000B6 System.Collections.Generic.IEnumerable`1<TResult2> System.Linq.Enumerable/WhereSelectArrayIterator`2::Select(System.Func`2<TResult,TResult2>)
// 0x000000B7 System.Collections.Generic.IEnumerable`1<TResult> System.Linq.Enumerable/WhereSelectArrayIterator`2::Where(System.Func`2<TResult,System.Boolean>)
// 0x000000B8 System.Void System.Linq.Enumerable/WhereSelectListIterator`2::.ctor(System.Collections.Generic.List`1<TSource>,System.Func`2<TSource,System.Boolean>,System.Func`2<TSource,TResult>)
// 0x000000B9 System.Linq.Enumerable/Iterator`1<TResult> System.Linq.Enumerable/WhereSelectListIterator`2::Clone()
// 0x000000BA System.Boolean System.Linq.Enumerable/WhereSelectListIterator`2::MoveNext()
// 0x000000BB System.Collections.Generic.IEnumerable`1<TResult2> System.Linq.Enumerable/WhereSelectListIterator`2::Select(System.Func`2<TResult,TResult2>)
// 0x000000BC System.Collections.Generic.IEnumerable`1<TResult> System.Linq.Enumerable/WhereSelectListIterator`2::Where(System.Func`2<TResult,System.Boolean>)
// 0x000000BD System.Void System.Linq.Enumerable/<>c__DisplayClass6_0`1::.ctor()
// 0x000000BE System.Boolean System.Linq.Enumerable/<>c__DisplayClass6_0`1::<CombinePredicates>b__0(TSource)
// 0x000000BF System.Void System.Linq.Enumerable/<>c__DisplayClass7_0`3::.ctor()
// 0x000000C0 TResult System.Linq.Enumerable/<>c__DisplayClass7_0`3::<CombineSelectors>b__0(TSource)
// 0x000000C1 System.Void System.Linq.Enumerable/<SelectManyIterator>d__17`2::.ctor(System.Int32)
// 0x000000C2 System.Void System.Linq.Enumerable/<SelectManyIterator>d__17`2::System.IDisposable.Dispose()
// 0x000000C3 System.Boolean System.Linq.Enumerable/<SelectManyIterator>d__17`2::MoveNext()
// 0x000000C4 System.Void System.Linq.Enumerable/<SelectManyIterator>d__17`2::<>m__Finally1()
// 0x000000C5 System.Void System.Linq.Enumerable/<SelectManyIterator>d__17`2::<>m__Finally2()
// 0x000000C6 TResult System.Linq.Enumerable/<SelectManyIterator>d__17`2::System.Collections.Generic.IEnumerator<TResult>.get_Current()
// 0x000000C7 System.Void System.Linq.Enumerable/<SelectManyIterator>d__17`2::System.Collections.IEnumerator.Reset()
// 0x000000C8 System.Object System.Linq.Enumerable/<SelectManyIterator>d__17`2::System.Collections.IEnumerator.get_Current()
// 0x000000C9 System.Collections.Generic.IEnumerator`1<TResult> System.Linq.Enumerable/<SelectManyIterator>d__17`2::System.Collections.Generic.IEnumerable<TResult>.GetEnumerator()
// 0x000000CA System.Collections.IEnumerator System.Linq.Enumerable/<SelectManyIterator>d__17`2::System.Collections.IEnumerable.GetEnumerator()
// 0x000000CB System.Void System.Linq.Enumerable/<ConcatIterator>d__59`1::.ctor(System.Int32)
// 0x000000CC System.Void System.Linq.Enumerable/<ConcatIterator>d__59`1::System.IDisposable.Dispose()
// 0x000000CD System.Boolean System.Linq.Enumerable/<ConcatIterator>d__59`1::MoveNext()
// 0x000000CE System.Void System.Linq.Enumerable/<ConcatIterator>d__59`1::<>m__Finally1()
// 0x000000CF System.Void System.Linq.Enumerable/<ConcatIterator>d__59`1::<>m__Finally2()
// 0x000000D0 TSource System.Linq.Enumerable/<ConcatIterator>d__59`1::System.Collections.Generic.IEnumerator<TSource>.get_Current()
// 0x000000D1 System.Void System.Linq.Enumerable/<ConcatIterator>d__59`1::System.Collections.IEnumerator.Reset()
// 0x000000D2 System.Object System.Linq.Enumerable/<ConcatIterator>d__59`1::System.Collections.IEnumerator.get_Current()
// 0x000000D3 System.Collections.Generic.IEnumerator`1<TSource> System.Linq.Enumerable/<ConcatIterator>d__59`1::System.Collections.Generic.IEnumerable<TSource>.GetEnumerator()
// 0x000000D4 System.Collections.IEnumerator System.Linq.Enumerable/<ConcatIterator>d__59`1::System.Collections.IEnumerable.GetEnumerator()
// 0x000000D5 System.Void System.Linq.Enumerable/<UnionIterator>d__71`1::.ctor(System.Int32)
// 0x000000D6 System.Void System.Linq.Enumerable/<UnionIterator>d__71`1::System.IDisposable.Dispose()
// 0x000000D7 System.Boolean System.Linq.Enumerable/<UnionIterator>d__71`1::MoveNext()
// 0x000000D8 System.Void System.Linq.Enumerable/<UnionIterator>d__71`1::<>m__Finally1()
// 0x000000D9 System.Void System.Linq.Enumerable/<UnionIterator>d__71`1::<>m__Finally2()
// 0x000000DA TSource System.Linq.Enumerable/<UnionIterator>d__71`1::System.Collections.Generic.IEnumerator<TSource>.get_Current()
// 0x000000DB System.Void System.Linq.Enumerable/<UnionIterator>d__71`1::System.Collections.IEnumerator.Reset()
// 0x000000DC System.Object System.Linq.Enumerable/<UnionIterator>d__71`1::System.Collections.IEnumerator.get_Current()
// 0x000000DD System.Collections.Generic.IEnumerator`1<TSource> System.Linq.Enumerable/<UnionIterator>d__71`1::System.Collections.Generic.IEnumerable<TSource>.GetEnumerator()
// 0x000000DE System.Collections.IEnumerator System.Linq.Enumerable/<UnionIterator>d__71`1::System.Collections.IEnumerable.GetEnumerator()
// 0x000000DF System.Void System.Linq.Enumerable/<CastIterator>d__99`1::.ctor(System.Int32)
// 0x000000E0 System.Void System.Linq.Enumerable/<CastIterator>d__99`1::System.IDisposable.Dispose()
// 0x000000E1 System.Boolean System.Linq.Enumerable/<CastIterator>d__99`1::MoveNext()
// 0x000000E2 System.Void System.Linq.Enumerable/<CastIterator>d__99`1::<>m__Finally1()
// 0x000000E3 TResult System.Linq.Enumerable/<CastIterator>d__99`1::System.Collections.Generic.IEnumerator<TResult>.get_Current()
// 0x000000E4 System.Void System.Linq.Enumerable/<CastIterator>d__99`1::System.Collections.IEnumerator.Reset()
// 0x000000E5 System.Object System.Linq.Enumerable/<CastIterator>d__99`1::System.Collections.IEnumerator.get_Current()
// 0x000000E6 System.Collections.Generic.IEnumerator`1<TResult> System.Linq.Enumerable/<CastIterator>d__99`1::System.Collections.Generic.IEnumerable<TResult>.GetEnumerator()
// 0x000000E7 System.Collections.IEnumerator System.Linq.Enumerable/<CastIterator>d__99`1::System.Collections.IEnumerable.GetEnumerator()
// 0x000000E8 System.Void System.Linq.Enumerable/<RepeatIterator>d__117`1::.ctor(System.Int32)
// 0x000000E9 System.Void System.Linq.Enumerable/<RepeatIterator>d__117`1::System.IDisposable.Dispose()
// 0x000000EA System.Boolean System.Linq.Enumerable/<RepeatIterator>d__117`1::MoveNext()
// 0x000000EB TResult System.Linq.Enumerable/<RepeatIterator>d__117`1::System.Collections.Generic.IEnumerator<TResult>.get_Current()
// 0x000000EC System.Void System.Linq.Enumerable/<RepeatIterator>d__117`1::System.Collections.IEnumerator.Reset()
// 0x000000ED System.Object System.Linq.Enumerable/<RepeatIterator>d__117`1::System.Collections.IEnumerator.get_Current()
// 0x000000EE System.Collections.Generic.IEnumerator`1<TResult> System.Linq.Enumerable/<RepeatIterator>d__117`1::System.Collections.Generic.IEnumerable<TResult>.GetEnumerator()
// 0x000000EF System.Collections.IEnumerator System.Linq.Enumerable/<RepeatIterator>d__117`1::System.Collections.IEnumerable.GetEnumerator()
// 0x000000F0 System.Void System.Linq.EmptyEnumerable`1::.cctor()
// 0x000000F1 System.Func`2<TElement,TElement> System.Linq.IdentityFunction`1::get_Instance()
// 0x000000F2 System.Void System.Linq.IdentityFunction`1/<>c::.cctor()
// 0x000000F3 System.Void System.Linq.IdentityFunction`1/<>c::.ctor()
// 0x000000F4 TElement System.Linq.IdentityFunction`1/<>c::<get_Instance>b__1_0(TElement)
// 0x000000F5 System.Linq.IOrderedEnumerable`1<TElement> System.Linq.IOrderedEnumerable`1::CreateOrderedEnumerable(System.Func`2<TElement,TKey>,System.Collections.Generic.IComparer`1<TKey>,System.Boolean)
// 0x000000F6 System.Linq.Lookup`2<TKey,TElement> System.Linq.Lookup`2::Create(System.Collections.Generic.IEnumerable`1<TSource>,System.Func`2<TSource,TKey>,System.Func`2<TSource,TElement>,System.Collections.Generic.IEqualityComparer`1<TKey>)
// 0x000000F7 System.Void System.Linq.Lookup`2::.ctor(System.Collections.Generic.IEqualityComparer`1<TKey>)
// 0x000000F8 System.Collections.Generic.IEnumerator`1<System.Linq.IGrouping`2<TKey,TElement>> System.Linq.Lookup`2::GetEnumerator()
// 0x000000F9 System.Collections.IEnumerator System.Linq.Lookup`2::System.Collections.IEnumerable.GetEnumerator()
// 0x000000FA System.Int32 System.Linq.Lookup`2::InternalGetHashCode(TKey)
// 0x000000FB System.Linq.Lookup`2/Grouping<TKey,TElement> System.Linq.Lookup`2::GetGrouping(TKey,System.Boolean)
// 0x000000FC System.Void System.Linq.Lookup`2::Resize()
// 0x000000FD System.Void System.Linq.Lookup`2/Grouping::Add(TElement)
// 0x000000FE System.Collections.Generic.IEnumerator`1<TElement> System.Linq.Lookup`2/Grouping::GetEnumerator()
// 0x000000FF System.Collections.IEnumerator System.Linq.Lookup`2/Grouping::System.Collections.IEnumerable.GetEnumerator()
// 0x00000100 System.Int32 System.Linq.Lookup`2/Grouping::System.Collections.Generic.ICollection<TElement>.get_Count()
// 0x00000101 System.Boolean System.Linq.Lookup`2/Grouping::System.Collections.Generic.ICollection<TElement>.get_IsReadOnly()
// 0x00000102 System.Void System.Linq.Lookup`2/Grouping::System.Collections.Generic.ICollection<TElement>.Add(TElement)
// 0x00000103 System.Void System.Linq.Lookup`2/Grouping::System.Collections.Generic.ICollection<TElement>.Clear()
// 0x00000104 System.Boolean System.Linq.Lookup`2/Grouping::System.Collections.Generic.ICollection<TElement>.Contains(TElement)
// 0x00000105 System.Void System.Linq.Lookup`2/Grouping::System.Collections.Generic.ICollection<TElement>.CopyTo(TElement[],System.Int32)
// 0x00000106 System.Boolean System.Linq.Lookup`2/Grouping::System.Collections.Generic.ICollection<TElement>.Remove(TElement)
// 0x00000107 System.Int32 System.Linq.Lookup`2/Grouping::System.Collections.Generic.IList<TElement>.IndexOf(TElement)
// 0x00000108 System.Void System.Linq.Lookup`2/Grouping::System.Collections.Generic.IList<TElement>.Insert(System.Int32,TElement)
// 0x00000109 System.Void System.Linq.Lookup`2/Grouping::System.Collections.Generic.IList<TElement>.RemoveAt(System.Int32)
// 0x0000010A TElement System.Linq.Lookup`2/Grouping::System.Collections.Generic.IList<TElement>.get_Item(System.Int32)
// 0x0000010B System.Void System.Linq.Lookup`2/Grouping::System.Collections.Generic.IList<TElement>.set_Item(System.Int32,TElement)
// 0x0000010C System.Void System.Linq.Lookup`2/Grouping::.ctor()
// 0x0000010D System.Void System.Linq.Lookup`2/Grouping/<GetEnumerator>d__7::.ctor(System.Int32)
// 0x0000010E System.Void System.Linq.Lookup`2/Grouping/<GetEnumerator>d__7::System.IDisposable.Dispose()
// 0x0000010F System.Boolean System.Linq.Lookup`2/Grouping/<GetEnumerator>d__7::MoveNext()
// 0x00000110 TElement System.Linq.Lookup`2/Grouping/<GetEnumerator>d__7::System.Collections.Generic.IEnumerator<TElement>.get_Current()
// 0x00000111 System.Void System.Linq.Lookup`2/Grouping/<GetEnumerator>d__7::System.Collections.IEnumerator.Reset()
// 0x00000112 System.Object System.Linq.Lookup`2/Grouping/<GetEnumerator>d__7::System.Collections.IEnumerator.get_Current()
// 0x00000113 System.Void System.Linq.Lookup`2/<GetEnumerator>d__12::.ctor(System.Int32)
// 0x00000114 System.Void System.Linq.Lookup`2/<GetEnumerator>d__12::System.IDisposable.Dispose()
// 0x00000115 System.Boolean System.Linq.Lookup`2/<GetEnumerator>d__12::MoveNext()
// 0x00000116 System.Linq.IGrouping`2<TKey,TElement> System.Linq.Lookup`2/<GetEnumerator>d__12::System.Collections.Generic.IEnumerator<System.Linq.IGrouping<TKey,TElement>>.get_Current()
// 0x00000117 System.Void System.Linq.Lookup`2/<GetEnumerator>d__12::System.Collections.IEnumerator.Reset()
// 0x00000118 System.Object System.Linq.Lookup`2/<GetEnumerator>d__12::System.Collections.IEnumerator.get_Current()
// 0x00000119 System.Void System.Linq.Set`1::.ctor(System.Collections.Generic.IEqualityComparer`1<TElement>)
// 0x0000011A System.Boolean System.Linq.Set`1::Add(TElement)
// 0x0000011B System.Boolean System.Linq.Set`1::Find(TElement,System.Boolean)
// 0x0000011C System.Void System.Linq.Set`1::Resize()
// 0x0000011D System.Int32 System.Linq.Set`1::InternalGetHashCode(TElement)
// 0x0000011E System.Void System.Linq.GroupedEnumerable`3::.ctor(System.Collections.Generic.IEnumerable`1<TSource>,System.Func`2<TSource,TKey>,System.Func`2<TSource,TElement>,System.Collections.Generic.IEqualityComparer`1<TKey>)
// 0x0000011F System.Collections.Generic.IEnumerator`1<System.Linq.IGrouping`2<TKey,TElement>> System.Linq.GroupedEnumerable`3::GetEnumerator()
// 0x00000120 System.Collections.IEnumerator System.Linq.GroupedEnumerable`3::System.Collections.IEnumerable.GetEnumerator()
// 0x00000121 System.Collections.Generic.IEnumerator`1<TElement> System.Linq.OrderedEnumerable`1::GetEnumerator()
// 0x00000122 System.Linq.EnumerableSorter`1<TElement> System.Linq.OrderedEnumerable`1::GetEnumerableSorter(System.Linq.EnumerableSorter`1<TElement>)
// 0x00000123 System.Collections.IEnumerator System.Linq.OrderedEnumerable`1::System.Collections.IEnumerable.GetEnumerator()
// 0x00000124 System.Linq.IOrderedEnumerable`1<TElement> System.Linq.OrderedEnumerable`1::System.Linq.IOrderedEnumerable<TElement>.CreateOrderedEnumerable(System.Func`2<TElement,TKey>,System.Collections.Generic.IComparer`1<TKey>,System.Boolean)
// 0x00000125 System.Void System.Linq.OrderedEnumerable`1::.ctor()
// 0x00000126 System.Void System.Linq.OrderedEnumerable`1/<GetEnumerator>d__1::.ctor(System.Int32)
// 0x00000127 System.Void System.Linq.OrderedEnumerable`1/<GetEnumerator>d__1::System.IDisposable.Dispose()
// 0x00000128 System.Boolean System.Linq.OrderedEnumerable`1/<GetEnumerator>d__1::MoveNext()
// 0x00000129 TElement System.Linq.OrderedEnumerable`1/<GetEnumerator>d__1::System.Collections.Generic.IEnumerator<TElement>.get_Current()
// 0x0000012A System.Void System.Linq.OrderedEnumerable`1/<GetEnumerator>d__1::System.Collections.IEnumerator.Reset()
// 0x0000012B System.Object System.Linq.OrderedEnumerable`1/<GetEnumerator>d__1::System.Collections.IEnumerator.get_Current()
// 0x0000012C System.Void System.Linq.OrderedEnumerable`2::.ctor(System.Collections.Generic.IEnumerable`1<TElement>,System.Func`2<TElement,TKey>,System.Collections.Generic.IComparer`1<TKey>,System.Boolean)
// 0x0000012D System.Linq.EnumerableSorter`1<TElement> System.Linq.OrderedEnumerable`2::GetEnumerableSorter(System.Linq.EnumerableSorter`1<TElement>)
// 0x0000012E System.Void System.Linq.EnumerableSorter`1::ComputeKeys(TElement[],System.Int32)
// 0x0000012F System.Int32 System.Linq.EnumerableSorter`1::CompareKeys(System.Int32,System.Int32)
// 0x00000130 System.Int32[] System.Linq.EnumerableSorter`1::Sort(TElement[],System.Int32)
// 0x00000131 System.Void System.Linq.EnumerableSorter`1::QuickSort(System.Int32[],System.Int32,System.Int32)
// 0x00000132 System.Void System.Linq.EnumerableSorter`1::.ctor()
// 0x00000133 System.Void System.Linq.EnumerableSorter`2::.ctor(System.Func`2<TElement,TKey>,System.Collections.Generic.IComparer`1<TKey>,System.Boolean,System.Linq.EnumerableSorter`1<TElement>)
// 0x00000134 System.Void System.Linq.EnumerableSorter`2::ComputeKeys(TElement[],System.Int32)
// 0x00000135 System.Int32 System.Linq.EnumerableSorter`2::CompareKeys(System.Int32,System.Int32)
// 0x00000136 System.Void System.Linq.Buffer`1::.ctor(System.Collections.Generic.IEnumerable`1<TElement>)
// 0x00000137 TElement[] System.Linq.Buffer`1::ToArray()
// 0x00000138 System.Void System.Collections.Generic.HashSet`1::.ctor()
// 0x00000139 System.Void System.Collections.Generic.HashSet`1::.ctor(System.Collections.Generic.IEqualityComparer`1<T>)
// 0x0000013A System.Void System.Collections.Generic.HashSet`1::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
// 0x0000013B System.Void System.Collections.Generic.HashSet`1::System.Collections.Generic.ICollection<T>.Add(T)
// 0x0000013C System.Void System.Collections.Generic.HashSet`1::Clear()
// 0x0000013D System.Boolean System.Collections.Generic.HashSet`1::Contains(T)
// 0x0000013E System.Void System.Collections.Generic.HashSet`1::CopyTo(T[],System.Int32)
// 0x0000013F System.Boolean System.Collections.Generic.HashSet`1::Remove(T)
// 0x00000140 System.Int32 System.Collections.Generic.HashSet`1::get_Count()
// 0x00000141 System.Boolean System.Collections.Generic.HashSet`1::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
// 0x00000142 System.Collections.Generic.HashSet`1/Enumerator<T> System.Collections.Generic.HashSet`1::GetEnumerator()
// 0x00000143 System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.HashSet`1::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
// 0x00000144 System.Collections.IEnumerator System.Collections.Generic.HashSet`1::System.Collections.IEnumerable.GetEnumerator()
// 0x00000145 System.Void System.Collections.Generic.HashSet`1::GetObjectData(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
// 0x00000146 System.Void System.Collections.Generic.HashSet`1::OnDeserialization(System.Object)
// 0x00000147 System.Boolean System.Collections.Generic.HashSet`1::Add(T)
// 0x00000148 System.Void System.Collections.Generic.HashSet`1::CopyTo(T[])
// 0x00000149 System.Void System.Collections.Generic.HashSet`1::CopyTo(T[],System.Int32,System.Int32)
// 0x0000014A System.Void System.Collections.Generic.HashSet`1::Initialize(System.Int32)
// 0x0000014B System.Void System.Collections.Generic.HashSet`1::IncreaseCapacity()
// 0x0000014C System.Void System.Collections.Generic.HashSet`1::SetCapacity(System.Int32)
// 0x0000014D System.Boolean System.Collections.Generic.HashSet`1::AddIfNotPresent(T)
// 0x0000014E System.Int32 System.Collections.Generic.HashSet`1::InternalGetHashCode(T)
// 0x0000014F System.Void System.Collections.Generic.HashSet`1/Enumerator::.ctor(System.Collections.Generic.HashSet`1<T>)
// 0x00000150 System.Void System.Collections.Generic.HashSet`1/Enumerator::Dispose()
// 0x00000151 System.Boolean System.Collections.Generic.HashSet`1/Enumerator::MoveNext()
// 0x00000152 T System.Collections.Generic.HashSet`1/Enumerator::get_Current()
// 0x00000153 System.Object System.Collections.Generic.HashSet`1/Enumerator::System.Collections.IEnumerator.get_Current()
// 0x00000154 System.Void System.Collections.Generic.HashSet`1/Enumerator::System.Collections.IEnumerator.Reset()
static Il2CppMethodPointer s_methodPointers[340] = 
{
	SR_GetString_mD7FC73A3473F4F165E55F8B4A7088F2E9F9CC412,
	ReaderWriterCount__ctor_m0C083844BE7A2C4172B9684CF08A786C1E51874B,
	ReaderWriterLockSlim_InitializeThreadCounts_m256062BF3249D0A11A4A3F9BD625198B719C98AD,
	ReaderWriterLockSlim__ctor_m76201E97862C090097E879E3F10FAA8CD0ADF1A1,
	ReaderWriterLockSlim_IsRWEntryEmpty_mC8DAC7CFE64A30203FF50C4823149F25FB15D116,
	ReaderWriterLockSlim_IsRwHashEntryChanged_m3EDD34CB05C022D9A2852EA9BA753642D20A4617,
	ReaderWriterLockSlim_GetThreadRWCount_m8CCEECA7603C844BACCAB04BB083976E0A85AD5A,
	ReaderWriterLockSlim_EnterReadLock_m2BE91DD529FF6A951B0A9BF4E99CDD57E7907FEF,
	ReaderWriterLockSlim_TryEnterReadLock_m52AE956B043DA2B69FAB59B8D5229F6A6A03A51B,
	ReaderWriterLockSlim_TryEnterReadLock_mA2D7C92AC16257ACBCA744B16B6C47485C6F969A,
	ReaderWriterLockSlim_TryEnterReadLockCore_mC6BC860B780596E9593D2748682CB7296EE9D1F7,
	ReaderWriterLockSlim_EnterWriteLock_m6906FE7058BCD5857E7036D763FA14A94D395386,
	ReaderWriterLockSlim_TryEnterWriteLock_mE8293BAF7A39EDB8E5C3C9F39BC6B404BC83C38F,
	ReaderWriterLockSlim_TryEnterWriteLock_m8DC7B1653133775A79E6FDBE072DF63C8E6822C7,
	ReaderWriterLockSlim_TryEnterWriteLockCore_m92031DB0DAF78611332535E47CCC146C90B9706F,
	ReaderWriterLockSlim_EnterUpgradeableReadLock_m49E33B0F164253250D2CE7D4F369365D4A278319,
	ReaderWriterLockSlim_TryEnterUpgradeableReadLock_mF0F2532E93D3821AD35647FE1DA56C2FEF01FDAA,
	ReaderWriterLockSlim_TryEnterUpgradeableReadLock_mE362E867B7E04114EF8BECDC6FF15D65C7D01C13,
	ReaderWriterLockSlim_TryEnterUpgradeableReadLockCore_mC3C1C2217C8FA13B0FA3C6C29C12052859A1B49B,
	ReaderWriterLockSlim_ExitReadLock_m912A6E70DAE228A948E8EB78C1C2EE49D78E9ADF,
	ReaderWriterLockSlim_ExitWriteLock_m2A49859BD012D6C7FCC58A7C37C3702B8C761BE2,
	ReaderWriterLockSlim_ExitUpgradeableReadLock_m870A35845A911065A8897A68EB1F9C28C08E38DE,
	ReaderWriterLockSlim_LazyCreateEvent_m02E55386C7E31D1CB3FC38C5D2E1EB89EB617644,
	ReaderWriterLockSlim_WaitOnEvent_m6C6EB73C0C8D57FF14A6F1EBE8A6756407903FF1,
	ReaderWriterLockSlim_ExitAndWakeUpAppropriateWaiters_m203B2A315D7812546CE39C84789145491C7729C5,
	ReaderWriterLockSlim_ExitAndWakeUpAppropriateWaitersPreferringWriters_mDE8BD2C760A17C2045AE43994C0D7E85AB316D5D,
	ReaderWriterLockSlim_ExitAndWakeUpAppropriateReadWaiters_mA0AA94088046F5DFAA395A193C3C45D3D2086389,
	ReaderWriterLockSlim_IsWriterAcquired_m9F14403D5E6BA8D9840274FFA9CA59E03FE9ADD9,
	ReaderWriterLockSlim_SetWriterAcquired_m24C2FFF43CE66EAF4E5434B345B1858EE92E39E2,
	ReaderWriterLockSlim_ClearWriterAcquired_m81A620F80C7AFCF5E4AC1DAB2F2A5B262E42AFD8,
	ReaderWriterLockSlim_SetWritersWaiting_m0E15480965179DBE2CF352BD140188DB5359DF5C,
	ReaderWriterLockSlim_ClearWritersWaiting_m97456CAD2756BAD28B6C0EF9B42E60BE7A35D669,
	ReaderWriterLockSlim_SetUpgraderWaiting_mF6E393FE0C46184DF6A1B1B8F2EFBD40A307827F,
	ReaderWriterLockSlim_ClearUpgraderWaiting_m85BBABDCA1A01D201683926A892CD4F75EAAA44A,
	ReaderWriterLockSlim_GetNumReaders_mC51603FD596E2B077422FA9331023A5D87DD3D2D,
	ReaderWriterLockSlim_EnterMyLock_mCE063DE6AD5C084F0EC81D24610EF4AD5A2F0E2A,
	ReaderWriterLockSlim_EnterMyLockSpin_mC63264D1309E935E22E6E2BFCAD04EAB8A19AE3C,
	ReaderWriterLockSlim_ExitMyLock_m4D09639AB6AA1E1FEF2BAEA70F3368B1DB88C6A8,
	ReaderWriterLockSlim_SpinWait_m4B2AD68AFAF0D0D8C4FACC6EE090CAA5AE7A8B00,
	ReaderWriterLockSlim_Dispose_m342639E5C3D64460ADC87ED495F4FD43B8C57A7C,
	ReaderWriterLockSlim_Dispose_mDA9910A42284308F6EA8F8A294FF52AC4E05699E,
	ReaderWriterLockSlim_get_IsReadLockHeld_m7DCD5FD5F491F1F19B202A8CE4C9609E6FD54B78,
	ReaderWriterLockSlim_get_IsUpgradeableReadLockHeld_mCD549D784C4E29D93D7D1DC7F87D07BBD2B054E4,
	ReaderWriterLockSlim_get_IsWriteLockHeld_m76BAF63331AE3EA08A46001A77C3B8ED95E6FE25,
	ReaderWriterLockSlim_get_RecursiveReadCount_m4F403802E0535224A75B6ACB914795E3255EBE6C,
	ReaderWriterLockSlim_get_RecursiveUpgradeCount_m57C446384AA3302B1D8551FDF094E0614B43D507,
	ReaderWriterLockSlim_get_RecursiveWriteCount_mB157295491E285DF2269906A16A90F972474367A,
	ReaderWriterLockSlim_get_WaitingReadCount_m7ABA9E4099E3A845BA69B2DE31F1620468A96168,
	ReaderWriterLockSlim_get_WaitingUpgradeCount_m6427A931CE9CD0A0A759ECE9FB89A2953D53F679,
	ReaderWriterLockSlim_get_WaitingWriteCount_m4C34DCD34734878CF89D94EFAED6772AC5C803EE,
	TimeoutTracker__ctor_mD01DAC1C1322B0E6DB8D847D6BB02DF0DEC14553,
	TimeoutTracker_get_RemainingMilliseconds_m2DF873714961B9BED784F45364703221E4259F04,
	TimeoutTracker_get_IsExpired_mE4ED4EDAA3A57C06198F451E21D347640DB1EA38,
	AesManaged__ctor_m79644F6BCD0E8C2D8BAF1B1E22E90D3C364F5C57,
	AesManaged_get_FeedbackSize_mCFE4C56DFF81F5E616CE535AB7D9E37DC1B7A937,
	AesManaged_set_FeedbackSize_mD898DD8078EA74C68D8DA8D461E79ABE7BF0BDE2,
	AesManaged_get_IV_mB1D7896A5F5E71B8B7938A5DF3A743FC2E444018,
	AesManaged_set_IV_m1DBDC4FDAE66A5F2FA99AA4A4E76769BB8897D1E,
	AesManaged_get_Key_m4CC3B2D28A918B935AD42F3F8D54E93A6CB2FA31,
	AesManaged_set_Key_m35D61E5FD8942054840B1F24E685E91E3E6CA6E1,
	AesManaged_get_KeySize_mBE6EA533BD5978099974A74FF3DE3ECB8B173CD6,
	AesManaged_set_KeySize_m2003A2B9200003C23B544F56E949A0630AA87F93,
	AesManaged_get_Mode_mF9D7222B2AB685AC46F4564B6F2247114244AEF6,
	AesManaged_set_Mode_mA5CF4C1F3B41503C6E09373ADB0B8983A6F61460,
	AesManaged_get_Padding_mD81B3F96D3421F6CD2189A01D65736A9098ACD45,
	AesManaged_set_Padding_m6B07EC4A0F1F451417DC0AC64E9D637D7916866B,
	AesManaged_CreateDecryptor_m41AE4428FE60C9FD485640F3A09F1BF345452A3C,
	AesManaged_CreateDecryptor_m7240F8C38B99CE73159DE7455046E951C4900268,
	AesManaged_CreateEncryptor_mB2BBCAB8753A59FFB572091D2EF80F287CD951BF,
	AesManaged_CreateEncryptor_m1E4EB80DE75FCF9E940228E1D7664C0EA1378153,
	AesManaged_Dispose_mB0D969841D51825F37095A93E73A50C15C1A1477,
	AesManaged_GenerateIV_mBB19651CC37782273A882055D4E63370268F2D91,
	AesManaged_GenerateKey_mF6673B955AE82377595277C6B78C7DA8A16F480E,
	AesCryptoServiceProvider__ctor_mA9857852BC34D8AB0F463C1AF1837CBBD9102265,
	AesCryptoServiceProvider_GenerateIV_m18539D5136BA9A2FC71F439150D16E35AD3BF5C4,
	AesCryptoServiceProvider_GenerateKey_m574F877FD23D1F07033FC035E89BE232303F3502,
	AesCryptoServiceProvider_CreateDecryptor_mAB5FB857F549A86D986461C8665BE6B2393305D1,
	AesCryptoServiceProvider_CreateEncryptor_m6BF20D5D8424DB627CD3010D9E4C8555C6BD0465,
	AesCryptoServiceProvider_get_IV_m6A46F1C255ABE41F98BEE8C0C37D6AFBB9F29D34,
	AesCryptoServiceProvider_set_IV_mCB88C0F651B17F3EC7575F16E14C9E3BD2DB24DB,
	AesCryptoServiceProvider_get_Key_mAC979BC922E8F1F15B36220E77972AC9CE5D5252,
	AesCryptoServiceProvider_set_Key_m65785032C270005BC120157A0C9D019F6F6BC96F,
	AesCryptoServiceProvider_get_KeySize_m3081171DF6C11CA55ECEBA29B9559D18E78D8058,
	AesCryptoServiceProvider_set_KeySize_mA994D2D3098216C0B8C4F02C0F0A0F63D4256218,
	AesCryptoServiceProvider_get_FeedbackSize_m9DC2E1C3E84CC674ADB2D7E6B06066F333BEC89D,
	AesCryptoServiceProvider_set_FeedbackSize_m5B367A05D9F985C7C83425637637B840858C255D,
	AesCryptoServiceProvider_get_Mode_m3E1CBFD4D7CE748F3AB615EB88DE1A5D7238285D,
	AesCryptoServiceProvider_set_Mode_mFE7044929761BABE312D1146B0ED51B331E35D63,
	AesCryptoServiceProvider_get_Padding_m89D49B05949BA2C6C557EFA5211B4934D279C7AD,
	AesCryptoServiceProvider_set_Padding_mD3353CD8F4B931AA00203000140520775643F96E,
	AesCryptoServiceProvider_CreateDecryptor_mB1F90A7339DA65542795E17DF9C37810BD088DDF,
	AesCryptoServiceProvider_CreateEncryptor_m9555DFFCA344DF06C8B88DDE2EB987B3958EC6BB,
	AesCryptoServiceProvider_Dispose_m7123198904819E2BF2B1398E20047B316C3D7D1E,
	AesTransform__ctor_m3903A599E8B2C3F7AB3B70E1258980151D639598,
	AesTransform_ECB_m2E2F4E2B307B0D34FEADF38684007E622FCEDFD1,
	AesTransform_SubByte_m2D77D545ABD3D84C04741B80ABB74BEFE8C55679,
	AesTransform_Encrypt128_m57DA74A7E05818DFD92F2614F8F65B0D1E696129,
	AesTransform_Decrypt128_m075F7BA40A4CFECA6F6A379065B731586EDDB23A,
	AesTransform__cctor_mAC6D46ED54345C2D23DFCA026C69029757222CFD,
	Error_ArgumentNull_m0EDA0D46D72CA692518E3E2EB75B48044D8FD41E,
	Error_ArgumentOutOfRange_m2EFB999454161A6B48F8DAC3753FDC190538F0F2,
	Error_MoreThanOneElement_mDB56C9FA4C344A86553D6AFB66D10B290302C53A,
	Error_MoreThanOneMatch_m4C4756AF34A76EF12F3B2B6D8C78DE547F0FBCF8,
	Error_NoElements_mB89E91246572F009281D79730950808F17C3F353,
	Error_NotSupported_m51A0560ABF374B66CF6D1208DAF27C4CBAD9AABA,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	Enumerable_Sum_m5A80A2FF702FA59CBEDB5AAEDB98FE91BF8B557A,
	NULL,
	Enumerable_Max_m2E60496646FFAAB20A13DEE9F52EC21F0054B72B,
	Enumerable_Average_mCC66070D3714E5082DD32AAB83A2E6F158241BB6,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
};
extern void TimeoutTracker__ctor_mD01DAC1C1322B0E6DB8D847D6BB02DF0DEC14553_AdjustorThunk (void);
extern void TimeoutTracker_get_RemainingMilliseconds_m2DF873714961B9BED784F45364703221E4259F04_AdjustorThunk (void);
extern void TimeoutTracker_get_IsExpired_mE4ED4EDAA3A57C06198F451E21D347640DB1EA38_AdjustorThunk (void);
static Il2CppTokenAdjustorThunkPair s_adjustorThunks[3] = 
{
	{ 0x06000033, TimeoutTracker__ctor_mD01DAC1C1322B0E6DB8D847D6BB02DF0DEC14553_AdjustorThunk },
	{ 0x06000034, TimeoutTracker_get_RemainingMilliseconds_m2DF873714961B9BED784F45364703221E4259F04_AdjustorThunk },
	{ 0x06000035, TimeoutTracker_get_IsExpired_mE4ED4EDAA3A57C06198F451E21D347640DB1EA38_AdjustorThunk },
};
static const int32_t s_InvokerIndices[340] = 
{
	5066,
	3479,
	3479,
	2870,
	5111,
	2545,
	2302,
	3479,
	2528,
	2628,
	2628,
	3479,
	2528,
	2628,
	2628,
	3479,
	2528,
	2628,
	2628,
	3479,
	3479,
	3479,
	1470,
	677,
	3479,
	3479,
	3479,
	3451,
	3479,
	3479,
	3479,
	3479,
	3479,
	3479,
	3397,
	3479,
	3479,
	3479,
	5156,
	3479,
	2920,
	3451,
	3451,
	3451,
	3397,
	3397,
	3397,
	3397,
	3397,
	3397,
	2870,
	3397,
	3451,
	3479,
	3397,
	2870,
	3414,
	2887,
	3414,
	2887,
	3397,
	2870,
	3397,
	2870,
	3397,
	2870,
	3414,
	1315,
	3414,
	1315,
	2920,
	3479,
	3479,
	3479,
	3479,
	3479,
	1315,
	1315,
	3414,
	2887,
	3414,
	2887,
	3397,
	2870,
	3397,
	2870,
	3397,
	2870,
	3397,
	2870,
	3414,
	3414,
	2920,
	794,
	1747,
	2140,
	1142,
	1142,
	5226,
	5066,
	5066,
	5208,
	5208,
	5208,
	5208,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	5010,
	-1,
	5126,
	5126,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
};
static const Il2CppTokenRangePair s_rgctxIndices[75] = 
{
	{ 0x0200000C, { 113, 4 } },
	{ 0x0200000D, { 117, 9 } },
	{ 0x0200000E, { 128, 7 } },
	{ 0x0200000F, { 137, 10 } },
	{ 0x02000010, { 149, 11 } },
	{ 0x02000011, { 163, 9 } },
	{ 0x02000012, { 175, 12 } },
	{ 0x02000013, { 190, 1 } },
	{ 0x02000014, { 191, 2 } },
	{ 0x02000015, { 193, 12 } },
	{ 0x02000016, { 205, 9 } },
	{ 0x02000017, { 214, 12 } },
	{ 0x02000018, { 226, 6 } },
	{ 0x02000019, { 232, 4 } },
	{ 0x0200001A, { 236, 2 } },
	{ 0x0200001B, { 238, 4 } },
	{ 0x0200001C, { 242, 3 } },
	{ 0x0200001F, { 245, 17 } },
	{ 0x02000020, { 266, 5 } },
	{ 0x02000021, { 271, 1 } },
	{ 0x02000023, { 272, 8 } },
	{ 0x02000025, { 280, 4 } },
	{ 0x02000026, { 284, 3 } },
	{ 0x02000027, { 289, 5 } },
	{ 0x02000028, { 294, 7 } },
	{ 0x02000029, { 301, 3 } },
	{ 0x0200002A, { 304, 7 } },
	{ 0x0200002B, { 311, 4 } },
	{ 0x0200002C, { 315, 21 } },
	{ 0x0200002E, { 336, 2 } },
	{ 0x0600006A, { 0, 10 } },
	{ 0x0600006B, { 10, 10 } },
	{ 0x0600006C, { 20, 5 } },
	{ 0x0600006D, { 25, 5 } },
	{ 0x0600006E, { 30, 1 } },
	{ 0x0600006F, { 31, 2 } },
	{ 0x06000070, { 33, 2 } },
	{ 0x06000071, { 35, 1 } },
	{ 0x06000072, { 36, 4 } },
	{ 0x06000073, { 40, 1 } },
	{ 0x06000074, { 41, 2 } },
	{ 0x06000075, { 43, 1 } },
	{ 0x06000076, { 44, 2 } },
	{ 0x06000077, { 46, 3 } },
	{ 0x06000078, { 49, 2 } },
	{ 0x06000079, { 51, 1 } },
	{ 0x0600007A, { 52, 7 } },
	{ 0x0600007B, { 59, 2 } },
	{ 0x0600007C, { 61, 2 } },
	{ 0x0600007D, { 63, 4 } },
	{ 0x0600007E, { 67, 4 } },
	{ 0x0600007F, { 71, 4 } },
	{ 0x06000080, { 75, 3 } },
	{ 0x06000081, { 78, 4 } },
	{ 0x06000082, { 82, 4 } },
	{ 0x06000083, { 86, 3 } },
	{ 0x06000084, { 89, 1 } },
	{ 0x06000085, { 90, 2 } },
	{ 0x06000086, { 92, 1 } },
	{ 0x06000087, { 93, 1 } },
	{ 0x06000088, { 94, 3 } },
	{ 0x06000089, { 97, 3 } },
	{ 0x0600008A, { 100, 2 } },
	{ 0x0600008B, { 102, 2 } },
	{ 0x0600008C, { 104, 5 } },
	{ 0x0600008D, { 109, 3 } },
	{ 0x0600008F, { 112, 1 } },
	{ 0x060000A1, { 126, 2 } },
	{ 0x060000A6, { 135, 2 } },
	{ 0x060000AB, { 147, 2 } },
	{ 0x060000B1, { 160, 3 } },
	{ 0x060000B6, { 172, 3 } },
	{ 0x060000BB, { 187, 3 } },
	{ 0x060000F6, { 262, 4 } },
	{ 0x06000124, { 287, 2 } },
};
static const Il2CppRGCTXDefinition s_rgctxValues[338] = 
{
	{ (Il2CppRGCTXDataType)2, 3486 },
	{ (Il2CppRGCTXDataType)3, 11889 },
	{ (Il2CppRGCTXDataType)2, 5290 },
	{ (Il2CppRGCTXDataType)2, 4817 },
	{ (Il2CppRGCTXDataType)3, 21744 },
	{ (Il2CppRGCTXDataType)2, 3633 },
	{ (Il2CppRGCTXDataType)2, 4824 },
	{ (Il2CppRGCTXDataType)3, 21787 },
	{ (Il2CppRGCTXDataType)2, 4819 },
	{ (Il2CppRGCTXDataType)3, 21758 },
	{ (Il2CppRGCTXDataType)2, 3487 },
	{ (Il2CppRGCTXDataType)3, 11890 },
	{ (Il2CppRGCTXDataType)2, 5336 },
	{ (Il2CppRGCTXDataType)2, 4826 },
	{ (Il2CppRGCTXDataType)3, 21801 },
	{ (Il2CppRGCTXDataType)2, 3651 },
	{ (Il2CppRGCTXDataType)2, 4834 },
	{ (Il2CppRGCTXDataType)3, 21875 },
	{ (Il2CppRGCTXDataType)2, 4830 },
	{ (Il2CppRGCTXDataType)3, 21835 },
	{ (Il2CppRGCTXDataType)2, 1255 },
	{ (Il2CppRGCTXDataType)3, 468 },
	{ (Il2CppRGCTXDataType)3, 469 },
	{ (Il2CppRGCTXDataType)2, 2246 },
	{ (Il2CppRGCTXDataType)3, 9373 },
	{ (Il2CppRGCTXDataType)2, 1262 },
	{ (Il2CppRGCTXDataType)3, 508 },
	{ (Il2CppRGCTXDataType)3, 509 },
	{ (Il2CppRGCTXDataType)2, 2270 },
	{ (Il2CppRGCTXDataType)3, 9383 },
	{ (Il2CppRGCTXDataType)3, 25219 },
	{ (Il2CppRGCTXDataType)2, 1287 },
	{ (Il2CppRGCTXDataType)3, 724 },
	{ (Il2CppRGCTXDataType)2, 4266 },
	{ (Il2CppRGCTXDataType)3, 18630 },
	{ (Il2CppRGCTXDataType)3, 10258 },
	{ (Il2CppRGCTXDataType)3, 10301 },
	{ (Il2CppRGCTXDataType)2, 3458 },
	{ (Il2CppRGCTXDataType)2, 2552 },
	{ (Il2CppRGCTXDataType)3, 10091 },
	{ (Il2CppRGCTXDataType)3, 25154 },
	{ (Il2CppRGCTXDataType)2, 1271 },
	{ (Il2CppRGCTXDataType)3, 579 },
	{ (Il2CppRGCTXDataType)3, 25271 },
	{ (Il2CppRGCTXDataType)2, 1291 },
	{ (Il2CppRGCTXDataType)3, 760 },
	{ (Il2CppRGCTXDataType)2, 1579 },
	{ (Il2CppRGCTXDataType)3, 2284 },
	{ (Il2CppRGCTXDataType)3, 2285 },
	{ (Il2CppRGCTXDataType)2, 3634 },
	{ (Il2CppRGCTXDataType)3, 12913 },
	{ (Il2CppRGCTXDataType)3, 25253 },
	{ (Il2CppRGCTXDataType)2, 1759 },
	{ (Il2CppRGCTXDataType)3, 3594 },
	{ (Il2CppRGCTXDataType)2, 2827 },
	{ (Il2CppRGCTXDataType)2, 2992 },
	{ (Il2CppRGCTXDataType)3, 9381 },
	{ (Il2CppRGCTXDataType)3, 9382 },
	{ (Il2CppRGCTXDataType)3, 3595 },
	{ (Il2CppRGCTXDataType)2, 2780 },
	{ (Il2CppRGCTXDataType)3, 25131 },
	{ (Il2CppRGCTXDataType)2, 1269 },
	{ (Il2CppRGCTXDataType)3, 565 },
	{ (Il2CppRGCTXDataType)2, 3268 },
	{ (Il2CppRGCTXDataType)2, 2582 },
	{ (Il2CppRGCTXDataType)2, 2793 },
	{ (Il2CppRGCTXDataType)2, 2979 },
	{ (Il2CppRGCTXDataType)2, 3269 },
	{ (Il2CppRGCTXDataType)2, 2583 },
	{ (Il2CppRGCTXDataType)2, 2794 },
	{ (Il2CppRGCTXDataType)2, 2980 },
	{ (Il2CppRGCTXDataType)2, 3270 },
	{ (Il2CppRGCTXDataType)2, 2584 },
	{ (Il2CppRGCTXDataType)2, 2795 },
	{ (Il2CppRGCTXDataType)2, 2981 },
	{ (Il2CppRGCTXDataType)2, 2796 },
	{ (Il2CppRGCTXDataType)2, 2982 },
	{ (Il2CppRGCTXDataType)3, 9374 },
	{ (Il2CppRGCTXDataType)2, 3271 },
	{ (Il2CppRGCTXDataType)2, 2585 },
	{ (Il2CppRGCTXDataType)2, 2797 },
	{ (Il2CppRGCTXDataType)2, 2983 },
	{ (Il2CppRGCTXDataType)2, 3272 },
	{ (Il2CppRGCTXDataType)2, 2586 },
	{ (Il2CppRGCTXDataType)2, 2798 },
	{ (Il2CppRGCTXDataType)2, 2984 },
	{ (Il2CppRGCTXDataType)2, 2799 },
	{ (Il2CppRGCTXDataType)2, 2985 },
	{ (Il2CppRGCTXDataType)3, 9375 },
	{ (Il2CppRGCTXDataType)3, 25202 },
	{ (Il2CppRGCTXDataType)2, 1281 },
	{ (Il2CppRGCTXDataType)3, 709 },
	{ (Il2CppRGCTXDataType)2, 1983 },
	{ (Il2CppRGCTXDataType)2, 2776 },
	{ (Il2CppRGCTXDataType)2, 2777 },
	{ (Il2CppRGCTXDataType)2, 2977 },
	{ (Il2CppRGCTXDataType)3, 9372 },
	{ (Il2CppRGCTXDataType)2, 2775 },
	{ (Il2CppRGCTXDataType)2, 2976 },
	{ (Il2CppRGCTXDataType)3, 9371 },
	{ (Il2CppRGCTXDataType)2, 2581 },
	{ (Il2CppRGCTXDataType)2, 2791 },
	{ (Il2CppRGCTXDataType)2, 2580 },
	{ (Il2CppRGCTXDataType)3, 25161 },
	{ (Il2CppRGCTXDataType)3, 8541 },
	{ (Il2CppRGCTXDataType)2, 2060 },
	{ (Il2CppRGCTXDataType)2, 2779 },
	{ (Il2CppRGCTXDataType)2, 2978 },
	{ (Il2CppRGCTXDataType)2, 3122 },
	{ (Il2CppRGCTXDataType)2, 2825 },
	{ (Il2CppRGCTXDataType)2, 2991 },
	{ (Il2CppRGCTXDataType)3, 9594 },
	{ (Il2CppRGCTXDataType)3, 25204 },
	{ (Il2CppRGCTXDataType)3, 11891 },
	{ (Il2CppRGCTXDataType)3, 11893 },
	{ (Il2CppRGCTXDataType)2, 698 },
	{ (Il2CppRGCTXDataType)3, 11892 },
	{ (Il2CppRGCTXDataType)3, 11901 },
	{ (Il2CppRGCTXDataType)2, 3490 },
	{ (Il2CppRGCTXDataType)2, 4820 },
	{ (Il2CppRGCTXDataType)3, 21759 },
	{ (Il2CppRGCTXDataType)3, 11902 },
	{ (Il2CppRGCTXDataType)2, 2871 },
	{ (Il2CppRGCTXDataType)2, 3027 },
	{ (Il2CppRGCTXDataType)3, 9398 },
	{ (Il2CppRGCTXDataType)3, 25134 },
	{ (Il2CppRGCTXDataType)2, 4831 },
	{ (Il2CppRGCTXDataType)3, 21836 },
	{ (Il2CppRGCTXDataType)3, 11894 },
	{ (Il2CppRGCTXDataType)2, 3489 },
	{ (Il2CppRGCTXDataType)2, 4818 },
	{ (Il2CppRGCTXDataType)3, 21745 },
	{ (Il2CppRGCTXDataType)3, 9397 },
	{ (Il2CppRGCTXDataType)3, 11895 },
	{ (Il2CppRGCTXDataType)3, 25133 },
	{ (Il2CppRGCTXDataType)2, 4827 },
	{ (Il2CppRGCTXDataType)3, 21802 },
	{ (Il2CppRGCTXDataType)3, 11908 },
	{ (Il2CppRGCTXDataType)2, 3491 },
	{ (Il2CppRGCTXDataType)2, 4825 },
	{ (Il2CppRGCTXDataType)3, 21788 },
	{ (Il2CppRGCTXDataType)3, 12965 },
	{ (Il2CppRGCTXDataType)3, 7031 },
	{ (Il2CppRGCTXDataType)3, 9399 },
	{ (Il2CppRGCTXDataType)3, 7030 },
	{ (Il2CppRGCTXDataType)3, 11909 },
	{ (Il2CppRGCTXDataType)3, 25135 },
	{ (Il2CppRGCTXDataType)2, 4835 },
	{ (Il2CppRGCTXDataType)3, 21876 },
	{ (Il2CppRGCTXDataType)3, 11922 },
	{ (Il2CppRGCTXDataType)2, 3493 },
	{ (Il2CppRGCTXDataType)2, 4833 },
	{ (Il2CppRGCTXDataType)3, 21838 },
	{ (Il2CppRGCTXDataType)3, 11923 },
	{ (Il2CppRGCTXDataType)2, 2874 },
	{ (Il2CppRGCTXDataType)2, 3030 },
	{ (Il2CppRGCTXDataType)3, 9403 },
	{ (Il2CppRGCTXDataType)3, 9402 },
	{ (Il2CppRGCTXDataType)2, 4822 },
	{ (Il2CppRGCTXDataType)3, 21761 },
	{ (Il2CppRGCTXDataType)3, 25141 },
	{ (Il2CppRGCTXDataType)2, 4832 },
	{ (Il2CppRGCTXDataType)3, 21837 },
	{ (Il2CppRGCTXDataType)3, 11915 },
	{ (Il2CppRGCTXDataType)2, 3492 },
	{ (Il2CppRGCTXDataType)2, 4829 },
	{ (Il2CppRGCTXDataType)3, 21804 },
	{ (Il2CppRGCTXDataType)3, 9401 },
	{ (Il2CppRGCTXDataType)3, 9400 },
	{ (Il2CppRGCTXDataType)3, 11916 },
	{ (Il2CppRGCTXDataType)2, 4821 },
	{ (Il2CppRGCTXDataType)3, 21760 },
	{ (Il2CppRGCTXDataType)3, 25140 },
	{ (Il2CppRGCTXDataType)2, 4828 },
	{ (Il2CppRGCTXDataType)3, 21803 },
	{ (Il2CppRGCTXDataType)3, 11929 },
	{ (Il2CppRGCTXDataType)2, 3494 },
	{ (Il2CppRGCTXDataType)2, 4837 },
	{ (Il2CppRGCTXDataType)3, 21878 },
	{ (Il2CppRGCTXDataType)3, 12966 },
	{ (Il2CppRGCTXDataType)3, 7033 },
	{ (Il2CppRGCTXDataType)3, 9405 },
	{ (Il2CppRGCTXDataType)3, 9404 },
	{ (Il2CppRGCTXDataType)3, 7032 },
	{ (Il2CppRGCTXDataType)3, 11930 },
	{ (Il2CppRGCTXDataType)2, 4823 },
	{ (Il2CppRGCTXDataType)3, 21762 },
	{ (Il2CppRGCTXDataType)3, 25142 },
	{ (Il2CppRGCTXDataType)2, 4836 },
	{ (Il2CppRGCTXDataType)3, 21877 },
	{ (Il2CppRGCTXDataType)3, 9394 },
	{ (Il2CppRGCTXDataType)3, 9395 },
	{ (Il2CppRGCTXDataType)3, 9414 },
	{ (Il2CppRGCTXDataType)3, 727 },
	{ (Il2CppRGCTXDataType)3, 726 },
	{ (Il2CppRGCTXDataType)2, 2863 },
	{ (Il2CppRGCTXDataType)2, 3021 },
	{ (Il2CppRGCTXDataType)3, 9396 },
	{ (Il2CppRGCTXDataType)2, 2889 },
	{ (Il2CppRGCTXDataType)2, 3049 },
	{ (Il2CppRGCTXDataType)3, 729 },
	{ (Il2CppRGCTXDataType)2, 985 },
	{ (Il2CppRGCTXDataType)2, 1288 },
	{ (Il2CppRGCTXDataType)3, 725 },
	{ (Il2CppRGCTXDataType)3, 728 },
	{ (Il2CppRGCTXDataType)3, 581 },
	{ (Il2CppRGCTXDataType)3, 582 },
	{ (Il2CppRGCTXDataType)2, 2859 },
	{ (Il2CppRGCTXDataType)2, 3018 },
	{ (Il2CppRGCTXDataType)3, 584 },
	{ (Il2CppRGCTXDataType)2, 690 },
	{ (Il2CppRGCTXDataType)2, 1272 },
	{ (Il2CppRGCTXDataType)3, 580 },
	{ (Il2CppRGCTXDataType)3, 583 },
	{ (Il2CppRGCTXDataType)3, 762 },
	{ (Il2CppRGCTXDataType)3, 763 },
	{ (Il2CppRGCTXDataType)2, 4400 },
	{ (Il2CppRGCTXDataType)3, 19453 },
	{ (Il2CppRGCTXDataType)2, 2866 },
	{ (Il2CppRGCTXDataType)2, 3023 },
	{ (Il2CppRGCTXDataType)3, 19454 },
	{ (Il2CppRGCTXDataType)3, 765 },
	{ (Il2CppRGCTXDataType)2, 696 },
	{ (Il2CppRGCTXDataType)2, 1292 },
	{ (Il2CppRGCTXDataType)3, 761 },
	{ (Il2CppRGCTXDataType)3, 764 },
	{ (Il2CppRGCTXDataType)3, 567 },
	{ (Il2CppRGCTXDataType)2, 688 },
	{ (Il2CppRGCTXDataType)3, 569 },
	{ (Il2CppRGCTXDataType)2, 1270 },
	{ (Il2CppRGCTXDataType)3, 566 },
	{ (Il2CppRGCTXDataType)3, 568 },
	{ (Il2CppRGCTXDataType)2, 692 },
	{ (Il2CppRGCTXDataType)2, 1282 },
	{ (Il2CppRGCTXDataType)3, 710 },
	{ (Il2CppRGCTXDataType)3, 711 },
	{ (Il2CppRGCTXDataType)2, 5349 },
	{ (Il2CppRGCTXDataType)2, 1984 },
	{ (Il2CppRGCTXDataType)2, 1157 },
	{ (Il2CppRGCTXDataType)3, 0 },
	{ (Il2CppRGCTXDataType)2, 2287 },
	{ (Il2CppRGCTXDataType)3, 9386 },
	{ (Il2CppRGCTXDataType)2, 1161 },
	{ (Il2CppRGCTXDataType)3, 4 },
	{ (Il2CppRGCTXDataType)2, 1161 },
	{ (Il2CppRGCTXDataType)2, 4124 },
	{ (Il2CppRGCTXDataType)3, 17237 },
	{ (Il2CppRGCTXDataType)3, 17239 },
	{ (Il2CppRGCTXDataType)3, 10097 },
	{ (Il2CppRGCTXDataType)3, 8568 },
	{ (Il2CppRGCTXDataType)2, 2073 },
	{ (Il2CppRGCTXDataType)2, 5418 },
	{ (Il2CppRGCTXDataType)2, 1277 },
	{ (Il2CppRGCTXDataType)3, 678 },
	{ (Il2CppRGCTXDataType)3, 17238 },
	{ (Il2CppRGCTXDataType)2, 574 },
	{ (Il2CppRGCTXDataType)2, 3137 },
	{ (Il2CppRGCTXDataType)3, 17240 },
	{ (Il2CppRGCTXDataType)3, 17241 },
	{ (Il2CppRGCTXDataType)2, 2553 },
	{ (Il2CppRGCTXDataType)3, 10096 },
	{ (Il2CppRGCTXDataType)2, 5404 },
	{ (Il2CppRGCTXDataType)2, 2808 },
	{ (Il2CppRGCTXDataType)2, 2990 },
	{ (Il2CppRGCTXDataType)3, 9378 },
	{ (Il2CppRGCTXDataType)3, 9379 },
	{ (Il2CppRGCTXDataType)3, 24613 },
	{ (Il2CppRGCTXDataType)2, 1280 },
	{ (Il2CppRGCTXDataType)3, 702 },
	{ (Il2CppRGCTXDataType)3, 10098 },
	{ (Il2CppRGCTXDataType)3, 22132 },
	{ (Il2CppRGCTXDataType)2, 1040 },
	{ (Il2CppRGCTXDataType)3, 8576 },
	{ (Il2CppRGCTXDataType)2, 2076 },
	{ (Il2CppRGCTXDataType)2, 5438 },
	{ (Il2CppRGCTXDataType)3, 19450 },
	{ (Il2CppRGCTXDataType)3, 19451 },
	{ (Il2CppRGCTXDataType)2, 3143 },
	{ (Il2CppRGCTXDataType)3, 19452 },
	{ (Il2CppRGCTXDataType)2, 600 },
	{ (Il2CppRGCTXDataType)3, 17242 },
	{ (Il2CppRGCTXDataType)2, 4126 },
	{ (Il2CppRGCTXDataType)3, 17243 },
	{ (Il2CppRGCTXDataType)3, 10092 },
	{ (Il2CppRGCTXDataType)2, 1276 },
	{ (Il2CppRGCTXDataType)3, 671 },
	{ (Il2CppRGCTXDataType)3, 18617 },
	{ (Il2CppRGCTXDataType)2, 4267 },
	{ (Il2CppRGCTXDataType)3, 18631 },
	{ (Il2CppRGCTXDataType)2, 1580 },
	{ (Il2CppRGCTXDataType)3, 2286 },
	{ (Il2CppRGCTXDataType)3, 18623 },
	{ (Il2CppRGCTXDataType)3, 7007 },
	{ (Il2CppRGCTXDataType)2, 747 },
	{ (Il2CppRGCTXDataType)3, 18618 },
	{ (Il2CppRGCTXDataType)2, 4263 },
	{ (Il2CppRGCTXDataType)3, 2548 },
	{ (Il2CppRGCTXDataType)2, 1613 },
	{ (Il2CppRGCTXDataType)2, 2027 },
	{ (Il2CppRGCTXDataType)3, 7013 },
	{ (Il2CppRGCTXDataType)3, 18619 },
	{ (Il2CppRGCTXDataType)3, 7002 },
	{ (Il2CppRGCTXDataType)3, 7003 },
	{ (Il2CppRGCTXDataType)3, 7001 },
	{ (Il2CppRGCTXDataType)3, 7004 },
	{ (Il2CppRGCTXDataType)2, 2023 },
	{ (Il2CppRGCTXDataType)2, 5402 },
	{ (Il2CppRGCTXDataType)3, 9385 },
	{ (Il2CppRGCTXDataType)3, 7006 },
	{ (Il2CppRGCTXDataType)2, 2714 },
	{ (Il2CppRGCTXDataType)3, 7005 },
	{ (Il2CppRGCTXDataType)2, 2591 },
	{ (Il2CppRGCTXDataType)2, 5341 },
	{ (Il2CppRGCTXDataType)2, 2830 },
	{ (Il2CppRGCTXDataType)2, 2995 },
	{ (Il2CppRGCTXDataType)3, 8557 },
	{ (Il2CppRGCTXDataType)2, 2068 },
	{ (Il2CppRGCTXDataType)3, 10115 },
	{ (Il2CppRGCTXDataType)3, 10116 },
	{ (Il2CppRGCTXDataType)3, 10121 },
	{ (Il2CppRGCTXDataType)2, 3132 },
	{ (Il2CppRGCTXDataType)3, 10118 },
	{ (Il2CppRGCTXDataType)3, 26034 },
	{ (Il2CppRGCTXDataType)2, 2028 },
	{ (Il2CppRGCTXDataType)3, 7023 },
	{ (Il2CppRGCTXDataType)1, 2703 },
	{ (Il2CppRGCTXDataType)2, 5355 },
	{ (Il2CppRGCTXDataType)3, 10117 },
	{ (Il2CppRGCTXDataType)1, 5355 },
	{ (Il2CppRGCTXDataType)1, 3132 },
	{ (Il2CppRGCTXDataType)2, 5436 },
	{ (Il2CppRGCTXDataType)2, 5355 },
	{ (Il2CppRGCTXDataType)3, 10122 },
	{ (Il2CppRGCTXDataType)3, 10120 },
	{ (Il2CppRGCTXDataType)3, 10119 },
	{ (Il2CppRGCTXDataType)2, 537 },
	{ (Il2CppRGCTXDataType)3, 7034 },
	{ (Il2CppRGCTXDataType)2, 711 },
};
extern const CustomAttributesCacheGenerator g_System_Core_AttributeGenerators[];
IL2CPP_EXTERN_C const Il2CppCodeGenModule g_System_Core_CodeGenModule;
const Il2CppCodeGenModule g_System_Core_CodeGenModule = 
{
	"System.Core.dll",
	340,
	s_methodPointers,
	3,
	s_adjustorThunks,
	s_InvokerIndices,
	0,
	NULL,
	75,
	s_rgctxIndices,
	338,
	s_rgctxValues,
	NULL,
	g_System_Core_AttributeGenerators,
	NULL, // module initializer,
	NULL,
	NULL,
	NULL,
};
