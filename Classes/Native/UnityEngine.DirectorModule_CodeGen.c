﻿#include "pch-c.h"
#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include "codegen/il2cpp-codegen-metadata.h"





// 0x00000001 System.Void UnityEngine.Playables.PlayableDirector::set_extrapolationMode(UnityEngine.Playables.DirectorWrapMode)
extern void PlayableDirector_set_extrapolationMode_mAC2154C44E1B1FA62A6B85A39463BEF05538ED42 (void);
// 0x00000002 UnityEngine.Playables.DirectorWrapMode UnityEngine.Playables.PlayableDirector::get_extrapolationMode()
extern void PlayableDirector_get_extrapolationMode_m12388157053D75727AC7725F43FDAEBDD70EA212 (void);
// 0x00000003 UnityEngine.Playables.PlayableAsset UnityEngine.Playables.PlayableDirector::get_playableAsset()
extern void PlayableDirector_get_playableAsset_mAD7C43BF96CA5F6553D52DA32C2D14C8501CE45A (void);
// 0x00000004 System.Void UnityEngine.Playables.PlayableDirector::set_playableAsset(UnityEngine.Playables.PlayableAsset)
extern void PlayableDirector_set_playableAsset_m6A7954E07B87A8764468294D1264572301656CE3 (void);
// 0x00000005 System.Void UnityEngine.Playables.PlayableDirector::Play(UnityEngine.Playables.PlayableAsset)
extern void PlayableDirector_Play_m3B2FB884B24E5571D9A111B205406FBF6594D248 (void);
// 0x00000006 System.Void UnityEngine.Playables.PlayableDirector::Play(UnityEngine.Playables.PlayableAsset,UnityEngine.Playables.DirectorWrapMode)
extern void PlayableDirector_Play_m8651F01443534ED0077E3797807A9941E65BEF58 (void);
// 0x00000007 System.Void UnityEngine.Playables.PlayableDirector::Play()
extern void PlayableDirector_Play_m13D31AD720EEE25E93A21394B225EA10300C47C4 (void);
// 0x00000008 UnityEngine.Object UnityEngine.Playables.PlayableDirector::GetGenericBinding(UnityEngine.Object)
extern void PlayableDirector_GetGenericBinding_m38A4F65D838B0E0E4E5D96A7A648FBDAAB62ADD8 (void);
// 0x00000009 System.Void UnityEngine.Playables.PlayableDirector::SetWrapMode(UnityEngine.Playables.DirectorWrapMode)
extern void PlayableDirector_SetWrapMode_m0DC5E14F7F407E11C3CAA04769E712100548FED8 (void);
// 0x0000000A UnityEngine.Playables.DirectorWrapMode UnityEngine.Playables.PlayableDirector::GetWrapMode()
extern void PlayableDirector_GetWrapMode_mFCC5A829BF7AF2AB38AE1C9B34E1B153201D075B (void);
// 0x0000000B System.Void UnityEngine.Playables.PlayableDirector::SetPlayableAsset(UnityEngine.ScriptableObject)
extern void PlayableDirector_SetPlayableAsset_mF4017B7571D11515B2604CAAEF19435DEA0BE692 (void);
// 0x0000000C UnityEngine.ScriptableObject UnityEngine.Playables.PlayableDirector::Internal_GetPlayableAsset()
extern void PlayableDirector_Internal_GetPlayableAsset_m92B5F23CCF576F9501B886D31080C8BBC3AD9BFF (void);
// 0x0000000D System.Void UnityEngine.Playables.PlayableDirector::SendOnPlayableDirectorPlay()
extern void PlayableDirector_SendOnPlayableDirectorPlay_mB5DE72647457F7E3757EAD2A39859BD835DE83FE (void);
// 0x0000000E System.Void UnityEngine.Playables.PlayableDirector::SendOnPlayableDirectorPause()
extern void PlayableDirector_SendOnPlayableDirectorPause_m6F06A18921F0F68BAE114F5DAF032CEAA77EDBC7 (void);
// 0x0000000F System.Void UnityEngine.Playables.PlayableDirector::SendOnPlayableDirectorStop()
extern void PlayableDirector_SendOnPlayableDirectorStop_m60479449EA74C565E5D7B34E15CA60030C216D08 (void);
static Il2CppMethodPointer s_methodPointers[15] = 
{
	PlayableDirector_set_extrapolationMode_mAC2154C44E1B1FA62A6B85A39463BEF05538ED42,
	PlayableDirector_get_extrapolationMode_m12388157053D75727AC7725F43FDAEBDD70EA212,
	PlayableDirector_get_playableAsset_mAD7C43BF96CA5F6553D52DA32C2D14C8501CE45A,
	PlayableDirector_set_playableAsset_m6A7954E07B87A8764468294D1264572301656CE3,
	PlayableDirector_Play_m3B2FB884B24E5571D9A111B205406FBF6594D248,
	PlayableDirector_Play_m8651F01443534ED0077E3797807A9941E65BEF58,
	PlayableDirector_Play_m13D31AD720EEE25E93A21394B225EA10300C47C4,
	PlayableDirector_GetGenericBinding_m38A4F65D838B0E0E4E5D96A7A648FBDAAB62ADD8,
	PlayableDirector_SetWrapMode_m0DC5E14F7F407E11C3CAA04769E712100548FED8,
	PlayableDirector_GetWrapMode_mFCC5A829BF7AF2AB38AE1C9B34E1B153201D075B,
	PlayableDirector_SetPlayableAsset_mF4017B7571D11515B2604CAAEF19435DEA0BE692,
	PlayableDirector_Internal_GetPlayableAsset_m92B5F23CCF576F9501B886D31080C8BBC3AD9BFF,
	PlayableDirector_SendOnPlayableDirectorPlay_mB5DE72647457F7E3757EAD2A39859BD835DE83FE,
	PlayableDirector_SendOnPlayableDirectorPause_m6F06A18921F0F68BAE114F5DAF032CEAA77EDBC7,
	PlayableDirector_SendOnPlayableDirectorStop_m60479449EA74C565E5D7B34E15CA60030C216D08,
};
static const int32_t s_InvokerIndices[15] = 
{
	2870,
	3397,
	3414,
	2887,
	2887,
	1743,
	3479,
	2300,
	2870,
	3397,
	2887,
	3414,
	3479,
	3479,
	3479,
};
extern const CustomAttributesCacheGenerator g_UnityEngine_DirectorModule_AttributeGenerators[];
IL2CPP_EXTERN_C const Il2CppCodeGenModule g_UnityEngine_DirectorModule_CodeGenModule;
const Il2CppCodeGenModule g_UnityEngine_DirectorModule_CodeGenModule = 
{
	"UnityEngine.DirectorModule.dll",
	15,
	s_methodPointers,
	0,
	NULL,
	s_InvokerIndices,
	0,
	NULL,
	0,
	NULL,
	0,
	NULL,
	NULL,
	g_UnityEngine_DirectorModule_AttributeGenerators,
	NULL, // module initializer,
	NULL,
	NULL,
	NULL,
};
